/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 20, 2004 
 * 
 */
package com.osserp.core.employees;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.jdom2.Element;

import com.osserp.core.contacts.OfficePhone;
import com.osserp.core.contacts.Phone;
import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.system.BranchOffice;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Employee extends ClassifiedContact {

    static final Long PICTURE = 7L;

    /**
     * Returns the accounting id
     * @return accountingId
     */
    Long getAccountingId();

    /**
     * Sets the accounting id
     * @param accountingId
     */
    void setAccountingId(Long accountingId);

    /**
     * Returns the default branch office
     * @return branch
     */
    BranchOffice getBranch();

    /**
     * Indicates that employee is ignoring branch
     * @return fromBranch
     */
    boolean isIgnoringBranch();

    /**
     * Indicates that user is same branch
     * @param branchId
     * @return true if employee is member of same branch
     */
    boolean isFromBranch(Long branchId);

    /**
     * Indicates if an employee is supporting a given branch.
     * @param branch
     * @return true if employee is from headquarter, same branch or <br/>
     * has a role with enabled 'ignoringBranch' property assigned
     */
    boolean isSupportingBranch(BranchOffice branch);

    /**
     * Indicates if an employee is supporting a given company.
     * @param companyId
     * @return true if employee supports company
     */
    boolean isSupportingCompany(Long companyId);

    /**
     * Indicates that employee is executive of company
     * @param companyId
     * @return true if employee is executive of company
     */
    boolean isCompanyExecutive(Long companyId);

    /**
     * Indicates that employee is executive of a company
     * @return true if employee is executive of a company
     */
    boolean isCompanyExecutive();

    /**
     * Checks whether employee is system's internal account
     * @return true if so
     */
    boolean isSystemAccount();

    /**
     * Employee type
     * @return employeeType
     */
    EmployeeType getEmployeeType();

    /**
     * Sets the employee type
     * @param employeeType
     */
    void setEmployeeType(EmployeeType employeeType);

    /**
     * Returns the internal phone
     * @return internalPhone
     */
    OfficePhone getInternalPhone();

    /**
     * Sets the internal phone
     * @param internalPhone
     */
    void setInternalPhone(OfficePhone internalPhone);

    /**
     * Removes the internal phone number
     */
    void removeInternalPhone();

    /**
     * Indicates that the employee is in active state
     * @return active
     */
    boolean isActive();

    /**
     * Sets the employee in an active/deactivated state
     * @param active
     */
    void setActive(boolean active);

    /**
     * Provides the cost center id
     * @return costCenter
     */
    String getCostCenter();

    /**
     * Sets the cost center
     * @param costCenter
     */
    void setCostCenter(String costCenter);

    /**
     * Provides the initials
     * @return initials
     */
    String getInitials();

    /**
     * Sets the initials
     * @param initials
     */
    void setInitials(String initials);

    /**
     * The begin-date of the entity.
     * @return beginDate
     */
    Date getBeginDate();

    /**
     * Sets the begin date
     * @param beginDate
     */
    void setBeginDate(Date beginDate);

    /**
     * The end-date
     * @return endDate
     */
    Date getEndDate();

    /**
     * Sets the end date
     * @param endDate
     */
    void setEndDate(Date endDate);

    /**
     * Indicates if employee has a role assigned
     * @param groupId
     * @return true if group is assigned
     */
    boolean isRoleAssigned(Long groupId);

    /**
     * Provides the default group if employee is member of more than one group
     * @return defaultGroup
     */
    EmployeeGroup getDefaultGroup();

    /**
     * Provides the default role config
     * @return default role config or null if not defined
     */
    EmployeeRoleConfig getDefaultRoleConfig();

    /**
     * Provides the default role
     * @return default role or null if not defined
     */
    EmployeeRole getDefaultRole();

    /**
     * Provides employees role configs
     * @return roleConfigs
     */
    List<EmployeeRoleConfig> getRoleConfigs();

    /**
     * Adds a new role config
     * @param user
     * @param branch
     * @param description or null
     */
    void addRoleConfig(Employee user, BranchOffice branch, String description);

    /**
     * Removes a role config
     * @param id
     */
    void removeRoleConfig(Long id);

    /**
     * Adds a new role to a config
     * @param config
     * @param user
     * @param group
     * @param status
     */
    void addRole(EmployeeRoleConfig config, Employee user, EmployeeGroup group, EmployeeStatus status);

    /**
     * Removes a role from a config
     * @param config
     * @param user
     * @param id
     */
    void removeRole(EmployeeRoleConfig config, Employee user, Long id);

    /**
     * Sets default role
     * @param config
     * @param user
     * @param id
     */
    void setDefaultRole(EmployeeRoleConfig config, Employee user, Long id);

    /**
     * Provides the phone number used in records
     * @return for printing records
     */
    Phone getPhoneForRecord();

    /**
     * Provides the printable phone device id
     * @return id
     */
    Long getPrintPhoneDevice();

    /**
     * Sets the printable phone device id
     * @param printPhoneDevice
     */
    void setPrintPhoneDevice(Long printPhoneDevice);

    /**
     * Provides the role of the employee. The role is also a candidate for signature and business card text.
     * @return role
     */
    String getRole();

    /**
     * Sets the role of the employee
     * @param role
     */
    void setRole(String role);

    /**
     * Provides the signature as configured by signatureSource or by business rule if signatureSource not configured. The business rule says that existing role
     * overrides default group.
     * @return signature string or empty string if signatureSource is 'none', is 'role' but role is not defined or signatureSource is 'group' or signatureSource
     * is null and role is empty and a group relation does not exist.
     */
    String getSignature();

    /**
     * Provides information about the source of
     * @return signatureSource
     */
    String getSignatureSource();

    /**
     * Sets the signature source of the employee
     * @param signatureSource
     */
    void setSignatureSource(String signatureSource);

    /**
     * Indicates that employee has a company email account
     * @return emailAccount
     */
    boolean isEmailAccount();

    /**
     * Sets that employee has a company email account
     * @param emailAccount
     */
    void setEmailAccount(boolean emailAccount);

    /**
     * Indicates that employee has a company email and email is already created
     * @return true if so
     */
    boolean isEmailCreated();

    /**
     * Sets that employees email is created
     * @param emailCreated
     */
    void setEmailCreated(boolean emailCreated);

    /**
     * Creates a default mail account name
     * @return mail account name
     */
    String createMailAccountName();

    /**
     * Indicates that employee gets business cards
     * @return businessCard
     */
    boolean isBusinessCard();

    void setBusinessCard(boolean businessCard);

    /**
     * Provides business card addon text
     * @return businessCardText
     */
    String getBusinessCardText();

    void setBusinessCardText(String businessCardText);

    /**
     * Indicates that employee gets international business cards
     * @return internationalBusinessCard
     */
    boolean isInternationalBusinessCard();

    void setInternationalBusinessCard(boolean internationalBusinessCard);

    /**
     * Provides international business card addon text
     * @return internationalBusinessCardText
     */
    String getInternationalBusinessCardText();

    void setInternationalBusinessCardText(
            String internationalBusinessCardText);

    /**
     * Indicates that user has company cellular phone
     * @return cellularPhone
     */
    boolean isCellularPhone();

    void setCellularPhone(boolean cellularPhone);

    /**
     * Indicates that time recording is enabled for this employee
     * @return timeRecordingEnabled
     */
    boolean isTimeRecordingEnabled();

    /**
     * Sets that time recording is enabled for this employee
     * @param timeRecordingEnabled
     */
    void setTimeRecordingEnabled(boolean timeRecordingEnabled);

    /**
     * Indicates that employee info data should be exported to online cms
     * @return internetExportEnabled
     */
    boolean isInternetExportEnabled();

    /**
     * Enables/disables internet export
     * @param internetExportEnabled
     */
    void setInternetExportEnabled(boolean internetExportEnabled);

    /**
     * Provides the phone number to display in internet exports
     * @return internetPhone
     */
    Phone getInternetPhone();

    /**
     * Sets the internet phone id
     * @param internetPhoneId
     */
    void setInternetPhoneId(Long internetPhoneId);

    /**
     * Provides the default language code of the employee
     * @return default language code
     */
    String getDefaultLanguage();

    /**
     * Sets the default language code of the employee
     * @param default language code
     */
    void setDefaultLanguage(String defaultLanguage);

    /**
     * Provides the id of default disciplinarian
     * @return defaultDisciplinarian
     */
    Long getDefaultDisciplinarian();

    /**
     * Sets the id of default disciplinarian
     * @param defaultDisciplinarian
     */
    void setDefaultDisciplinarian(Long defaultDisciplinarian);

    /**
     * Provides the description in default language. The description is used for presentation of employee data
     * @return description
     */
    EmployeeDescription getDescription();

    /**
     * Provides the description for an i18n language. The description is used for presentation of employee data
     * @return description
     */
    EmployeeDescription getDescription(Long i18nConfigId);

    /**
     * Updates a description
     * @param user
     * @param configId
     * @param name
     * @param description
     * @param defaultDescription
     */
    void updateDescription(
            Employee user,
            Long configId,
            String name,
            String description,
            boolean defaultDescription);

    boolean isDrawsSalary();

    void setDrawsSalary(boolean drawsSalary);

    boolean isDrawsProvision();

    void setDrawsProvision(boolean drawsProvision);

    boolean isTimeAndMaterial();

    void setTimeAndMaterial(boolean timeAndMaterial);

    boolean isServiceContract();

    void setServiceContract(boolean serviceContract);

    boolean isConsultant();

    void setConsultant(boolean consultant);

    boolean isTimeWorker();

    void setTimeWorker(boolean timeWorker);

    boolean isSalesPartner();

    void setSalesPartner(boolean salesPartner);

    /**
     * Provides groupware related values as map of strings as follows: <br/>
     * id <br/>
     * businessPhone <br/>
     * city <br/>
     * description <br/>
     * email <br/>
     * homePhone <br/>
     * mobilePhone <br/>
     * position <br/>
     * role <br/>
     * street <br/>
     * zipcode
     * @return groupware values
     */
    Map<String, Object> getGroupwareValues();

    /**
     * Provides employee xml for record printage
     * @param elementName
     * @return xml
     */
    Element getRecordXML(String elementName);

    /**
     * @return the defaultStock
     */
    Long getDefaultStock();

    /**
     * @param defaultStock the defaultStock to set
     */
    void setDefaultStock(Long defaultStock);
}
