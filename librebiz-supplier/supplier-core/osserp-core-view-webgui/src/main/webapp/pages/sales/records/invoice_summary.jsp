<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>

<c:set var="actionUrl" value="${requestScope.baseUrl}"/>
<c:set var="record" value="${sessionScope.recordView.record}"/>
<c:choose>
	<c:when test="${record.downpayment and !record.itemsChangeable}">
		<c:set var="amounts" value="${record}"/>
	</c:when>
	<c:otherwise>
		<c:set var="amounts" value="${record.amounts}"/>
	</c:otherwise>
</c:choose>
<table class="recordSummary">
	<tr>
		<td class="phead">
            <span>
                <fmt:message key="invoiceAmount"/>
                <span class="desc">
                    (<fmt:message key="netTurnoverTaxGrossBegin"/>
                    <span> <o:number value="${record.amounts.taxRate}" format="tax"/>%</span>
                    <c:if test="${amounts.reducedTaxAmount > 0}">
                    <span> / </span><o:number value="${record.amounts.reducedTaxRate}" format="tax"/>%
                    </c:if>
                    <span> <fmt:message key="netTurnoverTaxGrossEnd"/></span>)
                </span>
             </span>
        </td>
		<c:choose>
			<c:when test="${record.taxFree}">
				<td class="pnet"><o:number value="${amounts.amount}" format="currency"/> </td>
				<td class="ptax"><o:number value="0.00" format="currency"/> </td>
				<td class="pamount"><o:number value="${amounts.amount}" format="currency"/> </td>
			</c:when>
			<c:otherwise>
				<td class="pnet"><o:number value="${amounts.amount}" format="currency"/> </td>
				<td class="ptax"><o:number value="${amounts.taxAmount + amounts.reducedTaxAmount}" format="currency"/> </td>
				<td class="pamount"><o:number value="${amounts.grossAmount}" format="currency"/> </td>
			</c:otherwise>
		</c:choose>
	</tr>
	<tr>
		<td colspan="4" class="pspace"></td>
	</tr>
	<c:forEach var="payment" items="${record.payments}" varStatus="s">
		<tr>
			<c:choose>
				<c:when test="${s.index == 0}">
					<td colspan="2" class="payment"><fmt:message key="payments"/></td>
				</c:when>
				<c:otherwise>
					<td colspan="2" class="payment"></td>
				</c:otherwise>
			</c:choose>
			<td class="pdate" style="text-align: right;">
				<span>
					<o:date value="${payment.paid}"/>
				</span>
			</td>
			<td class="pamount"><span title="${payment.type.name}"><o:number value="${payment.amount}" format="currency" /></span></td>
		</tr>
	</c:forEach>
	<c:if test="${!record.downpayment}">
		<c:forEach var="creditNote" items="${record.creditNotes}" varStatus="s">
			<c:if test="${!creditNote.canceled}">
				<tr>
					<c:choose>
						<c:when test="${s.index == 0}">
							<td colspan="2" class="payment"><fmt:message key="creditNotes"/></td>
						</c:when>
						<c:otherwise>
							<td colspan="2" class="payment"></td>
						</c:otherwise>
					</c:choose>
					<td class="pdate"><o:date value="${creditNote.created}"/></td>
					<td class="pamount"><o:number value="${creditNote.amounts.grossAmount}" format="currency"/> </td>
				</tr>
				<c:forEach var="payment" items="${creditNote.payments}" varStatus="s">
					<tr>
						<c:choose>
							<c:when test="${s.index == 0}">
								<td colspan="2" class="payment"><fmt:message key="payOuts"/></td>
							</c:when>
							<c:otherwise>
								<td colspan="2" class="payment"></td>
							</c:otherwise>
						</c:choose>
						<td class="pdate"><o:date value="${payment.paid}"/></td>
						<td class="pamount<c:if test="${payment.outflowOfCash}"> error</c:if>">
                            <o:number value="${payment.amount}" format="currency"/> 
                        </td>
					</tr>
				</c:forEach>
			</c:if>
		</c:forEach>
	</c:if>
	<tr>
		<td colspan="4" class="pspace"></td>
	</tr>
	<tr>
		<td colspan="2" class="payment"><fmt:message key="openAmount"/></td>
		<td class="pdate"><o:date current="true"/></td>
		<td class="pamount">
            <o:number value="${record.dueAmount}" format="currency"/>
        </td>
	</tr>
	<tr>
		<td colspan="4" class="plast"></td>
	</tr>
</table>
