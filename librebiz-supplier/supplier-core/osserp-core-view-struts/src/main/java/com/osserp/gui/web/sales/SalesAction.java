/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 04-Nov-2006 11:04:56 
 * 
 */
package com.osserp.gui.web.sales;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.struts.StrutsForm;
import com.osserp.gui.BusinessCaseView;
import com.osserp.gui.client.Actions;
import com.osserp.gui.client.sales.SalesUtil;
import com.osserp.gui.client.sales.SalesView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesAction extends AbstractSalesAction {
    private static Logger log = LoggerFactory.getLogger(SalesAction.class.getName());

    /**
     * Enables parent edit mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward enableParentEdit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableParentEdit() request from " + getUser(session).getId());
        }
        SalesView view = getSalesView(session);
        view.setParentEditMode(true);
        return mapping.findForward(Actions.DISPLAY);
    }

    /**
     * Change customer for this sales
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward changeCustomer(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("changeCustomer() request from " + getUser(session).getId());
        }
        BusinessCaseView view = getBusinessCaseView(session);
        try {
            Long id = RequestUtil.fetchId(request);
            view.updateCustomer(id);
        } catch (ClientException c) {
            saveError(request, c.getMessage());
        }
        return mapping.findForward(Actions.DISPLAY);
    }

    /**
     * Disables parent edit mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward disableParentEdit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disableParentEdit() request from " + getUser(session).getId());
        }
        SalesView view = getSalesView(session);
        view.setParentEditMode(false);
        return mapping.findForward(Actions.DISPLAY);
    }

    /**
     * Sets parent id
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward updateParent(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("updateParent() request from " + getUser(session).getId());
        }
        SalesView view = getSalesView(session);
        try {
            view.updateParent(new StrutsForm(form));
        } catch (ClientException c) {
            RequestUtil.saveError(request, c.getMessage());
        }
        return mapping.findForward(Actions.DISPLAY);
    }

    /**
     * Enables reference setup
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward enableReferenceSetup(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableReferenceSetup() request from " + getUser(session).getId());
        }
        SalesView view = getSalesView(session);
        view.setReferenceSetupMode(true);
        return mapping.findForward(Actions.DISPLAY);
    }

    /**
     * See method name
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward disableReferenceSetup(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disableReferenceSetup() request from " + getUser(session).getId());
        }
        SalesView view = getSalesView(session);
        view.setReferenceSetupMode(false);
        return mapping.findForward(Actions.DISPLAY);
    }

    /**
     * See method name
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward updateReferenceStatus(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("updateReferenceStatus() request from " + getUser(session).getId());
        }
        SalesView view = getSalesView(session);
        try {
            view.updateReferenceStatus(new StrutsForm(form));
        } catch (ClientException c) {
            RequestUtil.saveError(request, c.getMessage());
        } catch (PermissionException p) {
            RequestUtil.saveError(request, p.getMessage());
        }
        return mapping.findForward(Actions.DISPLAY);
    }

    /**
     * See method name
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward removeEmployeeSelection(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("removeEmployeeSelection() request from " + getUser(session).getId());
        }
        SalesView view = getSalesView(session);
        view.removeEmployee(RequestUtil.getTarget(request));
        return mapping.findForward(Actions.DISPLAY);
    }

    /**
     * See method name
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward delete(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("delete() request from " + getUser(session).getId());
        }
        throw new UnsupportedOperationException();
        /*
         * BusinessCaseService service = getBusinessService(session); SaleView view = getSaleView(service); try { view.setDeliveryDateMode(true); }
         * catch (ClientException c) { service.saveError(request, c.getMessage()); } return mapping.findForward(Actions.DISPLAY);
         */
    }

    /**
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward enableChangeValuesOnClosedSalesMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableChangeValuesOnClosedSalesMode() request from " + getUser(session).getId());
        }
        SalesView view = getSalesView(session);
        view.setChangeValuesOnClosedMode(true);
        return mapping.findForward(Actions.DISPLAY);
    }
    

    /**
     * Delete sales, sales order and all related artefacts and restores
     * previous request status before salesCreate.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward restoreRequestStatus(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("restoreRequestStatus() request from " + getUser(session).getId());
        }
        SalesView view = getSalesView(session);
        try {
            BusinessCaseView requestView = SalesUtil.createBusinessCaseView(
                servlet.getServletContext(),
                request,
                SalesUtil.loadRequest(request, view.restoreRequestStatus()));
            if (log.isDebugEnabled()) {
                log.debug("restoreRequestStatus() done [request=" 
                        + requestView.getRequest().getPrimaryKey() 
                        + "]");
            }
            return mapping.findForward(Actions.PLAN);
        } catch (Exception e) {
            if (log.isDebugEnabled()) {
                log.debug("restoreRequestStatus() caught exception [message=" + e.getMessage() + "]", e);
            }
            RequestUtil.saveError(request, e.getMessage());
            return mapping.findForward(Actions.DISPLAY);
        }
    }

    /**
     * Removes closed status action from businessCase and restores pervious status
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward reopenClosed(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("reopenClosed() request from " + getUser(session).getId());
        }
        SalesView view = getSalesView(session);
        try {
            view.reopenClosed();
        } catch (Exception e) {
            if (log.isDebugEnabled()) {
                log.debug("reopenClosed() caught exception [message=" + e.getMessage() + "]", e);
            }
            RequestUtil.saveError(request, e.getMessage());
        }
        return mapping.findForward(Actions.DISPLAY);
    }
    
}
