/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 6, 2007 1:42:06 PM 
 * 
 */
package com.osserp.core.model;

import java.util.Date;

import com.osserp.core.BusinessType;
import com.osserp.core.sales.SalesListProductItem;
import com.osserp.core.sales.SalesListItem;
import com.osserp.core.system.BranchOffice;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesListProductItemVO extends SalesListItemVO
        implements SalesListProductItem {

    private Long packageListId = null;
    private Long productId = null;
    private String productName = null;
    private Double quantity = null;
    private Double delivered = null;
    private Long stockId = null;

    protected SalesListProductItemVO() {
        super();
    }

    public SalesListProductItemVO(
            String customerName,
            String shortName,
            Long contactType,
            Long id,
            Long requestId,
            BusinessType type,
            Long customerId,
            Long status,
            Date created,
            Long createdBy,
            Long managerId,
            Long managerSubId,
            Long salesId,
            BranchOffice branch,
            String name,
            String street,
            String zipcode,
            String city,
            Double capacity,
            boolean stopped,
            boolean cancelled,
            Long installerId,
            Date installationDate,
            Date deliveryDate,
            Date confirmationDate,
            Long packageListId,
            Long productId,
            String productName,
            Double quantity,
            Double delivered,
            Long stockId,
            Long originId,
            Long originTypeId,
            String accountingReference,
            Long shippingId,
            Long paymentId) {
        super(
                customerName,
                shortName,
                contactType,
                id,
                requestId,
                type,
                customerId,
                status,
                created,
                createdBy,
                managerId,
                null, // managerKey
                managerSubId,
                salesId,
                null, // salesKey
                branch,
                name,
                street,
                zipcode,
                city,
                capacity,
                stopped,
                cancelled,
                installerId,
                null, // lastActionDate
                originId,
                originTypeId,
                accountingReference,
                shippingId,
                paymentId,
                deliveryDate,
                confirmationDate);
        update(
                installationDate,
                packageListId,
                productId,
                productName,
                quantity,
                delivered,
                stockId);
    }

    protected SalesListProductItemVO(SalesListProductItem vo) {
        super(vo);
        this.packageListId = vo.getPackageListId();
        this.productId = vo.getProductId();
        this.productName = vo.getProductName();
        this.quantity = vo.getQuantity();
        this.delivered = vo.getDelivered();
        this.stockId = vo.getStockId();
    }

    public SalesListProductItemVO(
            SalesListItem vo,
            Date installationDate,
            Long packageListId,
            Long productId,
            String productName,
            Double quantity,
            Double delivered,
            Long stockId) {
        super(vo);
        update(installationDate, packageListId, productId, productName, quantity, delivered, stockId);
    }

    protected void update(
            Date installationDate,
            Long packageListId,
            Long productId,
            String productName,
            Double quantity,
            Double delivered,
            Long stockId) {
        setInstallationDate(installationDate);
        this.packageListId = packageListId;
        this.productId = productId;
        this.productName = productName;
        this.quantity = quantity;
        this.delivered = delivered;
        this.stockId = stockId;
    }

    public Long getPackageListId() {
        return packageListId;
    }

    protected void setPackageListId(Long packageListId) {
        this.packageListId = packageListId;
    }

    public Long getProductId() {
        return productId;
    }

    protected void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    protected void setProductName(String productName) {
        this.productName = productName;
    }

    public Double getQuantity() {
        return quantity;
    }

    protected void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Double getDelivered() {
        return delivered;
    }

    protected void setDelivered(Double delivered) {
        this.delivered = delivered;
    }

    public Long getStockId() {
        return stockId;
    }

    protected void setStockId(Long stockId) {
        this.stockId = stockId;
    }

    public Double getOutstanding() {
        return quantity - delivered;
    }
}
