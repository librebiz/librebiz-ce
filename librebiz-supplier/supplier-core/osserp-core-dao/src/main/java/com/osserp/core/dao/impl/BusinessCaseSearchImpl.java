/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 16.12.2012 
 * 
 */
package com.osserp.core.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.service.ResourceLocator;
import com.osserp.common.service.impl.AbstractResourceAwareService;

import com.osserp.core.BusinessCase;
import com.osserp.core.BusinessCaseSearch;
import com.osserp.core.customers.CustomerSummary;
import com.osserp.core.dao.ProjectQueries;
import com.osserp.core.dao.Projects;
import com.osserp.core.dao.Requests;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordSearch;
import com.osserp.core.model.contacts.groups.CustomerSummaryVO;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class BusinessCaseSearchImpl extends AbstractResourceAwareService implements BusinessCaseSearch {
    private static Logger log = LoggerFactory.getLogger(BusinessCaseSearchImpl.class.getName());
    
    private Projects projects = null;
    private ProjectQueries projectQueries = null;
    private RecordSearch recordSearch = null;
    private Requests requests = null;

    public BusinessCaseSearchImpl() {
        super();
    }

    /**
     * Creates a new common businessCase search
     * @param requests
     * @param projects
     * @param projectQueries
     */
    public BusinessCaseSearchImpl(
            ResourceLocator resourceLocator,
            Requests requests,
            Projects projects,
            ProjectQueries projectQueries,
            RecordSearch recordSearch) {
        super(resourceLocator);
        this.requests = requests;
        this.projects = projects;
        this.projectQueries = projectQueries;
        this.recordSearch = recordSearch;
    }

    /* (non-Javadoc)
     * @see com.osserp.core.BusinessCaseSearch#findBusinessCase(java.lang.Long)
     */
    public BusinessCase findBusinessCase(Long id) {
        BusinessCase businessCase = null;
        if (requests.exists(id)) {
            businessCase = requests.load(id);
            if (businessCase.isClosed()) {
                BusinessCase sales = projects.findByRequest(businessCase.getPrimaryKey());
                if (sales != null) {
                    return sales;
                }
            }
            return businessCase;
        }
        if (projects.exists(id)) {
            return projects.load(id);
        }
        Record record = recordSearch.findSales(id);
        if (record != null) {
            return findBusinessCase(record);
        }
        if (log.isDebugEnabled()) {
            log.debug("findBusinessCase() did not find request or sales [id=" + id + "]");
        }
        return null;
    }

    /* (non-Javadoc)
     * @see com.osserp.core.BusinessCaseSearch#findBusinessCase(com.osserp.core.finance.Record)
     */
    public BusinessCase findBusinessCase(Record record) {
        if (record == null || record.getType() == null
                || (record.getBusinessCaseId() == null && record.getReference() == null)) {
            return null;
        }
        return findBusinessCase(record.getBusinessCaseId() != null ? record.getBusinessCaseId() : record.getReference());
    }

    public BusinessCase findLatest(Long customerId) {
        CustomerSummary summary = getCustomerSummary(customerId);
        if (summary.getLatestSalesId() != null) {
            return projects.load(summary.getLatestSalesId());
        }
        if (summary.getLatestRequestId() != null) {
            return requests.load(summary.getLatestRequestId());
        }
        return null;
    }

    public CustomerSummary getCustomerSummary(Long customerId) {
        CustomerSummaryVO vo = new CustomerSummaryVO(customerId);
        projects.addSummary(vo);
        requests.addSummary(vo);
        return vo;
    }

    protected Projects getProjects() {
        return projects;
    }

    public void setProjects(Projects projects) {
        this.projects = projects;
    }

    protected ProjectQueries getProjectQueries() {
        return projectQueries;
    }

    public void setProjectQueries(ProjectQueries projectQueries) {
        this.projectQueries = projectQueries;
    }

    protected Requests getRequests() {
        return requests;
    }

    public void setRequests(Requests requests) {
        this.requests = requests;
    }

    protected RecordSearch getRecordSearch() {
        return recordSearch;
    }

    public void setRecordSearch(RecordSearch recordSearch) {
        this.recordSearch = recordSearch;
    }
}
