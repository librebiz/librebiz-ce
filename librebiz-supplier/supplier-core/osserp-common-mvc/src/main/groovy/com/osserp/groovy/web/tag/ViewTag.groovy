/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 24, 2011 8:29:05 AM 
 * 
 */
package com.osserp.groovy.web.tag

import com.osserp.groovy.web.WebUtil
import com.osserp.groovy.web.View

/**
 *
 * @author jg <jg@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
class ViewTag extends AbstractGroovyTag {
    
    String viewName

    /**
     * Fetches view by given name in request and page scope so view can be referenced as 'view'
     * @return empty string
     */
    @Override
    protected String execute() {
        View view = WebUtil.fetchView(request, viewName)
        if (view) {
            request.setAttribute('view', view)
            jspContext.setAttribute('view', view)
            if (view.customNavigationAvailable) {
                view.generateNavigation()
            }
        }
        ''
    }
}

