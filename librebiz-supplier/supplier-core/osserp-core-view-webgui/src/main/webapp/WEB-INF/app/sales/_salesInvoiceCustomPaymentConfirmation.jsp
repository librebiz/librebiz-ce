<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="invoiceNumberShort" />
                <o:out value="${view.record.number}" />
                <span> / </span>
                <o:date value="${view.record.created}" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="selectedActionName"> <v:link url="${view.baseLink}/selectPaymentType" title="selectType">
                            <fmt:message key="action" />
                        </v:link>
                    </label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <fmt:message key="customFinancialTransaction"/>
                </div>
            </div>
        </div>
        
        <div class="row next">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="amount">
                        <fmt:message key="customFinancialTransactionValue" />
                    </label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <v:number name="amount" styleClass="form-control" value="${view.record.dueAmount}" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <p style="padding-top:5px;">(<fmt:message key="includesTaxShort" />)</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row next">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="customFinancialTransactionTypeName">
                        <fmt:message key="customFinancialTransactionTypeName" />
                    </label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <v:text name="customHeader" styleClass="form-control" />
                </div>
            </div>
        </div>

        <div class="row next">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="customFinancialTransactionTypeInfo">
                        <fmt:message key="customFinancialTransactionTypeInfo" />
                    </label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <v:textarea name="note" styleClass="form-control" />
                </div>
            </div>
        </div>

        <div class="row next">
            <div class="col-md-4"></div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <o:submit value="clearAmount" styleClass="form-control" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
