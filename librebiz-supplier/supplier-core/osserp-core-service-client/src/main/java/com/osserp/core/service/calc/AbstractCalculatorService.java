/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 10, 2011 11:34:18 AM 
 * 
 */
package com.osserp.core.service.calc;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.core.calc.Calculation;
import com.osserp.core.calc.CalculatorService;
import com.osserp.core.dao.Calculations;
import com.osserp.core.dao.Projects;
import com.osserp.core.dao.Requests;
import com.osserp.core.employees.Employee;
import com.osserp.core.requests.Request;
import com.osserp.core.sales.Sales;
import com.osserp.core.sales.SalesOfferManager;
import com.osserp.core.sales.SalesOrderManager;
import com.osserp.core.service.impl.AbstractService;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractCalculatorService extends AbstractService implements CalculatorService {
    private static Logger log = LoggerFactory.getLogger(AbstractCalculatorService.class.getName());

    private Calculations calculations;
    private Projects projects;
    private Requests requests;
    private SalesOfferManager salesOfferManager;
    private SalesOrderManager salesOrderManager;

    protected AbstractCalculatorService(
            Calculations calculations,
            Projects projects,
            Requests requests,
            SalesOfferManager salesOfferManager,
            SalesOrderManager salesOrderManager) {
        super();
        this.calculations = calculations;
        this.projects = projects;
        this.requests = requests;
        this.salesOfferManager = salesOfferManager;
        this.salesOrderManager = salesOrderManager;
    }

    public final void calculate(Calculation calculation, DomainUser user, boolean calculateOnly) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("calculate() invoked [calculation=" + calculation.getId()
                    + ", user=" + user.getEmployee().getId()
                    + ", calculateOnly=" + calculateOnly
                    + "]");
        }
        calculate(calculation, calculateOnly);
        if (!calculateOnly) {
            save(calculation, user.getEmployee());
        }
    }

    protected abstract void calculate(Calculation calculation, boolean calculateOnly) throws ClientException;

    private void save(Calculation calculation, Employee user) throws ClientException {
        calculation.setChangedBy(user.getId());
        calculation.setChanged(new Date(System.currentTimeMillis()));
        calculations.save(calculation);

        if (projects.exists(calculation.getReference())) {
            updateSales(calculation, projects.load(calculation.getReference()), user);
        } else {
            updateRequest(calculation, requests.load(calculation.getReference()), user);
        }
    }

    private void updateRequest(Calculation active, Request request, Employee user) throws ClientException {
        salesOfferManager.update(user, request, active);
    }

    private void updateSales(Calculation active, Sales sales, Employee user) {
        if (active.getPlantCapacity() != null && active.getPlantCapacity() > 0) {
            sales.setCapacity(active.getPlantCapacity());
        } else if (active.getPrice() != null) {
            sales.setCapacity(active.getPrice().doubleValue());
        }
        projects.save(sales);
        salesOrderManager.updateByCalculation(user, sales, active);
    }

    protected final Calculations getCalculations() {
        return calculations;
    }
}
