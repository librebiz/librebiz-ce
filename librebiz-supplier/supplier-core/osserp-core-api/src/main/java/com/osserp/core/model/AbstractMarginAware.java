/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 26, 2011 12:02:28 PM 
 * 
 */
package com.osserp.core.model;

import com.osserp.common.Constants;
import com.osserp.common.beans.AbstractClass;

import com.osserp.core.products.MarginAware;

/**
 * @author Rainer Kirchner <rk@osserp.com>
 */
public abstract class AbstractMarginAware extends AbstractClass implements MarginAware {

    private Double consumerMargin = Constants.DOUBLE_NULL;
    private Double resellerMargin = Constants.DOUBLE_NULL;
    private Double partnerMargin = Constants.DOUBLE_NULL;

    private Double consumerMinimumMargin = Constants.DOUBLE_NULL;
    private Double resellerMinimumMargin = Constants.DOUBLE_NULL;
    private Double partnerMinimumMargin = Constants.DOUBLE_NULL;

    protected AbstractMarginAware() {
        super();
    }

    AbstractMarginAware(
            Double consumerMargin,
            Double resellerMargin,
            Double partnerMargin,
            Double consumerMinimumMargin,
            Double resellerMinimumMargin,
            Double partnerMinimumMargin) {
        super();
        this.consumerMargin = (consumerMargin == null ? Constants.DOUBLE_NULL : consumerMargin);
        this.resellerMargin = (resellerMargin == null ? Constants.DOUBLE_NULL : resellerMargin);
        this.partnerMargin = (partnerMargin == null ? Constants.DOUBLE_NULL : partnerMargin);
        this.consumerMinimumMargin = (consumerMinimumMargin == null ? Constants.DOUBLE_NULL : consumerMinimumMargin);
        this.resellerMinimumMargin = (resellerMinimumMargin == null ? Constants.DOUBLE_NULL : resellerMinimumMargin);
        this.partnerMinimumMargin = (partnerMinimumMargin == null ? Constants.DOUBLE_NULL : partnerMinimumMargin);
    }

    protected AbstractMarginAware(MarginAware other) {
        super();
        this.consumerMargin = other.getConsumerMargin() != null ? other.getConsumerMargin() : Constants.DOUBLE_NULL;
        this.resellerMargin = other.getResellerMargin() != null ? other.getResellerMargin() : Constants.DOUBLE_NULL;
        this.partnerMargin = other.getPartnerMargin() != null ? other.getPartnerMargin() : Constants.DOUBLE_NULL;
        this.consumerMinimumMargin = (other.getConsumerMinimumMargin() == null ? Constants.DOUBLE_NULL : other.getConsumerMinimumMargin());
        this.resellerMinimumMargin = (other.getResellerMinimumMargin() == null ? Constants.DOUBLE_NULL : other.getResellerMinimumMargin());
        this.partnerMinimumMargin = (other.getPartnerMinimumMargin() == null ? Constants.DOUBLE_NULL : other.getPartnerMinimumMargin());
    }

    public Double getConsumerMargin() {
        return consumerMargin;
    }

    public void setConsumerMargin(Double consumerMargin) {
        this.consumerMargin = consumerMargin;
    }

    public Double getPartnerMargin() {
        return partnerMargin;
    }

    public void setPartnerMargin(Double partnerMargin) {
        this.partnerMargin = partnerMargin;
    }

    public Double getResellerMargin() {
        return resellerMargin;
    }

    public void setResellerMargin(Double resellerMargin) {
        this.resellerMargin = resellerMargin;
    }

    public Double getConsumerMinimumMargin() {
        return consumerMinimumMargin;
    }

    public void setConsumerMinimumMargin(Double consumerMinimumMargin) {
        this.consumerMinimumMargin = consumerMinimumMargin;
    }

    public Double getResellerMinimumMargin() {
        return resellerMinimumMargin;
    }

    public void setResellerMinimumMargin(Double resellerMinimumMargin) {
        this.resellerMinimumMargin = resellerMinimumMargin;
    }

    public Double getPartnerMinimumMargin() {
        return partnerMinimumMargin;
    }

    public void setPartnerMinimumMargin(Double partnerMinimumMargin) {
        this.partnerMinimumMargin = partnerMinimumMargin;
    }

    public void updateChanged(Long user) {
        // implementation required by interface, does nothing.
    }

}
