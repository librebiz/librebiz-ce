/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 19-Jun-2006 11:39:13 
 * 
 */
package com.osserp.gui.client.sales.impl;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;
import com.osserp.common.web.ViewName;

import com.osserp.core.finance.CancellableRecord;
import com.osserp.core.finance.Cancellation;
import com.osserp.core.finance.CreditNote;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordManager;
import com.osserp.core.sales.SalesCancellationManager;

import com.osserp.gui.client.records.RecordView;
import com.osserp.gui.client.records.impl.AbstractRecordView;
import com.osserp.gui.client.sales.SalesCancellationView;
import com.osserp.gui.client.sales.SalesCreditNoteView;
import com.osserp.gui.client.sales.SalesInvoiceView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("salesCancellationView")
public class SalesCancellationViewImpl extends AbstractRecordView
        implements SalesCancellationView {
    private static Logger log = LoggerFactory.getLogger(SalesCancellationViewImpl.class.getName());
    private RecordView canceledView = null;

    protected SalesCancellationViewImpl() {
        super();
    }

    public RecordView getCanceledView() {
        return canceledView;
    }

    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        super.init(request);
        canceledView = (RecordView) request.getAttribute(SalesCancellationView.CANCELED_VIEW_PARAM_KEY);
        if (canceledView == null) {
            String canceledViewName = request.getParameter(SalesCancellationView.CANCELED_VIEW_PARAM_KEY);
            if (isSet(canceledViewName)) {
                canceledView = (RecordView) request.getSession(true).getAttribute(canceledViewName);
            }
        }
        if (canceledView != null && canceledView.getBean() != null) {

            if (canceledView instanceof SalesCreditNoteView) {
                SalesCreditNoteView creditNoteView = (SalesCreditNoteView) canceledView;
                CreditNote note = (CreditNote) creditNoteView.getBean();
                if (log.isDebugEnabled()) {
                    log.debug("init() fetched credit note [id=" + note.getId() + "]");
                }
                if (note.isCanceled()) {
                    Cancellation c = note.getCancellation();
                    if (log.isDebugEnabled()) {
                        log.debug("init() found cancellation [id=" + c.getId() + "]");
                    }
                    setRecord(c);
                }
            } else if (canceledView instanceof SalesInvoiceView) {
                SalesInvoiceView invoiceView = (SalesInvoiceView) canceledView;
                Invoice invoice = (Invoice) invoiceView.getRecord();
                if (log.isDebugEnabled()) {
                    log.debug("init() fetched invoice [id=" + invoice.getId() + "]");
                }
                if (invoice.isCanceled() && invoice instanceof CancellableRecord) {
                    Cancellation c = ((CancellableRecord) invoice).getCancellation();
                    if (log.isDebugEnabled()) {
                        log.debug("init() found cancellation [id=" + c.getId() + "]");
                    }
                    setRecord(c);
                }
            } else {
                log.warn("init() unknown record view found [class="
                        + canceledView.getClass().getName() + "]");
            }
        } else if (log.isInfoEnabled()) {
            log.info("init() no cancelable "
                    + (canceledView == null ? "recordView" : "record")
                    + " found in request context!");
        }
    }

    public void create() throws ClientException, PermissionException {
        SalesCancellationManager manager = getCancellationManager();
        if (canceledView instanceof SalesInvoiceView) {
            Cancellation c = manager.create(
                    getDomainUser().getEmployee(),
                    (Invoice) canceledView.getRecord(),
                    null,
                    null);
            setRecord(manager.find(c.getId()));
            setExitTarget("salesInvoiceView");
            if (log.isDebugEnabled()) {
                log.debug("create() done [cancellation" + c.getId()
                        + ", reference=" + c.getReference()
                        + ", exitTarget=salesInvoiceView]");
            }
        } else if (canceledView instanceof SalesCreditNoteView) {
            Cancellation c = manager.create(
                    getDomainUser().getEmployee(),
                    (CreditNote) canceledView.getRecord(),
                    null,
                    null);
            setRecord(manager.find(c.getId()));
            setExitTarget("salesCreditNoteView");
            if (log.isDebugEnabled()) {
                log.debug("create() done [cancellation" + c.getId()
                        + ", reference=" + c.getReference()
                        + ", exitTarget=salesCreditNoteView]");
            }
        }
    }

    @Override
    public void save(Form fh) throws ClientException, PermissionException {
        SalesCancellationManager manager = getCancellationManager();
        Record record = getRecord();
        manager.update(
                record,
                fh.getLong("signatureLeft"),
                fh.getLong("signatureRight"),
                fh.getLong("personId"),
                fh.getString("note"),
                fh.getBoolean("printBusinessCaseId"),
                fh.getBoolean("printBusinessCaseInfo"));
        refresh();
        savePrintOptionDefaults(fh, getRecord());
    }

    protected boolean isRecordDeletePermissionGrant(boolean permissionsGrant) {
        Record record = getRecord();
        return permissionsGrant && !record.isUnchangeable();
    }

    protected SalesCancellationManager getCancellationManager() {
        return (SalesCancellationManager) getRecordManager();
    }

    @Override
    public RecordManager getRecordManager() {
        return (SalesCancellationManager) getService(SalesCancellationManager.class.getName());
    }

    @Override
    public boolean isProvidingStatusHistory() {
        return true;
    }
}
