/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 11-Jul-2005 11:41:39 
 * 
 */
package com.osserp.common;

import java.io.Serializable;

import com.osserp.common.beans.AbstractClass;
import com.osserp.common.util.NumberUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SearchRequest extends AbstractClass implements Serializable {
    private static final long serialVersionUID = 42L;

    public static final int IGNORE_FETCHSIZE = -1;

    private String columnKey = null;
    private String pattern = null;
    private boolean startsWith = false;
    private String searchType = null;
    private boolean includeCanceled = false;
    private boolean openOnly = false;
    private String orderByColumn = null;
    private boolean orderByDescendent = false;
    private int fetchSize = 0;
    private boolean ignoreFetchSize = false;
    private boolean changed = true;

    /**
     * Creates a new search request with default or empty params.
     */
    protected SearchRequest() {
        super();
    }

    /**
     * Creates a new search request
     * @param columnKey
     * @param pattern
     * @param startsWith
     */
    public SearchRequest(
            String columnKey,
            String pattern,
            boolean startsWith) {
        super();
        this.columnKey = columnKey;
        this.pattern = pattern;
        this.startsWith = startsWith;
    }

    /**
     * Creates a new search request
     * @param previous
     * @param columnKey
     * @param pattern
     * @param startsWith
     */
    public SearchRequest(
            SearchRequest previous,
            String columnKey,
            String pattern,
            boolean startsWith) {
        this(previous);
        if (previous != null) {
            changed = isChanged(this.columnKey, columnKey)
                    || isChanged(this.pattern, pattern)
                    || isChanged(this.startsWith, startsWith);
        }
        this.columnKey = columnKey;
        this.pattern = pattern;
        this.startsWith = startsWith;
    }

    /**
     * Creates a new search request by existing
     * @param previous
     * @param columnKey
     * @param pattern
     * @param startsWith
     * @param fetchSize
     */
    public SearchRequest(
            SearchRequest previous,
            String columnKey,
            String pattern,
            boolean startsWith,
            int fetchSize) {
        this(previous, columnKey, pattern, startsWith);
        this.fetchSize = fetchSize;
    }

    /**
     * Creates a new search request
     * @param previous
     * @param columnKey
     * @param pattern
     * @param startsWith
     * @param fetchSize
     * @param openOnly
     */
    public SearchRequest(
            SearchRequest previous,
            String columnKey,
            String pattern,
            boolean startsWith,
            int fetchSize,
            boolean openOnly) {
        this(previous, columnKey, pattern, startsWith, fetchSize);
        if (previous != null && !changed && isChanged(this.openOnly, openOnly)) {
            changed = true;
        }
        this.openOnly = openOnly;
    }

    /**
     * Creates a new search request for business objects reusing existing
     * @param previous
     * @param columnKey
     * @param pattern
     * @param startsWith
     * @param openOnly
     */
    public SearchRequest(
            SearchRequest previous,
            String columnKey,
            String pattern,
            boolean startsWith,
            boolean openOnly) {
        this(previous, columnKey, pattern, startsWith);
        if (previous != null && !changed && isChanged(this.openOnly, openOnly)) {
            changed = true;
        }
        this.openOnly = openOnly;
    }

    /**
     * Creates a new search request for business objects 
     * @param columnKey
     * @param pattern
     * @param startsWith
     * @param ignoreFetchSize
     * @param openOnly
     */
    public SearchRequest(
            String columnKey,
            String pattern,
            boolean startsWith,
            boolean openOnly) {
        this(null, columnKey, pattern, startsWith);
        this.openOnly = openOnly;
    }

    /**
     * Creates a new search request for business objects
     * @param columnKey
     * @param pattern
     * @param startsWith
     * @param openOnly
     * @param includeCanceled
     */
    public SearchRequest(
            String columnKey,
            String pattern,
            boolean startsWith,
            boolean openOnly,
            boolean includeCanceled) {
        this(columnKey, pattern, startsWith, openOnly);
        this.includeCanceled = includeCanceled;
    }

    /**
     * Creates a new search request by other if exists
     * @param other
     */
    public SearchRequest(SearchRequest other) {
        super();
        if (other != null) {
            this.columnKey = other.columnKey;
            this.pattern = other.pattern;
            this.startsWith = other.startsWith;
            this.searchType = other.searchType;
            this.includeCanceled = other.includeCanceled;
            this.openOnly = other.openOnly;
            this.orderByColumn = other.orderByColumn;
            this.orderByDescendent = other.orderByDescendent;
            this.fetchSize = other.fetchSize;
            this.ignoreFetchSize = other.ignoreFetchSize;
            changed = false;
        }
    }

    /**
     * The column key represents a property of an entitiy where we lookup for matches
     * @return columnKey
     */
    public String getColumnKey() {
        return columnKey;
    }

    /**
     * Sets the column key representing a property of an entitiy where we lookup for matches
     * @param columnKey
     */
    public void setColumnKey(String columnKey) {
        this.columnKey = columnKey;
    }

    /**
     * The pattern to lookup for at specified property of the entities we search
     * @return pattern
     */
    public String getPattern() {
        return pattern;
    }

    /**
     * Sets the pattern to lookup for at specified property of the entities we search
     * @param pattern
     */
    public void setPattern(String pattern) throws ClientException {
        if (pattern != null) {
            if (pattern.indexOf("*") > -1 || pattern.indexOf("'") > -1) {
                throw new ClientException(ErrorCode.SEARCH);
            }
        }
        this.pattern = pattern;
    }

    public Long getPatternAsNumber() {
        return NumberUtil.createLong(pattern);
    }

    public boolean isStartsWith() {
        return startsWith;
    }

    public void setStartsWith(boolean startsWith) {
        this.startsWith = startsWith;
    }

    public String getSearchType() {
        return searchType;
    }

    public void setSearchType(String searchType) {
        this.searchType = searchType;
    }

    public boolean isIncludeCanceled() {
        return includeCanceled;
    }

    public void setIncludeCanceled(boolean includeCanceled) {
        this.includeCanceled = includeCanceled;
    }

    public boolean isOpenOnly() {
        return openOnly;
    }

    public void setOpenOnly(boolean openOnly) {
        this.openOnly = openOnly;
    }

    public String getOrderByColumn() {
        return orderByColumn;
    }

    public void setOrderByColumn(String orderByColumn) {
        this.orderByColumn = orderByColumn;
    }

    public boolean isOrderByDescendent() {
        return orderByDescendent;
    }

    public void setOrderByDescendent(boolean orderByDescendent) {
        this.orderByDescendent = orderByDescendent;
    }

    public int getFetchSize() {
        return fetchSize;
    }

    public void setFetchSize(int fetchSize) {
        this.fetchSize = fetchSize;
    }

    public boolean isIgnoreFetchSize() {
        return ignoreFetchSize;
    }

    public void setIgnoreFetchSize(boolean ignoreFetchSize) {
        this.ignoreFetchSize = ignoreFetchSize;
    }

    public boolean isChanged() {
        return changed;
    }

    public void setChanged(boolean changed) {
        this.changed = changed;
    }

    protected boolean isChanged(Object source, Object update) {
        if (source == null && (update != null && (!(update instanceof Long) || isSet((Long) update)))) {
            return true;
        }
        if (update == null && (source != null && (!(source instanceof Long) || isSet((Long) source)))) {
            return true;
        }
        if (source != null && update != null && !source.equals(update)) {
            return true;
        }
        return false;
    }

    protected boolean isChanged(boolean source, boolean update) {
        return (source && !update) || (!source && update);
    }
}
