<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="recordPaymentAgreementView"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="title"><o:displayTitle/></tiles:put>
	<c:if test="${!view.bean.paymentAgreement.hidePayments}">
	<tiles:put name="onload" type="string">onload="initPaymentFields();"</tiles:put>
	<tiles:put name="javascript" type="string">
		<script type="text/javascript">
			<c:import url="/js/payment-agreement.js"/>
		</script>
	</tiles:put>
	</c:if>
	<tiles:put name="headline"><fmt:message key="paymentConditions"/></tiles:put>
	<tiles:put name="headline_right">
        <v:navigation/>
	</tiles:put>
	<tiles:put name="content" type="string">
        <div class="content-area" id="recordPaymentAgreementContent">
			<div class="row">
                <v:form id="recordPaymentAgreementForm" url="/records/recordPaymentAgreement/save">
                    <c:import url="_recordPaymentAgreement.jsp"/>
                </v:form>
			</div>
		</div>
	</tiles:put>
</tiles:insert>
