<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<c:set var="sortArrow">
    <c:choose>
        <c:when test="${view.sortDescending}">&#x25BE;</c:when>
        <c:otherwise>&#x25B4;</c:otherwise>
    </c:choose>
</c:set>
<v:form url="/events/userEvents/closeEvents" name="eventCloseForm">
    <div class="table-responsive table-responsive-default">
        <table class="table">
            <thead style="position: relative;">
                <tr>
                    <th class="dateTime"><v:link url="/events/userEvents/sort?by=eventDate">
                            <fmt:message key="appointment" />
                            <c:if test="${view.sortBy == 'eventDate'}">
                                <c:out escapeXml="false" value="${sortArrow}" />
                            </c:if>
                        </v:link>
                    </th>
                    <th class="eventName"><fmt:message key="job" /></th>
                    <th class="referenceNumber"><v:link url="/events/userEvents/sort?by=referenceId">
                            <fmt:message key="referenceNumberShort" />
                            <c:if test="${view.sortBy == 'referenceId'}">
                                <c:out escapeXml="false" value="${sortArrow}" />
                            </c:if>
                        </v:link>
                    </th>
                    <th class="sourceName"><fmt:message key="reference" /></th>
                    <th class="dateTime"><v:link url="/events/userEvents/sort?by=created">
                            <fmt:message key="dateTime" />
                            <c:if test="${view.sortBy == 'created'}">
                                <c:out escapeXml="false" value="${sortArrow}" />
                            </c:if>
                        </v:link>
                    </th>
                    <th class="center" colspan="2"><fmt:message key="action" /></th>
                </tr>
            </thead>
            <tbody>
                <c:choose>
                    <c:when test="${empty view or empty view.list}">
                        <tr>
                            <td colspan="7"><fmt:message key="noEventsAvailable" /></td>
                        </tr>
                    </c:when>
                    <c:otherwise>
                        <c:forEach var="event" items="${view.list}" varStatus="s">
                            <c:choose>
                                <c:when test="${(s.index % 2) <= 0}"><c:set var="rowclass" value="altrow" /></c:when>
                                <c:otherwise><c:set var="rowclass" value="none" /></c:otherwise>
                            </c:choose>
                            <tr<c:if test="${rowclass != 'none'}"> class="${rowclass}"</c:if>>
                                <c:choose>
                                    <c:when test="${event.action.type.appointment}">
                                        <td class="dateTime"><o:date value="${event.appointmentDate}" addcentury="false" addtime="true" /></td>
                                    </c:when>
                                    <c:otherwise>
                                        <td class="dateTime"><o:date value="${event.expires}" addcentury="false" /></td>
                                    </c:otherwise>
                                </c:choose>
                                <c:choose>
                                    <c:when test="${view.foreignView}">
                                        <td class="eventName"><c:choose>
                                                <c:when test="${event.alert}">
                                                    <c:choose>
                                                        <c:when test="${!empty event.headline}">
                                                            <span class="errortext"><o:out value="${event.headline}" /></span>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <span class="errortext"><o:out value="${event.action.name}" /></span>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:when>
                                                <c:otherwise>
                                                    <c:choose>
                                                        <c:when test="${event.action.info}">[I]&nbsp;</c:when>
                                                        <c:otherwise>[A]&nbsp;</c:otherwise>
                                                    </c:choose>
                                                    <c:choose>
                                                        <c:when test="${!empty event.headline}">
                                                            <o:out value="${event.headline}" />
                                                        </c:when>
                                                        <c:otherwise>
                                                            <o:out value="${event.action.name}" />
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:otherwise>
                                            </c:choose>
                                        </td>
                                    </c:when>
                                    <c:otherwise>
                                        <td class="eventName"><c:choose>
                                                <c:when test="${event.action.info}">[I]&nbsp;</c:when>
                                                <c:otherwise>[A]&nbsp;</c:otherwise>
                                            </c:choose> <o:link name="event" property="link" scope="page">
                                                <span <c:if test="${event.alert}">class="errortext"</c:if>><o:out value="${event.action.name}" /></span>
                                            </o:link>
                                        </td>
                                    </c:otherwise>
                                </c:choose>
                                <td class="referenceNumber"><o:out value="${event.referenceId}" /></td>
                                <td class="sourceName"><o:out value="${event.description}" /></td>
                                <td class="dateTime"><o:date value="${event.created}" addcentury="false" addtime="true" /></td>
                                <c:choose>
                                    <c:when test="${view.closeMode}">
                                        <td class="icon center">
                                            <c:if test="${!view.employeeSelectionEnabled and event.action.closeableByTarget}">
                                                <input type="checkbox" name="id" value="<o:out value="${event.id}"/>" />
                                             </c:if>
                                         </td>
                                        <o:permission role="close_todos,executive" info="permissionCloseTodo">
                                            <c:if test="${view.employeeSelectionEnabled}">
                                                <td class="icon center">
                                                    <input type="checkbox" name="id" value="<o:out value="${event.id}"/>" />
                                                </td>
                                            </c:if>
                                        </o:permission>
                                        <o:forbidden role="close_todos,executive">
                                            <c:if test="${view.employeeSelectionEnabled}">
                                                <td class="icon center">
                                                </td>
                                            </c:if>
                                        </o:forbidden>
                                    </c:when>
                                    <c:otherwise>
                                        <td class="icon center">
                                            <v:ajaxLink url="/events/eventEditor/forward?id=${event.id}&view=userEventsView" linkId="eventEditor">
                                                <o:img name="texteditIcon" />
                                            </v:ajaxLink>
                                        </td>
                                        <c:choose>
                                            <c:when test="${!view.employeeSelectionEnabled and event.action.closeableByTarget}">
                                                <td class="icon center">
                                                    <a href="javascript:confirmLink('<fmt:message key="confirmCloseTodo"/>','<v:url value="/events/userEvents/closeEvents?id=${event.id}"/>');" title="<fmt:message key="closeTodo"/>"><o:img name="deleteIcon" /></a>
                                                </td>
                                            </c:when>
                                            <c:otherwise>
                                                <o:permission role="close_todos,executive" info="permissionCloseTodo">
                                                    <td class="icon center">
                                                        <c:if test="${view.employeeSelectionEnabled or view.user.employee.companyExecutive}">
                                                            <a href="javascript:confirmLink('<fmt:message key="confirmCloseTodo"/>','<v:url value="/events/userEvents/closeEvents?id=${event.id}"/>');" title="<fmt:message key="closeTodo"/>"><o:img name="deleteIcon" /></a>
                                                        </c:if>
                                                    </td>
                                                </o:permission>
                                                <o:forbidden role="close_todos,executive">
                                                    <c:if test="${view.employeeSelectionEnabled}">
                                                        <td class="icon center">
                                                        </td>
                                                    </c:if>
                                                </o:forbidden>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:otherwise>
                                </c:choose>
                            </tr>
                            <c:if test="${!empty event.message or !empty event.notes}">
                                <tr<c:if test="${rowclass != 'none'}"> class="${rowclass}"</c:if>>
                                    <td class="dateTime">&nbsp;</td>
                                    <td valign="top" colspan="4">
                                        <c:forEach var="note" items="${event.notes}">
                                            <span class="boldtext"><o:date value="${note.created}" addtime="true" /> <oc:employee value="${note.createdBy}" />:</span>
                                            <br />
                                            <o:out value="${note.note}" />
                                            <br />
                                        </c:forEach> 
                                        <c:if test="${!empty event.message}">
                                            <c:if test="${!empty event.action.callerId}">
                                                <span class="boldtext"><fmt:message key="infoFromFcs" /> <oc:options name="${event.action.type.flowControlActionsName}" value="${event.action.callerId}" />:</span>
                                                <br />
                                            </c:if>
                                            <o:textOut value="${event.message}" />
                                        </c:if></td>
                                    <td class="action" colspan="2"></td>
                                </tr>
                            </c:if>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
            </tbody>
        </table>
    </div>
    <c:if test="${view.closeMode}">
        <div style="text-align: right; margin-top: 5px;">
            <input type="button" value="<fmt:message key="checkAll"/>" onclick="ojsForms.checkAll(document.eventCloseForm.id);" />
            <input type="button" value="<fmt:message key="uncheckAll"/>" onclick="ojsForms.uncheckAll(document.eventCloseForm.id);" />
            <input type="submit" value="<fmt:message key="closeChecked"/>" onclick="return confirm('<fmt:message key="confirmCloseTodos"/>');" />
        </div>
    </c:if>
</v:form>
