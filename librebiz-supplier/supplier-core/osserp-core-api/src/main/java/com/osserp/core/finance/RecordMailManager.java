/**
 *
 * Copyright (C) 2018 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 28, 2018 
 * 
 */
package com.osserp.core.finance;

import java.util.Map;

import com.osserp.common.ClientException;

import com.osserp.core.employees.Employee;
import com.osserp.core.mail.MailMessageContext;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface RecordMailManager {


    /**
     * Creates a mail message with existing pdf document as payload
     * @param user
     * @param record
     * @param opts default opts consist of 'successTarget' and 'exitTarget'
     * @return mail message context
     * @throws ClientException if validation failed, e.g. document not exists, customer email address missing or other problem
     */
    MailMessageContext createContext(
            Employee user,
            Record record,
            Map<String, String> opts) throws ClientException;

    /**
     * Creates a mail message with provided pdf document as payload
     * @param user
     * @param record
     * @param document
     * @param opts default opts consist of 'successTarget' and 'exitTarget'
     * @return mail message context
     * @throws ClientException if customer email address missing or other problem
     */
    MailMessageContext createArchiveContext(
            Employee user,
            Record record,
            RecordDocument document,
            Map<String, String> opts) throws ClientException;

    /**
     * Log when mail was sent
     * @param user
     * @param record
     * @param originator
     * @param recipients
     * @param recipientsCC
     * @param recipientsBCC
     * @param messageID
     */
    public void writeLog(
            Employee user,
            Record record,
            String originator,
            String recipients,
            String recipientsCC,
            String recipientsBCC,
            String messageID);

}
