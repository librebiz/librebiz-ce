/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 11, 2006 6:54:42 PM 
 * 
 */
package com.osserp.core.sales;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;

import com.osserp.core.customers.Customer;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.InvoiceManager;
import com.osserp.core.finance.ListItem;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.Record;
import com.osserp.core.products.Product;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SalesInvoiceManager extends InvoiceManager, DebitRecordCreator {

    /**
     * Changes the number (id) of an invoice. Use with care!
     * @param user
     * @param salesInvoice
     * @param id
     * @return invoice with updated id or unchanged invoice if id change failed for any reason, see logs.
     * @throws ClientException if change failed on user input
     */
    SalesInvoice changeNumber(Employee user, SalesInvoice salesInvoice, Long id) throws ClientException;

    /**
     * Provides all third party invoices of a sales
     * @param sales
     * @return third party invoices
     */
    List<SalesInvoice> findThirdParty(Sales sales);

    /**
     * Creates an invoice by order
     * @param user
     * @param order
     * @param type
     * @param thirdParty optional customer if differs from orders customer
     * @param partialInvoiceAmount
     * @param partialInvoiceAmountGross
     * @param copyNote indicates if existing order note should be copied to invoice
     * @param copyTerms indicates if terms of order should be copied to invoice
     * @param copyItems indicates if items of order should be copied to invoice
     * @param userDefinedId
     * @param userDefinedDate
     * @throws ClientException if validation failed
     */
    SalesInvoice create(
            Employee user, 
            Order order, 
            Long type, 
            Customer thirdParty, 
            BigDecimal partialInvoiceAmount, 
            BigDecimal partialInvoiceAmountGross,
            boolean copyNote,
            boolean copyTerms,
            boolean copyItems,
            Long userDefinedId, 
            Date userDefinedDate) throws ClientException;

    /**
     * Creates a partial invoice
     * @param user creating the invoice
     * @param order
     * @param percentage
     * @param amount
     * @param product to add
     * @param note
     * @param printOrderItems
     * @param copyNote indicates if existing order note should be copied to invoice
     * @param copyTerms indicates if terms of order should be copied to invoice
     * @param userDefinedId
     * @param userDefinedDate
     * @throws ClientException if amount is null or 0.00
     */
    SalesInvoice create(
            Employee user,
            Order order,
            boolean percentage,
            BigDecimal amount,
            Product product,
            String note,
            boolean printOrderItems,
            boolean copyNote,
            boolean copyTerms,
            Long userDefinedId, 
            Date userDefinedDate) throws ClientException;

    /**
     * Creates a new invoice by existing.
     * @param user
     * @param salesInvoice
     * @param copyReferenceId indicates if id of reference should be set as reference of new record
     * @param userDefinedId
     * @param userDefinedDate
     * @param historical
     * @return invoice
     */
    SalesInvoice create(
            Employee user, 
            SalesInvoice salesInvoice, 
            boolean copyReferenceId, 
            Long userDefinedId, 
            Date userDefinedDate, 
            boolean historical);
    
    /**
     * Enables historical status
     * @param invoice
     * @return invoice with updated status
     */
    SalesInvoice enableHistoricalStatus(SalesInvoice invoice);
    
    /**
     * Disables historical status
     * @param invoice
     * @return invoice with updated status
     */
    SalesInvoice disableHistoricalStatus(SalesInvoice invoice);
    
    /**
     * Provides all internal invoices of given company to an internal customer
     * @param company
     * @param customer
     * @return invoices or empty list
     */
    List<Record> findInternal(Long company, Customer customer);

    /**
     * Provides all records containing an product
     * @param product
     * @return items
     */
    List<ListItem> findProductListing(Product product);

    /**
     * @param sales
     * @return partial invoices
     */
    List<SalesInvoice> findPartial(Sales sales);

    /**
     * Indicates that invoice affects stock (e.g. requires to create a delivery note on release)
     * @param invoice
     * @return true if releasing invoice creates a delivery note
     */
    boolean isStockAffecting(Invoice invoice);

    /**
     * Indicates that all invoice items are available on stock
     * @param invoice
     * @return true if all items available
     */
    boolean isAvailableOnStock(Invoice invoice);

    /**
     * Updates the est35a amount
     * @param invoice
     * @param deEst35a
     * @param deEst35aTax
     * @return invoice
     */
    Invoice updateDeEst35a(Invoice invoice, String deEst35a, String deEst35aTax);

}
