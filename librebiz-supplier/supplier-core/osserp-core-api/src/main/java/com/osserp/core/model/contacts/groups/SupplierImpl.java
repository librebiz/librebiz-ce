/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 24-May-2005 09:27:40 
 * 
 */
package com.osserp.core.model.contacts.groups;

import java.util.ArrayList;
import java.util.List;

import org.jdom2.Element;

import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.ContactPaymentAgreement;
import com.osserp.core.finance.PaymentCondition;
import com.osserp.core.model.contacts.AbstractContactAware;
import com.osserp.core.products.ProductGroup;
import com.osserp.core.suppliers.Supplier;
import com.osserp.core.suppliers.SupplierType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SupplierImpl extends AbstractContactAware implements Supplier {
    
    private SupplierType supplierType = null;
    private boolean internal = false;
    
    private boolean simpleBilling;
    
    private boolean simpleInvoiceBookingEnabled = false;
    private Long simpleInvoiceBookingProduct;
    
    private boolean directInvoiceBookingEnabled = false;
    private Long directInvoiceBookingSelection = null;
    
    private List<ProductGroup> productGroups = new ArrayList<ProductGroup>();
    private List<ContactPaymentAgreement> paymentAgreements = new ArrayList<ContactPaymentAgreement>();

    protected SupplierImpl() {
        setGroupId(Supplier.SUPPLIER);
    }

    public SupplierImpl(
            Long id,
            SupplierType type,
            Long createdBy,
            Contact contact,
            PaymentCondition pa) {
        super(id, Supplier.SUPPLIER, createdBy, contact, null);
        supplierType = type;
        setStatusId(Supplier.DEACTIVATED);
        SupplierPaymentAgreementImpl agreement = new SupplierPaymentAgreementImpl(this, pa);
        paymentAgreements.add(agreement);
    }

    protected SupplierImpl(Supplier o) {
        super(o);
        internal = o.isInternal();
        supplierType = o.getSupplierType();
        simpleBilling = o.isSimpleBilling();
        simpleInvoiceBookingEnabled = o.isSimpleInvoiceBookingEnabled();
        simpleInvoiceBookingProduct = o.getSimpleInvoiceBookingProduct();
        directInvoiceBookingEnabled = o.isDirectInvoiceBookingEnabled();
        directInvoiceBookingSelection = o.getDirectInvoiceBookingSelection();
        // TODO add collections
    }

    @Override
    public Object clone() {
        return new SupplierImpl(this);
    }

    public boolean isActivated() {
        return (getStatusId() != null
        && Supplier.ACTIVATED.equals(getStatusId()));
    }

    public void changeSupplierType(SupplierType type) {
        if (type == null) {
            supplierType = null;
        } else {
            supplierType = type;
            simpleBilling = type.isSimpleBilling();
            directInvoiceBookingEnabled = type.isDirectInvoiceBookingEnabled();
            directInvoiceBookingSelection = type.getDirectInvoiceBookingSelection();
            simpleInvoiceBookingEnabled = type.isSimpleInvoiceBookingEnabled();
            simpleInvoiceBookingProduct = type.getSimpleInvoiceBookingProduct();
            ContactPaymentAgreement pa = getPaymentAgreement();
            if (type.isTaxFree() &&  pa != null) {
                pa.setTaxFree(true);
                pa.setTaxFreeId(type.getTaxFreeId());
            }
            if (simpleBilling && !isActivated()) {
                setStatusId(Supplier.ACTIVATED);
            }
        }
    }

    public SupplierType getSupplierType() {
        return supplierType;
    }

    public void setSupplierType(SupplierType supplierType) {
        this.supplierType = supplierType;
    }

    public boolean isInternal() {
        return internal;
    }

    public void setInternal(boolean internal) {
        this.internal = internal;
    }

    public boolean isSimpleBilling() {
        return simpleBilling;
    }

    public void setSimpleBilling(boolean simpleBilling) {
        this.simpleBilling = simpleBilling;
    }

    public boolean isSimpleInvoiceBookingAvailable() {
        return simpleInvoiceBookingEnabled && isSet(simpleInvoiceBookingProduct);
    }

    public boolean isSimpleInvoiceBookingEnabled() {
        return simpleInvoiceBookingEnabled;
    }

    public void setSimpleInvoiceBookingEnabled(boolean simpleInvoiceBookingEnabled) {
        this.simpleInvoiceBookingEnabled = simpleInvoiceBookingEnabled;
    }

    public Long getSimpleInvoiceBookingProduct() {
        return simpleInvoiceBookingProduct;
    }

    public void setSimpleInvoiceBookingProduct(Long simpleInvoiceBookingProduct) {
        this.simpleInvoiceBookingProduct = simpleInvoiceBookingProduct;
    }

    public boolean isDirectInvoiceBookingEnabled() {
        return directInvoiceBookingEnabled;
    }

    public void setDirectInvoiceBookingEnabled(boolean directInvoiceBookingEnabled) {
        this.directInvoiceBookingEnabled = directInvoiceBookingEnabled;
    }

    public Long getDirectInvoiceBookingSelection() {
        return directInvoiceBookingSelection;
    }

    public void setDirectInvoiceBookingSelection(Long directInvoiceBookingSelection) {
        this.directInvoiceBookingSelection = directInvoiceBookingSelection;
    }
    
    public List<ProductGroup> getProductGroups() {
        return productGroups;
    }

    public void setProductGroups(List<ProductGroup> productGroups) {
        if (productGroups != null) {
            this.productGroups = productGroups;
        }
    }

    public ContactPaymentAgreement getPaymentAgreement() {
        if (paymentAgreements == null) {
            paymentAgreements = new ArrayList<>();
        }
        if (paymentAgreements.isEmpty()) {
            paymentAgreements.add(new SupplierPaymentAgreementImpl(this, null));
        }
        return paymentAgreements.get(0);
    }

    public boolean isBillingOnly() {
        return (supplierType == null);
    }

    @Override
    public Element getXML() {
        Element root = new Element("supplier");
        if (getId() != null) {
            root.addContent(new Element("supplierId").setText(getId().toString()));
        }
        if (getSupplierType() != null) {
            root.addContent(new Element("supplierType").setText(getSupplierType().toString()));
        }
        root.addContent(getAddressXML());
        return root;
    }

    @Override
    public Long getGroupId() {
        Long id = super.getGroupId();
        return (id != null ? id : Contact.SUPPLIER);
    }

    protected List<ContactPaymentAgreement> getPaymentAgreements() {
        return paymentAgreements;
    }

    protected void setPaymentAgreements(
            List<ContactPaymentAgreement> paymentAgreements) {
        this.paymentAgreements = paymentAgreements;
    }
}
