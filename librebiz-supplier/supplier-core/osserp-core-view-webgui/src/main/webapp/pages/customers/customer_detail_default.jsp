<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="contactView" value="${sessionScope.contactView}" />
<c:set var="contact" value="${sessionScope.contactView.customer}" />
<div class="column50l">
    <div class="subcolumnl">
        <div class="spacer"></div>
        <c:import url="/pages/customers/customer_display.jsp" />
    </div>
</div>
<c:if test="${!contact.branchOffice}">
<div class="column50r">
    <div class="subcolumnr">
        <div class="spacer"></div>
        <!-- header -->
        <c:if test="${!contact.pos}">
            <div class="contentBox">
                <div class="contentBoxHeader">
                    <div class="contentBoxHeaderLeft">
                        <fmt:message key="requests" />
                    </div>
                    <div class="contentBoxHeaderRight">
                        <c:if test="${!contact.grantContactNone}">
                            <a href="<v:url value="/requests/requestCreator/forward?id=${contact.id}&exit=/customerDisplay.do"/>" title="<fmt:message key="createNew"/>"><o:img name="newIcon" /></a>
                        </c:if>
                    </div>
                </div>
                <div class="contentBoxData">
                    <div class="subcolumns">
                        <div class="subcolumn">
                            <table class="valueTable first33">
                                <tbody>
                                    <c:choose>
                                        <c:when test="${empty contactView.summary or empty contactView.summary.requests}">
                                            <tr>
                                                <td>&#160;</td>
                                                <td><fmt:message key="noneAvailable" /></td>
                                            </tr>
                                        </c:when>
                                        <c:otherwise>
                                            <c:choose>
                                                <c:when test="${contactView.summary.requestCount == 1}">
                                                    <tr>
                                                        <td>1</td>
                                                        <td><a href="<c:url value="/customers.do?method=forwardRequest&id=${contactView.summary.latestRequestId}"/>"> <fmt:message key="openRequest" />
                                                        </a></td>
                                                    </tr>
                                                </c:when>
                                                <c:when test="${contactView.summary.requestCount > 1}">
                                                    <tr>
                                                        <td><o:out value="${contactView.summary.requestCount}" /></td>
                                                        <td><a href="<c:url value="/customers.do?method=enableRequestListing&cancelled=false"/>" title="<fmt:message key="displayList"/>"> <fmt:message key="openRequests" />
                                                        </a> / <a href="<c:url value="/customers.do?method=forwardRequest&id=${contactView.summary.latestRequestId}"/>"> <fmt:message key="latestRequestLabel" />
                                                        </a></td>
                                                    </tr>
                                                </c:when>
                                            </c:choose>
                                            <c:if test="${contactView.summary.cancelledRequestCount > 0}">
                                                <tr>
                                                    <td><o:out value="${contactView.summary.cancelledRequestCount}" /></td>
                                                    <td><a href="<c:url value="/customers.do?method=enableRequestListing&cancelled=true"/>"> <fmt:message key="cancelled" />
                                                    </a> <c:if test="${!empty contactView.summary.latestCancelledRequestId}">
                                                            /
                                                            <a href="<c:url value="/customers.do?method=forwardRequest&id=${contactView.summary.latestCancelledRequestId}"/>"> <fmt:message key="latestCancelled" />
                                                            </a>
                                                        </c:if></td>
                                                </tr>
                                            </c:if>
                                        </c:otherwise>
                                    </c:choose>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="spacer"></div>
        </c:if>
        <c:if test="${!empty contactView.summary and !empty contactView.summary.sales}">
            <div class="contentBox">
                <div class="contentBoxHeader">
                    <div class="contentBoxHeaderLeft">
                        <fmt:message key="orders" />
                    </div>
                </div>
                <div class="contentBoxData">
                    <div class="subcolumns">
                        <div class="subcolumn">
                            <table class="valueTable first33">
                                <tbody>
                                    <c:choose>
                                        <c:when test="${contactView.summary.salesCount == 1}">
                                            <tr>
                                                <td>1</td>
                                                <td><a href="<c:url value="/loadSales.do?exit=customer&id=${contactView.summary.latestSalesId}"/>"><fmt:message key="orderFound" /></a></td>
                                            </tr>
                                        </c:when>
                                        <c:when test="${contactView.summary.salesCount > 1}">
                                            <tr>
                                                <td><o:out value="${contactView.summary.salesCount}" /></td>
                                                <td><a href="<c:url value="/customers.do?method=enableSalesListing&cancelled=false&closed=true"/>"> <fmt:message key="ordersFound" />
                                                </a> <c:if test="${contactView.summary.salesCount == contactView.summary.openSalesCount}">
                                                        /
                                                        <a href="<c:url value="/loadSales.do?exit=customer&id=${contactView.summary.latestSalesId}"/>"> <fmt:message key="latestOrder" />
                                                        </a>
                                                    </c:if></td>
                                            </tr>
                                            <c:if test="${contactView.summary.openSalesCount > 0 and contactView.summary.salesCount != contactView.summary.openSalesCount}">
                                                <tr>
                                                    <td><o:out value="${contactView.summary.openSalesCount}" /></td>
                                                    <td><a href="<c:url value="/customers.do?method=enableSalesListing&cancelled=false&closed=false"/>"> <fmt:message key="thereofOpen" />
                                                    </a> / <a href="<c:url value="/loadSales.do?exit=customer&id=${contactView.summary.latestSalesId}"/>"> <fmt:message key="latestOrder" />
                                                    </a></td>
                                                </tr>
                                            </c:if>
                                        </c:when>
                                    </c:choose>
                                    <c:if test="${contactView.summary.cancelledSalesCount > 0}">
                                        <tr>
                                            <td><o:out value="${contactView.summary.cancelledSalesCount}" /></td>
                                            <td><a href="<c:url value="/customers.do?method=enableSalesListing&cancelled=true"/>"> <fmt:message key="cancelled" />
                                            </a> <c:if test="${!empty contactView.summary.latestCancelledSalesId}">
                                                    /
                                                    <a href="<c:url value="/loadSales.do?exit=customer&id=${contactView.summary.latestCancelledSalesId}"/>"> <fmt:message key="latestCancelled" />
                                                    </a>
                                                </c:if></td>
                                        </tr>
                                    </c:if>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </c:if>
    </div>
</div>
</c:if>