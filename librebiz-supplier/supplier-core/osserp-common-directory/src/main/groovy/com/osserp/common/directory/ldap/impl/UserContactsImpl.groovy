/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 *
 */
package com.osserp.common.directory.ldap.impl

import org.apache.directory.groovyldap.LDAP

import com.osserp.common.directory.ldap.LdapConfig
import com.osserp.common.directory.ldap.dao.UserContacts

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class UserContactsImpl extends AbstractLdapDao implements UserContacts {

    UserContactsImpl(LdapConfig configInstance) {
        super(configInstance)
    }


    private String createContactDn(String uid, String cn) {
        //cn=firstname lastname,ou=addressbook,uid=useruid,ou=Users,dc=domain,dc=tld
        "cn=${cn},ou=adressbook,uid=${uid},ou=Users,${config.base}"
    }

    private boolean contactExists(LDAP ldap, String uid, Map contact) {
        def cn = contact.cn
        if (cn && uid) {
            return ldap.exists(createContactDn(uid, cn))
        }
        return false
    }

    private boolean createAddressbook(LDAP ldap, String uid) {
        if (!addressbookExists(ldap, uid)) {
            def entryDn = createAddressbookDn(uid)
            def attrs = [
                objectclass: [
                    'top',
                    'organizationalUnit']
            ]
            ldap.add(entryDn, attrs)
            return addressbookExists(ldap, uid)
        }
        return false
    }

    private String createAddressbookDn(String uid) {
        //ou=addressbook,uid=useruid,ou=Users,dc=domain,dc=tld
        "ou=addressbook,uid=${uid},ou=Users,${config.base}"
    }

    private boolean addressbookExists(LDAP ldap, String uidOrDn) {
        def dn = uidOrDn
        if (!(dn =~ /ou=addressbook/)) {
            dn = createAddressbookDn(uidOrDn)
        }
        ldap.exists(dn)
    }
}
