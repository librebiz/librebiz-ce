SET search_path = osserp, pg_catalog;

INSERT INTO project_contract_status (id, name, i18nkey, running) VALUES (1, 'Entwurf', NULL, false);
INSERT INTO project_contract_status (id, name, i18nkey, running) VALUES (2, 'Angebot', NULL, false);
INSERT INTO project_contract_status (id, name, i18nkey, running) VALUES (3, 'Laufend', NULL, true);
INSERT INTO project_contract_status (id, name, i18nkey, running) VALUES (4, 'Beendet', NULL, false);
SELECT pg_catalog.setval('project_contract_status_id_seq', 4, true);

INSERT INTO project_contract_types (id, end_of_life, name, created_by, label) VALUES (0, false, 'Standard', 6000, 'Standard');
INSERT INTO project_contract_types (id, end_of_life, name, created_by, label) VALUES (1, true, 'Dienstleistungsvertrag', 6000, 'Time & Material');
INSERT INTO project_contract_types (id, end_of_life, name, created_by, label) VALUES (2, true, 'Vermietung', 6000, 'Tenancy Agreement');
SELECT pg_catalog.setval('project_contract_types_id_seq', 2, true);

UPDATE system_properties SET pvalue = '0.16.0', ptype = 'java.lang.String' WHERE name = 'dbmsSchemaVersion';
