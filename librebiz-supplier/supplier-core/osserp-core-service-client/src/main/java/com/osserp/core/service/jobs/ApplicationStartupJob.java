/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 31, 2009 12:31:52 AM 
 * 
 */
package com.osserp.core.service.jobs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.service.Locator;
import com.osserp.common.service.impl.AbstractJob;

import com.osserp.core.dao.ProductPlanningCache;
import com.osserp.core.dao.Products;
import com.osserp.core.dao.RequestListCache;
import com.osserp.core.finance.Stock;
import com.osserp.core.system.SystemConfigManager;

/**
 * {@code ApplicationStartupJob} runs once 15 seconds after application startup and performs an initialization of most frequent caches.
 * 
 * 
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ApplicationStartupJob extends AbstractJob {
    private static Logger log = LoggerFactory.getLogger(ApplicationStartupJob.class.getName());

    protected ApplicationStartupJob(Locator locator) {
        super(locator);
    }

    @Override
    protected void execute() {
        RequestListCache cache = (RequestListCache) getService(RequestListCache.class.getName());
        cache.refresh("<initial/>");
        long duration = System.currentTimeMillis();
        SystemConfigManager systemConfigManager = (SystemConfigManager)
                getService(SystemConfigManager.class.getName());
        Stock stock = systemConfigManager.getDefaultStock();
        if (stock != null && stock.getId() != null &&
                systemConfigManager.isSystemPropertyEnabled("stockManagement")) {
            ProductPlanningCache planningCache = (ProductPlanningCache)
                    getService(ProductPlanningCache.class.getName());
            planningCache.isPlanningAvailable(stock.getId());
            duration = System.currentTimeMillis() - duration;
            if (log.isDebugEnabled()) {
                log.debug("execute: planningCache initialized [duration=" + duration + "ms]");
            }
        }

        Products products = (Products) getService(Products.class.getName());
        duration = System.currentTimeMillis();
        int stockAwareCount = products.getStockAware(false).size();
        duration = System.currentTimeMillis() - duration;
        if (log.isDebugEnabled()) {
            log.debug("execute: stock aware initialized [count=" + stockAwareCount
                    + ", duration=" + duration + "ms]");
        }

        /**
         * TimeRecordingManager timeRecordingManager = (TimeRecordingManager) getService(TimeRecordingManager.class.getName()); EmployeeManager
         * employeeManager = (EmployeeManager) getService(EmployeeManager.class.getName());
         * 
         * List<Employee> activeEmployees = employeeManager.findActiveEmployees();
         * 
         * if (log.isDebugEnabled()) { log.debug("Try to update the day expired hours for this year at all employees"); }
         * 
         * for (int i = 0, j = activeEmployees.size(); i < j; i++) { try { if (timeRecordingManager.updateDayExpiredHours(activeEmployees.get(i),
         * DateUtil.createDate("01.01", true), DateUtil.subtractDays(DateUtil.getCurrentDate(), 1))) { if (log.isDebugEnabled()) {
         * log.debug("Updated day expired hours for: " + activeEmployees.get(i).getName()); } } } catch (ClientException e) {
         * log.warn("Can't book day expired hours for " + activeEmployees.get(i).getName() + ": " + e.getMessage());
         * 
         * } }
         * 
         * 
         * if (log.isDebugEnabled()) { log.debug("Try to update the month expired hours for this year at all employees"); } for (int i = 0, j =
         * activeEmployees.size(); i < j; i++) { for (int k = 0; k < DateUtil.getCurrentMonth(); k++) { try { if
         * (timeRecordingManager.updateMonthExpiredHours(activeEmployees.get(i), DateUtil.createMonth(k, 2009))) { if (log.isDebugEnabled()) {
         * log.debug("Updated month expired hours for: " + activeEmployees.get(i).getName()); } } } catch (ClientException e) {
         * log.warn("Can't book day expired hours for " + activeEmployees.get(i).getName() + ": " + e.getMessage());
         * 
         * } } }
         **/

    }
}
