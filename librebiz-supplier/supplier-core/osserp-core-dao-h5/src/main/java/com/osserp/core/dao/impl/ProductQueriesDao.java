/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Jun 6, 2009 7:06:11 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;
import org.hibernate.SessionFactory;

import com.osserp.common.BackendException;
import com.osserp.common.Constants;
import com.osserp.common.OptionsCache;
import com.osserp.common.SearchRequest;
import com.osserp.common.dao.Tables;
import com.osserp.common.util.CollectionUtil;
import com.osserp.common.util.StringUtil;
import com.osserp.core.Item;
import com.osserp.core.dao.ProductQueries;
import com.osserp.core.dao.Products;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.finance.Record;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductClassificationConfig;
import com.osserp.core.products.ProductPriceQueryResult;
import com.osserp.core.products.ProductSelection;
import com.osserp.core.products.ProductSelectionConfig;
import com.osserp.core.products.ProductSelectionConfigItem;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author so <so@osserp.com>
 * 
 */
public class ProductQueriesDao extends AbstractProductSelectionAwares implements ProductQueries {
    private static Logger log = LoggerFactory.getLogger(ProductQueriesDao.class.getName());
    private int blankSearchDefaultFetchsize = 100;
    private String salesOrderItemTable;
    private String salesOrderTable;
    private String productClassificationConfigs;

    protected ProductQueriesDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache optionsCache,
            Products products) {
        super(jdbcTemplate, tables, sessionFactory, optionsCache, products);
        productClassificationConfigs = getTable(TableKeys.PRODUCT_CLASSIFICATION_CONFIGS);
        salesOrderItemTable = getTable(TableKeys.SALES_ORDER_ITEMS);
        salesOrderTable = getTable(TableKeys.SALES_ORDERS);
    }

    /**
     * Finds all active products available by an optional selection
     * @param userSelection
     * @param includeEol
     * @param fetchSize
     * @return products
     */
    public List<Product> find(ProductSelection userSelection, boolean includeEol, int fetchSize) {
        List<Long> primaryKeys = (List<Long>) jdbcTemplate.query(
                completeQuery(createSelect(userSelection, includeEol), fetchSize), getLongRowMapper());
        List<Product> list = (primaryKeys.isEmpty() ? new ArrayList<>() : load(primaryKeys));
        if (log.isDebugEnabled()) {
            log.debug("find() done [selection=" + (userSelection == null ? "null" : userSelection.getId())
                    + ", count=" + list.size() + "]");
        }
        return list;
    }

    public List<Product> find(ProductSelectionConfig config, boolean includeEol, int fetchSize) {
        List<Long> primaryKeys = (List<Long>) jdbcTemplate.query(
                completeQuery(createSelect(config, includeEol), fetchSize),
                getLongRowMapper());
        List<Product> list = (primaryKeys.isEmpty() ? new ArrayList<>() : load(primaryKeys));
        if (log.isDebugEnabled()) {
            log.debug("find() done [config=" + (config == null ? "null" : config.getId())
                    + ", count=" + list.size() + "]");
        }
        return list;
    }

    public List<Product> find(ProductSelectionConfigItem item, boolean includeEol, int fetchSize) {
        List<Long> primaryKeys = (List<Long>) jdbcTemplate.query(
                completeQuery(createSelect(item, includeEol), fetchSize), getLongRowMapper());
        List<Product> list = (primaryKeys.isEmpty() ? new ArrayList<>() : load(primaryKeys));
        if (log.isDebugEnabled()) {
            log.debug("find() done [item="
                    + ((item != null && item.getId() == null) ? "manual" :
                            (item == null ? "null" : item.getId()))
                    + ", count=" + list.size() + "]");
        }
        return list;
    }

    public List<Product> find(
            ProductSelectionConfig config,
            String columnKey,
            String val,
            boolean startsWith,
            boolean includeEol,
            int fetchSize) {

        if (isEmpty(columnKey) || isEmpty(val)) {
            if (log.isDebugEnabled()) {
                log.debug("find() invoked with empty column and/or value, " +
                        "forwarding to selection only search...");
            }
            return find(config, includeEol, fetchSize);
        }
        StringBuilder sql = createSelect(config, includeEol);
        Boolean whereClause = false;
        if (sql.toString().indexOf(" WHERE ") != -1) {
            whereClause = true;
        }
        appendSearchParams(
                sql,
                getColumn(columnKey),
                val,
                startsWith,
                false,
                whereClause);
        List<Long> primaryKeys = (List<Long>) jdbcTemplate.query(
                completeQuery(sql, fetchSize), getLongRowMapper());
        List<Product> list = (primaryKeys.isEmpty() ? new ArrayList<>() : load(primaryKeys));
        if (log.isDebugEnabled()) {
            log.debug("find() done [config=" + (config == null ? "null" : config.getId())
                    + ", columnKey=" + columnKey + ", val=" + val + ", startsWith=" + startsWith
                    + ", includeEol=" + includeEol + ", count=" + list.size() + "]");
        }
        return list;
    }

    public List<ProductClassificationConfig> findClassificationConfigs() {
        String orderByColumns = "";
        String columns = getPropertyString("productClassificationOrderByColumns");
        if (columns != null) {
            orderByColumns = " ORDER BY " + columns;
        } else {
            orderByColumns = " ORDER BY type_id, group_name";
        }
        List<ProductClassificationConfig> result = (List<ProductClassificationConfig>) jdbcTemplate.query(
                "SELECT * FROM " + productClassificationConfigs + orderByColumns,
                new RowMapperResultSetExtractor(new ProductClassificationConfigRowMapper(getOptionsCache())));
        return result;
    }

    private void appendSearchParams(
            StringBuilder sql,
            String column,
            String val,
            boolean startsWith,
            boolean orderByColumn,
            boolean isWhereClauseGiven) {

        if (column != null && val != null) {
            String value = fetchInsecureInput(val);
            if (isWhereClauseGiven) {
                sql.append(" AND ");
            } else {
                sql.append(" WHERE ");
            }
            sql.append(column);
            if (startsWith) {
                sql.append("::varchar ~* '^");
            } else {
                sql.append("::varchar ~* '");
            }
            String[] vals = StringUtil.getTokenArray(value, " ");
            if (vals.length == 1) {
                sql.append(value).append("'");
            } else {
                sql.append(vals[0]).append("'");
                for (int i = 2; i <= vals.length; i++) {
                    sql
                        .append(" AND ")
                        .append(column)
                        .append("::varchar ~* '")
                        .append(vals[i-1])
                        .append("'");
                }
            }
        }
        if (orderByColumn) {
            sql.append(" ORDER BY ").append(column);
        }
    }

    private String completeQuery(StringBuilder sql, int fetchsize) {
        if (sql.indexOf("ORDER BY") < 0) {
            sql.append(" ORDER BY name");
        }
        sql.append(" limit ").append(fetchsize == 0 || SearchRequest.IGNORE_FETCHSIZE == fetchsize ? blankSearchDefaultFetchsize : fetchsize);
        if (log.isDebugEnabled()) {
            log.debug("completeQuery: done [sql=" + sql.toString() + "]");
        }
        return sql.toString();
    }

    @Override
    protected final String getColumn(String columnKey) {
        if (columnKey == null) {
            throw new BackendException("error.configuration");
        }
        if (columnKey.equals(Constants.SEARCH_BY_ID)) {
            return "product_id";
        } else if (columnKey.equals(Constants.SEARCH_BY_MATCHCODE)) {
            return "matchcode";
        } else if (columnKey.equals(Constants.SEARCH_BY_NAME)) {
            return "name";
        } else if (columnKey.equals(Constants.SEARCH_BY_DESCRIPTION)) {
            return "description";
        } else {
            throw new BackendException("error.configuration");
        }
    }

    public List<Product> addLastPricePaid(List<Product> productList, Long contactReference, boolean contactReferenceCustomer) {
        if (!isEmpty(contactReference) && contactReferenceCustomer && !productList.isEmpty()) {
            String ids = CollectionUtil.createEntityIds(productList);
            Map<Long, ProductPriceQueryResult> result = getLastPricePaid(ids, contactReference, contactReferenceCustomer); 
            for (int i = 0, j = productList.size(); i < j; i++) {
                Product next = productList.get(i);
                ProductPriceQueryResult res = result.get(next.getProductId());
                if (res != null) {
                    next.getSummary().setLastPricePaid(res.getPrice());
                    next.getSummary().setLastPricePaidOn(res.getCreated());
                }
            }
        }
        return productList;
    }

    public void addLastPricePaid(Record record, Long contactReference, boolean contactReferenceCustomer) {
        List<Long> list = new ArrayList<>();
        for (int i = 0, j = record.getItems().size(); i < j; i++) {
            list.add(record.getItems().get(i).getProduct().getProductId());
        }
        String ids = CollectionUtil.createEntityIds(list);
        Map<Long, ProductPriceQueryResult> result = getLastPricePaid(ids, contactReference, contactReferenceCustomer);
        for (int i = 0, j = record.getItems().size(); i < j; i++) {
            Item next = record.getItems().get(i);
            ProductPriceQueryResult res = result.get(next.getProduct().getProductId());
            if (res != null) {
                next.getProduct().getSummary().setLastPricePaid(res.getPrice());
                next.getProduct().getSummary().setLastPricePaidOn(res.getCreated());
            }
        }
    }

    public Map<Long, ProductPriceQueryResult> getLastPricePaid(String idList, Long contactReference, boolean contactReferenceCustomer) {
        Map<Long, ProductPriceQueryResult> result = new HashMap<>();
        if (!isEmpty(idList) && !isEmpty(contactReference) && contactReferenceCustomer) {
            List<ProductPriceQueryResult> list = new ArrayList<>();
            StringBuilder sql = new StringBuilder("SELECT "
                    + "i.id as item_id,"
                    + " i.reference_id as record_id,"
                    + " o.type_id as record_type,"
                    + " i.product_id,"
                    + " o.contact_id,"
                    + " i.price,"
                    + " o.created as price_date"
                    + " FROM ");
            sql.append(salesOrderItemTable)
                .append(" i LEFT JOIN ")
                .append(salesOrderTable)
                .append(" o ON (i.reference_id = o.id AND o.status >= ")
                .append(Record.STAT_SENT)
                .append(") WHERE i.product_id IN (")
                .append(idList)
                .append(") AND o.contact_id = ? AND o.created = (SELECT max(created) FROM ")
                .append(salesOrderTable)
                .append(" so WHERE so.status >= ")
                .append(Record.STAT_SENT)
                .append(" AND so.contact_id = ? AND so.id IN (SELECT reference_id FROM ")
                .append(salesOrderItemTable)
                .append(" soi WHERE soi.product_id = i.product_id))");
            list = (List<ProductPriceQueryResult>) jdbcTemplate.query(
                    sql.toString(),
                    new Object[] { contactReference, contactReference },
                    new int[] { Types.BIGINT, Types.BIGINT },
                    ProductPriceQueryRowMapper.getReader(getOptionsCache()));
            if (!list.isEmpty()) {
                for (int i = 0, j = list.size(); i < j; i++) {
                    ProductPriceQueryResult next = list.get(i);
                    result.put(next.getProductId(), next);
                }
            }
        }
        return result;
    }

    public boolean isMatching(List<ProductSelectionConfig> selectionConfigs, Long productId) {
        for (int i = 0, j = selectionConfigs.size(); i < j; i++) {
            ProductSelectionConfig config = selectionConfigs.get(i);
            for (int k = 0, l = config.getItems().size(); k < l; k++) {
                ProductSelectionConfigItem item = config.getItems().get(k);
                List<Long> primaryKeys = findMatchingIds(item, true);
                
                for (int m = 0, n = primaryKeys.size(); m < n; m++) {
                    Long nextProduct = primaryKeys.get(m);
                    if (nextProduct.equals(productId)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private List<Long> findMatchingIds(ProductSelectionConfigItem item, boolean includeEol) {
        return (List) jdbcTemplate.query(createSelect(item, includeEol).toString(), getLongRowMapper());
    }
}
