/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 4, 2011 1:43:44 PM 
 * 
 */
package com.osserp.core.dao.hrm.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;
import com.osserp.common.util.DateUtil;
import com.osserp.core.hrm.TimeRecording;
import com.osserp.core.hrm.TimeRecordingPeriod;
import com.osserp.core.model.hrm.TimeRecordingImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 */
public abstract class AbstractTimeRecordings extends AbstractTimeRecordingAware {
    private static Logger log = LoggerFactory.getLogger(AbstractTimeRecordings.class.getName());

    protected AbstractTimeRecordings(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache options,
            String timeRecordingTableKey,
            String timeRecordingPeriodTableKey,
            String timeRecordTableKey,
            String timeRecordCorrectionsTableKey,
            String timeRecordingApprovalsTableKey) {
        super(jdbcTemplate, tables, sessionFactory, options, timeRecordingTableKey,
                timeRecordingPeriodTableKey, timeRecordTableKey,
                timeRecordCorrectionsTableKey, timeRecordingApprovalsTableKey);
    }

    public TimeRecording load(Long id) {
        return (TimeRecording) getCurrentSession().load(TimeRecordingImpl.class.getName(), id);
    }

    public TimeRecording createClosingsIfRequired(TimeRecording recording) {
        long start = System.currentTimeMillis();
        if (log.isDebugEnabled()) {
            log.debug("createClosingsIfRequired() invoked [recording=" + (recording == null ? "null" : recording.getId()) + "]");
        }
        if (recording != null && (recording.getLastClosingActionTime() == null
                || !DateUtil.isSameDay(DateUtil.getCurrentDate(), recording.getLastClosingActionTime()))) {

            boolean created = createClosings(recording);
            recording.setLastClosingActionTime(DateUtil.getCurrentDate());
            save(recording);
            TimeRecording updated = load(recording.getId());
            if (log.isDebugEnabled()) {
                log.debug("createClosingsIfRequired() done [id=" + updated.getId()
                        + ", employee=" + updated.getReference()
                        + ", updatesPerformed=" + created
                        + ", duration=" + (System.currentTimeMillis() - start) + "]");
            }
            return updated;
        } else if (log.isDebugEnabled()) {
            log.debug("createClosingsIfRequired() ignoring call [recording="
                    + (recording == null ? "null]" :
                            (recording.getId() + ", lastClosingActionTime=" +
                            (recording.getLastClosingActionTime() == null ? "null]" :
                                    (recording.getLastClosingActionTime()
                                            + ", isSameDay="
                                            + DateUtil.isSameDay(DateUtil.getCurrentDate(), recording.getLastClosingActionTime())
                                            + "]")
                            )
                            )
                    ));
        }
        return recording;
    }

    private boolean createClosings(TimeRecording recording) {
        boolean closingCreated = false;
        if (recording != null) {
            for (int i = 0, j = recording.getRecordings().size(); i < j; i++) {
                TimeRecordingPeriod next = recording.getRecordings().get(i);
                if (next.createClosingsIfRequired(getStopType(), getDefaultStatus())) {
                    closingCreated = true;
                    save(recording);
                }
            }
        }
        return closingCreated;
    }
}
