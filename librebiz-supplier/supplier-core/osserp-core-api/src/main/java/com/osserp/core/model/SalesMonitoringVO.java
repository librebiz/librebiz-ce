/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 1, 2008 5:27:55 PM 
 * 
 */
package com.osserp.core.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.jdom2.Element;

import com.osserp.common.Option;
import com.osserp.common.util.DateFormatter;
import com.osserp.common.util.NumberFormatter;
import com.osserp.common.util.StringUtil;

import com.osserp.core.BusinessType;
import com.osserp.core.projects.ProjectFcsDisplay;
import com.osserp.core.sales.SalesMonitoringItem;
import com.osserp.core.system.BranchOffice;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesMonitoringVO extends SalesListItemVO implements SalesMonitoringItem {

    // fcs dates
    private Date releaseDate = null;
    private Date verifyDate = null;
    private Date startupDate = null;
    private Date downpaymentRequest = null;
    private Date downpaymentResponse = null;
    private Date deliveryInvoiceRequest = null;
    private Date deliveryInvoiceResponse = null;
    private Date finalInvoiceRequest = null;
    private Date finalInvoiceResponse = null;
    private Date stopDate = null;
    private String stopNote = null;

    private Date partialPaymentDate = null;
    private String partialPaymentNote = null;
    private Double openAmount = null;
    private List<ProjectFcsDisplay> flowControlSheet = new ArrayList<ProjectFcsDisplay>();

    protected SalesMonitoringVO() {
        super();
    }

    public SalesMonitoringVO(
            String customerName,
            String shortName,
            Long contactType,
            Long id,
            Long requestId,
            BusinessType type,
            Long customerId,
            Long status,
            Date created,
            Long createdBy,
            Long managerId,
            Long managerSubId,
            Long salesId,
            BranchOffice branch,
            String name,
            String street,
            String zipcode,
            String city,
            Double capacity,
            boolean stopped,
            boolean cancelled,
            Long installerId,
            Integer installation_days,
            Date installation_date,
            Date delivery_date,
            Date confirmation_date,
            Long originId,
            Long originTypeId,
            Long fcs_id,
            Date action_created,
            Long action_created_by,
            boolean action_cancels_other,
            Long action_canceled_by,
            Long action_id,
            String action_note,
            String action_name,
            Integer action_order_id,
            Long action_status,
            Long action_group,
            boolean action_cancels_sales,
            boolean action_stops_sales,
            boolean action_starts_up,
            boolean action_manager_selecting,
            boolean action_initializing,
            boolean action_releaeseing,
            boolean requests_dp,
            boolean response_dp,
            boolean requests_di,
            boolean response_di,
            boolean requests_fi,
            boolean response_fi,
            boolean action_closing_delivery,
            boolean action_enabling_delivery,
            boolean action_partial_delivery,
            boolean action_confirming,
            boolean action_verifying,
            boolean action_partial_payment,
            String accountingReference,
            Long shippingId,
            Long paymentId) {
        this(
                customerName,
                shortName,
                contactType,
                id,
                requestId,
                type,
                customerId,
                status,
                created,
                createdBy,
                managerId,
                null,
                managerSubId,
                salesId,
                null, // salesKey
                branch,
                name,
                street,
                zipcode,
                city,
                capacity,
                stopped,
                cancelled,
                installerId,
                installation_days,
                installation_date,
                delivery_date,
                confirmation_date,
                originId,
                originTypeId,
                fcs_id,
                action_created,
                action_created_by,
                action_cancels_other,
                action_canceled_by,
                action_id,
                action_note,
                action_name,
                action_order_id,
                action_status,
                action_group,
                action_cancels_sales,
                action_stops_sales,
                action_starts_up,
                action_manager_selecting,
                action_initializing,
                action_releaeseing,
                requests_dp,
                response_dp,
                requests_di,
                response_di,
                requests_fi,
                response_fi,
                action_closing_delivery,
                action_enabling_delivery,
                action_partial_delivery,
                action_confirming,
                action_verifying,
                action_partial_payment,
                accountingReference,
                shippingId,
                paymentId);
    }

    public SalesMonitoringVO(
            String customerName,
            String shortName,
            Long contactType,
            Long id,
            Long requestId,
            BusinessType type,
            Long customerId,
            Long status,
            Date created,
            Long createdBy,
            Long managerId,
            String managerKey,
            Long managerSubId,
            Long salesId,
            String salesKey,
            BranchOffice branch,
            String name,
            String street,
            String zipcode,
            String city,
            Double capacity,
            boolean stopped,
            boolean cancelled,
            Long installerId,
            Integer installation_days,
            Date installation_date,
            Date delivery_date,
            Date confirmation_date,
            Long originId,
            Long originTypeId,
            Long fcs_id,
            Date action_created,
            Long action_created_by,
            boolean action_cancels_other,
            Long action_canceled_by,
            Long action_id,
            String action_note,
            String action_name,
            Integer action_order_id,
            Long action_status,
            Long action_group,
            boolean action_cancels_sales,
            boolean action_stops_sales,
            boolean action_starts_up,
            boolean action_manager_selecting,
            boolean action_initializing,
            boolean action_releaeseing,
            boolean requests_dp,
            boolean response_dp,
            boolean requests_di,
            boolean response_di,
            boolean requests_fi,
            boolean response_fi,
            boolean action_closing_delivery,
            boolean action_enabling_delivery,
            boolean action_partial_delivery,
            boolean action_confirming,
            boolean action_verifying,
            boolean action_partial_payment,
            String accountingReference,
            Long shippingId,
            Long paymentId) {
        super(
                customerName,
                shortName,
                contactType,
                id,
                requestId,
                type,
                customerId,
                status,
                created,
                createdBy,
                managerId,
                managerKey,
                managerSubId,
                salesId,
                salesKey,
                branch,
                name,
                street,
                zipcode,
                city,
                capacity,
                stopped,
                cancelled,
                installerId,
                null,
                originId,
                originTypeId,
                accountingReference,
                shippingId,
                paymentId,
                delivery_date,
                confirmation_date);
        setInstallationDate(installation_date);
        setInstallationDays(installation_days == null ? 0 : installation_days);

        addFlowControl(
                fcs_id,
                action_created,
                action_created_by,
                action_cancels_other,
                action_canceled_by,
                action_id,
                action_note,
                action_name,
                action_order_id,
                action_status,
                action_group,
                action_cancels_sales,
                action_stops_sales,
                action_starts_up,
                action_manager_selecting,
                action_initializing,
                action_releaeseing,
                requests_dp,
                response_dp,
                requests_di,
                response_di,
                requests_fi,
                response_fi,
                action_closing_delivery,
                action_enabling_delivery,
                action_partial_delivery,
                action_confirming,
                action_verifying,
                action_partial_payment);
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    protected void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Date getVerifyDate() {
        return verifyDate;
    }

    protected void setVerifyDate(Date verifyDate) {
        this.verifyDate = verifyDate;
    }

    public Date getStartupDate() {
        return startupDate;
    }

    protected void setStartupDate(Date startupDate) {
        this.startupDate = startupDate;
    }

    public Date getDownpaymentRequest() {
        return downpaymentRequest;
    }

    protected void setDownpaymentRequest(Date downpaymentRequest) {
        this.downpaymentRequest = downpaymentRequest;
    }

    public Date getDownpaymentResponse() {
        return downpaymentResponse;
    }

    protected void setDownpaymentResponse(Date downpaymentResponse) {
        this.downpaymentResponse = downpaymentResponse;
    }

    public Date getDeliveryInvoiceRequest() {
        return deliveryInvoiceRequest;
    }

    protected void setDeliveryInvoiceRequest(Date deliveryInvoiceRequest) {
        this.deliveryInvoiceRequest = deliveryInvoiceRequest;
    }

    public Date getDeliveryInvoiceResponse() {
        return deliveryInvoiceResponse;
    }

    protected void setDeliveryInvoiceResponse(Date deliveryInvoiceResponse) {
        this.deliveryInvoiceResponse = deliveryInvoiceResponse;
    }

    public Date getFinalInvoiceRequest() {
        return finalInvoiceRequest;
    }

    protected void setFinalInvoiceRequest(Date finalInvoiceRequest) {
        this.finalInvoiceRequest = finalInvoiceRequest;
    }

    public Date getFinalInvoiceResponse() {
        return finalInvoiceResponse;
    }

    protected void setFinalInvoiceResponse(Date finalInvoiceResponse) {
        this.finalInvoiceResponse = finalInvoiceResponse;
    }

    public Date getPartialPaymentDate() {
        return partialPaymentDate;
    }

    protected void setPartialPaymentDate(Date partialPaymentDate) {
        this.partialPaymentDate = partialPaymentDate;
    }

    public String getPartialPaymentNote() {
        return partialPaymentNote;
    }

    protected void setPartialPaymentNote(String partialPaymentNote) {
        this.partialPaymentNote = partialPaymentNote;
    }

    public Date getStopDate() {
        return stopDate;
    }

    protected void setStopDate(Date stopDate) {
        this.stopDate = stopDate;
    }

    public String getStopNote() {
        return stopNote;
    }

    protected void setStopNote(String stopNote) {
        this.stopNote = stopNote;
    }

    public Double getOpenAmount() {
        return openAmount;
    }

    public void setOpenAmount(Double openAmount) {
        this.openAmount = openAmount;
    }

    public List<ProjectFcsDisplay> getFlowControlSheet() {
        return flowControlSheet;
    }

    public void addFlowControl(
            Long id,
            Date created,
            Long createdBy,
            boolean cancelsAction,
            Long canceledBy,
            Long actionId,
            String note,
            String name,
            Integer orderId,
            Long status,
            Long group,
            boolean cancels,
            boolean stops,
            boolean startsUp,
            boolean managerSelecting,
            boolean initializing,
            boolean releaeseing,
            boolean requests_dp,
            boolean response_dp,
            boolean requests_di,
            boolean response_di,
            boolean requests_fi,
            boolean response_fi,
            boolean closingDelivery,
            boolean enablingDelivery,
            boolean partialDelivery,
            boolean confirming,
            boolean verifying,
            boolean partialPayment) {
        ProjectFcsDisplay o = new ProjectFcsDisplay(
                id,
                created,
                createdBy,
                getId(),
                cancelsAction,
                canceledBy,
                actionId,
                note,
                name,
                orderId,
                status,
                group,
                cancels,
                stops,
                startsUp,
                managerSelecting,
                initializing,
                releaeseing,
                requests_dp,
                response_dp,
                requests_di,
                response_di,
                requests_fi,
                response_fi,
                closingDelivery,
                enablingDelivery,
                partialDelivery,
                confirming,
                verifying,
                partialPayment);
        flowControlSheet.add(o);
        if (!o.isCancelsAction() && o.getCanceledBy() == null) {
            if (o.isConfirming()) {
                setConfirmationDate(o.getCreated());
            } else if (o.isReleaeseing()) {
                this.releaseDate = o.getCreated();
            } else if (o.isStartsUp()) {
                this.startupDate = o.getCreated();
            } else if (o.isVerifying()) {
                this.verifyDate = o.getCreated();
            } else if (o.isPartialPayment()) {
                this.partialPaymentDate = o.getCreated();
                this.partialPaymentNote = o.getNote();
            } else if (o.isRequests_dp()) {
                this.downpaymentRequest = o.getCreated();
            } else if (o.isResponse_dp()) {
                this.downpaymentResponse = o.getCreated();
            } else if (o.isRequests_di()) {
                this.deliveryInvoiceRequest = o.getCreated();
            } else if (o.isResponse_di()) {
                this.deliveryInvoiceResponse = o.getCreated();
            } else if (o.isRequests_fi()) {
                this.finalInvoiceRequest = o.getCreated();
            } else if (o.isResponse_fi()) {
                this.finalInvoiceResponse = o.getCreated();
            } else if (o.isStops() && isStopped()) {
                this.stopDate = o.getCreated();
                this.stopNote = o.getNote();
            }
        }
    }

    public Element getValues(Map employees) {
        Element prj = new Element("project");
        prj.addContent(new Element("prj").setText(String.valueOf(getId().longValue())));
        prj.addContent(new Element("name").setText(StringUtil.cut(getName(), 28, true)));
        prj.addContent(new Element("status").setText(String.valueOf(getStatus().intValue())));
        if (getSalesId() == null) {
            prj.addContent(new Element("sales").setText(""));
        } else {
            prj.addContent(new Element("sales").setText(getEmployee(employees, getSalesId())));
        }
        if (getManagerId() == null) {
            prj.addContent(new Element("manager").setText(""));
        } else {
            prj.addContent(new Element("manager").setText(getEmployee(employees, getManagerId())));
        }
        prj.addContent(new Element("startRequest").setText(DateFormatter.getShortDate(getCreated())));
        prj.addContent(new Element("started").setText(DateFormatter.getShortDate(releaseDate)));
        prj.addContent(new Element("downpaymentRequest").setText(DateFormatter.getShortDate(downpaymentRequest)));
        prj.addContent(new Element("downpayment").setText(DateFormatter.getShortDate(downpaymentResponse)));
        prj.addContent(new Element("deliveryInvoiceRequest").setText(DateFormatter.getShortDate(deliveryInvoiceRequest)));
        prj.addContent(new Element("deliveryInvoice").setText(DateFormatter.getShortDate(deliveryInvoiceResponse)));
        prj.addContent(new Element("finalInvoiceRequest").setText(DateFormatter.getShortDate(finalInvoiceRequest)));
        prj.addContent(new Element("finalInvoice").setText(DateFormatter.getShortDate(finalInvoiceResponse)));
        prj.addContent(new Element("partialPayment").setText(DateFormatter.getShortDate(partialPaymentDate)));
        if (isStopped()) {
            prj.addContent(new Element("stopped").setText(DateFormatter.getShortDate(stopDate)));
        } else {
            prj.addContent(new Element("stopped"));
        }
        prj.addContent(new Element("amount").setText(NumberFormatter.getValue(openAmount, NumberFormatter.CURRENCY)));
        return prj;
    }

    protected void setFlowControlSheet(List<ProjectFcsDisplay> flowControlSheet) {
        this.flowControlSheet = flowControlSheet;
    }

    protected String getEmployee(Map map, Long id) {
        if (map == null) {
            return "";
        } else if (id == null) {
            return "";
        } else {
            Option o = (Option) map.get(id);
            if (o == null) {
                return "";
            }
            return o.getName();
        }
    }

}
