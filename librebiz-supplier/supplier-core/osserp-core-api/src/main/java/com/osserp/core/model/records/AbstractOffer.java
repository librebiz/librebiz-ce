/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 29-Jan-2007 18:58:50 
 * 
 */
package com.osserp.core.model.records;

import java.util.Date;
import java.util.List;

import com.osserp.core.ItemPosition;
import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.BookingType;
import com.osserp.core.finance.Offer;
import com.osserp.core.finance.RecordType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractOffer extends AbstractPaymentAgreementAwareRecord implements Offer {
    private Date delivery;
    private Date validUntil = null;

    /**
     * Default constructor required by Serializable
     */
    protected AbstractOffer() {
        super();
    }

    /**
     * Creates a new offer by calculation items.
     * @param id
     * @param company
     * @param branchId
     * @param type
     * @param reference
     * @param contact
     * @param createdBy
     * @param taxRate
     * @param reducedTaxRate
     * @param validUntil
     * @param sales
     * @param items
     * @param optionalItems
     */
    protected AbstractOffer(
            Long id,
            Long company,
            Long branchId,
            RecordType type,
            Long reference,
            ClassifiedContact contact,
            Employee createdBy,
            Double taxRate,
            Double reducedTaxRate,
            Date validUntil,
            Long sales,
            List<ItemPosition> items,
            List<ItemPosition> optionalItems) {
        super(
                id,
                company,
                branchId,
                type,
                reference,
                contact,
                createdBy,
                taxRate,
                reducedTaxRate,
                sales,
                items,
                optionalItems);
        this.validUntil = validUntil;
    }

    /**
     * Creates a new payment aware record with customized change-item flags
     * @param id
     * @param company
     * @param branchId
     * @param type
     * @param reference
     * @param contact
     * @param createdBy
     * @param sale
     * @param taxRate
     * @param reducedTaxRate
     */
    protected AbstractOffer(
            Long id,
            RecordType type,
            BookingType bookingType,
            Long company,
            Long branchId,
            Long reference,
            ClassifiedContact contact,
            Employee createdBy,
            Long sale,
            Double taxRate,
            Double reducedTaxRate) {
        super(
                id,
                company,
                branchId,
                type,
                reference,
                contact,
                createdBy,
                sale,
                taxRate,
                reducedTaxRate,
                true,
                true);
        setBookingType(bookingType);
    }

    public Date getDelivery() {
        return delivery;
    }

    public void setDelivery(Date delivery) {
        this.delivery = delivery;
    }

    public Date getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(Date validUntil) {
        this.validUntil = validUntil;
    }

}
