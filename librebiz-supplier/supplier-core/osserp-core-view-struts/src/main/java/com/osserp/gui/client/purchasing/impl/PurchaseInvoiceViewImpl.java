/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 26-Jun-2006 13:34:19 
 * 
 */
package com.osserp.gui.client.purchasing.impl;

import java.util.Date;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;
import com.osserp.common.web.ViewName;

import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.finance.RecordManager;
import com.osserp.core.purchasing.PurchaseInvoice;
import com.osserp.core.purchasing.PurchaseInvoiceManager;
import com.osserp.core.purchasing.PurchaseInvoiceType;
import com.osserp.core.purchasing.PurchaseOrder;
import com.osserp.core.sales.Sales;
import com.osserp.core.sales.SalesSearch;

import com.osserp.gui.client.purchasing.PurchaseInvoiceView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("purchaseInvoiceView")
public class PurchaseInvoiceViewImpl extends AbstractPurchaseInvoiceView implements PurchaseInvoiceView {
    private static Logger log = LoggerFactory.getLogger(PurchaseInvoiceViewImpl.class.getName());
    private boolean purchasePaymentWarnings = false;
    private boolean deleteClosedMode = false;
    private Date deliveredFrom = null;
    private Date deliveredUntil = null;
    private String value = null;
    private String[] requiredPermissionsDelete = null;
    private Sales sales = null;

    protected PurchaseInvoiceViewImpl() {
        super();
        requiredPermissionsDelete = new String[] { "executive", "purchase_invoice_delete" };
    }

    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        super.init(request);
        purchasePaymentWarnings = isSystemPropertyEnabled("purchasePaymentWarnings");
    }

    public void initDisplay(Invoice invoice, String exitTarget) {
        setRecord(invoice);
        setExitTarget(exitTarget);
    }

    public void create(PurchaseOrder order) throws ClientException, PermissionException {
        PurchaseInvoiceManager manager = getInvoiceManager();
        Invoice invoice = manager.create(getDomainUser().getEmployee(), order);
        setRecord(invoice);
        refresh();
        if (log.isDebugEnabled()) {
            log.debug("create() done for order " + order.getId()
                    + ", new invoice " + invoice.getId());
        }
    }

    public void loadOther(Long id) {
        Record record = loadRecord(id);
        initContactPerson(record);
        setRecord(record);
    }

    public boolean isPurchasePaymentWarnings() {
        return purchasePaymentWarnings;
    }

    public void setPurchasePaymentWarnings(boolean purchasePaymentWarnings) {
        this.purchasePaymentWarnings = purchasePaymentWarnings;
    }

    public boolean isDeleteClosedMode() {
        return deleteClosedMode;
    }

    public void setDeleteClosedMode(boolean deleteClosedMode) {
        this.deleteClosedMode = deleteClosedMode;
    }

    public void deleteClosed(Form fh) throws ClientException, PermissionException {
        if (!getDomainUser().isPermissionGrant(requiredPermissionsDelete)) {
            throw new PermissionException();
        }
        PurchaseInvoice record = (PurchaseInvoice) getRecord();
        PurchaseInvoiceManager manager = getInvoiceManager();
        manager.cancel(record, getDomainEmployee(), fh.getString(Form.NOTE));
        setBean(null);
        deleteClosedMode = false;
        setSetupMode(false);
        for (Iterator<RecordDisplay> i = getList().iterator(); i.hasNext();) {
            RecordDisplay next = i.next();
            if (next.getId().equals(record.getId())) {
                i.remove();
                if (log.isDebugEnabled()) {
                    log.debug("deleteClosed() removed deleted record [id=" + next.getId() + "]");
                }
            }
        }
    }

    public void openClosed() throws ClientException, PermissionException {
        PurchaseInvoice record = (PurchaseInvoice) getRecord();
        PurchaseInvoiceManager manager = getInvoiceManager();
        manager.reopenClosed(getDomainEmployee(), record);
        setSetupMode(false);
        refresh();
    }

    public boolean isRecordReopened() {
        PurchaseInvoice record = (PurchaseInvoice) getBean();
        return (record != null && PurchaseInvoice.STAT_CHANGED.equals(record.getStatus()));
    }

    public boolean isConditionsEdit() {
        return false;
    }

    public Date getDeliveredFrom() {
        return deliveredFrom;
    }

    public void setDeliveredFrom(Date deliveredFrom) {
        this.deliveredFrom = deliveredFrom;
    }

    public Date getDeliveredUntil() {
        return deliveredUntil;
    }

    public void setDeliveredUntil(Date deliveredUntil) {
        this.deliveredUntil = deliveredUntil;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Sales getReferencedSales() {
        return sales;
    }

    @Override
    public void setRecord(Record record) {
        super.setRecord(record);
        if (record != null) { 
            if (record.getBusinessCaseId() != null) {
                try {
                    sales = getSalesSearch().fetchSales(record.getBusinessCaseId());
                } catch (Exception e) {
                    log.warn("setRecord() failed to set referenced sales [message=" + e.getMessage() + "]", e);
                }
            }
            if (!((PurchaseInvoiceType) record.getBookingType()).isPrintable()) {
                loadReferencedDocument(record);
            }
        } else {
            sales = null;
            setDocument(null);
        }
    }

    @Override
    public RecordManager getRecordManager() {
        return getInvoiceManager();
    }

    protected PurchaseInvoiceManager getInvoiceManager() {
        return (PurchaseInvoiceManager) getService(PurchaseInvoiceManager.class.getName());
    }

    protected SalesSearch getSalesSearch() {
        return (SalesSearch) getService(SalesSearch.class.getName());
    }
}
