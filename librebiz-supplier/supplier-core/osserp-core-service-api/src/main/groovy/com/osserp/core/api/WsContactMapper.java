/**
 *
 * Copyright (C) 2012 The original author or authors.
 * 
 * Created on 12.12.2012 
 * 
 */
package com.osserp.core.api;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.osserp.api.WsContact;
import com.osserp.api.WSContact;
import com.osserp.common.Entity;
import com.osserp.common.mail.EmailAddress;
import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.Phone;
import com.osserp.core.contacts.PhoneType;

/**
 * 
 * @author rk <rk@osserp.com>
 * 
 */
class WsContactMapper {

    public static final String CUSTOMER_STATUS = "customer";
    public static final String DEFAULT_STATUS = "interest";

    WsContactMapper() {
        super();
    }

    WSContact map(Contact contact) {
        WsContact wsc = new WsContact();
        wsc.setDateOfBirth(contact.getBirthDate());
        wsc.setFirstName(contact.getFirstName());
        wsc.setLastName(contact.getLastName());
        wsc.setGroupname(contact.getOffice());
        wsc.setJobTitle(contact.getPosition());
        if (contact.getSalutation() != null && contact.getSalutation().getName() != null) {
            wsc.setSalutation(contact.getSalutation().getName());
        }
        if (contact.getTitle() != null && contact.getTitle().getName() != null) {
            wsc.setTitle(contact.getTitle().getName());
        }
        if (contact.getAddress() != null) {
            wsc.setCity(contact.getAddress().getCity());
            //wsc.setCountry(getOption('countryNames', contact.address.country))
            wsc.setStreet(contact.getAddress().getStreet());
            //wsc.setFederalState(getOption('federalStates', contact.address.federalStateId))
            wsc.setStreetAddon(contact.getAddress().getStreetAddon());
            wsc.setZipcode(contact.getAddress().getZipcode());
        }
        if (contact.isBusiness()) {
            wsc = addBusinessCommunication(wsc, contact);
        } else {
            wsc = addPrivateCommunication(wsc, contact);
        }
        wsc.setStatus(DEFAULT_STATUS);
        return wsc;
    }

    private WsContact addBusinessCommunication(WsContact wsc, Contact contact) {
        // email address setting
        List<EmailAddress> otherMailAddresses = new ArrayList<EmailAddress>();
        for (int i = 0, j = contact.getEmails().size(); i < j; i++) {
            EmailAddress mail = contact.getEmails().get(i);
            if (mail.isBusiness() && mail.isPrimary()) {
                wsc.setBusinessEmail(mail.getEmail());
            } else if (mail.isBusiness()) {
                otherMailAddresses.add(mail);
            }
        }
        if (wsc.getBusinessEmail() == null && !otherMailAddresses.isEmpty()) {

            EmailAddress latestMailAddress = null;
            for (int i = 0, j = otherMailAddresses.size(); i < j; i++) {
                EmailAddress mail = otherMailAddresses.get(i);
                if (latestMailAddress == null) {
                    latestMailAddress = mail;
                    
                } else if (isLatestDate(mail, latestMailAddress)) {
                    latestMailAddress = mail;
                }
            }
            if (latestMailAddress != null) {
                wsc.setBusinessEmail(latestMailAddress.getEmail());
            }
        }

        // phone number setting

        List<Phone> otherPhoneNumbers = new ArrayList<Phone>();
        for (int i = 0, j = contact.getPhoneNumbers().size(); i < j; i++) {
            Phone next = contact.getPhoneNumbers().get(i);
            if (PhoneType.PHONE.equals(next.getDevice())) {
                if (next.isBusiness() && next.isPrimary()) {
                    wsc.setBusinessPhone(next.getFormattedNumber());
                } else if (next.isBusiness()) {
                    otherPhoneNumbers.add(next);
                }
            }
        }
        if (wsc.getBusinessPhone() == null && !otherPhoneNumbers.isEmpty()) {

            Phone latestPhone = null;
            for (int i = 0, j = otherPhoneNumbers.size(); i < j; i++) {
                Phone next = otherPhoneNumbers.get(i);
                if (latestPhone == null) {
                    latestPhone = next;
                    
                } else if (isLatestDate(next, latestPhone)) {
                    latestPhone = next;
                }
            }
            if (latestPhone != null) {
                wsc.setBusinessPhone(latestPhone.getFormattedNumber());
            }
        }

        // fax number setting

        List<Phone> otherFaxNumbers = new ArrayList<Phone>();
        for (int i = 0, j = contact.getFaxNumbers().size(); i < j; i++) {
            Phone next = contact.getFaxNumbers().get(i);
            if (PhoneType.FAX.equals(next.getDevice())) {
                if (next.isBusiness() && next.isPrimary()) {
                    wsc.setBusinessFax(next.getFormattedNumber());
                } else if (next.isBusiness()) {
                    otherFaxNumbers.add(next);
                }
            }
        }
        if (wsc.getBusinessFax() == null && !otherFaxNumbers.isEmpty()) {

            Phone latestFax = null;
            for (int i = 0, j = otherFaxNumbers.size(); i < j; i++) {
                Phone next = otherFaxNumbers.get(i);
                if (latestFax == null) {
                    latestFax = next;
                    
                } else if (isLatestDate(next, latestFax)) {
                    latestFax = next;
                }
            }
            if (latestFax != null) {
                wsc.setBusinessFax(latestFax.getFormattedNumber());
            }
        }

        // mobile number setting

        List<Phone> otherMobileNumbers = new ArrayList<Phone>();
        for (int i = 0, j = contact.getMobiles().size(); i < j; i++) {
            Phone next = contact.getMobiles().get(i);
            if (PhoneType.MOBILE.equals(next.getDevice())) {
                if (next.isBusiness() && next.isPrimary()) {
                    wsc.setBusinessMobile(next.getFormattedNumber());
                } else if (next.isBusiness()) {
                    otherMobileNumbers.add(next);
                }
            }
        }
        if (wsc.getBusinessMobile() == null && !otherMobileNumbers.isEmpty()) {

            Phone latestMobile = null;
            for (int i = 0, j = otherMobileNumbers.size(); i < j; i++) {
                Phone next = otherMobileNumbers.get(i);
                if (latestMobile == null) {
                    latestMobile = next;
                    
                } else if (isLatestDate(next, latestMobile)) {
                    latestMobile = next;
                }
            }
            if (latestMobile != null) {
                wsc.setBusinessMobile(latestMobile.getFormattedNumber());
            }
        }
        return wsc;
    }

    private WsContact addPrivateCommunication(WsContact wsc, Contact contact) {
        // email address setting
        List<EmailAddress> otherMailAddresses = new ArrayList<EmailAddress>();
        for (int i = 0, j = contact.getEmails().size(); i < j; i++) {
            EmailAddress mail = contact.getEmails().get(i);
            if (mail.isPrivate() && mail.isPrimary()) {
                wsc.setPrivateEmail(mail.getEmail());
            } else if (mail.isPrivate()) {
                otherMailAddresses.add(mail);
            }
        }
        if (wsc.getPrivateEmail() == null && !otherMailAddresses.isEmpty()) {

            EmailAddress latestMailAddress = null;
            for (int i = 0, j = otherMailAddresses.size(); i < j; i++) {
                EmailAddress mail = otherMailAddresses.get(i);
                if (latestMailAddress == null) {
                    latestMailAddress = mail;
                    
                } else if (isLatestDate(mail, latestMailAddress)) {
                    latestMailAddress = mail;
                }
            }
            if (latestMailAddress != null) {
                wsc.setPrivateEmail(latestMailAddress.getEmail());
            }
        }

        // phone number setting

        List<Phone> otherPhoneNumbers = new ArrayList<Phone>();
        for (int i = 0, j = contact.getPhoneNumbers().size(); i < j; i++) {
            Phone next = contact.getPhoneNumbers().get(i);
            if (PhoneType.PHONE.equals(next.getDevice())) {
                if (next.isPrivate() && next.isPrimary()) {
                    wsc.setPrivatePhone(next.getFormattedNumber());
                } else if (next.isPrivate()) {
                    otherPhoneNumbers.add(next);
                }
            }
        }
        if (wsc.getPrivatePhone() == null && !otherPhoneNumbers.isEmpty()) {

            Phone latestPhone = null;
            for (int i = 0, j = otherPhoneNumbers.size(); i < j; i++) {
                Phone next = otherPhoneNumbers.get(i);
                if (latestPhone == null) {
                    latestPhone = next;
                    
                } else if (isLatestDate(next, latestPhone)) {
                    latestPhone = next;
                }
            }
            if (latestPhone != null) {
                wsc.setPrivatePhone(latestPhone.getFormattedNumber());
            }
        }

        // fax number setting

        List<Phone> otherFaxNumbers = new ArrayList<Phone>();
        for (int i = 0, j = contact.getFaxNumbers().size(); i < j; i++) {
            Phone next = contact.getFaxNumbers().get(i);
            if (PhoneType.FAX.equals(next.getDevice())) {
                if (next.isPrivate() && next.isPrimary()) {
                    wsc.setPrivateFax(next.getFormattedNumber());
                } else if (next.isPrivate()) {
                    otherFaxNumbers.add(next);
                }
            }
        }
        if (wsc.getPrivateFax() == null && !otherFaxNumbers.isEmpty()) {
            
            Phone latestFax = null;
            for (int i = 0, j = otherFaxNumbers.size(); i < j; i++) {
                Phone next = otherFaxNumbers.get(i);
                if (latestFax == null) {
                    latestFax = next;
                    
                } else if (isLatestDate(next, latestFax)) {
                    latestFax = next;
                }
            }
            if (latestFax != null) {
                wsc.setPrivateFax(latestFax.getFormattedNumber());
            }
        }

        // mobile number setting

        List<Phone> otherMobileNumbers = new ArrayList<Phone>();
        for (int i = 0, j = contact.getMobiles().size(); i < j; i++) {
            Phone next = contact.getMobiles().get(i);
            if (PhoneType.MOBILE.equals(next.getDevice())) {
                if (next.isPrivate() && next.isPrimary()) {
                    wsc.setPrivateMobile(next.getFormattedNumber());
                } else if (next.isPrivate()) {
                    otherMobileNumbers.add(next);
                }
            }
        }
        if (wsc.getPrivateMobile() == null && !otherMobileNumbers.isEmpty()) {

            Phone latestMobile = null;
            for (int i = 0, j = otherMobileNumbers.size(); i < j; i++) {
                Phone next = otherMobileNumbers.get(i);
                if (latestMobile == null) {
                    latestMobile = next;
                    
                } else if (isLatestDate(next, latestMobile)) {
                    latestMobile = next;
                }
            }
            if (latestMobile != null) {
                wsc.setPrivateMobile(latestMobile.getFormattedNumber());
            }
        }
        return wsc;
    }

    private boolean isLatestDate(Entity a, Entity b) {
        Date da = null;
        Date db = null;
        if (a.getChanged() != null) {
            da = a.getChanged();
        } else {
            da = a.getCreated();
        }
        if (b.getChanged() != null) {
            db = b.getChanged();
        } else {
            db = b.getCreated();
        }
        if (da == null) {
            return false;
        }
        if (db == null) {
            return true;
        }
        return da.after(db);
    }
}
