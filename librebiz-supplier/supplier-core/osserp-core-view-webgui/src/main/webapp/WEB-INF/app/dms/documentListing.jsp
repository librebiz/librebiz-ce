<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<o:resource/>
<o:adminContext/>
<v:view viewName="documentListingView"/>
<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title"><o:displayTitle/></tiles:put>
    <tiles:put name="styles" type="string">
        <style type="text/css">
        .docListSize { text-align: right; }
        </style>
    </tiles:put>
    <tiles:put name="headline">
        <fmt:message key="${view.headerName}"/>
        <c:if test="${!empty view.documentType}"> - <o:out value="${view.documentType.name}"/></c:if>
        - <fmt:message key="count"/> <o:out value="${view.documentCount}"/>
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation/>
    </tiles:put>
    <tiles:put name="content" type="string">
        <div class="content-area" id="documentListingContent">
            <div class="row">
                <c:import url="_documentListing.jsp"/>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
