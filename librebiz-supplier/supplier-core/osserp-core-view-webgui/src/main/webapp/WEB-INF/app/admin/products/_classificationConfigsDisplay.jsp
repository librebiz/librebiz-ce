<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="classificationConfigsView" />

<div class="col-md-12 panel-area">
    <div class="table-responsive table-responsive-default">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th><fmt:message key="productType" /></th>
                    <th><fmt:message key="productGroup" /></th>
                    <th><fmt:message key="category" /></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="obj" items="${view.productClassificationConfigs}">
                    <tr>
                        <td>
                            <v:link url="${view.baseLink}/select?id=${obj.type.id}">
                                <o:out value="${obj.type.name}" />
                            </v:link>
                        </td>
                        <td>
                            <c:if test="${!empty obj.group.name}">
                                <v:link url="${view.baseLink}/select?id=${obj.type.id}&group=${obj.group.id}">
                                    <o:out value="${obj.group.name}" />
                                </v:link>
                            </c:if>
                        </td>
                        <td>
                            <c:if test="${!empty obj.category.name}">
                                <v:link url="${view.baseLink}/select?id=${obj.type.id}&group=${obj.group.id}&category=${obj.category.id}">
                                    <o:out value="${obj.category.name}" />
                                </v:link>
                            </c:if>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
</div>
