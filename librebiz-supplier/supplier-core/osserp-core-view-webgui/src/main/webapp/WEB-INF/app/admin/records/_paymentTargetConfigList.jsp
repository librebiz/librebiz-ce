<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-12 panel-area">
    <div class="table-responsive table-responsive-default">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th><fmt:message key="label"/></th>
                    <th class="right"><fmt:message key="target"/></th>
                    <th><fmt:message key="paid"/></th>
                    <th><fmt:message key="discount"/></th>
                    <th class="right"><fmt:message key="targetOne"/></th>
                    <th class="right">&percnt;</th>
                    <th class="right"><fmt:message key="targetTwo"/></th>
                    <th class="right">&percnt;</th>
                </tr>
            </thead>
            <tbody>
                <c:choose>
                    <c:when test="${empty view.list}">
                        <tr><td colspan="8"><fmt:message key="noDataAvailable"/></td></tr>
                    </c:when>
                    <c:otherwise>
                        <c:forEach var="ptarget" items="${view.list}">
                            <tr>
                                <td><v:link url="${view.baseLink}/select?id=${ptarget.id}"><o:out value="${ptarget.name}"/></v:link></td>
                                <td class="right"><o:out value="${ptarget.paymentTarget}"/></td>
                                <td><c:choose><c:when test="${ptarget.paid}"><fmt:message key="yes"/></c:when><c:otherwise><fmt:message key="no"/></c:otherwise></c:choose></td>
                                <td><c:choose><c:when test="${ptarget.withDiscount}"><fmt:message key="yes"/></c:when><c:otherwise><fmt:message key="no"/></c:otherwise></c:choose></td>
                                <td class="right"><o:out value="${ptarget.discount1Target}"/></td>
                                <td class="right"><o:number value="${ptarget.discount1}" format="percent"/></td>
                                <td class="right"><o:out value="${ptarget.discount2Target}"/></td>
                                <td class="right"><o:number value="${ptarget.discount2}" format="percent"/></td>
                            </tr>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
            </tbody>
        </table>
    </div>
</div>
