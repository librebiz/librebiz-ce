/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 26, 2010 2:12:45 PM 
 * 
 */
package com.osserp.common.web.tags;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractActionIconTag extends AbstractOutTag {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String method;

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    protected abstract String getDefaultIcon();

    /**
     * Override this to manipulate provided url (add custom context, params, etc.)
     * @return url
     */
    protected String createUrl() {
        return createUrl(url);
    }

    /**
     * Renders an img tag with icon
     * @return iconImg
     */
    protected String createIconImg() {
        StringBuilder buffer = new StringBuilder("<img");
        buffer.append(createIconSrc()).append("\"");
        buffer = addStyleClass(buffer);
        buffer = addStyle(buffer);
        buffer.append("/>");
        return buffer.toString();
    }

    /**
     * Renders a 'src' attribute with icon url
     * @return iconSrc
     */
    protected String createIconSrc() {
        StringBuilder buffer = new StringBuilder(" src=\"");
        if (name != null) {
            buffer.append(createImageUrl(name));
        } else {
            buffer.append(createImageUrl(getDefaultIcon()));
        }
        buffer.append("\"");
        return buffer.toString();
    }
}
