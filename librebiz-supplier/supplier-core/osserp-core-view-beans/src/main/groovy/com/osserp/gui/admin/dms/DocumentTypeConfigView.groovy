/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 12, 2016 
 * 
 */
package com.osserp.gui.admin.dms

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ActionException
import com.osserp.common.ClientException
import com.osserp.common.EntityRelation
import com.osserp.common.ErrorCode
import com.osserp.common.dms.DmsManager
import com.osserp.common.dms.DocumentType
import com.osserp.common.dms.DocumentTypeManager

import com.osserp.core.dms.CoreDocumentTypeManager

import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class DocumentTypeConfigView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(DocumentTypeConfigView.class.getName())

    List<DocumentType> existingCredentials = []
    
    String credentialContext = 'documentTypes'
    Integer documentCount = 0

    DocumentTypeConfigView() {
        enableAutoreload()
        headerName  = 'documentTypeConfigView'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            reload()
            Long id = form.getLong('id')
            if (id) {
                bean = list.find { it.id == id }
            }
        }
    }
    
    @Override
    void reload() {
        list = documentTypeManager.findConfigurable()
        documentCount = list.size()
        if (bean) {
            bean = list.find { it.id == bean.id } 
            documentCount = dmsManager.countByType(documentType)
            logger.debug("reload: done [type=${bean?.id}, documentCount=${documentCount}]")
        } else {
            documentCount = 0
            logger.debug("reload: done [type=not selected]")
        }
    }
    
    @Override
    void select() {
        super.select()
        reload()
        if (bean && !credentialsAvailable) {
            existingCredentials = typesWithExistingCredentials
        }
    }

    void copyCredentials() {
        if (bean) {
            Long id = form.getLong('id')
            DocumentType source = existingCredentials.find {
                it.id == id
            }
            if (source) {
                securityService.copyText(credentialContext, source, documentType)
            }
            reload()
        }
    }

    void changeFormat() {
        if (bean) {
            documentTypeManager.changePersistenceFormat(
                documentType, getFormat())
            reload()
        }
    }
    
    void changeDownloadName() {
        Long id = form.getLong('id')
        DocumentType selected = list.find { it.id == id }
        if (selected) {
            documentTypeManager.changeDownloadName(selected)
            reload()
        }
    }
    
    final String getCredentials() {
        if (root && bean && credentialContext) {
            return securityService.readText(bean, credentialContext)
        }
        logger.debug("getCredentials: user not root or other requirement missing")
        return '**...'
    }
    
    final boolean isCredentialsDisplayAvailable() {
        (root && bean && credentialContext && credentialsAvailable)
    }

    final boolean isCredentialsAvailable() {
        if (!bean || !credentialContext) {
            return false
        }
        return (securityService.readText(bean, credentialContext) != null)
    }
    
    protected int getFormat() {
        (documentType.persistenceFormat == DocumentType.FORMAT_PLAIN) ? 
            DocumentType.FORMAT_CRYPT : DocumentType.FORMAT_PLAIN
    }
    
    protected List<DocumentType> getTypesWithExistingCredentials() {
        List<DocumentType> other = list.each { DocumentType type ->
            type.id != documentType.id  }
        securityService.textAvailable(credentialContext, other)
    }
    
    protected DocumentType getDocumentType() {
        if (!(bean instanceof DocumentType)) {
            throw new ActionException()
        }
        return bean
    }
    
    protected DocumentTypeManager getDocumentTypeManager() {
        getService(CoreDocumentTypeManager.class.getName())
    }
    
    protected DmsManager getDmsManager() {
        getService(documentType.serviceName)
    }
}
