<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="searchView" scope="session" value="${sessionScope.calculationTemplateProductSearchView}"/>
<o:viewset view="${searchView}" name="searchUrl" value="/calculationTemplateProductSearch.do"/>
<o:viewset view="${searchView}" name="searchResultPage" value="/pages/calc/calculation_template_product_search_result_ajax.jsp"/>
<o:viewset view="${searchView}" name="productExitTarget" value="calculationTemplateProductSearchReload"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title"><o:displayTitle/></tiles:put>
    <tiles:put name="onload" type="string">onload="initSearch(); setSearchFocus();"</tiles:put>
    <tiles:put name="styles" type="string">
        <style type="text/css">
            <c:import url="/pages/calc/calculation_product_search_result.css"/>
        </style>
    </tiles:put>
    <tiles:put name="javascript" type="string">
        <script type="text/javascript">
            <c:import url="/pages/products/product_search.js"/>
        </script>
    </tiles:put>
    <tiles:put name="headline"><fmt:message key="productSearch"/></tiles:put>
    <tiles:put name="headline_right">
        <ul>
            <li><a href="<c:url value="/calculationTemplateProductSearch.do?method=exit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a></li>
            <li><a href="<c:url value="/calculationTemplateProductSearch.do?method=toggleLazyIgnoring"/>" title="<fmt:message key="switchFlowless"/>"><o:img name="reloadIcon"/></a></li>
            <li><v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link></li>
        </ul>
    </tiles:put>
    <tiles:put name="content" type="string">
        <div class="row">
            <div class="col-lg-12 panel-area">
                <div class="panel-body">
                    <c:import url="/pages/products/_product_search_form.jsp"/>
                </div>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
