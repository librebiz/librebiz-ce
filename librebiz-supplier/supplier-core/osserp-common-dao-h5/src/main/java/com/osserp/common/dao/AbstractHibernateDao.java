/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 24-Dec-2006 09:53:37 
 * 
 */
package com.osserp.common.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.transaction.annotation.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.osserp.common.BackendException;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@Transactional
public abstract class AbstractHibernateDao extends AbstractDao {
    private static Logger log = LoggerFactory.getLogger(AbstractHibernateDao.class.getName());

    private SessionFactory sessionFactory;

    public AbstractHibernateDao() {
        super();
    }

    public AbstractHibernateDao(SessionFactory sessionFactory) {
        super();
        this.sessionFactory = sessionFactory;
    }

    protected Session getCurrentSession() {
        try {
            return sessionFactory.getCurrentSession();
        } catch (Exception e) {
            log.error("getCurrentSession: failed [message=" + e.getMessage() + "]", e);
            throw new BackendException(e);
        }
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
