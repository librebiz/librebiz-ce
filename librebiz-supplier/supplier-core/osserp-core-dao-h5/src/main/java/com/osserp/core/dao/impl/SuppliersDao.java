/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 8, 2006 1:00:43 AM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;
import org.hibernate.SessionFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.common.OptionsCache;
import com.osserp.common.beans.OptionImpl;
import com.osserp.common.dao.Tables;
import com.osserp.common.util.CollectionUtil;

import com.osserp.core.Comparators;
import com.osserp.core.Options;
import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.contacts.Contact;
import com.osserp.core.dao.Contacts;
import com.osserp.core.dao.Emails;
import com.osserp.core.dao.PaymentConditions;
import com.osserp.core.dao.Suppliers;
import com.osserp.core.dao.SystemCompanies;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.finance.PaymentCondition;
import com.osserp.core.model.contacts.ContactImpl;
import com.osserp.core.model.contacts.groups.SupplierImpl;
import com.osserp.core.model.contacts.groups.SupplierTypeImpl;
import com.osserp.core.products.ProductGroup;
import com.osserp.core.suppliers.Supplier;
import com.osserp.core.suppliers.SupplierType;
import com.osserp.core.system.SystemCompany;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SuppliersDao extends ClassifiedContactsDao implements Suppliers {
    private static Logger log = LoggerFactory.getLogger(SuppliersDao.class.getName());
    private static final String LIST =
            "suppliers.id,suppliers.contact_id," +
                    "contacts.salutation,contacts.firstname,contacts.lastname," +
                    "contacts.street,contacts.zipcode,contacts.city";
    private static final Long SALARY_ACCOUNT_DEFAULT = 7L;
    private String suppliersSequence = null;
    private String suppliersTable = null;
    private String supplierProductGroupsTable = null;
    private String deliveryNotesTable = null;
    private String deliveryNoteItemsTable = null;
    private PaymentConditions conditions = null;
    private SystemCompanies companies = null;

    public SuppliersDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            Contacts contacts,
            Emails emails,
            OptionsCache namesCache,
            PaymentConditions conditions,
            SystemCompanies companies) {
        super(jdbcTemplate, tables, sessionFactory, contacts, emails, namesCache);
        this.suppliersSequence = getTable(TableKeys.SUPPLIER_SEQUENCE);
        this.suppliersTable = getTable(TableKeys.SUPPLIERS);
        this.supplierProductGroupsTable = getTable(TableKeys.SUPPLIER_PRODUCT_GROUPS);
        this.deliveryNotesTable = getTable(TableKeys.PURCHASE_DELIVERY_NOTES);
        this.deliveryNoteItemsTable = getTable(TableKeys.PURCHASE_DELIVERY_NOTE_ITEMS);
        this.conditions = conditions;
        this.companies = companies;
    }
    
    /**
     * Loads supplier object from persistent storage
     * @param id
     * @return supplier
     */
    protected Supplier load(Long id) {
        SupplierImpl obj = (SupplierImpl) getCurrentSession().load(SupplierImpl.class, id);
        return obj;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.impl.AbstractContactAwareDao#getEntityClass()
     */
    @Override
    protected Class<SupplierImpl> getEntityClass() {
        return SupplierImpl.class;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.impl.AbstractContactAwareDao#getEntityTable()
     */
    @Override
    protected String getEntityTable() {
        return suppliersTable;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.ContactRelations#find(java.lang.Long)
     */
    public ClassifiedContact find(Long id) {
        try {
            return load(id);
        } catch (Throwable t) {
            return null;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.ContactRelations#create(java.lang.Long, com.osserp.core.contacts.Contact, java.lang.Long)
     */
    public ClassifiedContact create(Long createdBy, Contact contact, Long customId) {
        if (!isEmpty(customId) && exists(customId)) {
            throw new BackendException(ErrorCode.CONTACT_EXISTS);
        }
        SupplierType defaultType = getDefaultSupplierType();
        SupplierImpl obj = new SupplierImpl(
                !isEmpty(customId) ? customId : getNextSupplierId(),
                defaultType,
                createdBy,
                contact,
                getDefaultCondition());
        if (contact.isPublicInstitution()) {
            obj.setDirectInvoiceBookingEnabled(true);
        }
        if (obj.getRc() instanceof ContactImpl) {
            ContactImpl ci = (ContactImpl) obj.getRc();
            ci.setSupplier(true);
            ci.setOther(false);
        }
        save(obj);
        return obj;
    }

    public void createSalaryAccount(Long createdBy, Long supplierId, Contact employee) {
        SupplierType type = getSalaryType();
        SupplierImpl obj = new SupplierImpl(
                supplierId,
                type,
                createdBy,
                employee,
                getDefaultCondition());
        obj.setSimpleBilling(true);
        save(obj);
    }

    public boolean isSalaryAccountExisting(Long supplierId) {
        StringBuilder sql = new StringBuilder();
        sql
                .append("SELECT count(*) FROM ")
                .append(suppliersTable)
                .append(" WHERE id = ")
                .append(supplierId);
        return (jdbcTemplate.queryForObject(sql.toString(), Integer.class) > 0);
    }

    public Supplier addProductGroup(Supplier supplier, ProductGroup productGroup) {
        boolean added = false;
        for (int i = 0, j = supplier.getProductGroups().size(); i < j; i++) {
            ProductGroup next = supplier.getProductGroups().get(i);
            if (next.getId().equals(productGroup.getId())) {
                added = true;  
                break;
            }
        }
        if (!added) {
            supplier.getProductGroups().add(productGroup);
            save(supplier);
            if (log.isDebugEnabled()) {
                log.debug("addProductGroup() done [supplier=" + supplier.getId()
                        + ", group=" + productGroup.getId() + "]");
            }
        }
        return supplier;
    }

    @SuppressWarnings("unchecked")
    public List<Supplier> findByProductGroup(Long groupId) {
        List<Supplier> result = new ArrayList<Supplier>();
        StringBuilder sql = new StringBuilder();
        sql
                .append("SELECT id FROM ")
                .append(suppliersTable)
                .append(" WHERE id IN (SELECT supplier_id FROM ")
                .append(supplierProductGroupsTable)
                .append(" WHERE product_group_id = ?)");
        final Object[] params = { groupId };
        final int[] types = { Types.BIGINT };
        List<Long> ids = (List) jdbcTemplate.query(sql.toString(), params, types, getLongRowMapper());
        for (int i = 0, j = ids.size(); i < j; i++) {
            result.add(load(ids.get(i)));
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    public List<Supplier> findByDeliveries(Long productId) {
        List<Supplier> result = new ArrayList<Supplier>();
        StringBuilder sql = new StringBuilder();
        sql
                .append("SELECT id FROM ")
                .append(suppliersTable)
                .append(" WHERE id IN (SELECT contact_id FROM ")
                .append(deliveryNotesTable)
                .append(" WHERE id IN (SELECT reference_id FROM ")
                .append(deliveryNoteItemsTable)
                .append(" WHERE product_id = ?))");
        final Object[] params = { productId };
        final int[] types = { Types.BIGINT };
        List<Long> ids = (List) jdbcTemplate.query(sql.toString(), params, types, getLongRowMapper());
        for (int i = 0, j = ids.size(); i < j; i++) {
            result.add(load(ids.get(i)));
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    public Supplier findByCompany(Long company) throws ClientException {
        Supplier result = null;
        try {
            SystemCompany companyObject = companies.getCompany(company);
            StringBuilder sql = new StringBuilder();
            sql
                    .append("SELECT id FROM ")
                    .append(suppliersTable)
                    .append(" WHERE contact_id = ?");
            final Object[] params = { companyObject.getContact().getContactId() };
            final int[] types = { Types.BIGINT };
            List<Long> ids = (List) jdbcTemplate.query(sql.toString(), params, types, getLongRowMapper());
            result = (ids.isEmpty() ? null : load(ids.get(0)));
        } catch (Throwable t) {
            log.warn("findByCompany() ignoring exception [message=" + t.getMessage() + "]", t);
        }
        if (result == null) {
            throw new ClientException(ErrorCode.SUPPLIER_UNKNOWN);
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    public List<Supplier> findByDirectInvoiceBooking() {
        List<Supplier> result = new ArrayList<Supplier>();
        StringBuilder sql = new StringBuilder();
        sql
                .append("SELECT id FROM ")
                .append(suppliersTable)
                .append(" WHERE direct_pi_booking = true");
        List<Long> ids = (List) jdbcTemplate.query(sql.toString(), getLongRowMapper());
        for (int i = 0, j = ids.size(); i < j; i++) {
            result.add(load(ids.get(i)));
        }
        return CollectionUtil.sort(result, Comparators.createSupplierNameComparator(false));
    }

    public List<Supplier> findBySimpleBilling() {
        List<Supplier> result = new ArrayList<Supplier>();
        StringBuilder sql = new StringBuilder();
        sql
                .append("from ")
                .append(SupplierImpl.class.getName())
                .append(" o where o.simpleBilling = true");
        
        result = getCurrentSession().createQuery(sql.toString()).list();
        return CollectionUtil.sort(result, Comparators.createSupplierNameComparator(false));
    }

    @SuppressWarnings("unchecked")
    public List<ProductGroup> findProductGroups(Long supplierId) {
        StringBuilder query = new StringBuilder(64);
        query
                .append("SELECT product_group_id FROM ")
                .append(supplierProductGroupsTable)
                .append(" WHERE supplier_id = ?");
        List<Long> list = (List) jdbcTemplate.query(query.toString(), new Object[] { supplierId }, new int[] { Types.BIGINT }, getLongRowMapper());
        List<ProductGroup> result = new ArrayList<ProductGroup>();
        Map<Long, ProductGroup> groups = new java.util.HashMap(namesCache.getMap(Options.PRODUCT_GROUPS));
        for (int i = 0, j = list.size(); i < j; i++) {
            result.add(groups.get(list.get(i)));
        }
        return result;
    }

    public List<Option> getShippingCompanies() {
        return fetchShipping("v_shipping_companies");
    }

    public List<Option> getShippingCompanyKeys() {
        List<Option> list = fetchShipping("v_shipping_company_keys");
        for (int i = 0, j = list.size(); i < j; i++) {
            Option next = list.get(i);
            if (isNotSet(next.getName())) {
                if (isSet(next.getDescription())) {
                    if (next.getDescription().length() <= 3) {
                        next.setName(next.getDescription());
                    } else {
                        next.setName(next.getDescription().substring(0, 3));
                    }
                }
            }
        }
        return list;
    }

    private List<Option> fetchShipping(String dbviewName) {
        return (List) jdbcTemplate.query(
                "SELECT id, contact_id, name, description FROM " + dbviewName,
                new RowMapperResultSetExtractor(new ShippingRowMapper()));
    }

    private class ShippingRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int index) throws SQLException {
            Option result = new OptionImpl(
                    Long.valueOf(rs.getLong(1)),
                    Long.valueOf(rs.getLong(2)),
                    rs.getString(3));
            result.setDescription(rs.getString(4));
            return result;
        }
    }

    public List<SupplierType> getSupplierTypes() {
        String hql = "from " + SupplierTypeImpl.class.getName() + " o order by o.id";
        return getCurrentSession().createQuery(hql).list();
    }

    public SupplierType getSalaryType() {
        SupplierType defaultType = null;
        List<SupplierType> list = getSupplierTypes();
        for (int i = 0, j = list.size(); i < j; i++) {
            SupplierType next = list.get(i);
            if (next.isSalaryAccount()) {
                return next;
            } else if (SALARY_ACCOUNT_DEFAULT.equals(next.getId())) {
                defaultType = next;
            }
        }
        if (defaultType == null) {
            // neither default type nor configured type found
            throw new BackendException("missing supplier type with salary"
                    + " enabled or default type " + SALARY_ACCOUNT_DEFAULT);
        }
        return null;
    }

    public SupplierType getDefaultSupplierType() {
        String hql = "from " + SupplierTypeImpl.class.getName() + " o where o.defaultType = true";
        List<SupplierType> result = getCurrentSession().createQuery(hql).list();
        if (result.isEmpty()) {
            // take the first activated as default if no default defined 
            result = getSupplierTypes();
        }
        return result.isEmpty() ? null : result.get(0);
    }

    /**
     * Provides the basic statement required for queries
     * @return query
     */
    @Override
    protected StringBuilder getQuery() {
        StringBuilder query = new StringBuilder(64);
        query
                .append("SELECT ")
                .append(LIST)
                .append(" FROM ")
                .append(suppliersTable)
                .append(",")
                .append(contactsTable)
                .append(" WHERE suppliers.contact_id = contacts.contact_id");
        return query;
    }

    /**
     * Provides the statement required for ordered queries
     * @return query with order by lastname clause
     */
    @Override
    protected String getOrderedQuery() {
        StringBuilder query = getQuery();
        query.append(" ORDER BY lastname");
        return query.toString();
    }

    private PaymentCondition getDefaultCondition() {
        return conditions.getPaymentConditionDefault(false);
    }

    private Long getNextSupplierId() {
        return getNextLong(suppliersSequence);
    }
}
