/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 16, 2016 
 * 
 */
package com.osserp.common;

import java.util.List;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface DetailsManager {

    /**
     * Provides existing product properties
     * @param ignorable properties assigned to this object will be ignored (optional)
     * @return list of properties with logical name and localized i18n label as value. The list is sorted by value.
     */
    List<Property> findExistingProperties(Details ignorable);

    /**
     * Provides all values with provided value name
     * @param label
     * @param ignorable
     * @return list of previously created values or empty list if none exist
     */
    List<Property> findExistingValues(String label, List<Property> ignorable);

    /**
     * Adds a new property to an product
     * @param user
     * @param details
     * @param language
     * @param label
     * @param value
     * @return details with property added
     * @throws ClientException if any required value is missing
     */
    Details addProperty(User user, Details details, String language, String label, String value) throws ClientException;

    /**
     * Removes a property from an product
     * @param user
     * @param details
     * @param property
     * @return details with property removed
     */
    Details removeProperty(User user, Details details, Property property);

    /**
     * Updates the value of an assigned property
     * @param user
     * @param details
     * @param property with value to update
     * @param value
     * @return details with updated property
     */
    Details updateProperty(User user, Details details, Property property, String value);

    /**
     * Updates the label of a property
     * @param user
     * @param property
     * @param language
     * @param value
     */
    void updatePropertyLabel(User user, Property property, String language, String value);

}
