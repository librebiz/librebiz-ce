<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-12 panel-area panel-area-default">
    <div class="panel-body">
        <div class="table-responsive table-responsive-default">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th><fmt:message key="templateName" /></th>
                        <th><fmt:message key="mailSubject" /></th>
                        <th><fmt:message key="originator" /></th>
                        <th><fmt:message key="archiveAddress" /></th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="obj" items="${view.list}" varStatus="s">
                        <c:if test="${!obj.disabled}">
                            <tr>
                                <td>
                                    <v:link url="/admin/mail/mailTemplateConfig/select?id=${obj.id}">
                                        <o:out value="${obj.name}" />
                                    </v:link>
                                </td>
                                <td><o:out value="${obj.subject}" /></td>
                                <td>
                                    <c:choose>
                                        <c:when test="${obj.sendAsUser}">
                                            <fmt:message key="sendAsUserLabelShort" />
                                        </c:when>
                                        <c:when test="${empty obj.originator}">
                                            <span class="error">
                                                <fmt:message key="notDefined" />
                                            </span>
                                        </c:when>
                                        <c:otherwise>
                                            <o:out value="${obj.originator}" />
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td>
                                    <c:choose>
                                        <c:when test="${empty obj.recipientsBCC}">
                                            <fmt:message key="none" />
                                        </c:when>
                                        <c:otherwise>
                                            <o:out value="${obj.recipientsBCC}" />
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                            </tr>
                        </c:if>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>

