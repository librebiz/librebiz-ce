/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 29, 2008 7:35:51 AM 
 * 
 */
package com.osserp.core.finance;

import java.util.Date;
import java.util.List;

import org.jdom2.Document;

import com.osserp.common.ClientException;
import com.osserp.common.Option;
import com.osserp.core.employees.Employee;
import com.osserp.core.system.SystemCompany;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface StocktakingManager {

    /**
     * Provides all available stocktaking types
     * @return types
     */
    List<Option> getTypes();

    /**
     * Provides a list of all closed stocktakings
     * @param company
     * @return closed stocktakings
     */
    List<Option> getClosed(Long company);

    /**
     * Finds a stocktaking by id
     * @return stocktaking
     */
    Stocktaking find(Long id);

    /**
     * Provides current open stocktaking
     * @param company
     * @return open or null if none exist
     */
    Stocktaking getOpen(Long company);

    /**
     * Validates new type and name to create against existing stocktakings. This method should be invoked before calling create
     * @param availableClosed
     * @param currentOpen
     * @param typeToValidate
     * @param nameToValidate
     * @throws ClientException if any required value missing or name already exists
     */
    void validate(
            List<Option> availableClosed,
            Stocktaking currentOpen,
            Option typeToValidate,
            String nameToValidate) throws ClientException;

    /**
     * Creates a new stocktaking
     * @param company
     * @param type
     * @param name
     * @param recordDate
     * @param createdBy
     * @return new created stocktaking
     * @throws ClientException if any previous not closed stocktaking exists
     */
    Stocktaking create(Long company, Option type, String name, Date recordDate, Long createdBy) throws ClientException;

    /**
     * Deletes stocktaking object if not already finished
     * @param st
     */
    void delete(Stocktaking st);

    /**
     * Refreshs all items of a stocktaking
     * @param st
     */
    void refreshItems(Stocktaking st);

    /**
     * Initializes all products counters with expected count
     * @param st
     */
    void setCountedAsExpected(Stocktaking st);

    /**
     * Resets all counters to zero
     * @param st
     */
    void resetCounters(Stocktaking st);

    /**
     * Sets a stocktakings status as printed
     * @param st
     */
    void markListPrinted(Stocktaking st);

    /**
     * Closes a stocktaking
     * @param employee
     * @param st
     */
    void close(Employee employee, Stocktaking st);

    /**
     * Persists a stocktaking
     * @param st
     */
    void save(Stocktaking st);

    /**
     * Creates an xml document for printing
     * @param user
     * @param stocktaking
     * @param company
     * @param stylesheetName
     * @return document
     * @throws ClientException if any requirements not met
     */
    Document createDocument(
            DomainUser user, 
            Stocktaking stocktaking,
            SystemCompany company,
            String stylesheetName) throws ClientException;

}
