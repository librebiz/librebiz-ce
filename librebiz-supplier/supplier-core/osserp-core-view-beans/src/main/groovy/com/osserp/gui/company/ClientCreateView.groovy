/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.gui.company

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ErrorCode

import com.osserp.core.contacts.Contact
import com.osserp.core.contacts.ContactSearch
import com.osserp.core.system.SystemCompanyManager

import com.osserp.groovy.web.ViewContextException

import com.osserp.gui.CoreView

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public class ClientCreateView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(ClientCreateView.class.getName())

    ClientCreateView() {
        dependencies = ['contactView', 'contactSearchView']
        headerName = 'systemCompanyCreation'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (!env.selectedContact) {
            logger.debug('initRequest: no contact selected...')
            def selectedContact = env.contactSearchView?.bean ?: env.contactView?.bean
            if (!selectedContact && form.params.id) {
                logger.debug('initRequest: found id param...')
                selectedContact = contactSearch.find(form.getLong('id'))
            }
            if (selectedContact) {
                logger.debug("initRequest: contact selected [contactId=${selectedContact.contactId}]")
                if (!(selectedContact instanceof Contact)) {
                    selectedContact = contactSearch.find(selectedContact.contactId)
                }
                env.selectedContact = selectedContact
                if (selectedContact.client) {
                    logger.debug("initRequest: error: selected contact is already client [contactId=${selectedContact.contactId}]")
                    throw new ViewContextException(ErrorCode.CONTACT_ALREADY_CLIENT, url('/company/clientCompany/forward'))
                }
            }
        }
        if (!bean && env.selectedContact) {
            // initial call
            logger.debug('initRequest: initial call, bean not initialized...')
            bean = systemCompanyManager.createPoposal(env.selectedContact)
            logger.debug("initRequest: assigned proposal [id=${bean.id}, name=${bean.name}]")
        }
    }

    @Override
    void save() {
        if (!bean) {
            throw new ViewContextException(ErrorCode.INVALID_CONTEXT)
        }
        bean.id = form.getLong('number')
        bean.name = form.getString('name')
        bean = systemCompanyManager.create(bean)
        setSelectionTarget(createTarget("/contacts.do?method=load&id=${bean.contact.contactId}"))
    }

    private ContactSearch getContactSearch() {
        getService(ContactSearch.class.getName())
    }

    private SystemCompanyManager getSystemCompanyManager() {
        getService(SystemCompanyManager.class.getName())
    }
}
