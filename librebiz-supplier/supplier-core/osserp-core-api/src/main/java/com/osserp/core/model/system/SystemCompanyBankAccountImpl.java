/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 22-Jan-2008 
 * 
 */
package com.osserp.core.model.system;

import com.osserp.core.BankAccount;
import com.osserp.core.model.AbstractBankAccount;
import com.osserp.core.system.SystemCompany;

/**
 * 
 * @author cf <cf@osserp.com>
 * @author tn <tn@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public class SystemCompanyBankAccountImpl extends AbstractBankAccount {

    /**
     * Default constructor required to implement Serializable.
     */
    protected SystemCompanyBankAccountImpl() {
        super();
    }

    /**
     * Default constructor required to implement Serializable.
     */
    protected SystemCompanyBankAccountImpl(BankAccount o) {
        super(o);
    }

    /**
     * Default constructor creating new bank accounts
     * @param company
     * @param shortkey
     * @param name
     * @param nameAddon
     * @param bankAccountNumberIntl
     * @param bankIdentificationCodeIntl
     * @param useInDocumentFooter
     * @param taxAccount
     * @param created
     * @param createdBy
     */
    public SystemCompanyBankAccountImpl(
            SystemCompany company,
            String shortkey,
            String name,
            String nameAddon,
            String bankAccountNumberIntl,
            String bankIdentificationCodeIntl,
            boolean useInDocumentFooter,
            boolean taxAccount,
            Long createdBy) {
        super(company.getId(), shortkey, name, nameAddon, 
                bankAccountNumberIntl, bankIdentificationCodeIntl, 
                useInDocumentFooter, taxAccount, createdBy);
    }

    @Override
    public Object clone() {
        return new SystemCompanyBankAccountImpl(this);
    }
}
