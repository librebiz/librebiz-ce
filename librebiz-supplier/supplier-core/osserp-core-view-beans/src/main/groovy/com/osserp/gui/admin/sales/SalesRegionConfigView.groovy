/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.gui.admin.sales

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.groovy.web.MenuItem

import com.osserp.core.employees.Employee
import com.osserp.core.employees.EmployeeGroup
import com.osserp.core.employees.EmployeeSearch

import com.osserp.core.sales.SalesRegion
import com.osserp.core.sales.SalesRegionManager

import com.osserp.groovy.web.ViewContextException
import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class SalesRegionConfigView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(SalesRegionConfigView.class.getName())
    List<Employee> salesStaff = []


    SalesRegionConfigView() {
        super()
        providesCreateMode()
        providesEditMode()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (forwardRequest) {
            list = salesRegionManager.findAll()
        }
    }

    @Override
    void enableCreateMode() {
        super.enableCreateMode()
        salesStaff = employeeSearch.findByGroup(null, EmployeeGroup.SALES)
    }

    @Override
    void enableEditMode() {
        super.enableEditMode()
        salesStaff = employeeSearch.findByGroup(null, EmployeeGroup.SALES)
    }

    @Override
    void save() {
        String name = form.getString('name')
        Long salesId = form.getLong('salesId')
        Long salesExecutiveId = form.getLong('salesExecutiveId')
        Employee sales
        Employee salesExecutive
        if (salesExecutiveId) {
            salesExecutive = salesStaff.find { it.id == salesExecutiveId }
        }
        if (salesId) {
            sales = salesStaff.find { it.id == salesId }
        }
        if (createMode) {
            bean = salesRegionManager.create(domainEmployee, name, sales, salesExecutive)
            disableCreateMode()
        } else {
            bean = salesRegionManager.change(domainEmployee, bean, name, sales, salesExecutive)
            disableEditMode()
        }
        list = salesRegionManager.findAll()
    }

    boolean isZipcodeAddMode() {
        env.zipcodeAddMode
    }

    void disableZipcodeAddMode() {
        env.zipcodeAddMode = false
        createCustomNavigation()
    }

    void enableZipcodeAddMode() {
        env.zipcodeAddMode = true
        createCustomNavigation()
    }

    void addZipcode() {
        String zipcode = form.getString('zipcode')
        if (zipcode) {
            bean = salesRegionManager.addZipcode(domainEmployee, bean, zipcode)
            list = salesRegionManager.findAll()
        }
        env.zipcodeAddMode = false
        createCustomNavigation()
    }

    void removeZipcode() {
        String zipcode = form.getString('zipcode')
        if (zipcode) {
            bean = salesRegionManager.removeZipcode(domainEmployee, bean, zipcode)
            list = salesRegionManager.findAll()
        }
        env.zipcodeAddMode = false
        createCustomNavigation()
    }

    boolean isDisplayEol() {
        env.displayEol
    }

    void toggleDisplayEol() {
        if (displayEol) {
            env.displayEol = false
        } else {
            env.displayEol = true
        }
        createCustomNavigation()
    }

    void toggleEol() {
        Long id = form.getLong('id')
        if (id) {
            SalesRegion region = salesRegionManager.findRegion(id)
            if (region) {
                salesRegionManager.toggleEol(region)
                list = salesRegionManager.findAll()
            } else {
                logger.warn("toggleEol: backend did no longer find region [id=${id}]")
            }
        }
    }

    void delete() {
        Long id = form.getLong('id')
        if (id) {
            salesRegionManager.deleteRegion(id)
            list = salesRegionManager.findAll()
        }
    }

    @Override
    protected void createCustomNavigation() {
        nav.listNavigation = [exitLink, toggleDisplayEolLink, createLink, homeLink]
    }

    protected MenuItem getToggleDisplayEolLink() {
        if (displayEol) {
            return navigationLink("/${context.name}/toggleDisplayEol", 'nosmileIcon', 'displayActive')
        }
        return navigationLink("/${context.name}/toggleDisplayEol", 'smileIcon', 'displayResign')
    }

    protected SalesRegionManager getSalesRegionManager() {
        getService(SalesRegionManager.class.getName())
    }
}

