/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 13, 2006 7:45:16 PM 
 * 
 */
package com.osserp.common.dms.model;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.beans.OptionImpl;
import com.osserp.common.dms.DmsConfig;
import com.osserp.common.dms.DocumentType;
import com.osserp.common.util.StringUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class DocumentTypeImpl extends OptionImpl implements DocumentType {

    private static final String UNMANAGED = "DIRECT_ACCESS";
    
    private DmsConfig config;
    private String prefix = null;
    private String postfix = null;
    private String suffix = null;
    private boolean overwrite = true;
    private int persistenceMode = DocumentType.DATABASE_STORE;
    private int persistenceFormat = DocumentType.FORMAT_PLAIN;
    private String contextName = null;
    private String contextPath = null;
    private boolean noteRequired;
    private boolean suffixByFile;
    private Long count = null;
    private boolean supportingReferenceType;
    private boolean referenceSelection;
    private String referenceUrl = null;
    private String namingStyle = null;
    private String serviceName = null;
    private boolean locked;
    private boolean searchableEnabled;
    private boolean encryptionSupported;
    private boolean validityPeriodSupported;
    private boolean useFilenameForDownload;
    private String preventSuffix = null;
    private boolean createThumbmail;
    private int thumbmailWidth = 0;
    private int thumbmailHeight = 0;

    protected DocumentTypeImpl() {
        super();
    }

    /**
     * Creates a new documentType
     * @param config
     * @param id
     * @param name
     * @param prefix
     * @param suffix
     * @param overwrite
     * @param persistenceMode
     * @param postfix
     * @param contextName
     * @param contextPath
     * @param noteRequired
     * @param suffixByFile
     * @param referenceSelection
     * @param namingStyle
     * @param serviceName
     */
    public DocumentTypeImpl(
            DmsConfig config,
            Long id,
            String name,
            String prefix,
            String suffix,
            boolean overwrite,
            int persistenceMode,
            String postfix,
            String contextName,
            String contextPath,
            boolean noteRequired,
            boolean suffixByFile,
            boolean referenceSelection,
            String namingStyle,
            String serviceName) {

        super(id, name);
        this.config = config;
        this.prefix = prefix;
        this.suffix = suffix;
        this.overwrite = overwrite;
        this.postfix = postfix;
        this.persistenceMode = persistenceMode;
        this.contextName = contextName;
        this.contextPath = contextPath;
        this.noteRequired = noteRequired;
        this.suffixByFile = suffixByFile;
        this.referenceSelection = referenceSelection;
        this.namingStyle = namingStyle;
        this.serviceName = serviceName;
    }

    public DmsConfig getConfig() {
        return config;
    }

    public void setConfig(DmsConfig config) {
        this.config = config;
    }

    public final boolean isConfigurable() {
        return (!isTemplate() 
                && !isDatabaseStore() 
                && serviceName != null 
                && !UNMANAGED.equals(serviceName));
    }
    
    
    public boolean isSearchable() {
        if (!searchableEnabled) {
            return false;
        }
        return !isTemplate();
    }

    protected boolean isSearchableEnabled() {
        return searchableEnabled;
    }

    protected void setSearchableEnabled(boolean searchableEnabled) {
        this.searchableEnabled = searchableEnabled;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getPostfix() {
        return postfix;
    }

    public void setPostfix(String postfix) {
        this.postfix = postfix;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public boolean isOverwrite() {
        return overwrite;
    }

    public void setOverwrite(boolean overwrite) {
        this.overwrite = overwrite;
    }

    public boolean isValidityPeriodSupported() {
        return validityPeriodSupported;
    }

    public void setValidityPeriodSupported(boolean validityPeriodSupported) {
        this.validityPeriodSupported = validityPeriodSupported;
    }

    public boolean isEncryptionSupported() {
        return encryptionSupported;
    }

    public void setEncryptionSupported(boolean encryptionSupported) {
        this.encryptionSupported = encryptionSupported;
    }

    public int getPersistenceMode() {
        return persistenceMode;
    }

    protected void setPersistenceMode(int persistenceMode) {
        this.persistenceMode = persistenceMode;
    }

    public int getPersistenceFormat() {
        return persistenceFormat;
    }

    public void setPersistenceFormat(int persistenceFormat) {
        this.persistenceFormat = persistenceFormat;
    }

    public boolean isFormatPlain() {
        return DocumentType.FORMAT_PLAIN == persistenceFormat;
    }

    public boolean isDatabaseStore() {
        return persistenceMode == DATABASE_STORE;
    }

    public boolean isFilesystemStore() {
        return persistenceMode == FILESYSTEM_STORE;
    }

    public boolean isTemplate() {
        return persistenceMode == TEMPLATE;
    }

    public String getContextName() {
        return contextName;
    }

    public void setContextName(String contextName) {
        this.contextName = contextName;
    }

    public String getContextPath() {
        return contextPath;
    }

    public void setContextPath(String contextPath) {
        this.contextPath = contextPath;
    }

    public String getUploadPath() {
        if (config != null && config.getRoot() != null) {
            if (isSet(contextPath)) {
                // upload path of user uploaded content
                return config.getRoot() + contextPath;
            }
            // upload path of system owned fixed documents
            return completedPath(config.getRoot());
        }
        // use case unknown
        return contextPath;
    }

    public String getDownloadUrl() {
        if (config != null && config.getUrl() != null) {
            if (isSet(contextPath)) {
                // url of user uploaded content
                return config.getUrl() + contextPath;
            }
            // url of system owned fixed documents
            return completedPath(config.getUrl());
        }
        // use case unknown
        return contextPath;
    }
    
    public String getPrintUrl() {
        if (config != null && isSet(config.getPrintUrl())) {
            return config.getPrintUrl();
        }
        return "";
    }

    private String completedPath(String path) {
        if (isNotSet(path)) {
            return "/";
        }
        if (!path.endsWith("/")) {
            return path + "/";
        }
        return path;
    }

    public boolean isNoteRequired() {
        return noteRequired;
    }

    public void setNoteRequired(boolean noteRequired) {
        this.noteRequired = noteRequired;
    }

    public boolean isSuffixByFile() {
        return suffixByFile;
    }

    public void setSuffixByFile(boolean suffixByFile) {
        this.suffixByFile = suffixByFile;
    }

    public boolean isSupportingReferenceType() {
        return supportingReferenceType;
    }

    public void setSupportingReferenceType(boolean supportingReferenceType) {
        this.supportingReferenceType = supportingReferenceType;
    }

    public boolean isReferenceSelection() {
        return referenceSelection;
    }

    public void setReferenceSelection(boolean referenceSelection) {
        this.referenceSelection = referenceSelection;
    }

    public String getReferenceUrl() {
        return referenceUrl;
    }

    public void setReferenceUrl(String referenceUrl) {
        this.referenceUrl = referenceUrl;
    }

    public String getNamingStyle() {
        return namingStyle;
    }

    public void setNamingStyle(String namingStyle) {
        this.namingStyle = namingStyle;
    }

    public boolean isUseFilenameForDownload() {
        return useFilenameForDownload;
    }

    public void setUseFilenameForDownload(boolean useFilenameForDownload) {
        this.useFilenameForDownload = useFilenameForDownload;
    }

    public void toggleFilenameForDownload() {
        useFilenameForDownload = !useFilenameForDownload;
    }

    public boolean isCreateThumbmail() {
        return createThumbmail;
    }

    public void setCreateThumbmail(boolean createThumbmail) {
        this.createThumbmail = createThumbmail;
    }

    public int getThumbmailWidth() {
        return thumbmailWidth;
    }

    public void setThumbmailWidth(int thumbmailWidth) {
        this.thumbmailWidth = thumbmailWidth;
    }

    public int getThumbmailHeight() {
        return thumbmailHeight;
    }

    public void setThumbmailHeight(int thumbmailHeight) {
        this.thumbmailHeight = thumbmailHeight;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getPreventSuffix() {
        return preventSuffix;
    }

    public void setPreventSuffix(String preventSuffix) {
        this.preventSuffix = preventSuffix;
    }

    public void validateSuffix(String fileSuffix) throws ClientException {
        if (!isSuffixByFile()) {
            if (isNotSet(getSuffix())) {
                throw new ClientException(ErrorCode.DOCUMENT_TYPE_CONFIG);
            }
            if (isNotSet(fileSuffix) || !fileSuffix.equalsIgnoreCase(getSuffix())
                    && !(getSuffix().equalsIgnoreCase("jpg")
                            && "jpeg".equalsIgnoreCase(fileSuffix))) {
                throw new ClientException(ErrorCode.DOCUMENT_TYPE_INVALID);
            }
        }
        if (isSet(preventSuffix) && isSet(fileSuffix)) {
            String[] slist = StringUtil.getTokenArray(preventSuffix);
            if (slist != null) {
                for (int i = 0, j = slist.length; i < j; i++) {
                    String next = slist[i];
                    if (next != null && next.equalsIgnoreCase(fileSuffix)) {
                        throw new ClientException(ErrorCode.DOCUMENT_TYPE_INVALID);
                    }
                }
            }
        }
    }
}
