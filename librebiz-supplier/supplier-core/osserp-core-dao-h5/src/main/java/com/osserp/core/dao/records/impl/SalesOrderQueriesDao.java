/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 25, 2008 6:27:04 AM 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.sql.Types;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.dao.Tables;
import com.osserp.core.dao.ProductSummaryCache;
import com.osserp.core.dao.impl.AbstractDao;
import com.osserp.core.dao.records.SalesOrderQueries;
import com.osserp.core.views.OrderItemsDisplay;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesOrderQueriesDao extends AbstractDao
        implements SalesOrderQueries {
    private static Logger log = LoggerFactory.getLogger(SalesOrderQueriesDao.class.getName());

    private ProductSummaryCache summaryCache = null;

    public SalesOrderQueriesDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            ProductSummaryCache summaryCache) {
        super(jdbcTemplate, tables, sessionFactory);
        this.summaryCache = summaryCache;
    }

    public List<OrderItemsDisplay> getOpenItems(Long stockId, Long productId) {
        if (log.isDebugEnabled()) {
            if (productId != null) {
                log.debug("getOpenItems() invoked [stock=" + stockId + ", product=" + productId + "]");
            } else {
                log.debug("getOpenItems() invoked [stock=" + stockId + ", products=all]");
            }
        }
        return (List<OrderItemsDisplay>) jdbcTemplate.query(
                RecordProcs.SALES_ORDERS_OPEN_ITEMS_BY_STOCK_AND_PRODUCT,
                new Object[] { stockId, productId },
                new int[] { Types.BIGINT, Types.BIGINT },
                OrderItemsDisplayRowMapper.getReader(stockId, summaryCache, true, false));
    }

    public List<OrderItemsDisplay> getUnreleasedItems(Long stockId, Long productId) {
        if (log.isDebugEnabled()) {
            if (productId != null) {
                log.debug("getUnreleasedItems() invoked [stock=" + stockId + ", product=" + productId + "]");
            } else {
                log.debug("getUnreleasedItems() invoked [stock=" + stockId + ", products=all]");
            }
        }
        return (List<OrderItemsDisplay>) jdbcTemplate.query(
                RecordProcs.SALES_ORDERS_UNRELEASED_ITEMS_BY_STOCK_AND_PRODUCT,
                new Object[] { stockId, productId },
                new int[] { Types.BIGINT, Types.BIGINT },
                OrderItemsDisplayRowMapper.getReader(stockId, summaryCache, true, true));
    }

    public List<OrderItemsDisplay> getVacantItems(Long stockId, Long productId) {
        if (log.isDebugEnabled()) {
            if (productId != null) {
                log.debug("getVacantItems() invoked [stock=" + stockId + ", product=" + productId + "]");
            } else {
                log.debug("getVacantItems() invoked [stock=" + stockId + ", products=all]");
            }
        }
        return (List<OrderItemsDisplay>) jdbcTemplate.query(
                RecordProcs.SALES_ORDERS_VACANT_ITEMS_BY_STOCK_AND_PRODUCT,
                new Object[] { stockId, productId },
                new int[] { Types.BIGINT, Types.BIGINT },
                OrderItemsDisplayRowMapper.getReader(stockId, summaryCache, true, true));
    }

}
