/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 04-Feb-2007 14:11:11 
 * 
 */
package com.osserp.core.service.records;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.core.dao.Products;
import com.osserp.core.dao.records.SalesCancellations;
import com.osserp.core.dao.records.SalesCreditNotes;
import com.osserp.core.dao.records.SalesInvoices;
import com.osserp.core.dao.records.SalesOrderDownpayments;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.CancellableRecord;
import com.osserp.core.finance.Cancellation;
import com.osserp.core.finance.CreditNote;
import com.osserp.core.finance.Downpayment;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordType;
import com.osserp.core.sales.SalesCancellationManager;
import com.osserp.core.sales.SalesDeliveryNoteManager;
import com.osserp.core.sales.SalesInvoice;
import com.osserp.core.system.SystemConfigManager;
import com.osserp.core.tasks.ProductPlanningCacheSender;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesCancellationManagerImpl extends AbstractRecordManager
        implements SalesCancellationManager {
    private static Logger log = LoggerFactory.getLogger(SalesCancellationManagerImpl.class.getName());
    
    private SalesInvoices invoices = null;
    private SalesCreditNotes creditNotes = null;
    private SalesDeliveryNoteManager deliveryNoteManager = null;
    private SalesOrderDownpayments downpayments = null;

    public SalesCancellationManagerImpl(
            SalesCancellations records,
            SystemConfigManager systemConfigManager,
            Products products,
            ProductPlanningCacheSender productPlanningTask,
            SalesInvoices invoices,
            SalesCreditNotes creditNotes,
            SalesOrderDownpayments downpayments,
            SalesDeliveryNoteManager deliveryNoteManager) {
        super(records, systemConfigManager, products, productPlanningTask);
        this.invoices = invoices;
        this.creditNotes = creditNotes;
        this.deliveryNoteManager = deliveryNoteManager;
        this.downpayments = downpayments;
    }

    public Cancellation create(Employee user, CreditNote note, Date date, Long customId) throws ClientException {
        if (note.isCanceled()) {
            throw new ClientException(ErrorCode.RECORD_ALREADY_CANCELLED);
        }
        return creditNotes.cancel(user, note, date, customId);
    }

    public Cancellation create(Employee user, Invoice invoice, Date date, Long customId) throws ClientException {
        if (invoice.isCanceled()) {
            throw new ClientException(ErrorCode.RECORD_ALREADY_CANCELLED);
        }
        if (invoice instanceof Downpayment) {
            return downpayments.cancel(user, (Downpayment) invoice, date).getCancellation();
        }
        return invoices.cancel(user, (SalesInvoice) invoice, date, customId).getCancellation();
    }

    @Override
    public void delete(Record record, Employee employee) throws ClientException {
        if (!(record instanceof Cancellation)) {
            throw new ActionException();
        }
        Cancellation cancellation = (Cancellation) record;
        if (RecordType.SALES_INVOICE.equals(cancellation.getCancelledType())) {
            invoices.restoreCanceled(employee, cancellation);
        } else if (RecordType.SALES_CREDIT_NOTE.equals(cancellation.getCancelledType())) {
            creditNotes.resetCanceled(employee, cancellation);
        }
    }

    public Cancellation find(CancellableRecord cancellable) {
        return getCancellationsDao().loadCancellation(cancellable);
    }

    public void update(
            Record record,
            Long signatureLeft,
            Long signatureRight,
            Long personId,
            String note,
            boolean printBusinessCaseId,
            boolean printBusinessCaseInfo)
            throws ClientException {

        if (log.isDebugEnabled()) {
            StringBuilder buffer = new StringBuilder(96);
            buffer
                    .append("update() invoked for cancellation ").append(record.getId())
                    .append("\nnote ").append(note)
                    .append("\nsignatureLeft ").append(signatureLeft)
                    .append("\nsignatureRight ").append(signatureRight);
            log.debug(buffer.toString());
        }
        if (record.getStatus() >= Cancellation.STAT_SENT.longValue()) {
            throw new ClientException(ErrorCode.RECORD_UNCHANGEABLE);
        }
        record.update(
                signatureLeft,
                signatureRight,
                personId,
                note,
                record.isTaxFree(),
                record.getTaxFreeId(),
                record.getAmounts().getTaxRate(),
                record.getAmounts().getReducedTaxRate(),
                record.getCurrency(),
                record.getLanguage(),
                record.getBranchId(),
                record.getShippingId(),
                null,
                false,
                false,
                false,
                printBusinessCaseId,
                printBusinessCaseInfo,
                false,
                false,
                false,
                false,
                false);
        getDao().save(record);
    }

    @Override
    protected void doAfterStatusUpdate(
            Record record,
            Employee employee,
            Long oldStatus) {
        if (Record.STAT_SENT.equals(record.getStatus()) && record instanceof Cancellation
                && !RecordType.SALES_DOWNPAYMENT.equals(
                        ((Cancellation) record).getCancelledType()) ) {
            deliveryNoteManager.createRollin(employee, (Cancellation) record);
        }
    }

    protected SalesCancellations getCancellationsDao() {
        return (SalesCancellations) getDao();
    }
}