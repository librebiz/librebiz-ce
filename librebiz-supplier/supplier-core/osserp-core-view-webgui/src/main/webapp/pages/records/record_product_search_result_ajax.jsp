<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<o:logger write="page invoked..." level="debug"/>
<c:set var="view" value="${sessionScope.searchView}"/>
<c:set var="productExitTarget" value="${view.data['productExitTarget']}"/>
<o:form name="multipleItemListForm" url="${view.data['searchUrl']}">
<input type="hidden" name="method" value="save" />
<div class="table-responsive table-responsive-default">

    <table class="table table-striped">
        <thead>
            <tr>
                <th class="productNumber">
                    <fmt:message key="product"/>
                </th>
                <th class="productName">
                    <fmt:message key="name"/>
                </th>
                <th class="productSalesOrders">
                    <fmt:message key="order"/>
                </th>
                <th class="productStock">
                    <fmt:message key="inventory"/>
                </th>
                <th class="productPurchaseReceipts">
                    <fmt:message key="stockReceiptShort"/>
                </th>
                <th class="productPurchaseOrders">
                    <fmt:message key="purchaseOrderShort"/>
                </th>
                <c:choose>
                    <c:when test="${view.recordView.priceInputEnabled}">
                        <th class="productBasePrice" style="text-align: right;">
                            <span style="margin-right: 2px;" title="<fmt:message key="partnerPrice"/>"><fmt:message key="partnerPriceShort"/></span>
                        </th>
                        <th class="productTaxRate" style="text-align: right;">
                            <span style="margin-right: 2px;"><fmt:message key="vat"/></span>
                        </th>
                        <th class="productQuantity" style="text-align: right;">
                            <span style="margin-right: 2px;"><fmt:message key="quantity"/></span>
                        </th>
                        <th class="productPrice" style="text-align: right;">
                            <span style="margin-right: 2px;"><fmt:message key="price"/></span>
                        </th>
                    </c:when>
                    <c:otherwise>
                        <th class="productQuantity" style="text-align: right;">
                            <span style="margin-right: 2px;"><fmt:message key="quantity"/></span>
                        </th>
                    </c:otherwise>
                </c:choose>
                <th class="productAction"> </th>
            </tr>
        </thead>
        <tbody>
            <c:choose>
                <c:when test="${empty view.list}">
                    <tr>
                        <td class="left" colspan="<c:choose><c:when test="${view.recordView.priceInputEnabled}">11</c:when><c:otherwise>8</c:otherwise></c:choose>">
                            <fmt:message key="searchUnsuccessfulPleaseChangeYourInput" />
                        </td>
                    </tr>
                </c:when>
                <c:otherwise>
                    <c:forEach var="product" items="${view.list}" varStatus="s">
                        <tr id="product${product.productId}">
                            <td class="productNumber">
                                <a href="<c:url value="/products.do?method=load&id=${product.productId}&exit=${productExitTarget}&exitId=product${product.productId}"/>" title="<fmt:message key="loadProduct"/>">
                                    <o:out value="${product.productId}" /><input type="hidden" name="productId" value="<o:out value="${product.productId}"/>" />
                                </a>
                            </td>
                            <td class="productName">
                                <oc:systemPropertyEnabled name="productCustomNameSupport">
                                    <c:choose>
                                        <c:when test="${!product.affectsStock and (product.group.customNameSupport or product.category.customNameSupport)}">
                                            <input type="text" class="form-control" style="text-align:left;" name="customName" value="<o:out value="${product.name}"/>"/>
                                        </c:when>
                                        <c:otherwise>
                                            <input type="text" class="form-control" style="text-align:left;" name="dummyName" value="<o:out value="${product.name}"/>" disabled="disabled"/>
                                            <input type="hidden" name="customName" value=""/>
                                        </c:otherwise>
                                    </c:choose>
                                </oc:systemPropertyEnabled>
                                <oc:systemPropertyDisabled name="productCustomNameSupport">
                                    <o:out value="${product.name}" /><input type="hidden" name="customName" value=""/>
                                </oc:systemPropertyDisabled>
                            </td>
                            <td class="productSalesOrders"><span title="<fmt:message key="currentOrdersExclusiveThis"/>"><o:number value="${product.summary.ordered}" format="integer"/></span></td>
                            <td class="productStock"><span title="<fmt:message key="currentOnStock"/>"><o:number value="${product.summary.stock}" format="integer"/></span></td>
                            <td class="productPurchaseReceipts"><span title="<fmt:message key="currentPurchaseDelivery"/>"><o:number value="${product.summary.receipt}" format="integer"/></span></td>
                            <td class="productPurchaseOrders"><span title="<fmt:message key="currentPurchaseOrders"/>"><o:number value="${product.summary.expected}" format="integer"/></span></td>
                            <c:choose>
                                <c:when test="${view.recordView.priceInputEnabled}">
                                    <td class="productBasePrice">
                                        <c:choose>
                                            <c:when test="${product.specificPartnerPriceAvailable}">
                                                <span title="<fmt:formatNumber value="${product.partnerPrice}" maxFractionDigits="2" minFractionDigits="2"/>/<fmt:message key="piece"/>">
                                                    <fmt:formatNumber value="${product.specificPartnerPrice}" maxFractionDigits="2" minFractionDigits="2"/>/EH
                                                </span>
                                            </c:when>
                                            <c:otherwise>
                                                <fmt:formatNumber value="${product.partnerPrice}" maxFractionDigits="2" minFractionDigits="2"/>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                    <c:choose>
                                        <c:when test="${product.productId == view.existingProduct}">
                                            <td class="productTaxRate">
                                                <select name="taxRate" class="form-control right">
                                                    <c:choose>
                                                        <c:when test="${product.reducedTax}">
                                                            <c:forEach var="rate" items="${view.reducedTaxRates}">
                                                                <c:choose>
                                                                    <c:when test="${view.existingReducedTaxRate == rate}">
                                                                        <option value="<o:out value="${rate}"/>" selected="selected">
                                                                            <o:number value="${rate}" format="tax" /> <span>%</span>
                                                                        </option>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <option value="<o:out value="${rate}"/>">
                                                                            <o:number value="${rate}" format="tax" /> <span>%</span>
                                                                        </option>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                            </c:forEach>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <c:forEach var="rate" items="${view.taxRates}">
                                                                <c:choose>
                                                                    <c:when test="${view.existingTaxRate == rate}">
                                                                        <option value="<o:out value="${rate}"/>" selected="selected">
                                                                            <o:number value="${rate}" format="tax" /> <span>%</span>
                                                                        </option>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <option value="<o:out value="${rate}"/>">
                                                                            <o:number value="${rate}" format="tax" /> <span>%</span>
                                                                        </option>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                            </c:forEach>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </select>
                                            </td>
                                            <td class="productQuantity">
                                                <input type="text" class="form-control" name="quantity" value="<o:number value="${view.existingQuantity}" format="decimal"/>" />
                                            </td>
                                            <td class="productPrice">
                                                <input type="text" class="form-control" name="price" value="<o:number value="${view.existingPrice}" format="currency"/>"/>
                                            </td>
                                        </c:when>
                                        <c:otherwise>
                                            <td class="productTaxRate">
                                                <select name="taxRate" class="form-control right">
                                                    <c:choose>
                                                        <c:when test="${product.reducedTax}">
                                                            <c:forEach var="rate" items="${view.reducedTaxRates}">
                                                                <c:choose>
                                                                    <c:when test="${view.existingReducedTaxRate == rate}">
                                                                        <option value="<o:out value="${rate}"/>" selected="selected">
                                                                            <o:number value="${rate}" format="tax" /> <span>%</span>
                                                                        </option>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <option value="<o:out value="${rate}"/>">
                                                                            <o:number value="${rate}" format="tax" /> <span>%</span>
                                                                        </option>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                            </c:forEach>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <c:forEach var="rate" items="${view.taxRates}">
                                                                <c:choose>
                                                                    <c:when test="${view.existingTaxRate == rate}">
                                                                        <option value="<o:out value="${rate}"/>" selected="selected">
                                                                            <o:number value="${rate}" format="tax" /> <span>%</span>
                                                                        </option>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <option value="<o:out value="${rate}"/>">
                                                                            <o:number value="${rate}" format="tax" /> <span>%</span>
                                                                        </option>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                            </c:forEach>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </select>
                                            </td>
                                            <td class="productQuantity">
                                                <input type="text" class="form-control" name="quantity" value="0" />
                                            </td>
                                            <td class="productPrice">
                                                <input type="text" class="form-control" name="price" value="<oc:defaultPrice product="${product}" contact="${view.recordView.record.contact}" method="${view.productPriceMethod}" missing="${view.productPriceDefault}" />"/>
                                            </td>
                                        </c:otherwise>
                                    </c:choose>
                                </c:when>
                                <c:otherwise>
                                    <td class="productQuantity">
                                        <input type="text" class="form-control" name="taxRate" value="0" />
                                        <input type="text" class="form-control" name="quantity" value="0" />
                                        <input type="hidden" name="price" value="0" />
                                    </td>
                                </c:otherwise>
                            </c:choose>
                            <td class="productAction">
                                <input type="image" src="<c:url value="${applicationScope.saveIcon}"/>" />
                            </td>
                        </tr>
                        <tr>
                            <td class="productNumber">&nbsp;</td>
                            <td colspan="<c:choose><c:when test="${view.recordView.priceInputEnabled}">8</c:when><c:otherwise>6</c:otherwise></c:choose>" class="productNote">
                                <input type="text" class="form-control" name="note" style="text-align: left;" value=""/>
                            </td>
                            <td class="productAction">&nbsp;</td>
                        </tr>
                    </c:forEach>
                </c:otherwise>
            </c:choose>
        </tbody>
    </table>
</div>
</o:form>
