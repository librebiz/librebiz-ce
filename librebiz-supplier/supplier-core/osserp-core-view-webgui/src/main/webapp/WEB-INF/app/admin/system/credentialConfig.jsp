<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="credentialConfigView"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="javascript" type="string">
		<script type="text/javascript">
		</script>
	</tiles:put>
	<tiles:put name="headline">
		<fmt:message key="credentialConfigView"/>
	</tiles:put>
	<tiles:put name="headline_right">
		<v:navigation/>
	</tiles:put>
	<tiles:put name="content" type="string">
        <div class="content-area">
            <v:form id="credentialConfigForm" url="/admin/system/credentialConfig/save">
                <c:choose>
                    <c:when test="${view.credentialsSaved}">
                        <c:import url="_credentialConfigSuccess.jsp"/>
                    </c:when>
                    <c:when test="${view.staticContext}">
                        <c:import url="_credentialConfigStatic.jsp"/>
                    </c:when>
                    <c:otherwise>
                        <c:import url="_credentialConfig.jsp"/>
                    </c:otherwise>
                </c:choose>
            </v:form>
        </div>
	</tiles:put>
</tiles:insert>