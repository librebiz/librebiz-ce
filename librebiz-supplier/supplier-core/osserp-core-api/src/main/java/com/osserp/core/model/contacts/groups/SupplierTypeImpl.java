/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 16, 2014 2:24:42 PM
 * 
 */
package com.osserp.core.model.contacts.groups;

import com.osserp.common.beans.AbstractOption;

import com.osserp.core.suppliers.SupplierType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SupplierTypeImpl extends AbstractOption implements SupplierType {

    private boolean defaultType; 
    private boolean billingOnly;
    private boolean overrideSupplierName;
    private boolean providesBankaccounts;
    private boolean pubAuthorities;
    private boolean taxAuthorities;
    private boolean taxFree;
    private Long taxFreeId;
    private boolean simpleBilling;
    private Long simpleInvoiceBookingProduct;
    private boolean simpleInvoiceBookingEnabled = false;
    private boolean directInvoiceBookingEnabled = false;
    private Long directInvoiceBookingSelection = null;
    private boolean salaryAccount = false;
    private boolean shipping = false;

    protected SupplierTypeImpl() {
        super();
    }

    public boolean isDefaultType() {
        return defaultType;
    }

    public void setDefaultType(boolean defaultType) {
        this.defaultType = defaultType;
    }

    public boolean isBillingOnly() {
        return billingOnly;
    }

    public void setBillingOnly(boolean billingOnly) {
        this.billingOnly = billingOnly;
    }

    public boolean isOverrideSupplierName() {
        return overrideSupplierName;
    }

    public void setOverrideSupplierName(boolean overrideSupplierName) {
        this.overrideSupplierName = overrideSupplierName;
    }

    public boolean isProvidesBankaccounts() {
        return providesBankaccounts;
    }

    public void setProvidesBankaccounts(boolean providesBankaccounts) {
        this.providesBankaccounts = providesBankaccounts;
    }

    public boolean isPubAuthorities() {
        return pubAuthorities;
    }

    public void setPubAuthorities(boolean pubAuthorities) {
        this.pubAuthorities = pubAuthorities;
    }

    public boolean isTaxAuthorities() {
        return taxAuthorities;
    }

    public void setTaxAuthorities(boolean taxAuthorities) {
        this.taxAuthorities = taxAuthorities;
    }

    public boolean isTaxFree() {
        return taxFree;
    }

    public void setTaxFree(boolean taxFree) {
        this.taxFree = taxFree;
    }

    public Long getTaxFreeId() {
        return taxFreeId;
    }

    public void setTaxFreeId(Long taxFreeId) {
        this.taxFreeId = taxFreeId;
    }

    public boolean isSimpleBilling() {
        return simpleBilling;
    }

    public void setSimpleBilling(boolean simpleBilling) {
        this.simpleBilling = simpleBilling;
    }

    public boolean isSimpleInvoiceBookingAvailable() {
        return simpleInvoiceBookingEnabled && isSet(simpleInvoiceBookingProduct);
    }

    public boolean isSimpleInvoiceBookingEnabled() {
        return simpleInvoiceBookingEnabled;
    }

    public void setSimpleInvoiceBookingEnabled(boolean simpleInvoiceBookingEnabled) {
        this.simpleInvoiceBookingEnabled = simpleInvoiceBookingEnabled;
    }

    public Long getSimpleInvoiceBookingProduct() {
        return simpleInvoiceBookingProduct;
    }

    public void setSimpleInvoiceBookingProduct(Long simpleInvoiceBookingProduct) {
        this.simpleInvoiceBookingProduct = simpleInvoiceBookingProduct;
    }

    public boolean isDirectInvoiceBookingEnabled() {
        return directInvoiceBookingEnabled;
    }

    public void setDirectInvoiceBookingEnabled(boolean directInvoiceBookingEnabled) {
        this.directInvoiceBookingEnabled = directInvoiceBookingEnabled;
    }

    public Long getDirectInvoiceBookingSelection() {
        return directInvoiceBookingSelection;
    }

    public void setDirectInvoiceBookingSelection(Long directInvoiceBookingSelection) {
        this.directInvoiceBookingSelection = directInvoiceBookingSelection;
    }

    public boolean isSalaryAccount() {
        return salaryAccount;
    }

    public void setSalaryAccount(boolean salaryAccount) {
        this.salaryAccount = salaryAccount;
    }

    public boolean isShipping() {
        return shipping;
    }

    public void setShipping(boolean shipping) {
        this.shipping = shipping;
    }
}
