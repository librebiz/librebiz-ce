/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 08-Feb-2007 18:08:47 
 * 
 */
package com.osserp.core.service.records;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.osserp.common.util.NumberUtil;
import com.osserp.common.util.StringUtil;

import com.osserp.core.dao.SystemConfigs;
import com.osserp.core.dao.records.RecordTypes;

import com.osserp.core.finance.BillingType;
import com.osserp.core.finance.BillingTypeSearch;
import com.osserp.core.finance.Downpayment;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.Payment;
import com.osserp.core.finance.PaymentAwareRecord;
import com.osserp.core.finance.Records;
import com.osserp.core.service.impl.AbstractService;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class BillingTypeSearchImpl extends AbstractService implements BillingTypeSearch {
    private SystemConfigs systemConfigs = null;
    private RecordTypes billingTypes = null;

    public BillingTypeSearchImpl(RecordTypes billingTypes, SystemConfigs systemConfigs) {
        super();
        this.billingTypes = billingTypes;
        this.systemConfigs = systemConfigs;
    }

    public List<BillingType> getBillingTypes(String contextName) {
        return billingTypes.getBillingTypes(contextName);
    }

    public List<BillingType> findInvoiceTypes() {
        return billingTypes.getBillingTypes(BillingType.CLASS_INVOICE);
    }

    public List<BillingType> findPaymentTypes() {
        return billingTypes.getBillingTypes(BillingType.CLASS_INVOICE_PAYMENT);
    }

    public List<BillingType> getPaymentTypes(DomainUser user, PaymentAwareRecord record) {
        boolean downpayment = (record instanceof Downpayment);
        boolean invoiceWithPayments = (!downpayment
                && (record instanceof Invoice && !record.getPayments().isEmpty()));
        List<BillingType> result = findPaymentTypes();
        for (Iterator<BillingType> i = result.iterator(); i.hasNext();) {
            BillingType next = i.next();
            if (downpayment && (BillingType.TYPE_CREDIT_NOTE.equals(next.getId())
                    || BillingType.TYPE_CASH_DISCOUNT.equals(next.getId()))) {
                // don't create creditNotes for downpayments
                i.remove();
            } else if (BillingType.TYPE_CLEARING.equals(next.getId())) {
                Double dueAmount = record.getDueAmount().doubleValue();
                if (downpayment || dueAmount == 0) {
                    i.remove();
                } else {
                    Double limit = NumberUtil.createDouble(systemConfigs.getSystemProperty(
                            BillingType.CLEARING_LIMIT_PROPERTY));
                    if ( (dueAmount > 0 && dueAmount > limit)
                            || (dueAmount < 0 && dueAmount < (limit * (-1))) ) {
                        // don't provide clearing above or below limit
                        i.remove();
                    }
                }
            } else if (BillingType.TYPE_CUSTOM_PAYMENT.equals(next.getId())) {
                if (downpayment || isNotSet(next.getPermissions())
                        || !user.isPermissionGrant(StringUtil.getTokenArray(next.getPermissions()))) {
                    i.remove();
                }
            } else if (invoiceWithPayments && BillingType.TYPE_CANCELLATION.equals(next.getId())
                    && Records.isPaymentAssigned((Invoice) record)) {
                // don't cancel invoice with payments, use creditNote instead
                i.remove();
            } else if (!invoiceWithPayments && next.isCashDiscount()) {
                // don't offer to select cash discount as long as nothing paid
                i.remove();
            } else if (!record.getPayments().isEmpty() &&
                    (record.getDueAmount() != null && record.getDueAmount().longValue() == 0)
                    && (Records.isRegularPayment(next) // // don't add payments to paid invoice 
                            // don't add discount if already exists:
                            || (next.isCashDiscount() && isCashDiscountAdded(record)))) {
                i.remove();
            } else if (!record.getPayments().isEmpty() && record.getDueAmount() != null) {

                Double dueAmount = record.getDueAmount().doubleValue();
                if (dueAmount == 0 && (Records.isRegularPayment(next) // don't add payments to paid invoice
                            // don't add discount if already exists:
                            || (next.isCashDiscount() && isCashDiscountAdded(record)))) {
                    i.remove();
                }
            }
        }
        return result;
    }

    private boolean isCashDiscountAdded(PaymentAwareRecord record) {
        for (int i = 0, j = record.getPayments().size(); i < j; i++) {
            Payment p = record.getPayments().get(i);
            if (p.getType() instanceof BillingType 
                    && ((BillingType) p.getType()).isCashDiscount()) {
                return true;
            }
        }
        return false;
    }

    public Map<Long, Long> findBillingFcsRelations() {
        return billingTypes.findBillingFcsRelations();
    }

}
