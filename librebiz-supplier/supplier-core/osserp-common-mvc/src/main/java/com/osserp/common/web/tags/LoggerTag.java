/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 12-Aug-2005 10:24:01 
 * 
 */
package com.osserp.common.web.tags;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.web.RequestUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class LoggerTag extends AbstractSimpleValueTag {
    private static Logger log = LoggerFactory.getLogger(LoggerTag.class.getName());

    private static final String DEBUG = "debug";
    private static final String INFO = "info";
    private static final String WARN = "warn";
    private static final String ERROR = "error";

    protected String write = null;

    public String getWrite() {
        return (write);
    }

    public void setWrite(String write) {
        this.write = write;
    }

    protected String level = null;

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    private boolean toString = false;

    public boolean isToString() {
        return toString;
    }

    public void setToString(boolean toString) {
        this.toString = toString;
    }

    @Override
    public void doTag() throws JspException, IOException {
        HttpServletRequest req = getRequest();
        StringBuilder buffer = new StringBuilder(
                RequestUtil.createPageName(req)).append("; ");
        if (write != null) {
            buffer.append(write);
        }
        if (value != null) {
            if (toString) {
                buffer
                        .append(", dump [")
                        .append(value.getClass().getName())
                        .append("] values:\n")
                        .append(value.toString());
            } else {
                buffer.append(": ").append(value.toString());
            }
        }
        if (level == null || level.equals(DEBUG)) {
            if (log.isDebugEnabled()) {
                log.debug(buffer.toString());
            }
        } else if (level.equals(INFO)) {
            if (log.isInfoEnabled()) {
                log.info(buffer.toString());
            }
        } else if (level.equals(WARN)) {
            log.warn(buffer.toString());
        } else if (level.equals(ERROR)) {
            log.error(buffer.toString());
        } else {
            log.warn("LoggerTag() called with invalid level!");
        }
    }
}
