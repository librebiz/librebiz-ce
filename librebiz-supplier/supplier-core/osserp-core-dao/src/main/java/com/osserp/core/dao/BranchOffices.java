/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 05-Aug-2008 
 * 
 */
package com.osserp.core.dao;

import java.util.List;
import java.util.Map;

import com.osserp.core.system.BranchOffice;

/**
 * 
 * @author cf <cf@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public interface BranchOffices {

    /**
     * Provides the branch office with the given id
     * @return branchOffice.
     * @throws RuntimeException if not exists
     */
    BranchOffice getBranchOffice(Long id);

    /**
     * Provides the primary office.
     * @return branchOffice.
     * @throws RuntimeException if not exists
     */
    BranchOffice getHeadquarter();

    /**
     * Provides a list of all available branch offices whose related
     * contact has not been set as deleted (contact.status != -10).
     * @return branchOffices
     */
    List<BranchOffice> getBranchOffices();

    /**
     * Provides all company related branchs
     * @param id
     * @return branchs by company
     */
    List<BranchOffice> getByCompany(Long id);

    /**
     * Loads a branch by contact. Throws runtime exception if branch not exists.
     * @param contactId
     * @return branch by contact
     */
    BranchOffice getByContact(Long contactId);

    /**
     * Fetches a branch by contact if exists
     * @param contactId
     * @return branch by contact or null if not exists
     */
    BranchOffice findByContact(Long contactId);

    /**
     * Creates a value map of a branch office
     * @param branch
     * @return value map
     */
    Map<String, Object> createValueMap(Long branch);

    /**
     * saves a branch object
     * @param branchOffice
     */
    void save(BranchOffice branchOffice);

}
