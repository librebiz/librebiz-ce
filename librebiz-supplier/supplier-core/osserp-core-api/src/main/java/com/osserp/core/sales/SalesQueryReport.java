/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 18, 2007 7:57:08 AM 
 * 
 */
package com.osserp.core.sales;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.osserp.core.BusinessType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesQueryReport implements Comparable, Serializable {

    private BusinessType type = null;
    private Double capacity = 0d;
    private List list = new ArrayList<>();

    protected SalesQueryReport() {
        super();
    }

    @SuppressWarnings("unchecked")
    public SalesQueryReport(
            BusinessType type,
            Object businessObject,
            Double businessObjectCapacity) {
        this.type = type;
        if (businessObjectCapacity != null) {
            capacity = businessObjectCapacity;
        }
        list.add(businessObject);
    }

    public BusinessType getType() {
        return type;
    }

    public Double getCapacity() {
        return capacity;
    }

    @SuppressWarnings("unchecked")
    public void addBusinessObject(Object businessObject, Double businessObjectCapacity) {
        if (businessObject != null && businessObjectCapacity != null) {
            list.add(businessObject);
            capacity = capacity + businessObjectCapacity;
        }
    }

    public int compareTo(Object arg0) {
        if (type == null || !(arg0 instanceof SalesQueryReport)) {
            return 0;
        }
        SalesQueryReport other = (SalesQueryReport) arg0;
        return type.getName().compareTo(other.getType().getName());
    }

    @Override
    public boolean equals(Object arg0) {
        if (arg0 == null || !(arg0 instanceof SalesQueryReport) || type == null) {
            return false;
        }
        SalesQueryReport other = (SalesQueryReport) arg0;
        return type.getId().equals(other.getType().getId());
    }

    // readonly properties

    protected void setCapacity(Double capacity) {
        this.capacity = capacity;
    }

    protected void setType(BusinessType type) {
        this.type = type;
    }
}
