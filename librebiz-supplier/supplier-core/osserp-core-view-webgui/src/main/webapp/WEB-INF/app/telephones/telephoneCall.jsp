<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="telephoneCallView"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="title"><o:displayTitle/></tiles:put>
	<tiles:put name="javascript" type="string">
		<script type="text/javascript">
			telephoneCall = new Object();
			telephoneCall.checkSearchException = function() {
				try {
					if ($('dynamicForm')) {
						if ( !ojsForms.validDate($('dynamicForm').beginDate.value, false) )	{
							alert(ojsI18n.enterAValidDate);
							return false;
						}
						if ( !ojsForms.validDate($('dynamicForm').endDate.value, false) )	{
							alert(ojsI18n.enterAValidDate);
							return false;
						}
					}
					if ($('ajaxContent')) {
						$('ajaxContent').innerHTML = "<div style='width: 100%; text-align: center;'><div style='text-align:center; border:1px solid #D6DDE6; padding:10px;'><img src='/osdb/img/ajax_loader.gif' style='margin:5px;'/><br/>"+ ojsI18n.loading+"</div></div>";
					}
					return true;
				} catch (e) {
					return false;
				}
			}
		</script>
	</tiles:put>
	<tiles:put name="headline"><fmt:message key="callList"/></tiles:put>
	<tiles:put name="headline_right">
		<v:navigation/>
	</tiles:put>
	<tiles:put name="content" type="string">
		<c:if test="${!empty sessionScope.error}">
			&nbsp;
			<div class="errormessage">
				<fmt:message key="error"/>: <fmt:message key="${sessionScope.error}"/>
			</div>
            <o:removeErrors/>
		</c:if>
		<div class="spacer"></div>
		<div class="subcolumns">
			<div class="subcolumn">
				<div class="contentBox">
					<div class="contentBoxData">
						<div class="spacer"></div>
						<o:ajaxLookupForm name="dynamicForm" url="/app/telephones/telephoneCall/search" targetElement="ajaxContent" events="onkeyup" preRequest="if (telephoneCall.checkSearchException()) {" postRequest="}" minChars="0">
							<div class="subcolumns">
								<div class="column33l">
									<div class="subcolumnl">
										<select id="value" name="value" style="width: 150px;" onChange="javascript: $('dynamicForm').onkeyup();">
											<option value="" <c:if test="${empty view.currentConfig}">selected="selected"</c:if>><fmt:message key="telephoneConfiguration"/> ...</option>
											<c:forEach var="obj" items="${view.telephoneConfigurations}">
												<option value="${obj.id}" <c:if test="${!empty view.currentConfig && obj.id == view.currentConfig.id}">selected="selected"</c:if>><o:out value="${obj.displayName}"/> - <o:out value="${obj.internal}"/></option>
											</c:forEach>
										</select>
										<a href="javascript:if(telephoneCall.checkSearchException()) {$('dynamicForm').onkeyup();}">
											<o:img name="replaceIcon" styleClass="bigicon" style="padding: 0px 0px 0px 10px;" />
										</a>
									</div>
								</div>
								<div class="column33l">
									<div class="subcolumnl">
										<fmt:message key="fromDate"/> <input class="text" type="text" name="beginDate" id="beginDate" value="<o:date value="${view.beginDate}"/>" class="dateWithPicker"/> <o:datepicker input="beginDate"/>
									</div>
								</div>
								<div class="column33l">
									<div class="subcolumnl">
										<div class="subcolumnl">
											<fmt:message key="til"/> <input class="text" type="text" name="endDate" id="endDate" value="<o:date value="${view.endDate}"/>" class="dateWithPicker"/> <o:datepicker input="endDate"/>
										</div>
									</div>
								</div>
							</div>
						</o:ajaxLookupForm>
						<div class="spacer"></div>
					</div>
				</div>
				<div class="spacer"></div>
				<div id="ajaxDiv">
					<div id="ajaxContent">
						<c:import url="_telephoneCall.jsp"/>
					</div>
				</div>
			</div>
		</div>
		<div class="spacer"></div>
	</tiles:put>
</tiles:insert>
