<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>

<c:set var="view" value="${requestScope.recordListingView}"/>
<c:set var="data" value="${view.data}"/>

<div class="row">
    <div class="col-lg-12 panel-area">
        <c:if test="${!empty sessionScope.message}">
            <div class="recordheader" style="margin-left: 20px;">
                <p class="boldtext">
                    <o:out value="${sessionScope.message}"/>
                    <c:remove var="message" scope="session"/>
                </p>
            </div>
        </c:if>
        <div class="table-responsive table-responsive-default">
			<table class="table table-striped">
				<thead>
					<tr>
						<th><fmt:message key="number"/></th>
						<th><fmt:message key="name"/></th>
						<th><fmt:message key="from"/></th>
						<th class="right"><fmt:message key="amount"/></th>
						<th class="right"><fmt:message key="turnoverTax1"/></th>
						<th class="right"><fmt:message key="gross"/></th>
						<th class="center"> </th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="record" items="${view.list}" varStatus="s">
						<tr>
							<td><a href="<c:url value="${view.selectionUrl}"><c:param name="method" value="${view.selectionUrlActionName}"/><c:param name="id" value="${record.id}"/><c:param name="exit" value="${view.selectionUrlExitTarget}"/></c:url>"><o:out value="${record.number}"/></a></td>
							<c:choose>
								<c:when test="${data['recordDisplay']}">
									<td><o:out value="${record.contactName}"/></td>
									<td><o:date value="${record.created}"/></td>
									<td class="right"><o:number value="${record.netAmount}" format="currency"/></td>
									<td class="right"><o:number value="${record.taxAmount}" format="currency"/></td>
									<td class="right"><o:number value="${record.grossAmount}" format="currency"/></td>
								</c:when>
								<c:otherwise>
									<td><o:out value="${record.contact.displayName}"/></td>
									<td><o:date value="${record.created}"/></td>
									<td class="right"><o:number value="${record.amounts.amount}" format="currency"/></td>
									<td class="right"><o:number value="${record.amounts.taxAmount}" format="currency"/></td>
									<td class="right"><o:number value="${record.amounts.grossAmount}" format="currency"/></td>
								</c:otherwise>
							</c:choose>
							<td class="center"><o:out value="${record.currencySymbol}"/></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>

	</div>
</div>