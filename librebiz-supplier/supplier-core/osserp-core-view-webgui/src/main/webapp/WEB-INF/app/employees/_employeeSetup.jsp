<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <o:out value="${view.contact.displayName}"/>
            </h4>
        </div>
    </div>
    <div class="panel-body">
    
        <c:if test="${empty view.selectedBranch}">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="branchOffice"> <fmt:message key="branch" />
                        </label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <oc:select name="branchId" options="${view.branchOfficeList}" styleClass="form-control" />
                    </div>
                </div>
            </div>
        </c:if>

        <div class="row next">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="employeeType"> <fmt:message key="typeLabel" />
                    </label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <oc:select name="typeId" options="employeeTypes" styleClass="form-control" value="${view.selectedEmployeeType.id}" />
                </div>
            </div>
        </div>
        
        <div class="row next">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="employeeGroup"> <fmt:message key="group" />
                    </label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <oc:select name="groupId" options="employeeGroups" styleClass="form-control" value="${view.selectedEmployeeGroup.id}" />
                </div>
            </div>
        </div>
        
        <div class="row next">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="employeeStatus"> <fmt:message key="status" />
                    </label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <oc:select name="statusId" options="employeeStatus" styleClass="form-control" value="${view.selectedEmployeeStatus.id}" />
                </div>
            </div>
        </div>

        <v:date name="beginDate" styleClass="form-control" value="${view.beginDate}" label="dateStart" picker="true" />

        <div class="row next">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="role"><fmt:message key="role" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <v:text styleClass="form-control" name="role" value="${view.rolename}" />
                </div>
            </div>
        </div>

        <div class="row next">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="initials"><fmt:message key="initials" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <c:choose>
                        <c:when test="${!empty view.selectedEmployee.initials}">
                            <v:text styleClass="form-control" name="initials" />
                        </c:when>
                        <c:otherwise>
                            <v:text styleClass="form-control" name="initials" value="${view.initialSuggestion}" />
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>
        
        <div class="row next">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="account"> </label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <div class="checkbox">
                        <label for="headquarter"> <v:checkbox id="createAccount" value="true" /> <fmt:message key="createEmployeeUserAccount" />
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <div class="row next">
            <div class="col-md-4"></div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <v:submitExit url="/employees/employeeSetup/exit" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <o:submit styleClass="form-control" />
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
