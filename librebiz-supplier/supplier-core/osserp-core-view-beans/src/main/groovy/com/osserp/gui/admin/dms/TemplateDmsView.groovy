/**
 *
 * Copyright (C) 2015 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 18.08.2015 
 * 
 */
package com.osserp.gui.admin.dms

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.FileObject
import com.osserp.common.dms.TemplateDmsManager

import com.osserp.core.BusinessCaseTemplateManager
import com.osserp.core.dms.TemplateDocument
import com.osserp.core.finance.RecordSearch
import com.osserp.core.finance.RecordType

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class TemplateDmsView extends AbstractTemplateDmsView {
    private static Logger logger = LoggerFactory.getLogger(TemplateDmsView.class.getName())
    
    String templatePath
    String contextName
    List<RecordType> recordTypes = []
    RecordType recordType
    
    TemplateDmsView() {
        super()
        providesCreateMode()
        enableCompanyPreselection()
    }

    @Override
    public void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            // companyPreselection may have preselected users branch
            if (multipleBranchsAvailable) {
                selectedBranch = null
            }
            contextName = form.getString('context')
            reload()
        }
    }

    @Override
    boolean isCustomTemplate() {
        true
    }

    @Override
    void save() {
        if (createMode) {

            String name = form.getString('name')
            Long categoryId = form.getLong('categoryId')
            String templateName = form.getString('filename')
            if (recordMode && !categoryId) {
                throw new ClientException(ErrorCode.TYPE_REQUIRED)
            }
            if (recordMode) {
                recordType = recordTypes.find { it.id == categoryId }
                if (!recordType?.resourceKey) {
                    throw new ClientException(ErrorCode.TYPE_INVALID)
                }
                templateName = recordType.resourceKey
                name = recordType.name
            }
            Long branchId = form.getLong('branchId')
            if (branchId) {
                selectSelectedBranch(branchId)
            }
            FileObject upload = form.getUpload()
            logger.debug("save: invoked [upload=${upload?.fileName}, name=${name}, templateName=${templateName}]")
            if (!upload) {
                throw new ClientException(ErrorCode.FILE_ACCESS)
            }
            businessCaseTemplateManager.createTemplate(
                domainUser,
                contextName,
                form.getLong('requestId'),
                selectedBranch?.id,
                name,
                form.getString('description'),
                templateName,
                upload,
                categoryId)
            recordType = null
            disableCreateMode()
            reload()
        } else if (editMode) {
            super.save()
        }
    }

    boolean isFilesMode() {
        'files' == contextName
    }

    boolean isRecordMode() {
        'records' == contextName
    }

    void delete() {
        Long id = form.getLong('id')
        if (id) {
            TemplateDocument document = list.find { it.id == id }
            if (document) {
                businessCaseTemplateManager.deleteTemplate(domainUser, document)
                reload()
            }
        }
    }

    @Override
    void reload() {
        if (contextName) {

            if (recordMode && !recordTypes) {
                List<RecordType> rtypes = recordSearch.recordTypes
                rtypes.each {
                    if (it.resourceKey) {
                        recordTypes << it
                    }
                }
            }
            if (selectedBranch) {
                list = businessCaseTemplateManager.getTemplates(contextName, selectedBranch.id)
            } else {
                list = businessCaseTemplateManager.getAllTemplates(contextName)
            }
            logger.debug("reload: done [contextName=${contextName}, count=${list.size()}]")
        } 
    }

    @Override    
    boolean isDocumentDeleteSupported() {
        true
    }

    protected BusinessCaseTemplateManager getBusinessCaseTemplateManager() {
        getService(BusinessCaseTemplateManager.class.getName())
    }

    protected TemplateDmsManager getTemplateDmsManager() {
        getService(TemplateDmsManager.class.getName())
    }

    protected RecordSearch getRecordSearch() {
        getService(RecordSearch.class.getName())
    }
}
