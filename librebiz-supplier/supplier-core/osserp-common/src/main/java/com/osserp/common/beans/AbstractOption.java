/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 2 Mar 2007 11:38:23 
 * 
 */
package com.osserp.common.beans;

import java.util.Date;
import java.util.Map;

import org.jdom2.Element;

import com.osserp.common.Option;
import com.osserp.common.xml.JDOMUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractOption extends AbstractEntity implements Option {

    private String name = null;
    private String description = null;
    private String resourceKey = null;

    /**
     * Default constructor adding serialializable capability to entities
     */
    protected AbstractOption() {
        super();
    }

    /**
     * ID assigning constructor
     * @param id
     */
    protected AbstractOption(Long id) {
        super(id);
    }

    /**
     * Common constructor with id assignment
     * @param id
     * @param name
     */
    protected AbstractOption(Long id, String name) {
        super(id);
        this.name = name;
    }

    /**
     * All value constructor
     * @param id
     * @param name
     * @param endOfLife
     */
    protected AbstractOption(Long id, String name, boolean endOfLife) {
        this(id, name);
        setEndOfLife(endOfLife);
    }

    /**
     * Standard constructor. Assumes that ID is assigned by a persistence instance or not supported.
     * @param name
     */
    protected AbstractOption(String name) {
        super();
        this.name = name;
    }

    /**
     * Standard constructor with additional resource key. Assumes that ID is assigned by a persistence instance or not supported.
     * @param name
     * @param resourceKey
     */
    protected AbstractOption(String name, String resourceKey) {
        super();
        this.name = name;
        this.resourceKey = resourceKey;
    }

    /**
     * Standard constructor with additional resource key. Assumes that ID is assigned by a persistence instance or not supported.
     * @param name
     * @param description
     * @param resourceKey
     */
    protected AbstractOption(String name, String description, String resourceKey) {
        super();
        this.name = name;
        this.description = description;
        this.resourceKey = resourceKey;
    }

    /**
     * All value constructor
     * @param id
     * @param name
     * @param resourceKey
     */
    protected AbstractOption(Long id, String name, String resourceKey) {
        this(id, name);
        this.resourceKey = resourceKey;
    }

    /**
     * All value constructor
     * @param id
     * @param name
     * @param description
     * @param resourceKey
     */
    protected AbstractOption(Long id, String name, String description, String resourceKey) {
        this(id, name, resourceKey);
        this.description = description;
    }

    /**
     * All value constructor
     * @param id
     * @param name
     * @param description
     * @param resourceKey
     * @param createdBy
     */
    protected AbstractOption(Long id, String name, String description, String resourceKey, Long createdBy) {
        this(id, name, resourceKey);
        setCreatedBy(createdBy);
        this.description = description;
    }

    /**
     * All value constructor
     * @param id
     * @param reference
     * @param name
     */
    protected AbstractOption(Long id, Long reference, String name) {
        super(id, reference);
        this.name = name;
    }

    /**
     * All value constructor
     * @param id
     * @param reference
     * @param name
     * @param resourceKey
     */
    protected AbstractOption(Long id, Long reference, String name, String resourceKey) {
        super(id, reference);
        this.name = name;
        this.resourceKey = resourceKey;
    }

    /**
     * All value constructor
     * @param id
     * @param reference
     * @param name
     * @param created
     * @param createdBy
     */
    protected AbstractOption(Long id, Long reference, String name, Date created, Long createdBy) {
        super(id, reference, created, createdBy);
        this.name = name;
    }

    /**
     * All value constructor
     * @param id
     * @param reference
     * @param name
     * @param created
     * @param createdBy
     * @param changed
     * @param changedBy
     * @param endOfLife
     */
    protected AbstractOption(Long id, Long reference, String name, Date created, Long createdBy, Date changed, Long changedBy, boolean endOfLife) {
        super(id, reference, created, createdBy, changed, changedBy, endOfLife);
    }

    /**
     * All value constructor
     * @param id
     * @param reference
     * @param name
     * @param description
     * @param created
     * @param createdBy
     */
    protected AbstractOption(Long id, Long reference, String name, String description, Date created, Long createdBy) {
        super(id, reference, created, createdBy);
        this.name = name;
        this.description = description;
    }

    /**
     * Creates a new option by name
     * @param name
     * @param description
     * @param createdBy
     */
    protected AbstractOption(String name, String description, Long createdBy) {
        this(name);
        this.description = description;
        setCreated(new Date(System.currentTimeMillis()));
        setCreatedBy(createdBy);
    }

    /**
     * Creates a new option by name
     * @param name
     * @param createdBy
     */
    protected AbstractOption(String name, Long createdBy) {
        this(name, (String) null, createdBy);
    }

    /**
     * Creates a new option by name
     * @param name
     * @param reference
     * @param createdBy
     */
    protected AbstractOption(String name, Long reference, Long createdBy) {
        this(name, (String) null, createdBy);
        setReference(reference);
    }

    /**
     * Creates a new option by name
     * @param name
     * @param description
     * @param reference
     * @param createdBy
     */
    protected AbstractOption(String name, String description, Long reference, Long createdBy) {
        this(name, description, createdBy);
        setReference(reference);
    }

    /**
     * Cloning constructor, copying all values including id! You have to reset or reassign the id manually if value only copying intended.
     * @param vo
     */
    protected AbstractOption(Option vo) {
        super(vo);
        description = vo.getDescription();
        name = vo.getName();
        resourceKey = vo.getResourceKey();
    }
    
    protected AbstractOption(Element xml) {
        super(xml);
        name = JDOMUtil.fetchString(xml, "name");
        description = JDOMUtil.fetchString(xml, "description");
        resourceKey = JDOMUtil.fetchString(xml, "resourceKey");
    }

    /**
     * Mapped constructor, copying all values found including id! You have to reset or reassign the id manually if value only copying intended.
     * @param map
     */
    protected AbstractOption(Map<String, Object> map) {
        super(map);
        description = (String) map.get("description");
        name = (String) map.get("name");
        resourceKey = (String) map.get("resourceKey");
    }

    @Override
    protected Map<String, Object> getMapped() {
        Map<String, Object> map = super.getMapped();
        putIfExists(map, "description", description);
        putIfExists(map, "name", name);
        putIfExists(map, "resourceKey", resourceKey);
        return map;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getResourceKey() {
        return resourceKey;
    }

    public void setResourceKey(String resourceKey) {
        this.resourceKey = resourceKey;
    }
    
    public boolean isSelectionLabel() {
        return Long.valueOf(0).equals(getId());
    }

    @Override
    public Object clone() {
        throw new UnsupportedOperationException("missing implementation: method->clone, class="
                + getClass().getName());
    }

    @Override
    public Element getXML(String rootElementName) {
        Element root = super.getXML(rootElementName);
        root.addContent(JDOMUtil.createElement("description", description));
        root.addContent(JDOMUtil.createElement("name", name));
        root.addContent(JDOMUtil.createElement("resourceKey", resourceKey));
        return root;
    }
}
