/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 30-Jan-2007 11:40:34 
 * 
 */
package com.osserp.core.model.records;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.util.DateUtil;

import com.osserp.core.contacts.ContactPaymentAgreement;
import com.osserp.core.customers.Customer;
import com.osserp.core.finance.Amounts;
import com.osserp.core.finance.PaymentAgreement;
import com.osserp.core.finance.PaymentAgreementAwareRecord;
import com.osserp.core.finance.PaymentCondition;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordPaymentAgreement;
import com.osserp.core.model.PaymentAgreementImpl;
import com.osserp.core.suppliers.Supplier;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractRecordPaymentAgreement extends PaymentAgreementImpl
        implements RecordPaymentAgreement {
    private static Logger log = LoggerFactory.getLogger(AbstractRecordPaymentAgreement.class.getName());
    private Double pricePerUnit = null;
    private Double downpaymentAmount = null;
    private Double deliveryInvoiceAmount = null;
    private Double finalInvoiceAmount = null;
    private boolean amountsFixed = false;

    protected AbstractRecordPaymentAgreement() {
        super();
        initPercentageIfInvalid();
    }

    protected AbstractRecordPaymentAgreement(Record record) {
        super(record.getId(), record.getCreatedBy());
        if (record.getContact() instanceof Customer) {
            Customer related = (Customer) record.getContact();
            if (related.getPaymentAgreement() != null) {
                update(related.getPaymentAgreement());
                if (related.getPaymentAgreement().isTaxFree()) {
                    record.enableTaxFree(related.getPaymentAgreement().getTaxFreeId());
                }
            }
        } else if (record.getContact() instanceof Supplier) {
            Supplier related = (Supplier) record.getContact();
            if (related.getPaymentAgreement() != null) {
                update(related.getPaymentAgreement());
                if (related.getPaymentAgreement().isTaxFree()) {
                    record.enableTaxFree(related.getPaymentAgreement().getTaxFreeId());
                }
            }
        }
        initPercentageIfInvalid();
    }

    protected AbstractRecordPaymentAgreement(
            Record record,
            PaymentAgreementAwareRecord other) {

        super(record.getId(), record.getCreatedBy());
        RecordPaymentAgreement op = other.getPaymentAgreement();
        if (op != null) {
            updateCommons(
                    op.isHidePayments(),
                    op.getDownpaymentTargetId(),
                    op.getDeliveryInvoiceTargetId(),
                    op.getFinalInvoiceTargetId(),
                    op.getPaymentCondition(),
                    op.getNote(),
                    record.getCreatedBy());
            
            if (!isHidePayments()) {
                
                if (op.isAmountsFixed()) {
                    updateWithFixedAmounts(
                        record.getAmounts(),
                        op.getDownpaymentAmount(),
                        op.getDeliveryInvoiceAmount(),
                        op.getFinalInvoiceAmount());
                } else {
                    try {
                        updateWithPercentage(
                            op.getDownpaymentPercent(),
                            op.getDeliveryInvoicePercent(),
                            op.getFinalInvoicePercent());
                    } catch (ClientException ignorable) {
                        // we ignore this
                    }
                }
            }
        }
        initPercentageIfInvalid();
    }

    protected AbstractRecordPaymentAgreement(Record record, PaymentAgreement defaultPa) {
        super(record.getId(), record.getCreatedBy());

        if (log.isDebugEnabled()) {
            log.debug("<init> invoked [record="
                    + (record.getId() != null ? record.getId() : "null")
                    + ", defaultPaymentAgreement=" + (defaultPa != null ? defaultPa.getId() : "null")
                    + "]");
        }
        if (defaultPa != null) {
            updateCommons(
                    defaultPa.isHidePayments(),
                    defaultPa.getDownpaymentTargetId(),
                    defaultPa.getDeliveryInvoiceTargetId(),
                    defaultPa.getFinalInvoiceTargetId(),
                    defaultPa.getPaymentCondition(),
                    defaultPa.getNote(),
                    record.getCreatedBy());
            
            try {
                updateWithPercentage(
                        defaultPa.getDownpaymentPercent(),
                        defaultPa.getDeliveryInvoicePercent(),
                        defaultPa.getFinalInvoicePercent());
            } catch (ClientException ignorable) {
                // we ignore this
            }
        }
        initPercentageIfInvalid();
    }

    private void initPercentageIfInvalid() {
        if (deliveryInvoicePercent == null
                || downpaymentPercent == null
                || finalInvoicePercent == null) {
            
            downpaymentPercent = 0d;
            deliveryInvoicePercent = 0d;
            finalInvoicePercent = 1d;
        }
    }

    public final void update(
            Amounts amounts,
            boolean hidePayments,
            boolean amountsFixed,
            Double downpayment,
            Double deliveryInvoice,
            Double finalInvoice,
            Long downpaymentTargetId,
            Long deliveryInvoiceTargetId,
            Long finalInvoiceTargetId,
            PaymentCondition paymentCondition,
            String note,
            Long changedBy)
            throws ClientException {
        
        updateCommons(
                hidePayments,
                downpaymentTargetId,
                deliveryInvoiceTargetId,
                finalInvoiceTargetId,
                paymentCondition,
                note,
                changedBy);
        if (!hidePayments) {
            setHidePayments(false);
            if (amountsFixed) {
                updateWithFixedAmounts(amounts, downpayment, deliveryInvoice, finalInvoice);
            } else {
                updateWithPercentage(downpayment, deliveryInvoice, finalInvoice);
            }
        }
    }

    private void updateWithFixedAmounts(
            Amounts amounts,
            Double downpayment,
            Double deliveryInvoice,
            Double finalInvoice) {

        if (log.isDebugEnabled()) {
            log.debug("updateWithFixedAmounts() invoked [record=" + getReference()
                    + ", downpayment=" + downpayment
                    + ", deliveryInvoice=" + deliveryInvoice
                    + ", finalInvoice=" + finalInvoice
                    + "]");
        }

        setAmountsFixed(true);
        BigDecimal[] payments = getPayments(
                amounts.getAmount(),
                downpayment,
                deliveryInvoice,
                finalInvoice);
        downpaymentAmount = payments[0].doubleValue();
        deliveryInvoiceAmount = payments[1].doubleValue();
        finalInvoiceAmount = payments[2].doubleValue();
        downpaymentPercent = null;
        deliveryInvoicePercent = null;
        finalInvoicePercent = null;
    }

    private void updateWithPercentage(
            Double downpayment,
            Double deliveryInvoice,
            Double finalInvoice) throws ClientException {

        if (log.isDebugEnabled()) {
            log.debug("updateWithPercentage() invoked [record=" + getReference()
                    + ", downpayment=" + downpayment
                    + ", deliveryInvoice=" + deliveryInvoice
                    + ", finalInvoice=" + finalInvoice
                    + "]");
        }
        if (downpayment == null 
                && deliveryInvoice == null 
                && finalInvoice == null) {
            downpayment = deliveryInvoice = 0d;
            finalInvoice = 1d;
        }
        setAmountsFixed(false);
        Double[] steps = getPaymentSteps(
                amountsFixed,
                downpayment,
                deliveryInvoice,
                finalInvoice);
        downpaymentPercent = steps[0];
        deliveryInvoicePercent = steps[1];
        finalInvoicePercent = steps[2];
        downpaymentAmount = null;
        deliveryInvoiceAmount = null;
        finalInvoiceAmount = null;
    }

    public void updateCommons(
            boolean hidePayments,
            Long downpaymentTargetId,
            Long deliveryInvoiceTargetId,
            Long finalInvoiceTargetId,
            PaymentCondition paymentCondition,
            String note,
            Long changedBy) {
        boolean hidePaymentsChanged = (isHidePayments() != hidePayments);
        if (hidePaymentsChanged) {
            toggleHidePayments();
        }
        this.downpaymentTargetId = downpaymentTargetId;
        this.deliveryInvoiceTargetId = deliveryInvoiceTargetId;
        this.finalInvoiceTargetId = finalInvoiceTargetId;
        this.paymentCondition = paymentCondition;
        this.note = note;
        setChangedBy(changedBy);
        setChanged(DateUtil.getCurrentDate());
        if (log.isDebugEnabled()) {
            log.debug("updateCommons() done [id=" + getId()
                    + ", record=" + getReference()
                    + ", hidePayments=" + hidePayments
                    + ", hidePaymentsChanged=" + hidePaymentsChanged
                    + ", downpaymentTargetId=" + downpaymentTargetId
                    + ", deliveryInvoiceTargetId=" + deliveryInvoiceTargetId
                    + ", finalInvoiceTargetId=" + finalInvoiceTargetId
                    + ", paymentCondition="
                    + (paymentCondition == null ? "null" : paymentCondition.getId())
                    + ", note=" + note
                    + ", changedBy=" + changedBy
                    + "]");
        }
    }

    public Double getPricePerUnit() {
        return pricePerUnit;
    }

    public void setPricePerUnit(Double pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }

    public Double getDownpaymentAmount() {
        return downpaymentAmount;
    }

    public void setDownpaymentAmount(Double downpaymentAmount) {
        this.downpaymentAmount = downpaymentAmount;
    }

    public Double getDeliveryInvoiceAmount() {
        return deliveryInvoiceAmount;
    }

    public void setDeliveryInvoiceAmount(Double deliveryInvoiceAmount) {
        this.deliveryInvoiceAmount = deliveryInvoiceAmount;
    }

    public Double getFinalInvoiceAmount() {
        return finalInvoiceAmount;
    }

    public void setFinalInvoiceAmount(Double finalInvoiceAmount) {
        this.finalInvoiceAmount = finalInvoiceAmount;
    }

    public boolean isAmountsFixed() {
        return amountsFixed;
    }

    public void setAmountsFixed(boolean amountsFixed) {
        this.amountsFixed = amountsFixed;
    }

    public void toggleHidePayments() {
        setHidePayments(!isHidePayments());
        setDownpaymentPercent(0d);
        setDeliveryInvoicePercent(0d);
        setFinalInvoicePercent(1d);
        setDownpaymentTargetId(0L);
        setDeliveryInvoiceTargetId(0L);
        setFinalInvoiceTargetId(0L);
        setAmountsFixed(false);
    }

    protected Double[] getPaymentSteps(boolean amountsFixed, Double p1, Double p2, Double p3)
            throws ClientException {
        if ((p1 == null || p1 == 0)
                && (p2 == null || p2 == 0)
                && (getPaymentStep(p3) != 100)) {
            if (!amountsFixed) {
                if (log.isDebugEnabled()) {
                    log.debug("getPaymentSteps() failed by missing payment steps [p1="
                            + p1 + ", p2=" + p2 + ", p3=" + p3 + "]");
                }
                throw new ClientException(ErrorCode.PAYMENT_STEPS_MISSING);
            }
        }
        Double[] result = new Double[3];
        double ps1 = getPaymentStep(p1);
        double ps2 = getPaymentStep(p2);
        double ps3 = getPaymentStep(p3);
        if (ps3 == 0) {
            ps3 = 100 - (ps1 + ps2);
        }
        if ((ps1 + ps2 + ps3) != 100) {
            if (log.isDebugEnabled()) {
                log.debug("getPaymentSteps() failed by invalid payment steps [ps1="
                        + ps1 + ", ps2=" + ps2 + ", ps3=" + ps3 + "]");
            }
            throw new ClientException(ErrorCode.PAYMENT_STEPS_INVALID);
        }
        result[0] = p1;
        result[1] = p2;
        result[2] = ps3 / 100;
        return result;
    }

    protected BigDecimal[] getPayments(BigDecimal price, Double p1, Double p2, Double p3) {
        BigDecimal[] result = new BigDecimal[3];
        BigDecimal p = getValue(price);
        BigDecimal dp1 = getValue(p1);
        BigDecimal dp2 = getValue(p2);
        BigDecimal dp3 = getValue(p3);
        if (dp3.doubleValue() == 0) {
            dp3 = p.subtract(dp1.add(dp2));
        }
        result[0] = getValue(p1);
        result[1] = getValue(p2);
        result[2] = getValue(dp3);
        return result;
    }

    private double getPaymentStep(Double ps) {
        if (ps == null) {
            return 0;
        }
        return Math.round(ps.doubleValue() * 100);
    }

    private BigDecimal getValue(BigDecimal payment) {
        if (payment == null) {
            return new BigDecimal(0);
        }
        return payment;
    }

    private BigDecimal getValue(Double payment) {
        if (payment == null) {
            return new BigDecimal(0);
        }
        return new BigDecimal(payment);
    }

    private void update(ContactPaymentAgreement other) {
        setHidePayments(other.isHidePayments());
        setDeliveryInvoicePercent(other.getDeliveryInvoicePercent());
        setDeliveryInvoiceTargetId(other.getDeliveryInvoiceTargetId());
        setDownpaymentPercent(other.getDownpaymentPercent());
        setDownpaymentTargetId(other.getDownpaymentTargetId());
        setFinalInvoicePercent(other.getFinalInvoicePercent());
        setFinalInvoiceTargetId(other.getFinalInvoiceTargetId());
        if (other.getPaymentCondition() != null) {
            paymentCondition = other.getPaymentCondition();
        }
    }
}
