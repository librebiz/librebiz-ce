/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 22, 2013 03:57:18 PM 
 * 
 */
package com.osserp.core.service.records;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.mail.Mail;
import com.osserp.common.service.ResourceLocator;

import com.osserp.core.BusinessNote;
import com.osserp.core.NoteAware;
import com.osserp.core.dao.BusinessNotes;
import com.osserp.core.dao.Employees;
import com.osserp.core.dao.MailTemplates;
import com.osserp.core.dao.records.PurchaseOrders;
import com.osserp.core.employees.Employee;
import com.osserp.core.mail.MailMessageContext;
import com.osserp.core.mail.MailTemplate;
import com.osserp.core.model.NoteAwareVO;
import com.osserp.core.purchasing.PurchaseOrder;
import com.osserp.core.purchasing.PurchaseOrderNoteManager;
import com.osserp.core.service.impl.AbstractNoteManager;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PurchaseOrderNoteManagerImpl extends AbstractNoteManager implements PurchaseOrderNoteManager {
    private PurchaseOrders purchaseOrders;
    private ResourceLocator resourceLocator;

    public PurchaseOrderNoteManagerImpl(
            BusinessNotes businessNotes,
            MailTemplates mailTemplates,
            Employees employees,
            PurchaseOrders purchaseOrders,
            ResourceLocator resourceLocator) {
        super(businessNotes, mailTemplates, employees);
        this.purchaseOrders = purchaseOrders;
        this.resourceLocator = resourceLocator;
    }

    @Override
    public NoteAware load(Long primaryKey) {
        return create(getPurchaseOrder(primaryKey));
    }

    @Override
    protected String getTypeName() {
        return "purchaseOrderNote";
    }

    public MailMessageContext setupMail(Employee user, NoteAware noteAware, Map<String, String> opts) throws ClientException {
        MailTemplate mtpl = getMailTemplate();
        String originator = getOriginator(user, mtpl);
        StringBuilder subject = new StringBuilder();
        PurchaseOrder order = getPurchaseOrder(noteAware.getPrimaryKey());
        subject.append(getResourceString(order));
        subject.append(" ");
        if (order.getReference() == null) {
            subject.append(order.getNumber());
        } else {
            subject.append(order.getReferenceNumber());
        }
        subject.append(" - ").append(order.getName());
        Mail mail = new Mail(originator, subject.toString(), opts.get("message"));
        return new MailMessageContext(mtpl, mail, opts);
    }

    private NoteAware create(PurchaseOrder order) {
        List<BusinessNote> notes = getBusinessNotes().findByReference(getTypeName(), order.getId());
        return new NoteAwareVO(order.getId(), order.getContact().getName(), notes);
    }

    private PurchaseOrder getPurchaseOrder(Long id) {
        return (PurchaseOrder) purchaseOrders.load(id);
    }

    private String getResourceString(PurchaseOrder order) {
        String lang = order.getLanguage();
        if (lang == null) {
            lang = Constants.DEFAULT_LANGUAGE;
        }
        String result = "Order";
        if (resourceLocator != null) {
            ResourceBundle bundle = resourceLocator.getResourceBundle(new Locale(lang));
            if (bundle.containsKey("purchaseOrder")) {
                result = bundle.getString("purchaseOrder");
            }
        }
        return result;
    }
}
