<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="table-responsive table-responsive-default">
	<table class="table">
		<thead>
			<tr>
				<th class="recordType"><fmt:message key="type" /></th>
				<th class="recordNumber"><fmt:message key="number" /></th>
				<th class="recordDate"><v:sortLink key="created"><fmt:message key="ofDate"/></v:sortLink></th>
				<th class="recordNumber"><fmt:message key="referenceNumberShort" /></th>
				<th class="recordAmount"><fmt:message key="amount" /></th>
				<th class="recordVat"><fmt:message key="turnoverTax1" /></th>
				<th class="recordVat"><fmt:message key="turnoverTax2" /></th>
				<th class="recordAmount"><fmt:message key="total" /></th>
				<th class="recordAction"> </th>
			</tr>
		</thead>
		<tbody>
			<c:choose>
				<c:when test="${empty view.list}">
           			<tr>
               			<td colspan="9">
                   			<span><fmt:message key="noRecordsAvailable" /></span> 
                   			<span style="margin-left: 20px;"> 
                   				<v:link url="/customers/customerSalesRecords/enableCreateMode">[<fmt:message key="create" />]</v:link>
                   			</span>
               			</td>
           			</tr>
				</c:when>
				<c:otherwise>
					<c:forEach var="record" items="${view.list}" varStatus="s">
						<tr<c:if test="${!record.payment}"> id="record${record.id}"</c:if><c:if test="${record.payment}"> id="payment${record.id}" class="altrow"</c:if>>
							<c:choose>
								<c:when test="${record.canceled}">
									<td class="recordType canceled"><o:out value="${record.type.numberPrefix}"/></td>
                                    <td class="recordNumber canceled">
    									<c:choose>
                                            <c:when test="${record.downpayment}">
                                                <a href="<c:url value="/salesInvoice.do?method=displayDownpayment&exit=customer&readonly=true&id=${record.id}&exitId=record${record.id}"/>"><o:out value="${record.number}" /></a>
                                            </c:when>
                                            <c:when test="${record.invoice}">
                                                <a href="<c:url value="/salesInvoice.do?method=display&exit=customer&readonly=true&id=${record.id}&exitId=record${record.id}&target=/customers/customerSalesRecords/reload"/>"><o:out value="${record.number}" /></a>
                                            </c:when>
							     			<c:when test="${record.creditNote}">
												<a href="<c:url value="/salesCreditNote.do?method=display&exit=customer&readonly=true&id=${record.id}&exitId=record${record.id}"/>"><o:out value="${record.number}" /></a>
				            				</c:when>
			     							<c:when test="${record.cancellation}">
												<a href="<c:url value="/salesCancellation.do?method=display&exit=customer&readonly=true&id=${record.id}&exitId=record${record.id}"/>"><o:out value="${record.number}" /></a>
	       									</c:when>
    									</c:choose>
                                    </td>
									<td class="recordDate canceled"><o:date value="${record.created}" /></td>
									<td class="recordNumber canceled">
                                        <c:choose>
                                            <c:when test="${!empty record.reference and (record.cancellation or record.creditNote)}">
                                                <a href="<c:url value="/salesInvoice.do?method=display&exit=customer&readonly=true&id=${record.reference}&target=/customers/customerSalesRecords/reload"/>">
                                                    <o:out value="${record.referenceNumber}" />
                                                </a>
                                            </c:when>
                                            <c:when test="${!empty record.reference}">
                                                <o:out value="${record.referenceNumber}" />
                                            </c:when>
                                        </c:choose>
                                    </td>
									<td class="recordAmount canceled"><o:number value="${record.amount}" format="currency" /></td>
									<c:choose>
										<c:when test="${record.taxFree}">
											<td class="recordVat canceled"><o:number value="0.00" format="currency" /></td>
											<td class="recordVat canceled"><o:number value="0.00" format="currency" /></td>
											<td class="recordAmount canceled"><o:number value="${record.amount}" format="currency" /></td>
										</c:when>
										<c:otherwise>
											<td class="recordVat canceled"><o:number value="${record.tax}" format="currency" /></td>
											<td class="recordVat canceled"><o:number value="${record.reducedTax}" format="currency" /></td>
											<td class="recordAmount canceled"><o:number value="${record.gross}" format="currency" /></td>
										</c:otherwise>
									</c:choose>
									<td class="recordAction">
									   <c:if test="${record.unchangeable}">
                                            <c:choose>
                                                <c:when test="${record.downpayment}">
                                                    <v:link url="/records/recordPrint/print?name=salesDownpayment&id=${record.id}" title="documentPrint"><o:img name="printIcon" /></v:link>
                                                </c:when>
                                                <c:when test="${record.invoice}">
                                                    <v:link url="/records/recordPrint/print?name=salesInvoice&id=${record.id}" title="documentPrint"><o:img name="printIcon" /></v:link>
                                                </c:when>
                                                <c:when test="${record.creditNote}">
                                                    <v:link url="/records/recordPrint/print?name=salesCreditNote&id=${record.id}" title="documentPrint"><o:img name="printIcon" /></v:link>
                                                </c:when>
                                            </c:choose>
                                        </c:if>
								    </td>
								</c:when>
								<c:when test="${record.payment}">
									<td class="recordType"><o:out value="${record.type.numberPrefix}"/></td>
									<td class="recordNumber">
                                        <c:choose>
                                            <c:when test="${record.downpaymentPayment}">
                                                <a href="<c:url value="/salesInvoice.do?method=displayDownpayment&exit=customer&readonly=true&id=${record.reference}&exitId=payment${record.id}"/>"><o:out value="${record.referenceNumber}" /></a>
                                            </c:when>
                                            <c:when test="${record.outpayment}">
                                                <a href="<c:url value="/salesCreditNote.do?method=display&exit=customer&readonly=true&id=${record.reference}&exitId=payment${record.id}"/>"><o:out value="${record.referenceNumber}" /></a>
                                            </c:when>
                                            <c:otherwise>
                                                <a href="<c:url value="/salesInvoice.do?method=display&exit=customer&readonly=true&id=${record.reference}&exitId=payment${record.id}&target=/customers/customerSalesRecords/reload"/>"><o:out value="${record.referenceNumber}" /></a>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
									<td class="recordDate"><o:date value="${record.created}" /></td>
									<td colspan="4">&nbsp;</td>
									<td class="recordAmount"><o:number value="${record.gross}" format="currency" /></td>
                                    <td class="recordAction"> </td>
								</c:when>
								<c:otherwise>
									<td class="recordType"><o:out value="${record.type.numberPrefix}"/></td>
									<c:choose>
										<c:when test="${record.unchangeable}">
											<c:choose>
                                                <c:when test="${record.downpayment}">
                                                    <td class="recordNumber">
                                                        <a href="<c:url value="/salesInvoice.do?method=displayDownpayment&exit=customer&readonly=true&id=${record.id}&exitId=record${record.id}&target=/customers/customerSalesRecords/reload"/>"><o:out value="${record.number}" /></a>
                                                    </td>
                                                </c:when>
                                                <c:when test="${record.invoice}">
                                                    <td class="recordNumber">
                                                        <a href="<c:url value="/salesInvoice.do?method=display&exit=customer&readonly=true&id=${record.id}&exitId=record${record.id}&target=/customers/customerSalesRecords/reload"/>"><o:out value="${record.number}" /></a>
                                                    </td>
                                                </c:when>
												<c:when test="${record.creditNote}">
													<td class="recordNumber">
                                                        <a href="<c:url value="/salesCreditNote.do?method=display&exit=customer&readonly=true&id=${record.id}&exitId=record${record.id}&target=/customers/customerSalesRecords/reload"/>"><o:out value="${record.number}" /></a>
													</td>
												</c:when>
												<c:when test="${record.cancellation}">
													<td class="recordNumber">
                                                        <a href="<c:url value="/salesCancellation.do?method=display&exit=customer&readonly=true&id=${record.id}&exitId=record${record.id}"/>"><o:out value="${record.number}" /></a>
													</td>
												</c:when>
											</c:choose>
										</c:when>
										<c:otherwise>
											<c:choose>
                                                <c:when test="${record.downpayment}">
                                                    <td class="recordNumber">
                                                        <a href="<c:url value="/salesInvoice.do?method=displayDownpayment&exit=customer&readonly=false&id=${record.id}&exitId=record${record.id}&target=/customers/customerSalesRecords/reload"/>"><o:out value="${record.number}" /></a>
                                                    </td>
                                                </c:when>
                                                <c:when test="${record.invoice}">
                                                    <td class="recordNumber">
                                                        <a href="<c:url value="/salesInvoice.do?method=display&exit=customer&readonly=false&id=${record.id}&exitId=record${record.id}&target=/customers/customerSalesRecords/reload"/>"><o:out value="${record.number}" /></a>
                                                    </td>
                                                </c:when>
                                                <c:when test="${record.creditNote}">
                                                    <td class="recordNumber">
                                                        <a href="<c:url value="/salesCreditNote.do?method=display&exit=customer&readonly=false&id=${record.id}&exitId=record${record.id}&target=/customers/customerSalesRecords/reload"/>"><o:out value="${record.number}" /></a>
                                                    </td>
                                                </c:when>
                                                <c:when test="${record.cancellation}">
                                                    <td class="recordNumber">
                                                        <a href="<c:url value="/salesCancellation.do?method=display&exit=customer&readonly=false&id=${record.id}&exitId=record${record.id}"/>"><o:out value="${record.number}" /></a>
                                                    </td>
                                                </c:when>
											</c:choose>
										</c:otherwise>
									</c:choose>
									<td class="recordDate"><o:date value="${record.created}" /></td>
									<td class="recordNumber">
                                        <c:choose>
                                            <c:when test="${!empty record.reference and (record.cancellation or record.creditNote)}">
                                                <a href="<c:url value="/salesInvoice.do?method=display&exit=customer&readonly=true&id=${record.reference}&target=/customers/customerSalesRecords/reload"/>">
                                                    <o:out value="${record.referenceNumber}" />
                                                </a>
                                            </c:when>
                                            <c:when test="${!empty record.reference}">
                                                <o:out value="${record.referenceNumber}" />
                                            </c:when>
                                        </c:choose>
                                    </td>
									<td class="recordAmount"><o:number value="${record.amount}" format="currency" /></td>
									<c:choose>
										<c:when test="${record.taxFree}">
											<td class="recordVat"><o:number value="0.00" format="currency" /></td>
											<td class="recordVat"><o:number value="0.00" format="currency" /></td>
                                            <c:choose>
                                                <c:when test="${record.amountOpen}">
                                                    <td class="recordAmount error"><o:number value="${record.amount}" format="currency" /></td>
                                                </c:when>
                                                <c:otherwise>
                                                    <td class="recordAmount"><o:number value="${record.amount}" format="currency" /></td>
                                                </c:otherwise>
                                            </c:choose>
										</c:when>
										<c:otherwise>
											<td class="recordVat"><o:number value="${record.tax}" format="currency" /></td>
											<td class="recordVat"><o:number value="${record.reducedTax}" format="currency" /></td>
                                            <c:choose>
                                                <c:when test="${record.amountOpen}">
                                                    <td class="recordAmount error"><o:number value="${record.gross}" format="currency" /></td>
                                                </c:when>
                                                <c:otherwise>
                                                    <td class="recordAmount"><o:number value="${record.gross}" format="currency" /></td>
                                                </c:otherwise>
                                            </c:choose>
										</c:otherwise>
									</c:choose>
									<td class="recordAction">
									   <c:if test="${record.unchangeable}">
                                            <c:choose>
                                                <c:when test="${record.downpayment}">
                                                    <v:link url="/records/recordPrint/print?name=salesDownpayment&id=${record.id}" title="documentPrint"><o:img name="printIcon" /></v:link>
                                                </c:when>
                                                <c:when test="${record.invoice}">
                                                    <v:link url="/records/recordPrint/print?name=salesInvoice&id=${record.id}" title="documentPrint"><o:img name="printIcon" /></v:link>
                                                </c:when>
                                                <c:when test="${record.creditNote}">
                                                    <v:link url="/records/recordPrint/print?name=salesCreditNote&id=${record.id}" title="documentPrint"><o:img name="printIcon" /></v:link>
                                                </c:when>
                                                <c:when test="${record.cancellation}">
                                                    <v:link url="/records/recordPrint/print?name=salesCancellation&id=${record.id}" title="documentPrint"><o:img name="printIcon" /></v:link>
                                                </c:when>
                                            </c:choose>
									   </c:if>
								    </td>
								</c:otherwise>
							</c:choose>
						</tr>
					</c:forEach>
					<tr>
						<th colspan="7"><fmt:message key="balanceWithoutDownpayments" /></th>
						<th class="recordAmount"><o:number value="${view.accountBalance}" format="currency"/></th>
						<th class="recordAction"> </th>
					</tr>
				</c:otherwise>
			</c:choose>
		</tbody>
	</table>
</div>
