/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Nov 1, 2010 10:58:40 AM 
 * 
 */
package com.osserp.gui.reporting.selectors

import javax.servlet.http.HttpServletRequest

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.common.ClientException
import com.osserp.groovy.web.PopupViewController

/**
 *
 * @author jg <jg@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
@RequestMapping("/reporting/selectors/selectStatusPopup/*")
@Controller class SelectStatusPopupController extends PopupViewController {
    private static Logger logger = LoggerFactory.getLogger(SelectStatusPopupController.class.getName())

    @RequestMapping
    @Override
    def save(HttpServletRequest request) {
        logger.debug("save: invoked [user=${getUserId(request)}]")
        SelectStatusPopupView view = getView(request)
        try {
            view.save()
        } catch (ClientException e) {
            if (logger.debugEnabled) {
                logger.debug("save: caught exception [message=${e.message}]")
            }
            request.getSession().setAttribute('error', e.message)
            return defaultPage
        }
        reloadParent
    }
}
