/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 3, 2005 
 * 
 */
package com.osserp.gui.web.sales;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ActionException;
import com.osserp.common.PermissionException;
import com.osserp.common.User;
import com.osserp.common.web.RequestUtil;
import com.osserp.gui.BusinessCaseView;
import com.osserp.gui.client.sales.SalesUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesLoadForwarder extends AbstractSalesForwarder {
    private static Logger log = LoggerFactory.getLogger(SalesLoadForwarder.class.getName());

    /**
     * Loads a sale from persistent storage
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    @Override
    public ActionForward execute(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            User user = getUser(session);
            log.debug("execute() invoked [user="
                    + user.getId()
                    + ", session="
                    + user.getSessionId()
                    + ", locale="
                    + (user.getLocale() == null ? "null" : user.getLocale().toString())
                    + "]");
        }
        Long salesId = RequestUtil.getId(request);
        String exit = RequestUtil.getExit(request);
        if (exit == null) {
            exit = RequestUtil.getTarget(request);
        }
        try {
            BusinessCaseView view = createBusinessCaseView(request, salesId, exit);
            if (log.isDebugEnabled()) {
                log.debug("execute() done [sales=" + view.getId() + "]");
            }
        } catch (ActionException e) {
            return saveErrorAndReturn(request, e.getMessage());
        } catch (PermissionException e) {
            return saveErrorAndReturn(request, e.getMessage());
        }
        try {
            SalesUtil.removeBillingView(request.getSession());
        } catch (Throwable t) {
            // we ignore this
        }
        return super.execute(mapping, form, request, response);
    }
}
