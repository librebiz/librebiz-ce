/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 30-Oct-2006 18:13:50 
 * 
 */
package com.osserp.core.model.projects;

import java.util.Date;

import org.jdom2.Element;

import com.osserp.common.xml.JDOMUtil;
import com.osserp.core.model.SalesImpl;
import com.osserp.core.projects.Project;
import com.osserp.core.requests.Request;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProjectImpl extends SalesImpl implements Project {

    private Long installerId = null;
    private Date startUp = null;
    private int installationDays;
    private Date installationDate = null;

    protected ProjectImpl() {
        super();
    }

    public ProjectImpl(Long id, Long createdBy, Request request) {
        super(id, createdBy, request);
    }

    public Long getInstallerId() {
        return installerId;
    }

    public void setInstallerId(Long installerId) {
        this.installerId = installerId;
    }

    public Date getStartUp() {
        return startUp;
    }

    public void setStartUp(Date startUp) {
        this.startUp = startUp;
    }

    public int getInstallationDays() {
        return installationDays;
    }

    public void setInstallationDays(int installationDays) {
        this.installationDays = installationDays;
    }

    public Date getInstallationDate() {
        return installationDate;
    }

    public void setInstallationDate(Date installationDate) {
        this.installationDate = installationDate;
    }

    public void updateInstallationDate(Date installationDate, int installationDays) {
        this.installationDate = installationDate;
        this.installationDays = installationDays;
    }

    @Override
    public Element getXML() {
        return getXML("root");
    }

    @Override
    public Element getXML(String rootElementName) {
        Element root = super.getXML(rootElementName);
        root.addContent(JDOMUtil.createElement("installerId", installerId));
        root.addContent(JDOMUtil.createElement("startUp", startUp));
        root.addContent(JDOMUtil.createElement("installationDays", installationDays));
        root.addContent(JDOMUtil.createElement("installationDate", installationDate));
        return root;
    }
}
