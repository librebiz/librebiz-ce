/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 22, 2009 10:14:07 AM 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.hibernate.SessionFactory;

import com.osserp.common.Constants;
import com.osserp.common.dao.AbstractHibernateDao;

import com.osserp.core.BusinessType;
import com.osserp.core.dao.DefaultPaymentAgreements;
import com.osserp.core.dao.PaymentConditions;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.PaymentAgreement;
import com.osserp.core.finance.PaymentCondition;
import com.osserp.core.model.DefaultPaymentAgreementImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class DefaultPaymentAgreementsDao extends AbstractHibernateDao implements DefaultPaymentAgreements {
    private static Logger log = LoggerFactory.getLogger(DeliveryConditionsDao.class.getName());
    private PaymentConditions conditions = null;

    public DefaultPaymentAgreementsDao(SessionFactory sessionFactory, PaymentConditions conditions) {
        super(sessionFactory);
        this.conditions = conditions;
    }

    public PaymentAgreement createDefaultPaymentAgreement(Employee user, BusinessType businessType) {
        DefaultPaymentAgreementImpl obj = new DefaultPaymentAgreementImpl(
                user.getId(), businessType, fetchDefaultCondition(businessType));
        save(obj);
        return obj;
    }

    public PaymentAgreement load(Long id) {
        if (id != null) {
            try {
                PaymentAgreement pa = (PaymentAgreement) getCurrentSession().load(
                        DefaultPaymentAgreementImpl.class, id);
                return pa;
            } catch (Throwable t) {
                if (log.isInfoEnabled()) {
                    log.debug("load() failed [id=" + id + ", message="
                            + t.getMessage() + "]");
                }
            }
        }
        return null;
    }

    public List<PaymentAgreement> findAll() {
        return getCurrentSession().createQuery("from " + DefaultPaymentAgreementImpl.class.getName()).list();
    }

    public PaymentAgreement find(BusinessType businessType) {
        PaymentAgreement result = fetchDefault(businessType);
        if (result == null) {
            PaymentCondition defaultCondition = 
                    conditions.getPaymentConditionDefault(businessType.isSales());
            result = new DefaultPaymentAgreementImpl(
                    Constants.SYSTEM_EMPLOYEE, 
                    businessType, 
                    defaultCondition);
            save(result);
        }
        return result;
    }

    private PaymentAgreement fetchDefault(BusinessType businessType) {
        StringBuilder hql = new StringBuilder();
        hql
                .append("from ")
                .append(DefaultPaymentAgreementImpl.class.getName())
                .append(" o where o.reference = :businessTypeId");
        List<PaymentAgreement> list = getCurrentSession().createQuery(
                hql.toString()).setParameter("businessTypeId", businessType.getId()).list();
        return (list.isEmpty() ? null : list.get(0));
    }

    public void save(PaymentAgreement obj) {
        getCurrentSession().saveOrUpdate(DefaultPaymentAgreementImpl.class.getName(), obj);
    }

    private PaymentCondition fetchDefaultCondition(BusinessType type) {
        PaymentCondition result = null;
        List<PaymentCondition> all = conditions.getPaymentConditions();
        for (int i = 0, j = all.size(); i < j; i++) {
            PaymentCondition next = all.get(i);
            if (next.isDefaultCondition()) {
                result = next;
                if (next.getLanguage() != null && next.getLanguage().equals(type.getLanguage())) {
                    break;
                }
            }
        }
        return result;
    }
}
