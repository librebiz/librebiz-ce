/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 5, 2006 7:20:32 AM 
 * 
 */
package com.osserp.common.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.commons.beanutils.PropertyUtils;

import com.osserp.common.Entity;
import com.osserp.common.PersistentObject;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@SuppressWarnings("unchecked")
public class CollectionUtil {

    private static Logger log = LoggerFactory.getLogger(CollectionUtil.class.getName());

    public static final String DEFAULT_PK_PROPERTY = "id";

    /**
     * Creates a set of objects
     * @param array
     * @return array objects
     */
    public static Set createSet(Object[] array) {
        Set result = new HashSet();
        if (array != null && array.length > 0) {
            for (int i = 0, j = array.length; i < j; i++) {
                result.add(array[i]);
            }
        }
        return result;
    }

    /**
     * Creates a map of entities. List content must be an instance of Entity, objects will be mapped by entity id.
     * @param list
     * @return map
     */
    public static Map createEntityMap(List list) {
        Map result = new HashMap<>();
        if (list != null && !list.isEmpty()) {
            for (int i = 0, j = list.size(); i < j; i++) {
                Object next = list.get(i);
                if (next instanceof Entity) {
                    result.put(((Entity) next).getId(), next);
                }
            }
        }
        return result;
    }

    /**
     * Indicates that value is member of an array
     * @param array
     * @param value
     * @return true if array contains given value
     */
    public static boolean contains(Long[] array, Long value) {
        if (array != null) {
            for (int i = 0, j = array.length; i < j; i++) {
                if (array[i].equals(value)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Tries to find the object with given property equals the specified value from given collection
     * @param col where we lookup for an object containing equal property
     * @param id
     * @return matching collection member or null if no such object exists
     */
    public static Object getById(Collection col, Integer id) {
        return getById(col, Long.valueOf(id.longValue()));
    }

    /**
     * Tries to find the object with given property equals the specified value from given collection
     * @param col where we lookup for an object containing equal property
     * @param id
     * @return matching collection member or null if no such object exists
     */
    public static Object getById(Collection col, Long id) {
        return getByProperty(col, "id", id);
    }

    /**
     * Tries to find the object with given property equals the specified value from given collection
     * @param col where we lookup for an object containing equal property
     * @param property name
     * @param value that should match
     * @return matching collection member or null if no such object exists
     */
    public static Object getByProperty(Collection col, String property, Object value) {
        if (value != null) {
            for (Iterator i = col.iterator(); i.hasNext();) {
                Object obj = i.next();
                try {
                    Object prop = PropertyUtils.getProperty(obj, property);
                    if (value.equals(prop)) {
                        return obj;
                    }
                } catch (Exception e) {
                    log.warn("getByProperty() ignoring exception [property=" + property
                            + ", value=" + value + ", errorMessage=" + e.toString() + "]");
                }
            }
        }
        return null;
    }

    /**
     * Tries to replace the object where given property equals the specified value from given collection
     * @param col where we lookup for an object containing equal property
     * @param property name
     * @param bean value that should match
     */
    public static void replaceByProperty(Collection<Object> col, String property, Object bean) {
        Object currentId = getPrimaryKey(bean, property);
        List<Object> list = new ArrayList(col);
        for (int i = 0, j = list.size(); i < j; i++) {
            Object obj = list.get(i);
            try {
                Object prop = PropertyUtils.getProperty(obj, property);
                if (currentId.equals(prop)) {
                    list.set(i, bean);
                    if (log.isDebugEnabled()) {
                        log.debug("replaceByProperty() done");
                    }
                    break;
                }
            } catch (Exception e) {
                break;
            }
        }
    }

    /**
     * Tries to find the object with given property equals the specified value from given collection
     * @param col where we lookup for an object containing equal property
     * @param property name
     * @param bean value that should match
     */
    public static void removeByProperty(Collection col, String property, Object bean) {
        Object currentId = getPrimaryKey(bean, property);
        if (currentId != null) {
            for (Iterator i = col.iterator(); i.hasNext();) {
                Object obj = i.next();
                try {
                    Object prop = PropertyUtils.getProperty(obj, property);
                    if (currentId.equals(prop)) {
                        i.remove();
                        if (log.isDebugEnabled()) {
                            log.debug("removeByProperty() done [property=" + prop
                                    + ", id=" + currentId + "]");
                        }
                        break;
                    }
                } catch (Exception e) {
                    break;
                }
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("removeByProperty() done, list size is " + col.size());
        }
    }

    public static List<? extends Entity> removeDups(List<? extends Entity> items) {
        Set<Long> added = new HashSet();
        for (Iterator<? extends Entity> iter = items.iterator(); iter.hasNext();) {
            Entity next = iter.next();
            if (added.contains(next.getPrimaryKey())) {
                iter.remove();
            } else {
                added.add(next.getPrimaryKey());
            }
        }
        return items;
    }

    /**
     * Reverse a list
     * @param list
     * @return reversed list
     */
    public static List reverse(List list) {
        List result = new ArrayList<>();
        for (int i = 0, j = list.size(); i < j; i++) {
            Object obj = list.get(i);
            result.add(0, obj);
        }
        return result;
    }

    /**
     * Sorts list by default comparable
     * @param list
     * @return sorted list
     */
    public static List sort(List list) {
        Collections.sort(list);
        return list;
    }

    /**
     * Sorts list by comparator
     * @param list
     * @param comparator
     * @return sorted list
     */
    public static List sort(List list, Comparator comparator) {
        Collections.sort(list, comparator);
        return list;
    }

    /**
     * Sorts list by property name
     * @param list
     * @param property
     * @return sorted list
     */
    public static List sort(List list, String property, boolean reverse) {
        Collections.sort(list, new CommonComparator(property));
        return reverse ? reverse(list) : list;
    }

    /**
     * Moves an entity to next list position
     * @param list of objects with primary key as property 'id'
     * @param obj with primary key as property 'id'
     */
    public static void moveNext(List list, Object obj) {
        moveNext(list, obj, DEFAULT_PK_PROPERTY);
    }

    /**
     * Moves an entity to next list position
     * @param list
     * @param obj
     * @param primaryKey name of the primary key property
     */
    public static void moveNext(List list, Object obj, String primaryKey) {
        if (list.size() > 1) {
            int idx = -1;
            if (log.isDebugEnabled()) {
                log.debug("moveNext() invoked [entities=" + createEntityIds(list, primaryKey) + "]");
            }
            Long objId = getPrimaryKeyId(obj, primaryKey);
            if (objId != null) {
                for (Iterator i = list.iterator(); i.hasNext();) {
                    idx++;
                    Object next = i.next();
                    Long pk = getPrimaryKeyId(next, primaryKey);
                    if (pk != null && pk.equals(objId)) {
                        if (idx + 1 < list.size()) {
                            i.remove();
                            list.add(idx + 1, next);
                            break;
                        }
                    }
                }
            }
            if (log.isDebugEnabled()) {
                log.debug("moveNext() done [entities=" + createEntityIds(list, primaryKey) + "]");
            }
        }
    }

    /**
     * Moves an entity with primary key property 'id' to previous list position
     * @param list of objects with primary key as property 'id'
     * @param obj with primary key as property 'id'
     */
    public static void movePrevious(List list, Object obj) {
        movePrevious(list, obj, DEFAULT_PK_PROPERTY);
    }

    /**
     * Moves an entity to previous list position
     * @param list
     * @param obj
     * @param primaryKey name of the primary key property
     */
    public static void movePrevious(List list, Object obj, String primaryKey) {
        int idx = -1;
        if (log.isDebugEnabled()) {
            log.debug("movePrevious() invoked [entities=" + createEntityIds(list, primaryKey) + "]");
        }
        Long objId = getPrimaryKeyId(obj, primaryKey);

        if (objId != null) {
            for (Iterator i = list.iterator(); i.hasNext();) {
                idx++;
                Object next = i.next();
                Long pk = getPrimaryKeyId(next, primaryKey);
                if (pk != null && pk.equals(objId)) {
                    if (idx > 0) {
                        i.remove();
                        list.add(idx - 1, next);
                        break;
                    }
                }
            }
            if (log.isDebugEnabled()) {
                log.debug("movePrevious() done [entities="
                        + createEntityIds(list, primaryKey) + "]");
            }
        }
    }

    /**
     * Creates a comma separated list if all entity ids
     * @param list
     * @param primaryKey name of the primary key property
     * @return entityIds
     */
    public static String createEntityIds(List list, String primaryKey) {
        StringBuilder b = new StringBuilder();
        for (int i = 0, j = list.size(); i < j; i++) {
            Object next = list.get(i);
            b.append(getPrimaryKey(next, primaryKey));
            if (i < (list.size() - 1)) {
                b.append(",");
            }
        }
        return b.toString();
    }

    /**
     * Creates a comma separated list if all entity ids
     * @param list of objects with primary key as property 'id'
     * @return entityIds
     */
    public static String createEntityIds(List list) {
        return createEntityIds(list, DEFAULT_PK_PROPERTY);
    }

    public static String[] toArray(Set<String> set) {
        return set.toArray(new String[0]);
    }

    /**
     * Creates a copy of the given map ignoring all provided keys
     * @param src
     * @param ignoreKeys
     * @return copy of src without elements from ignoreKeys
     */
    public static Map copy(Map src, List ignoreKeys) {
        Map result = new HashMap<>();
        for (Iterator i = src.keySet().iterator(); i.hasNext();) {
            Object key = i.next();
            if (!inList(ignoreKeys, key)) {
                result.put(key, src.get(key));
            }
        }
        return result;
    }

    private static boolean inList(List list, Object obj) {
        if (list != null && obj != null) {
            for (int i = 0, j = list.size(); i < j; i++) {
                Object next = list.get(i);
                if (next.equals(obj)) {
                    return true;
                }
            }
        }
        return false;
    }

    private static Object getPrimaryKey(Object obj, String property) {
        if (obj instanceof Long) {
            // obj is primary key itself
            return obj;
        }
        if (obj instanceof PersistentObject) {
            return ((PersistentObject) obj).getPrimaryKey();
        }
        try {
            return PropertyUtils.getProperty(obj, property);
        } catch (Throwable t) {
            return null;
        }
    }

    private static Long getPrimaryKeyId(Object obj, String property) {
        try {
            return (Long) getPrimaryKey(obj, property);
        } catch (Throwable t) {
            return null;
        }
    }

}
