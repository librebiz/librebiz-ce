/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 17, 2010 9:09:00 AM 
 * 
 */
package com.osserp.common.web.tags;

import javax.servlet.jsp.JspException;

/**
 * Renders an img icon tag by a logical name. The logical name must be defined as servlet context attribute referencing the image path without context
 * attribute.
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ImgTag extends AbstractImageTag {

    @Override
    public String getOutput() throws JspException {
        StringBuilder buffer = new StringBuilder("<img src=\"");
        buffer.append(getImageUrl()).append("\" class=\"");
        if (getStyleClass() == null || getStyleClass().length() < 1) {
            buffer.append("icon\"");
        } else {
            buffer.append(getStyleClass()).append("\"");
        }
        if (getStyle() != null && getStyle().length() > 0) {
            buffer.append(" style=\"").append(getStyle()).append("\"");
        }
        if (getOnclick() != null && getOnclick().length() > 0) {
            buffer.append(" onclick=\"").append(getOnclick()).append("\"");
        }
        if (getTitle() != null && getTitle().length() > 0) {
            buffer.append(" title=\"").append(getResourceString(getTitle())).append("\"");
        }
        
        buffer.append("/>");
        return buffer.toString();
    }
}
