/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 29, 2011 12:45:13 PM 
 * 
 */
package com.osserp.gui.admin.products

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.core.Options
import com.osserp.core.products.ProductCategoryManager
import com.osserp.core.products.ProductGroupManager
import com.osserp.core.products.ProductTypeManager
import com.osserp.groovy.web.ViewContextException
import com.osserp.gui.CoreView

/**
 *
 * @author eh <eh@osserp.com>
 * 
 */
class ClassificationConfigCreatorView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(ClassificationConfigCreatorView.class.getName())

    private final String TYPE = "type"
    private final String GROUP = "group"
    private final String CATEGORY = "category"

    def activeMode = TYPE

    ClassificationConfigCreatorView() {
        dependencies = ['classificationConfigsView']
        enablePopupView()
        headerName = 'createProductType'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (!env.classificationConfigsView) {
            logger.error('initRequest: classificationConfigsView not bound')
            throw new ViewContextException(this)
        }

        def mode = form.getString('mode')
        if (mode) {
            if (mode == GROUP) {
                activeMode = GROUP
                headerName = 'createProductGroup'
            } else if (mode == CATEGORY) {
                activeMode = CATEGORY
                headerName = 'createProductCategory'
            }
        }
    }

    @Override
    void save() {
        def name = form.getString('name')
        def description = form.getString('description')
        if (!name) {
            throw new ClientException(ErrorCode.VALUES_MISSING)
        }

        if (activeMode == GROUP) {
            def group = productGroupManager.create(domainEmployee, name, description)
            env.classificationConfigsView.form.params.id = group.id
            env.classificationConfigsView.addGroup()
            reloadOptions(Options.PRODUCT_GROUPS)
            logger.debug("save: created new ProductGroup [name=${name}]")
        } else if (activeMode == CATEGORY) {
            def cat = productCategoryManager.create(domainEmployee, name, description)
            env.classificationConfigsView.form.params.id = cat.id
            env.classificationConfigsView.addCategory()
            reloadOptions(Options.PRODUCT_CATEGORIES)
            logger.debug("save: created new ProductCategory [name=${name}]")
        } else {
            productTypeManager.create(domainEmployee, name, description)
            reloadOptions(Options.PRODUCT_TYPES)
            logger.debug("save: created new ProductType [name=${name}]")
        }
        env.classificationConfigsView.reload()
    }

    private ProductTypeManager getProductTypeManager() {
        getService(ProductTypeManager.class.getName())
    }

    private ProductGroupManager getProductGroupManager() {
        getService(ProductGroupManager.class.getName())
    }

    private ProductCategoryManager getProductCategoryManager() {
        getService(ProductCategoryManager.class.getName())
    }
}
