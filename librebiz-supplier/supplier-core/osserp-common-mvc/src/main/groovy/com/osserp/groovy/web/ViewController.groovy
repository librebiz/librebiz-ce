/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 18, 2010 
 * 
 */
package com.osserp.groovy.web

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.remoting.RemoteAccessException
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.common.ClientException
import com.osserp.common.PermissionException
import com.osserp.common.TimeoutException
import com.osserp.common.dms.DocumentDataView
import com.osserp.common.web.RequestUtil

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@Controller
abstract class ViewController extends AbstractController {
    private static Logger logger = LoggerFactory.getLogger(ViewController.class.getName())

    private Set ajaxCalls = new HashSet()
    
    ViewController() {
        super()
    }

    protected ViewController(List ajaxMethods) {
        ajaxCalls = new HashSet(ajaxMethods)
    }

    /**
     * Appends id and view.selectionExit to view.selectionTarget and returns resulting string 
     * @param view
     * @param id
     * @return created target (e.g. view.selectionTarget?id=ID&exit=view.selectionExit)
     */
    protected String createSelectionTarget(View view, Long id) {
        return appendParam(appendParam(view.selectionTarget, view.selectionTargetIdParam, id), 'exit', view.selectionExit)
    }

    @RequestMapping
    def forward(HttpServletRequest request) {
        logger.debug("forward: invoked [user=${getUserId(request)}, class=${getClass().getName()}]")
        // we call fetchView to check for user related requirements
        def view = fetchView(request)
        if (view?.popupView || pageAreaOnly) {
            logger.debug('forward: invoked in popup context...')
            return pageArea
        }
        return defaultPage
    }

    @RequestMapping
    def reload(HttpServletRequest request) {
        logger.debug("reload: invoked [user=${getUserId(request)}, class=${getClass().getName()}]")
        try {
            def view = fetchView(request)
            if (view?.autoreloadEnabled) {
                logger.debug("reload: found view to reload [name=${view.name}]")
                view.reload()
            } else if (!view && getUser(request)) {
                logger.debug("reload: did not find view but user logged in; forward to /index")
                return redirect(request, '/index')
            }
            
        } catch (TimeoutException e) {
            if (ajaxCalls.contains('reload')) {
                logger.debug('reload: caught timeout in ajax call')
                throw new PopupTimeoutException()
            }
            throw e

        } catch (Exception e) {
            // reload does nothing for other than timeout
            logger.debug("reload: caught exception [message=${e.message}]", e)
        }
        return defaultPage
    }

    @RequestMapping
    def exit(HttpServletRequest request) {
        if (pageAreaOnly) {
            return areaExit(request)
        }
        executeExit(request)
    }

    @RequestMapping
    def home(HttpServletRequest request) {
        executeHome(request)
    }

    @RequestMapping
    def enableSetupMode(HttpServletRequest request) {
        logger.debug("enableSetupMode: invoked [user=${getUserId(request)}, class=${getClass().getName()}]")
        View view = getView(request)
        try {
            view.enableSetupMode()

        } catch (TimeoutException e) {
            if (ajaxCalls.contains('enableSetupMode')) {
                logger.debug('enableSetupMode: caught timeout in ajax call')
                throw new PopupTimeoutException()
            }
            throw e

        } catch (PermissionException t) {
            if (logger.debugEnabled) {
                logger.debug("enableSetupMode: caught exception [message=${t.message}]")
            }
            saveError(request, t.message)
        }
        return defaultPage
    }

    @RequestMapping
    def disableSetupMode(HttpServletRequest request) {
        logger.debug("disableSetupMode: invoked [user=${getUserId(request)}, class=${getClass().getName()}]")
        try {
            View view = getView(request)
            view.disableSetupMode()
            return defaultPage

        } catch (TimeoutException e) {
            if (ajaxCalls.contains('disableSetupMode')) {
                logger.debug('disableSetupMode: caught timeout in ajax call')
                throw new PopupTimeoutException()
            }
            throw e
        }
    }

    @RequestMapping
    def enableCreateMode(HttpServletRequest request) {
        logger.debug("enableCreateMode: invoked [user=${getUserId(request)}, class=${getClass().getName()}]")
        View view = getView(request)
        try {
            view.enableCreateMode()

        } catch (TimeoutException e) {
            if (ajaxCalls.contains('enableCreateMode')) {
                logger.debug('enableCreateMode: caught timeout in ajax call')
                throw new PopupTimeoutException()
            }
            throw e
        } catch (ClientException t) {
            if (logger.debugEnabled) {
                logger.debug("enableCreateMode: caught client exception [message=${t.message}]")
            }
            saveError(request, t.message)
        } catch (PermissionException t) {
            if (logger.debugEnabled) {
                logger.debug("enableCreateMode: caught permission exception [message=${t.message}]")
            }
            saveError(request, t.message)
        }
        return defaultPage
    }

    @RequestMapping
    def disableCreateMode(HttpServletRequest request) {
        logger.debug("disableCreateMode: invoked [user=${getUserId(request)}, class=${getClass().getName()}]")
        try {
            View view = getView(request)
            view.disableCreateMode()
            if (view.externalInvocationMode) {
                return executeExit(request)
            }
            return defaultPage

        } catch (TimeoutException e) {
            if (ajaxCalls.contains('disableCreateMode')) {
                logger.debug('disableCreateMode: caught timeout in ajax call')
                throw new PopupTimeoutException()
            }
            throw e
        }
    }

    @RequestMapping
    def enableEditMode(HttpServletRequest request) {
        logger.debug("enableEditMode: invoked [user=${getUserId(request)}, class=${getClass().getName()}]")
        View view = getView(request)
        try {
            view.enableEditMode()

        } catch (TimeoutException e) {
            if (ajaxCalls.contains('enableEditMode')) {
                logger.debug('enableEditMode: caught timeout in ajax call')
                throw new PopupTimeoutException()
            }
            throw e
        } catch (Exception e) {
            if (logger.debugEnabled) {
                logger.debug("enableEditMode: caught exception [message=${e.message}]")
            }
            saveError(request, e.message)
        }
        return defaultPage
    }

    @RequestMapping
    def disableEditMode(HttpServletRequest request) {
        logger.debug("disableEditMode: invoked [user=${getUserId(request)}, class=${getClass().getName()}]")
        try {
            View view = getView(request)
            view.disableEditMode()
            if (view.externalInvocationMode) {
                return executeExit(request)
            }
            return defaultPage

        } catch (TimeoutException e) {
            if (ajaxCalls.contains('disableEditMode')) {
                logger.debug('disableEditMode: caught timeout in ajax call')
                throw new PopupTimeoutException()
            }
            throw e
        }
    }

    /**
     * Expects current view as instanceof DocumentDataView and writes the 
     * stream provided by document data to the response.
     * @param request
     * @param response
     * @return back to current page if document not exists or exception was thrown.
     */
    @RequestMapping
    def print(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("print: invoked [user=${getUserId(request)}, class=${getClass().getName()}]")
        DocumentDataView view = getView(request)
        try {
            savePdf(request, view.getDocumentData())
            renderPdf(request, response)
            
        } catch (Exception e) {
            logger.debug("print: caught exception [message=${e.message}]", e)
            saveError(request, e.message)
            return currentPage
        }
    }

    @RequestMapping
    def save(HttpServletRequest request) {
        logger.debug("save: invoked [user=${getUserId(request)}, class=${getClass().getName()}]")
        View view = getView(request)
        try {
            executeSave(request)
    
        } catch (ViewRedirectException e) {
            logger.debug("save: caught view redirect exception [user=${getUserId(request)}, message=${e.message}]")
            return redirect(request, e.message)
        }
        if (!errorAvailable(request) && view.externalInvocationMode 
                && (view.createMode || view.editMode) ) {
            logger.debug('save: done; invoking executeExit on externalInvocationMode')
            return executeExit(request)
        }
        logger.debug('save: done; returning to defaultPage')
        return defaultPage
    }

    @RequestMapping
    def select(HttpServletRequest request) {
        logger.debug("select: invoked [user=${getUserId(request)}, class=${getClass().getName()}]")
        try {
            View view = getView(request)
            view.select()

            if (!view.form.params.displaySelection && view.form.params.selectionTarget) {
                return view.form.params.selectionTarget
            }

        } catch (ClientException e) {
            if (logger.debugEnabled) {
                logger.debug("select: caught exception [class=${e.getClass().getName()}, message=${e.message}]")
            }
            saveError(request, e.message)
            
        } catch (TimeoutException e) {
            if (ajaxCalls.contains('select')) {
                logger.debug('select: caught timeout in ajax call')
                throw new PopupTimeoutException()
            }
            throw e
        }
        return defaultPage
    }
    
    

    @RequestMapping
    def collect(HttpServletRequest request) {
        logger.debug("collect: invoked [user=${getUserId(request)}, class=${getClass().getName()}]")
        try {
            View view = getView(request)
            view.collect()
            return defaultPage

        } catch (TimeoutException e) {
            if (ajaxCalls.contains('collect')) {
                logger.debug('collect: caught timeout in ajax call')
                throw new PopupTimeoutException()
            }
            throw e
        }
    }

    @RequestMapping
    def removeCollected(HttpServletRequest request) {
        logger.debug("removeCollected: invoked [user=${getUserId(request)}, class=${getClass().getName()}]")
        try {
            View view = getView(request)
            view.removeCollected()
            return defaultPage

        } catch (TimeoutException e) {
            if (ajaxCalls.contains('removeCollected')) {
                logger.debug('removeCollected: caught timeout in ajax call')
                throw new PopupTimeoutException()
            }
            throw e
        }
    }
    
    @RequestMapping
    def listNext(HttpServletRequest request) {
        logger.debug("listNext: invoked [user=${getUserId(request)}, class=${getClass().getName()}]")
        View view = getView(request)
        view.listNext()
        return defaultPage
    }
    
    @RequestMapping
    def listPrevious(HttpServletRequest request) {
        logger.debug("listPrevious: invoked [user=${getUserId(request)}, class=${getClass().getName()}]")
        View view = getView(request)
        view.listPrevious()
        return defaultPage
    }

    @RequestMapping
    def savePosition(HttpServletRequest request) {
        logger.debug("savePosition: invoked [user=${getUserId(request)}, class=${getClass().getName()}]")
        return defaultPage
    }

    protected String getReloadParent() {
        'reloadParent'
    }
    
    @RequestMapping
    def areaExit(HttpServletRequest request) {
        executeExit(request)
        'reloadParent'
    }
    
    @RequestMapping
    def areaHome(HttpServletRequest request) {
        executeHome(request)
        'reloadParent'
    }

    @RequestMapping
    def areaSave(HttpServletRequest request) {
        def result = defaultPage
        View view = getView(request)
        try {
            executeSave(request)
        } catch (ViewRedirectException e) {
            return redirect(request, e.message)
        } 
        if (!errorAvailable(request)) {
            if (view.popupSaveExit) {
                logger.debug('areaSave: found popupSaveExit enabled')
                return areaExit(request)
            }
            if (view.popupSaveReloadParent) {
                logger.debug('areaSave: found popupSaveReloadParent enabled')
                return 'reloadParent'
            }
        }
        pageArea
    }
    
    protected final def executeSave(HttpServletRequest request) throws ViewRedirectException {
        logger.debug("executeSave: invoked [user=${getUserId(request)}, class=${getClass().getName()}]")
        try {
            View view = getView(request)
            view.save()
            if (view.updateOnlyTarget) {
                logger.debug("executeSave: found updateOnlyTarget [url=${view.updateOnlyTarget}]")
                throw new ViewRedirectException(view.updateOnlyTarget)
            }

        } catch (TimeoutException e) {
            if (ajaxCalls.contains('save')) {
                logger.debug('executeSave: caught timeout in ajax call')
                throw new PopupTimeoutException()
            }
            throw e
        } catch (ClientException e) {
            if (logger.debugEnabled) {
                logger.debug("executeSave: caught exception [class=${e.getClass().getName()}, message=${e.message}]")
            }
            saveError(request, e.message)
        } catch (PermissionException e) {
            if (logger.debugEnabled) {
                logger.debug("executeSave: caught exception [class=${e.getClass().getName()}, message=${e.message}]")
            }
            saveError(request, e.message)
        } catch (RemoteAccessException e) {
            if (logger.debugEnabled) {
                logger.debug("executeSave: caught exception [class=${e.getClass().getName()}, message=${e.message}]")
            }
            saveError(request, e.message)
        }
    }
    
    protected def executeExit(HttpServletRequest request) {
        logger.debug("executeExit: invoked [user=${getUserId(request)}, class=${getClass().getName()}]")
        try {
            View view = getView(request)
            def exitId = view.exitId
            def exitTarget = view.exitTarget
            if (!exitTarget) {
                exitTarget = view.homeTarget
            } else if (exitTarget.startsWith('redirect')) {
                exitTarget = appendExitId(exitTarget, exitId)
            } else {
                RequestUtil.saveExitId(request, exitId)
            }
            if (view.destroyViewOnExit) {
                removeView(request, view)
            }
            logger.debug("executeExit: done [target=${exitTarget}, exitId=${exitId}]")
            return exitTarget

        } catch (TimeoutException e) {
            if (ajaxCalls.contains('exit')) {
                logger.debug('executeExit: caught timeout in ajax call')
                throw new PopupTimeoutException()
            }
            throw e
        }
    }
    
    @RequestMapping
    def executeHome(HttpServletRequest request) {
        logger.debug("executeHome: invoked [user=${getUserId(request)}, class=${getClass().getName()}]")
        try {
            View view = getView(request)
            def exitTarget = view.homeTarget
            if (view.destroyViewOnHome) {
                removeView(request, view)
            }
            return exitTarget

        } catch (TimeoutException e) {
            if (ajaxCalls.contains('home')) {
                logger.debug('executeHome: caught timeout in ajax call')
                throw new PopupTimeoutException()
            }
            throw e
        }
    }
}
