/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 3, 2005 
 * 
 */
package com.osserp.common.web.struts;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.DynaActionForm;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.FileObject;
import com.osserp.common.util.DateUtil;
import com.osserp.common.util.NumberFormatter;
import com.osserp.common.web.AbstractForm;
import com.osserp.common.web.Form;

/**
 * 
 * Helper class for extracting retrieved values from an dyna action form. Usage of this class simplifies property access only
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class StrutsForm extends AbstractForm implements Form {
    private static Logger log = LoggerFactory.getLogger(StrutsForm.class.getName());

    private DynaActionForm form = null;

    protected StrutsForm() {
        super();
    }

    public StrutsForm(ActionForm actionForm) {
        if (actionForm instanceof DynaActionForm) {
            if (log.isDebugEnabled()) {
                log.debug("<init> invoked with dyna action form");
            }
            form = (DynaActionForm) actionForm;
        } else {
            log.warn("<init> invoked with unknown form [class="
                    + (actionForm == null ? "null" : actionForm.getClass().getName())
                    + "]");
        }
    }

    /**
     * Tries to extract an object from the form variable key.
     * @param key the name of the variable holding the value
     * @return value or null if variable not exists
     */
    public Object getValue(String key) {
        try {
            return form.get(key);
        } catch (Throwable ignore) {
            log.error("getValue(" + key + ") ignoring of exception:\n"
                    + ignore.toString());
            return null;
        }
    }

    /**
     * Tries to extract a String from the form variable key.
     * @param key / name of the variable holding the value
     * @return value or null if variable not exists
     */
    public String getString(String key) {
        try {
            String s = (String) form.get(key);
            if (s == null || s.length() < 1) {
                return null;
            }
            return s;
        } catch (Exception ignore) {
            log.error("getString(" + key + ") ignoring of exception:\n"
                    + ignore.toString());
            return null;
        }
    }

    /**
     * Tries to extract a String from the form variable key.
     * @param key the name of the variable holding the value
     * @param required indicates if an exception should be thrown if no value under key found
     * @return value or null if variable not exists AND required is false
     * @throws ClientException if required is true and no string set
     */
    public String getString(String key, boolean required) throws ClientException {
        String res = getString(key);
        if (required && (res == null || res.length() < 1)) {
            throw new ClientException(ErrorCode.VALUES_MISSING);
        }
        return res;
    }

    /**
     * Tries to extract an Integer from the form variable key.
     * @param key the name of the variable holding the value
     * @return value or null if variable not exists
     */
    public Integer getInteger(String key) {
        try {
            return (Integer) form.get(key);
        } catch (Throwable ignore) {
            log.error("getInteger(" + key + ") ignoring of exception:\n"
                    + ignore.toString());
            return null;
        }
    }

    /**
     * Tries to get an int from a form property defined as Integer
     * @param key
     * @return value
     * @throws ClientException if no int found
     */
    public int getIntVal(String key) throws ClientException {
        try {
            return ((Integer) form.get(key)).intValue();
        } catch (Throwable ex) {
            log.error("getInt(" + key + ") failed: " + ex.toString());
            throw new ClientException("int.expected");
        }
    }

    /**
     * Tries to get an object specified as Long in form definition
     * @param key / name of the form field
     * @return value or null if not found
     */
    public Long getLong(String key) {
        try {
            return (Long) form.get(key);
        } catch (Throwable ignore) {
            return null;
        }
    }

    /**
     * Tries to get the long value of an object specified as Long in form definition
     * @param key / name of the form field
     * @return value or -1 if not found
     */
    public long getLongVal(String key) {
        Long l = getLong(key);
        return (l == null) ? -1 : l.longValue();
    }

    public Long getSelection(String key) {
        Long l = getLong(key);
        return l == null || l == 0 ? null : l;
    }

    /**
     * Tries to get a boolean value of an object specified as Boolean in form definition
     * @param key / name of the form field
     * @return true if set, false if not set or found
     */
    public boolean getBoolean(String key) {
        try {
            Boolean b = (Boolean) form.get(key);
            return (b == null) ? false : b.booleanValue();
        } catch (Exception ignore) {
            log.error("getBoolean(" + key + ") ignoring of exception:\n"
                    + ignore.toString());
            return false;
        }
    }

    /**
     * Tries to retrieve an array of String objects specified as String[] in the form definition.
     * @param key / name of the form field
     * @return array of string or null if not set or found
     */
    public String[] getIndexedStrings(String key) {
        try {
            return (String[]) form.get(key);
        } catch (Exception e) {
            if (log.isDebugEnabled()) {
                log.debug("getIndexedStrings(" + key
                        + ") throws exception: " + e.toString(), e);
            }
            return null;
        }
    }

    /**
     * Tries to retrieve an array of Integer objects specified as Integer[] in the form definition.
     * @param key / name of the form field
     * @return array of integer or null if not set or found
     */
    public Integer[] getIndexedIntegers(String key) {
        try {
            return (Integer[]) form.get(key);
        } catch (Exception e) {
            if (log.isDebugEnabled()) {
                log.debug("getIndexedIntegers(" + key
                        + ") throws exception: " + e.toString(), e);
            }
            return null;
        }
    }

    /**
     * Tries to retrieve an array of Long objects specified as Long[] in the form definition.
     * @param key / name of the form field
     * @return array of long or null if not set or found
     */
    public Long[] getIndexedLong(String key) {
        try {
            return (Long[]) form.get(key);
        } catch (Exception e) {
            if (log.isDebugEnabled()) {
                log.debug("getIndexedLong(" + key
                        + ") throws exception: " + e.toString(), e);
            }
            return null;
        }
    }

    /**
     * Retrieves indexed double value
     * @param key of form field
     * @return array of double values or null if none exist
     * @throws ClientException if any value has an invalid format
     */
    public Double[] getIndexedDouble(String key) throws ClientException {
        String[] sarray = getIndexedStrings(key);
        if (sarray == null || sarray.length == 0) {
            return null;
        }
        int l = sarray.length;
        Double[] res = new Double[l];
        for (int i = 0, j = l; i < j; i++) {
            res[i] = NumberFormatter.createDouble(sarray[i], true);
        }
        return res;
    }

    /**
     * Retrieves a request parameter string as date
     * @param key request param value of key
     * @return date or null if not set
     * @throws ClientException if value was set but illegal format
     */
    public Date getDate(String key) throws ClientException {
        return DateUtil.createDate(getString(key), false);
    }

    /**
     * Tries to extract a Double value from the form variable key.
     * @param key the name of the variable holding the value
     * @return big decimal object or null if variable not exists
     * @throws ClientException if value exists and is no number
     */
    public BigDecimal getDecimal(String key) throws ClientException {
        try {
            String value = getString(key);
            return value == null ? null : NumberFormatter.createDecimal(value, false);
        } catch (ClientException e) {
            if (log.isDebugEnabled()) {
                log.debug("getDouble() invalid value under key " + key);
            }
            throw e;
        }
    }

    /**
     * Tries to extract a Double value from the form variable key.
     * @param key the name of the variable holding the value
     * @return object or null if variable not exists
     * @throws ClientException if value exists and is no number
     */
    public Double getDouble(String key) throws ClientException {
        try {
            String value = getString(key);
            return value == null ? null : NumberFormatter.createDouble(value, false);
        } catch (ClientException e) {
            if (log.isDebugEnabled()) {
                log.debug("getDouble() invalid value under key " + key);
            }
            throw e;
        }
    }

    public String getDoubleAsString(String key) throws ClientException {
        Object value = getValue(key);
        if (value == null) {
            return null;
        }
        Double result = NumberFormatter.createDouble(value.toString(), false);
        return (result == null ? null : result.toString());
    }

    /**
     * Provides a decimal percentage representation of a percentage value expected as string under given key
     * @param key
     * @return percentage representation or 0
     */
    public Double getPercentage(String key) {
        try {
            String s = getString(key);
            if (s == null) {
                return null;
            }
            if (log.isDebugEnabled()) {
                log.debug("getPercentage() retrieved " + s);
            }
            BigDecimal bd = new BigDecimal(s).divide(new BigDecimal("100"));
            Double result = Double.valueOf(bd.toPlainString());
            if (log.isDebugEnabled()) {
                log.debug("getPercentage() calulated percentage as value is "
                        + bd.doubleValue() + "\nresult as plain string is "
                        + bd.toPlainString() + "\ngenerated double is "
                        + result);
            }
            return result;
        } catch (NumberFormatException nfe) {
            return 0d;
        }
    }

    /**
     * Provides a decimal percentage representation of a percentage value expected as string under given key
     * @param key
     * @param required
     * @return percentage representation or null if required is false and no value found
     * @throws ClientException if string found under key represents no number
     */
    public Double getPercentage(String key, boolean required) throws ClientException {
        try {
            String s = getString(key);
            if (s == null) {
                if (required) {
                    throw new ClientException(ErrorCode.VALUES_MISSING);
                }
                return null;
            }
            if (log.isDebugEnabled()) {
                log.debug("getPercentage() retrieved " + s);
            }
            BigDecimal bd = new BigDecimal(s).divide(new BigDecimal("100"));
            Double result = Double.valueOf(bd.toPlainString());
            if (log.isDebugEnabled()) {
                log.debug("getPercentage() calulated percentage as value is "
                        + bd.doubleValue() + "\nresult as plain string is "
                        + bd.toPlainString() + "\ngenerated double is "
                        + result);
            }
            return result;
        } catch (NumberFormatException nfe) {
            throw new ClientException(ErrorCode.NUMBER_EXPECTED);
        }
    }

    public void setValue(String key, Object value) {
        try {
            form.set(key, value);
        } catch (Exception e) {
            log.error("setValue() failed on property " + key + ": "
                    + e.toString(), e);
        }
    }

    public void resetValue(String key) {
        form.set(key, null);
    }

    public Object getForm() {
        return form;
    }

    public FileObject getUpload() throws ClientException {
        return null;
    }

    public List<FileObject> getUploads() {
        return new ArrayList<>();
    }

    public Map getMapped() {
        return (form == null ? new HashMap<>() : form.getMap());
    }
}
