/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 7, 2009 7:46:56 AM 
 * 
 */
package com.osserp.core.service.products;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.util.DateUtil;

import com.osserp.core.dao.ProductClassificationEntities;
import com.osserp.core.employees.Employee;
import com.osserp.core.products.ProductClassificationEntity;
import com.osserp.core.products.ProductClassificationEntityManager;
import com.osserp.core.service.impl.AbstractService;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class AbstractProductClassificationEntityManager extends AbstractService implements ProductClassificationEntityManager {
    private static Logger log = LoggerFactory.getLogger(AbstractProductClassificationEntityManager.class.getName());

    private ProductClassificationEntities entities = null;

    protected AbstractProductClassificationEntityManager(ProductClassificationEntities entities) {
        super();
        this.entities = entities;
    }

    public ProductClassificationEntity create(Employee user, String name, String description) throws ClientException {
        return entities.create(user, name, description);
    }

    public List<ProductClassificationEntity> getConfigs() {
        return (List<ProductClassificationEntity>) entities.getConfigs();
    }

    public List<ProductClassificationEntity> getList() {
        return (List<ProductClassificationEntity>) entities.getList();
    }

    public void update(Employee user, ProductClassificationEntity entity) throws ClientException {
        if (isNotSet(entity.getName())) {
            throw new ClientException(ErrorCode.NAME_MISSING);
        }
        List<ProductClassificationEntity> list = getList();
        for (int i = 0, j = list.size(); i < j; i++) {
            ProductClassificationEntity next = list.get(i);
            if (!next.getId().equals(entity.getId())) {
                if (next.getName().equalsIgnoreCase(entity.getName())) {
                    throw new ClientException(ErrorCode.NAME_EXISTS);
                }
            }
        }
        entity.setChanged(DateUtil.getCurrentDate());
        entity.setChangedBy(user.getId());
        if (log.isDebugEnabled()) {
            log.debug("update() invoked [class=" + entity.getClass().getName() + ", values=\n"
                    + entity.toString() + "]");
        }
        entities.save(entity);
    }

    protected ProductClassificationEntities getEntities() {
        return entities;
    }
}
