<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="table-responsive table-responsive-default">
    <table class="table">
        <thead>
            <tr>
                <th class="recordDate"><fmt:message key="bankAccount" /></th>
                <th class="recordType"><fmt:message key="type" /></th>
                <th class="recordDate"><fmt:message key="demand" /></th>
                <th class="recordName"><fmt:message key="name" /></th>
                <th class="recordNote"><fmt:message key="description" /></th>
                <th class="recordAmount"><fmt:message key="amount" /></th>
                <th class="recordAction"></th>
            </tr>
        </thead>
        <tbody>
            <c:choose>
                <c:when test="${empty view.list}">
                    <tr>
                        <td colspan="7"><fmt:message key="noRecordsAvailable" /></td>
                    </tr>
                </c:when>
                <c:otherwise>
                    <c:forEach var="record" items="${view.list}" varStatus="s">
                        <c:if test="${!record.accountStatusOnly}">
                            <tr id="record${record.id}">
                                <td class="recordDate"><o:date value="${record.paidOn}" /></td>
                                <td class="recordType"><c:choose>
                                        <c:when test="${record.purchaseInvoicePayment}">
                                            <a href="<c:url value="/purchaseInvoice.do?method=display&exit=annualReport&readonly=true&id=${record.invoiceId}&exitId=record${record.id}"/>"> <o:out value="${record.recordType.name}" />
                                            </a>
                                        </c:when>
                                        <c:when test="${record.salesInvoicePayment}">
                                            <a href="<c:url value="/salesInvoice.do?method=display&exit=annualReport&readonly=true&id=${record.invoiceId}&exitId=record${record.id}"/>"> <o:out value="${record.recordType.name}" />
                                            </a>
                                        </c:when>
                                        <c:when test="${record.salesDownpayment}">
                                            <a href="<c:url value="/salesInvoice.do?method=displayDownpayment&exit=annualReport&readonly=true&id=${record.invoiceId}&exitId=record${record.id}"/>"> <o:out value="${record.recordType.name}" />
                                            </a>
                                        </c:when>
                                        <c:otherwise>
                                            <v:link url="/accounting/commonPayment/forward?id=${record.id}&exit=/accounting/annualReport/reload&exitId=record${record.id}">
                                                <o:out value="${record.recordType.name}" />
                                            </v:link>
                                        </c:otherwise>
                                    </c:choose></td>
                                <td class="recordDate"><o:date value="${record.created}" /></td>
                                <td class="recordName">
                                       <o:out value="${record.contactName}" limit="35" />
                                </td>
                                <td class="recordNote"><c:choose>
                                        <c:when test="${record.purchaseInvoicePayment}">
                                            <a href="<c:url value="/purchaseInvoice.do?method=display&exit=annualReport&readonly=true&id=${record.invoiceId}&exitId=record${record.id}"/>"> <o:out value="${record.invoiceNumber}" title="purchaseInvoice" />
                                            </a>
                                        </c:when>
                                        <c:when test="${record.salesInvoicePayment}">
                                            <a href="<c:url value="/salesInvoice.do?method=display&exit=annualReport&readonly=true&id=${record.invoiceId}&exitId=record${record.id}"/>"> <o:out value="${record.invoiceNumber}" title="invoice" />
                                            </a>
                                        </c:when>
                                        <c:when test="${record.salesDownpayment}">
                                            <a href="<c:url value="/salesInvoice.do?method=displayDownpayment&exit=annualReport&readonly=true&id=${record.invoiceId}&exitId=record${record.id}"/>"> <o:out value="${record.invoiceNumber}" title="downpayment" />
                                            </a>
                                        </c:when>
                                        <c:otherwise>
                                            <v:link url="/accounting/commonPayment/forward?id=${record.id}&exit=/accounting/annualReport/reload&exitId=record${record.id}">
                                                <o:out value="${record.note}" limit="36" />
                                            </v:link>
                                        </c:otherwise>
                                    </c:choose></td>
                                <td class="recordAmount"><span title="<o:number value="${record.accountStatusSummary}" format="currency" />"> <o:number value="${record.grossAmount}" format="currency" />
                                </span></td>
                                <td class="recordAction"><c:choose>
                                        <c:when test="${record.commonPayment and !empty record.documentId}">
                                            <v:link url="/dms/document/print?id=${record.documentId}" title="documentPrint">
                                                <o:img name="printIcon" />
                                            </v:link>
                                        </c:when>
                                        <c:otherwise>
                                            <c:if test="${record.unchangeable and !empty record.contextName}">
                                                <v:link url="/records/recordPrint/print?name=${record.contextName}&id=${record.invoiceId}" title="documentPrint">
                                                    <o:img name="printIcon" />
                                                </v:link>
                                            </c:if>
                                        </c:otherwise>
                                    </c:choose></td>
                            </tr>
                        </c:if>
                    </c:forEach>
                </c:otherwise>
            </c:choose>
        </tbody>
    </table>
</div>
