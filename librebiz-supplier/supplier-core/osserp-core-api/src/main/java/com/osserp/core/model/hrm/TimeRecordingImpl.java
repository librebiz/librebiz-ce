/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 22, 2007 1:23:39 PM 
 * 
 */
package com.osserp.core.model.hrm;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.PublicHoliday;
import com.osserp.common.beans.AbstractEntity;
import com.osserp.common.util.DateUtil;
import com.osserp.core.employees.Employee;
import com.osserp.core.hrm.TimeRecord;
import com.osserp.core.hrm.TimeRecordMarkerType;
import com.osserp.core.hrm.TimeRecordStatus;
import com.osserp.core.hrm.TimeRecordType;
import com.osserp.core.hrm.TimeRecording;
import com.osserp.core.hrm.TimeRecordingConfig;
import com.osserp.core.hrm.TimeRecordingDay;
import com.osserp.core.hrm.TimeRecordingMonth;
import com.osserp.core.hrm.TimeRecordingPeriod;
import com.osserp.core.hrm.TimeRecordingYear;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class TimeRecordingImpl extends AbstractEntity implements TimeRecording {
    private static Logger log = LoggerFactory.getLogger(TimeRecordingImpl.class.getName());

    private TimeRecordingPeriod selectedPeriod = null;
    private boolean systemTimeBookingEnabled = true;
    private boolean systemTimeBookingRequired = true;
    private Long terminalChipNumber = null;
    private Long defaultRecordType = TimeRecording.DEFAULT_RECORD_TYPE;
    private Date lastClosingActionTime = DateUtil.getCurrentDate();

    private List<TimeRecordingPeriod> recordings = new ArrayList<TimeRecordingPeriod>();

    protected TimeRecordingImpl() {
        super();
    }

    public TimeRecordingImpl(Employee user, Employee reference) {
        super((Long) null, reference.getId(), user.getId());
    }

    public Long getTerminalChipNumber() {
        return terminalChipNumber;
    }

    public void setTerminalChipNumber(Long terminalChipNumber) {
        this.terminalChipNumber = terminalChipNumber;
    }

    public boolean isSystemTimeBookingEnabled() {
        return systemTimeBookingEnabled;
    }

    public void setSystemTimeBookingEnabled(boolean systemTimeBookingEnabled) {
        this.systemTimeBookingEnabled = systemTimeBookingEnabled;
    }

    public boolean isSystemTimeBookingRequired() {
        return systemTimeBookingRequired;
    }

    public void setSystemTimeBookingRequired(boolean systemTimeBookingRequired) {
        this.systemTimeBookingRequired = systemTimeBookingRequired;
    }

    public Long getDefaultRecordType() {
        return defaultRecordType;
    }

    public void setDefaultRecordType(Long defaultRecordType) {
        this.defaultRecordType = defaultRecordType;
    }

    public TimeRecordingPeriod getCurrentPeriod() {
        return fetchCurrentRecording();
    }

    public TimeRecord getLastRecord() {
        TimeRecordingPeriod currentPeriod = (selectedPeriod != null ? selectedPeriod : getCurrentPeriod());
        if (currentPeriod != null) {
            return currentPeriod.getLastRecord();
        }
        return null;
    }

    public List<TimeRecordingPeriod> getRecordings() {
        return recordings;
    }

    protected void setRecordings(List<TimeRecordingPeriod> recordings) {
        this.recordings = recordings;
    }

    public void addPeriod(Employee user, TimeRecordingConfig config, Date whenValid) {
        TimeRecordingPeriodImpl obj = new TimeRecordingPeriodImpl(this, config, user, whenValid);
        this.recordings.add(obj);
    }

    public void selectCurrentPeriod() {
        this.selectPeriod(fetchCurrentRecording());
    }

    public void selectLatestPeriod() {
        this.selectPeriod(fetchLatestRecording());
    }

    public void selectPeriod(Long id) {
        if (id == null) {
            this.selectedPeriod = null;
        } else {
            for (int i = 0, j = recordings.size(); i < j; i++) {
                TimeRecordingPeriod next = recordings.get(i);
                if (next.getId().equals(id)) {
                    this.selectPeriod(next);
                    break;
                }
            }
        }
    }

    public void selectPeriod(Date date) {
        if (date == null) {
            this.selectedPeriod = null;
        } else {
            for (int i = 0, j = recordings.size(); i < j; i++) {
                TimeRecordingPeriod next = recordings.get(i);
                if (DateUtil.isDayInInterval(date, next.getValidFrom(), next.getValidTil())) {
                    this.selectPeriod(next);
                    break;
                }
            }
        }
    }

    private void selectPeriod(TimeRecordingPeriod selection) {
        this.selectedPeriod = selection;
    }

    public TimeRecordingPeriod getSelectedPeriod() {
        return selectedPeriod;
    }

    protected void setSelectedPeriod(TimeRecordingPeriod selectedPeriod) {
        this.selectedPeriod = selectedPeriod;
    }

    public List<TimeRecordingPeriod> getAvailablePeriods() {
        return recordings;
    }

    public int getCarryoverMinutes() {
        if (selectedPeriod == null
                || selectedPeriod.getSelectedYear() == null
                || selectedPeriod.getSelectedYear().getSelectedMonth() == null) {

            TimeRecordingMonth last = null;
            for (int i = 0, j = recordings.size(); i < j; i++) {
                TimeRecordingPeriod trp = recordings.get(i);
                for (int k = 0, l = trp.getSupportedYears().size(); k < l; k++) {
                    TimeRecordingYear year = trp.getSupportedYears().get(k);
                    for (int m = 0, n = year.getSupportedMonths().size(); m < n; m++) {
                        TimeRecordingMonth month = year.getSupportedMonths().get(m);
                        if (month.isCurrentMonth()) {
                            return month.getMinutesCarryover();
                        }
                        last = month;
                    }
                }
            }
            if (last != null) {
                return last.getMinutesCarryover();
            }
            return 0;
        }
        return getSelectedPeriod().getSelectedYear().getSelectedMonth().getMinutesCarryover();
    }

    public double getRemainingLeave() {
        if (selectedPeriod == null || selectedPeriod.getSelectedYear() == null) {

            TimeRecordingYear last = null;
            for (int i = 0, j = recordings.size(); i < j; i++) {
                TimeRecordingPeriod trp = recordings.get(i);
                for (int k = 0, l = trp.getSupportedYears().size(); k < l; k++) {
                    TimeRecordingYear year = trp.getSupportedYears().get(k);
                    if (year.isCurrent()) {
                        return year.getRemainingLeave();
                    }
                    last = year;
                }
            }
            if (last != null) {
                return last.getRemainingLeave();
            }
            return 0;
        }
        return getSelectedPeriod().getSelectedYear().getRemainingLeave();
    }

    public void checkRecordDate(Date startDate, Date stopDate) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("checkRecordDate() invoked [id=" + getId() + ", employee=" + getReference()
                    + ", startDate=" + startDate + ", stopDate=" + stopDate + "]");
        }
        if (startDate == null || stopDate == null) {
            throw new ClientException(ErrorCode.DATE_MISSING);
        }
        if (selectedPeriod != null) {
            this.selectedPeriod.selectMonth(DateUtil.createMonth(startDate));
            if (selectedPeriod.getSelectedYear() != null && selectedPeriod.getSelectedYear().getSelectedMonth() != null) {
                TimeRecordingMonth month = selectedPeriod.getSelectedYear().getSelectedMonth();
                for (int i = 0, j = month.getDays().size(); i < j; i++) {
                    TimeRecordingDay day = month.getDays().get(i);
                    if (DateUtil.isSameDay(day.getDate(), startDate)) {
                        if (day.isOpen()) {
                            throw new ClientException(ErrorCode.BOOKING_STOP_MISSING);
                        }
                        if (day.isBooking(startDate, stopDate)) {
                            throw new ClientException(ErrorCode.TIME_ALREADY_BOOKED);
                        }
                    }
                }
            }
        }
    }

    public void checkRecordDate(Date date, boolean startBooking) throws ClientException {
        if (date == null) {
            throw new ClientException(ErrorCode.DATE_MISSING);
        }
        if (selectedPeriod != null) {
            this.selectedPeriod.selectMonth(DateUtil.createMonth(date));
            if (selectedPeriod.getSelectedYear() != null && selectedPeriod.getSelectedYear().getSelectedMonth() != null) {
                TimeRecordingMonth month = selectedPeriod.getSelectedYear().getSelectedMonth();
                for (int o = 0, p = month.getDays().size(); o < p; o++) {
                    TimeRecordingDay day = month.getDays().get(o);
                    if (DateUtil.isSameDay(day.getDate(), date)) {
                        if (startBooking && day.isOpen()) {
                            throw new ClientException(ErrorCode.BOOKING_STOP_MISSING);
                        }
                        if (!startBooking && !day.isOpen()) {
                            throw new ClientException(ErrorCode.BOOKING_START_MISSING);
                        }
                        if (day.isBooking(date, startBooking)) {
                            throw new ClientException(ErrorCode.TIME_ALREADY_BOOKED);
                        }
                    }
                }
            }
        }
    }

    public void addRecord(
            Employee user,
            TimeRecordType type,
            TimeRecordStatus status,
            Date value,
            String note,
            boolean systemTime)
            throws ClientException {
        if (selectedPeriod != null) {
            for (Iterator<TimeRecordingPeriod> i = recordings.iterator(); i.hasNext();) {
                TimeRecordingPeriod next = i.next();
                if (next.getId().equals(selectedPeriod.getId())) {
                    if (next instanceof TimeRecordingPeriodImpl) {
                        TimeRecordingPeriodImpl obj = (TimeRecordingPeriodImpl) next;
                        obj.addRecord(user, type, status, value, note, systemTime, null);
                        this.selectedPeriod = obj;
                        break;
                    }
                }
            }
        }
    }

    public void addRecord(
            TimeRecordType type,
            TimeRecordStatus status,
            Date value,
            Long terminalId)
            throws ClientException {

        if (selectedPeriod != null) {
            for (Iterator<TimeRecordingPeriod> i = recordings.iterator(); i.hasNext();) {
                TimeRecordingPeriod next = i.next();
                if (next.getId().equals(selectedPeriod.getId())) {
                    if (next instanceof TimeRecordingPeriodImpl) {
                        TimeRecordingPeriodImpl obj = (TimeRecordingPeriodImpl) next;
                        obj.addRecord(getReference(), type, status, value, null, false, terminalId);
                        this.selectedPeriod = obj;
                        break;
                    }
                }
            }
        }
    }

    public void addMarker(
            Employee user,
            TimeRecordMarkerType type,
            Date date,
            Double value,
            String note)
            throws ClientException {
        if (selectedPeriod != null) {
            for (Iterator<TimeRecordingPeriod> i = recordings.iterator(); i.hasNext();) {
                TimeRecordingPeriod next = i.next();
                if (next.getId().equals(selectedPeriod.getId())) {
                    if (next instanceof TimeRecordingPeriodImpl) {
                        TimeRecordingPeriodImpl obj = (TimeRecordingPeriodImpl) next;
                        obj.addMarker(user, type, date, value, note);
                        this.selectedPeriod = obj;
                        break;
                    }
                }
            }
        }
    }

    public TimeRecord createCorrection(
            Employee user,
            TimeRecord record,
            Date startDate,
            Date stopDate,
            String note,
            TimeRecordStatus status)
            throws ClientException {
        if (selectedPeriod != null) {
            for (Iterator<TimeRecordingPeriod> i = recordings.iterator(); i.hasNext();) {
                TimeRecordingPeriod next = i.next();
                if (next.getId().equals(selectedPeriod.getId())) {
                    if (next instanceof TimeRecordingPeriodImpl) {
                        TimeRecordingPeriodImpl obj = (TimeRecordingPeriodImpl) next;
                        TimeRecord correctionRecord = obj.createCorrection(user, record, startDate, stopDate, note, status);
                        if (correctionRecord != null) {
                            this.selectedPeriod = obj;
                            return correctionRecord;
                        }
                    }
                }
            }
        }
        return null;
    }

    public void updatePeriod(Date validFrom, Date validTil, Double carryoverLeave, Double carryoverHours, Double extraLeave, Double firstYearLeave, Long state,
            Long terminalChipNo) throws ClientException {
        terminalChipNumber = terminalChipNo;
        if (selectedPeriod != null) {
            for (Iterator<TimeRecordingPeriod> i = recordings.iterator(); i.hasNext();) {
                TimeRecordingPeriod next = i.next();
                if (next.getId().equals(selectedPeriod.getId())) {
                    if (next instanceof TimeRecordingPeriodImpl) {
                        TimeRecordingPeriodImpl obj = (TimeRecordingPeriodImpl) next;

                        if (validFrom != null) {
                            if (validTil != null) {
                                if (DateUtil.isLaterDay(validFrom, validTil)) {
                                    throw new ClientException(ErrorCode.DATE_START_AFTER_STOP);
                                }
                                for (int k = 0, l = obj.getRecordList().size(); k < l; k++) {
                                    TimeRecord record = obj.getRecordList().get(k);
                                    if (record.getValue().after(validTil)
                                            && !DateUtil.isSameDay(record.getValue(), validTil)) {
                                        throw new ClientException(ErrorCode.BOOKING_AFTER_VALIDTIL);
                                    }
                                }
                            }
                            for (int k = 0, l = obj.getRecordList().size(); k < l; k++) {
                                TimeRecord record = obj.getRecordList().get(k);
                                if (record.getValue().before(validFrom)
                                        && !DateUtil.isSameDay(record.getValue(), validFrom)) {
                                    throw new ClientException(ErrorCode.BOOKING_BEFORE_VALIDFROM);
                                }
                            }
                        } else {
                            throw new ClientException(ErrorCode.DATE_FROM_MISSING);
                        }
                        obj.setValidFrom(validFrom);
                        obj.setValidTil(validTil);

                        if (carryoverLeave != null) {
                            obj.setCarryoverLeave(carryoverLeave);
                        }
                        if (carryoverHours != null) {
                            obj.setCarryoverHours(carryoverHours);
                        }
                        if (extraLeave != null) {
                            obj.setExtraLeave(extraLeave);
                        }
                        if (firstYearLeave != null) {
                            obj.setFirstYearLeave(firstYearLeave);
                        }
                        if (state != null) {
                            obj.setState(state);
                        }
                    }
                    break;
                }
            }
        }
    }

    public boolean isPreviousPeriodAvailable() {
        if (selectedPeriod != null) {
            for (int i = 0, j = recordings.size(); i < j; i++) {
                TimeRecordingPeriod next = recordings.get(i);
                if (!next.getId().equals(selectedPeriod.getId())
                        && next.getValidTil() != null
                        && selectedPeriod.getValidFrom().after(next.getValidTil())) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isNextPeriodAvailable() {
        if (selectedPeriod != null) {
            for (int i = 0, j = recordings.size(); i < j; i++) {
                TimeRecordingPeriod next = recordings.get(i);
                if (!next.getId().equals(selectedPeriod.getId())
                        && next.getValidFrom() != null
                        && selectedPeriod.getValidTil() != null
                        && selectedPeriod.getValidTil().before(next.getValidFrom())) {
                    return true;
                }
            }
        }
        return false;
    }

    public TimeRecordingPeriod getNextPeriod() {
        if (selectedPeriod != null) {
            for (int i = 0, j = recordings.size(); i < j; i++) {
                TimeRecordingPeriod next = recordings.get(i);
                if (!next.getId().equals(selectedPeriod.getId())
                        && next.getValidFrom() != null
                        && selectedPeriod.getValidTil() != null
                        && selectedPeriod.getValidTil().before(next.getValidFrom())) {
                    return next;
                }
            }
        }
        return null;
    }

    public Date getLastClosingActionTime() {
        return lastClosingActionTime;
    }

    public void setLastClosingActionTime(Date lastClosingActionTime) {
        this.lastClosingActionTime = lastClosingActionTime;
    }

    public void refreshCurrentSelection() {
        if (selectedPeriod != null && selectedPeriod.getSelectedYear() != null
                && selectedPeriod.getSelectedYear().getSelectedMonth() != null) {
            TimeRecordingYear year = selectedPeriod.getSelectedYear();
            this.selectedPeriod.selectMonth(year.getSelectedMonth());
        }
    }

    public void removeRecord(Long id) {
        for (Iterator<TimeRecordingPeriod> i = recordings.iterator(); i.hasNext();) {
            TimeRecordingPeriod next = i.next();
            if (next.removeRecord(id)) {
                break;
            }
        }
    }

    public void removeMarker(Long id) {
        for (Iterator<TimeRecordingPeriod> i = recordings.iterator(); i.hasNext();) {
            TimeRecordingPeriod next = i.next();
            if (next.removeMarker(id)) {
                break;
            }
        }
    }

    public void synchronizePublicHolidays(List<PublicHoliday> allPublicHoliday) {
        for (Iterator<TimeRecordingPeriod> i = recordings.iterator(); i.hasNext();) {
            TimeRecordingPeriodImpl obj = (TimeRecordingPeriodImpl) i.next();
            obj.synchronizePublicHolidays(allPublicHoliday);
        }
    }

    private TimeRecordingPeriod fetchCurrentRecording() {
        if (!recordings.isEmpty()) {
            for (int i = 0, j = recordings.size(); i < j; i++) {
                TimeRecordingPeriod next = recordings.get(i);
                if (next.isCurrent()) {
                    return next;
                }
            }
        }
        return null;
    }

    private TimeRecordingPeriod fetchLatestRecording() {
        Date now = DateUtil.getCurrentDate();
        Date latest = null;
        TimeRecordingPeriod period = null;

        if (!recordings.isEmpty()) {
            for (int i = 0, j = recordings.size(); i < j; i++) {
                TimeRecordingPeriod next = recordings.get(i);
                if (DateUtil.isLaterDay(now, next.getValidTil()) && (latest == null || DateUtil.isLaterDay(next.getValidTil(), latest))) {
                    latest = next.getValidTil();
                    period = next;
                }
            }
            return period;
        }
        return null;
    }
}
