<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<c:if test="${!empty sessionScope.errors}">
<o:logger write="current page has errors" level="debug"/>
<tr><td><fmt:message key="error"/></td><td class="error"><o:out value="${sessionScope.errors}"/></td></tr>
<o:removeErrors/>
</c:if>
