<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="phoneNumberSearchPopupView"/>

<div class="modalBoxHeader">
	<div class="modalBoxHeaderLeft">
		<fmt:message key="phoneNumberSearch"/>
	</div>
	<div class="modalBoxHeaderRight">
		<div class="boxnav">
			<ul>
				<c:forEach var="nav" items="${view.navigation}">
					<li>
						<v:ajaxLink url="${nav.link}" targetElement="${view.name}_popup" title="${nav.title}"><o:img name="${nav.icon}"/></v:ajaxLink>
					</li>
				</c:forEach>
			</ul>
		</div>
	</div>
</div>
<div class="modalBoxData">
	<div class="subcolumns">
		<div class="subcolumn">
			<div class="spacer"></div>
			<c:if test="${!empty sessionScope.error}">
				&nbsp;
				<div class="errormessage">
					<fmt:message key="error"/>: <fmt:message key="${sessionScope.error}"/>
				</div>
                <o:removeErrors/>
			</c:if>
			<table class="table table-striped" style="width: 650px; table-layout: fixed;">
				<thead>
					<tr>
						<th id="phone"><fmt:message key="phone" /></th>
						<th id="type"><fmt:message key="type"/></th>
						<th id="name"><fmt:message key="companySlashName" /></th>
						<th id="city"><fmt:message key="city" /></th>
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${empty view.list}">
							<tr>
								<td valign="top" colspan="4">
									<fmt:message key="searchUnsuccessfulPleaseChangeYourInput" />
								</td>
							</tr>
						</c:when>
						<c:otherwise>
							<c:forEach var="contact" items="${view.list}" varStatus="s">
								<tr>
									<td valign="top">
										<c:choose>
											<c:when test="${contact.fax}">
												<o:out value="${contact.phoneDisplay}" />
											</c:when>
											<c:otherwise>
												<oc:phone provideIcon="true" value="${contact.phoneDisplay}"/>
											</c:otherwise>
										</c:choose>
									</td>
									<td valign="top"><o:out value="${contact.phoneTypeDisplay}" /></td>
									<td valign="top">
										<o:out value="${contact.displayName}" /><c:if test="${!empty contact.parent}"> (<o:out value="${contact.parent}"/>)</c:if>
									</td>
									<td valign="top"><o:out value="${contact.addressDisplay}" /></td>
								</tr>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</tbody>
			</table>
			<div class="spacer"></div>
		</div>
	</div>
</div>