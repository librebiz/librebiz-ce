/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 3, 2011 2:42:25 PM 
 * 
 */
package com.osserp.core.service.hrm;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jdom2.Document;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.OptionsCache;
import com.osserp.common.service.ResourceLocator;
import com.osserp.common.util.DateFormatter;
import com.osserp.common.util.DateUtil;
import com.osserp.common.util.NumberFormatter;

import com.osserp.core.dao.hrm.TimeRecordings;
import com.osserp.core.dms.CoreStylesheetService;
import com.osserp.core.employees.Employee;
import com.osserp.core.hrm.TimeRecording;
import com.osserp.core.hrm.TimeRecordingDay;
import com.osserp.core.hrm.TimeRecordingMonth;
import com.osserp.core.hrm.TimeRecordingPeriod;
import com.osserp.core.hrm.TimeRecordingQueryManager;
import com.osserp.core.service.doc.XmlService;
import com.osserp.core.service.impl.AbstractOutputService;
import com.osserp.core.tasks.TimeRecordingQuerySender;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class TimeRecordingQueryManagerImpl extends AbstractOutputService implements TimeRecordingQueryManager {

    private TimeRecordings timeRecordings = null;
    private TimeRecordingQuerySender timeRecordingQuerySender = null;

    protected TimeRecordingQueryManagerImpl(
            ResourceLocator resourceLocator,
            OptionsCache optionsCache,
            CoreStylesheetService stylesheetService,
            XmlService xmlService,
            TimeRecordings timeRecordings,
            TimeRecordingQuerySender timeRecordingQuerySender) {
        super(resourceLocator, optionsCache, stylesheetService, xmlService);
        this.timeRecordings = timeRecordings;
        this.timeRecordingQuerySender = timeRecordingQuerySender;
    }

    public boolean exists(Employee employee) {
        return timeRecordings.exists(employee);
    }

    public void query(String date, String mail, Long typeId) {
        timeRecordingQuerySender.send(date, mail, typeId);
    }

    public String[] getTimebankQuery(Employee employee, Date date) {
        TimeRecording recording = timeRecordings.createClosingsIfRequired(timeRecordings.find(employee));
        if (recording != null) {
            recording.selectPeriod(date);
            TimeRecordingPeriod period = recording.getSelectedPeriod();
            if (period != null && period.getConfig().isRecordingHours() && period.getConfig().getHoursDaily() != 0) {
                TimeRecordingMonth recordingMonth = period.fetchMonth(DateUtil.createMonth(date));
                if (recordingMonth != null) {
                    String[] result = new String[8];
                    result[0] = employee.getId().toString();
                    result[1] = employee.getLastName();
                    result[2] = employee.getFirstName();
                    result[3] = employee.getCostCenter();
                    Double remainingLeave = recordingMonth.getRemainingMinutesLeave() / period.getConfig().getHoursDaily() / 60;
                    result[4] = NumberFormatter.getValue(remainingLeave, NumberFormatter.DECIMAL);
                    Double carryover = recordingMonth.getMinutesCarryover() / 60D;
                    result[5] = NumberFormatter.getValue(carryover, NumberFormatter.CURRENCY);
                    result[6] = NumberFormatter.getValue(carryover - period.getConfig().getCarryoverHoursMax(), NumberFormatter.CURRENCY);
                    Double lost = recordingMonth.getMinutesLost() / 60D;
                    result[7] = NumberFormatter.getValue(lost, NumberFormatter.CURRENCY);
                    return result;
                }
            }
        }
        return null;
    }

    public List<String[]> getBookingQuery(Employee employee, Date date) {
        List<String[]> list = new ArrayList<String[]>();
        TimeRecording recording = timeRecordings.createClosingsIfRequired(timeRecordings.find(employee));
        if (recording != null) {
            recording.selectPeriod(date);
            TimeRecordingPeriod period = recording.getSelectedPeriod();
            if (period != null && period.getConfig().isRecordingHours() && period.getConfig().getHoursDaily() != 0) {
                period.selectMonth(DateUtil.createMonth(date));
                if (period.getSelectedYear() != null && period.getSelectedYear().getSelectedMonth() != null) {
                    TimeRecordingMonth month = period.getSelectedYear().getSelectedMonth();
                    for (int o = 0, p = month.getDays().size(); o < p; o++) {
                        TimeRecordingDay day = month.getDays().get(o);
                        if (DateUtil.isLaterDay(DateUtil.getCurrentDate(), day.getDate())) {
                            if (day.getIncorrectRecord() != null) {
                                String[] result = new String[5];
                                result[0] = employee.getId().toString();
                                result[1] = employee.getName();
                                result[2] = employee.getCostCenter();
                                result[3] = DateFormatter.getDate(day.getDate());
                                result[4] = DateFormatter.getTime(day.getIncorrectRecord());
                                list.add(result);
                            } else if (day.isNoBooking()) {
                                String[] result = new String[5];
                                result[0] = employee.getId().toString();
                                result[1] = employee.getName();
                                result[2] = employee.getCostCenter();
                                result[3] = DateFormatter.getDate(day.getDate());
                                result[4] = "---";
                                list.add(result);
                            }
                        }
                    }
                }
            }
        }
        return list;
    }

    protected Document createDocument(Employee user, TimeRecording recording) throws ClientException {
        Document doc = xmlService.createDocument(user);
        TimeRecordingPeriod period = recording.getSelectedPeriod();
        if (period == null || period.getSelectedYear() == null) {
            throw new ClientException(ErrorCode.YEAR_SELECTION_MISSING);
        }
        if (period.getSelectedYear().getSelectedMonth() == null) {
            throw new ClientException(ErrorCode.MONTH_SELECTION_MISSING);
        }
        if (period.getConfig().isRecordingHours()) {

        }
        return doc;
    }

}
