/**
 *
 * Copyright (C) 2010, 2013 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Nov 8, 2010 10:29:10 AM 
 * 
 */
package com.osserp.groovy.web.tag

/**
 *
 * @author jg <jg@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public class LinkTag extends AbstractTag {

    String url = ''
    String exit = ''
    String parameters = ''
    boolean confirm = false
    String header = 'irrevocableAction'
    String message = ''

    @Override
    protected String getStartTag() {
        if (!isRequiredRoleAssigned()) {
            return '<span>'
        }
        String createdUrl = createLink()
        StringBuilder buffer = new StringBuilder()
        buffer.append('<a href="')
        if (confirm) {
            buffer.append(createConfirmationLink())
        } else {
            buffer.append(createdUrl)
        }
        buffer.append('"').append(renderCommonAttributes()).append(renderTarget()).append('>')
        return buffer.toString()
    }

    @Override
    protected String getEndTag() {
        isRequiredRoleAssigned() ? '</a>' : '</span>'
    }

    protected String getBaseUrl() {
        createUrl(url)
    }

    protected String createLink() {
        StringBuilder buffer = new StringBuilder()
        buffer.append(baseUrl)
        if (exit || parameters) {
            baseUrl.contains('?') ? buffer.append('&') : buffer.append('?')
            if (exit) {
                buffer.append('exit=').append(exit)
            }
            if (exit && parameters) {
                buffer.append('&')
            }
            buffer.append(parameters)
        }
        return buffer.toString()
    }
    
    //<a href="
    //        javascript:onclick=confirmLink(
    //       'Irrevocable action:\n Confirm delete picture',
    //       '/sales/salesPicture/delete?id=123');
    //        " title="delete">
    protected String createConfirmationLink() {
        StringBuilder buffer = new StringBuilder('javascript:onclick=confirmLink(\'')
        buffer.append(getResourceString(header))
        if (message) {
            buffer.append(':\\n').append(getResourceString(message))
        }
        buffer.append('\',\'').append(createLink()).append('\');')
    }
    
}
