/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 14, 2008 1:56:03 PM 
 * 
 */
package com.osserp.core.model.hrm;

import java.util.Date;

import com.osserp.common.ClientException;
import com.osserp.core.employees.Employee;
import com.osserp.core.hrm.TimeRecord;
import com.osserp.core.hrm.TimeRecordCorrection;
import com.osserp.core.hrm.TimeRecordStatus;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class TimeRecordCorrectionImpl extends AbstractTimeRecord implements TimeRecordCorrection {

    private Date stopValue = null;

    protected TimeRecordCorrectionImpl() {
        super();
    }

    protected TimeRecordCorrectionImpl(Employee user, TimeRecord reference) {
        super(reference.getType(), reference.getStatus(), reference.getId(), user, reference.getValue(), reference.getNote(), reference.isSystemTime());
        this.stopValue = reference.getClosedBy() == null ? null : reference.getClosedBy().getValue();
    }

    private TimeRecordCorrectionImpl(TimeRecordCorrection other) {
        super(other);
        this.stopValue = other.getStopValue();
    }

    @Override
    public Object clone() {//throws CloneNotSupportedException {
        return new TimeRecordCorrectionImpl(this);
    }

    public Date getStopValue() {
        return stopValue;
    }

    public void setStopValue(Date stopValue) {
        this.stopValue = stopValue;
    }

    public TimeRecordCorrection createCorrection(Employee user, Date correction,
            String note, TimeRecordStatus status) throws ClientException {
        throw new IllegalStateException("correction.unchangeable");
    }

    public void resetCorrection(TimeRecordCorrection correction) {
        throw new IllegalStateException("correction.unchangeable");
    }
}
