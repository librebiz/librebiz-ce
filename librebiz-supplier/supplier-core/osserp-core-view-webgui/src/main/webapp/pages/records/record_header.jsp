<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<c:set var="view" value="${sessionScope.recordView}"/>
<c:set var="record" value="${view.record}"/>
<span>
<c:choose><c:when test="${!empty record.bookingType}"><o:out value="${record.bookingType.name}"/></c:when><c:otherwise><o:out value="${record.type.name}"/></c:otherwise></c:choose> <o:out value="${record.number}"/>
/ <fmt:formatDate value="${record.created}"/>
/ <o:ajaxLink linkId="${'createdBy'}" url="${'/employeeInfo.do?id='}${record.createdBy}"><oc:employee initials="true" value="${record.createdBy}"/></o:ajaxLink>
</span>
