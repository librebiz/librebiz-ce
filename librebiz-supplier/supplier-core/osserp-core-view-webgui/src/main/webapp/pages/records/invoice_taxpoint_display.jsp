<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<c:set var="view" value="${sessionScope.recordView}"/>
<c:set var="record" value="${view.record}"/>
<c:if test="${!empty record.taxPoint && empty record.timeOfSupply}">
    <div class="recordLabelValue">
        <fmt:message key="taxPoint"/>:
        <o:date value="${record.taxPoint}"/>
    </div>
</c:if>
<c:if test="${!empty record.timeOfSupply}">
    <div class="recordLabelValue">
        <fmt:message key="timeOfSupply"/>:
        <o:out value="${record.timeOfSupply}"/>
    </div>
</c:if>
