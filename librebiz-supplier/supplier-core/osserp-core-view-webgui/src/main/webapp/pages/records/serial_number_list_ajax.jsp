<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>

<c:set var="view" value="${sessionScope.serialNumberQueryView}"/>
<c:set var="changePermission" value="false"/>
<o:permission role="purchasing,executive" info="permissionChangeSerialNumber">
<c:set var="changePermission" value="true"/>
</o:permission>
<div class="table-responsive table-responsive-default">	
<table class="table table-striped">
<thead>
	<tr>
		<th class="recordType"><fmt:message key="type"/></th>
        <th class="productNumber"><fmt:message key="product"/></th>
        <th class="productName"><fmt:message key="description"/></th>
		<th class="recordNumber"><fmt:message key="recordNumber"/></th>
		<th class="serialNumber"><fmt:message key="serialNumber"/></th>
		<th class="created"><fmt:message key="from"/></th>
	</tr>
</thead>
<tbody>
<c:choose>
<c:when test="${empty view.list}">
	<tr>
		<td valign="top" colspan="6"><fmt:message key="searchUnsuccessfulPleaseChangeYourInput"/></td>
	</tr>
</c:when>
<c:otherwise>
<c:forEach var="obj" items="${view.list}" varStatus="s">
	<tr>
		<c:choose>
		<c:when test="${obj.sales}">
		<td class="recordType">
			<fmt:message key="recordLabelOutgoing"/>
		</td>
		<td class="productNumber">
			<a href="<c:url value="/products.do"><c:param name="method" value="load"/><c:param name="id" value="${obj.productId}"/><c:param name="exit" value="serialNumbers"/></c:url>" title="<fmt:message key="loadProduct"/>">
				<o:out value="${obj.productId}"/>
			</a>
		</td>
        <td class="productName"><o:out value="${obj.name}"/></td>
		<td class="recordNumber">
			<a href="<c:url value="/salesDeliveryNote.do"><c:param name="method" value="display"/><c:param name="readonly" value="true"/><c:param name="exit" value="serialNumbers"/><c:param name="id" value="${obj.reference}"/></c:url>"><o:out value="${obj.recordNumber}"/></a>
		</td>
		</c:when>
		<c:otherwise>
		<td class="recordType">
			<fmt:message key="recordLabelIncoming"/>
		</td>
		<td class="productNumber">
			<a href="<c:url value="/products.do"><c:param name="method" value="load"/><c:param name="id" value="${obj.productId}"/><c:param name="exit" value="serialNumbers"/></c:url>" title="<fmt:message key="loadProduct"/>">
				<o:out value="${obj.productId}"/>
			</a>
		</td>
        <td class="productName"><o:out value="${obj.name}"/></td>
		<td class="recordNumber">
			<a href="<c:url value="/purchaseDeliveryNote.do"><c:param name="method" value="display"/><c:param name="readonly" value="true"/><c:param name="exit" value="serialNumbers"/><c:param name="id" value="${obj.reference}"/></c:url>"><o:out value="${obj.recordNumber}"/></a>
		</td>
		</c:otherwise>
		</c:choose>
		<td class="serialNumber">
		<c:choose>
		<c:when test="${changePermission}">
			<a href="<c:url value="/serialNumbers.do"><c:param name="method" value="select"/><c:param name="id" value="${obj.id}"/></c:url>">
				<o:out value="${obj.serialNumber}"/>
			</a>
		</c:when>
		<c:otherwise>
			<o:out value="${obj.serialNumber}"/>
		</c:otherwise>
		</c:choose>
		</td>
		<td class="created"><o:date value="${obj.created}"/></td>
	</tr>
</c:forEach>
</c:otherwise>
</c:choose>
</tbody>
</table>
</div>
