<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<v:view viewName="${sessionScope.contactInputViewName}" />

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for="street"><fmt:message key="street" /></label>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <v:text name="street" styleClass="form-control" value="${view.bean.address.street}" objectpath="address.street" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for="zipcodeAndCity">
                <oc:systemPropertyEnabled name="zipcodeSearchExternal">
                    <v:ajaxLink title="zipcodeSearch" url="/contacts/zipcodeSearchPopup/forward?callFrom=contactCreate" linkId="zipcodeSearchPopupView">
                        <fmt:message key="zipcode" /> / <fmt:message key="city" />
                    </v:ajaxLink>
                </oc:systemPropertyEnabled>
                <oc:systemPropertyDisabled name="zipcodeSearchExternal">
                    <fmt:message key="zipcode" /> / <fmt:message key="city" />
                </oc:systemPropertyDisabled>
            </label>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <v:text name="zipcode" styleClass="form-control" autoupdate="${view.addressAutocompletionSupport}" value="${view.bean.address.zipcode}" objectpath="address.zipcode" />
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <v:text name="city" styleClass="form-control" value="${view.bean.address.city}" objectpath="address.city" />
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for="country"><fmt:message key="country" /></label>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <oc:select name="country" options="countryNames" autoupdate="${view.addressAutocompletionSupport}" styleClass="form-control" prompt="false" value="${view.bean.address.country}" objectpath="address.country" />
        </div>
    </div>
</div>

<c:choose>
    <c:when test="${view.customFederalState}">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="federalState"><fmt:message key="federalState" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <v:text name="federalStateName" styleClass="form-control" value="${view.bean.address.federalStateName}" objectpath="address.federalStateName" />
                </div>
            </div>
        </div>
    </c:when>
    <c:otherwise>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="federalState"><fmt:message key="federalState" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <oc:select name="federalStateId" styleClass="form-control" options="federalStates" id="federalStateId" prompt="false" value="${view.bean.address.federalStateId}" objectpath="address.federalStateId" />
                </div>
            </div>
        </div>
    </c:otherwise>
</c:choose>
