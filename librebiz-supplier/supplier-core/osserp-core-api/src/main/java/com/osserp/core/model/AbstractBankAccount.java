/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 30.05.2012 
 * 
 */
package com.osserp.core.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.jdom2.Element;

import com.osserp.common.beans.AbstractOption;
import com.osserp.common.dms.DmsDocument;
import com.osserp.common.xml.JDOMUtil;

import com.osserp.core.BankAccount;

/**
 * 
 * @author tn <tn@osserp.com>
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractBankAccount extends AbstractOption implements BankAccount {
    private String nameAddon = null;
    private String ownerName = null;
    private String shortkey = null;
    private String bankAccountNumberIntl = null;
    private String bankIdentificationCodeIntl = null;
    private boolean directDebitAuth = false;
    private Date directDebitAuthDate = null;
    private boolean useInDocumentFooter = false;
    private boolean cashAccount = false;
    private boolean taxAccount = false;
    private Long supplierId;

    private List<DmsDocument> accountDocuments = new ArrayList<DmsDocument>();
    
    /**
     * Default constructor required to implement Serializable.
     */
    protected AbstractBankAccount() {
        super();
    }

    /**
     * Default constructor creating new bank accounts
     * @param reference
     * @param shortkey
     * @param name
     * @param nameAddon
     * @param bankAccountNumberIntl
     * @param bankIdentificationCodeIntl
     * @param useInDocumentFooter
     * @param taxAccount
     * @param createdBy
     */
    protected AbstractBankAccount(
            Long reference,
            String shortkey,
            String name,
            String nameAddon,
            String bankAccountNumberIntl,
            String bankIdentificationCodeIntl,
            boolean useInDocumentFooter,
            boolean taxAccount,
            Long createdBy) {
        super(name, reference, createdBy);
        this.shortkey = shortkey;
        this.nameAddon = nameAddon;
        this.bankAccountNumberIntl = bankAccountNumberIntl;
        this.bankIdentificationCodeIntl = bankIdentificationCodeIntl;
        this.useInDocumentFooter = useInDocumentFooter;
        this.taxAccount = taxAccount;
    }

    /**
     * Default constructor creating new contact bank accounts
     * @param reference
     * @param ownerName
     * @param name
     * @param bankAccountNumberIntl
     * @param bankIdentificationCodeIntl
     * @param createdBy
     * @param directDebitAuth
     * @param directDebitAuthDate
     */
    protected AbstractBankAccount(
            Long reference,
            String ownerName,
            String name,
            String nameAddon,
            String bankAccountNumberIntl,
            String bankIdentificationCodeIntl,
            Long createdBy,
            boolean directDebitAuth,
            Date directDebitAuthDate) {
        this(reference, null, name, nameAddon, bankAccountNumberIntl, 
                bankIdentificationCodeIntl, false, false, createdBy);
        this.directDebitAuth = directDebitAuth;
        this.directDebitAuthDate = directDebitAuthDate;
        this.ownerName = ownerName;
    }

    /**
     * Constructor for clone operations
     * @param other
     */
    protected AbstractBankAccount(BankAccount other) {
        super(other);
        bankAccountNumberIntl = other.getBankAccountNumberIntl();
        bankIdentificationCodeIntl = other.getBankAccountNumberIntl();
        directDebitAuth = other.isDirectDebitAuth();
        directDebitAuthDate = other.getDirectDebitAuthDate();
        ownerName = other.getOwnerName();
        nameAddon = other.getNameAddon();
        useInDocumentFooter = other.isUseInDocumentFooter();
        taxAccount = other.isTaxAccount();
        shortkey = other.getShortkey();
        supplierId = other.getSupplierId();
    }

    @Override
    public Map<String, Object> getMapped() {
        Map<String, Object> map = super.getMapped();
        putIfExists(map, "bankAccountNumberIntl", bankAccountNumberIntl);
        putIfExists(map, "bankIdentificationCodeIntl", bankIdentificationCodeIntl);
        putIfExists(map, "bankName", getName());
        putIfExists(map, "bankNameAddon", getNameAddon());
        putIfExists(map, "shortkey", getShortkey());
        putIfExists(map, "ownerName", getOwnerName());
        putIfExists(map, "referenceId", getReference());
        putIfExists(map, "directDebitAuth", isDirectDebitAuth());
        putIfExists(map, "directDebitAuthDate", getDirectDebitAuthDate());
        putIfExists(map, "useInDocumentFooter", useInDocumentFooter);
        putIfExists(map, "taxAccount", taxAccount);
        putIfExists(map, "supplierId", supplierId);
        return map;
    }

    public String getNameAddon() {
        return nameAddon;
    }

    public void setNameAddon(String nameAddon) {
        this.nameAddon = nameAddon;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getBankAccountNumberIntl() {
        return bankAccountNumberIntl;
    }

    public void setBankAccountNumberIntl(String bankAccountNumberIntl) {
        this.bankAccountNumberIntl = bankAccountNumberIntl;
    }

    public String getBankIdentificationCodeIntl() {
        return bankIdentificationCodeIntl;
    }

    public void setBankIdentificationCodeIntl(String bankIdentificationCodeIntl) {
        this.bankIdentificationCodeIntl = bankIdentificationCodeIntl;
    }

    public boolean isDirectDebitAuth() {
        return directDebitAuth;
    }

    public void setDirectDebitAuth(boolean directDebitAuth) {
        this.directDebitAuth = directDebitAuth;
    }

    public Date getDirectDebitAuthDate() {
        return directDebitAuthDate;
    }

    public void setDirectDebitAuthDate(Date directDebitAuthDate) {
        this.directDebitAuthDate = directDebitAuthDate;
    }

    public boolean isUseInDocumentFooter() {
        return useInDocumentFooter;
    }

    public void setUseInDocumentFooter(boolean useInDocumentFooter) {
        this.useInDocumentFooter = useInDocumentFooter;
    }

    public boolean isCashAccount() {
        return cashAccount;
    }

    public void setCashAccount(boolean cashAccount) {
        this.cashAccount = cashAccount;
    }

    public boolean isTaxAccount() {
        return taxAccount;
    }

    public void setTaxAccount(boolean taxAccount) {
        this.taxAccount = taxAccount;
    }

    public String getShortkey() {
        return shortkey;
    }

    public void setShortkey(String shortkey) {
        this.shortkey = shortkey;
    }

    public Long getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Long supplierId) {
        this.supplierId = supplierId;
    }
    
    public List<DmsDocument> getAccountDocuments() {
        return accountDocuments;
    }

    public void setAccountDocuments(List<DmsDocument> accountDocuments) {
        this.accountDocuments = accountDocuments;
    }

    @Override
    public Element getXML() {
        return getXML("root");
    }

    @Override
    public Element getXML(String rootElementName) {
        Element root = super.getXML(rootElementName);
        root.addContent(JDOMUtil.createElement("bankAccountNumberIntl", bankAccountNumberIntl));
        root.addContent(JDOMUtil.createElement("bankIdentificationCodeIntl", bankIdentificationCodeIntl));
        root.addContent(JDOMUtil.createElement("directDebitAuth", directDebitAuth));
        root.addContent(JDOMUtil.createElement("directDebitAuthDate", directDebitAuthDate));
        root.addContent(JDOMUtil.createElement("ownerName", ownerName));
        root.addContent(JDOMUtil.createElement("nameAddon", nameAddon));
        root.addContent(JDOMUtil.createElement("shortkey", shortkey));
        root.addContent(JDOMUtil.createElement("useInDocumentFooter", useInDocumentFooter));
        root.addContent(JDOMUtil.createElement("cashAccount", cashAccount));
        root.addContent(JDOMUtil.createElement("taxAccount", taxAccount));
        root.addContent(JDOMUtil.createElement("supplierId", supplierId));
        return root;
    }
}
