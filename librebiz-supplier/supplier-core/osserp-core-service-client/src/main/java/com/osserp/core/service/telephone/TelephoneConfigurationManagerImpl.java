/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 1, 2010 1:11:00 PM 
 * 
 */
package com.osserp.core.service.telephone;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.osserp.core.dao.telephone.TelephoneConfigurations;
import com.osserp.core.model.telephone.TelephoneConfigurationImpl;
import com.osserp.core.service.impl.AbstractService;
import com.osserp.core.telephone.Telephone;
import com.osserp.core.telephone.TelephoneConfiguration;
import com.osserp.core.telephone.TelephoneConfigurationChangesetManager;
import com.osserp.core.telephone.TelephoneConfigurationManager;
import com.osserp.core.telephone.TelephoneGroupManager;
import com.osserp.core.telephone.TelephoneManager;
import com.osserp.core.tasks.SfaSubscriberSender;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public class TelephoneConfigurationManagerImpl extends AbstractService implements TelephoneConfigurationManager {

    private TelephoneConfigurations telephoneConfigurationsDao = null;
    private TelephoneManager telephoneManager = null;
    private SfaSubscriberSender sfaSubscriberSender = null;
    private TelephoneConfigurationChangesetManager telephoneConfigurationChangesetManager = null;
    private TelephoneGroupManager telephoneGroupManager = null;

    public TelephoneConfigurationManagerImpl(
            TelephoneConfigurations telephoneConfigurations,
            TelephoneManager telephoneManager,
            TelephoneConfigurationChangesetManager telephoneConfigurationChangesetManager,
            TelephoneGroupManager telephoneGroupManager,
            SfaSubscriberSender sfaSubscriberSender) {
        super();
        this.telephoneConfigurationsDao = telephoneConfigurations;
        this.telephoneManager = telephoneManager;
        this.telephoneConfigurationChangesetManager = telephoneConfigurationChangesetManager;
        this.telephoneGroupManager = telephoneGroupManager;
        this.sfaSubscriberSender = sfaSubscriberSender;
    }

    public TelephoneConfiguration create(String accountCode, String displayName, String emailAddress) {
        return create(null, null, null, accountCode, displayName, emailAddress, null);
    }

    public TelephoneConfiguration create(Long employeeId, String uid, String internal, String accountCode, String displayName, String emailAddress,
            Telephone telephone) {
        TelephoneConfiguration defaultConf = telephoneConfigurationsDao.getDefaultConfig();
        TelephoneConfiguration tc = new TelephoneConfigurationImpl(
                employeeId,
                uid,
                internal,
                (accountCode != null) ? accountCode : defaultConf.getAccountCode(),
                (displayName != null) ? displayName : defaultConf.getDisplayName(),
                (emailAddress != null) ? emailAddress : defaultConf.getEmailAddress(),
                telephone
                );

        //set default config to new TelephoneConfiguration
        tc.setCallForwardDelaySeconds(defaultConf.getCallForwardDelaySeconds());
        tc.setCallForwardEnabled(defaultConf.isCallForwardEnabled());
        tc.setCallForwardIfBusyEnabled(defaultConf.isCallForwardIfBusyEnabled());
        tc.setCallForwardIfBusyTargetPhoneNumber(defaultConf.getCallForwardIfBusyTargetPhoneNumber());
        tc.setCallForwardTargetPhoneNumber(defaultConf.getCallForwardTargetPhoneNumber());
        tc.setParallelCallEnabled(defaultConf.isParallelCallEnabled());
        tc.setParallelCallSetDelaySeconds(defaultConf.getParallelCallSetDelaySeconds());
        tc.setParallelCallTargetPhoneNumber(defaultConf.getParallelCallTargetPhoneNumber());
        tc.setVoiceMailDelaySeconds(defaultConf.getVoiceMailDelaySeconds());
        tc.setVoiceMailEnabled(defaultConf.isVoiceMailEnabled());
        tc.setVoiceMailEnabledIfBusy(defaultConf.isVoiceMailEnabledIfBusy());
        tc.setVoiceMailEnabledIfUnavailable(defaultConf.isVoiceMailEnabledIfUnavailable());
        tc.setTelephoneGroup(defaultConf.getTelephoneGroup());
        save(tc);
        return tc;
    }

    public List<TelephoneConfiguration> getAllTelephoneConfigurations() {
        return telephoneConfigurationsDao.getAllTelephoneConfigurations();
    }

    public List<TelephoneConfiguration> getAllTelephoneConfigurationsTemplates() {
        return telephoneConfigurationsDao.getAllTelephoneConfigurationsTemplates();
    }

    public TelephoneConfiguration find(Long id) {
        return telephoneConfigurationsDao.find(id);
    }

    public TelephoneConfiguration findByEmployeeId(Long id) {
        return telephoneConfigurationsDao.findByEmployeeId(id);
    }

    public List<TelephoneConfiguration> findByReference(Long id) {
        return telephoneConfigurationsDao.findByReference(id);
    }

    public List<TelephoneConfiguration> findByBranchId(Long branchId) {
        return telephoneConfigurationsDao.findByBranchId(branchId);
    }

    public TelephoneConfiguration findByReference(String internal, Long branchId) {
        return telephoneConfigurationsDao.findByReference(internal, branchId);
    }

    public List<TelephoneConfiguration> findByTelephoneGroup(Long groupId) {
        return telephoneConfigurationsDao.findByTelephoneGroup(groupId);
    }

    public TelephoneConfiguration find(Long employeeId, Long branchId) {
        return telephoneConfigurationsDao.find(employeeId, branchId);
    }

    public List<TelephoneConfiguration> search(Long branchId, boolean includeEmployeeTelephoneSystems, boolean onlyTelephoneExchanges) {
        List<TelephoneConfiguration> list = new ArrayList<TelephoneConfiguration>();
        if (branchId != null) {
            list = findByBranchId(branchId);
        } else {
            list = getAllTelephoneConfigurations();
        }
        if (!onlyTelephoneExchanges && !includeEmployeeTelephoneSystems) {
            List<TelephoneConfiguration> tmpList = new ArrayList<TelephoneConfiguration>();
            for (Iterator<TelephoneConfiguration> iterator = list.iterator(); iterator.hasNext();) {
                TelephoneConfiguration tc = iterator.next();
                if (tc.getEmployeeId() == null) {
                    tmpList.add(tc);
                }
            }
            list = tmpList;
        } else if (onlyTelephoneExchanges) {
            List<TelephoneConfiguration> tmpList = new ArrayList<TelephoneConfiguration>();
            for (Iterator<TelephoneConfiguration> iterator = list.iterator(); iterator.hasNext();) {
                TelephoneConfiguration tc = iterator.next();
                if (tc.isTelephoneExchange()) {
                    tmpList.add(tc);
                }
            }
            list = tmpList;
        }
        return list;
    }

    public void resetInternal(Long userId, Long employeeId, String value) {
        TelephoneConfiguration tc = telephoneConfigurationsDao.findByEmployeeId(employeeId);
        if (tc != null) {
            this.telephoneConfigurationChangesetManager.create(userId, tc.getId(), TelephoneConfigurationImpl.INTERNAL, tc.getInternal(), value);
            tc.setInternal(value);
            save(tc);
        }
    }

    public void updateInternal(Long userId, Long id, String value) {
        TelephoneConfiguration tc = telephoneConfigurationsDao.find(id);
        if (tc != null) {
            this.telephoneConfigurationChangesetManager.create(userId, tc.getId(), TelephoneConfigurationImpl.INTERNAL, tc.getInternal(), value);
            tc.setInternal(value);
            save(tc);
        }
    }

    public void updateAccountCode(Long userId, Long id, String value) {
        TelephoneConfiguration tc = telephoneConfigurationsDao.find(id);
        if (tc != null) {
            this.telephoneConfigurationChangesetManager.create(userId, tc.getId(), TelephoneConfigurationImpl.ACCOUNT_CODE, tc.getAccountCode(), value);
            tc.setAccountCode(value);
            save(tc);
        }
    }

    public void updateDisplayName(Long userId, Long id, String value) {
        TelephoneConfiguration tc = telephoneConfigurationsDao.find(id);
        if (tc != null) {
            this.telephoneConfigurationChangesetManager.create(userId, tc.getId(), TelephoneConfigurationImpl.DISPLAY_NAME, tc.getDisplayName(), value);
            tc.setDisplayName(value);
            save(tc);
        }
    }

    public void updateEmailAddress(Long userId, Long id, String value) {
        TelephoneConfiguration tc = telephoneConfigurationsDao.find(id);
        if (tc != null) {
            this.telephoneConfigurationChangesetManager.create(userId, tc.getId(), TelephoneConfigurationImpl.EMAIL_ADDRESS, tc.getEmailAddress(), value);
            tc.setEmailAddress(value);
            save(tc);
        }
    }

    public void updateTelephone(Long userId, Long id, Long telephoneId) {
        TelephoneConfiguration tc = telephoneConfigurationsDao.find(id);
        Telephone telephone = telephoneManager.find(telephoneId);
        if (tc != null) {
            this.telephoneConfigurationChangesetManager.create(userId, tc.getId(), TelephoneConfigurationImpl.TELEPHONE, (tc.getTelephone() != null) ? tc
                    .getTelephone().getMac() : null, (telephone != null) ? telephone.getMac() : null);
            if (tc.isInternalEditable()) {
                tc.setInternal(null);
            }
            tc.setTelephone(telephone);
            save(tc);
        }
    }

    public void updateVoiceMailEnabled(Long userId, Long id, boolean value) {
        TelephoneConfiguration tc = telephoneConfigurationsDao.find(id);
        if (tc != null) {
            this.telephoneConfigurationChangesetManager.create(userId, tc.getId(), TelephoneConfigurationImpl.VOICE_MAIL_ENABLED,
                    (tc.isVoiceMailEnabled()) ? "true" : "false", (value) ? "true" : "false");
            tc.setVoiceMailEnabled(value);
            save(tc);
        }
    }

    public void updateVoiceMailDelaySeconds(Long userId, Long id, Integer value) {
        TelephoneConfiguration tc = telephoneConfigurationsDao.find(id);
        if (tc != null) {
            this.telephoneConfigurationChangesetManager.create(userId, tc.getId(), TelephoneConfigurationImpl.VOICE_MAIL_DELAY_SECONDS, tc
                    .getVoiceMailDelaySeconds().toString(), value.toString());
            tc.setVoiceMailDelaySeconds(value);
            save(tc);
        }
    }

    public void updateVoiceMailEnabledIfBusy(Long userId, Long id, boolean value) {
        TelephoneConfiguration tc = telephoneConfigurationsDao.find(id);
        if (tc != null) {
            this.telephoneConfigurationChangesetManager.create(userId, tc.getId(), TelephoneConfigurationImpl.VOICE_MAIL_ENABLED_IF_BUSY,
                    (tc.isVoiceMailEnabledIfBusy()) ? "true" : "false", (value) ? "true" : "false");
            tc.setVoiceMailEnabledIfBusy(value);
            save(tc);
        }
    }

    public void updateVoiceMailEnabledIfUnavailable(Long userId, Long id, boolean value) {
        TelephoneConfiguration tc = telephoneConfigurationsDao.find(id);
        if (tc != null) {
            this.telephoneConfigurationChangesetManager.create(userId, tc.getId(), TelephoneConfigurationImpl.VOICE_MAIL_ENABLED_IF_UNAVAILABLE,
                    (tc.isVoiceMailEnabledIfUnavailable()) ? "true" : "false", (value) ? "true" : "false");
            tc.setVoiceMailEnabledIfUnavailable(value);
            save(tc);
        }
    }

    public void updateParallelCallTargetPhoneNumber(Long userId, Long id, String value) {
        TelephoneConfiguration tc = telephoneConfigurationsDao.find(id);
        if (tc != null) {
            this.telephoneConfigurationChangesetManager.create(userId, tc.getId(), TelephoneConfigurationImpl.PARALLEL_CALL_TARGET_PHONE_NUMBER,
                    tc.getParallelCallTargetPhoneNumber(), value);
            tc.setParallelCallTargetPhoneNumber(value);
            save(tc);
        }
    }

    public void updateParallelCallEnabled(Long userId, Long id, boolean value) {
        TelephoneConfiguration tc = telephoneConfigurationsDao.find(id);
        if (tc != null) {
            this.telephoneConfigurationChangesetManager.create(userId, tc.getId(), TelephoneConfigurationImpl.PARALLEL_CALL_ENABLED,
                    (tc.isParallelCallEnabled()) ? "true" : "false", (value) ? "true" : "false");
            tc.setParallelCallEnabled(value);
            save(tc);
        }
    }

    public void updateParallelCallSetDelaySeconds(Long userId, Long id, Integer value) {
        TelephoneConfiguration tc = telephoneConfigurationsDao.find(id);
        if (tc != null) {
            this.telephoneConfigurationChangesetManager.create(userId, tc.getId(), TelephoneConfigurationImpl.PARALLEL_CALL_SET_DELAY_SECONDS, tc
                    .getParallelCallSetDelaySeconds().toString(), value.toString());
            tc.setParallelCallSetDelaySeconds(value);
            save(tc);
        }
    }

    public void updateCallForwardTargetPhoneNumber(Long userId, Long id, String value) {
        TelephoneConfiguration tc = telephoneConfigurationsDao.find(id);
        if (tc != null) {
            this.telephoneConfigurationChangesetManager.create(userId, tc.getId(), TelephoneConfigurationImpl.CALL_FORWARD_TARGET_PHONE_NUMBER,
                    tc.getParallelCallTargetPhoneNumber(), value);
            tc.setCallForwardTargetPhoneNumber(value);
            save(tc);
        }
    }

    public void updateCallForwardDelaySeconds(Long userId, Long id, Integer value) {
        TelephoneConfiguration tc = telephoneConfigurationsDao.find(id);
        if (tc != null) {
            this.telephoneConfigurationChangesetManager.create(userId, tc.getId(), TelephoneConfigurationImpl.CALL_FORWARD_DELAY_SECONDS, tc
                    .getCallForwardDelaySeconds().toString(), value.toString());
            tc.setCallForwardDelaySeconds(value);
            save(tc);
        }
    }

    public void updateCallForwardEnabled(Long userId, Long id, boolean value) {
        TelephoneConfiguration tc = telephoneConfigurationsDao.find(id);
        if (tc != null) {
            this.telephoneConfigurationChangesetManager.create(userId, tc.getId(), TelephoneConfigurationImpl.CALL_FORWARD_ENABLED,
                    (tc.isCallForwardEnabled()) ? "true" : "false", (value) ? "true" : "false");
            tc.setCallForwardEnabled(value);
            save(tc);
        }
    }

    public void updateCallForwardIfBusyTargetPhoneNumber(Long userId, Long id, String value) {
        TelephoneConfiguration tc = telephoneConfigurationsDao.find(id);
        if (tc != null) {
            this.telephoneConfigurationChangesetManager.create(userId, tc.getId(), TelephoneConfigurationImpl.CALL_FORWARD_IF_BUSY_TARGET_PHONE_NUMBER,
                    tc.getCallForwardIfBusyTargetPhoneNumber(), value);
            tc.setCallForwardIfBusyTargetPhoneNumber(value);
            save(tc);
        }
    }

    public void updateCallForwardIfBusyEnabled(Long userId, Long id, boolean value) {
        TelephoneConfiguration tc = telephoneConfigurationsDao.find(id);
        if (tc != null) {
            this.telephoneConfigurationChangesetManager.create(userId, tc.getId(), TelephoneConfigurationImpl.CALL_FORWARD_IF_BUSY_ENABLED,
                    (tc.isCallForwardIfBusyEnabled()) ? "true" : "false", (value) ? "true" : "false");
            tc.setCallForwardIfBusyEnabled(value);
            save(tc);
        }
    }

    public void updateTelephoneGroup(Long userId, Long id, Long value) {
        TelephoneConfiguration tc = telephoneConfigurationsDao.find(id);
        if (tc != null) {
            this.telephoneConfigurationChangesetManager.create(userId, tc.getId(), TelephoneConfigurationImpl.TELEPHONE_GROUP,
                    (tc.getTelephoneGroup() != null) ? tc.getTelephoneGroup().getId().toString() : null, (value != null) ? value.toString() : null);
            tc.setTelephoneGroup(telephoneGroupManager.find(value));
            save(tc);
        }
    }

    public void updateAvailable(Long userId, Long id, boolean value) {
        TelephoneConfiguration tc = telephoneConfigurationsDao.find(id);
        if (tc != null) {
            this.telephoneConfigurationChangesetManager.create(userId, tc.getId(), TelephoneConfigurationImpl.AVAILABLE, (tc.isAvailable()) ? "true" : "false",
                    (value) ? "true" : "false");
            tc.setAvailable(value);
            save(tc);
        }
    }

    public void updateBusyOnBusy(Long userId, Long id, boolean value) {
        TelephoneConfiguration tc = telephoneConfigurationsDao.find(id);
        if (tc != null) {
            this.telephoneConfigurationChangesetManager.create(userId, tc.getId(), TelephoneConfigurationImpl.BUSY_ON_BUSY, (tc.isBusyOnBusy()) ? "true"
                    : "false", (value) ? "true" : "false");
            tc.setBusyOnBusy(value);
            save(tc);
        }
    }

    public void updateVoiceMailDeleteAfterEmailSend(Long userId, Long id, boolean value) {
        TelephoneConfiguration tc = telephoneConfigurationsDao.find(id);
        if (tc != null) {
            this.telephoneConfigurationChangesetManager.create(userId, tc.getId(), TelephoneConfigurationImpl.VOICE_MAIL_DELETE_AFTER_EMAIL_SEND,
                    (tc.isVoiceMailDeleteAfterEmailSend()) ? "true" : "false", (value) ? "true" : "false");
            tc.setVoiceMailDeleteAfterEmailSend(value);
            save(tc);
        }
    }

    public void updateVoiceMailSendAsEmail(Long userId, Long id, boolean value) {
        TelephoneConfiguration tc = telephoneConfigurationsDao.find(id);
        if (tc != null) {
            this.telephoneConfigurationChangesetManager.create(userId, tc.getId(), TelephoneConfigurationImpl.VOICE_MAIL_SEND_AS_EMAIL,
                    (tc.isVoiceMailSendAsEmail()) ? "true" : "false", (value) ? "true" : "false");
            tc.setVoiceMailSendAsEmail(value);
            save(tc);
        }
    }

    public void updateTelephoneExchange(Long userId, Long id, boolean value) {
        TelephoneConfiguration tc = telephoneConfigurationsDao.find(id);
        if (tc != null) {
            this.telephoneConfigurationChangesetManager.create(userId, tc.getId(), TelephoneConfigurationImpl.TELEPHONE_EXCHANGE,
                    (tc.isTelephoneExchange()) ? "true" : "false", (value) ? "true" : "false");
            tc.setTelephoneExchange(value);
            save(tc);
        }
    }

    private void save(TelephoneConfiguration tc) {
        this.telephoneConfigurationsDao.save(tc);
        this.syncronize(tc);
    }

    private void syncronize(TelephoneConfiguration tc) {
        if (tc != null) {
            this.sfaSubscriberSender.send(tc.getId());
        }
    }
}
