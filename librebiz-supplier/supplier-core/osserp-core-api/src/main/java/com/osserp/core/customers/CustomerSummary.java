/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 25, 2005 
 * 
 */
package com.osserp.core.customers;

import java.util.List;

import com.osserp.common.Entity;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface CustomerSummary extends Entity {

    /**
     * Provides all planned projects of a customer
     * @return summary of all requests
     */
    List<SalesSummary> getRequests();

    /**
     * Provides the total count of requests
     * @return requestCount
     */
    Integer getRequestCount();

    /**
     * Provides the total count of cancelled requests
     * @return cancelledRequestCount
     */
    Integer getCancelledRequestCount();

    /**
     * Provides the id of the latest request if exists.
     * If a cancelled request found but an older not-canceled request
     * exists, the older not-canceled request id will be returned.   
     * @return request id or null if not exists
     */
    Long getLatestRequestId();

    /**
     * Adds a request to the request list
     * @param summary
     */
    void addRequest(SalesSummary summary);

    /**
     * Provides all running or closed sales of a customer
     * @return summary of all sales
     */
    List<SalesSummary> getSales();

    /**
     * Provides the total count of sales excluding canceled
     * @return salesCount
     */
    Integer getSalesCount();

    /**
     * Provides the total count of cancelled sales
     * @return cancelledSalesCount
     */
    Integer getCancelledSalesCount();

    /**
     * Provides the total count of open sales (status < 100 & not canceled)
     * @return openSalesCount
     */
    Integer getOpenSalesCount();

    /**
     * Provides the id of the latest sales if exists.
     * If a cancelled sales found but an older not-canceled sales
     * exists, the older not-canceled sales id will be returned.   
     * @return sales id or null if not exists
     */
    Long getLatestSalesId();

    /**
     * Adds a sales to the sales list
     * @param summary
     */
    void addSales(SalesSummary summary);

    /**
     * Provides the id of the latest cancelled request if available
     * @return id or null if no cancelled request exists
     */
    Long getLatestCancelledRequestId();

    /**
     * Provides the id of the latest cancelled sales if available
     * @return id or null if no cancelled sales exists
     */
    Long getLatestCancelledSalesId();
}
