/**
 *
 * Copyright (C) 2006, 2012, 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 16, 2016 
 * 
 */
package com.osserp.gui.records

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.core.products.ProductSelection
import com.osserp.core.products.ProductSelectionConfigItem
import com.osserp.core.products.ProductSelectionManager

import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
abstract class AbstractRecordDisplayListView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(AbstractRecordDisplayListView.getName())
    
    protected ProductSelection employeeProductFilter
    
    ProductSelection config
    List<ProductSelectionConfigItem> productSelections = []
    
    Date listStartDate
    Date listStopDate
    String listSearchValue
    String sortBy

    boolean listItems

    void toggleListItems() {
        listItems = !listItems
    }
    
    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            employeeProductFilter = productSelectionManager.getSelection(getDomainEmployee(), true)
            if (employeeProductFilter == null) {
                logger.debug("initRequest: neither default nor dedicated product selection found [user=${domainUser.id}]")
            } else if (!employeeProductFilter.id) {
                logger.debug("initRequest: default product selection found [user=${domainUser.id}]")
            } else {
                logger.debug("initRequest: product selection found [user=${domainUser.id}, filter=${employeeProductFilter.id}]")
            }
        }
    }
    
    protected abstract void reloadList();

    @Override
    void save() {
        resetSearch()
        listStartDate = getDate('listStartDate')
        listStopDate =  getDate('listStopDate')
        listSearchValue = getString('listSearchValue')
        reloadList()
    }
    
    void resetSearchParams() {
        resetSearch()
        reloadList()
    }

    protected void resetSearch() {
        listStartDate = null
        listStopDate = null
        listSearchValue = null
    }
    
    protected ProductSelectionManager getProductSelectionManager() {
        getService(ProductSelectionManager.class.getName())
    }
    
}

