/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.osserp.gui.products

import com.osserp.common.dms.DmsManager
import com.osserp.common.dms.DmsReference
import com.osserp.gui.CoreView

/**
 * 
 * @author cf <cf@osserp.com>
 *
 */
public class ProductDocumentPopupView extends CoreView {

    def groupId

    ProductDocumentPopupView() {
        super()
        dependencies = ['productView']
        enablePopupView()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (!list) {
            groupId = form.getLong('groupId') ?: groupId
            if (groupId) {
                reload()
            }
        }
    }

    void reload() {
        list = dmsManager.findByReference(new DmsReference(groupId, env.productView?.bean?.productId))
    }

    void delete() {
        def id = form.getLong('id')
        def doc = dmsManager.getDocument(id)
        dmsManager.deleteDocument(doc)
        reload()
        env.productView?.reload()
    }

    protected DmsManager getDmsManager() {
        getService(DmsManager.class.getName())
    }
}
