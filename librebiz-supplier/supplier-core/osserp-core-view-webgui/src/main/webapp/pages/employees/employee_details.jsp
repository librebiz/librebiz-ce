<%@ page import="com.osserp.core.contacts.PhoneType" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="contactView" value="${sessionScope.contactView}"/>
<c:set var="contact" value="${contactView.bean}"/>
<c:set var="phoneDev" scope="request"><%= PhoneType.PHONE%></c:set>
<c:set var="mobileDev" scope="request"><%= PhoneType.MOBILE%></c:set>
<c:set var="officeDev" scope="request"><%= PhoneType.OFFICE%></c:set>
<c:set var="internetConfigPermission" value="false"/>
<c:set var="timeRecordingPermission" value="false"/>
<o:permission role="hrm,executive,employee_internet_config" info="permissionInternetConfig">
	<c:set var="internetConfigPermission" scope="request" value="true"/>
</o:permission>
<o:permission role="hrm,executive,time_recording_admin,time_recording_display" info="permissionTimeRecording">
	<c:set var="timeRecordingPermission" scope="request" value="true"/>
</o:permission>
<c:set var="editTelephoneConfigurationPermission" value="false"/>
<o:permission role="telephone_system_admin" info="permissionTelephoneSystemAdmin">
	<c:set var="editTelephoneConfigurationPermission" value="true"/>
</o:permission>
<c:set var="editOwnTelephoneConfigurationPermission" value="false"/>
<o:permission role="telephone_configuration_own_edit" info="permissionTelephoneSystemAdmin">
	<c:set var="editOwnTelephoneConfigurationPermission" value="true"/>
</o:permission>

<c:if test="${contactView.employeeView}">
	<div class="subcolumns">
		<div class="column50l">
			<div class="subcolumnl">
            
				<div class="contentBox">
					<div class="contentBoxHeader">
						<div class="contentBoxHeaderLeft">
                            <div class="subHeaderLeft"> </div>
                            <div class="subHeaderRight">
                                <span style="margin-right: 8px;"><fmt:message key="employee" /></span><o:out value="${contact.id}" />
                            </div>
						</div>
						<div class="contentBoxHeaderRight">
							<div class="boxnav">
								<ul>
                                    <li>
                                        <v:link url="/employees/employeeLetter/forward?exit=/employeeDisplay.do" title="correspondenceTitle">
                                            <o:img name="letterIcon" />
                                        </v:link>
                                    </li>
									<o:permission role="employee_assign,employee_activation,hrm,permission_grant,client_edit" info="permissionChangeEmployeeDetails">
										<c:if test="${!contactView.detailsEditMode && !contact.systemAccount}">
											<li><a href="<c:url value="/employees.do?method=enableDetailsEdit"/>" title="<fmt:message key="changeDetails"/>"><o:img name="writeIcon" /></a></li>
										</c:if>
									</o:permission>
								</ul>
							</div>
						</div>
					</div>
					<div class="contentBoxData">
						<div class="subcolumns">
							<div class="subcolumn">
								<div class="smallSpacer"></div>
								<c:choose>
									<c:when test="${!contactView.detailsEditMode}">
										<table class="valueTable first33">
											<tbody>
												<c:if test="${!empty contact.employeeType}">
													<tr>
														<td><fmt:message key="typeLabel"/></td>
														<td><o:out value="${contact.employeeType.name}"/></td>
													</tr>
												</c:if>
												<tr>
													<td><fmt:message key="status"/></td>
													<td>
														<o:permission role="employee_activation,permission_grant,client_edit" info="permissionEmployeeSetup">
															<c:choose>
																<c:when test="${contact.active}">
																	<c:choose>
																		<c:when test="${!contact.systemAccount}">
																			<a href="javascript:onclick=confirmLink('<fmt:message key="employeeDeactivation"/>:\n<fmt:message key="allAccountsWillBeDeactivated"/>','<c:url value="/employees.do?method=changeActivation"/>');" title="<fmt:message key="employeeActivation"/>">
																				<fmt:message key="activated"/>
																			</a>
																		</c:when>
																		<c:otherwise>
																			<fmt:message key="activated"/>
																		</c:otherwise>
																	</c:choose>
																</c:when>
																<c:otherwise>
																	<c:choose>
																		<c:when test="${!contact.systemAccount}">
																			<a href="<c:url value="/employees.do?method=changeActivation"/>" title="<fmt:message key="employeeActivation"/>">
																				<fmt:message key="deactivated"/>
																			</a>
																		</c:when>
																		<c:otherwise>
																			<fmt:message key="deactivated"/>
																		</c:otherwise>
																	</c:choose>
																</c:otherwise>
															</c:choose>
														</o:permission>
														<o:forbidden role="employee_activation,permission_grant,client_edit">
															<c:choose>
																<c:when test="${contact.active}">
																	<fmt:message key="activated"/>
																</c:when>
																<c:otherwise>
																	<fmt:message key="deactivated"/>
																</c:otherwise>
															</c:choose>
														</o:forbidden>
													</td>
												</tr>
												<%-- system account settings --%>
												<c:if test="${!contact.client and !contact.branchOffice}">
													<c:import url="/pages/employees/employee_account.jsp"/>
												</c:if>
												<%-- roles --%>
												<c:if test="${!contact.systemAccount}">
												<tr>
													<td>
														<o:permission role="hrm,employee_activate,permission_grant,client_edit" info="permissionEditBranch">
                                                            <v:ajaxLink url="/employees/employeeRoleConfig/forward" linkId="employeeRoleConfig">
                                                                <fmt:message key="branch"/>
                                                            </v:ajaxLink>
														</o:permission>
														<o:forbidden role="hrm,employee_activate,permission_grant,client_edit">
															<fmt:message key="branch"/>
														</o:forbidden>
													</td>
													<td>
														<c:choose>
															<c:when test="${empty contact.roleConfigs}">
																<fmt:message key="noGroupsAssigned"/>
															</c:when>
															<c:otherwise>
																<c:forEach var="config" items="${contact.roleConfigs}">
																	<c:choose>
																		<c:when test="${config.defaultRole}">
																			<span class="boldtext">
																				<o:out value="${config.branch.shortkey}"/> - <o:out value="${config.branch.name}"/>:
																			</span><br />
																		</c:when>
																		<c:otherwise>
																			<span><o:out value="${config.branch.shortkey}"/> - <o:out value="${config.branch.name}"/>:</span><br />
																		</c:otherwise>
																	</c:choose>
																	<c:forEach var="role" items="${config.roles}">
																		<c:choose>
																			<c:when test="${role.defaultRole}">
																				<span>- <span class="boldtext"><o:out value="${role.group.name}"/></span></span><br />
																			</c:when>
																			<c:otherwise>
																				<span>- <o:out value="${role.group.name}"/></span><br />
																			</c:otherwise>
																		</c:choose>
																	</c:forEach>
																</c:forEach>
															</c:otherwise>
														</c:choose>
													</td>
												</tr>
												<c:set var="isHrm" value="false"/>
												<o:permission role="hrm,disciplinarian_view,disciplinarian_edit,client_edit,executive" info="permissionViewDisciplinarian">
													<tr>
														<td>
															<o:permission role="hrm,disciplinarian_edit,client_edit" info="permissionEditDisciplinarian">
																<c:set var="isHrm" value="true"/>
																<a href="<v:url value="/employees/disciplinarian/forward?exit=/employeeDisplay.do"/>">
																	<fmt:message key="disciplinarian"/>
																</a>
															</o:permission>
															<o:forbidden role="hrm,disciplinarian_edit,client_edit">
																<fmt:message key="disciplinarian"/>
															</o:forbidden>
														</td>
														<td>
															<c:choose>
																<c:when test="${empty contactView.disciplinarians}">
																	<fmt:message key="noDisciplinariansAssigned"/>
																</c:when>
																<c:otherwise>
																	<c:forEach var="employeeDisciplinarian" items="${contactView.disciplinarians}">
																		<c:choose>
																			<c:when test="${employeeDisciplinarian.disciplinarian.id == contactView.bean.defaultDisciplinarian}">
																				<o:out value="${employeeDisciplinarian.disciplinarian.name}"/> <c:if test="${contactView.disciplinariansCount > 1}">(D)</c:if><br />
																			</c:when>
																			<c:otherwise>
																				<c:choose>
																					<c:when test="${contactView.disciplinariansCount > 1 and isHrm}">
																						<a href="<c:url value="/employees.do"><c:param name="method" value="setDefaultDisciplinarian"/><c:param name="id" value="${employeeDisciplinarian.disciplinarian.id}"/></c:url>" title="<fmt:message key="setAsDefaultDisciplinarian"/>">
																							<o:out value="${employeeDisciplinarian.disciplinarian.name}"/><br />
																						</a>
																					</c:when>
																					<c:otherwise>
																						<o:out value="${employeeDisciplinarian.disciplinarian.name}"/><br />
																					</c:otherwise>
																				</c:choose>
																			</c:otherwise>
																		</c:choose>
																	</c:forEach>
																</c:otherwise>
															</c:choose>
														</td>
													</tr>
												</o:permission>
												<c:if test="${!empty contact.role}">
													<tr>
														<td><fmt:message key="role"/></td>
														<td><o:out value="${contact.role}"/></td>
													</tr>
												</c:if>
												<tr>
													<td><fmt:message key="signatureBy"/></td>
													<td>
														<c:choose>
															<c:when test="${empty contact.signatureSource}">
																<span title="<fmt:message key="role"/>, <fmt:message key="group"/>, <fmt:message key="empty"/>">
																	<fmt:message key="automaticallyLabel"/>
																</span>
															</c:when>
															<c:otherwise>
																<fmt:message key="${contact.signatureSource}"/>
															</c:otherwise>
														</c:choose>
													</td>
												</tr>
												</c:if>
												<c:if test="${!empty contact.costCenter}">
													<tr>
														<td><fmt:message key="costCenter"/></td>
														<td><o:out value="${contact.costCenter}"/></td>
													</tr>
												</c:if>
												<c:if test="${!empty contact.beginDate}">
													<tr>
														<td><fmt:message key="entrance"/></td>
														<td><o:date value="${contact.beginDate}"/></td>
													</tr>
												</c:if>
												<c:if test="${!contact.active and !empty contact.endDate}">
													<tr>
														<td><fmt:message key="until"/></td>
														<td><o:date value="${contact.endDate}"/></td>
													</tr>
												</c:if>
												<c:if test="${!contact.systemAccount}">
													<tr>
														<td><fmt:message key="initials"/></td>
														<td><o:out value="${contact.initials}"/></td>
													</tr>
													<c:import url="/pages/employees/employee_website_sync.jsp"/>
												</c:if>
												<tr>
													<td><fmt:message key="contactDisplay"/></td>
													<td>
														<c:choose>
															<c:when test="${contactView.defaultDisplay}">
																<a href="<c:url value="/employees.do?method=toggleDefaultDisplayGroup"/>" title="<fmt:message key="contactDisplayLoadDescription"/>"><fmt:message key="contactDisplayLoad"/></a>
															</c:when>
															<c:otherwise>
																<a href="<c:url value="/employees.do?method=toggleDefaultDisplayGroup"/>" title="<fmt:message key="contactDisplayDefaultDescription"/>"><fmt:message key="contactDisplayDefault"/></a>
															</c:otherwise>
														</c:choose>
													</td>
												</tr>
											</tbody>
										</table>
									</c:when>
									<c:otherwise>
										<c:import url="/pages/employees/employee_edit.jsp"/>
									</c:otherwise>
								</c:choose>
								<div class="smallSpacer"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="spacer"></div>
			</div>
		</div>
		<div class="column50r">
			<div class="subcolumnr">
				<c:if test="${!contact.systemAccount}">
					<c:import url="/pages/employees/employee_phone.jsp"/>
				</c:if>
				<c:import url="/pages/employees/employee_description.jsp"/>
				<c:if test="${!contact.systemAccount}">
					<c:import url="/pages/employees/employee_config.jsp"/>
				</c:if>
			</div>
		</div>
	</div>
</c:if>
