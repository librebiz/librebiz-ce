/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 15, 2012 2:55:42 AM 
 * 
 */
package com.osserp.gui.mail

import javax.servlet.http.HttpServletRequest

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.common.ClientException
import com.osserp.groovy.web.ViewController

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
@RequestMapping("/mail/mailEditor/*")
@Controller class MailEditorController extends ViewController {
    private static Logger logger = LoggerFactory.getLogger(MailEditorController.class.getName())

    @RequestMapping
    def addRecipients(HttpServletRequest request) {
        logger.debug("addRecipients: invoked [user=${getUserId(request)}]")
        MailEditorView view = getView(request)
        view.addRecipients()
        defaultPage
    }

    @RequestMapping
    def addRecipientsCC(HttpServletRequest request) {
        logger.debug("addRecipientsCC: invoked [user=${getUserId(request)}]")
        MailEditorView view = getView(request)
        view.addRecipientsCC()
        defaultPage
    }

    @RequestMapping
    def addRecipientsBCC(HttpServletRequest request) {
        logger.debug("addRecipientsBCC: invoked [user=${getUserId(request)}]")
        MailEditorView view = getView(request)
        view.addRecipientsBCC()
        defaultPage
    }

    @Override
    @RequestMapping
    def save(HttpServletRequest request) {
        logger.debug("save: invoked [user=${getUserId(request)}]")
        String target = super.save(request)
        MailEditorView view = getView(request)
        if (view.sendMail) {
            return send(request)
        } else {
            return target
        }
    }

    @RequestMapping
    def send(HttpServletRequest request) {
        logger.debug("send: invoked [user=${getUserId(request)}]")
        MailEditorView view = getView(request)
        try {
            String target = view.send()
            if (view.mailMessageContext.errorMessage) {
                saveError(request, view.mailMessageContext.errorMessage)
            }
            logger.debug("send: done [target=${target}]")
            return target
        } catch (ClientException e) {
            saveError(request, e.message)
        }
        defaultPage
    }

    @RequestMapping
    def toggleIgnoreFixedRecipients(HttpServletRequest request) {
        logger.debug("toggleIgnoreFixedRecipients: invoked [user=${getUserId(request)}]")
        MailEditorView view = getView(request)
        view.toggleIgnoreFixedRecipients()
        defaultPage
    }

    @RequestMapping
    def enableRecipientSelectionMode(HttpServletRequest request) {
        logger.debug("enableRecipientSelectionMode: invoked [user=${getUserId(request)}]")
        MailEditorView view = getView(request)
        view.enableRecipientSelectionMode()
        defaultPage
    }

    @RequestMapping
    def disableRecipientSelectionMode(HttpServletRequest request) {
        logger.debug("disableRecipientSelectionMode: invoked [user=${getUserId(request)}]")
        MailEditorView view = getView(request)
        view.disableRecipientSelectionMode()
        defaultPage
    }

    @RequestMapping
    def addSuggestedRecipient(HttpServletRequest request) {
        logger.debug("addSuggestedRecipient: invoked [user=${getUserId(request)}]")
        MailEditorView view = getView(request)
        view.addSuggestedRecipient()
        defaultPage
    }

    @RequestMapping
    def addSuggestedRecipientCC(HttpServletRequest request) {
        logger.debug("addSuggestedRecipientCC: invoked [user=${getUserId(request)}]")
        MailEditorView view = getView(request)
        view.addSuggestedRecipientCC()
        defaultPage
    }
}
