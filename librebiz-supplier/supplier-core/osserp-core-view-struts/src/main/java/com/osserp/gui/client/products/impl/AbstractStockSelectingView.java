/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 15, 2008 9:28:29 PM 
 * 
 */
package com.osserp.gui.client.products.impl;

import static com.osserp.common.ErrorCode.DEFAULT_STOCK_MISSING;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.util.CollectionUtil;

import com.osserp.core.finance.Stock;

import com.osserp.gui.client.impl.AbstractView;
import com.osserp.gui.client.products.StockSelectingView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractStockSelectingView extends AbstractView implements StockSelectingView {
    private static Logger log = LoggerFactory.getLogger(AbstractStockSelectingView.class.getName());
    private List<Stock> availableStocks = new ArrayList<Stock>();
    private Stock selectedStock = null;

    /**
     * Initializes available stocks and preselects selected stock with default stock. View name will be set if annotated and action target is initialized as
     * 'success'.
     */
    protected AbstractStockSelectingView() {
        super();
    }

    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        super.init(request);
        availableStocks = getSystemConfigManager().getAvailableStocks();
        for (int i = 0, j = availableStocks.size(); i < j; i++) {
            Stock next = availableStocks.get(i);
            if (next.isDefaultStock()) {
                this.selectedStock = next;
                if (log.isDebugEnabled()) {
                    log.debug("<init> set default stock as selected [stock=" + selectedStock.getId() + "]");
                }
                break;
            }
        }
        if (selectedStock == null) {
            throw new BackendException(DEFAULT_STOCK_MISSING);
        }
    }

    public List<Stock> getAvailableStocks() {
        return availableStocks;
    }

    protected void setAvailableStocks(List<Stock> availableStocks) {
        this.availableStocks = availableStocks;
    }

    public Stock getSelectedStock() {
        return selectedStock;
    }

    protected void setSelectedStock(Stock selectedStock) {
        this.selectedStock = selectedStock;
    }

    public void selectStock(Long id) {
        this.selectedStock = (Stock) CollectionUtil.getById(availableStocks, id);
        if (log.isDebugEnabled()) {
            log.debug("selectStock() done [stock="
                    + (selectedStock == null ? "null" : selectedStock.getId())
                    + "]");
        }
    }
}
