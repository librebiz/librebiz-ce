/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 27, 2007 4:06:05 PM 
 * 
 */
package com.osserp.core;

import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.core.employees.Employee;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface FcsManager {

    /**
     * Initializes the fcs wastebasket
     * @param businessCase
     */
    void initializeWastebasket(BusinessCase businessCase);

    /**
     * Creates a flow control action list for add operations. The list contains all available flow control actions whose are not already added. If an actions
     * displayEverytime flag is set, the action is always added.
     * @param businessCase
     * @return available flow control actions
     */
    List<FcsAction> createActionList(BusinessCase businessCase);

    /**
     * Checks if an action depends on another action not already set. Method is used if check is required before try to add the action 'cause addFlowControl()
     * invoked these method automatically.
     * @param todos
     * @param selected action to check
     * @throws ClientException if action depends on another action
     */
    void checkDependencies(List<FcsAction> todos, FcsAction selected) throws ClientException;

    /**
     * Provides available flow control closing status 
     * @param type
     * @return closings
     */
    List<FcsClosing> findClosings(Long type);

    /**
     * Creates a new closing status
     * @param user
     * @param name
     * @param type
     * @return new created closing status
     */
    FcsClosing createClosing(Employee user, String name, Long type);

    /**
     * Adds a new flow control action to a business case
     * @param employee
     * @param businessCase
     * @param action of the action we like to add
     * @param created when the action was performed
     * @param note for the action
     * @param headline if action is displayed reverse
     * @param closing optional closing status selection
     * @param ignoreDependencies
     * @throws ClientException if project not exists, created is in the future or note is required and not set
     */
    void addFlowControl(
            Employee employee,
            BusinessCase businessCase,
            FcsAction action,
            Date created,
            String note,
            String headline,
            FcsClosing closing,
            boolean ignoreDependencies) throws ClientException;

    /**
     * Cancels an existing flow control entry.
     * @param employeeId performing cancel action
     * @param businessCase
     * @param item
     * @param note with the reason why cancel is needed
     * @return true if canceled with success
     * @throws ClientException if note is not set
     */
    boolean cancelFlowControl(
            Long employeeId,
            BusinessCase businessCase,
            FcsItem item,
            String note) throws ClientException;

    /**
     * Throws the fcs action of given id to the wastebasket
     * @param employeeId
     * @param businessCase
     * @param templateId
     * @throws ClientException if object does not exist
     */
    void throwFlowControl(
            Long employeeId,
            BusinessCase businessCase,
            Long templateId) throws ClientException;

    /**
     * Provides a list of all actions related to the type of the provided
     * business case. Each actions 'changed' property will be overriden with 
     * the date when the action was executed for the provided business case.
     * The list is used to show a complete status list with the specific dates
     * by a dedicated businessCase. 
     * @param object
     * @return list of all actions with created timestamps.
     */
    List<FcsAction> getActionStatusInfo(BusinessCase object);

    /**
     * Provides the content of the flow control wastebasket for a businessCase
     * @param businessCase
     * @return wastebasket items or empty list
     */
    List<FcsItem> getWastebasket(BusinessCase businessCase);

    /**
     * Restores a flow control action from the wastebasket
     * @param businessCase
     * @param wastebasketId
     */
    void restoreFlowControl(BusinessCase businessCase, Long wastebasketId);

    /**
     * Updates a fcs note entry
     * @param item whose note should be updated
     * @param userName who requests the update operation
     * @param noteUpdate text we add to the note
     */
    void updateFcsNote(FcsItem item, String userName, String noteUpdate);

    /**
     * Resets the cancelled status of businessCase if type and implementation supports this.
     * @param employee
     * @param businessCase
     * @return resetted businessCase
     * @throws ClientException if cancellation not supported or permission denied
     */
    BusinessCase resetCancellation(Employee employee, BusinessCase businessCase) throws ClientException;

    /**
     * Resets the closed status of businessCase if type and implementation supports this 
     * and employee has sufficient permission.
     * @param employee
     * @param businessCase
     * @return reopened businessCase
     * @throws ClientException if reopen not supported or permission denied
     */
    BusinessCase resetClosed(Employee employee, BusinessCase businessCase) throws ClientException;
}
