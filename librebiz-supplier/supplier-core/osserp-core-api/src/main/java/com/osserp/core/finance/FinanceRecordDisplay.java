/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 18, 2008 1:37:12 PM 
 * 
 */
package com.osserp.core.finance;

import java.util.Date;

import com.osserp.common.Option;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface FinanceRecordDisplay extends Option {

    // record values

    /**
     * Indicates if record is an internal record only
     * @return true if internal record
     */
    boolean isInternal();

    /**
     * Indicates if record is unchangeable 
     * @return unchangeable
     */
    boolean isUnchangeable();

    /**
     * Indicates record status completed
     * @return completed
     */
    boolean isCompleted();
    
    /**
     * Provides the record id as formatted number string
     * @return number
     */
    String getNumber();

    /**
     * Provides the reference id as formatted number string
     * @return referenceNumber
     */
    String getReferenceNumber();

    /**
     * Provides the i18n name of the associated type
     * @return 18n type name
     */
    String getTypeName();

    /**
     * Provides the name of the record context, e.g. typeName of record or 
     * typeName of related record if this record is a payment.
     * @return 18n type name
     */
    String getContextName();

    /**
     * Provides the type of record
     * @return recordType
     */
    RecordType getRecordType();

    /**
     * Provides booking type if supported
     * @return bookingType
     */
    BookingType getBookingType();

    /**
     * The id of the currency
     * @return currency id
     */
    Long getCurrency();

    /**
     * Currency key
     * @return currency key
     */
    String getCurrencyKey();

    /**
     * Currency symbol
     * @return currency symbol
     */
    String getCurrencySymbol();

    /**
     * Indicates if record is tax free
     * @return taxFree
     */
    boolean isTaxFree();

    /**
     * Provides the id to the basics of the tax free setting 
     * @return taxFreeId
     */
    Long getTaxFreeId();

    /**
     * Provides the taxpoint date
     * @return taxPoint
     */
    Date getTaxPoint();

    /**
     * Provides the net amount
     * @return netAmount
     */
    Double getNetAmount();

    /**
     * The tax amount
     * @return taxAmount
     */
    Double getTaxAmount();

    /**
     * The reduced tax amount
     * @return reducedTaxAmount
     */
    Double getReducedTaxAmount();

    /**
     * The gross amount
     * @return grossAmount
     */
    Double getGrossAmount();

    /**
     * Provides the record status
     * @return status
     */
    Long getStatus();

    /**
     * Provides the id of the related sales business case (optional)
     * @return sales or null if record not related to a sales case
     */
    Long getSales();

    /**
     * Provides the date the record was created on.
     * @return date created on 
     */
    Date getCreated();

    /**
     * Provides the year part of record date
     * @return year created 
     */
    int getCreatedInYear();

    /**
     * Provides the id of the employee creating the record.
     * @param createdBy id 
     */
    Long getCreatedBy();

    /**
     * Provides the id of a referenced document if available
     * @return documentId or null if non available
     */
    Long getDocumentId();

    // contact values
    
    /**
     * Provides the drawers company id
     * @return company id
     */
    Long getCompany();

    /**
     * Provides the drawers branch office id
     * @return branch id or null if no dedicated office assigned
     */
    Long getBranchId();
    
    /**
     * Provides the id of the related contact of the record recipient.
     * @return contactId
     */
    Long getContactId();

    /**
     * Provides the id of the related contact type (e.g. supplier or customer)
     * @return contactType id
     */
    Long getContactType();

    /**
     * Provides the display name as a combination of contacts name components,
     * e.g. 'lastname' for companies and 'lastname, firstname' for consumers 
     * @return
     */
    String getContactName();

    /**
     * Provides the lastname
     * @return lastname
     */
    String getLastname();

    /**
     * Provides the firstname
     * @return firstname
     */
    String getFirstname();

    /**
     * The address street
     * @return street
     */
    String getStreet();

    /**
     * The address zipcode
     * @return zipcode
     */
    String getZipcode();

    /**
     * The address city
     * @return city
     */
    String getCity();

    // output values 
    
    /**
     * Provides most important values as array. 
     * See valueKeys for details. 
     * @return values
     */
    String[] getValues();

    /**
     * Provides the i18n keys for the array provided by values
     * @return valueKeys keys related to the values array
     */
    String[] getValueKeys();

    
    // accounting export status values
    
    
    /**
     * Provides the id of the related export 
     * @return export id
     */
    Long getExportId();

    /**
     * Date of the record export
     * @return export date
     */
    Date getExportDate();

    /**
     * Indicates if an export is ignorable if record display is mapped to an exportView 
     * @return true if export should be ignored for further workflows 
     * (e.g. historical or imported record)
     */
    boolean isExportIgnorable();
}
