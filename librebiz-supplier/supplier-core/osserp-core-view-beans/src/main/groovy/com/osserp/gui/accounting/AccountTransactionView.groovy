/**
 *
 * Copyright (C) 2015 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 23, 2015
 * 
 */
package com.osserp.gui.accounting

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode

import com.osserp.core.finance.BillingType
import com.osserp.core.finance.CommonPayment
import com.osserp.core.finance.CommonPaymentManager
import com.osserp.core.finance.Payment
import com.osserp.core.suppliers.Supplier
import com.osserp.core.suppliers.SupplierSearch

import com.osserp.groovy.web.MenuItem

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class AccountTransactionView extends AbstractAccountingView {
    private static Logger logger = LoggerFactory.getLogger(AccountTransactionView.class.getName())
    
    List<BillingType> billingTypes = []
    BillingType selectedType
    
    List<CommonPayment> templates = []
    CommonPayment selectedTemplate
    
    boolean missingContactByBookingType = false
    boolean supplierMode = false
    
    AccountTransactionView() {
        super()
        enableAutoreload()
        providesCreateMode()
        enableCompanyPreselection()
        enableBankAccountPreselection()
        providesCustomNavigation()
    }
    
    @Override
    public void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        
        if (forwardRequest) {
            
            Long supplierId = getLong('recipient')
            if (supplierId) {
                selectedContact = supplierSearch.findById(supplierId)
                if (selectedContact) {
                    billingTypes = commonPaymentManager.findSupportedTypes(selectedContact)
                    supplierMode = true
                    reload()
                }
            } else {
                billingTypes = commonPaymentManager.supportedTypes
            }
            if (getBoolean('enableCreate')) {
                try {
                    enableCreateMode()
                } catch (ClientException e) {
                    logger.info("initRequest: failed to enableCreateMode on forward request [message=${e.message}]")
                }
                enableExternalInvocationMode()
            }
            if (!selectedBankAccount && bankAccounts.size() == 1) {
                selectedBankAccount = bankAccounts.get(0)
            }
        }
    }
    
    @Override
    void createCustomNavigation() {
        if (supplierMode) {
            nav.listNavigation = [ exitLink, createLink, homeLink]
        } else {
            nav.listNavigation = [ deselectBankAccountLink, createLink, homeLink]
        }
    }

    @Override
    MenuItem getCreateLink() {
        MenuItem clnk = super.getCreateLink()
        clnk.permissions = "purchasing,${TRANSACTION_CREATE_PERMISSIONS}"
        return clnk
    }

    @Override
    public void enableCreateMode() {
        missingContactByBookingType = false
        templates = commonPaymentManager.templates
        if (!supplierMode) {
            contacts = supplierSearch.findBySimpleBilling()
            if (!contacts) {
                throw new ClientException(ErrorCode.CONTACTS_MISSING_BOOKING_TYPE)
            }
        }
        super.enableCreateMode()
    }
    
    @Override
    public void disableCreateMode() {
        super.disableCreateMode()
        selectedTemplate = null
        selectedType = null
        billingTypes = commonPaymentManager.supportedTypes
        if (!supplierMode) {
            selectedContact = null
        }
    }
    
    @Override
    void selectContact() {
        super.selectContact()
        if (selectedContact instanceof Supplier) {
            Supplier supplier = selectedContact 
            if (!selectedType) {
                billingTypes = commonPaymentManager.findSupportedTypes(supplier)
            }
        } else {
            billingTypes = commonPaymentManager.supportedTypes
        }
    }

    @Override
    void save() {
        if (createMode) {
            boolean tpl = form.getBoolean('template')
            String templateName = null
            if (tpl) {
                templateName = selectedTemplate?.templateName ?: form.getString('templateName')
                if (!templateName && tpl) {
                    throw new ClientException(ErrorCode.TEMPLATE_NAME_REQUIRED)
                }
            }
            boolean taxFree = form.getBoolean('taxFree')
            Long taxFreeId = form.getLong('taxFreeId')
            if (taxFreeId && !taxFree) {
                throw new ClientException(ErrorCode.TAX_FREE_REASON_INVALID)
            }
            String note = form.getString('note')
            if (!note) {
                throw new ClientException(ErrorCode.BOOKING_TEXT_REQUIRED)
            }  
            Payment obj = commonPaymentManager.create(
                domainEmployee,
                selectedContact,
                selectedBranch,
                selectedBankAccount,
                selectedType?.id,
                selectedCurrency,
                form.getDecimal('amount'),
                form.getDate('recordDate'),
                form.getDate('paid'),
                form.getString('note'),
                form.getBoolean('reducedTax'),
                taxFree,
                taxFreeId,
                tpl,
                templateName)
            if (tpl && selectedTemplate) {
                commonPaymentManager.disableTemplate(selectedTemplate)
            }
            disableCreateMode()
            reload()
            logger.debug("save: payment created [id=${obj?.id}]")
        } else if (editMode) {
            // TODO add an update method to enable corrections
            // use delete method until then
        }
    }
    
    void copy() {
        Long id = form.getLong('id')
        CommonPayment src = list.find { it.id == id }
        if (!src && id) {
            src = commonPaymentManager.getPayment(id)
            if (src && form.getString('exit')) {
                enableExternalInvocationMode()
            }
        }
        initCopy(src)
    }

    void initCopy(CommonPayment src) {
        if (src) {
            logger.debug("initCopy: invoked [id=${src?.id}, company=${src.company}, branch=${src.branchId}]")
            selectedContact = src.contact
            selectCompany(src.company, src.branchId)
            selectedType = src.type
            selectedCurrency = src.currency
            form.setValue('amount', src.amount)
            form.setValue('note', src.note)
            form.setValue('reducedTax', src.reducedTax)
            form.setValue('taxFree', src.taxFree)
            if (src.taxFree) {
                form.setValue('taxFreeId', src.taxFreeId)
            }
            preselectSelectedBankAccount(src.bankAccountId)
            super.enableCreateMode()
            logger.debug("initCopy: done [id=${src?.id}]")
        } else {
            logger.warn('initCopy: missing src object!')
        }
    }
    
    @Override
    protected void selectSelectedBankAccount(Long id) {
        super.selectSelectedBankAccount(id)
        reload()
    }

    void selectTemplate() {
        Long id = form.getLong('id')
        if (id) {
            selectedTemplate = templates.find { it.id == id }
            if (selectedTemplate) {
                // set values
                form.setValue('amount', selectedTemplate.amount)
                form.setValue('note', selectedTemplate.note)
                if (selectedTemplate.taxFree) {
                    form.setValue('taxFree', 'true')
                }
                if (selectedTemplate.taxFreeId) {
                    form.setValue('taxFreeId', selectedTemplate.taxFreeId)
                }
                selectedType = selectedTemplate.type
                if (selectedType?.salary) {
                    contacts = supplierSearch.salaryAccounts
                } else if (selectedType) {
                    contacts = supplierSearch.simpleBillingAccounts
                } else {
                    contacts = supplierSearch.findBySimpleBilling()
                }
                if (!selectedContact) {
                    selectedContact = selectedTemplate.contact
                }
            }

        } else {
            selectedTemplate = null
            selectedType = null
            selectedContact = null
            if (!supplierMode) {
                contacts = supplierSearch.findBySimpleBilling()
            }
            billingTypes = commonPaymentManager.supportedTypes
        }
    }
    
    void selectType() {
        Long id = form.getLong('id')
        if (id) {
            selectedType = billingTypes.find { it.id == id }
            
            if (!selectedContact) {
                if (selectedType?.salary) {
                    contacts = supplierSearch.salaryAccounts
                } else {
                    contacts = supplierSearch.simpleBillingAccounts
                }
                if (!contacts) {
                    disableCreateMode()
                    missingContactByBookingType = true
                    throw new ClientException(ErrorCode.CONTACTS_MISSING_BOOKING_TYPE)
                }
            }

        } else {
            selectedType = null
            if (!supplierMode) {
                contacts = supplierSearch.findBySimpleBilling()
            }
        }
    }

    void delete() {
        Long id = form.getLong('id')
        if (id) {
            Payment selected = list.find { it.id == id }
            if (selected) {
                commonPaymentManager.delete(selected)
            }
            
        } else if (bean instanceof Payment) {
            commonPaymentManager.delete(bean)
        }
        // gui may provide delete icon in editMode
        disableEditMode()
        reload()
    }

    @Override
    void reload() {
        if (selectedBankAccount && !supplierMode) {
            
            list = commonPaymentManager.getByBankAccount(selectedBankAccount)
            if (filterByContactEnabled) {
                def tmplst = []
                list.each {
                    if (filterByContactMatch(it.contactId)) {
                        tmplst << it
                    }
                }
                list = tmplst
            }
            logger.debug("reload: done [payments=${list?.size()}]")
            
        } else if (supplierMode && selectedContact) {
        
            list = commonPaymentManager.getByContact(selectedContact)
            
        } else {
            logger.debug('reload: list reset done, selectedBankAccount missing')
            list = []
        }
        missingContactByBookingType = false
    }
    
    protected CommonPaymentManager getCommonPaymentManager() {
        getService(CommonPaymentManager.class.getName())
    }
    
    protected SupplierSearch getSupplierSearch() {
        getService(SupplierSearch.class.getName())
    }
}
