/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 20, 2008 12:29:35 PM 
 * 
 */
package com.osserp.core.service.doc;

import java.io.StringReader;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.dms.DmsConfig;
import com.osserp.common.dms.DmsDocument;
import com.osserp.common.dms.DmsManager;
import com.osserp.common.service.ResourceLocator;
import com.osserp.common.service.TemplateManager;
import com.osserp.common.service.impl.StylesheetServiceImpl;
import com.osserp.common.util.FileUtil;

import com.osserp.core.BusinessTemplate;
import com.osserp.core.dms.CoreStylesheetService;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordType;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.system.SystemCompanyManager;
import com.osserp.core.system.SystemConfigManager;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CoreStylesheetServiceImpl extends StylesheetServiceImpl implements CoreStylesheetService {
    private static Logger log = LoggerFactory.getLogger(CoreStylesheetServiceImpl.class.getName());

    private SystemConfigManager systemConfigManager = null;
    private SystemCompanyManager systemCompanyManager = null;
    private DmsManager dmsManager = null;

    protected CoreStylesheetServiceImpl(
            ResourceLocator resourceLocator,
            TemplateManager templateManager,
            String userConfigFile,
            SystemConfigManager systemConfigManager,
            SystemCompanyManager systemCompanyManager,
            DmsManager dmsManager) {
        super(resourceLocator, templateManager, userConfigFile);
        this.systemConfigManager = systemConfigManager;
        this.systemCompanyManager = systemCompanyManager;
        this.dmsManager = dmsManager;
    }

    @Override
    public Source getStylesheetSource(String name) {
        return getStylesheetSource(name, Constants.HEADQUARTER, Constants.DEFAULT_LOCALE_OBJECT);
    }

    @Override
    protected boolean isDocumentOutputDebugMap() {
        return super.isDocumentOutputDebugMap()
                || (systemConfigManager != null
                && systemConfigManager.isSystemPropertyEnabled("documentOutputDebugMap"));
    }

    @Override
    protected boolean isPropertyEnabled(String name) {
        return (systemConfigManager != null && systemConfigManager.isSystemPropertyEnabled(name));
    }

    public Source getStylesheetSource(Record record, Map<String, Object> templateOptions) throws ClientException {
        try {
            String text = getStylesheetText(record, templateOptions);
            if (log.isDebugEnabled() && isSheetLoggingEnabled()) {
                log.debug(text);
            }
            StreamSource sheet = new StreamSource(new StringReader(text));
            return sheet;
        } catch (Exception e) {
            if (log.isDebugEnabled()) {
                log.debug("getStylesheetSource() caught exception [message=" + e.getMessage() + "]", e);
            }
            return null;
        }
    }

    public String getStylesheetText(Record record, Map<String, Object> templateOptions) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("getStylesheetText() invoked [record=" + (record == null ? "null" : (record.getId())
                    + ", type=" + record.getType().getId()) + "]");
        }
        if (record == null) {
            throw new IllegalArgumentException("given record was null");
        }
        if (record.getCompany() == null || record.getBranchId() == null) {
            throw new ClientException("company/branch missing");
        }
        try {
            Map<String, Object> values = getMap(record.getBranchId(), 
                    record.getLanguage() == null ?
                            Constants.DEFAULT_LOCALE_OBJECT :
                                new Locale(record.getLanguage()),
                    templateOptions);
            String formatConditions = systemConfigManager.isSystemPropertyEnabled(
                    "documentConditionsFormatted") ? "true" : "false";
            values.put("recordConditionsFormatted", formatConditions);

            if (log.isDebugEnabled()) {
                log.debug("getStylesheetText() value map initialized");
            }
            return createText(record, values);
        } catch (Exception e) {
            if (log.isDebugEnabled()) {
                log.debug("getStylesheetText() caught exception [message=" + e.getMessage() + "]", e);
            }
            return null;
        }
    }

    protected String createText(Record record, Map<String, Object> values) {
        BranchOffice office = fetchBranch(record.getBranchId());
        if (office == null || office.isDefaultTemplateSelection()) {
            // no office found, no thirdparty or thirdparty without templates
            if (log.isDebugEnabled() && office != null && office.isThirdparty()) {
                log.debug("createText() found thirdparty using default sheets [branch="
                        + office.getId() + ", customTemplates=false]");
            }
            return createDefaultText(record.getType(), values);
        }
        // /xsl/branchs/${branch.shortname} by default
        String customPath = getCustomTemplatePath(office);
        
        if (log.isDebugEnabled()) {
            log.debug("createText() found custom template config [branch="
                    + office.getId() + ", customTemplatePath=" + customPath + "]");
        }
        
        StringBuilder path = new StringBuilder(customPath);
        if (!office.isRecordTemplatesDefault()) {
            if (record.getType().getResourceKey() != null) {
                path.append("/").append(record.getType().getResourceKey());
            } else {
                path.append("/record_").append(record.getType().getId());
            }
            String text = templateManager.createText(
                    TemplateManager.createFilename(path.toString()),
                    addDefaults(record.getType(), values));
            return text;
        }
        String styleSheetName = TemplateManager.createFilename(
                path.toString() + "/records/xsl_rtp_" + record.getType().getId().toString());
        putIfExists(values, "styleSheetName", styleSheetName);
        if (log.isDebugEnabled()) {
            log.debug("getStylesheetSource() added stylesheet name [value=" + styleSheetName + "]");
        }
        String text = templateManager.createText(
                path.append("/records/xsl_record_template").toString(), values);
        return text;
    }
    
    protected String createDefaultText(RecordType recordType, Map<String, Object> values) {
        String styleSheetName = TemplateManager.createFilename(
                "xsl/records/xsl_rtp_" + recordType.getId().toString());
        putIfExists(values, "styleSheetName", styleSheetName);
        if (log.isDebugEnabled()) {
            log.debug("getStylesheetSource() added stylesheet name [value=" + styleSheetName + "]");
        }
        String text = templateManager.createText(
                "xsl/records/xsl_record_template", addDefaults(recordType, values));
        return text;
    }
    
    protected Map<String, Object> addDefaults(RecordType recordType, Map<String, Object> values) {
        if (recordType != null && systemConfigManager != null) {
            if (recordType.isAddTermsAndConditionsOnPrint()
                    && systemConfigManager.isSystemPropertyEnabled("recordTermsAndConditionsAvailable")) {
                values.put("addTermsAndConditions", "true");
            } else {
                values.put("addTermsAndConditions", "false");
            }
            if (recordType.isAddRightToCancelOnPrint()
                    && systemConfigManager.isSystemPropertyEnabled("recordRightToCancelAvailable")) {
                values.put("addRightToCancel", "true");
            } else {
                values.put("addRightToCancel", "false");
            }
        }
        return values;
    }
    
    protected BranchOffice fetchBranch(Long id) {
        return (isNotSet(id) ? null : systemConfigManager.getBranch(id));
    }

    public Source getStylesheetSource(String name, Long branchId, Locale locale) {
        Map<String, Object> values = getMap(branchId, locale, new HashMap<>());
        String text = templateManager.createText(name, values);
        if (log.isDebugEnabled() && isSheetLoggingEnabled()) {
            //logs the (generated) XSLT Stylesheet
            log.debug("getStylesheetSource() done [name=" + name + ", branch=" + branchId
                    + ", locale=" + (locale == null ? "null" : locale.getDisplayName()) + "]\n");
            log.debug(text);
        }
        StreamSource sheet = new StreamSource(new StringReader(text));
        return sheet;
    }
    
    protected boolean isSheetLoggingEnabled() {
        return systemConfigManager.isSystemPropertyEnabled("documentOutputDebugXSL");
    }
    
    protected boolean isXmlLoggingEnabled() {
        return systemConfigManager.isSystemPropertyEnabled("documentOutputDebugXML");
    }

    public Source getStylesheetSource(BusinessTemplate template, Long branchId, Locale locale) {
        assert template != null;
        if (isNotSet(template.getTemplateId())) {
            log.warn("getStylesheetSource() businessTemplate does not provide templateId [id="
                    + template.getId() + "]");
            return null;
        }
        DmsDocument document = dmsManager.findDocument(template.getTemplateId());
        if (document == null) {
            log.warn("getStylesheetSource() templateId provided by businessTemplate does not exist [id="
                    + template.getId() + ", templateId=" + template.getTemplateId() + "]");
            return null;
        }
        return getStylesheetSource(document.getFileName(), branchId, locale);
    }

    
    protected Map<String, Object> getMap(Long branchId, Locale locale, Map<String, Object> templateOptions) {
        if (log.isDebugEnabled()) {
            log.debug("getMap() invoked [branch=" + branchId + ", locale="
                    + (locale == null ? "null" : locale.getLanguage()) + "]");
        }
        Map<String, Object> values = (branchId != null && systemConfigManager != null)
                ? systemConfigManager.getPrintOptions(branchId) : new HashMap<>();

        if (systemCompanyManager != null) {
            BranchOffice office = systemConfigManager.getBranch(branchId);
            String logoPath = systemCompanyManager.getLogoPath(office);
            if (isSet(logoPath)) {
                if (log.isDebugEnabled()) {
                    log.debug("getMap() added logo path [file=" + logoPath + "]");
                }
                values.put("logoPath", logoPath);
            }
        }
        if (!templateOptions.isEmpty()) {
            Set<String> keys = templateOptions.keySet();
            for(String nextKey : keys) {
                values.put(nextKey, templateOptions.get(nextKey));
            }
        }
        return completeMap(values, locale);
    }
    
    public void createCustomTemplatePathIfRequired(BranchOffice office) {
        if (office != null && office.getShortname() != null) {
            String tplPath = getCustomTemplatePath(office);
            if (isSet(tplPath)) {
                StringBuilder dir = new StringBuilder(getTemplateRoot());
                dir.append("/").append(tplPath);
                tplPath = dir.toString();
                FileUtil.createDir(tplPath);
            }
        }
    }
    
    /**
     * Creates custom path by system property and shortname of branch
     * @param office
     * @return path to custom templates
     */
    protected String getCustomTemplatePath(BranchOffice office) {
        StringBuilder result = new StringBuilder(fetchCustomTemplatePath());
        if (office != null && isSet(office.getShortname())) {
            result.append("/").append(office.getShortname());
        }
        return result.toString();
    }
    
    protected final String fetchCustomTemplatePath() {
        String result = null;
        if (systemConfigManager != null) {
            result = systemConfigManager.getSystemProperty(DmsConfig.CUSTOM_TEMPLATE_PATH_PROPERTY);
            if (isNotSet(result)) {
                log.warn("getCustomTemplatePath() system property not found [name="
                        + DmsConfig.CUSTOM_TEMPLATE_PATH_PROPERTY + "]");
            }
        }
        return isNotSet(result) ? "" : result;
    }
}
