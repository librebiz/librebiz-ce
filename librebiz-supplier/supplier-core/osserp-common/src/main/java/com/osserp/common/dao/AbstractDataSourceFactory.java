/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 07.10.2004 
 * 
 */
package com.osserp.common.dao;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.util.PropertyUtil;
import com.osserp.common.util.StringUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractDataSourceFactory implements DataSourceFactory {
    private static Logger log = LoggerFactory.getLogger(AbstractDataSourceFactory.class.getName());

    protected Map<String, Object> properties = new HashMap<String, Object>();
    protected DataSource ds = null;

    protected AbstractDataSourceFactory() throws Exception {
        initProperties();
        initDataSource();
    }

    public DataSource getDataSource() {
        return ds;
    }

    public Map<String, Object> getProperties() {
        return properties;
    }

    public void setProperty(String name, String value) {
        properties.put(name, value);
    }

    /**
     * Implementing class must implement this method to initialize the datasource properly.
     * @throws Exception e
     */
    protected abstract void initDataSource() throws Exception;

    /**
     * Reads the specified property file and puts the key/name pairs to our table map
     * @param propertyFile
     * @throws Exception if something goes wrong
     */
    private void initProperties() throws Exception {
        properties = new HashMap<String, Object>();
        String configFile = StringUtil.replace(getClass().getName(), ".", "/") + ".properties";
        Properties props = PropertyUtil.getProperties(configFile);
        if (props.isEmpty()) {
            log.warn("initProperties() properties are empty!");
        }
        @SuppressWarnings("rawtypes")
        Enumeration e = props.keys();
        while (e.hasMoreElements()) {
            String s = (String) e.nextElement();
            properties.put(s, props.get(s));
        }
    }

    protected String getString(String key) {
        return properties == null ? "" : (String) properties.get(key);
    }

    protected int getInt(String key) {
        String value = properties == null ? "" : (String) properties.get(key);
        return "".equals(value) ? 0 : Integer.valueOf(value).intValue();
    }

}
