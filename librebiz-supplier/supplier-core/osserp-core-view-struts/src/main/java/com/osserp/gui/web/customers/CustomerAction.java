/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 03-Jun-2006 21:52:50 
 * 
 */
package com.osserp.gui.web.customers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.struts.StrutsForm;
import com.osserp.gui.client.contacts.CustomerView;
import com.osserp.gui.web.contacts.AbstractContactViewAwareAction;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CustomerAction extends AbstractContactViewAwareAction {
    private static Logger log = LoggerFactory.getLogger(CustomerAction.class.getName());

    /**
     * Enables sales listing mode on current view
     * @param mapping
     * @param form
     * @param httpRequest
     * @param httpResponse
     * @return forward
     * @throws Exception
     */
    public ActionForward enableSalesListing(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse)
            throws Exception {

        HttpSession session = httpRequest.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableSalesListing() request from " + getUser(session).getId());
        }
        CustomerView view = getCustomerView(session);
        boolean cancelled = RequestUtil.getBoolean(httpRequest, "cancelled");
        boolean closed = RequestUtil.getBoolean(httpRequest, "closed");
        
        if (cancelled && isSet(view.getSummary().getCancelledSalesCount())
                && view.getSummary().getCancelledSalesCount() == 1) {
            view.disableAllModes();
            ActionRedirect redirect = new ActionRedirect(mapping.findForwardConfig("forwardSales"));
            redirect.addParameter("method", "loadSales");
            redirect.addParameter("id", view.getSummary().getLatestCancelledSalesId());
            redirect.addParameter("exit", "customer");
            return redirect;
        }
        view.enableSalesListingMode(cancelled, closed);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Enables request listing mode on current view
     * @param mapping
     * @param form
     * @param httpRequest
     * @param httpResponse
     * @return forward
     * @throws Exception
     */
    public ActionForward enableRequestListing(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse)
            throws Exception {

        HttpSession session = httpRequest.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableRequestListing() request from " + getUser(session).getId());
        }
        CustomerView view = getCustomerView(session);
        view.enableRequestListingMode(RequestUtil.getBoolean(httpRequest, "cancelled"));
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Forwards to a requestEnables request listing mode on current view
     * @param mapping
     * @param form
     * @param httpRequest
     * @param httpResponse
     * @return forward
     * @throws Exception
     */
    public ActionForward forwardRequest(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse)
            throws Exception {

        HttpSession session = httpRequest.getSession();
        if (log.isDebugEnabled()) {
            log.debug("forwardRequest() request from " + getUser(session).getId());
        }
        Long id = RequestUtil.getId(httpRequest);
        ActionRedirect redirect = new ActionRedirect(mapping.findForwardConfig("forwardRequest"));
        redirect.addParameter("method", "select");
        redirect.addParameter("id", id);
        redirect.addParameter("exit", "customer");
        return redirect;
    }

    /**
     * Forwards to a requestEnables request listing mode on current view
     * @param mapping
     * @param form
     * @param httpRequest
     * @param httpResponse
     * @return forward
     * @throws Exception
     */
    public ActionForward forwardSales(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse)
            throws Exception {

        HttpSession session = httpRequest.getSession();
        if (log.isDebugEnabled()) {
            log.debug("forwardSales() request from " + getUser(session).getId());
        }
        Long id = RequestUtil.getId(httpRequest);
        ActionRedirect redirect = new ActionRedirect(mapping.findForwardConfig("forwardSales"));
        redirect.addParameter("method", "loadSales");
        redirect.addParameter("id", id);
        redirect.addParameter("exit", "customer");
        return redirect;
    }

    /**
     * Enables request listing mode on current view
     * @param mapping
     * @param form
     * @param httpRequest
     * @param httpResponse
     * @return forward
     * @throws Exception
     */
    public ActionForward toggleBusinessCaseDisplayMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse)
            throws Exception {

        HttpSession session = httpRequest.getSession();
        if (log.isDebugEnabled()) {
            log.debug("toggleBusinessCaseDisplayMode() request from " + getUser(session).getId());
        }
        CustomerView view = getCustomerView(session);
        view.toggleIncludeAllBusinessCasesMode();
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Enables request listing mode on current view
     * @param mapping
     * @param form
     * @param httpRequest
     * @param httpResponse
     * @return forward
     * @throws Exception
     */
    public ActionForward disableAllModes(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse)
            throws Exception {

        HttpSession session = httpRequest.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disableAllModes() request from " + getUser(session).getId());
        }
        CustomerView view = getCustomerView(session);
        view.disableAllModes();
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Enables details edit mode on current view
     * @param mapping
     * @param form
     * @param httpRequest
     * @param httpResponse
     * @return forward
     * @throws Exception
     */
    public ActionForward enableDetailsEdit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse)
            throws Exception {

        HttpSession session = httpRequest.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableDetailsEdit() request from " + getUser(session).getId());
        }
        CustomerView view = getCustomerView(session);
        view.setDetailsEditMode(true);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Disables details edit mode on current view
     * @param mapping
     * @param form
     * @param httpRequest
     * @param httpResponse
     * @return forward
     * @throws Exception
     */
    public ActionForward disableDetailsEdit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse)
            throws Exception {

        HttpSession session = httpRequest.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disableDetailsEdit() request from " + getUser(session).getId());
        }
        CustomerView view = getCustomerView(session);
        view.setDetailsEditMode(false);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Updates details
     * @param mapping
     * @param form
     * @param httpRequest
     * @param httpResponse
     * @return forward
     * @throws Exception
     */
    public ActionForward updateDetails(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse)
            throws Exception {

        HttpSession session = httpRequest.getSession();
        if (log.isDebugEnabled()) {
            log.debug("updateDetails() request from " + getUser(session).getId());
        }
        CustomerView view = getCustomerView(session);
        try {
            view.updateDetails(new StrutsForm(form));
        } catch (ClientException c) {
            saveError(httpRequest, c.getMessage());
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Enables discount edit mode on current view
     * @param mapping
     * @param form
     * @param httpRequest
     * @param httpResponse
     * @return forward
     * @throws Exception
     */
    public ActionForward enableDiscountEdit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse)
            throws Exception {

        HttpSession session = httpRequest.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableDiscountEdit() request from " + getUser(session).getId());
        }
        CustomerView view = getCustomerView(session);
        view.setDiscountEditMode(true);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Disables discount edit mode on current view
     * @param mapping
     * @param form
     * @param httpRequest
     * @param httpResponse
     * @return forward
     * @throws Exception
     */
    public ActionForward disableDiscountEdit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse)
            throws Exception {

        HttpSession session = httpRequest.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disableDiscountEdit() request from " + getUser(session).getId());
        }
        CustomerView view = getCustomerView(session);
        view.setDiscountEditMode(false);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Updates discount
     * @param mapping
     * @param form
     * @param httpRequest
     * @param httpResponse
     * @return forward
     * @throws Exception
     */
    public ActionForward updateDiscount(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse)
            throws Exception {

        HttpSession session = httpRequest.getSession();
        if (log.isDebugEnabled()) {
            log.debug("updateDiscount() request from " + getUser(session).getId());
        }
        CustomerView view = getCustomerView(session);
        try {
            view.updateDiscount(new StrutsForm(form));
        } catch (ClientException c) {
            saveError(httpRequest, c.getMessage());
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Enables payment agreement edit mode on current view
     * @param mapping
     * @param form
     * @param httpRequest
     * @param httpResponse
     * @return forward
     * @throws Exception
     */
    public ActionForward enablePaymentAgreementEdit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse)
            throws Exception {

        HttpSession session = httpRequest.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enablePaymentAgreementEdit() request from " + getUser(session).getId());
        }
        CustomerView view = getCustomerView(session);
        view.setPaymentAgreementEditMode(true);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Disables payment agreement edit mode on current view
     * @param mapping
     * @param form
     * @param httpRequest
     * @param httpResponse
     * @return forward
     * @throws Exception
     */
    public ActionForward disablePaymentAgreementEdit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse)
            throws Exception {

        HttpSession session = httpRequest.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disablePaymentAgreementEdit() request from " + getUser(session).getId());
        }
        CustomerView view = getCustomerView(session);
        view.setPaymentAgreementEditMode(false);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Updates payment agreement
     * @param mapping
     * @param form
     * @param httpRequest
     * @param httpResponse
     * @return forward
     * @throws Exception
     */
    public ActionForward updatePaymentAgreement(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse)
            throws Exception {

        HttpSession session = httpRequest.getSession();
        if (log.isDebugEnabled()) {
            log.debug("updatePaymentAgreement() request from " + getUser(session).getId());
        }
        CustomerView view = getCustomerView(session);
        try {
            view.updatePaymentAgreement(new StrutsForm(form));
        } catch (ClientException c) {
            saveError(httpRequest, c.getMessage());
        }
        return mapping.findForward(view.getActionTarget());
    }

    protected CustomerView fetchCustomerView(HttpSession session) throws PermissionException {
        return (CustomerView) fetchView(session, CustomerView.class);
    }

    protected CustomerView getCustomerView(HttpSession session) throws PermissionException {
        CustomerView view = fetchCustomerView(session);
        if (view == null) {
            throw new ActionException("view not bound!");
        }
        return view;
    }

}
