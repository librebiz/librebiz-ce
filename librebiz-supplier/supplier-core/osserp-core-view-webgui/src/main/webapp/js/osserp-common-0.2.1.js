/**
 * osserp-core - osserp-common-0.2.1.js - Common functions.
 * Copyright 2001-2021 The original author or authors.
 * Licensed under the Apache License, Version 2.0.
 * 
 * ojsCommon - A collection of common functions that can be re-used everywhere.
 */

var ojsCommon = {};

ojsCommon.contextPath = "/osdb";
ojsCommon.logLevel = 0; // 0=DEBUG, 1=INFO, 2=WARN, 3=ERROR, 4=OFF

try {
	console.debug("DEBUG [ojsCommon] <init> done");
	ojsCommon.debugMode = true;
} catch(e) {
	ojsCommon.debugMode = false;
}

/**
 * Logs the given message if there is a console that can be written to. Otherwise does nothing.
 */
ojsCommon.consoleLog = function(message) {
	if (ojsCommon.debugMode) {
		console.log(message);
  }
}

ojsCommon.consoleDebug = function(message) {
	if (ojsCommon.debugMode && ojsCommon.logLevel == 0) {
		console.debug("DEBUG " + message);
  }
}

ojsCommon.consoleInfo = function(message) {
	if (ojsCommon.debugMode && ojsCommon.logLevel <= 1) {
		console.info("INFO " + message);
  }
}

ojsCommon.consoleWarn = function(message) {
	if (ojsCommon.debugMode && ojsCommon.logLevel <= 2) {
		console.warn("WARN " + message);
  }
}

ojsCommon.consoleError = function(message) {
	if (ojsCommon.debugMode && ojsCommon.logLevel <= 3) {
		console.error("ERROR " + message);
  }
}


/**
 * toggles the CSS "display" attribute of a given HTML Element
 * if displayType not given, "block" is used
 */
ojsCommon.toggleDisplay = function(elementName, displayType) {
  if($(elementName).style.display == 'none') {
		if(displayType != null) {
			$(elementName).style.display = displayType;
		} else {
			$(elementName).style.display = 'block';
		}
  } else {
    $(elementName).style.display = 'none';
  }
}

/**
 * sets the CSS "display" attribute of a given HTML Element
 */
ojsCommon.setDisplay = function(elementName, display) {
  $(elementName).style.display = display;
}

/**
 * toggles the CSS "visibility" attribute of a given HTML Element
 */
ojsCommon.toggleVisibility = function(elementName) {
  if($(elementName).style.visibility == 'visible') {
    $(elementName).style.visibility = 'hidden';
  } else {
    $(elementName).style.visibility = 'visible';
  }
}

/**
 * sets the CSS "visibility" attribute of a given HTML Element
 */
ojsCommon.setVisibility = function(elementName, visibility) {
  $(elementName).style.visibility = visibility;
}

/**
 * sets the property 'disabled' of a given HTML Form Element
 */
ojsCommon.setDisabled = function(elementName, value) {
  $(elementName).disabled = value;
  if($(elementName).disabled == true) {
    $(elementName).style.backgroundColor = '#EFEBE7';
    $(elementName).style.background = '#EFEBE7 none repeat scroll 0 0';
  } else {
  	$(elementName).style.backgroundColor = '';
  	$(elementName).style.background = '';
  }
}

/**
 * toggles the property 'disabled' of a given HTML Form Element
 */
ojsCommon.toggleDisabled = function(elementName) {
  if($(elementName).disabled == false) {
    ojsCommon.setDisabled(elementName, true);
  } else {
    ojsCommon.setDisabled(elementName, false);
  }
}



/**
 * returns true if the given value consists of the given characters
 */
ojsCommon.consistsOf = function(sText, validChars) {
  var valid = true;
  var Char;
  for (var i=0; i < sText.length && valid == true; i++) {
    Char = sText.charAt(i);
    if (validChars.indexOf(Char) == -1) {
      valid = false;
    }
  }
  return valid;
}


/**
 * returns true if the given value is a floating point value
 */
ojsCommon.isFloat = function(value) {
	return (value.toString().search(/^-?[0-9]+([\.\,]{1}[0-9]+)?$/) == 0);
}

/**
 * returns true if the given value is an Integer
 */
ojsCommon.isInt = function(value) {
  return (value.toString().search(/^-?[0-9]+$/) == 0);
}

/**
 * returns true if the given value is a number (float or int)
 * 
 */
ojsCommon.isNumeric = function(value) {
  if(ojsCommon.isInt(value) || ojsCommon.isFloat(value)) {
    return true;
  }
  return false;
}
	
/**
 * calculates the position of sourceElement and places targetElement directly under it
 * @param sourceElement: the id of the HTML element that is already positioned
 * @param targetElement: the id of the HTML element that shall be positioned according to sourceElement
 */
ojsCommon.positionUnder = function(sourceElement, targetElement) {
	var source = {}
	source.width  = $(sourceElement).offsetWidth;
	source.height = $(sourceElement).offsetHeight;
	source.left   = ojsCommon.calculateOffsetLeft(sourceElement);
	source.top    = ojsCommon.calculateOffsetTop(sourceElement);

	var target = {}
	target.width = source.width;
	target.left  = source.left + 2;
	target.top   = source.top + source.height + 2;
	
	$(targetElement).style.position = "absolute";
	$(targetElement).style.width = target.width + "px";
	$(targetElement).style.left  = target.left  + "px";
	$(targetElement).style.top   = target.top   + "px";
	ojsCommon.consoleDebug("[ojsCommon] positionUnder() width: " + target.width + "; left: " + source.left + "; top: " + source.top + ";");
}


/**
 * calculates either "offsetLeft" or "offsetTop" of an HTML element
 * @param element: the id of the element
 * @param attr: "offsetLeft" or "offsetTop"?
 */
ojsCommon.calculateOffset = function(element, attr) {
	element = $(element);
	var offset = 0;
	while(element) {
		offset += element[attr]; 
		element = element.offsetParent;
	}
	return offset;
}

/**
 * calculates "offsetLeft" of an HTML element
 * @param element: the id of the element
 */
ojsCommon.calculateOffsetLeft = function(element) {
	return ojsCommon.calculateOffset(element, "offsetLeft");
}

/**
 * calculates "offsetTop" of an HTML element
 * @param element: the id of the element
 */
ojsCommon.calculateOffsetTop = function(element) {
	return ojsCommon.calculateOffset(element, "offsetTop");
}

/**
 * Tells the system to wait a certain time before executing any further commands.
 * @param milliseconds
 */
ojsCommon.pause = function(milliseconds) {
	var now = new Date();
	var exitTime = now.getTime() + milliseconds;
	while (true) {
		now = new Date();
		if (now.getTime() > exitTime)
			return;
	}
}

/*
 * Prepends the contextPath to the given URL, but only if the url doesn't already start with the contextPath.
 * @param url: The URL to prepend the context path to.
 */
ojsCommon.prependContextPath = function(url) {
	var cpLength = ojsCommon.contextPath.length;
	var urlSubstring = url.substring(0,cpLength);
	
	if (urlSubstring != ojsCommon.contextPath) {
		return (ojsCommon.contextPath + url);
  }
	return url;
}

/*
 * Forwards the user to the given url.
 * @param url
 */
ojsCommon.gotoUrl = function(url) {
	url = ojsCommon.prependContextPath(url);
  location.href = url;
}

/*
 * @param aCharacter
 * returns true if given character is lower case
 */
ojsCommon.isLowerCase = function(aCharacter) {
	var reg = /[a-z]/;
	if (aCharacter.match(reg) == null) {
		return false;
	}
	return true;
}