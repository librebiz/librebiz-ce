/**
 *
 * Copyright (C) 2018 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 4, 2018 
 * 
 */
package com.osserp.core.service.mail;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.velocity.app.VelocityEngine;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.mail.EmailAddress;
import com.osserp.common.service.ResourceLocator;
import com.osserp.common.service.impl.AbstractTemplateAwareService;
import com.osserp.core.contacts.Phone;
import com.osserp.core.dao.MailTemplates;
import com.osserp.core.employees.Employee;
import com.osserp.core.mail.MailTemplate;
import com.osserp.core.mail.MailTemplateManager;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class MailTemplateManagerImpl extends AbstractTemplateAwareService implements MailTemplateManager {

    private MailTemplates mailTemplates;
    private ResourceLocator resourceLocator;

    protected MailTemplateManagerImpl(VelocityEngine engine, MailTemplates mailTemplates, ResourceLocator resourceLocator) {
        super(engine);
        this.mailTemplates = mailTemplates;
        this.resourceLocator = resourceLocator;
    }

    public List<MailTemplate> getTemplates() {
        return mailTemplates.findTemplates();
    }

    public MailTemplate findTemplate(String name) {
        return mailTemplates.findTemplate(name);
    }

    public MailTemplate getTemplate(Long id) {
        return mailTemplates.getTemplate(id);
    }

    public MailTemplate update(MailTemplate mailTemplate) throws ClientException {
        assert mailTemplate != null;
        if (!mailTemplate.isSendAsUser() && isNotSet(mailTemplate.getOriginator())) {
            throw new ClientException(ErrorCode.ORIGINATOR_MISSING);
        }
        if (isNotSet(mailTemplate.getSubject())) {
            throw new ClientException(ErrorCode.SUBJECT_MISSING);
        }
        if (mailTemplate.isPersonalizedFooter() &&
                !mailTemplate.isAddGroup() && !mailTemplate.isAddUser()) {
            throw new ClientException(ErrorCode.TEMPLATE_FOOTER_CONFIG_AMBIGUOUS);
        }
        mailTemplates.update(mailTemplate);
        return mailTemplates.getTemplate(mailTemplate.getId());
    }

    public Map<String, Object> createParameters(Employee user, MailTemplate mailTemplate) {
        Map<String, Object> model = new HashMap<>();
        model.put("personalizedHeader", mailTemplate.isPersonalizedHeader());
        model.put("personalizedFooter", mailTemplate.isPersonalizedFooter());
        if (mailTemplate.isPersonalizedFooter()) {

            if (mailTemplate.isAddUser()) {
                if (isSet(mailTemplate.getUsername())) {
                    model.put("currentUser", mailTemplate.getUsername());
                } else {
                    model.put("currentUser", user.getName());
                    if (!user.getEmails().isEmpty()) {
                        for (int i = 0, j = user.getEmails().size(); i < j; i++) {
                            EmailAddress next = user.getEmails().get(i);
                            if (next.isBusiness() && next.isPrimary()) {
                                model.put("currentUserEmail", next.getEmail());
                                break;
                            }
                        }
                    }
                    if (!user.getMobiles().isEmpty()) {
                        Phone mobile = user.getMobile();
                        if (mobile.isBusiness() && mobile.isPrimary()) {
                            model.put("currentUserMobile", mobile.getFormattedNumber());
                        }
                    }
                    if (!user.getPhones().isEmpty()) {
                        Phone phone = user.getPhone();
                        if (phone.isBusiness() && phone.isPrimary()) {
                            model.put("currentUserPhone", phone.getFormattedNumber());
                        }
                    }
                }
            }
        }
        if (mailTemplate.isAddGroup()) {
            String userGroup = user.getDefaultGroup() != null ? user.getDefaultGroup().getName() : null;
            if (isSet(mailTemplate.getGroupname())) {
                model.put("currentUserGroup", mailTemplate.getGroupname());
            } else if (isSet(userGroup)) {
                model.put("currentUserGroup", userGroup);
            }
        }
        return model;
    }

    public String renderExample(Employee user, MailTemplate mailTemplate) {
        assert mailTemplate != null;
        Map<String, Object> model = createParameters(user, mailTemplate);
        if (!mailTemplate.getParameterMap().isEmpty()) {
            model.putAll(mailTemplate.getParameterMap());
        }
        if (resourceLocator != null) {
            String salutation = resourceLocator.getMessage("defaultSalutation", Constants.DEFAULT_LOCALE_OBJECT);
            if (isSet(salutation)) {
                model.put("salutation", salutation);
            }
        }
        return createText(mailTemplate.getTemplate(), model);
    }
}
