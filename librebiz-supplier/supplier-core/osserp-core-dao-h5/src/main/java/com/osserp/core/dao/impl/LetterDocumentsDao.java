/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 10, 2008 7:55:59 AM 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.Date;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.ClientException;
import com.osserp.common.FileObject;
import com.osserp.common.User;
import com.osserp.common.dao.Tables;
import com.osserp.common.dms.DmsDocument;
import com.osserp.common.dms.DmsReference;
import com.osserp.common.dms.DocumentType;
import com.osserp.common.dms.dao.DocumentEncoder;
import com.osserp.common.dms.dao.impl.DatabaseDocumentsDao;
import com.osserp.common.service.SecurityService;

import com.osserp.core.dao.LetterDocuments;
import com.osserp.core.dms.Letter;
import com.osserp.core.model.letters.LetterDocumentImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class LetterDocumentsDao extends DatabaseDocumentsDao implements LetterDocuments {
    private String sequenceName = null;

    public LetterDocumentsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            Long documentTypeId,
            String documentTableKey,
            JdbcTemplate imageTemplate,
            String documentBinaryTableKey,
            String documentSequenceKey,
            DocumentEncoder documentEncoder, 
            SecurityService authenticationProvider) {
        super(
                jdbcTemplate,
                tables,
                sessionFactory,
                documentTypeId,
                documentTableKey,
                null, // categoryTableKey not supported by letters now
                imageTemplate,
                documentBinaryTableKey,
                documentEncoder, 
                authenticationProvider);
        sequenceName = getTable(documentSequenceKey);
    }

    public DmsDocument create(User user, Letter letter, byte[] file)
            throws ClientException {
        StringBuilder fileName = new StringBuilder(96);
        if (letter.getReference() != null) {
            fileName.append(letter.getReference()).append("-");
        }
        DocumentType documentType = getDocumentType();
        DmsReference reference = new DmsReference(documentType.getId(), letter.getId());
        fileName
                .append(letter.getId())
                .append("-")
                .append(System.currentTimeMillis())
                .append(".")
                .append(getDocumentType().getSuffix());
        FileObject fileObject = new FileObject(fileName.toString(), file);
        DmsDocument doc = create(
                user,
                reference,
                fileObject,
                null,
                null,
                null,
                null,
                null,
                null);
        return doc;
    }

    @Override
    protected DmsDocument createObject(
            User user, 
            DocumentType documentType, 
            DmsReference reference, 
            String fileName, 
            long fileSize, 
            String note, 
            Date sourceDate, 
            Long categoryId, 
            Date validFrom, 
            Date validTil,
            String messageId) {
        Long id = getNextLong(sequenceName);
        return new LetterDocumentImpl(id, reference.getReferenceId(), documentType, fileName, fileSize, user.getId(), note, categoryId);
    }

    @Override
    protected Class<LetterDocumentImpl> getPersistentClass() {
        return LetterDocumentImpl.class;
    }
}
