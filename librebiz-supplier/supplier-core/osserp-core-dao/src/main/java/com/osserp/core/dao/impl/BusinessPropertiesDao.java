/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 18, 2016 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.Types;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import com.osserp.common.BackendException;
import com.osserp.common.ErrorCode;
import com.osserp.common.Property;
import com.osserp.common.dao.AbstractTablesAwareSpringDao;
import com.osserp.common.dao.Tables;

import com.osserp.core.dao.BusinessProperties;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class BusinessPropertiesDao extends AbstractTablesAwareSpringDao implements BusinessProperties {
    private static Logger log = LoggerFactory.getLogger(BusinessPropertiesDao.class.getName());

    private String propertiesTable;
    
    public BusinessPropertiesDao(JdbcTemplate jdbcTemplate, Tables tables) {
        super(jdbcTemplate, tables);
        propertiesTable = tables.getName("businessProperties");
    }

    /* (non-Javadoc)
     * @see com.osserp.core.dao.BusinessProperties#findExistingValues(java.lang.String)
     */
    public List<Property> findExistingValues(String name) {
        if (name == null) {
            throw new BackendException(ErrorCode.APPLICATION_CONFIG);
        }
        StringBuilder sql = new StringBuilder(96);
        sql
                .append("SELECT distinct (pvalue), * FROM ")
                .append(propertiesTable)
                .append(" WHERE name = ? ORDER BY pvalue");
        final Object[] params = { name };
        final int[] types = { Types.VARCHAR };
        List result = (List) jdbcTemplate.query(
                sql.toString(), params, types, BusinessPropertyRowMapper.getReader());
        
        if (log.isDebugEnabled()) {
            log.debug("findExistingValues() done [name=" + name 
                    + ", count=" + result.size() + "]");
        }
        return result;
    }

}
