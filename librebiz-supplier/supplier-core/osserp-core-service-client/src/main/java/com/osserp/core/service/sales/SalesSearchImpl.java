/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 20 Mar 2007 10:06:24 
 * 
 */
package com.osserp.core.service.sales;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.dao.QueryResult;
import com.osserp.common.service.ResourceLocator;
import com.osserp.common.util.DateFormatter;
import com.osserp.common.util.DateUtil;
import com.osserp.common.util.StringUtil;

import com.osserp.core.BusinessCase;
import com.osserp.core.BusinessCaseFilter;
import com.osserp.core.BusinessCaseSearchRequest;
import com.osserp.core.FcsAction;
import com.osserp.core.Item;
import com.osserp.core.dao.BusinessCaseQueries;
import com.osserp.core.dao.ProjectQueries;
import com.osserp.core.dao.Projects;
import com.osserp.core.dao.Requests;
import com.osserp.core.dao.impl.BusinessCaseSearchImpl;
import com.osserp.core.finance.FinanceRecord;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordSearch;
import com.osserp.core.projects.ProjectFcsManager;
import com.osserp.core.requests.RequestFcsManager;
import com.osserp.core.sales.Sales;
import com.osserp.core.sales.SalesInvoiceManager;
import com.osserp.core.sales.SalesListItem;
import com.osserp.core.sales.SalesOrder;
import com.osserp.core.sales.SalesOrderManager;
import com.osserp.core.sales.SalesSearch;
import com.osserp.core.sales.SalesUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesSearchImpl extends BusinessCaseSearchImpl implements SalesSearch {
    private static Logger log = LoggerFactory.getLogger(SalesSearchImpl.class.getName());
    private SalesInvoiceManager invoiceManager = null;
    private SalesOrderManager orderManager = null;
    private RequestFcsManager requestFcsManager = null;
    private ProjectFcsManager projectFcsManager = null;
    private BusinessCaseQueries businessCaseQueries = null;
    private Set<String> businessCaseDateColumns = null;
    private Set<String> businessCaseI18NColumns = null;
    private Date businessCaseStatusReportTime = null;

    public SalesSearchImpl() {
        super();
    }

    public SalesSearchImpl(
            ResourceLocator resourceLocator,
            Requests requests,
            Projects projects,
            ProjectQueries projectQueries,
            RecordSearch recordSearch,
            RequestFcsManager requestFcsManager,
            ProjectFcsManager projectFcsManager,
            BusinessCaseQueries businessCaseQueries,
            SalesOrderManager orderManager,
            SalesInvoiceManager invoiceManager) {
        super(resourceLocator, requests, projects, projectQueries, recordSearch);
        this.invoiceManager = invoiceManager;
        this.orderManager = orderManager;
        this.projectFcsManager = projectFcsManager;
        this.requestFcsManager = requestFcsManager;
        this.businessCaseQueries = businessCaseQueries;

        businessCaseDateColumns = new HashSet<String>();
        String[] dcols = new String[] {
                "action_created",
                "created",
                "initial_created",
                "installation_date",
                "last_note_date",
                "order_probability_date",
                "presentation_date",
                "sales_talk_date"
        };
        for (int i = 0, j = dcols.length; i < j; i++) {
            businessCaseDateColumns.add(dcols[i]);
        }
        businessCaseI18NColumns = new HashSet<String>();
        String[] icols = new String[] {
                "contactgrant"
        };
        for (int i = 0, j = icols.length; i < j; i++) {
            businessCaseI18NColumns.add(icols[i]);
        }
    }

    public boolean exists(Long projectId) {
        return getProjects().exists(projectId);
    }

    public Sales fetchSales(Long id) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("findById() invoked for " + id);
        }
        if (getProjects().exists(id)) {
            return getProjects().load(id);
        }
        throw new ClientException(ErrorCode.SEARCH);
    }

    public List<FcsAction> getActionStatusInfo(BusinessCase object) {
        assert (object != null);
        if (object instanceof Sales) {
            return projectFcsManager.getActionStatusInfo(object);
        }
        return requestFcsManager.getActionStatusInfo(object);
    }

    public Record fetchMostCurrentRecord(Sales sales) throws ClientException {
        SalesOrder order = loadOrder(sales);
        List<? extends FinanceRecord> invoices = invoiceManager.getByReference(order.getId());
        if (log.isDebugEnabled()) {
            log.debug("fetchMostCurrentRecord() fetched invoices [order="
                    + order.getId() + ", count=" + invoices.size() + "]");
        }
        for (Iterator<? extends FinanceRecord> i = invoices.iterator(); i.hasNext();) {
            Invoice next = (Invoice) i.next();
            if (next.isCanceled() || next.isPartial()) {
                if (log.isDebugEnabled()) {
                    log.debug("fetchMostCurrentRecord() ignoring invoice [order="
                            + order.getId() + ", invoice=" + next.getId()
                            + ", canceled=" + next.isCanceled()
                            + ", partial=" + next.isPartial()
                            + "]");
                }
                i.remove();
            } else if (!containsPackage(next.getItems())) {
                if (log.isDebugEnabled()) {
                    log.debug("fetchMostCurrentRecord() ignoring invoice [order=" + order.getId()
                            + ", invoice=" + next.getId() + ", packageMissing=true]");
                }
                i.remove();
            }
        }
        if (invoices.isEmpty()) {
            if (log.isDebugEnabled()) {
                log.debug("fetchMostCurrentRecord() done, returning order "
                        + order.getId());
            }
            return order;
        }
        if (invoices.size() == 1) {
            if (log.isDebugEnabled()) {
                FinanceRecord invoice = invoices.get(0);
                log.debug("fetchMostCurrentRecord() done, returning invoice "
                        + invoice.getId());
            }
            return (Record) invoices.get(0);
        }
        throw new ClientException(null, invoices);
    }

    public Record findOrder(Sales sales) {
        return orderManager.getBySales(sales);
    }

    public List<SalesListItem> find(BusinessCaseSearchRequest request) {
        List<SalesListItem> result = new ArrayList<>();
        if (request != null && request.getDomainUser() != null) {
            List<SalesListItem> all = getProjectQueries().find(request);
            result = new ArrayList(SalesUtil.filter(request.getDomainUser(), all));
        }
        return result;
    }

    public List<SalesListItem> findServiceSales(Long salesId) {
        long start = System.currentTimeMillis();
        List<SalesListItem> result = getProjectQueries().findServiceSales(salesId);
        if (log.isDebugEnabled()) {
            log.debug("findServiceSales() done [sales=" + salesId
                    + ", count=" + result.size()
                    + ", duration=" + (System.currentTimeMillis() - start) + "ms]");
        }
        return result;
    }

    public QueryResult getBusinessCaseStatus(BusinessCaseFilter filter) {
        return businessCaseQueries.getBusinessCaseStatus(filter, false);
    }

    public String getBusinessCaseStatusSheet(BusinessCaseFilter filter) {
        QueryResult data = businessCaseQueries.getBusinessCaseStatus(filter, true);
        String[] columns = businessCaseQueries.fetchBusinessCaseStatusReportResult();
        List<String[]> result = new ArrayList<String[]>();
        if (columns == null || data.getRows().isEmpty()) {
            return "";
        }
        // add table header columns
        result.add(columns);
        for (int i = 0, j = data.getRows().size(); i < j; i++) {
            Map<String, Object> row = data.getRows().get(i);
            String[] obj = new String[columns.length];
            for (int k = 0, l = columns.length; k < l; k++) {
                String column = columns[k];
                obj[k] = fetchValue(row, column);
            }
            result.add(obj);
        }
        return StringUtil.createSheet(result);
    }

    public void reloadCachedBusinessCaseStatus() {
        Date start = new Date(System.currentTimeMillis());
        businessCaseQueries.reloadCachedViews();
        businessCaseStatusReportTime = start;
    }

    public Date getBusinessCaseStatusCacheTime() {
        return businessCaseStatusReportTime;
    }

    private String fetchValue(Map<String, Object> row, String column) {
        Object val = row.get(column);
        if (val instanceof Date) {
            try {
                if (val instanceof Timestamp) {
                    return DateFormatter.getISO((Timestamp) val);
                }
                return DateFormatter.getISO((Date) val);
            } catch (Exception e) {
                // ignore and try next steps
            }
        }
        if (val instanceof String) {
            if (businessCaseDateColumns.contains(column)) {
                // potential date provided as string; we try to provide unique format
                Date result = DateUtil.getDate((String) val);
                if (result != null) {
                    return DateFormatter.getISO(result);
                }
            } else if (businessCaseI18NColumns.contains(column)) {
                String result = getResourceString((String) val);
                if (result != null && !result.startsWith("?")) {
                    return result;
                }
            }
        }
        return (val == null ? "" : val.toString());
    }

    private SalesOrder loadOrder(Sales sale) throws ClientException {
        SalesOrder order = (SalesOrder) orderManager.getBySales(sale);
        if (order == null) {
            throw new ClientException(ErrorCode.ORDER_MISSING);
        }
        return order;
    }

    private boolean containsPackage(List<Item> items) {
        for (int i = 0, j = items.size(); i < j; i++) {
            Item next = items.get(i);
            if (next.getProduct().isPlant()) {
                return true;
            }
        }
        return false;
    }

    protected SalesInvoiceManager getInvoiceManager() {
        return invoiceManager;
    }

    public void setInvoiceManager(SalesInvoiceManager invoiceManager) {
        this.invoiceManager = invoiceManager;
    }

    protected SalesOrderManager getOrderManager() {
        return orderManager;
    }

    public void setOrderManager(SalesOrderManager orderManager) {
        this.orderManager = orderManager;
    }

    protected RequestFcsManager getRequestFcsManager() {
        return requestFcsManager;
    }

    public void setRequestFcsManager(RequestFcsManager requestFcsManager) {
        this.requestFcsManager = requestFcsManager;
    }

    protected ProjectFcsManager getProjectFcsManager() {
        return projectFcsManager;
    }

    public void setProjectFcsManager(ProjectFcsManager projectFcsManager) {
        this.projectFcsManager = projectFcsManager;
    }

    protected BusinessCaseQueries getBusinessCaseQueries() {
        return businessCaseQueries;
    }

    public void setBusinessCaseQueries(BusinessCaseQueries businessCaseQueries) {
        this.businessCaseQueries = businessCaseQueries;
    }

}
