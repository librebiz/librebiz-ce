/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jul 31, 2007 5:40:58 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import com.osserp.core.events.EventAlertDisplay;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class EventAlertDisplayRowMapper extends EventDisplayRowMapper {

    /*
     * supported type create type tp_event_alert_info as ( id bigint, name text, sales_id bigint, sales_name text, manager_id bigint, manager_name text,
     * is_alert boolean, event_id bigint, event_type bigint, event_name text, event_cfg_id bigint, event_created timestamp, event_expires timestamp,
     * event_rcp_id bigint, event_rcp_name text, src_id bigint, src_cfg_id bigint, src_rcp_id bigint, src_rcp_name text, src_created timestamp, src_expires
     * timestamp, );
     */
    @Override
    public Object mapRow(ResultSet rs, int index) throws SQLException {
        return new EventAlertDisplay(
                Long.valueOf(rs.getLong(1)),
                rs.getString(2),
                Long.valueOf(rs.getLong(3)),
                rs.getString(4),
                Long.valueOf(rs.getLong(5)),
                rs.getString(6),
                rs.getBoolean(7),
                Long.valueOf(rs.getLong(8)),
                Long.valueOf(rs.getLong(9)),
                rs.getString(10),
                Long.valueOf(rs.getLong(11)),
                rs.getTimestamp(12),
                rs.getTimestamp(13),
                Long.valueOf(rs.getLong(14)),
                rs.getString(15),
                Long.valueOf(rs.getLong(16)),
                Long.valueOf(rs.getLong(17)),
                Long.valueOf(rs.getLong(18)),
                rs.getString(19),
                rs.getTimestamp(20),
                rs.getTimestamp(21));
    }

    public static RowMapperResultSetExtractor getReader() {
        return new RowMapperResultSetExtractor(new EventAlertDisplayRowMapper());
    }
}
