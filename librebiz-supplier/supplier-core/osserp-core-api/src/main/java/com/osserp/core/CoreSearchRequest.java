/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 6, 2016 
 * 
 */
package com.osserp.core;

import com.osserp.common.SearchRequest;

import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CoreSearchRequest extends SearchRequest {

    private Long branchId = null;
    private DomainUser domainUser = null;

    protected CoreSearchRequest() {
        super();
    }

    /**
     * Creates a new project search request
     * @param columnKey
     * @param pattern
     * @param startsWith
     */
    protected CoreSearchRequest(
            String columnKey,
            String pattern,
            boolean startsWith) {
        super(columnKey, pattern, startsWith);
    }

    /**
     * Creates a new project search request
     * @param columnKey
     * @param pattern
     * @param startsWith
     * @param openOnly
     */
    protected CoreSearchRequest(
            String columnKey,
            String pattern,
            boolean startsWith,
            boolean openOnly) {
        super(columnKey, pattern, startsWith, openOnly);
    }

    /**
     * Creates a new domain search request
     * @param domainUser
     * @param columnKey
     * @param pattern
     * @param startsWith
     * @param openOnly
     */
    protected CoreSearchRequest(
            DomainUser domainUser,
            String columnKey,
            String pattern,
            boolean startsWith,
            boolean openOnly) {
        super(columnKey, pattern, startsWith, openOnly);
        this.domainUser = domainUser;
    }

    /**
     * Creates a new domain search request
     * @param domainUser
     * @param columnKey
     * @param pattern
     * @param startsWith
     * @param openOnly
     * @param branchId
     */
    protected CoreSearchRequest(
            DomainUser domainUser,
            String columnKey,
            String pattern,
            boolean startsWith,
            boolean openOnly,
            Long branchId) {
        this(domainUser, columnKey, pattern, startsWith, openOnly);
        this.branchId = branchId;
    }

    /**
     * Creates a new domain search request
     * @param domainUser
     * @param columnKey
     * @param pattern
     * @param startsWith
     * @param openOnly
     * @param branchId
     */
    protected CoreSearchRequest(
            CoreSearchRequest previous,
            String columnKey,
            String pattern,
            boolean startsWith,
            boolean openOnly,
            Long branchId) {
        super(previous, columnKey, pattern, startsWith, openOnly);
        if (previous != null) {
            if (isChanged(getBranchId(), branchId)) {
                setChanged(true);
            }
            domainUser = previous.domainUser;
        }
        this.branchId = branchId;
    }

    /**
     * Provides the id of selected branch
     * @return branch id or null if non selected
     */
    public Long getBranchId() {
        return branchId;
    }

    /**
     * Sets selected branch id
     * @param branchId
     */
    public void setBranchId(Long branchId) {
        this.branchId = branchId;
    }

    /**
     * Provides the user performing the search request
     * @return domainUser or null on public search
     */
    public DomainUser getDomainUser() {
        return domainUser;
    }

    /**
     * Sets the user performing the search request
     * @param domainUser
     */
    public void setDomainUser(DomainUser domainUser) {
        this.domainUser = domainUser;
    }

}
