/**
 *
 * Copyright (C) 2017 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jul 22, 2017 
 * 
 */
package com.osserp.core.customers

import com.osserp.core.contacts.ContactSearchResult

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class CustomerSearchResult extends ContactSearchResult {

    /**
     * Date if latest request with pending state
     */
    Date lastRequestDate

    /**
     * Date of last sales invoice
     */
    Date lastSalesDate

    /**
     * Net amount of last sales invoice
     */
    Double lastSales

    /**
     * Sales total (e.g. sum of all sales invoice net amount)
     */
    Double salesTotal

    /**
     * Count of pending requests    
     */
    Long requestCount

    /**
     * Count of cancelled requests    
     */
    Long requestCancelledCount
    
    /**
     * Count of pending sales orders
     */
    Long orderCount
    
    /**
     * Date of last pending sales order
     */
    Date lastOrderDate

    /**
     * Indicates if all existing requests are cancelled requests
     * @return true if all cancelled
     */
    boolean isAllRequestsCancelled() {
        (requestCount <= requestCancelledCount)
    }

    /**
     * Provides the column names matching resulting rows
     * @return column names
     */
    static String[] getSearchResultHeader() {
        return [
            'id',
            'title',
            'lastname',
            'firstname',
            'street',
            'zipcode',
            'city',
            'email',
            'created',
            'requestCount',
            'lastRequestDate',
            'lastSalesDate',
            'lastSales',
            'salesTotal',
            'orderCount',
            'lastOrderDate'
        ]
    }

    /**
     * Provides the values as selected by default customer query as array.
     * See searchResultHeader for details about field association
     * @return search result rows
     */
    String[] getSearchResultRow() {
        return [
            id,
            !salutation ? '' : salutation.name,
            lastname,
            firstname,
            street,
            zipcode,
            city,
            email,
            createDateForSheet(created),
            requestCount,
            createDateForSheet(lastRequestDate),
            createDateForSheet(lastSalesDate),
            lastSales,
            createCurrencyForSheet(salesTotal),
            orderCount,
            createDateForSheet(lastOrderDate)
        ]
    }
}
