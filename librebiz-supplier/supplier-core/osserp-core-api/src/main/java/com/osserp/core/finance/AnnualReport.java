/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 12, 2016
 * 
 */
package com.osserp.core.finance;

import java.util.List;

import com.osserp.core.purchasing.PurchaseRecordSummary;
import com.osserp.core.sales.SalesRecordSummary;
import com.osserp.core.system.SystemCompany;


/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public interface AnnualReport extends FinanceReport {
    
    /**
     * Provides the reporting company
     * @return company
     */
    SystemCompany getCompany();
    
    /**
     * Provides the summary amount of outstanding sales payments and supplier refunds.
     * @return outstanding amounts
     */
    Double getAmountOutstanding();

    /**
     * Provides the outstanding amount to pay for supplier invoices and costs
     * @return amount to pay
     */
    Double getAmountToPay();

    /**
     * Provides the summary of all payments by customers and supplier refunds 
     * @return amount of cash in
     */
    Double getCashIn();

    /**
     * Provides a list of all incoming payments
     * @return payment receipt
     */
    List<PaymentDisplay> getCashInList();

    /**
     * Provides the summary of all purchase, tax and other costs payments   
     * @return amount of cash out
     */
    Double getCashOut();

    /**
     * Provides a list of all outgoing payments
     * @return payments
     */
    List<PaymentDisplay> getCashOutList();

    /**
     * Provides the net sales amount
     * @return sales amount
     */
    Double getSalesAmount();

    /**
     * Provides the gross sales amount including tax
     * @return sales amount gross
     */
    Double getSalesGrossAmount();

    /**
     * Provides the sales tax amount (e.g. salesGross - sales)
     * @return tax on sales
     */
    Double getSalesTaxAmount();

    /**
     * Provides the net purchase amount
     * @return purchase amount
     */
    Double getPurchaseAmount();

    /**
     * Provides the gross purchase amount
     * @return purchase amount
     */
    Double getPurchaseGrossAmount();

    /**
     * Provides the tax amount of purchases (e.g. purchaseGross - purchase)
     * @return tax on purchases
     */
    Double getPurchaseTaxAmount();

    /**
     * Provides the net other costs amount
     * @return other costs amount
     */
    Double getCostsAmount();

    /**
     * Provides the gross other costs amount
     * @return other costs amount
     */
    Double getCostsGrossAmount();

    /**
     * Provides the gross other costs amount
     * @return other costs amount
     */
    Double getCostsTaxfreeAmount();

    /**
     * Provides the net amount of all supplier invoices
     * @return supplier invoice amount
     */
    double getSupplierAmount();

    /**
     * Provides the net amount of all supplier credit notes
     * @return supplier credit amount
     */
    double getSupplierCreditAmount();
    
    /**
     * Provides the sum of costs, costsFreeTax and purchaseAmount,
     * e.g. same as supplierAmount - supplierCredit.
     * @return total amount of purchases minus supplier credit
     */
    Double getPurchaseTotalAmount();

    /**
     * Provides the total amount of all salary relating payments
     * @return salaryAmount
     */
    Double getSalaryAmount();

    /**
     * Provides the total amount of all salary relating refunds
     * @return salaryRefundAmount
     */
    Double getSalaryRefundAmount();

    /**
     * Provides the total amount of all tax relating payments
     * @return taxAmount
     */
    Double getTaxPaymentAmount();

    /**
     * Provides the total amount of all tax relating payments
     * @return taxAmount
     */
    Double getTaxRefundAmount();
    
    /**
     * Checks if payment has bankAccountId set and provides bankAccount 
     * shortname if present
     * @param payment
     * @return bank account shortname or empty string if not paid or found
     */
    String getBankAccountByPayment(PaymentDisplay payment);
    
    /**
     * Checks if record has payment and provides relating bankAccount shortname
     * @param summary
     * @return bank account shortname or empty string if not paid or found
     */
    String getBankAccountBySummary(RecordSummary summary);

    /**
     * Provides all sales records created in year
     * @return sales records
     */
    List<SalesRecordSummary> getSalesRecords();

    /**
     * Provides all purchase records created in year
     * @return purchase records
     */
    List<PurchaseRecordSummary> getPurchaseRecords();

    /**
     * Provides all other cost records of the year
     * @return common costs
     */
    List<CommonPaymentSummary> getCommonRecords();
    
    /**
     * Provides all purchase records and common records in one list, 
     * e.g. creditor record list.
     * @return supplier records
     */
    List<RecordSummary> getSupplierRecords();

}
