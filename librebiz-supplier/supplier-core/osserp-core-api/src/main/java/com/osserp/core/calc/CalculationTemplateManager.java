/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 29, 2011 3:20:55 PM 
 * 
 */
package com.osserp.core.calc;

import java.util.List;

import com.osserp.core.BusinessType;
import com.osserp.core.products.Product;

/**
 * 
 * @author jg <jg@osserp.com>
 * @author rk <rk@osserp.com>
 */
public interface CalculationTemplateManager extends CalculationTemplateService {

    /**
     * Find all calculation template
     * @return list of calculationTemplate
     */
    List<CalculationTemplate> findAll();

    /**
     * Load a calculation template
     * @param id
     * @return calculationTemplate
     */
    CalculationTemplate load(Long id);

    /**
     * Creates a new template
     * @param name
     * @param product
     * @param businessType
     * @return created template
     */
    CalculationTemplate createTemplate(String name, Product product, BusinessType businessType);

    /**
     * Add an product to the template
     * @param calculationTemplate
     * @param product
     * @param note
     * @param group
     */
    void addProduct(CalculationTemplate calculationTemplate, Product product, String note, Long group);

    /**
     * Removes a item with given id from template
     * @param calculationTemplate
     * @param itemId
     */
    void removeItem(CalculationTemplate calculationTemplate, Long itemId);

    /**
     * Toggles item.optional flag
     * @param calculationTemplate
     * @param itemId
     */
    void toggleOptionalItem(CalculationTemplate calculationTemplate, Long itemId);

    /**
     * Deletes a calculation template
     * @param id
     */
    void delete(Long id);
}
