/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Aug 30, 2009 
 * 
 */
package com.osserp.gui.tags;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.Tag;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.commons.beanutils.PropertyUtils;

import com.osserp.common.Option;
import com.osserp.common.beans.OptionImpl;
import com.osserp.common.web.tags.AbstractClassicValueTag;

import com.osserp.core.CoreServiceProvider;

/**
 * 
 * @author eh <eh@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public class SelectTag extends AbstractClassicValueTag {
    private static Logger log = LoggerFactory.getLogger(SelectTag.class.getName());

    private String name = null;
    private String nameProperty = "name";
    private String onchange = null;
    protected Object options = null;
    private List<Option> preOptions = null;
    protected boolean emptyPrompt = false;
    protected boolean prompt = true;
    private String promptKey = null;
    private int size = 1;
    private int tabindex = -1;
    private String url = null;
    private String valueProperty = "id";
    private String disabled = "false";
    private boolean resourceLookup = false;
    private boolean autoupdate = false;
    private String undefined = null;

    private boolean subtagEntered = false;

    /**
     * Provides the name attribute value of the select tag
     * @return name
     */
    protected String getName() {
        return name;
    }

    /**
     * Sets the name of the select option to render
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Provides the name of the object's property in a given list that should be used as name in the rendered option. This is only used when the options contain
     * a list,
     * @return name
     */
    protected String getNameProperty() {
        return nameProperty;
    }

    /**
     * Sets the name of the object's property in a given list that should be used as name in the rendered option. This is only used when the options contain a
     * list is not an option name.
     * @param name
     */
    public void setNameProperty(String nameProperty) {
        this.nameProperty = nameProperty;
    }

    /**
     * Sets the value of the optional onchange property
     * @param onchange
     */
    public void setOnchange(String onchange) {
        this.onchange = onchange;
    }

    /**
     * Sets the name of the options list to render as option tags
     * @param options
     */
    public void setOptions(Object options) {
        this.options = options;
    }

    /**
     * Indicates that a prompting default option with option id 0 should be added as first option.<br/>
     * This is an optional param whose default is true. You have to set it to default if options provide there own default.
     * @param prompt
     */
    public void setPrompt(boolean prompt) {
        this.prompt = prompt;
    }

    /**
     * Indicates that a default option with id 0 and an empty text should be added as first option.<br/>
     * @return the emptyPrompt
     */
    public boolean isEmptyPrompt() {
        return emptyPrompt;
    }

    /**
     * @param emptyPrompt the emptyPrompt to set
     */
    public void setEmptyPrompt(boolean emptyPrompt) {
        this.emptyPrompt = emptyPrompt;
    }

    /**
     * Sets an optional key of the name to set if prompt option 0 is enabled.<br/>
     * @param promptKey
     */
    public void setPromptKey(String promptKey) {
        this.promptKey = promptKey;
    }

    /**
     * Renders an additional undefined option with value -1 and i18n key
     * corresponding with undefined as label if defined
     * @return undefined
     */
    public String getUndefined() {
        return undefined;
    }

    /**
     * Sets an additional undefined option. This adds an option with value -1 
     * and the corresponding i18n key of the undefined value as label.
     * @return undefined
     */
    public void setUndefined(String undefined) {
        this.undefined = undefined;
    }

    /**
     * Sets an optional size property. Defaults to 1.
     * @param size
     */
    public void setSize(int size) {
        this.size = size;
    }

    /**
     * Sets the optional tabindex property.
     * @param tabindex
     */
    public void setTabindex(int tabindex) {
        this.tabindex = tabindex;
    }

    public void setUrl(String url) {
        this.url = getContextPath() + url;
    }

    /**
     * Sets the name of the object's property in a given list that should be used as value in the rendered option. This is only used when the options contain a
     * list, not an option name.
     * @param name
     */
    public void setValueProperty(String valueProperty) {
        this.valueProperty = valueProperty;
    }

    /**
     * Indicates text output interprets value as resource key forcing an i18n lookup for value
     * @return the resourceLookup
     */
    public boolean isResourceLookup() {
        return resourceLookup;
    }

    /**
     * Enables/disables resource lookups for provided string value properties.
     * @param resourceLookup the resourceLookup to set
     */
    public void setResourceLookup(boolean resourceLookup) {
        this.resourceLookup = resourceLookup;
    }

    /**
     * Causes form submission on change event if enabled. 
     * @return true if form should be submit when value changes
     */
    public boolean isAutoupdate() {
        return autoupdate;
    }

    /**
     * Sets autoupdate property. Default value is false.
     * @param autoupdate true to enable. 
     */
    public void setAutoupdate(boolean autoupdate) {
        this.autoupdate = autoupdate;
    }

    /**
     * @param disabled
     */
    public void setDisabled(String disabled) {
        this.disabled = disabled;
    }

    public StringBuilder addOnchange(StringBuilder buffer) {
        if (autoupdate) {
            buffer.append(" onchange=\"enableUpdateOnly(); sendForm();\"");
        } else if (onchange != null && onchange.length() > 0) {
            buffer.append(" onchange=\"").append(onchange).append("\" ");
        }
        return buffer;
    }

    public StringBuilder addTabindex(StringBuilder buffer) {
        if (tabindex >= 0) {
            buffer.append(" tabindex=\"").append(tabindex).append("\" ");
        }
        return buffer;
    }

    public StringBuilder addDisabled(StringBuilder buffer) {
        if (disabled.equals("true")) {
            buffer.append(" disabled=\"disabled\" ");
        }
        return buffer;
    }

    @Override
    public int doStartTag() throws JspException {
        return Tag.EVAL_BODY_INCLUDE;
    }

    @Override
    public int doEndTag() throws JspException {
        try {
            JspWriter out = pageContext.getOut();
            String output = null;
            try {
                output = getOutput();
            } catch (Throwable t) {
                log.warn("doEndTag() ignoring error on attempt to get output [message=" + t.getMessage() + "]", t);
                output = "";
            }
            out.print(output);
        } catch (IOException ioe) {
            log.error("doTag() error getting writer");
        }
        preOptions = null;
        return EVAL_PAGE;
    }

    public String getOutput() {
        StringBuilder buffer = new StringBuilder(1024);
        buffer
                .append("<select name=\"").append(name).append("\"")
                .append(" size=\"").append(size).append("\"");
        addTabindex(buffer);
        addOnchange(buffer);
        addStyle(buffer);
        addStyleClass(buffer);
        addId(buffer);
        addDisabled(buffer);
        buffer.append(">").append(createOptions()).append("</select>");
        return buffer.toString();
    }
    
    protected String createOptions() {
        StringBuilder buffer = new StringBuilder();
        if (prompt || emptyPrompt) {
            if (url == null) {
                buffer.append("<option value=\"0\">");
                buffer.append(getDefaultOptionName(emptyPrompt, promptKey));
                buffer.append("</option>");
            } else {
                buffer.append("<option value=\"").append(url).append("\">");
                buffer.append(getDefaultOptionName(emptyPrompt, promptKey));
                buffer.append("</option>");
            }
        }
        if (preOptions != null) {
            buffer.append(createSpecificOptions(preOptions));
        }
        if (options instanceof List) {
            List optionList = (List) options;
            if (optionList.size() > 0 && optionList.get(0) instanceof Option
                    && (valueProperty.equals("id") && nameProperty.equals("name"))) {
                buffer.append(createSpecificOptions(optionList));
            } else {
                buffer.append(createUnspecificOptions(optionList));
            }
        } else if (options instanceof String) {
            String sOptions = (String) options;
            List<Option> optionList = CoreServiceProvider.getOptionList(
                    getRequest(), sOptions);
            buffer.append(createSpecificOptions(optionList));
        }
        if (undefined != null) {
            buffer.append("<option value=\"-1\">");
            buffer.append(getResourceString(undefined));
            buffer.append("</option>");
        }
        return buffer.toString();
    }

    private String getDefaultOptionName(boolean empty, String key) {
        if (empty) {
            return " ";
        }
        if (key != null && key.length() > 0) {
            return getResourceString(promptKey);
        }
        return getResourceString("promptSelect");        
    }

    /**
     * Creates html option entries by using an Option list
     * @param options the Option list
     * @return a String with html option entries
     */
    protected String createSpecificOptions(List<Option> optionList) {
        StringBuilder buffer = new StringBuilder();
        Object df = fetchDefault();
        for (int i = 0, j = optionList.size(); i < j; i++) {
            Option next = optionList.get(i);
            if (!next.isEndOfLife()) {
                buffer
                        .append("<option value=\"")
                        .append(createOptionValue(next.getId()))
                        .append("\"");
                if (df != null && df.equals(next.getId())) {
                    buffer.append(" selected=\"selected\"");
                }
                buffer.append(">");
                if (resourceLookup) {
                    buffer.append(getResourceString(next.getName()));
                } else if (next.getResourceKey() != null) {
                    buffer.append(getResourceString(next.getResourceKey()));
                } else {
                    buffer.append(next.getName());
                }
                buffer.append("</option>");
            }
        }
        return buffer.toString();
    }

    /**
     * Creates html option entries by using an raw list. For this nameProperty 
     * and valueProperty are used to get value and name; the defaults are "id" 
     * and "name". If one of the properties cannot be found, the whole entry is 
     * ignored.
     * @param options the list
     * @return a String with html option entries
     */
    protected String createUnspecificOptions(List optionList) {
        StringBuilder buffer = new StringBuilder();
        Object df = fetchDefault();
        for (int i = 0, j = optionList.size(); i < j; i++) {
            Object next = optionList.get(i);
            try {
                Object optionName = PropertyUtils.getProperty(next, nameProperty);
                Object optionValue = PropertyUtils.getProperty(next, valueProperty);
                buffer
                        .append("<option value=\"")
                        .append(createOptionValue(optionValue))
                        .append("\"");
                if (df != null && df.equals(optionValue)) {
                    buffer.append(" selected=\"selected\"");
                }
                buffer.append(">");
                if (resourceLookup && optionName instanceof String) {
                    buffer.append(getResourceString(optionName.toString()));
                } else {
                    buffer.append(optionName);
                }
                buffer.append("</option>");
            } catch (Exception e) {
                log.warn("createUnspecificOptions() ignoring exception [nameProperty=" + nameProperty
                        + ", valueProperty=" + valueProperty + ", errorMessage=" + e.toString() + "]");
            }
        }
        return buffer.toString();
    }

    protected Object createOptionValue(Object o) {
        if (url != null) {
            return url + o;
        }
        return o;
    }

    protected Object fetchDefault() {
        return fetchValueByType(value);
    }

    protected final Object fetchValueByType(Object val) {
        if (val != null) {
            if (val instanceof Long) {
                return val;
            }
            if (val instanceof Option) {
                return ((Option) val).getId();
            }
            if (val instanceof Integer) {
                return Long.valueOf((Integer) val);
            }
            if (val instanceof String) {
                try {
                    return Long.valueOf((String) val);
                } catch (Throwable t) {
                    //ignore this and return the string by itself
                    return val;
                }
            }
        }
        return null;
    }

    /**
     * Puts an option to render before the options list.
     * @param value the option value
     * @param name the option name
     */
    public void putOption(Long idVal, String nameVal) {
        if (preOptions == null) {
            preOptions = new ArrayList<Option>();
        }
        preOptions.add(new OptionImpl(idVal, nameVal));
    }

    /**
     * Returns status indicating whether a subtag should run or not.
     * 
     * @return <tt>true</tt> if the subtag should evaluate its condition and decide whether to run, <tt>false</tt> otherwise.
     */
    public boolean gainPermission() {
        if (!subtagEntered) {
            subtagEntered = true;
            return (true);
        }
        return (false);
    }

    /**
     * A subtag calls this when it has finished successfully (with EVAL_BODY_INCLUDE).
     */
    public void subtagSucceeded() {
        if (!subtagEntered) {
            throw new IllegalStateException("The subtag was not entered yet");
        }
        subtagEntered = false;
    }
}
