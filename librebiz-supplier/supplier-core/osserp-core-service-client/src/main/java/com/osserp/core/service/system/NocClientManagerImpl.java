/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 23, 2013 
 * 
 */
package com.osserp.core.service.system;

import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.mail.MailDomain;
import com.osserp.core.dao.NocClients;
import com.osserp.core.system.NocClient;
import com.osserp.core.system.NocClientManager;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class NocClientManagerImpl implements NocClientManager {

    private NocClients nocClients;

    protected NocClientManagerImpl(NocClients mailDomains) {
        super();
        this.nocClients = mailDomains;
    }

    public NocClient getDefaultClient() {
        return nocClients.getDefaultClient();
    }

    public NocClient getClient(Long id) {
        return nocClients.getClient(id);
    }

    public List<NocClient> getClients() {
        return nocClients.getAllClients();
    }

    public List<MailDomain> getDomains() {
        return nocClients.getAllDomains();
    }

    public List<MailDomain> getDomainsByReference(Long referenceId) {
        return nocClients.getDomains(referenceId);
    }

    public MailDomain createDomain(Long user, NocClient client, MailDomain parentDomain, String name, String provider, Date domainCreated, Date domainExpires)
            throws ClientException {
        return nocClients.createDomain(user, client.getId(), parentDomain, name, provider, domainCreated, domainExpires);
    }

    public MailDomain updateDomain(
            MailDomain domain,
            Long user,
            String provider,
            String smtpServerName,
            String hostIp,
            String hostMx,
            String hostSecondaryMx,
            String hostMxIp,
            String hostSecondaryMxIp,
            Date domainCreated,
            Date domainExpires) {
        domain.update(user, provider, smtpServerName, hostIp, hostMx, hostSecondaryMx, hostMxIp, hostSecondaryMxIp, domainCreated, domainExpires);
        nocClients.save(domain);
        return nocClients.getDomain(domain.getId());
    }
}
