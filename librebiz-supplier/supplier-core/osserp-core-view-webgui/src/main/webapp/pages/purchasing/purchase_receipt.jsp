<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.purchaseReceiptView}">
    <o:logger write="purchaseReceiptView not bound!" level="debug" />
    <c:redirect url="/errors/error_context.jsp" />
</c:if>
<c:set var="view" value="${sessionScope.purchaseReceiptView}" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>

    <tiles:put name="headline">
        <o:listSize value="${view.list}" />
        <fmt:message key="deliveriesWithoutBookedInvoice" />
        <c:if test="${!empty view.selectedProduct}">
            [ <o:out value="${view.selectedProduct.productId}" /> - <o:out value="${view.selectedProduct.name}" /> ]
        </c:if>
    </tiles:put>
    <tiles:put name="headline_right">
        <ul>
            <li><a href="<c:url value="/purchaseReceipt.do?method=exit"/>"><o:img name="backIcon" /></a></li>
            <li>
                <v:link url="/index" title="backToMenu">
                    <o:img name="homeIcon" />
                </v:link>
            </li>
        </ul>
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area">
            <div class="row">
                <div class="col-md-12 panel-area">
                    <div class="table-responsive table-responsive-default">
                        <c:choose>
                            <c:when test="${!empty view.selectedProduct}">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th><fmt:message key="date" /></th>
                                            <th class="right"><fmt:message key="number" /></th>
                                            <th><fmt:message key="supplier" /></th>
                                            <th class="right" style="text-align: right;"><fmt:message key="quantity" /></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="record" items="${view.list}">
                                            <c:forEach var="item" items="${record.items}">
                                                <c:if test="${view.selectedProduct.productId == item.product.productId}">
                                                    <tr>
                                                        <td><o:date value="${record.created}" /></td>
                                                        <td class="right">
                                                            <a href="<c:url value="/purchaseDeliveryNote.do?method=select&exit=purchaseReceiptDisplay&id=${record.id}"/>">
                                                                <o:out value="${record.number}" />
                                                            </a>
                                                        </td>
                                                        <td><o:out value="${record.contact.displayName}" /></td>
                                                        <td class="right"><o:number value="${item.quantity}" format="decimal" /></td>
                                                    </tr>
                                                </c:if>
                                            </c:forEach>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </c:when>
                            <c:otherwise>
                                <table class="table">
                                    <%-- display all items --%>
                                    <thead>
                                        <tr>
                                            <th><a href="<c:url value="/purchaseReceipt.do?method=sort&target=orderByNumber"/>"><fmt:message key="number" /></a></th>
                                            <th><a href="<c:url value="/purchaseReceipt.do?method=sort&target=orderByName"/>"><fmt:message key="supplier" /></a></th>
                                            <th class="right">
                                                <a href="<c:url value="/purchaseReceipt.do?method=sort&target=orderByCreated"/>">
                                                    <fmt:message key="date" /> / <fmt:message key="numberShort" />
                                                </a>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:choose>
                                            <c:when test="${empty view.list}">
                                                <tr>
                                                    <td colspan="3" style="width: 789px;"><fmt:message key="noDeliveryNotesFound" /></td>
                                                </tr>
                                            </c:when>
                                            <c:otherwise>
                                                <c:forEach var="record" items="${view.list}">
                                                    <tr class="altrow">
                                                        <td>
                                                            <a href="<c:url value="/purchaseDeliveryNote.do?method=select&id=${record.id}&exit=purchaseReceiptDisplay&provideOrderLink=true"/>">
                                                                <o:out value="${record.number}" />
                                                            </a>
                                                        </td>
                                                        <td><o:out value="${record.contact.displayName}" /></td>
                                                        <td class="right"><o:date value="${record.created}" /></td>
                                                    </tr>
                                                    <c:forEach var="item" items="${record.items}">
                                                        <tr>
                                                            <td>
                                                                <a href="<c:url value="/products.do?method=load&id=${item.product.productId}&stockId=${item.product.currentStock}&exit=purchaseReceiptDisplay"/>">
                                                                    <o:out value="${item.product.productId}" />
                                                                </a>
                                                            </td>
                                                            <td><o:out value="${item.productName}" /></td>
                                                            <td class="right"><o:number value="${item.quantity}" format="decimal" /></td>
                                                        </tr>
                                                    </c:forEach>
                                                </c:forEach>
                                            </c:otherwise>
                                        </c:choose>
                                    </tbody>
                                </table>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
