/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.service.planning;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.osserp.common.Constants;
import com.osserp.common.util.DateUtil;

import com.osserp.core.dao.ProductPlanningCache;
import com.osserp.core.dao.Projects;
import com.osserp.core.dao.Requests;
import com.osserp.core.dao.records.PurchaseOrders;
import com.osserp.core.finance.Order;
import com.osserp.core.model.products.ProductPlanningAwareVO;
import com.osserp.core.planning.ProductPlanning;
import com.osserp.core.planning.ProductPlanningAware;
import com.osserp.core.planning.ProductPlanningManager;
import com.osserp.core.planning.ProductPlanningSettings;
import com.osserp.core.planning.SalesPlanningSummary;
import com.osserp.core.products.Product;
import com.osserp.core.projects.results.ProjectListItem;
import com.osserp.core.requests.Request;
import com.osserp.core.sales.Sales;
import com.osserp.core.sales.SalesListItem;
import com.osserp.core.views.OrderItemsDisplay;

/**
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProductPlanningManagerImpl implements ProductPlanningManager {

    private ProductPlanningCache planningCache = null;
    private Requests requests = null;
    private Projects projects = null;
    private PurchaseOrders purchaseOrders = null;

    /**
     * Creates a new product planning manager
     * @param planningCache
     * @param requests
     * @param projects
     * @param purchaseOrders
     */
    public ProductPlanningManagerImpl(
            ProductPlanningCache planningCache,
            Requests requests,
            Projects projects,
            PurchaseOrders purchaseOrders) {
        super();
        this.planningCache = planningCache;
        this.requests = requests;
        this.projects = projects;
        this.purchaseOrders = purchaseOrders;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.planning.ProductPlanningManager#find(java.lang.Long)
     */
    public ProductPlanningAware find(Long id) {
        if (projects.exists(id)) {
            Sales sales = projects.load(id);
            return new ProductPlanningAwareVO(
                    sales.getId(),
                    sales.getName(),
                    sales.getStatus(),
                    true,
                    sales.getRequest().getDeliveryDate());
        }
        if (requests.exists(id)) {
            Request request = requests.load(id);
            return new ProductPlanningAwareVO(
                    request.getRequestId(),
                    request.getName(),
                    request.getStatus(),
                    true,
                    DateUtil.addDays(new Date(), Constants.DEFAULT_DELIVERY_DAYS));
        }
        if (purchaseOrders.exists(id)) {
            Order record = (Order) purchaseOrders.load(id);
            return new ProductPlanningAwareVO(
                    record.getId(),
                    record.getName(),
                    record.getStatus(),
                    false,
                    record.getDelivery());
        }
        return null;
    }

    public OrderItemsDisplay createVirtual(
            ProductPlanningAware planningAware,
            Product product,
            Double quantity,
            Long stockId) {

        OrderItemsDisplay orderItemDisplay = new OrderItemsDisplay(
                planningAware.getId(),
                planningAware.getName(),
                planningAware.getId(),
                DateUtil.getCurrentDate(),
                planningAware.getDesignatedDeliveryDate(),
                product,
                quantity,
                planningAware.isSales(),
                false,
                stockId);
        return orderItemDisplay;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.planning.ProductPlanningManager#isPlanningAvailable(java.lang.Long)
     */
    public boolean isPlanningAvailable(Long stock) {
        return planningCache.isPlanningAvailable(stock);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.planning.ProductPlanningManager#getPlanning(java.lang.Long, java.util.Date, boolean)
     */
    public ProductPlanning getPlanning(Long stock, Date horizon, boolean confirmedPurchaseOnly) {
        return planningCache.getPlanning(stock, horizon, confirmedPurchaseOnly);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.planning.ProductPlanningManager#getSettings(java.lang.Long)
     */
    public ProductPlanningSettings getSettings(Long reference) {
        return planningCache.getSettings(reference);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.planning.ProductPlanningManager#save(com.osserp.core.planning.ProductPlanningSettings)
     */
    public void save(ProductPlanningSettings settings) {
        planningCache.save(settings);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.planning.ProductPlanningManager#isAvailableForDelivery(com.osserp.core.sales.Sales)
     */
    public boolean isAvailableForDelivery(Sales sales) {
        return planningCache.isAvailableForDelivery(sales.getId(), sales.getStatus());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.planning.ProductPlanningManager#isAvailableForDelivery(com.osserp.core.sales.SalesListItem)
     */
    public boolean isAvailableForDelivery(SalesListItem sales) {
        return planningCache.isAvailableForDelivery(sales.getId(), sales.getStatus());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.planning.ProductPlanningManager#isAvailableForDelivery(com.osserp.core.projects.results.ProjectListItem)
     */
    public boolean isAvailableForDelivery(ProjectListItem sales) {
        return planningCache.isAvailableForDelivery(
                sales.getId(),
                sales.getStatus() == null ? null : Long.valueOf(sales.getStatus().longValue()));
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.planning.ProductPlanningManager#getItems(com.osserp.core.sales.Sales)
     */
    public List<SalesPlanningSummary> getItems(Sales sales) {
        if (sales.getStatus() >= Sales.STATUS_BILLED) {
            return new ArrayList<SalesPlanningSummary>();
        }
        boolean listVacant = !sales.isReleased();
        return planningCache.getItems(sales.getId(), listVacant);
    }

}
