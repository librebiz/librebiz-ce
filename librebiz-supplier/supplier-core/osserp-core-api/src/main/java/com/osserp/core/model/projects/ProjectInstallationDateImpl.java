/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 30-Oct-2006 06:31:20 
 * 
 */
package com.osserp.core.model.projects;

import java.util.Date;

import org.jdom2.Element;

import com.osserp.core.model.TargetDateImpl;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author cf <cf@osserp.com>
 * 
 */
public class ProjectInstallationDateImpl extends TargetDateImpl {

    protected ProjectInstallationDateImpl() {
        super();
    }

    /**
     * Default constructor to create a new installation date
     * @param reference (e.g. request, sales, project...)
     * @param createdBy
     * @param installation date
     */
    public ProjectInstallationDateImpl(Long reference, Long createdBy, Date date) {
        super(reference, createdBy, date);
    }

    @Override
    public Element getXML() {
        return getXML("root");
    }

    @Override
    public Element getXML(String rootElementName) {
        Element root = super.getXML(rootElementName);
        return root;
    }
}
