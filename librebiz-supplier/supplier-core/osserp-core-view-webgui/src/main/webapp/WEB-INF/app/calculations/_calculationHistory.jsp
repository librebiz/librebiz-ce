<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="calculationHistoryView"/>

<div class="modalBoxHeader">
    <div class="modalBoxHeaderLeft">
        <fmt:message key="calculationHistory"/>
    </div>
    <div class="modalBoxHeaderRight">
        <div class="boxnav">
        </div>
    </div>
</div>
<div class="modalBoxData">
    <div class="row">
        <div class="col-md-12 panel-area">
            <div class="table-responsive">
                <table class="table table-striped">
                    <tr>
                        <th class="center"><fmt:message key="numShort"/></th>
                        <th><fmt:message key="date"/></th>
                        <th><fmt:message key="nameCreatedFor"/></th>
                        <th class="right"><fmt:message key="netPrice"/></th>
                    </tr>
                    <c:forEach var="calc" items="${view.list}">
                        <tr>
                            <td class="center"><o:out value="${calc.calculationNumber}"/></td>
                            <td><o:date value="${calc.created}" addtime="true" addcentury="false"/></td>
                            <td>
                                <a href="<c:url value="/calculations.do?method=select&id=${calc.id}"/>">
                                    <c:choose>
                                        <c:when test="${calc.initial}">
                                            <fmt:message key="offerCalculation"/>
                                        </c:when>
                                        <c:otherwise>
                                            <o:out value="${calc.name}"/>
                                        </c:otherwise>
                                    </c:choose>
                                </a>
                            </td>
                            <td class="right"><o:number value="${calc.netAmount}" format="currency"/></td>
                        </tr>
                    </c:forEach>
                    <c:if test="${view.calculationCreateable}">
                        <tr>
                            <td> </td>
                            <td colspan="3"><a href="<c:url value="/calculations.do?method=forwardCreate"/>"><fmt:message key="createNewCalculation"/></a></td>
                        </tr>
                    </c:if>
                </table>
            </div>
        </div>
    </div>
</div>
