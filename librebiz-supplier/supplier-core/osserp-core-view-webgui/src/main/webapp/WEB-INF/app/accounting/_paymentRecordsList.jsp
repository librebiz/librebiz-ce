<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="table-responsive table-responsive-default">
	<table class="table">
		<thead>
			<tr>
                <th class="recordDate"><fmt:message key="bankAccount" /></th>
                <th class="recordType"><fmt:message key="type" /></th>
                <th class="recordDate"><fmt:message key="demand"/></th>
				<th class="recordName"><fmt:message key="name" /></th>
				<th class="recordNote"><fmt:message key="description" /></th>
				<th class="recordAmount"><fmt:message key="amount" /></th>
                <o:permission role="accounting_documents,accounting_bank_documents,accounting_accountant_deny,executive_accounting,executive" info="permissionEditFinanceRecords">
				<th colspan="3"> </th>
                </o:permission>
                <o:forbidden role="accounting_documents,accounting_bank_documents,accounting_accountant_deny,executive_accounting,executive" info="permissionEditFinanceRecords">
                <th> </th>
                </o:forbidden>
			</tr>
		</thead>
		<tbody>
			<c:choose>
				<c:when test="${empty view.list}">
                    <tr>
                        <td colspan="7">
                            <o:permission role="accounting_documents,accounting_bank_documents,accounting_accountant_deny,executive_accounting,executive" info="permissionViewFinanceReports">
                                <c:choose>
                                    <c:when test="${!empty sessionScope.errors}">
                                        <span class="error"><o:out value="${sessionScope.errors}"/></span>
                                        <span style="margin-left: 20px;"> 
                                            <v:link url="/contacts/contactSearch/forward">[<fmt:message key="contactSelection" />]</v:link>
                                        </span>
                                        <o:removeErrors/>
                                    </c:when>
                                    <c:otherwise>
                                        <span><fmt:message key="noRecordsAvailable" /></span>
                                        <c:choose>
                                            <c:when test="${!empty view.selectedBankAccount}">
                                                <span style="margin-left: 20px;"> 
                                                    <v:link url="/accounting/accountTransaction/forward?account=${view.selectedBankAccount.id}&exit=/accounting/accountingIndex/forward">[<fmt:message key="create" />]</v:link>
                                                </span>
                                            </c:when>
                                            <c:otherwise>
                                                <span style="margin-left: 20px;"> 
                                                    <v:link url="/accounting/accountTransaction/forward?exit=/accounting/accountingIndex/forward">[<fmt:message key="create" />]</v:link>
                                                </span>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:otherwise>
                                </c:choose>
                            </o:permission>
                            <o:forbidden role="accounting_documents,accounting_bank_documents,accounting_accountant_deny,executive_accounting,executive" info="permissionViewFinanceReports">
                                <span><fmt:message key="noRecordsAvailable" /></span>
                            </o:forbidden>
                        </td>
                    </tr>
				</c:when>
				<c:otherwise>
					<c:forEach var="record" items="${view.list}" varStatus="s">
                        <c:if test="${empty view.year or view.year == record.paidInYear}">
                        <c:choose>
                            <c:when test="${record.accountStatusOnly}">
                                <tr class="summary">
                                    <td class="recordDate"><o:date value="${record.paidOn}" /></td>
                                    <td class="recordType"><fmt:message key="${record.accountStatusName}" /></td>
                                    <td class="recordSummary" colspan="3"> </td>
                                    <td class="recordAmount"><o:number value="${record.grossAmount}" format="currency" /></td>
                                    <o:permission role="accounting_documents,accounting_bank_documents,accounting_accountant_deny,executive_accounting,executive" info="permissionEditFinanceRecords">
                                    <td colspan="3"> </td>
                                    </o:permission>
                                    <o:forbidden role="accounting_documents,accounting_bank_documents,accounting_accountant_deny,executive_accounting,executive" info="permissionEditFinanceRecords">
                                    <td> </td>
                                    </o:forbidden>
                                </tr>
                            </c:when>
                            <c:otherwise>
                                <tr id="record${record.id}"<c:if test="${record.outflowOfCash}"> class="altrow"</c:if>>
                                    <td class="recordDate"><o:date value="${record.paidOn}" /></td>
                                    <td class="recordType">
                                        <c:choose>
                                            <c:when test="${record.purchaseInvoicePayment}">
                                                <a  href="<c:url value="/purchaseInvoice.do?method=display&exit=paymentRecordsDisplay&readonly=true&id=${record.invoiceId}&exitId=record${record.id}"/>">
                                                    <o:out value="${record.recordType.name}"/>
                                                </a>
                                            </c:when>
                                            <c:when test="${record.salesInvoicePayment}">
                                                <a href="<c:url value="/salesInvoice.do?method=display&exit=paymentRecordsDisplay&readonly=true&id=${record.invoiceId}&exitId=record${record.id}"/>">
                                                    <o:out value="${record.recordType.name}"/>
                                                </a>
                                            </c:when>
                                            <c:when test="${record.salesDownpayment}">
                                                <a href="<c:url value="/salesInvoice.do?method=displayDownpayment&exit=paymentRecordsDisplay&readonly=true&id=${record.invoiceId}&exitId=record${record.id}"/>">
                                                    <o:out value="${record.recordType.name}"/>
                                                </a>
                                            </c:when>
                                            <c:otherwise>
                                                <v:link url="/accounting/commonPayment/forward?id=${record.id}&exit=/accounting/paymentRecords/reload&exitId=record${record.id}">
                                                    <o:out value="${record.recordType.name}"/>
                                                </v:link>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td class="recordDate"><o:date value="${record.created}" /></td>
                                    <td class="recordName">
                                        <c:choose>
                                            <c:when test="${view.filterByContactEnabled}">
                                                <v:link url="/accounting/paymentRecords/filterByContact" title="displayAll">
                                                    <o:out value="${record.contactName}" limit="35" />
                                                </v:link>
                                            </c:when>
                                            <c:otherwise>
                                                <v:link url="/accounting/paymentRecords/filterByContact?id=${record.contactId}" title="displayContactBookingsOnly">
                                                    <o:out value="${record.contactName}" limit="35" />
                                                </v:link>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td class="recordNote">
                                        <c:choose>
                                            <c:when test="${record.purchaseInvoicePayment}">
                                                <a href="<c:url value="/purchaseInvoice.do?method=display&exit=paymentRecordsDisplay&readonly=true&id=${record.invoiceId}&exitId=record${record.id}"/>">
                                                    <o:out value="${record.invoiceNumber}" title="purchaseInvoice"/>
                                                </a>
                                            </c:when>
                                            <c:when test="${record.salesInvoicePayment}">
                                                <a href="<c:url value="/salesInvoice.do?method=display&exit=paymentRecordsDisplay&readonly=true&id=${record.invoiceId}&exitId=record${record.id}"/>">
                                                    <o:out value="${record.invoiceNumber}" title="invoice" />
                                                </a>
                                            </c:when>
                                            <c:when test="${record.salesDownpayment}">
                                                <a href="<c:url value="/salesInvoice.do?method=displayDownpayment&exit=paymentRecordsDisplay&readonly=true&id=${record.invoiceId}&exitId=record${record.id}"/>">
                                                    <o:out value="${record.invoiceNumber}" title="downpayment"/>
                                                </a>
                                            </c:when>
                                            <c:otherwise>
                                                <v:link url="/accounting/commonPayment/forward?id=${record.id}&exit=/accounting/paymentRecords/reload&exitId=record${record.id}">
                                                    <o:out value="${record.note}" limit="36" />
                                                </v:link>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td class="recordAmount">
                                        <span title="<o:number value="${record.accountStatusSummary}" format="currency" />">
                                            <o:number value="${record.grossAmount}" format="currency" />
                                        </span>
                                    </td>
                                    <c:choose>
                                        <c:when test="${record.commonPayment}">
                                                <o:permission role="accounting_documents,accounting_bank_documents,accounting_accountant_deny,executive_accounting,executive" info="permissionEditFinanceRecords">
                                                    <td class="icon center">
                                                        <v:link url="/accounting/paymentRecords/increaseTime?id=${record.id}&context=commonPayment" title="moveUpSameDay">
                                                            <o:img name="upIcon"/>
                                                        </v:link>
                                                    </td>
                                                    <td class="icon center">
                                                        <v:link url="/accounting/accountTransaction/copy?id=${record.id}&exit=/accounting/paymentRecords/reload" title="copyDataset">
                                                            <o:img name="copyIcon"/>
                                                        </v:link>
                                                    </td>
                                                </o:permission>
                                                <td class="icon center">
                                                    <c:if test="${!empty record.documentId}">
                                                        <v:link url="/dms/document/print?id=${record.documentId}" title="documentPrint"><o:img name="printIcon" /></v:link>
                                                    </c:if>
                                                </td>
                                            </c:when>
                                            <c:otherwise>
                                                <o:permission role="accounting_documents,accounting_bank_documents,accounting_accountant_deny,executive_accounting,executive" info="permissionEditFinanceRecords">
                                                    <td class="icon center">
                                                        <c:choose>
                                                            <c:when test="${record.purchaseInvoicePayment}">
                                                                <v:link url="/accounting/paymentRecords/increaseTime?id=${record.id}&context=purchasePayment" title="moveUpSameDay">
                                                                    <o:img name="upIcon"/>
                                                                </v:link>
                                                            </c:when>
                                                            <c:when test="${record.salesInvoicePayment or record.salesDownpayment}">
                                                                <v:link url="/accounting/paymentRecords/increaseTime?id=${record.id}&context=salesPayment" title="moveUpSameDay">
                                                                    <o:img name="upIcon"/>
                                                                </v:link>
                                                            </c:when>
                                                        </c:choose>
                                                    </td>
                                                    <td class="icon center">
                                                        <span title="<fmt:message key="copyInvoiceBasedPaymentNotSupported"/>">
                                                            <o:img name="importantDisabledIcon"/>
                                                        </span>
                                                    </td>
                                                </o:permission>
                                                <td class="icon center">
                                                    <c:if test="${record.unchangeable and !empty record.contextName}">
                                                        <v:link url="/records/recordPrint/print?name=${record.contextName}&id=${record.invoiceId}" title="documentPrint"><o:img name="printIcon" /></v:link>
                                                    </c:if>
                                                </td>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                </tr>
                            </c:otherwise>
                        </c:choose>
                        </c:if>                       
					</c:forEach>
				</c:otherwise>
			</c:choose>
		</tbody>
	</table>
</div>
