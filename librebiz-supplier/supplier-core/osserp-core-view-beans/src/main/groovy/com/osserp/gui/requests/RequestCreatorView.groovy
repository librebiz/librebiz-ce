/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.gui.requests

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.Option
import com.osserp.common.util.DateUtil

import com.osserp.core.BusinessTypeSelection
import com.osserp.core.crm.Campaign
import com.osserp.core.customers.Customer
import com.osserp.core.customers.CustomerSearch
import com.osserp.core.employees.Employee
import com.osserp.core.requests.Request
import com.osserp.core.sales.Sales
import com.osserp.core.sales.SalesCreator
import com.osserp.core.sales.SalesPersonSelector
import com.osserp.core.system.BranchOffice

import com.osserp.groovy.web.ViewContextException

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class RequestCreatorView extends AbstractRequestEditorView {
    private static Logger logger = LoggerFactory.getLogger(RequestCreatorView.class.getName())
    private static int REQUEST_TO_OFFER_MAX_DAYS = 3
    Customer customer
    Employee employee
    List<Employee> salesStaff = []
    protected String businessTypeSelectionName
    Long businessCaseId
    Date date1 = new Date()
    Date date2


    RequestCreatorView() {
        super()
        headerName = 'createNewRequest'
        businessTypeSelectionName = 'requestCreator'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (forwardRequest) {

            initCustomer()

            addressAutocompletionSupport = addressAutocompletionSupportEnabled
            salesStaff = salesPersonSelector.getSalesPersons(domainUser)
            logger.debug("initRequest: salesStaff lookup done [size=${salesStaff.size()}]")
            employee = domainEmployee
            if (!salesStaff) {
                salesStaff = [employee]
            }
            date2 = fetchPresentationDate()
            businessCaseId = null

            initBusinessTypeSelection(businessTypeSelectionName)
        }
    }

    protected void initCustomer() {
        Long id = form.getLong('id')
        if (!id) {
            throw new ViewContextException('error.id.missing', this)
        }
        setSelectedCustomer(customerSearch.find(id))
        logger.debug("initCustomer: done [id=${customer.id}]")
    }

    protected void setSelectedCustomer(Customer selectedCustomer) {
        if (!selectedCustomer) {
            throw new ViewContextException('error.customer.missing', this)
        }
        if (!selectedCustomer.typeId) {
            throw new ViewContextException(ErrorCode.CUSTOMER_TYPE_MISSING)
        }
        // 0 may valid status, don't replace with (!customer.status)
        if (selectedCustomer.getStatus() == null) {
            throw new ViewContextException(ErrorCode.CUSTOMER_STATUS_MISSING)
        }
        customer = selectedCustomer
    }

    protected void initBusinessTypeSelection(String name) {
        if (name) {
            BusinessTypeSelection typeSelection = businessTypeManager.findTypeSelection(name)
            if (typeSelection?.types) {
                requestTypes = typeSelection.types
                logger.debug("initBusinessTypeSelection: businessType selection found [name=${name}, count=${requestTypes.size()}]")
            } else {
                logger.debug('initBusinessTypeSelection: businessType selection not found, using defaults')
            }
            if (requestTypes.size() == 1) {
                Option requestType = requestTypes.get(0)
                initBusinessCase(requestManager.createEmpty(domainUser, customer, requestType))
                env.typeSelectionEnabled = false
            } else {
                env.typeSelectionEnabled = true
            }
        }
    }

    Boolean getRequestCreated() {
        bean?.primaryKey
    }

    Boolean getTypeSelectionEnabled() {
        env.typeSelectionEnabled
    }

    void selectType() {
        Long typeId = form.getLong('id')
        if (!typeId) {
            bean = null
        } else {
            Option requestType = getSelectedRequestType(typeId)
            initBusinessCase(requestManager.createEmpty(domainUser, customer, requestType))
        }
    }

    protected void initBusinessCase(Request request) {
        if (request?.type?.directSales && (!date2 || (date2 && DateUtil.isFuture(date2)))) {
            // reset date created
            date2 = new Date()
        }
        bean = request
    }

    void setCampaign(Campaign campaign) {
        bean?.setOrigin(campaign?.id)
    }

    @Override
    void save() {
        Request request = (bean instanceof Sales) ? bean.request : bean
        if (request == null) {
            logger.warn('save: no request found in view!')
            throw new ViewContextException(this)
        }
        Long salesId = form.getLong('salesId')
        if (salesId) {
            Employee sales = employeeSearch.findById(salesId)
            BranchOffice office = sales?.branch
            if (sales) {
                if (request.type.forceDefaultBranch) {
                    request.updateSales(sales, null)
                } else {
                    request.updateSales(sales, office)
                }
            }
        }
        request.setSalesCoId(form.getLong('salesCoId'))

        String addressName = form.getString('name')
        if (addressName) {
            request.address.name = addressName
        }
        String street = form.getString('street')
        if (street) {
            request.address.street = street
        }
        String zipcode = form.getString('zipcode')
        if (zipcode) {
            request.address.zipcode = zipcode
        }
        String city = form.getString('city')
        if (city) {
            request.address.city = city
        }
        Long country = form.getLong('country')
        if (country) {
            request.address.country = country
        }
        Long originTypeId = form.getLong('originTypeId')
        if (originTypeId) {
            request.originType = originTypeId
        }

        businessCaseId = form.getLong('businessCaseId')
        date1 = form.getDate('date1')
        date2 = form.getDate('date2')

        if (!updateOnly) {

            requestManager.validateCommons(request)
            requestManager.validateAddress(request.address)

            request.setSalesTalkDate(date1)
            if (request.type.directSales) {
                request.setPresentationDate(date1)
                if (date2) {
                    if (DateUtil.isFuture(date2)) {
                        throw new ClientException(ErrorCode.DATE_FUTURE)
                    }
                    request.created = date2
                }
            } else {
                request.setPresentationDate(fetchPresentationDate(date2))
            }
            if (request.type.externalIdProvided) {
                if (!businessCaseId) {
                    throw new ClientException(ErrorCode.ID_REQUIRED)
                }
                if (requestManager.exists(businessCaseId)
                        || salesManager.exists(businessCaseId)) {
                    throw new ClientException(ErrorCode.ID_EXISTS)
                }
                request.setExternalId(businessCaseId)
            }
            Long created = requestManager.createRequest(request)
            request = requestManager.load(created)
            if (request.type.directSales) {

                def sales = salesCreator.create(
                        domainUser,
                        request,
                        (Long) null,   // selectedOffer
                        date2 ?: new Date(), // dateConfirmed
                        (String) null) // note
                bean = sales
                logger.debug("save: done, created directSales [id=${sales?.id}]")
            } else {
                bean = request
                logger.debug("save: done, created new request [id=${created}]")
            }
        } else {
            logger.debug('save: done in update mode')
        }
    }

    private Date fetchPresentationDate(Date userInput) throws ClientException {
        if (userInput && !DateUtil.isFuture(userInput)) {
            throw new ClientException(ErrorCode.DATE_PAST)
        }
        int maxDays = systemPropertyAsInt('requestToOfferMaxDays')
        return (userInput ?: DateUtil.addDays(DateUtil.currentDate,
                maxDays > 0 ? maxDays : REQUEST_TO_OFFER_MAX_DAYS))
    }

    protected CustomerSearch getCustomerSearch() {
        getService(CustomerSearch.class.getName())
    }

    private SalesCreator getSalesCreator() {
        getService(SalesCreator.class.getName())
    }

    private SalesPersonSelector getSalesPersonSelector() {
        getService(SalesPersonSelector.class.getName())
    }
}
