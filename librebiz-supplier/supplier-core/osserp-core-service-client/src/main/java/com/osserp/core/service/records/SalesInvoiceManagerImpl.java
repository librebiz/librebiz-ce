/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 02-Feb-2007 02:44:42 
 * 
 */
package com.osserp.core.service.records;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;

import com.osserp.core.Item;
import com.osserp.core.customers.Customer;
import com.osserp.core.dao.PaymentConditions;
import com.osserp.core.dao.Products;
import com.osserp.core.dao.Projects;
import com.osserp.core.dao.records.SalesInvoices;
import com.osserp.core.dao.records.SalesOrderVolumeExportConfigs;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.DeliveryNote;
import com.osserp.core.finance.Downpayment;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.ListItem;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.PaymentAwareRecord;
import com.osserp.core.finance.Record;
import com.osserp.core.products.Product;
import com.osserp.core.sales.Sales;
import com.osserp.core.sales.SalesDeliveryNoteManager;
import com.osserp.core.sales.SalesInvoice;
import com.osserp.core.sales.SalesInvoiceManager;
import com.osserp.core.sales.SalesOrderVolumeExportConfig;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.system.SystemConfigManager;
import com.osserp.core.tasks.ProductPlanningCacheSender;
import com.osserp.core.tasks.SalesVolumeReportSender;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesInvoiceManagerImpl extends AbstractInvoiceManager implements SalesInvoiceManager {
    private static Logger log = LoggerFactory.getLogger(SalesInvoiceManagerImpl.class.getName());
    private Projects projects = null;
    private SalesOrderVolumeExportConfigs exportConfigs = null;
    private SalesDeliveryNoteManager deliveryNoteManager = null;
    private SalesVolumeReportSender salesVolumeReportSender = null;
    private RecordPrintService recordPrintService;

    public SalesInvoiceManagerImpl(
            SalesInvoices records,
            SystemConfigManager systemConfigManager,
            Products products,
            ProductPlanningCacheSender productPlanningTask,
            PaymentConditions paymentConditions,
            Projects projects,
            SalesOrderVolumeExportConfigs exportConfigs,
            SalesDeliveryNoteManager deliveryNoteManager,
            RecordPrintService recordPrintService,
            SalesVolumeReportSender salesVolumeReportSender) {
        super(records, systemConfigManager, products, productPlanningTask, paymentConditions);
        this.projects = projects;
        this.deliveryNoteManager = deliveryNoteManager;
        this.exportConfigs = exportConfigs;
        this.salesVolumeReportSender = salesVolumeReportSender;
        this.recordPrintService = recordPrintService;
    }

    public SalesInvoice changeNumber(Employee user, SalesInvoice salesInvoice, Long id) throws ClientException {
        SalesInvoice changed = getInvoiceDao().changeNumber(user, salesInvoice, id);
        if (changed.getId().equals(id)) {
            recordPrintService.archiveCleanup(salesInvoice);
            if (changed.isUnchangeable()) {
                byte[] pdf = recordPrintService.createPdf(user, changed, false);
                recordPrintService.archive(user, changed, pdf);
            }
            return changed;
        }
        if (log.isInfoEnabled()) {
            log.info("changeNumber() done without change [invoice="
                    + salesInvoice.getId() + ", requestedId=" + id + "]");
        }
        return salesInvoice;
    }

    public SalesInvoice create(
            Employee user, 
            Order order, 
            Long type, 
            Customer thirdParty, 
            BigDecimal partialInvoiceAmount, 
            BigDecimal partialInvoiceAmountGross,
            boolean copyNote,
            boolean copyTerms,
            boolean copyItems,
            Long userDefinedId, 
            Date userDefinedDate) throws ClientException {

        if (order == null || user == null) {
            String err = "createInvoice() order or createdBy was null!";
            log.error(err);
            throw new BackendException(err);
        }
        // check that order is unchangeable, e.g. valid
        if (!order.isUnchangeable()) {
            throw new ClientException(ErrorCode.ORDER_MISSING);
        }
        SalesInvoices invoices = getInvoiceDao();
        return invoices.create(user, order, type, thirdParty, 
                partialInvoiceAmount, partialInvoiceAmountGross,
                copyNote, copyTerms, copyItems, userDefinedId, userDefinedDate);
    }

    public SalesInvoice create(
            Employee user,
            Order order,
            boolean percentage,
            BigDecimal amount,
            Product product,
            String note,
            boolean printOrderItems,
            boolean copyNote,
            boolean copyTerms,
            Long userDefinedId, 
            Date userDefinedDate) throws ClientException {

        return getInvoiceDao().create(
                user,
                order,
                percentage,
                amount,
                product,
                note,
                printOrderItems,
                copyNote,
                copyTerms, 
                userDefinedId, 
                userDefinedDate);
    }

    public SalesInvoice create(
            Employee user, 
            SalesInvoice salesInvoice, 
            boolean copyReferenceId, 
            Long userDefinedId, 
            Date userDefinedDate, 
            boolean historical) {
        if (log.isDebugEnabled()) {
            log.debug("create() invoked [user=" + user.getId() 
                    + ", existing=" + salesInvoice.getId() 
                    + ", copyReferenceId=" + copyReferenceId 
                    + ", userDefinedId=" + userDefinedId
                    + ", userDefinedDate=" + userDefinedDate
                    + ", historical=" + historical + "]");
        }
        return getInvoiceDao().create(user, salesInvoice, copyReferenceId, userDefinedId, userDefinedDate, historical);
    }

    public PaymentAwareRecord create(Employee user, BranchOffice office,
			Customer customer, Long id, Date created, boolean historical)
			throws ClientException {
    	validateCreate(getInvoiceDao().getRecordType(), id, created, historical);
        return getInvoiceDao().create(user, office, customer, id, created, historical);
    }

    public SalesInvoice enableHistoricalStatus(SalesInvoice invoice) {
        return getInvoiceDao().changeHistoricalStatus(invoice, true);
    }

    public SalesInvoice disableHistoricalStatus(SalesInvoice invoice) {
        return getInvoiceDao().changeHistoricalStatus(invoice, false);
    }

    public List<Record> findInternal(Long company, Customer customer) {
        if (log.isDebugEnabled()) {
            log.debug("find() invoked [company=" + company + ", customer="
                    + ((customer == null) ? "null" : customer.getId().toString()) + "]");
        }
        List<Record> result = new ArrayList(getByContact(customer, company));
        for (Iterator<Record> i = result.iterator(); i.hasNext();) {
            Record next = i.next();
            if (!next.isInternal()) {
                i.remove();
            }
        }
        return result;
    }

    public List<SalesInvoice> findThirdParty(Sales sales) {
        return getInvoiceDao().getThirdPartyBySales(sales);
    }

    public List<ListItem> findProductListing(Product product) {
        return getInvoiceDao().getProductListing(product.getProductId());
    }

    public List<SalesInvoice> findPartial(Sales sales) {
        return getInvoiceDao().getPartialBySales(sales);
    }

    @Override
    protected void doAfterStatusUpdate(Record record, Employee employee, Long oldStatus) {
        if (justReleased(record, oldStatus) && record instanceof Invoice) {

            Invoice invoice = (Invoice) record;
            if (invoice.getPaymentCondition().isDonation()) {
                invoice.updateStatus(Record.STAT_CLOSED);
                persist(invoice);
            }
            if (isStockAffecting(invoice)) {

                Record dn = deliveryNoteManager.create(employee, invoice);
                if (dn != null) {
                    if (log.isDebugEnabled()) {
                        log.debug("doAfterStatusUpdate() delivery note created [user="
                                + employee.getId()
                                + ", invoice=" + record.getId()
                                + ", deliveryNote=" + dn.getId()
                                + "]");
                    }
                    try {
                        deliveryNoteManager.updateStatus(
                                deliveryNoteManager.find(dn.getId()),
                                employee,
                                Record.STAT_SENT);
                    } catch (ClientException c) {
                        log.warn("doAfterStatusUpdate() failed [message=" + c.toString() + "]", c);
                    }
                }
            } else if (invoice.getBusinessCaseId() != null) {
                try {
                    Sales sales = projects.load(invoice.getBusinessCaseId());
                    if (sales.getType().isSubscription()) {
                        sales.setCapacity(getSummary(sales));
                        projects.save(sales);
                        if (log.isDebugEnabled()) {
                            log.debug("doAfterStatusUpdate() sales capacity update done [sales="
                                    + sales.getId()
                                    + ", capacity=" + sales.getCapacity()
                                    + "]");
                        }
                    }
                } catch (Exception e) {
                    // optional operation should not break invoice release
                    log.warn("doAfterStatusUpdate() failed [message=" + e.toString() + "]", e);
                }
                
            }
            if (salesVolumeReportSender != null) {
                salesVolumeReportSender.send(invoice);
            }
        }
    }
    
    
    protected void doAfterPrintValidation(Record record, Long nextStatus) throws ClientException {
        Invoice invoice = (Invoice) record;
        if (Record.STAT_SENT.equals(nextStatus) && !(invoice instanceof Downpayment)) {
            if (!invoice.isUnchangeable() 
                    && isStockAffecting(invoice) 
                    && !isAvailableOnStock(invoice)) {
                throw new ClientException(ErrorCode.STOCK_AVAILABILITY);
            }
        }
    }
    
    private Double getSummary(Sales sales) {
        if (sales == null) {
            return 0d;
        }
        Double result = sales.getCapacity();
        result = getInvoiceDao().getSummary(sales);
        return result;
    }

    public boolean isStockAffecting(Invoice invoice) {
        if (!invoice.isInternal() && invoice.getReference() == null) {
            for (SalesOrderVolumeExportConfig config : exportConfigs.getConfigs()) {
                if (invoice.getCompany().equals(config.getCompany().getId())
                        && invoice.getContact().getId().equals(config.getCustomer().getId())) {
                    return false;
                }
            }
            Long orderId = null;

            if (invoice instanceof SalesInvoice) {
                SalesInvoice si = (SalesInvoice) invoice;
                if (si.getThirdPartyOrder() != null) {
                    SalesInvoices dao = getInvoiceDao();
                    List<Record> finalInvoiceList = new ArrayList(dao.getByReference(
                            si.getThirdPartyOrder()));
                    if (!finalInvoiceList.isEmpty()) {
                        for (int i = 0, j = finalInvoiceList.size(); i < j; i++) {
                            Record finalInvoice = finalInvoiceList.get(i);
                            if (finalInvoice.isCanceled()) {
                                orderId = si.getThirdPartyOrder();
                            }
                        }
                    } else {
                        orderId = si.getThirdPartyOrder();
                    }
                }
            }
            if (orderId != null) {
                // fetch deliveries
                List<DeliveryNote> notes = deliveryNoteManager.getByOrder(orderId);
                Map<Long, Item> deliveredProducts = new java.util.HashMap<Long, Item>();
                for (int i = 0, j = notes.size(); i < j; i++) {
                    DeliveryNote dn = notes.get(i);
                    for (int k = 0, l = dn.getItems().size(); k < l; k++) {
                        Item item = dn.getItems().get(k);
                        if (deliveredProducts.containsKey(item.getProduct().getProductId())) {
                            Item existing = deliveredProducts.get(item.getProduct().getProductId());
                            existing.setQuantity(existing.getQuantity() + item.getQuantity());
                        } else {
                            Item clone = (Item) item.clone();
                            deliveredProducts.put(item.getProduct().getProductId(), clone);
                        }
                    }
                }
                // check against deliveries
                boolean foundViolation = false;
                for (int i = 0, j = invoice.getItems().size(); i < j; i++) {
                    Item item = invoice.getItems().get(i);
                    if (item.getProduct().isAffectsStock()) {
                        if (deliveredProducts.containsKey(item.getProduct().getProductId())) {
                            Item delivered = deliveredProducts.get(item.getProduct().getProductId());
                            if (item.getQuantity() > delivered.getQuantity()) {
                                foundViolation = true;
                                if (log.isDebugEnabled()) {
                                    log.debug("isDeliveryNoteAffecting() found product to check for stock availability");
                                }
                                break;
                            }
                        }
                    }
                }
                if (!foundViolation) {
                    if (log.isDebugEnabled()) {
                        log.debug("isDeliveryNoteAffecting() ignoring invoice while already delivered via delivery notes");
                    }
                    // TODO check against other invoices related to same order via thirdPartyOrder attribute 
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public boolean isAvailableOnStock(Invoice invoice) {
        boolean result = true;
        for (int i = 0, j = invoice.getItems().size(); i < j; i++) {
            Item item = invoice.getItems().get(i);
            getProducts().loadSummary(item.getProduct());
            item.getProduct().enableStock(item.getStockId());
            if (item.getProduct().isAffectsStock()) {
                Double inStock = item.getProduct().getSummary().getStock();
                Double inStockReceipt = item.getProduct().getSummary().getReceipt();
                if (item.getQuantity() > (inStock + inStockReceipt)) {
                    result = false;
                    break;
                }
            }
        }
        return result;
    }

    public Invoice updateDeEst35a(Invoice invoice, String deEst35a, String deEst35aTax) {
        if (invoice instanceof SalesInvoice) {
            SalesInvoice obj = (SalesInvoice) invoice;
            obj.setDeEst35a(deEst35a);
            obj.setDeEst35aTax(deEst35aTax);
            SalesInvoices invoices = getInvoiceDao();
            invoices.save(obj);
            return (Invoice) invoices.load(obj.getId());
        }
        return invoice;
    }

    private SalesInvoices getInvoiceDao() {
        return (SalesInvoices) getDao();
    }
}
