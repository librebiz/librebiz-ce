/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 6, 2009 4:52:36 PM 
 * 
 */
package com.osserp.core.products;

import com.osserp.common.Option;
import com.osserp.core.employees.Employee;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.system.SystemCompany;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ProductSelectionConfig extends ProductSelectionItemAware, Option {

    /**
     * Limits product selection to members of company
     * @return company or null if all companies supported by selection
     */
    SystemCompany getCompany();

    /**
     * Limits product selection to members of branch
     * @return branch or null if all branchs supported by selection
     */
    BranchOffice getBranch();

    /**
     * Provides the context if config is dedicated to any
     * @return context
     */
    ProductSelectionContext getContext();

    /**
     * Sets the -optional- selection context
     * @param context
     */
    void setContext(ProductSelectionContext context);

    /**
     * Indicates that selection is an exclusion
     * @return exclusion
     */
    boolean isExclusion();

    /**
     * Enables/disables exclusion flag
     * @param exlusion
     */
    void setExclusion(boolean exlusion);

    /**
     * Indicates that product matches any added selection item. An product matches selection if<br/>
     * - No selection items configured (filter is off) <br/>
     * - All selection properties are null <br/>
     * - Product matches configured (not null) properties <br/>
     * - Empty/null selection property evaluates true
     * @param product
     * @return true if product matches any configured selection item
     */
    boolean isMatching(Product product);

    /**
     * Adds a selection item
     * @param user
     * @param type
     * @param group
     * @param category
     */
    void addItem(
            Employee user,
            ProductType type,
            ProductGroup group,
            ProductCategory category);

    /**
     * Removes an item identified by primary key
     * @param id of the item to remove
     */
    void removeItem(Long id);

    /**
     * Indicates that selection is currently disabled.
     * @return true if filter is disabled
     */
    boolean isDisabled();

    /**
     * Enables/disables product selection config
     * @param disabled
     */
    void setDisabled(boolean disabled);

    /**
     * Indicates movable config (e.g. change context)
     * @return true if movable
     */
    boolean isMovable();

    /**
     * Enables/disables movable property
     * @param movable
     */
    void setMovable(boolean movable);
}
