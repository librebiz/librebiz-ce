<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<c:set var="contactView" value="${sessionScope.contactView}"/>
<c:set var="contact" value="${contactView.bean}"/>
<oc:systemPropertyEnabled name="websiteSyncSupport">
	<tr>
		<td><fmt:message key="internet" /></td>
		<td>
			<c:choose>
				<c:when test="${contact.internetExportEnabled}">
					<c:choose>
						<c:when test="${internetConfigPermission}">
							<a href="<c:url value="/employees.do?method=changeWebsiteDisplay"/>">
								<fmt:message key="internetDisplayEnabled" />
							</a>
						</c:when>
						<c:otherwise>
							<fmt:message key="internetDisplayEnabled" />
						</c:otherwise>
					</c:choose>
				</c:when>
				<c:otherwise>
					<c:choose>
						<c:when test="${internetConfigPermission}">
							<a href="<c:url value="/employees.do?method=changeWebsiteDisplay"/>">
								<fmt:message key="internetDisplayDisabled" />
							</a>
						</c:when>
						<c:otherwise>
							<fmt:message key="internetDisplayDisabled" />
						</c:otherwise>
					</c:choose>
				</c:otherwise>
			</c:choose>
		</td>
	</tr>
</oc:systemPropertyEnabled>
