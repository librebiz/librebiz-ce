/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.gui.directory

import javax.servlet.http.HttpServletRequest

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.common.ClientException
import com.osserp.groovy.web.ViewController

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class AbstractDirectoryUserController extends ViewController {
    private static Logger logger = LoggerFactory.getLogger(AbstractDirectoryUserController.class.getName())

    @RequestMapping
    def enableDisplayResignMode(HttpServletRequest request) {
        logger.debug("enableDisplayResignMode: invoked [user=${getUserId(request)}, class=${getClass().getName()}]")
        def view = getView(request)
        try {
            view.enableDisplayResignMode()
        } catch (ClientException e) {
            view.disableDisplayResignMode()
            saveError(request, e.message)
        }
        return defaultPage
    }

    @RequestMapping
    def disableDisplayResignMode(HttpServletRequest request) {
        logger.debug("disableDisplayResignMode: invoked [user=${getUserId(request)}, class=${getClass().getName()}]")
        def view = getView(request)
        view.disableDisplayResignMode()
        return defaultPage
    }

    @RequestMapping
    def enableMailAccount(HttpServletRequest request) {
        logger.debug("enableMailAccount: invoked [user=${getUserId(request)}, class=${getClass().getName()}]")
        def view = getView(request)
        view.enableMailAccount()
        return defaultPage
    }

    @RequestMapping
    def disableMailAccount(HttpServletRequest request) {
        logger.debug("disableMailAccount: invoked [user=${getUserId(request)}, class=${getClass().getName()}]")
        def view = getView(request)
        view.disableMailAccount()
        return defaultPage
    }

    @RequestMapping
    def enableMailAccountEditMode(HttpServletRequest request) {
        logger.debug("enableMailAccountEditMode: invoked [user=${getUserId(request)}, class=${getClass().getName()}]")
        def view = getView(request)
        view.enableMailAccountEditMode()
        return defaultPage
    }

    @RequestMapping
    def disableMailAccountEditMode(HttpServletRequest request) {
        logger.debug("disableMailAccountEditMode: invoked [user=${getUserId(request)}, class=${getClass().getName()}]")
        def view = getView(request)
        view.disableMailAccountEditMode()
        return defaultPage
    }

    @RequestMapping
    def enablePasswordEditMode(HttpServletRequest request) {
        logger.debug("enablePasswordEditMode: invoked [user=${getUserId(request)}, class=${getClass().getName()}]")
        def view = getView(request)
        view.enablePasswordEditMode()
        return defaultPage
    }

    @RequestMapping
    def disablePasswordEditMode(HttpServletRequest request) {
        logger.debug("disablePasswordEditMode: invoked [user=${getUserId(request)}, class=${getClass().getName()}]")
        def view = getView(request)
        view.disablePasswordEditMode()
        return defaultPage
    }

    @RequestMapping
    def userReAssign(HttpServletRequest request) {
        logger.debug("userReAssign: invoked [user=${getUserId(request)}, class=${getClass().getName()}]")
        def view = getView(request)
        view.userReAssign()
        return defaultPage
    }

    @RequestMapping
    def userResign(HttpServletRequest request) {
        logger.debug("userResign: invoked [user=${getUserId(request)}, class=${getClass().getName()}]")
        def view = getView(request)
        view.userResign()
        return defaultPage
    }

    @RequestMapping
    def selectProperty(HttpServletRequest request) {
        logger.debug("selectProperty: invoked [user=${getUserId(request)}, class=${getClass().getName()}]")
        def view = getView(request)
        view.selectProperty()
        return defaultPage
    }

    @RequestMapping
    def updateProperty(HttpServletRequest request) {
        logger.debug("updateProperty: invoked [user=${getUserId(request)}, class=${getClass().getName()}]")
        def view = getView(request)
        view.updateProperty()
        return defaultPage
    }
}

