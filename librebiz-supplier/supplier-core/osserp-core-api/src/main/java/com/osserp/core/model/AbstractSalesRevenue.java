/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 1, 2011 10:44:43 AM 
 * 
 */
package com.osserp.core.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.beans.AbstractOption;
import com.osserp.common.util.NumberUtil;

import com.osserp.core.BusinessCase;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Record;
import com.osserp.core.requests.Request;
import com.osserp.core.sales.Sales;
import com.osserp.core.sales.SalesRevenue;

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public abstract class AbstractSalesRevenue extends AbstractOption implements SalesRevenue, Serializable {
    private static Logger log = LoggerFactory.getLogger(AbstractSalesRevenue.class.getName());
    protected static final Double SALES_VOLUME_PERCENT = 100d;

    private BusinessCase businessCase = null;
    private Employee salesPerson = null;
    private List<Record> salesCreditNotes = new java.util.ArrayList<Record>();

    private Double _salesVolume = 0d;
    private Double _salesCreditVolume = 0d;

    private Double hardwareCostsA = 0d;
    protected String hardwareCostsADescription;
    private Double hardwareCostsB = 0d;
    protected String hardwareCostsBDescription;
    private Double hardwareCostsC = 0d;
    protected String hardwareCostsCDescription;
    private Double accessoryCosts = 0d;
    protected String accessoryCostsDescription;
    private Double serviceCosts = 0d;
    protected String serviceCostsDescription;

    private Double projectCosts = 0d;
    private Double unknownCosts = 0d;

    private Double agentCosts = 0d;
    private Double salesCosts = 0d;
    private Double tipCosts = 0d;

    private Double costs = 0d;
    private Double netCosts = 0d;
    private Double totalCosts = 0d;
    private Double finalCosts = 0d;

    private BigDecimal salesPrice = new BigDecimal(0d);
    private Double minimalMargin = 0d;
    private Double targetMargin = 0d;

    private boolean postCalculation = false;
    private SalesRevenue postCalculationRevenue = null;

    protected AbstractSalesRevenue() {
        super();
    }

    /**
     * Creates a new sales revenue calculation object
     * @param businessCase
     * @param salesPerson
     * @param id primary key of corresponding object
     * @param name
     * @param salesPrice
     * @param minimalMargin
     * @param targetMargin
     */
    protected AbstractSalesRevenue(
            BusinessCase businessCase,
            Employee salesPerson,
            Long id,
            String name,
            BigDecimal salesPrice,
            Double minimalMargin,
            Double targetMargin) {
        super(id, name);
        this.businessCase = businessCase;
        this.salesPerson = salesPerson;
        this.salesPrice = salesPrice;
        this.minimalMargin = minimalMargin;
        this.targetMargin = targetMargin;
        this.calculateSalesCosts();
    }

    /**
     * Creates a new sales revenue calculation object
     * @param businessCase
     * @param salesPerson
     * @param id primary key of corresponding object
     * @param name
     * @param salesPrice
     * @param minimalMargin
     * @param targetMargin
     * @param salesCreditNotes
     */
    protected AbstractSalesRevenue(
            BusinessCase businessCase,
            Employee salesPerson,
            Long id,
            String name,
            BigDecimal salesPrice,
            Double minimalMargin,
            Double targetMargin,
            List<Record> salesCreditNotes) {
        this(businessCase, salesPerson, id, name, salesPrice, minimalMargin, targetMargin);
        this.salesCreditNotes = salesCreditNotes;
        this.calculateSalesCosts();
    }

    /**
     * Creates a new sales revenue calculation object
     * @param businessCase
     * @param salesPerson
     * @param id
     * @param name
     * @param salesPrice
     * @param minimalMargin
     * @param targetMargin
     * @param salesCreditNotes
     * @param hardwareCostsA
     * @param hardwareCostsB
     * @param hardwareCostsC
     * @param accessoryCosts
     * @param serviceCosts
     */
    protected AbstractSalesRevenue(
            BusinessCase businessCase,
            Employee salesPerson,
            Long id,
            String name,
            BigDecimal salesPrice,
            Double minimalMargin,
            Double targetMargin,
            List<Record> salesCreditNotes,
            Double hardwareCostsA,
            Double hardwareCostsB,
            Double hardwareCostsC,
            Double accessoryCosts,
            Double serviceCosts) {
        this(businessCase, salesPerson, id, name, salesPrice, minimalMargin, targetMargin, salesCreditNotes);
        this.hardwareCostsA = hardwareCostsA;
        this.hardwareCostsB = hardwareCostsB;
        this.hardwareCostsC = hardwareCostsC;
        this.accessoryCosts = accessoryCosts;
        this.serviceCosts = serviceCosts;
        this.salesCreditNotes = salesCreditNotes;
        this.calculateSalesCosts();
        this.calculate();
    }

    protected abstract void calculate();

    public boolean isProvidesItems() {
        return false;
    }

    public boolean isPostCalculation() {
        return postCalculation;
    }

    public void setPostCalculation(boolean postCalculation) {
        this.postCalculation = postCalculation;
    }

    public SalesRevenue getPostCalculationRevenue() {
        return postCalculationRevenue;
    }

    public void setPostCalculationRevenue(SalesRevenue postCalculationRevenue) {
        this.postCalculationRevenue = postCalculationRevenue;
    }

    public BusinessCase getBusinessCase() {
        return businessCase;
    }

    public Employee getSalesPerson() {
        return salesPerson;
    }

    public Double getHardwareCostsA() {
        return hardwareCostsA;
    }

    public Double getHardwareCostsB() {
        return hardwareCostsB;
    }

    public Double getHardwareCostsC() {
        return hardwareCostsC;
    }

    public Double getAccessoryCosts() {
        return accessoryCosts;
    }

    public Double getServiceCosts() {
        return serviceCosts;
    }

    public Double getProjectCosts() {
        return projectCosts;
    }

    public Double getUnknownCosts() {
        return unknownCosts;
    }

    public Double getAgentCosts() {
        return agentCosts;
    }

    protected void setAgentCosts(Double agentCosts) {
        this.agentCosts = agentCosts;
    }

    public Double getSalesCosts() {
        return salesCosts;
    }

    protected void setSalesCosts(Double salesCosts) {
        this.salesCosts = salesCosts;
    }

    public Double getTipCosts() {
        return tipCosts;
    }

    protected void setTipCosts(Double tipCosts) {
        this.tipCosts = tipCosts;
    }

    public Double getCosts() {
        return costs;
    }

    protected void setCosts(Double costs) {
        this.costs = costs;
    }

    public Double getNetCosts() {
        return netCosts;
    }

    protected void setNetCosts(Double netCosts) {
        this.netCosts = netCosts;
    }

    public Double getTotalCosts() {
        return totalCosts;
    }

    protected void setTotalCosts(Double totalCosts) {
        this.totalCosts = totalCosts;
    }

    public Double getFinalCosts() {
        return finalCosts;
    }

    protected void setFinalCosts(Double finalCosts) {
        this.finalCosts = finalCosts;
    }

    public Double getSalesCreditVolume() {
        if (!salesCreditNotes.isEmpty() && (_salesCreditVolume == null || _salesCreditVolume.equals(0d))) {
            for (int i = 0, j = salesCreditNotes.size(); i < j; i++) {
                Record creditNote = salesCreditNotes.get(i);
                _salesCreditVolume = _salesCreditVolume + creditNote.getAmounts().getAmount().doubleValue();
            }
        }
        return _salesCreditVolume;
    }

    public Double getSalesVolume() {
        if (_salesVolume == null || _salesVolume.equals(0d)) {
            if (isSet(salesPrice)) {
                try {
                    _salesVolume = salesPrice.doubleValue();
                } catch (Throwable t) {
                    _salesVolume = 0d;
                }
            }
            if (!salesCreditNotes.isEmpty()) {
                if (log.isDebugEnabled()) {
                    log.debug("getSalesVolume() found credit note(s) [count=" + salesCreditNotes.size() + "]");
                }
                _salesVolume = _salesVolume - getSalesCreditVolume().doubleValue();
            }
        }
        return _salesVolume;
    }

    public Double getProfit() {
        return getSalesVolume() - costs;
    }

    public Double getMinimalMarginPercent() {
        Double result = (businessCase != null) ? businessCase.getType().getMinimalMargin() : 0d;
        if (isSet(minimalMargin)) {
            result = minimalMargin;
        }
        return NumberUtil.round(result, 2);
    }

    public Double getMissingMarginPercent() {
        if (!isProfitLowerMinimum()) {
            return 0d;
        }
        return NumberUtil.round(getMinimalMarginPercent() - getProfitPercent(), 2);
    }

    public Double getTargetMarginPercent() {
        Double result = (businessCase != null) ? businessCase.getType().getTargetMargin() : 0d;
        if (isSet(targetMargin)) {
            result = targetMargin;
        }
        return NumberUtil.round(result, 2);
    }

    public boolean isProfitLowerMinimum() {
        return getProfitPercent() < getMinimalMarginPercent();
    }

    public boolean isProfitLowerTarget() {
        return getProfitPercent() < getTargetMarginPercent();
    }

    public Double getTotalProfit() {
        return getSalesVolume() - totalCosts;
    }

    public Double getFinalProfit() {
        return getSalesVolume() - finalCosts;
    }

    public Double getHardwareCostsAPercent() {
        return getPercent(hardwareCostsA);
    }

    public String getHardwareCostsADescription() {
        return hardwareCostsADescription;
    }

    public Double getHardwareCostsBPercent() {
        return getPercent(hardwareCostsB);
    }

    public String getHardwareCostsBDescription() {
        return hardwareCostsBDescription;
    }

    public Double getHardwareCostsCPercent() {
        return getPercent(hardwareCostsC);
    }

    public String getHardwareCostsCDescription() {
        return hardwareCostsCDescription;
    }

    public Double getAccessoryCostsPercent() {
        return getPercent(accessoryCosts);
    }

    public String getAccessoryCostsDescription() {
        return accessoryCostsDescription;
    }

    public Double getServiceCostsPercent() {
        return getPercent(serviceCosts);
    }

    public String getServiceCostsDescription() {
        return serviceCostsDescription;
    }

    public Double getAgentCostsPercent() {
        return getPercent(agentCosts);
    }

    public Double getSalesCostsPercent() {
        return getPercent(salesCosts);
    }

    public Double getTipCostsPercent() {
        return getPercent(tipCosts);
    }

    public Double getCostsPercent() {
        return getPercent(costs);
    }

    public Double getTotalCostsPercent() {
        return getPercent(totalCosts);
    }

    public Double getProjectCostsPercent() {
        return getPercent(projectCosts);
    }

    public Double getUnknownCostsPercent() {
        return getPercent(unknownCosts);
    }

    public Double getFinalCostsPercent() {
        return getPercent(finalCosts);
    }

    public Double getSalesVolumePercent() {
        return SALES_VOLUME_PERCENT;
    }

    public Double getProfitPercent() {
        return getPercent(getProfit());
    }

    public Double getTotalProfitPercent() {
        return getPercent(getTotalProfit());
    }

    public Double getFinalProfitPercent() {
        return getPercent(getFinalProfit());
    }

    public Double getTargetVolume() {
        return netCosts / (1 - getTargetMarginPercent() / SALES_VOLUME_PERCENT);
    }

    public boolean isHardwareCostsAAvailable() {
        return isSet(hardwareCostsA);
    }

    public boolean isHardwareCostsBAvailable() {
        return isSet(hardwareCostsB);
    }

    public boolean isHardwareCostsCAvailable() {
        return isSet(hardwareCostsC);
    }

    public boolean isAccessoryCostsAvailable() {
        return isSet(accessoryCosts);
    }

    public boolean isServiceCostsAvailable() {
        return isSet(serviceCosts);
    }

    public boolean isProjectCostsAvailable() {
        return isSet(projectCosts);
    }

    public boolean isUnknownCostsAvailable() {
        return isSet(unknownCosts);
    }

    public boolean isAgentCostsAvailable() {
        return isSet(agentCosts);
    }

    public boolean isTipCostsAvailable() {
        return isSet(tipCosts);
    }

    public boolean isSalesCostsAvailable() {
        return isSet(salesCosts);
    }

    /**
     * Updates costs required by calculate. Required if costs not already set via constructor.
     * @param hardwareCostsA
     * @param hardwareCostsB
     * @param hardwareCostsC
     * @param accessoryCosts
     * @param serviceCosts
     * @param projectCosts
     * @param unknownCosts
     */
    protected void update(
            Double hardwareCostsA,
            Double hardwareCostsB,
            Double hardwareCostsC,
            Double accessoryCosts,
            Double serviceCosts,
            Double projectCosts,
            Double unknownCosts) {
        this.hardwareCostsA = hardwareCostsA;
        this.hardwareCostsB = hardwareCostsB;
        this.hardwareCostsC = hardwareCostsC;
        this.accessoryCosts = accessoryCosts;
        this.serviceCosts = serviceCosts;
        this.projectCosts = projectCosts;
        this.unknownCosts = unknownCosts;
    }

    /**
     * Calculates sales costs (agent,tip & provision)
     */
    protected final void calculateSalesCosts() {
        Request request = null;
        BusinessCase bc = getBusinessCase();
        if (bc instanceof Sales) {
            request = ((Sales) bc).getRequest();
        } else if (bc instanceof Request) {
            request = (Request) bc;
        }
        if (request != null) {
            if (request.getAgent() != null) {
                Double agentCommission = request.getAgentCommission();
                if (agentCommission != null) {
                    if (request.isAgentCommissionPercent()) {
                        if (salesPrice != null) {
                            setAgentCosts(salesPrice.multiply(new BigDecimal(agentCommission / 100)).doubleValue());
                        }
                    } else {
                        setAgentCosts(agentCommission);
                    }
                }
            }
            if (request.getTip() != null) {
                Double tipCommission = request.getTipCommission();
                if (tipCommission != null) {
                    setTipCosts(tipCommission);
                }
            }
            if (getSalesPerson() != null
                    && getSalesPerson().getEmployeeType() != null
                    && getSalesPerson().getEmployeeType().isSalesAgent()) {
                setSalesCosts(request.getSalesCommission() + request.getSalesCommissionGoodwill());
            }
        }
    }

    protected final Double getPercent(Double value) {
        try {
            return (value / (getSalesVolume() / SALES_VOLUME_PERCENT));
        } catch (Exception e) {
            return 0d;
        }
    }
}
