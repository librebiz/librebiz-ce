/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 25-Oct-2005 11:49:17 
 * 
 */
package com.osserp.core.events;

import java.util.Date;
import java.util.List;

import com.osserp.common.Entity;
import com.osserp.common.Note;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Event extends Entity {

    /**
     * Returns the recipient id
     * @return recipientId
     */
    Long getRecipientId();

    /**
     * Returns an optional reference wich may be a project, an order, etc. The type of this relation is determined by config type.
     * @return recipientId
     */
    Long getReferenceId();

    /**
     * Returns the action
     * @return EventAction action
     */
    EventAction getAction();

    /**
     * Provides the description of the event
     * @return description
     */
    String getDescription();

    /**
     * Provides a link
     * @return link
     */
    String getLink();

    /**
     * Sets the name of the action link to be performed from user to initiate terminating link
     * @param actionLink
     */
    void setLink(String link);

    /**
     * The name of the calling event
     * @return name
     */
    String getMessage();

    /**
     * The headline replacing action name if not empty
     * @return headline
     */
    String getHeadline();

    /**
     * The user who created this entity. This value is set at creation time
     * @return createdBy
     */
    Long getCreatedBy();

    /**
     * The creation-date of the entity. This value is set at creation time
     * @return created
     */
    Date getCreated();

    /**
     * The event expiration date
     * @return expires
     */
    Date getExpires();

    /**
     * Sets the expiration date
     * @return expires
     */
    void setExpires(Date expires);

    /**
     * Provides the date when the event is active
     * @return activation
     */
    Date getActivation();

    /**
     * Sets the activation date
     * @return activation
     */
    void setActivation(Date activation);

    /**
     * Provides the appointment date of the event if event is an appointment
     * @return appointmentDate
     */
    Date getAppointmentDate();

    /**
     * Sets the appointment date
     * @return appointmentDate
     */
    void setAppointmentDate(Date appointmentDate);
    
    /**
     * Provides the event date which is appointmentDate for appointments
     * and event expiration for other events (todos). 
     * @return eventDate
     */
    Date getEventDate();

    /**
     * Indicates that the event is an alert event
     * @return true if so, default is false
     */
    boolean isAlert();

    /**
     * Indicates that an alert has been sent
     * @return alertSent
     */
    boolean isAlertSent();

    /**
     * Sets that this event should be directed to a pool
     * @param alertSent
     */
    void setAlertSent(boolean alertSent);

    /**
     * Indicates that the event is closed
     * @return closed
     */
    boolean isClosed();

    /**
     * Sets that this event should be directed to a pool
     * @param closed
     */
    void setClosed(boolean closed);

    /**
     * Indicates when the event was closed
     * @return closedOn
     */
    Date getClosedOn();

    /**
     * Sets when the event was closed
     * @return closedOn
     */
    void setClosedOn(Date closedOn);

    /**
     * The user who closed the event.
     * @return closedBy
     */
    Long getClosedBy();

    /**
     * Sets the user who closes the event.
     * @param closedBy
     */
    void setClosedBy(Long closedBy);

    List<? extends Note> getNotes();

    void addNote(Long createdBy, String text);
}
