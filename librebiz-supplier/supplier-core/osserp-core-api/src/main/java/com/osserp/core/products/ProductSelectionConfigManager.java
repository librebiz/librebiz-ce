/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jul 3, 2009 9:18:11 AM 
 * 
 */
package com.osserp.core.products;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.core.employees.Employee;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.system.SystemCompany;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ProductSelectionConfigManager {

    /**
     * Creates a new product selection config
     * @param user
     * @param context
     * @param name
     * @param description
     * @return new created product selection config
     * @throws ClientException if name already exists
     * @throws IllegalArgumentException if user is null
     */
    ProductSelectionConfig create(
            Employee user,
            ProductSelectionContext context,
            String name,
            String description) throws ClientException;

    /**
     * Restricts usage of selection config to employees of a dedicated company and/or branch
     * @param user
     * @param config
     * @param company if selection could only be used in such a context
     * @param branch optional branch parameter if company based selection is limited to branch
     * @return product selection config
     * @throws ClientException if both company and branch missing
     */
    ProductSelectionConfig restrict(
            Employee user,
            ProductSelectionConfig config,
            SystemCompany company,
            BranchOffice branch) throws ClientException;

    /**
     * Changes the name of an product selection config
     * @param user
     * @param configId
     * @param name
     * @param disabled
     * @return product selection config
     * @throws ClientException
     */
    ProductSelectionConfig update(
            Employee user, 
            ProductSelectionConfig config, 
            ProductSelectionContext context, 
            String name, 
            String description,
            boolean disabled)
            throws ClientException;

    /**
     * Removes an item of an product selection config
     * @param user
     * @param config
     * @param id
     * @return product selection config
     * @throws ClientException
     */
    ProductSelectionConfig removeSelection(Employee user, ProductSelectionConfig config, Long id) throws ClientException;

    /**
     * Adds a selection to a config
     * @param user
     * @param config
     * @param type
     * @param group
     * @param category
     * @return product selection config
     */
    ProductSelectionConfig addSelection(Employee user, ProductSelectionConfig config, ProductType type, ProductGroup group, ProductCategory category)
            throws ClientException;

    /**
     * Adds a selection (single product) to a config
     * @param user
     * @param config
     * @param product
     * @return product selection config
     * @throws ClientException
     */
    ProductSelectionConfig addSelection(Employee user, ProductSelectionConfig config, Product product) throws ClientException;

    /**
     * Toggles the indication whether a config item is an exclusion or not
     * @param itemId
     * @throws ClientException
     */
    void toggleExclusion(Long itemId) throws ClientException;

    /**
     * Loads all product selection configs
     * @return list of product selection configs
     */
    List<ProductSelectionConfig> getSelectionConfigs();

    /**
     * Loads all product selection configs by context
     * @param context to lokkup for
     * @return list of product selection configs
     */
    List<ProductSelectionConfig> getSelectionConfigs(ProductSelectionContext context);

    /**
     * Provides an product selection config by primary key
     * @param id
     * @return obj by primary key
     */
    ProductSelectionConfig getSelectionConfig(Long id);

    /**
     * Provides a context by id
     * @param id
     * @return context
     */
    ProductSelectionContext getContext(Long id);

    /**
     * Loads all product selection contexts
     * @return list of product selection contexts
     */
    List<ProductSelectionContext> getSelectionContexts();
}
