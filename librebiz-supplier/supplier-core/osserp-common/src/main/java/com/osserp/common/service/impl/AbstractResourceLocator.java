/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 13, 2008 5:43:06 PM 
 * 
 */
package com.osserp.common.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.Constants;
import com.osserp.common.service.ResourceLocator;
import com.osserp.common.service.SerializableResourceBundle;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractResourceLocator implements ResourceLocator {
    private static Logger log = LoggerFactory.getLogger(AbstractResourceLocator.class.getName());
    
    protected static final String DEFAULT_APP_RES_PATH = 
            "/com/osserp/common/resources/application,"
            + "/com/osserp/common/resources/application-custom";
    
    private HashMap<String, ResourceBundle> resourceBundles = new HashMap<String, ResourceBundle>();
    private Locale defaultLocale = null;
    private String localePath = null;

    protected AbstractResourceLocator() {
        super();
        setLocalePath(DEFAULT_APP_RES_PATH);
        defaultLocale = new Locale(Constants.DEFAULT_LANGUAGE);
    }

    protected AbstractResourceLocator(String localePath) {
        super();
        setLocalePath(localePath);
        defaultLocale = new Locale(Constants.DEFAULT_LANGUAGE);
    }

    public Locale getDefaultLocale() {
        return defaultLocale;
    }

    public void setDefaultLocale(Locale defaultLocale) {
        this.defaultLocale = defaultLocale;
    }

    public String getMessage(String code) {
        return getMessage(code, null);
    }

    public String getMessage(String code, Locale locale) {
        if (code == null) {
            return null;
        }
        if (locale == null) {
            locale = defaultLocale;
        }
        try {
            return createResourceBundle(locale).getString(code);
        } catch (MissingResourceException e) {
            return code;
        } catch (Exception e) {
            log.warn("getMessage() failed [code=" + code + ", message=" + e.getMessage() + "]");
            return code;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.common.service.ResourceLocator#getResourceBundle(java.util.Locale)
     */
    public ResourceBundle getResourceBundle(Locale locale) {
        return new SerializableResourceBundle(getResourceBundleMap(locale));
    }

    public final Map<String, Object> getResourceBundleMap(Locale locale) {
        Map<String, Object> map = new HashMap<String, Object>();
        ResourceBundle bundle = createResourceBundle(locale);
        Enumeration<String> i = bundle.getKeys();
        while (i.hasMoreElements()) {
            String key = i.nextElement();
            map.put(key, bundle.getString(key));
        }
        return map;
    }

    private ResourceBundle createResourceBundle(Locale locale) {
        locale = (locale == null ? defaultLocale : locale);
        if (!resourceBundles.containsKey(locale.toString())) {
            try {
                ResourceBundle bundle = new PropertyResourceBundle(getResourceStream(locale));
                resourceBundles.put(locale.toString(), bundle);
            } catch (IOException e) {
            }
        }
        return resourceBundles.get(locale.toString());
    }

    public void reload(Locale locale) {
        locale = (locale == null ? defaultLocale : locale);
        try {
            ResourceBundle bundle = new PropertyResourceBundle(getResourceStream(locale));
            resourceBundles.put(locale.toString(), bundle);
        } catch (IOException e) {
            log.warn("reload() ignoring exception [message=" + e.getMessage() 
                + ", class=" + e.getClass().getName() + "]");
        }
    }

    abstract protected InputStream getResourceStream(Locale locale);

    public void setLocalePath(String localePath) {
        this.localePath = localePath;
    }

    public String getLocalePath() {
        return localePath;
    }

}
