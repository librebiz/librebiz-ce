/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 28, 2009 10:10:06 AM 
 * 
 */
package com.osserp.core.purchasing;

import java.util.List;

import com.osserp.common.ClientException;

import com.osserp.core.employees.Employee;
import com.osserp.core.finance.InvoiceManager;
import com.osserp.core.finance.Record;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface PurchaseInvoiceAwareManager extends InvoiceManager {

    /**
     * Cancels a closed purchase invoice
     * @param record
     * @param user
     * @param note
     * @throws ClientException if operation not supported by implementing record
     */
    void cancel(Record record, Employee user, String note) throws ClientException;

    /**
     * Creates a creditnote for an invoice. Purchase creditNotes are provided by
     * invoice records with negative quantities
     * @param user
     * @param invoice
     * @param customHeader
     * @return new created creditNote
     */
    PurchaseInvoice createCreditNote(Employee user, PurchaseInvoice invoice, String customHeader);

    /**
     * Provides the ids of creditNotes associated with a purchaseInvoice
     * @param invoice
     * @return list of associated creditNote ids or empty list if none found
     */
    List<Long> getCreditNoteIds(PurchaseInvoice invoice);

    /**
     * Provides available invoice types
     * @return invoiceTypes
     */
    List<PurchaseInvoiceType> getInvoiceTypes();

    /**
     * Provides invoice type by id
     * @param id
     * @return invoiceType
     */
    PurchaseInvoiceType getInvoiceType(Long id);

    /**
     * Update summary to override values with values provided by supplier
     * @param invoice
     * @param taxAmount
     * @param reducedTaxAmount
     * @param grossAmount
     * @throws ClientException if provided values exceeds allowed range
     */
    void updateSummary(PurchaseInvoice invoice, String taxAmount, String reducedTaxAmount, String grossAmount) throws ClientException;

}
