<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>

<c:set var="view" value="${sessionScope.recordView}"/>
<c:set var="record" value="${view.record}"/>
<c:if test="${!empty record.options}">
<div class="recordItems">
<table class="table table-striped">
	<tr>
		<th id="number"><fmt:message key="product"/></th>
		<th id="description"><fmt:message key="option"/></th>
		<th id="quantity" class="amount"><fmt:message key="quantity"/></th>
		<th id="quantity_unit" class="amount"><fmt:message key="quantityUnitShort"/></th>
		<th id="price" class="amount"><fmt:message key="price"/></th>
		<th id="total" class="amount"><fmt:message key="total"/></th>
	</tr>
	<c:forEach var="item" items="${record.options}" varStatus="s">
    <tr>
		<td>
			<o:popupSelect action="/productPopup.do" name="item" property="product.productId" scope="page" 
				header="product" dimension="${popupPageSize}" title="displayProductData">
				<o:out value="${item.product.productId}"/>
			</o:popupSelect>
		</td>
		<td><o:out value="${item.productName}"/></td>
		<td class="amount"><o:number value="${item.quantity}" format="decimal"/></td>
		<td class="amount"><oc:options name="quantityUnits" value="${item.product.quantityUnit}"/></td>
		<td class="amount"><o:number value="${item.price}" format="currency"/></td>
		<td class="amount"><o:number value="${item.amount}" format="currency"/></td>
	</tr>
	</c:forEach>
</table>
</div>
</c:if>
