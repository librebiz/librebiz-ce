/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 *
 * Created on 17.05.2013
 * 
 */
package com.osserp.gui.accounting

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.Constants
import com.osserp.common.util.DateUtil

import com.osserp.core.BankAccount
import com.osserp.core.contacts.ClassifiedContact

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class AbstractAccountingView extends AbstractBankAccountReferenceView {
    private static Logger logger = LoggerFactory.getLogger(AbstractAccountingView.class.getName())

    static final String TRANSACTION_CREATE_PERMISSIONS = 
        'accounting_documents,accounting_bank_documents,accounting_accountant_deny,executive_accounting,executive'
        
    Integer year
    
    List<ClassifiedContact> contacts = []
    ClassifiedContact selectedContact
    
    Long selectedCurrency = Constants.CURRENCY_EUR
    
    private Long _filterByContact
    
    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (forwardRequest) { 
            year = getInteger('year')
        }
    }
    
    void selectContact() {
        selectSelectedContact(form.getLong('id'))
    }
    
    protected void selectSelectedContact(Long id) {
        if (!id) {
            selectedContact = null
            logger.debug("selectSelectedContact: reset done")
        } else {
            selectedContact = contacts.find { id == it.id }
            logger.debug("selectSelectedContact: done [account=${selectedContact?.id}]")
        }
    }
    
    void filterByContact() {
        updateContactFilter(form.getLong('id'))
        reload()
    }
    
    protected void updateContactFilter(Long id) {
        if (!id) {
            _filterByContact = null
        } else {
            _filterByContact = id
        }
    }
    
    boolean isFilterByContactEnabled() {
        _filterByContact != null
    }
    
    protected boolean filterByContactMatch(Long id) {
        _filterByContact == id
    }
}
