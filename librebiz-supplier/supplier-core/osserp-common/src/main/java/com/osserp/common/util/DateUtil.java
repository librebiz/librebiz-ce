/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 6, 2004 
 * 
 */
package com.osserp.common.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import org.jdom2.Element;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.CalendarMonth;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.PublicHoliday;
import com.osserp.common.Year;
import com.osserp.common.beans.CalendarImpl;
import com.osserp.common.beans.CalendarMonthImpl;
import com.osserp.common.beans.YearImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class DateUtil {
    private static Logger log = LoggerFactory.getLogger(DateUtil.class.getName());

    public static final DateFormat GERMAN =
            new SimpleDateFormat("dd.MM.yyyy", Locale.GERMANY);

    public static final int LAST_MONTH = 11;
    private static final int MAX_INPUT_YEARS = 100;
    public static final long SECOND = 1000;
    public static final long MINUTE = 60 * SECOND; // 60.000
    public static final long HOUR = MINUTE * 60; // 3.600.000
    public static final long DAY = HOUR * 24; // 86.400.000

    private static DateFormat df = null;
    private static String centuryPrefix = null;

    static {
        df = GERMAN;
        df.setLenient(false);
        int i = Calendar.getInstance().get(Calendar.YEAR);
        centuryPrefix = String.valueOf(i).substring(0, 2);
    }

    /**
     * Provides current date and time
     * @return current date
     */
    public static Date getCurrentDate() {
        return new Date(System.currentTimeMillis());
    }

    /**
     * Provides current day of month [1..(28|29|30|31)]
     * @return current day
     */
    public static int getCurrentDay() {
        return getDay(getCurrentDate());
    }

    /**
     * Provides current month of year [0..11]
     * @return current month
     */
    public static int getCurrentMonth() {
        return getMonth(getCurrentDate());
    }

    /**
     * Provides current year
     * @return current year
     */
    public static int getCurrentYear() {
        return getYear(getCurrentDate());
    }

    /**
     * Provides the first day of the year at 00:00:00
     * @param year
     * @return first day of year at midnight
     */
    public static Date getFirstDayOfYear(int year) {
        return new CalendarImpl(1, 0, year).getDate();
    }
    
    /**
     * Creates the last day of the year at 23:59:59.
     * @param year
     * @return last day of the year just before midnight
     */
    public static Date getLastDayOfYear(int year) {
        return DateUtil.subtractMillis(getFirstDayOfYear(year+1), 1);
    }

    /**
     * Tests string for valid date
     * @param checkthis
     * @return true if string represents a valid date value
     */
    public static boolean isDate(String checkthis) {
        if (checkthis == null) {
            return false;
        }
        try {
            df.parse(checkthis);
            return true;
        } catch (ParseException pe) {
            return false;
        }
    }

    /**
     * Tries to create a date from given string. Allowed formats are as follows: dd.mm.yy[yy], dd/mm/yy[yy], dd,mm,yy[yy] and dd-mm-yy[yy]. If year is missing,
     * e.g. dd.mm or dd/mm, current year will be added
     * @param dateString
     * @param required if date must not be null or empty string
     * @return date
     * @throws ClientException if invalid format found or value missing if required is true
     */
    public static Date createDate(String dateString, boolean required) throws ClientException {
        String date = dateString;
        int hours = 0;
        int minutes = 0;
        Date result = null;
        int length = (date == null) ? 0 : date.length();
        if (length < 1) {
            if (required) {
                throw new ClientException(ErrorCode.DATE_MISSING);
            }
        } else if (length >= 16 && length <= 19 && (date != null && date.indexOf(" ") == 10)) {
            // 16 = 22.02.2012 14:27
            // 19 = 22.02.2012 14:27:25
            date = dateString.substring(0, 10);
            length = date.length();
            String time = dateString.substring(11, 16);
            hours = NumberUtil.createInteger(time.substring(0, 2));
            minutes = NumberUtil.createInteger(time.substring(3, 5));
        }
        if (length == 5 || length == 8 || length == 10) {
            //  5 = 02.10
            //  8 = 02.10.12
            // 10 = 02.10.2012
            StringBuffer dt = new StringBuffer(date);
            if (dt.indexOf("/") >= 0) {
                StringUtil.replace(dt, "/", ".");
            }
            if (dt.indexOf(",") >= 0) {
                StringUtil.replace(dt, ",", ".");
            }
            if (dt.indexOf("-") >= 0) {
                StringUtil.replace(dt, "-", ".");
            }
            if (dt.length() == 5) {
                dt.append(".").append(getYear(getCurrentDate()));
            }
            if (dt.length() == 10) {
                result = getDate(dt.toString());
            } else {
                StringBuffer b = new StringBuffer(dt);
                b.insert(6, centuryPrefix);
                result = getDate(b.toString());
            }
            if (result == null) {
                log.warn("createDate() date == null");
                log.warn("createDate() invalid date [" + date + "]");
                throw new ClientException(ErrorCode.DATE_FORMAT);
            }
        }
        if (result != null) {
            if (!validateDate(result)) {
                log.warn("createDate() validateDate(result) == false");
                log.warn("createDate() invalid date [" + date + "]");
                throw new ClientException(ErrorCode.DATE_FORMAT);
            }
        }
        if (hours > 0 || minutes > 0) {
            result = createDate(result, hours, minutes);
        }
        return result;
    }

    /**
     * Reports a date as valid if not longer than MAX_INPUT_YEARS ago and not more than MAX_INPUT_YEARS in the future
     * @param date
     * @return true if date matches as described above
     */
    public static boolean validateDate(Date date) {
        if (date == null) {
            return false;
        }
        Date currentYear = new Date(System.currentTimeMillis());
        return (date.before(addYears(currentYear, MAX_INPUT_YEARS))
        && date.after(subtractYears(currentYear, MAX_INPUT_YEARS)));
    }

    /**
     * Creates a new date by given date with time set to given time
     * @param dateString
     * @param required
     * @param hours
     * @param minutes
     * @return date and time
     */
    public static Date createDate(String dateString, boolean required, int hours, int minutes) throws ClientException {
        Date date = createDate(dateString, required);
        return createDate(date, hours, minutes);
    }

    /**
     * Creates a new date by given date with time set to given time
     * @param date
     * @param hours
     * @param minutes
     * @return date and time
     */
    public static Date createDate(Date date, int hours, int minutes) {
        if (date != null) {
            return new Date(resetTime(date).getTime() + hours * HOUR + minutes * MINUTE);
        }
        return date;
    }

    /**
     * Returns a new instance of a given date with reset time, e.g. 00:00
     * @param date
     * @return date with reset time
     */
    public static Date resetTime(Date date) {
        if (date != null) {
            Integer[] time = DateUtil.getTime(date);
            time[3] = time[4] = time[5] = time[6] = 0;
            return DateUtil.getDate(time);
        }
        return date;
    }

    /**
     * Converts a given string value to a date.
     * @param dateString
     * @return converted date or null if dateString is null or has an invalid format
     */
    public static Date getDate(String dateString) {
        if (dateString == null || dateString.length() == 0) {
            if (log.isDebugEnabled()) {
                log.debug("getDate() dateString == null || dateString.length() == 0");
            }
            return null;
        }
        try {
            return df.parse(dateString);
        } catch (Throwable t) {
            if (log.isDebugEnabled()) {
                log.debug("getDate() error while parsing date string [value=" + dateString + "]");
            }
            return null;
        }
    }

    public static Date parseIso(String dateString) {
        if (dateString != null) {
            try {
                LocalDate date = LocalDate.parse(dateString, DateTimeFormatter.ISO_DATE_TIME);
                return Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
            } catch (Throwable t) {
                if (log.isDebugEnabled()) {
                    log.debug("parseIso failed parsing date string [value=" + dateString + "]");
                }
            }
        }
        return null;
    }

    public static LocalDateTime parseIsoDateTime(String dateString) {
        if (dateString != null) {
            try {
                return LocalDateTime.parse(dateString, DateTimeFormatter.ISO_DATE_TIME);
            } catch (Throwable t) {
                if (log.isDebugEnabled()) {
                    log.debug("parseIsoDateTime failed parsing date string [value=" + dateString + "]");
                }
            }
        }
        return null;
    }

    public static LocalDateTime getLocalDateTime(Date date) {
        return date == null ? null :
            date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    public static Date getDate(LocalDateTime date) {
        return date == null ? null : Timestamp.valueOf(date);
    }

    public static long getDaysByMillis(Long ms) {
        return (ms == null || ms == 0) ? 0 : (ms / DAY);
    }

    public static long getMillisByDays(Long days) {
        return (days == null || days == 0) ? 0 : (days * DAY);
    }

    public static Date addDay(Date date) {
        return new Date(date.getTime() + DAY);
    }

    public static Date addDays(Date date, int days) {
        return new Date(date.getTime() + days * DAY);
    }

    public static Date addHours(Date date, int hours) {
        return new Date(date.getTime() + hours * HOUR);
    }

    public static Date addMillis(Date date, long millis) {
        return new Date(date.getTime() + millis);
    }

    public static Date addMinutes(Date date, int minutes) {
        return new Date(date.getTime() + minutes * MINUTE);
    }

    public static Date addYears(Date date, int years) {
        return new Date(date.getTime() + (365 * DAY * years));
    }

    public static Date subtractDays(Date date, int days) {
        return new Date(date.getTime() - days * DAY);
    }

    public static Date subtractMinutes(Date date, int minutes) {
        return new Date(date.getTime() - minutes * MINUTE);
    }

    public static Date subtractMillis(Date date, long millis) {
        return new Date(date.getTime() - millis);
    }

    public static Date subtractMonths(Date date, int months) {
        return new Date(date.getTime() - (months * 30 * DAY));
    }

    public static Date subtractYears(Date date, int years) {
        return new Date(date.getTime() - (365 * DAY * years));
    }
    
    public static Double getDurationHours(Date start, Date end) {
        assert (start != null && end != null);
        BigDecimal time = new BigDecimal(Long.valueOf(end.getTime() - start.getTime()).toString());
        try {
            return NumberUtil.roundDue(time.divide(new BigDecimal(HOUR)).doubleValue());
        } catch (Exception e) {
            try {
                return NumberUtil.roundDue(time.divide(new BigDecimal(HOUR), RoundingMode.HALF_UP).doubleValue());
            } catch (Exception e2) {
                try {
                    Double result = time.doubleValue();
                    result = (result / HOUR);
                    return result;
                } catch (Exception e3) {
                    // giving up
                    return null;
                }
            }
        }
    }

    /* extracting a single value (e.g. day, month, ...) as an int from a java.util.Date */

    public static int getDay(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        return cal.get(Calendar.DAY_OF_MONTH);
    }

    /**
     * Provides the month of the year [0..11]
     * @param date
     * @return month
     */
    public static int getMonth(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        return cal.get(Calendar.MONTH);
    }

    public static int getYear(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        return cal.get(Calendar.YEAR);
    }

    /* calculated values corresponding to a java.util.Date */

    /*
     * 1=sun ... 7=sat
     */
    public static int getDayOfWeek(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        int result = cal.get(Calendar.DAY_OF_WEEK);
        return result;
    }

    public static boolean isWeekend(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            return true;
        }
        return false;
    }

    public static int getFirstDayOfWeek(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        return cal.getFirstDayOfWeek();
    }

    public static int getWeekOfYear(Date date) {
        return getWeekOfYear(date, Locale.GERMANY);
    }

    public static int getWeekOfYear(Date date, Locale locale) {
        Calendar cal = new GregorianCalendar(TimeZone.getDefault(), locale);
        cal.setTime(date);
        return cal.get(Calendar.WEEK_OF_YEAR);
    }

    /* maximum counts */

    public static int getDaysOfMonthCount(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        return cal.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    public static int getDaysOfYearCount(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        return cal.getActualMaximum(Calendar.DAY_OF_YEAR);
    }

    public static int getWeeksOfYearCount(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        return cal.getActualMaximum(Calendar.WEEK_OF_YEAR);
    }

    /**
     * Provides all date values as int array
     * array[0] = Calendar.DAY_OF_MONTH <br>
     * array[1] = Calendar.MONTH <br>
     * array[2] = Calendar.YEAR <br>
     * array[3] = Calendar.HOUR_OF_DAY <br>
     * array[4] = Calendar.MINUTE <br>
     * array[5] = Calendar.SECOND <br>
     * array[6] = Calendar.MILLISECOND
     * @param date
     * @return array[7]
     */
    public static Integer[] getTime(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        Integer[] result = new Integer[7];
        result[0] = cal.get(Calendar.DAY_OF_MONTH);
        result[1] = cal.get(Calendar.MONTH);
        result[2] = cal.get(Calendar.YEAR);
        result[3] = cal.get(Calendar.HOUR_OF_DAY);
        result[4] = cal.get(Calendar.MINUTE);
        result[5] = cal.get(Calendar.SECOND);
        result[6] = cal.get(Calendar.MILLISECOND);
        return result;
    }
    
    public static Date getDate(Integer[] datetime) {
        Calendar cal = new GregorianCalendar();
        if (datetime != null) {
            cal.set(Calendar.DAY_OF_MONTH, datetime[0]);
            cal.set(Calendar.MONTH, datetime[1]);
            cal.set(Calendar.YEAR, datetime[2]);
            cal.set(Calendar.HOUR_OF_DAY, datetime[3]);
            cal.set(Calendar.MINUTE, datetime[4]);
            cal.set(Calendar.SECOND, datetime[5]);
            cal.set(Calendar.MILLISECOND, datetime[6]);
        }
        return cal.getTime();
    }
    
    public static Integer getTimeHours(Date date) {
        return fetchTimePart(date, 3);
    }
    
    public static Integer getTimeMinutes(Date date) {
        return fetchTimePart(date, 4);
    }
    
    private static Integer fetchTimePart(Date date, int column) {
        if (date != null) {
            Integer[] time = getTime(date);
            return time[column];
        }
        return null;
    }

    public static String getTime(Integer[] fields) {
        if (fields == null
                || fields.length < 7
                || fields[3] == null
                || fields[4] == null) {
            return "";
        }
        StringBuilder buffer = new StringBuilder();
        if (fields[3] < 10) {
            buffer.append("0");
        }
        buffer.append(fields[3]).append(":");
        if (fields[4] < 10) {
            buffer.append("0");
        }
        buffer.append(fields[4]);
        return buffer.toString();
    }

    public static long getMinutes(Long durationInMillis) {
        if (durationInMillis == null || durationInMillis == 0) {
            return 0;
        }
        return durationInMillis / DateUtil.MINUTE;
    }

    public static int[] getHoursAndMinutes(int minutes) {
        int minutesTotal = (minutes < 0 ? (minutes * -1) : minutes);
        int hours = (minutesTotal < 60 ? 0 : (minutesTotal / 60));
        int mins = (minutesTotal < 60 ? minutesTotal : (minutesTotal % 60));
        return new int[] { hours, mins };
    }

    public static int[] getHoursAndMinutes(double decimalHours) {
        int minutesTotal = Double.valueOf(decimalHours * 60).intValue();
        return DateUtil.getHoursAndMinutes(minutesTotal);
    }
    
    public static boolean isFuture(Date d) {
        return (d != null && getCurrentDate().before(d));
    }

    public static boolean isSameDay(Date a, Date b) {
        if (a == null && b == null) {
            return true;
        }
        if (a == null || b == null) {
            return false;
        }

        if ((getDay(a) == getDay(b))
                && (getMonth(a) == getMonth(b))
                && (getYear(a) == getYear(b))) {
            return true;
        }
        return false;
    }

    public static boolean isSameMonth(Date a, Date b) {
        int yearA = getYear(a);
        int yearB = getYear(b);
        if (yearA != yearB) {
            return false;
        }
        int monthA = getMonth(a);
        int monthB = getMonth(b);
        return monthA == monthB;
    }

    public static boolean isToday(Date date) {
        return isSameDay(getCurrentDate(), date);
    }

    /**
     * Indicates that day a is later than day b
     * @param a
     * @param b
     * @return true if day of date a is later than day of date b
     */
    public static boolean isLaterDay(Date a, Date b) {
        if (a == null && b == null) {
            return false;
        }
        if (a == null) {
            return true;
        }
        if (b == null) {
            return false;
        }
        if (isToday(a) && isToday(b)) {
            return false;
        }
        Integer[] timeA = getTime(a);
        Integer[] timeB = getTime(b);
        // test year
        if (timeA[2] > timeB[2]) {
            return true;
        }
        if (timeB[2] > timeA[2]) {
            return false;
        }
        // test month
        if (timeA[1] > timeB[1]) {
            return true;
        }
        if (timeB[1] > timeA[1]) {
            return false;
        }
        // test day
        if (timeA[0] > timeB[0]) {
            return true;
        }
        return false;
    }

    /**
     * Indicates that day test is in interval between begin and end (inculdes begin and end)
     * @param test
     * @param begin
     * @param end
     * @return true if day of date test is in interval between begin and end (inculdes begin and end)
     */
    public static boolean isDayInInterval(Date test, Date begin, Date end) {
        if (test == null && begin == null && end == null) {
            return false;
        }
        if (test == null) {
            return false;
        }
        if (begin == null && end == null) {
            return true;
        }
        if (begin == null) {
            if (!isLaterDay(test, end)) {
                return true;
            }
            return false;
        }
        if (end == null) {
            if (isLaterDay(test, begin) || isSameDay(test, begin)) {
                return true;
            }
            return false;
        }
        if (!isLaterDay(test, end) && (isLaterDay(test, begin) || isSameDay(test, begin))) {
            return true;
        }
        return false;
    }

    public static List<Date> createCalendarList(Date startDateInWeek, int weeksCount) {
        List<Date> diagram = new ArrayList<Date>();
        Date startDate = createStartDate(startDateInWeek);
        diagram.add(startDate);
        int daysCount = 7 * weeksCount;
        for (int i = 1, j = daysCount; i < j; i++) {
            diagram.add(DateUtil.addDays(startDate, i));
        }
        return diagram;
    }

    private static Date createStartDate(Date currentDate) {
        int currentDay = DateUtil.getDayOfWeek(currentDate);
        Date startDate = currentDate;
        if (currentDay == 1) {
            startDate = DateUtil.addDays(currentDate, 1);
        } else if (currentDay > 2) {
            startDate = DateUtil.subtractDays(currentDate, currentDay - 2);
        }
        return startDate;
    }

    public static void addDateAndTime(Element item, Date date) {
        if (date == null) {
            item.addContent(new Element("day"));
            item.addContent(new Element("month"));
            item.addContent(new Element("year"));
            item.addContent(new Element("hour"));
            item.addContent(new Element("min"));
            item.addContent(new Element("time"));

        } else {
            Integer[] time = DateUtil.getTime(date);
            item.addContent(new Element("day").setText(time[0].toString()));
            item.addContent(new Element("month").setText(time[1].toString()));
            item.addContent(new Element("year").setText(time[2].toString()));
            item.addContent(new Element("hour").setText(time[3].toString()));
            item.addContent(new Element("min").setText(time[4].toString()));
            item.addContent(new Element("time").setText(DateUtil.getTime(time)));
        }
    }

    public static String createMonth(int monthValue) {
        StringBuilder buf = new StringBuilder();
        if (monthValue < 9) {
            buf.append("0");
        }
        buf.append(monthValue + 1);
        return buf.toString();

    }

    /**
     * Provides a given date value in short date format, e.g. an integer of passed seconds since 01.01.1970 instead of default millisecond representation in
     * java.
     * @param bydate
     * @return short date format of given date or current if given date is null
     */
    public static int getShortFormat(Date bydate) {
        Date date = (bydate == null) ? new Date(System.currentTimeMillis()) : bydate;
        return Integer.parseInt(Long.valueOf(date.getTime()).toString().substring(0, 10));
    }

    /**
     * Creates a date object by short format
     * @param dateInShortFormat
     * @return date
     */
    public static Date getByShortFormat(int dateInShortFormat) {
        long date = Long.parseLong(Integer.valueOf(dateInShortFormat).toString() + "000");
        return new Date(date);
    }

    /**
     * Creates the last day of month
     * @param month (0-11)
     * @param year
     * @return last day of month in year
     */
    public static Date createLastDayOfMonth(int month, int year) {
        CalendarMonth m = DateUtil.createMonth(month, year);
        return new CalendarImpl(m.getDaysOfMonthCount(), month, year).getDate();
    }

    /**
     * Creates a new calendar month with current month values
     * @return current month
     */
    public static CalendarMonth createMonth() {
        Integer[] date = DateUtil.getTime(DateUtil.getCurrentDate());
        return DateUtil.createMonth(date[1], date[2]);
    }

    /**
     * Creates a new calendar month
     * @param month
     * @param year
     * @return month
     */
    public static CalendarMonth createMonth(int month, int year) {
        return new CalendarMonthImpl(month, year);
    }

    /**
     * Creates a new calendar month by date or current if date is null
     * @param date
     * @return calendar month of given date or current date if date is null
     */
    public static CalendarMonth createMonth(Date date) {
        Integer[] time = DateUtil.getTime(date == null ? DateUtil.getCurrentDate() : date);
        return new CalendarMonthImpl(time[1], time[2]);
    }

    /**
     * Creates a month interval
     * @param from
     * @param til
     * @return month list
     */
    public static List<CalendarMonth> createMonths(Date from, Date til) {
        List<CalendarMonth> result = new ArrayList<CalendarMonth>();
        Integer[] start = DateUtil.getTime(from == null ? DateUtil.getCurrentDate() : from);
        Integer[] end = DateUtil.getTime(til == null ? DateUtil.getCurrentDate() : til);
        int startMonth = start[1];
        int startYear = start[2];
        result.addAll(DateUtil.createMonths(startMonth, startYear));
        int endYear = end[2];
        while (startYear < endYear) {
            result.addAll(DateUtil.createMonths(0, ++startYear));
        }
        return result;
    }

    /**
     * Creates months interval for a year
     * @param from
     * @param year
     * @return month list
     */
    public static List<CalendarMonth> createMonths(int from, int year) {
        List<CalendarMonth> result = new ArrayList<CalendarMonth>();
        for (int i = from; i < 12; i++) {
            CalendarMonth month = new CalendarMonthImpl(i, year);
            result.add(month);
        }
        return result;
    }

    /**
     * Creates a list of years
     * @param first year
     * @param last year
     * @return years
     */
    public static List<Year> createYears(Date first, Date last) {
        return DateUtil.createYears(DateUtil.getYear(first), DateUtil.getYear(last));
    }

    /**
     * Creates a list of years
     * @param first year
     * @param last year
     * @return years
     */
    public static List<Year> createYears(int first, int last) {
        List<Year> result = new ArrayList<Year>();
        for (int i = first; i <= last; i++) {
            result.add(new YearImpl(i));
        }
        return result;
    }

    /**
     * Creates a list of years
     * @param startYear
     * @return years from startYear to currentYear
     */
    public static List<Year> createYearsTilNow(int startYear) {
        return createYears(startYear, DateUtil.getCurrentYear());
    }

    /**
     * Check if a date in the list of public holiday
     * @param publicHolidays
     * @param testDate
     * @return true if date a public holiday
     */
    public static boolean isPublicHoliday(List<PublicHoliday> publicHolidays, Date testDate) {
        for (int i = 0, j = publicHolidays.size(); i < j; i++) {
            if (isSameDay(publicHolidays.get(i).getDate(), testDate)) {
                return true;
            }

        }
        return false;
    }

    /**
     * Provides the start of an interval
     * @param intervalPattern
     * @return array interval type and value
     */
    public static String[] getTimeIntervalConfig(String intervalKey) {
        String[] result = new String[] { "Y", "1" };
        if (intervalKey != null) {
            StringBuffer buffer = new StringBuffer(intervalKey.length()-1);
            char c;
            for (int i = 0; i < intervalKey.length(); i++) {
                c = intervalKey.charAt(i);
                if (i == 0) { 
                    if (c != 'Y' && c != 'M' && c != 'D') {
                        result[0] = "Y";
                        result[1] = "1";
                        return result;
                    }
                    result[0] = Character.valueOf(c).toString();
                } else {
                    buffer.append(c);
                }
            }
            Integer number = NumberUtil.createInteger(buffer.toString());
            result[1] = number == null ? "1" : number.toString();
        }
        return result;
    }
}
