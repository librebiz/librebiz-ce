/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 4, 2010 2:10:02 PM 
 * 
 */
package com.osserp.core.dao.telephone.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.dao.AbstractHibernateAndSpringDao;
import com.osserp.common.dao.Tables;

import com.osserp.core.dao.telephone.SfaSubscriberToTelephoneGroups;
import com.osserp.core.model.telephone.SfaSubscriberToTelephoneGroupImpl;
import com.osserp.core.telephone.SfaSubscriberToTelephoneGroup;
import com.osserp.core.telephone.SfaTelephoneGroup;

/**
 * 
 * @author so <so@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public class SfaSubscriberToTelephoneGroupsDao extends AbstractHibernateAndSpringDao implements SfaSubscriberToTelephoneGroups {
    private static Logger log = LoggerFactory.getLogger(SfaSubscriberToTelephoneGroupsDao.class.getName());

    public SfaSubscriberToTelephoneGroupsDao() {
        super();
    }

    public SfaSubscriberToTelephoneGroupsDao(JdbcTemplate jdbcTemplate, Tables tables, SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
    }

    public void synchronize(SfaTelephoneGroup sfaTelephoneGroup, String uid) {
        if (log.isDebugEnabled()) {
            log.debug("synchronize() invoked for SfaSubscriberToTelephoneGroup [uid=" + uid + ", sfaTelephoneGroup=" + sfaTelephoneGroup + "]");
        }

        //set all subscriber to group relations with given uid to unused
        List<SfaSubscriberToTelephoneGroup> list = fetchSfaSubscriberToTelephoneGroups(uid);
        for (Iterator<SfaSubscriberToTelephoneGroup> iterator = list.iterator(); iterator.hasNext();) {
            SfaSubscriberToTelephoneGroup next = iterator.next();
            next.setUnused(true);
            next.setSynced(false);
            getCurrentSession().saveOrUpdate(SfaSubscriberToTelephoneGroupImpl.class.getName(), next);
        }

        //when telephoneGroup not null, then sync subscriber to new group
        if (sfaTelephoneGroup != null) {
            SfaSubscriberToTelephoneGroup next = fetchSfaSubscriberToTelephoneGroup(uid, sfaTelephoneGroup.getId());
            if (next != null && !sfaTelephoneGroup.isUnused()) {
                next.setSynced(false);
                next.setUnused(false);
            } else {
                next = new SfaSubscriberToTelephoneGroupImpl(uid, sfaTelephoneGroup.getTelephoneSystemId(), sfaTelephoneGroup);
            }
            if (log.isDebugEnabled()) {
                log.debug("synchronize() [sfaSubscriberToTelephoneGroup=" + next + "]");
            }
            getCurrentSession().saveOrUpdate(SfaSubscriberToTelephoneGroupImpl.class.getName(), next);
        }
    }

    public SfaSubscriberToTelephoneGroup fetchSfaSubscriberToTelephoneGroup(String uid, Long sfaTelephoneGroupId) {
        List<SfaSubscriberToTelephoneGroup> list = getCurrentSession().createQuery(
                "from " + SfaSubscriberToTelephoneGroupImpl.class.getName() 
                + " s where s.subscriberUid = :uid AND s.sfaTelephoneGroup.id = :groupid")
                .setParameter("uid", uid)
                .setParameter("groupid", sfaTelephoneGroupId)
                .list();
        return (list.isEmpty()) ? null : list.get(0);
    }

    public List<SfaSubscriberToTelephoneGroup> fetchSfaSubscriberToTelephoneGroups(String uid) {
        List<SfaSubscriberToTelephoneGroup> list = new ArrayList<SfaSubscriberToTelephoneGroup>();
        if (uid != null) {
            list = getCurrentSession().createQuery(
                    "from " + SfaSubscriberToTelephoneGroupImpl.class.getName() 
                    + " s where s.subscriberUid = :uid").setParameter("uid", uid ).list();
        }
        return list;
    }
}
