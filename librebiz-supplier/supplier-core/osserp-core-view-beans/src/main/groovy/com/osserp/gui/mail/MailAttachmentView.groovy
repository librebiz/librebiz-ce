/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 24, 2013 2:46:24 PM 
 * 
 */
package com.osserp.gui.mail

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.FileObject
import com.osserp.common.mail.Attachment
import com.osserp.common.mail.AttachmentImpl
import com.osserp.common.util.FileUtil
import com.osserp.groovy.web.ViewContextException
import com.osserp.gui.CoreView


/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class MailAttachmentView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(MailAttachmentView.class.getName())

    MailEditorView mailEditorView
    List<Attachment> collectedFiles = []

    MailAttachmentView() {
        dependencies = ['mailEditorView']
        //providesEditMode()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            mailEditorView = env['mailEditorView']
            if (!mailEditorView?.mail) {
                throw new ViewContextException('missing.mailEditorView.mail')
            }
            if (mailEditorView?.mailMessageContext?.viewHeader) {
                headerName = mailEditorView.mailMessageContext.viewHeader
            }
            logger.debug("initRequest: done [viewAvailable=${(headerName != null)}]")
        }
    }

    @Override
    void save() {
        FileObject upload = form.getUpload()
        if (!upload) {
            throw new ClientException(ErrorCode.FILE_MISSING)
        }
        if (upload.fileName && !alreadyAdded(upload.fileName)) {
            String storedFile = FileUtil.generateRandomFileName(attachmentDir, upload.fileName)
            FileUtil.persist(upload.fileData, storedFile)
            AttachmentImpl attachment = new AttachmentImpl(
                    upload.fileName,
                    storedFile,
                    upload.fileType)
            mailEditorView.mail.addAttachment(attachment)
        } else {
            logger.debug("save: ignoring upload [name=${upload.fileName}, alreadyAdded=${(upload.fileName != null)}]")
        }
    }

    void remove() {
        String fileName = form.getString('name')
        logger.debug("remove: invoked [name=${name}]")
        if (fileName) {
            for (Iterator i = mailEditorView.mail.attachments.iterator(); i.hasNext();) {
                Attachment next = i.next()
                if (next.fileName == fileName) {
                    try {
                        FileUtil.delete(next.file)
                    } catch (Exception e) {
                        logger.warn("remove: ignoring exception [message=${e.message}]")
                    }
                    i.remove()
                }
            }
        }
    }

    List<Attachment> getAttachments() {
        mailEditorView.mail.attachments ?: []
    }

    protected Boolean alreadyAdded(String fileName) {
        (collectedFiles.find { it.fileName == fileName } != null)
    }

    protected String getAttachmentDir() {
        getSystemProperty('attachmentPath')
    }
}
