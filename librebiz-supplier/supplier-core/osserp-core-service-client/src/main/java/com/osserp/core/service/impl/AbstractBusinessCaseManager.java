/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 3, 2007 3:02:43 PM 
 * 
 */
package com.osserp.core.service.impl;

import java.util.Date;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.User;

import com.osserp.core.BusinessCase;
import com.osserp.core.BusinessCaseManager;
import com.osserp.core.BusinessContract;
import com.osserp.core.dao.BusinessProperties;
import com.osserp.core.dao.Employees;
import com.osserp.core.dao.Requests;
import com.osserp.core.dao.ResourceProvider;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractBusinessCaseManager extends AbstractDetailsManager implements BusinessCaseManager {
    private static final String PROPERTY_I18N_CONTEXT = "businessCaseProperty";
    private Employees employees = null;

    protected AbstractBusinessCaseManager(
            ResourceProvider resourceProvider, 
            BusinessProperties businessProperties,
            Requests requests,
            Employees employees) {
        super(resourceProvider, businessProperties, requests);
        this.employees = employees;
    }

    @Override
    protected final String getI18nContextName() {
        return PROPERTY_I18N_CONTEXT;
    }

    public BusinessContract createContract(User user, BusinessCase businessCase) {
        return getRequests().createContract(user == null ? null : user.getId(), businessCase);
    }

    public BusinessContract getContract(BusinessCase businessCase) {
        return getRequests().findContract(businessCase);
    }

    public BusinessContract updateContract(User user, BusinessContract contract) throws ClientException {
        assert contract != null;
        if (contract.getType() == null) {
            throw new ClientException(ErrorCode.TYPE_MISSING);
        }
        if (contract.isTrackingRequired() && isNotSet(contract.getTrackingTypeDefault())) {
            throw new ClientException(ErrorCode.VALUES_MISSING);
        }
        contract.setChanged(new Date(System.currentTimeMillis()));
        if (user != null) {
            contract.setChangedBy(user.getId());
        }
        getRequests().update(contract);
        return getRequests().loadContract(contract.getId());
    }

    protected Employees getEmployees() {
        return employees;
    }

    protected void setEmployees(Employees employees) {
        this.employees = employees;
    }

    protected Requests getRequests() {
        return (Requests) getBusinessDetails();
    }

    protected void setRequests(Requests requests) {
        setBusinessDetails(requests);
    }
    
}
