/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Feb 23, 2010 2:10:27 PM 
 * 
 */
package com.osserp.core.dao.telephone;

import java.util.List;

import com.osserp.core.telephone.Telephone;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public interface Telephones {

    /**
     * Tries to find a telephone by id
     * @param id
     * @return telephone or null if not found
     */
    Telephone find(Long id);

    /**
     * Tries to find telephone by mac
     * @param mac
     * @return telephone or null if not found
     */
    Telephone findByMacaddress(String mac);

    /**
     * Tries to find all telephones by reference
     * @param reference
     * @return telephones or null if not found
     */
    List<Telephone> findByReference(Long reference);

    /**
     * Tries to find all telephones by branch
     * @param branch
     * @return telephones or null if not found
     */
    List<Telephone> findByBranch(Long branch);

    /**
     * Provides all telephones
     * @return telephones
     */
    List<Telephone> getAll();

    /**
     * Creates new telephone
     * @param telephone
     * @return telephone
     */
    Telephone createTelephone(Telephone telephone);

    /**
     * Persists current telephone values
     * @param telephone
     */
    void save(Telephone telephone);
}
