/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Nov 2, 2007 9:22:30 PM 
 * 
 */
package com.osserp.gui.tags;

import java.util.Map;

import javax.servlet.jsp.JspException;

import com.osserp.common.Constants;
import com.osserp.common.Option;
import com.osserp.common.web.tags.AbstractOutTag;

import com.osserp.core.CoreServiceProvider;
import com.osserp.core.Options;
import com.osserp.core.contacts.ContactSearch;
import com.osserp.core.contacts.ContactSearchResult;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author so <so@osserp.com>
 * 
 */
public class ContactTag extends AbstractOutTag {

    private boolean initials = false;

    public boolean getInitials() {
        return initials;
    }

    public void setInitials(boolean initials) {
        this.initials = initials;
    }

    @Override
    public String getOutput() throws JspException {
        Long emp = null;
        try {
            emp = (Long) value;
            if (emp == null) {
                return getResourceString("unknown");
            }
            Map map = null;
            if (emp.longValue() >= Constants.SYSTEM_EMPLOYEE) {
                if (initials) {
                    map = CoreServiceProvider.getOptionMap(getRequest(), Options.EMPLOYEE_KEYS);
                } else {
                    map = CoreServiceProvider.getOptionMap(getRequest(), Options.EMPLOYEES);
                }
            } else {
                map = CoreServiceProvider.getOptionMap(getRequest(), Options.USERS);
            }
            if (map.containsKey(emp)) {
                return cut(((Option) map.get(emp)).getName());
            }
            ContactSearch contactSearch = (ContactSearch) CoreServiceProvider.getService(
                    getRequest(), ContactSearch.class.getName());
            ContactSearchResult contact = contactSearch.findNameByContactId(emp);
            if (contact != null) {
                return contact.getDisplayName();
            }
            return getResourceString("unknown");
        } catch (Throwable t) {
            return getResourceString("unknown");
        }
    }
}
