/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 12, 2006 3:10:32 AM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import com.osserp.common.OptionsCache;

import com.osserp.core.BusinessType;
import com.osserp.core.model.RequestListItemVO;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RequestListRowMapper extends BusinessObjectRowMapper {

    protected RequestListRowMapper(OptionsCache optionsCache) {
        super(optionsCache);
    }

    public static final String SELECT_VALUES =
            "SELECT " +
                    "salutation," +
                    "firstname," +
                    "lastname," +
                    "contact_type," +
                    "plan_id," +
                    "type_id," +
                    "customer_id," +
                    "name," +
                    "sales_id," +
                    "manager_id," +
                    "branch_id," +
                    "sales_talk_date," +
                    "presentation_date," +
                    "status_id," +
                    "created," +
                    "created_by," +
                    "city," +
                    "origin_id," +
                    "order_probability," +
                    "order_probability_date," +
                    "plant_capacity," +
                    "last_note_date," +
                    "company_id," +
                    "stopped," + 
                    "origin_type_id," +
                    "street," +
                    "zipcode," + 
                    "accounting_ref," +
                    "shipping_id," +
                    "payment_id," +
                    "delivery_date FROM ";

    
    public Object mapRow(ResultSet rs, int index) throws SQLException {
        //		Long salutation = Long.valueOf(rs.getLong(1));
        BusinessType type = fetchBusinessType(fetchLong(rs, "type_id"));
        String firstName = fetchString(rs, "firstname");
        String lastName = fetchString(rs, "lastname");
        Long contactType = fetchLong(rs, "contact_type");
        String shortName = lastName;
        StringBuilder customerName = createContactName(
                firstName,
                lastName);
        Long managerId = fetchLong(rs, "manager_id");
        Long salesId = fetchLong(rs, "sales_id");
        Long branchId = fetchLong(rs, "branch_id");
        
        RequestListItemVO vo = new RequestListItemVO(
                fetchLong(rs, "plan_id"),
                fetchString(rs, "name"),
                fetchLong(rs, "customer_id"),
                customerName.toString(),
                shortName,
                contactType,
                fetchString(rs, "street"),
                fetchString(rs, "zipcode"),
                fetchString(rs, "city"),
                fetchLong(rs, "origin_id"),
                fetchLong(rs, "origin_type_id"),
                type, // type			
                getLong(rs, "status_id"),
                fetchDate(rs, "created"),
                fetchLong(rs, "created_by"),
                salesId,
                fetchEmployeeKey(salesId),
                managerId,
                fetchEmployeeKey(managerId),
                fetchBranch(branchId),
                fetchDate(rs, "sales_talk_date"),
                fetchDate(rs, "presentation_date"),
                fetchInt(rs, "order_probability"),
                fetchDate(rs, "order_probability_date"),
                fetchDouble(rs, "plant_capacity"),
                fetchDate(rs, "last_note_date"),
                fetchCompany(fetchLong(rs, "company_id")),
                fetchBoolean(rs, "stopped"),
                fetchString(rs, "accounting_ref"),
                fetchLong(rs, "shipping_id"),
                fetchLong(rs, "payment_id"),
                fetchDate(rs, "delivery_date"));
        return vo;
    }

    public static RowMapperResultSetExtractor getReader(OptionsCache cache) {
        return new RowMapperResultSetExtractor(new RequestListRowMapper(cache));
    }
}
