/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 01-Sep-2006 09:14:31 
 * 
 */
package com.osserp.gui.client.sales;

import java.util.List;
import java.util.Map;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.View;
import com.osserp.common.web.ViewName;
import com.osserp.core.sales.SalesOrderVolumeExportConfig;
import com.osserp.core.sales.SalesOrderVolumeExportOperation;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("salesVolumeInvoiceView")
public interface SalesVolumeInvoiceView extends View {

    /**
     * Provides available configs
     * @return configs
     */
    public List<SalesOrderVolumeExportConfig> getConfigs();

    /**
     * Provides the config
     * @return config
     */
    public SalesOrderVolumeExportConfig getConfig();

    /**
     * Selects a config
     * @param id
     * @throws ClientException if selected config is invalid
     */
    public void selectConfig(Long id) throws ClientException;

    /**
     * Provides current operation.
     * @return the operation
     */
    public SalesOrderVolumeExportOperation getOperation();

    /**
     * Provides the volume invoice archive listing. The map provides values as follows: <br/>
     * -key 'recordListing' providing archived invoices <br/>
     * -key 'header' a localized header wich may used by rendering view if no other provided <br/>
     * -key 'exitTarget' so target can find the way back
     * @return map with unreleased order listing values
     */
    public Map<String, Object> getInvoiceArchiveListing();

    /**
     * Provides the unreleased volume invoice listing. The map provides values as follows: <br/>
     * -key 'recordListing' providing open invoices <br/>
     * -key 'header' a localized header wich may used by rendering view if no other provided <br/>
     * -key 'exitTarget' so target can find the way back
     * @return map with unreleased order listing values
     */
    public Map<String, Object> getUnreleasedInvoiceListing();

    /**
     * Provides the unreleased order listing. The map provides values as follows: <br/>
     * -key 'recordListing' providing unreleased orders <br/>
     * -key 'header' a localized header wich may used by rendering view if no other provided <br/>
     * -key 'exitTarget' so target can find the way back
     * @return map with unreleased order listing values
     */
    public Map<String, Object> getUnrelasedOrderListing();

    /**
     * Indicates that view is in list mode
     * @return true if list mode
     */
    public boolean isListMode();

    /**
     * Indicates that view is in list mode and displaying exportable records
     * @return true if exportable list mode
     */
    public boolean isExportableMode();

    /**
     * Enables/disables exportable list mode
     * @param exportableMode
     */
    public void setExportableMode(boolean exportableMode);

    /**
     * Provides a comma separated list of required permissions to create new volume invoices
     * @return createPermissions
     */
    public String getCreatePermissions();

    /**
     * Creates a new volume
     * @throws ClientException if nothing to do
     * @throws PermissionException if user has no permission
     */
    public void createVolume() throws ClientException, PermissionException;

    /**
     * Refreshs the view
     */
    public void refresh();
}
