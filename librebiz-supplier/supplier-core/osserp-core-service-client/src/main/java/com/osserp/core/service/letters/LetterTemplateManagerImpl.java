/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 25, 2008 11:24:03 PM 
 * 
 */
package com.osserp.core.service.letters;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.OptionsCache;
import com.osserp.common.PermissionException;
import com.osserp.common.dms.DocumentData;

import com.osserp.core.dao.LetterContents;
import com.osserp.core.dao.LetterTemplates;
import com.osserp.core.dms.Letter;
import com.osserp.core.dms.LetterContent;
import com.osserp.core.dms.LetterTemplate;
import com.osserp.core.dms.LetterTemplateManager;
import com.osserp.core.dms.LetterType;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class LetterTemplateManagerImpl extends AbstractLetterContentManager implements LetterTemplateManager {
    private static Logger log = LoggerFactory.getLogger(LetterTemplateManagerImpl.class.getName());

    protected LetterTemplateManagerImpl(
            OptionsCache optionsCache,
            LetterContents letterContentsDao,
            LetterOutputService letterOutputService) {
        super(optionsCache, letterContentsDao, letterOutputService);
    }

    public List<LetterType> getTypes() {
        return getTemplatesDao().loadTypes();
    }

    public void updateType(
            LetterType letterType,
            String name,
            String description,
            String greetings,
            String stylesheetName) {
        letterType.setName(name);
        letterType.setDescription(description);
        letterType.setGreetings(greetings);
        letterType.setStylesheetName(stylesheetName);
        getTemplatesDao().save(letterType);
    }

    public List<LetterTemplate> findTemplates(LetterType type) {
        return getTemplatesDao().find(type);
    }

    public List<LetterTemplate> findTemplates(LetterType type, boolean embedded) {
        List<LetterTemplate> list = getTemplatesDao().find(type);
        for (Iterator<LetterTemplate> i = list.iterator(); i.hasNext();) {
            LetterTemplate next = i.next();
            if ((embedded && !next.isEmbedded()) || (!embedded && next.isEmbedded())) {
                i.remove();
            }
        }
        return list;
    }

    public LetterTemplate create(DomainUser user, Letter letter) {
        return getTemplatesDao().create(user.getEmployee(), letter);
    }

    public LetterTemplate create(
            DomainUser user,
            LetterType type,
            String name,
            Long branch,
            LetterTemplate template) {
        return getTemplatesDao().create(user.getEmployee(), type, name, branch, template);
    }

    @Override
    public String[] getDeletePermissions() {
        return new String[] { "letter_template_delete", "organisation_admin" };
    }

    public void delete(DomainUser user, LetterTemplate template) throws PermissionException {
        if (user == null || !user.isPermissionGrant(getDeletePermissions())) {
            throw new PermissionException();
        }
        getTemplatesDao().delete(template);
    }

    public void update(
            DomainUser user,
            LetterTemplate template,
            String templateName,
            String subject,
            String salutation,
            String greetings,
            String language,
            Long signatureLeft,
            Long signatureRight,
            boolean ignoreSalutation,
            boolean ignoreGreetings,
            boolean ignoreHeader,
            boolean ignoreSubject,
            boolean embedded,
            boolean embeddedAbove,
            String embeddedSpaceAfter,
            String contentKeepTogether,
            boolean defaultTemplate)
            throws ClientException {

        boolean wasDefault = template.isDefaultTemplate();
        template.update(
                user.getEmployee(),
                templateName,
                subject,
                salutation,
                greetings,
                language,
                signatureLeft,
                signatureRight,
                ignoreSalutation,
                ignoreGreetings,
                ignoreHeader,
                ignoreSubject,
                embedded,
                embeddedAbove,
                embeddedSpaceAfter,
                contentKeepTogether,
                defaultTemplate);
        getTemplatesDao().save(template);
        if (template.isEmbedded() && wasDefault != template.isDefaultTemplate()) {
            List<LetterTemplate> list = findTemplates(template.getType());
            if (list.size() > 1) {
                for (Iterator<LetterTemplate> i = list.iterator(); i.hasNext();) {
                    LetterTemplate next = i.next();
                    if (!next.getId().equals(template.getId())) {
                        if (template.isDefaultTemplate() && next.isDefaultTemplate()) {
                            next.setDefaultTemplate(false);
                            getTemplatesDao().save(next);
                        }
                    }
                }
            }
        }
    }

    public DocumentData getPdf(DomainUser user, LetterContent letter) {
        try {
            return createPdf(user, letter);
        } catch (Throwable t) {
            log.warn("getPdf() failed [message=" + t.getMessage()
                    + ", class=" + t.getClass().getName() + "]");
            return null;
        }
    }

    private LetterTemplates getTemplatesDao() {
        return (LetterTemplates) getLetterContentsDao();
    }
}
