/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 1, 2008 7:53:51 AM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import com.osserp.common.OptionsCache;

import com.osserp.core.model.SalesMonitoringVO;
import com.osserp.core.sales.SalesMonitoringItem;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesMonitoringRowMapper extends BusinessObjectRowMapper {

    public static final String SELECT_VALUES_FROM =
            "SELECT " +
                    "salutation," +
                    "firstname," +
                    "lastname," +
                    "contact_type," +
                    "id," +
                    "plan_id," +
                    "type_id," +
                    "customer_id," +
                    "status," +
                    "created," +
                    "created_by," +
                    "manager_id," +
                    "manager_sub_id," +
                    "sales_id," +
                    "branch_id," +
                    "name," +
                    "city," +
                    "plant_capacity," +
                    "stopped," +
                    "cancelled," +
                    "installer_id," +
                    "installation_days," +
                    "installation_date," +
                    "delivery_date," +
                    "confirmation_date," +
                    "fcs_id," +
                    "action_created," +
                    "action_created_by," +
                    "action_cancels_other," +
                    "action_canceled_by," +
                    "action_id," +
                    "action_note," +
                    "action_name," +
                    "action_order_id," +
                    "action_status," +
                    "action_group," +
                    "action_cancels_sales," +
                    "action_stops_sales," +
                    "action_starts_up," +
                    "action_manager_selecting," +
                    "action_initializing," +
                    "action_releaeseing," +
                    "requests_dp," +
                    "response_dp," +
                    "requests_di," +
                    "response_di," +
                    "requests_fi," +
                    "response_fi," +
                    "action_closing_delivery," +
                    "action_enabling_delivery," +
                    "action_partial_delivery," +
                    "action_confirming," +
                    "action_verifying," +
                    "action_partial_payment," +
                    "origin_id," +
                    "origin_type_id," +
                    "company_id," +
                    "request_created," +
                    "street," +
                    "zipcode," +
                    "accounting_ref," +
                    "shipping_id," +
                    "payment_id FROM ";

    public static final String SALES_MONITORING = SELECT_VALUES_FROM + "get_sales_monitoring(?)";
    public static final String SUBCONTRACTOR_MONITORING = SELECT_VALUES_FROM + "get_subcontractor_monitoring()";

    public SalesMonitoringRowMapper(OptionsCache optionsCache) {
        super(optionsCache);
    }

    public static RowMapperResultSetExtractor getReader(OptionsCache cache) {
        return new RowMapperResultSetExtractor(new SalesMonitoringRowMapper(cache));
    }

    public Object mapRow(ResultSet rs, int index) throws SQLException {
        String firstName = fetchString(rs, "firstname");// rs.getString(2);
        String lastName = fetchString(rs, "lastname");// rs.getString(3);
        Long contactType = fetchLong(rs, "contact_type");// Long.valueOf(rs.getLong(4));
        StringBuilder customerName = createContactName(
                firstName,
                lastName);
        SalesMonitoringRow row = new SalesMonitoringRow();
        row.customerName = customerName.toString();
        row.shortName = lastName;
        row.contact_type = contactType;
        row.id = fetchLong(rs, "id");
        row.plan_id = fetchLong(rs, "plan_id");
        row.type = fetchBusinessType(fetchLong(rs, "type_id"));
        row.customer_id = fetchLong(rs, "customer_id");
        row.status = fetchLong(rs, "status");
        row.created = fetchDate(rs, "created");
        row.created_by = fetchLong(rs, "created_by");
        row.manager_id = fetchLong(rs, "manager_id");
        row.managerKey = fetchEmployeeKey(row.manager_id);
        row.manager_sub_id = fetchLong(rs, "manager_sub_id");
        row.sales_id = fetchLong(rs, "sales_id");
        row.salesKey = fetchEmployeeKey(row.sales_id);
        row.branch = fetchBranch(fetchLong(rs, "branch_id"));
        row.name = fetchString(rs, "name");
        row.street = fetchString(rs, "street");
        row.zipcode = fetchString(rs, "zipcode");
        row.city = fetchString(rs, "city");
        row.plant_capacity = fetchDouble(rs, "plant_capacity");
        row.stopped = fetchBoolean(rs, "stopped");
        row.cancelled = fetchBoolean(rs, "cancelled");
        row.installer_id = fetchLong(rs, "installer_id");
        row.installation_days = fetchInteger(rs, "installation_days");
        row.installation_date = fetchDate(rs, "installation_date");
        row.delivery_date = fetchDate(rs, "delivery_date");
        row.confirmation_date = fetchDate(rs, "confirmation_date");
        row.fcs_id = fetchLong(rs, "fcs_id");
        row.action_created = fetchDate(rs, "action_created");
        row.action_created_by = fetchLong(rs, "action_created_by");
        row.action_cancels_other = fetchBoolean(rs, "action_cancels_other");
        row.action_canceled_by = fetchLong(rs, "action_canceled_by");
        row.action_id = fetchLong(rs, "action_id");
        row.action_note = fetchString(rs, "action_note");
        row.action_name = fetchString(rs, "action_name");
        row.action_order_id = fetchInteger(rs, "action_order_id");
        row.action_status = fetchLong(rs, "action_status");
        row.action_group = fetchLong(rs, "action_group");
        row.action_cancels_sales = fetchBoolean(rs, "action_cancels_sales");
        row.action_stops_sales = fetchBoolean(rs, "action_stops_sales");
        row.action_starts_up = fetchBoolean(rs, "action_starts_up");
        row.action_manager_selecting = fetchBoolean(rs, "action_manager_selecting");
        row.action_initializing = fetchBoolean(rs, "action_initializing");
        row.action_releaeseing = fetchBoolean(rs, "action_releaeseing");
        row.requests_dp = fetchBoolean(rs, "requests_dp");
        row.response_dp = fetchBoolean(rs, "response_dp");
        row.requests_di = fetchBoolean(rs, "requests_di");
        row.response_di = fetchBoolean(rs, "response_di");
        row.requests_fi = fetchBoolean(rs, "requests_fi");
        row.response_fi = fetchBoolean(rs, "response_fi");
        row.action_closing_delivery = fetchBoolean(rs, "action_closing_delivery");
        row.action_enabling_delivery = fetchBoolean(rs, "action_enabling_delivery");
        row.action_partial_delivery = fetchBoolean(rs, "action_partial_delivery");
        row.action_confirming = fetchBoolean(rs, "action_confirming");
        row.action_verifying = fetchBoolean(rs, "action_verifying");
        row.action_partial_payment = fetchBoolean(rs, "action_partial_payment");
        row.origin_id = fetchLong(rs, "origin_id");
        row.origin_type_id = fetchLong(rs, "origin_type_id");
        row.accounting_ref = fetchString(rs, "accounting_ref");
        row.shipping_id = fetchLong(rs, "shipping_id");
        row.payment_id = fetchLong(rs, "payment_id");
        return row;
    }

    public static boolean addItem(Map<Long, SalesMonitoringItem> map, SalesMonitoringRow row) {
        if (map.containsKey(row.id)) {
            SalesMonitoringItem existing = map.get(row.id);
            existing.addFlowControl(
                    row.fcs_id,
                    row.action_created,
                    row.action_created_by,
                    row.action_cancels_other,
                    row.action_canceled_by,
                    row.action_id,
                    row.action_note,
                    row.action_name,
                    row.action_order_id,
                    row.action_status,
                    row.action_group,
                    row.action_cancels_sales,
                    row.action_stops_sales,
                    row.action_starts_up,
                    row.action_manager_selecting,
                    row.action_initializing,
                    row.action_releaeseing,
                    row.requests_dp,
                    row.response_dp,
                    row.requests_di,
                    row.response_di,
                    row.requests_fi,
                    row.response_fi,
                    row.action_closing_delivery,
                    row.action_enabling_delivery,
                    row.action_partial_delivery,
                    row.action_confirming,
                    row.action_verifying,
                    row.action_partial_payment);
            return false;
        }
        SalesMonitoringItem obj = new SalesMonitoringVO(
                row.customerName,
                row.shortName,
                row.contact_type,
                row.id,
                row.plan_id,
                row.type,
                row.customer_id,
                row.status,
                row.created,
                row.created_by,
                row.manager_id,
                row.managerKey,
                row.manager_sub_id,
                row.sales_id,
                row.salesKey,
                row.branch,
                row.name,
                row.street,
                row.zipcode,
                row.city,
                row.plant_capacity,
                row.stopped,
                row.cancelled,
                row.installer_id,
                row.installation_days,
                row.installation_date,
                row.delivery_date,
                row.confirmation_date,
                row.origin_id,
                row.origin_type_id,
                row.fcs_id,
                row.action_created,
                row.action_created_by,
                row.action_cancels_other,
                row.action_canceled_by,
                row.action_id,
                row.action_note,
                row.action_name,
                row.action_order_id,
                row.action_status,
                row.action_group,
                row.action_cancels_sales,
                row.action_stops_sales,
                row.action_starts_up,
                row.action_manager_selecting,
                row.action_initializing,
                row.action_releaeseing,
                row.requests_dp,
                row.response_dp,
                row.requests_di,
                row.response_di,
                row.requests_fi,
                row.response_fi,
                row.action_closing_delivery,
                row.action_enabling_delivery,
                row.action_partial_delivery,
                row.action_confirming,
                row.action_verifying,
                row.action_partial_payment,
                row.accounting_ref,
                row.shipping_id,
                row.payment_id);
        map.put(row.id, obj);
        return true;
    }
}
