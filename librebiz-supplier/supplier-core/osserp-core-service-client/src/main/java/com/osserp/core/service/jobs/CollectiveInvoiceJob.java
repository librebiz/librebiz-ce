/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 30, 2010 8:27:30 PM 
 * 
 */
package com.osserp.core.service.jobs;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.UserManager;
import com.osserp.common.service.Locator;
import com.osserp.core.finance.VolumeInvoiceProcessor;
import com.osserp.core.sales.SalesOrderVolumeExportConfig;
import com.osserp.core.sales.SalesOrderVolumeExportManager;
import com.osserp.core.sales.SalesOrderVolumeExportOperation;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CollectiveInvoiceJob {
    private static Logger log = LoggerFactory.getLogger(CollectiveInvoiceJob.class.getName());
    private Locator locator = null;
    private Long systemUserId = null;

    protected CollectiveInvoiceJob(Locator locator, Long systemUser) {
        super();
        this.locator = locator;
        this.systemUserId = systemUser;
    }

    public void createInvoices() {
        DomainUser system = getSystemUser();
        SalesOrderVolumeExportManager exportManager = getExportManager();
        List<SalesOrderVolumeExportConfig> configs = exportManager.findConfigs();
        for (int i = 0, j = configs.size(); i < j; i++) {
            SalesOrderVolumeExportConfig config = configs.get(i);
            if (!config.isEol() && !config.isRunning()) {
                if (log.isDebugEnabled()) {
                    log.debug("createInvoices() processing next [config=" + config.getId()
                            + ", name=" + config.getName()
                            + "]");
                }
                try {
                    SalesOrderVolumeExportOperation operation = exportManager.getOperation(system.getEmployee(), config);
                    if (operation.getAvailableOrderCount() > 0) {
                        if (log.isDebugEnabled()) {
                            log.debug("createInvoices() found unbilled orders [count="
                                    + operation.getAvailableOrderCount()
                                    + "]");
                        }
                        VolumeInvoiceProcessor processor = (VolumeInvoiceProcessor) locator.lookupService(config.getVolumeProcessorName());
                        processor.execute(system, config, operation.getAvailableOrders());
                        if (log.isDebugEnabled()) {
                            log.debug("createInvoices() processing done [config=" + config.getId()
                                    + ", name=" + config.getName()
                                    + "]");
                        }
                    }
                } catch (Exception e) {
                    log.error("createInvoices() caught exception [message=" + e.getMessage() + "]", e);
                }
            } else if (log.isDebugEnabled()) {
                if (config.isEol()) {
                    log.debug("createInvoices() ignoring config [id=" + config.getId() + ", reason=eol]");
                }
                if (config.isRunning()) {
                    log.debug("createInvoices() ignoring config [id=" + config.getId() + ", reason=running");
                }
            }
        }
    }

    private DomainUser getSystemUser() {
        UserManager manager = (UserManager) locator.lookupService(UserManager.class.getName());
        return (DomainUser) manager.findById(systemUserId);
    }

    private SalesOrderVolumeExportManager getExportManager() {
        return (SalesOrderVolumeExportManager) locator.lookupService(SalesOrderVolumeExportManager.class.getName());
    }
}
