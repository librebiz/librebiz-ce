/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;

import com.osserp.common.dao.AbstractTablesAwareSpringDao;
import com.osserp.common.dao.Tables;
import com.osserp.core.dao.EventsCleanup;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.finance.Record;

/**
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class EventsCleanupDao extends AbstractTablesAwareSpringDao implements EventsCleanup {
    private static Logger log = LoggerFactory.getLogger(EventsCleanupDao.class.getName());

    private String eventsTable = null;
    private String eventActionsTable = null;
    private String purchaseOrdersTable = null;
    private Long purchaseOrderTypes = null;
    private String salesOrdersTable = null;
    private Long salesOrderTypes = null;

    /**
     * Creates a new events cleanup dao
     * @param jdbcTemplate
     * @param tables
     * @param purchaseOrderEventTypes
     * @param salesOrderEventTypes
     */
    protected EventsCleanupDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            Long purchaseOrderEventTypes,
            Long salesOrderEventTypes) {
        super(jdbcTemplate, tables);
        this.eventsTable = getTable(TableKeys.EVENTS);
        this.eventActionsTable = getTable(TableKeys.EVENT_ACTIONS);
        this.purchaseOrdersTable = getTable(TableKeys.PURCHASE_ORDERS);
        this.salesOrdersTable = getTable(TableKeys.SALES_ORDERS);
        this.purchaseOrderTypes = purchaseOrderEventTypes;
        this.salesOrderTypes = salesOrderEventTypes;
    }

    public void cleanup() {
        this.cleanupPurchaseOrderEvents();
        this.cleanupSalesOrderEvents();
    }

    private void cleanupPurchaseOrderEvents() {
        StringBuilder sql = new StringBuilder();
        sql
                .append("DELETE FROM ")
                .append(eventsTable)
                .append(" WHERE event_config_id IN (SELECT id FROM ")
                .append(eventActionsTable)
                .append(" WHERE type_id = ")
                .append(purchaseOrderTypes)
                .append(") AND reference_id IN (SELECT id FROM ")
                .append(purchaseOrdersTable)
                .append(" WHERE status = ")
                .append(Record.STAT_CLOSED)
                .append(")");
        int deleted = jdbcTemplate.update(sql.toString());
        if (log.isDebugEnabled() && deleted > 0) {
            log.debug("cleanupPurchaseOrderEvents() done [deleted=" + deleted + "]");
        }
    }

    private void cleanupSalesOrderEvents() {
        StringBuilder sql = new StringBuilder();
        sql
                .append("DELETE FROM ")
                .append(eventsTable)
                .append(" WHERE event_config_id IN (SELECT id FROM ")
                .append(eventActionsTable)
                .append(" WHERE type_id = ")
                .append(salesOrderTypes)
                .append(") AND reference_id IN (SELECT id FROM ")
                .append(salesOrdersTable)
                .append(" WHERE is_delivered OR status = ")
                .append(Record.STAT_CLOSED)
                .append(")");
        int deleted = jdbcTemplate.update(sql.toString());
        if (log.isDebugEnabled() && deleted > 0) {
            log.debug("cleanupSalesOrderEvents() done [deleted=" + deleted + "]");
        }
    }
}
