/**
 *
 * Copyright (C) 2017 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jul 22, 2017 
 * 
 */
package com.osserp.core.dao.contacts.impl

import groovy.sql.Sql

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.OptionsCache

import com.osserp.core.Options
import com.osserp.core.contacts.ContactType
import com.osserp.core.contacts.ContactSearchResult
import com.osserp.core.contacts.Salutation
import com.osserp.core.customers.CustomerSearchResult
import com.osserp.core.dao.CustomerQueries

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class CustomerQueriesDao extends AbstractContactQueriesDao implements CustomerQueries {
    private static Logger logger = LoggerFactory.getLogger(CustomerQueriesDao.class.getName())

    private static String VALUES = '''
        id,
        contact_id as contactId,
        type_id as type,
        created,
        created_by as createdBy,
        salutation,
        lastname,
        firstname,
        street,
        zipcode,
        city,
        email,
        request_date_last as lastRequestDate,
        request_count as requestCount,
        request_cancelled_count as requestCancelledCount,
        sales_date_last as lastSalesDate,
        sales_last as lastSales,
        sales_total as salesTotal,
        order_count as orderCount,
        order_date_last as lastOrderDate'''

    private static String SELECT = "SELECT $VALUES"
    private static String VIEW_PREFIX = 'v_'
    private static String VIEW_NAME_DEFAULT = 'customer_stats'

    List<ContactSearchResult> getCustomerList() {
        def query = "SELECT * FROM ${getTableName('customersView')} cu WHERE cu.contact_id " +
            "NOT IN (SELECT contact_id FROM ${getTableName('systemCompanies')}) " +
            "AND cu.contact_id NOT IN (SELECT contact_id FROM ${getTableName('systemCompanyBranchs')}) " +
            "AND cu.contact_id NOT IN (SELECT contact_id FROM ${getTableName('employees')}) ORDER BY id DESC"
        List list = []
        Sql sql = getSql()
        sql.eachRow(query.toString()) { obj ->
            list << createContactResult(obj)
        }
        return list
    }

    List<CustomerSearchResult> getCustomerStats(String context) {
        logger.debug("getCustomerStats: invoked [context=$context]")
        String ctx = context ?: VIEW_NAME_DEFAULT
        def query = "$SELECT FROM $VIEW_PREFIX$ctx"
        logger.debug("getCustomerStats: query created [sql=$query]")
        List list = []
        Sql sql = getSql()
        sql.eachRow(query.toString()) { obj ->
            def contact = createCustomerResult(obj)
            list << contact
        }
        logger.debug("getCustomerStats: done [customers=${list.size()}]")
        return list
    }

    private ContactSearchResult createContactResult(def obj) {
        def result = new ContactSearchResult(
                id: obj.id,
                contactId: obj.contact_id,
                lastname: obj.lastname,
                firstname: obj.firstname,
                street: obj.street,
                zipcode: obj.zipcode,
                city: obj.city,
                email: obj.email,
                created: obj.created,
                createdBy: obj.created_by
                )
        if (obj?.type_id) {
            result.type = fetchContactType(obj.type_id)
        }
        if (obj?.salutation) {
            Salutation salutation = fetchSalutation(obj.salutation)
            if (salutation) {
                result.salutation = salutation
            }
        }
        return result
    }

    private CustomerSearchResult createCustomerResult(def obj) {
        def result = new CustomerSearchResult(
                id: obj.id,
                contactId: obj.contactId,
                lastname: obj.lastname,
                firstname: obj.firstname,
                street: obj.street,
                zipcode: obj.zipcode,
                city: obj.city,
                email: obj.email,
                created: obj.created,
                createdBy: obj.createdBy,
                lastRequestDate: obj.lastRequestDate,
                lastSalesDate: obj.lastSalesDate,
                requestCount: obj.requestCount,
                requestCancelledCount: obj.requestCancelledCount,
                lastSales: obj.lastSales,
                salesTotal: obj.salesTotal,
                orderCount: obj.orderCount,
                lastOrderDate: obj.lastOrderDate
                )
        if (obj?.type) {
            result.type = fetchContactType(obj.type)
        }
        Salutation salutation = fetchSalutation(obj.salutation)
        if (salutation) {
            result.salutation = salutation
        }
        return result
    }
}
