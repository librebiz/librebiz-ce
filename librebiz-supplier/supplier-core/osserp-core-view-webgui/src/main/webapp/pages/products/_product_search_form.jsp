<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%-- Required parent setting example:
<o:viewset view="${productSearchView}" name="searchUrl" value="/productSearch.do"/>
<o:viewset view="${productSearchView}" name="searchResultPage" value="/pages/products/product_search_result_ajax.jsp"/>
--%>
<c:set var="view" value="${sessionScope.searchView}" />
<c:set var="url" value="${view.data['searchUrl']}" />
<c:set var="searchResult" value="${view.data['searchResultPage']}" />

<o:ajaxLookupForm url="${url}" name="productSearchForm" targetElement="ajaxContent" onSuccess="function() { checkInput(); }" preRequest="if (checkSearchException()) {" postRequest="}" minChars="0" events="onkeyup" activityElement="value">
    <div class="form-body">

        <c:import url="/pages/products/_product_search_classification.jsp" />
        
        <c:if test="${view.includeEolAvailable}">
            <div class="col-md-2">
                <div class="form-group">
                    <div class="checkbox">
                        <label for="includeEol"> <c:choose>
                                <c:when test="${view.includeEol}">
                                    <input type="checkbox" id="includeEol" name="includeEol" checked="checked" onchange="javascript:$('productSearchForm').onkeyup();" />
                                </c:when>
                                <c:otherwise>
                                    <input type="checkbox" id="includeEol" name="includeEol" onchange="javascript:$('productSearchForm').onkeyup();" />
                                </c:otherwise>
                            </c:choose> <fmt:message key="includeEol" />
                        </label>
                    </div>
                </div>
            </div>
        </c:if>

        <div class="row">
            <div class="col-md-2">
                <div class="form-group">
                    <div class="checkbox">
                        <label for="beginningWith"> <c:choose>
                                <c:when test="${view.startsWithSet}">
                                    <input type="checkbox" id="startsWith" name="startsWith" checked="checked" onchange="javascript:$('productSearchForm').onkeyup();" />
                                </c:when>
                                <c:otherwise>
                                    <input type="checkbox" id="startsWith" name="startsWith" onchange="javascript:$('productSearchForm').onkeyup();" />
                                </c:otherwise>
                            </c:choose> <fmt:message key="beginningWith" />
                        </label>
                    </div>
                </div>
            </div>

            <c:choose>
                <c:when test="${!view.productSelectionConfigsMode}">
                    <div class="col-md-2">
                        <div class="form-group">
                            <div class="checkbox">
                                <label for="noClassification"> <c:choose>
                                        <c:when test="${view.noClassification}">
                                            <input type="checkbox" id="noClassification" name="noClassification" checked="checked" onchange="javascript:checkNoClassification(); $('productSearchForm').onkeyup();" />
                                        </c:when>
                                        <c:otherwise>
                                            <input type="checkbox" id="noClassification" name="noClassification" onchange="javascript:checkNoClassification(); $('productSearchForm').onkeyup();" />
                                        </c:otherwise>
                                    </c:choose> <fmt:message key="withoutClassification" />
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="checkbox">
                                <label for="showAll"> <c:choose>
                                        <c:when test="${view.showAllProducts}">
                                            <input type="checkbox" id="showAll" name="showAll" checked="checked" onChange="javascript:checkShowAll(); checkInput(); $('productSearchForm').onkeyup();" />
                                        </c:when>
                                        <c:otherwise>
                                            <input type="checkbox" id="showAll" name="showAll" onChange="javascript:checkShowAll(); checkInput(); $('productSearchForm').onkeyup();" />
                                        </c:otherwise>
                                    </c:choose> <fmt:message key="withClassification" />
                                </label>
                            </div>
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="col-md-5">
                        <div class="form-group">
                            <div class="checkbox">
                                <label> <input type="checkbox" name="disabled" checked="checked" disabled="disabled" /> <fmt:message key="allProductsOfClassificationBySelectedFilter" />
                                </label>
                            </div>
                        </div>
                    </div>
                </c:otherwise>
            </c:choose>

        </div>
    </div>
</o:ajaxLookupForm>
<div id="ajaxDiv">
    <div id="ajaxContent">
        <c:if test="${!empty view.list}">
            <o:logger write="search result found... ${searchResult}" level="debug" />
            <c:import url="${searchResult}" />
        </c:if>
    </div>
</div>
