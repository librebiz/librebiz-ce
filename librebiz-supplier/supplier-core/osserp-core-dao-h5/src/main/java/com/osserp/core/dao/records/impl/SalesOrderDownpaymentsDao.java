/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 14, 2006 5:19:03 PM 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.ClientException;
import com.osserp.common.Option;
import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;
import com.osserp.core.Options;
import com.osserp.core.dao.PaymentConditions;
import com.osserp.core.dao.SystemCompanies;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.dao.TaxRates;
import com.osserp.core.dao.impl.Procs;
import com.osserp.core.dao.records.RecordInfoConfigs;
import com.osserp.core.dao.records.RecordMailLogs;
import com.osserp.core.dao.records.RecordPrintOptionDefaults;
import com.osserp.core.dao.records.SalesCancellations;
import com.osserp.core.dao.records.SalesOrderDownpayments;
import com.osserp.core.dao.records.SalesPayments;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Cancellation;
import com.osserp.core.finance.Downpayment;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.Payment;
import com.osserp.core.finance.PaymentAwareRecord;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.finance.RecordNumberCreator;
import com.osserp.core.finance.RecordType;
import com.osserp.core.model.records.SalesDownpaymentImpl;
import com.osserp.core.sales.Sales;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesOrderDownpaymentsDao extends AbstractSalesInvoiceRecords
        implements SalesOrderDownpayments {
    private static Logger log = LoggerFactory.getLogger(SalesOrderDownpaymentsDao.class.getName());
    
    private SalesCancellations cancellations = null;
    private String salesDownpaymentsTable = null;
    private String salesInvoicesTable = null;

    public SalesOrderDownpaymentsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache namesCache,
            TaxRates taxRates,
            SystemCompanies companies,
            RecordInfoConfigs infoConfigs,
            PaymentConditions paymentConditions,
            RecordNumberCreator recordNumberCreator,
            RecordPrintOptionDefaults recordPrintOptionDefaults,
            RecordMailLogs recordMailLogs,
            SalesPayments payments,
            SalesCancellations cancellationsDao) {
        super(
                jdbcTemplate,
                tables,
                sessionFactory,
                namesCache,
                taxRates,
                companies,
                infoConfigs,
                paymentConditions,
                recordNumberCreator,
                recordPrintOptionDefaults,
                recordMailLogs,
                payments);
        cancellations = cancellationsDao;
        salesDownpaymentsTable = getTable(TableKeys.SALES_ORDER_DOWNPAYMENTS);
        salesInvoicesTable = getTable(TableKeys.SALES_INVOICES);
        initializeDeletePermissions(DELETE_PERMISSION_DEFAULT);
    }

    @Override
    protected boolean isIgnoreCanceledStatusOnSearch() {
        return false;
    }

    @Override
    protected Long getDefaultRecordTypeId() {
        return RecordType.SALES_DOWNPAYMENT;
    }

    public Downpayment create(
            Long billingType,
            Order order,
            Employee createdBy,
            boolean copyNote,
            boolean copyTerms,
            boolean copyItems,
            Long userDefinedId, 
            Date userDefinedDate) throws ClientException {
        
        RecordType recordType = getRecordType();
        
        Long invoiceId = isSet(userDefinedId) ? userDefinedId :
            getNextRecordId(createdBy, order.getCompany(), recordType, userDefinedDate);
        
        SalesDownpaymentImpl obj = new SalesDownpaymentImpl(
                invoiceId,
                recordType,
                billingType,
                order,
                copyItems,
                createdBy);
        if (userDefinedDate != null) {
            obj.setCreated(userDefinedDate);
        }
        if (systemPropertyEnabled("salesDownpaymentHeaderByType")) {
            Option btype = getOption(Options.BILLING_TYPES, billingType);
            if (btype != null) {
                obj.setCustomHeader(btype.getName());
            }
        }
        saveInitial(obj);
        if (copyTerms) {
            List<Long> terms = getExistingInfoConfig(order);
            obj.addInfos(createdBy, terms);
            save(obj);
        } else {
            setDefaultInfos(createdBy, obj);
        }
        if (copyNote) {
            obj.setNote(order.getNote());
            save(obj);
        }
        return obj;
    }

    public Downpayment restoreCanceled(Employee user, Downpayment invoice) {
        if (invoice == null || !invoice.isCanceled()) {
            return invoice;
        }
        if (invoice.getIdCanceled() == null) {
            // canceled by method before 2023
            final Object[] params = { invoice.getId() };
            final int[] types = { Types.BIGINT };
            List<Long> result = (List<Long>) jdbcTemplate.query(
                    Procs.RESTORE_CANCELED_DOWNPAYMENT,
                    params,
                    types,
                    getLongRowMapper());
            if (result.isEmpty() || result.get(0) < 1L) {
                log.warn("restoreCanceled: unexpected result. Check database logs. Invoice: " + invoice.getId());
            }
        } else {
            Cancellation c = cancellations.loadCancellation(invoice);
            if (c != null) {
                cancellations.delete(c, user);
            }
        }
        SalesDownpaymentImpl obj = (SalesDownpaymentImpl) getCurrentSession().load(
                SalesDownpaymentImpl.class.getName(), invoice.getId());
        obj.restoreCancelled();
        save(obj);
        return obj;
    }

    public Downpayment cancel(Employee user, Downpayment invoice, Date date) throws ClientException {
        Cancellation result = cancellations.create(user, invoice, date, null);
        invoice.cancel(user, result);
        save(invoice);
        return invoice;
    }

    public void changeBranch(Sales sales) {
        if (sales != null && sales.getBranch() != null) {
            changeBranchBySales(
                salesDownpaymentsTable, 
                sales.getId(), 
                sales.getRequest().getBranch().getId(), 
                true);
        }
    }

    public boolean exists(Long recordId) {
        return exists(salesDownpaymentsTable, recordId);
    }

    public int count(Long referenceId) {
        return count(salesDownpaymentsTable, referenceId);
    }

    public int countBySales(Long salesId) {
        return countBySales(salesDownpaymentsTable, salesId);
    }

    public Date getCreated(Long id) {
        return getCreated(salesDownpaymentsTable, id);
    }

    public List<Downpayment> getByOrder(Order order) {
        return findByStatus(order, false);
    }

    public List<Downpayment> getCanceledByOrder(Order order) {
        return findByStatus(order, true);
    }

    protected List<Map<String, Object>> getWithPayments(boolean descending) {
        StringBuilder query = new StringBuilder("SELECT ");
        query
                .append(RecordDisplayRowMapper.RECORD_VALUES)
                .append(" FROM v_")
                .append(getQueryContext())
                .append("_query WHERE status = ")
                .append(Invoice.STAT_SENT)
                .append(" AND reference_id NOT IN (SELECT reference_id FROM ")
                .append(salesInvoicesTable)
                .append(" WHERE reference_id IS NOT NULL AND NOT is_canceled) ORDER BY id");
        if (descending) {
            query.append(" DESC");
        }
        List<Map<String, Object>> result = (List<Map<String, Object>>) jdbcTemplate.query(
                query.toString(),
                RecordDisplayRowMapper.getReader());
        return result;
    }

    private List<Downpayment> findByStatus(Order order, boolean canceled) {
        List<Downpayment> result = new ArrayList<>();
        StringBuilder sql = new StringBuilder("SELECT id FROM ");
        sql.append(salesDownpaymentsTable);
        if (canceled) {
            sql.append(" WHERE status = 99 AND reference_id = ");
        } else {
            sql.append(" WHERE status <> 99 AND reference_id = ");
        }
        sql.append(order.getId());
        
        List<Long> ids = (List<Long>) jdbcTemplate.query(sql.toString(), getLongRowMapper());
        for (int i = 0, j = ids.size(); i < j; i++) {
            result.add((Downpayment) load(ids.get(i)));
        }
        return result;
    }

    @Override
    protected Class<SalesDownpaymentImpl> getPersistentClass() {
        return SalesDownpaymentImpl.class;
    }

    @Override
    protected void loadPayments(PaymentAwareRecord record) {
        if (isSupportingPayments()) {
            SalesDownpaymentImpl invoice = (SalesDownpaymentImpl) record;
            List<Payment> list = getPayments().getByRecord(invoice);
            for (int i = 0, j = list.size(); i < j; i++) {
                Payment next = list.get(i);
                invoice.getPayments().add(next);
            }
        }
    }

    @Override
    protected void loadAfterAll(Record record) {
        if (record.isCanceled()) {
            SalesDownpaymentImpl invoice = (SalesDownpaymentImpl) record;
            if (invoice.getIdCanceled() != null) {
                try {
                    invoice.setCancellation((Cancellation)
                            cancellations.load(invoice.getIdCanceled()));
                } catch (Exception e) {
                    log.warn("Failed to load referenced cancellation: " + e.getMessage(), e);
                }
            }
        }
    }

    @Override
    protected List<RecordDisplay> addPayments(List<RecordDisplay> invoices) {
        long startTime = System.currentTimeMillis();
        List<RecordDisplay> result = new ArrayList<>();
        // function: get_sales_downpayment_paid_amount(record.id)
        String query = "SELECT get_sales_downpayment_paid_amount(?)";
        for (int i = 0, j = invoices.size(); i < j; i++) {
            RecordDisplay invoice = invoices.get(i);
            List<Double> paid = (List<Double>) jdbcTemplate.query(
                    query.toString(),
                    new Object[] { invoice.getId() },
                    new int[] { Types.BIGINT },
                    getDoubleRowMapper());
            invoice.setPaidAmount(paid.isEmpty() ? 0d : paid.get(0));
            result.add(invoice);
        }
        if (log.isDebugEnabled()) {
            log.debug("addPayments() done [duration=" + (System.currentTimeMillis() - startTime) + "ms]");
        }
        return result;
    }
}
