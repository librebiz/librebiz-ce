/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 22, 2007 12:06:45 PM 
 * 
 */
package com.osserp.core.hrm;

import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.EntityRelation;
import com.osserp.common.PublicHoliday;
import com.osserp.core.employees.Employee;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface TimeRecording extends EntityRelation {

    static final Long DEFAULT_RECORD_TYPE = 1L;

    /**
     * Indicates that booking by system time is enabled
     * @return systemTimeBookingEnabled
     */
    boolean isSystemTimeBookingEnabled();

    /**
     * Enables/disables booking by system time
     * @param systemTimeBookingEnabled
     */
    void setSystemTimeBookingEnabled(boolean systemTimeBookingEnabled);

    /**
     * Indicates that booking by system time is required
     * @return systemTimeBookingRequired
     */
    boolean isSystemTimeBookingRequired();

    /**
     * Enables/disables requirement for booking by system time
     * @param systemTimeBookingRequired
     */
    void setSystemTimeBookingRequired(boolean systemTimeBookingRequired);

    /**
     * Provides the last time record. Last time record will be fetched from selected period if a period was selected. If no period selected, method looks up for
     * current period.
     * @return lastRecord or null if none found
     */
    TimeRecord getLastRecord();

    /**
     * Provides all time recordings in descending order
     * @return recordings
     */
    List<TimeRecordingPeriod> getRecordings();

    /**
     * Adds a new period of recordings
     * @param user
     * @param config
     * @param whenValid
     */
    void addPeriod(Employee user, TimeRecordingConfig config, Date whenValid);

    /**
     * Sets current valid period as selected
     */
    void selectCurrentPeriod();

    /**
     * Sets latest valid period as selected
     */
    void selectLatestPeriod();

    /**
     * Provides current time recording
     * @return current or null if not available
     */
    TimeRecordingPeriod getCurrentPeriod();

    /**
     * Selects a period
     * @param id or null to deselect
     */
    void selectPeriod(Long id);

    /**
     * Selects a period
     * @param date
     */
    void selectPeriod(Date date);

    /**
     * Provides selected period
     * @return selectedPeriod or null if none selected
     */
    TimeRecordingPeriod getSelectedPeriod();

    /**
     * Updates current selected period
     * @param validFrom
     * @param validTil
     * @param carryoverLeave
     * @param carryoverHours
     * @param extraLeave
     * @param firstYearLeave
     * @param expireLeaveMonth
     * @param expireLeaveFromConfig
     * @param state
     * @param terminalChipNumber
     * @throws ClientException if validFrom or validTil is set and invalid
     */
    void updatePeriod(Date validFrom, Date validTil, Double carryoverLeave, Double carryoverHours, Double extraLeave, Double firstYearLeave, Long state,
            Long terminalChipNumber) throws ClientException;

    /**
     * Adds a new time record by gui form
     * @param user adding the record
     * @param type of the new record
     * @param status
     * @param value
     * @param note
     * @param systemTime indicates that value was created by systime
     * @throws ClientException
     */
    void addRecord(Employee user, TimeRecordType type, TimeRecordStatus status, Date value, String note, boolean systemTime) throws ClientException;

    /**
     * Adds a new time record by terminal booking
     * @param type of the new record
     * @param status
     * @param value
     * @parama terminalId
     * @throws ClientException
     */
    void addRecord(TimeRecordType type, TimeRecordStatus status, Date value, Long terminalId) throws ClientException;

    /**
     * Provides available time recording periods in this recording
     * @return available periods
     */
    List<TimeRecordingPeriod> getAvailablePeriods();

    /**
     * Adds a new marker to current selected period
     * @param user adding the marker
     * @param type of the new marker
     * @param date
     * @param value
     * @param note
     * @throws ClientException
     */
    void addMarker(Employee user, TimeRecordMarkerType type, Date date, Double value, String note) throws ClientException;

    /**
     * Creates a correction for an existing record
     * @param user performing correction
     * @param record to correct
     * @param startDate
     * @param stopDate
     * @param note to correction
     * @return correctionRecord or null if source record no longer exists
     * @throws ClientException if value or note not set
     */
    TimeRecord createCorrection(
            Employee user,
            TimeRecord record,
            Date startDate,
            Date stopDate,
            String note,
            TimeRecordStatus status)
            throws ClientException;

    /**
     * Indicates that a previos period is available. Returns everytimes false if none current selected.
     * @return previousPeriodAvailable
     */
    boolean isPreviousPeriodAvailable();

    /**
     * Indicates that a more current period as current selected is available. Returns everytimes false if none current selected.
     * @return nextPeriodAvailable
     */
    boolean isNextPeriodAvailable();

    /**
     * Provides the default type for time records
     * @return defaultRecordType
     */
    Long getDefaultRecordType();

    /**
     * Refreshs current selected period and month selections
     */
    void refreshCurrentSelection();

    /**
     * Removes a time record
     * @param id
     */
    void removeRecord(Long id);

    /**
     * Removes a marker
     * @param id
     */
    void removeMarker(Long id);

    Date getLastClosingActionTime();

    void setLastClosingActionTime(Date date);

    /**
     * Synchronizes public holiday of all periods by list of all
     * @param allPublicHoliday
     */
    void synchronizePublicHolidays(List<PublicHoliday> allPublicHoliday);

    /**
     * Provides the next period if available
     * @retrun nexPeriod or null if not available
     */
    TimeRecordingPeriod getNextPeriod();

    /**
     * Provides the terminal chip number
     * @retrun terminalChipNumber
     */
    Long getTerminalChipNumber();

    /**
     * Sets the terminal chip number
     * @retrun terminalChipNumber
     */
    void setTerminalChipNumber(Long terminalChipNumber);

    /**
     * Provides the carryover minutes
     * @return carryoverMinutes
     */
    int getCarryoverMinutes();

    /**
     * Provides remaining leave in days
     * @return remainingLeave
     */
    double getRemainingLeave();

    /**
     * Checks start and stop date on selectedPeriod for potential add operations
     * @param startDate
     * @param stopDate
     * @throws ClientException if a required booking is missing or date already booked
     */
    void checkRecordDate(Date startDate, Date stopDate) throws ClientException;

    /**
     * Checks start or stop date on selectedPeriod
     * @param startDate
     * @param startBooking indicates whether start or stop booking should be checked
     * @throws ClientException if a required booking is missing or date already booked
     */
    void checkRecordDate(Date date, boolean startBooking) throws ClientException;
}
