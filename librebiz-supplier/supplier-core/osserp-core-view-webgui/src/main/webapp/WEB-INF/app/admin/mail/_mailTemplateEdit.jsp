<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4><fmt:message key="templateEdit"/></h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="name"><fmt:message key="name"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <v:text styleClass="form-control" name="name"/>
                    </div>
                </div>
            </div>

            <c:choose>
                <c:when test="${view.bean.headless}">
                    <div class="row next">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="originator"><fmt:message key="originator"/></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <v:text styleClass="form-control" name="originator"/>
                            </div>
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="row next">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="originator"><fmt:message key="originator"/></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <div class="checkbox checkboxRelated">
                                    <label for="sendAsUser">
                                        <v:checkbox id="sendAsUser" />
                                        <fmt:message key="sendAsUserLabel"/>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="sendAsUserCheckbox"><fmt:message key="originatorDeviant"/></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <v:text styleClass="form-control" name="originator"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="fixedOriginCheckbox"></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <div class="checkbox checkboxRelated">
                                    <label for="fixedOriginator">
                                        <v:checkbox id="fixedOriginator" />
                                        <fmt:message key="notChangeable"/>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:otherwise>
            </c:choose>

            <div class="row next">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="recipientsBCC"><fmt:message key="archiveAddress"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <v:text styleClass="form-control" name="recipientsBCC"/>
                    </div>
                </div>
            </div>

            <c:if test="${!view.bean.headless}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="fixedRecipientsBCCCheckbox"></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox checkboxRelated">
                                <label for="fixedRecipientsBCC">
                                    <v:checkbox id="fixedRecipientsBCC" />
                                    <fmt:message key="notChangeable"/>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="hideRecipientsBCCCheckbox"></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox checkboxRelated">
                                <label for="hideRecipientsBCC">
                                    <v:checkbox id="hideRecipientsBCC" />
                                    <fmt:message key="notShown"/>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </c:if>

            <c:if test="${view.bean.headless && !view.bean.recipientsByAction}">
                <div class="row next">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="recipients"><fmt:message key="recipient"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="recipients"/>
                        </div>
                    </div>
                </div>
                <div class="row next">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="recipientsCC"><fmt:message key="recipientCC"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="recipientsCC"/>
                        </div>
                    </div>
                </div>
            </c:if>
            <c:if test="${!view.bean.headless && view.bean.recipientsByUser}">
                <div class="row next">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="recipientsCC"><fmt:message key="recipientCC"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="recipientsCC"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="fixedRecipientsCCCheckbox"></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox checkboxRelated">
                                <label for="fixedRecipientsCC">
                                    <v:checkbox id="fixedRecipientsCC" />
                                    <fmt:message key="notChangeable"/>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </c:if>

            <div class="row next">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="subject"><fmt:message key="subject"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <v:text styleClass="form-control" name="subject"/>
                    </div>
                </div>
            </div>

            <c:if test="${!view.bean.headless}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="fixedTextCheckbox"><fmt:message key="subjectAndText"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <label for="fixedText">
                                    <v:checkbox id="fixedText" />
                                    <fmt:message key="notChangeable"/>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </c:if>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="personalizedHeaderCheckbox"></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="checkbox">
                            <label for="personalizedHeader">
                                <v:checkbox id="personalizedHeader" />
                                <fmt:message key="personalizedHeader"/>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="personalizedFooterCheckbox"></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="checkbox">
                            <label for="personalizedFooter">
                                <v:checkbox id="personalizedFooter" />
                                <fmt:message key="personalizedFooter"/>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="addUserCheckbox"></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="checkbox">
                            <label for="addUser">
                                <v:checkbox id="addUser" />
                                <fmt:message key="templateAddUser"/>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="username"><fmt:message key="templateAddUserCustom"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <v:text styleClass="form-control" name="username"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="addGroupCheckbox"></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="checkbox">
                            <label for="addGroup">
                                <v:checkbox id="addGroup" />
                                <fmt:message key="templateAddGroup"/>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="groupname"><fmt:message key="templateAddGroupCustom"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <v:text styleClass="form-control" name="groupname"/>
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-4"> </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <v:submitExit url="/admin/mail/mailTemplateConfig/disableEditMode"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <o:submit styleClass="form-control" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
