<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<c:set var="contactView" value="${sessionScope.contactView}" />
<c:set var="contact" value="${sessionScope.contactView.customer}" />

<div class="contentBox">
    <div class="contentBoxHeader">
        <div class="contentBoxHeaderLeft">
            <table class="valueTable first33">
                <tbody>
                    <tr>
                        <td style="padding: 0 5px;"><span style="margin-right: 8px;"><fmt:message key="customer" /></span> <o:out value="${contact.id}" /></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="contentBoxHeaderRight">
            <div class="boxnav">
                <ul>
                    <c:if test="${!contact.grantContactNone}">
                        <li>
                            <v:link url="/customers/customerEditor/forward?exit=/customerDisplay.do" title="changeDetails">
                                <o:img name="writeIcon" />
                            </v:link>
                        </li>
                    </c:if>
                    <oc:systemPropertyEnabled name="customerLetterSupport">
                        <li>
                            <v:link url="/customers/customerLetter/forward?exit=/customerDisplay.do" title="correspondenceTitle">
                                <o:img name="letterIcon" />
                            </v:link>
                        </li>
                    </oc:systemPropertyEnabled>
                    <c:if test="${!contact.branchOffice}">
                        <li>
                            <v:link url="/customers/customerSalesRecords/forward?exit=/customerDisplay.do" title="summaryRecordJournal">
                                <o:img name="moneyIcon" />
                            </v:link>
                        </li>
                    </c:if>
                </ul>
            </div>
        </div>
    </div>
    <div class="contentBoxData">
        <div class="smallSpacer"></div>
        <div class="subcolumns">
            <div class="subcolumn">
                <table class="valueTable first33">
                    <tbody>
                        <%-- payment agreement --%>
                        <c:if test="${contact.reseller and !contact.client}">
                            <c:import url="/pages/contacts/shared/payment_agreement.jsp" />
                        </c:if>
                        <%-- customer details --%>
                        <c:if test="${!contactView.paymentAgreementEditMode}">
                            <tr>
                                <td><fmt:message key="taxTreatment" /></td>
                                <td><c:choose>
                                        <c:when test="${empty contact.vatId}">
                                            <oc:options name="customerTypes" value="${contact.typeId}" />
                                        </c:when>
                                        <c:otherwise>
                                            <oc:options name="customerTypes" value="${contact.typeId}" />
                                            <br />
                                            <o:out value="${contact.vatId}" />
                                        </c:otherwise>
                                    </c:choose></td>
                            </tr>
                            <c:choose>
                                <c:when test="${contactView.discountEditMode}">
                                    <o:form name="customerForm" url="/customers.do">
                                        <input type="hidden" name="method" value="updateDiscount" />
                                        <tr>
                                            <td><fmt:message key="rebate" /></td>
                                            <c:choose>
                                                <c:when test="${contact.discountEnabled}">
                                                    <td><input type="radio" name="discountEnabled" value="true" checked="checked" /> &nbsp;<fmt:message key="activated" /> &nbsp;&nbsp; <input type="radio" name="discountEnabled" value="false" />&nbsp; <fmt:message key="deactivated" /></td>
                                                </c:when>
                                                <c:otherwise>
                                                    <td><input type="radio" name="discountEnabled" value="true" />&nbsp; <fmt:message key="activated" />&nbsp;&nbsp; <input type="radio" name="discountEnabled" value="false" checked="checked" /> &nbsp;<fmt:message key="deactivated" /></td>
                                                </c:otherwise>
                                            </c:choose>
                                        </tr>
                                        <tr>
                                            <td><fmt:message key="classAProducts" /></td>
                                            <td><input type="text" name="discountA" class="number right" value="<o:number value="${contact.discountA}" format="percent"/>" /> %</td>
                                        </tr>
                                        <tr>
                                            <td><fmt:message key="classBProducts" /></td>
                                            <td><input type="text" name="discountB" class="number right" value="<o:number value="${contact.discountB}" format="percent"/>" /> %</td>
                                        </tr>
                                        <tr>
                                            <td><fmt:message key="classCProducts" /></td>
                                            <td><input type="text" name="discountC" class="number right" value="<o:number value="${contact.discountC}" format="percent"/>" /> %</td>
                                        </tr>
                                        <tr>
                                            <td><a href="<c:url value="/customers.do?method=disableDiscountEdit"/>"> <input type="button" value="<fmt:message key="break"/>" class="submit" />
                                            </a></td>
                                            <td><o:submit style="width:50%;" /></td>
                                        </tr>
                                    </o:form>
                                </c:when>
                                <c:when test="${!contact.client and contact.discountEnabled}">
                                    <o:permission role="ignore_price_limit,executive,executive_sales" info="permissionViewDiscount">
                                        <tr>
                                            <td><fmt:message key="rebate" /></td>
                                            <td><a href="<c:url value="/customers.do?method=enableDiscountEdit"/>"><fmt:message key="activated" /></a></td>
                                        </tr>
                                        <tr>
                                            <td><fmt:message key="classAProducts" /></td>
                                            <td><o:number value="${contact.discountA}" format="percent" /> %</td>
                                        </tr>
                                        <tr>
                                            <td><fmt:message key="classBProducts" /></td>
                                            <td><o:number value="${contact.discountB}" format="percent" /> %</td>
                                        </tr>
                                        <tr>
                                            <td><fmt:message key="classCProducts" /></td>
                                            <td><o:number value="${contact.discountC}" format="percent" /> %</td>
                                        </tr>
                                    </o:permission>
                                    <o:forbidden role="ignore_price_limit,executive,executive_sales">
                                        <tr>
                                            <td><fmt:message key="rebate" /></td>
                                            <td><fmt:message key="activated" /></td>
                                        </tr>
                                    </o:forbidden>
                                </c:when>
                                <c:otherwise>
                                    <c:if test="${!contact.client}">
                                        <o:permission role="ignore_price_limit,executive,executive_sales" info="permissionToggleDiscount">
                                            <tr>
                                                <td><fmt:message key="rebate" /></td>
                                                <td><a href="<c:url value="/customers.do?method=enableDiscountEdit"/>"><fmt:message key="deactivated" /></a></td>
                                            </tr>
                                        </o:permission>
                                        <o:forbidden role="ignore_price_limit,executive,executive_sales">
                                            <tr>
                                                <td><fmt:message key="rebate" /></td>
                                                <td><fmt:message key="deactivated" /></td>
                                            </tr>
                                        </o:forbidden>
                                    </c:if>
                                </c:otherwise>
                            </c:choose>
                            <tr>
                                <td><fmt:message key="status" /></td>
                                <td><oc:options name="customerStatus" value="${contact.statusId}" /></td>
                            </tr>
                            <c:if test="${!contact.client}">
                                <c:if test="${!empty contact.invoiceBy}">
                                    <tr>
                                        <td><fmt:message key="invoiceBy" /></td>
                                        <td><fmt:message key="${contact.invoiceBy}" /></td>
                                    </tr>
                                </c:if>
                                <c:if test="${!empty contact.invoiceEmail}">
                                    <tr>
                                        <td><fmt:message key="invoiceEmail" /></td>
                                        <td><o:out value="${contact.invoiceEmail}" /></td>
                                    </tr>
                                </c:if>
                                <c:if test="${contact.agent}">
                                    <tr>
                                        <td><fmt:message key="agent" /></td>
                                        <td><c:choose>
                                                <c:when test="${contact.agent}">
                                                    <fmt:message key="bigYes" />
                                                </c:when>
                                                <c:otherwise>
                                                    <fmt:message key="bigNo" />
                                                </c:otherwise>
                                            </c:choose> <c:if test="${contact.agent}">
                                                <o:permission role="executive,executive_sales,sales_agent_config" info="permissionViewProvision">
                                                    <span style="margin-left: 10px;"> <fmt:message key="provision" />: <c:choose>
                                                            <c:when test="${contact.agentCommission == 0}">
                                                                <fmt:message key="undefined" />
                                                            </c:when>
                                                            <c:otherwise>
                                                                <o:number value="${contact.agentCommission}" format="currency" />
                                                                <c:choose>
                                                                    <c:when test="${contact.agentCommissionPercent}">%</c:when>
                                                                    <c:otherwise>EUR</c:otherwise>
                                                                </c:choose>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </span>
                                                </o:permission>
                                            </c:if></td>
                                    </tr>
                                </c:if>
                                <c:if test="${contact.tipProvider}">
                                    <tr>
                                        <td><fmt:message key="tipProvider" /></td>
                                        <td><c:choose>
                                                <c:when test="${contact.tipProvider}">
                                                    <fmt:message key="bigYes" />
                                                </c:when>
                                                <c:otherwise>
                                                    <fmt:message key="bigNo" />
                                                </c:otherwise>
                                            </c:choose></td>
                                    </tr>
                                    <c:if test="${contact.tipProvider}">
                                        <o:permission role="executive,executive_sales,sales_agent_config" info="permissionViewProvision">
                                            <tr>
                                                <td><fmt:message key="tipCommission" /></td>
                                                <td><c:choose>
                                                        <c:when test="${contact.tipCommission == 0}">
                                                            <fmt:message key="undefined" />
                                                        </c:when>
                                                        <c:otherwise>
                                                            <o:number value="${contact.tipCommission}" format="currency" />
                                                            <span> EUR</span>
                                                        </c:otherwise>
                                                    </c:choose></td>
                                            </tr>
                                        </o:permission>
                                    </c:if>
                                </c:if>
                                <tr>
                                    <td><fmt:message key="contactDisplay" /></td>
                                    <td><c:choose>
                                            <c:when test="${contactView.defaultDisplay}">
                                                <a href="<c:url value="/customers.do?method=toggleDefaultDisplayGroup"/>" title="<fmt:message key="contactDisplayLoadDescription"/>"> <fmt:message key="contactDisplayLoad" />
                                                </a>
                                            </c:when>
                                            <c:otherwise>
                                                <a href="<c:url value="/customers.do?method=toggleDefaultDisplayGroup"/>" title="<fmt:message key="contactDisplayDefaultDescription"/>"> <fmt:message key="contactDisplayDefault" />
                                                </a>
                                            </c:otherwise>
                                        </c:choose></td>
                                </tr>
                                <oc:systemPropertyEnabled name="customerPropertySupport">
                                    <c:choose>
                                        <c:when test="${empty contact.propertyList}">
                                            <tr>
                                                <td><v:ajaxLink linkId="businessDetails" url="/customers/customerDetails/forward">
                                                        <fmt:message key="detail" />
                                                    </v:ajaxLink></td>
                                                <td><fmt:message key="none" /></td>
                                            </tr>
                                        </c:when>
                                        <c:otherwise>
                                            <tr>
                                                <td></td>
                                                <td><v:ajaxLink linkId="businessDetails" url="/customers/customerDetails/forward">
                                                        <fmt:message key="detail" />
                                                    </v:ajaxLink></td>
                                            </tr>
                                        </c:otherwise>
                                    </c:choose>
                                    <c:forEach var="prop" items="${contact.propertyList}" varStatus="s">
                                        <tr>
                                            <td><fmt:message key="${prop.name}" /></td>
                                            <td><o:out value="${prop.value}" /></td>
                                        </tr>
                                    </c:forEach>
                                </oc:systemPropertyEnabled>
                            </c:if>
                        </c:if>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="smallSpacer"></div>
    </div>
</div>
