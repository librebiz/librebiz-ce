/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 30, 2008 4:44:08 PM 
 * 
 */
package com.osserp.core.model.contacts;

import java.util.Date;

import com.osserp.common.beans.AbstractEntity;
import com.osserp.common.util.DateUtil;
import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.PrivateContact;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PrivateContactImpl extends AbstractEntity implements PrivateContact {

    private String owner = null;
    private Contact contact = null;
    private Contact company = null;
    private String contactType = null;
    private String contactInfo = null;
    private String countryName = null;
    private String groupwareKey = null;
    private String websiteKey = null;

    private Date lastSync = null;
    private boolean unused = false;

    protected PrivateContactImpl() {
        super();
    }

    public PrivateContactImpl(
            DomainUser user,
            Contact contact,
            Contact company,
            String countryName,
            String contactType) {
        super((Long) null, user.getEmployee().getId());
        if (contact instanceof AbstractContactAware) {
            AbstractContactAware aca = (AbstractContactAware) contact;
            this.contact = aca.getRc();
        } else {
            this.contact = contact;
        }
        if (company instanceof AbstractContactAware) {
            AbstractContactAware aca = (AbstractContactAware) company;
            this.company = aca.getRc();
        } else {
            this.company = company;
        }
        this.contactType = contactType;
        this.countryName = countryName;
        this.owner = user.getLdapUid();
    }

    public String getOwner() {
        return owner;
    }

    protected void setOwner(String owner) {
        this.owner = owner;
    }

    public Contact getContact() {
        return contact;
    }

    protected void setContact(Contact contact) {
        this.contact = contact;
    }

    public Contact getCompany() {
        return company;
    }

    protected void setCompany(Contact company) {
        this.company = company;
    }

    public String getContactType() {
        return contactType;
    }

    public void setContactType(String contactType) {
        this.contactType = contactType;
    }

    public String getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(String contactInfo) {
        this.contactInfo = contactInfo;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public boolean isUnused() {
        return unused;
    }

    public void setUnused(boolean unused) {
        this.unused = unused;
    }

    public String getGroupwareKey() {
        return groupwareKey;
    }

    public void setGroupwareKey(String groupwareKey) {
        this.groupwareKey = groupwareKey;
    }

    public String getWebsiteKey() {
        return websiteKey;
    }

    public void setWebsiteKey(String websiteKey) {
        this.websiteKey = websiteKey;
    }

    public Date getLastSync() {
        return lastSync;
    }

    public void setLastSync(Date lastSync) {
        this.lastSync = lastSync;
    }

    public void update(String contactType, String contactInfo) {
        this.contactType = contactType;
        this.contactInfo = contactInfo;
        setChanged(DateUtil.getCurrentDate());
    }
}
