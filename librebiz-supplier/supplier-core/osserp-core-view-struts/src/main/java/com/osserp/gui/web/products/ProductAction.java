/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 16.10.2004 
 * 
 */
package com.osserp.gui.web.products;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ClientException;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.View;
import com.osserp.gui.client.Actions;
import com.osserp.gui.client.products.ProductView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProductAction extends AbstractProductAction {
    private static Logger log = LoggerFactory.getLogger(ProductAction.class.getName());

    /**
     * Loads an product
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward load(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("load() request from " + getUser(session).getId());
        }
        View view = createView(request);
        return mapping.findForward(view.getActionTarget());
    }

    @Override
    public ActionForward disableListSelection(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        super.disableListSelection(mapping, form, request, response);
        return mapping.findForward(Actions.SEARCH);
    }

    /**
     * Returns to the product list provided by a previous search
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward exitDetails(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exitDetails() request from " + getUser(session).getId());
        }
        ProductView view = getProductView(session);
        view.reload();
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Updates prices a an product. Forwards to 'display' on success, 'prices' in case of errors
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward changeSerialnumberFlags(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("changeSerialnumberFlags() request from " + getUser(session).getId());
        }
        ProductView view = getProductView(session);
        try {
            view.changeSerialnumberFlag();
        } catch (ClientException e) {
            saveError(request, e.getMessage());
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Returns to the product list provided by a previous search
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward enableSalesVolumeDisplay(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableSalesVolumeDisplay() request from " + getUser(session).getId());
        }
        ProductView view = getProductView(session);
        view.setSalesVolumeMode(true);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Returns to the product list provided by a previous search
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward disableSalesVolumeDisplay(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableSalesVolumeDisplay() request from " + getUser(session).getId());
        }
        ProductView view = getProductView(session);
        view.setSalesVolumeMode(false);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Enables stock report display
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward enableStockReport(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableStockReport() request from " + getUser(session).getId());
        }
        ProductView view = fetchProductView(session);
        if (view == null) {
            view = createProductView(request);
        } else {
            // check if method was called from list and no product is selected
            Long id = RequestUtil.getLong(request, "id", false);
            if (id != null) {
                view.setSelection(id);
            }
        }
        view.enableStockReport(RequestUtil.getExit(request));
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Disables stock report display
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward disableStockReport(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disableStockReport() request from " + getUser(session).getId());
        }
        ProductView view = getProductView(session);
        return mapping.findForward(view.disableStockReport());
    }
}
