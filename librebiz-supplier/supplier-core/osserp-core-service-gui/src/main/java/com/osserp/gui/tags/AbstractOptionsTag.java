/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 17, 2010 7:28:53 AM 
 * 
 */
package com.osserp.gui.tags;

import java.util.Map;

import com.osserp.common.Option;
import com.osserp.common.web.tags.AbstractOutTag;

import com.osserp.core.CoreServiceProvider;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractOptionsTag extends AbstractOutTag {
    
    private boolean format = false;

    /**
     * Indicates if output string should be formatted.
     * @return true if format required
     */
    public boolean isFormat() {
        return format;
    }

    /**
     * Enables/disables output string formatting
     * @param format
     */
    public void setFormat(boolean format) {
        this.format = format;
    }

    protected final String getOptionValue(Map<Long, Option> options, Long id) {
        Option option = options.get(id);
        return createResult(option);
    }

    protected final String getOptionValue(String key, Long id) {
        Map<Long, Option> options = getOptions(key);
        Option option = options == null ? null : options.get(id);
        return createResult(option);
    }

    protected final Map<Long, Option> getOptions(String key) {
        return CoreServiceProvider.getOptionMap(getRequest(), key);
    }

    protected String createResult(Option option) {
        String result = null;
        if (option != null) {
            if (option.getResourceKey() != null && option.getResourceKey().length() > 0) {
                result = getObjectOutput(getResourceString(option.getResourceKey()));
            } else {
                result = getObjectOutput(option.getName());
            }
        }
        return (result == null) ? "" : (format ? getFormattedString(result) : result);
    }
}
