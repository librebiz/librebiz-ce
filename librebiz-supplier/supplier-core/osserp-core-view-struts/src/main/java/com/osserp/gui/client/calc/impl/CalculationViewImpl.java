/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 13-Aug-2006 15:53:10 
 * 
 */
package com.osserp.gui.client.calc.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.common.PermissionException;
import com.osserp.common.beans.OptionImpl;
import com.osserp.common.web.ViewName;

import com.osserp.core.BusinessCase;
import com.osserp.core.Options;
import com.osserp.core.calc.Calculation;
import com.osserp.core.calc.CalculationManager;
import com.osserp.core.calc.CalculationSearchResult;
import com.osserp.core.calc.Calculator;
import com.osserp.core.calc.CalculatorService;
import com.osserp.core.calc.PartlistManager;
import com.osserp.core.finance.DeliveryNote;
import com.osserp.core.finance.Record;
import com.osserp.core.requests.Request;
import com.osserp.core.sales.Sales;
import com.osserp.core.sales.SalesDeliveryNoteManager;
import com.osserp.core.sales.SalesOffer;
import com.osserp.core.sales.SalesOrder;
import com.osserp.core.sales.SalesRevenue;
import com.osserp.core.sales.SalesRevenueManager;

import com.osserp.gui.client.calc.CalculationView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("calculationView")
public class CalculationViewImpl extends AbstractCalculationAwareView implements CalculationView {
    private static Logger log = LoggerFactory.getLogger(CalculationViewImpl.class.getName());

    private String calculatorName = CalculatorService.CALCULATOR_BY_ITEMS;
    private SalesOrder order = null;
    private SalesOffer offer = null;
    private SalesRevenue revenue = null;

    private List<String> calculatorNames = new ArrayList<>();
    private List<Option> calculationNames = new ArrayList<>();
    private boolean calculationAssigned = false;
    private Calculator calculator = null;
    private String[] discountPermissions = null;
    private String discountPermissionsString = null;
    private String[] overridePartnerPricePermissions = null;
    private String overridePartnerPricePermissionsString = null;

    protected CalculationViewImpl() {
        super();
        calculatorNames.add(CalculatorService.CALCULATOR_BY_ITEMS);
    }

    protected CalculationViewImpl(String discountPermissions, String overridePartnerPricePermissions) {
        this();
        discountPermissionsString = discountPermissions;
        overridePartnerPricePermissionsString = overridePartnerPricePermissions;
    }

    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        super.init(request);
        discountPermissions = getSystemConfigManager().getPermissionConfig(discountPermissionsString);
        overridePartnerPricePermissions = getSystemConfigManager().getPermissionConfig(overridePartnerPricePermissionsString);
        loadCalculations();
    }

    public void init(Calculation calculation) {
        if (calculation != null) {
            setBean(getCalculationManager().getList(calculation.getId()));
            calculationAssigned = true;
            if (!getBusinessCase().isSalesContext()) {
                loadRecordReference(calculation);
            }
            if (log.isDebugEnabled()) {
                log.debug("init() done [calculation=" + calculation.getId() + "]");
            }
        } else {
            log.warn("init() invoked with null param as calculation");
        }
    }

    public void calculate() throws ClientException {
        if (calculator == null) {
            throw new ActionException("calculate", "calculator missing");
        }
        calculator.calculate();
        if (!getBusinessCase().isSalesContext()) {
            loadRecordReference(calculator.getCalculation());
        }
    }

    @Override
    public void refresh() {
        if (log.isDebugEnabled()) {
            log.debug("refresh() invoked [salesContext=" + isSalesContext() + "]");
        }
        if (calculationAssigned) {

            Calculation updated = (Calculation) getCalculationManager().getList(getCalculation().getId());
            setBean(updated);
            if (log.isDebugEnabled()) {
                log.debug("refresh() updated assigned [id=" + updated.getId() + "], values:\n"
                        + updated.getLogValues());
            }

        } else {
            CalculationManager calculationManager = getCalculationManager();
            BusinessCase businessCase = getBusinessCase();
            if (businessCase == null) {
                throw new ActionException("businessCase must not be null");
            }
            List<CalculationSearchResult> existing = calculationManager.findByReference(
                    businessCase.getPrimaryKey(), businessCase.getContextName());
            setList(existing);
            if (getBean() != null) {
                Calculation current = getCalculation();
                setBean(getCalculationManager().getList(current.getId()));
            } else if (calculator != null && calculator.getCalculation() != null) {
                Calculation calc = (Calculation) getCalculationManager().getList(
                        calculator.getCalculation().getId());
                setBean(calc);
            } else if (log.isDebugEnabled()) {
                log.debug("refresh() no calculation to refresh found!");
            }
        }
        loadDependencies();
    }

    public boolean isCalculationChangeable() {
        if (getBusinessCase().isSalesContext()) {
            if (order != null &&
                    (order.isCanceled()
                    || getOrder().isUnchangeable())) {
                return false;
            }
            Calculation current = getCalculation();
            if (current.isHistorical()) {
                return false;
            }
            return true;
        }
        if (offer != null && offer.isUnchangeable()) {
            return false;
        }
        return true;
    }
    
    public boolean isCalculationCreateable() {
        if (getBusinessCase().isSalesContext()) {
            if (getBusinessCase().isCancelled()) {
                return false;
            }
            if (order != null && getOrderManager().isBilled(order)) {
                return false;
            }
            return !isCalculationChangeable();
        }
        if (offer == null || !offer.isUnchangeable()) {
            return false;
        }
        return !getOfferManager().unreleasedOfferExisting(getRequest());
    }

    public boolean isOverridePartnerPricePermissionGrant() {
        return isPermissionGrant(overridePartnerPricePermissions);
    }

    public boolean isCalculatorAvailable() {
        return (calculator != null);
    }

    public Calculator getCalculator() {
        return calculator;
    }

    public SalesOffer getOffer() {
        return offer;
    }

    public SalesOrder getOrder() {
        return order;
    }

    public SalesRevenue getRevenue() {
        return revenue;
    }

    @Override
    public Calculation getCalculation() {
        return (Calculation) getBean();
    }

    @Override
    public void setSelection(Long id) {
        super.setSelection(id);
        if (id != null) {
            Calculation calc = (Calculation) getCalculationManager().getList(id);
            setBean(calc);
            loadRecordReference(calc);
        } else {
            resetSelection();
        }
    }

    protected void resetSelection() {
        refresh();
        List<CalculationSearchResult> existing = getList();
        Calculation calc = (Calculation) getCalculationManager().getList(existing.get(0).getId());
        setBean(calc);
        loadRecordReference(calc);
    }

    public void initCreate() throws ClientException {

        List<CalculationSearchResult> existing = getList();

        if (isSalesContext()) {
            if (order.isClosed()) {
                throw new ClientException(ErrorCode.ORDER_CLOSED);
            }
            if (getOrderManager().isBilled(order)) {
                throw new ClientException(ErrorCode.ORDER_BILLED);
            }
            SalesDeliveryNoteManager deliveryManager = (SalesDeliveryNoteManager) getService(SalesDeliveryNoteManager.class.getName());
            List<DeliveryNote> deliveryNotes = deliveryManager.getByOrder(order.getId());
            for (int i = 0, j = deliveryNotes.size(); i < j; i++) {
                DeliveryNote next = deliveryNotes.get(i);
                if (!next.isUnchangeable()) {
                    throw new ClientException(ErrorCode.UNRELEASED_RECORD);
                }
            }
            if (!order.isUnchangeable()) {
                boolean openCalcExists = false;
                for (int i = 0, j = existing.size(); i < j; i++) {
                    CalculationSearchResult calc = existing.get(i);
                    if (!calc.isHistorical()) {
                        openCalcExists = true;
                    }
                    if (openCalcExists) {
                        if (log.isInfoEnabled()) {
                            log.info("execute() open calculation found! Throwing exception...");
                        }
                        throw new ClientException(ErrorCode.CALCULATION_NOT_NEEDED);
                    }
                }
            }
            List<Option> result = new ArrayList(getOptions(Options.CALCULATION_NAMES));
            Set<String> set = new HashSet();
            for (int i = 0, j = existing.size(); i < j; i++) {
                CalculationSearchResult calc = existing.get(i);
                if (log.isDebugEnabled()) {
                    log.debug("execute() adding existing " + calc.getName());
                }
                set.add(calc.getName());
            }
            for (Iterator<Option> i = result.iterator(); i.hasNext();) {
                Option o = i.next();
                if (log.isDebugEnabled()) {
                    log.debug("execute() examining " + o.getName());
                }
                if (set.contains(o.getName())) {
                    i.remove();
                }
            }
            calculationNames = result;

        } else if (!existing.isEmpty()) {
            String calcName = getResourceString("calculation");
            if (calcName != null) {
                StringBuilder name = new StringBuilder(calcName);
                name.append(" ").append(existing.size() + 1);
                calculationNames.clear();
                calculationNames.add(new OptionImpl(1L, name.toString()));
            } else {
                calcName = Integer.valueOf(existing.size() + 1).toString();
            }
        }
    }

    public void create(String name) throws ClientException, PermissionException {
        BusinessCase bc = getBusinessCase();
        if (bc == null) {
            log.warn("create() no assoziated business case found");
            throw new ActionException();
        }
        if (log.isDebugEnabled()) {
            log.debug("create() invoked [businessCase="
                    + bc.getPrimaryKey()
                    + ", type=" + bc.getType().getId()
                    + ", salesContext=" + isSalesContext()
                    + ", name=" + name + "]");
        }
        CalculationManager manager = getCalculationManager();
        Calculation created = null;
        if (isSalesContext()) {
            Sales sales = getSales();
            created = manager.create(getDomainUser(), sales, name, calculatorName, null);
            order = (SalesOrder) getOrderManager().createNewVersion(getDomainUser().getEmployee(), sales, false, null);
        } else {
            Request request = getRequest();
            created = manager.create(getDomainUser(), request, name, calculatorName, null);
        }
        if (log.isDebugEnabled()) {
            log.debug("create() done [id=" + created.getId() + "]");
        }

        Calculation old = (Calculation) getBean();
        // last parameter = false, because plans must not be copied here, see #4078
        finalizeCreate(manager, created, old);
    }

    public void createCopy(SalesOffer salesOffer, String name) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("createCopy() invoked [offer=" + salesOffer.getId()
                    + ", name=" + name + "]");
        }
        if (isNotSet(name)) {
            throw new ClientException(ErrorCode.NAME_MISSING);
        }
        if (getList() != null && !getList().isEmpty()) {
            boolean newCalcAlreadyExists = false;
            Calculation current = getCalculation();
            if (current != null && offer == null) {
                loadRecordReference(current);
                if (offer == null) {
                    newCalcAlreadyExists = true;
                }
            }
            if (newCalcAlreadyExists && current != null) {
                if (log.isDebugEnabled()) {
                    log.debug("createCopy: New calculation already exists [id=" + current.getId() + "]");
                }
                return;
            }
            List<CalculationSearchResult> existing = getList();
            for (int i = 0, j = existing.size(); i < j; i++) {
                CalculationSearchResult next = existing.get(i);
                if (name.equalsIgnoreCase(next.getName())) {
                    throw new ClientException(ErrorCode.NAME_EXISTS);
                }
            }
        }
        CalculationManager manager = getCalculationManager();
        Calculation toCopy = getOfferManager().getCalculation(salesOffer);
        if (toCopy == null) {
            // offer without calculation may exist due to config changes
            toCopy = manager.create(getDomainUser(), getBusinessCase(), salesOffer);
        }
        BusinessCase businessCase = getBusinessCase(); 
        Calculation created = manager.create(
                getDomainUser(),
                businessCase,
                name,
                toCopy.getCalculatorClass(),
                toCopy);
        finalizeCreate(manager, created, toCopy);
    }

    private void finalizeCreate(CalculationManager manager, Calculation created, Calculation previous) {
        List<CalculationSearchResult> existing = getCalculationManager().findByReference(
                created.getReference(), created.getContextName());
        setList(existing);
        manager.lock(created, getDomainUser());
        setBean(created);
        if (previous != null) {
            try {
                PartlistManager partlistManager = (PartlistManager) getService(PartlistManager.class.getName());
                partlistManager.copyPartlists(previous, created);
            } catch (Exception e) {
                log.error("finalizeCreate() failed to copy partlists [message=" + e.getMessage() + "]");
            }
        }
    }

    public List<Option> getCalculationNames() {
        return calculationNames;
    }

    public List<String> getCalculatorNames() {
        return calculatorNames;
    }

    public String getCalculatorName() {
        return calculatorName;
    }

    public void setCalculatorName(String name) {
        this.calculatorName = name;
    }

    public void changeCalculator(String name) {
        if (isEditMode() && isCalculatorAvailable()) {
            setBean(getCalculator().changeCalculator(name));
        } else {
            this.calculatorName = name;
        }
    }

    public void toggleSalesPriceLock() {
        Calculation calc = getCalculation();
        if (calc != null) {
            if (log.isDebugEnabled()) {
                log.debug("toggleSalesPriceLock() invoked [calc="
                        + calc.getId()
                        + ", locked="
                        + calc.isSalesPriceLocked()
                        + "]");
            }
            setBean(getCalculationManager().toggleSalesPriceLock(getDomainUser(), calc));
            if (log.isDebugEnabled()) {
                Calculation ref = getCalculation();
                log.debug("toggleSalesPriceLock() done [calc="
                        + ref.getId()
                        + ", locked="
                        + ref.isSalesPriceLocked()
                        + "]");
            }
            if (isCalculatorAvailable() && calculator.getCalculation() != null
                    && calculator.getCalculation().getId().equals(calc.getId())) {
                calculator.updateCalculation(getCalculation());
            }
        }
    }

    @Override
    public void setEditMode(boolean editMode) throws PermissionException {
        super.setEditMode(editMode);
        calculator = new DefaultCalculatorImpl(
                getLocator(),
                getDomainUser(),
                getCalculation(),
                getBusinessCase(),
                discountPermissions,
                overridePartnerPricePermissions);
    }

    protected void createInitial(Record record) throws ClientException {
        Request request = getRequest();
        if (request.getType().isCalculationByEvent()) {
            throw new ClientException(ErrorCode.CALCULATION_BY_EVENT);
        }
        if (!request.getType().isRecordByCalculation()) {
            throw new ClientException(ErrorCode.CALCULATION_NOT_SUPPORTED);
        }
        CalculationManager manager = getCalculationManager();
        Calculation result = null;
        if (record == null) {
            // calculation before record -> default/traditional case
            result = manager.create(getDomainUser(), request, null, request.getType().getCalculatorName(), null);
        } else {
            // record before calculation -> businessCase & record created outside
            result = manager.create(getDomainUser(), getBusinessCase(), record);
        }
        List<CalculationSearchResult> list = new ArrayList(manager.findByReference(
                request.getRequestId(), request.getContextName()));
        setList(list);
        setBean(result);
        if (log.isDebugEnabled()) {
            log.debug("createInitial() done [id=" + result.getId() + "]");
        }
    }

    protected void loadCalculations() throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("loadCalculations() invoked [salesContext=" + isSalesContext() + "]");
        }
        BusinessCase businessCase = getBusinessCase();
        List<CalculationSearchResult> existing = getCalculationManager().findByReference(
                businessCase.getPrimaryKey(), businessCase.getContextName());
        if (existing == null || existing.isEmpty()) {
            if (log.isDebugEnabled()) {
                log.debug("loadCalculations() no calculation found");
            }
            if (isSalesContext()) {
                if (isSet(businessCase.getExternalReference())) {
                    createByExternal(getSales());
                } else {
                    SalesOrder order = (SalesOrder) getOrderManager().getBySales(getSales());
                    createInitial(order);
                }
            } else {
                SalesOffer salesOffer = null;
                if (isSet(businessCase.getExternalReference())) {
                    salesOffer = (SalesOffer) getOfferManager().findWithoutCalculation(getRequest());
                }
                createInitial(salesOffer);
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug("loadCalculations() " + existing.size() + " calculation(s) found");
            }
            CalculationSearchResult calc = fetchOpen(existing);
            if (calc == null && isSalesContext() && isSet(getSales().getExternalReference())) {
                Record o = getOrderManager().getBySales(getSales());
                if (!o.isUnchangeable()) {
                    createByExternal(getSales());
                } else {
                    calc = existing.get(0);
                    setBean(getCalculationManager().getList(calc.getId()));
                    setList(existing);
                }
            } else if (calc != null) {
                setBean(getCalculationManager().getList(calc.getId()));
                setList(existing);
                if (log.isDebugEnabled()) {
                    log.debug("loadCalculations() calculation select [id=" + calc.getId() + "]");
                }
            }
        }
        if (isSalesContext()) {
            order = (SalesOrder) getOrderManager().getBySales(getSales());
        } else if (getBean() != null) {
            Calculation calc = getCalculation();
            offer = (SalesOffer) getOfferManager().findByCalculation(calc);
        }
        loadDependencies();
    }

    private CalculationSearchResult fetchOpen(List<CalculationSearchResult> existing) {
        if (isSalesContext() && isSet(getSales().getExternalReference())) {
            CalculationSearchResult result = existing.get(0);
            if (result.isHistorical()) {
                return null;
            }
        }
        return existing.get(0);
    }

    private void createByExternal(Sales sales) throws ClientException {
        SalesOrder salesOrder = (SalesOrder) getOrderManager().getBySales(getSales());
        if (salesOrder == null) {
            if (log.isInfoEnabled()) {
                log.info("loadCalculations() external sales context but order not exists, throwing illegal state exception!");
            }
            throw new IllegalStateException("error.record.not.available");
        }
        createInitial(salesOrder);
    }

    private void loadDependencies() {
        if (getBean() != null) {
            Calculation calculation = getCalculation();
            loadRevenue(calculation);
        }
    }

    private void loadRecordReference(Calculation calculation) {
        if (!isSalesContext()) {
            try {
                offer = (SalesOffer) getOfferManager().findByCalculation(calculation);
            } catch (Exception e) {
                // should not happen in this context
            }
        }
    }

    private void loadRevenue(Calculation calculation) {
        try {
            if (log.isDebugEnabled()) {
                log.debug("loadRevenue() invoked [calculation=" + calculation.getId() + "]");
            }
            SalesRevenueManager salesRevenueManager = (SalesRevenueManager) getService(SalesRevenueManager.class.getName());
            revenue = salesRevenueManager.getRevenue(calculation.getId());

        } catch (Exception e) {
            log.warn("loadRevenue() failed [message=" + e.getMessage() + "]");
            // 	we do nothing here
        }
    }

}
