/**
 *
 * Copyright (C) 2017 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 5, 2017 
 * 
 */
package org.librebiz.bs

import java.nio.file.Files
import java.nio.file.attribute.GroupPrincipal
import java.nio.file.attribute.UserPrincipal

import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 *
 * @author Rainer Kirchner <rk@librebiz.org>
 *
 */
class DmsSetupTask extends AbstractBashTask {

    private static Logger logger = LoggerFactory.getLogger(DmsSetupTask.class)

    String dataDir
    String docDir
    String dmsDir
    String clientsDir

    public DmsSetupTask(boolean verbose, String docDir, String dataDir) {
        super(verbose)
        this.dataDir = dataDir
        this.docDir = docDir
        if (dataDir) {
            dmsDir = "${dataDir}/dms"
            clientsDir = "${dmsDir}/clients"
        }
    }

    boolean run() {
        if (checkFile(docDir, true, false)) {
            String defaultLogoName = 'company-100.jpg'
            String defaultLogo = "${docDir}/examples/${defaultLogoName}"
            if (checkFile(defaultLogo, false, false)) {

                if (checkFile(dataDir, true, false)
                && checkFile(dmsDir, true, false)
                && checkFile(clientsDir, true, true)) {

                    String logoDir = "${clientsDir}/logos"
                    if (!checkFile(logoDir, true, false)) {
                        UserPrincipal owner = getOwner(clientsDir)
                        GroupPrincipal group = getGroup(clientsDir)
                        if (owner && group) {
                            File logodir = new File(logoDir)
                            if (logodir.mkdir()) {
                                setOwner(logoDir, owner)
                                setGroup(logoDir, group)
                                String absoluteLogoPath = "${logoDir}/${defaultLogoName}"
                                try {
                                    Files.copy(new File(defaultLogo).toPath(), 
                                        new File(absoluteLogoPath).toPath())
                                    if (checkFile(absoluteLogoPath, false, true)) {
                                        setOwner(absoluteLogoPath, owner)
                                        setGroup(absoluteLogoPath, group)
                                    }
                                } catch (Exception e) {
                                    logger.warn("Failed to copy logo: ${defaultLogo}", e)
                                }
                            } else {
                                logger.warn("Failed to make directory: ${logoDir}")
                            }
                        } else {
                            logger.warn("Failed to get owner or group from file: ${logoDir}")
                        }
                    } else {
                        logger.debug('Default logo already exists')
                    }
                } else {
                    logger.debug("Data, dms or client directory not exists under ${dataDir}")
                }
            } else {
                logger.debug("Dummy logo not exists ${defaultLogo}")
            }
        } else {
            logger.debug("Documentation directory not exists ${docDir}")
        }
        return true
    }
}
