/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Jan 15, 2010 11:50:37 AM 
 * 
 */
package com.osserp.gui.web.hrm;

import com.osserp.common.web.View;
import com.osserp.gui.client.hrm.TimeRecordingQueryView;
import com.osserp.gui.web.struts.AbstractViewDispatcherAction;

/**
 * 
 * 
 * @author jg <jg@osserp.com>
 * 
 */
public class TimeRecordingQueryAction extends AbstractViewDispatcherAction {

    @Override
    protected Class<? extends View> getViewClass() {
        return TimeRecordingQueryView.class;
    }
}
