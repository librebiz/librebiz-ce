/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 6, 2009 7:15:52 PM 
 * 
 */
package com.osserp.core.model.calc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;

import com.osserp.core.Item;
import com.osserp.core.ItemList;
import com.osserp.core.ItemPosition;
import com.osserp.core.products.Product;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractItemList extends AbstractItemListDisplay implements ItemList {
    private static Logger log = LoggerFactory.getLogger(AbstractItemList.class.getName());

    private Date initialCreated = null;
    private boolean historical = false;
    private boolean locked = false;
    private Date lockTime = null;
    private Long lockedBy = null;
    private boolean wholesale = false;

    private List<ItemPosition> positions = new ArrayList<ItemPosition>();

    /**
     * Creates a new and empty item list required for object serialization. 
     */
    protected AbstractItemList() {
        super();
    }

    /**
     * Creates a new item list with minimal required values
     * @param reference
     * @param contextName
     * @param name
     * @param number
     * @param createdBy
     * @param initialCreated
     * @param wholesale
     */
    protected AbstractItemList(Long reference, String contextName, String name, Integer number, Long createdBy, Date initialCreated, boolean wholesale) {
        super(reference, contextName, name, number, createdBy);
        this.initialCreated = initialCreated;
        this.wholesale = wholesale;
    }

    protected abstract ItemPosition createPosition(String name, Long groupId, boolean discounts, boolean option, boolean partlist, Long partlistId);

    public final void addPosition(String name, Long groupId, boolean discounts, boolean option, boolean partlist, Long partlistId) {
        ItemPosition position = createPosition(name, groupId, discounts, option, partlist, partlistId);
        positions.add(position);
    }

    public void removeItem(Long itemId) {
        for (Iterator<ItemPosition> pi = positions.iterator(); pi.hasNext();) {
            ItemPosition next = pi.next();
            for (Iterator<Item> ii = next.getItems().iterator(); ii.hasNext();) {
                Item nextItem = ii.next();
                if (nextItem.getId().equals(itemId)) {
                    ii.remove();
                    break;
                }
            }
        }
        refresh();
    }

    public final void updateItem(
            Long itemId,
            String customName,
            Double quantity,
            BigDecimal partnerPrice,
            boolean partnerPriceOverridden,
            boolean partnerPriceOverriddenByPermission,
            BigDecimal price,
            String note) throws ClientException {

        if (isNotSet(quantity)) {
            throw new ClientException(ErrorCode.QUANTITY_MISSING);
        }
        Item item = fetchItem(itemId);
        if (item.getProduct().isBillingNoteRequired()
                && isNotSet(note)) {
            throw new ClientException(ErrorCode.DESCRIPTION_MISSING);
        }
        item.setCustomName(customName);
        item.setQuantity(fetchQuantity(item.getProduct(), quantity));
        item.setPrice(price == null ? new BigDecimal(Constants.DOUBLE_NULL) : price);
        item.setPriceOverridden(true);
        item.setNote(note);
        BigDecimal previousPp = item.getPartnerPrice();

        if (item.getProduct().isPriceByQuantity() && !item.isPartnerPriceOverridden()) {
            BigDecimal pp = new BigDecimal(item.getProduct().getPartnerPriceByQuantity(quantity));
            if (log.isDebugEnabled()) {
                log.debug("updateItem() setting partner price by quantity [id=" + itemId
                        + ", product=" + item.getProduct().getProductId()
                        + ", quantity=" + quantity
                        + ", partnerPrice=" + pp
                        + ", previousPartnerPrice=" + previousPp
                        + "]");
            }
            item.setPartnerPrice(pp);
        }
        if ((item.isPartnerPriceEditable() && partnerPriceOverridden)
                || ((!item.isPartnerPriceEditable() && partnerPriceOverridden && partnerPriceOverriddenByPermission))) {
            if (log.isDebugEnabled()) {
                log.debug("updateItem() partner price overridden [id=" + itemId
                        + ", product=" + item.getProduct().getProductId()
                        + ", quantity=" + quantity
                        + ", partnerPrice=" + partnerPrice
                        + ", previousPartnerPrice=" + previousPp
                        + ", partnerPriceOverriddenByPermission=" + partnerPriceOverriddenByPermission
                        + "]");
            }
            item.setManualPartnerPrice(partnerPrice);
        }
        doAfterUpdateOrReplace(item);
        refresh();
    }

    public void replaceItem(
            Long itemId,
            Product product,
            Double quantity,
            BigDecimal price,
            Date priceDate,
            BigDecimal partnerPrice,
            boolean partnerPriceEditable,
            BigDecimal purchasePrice,
            String note) throws ClientException {

        if (isNotSet(quantity)) {
            throw new ClientException(ErrorCode.QUANTITY_MISSING);
        }
        Item item = fetchItem(itemId);
        if (isAlreadyAdded(product, itemId)) {
            throw new ClientException(ErrorCode.ITEM_EXISTING);
        }
        item.replaceProduct(
                product,
                fetchQuantity(product, quantity),
                (price == null ? new BigDecimal(Constants.DOUBLE_NULL) : price),
                priceDate,
                partnerPrice,
                partnerPriceEditable,
                purchasePrice,
                note);
        doAfterUpdateOrReplace(item);
        refresh();
    }

    protected void doAfterUpdateOrReplace(Item updated) {
        // default does nothing, override if additional operation required
    }

    protected Double fetchQuantity(Product product, Double quantity) {
        return (quantity == null ? 1 : quantity);
    }

    public final List<Item> getAllItems() {
        List<Item> result = new ArrayList<Item>();
        if (positions != null && !this.positions.isEmpty()) {
            for (int i = 0, j = positions.size(); i < j; i++) {
                ItemPosition pos = positions.get(i);
                for (int k = 0, l = pos.getItems().size(); k < l; k++) {
                    result.add(pos.getItems().get(k));
                }
            }
        }
        return result;
    }

    public final ItemPosition getPosition(Long id) {
        for (int i = 0, j = positions.size(); i < j; i++) {
            ItemPosition pos = positions.get(i);
            if (pos.getId().equals(id)) {
                return pos;
            }
        }
        throw new ActionException("position " + id + " no longer exists!");
    }

    public ItemPosition getPositionByGroup(Long group) {
        for (int i = 0, j = getPositions().size(); i < j; i++) {
            ItemPosition next = getPositions().get(i);
            if (next.getGroupId().equals(group)) {
                return next;
            }
        }
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.ItemList#resetPosition(java.lang.Long)
     */
    public void resetPosition(Long configGroupId) {
        if (configGroupId != null) {
            for (Iterator<ItemPosition> i = positions.iterator(); i.hasNext();) {
                ItemPosition pos = i.next();
                if (configGroupId.equals(pos.getGroupId())) {
                    for (Iterator<Item> j = pos.getItems().iterator(); j.hasNext();) {
                        Item item = j.next();
                        j.remove();
                        if (log.isDebugEnabled()) {
                            log.debug("resetPosition() item removed [pos=" + pos.getId()
                                    + ", group=" + configGroupId
                                    + ", id=" + item.getId()
                                    + ", product=" + item.getProduct().getProductId()
                                    + ", quantity=" + item.getQuantity()
                                    + "]");
                        }
                    }
                    // configGroup is always unique within positions
                    break;
                }
            }
        }
    }

    public List<ItemPosition> getPositions() {
        return positions;
    }

    protected void setPositions(List<ItemPosition> positions) {
        this.positions = positions;
    }

    public final void moveUpItem(Long itemId) {
        ItemPosition position = fetchPositionByItem(itemId);
        int itemCount = position.getItems().size();
        if (!(itemCount > 1)) {
            // nothing to do
            return;
        }
        Item itemToMove = fetchItem(itemId);
        int currentOrder = itemToMove.getOrderId();
        if (currentOrder <= 1) {
            // nothing to do, item is already first 
            return;
        }
        for (int i = 0, j = itemCount; i < j; i++) {
            Item next = position.getItems().get(i);
            if (next.getOrderId() == currentOrder - 1) {
                next.setOrderId(currentOrder);
                break;
            }
        }
        itemToMove.setOrderId(currentOrder - 1);
    }

    public final void moveDownItem(Long itemId) {
        ItemPosition position = fetchPositionByItem(itemId);
        int itemCount = position.getItems().size();
        if (!(itemCount > 1)) {
            // nothing to do  
            return;
        }
        Item itemToMove = fetchItem(itemId);
        int currentOrder = itemToMove.getOrderId();
        if (currentOrder == itemCount) {
            // nothing to do, item is already last
            return;
        }
        for (int i = 0, j = itemCount; i < j; i++) {
            Item next = position.getItems().get(i);
            if (next.getOrderId() == currentOrder + 1) {
                next.setOrderId(currentOrder);
                break;
            }
        }
        itemToMove.setOrderId(currentOrder + 1);
    }

    public boolean isHistorical() {
        return historical;
    }

    public void setHistorical(boolean historical) {
        this.historical = historical;
    }

    public Date getInitialCreated() {
        return initialCreated;
    }

    public void setInitialCreated(Date initialCreated) {
        this.initialCreated = initialCreated;
    }

    public boolean isWholesale() {
        return wholesale;
    }

    public void setWholesale(boolean wholesale) {
        this.wholesale = wholesale;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public Date getLockTime() {
        return lockTime;
    }

    public void setLockTime(Date lockTime) {
        this.lockTime = lockTime;
    }

    public Long getLockedBy() {
        return lockedBy;
    }

    public void setLockedBy(Long lockedBy) {
        this.lockedBy = lockedBy;
    }

    public void lock(Long user, boolean lock) {
        if (lock) {
            locked = true;
            lockedBy = user;
            lockTime = new Date(System.currentTimeMillis());
        } else {
            locked = false;
            lockedBy = null;
            lockTime = null;
        }
    }

    public boolean isAlreadyAdded(Product product) {
        return isAlreadyAdded(product, null);
    }

    protected boolean isAlreadyAdded(Product product, Long itemId) {
        for (int i = 0, j = getPositions().size(); i < j; i++) {
            ItemPosition pos = getPositions().get(i);
            for (int k = 0, l = pos.getItems().size(); k < l; k++) {
                Item next = pos.getItems().get(k);
                if (next.getProduct().getProductId().equals(product.getProductId())) {
                    if (itemId == null || !itemId.equals(next.getId())) {
                        if (next.getQuantity() >= 0) {
                            if (next.getPrice().doubleValue() >= 0) {
                                if (log.isDebugEnabled()) {
                                    log.debug("isAlreadyAdded() reports true [item="
                                            + next.getId() + ", product="
                                            + next.getProduct().getProductId() + "]");
                                }
                                return true;
                            }
                        } else if (log.isDebugEnabled()) {
                            log.debug("isAlreadyAdded() ignoring correction [item="
                                    + next.getId() + ", product="
                                    + next.getProduct().getProductId() + ", quantity="
                                    + next.getQuantity()
                                    + "]");
                        }
                    }
                }
            }
        }
        return false;
    }

    protected final Item fetchItem(Long itemId) {
        for (int i = 0, j = positions.size(); i < j; i++) {
            ItemPosition pos = positions.get(i);
            for (int k = 0, l = pos.getItems().size(); k < l; k++) {
                Item next = pos.getItems().get(k);
                if (next.getId().equals(itemId)) {
                    return next;
                }
            }
        }
        throw new ActionException("item " + itemId + " no longer exists!");
    }

    private final ItemPosition fetchPositionByItem(Long itemId) {
        for (int i = 0, j = positions.size(); i < j; i++) {
            ItemPosition pos = positions.get(i);
            for (int k = 0, l = pos.getItems().size(); k < l; k++) {
                Item next = pos.getItems().get(k);
                if (next.getId().equals(itemId)) {
                    return pos;
                }
            }
        }
        throw new ActionException("item " + itemId + " has no position!");
    }

    /**
     * Refresh is invoked after items has changed. Default implementation does nothing.
     */
    protected void refresh() {
        // override if required
    }

}
