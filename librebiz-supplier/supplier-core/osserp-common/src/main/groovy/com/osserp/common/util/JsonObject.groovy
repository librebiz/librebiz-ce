/**
 *
 * Copyright (C) 2015 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 12, 2015 
 * 
 */
package com.osserp.common.util

import groovy.json.JsonBuilder

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class JsonObject implements Map {

    private Map obj = [:]

    public JsonObject() {
        super()
    }

    public JsonObject(Map map) {
        super()
        obj = new HashMap(map)
    }

    @Override
    public String toString() {
        JsonBuilder jsonBuilder = new JsonBuilder(obj)
        return jsonBuilder.toString()
    }

    // methods inherited by map

    public void clear() {
        obj.clear()
    }

    public boolean containsKey(Object arg0) {
        return obj.containsKey(arg0)
    }

    public boolean containsValue(Object arg0) {
        return obj.containsValue(arg0)
    }

    public Set entrySet() {
        return obj.entrySet()
    }

    public Object get(Object arg0) {
        return obj.get(arg0)
    }

    public Boolean getBoolean(String name) {
        Object o = get(name)
        if (o instanceof Boolean) {
            return (Boolean) o;
        } else if (o) {
            try {
                return Boolean.getBoolean(o.toString())
            } catch (Exception e) {
                // ignore
            }
        }
        return false
    }

    public int getInt(String name) {
        Object o = get(name)
        if (o instanceof Integer) {
            return ((Integer) o).intValue()
        } else if (o) {
            try {
                return Integer.valueOf(o.toString()).intValue()
            } catch (Exception e) {
                // ignore
            }
        }
        return 0
    }

    public Long getLong(String name) {
        Object o = get(name)
        if (o instanceof Long) {
            return (Long) o
        } else if (o) {
            try {
                return Long.valueOf(o.toString())
            } catch (Exception e) {
                // ignore
            }
        }
        return null
    }

    public List<JsonObject> getJsonList(String name) {
        List<JsonObject> result = []
        Object o = get(name)
        if (o instanceof List) {
            o.each {
                (it instanceof Map) ? result << new JsonObject(it) : result << new JsonObject()
            }
        }
        return result
    }

    public JsonObject getJsonObject(String name) {
        Object o = get(name)
        if (o instanceof Map) {
            return new JsonObject((Map) o)
        }
        return [:]
    }

    public String getString(String name) {
        Object o = get(name)
        if (o instanceof String) {
            return (String) o
        } else if (o) {
            return o.toString()
        }
        return null
    }

    public boolean isEmpty() {
        return obj.isEmpty()
    }

    public Set keySet() {
        return obj.keySet()
    }

    public Object put(Object arg0, Object arg1) {
        return obj.put(arg0, arg1)
    }

    public void putAll(Map arg0) {
        obj.putAll(arg0)
    }

    public Object remove(Object arg0) {
        return obj.remove(arg0)
    }

    public int size() {
        return obj.size()
    }

    public Collection values() {
        return obj.values()
    }
}
