/**
 *
 * Copyright (C) 2015 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 14, 2015 
 * 
 */
package com.osserp.core.dao.impl

import javax.sql.DataSource

import groovy.sql.Sql

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.User
import com.osserp.common.dao.DataAccessService

import com.osserp.core.FcsAction
import com.osserp.core.finance.RecordType
import com.osserp.core.dao.RequestFcsActions
import com.osserp.core.dao.SalesResetService
import com.osserp.core.sales.Sales

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class SalesResetServiceDao extends DataAccessService implements SalesResetService {
    private static Logger logger = LoggerFactory.getLogger(SalesResetServiceDao.class.getName())

    protected RequestFcsActions requestFcsActions
    
    /**
     * Default constructor is used to set dataSource by property 
     */
    protected SalesResetServiceDao() {
        super()
    }

    /**
     * Creates a new salesResetService
     * @param dataSource
     */
    protected SalesResetServiceDao(DataSource dataSource, RequestFcsActions requestFcsActions) {
        super(dataSource)
        this.requestFcsActions = requestFcsActions
    }
    
    Long reset(User user, Sales sales) throws ClientException {
        logger.debug("reset() invoked [sales=${sales.id}, type=${sales.type.id}]")
        Sql sql = getSql()
        Long result = sales.request.primaryKey
        Long salesId = sales.primaryKey
        executeUpdate(sql, "delete from sales_order_delivery_date_history where reference_id in (select id from sales_orders where sales_id = ?)", salesId)
        executeUpdate(sql, "delete from sales_order_items where reference_id in (select id from sales_orders where sales_id = ?)", salesId)
        executeUpdate(sql, "delete from sales_order_item_history where reference_id in (select id from sales_orders where sales_id = ?)", salesId)
        executeUpdate(sql, "delete from sales_order_infos where reference_id in (select id from sales_orders where sales_id = ?)", salesId)
        executeUpdate(sql, "delete from record_status_infos where type_id = ${RecordType.SALES_ORDER} and reference_id in (select id from sales_orders where sales_id = ?)", salesId)
        executeUpdate(sql, "delete from sales_orders where id in (select id from sales_orders where sales_id = ?)", salesId)
        executeUpdate(sql, "delete from project_suppliers where reference_id = ?", salesId)
        executeUpdate(sql, "delete from project_fcs where project_id = ?", salesId)
        executeUpdate(sql, "delete from projects where id = ?", salesId)
        Long requestId = sales.getRequest().getRequestId();
        long fcsCount = getCount(sql, "select count(*) from request_fcs where reference_id = ${requestId}")
        if (fcsCount > 1) {
            executeUpdate(sql, "delete from request_fcs where id = (select max(id) from v_request_fcs where reference_id = ?)", requestId)
            Object[] args = [ requestId, requestId ]
            executeUpdate(sql, "update project_plans set status_id = (select status from v_request_fcs where id = \
                                (select max(id) from v_request_fcs where reference_id = ?)) where plan_id = ?", args)
            
        } else if (fcsCount == 1) {
            // change fcs_id of the only existing fcs
            Integer status = 0
            FcsAction initial = getInitialRequestFcs(sales)
            if (initial) {
                status = initial.status
                Object[] args = [ status, requestId ]
                executeUpdate(sql, "update project_plans set status_id = ? where plan_id = ?", args)
                args = [ initial.id, requestId ]
                executeUpdate(sql, "update request_fcs set fcs_id = ? where reference_id = ?", args)
            } else {
                Object[] args = [ status, requestId ]
                executeUpdate(sql, "update project_plans set status_id = ? where plan_id = ?", args)
            }
        }
        // reset calculation
        executeUpdate(sql, "delete from calculations where historical = false and context = 'sales' and initial = false and reference_id = ? and \
                               id not in (select calculation_id from sales_offer_calculations)", requestId)
        executeUpdate(sql, "update calculations set historical = false, context = 'request', initial = false where id in (select calculation_id from \
                               sales_offer_calculations where reference_id in (select id from sales_offers where sales_id = ?))", requestId)
        // reset the status of the salesOffer selected to create the order
        executeUpdate(sql, "update sales_offers set status = 3 where status = 10 and sales_id = ?", requestId)
        logger.debug("reset: done [request=${result}]")
        return result
    }
    
    protected final Long fetchObjectId(Sql sqlcon, String statement, Object[] args) {
        Long result
        try {
            sqlcon.eachRow(statement, args) { obj ->
                result = obj.id
            }
        } catch (Exception e) {
            logger.error("fetchObjectId: failed [message=${e.message}]", e)
        }
        result
    }

    protected final boolean executeUpdate(Sql sqlcon, String statement, Object[] args) {
        try {
            if (args) {
                return (sqlcon.executeUpdate(statement, args) > 0)
            } else {
                return (sqlcon.executeUpdate(statement) > 0)
            }
        } catch (Exception e) {
            logger.warn("executeUpdate: caught exception [sql=${statement}, args=${args?.toString()}, message=${e.message}]")
        }
        false
    }
    
    protected final Map fetchRow(Sql sqlcon, String statement, Object[] args) {
        def result = fetchRows(sqlcon, statement, args)
        if (result.size() > 0) {
            logger.debug("fetchRow: rows found [count=${result.size()}]")
        }
        (result.size() > 0 ? result.get(0) : null)
    }

    protected final List<Map> fetchRows(Sql sqlcon, String statement, Object[] args) {
        def result = []
        try {
            if (args) {
                result = sqlcon.rows(statement, args)
            } else {
                result = sqlcon.rows(statement)
            }
        } catch (Exception e) {
            logger.warn("fetchRows: caught exception [sql=${statement}, message=${e.message}]", e)
        }
        result
    }
    
    private final long getCount(Sql sql, String statement) {
        long count = 0
        try {
            sql.eachRow(statement) { row ->
                count = row[0]
            }
        } catch (Exception e) {1
            logger.warn("getCount: caught exception [sql=${statement}, message=${e.message}]", e)
        }
        return count
    }
    
    private FcsAction getInitialRequestFcs(Sales sales) {
        List<FcsAction> all = requestFcsActions.findByType(sales.type.id)
        if (all) {
            return all.get(0)
        }
        return null
    }
    
    protected void setRequestFcsActions(RequestFcsActions requestFcsActions) {
        this.requestFcsActions = requestFcsActions
    }
}
