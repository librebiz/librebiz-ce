<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>

<c:if test="${!empty sessionScope.contactSettingsView and !empty sessionScope.contactSettingsView.bean}">
	<c:set var="contact" value="${sessionScope.contactSettingsView.bean}"/>
	<o:permission role="executive,office_management,customer_service" info="permissionContactUserSalutationsPopup">
		<div class="modalBoxHeader">
			<div class="modalBoxHeaderLeft">
				<fmt:message key="changeSalutation"/>
			</div>
		</div>
		<div class="modalBoxData">
			<div class="subcolumns">
				<div class="subcolumn">
					<div class="spacer"></div>
					<o:ajaxForm name="contactForm" targetElement="contact_user_salutations_popup" url="/contacts.do">
						<input type="hidden" name="method" value="updateUserSalutations"/>
						<table class="valueTable">
							<c:if test="${!empty sessionScope.errors}">
								<c:set var="errorsColspanCount" scope="request" value="2"/>
								<c:import url="/errors/_errors_table_entry.jsp"/>
							</c:if>
							<tr>
								<td><fmt:message key="addressField"/></td>
								<td><input type="text" class="longText" name="userNameAddressField" value="<o:out value="${contact.userNameAddressField}" />" /></td>
							</tr>
							<tr>
								<td><fmt:message key="salutation"/></td>
								<td><input type="text" class="longText" name="userSalutation" value="<o:out value="${contact.userSalutation}" />" /></td>
							</tr>
                            <tr>
                                <td class="row-submit">
                                </td>
                                <td class="row-submit">
                                    <o:submit/>
                                </td>
                            </tr>
						</table>
					</o:ajaxForm>
					<div class="spacer"></div>
				</div>
			</div>
		</div>
	</o:permission>
</c:if>
