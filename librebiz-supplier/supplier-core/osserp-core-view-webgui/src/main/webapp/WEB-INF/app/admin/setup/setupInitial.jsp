<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<o:resource/>
<o:setupContext/>
<v:view viewName="setupInitialView"/>
<c:set scope="session" var="contactInputViewName" value="setupInitialView" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="javascript" type="string">
		<script type="text/javascript">
		</script>
	</tiles:put>
	<tiles:put name="headline">
		<fmt:message key="setupInitialView"/>
	</tiles:put>
	<tiles:put name="headline_right">
	</tiles:put>
	<tiles:put name="content" type="string">
        <div class="content-area">
            <v:form id="setupInitialForm" url="/admin/setup/setupInitial/save">
                <div class="row">
                    <c:import url="${viewdir}/admin/setup/_setupInitialCompany.jsp"/>
                    <c:import url="${viewdir}/admin/setup/_setupInitialContact.jsp"/>
                </div>    
            </v:form>
        </div>
	</tiles:put>
</tiles:insert>