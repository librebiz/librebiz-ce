/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 15, 2014 
 * 
 */
package com.osserp.core.system;

import java.util.Map;

import com.osserp.common.Option;
import com.osserp.common.Property;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SystemSetup extends Option {
    
    /**
     * Provides the current status of the setup process
     * @return the currentStatus
     */
    String getCurrentStatus();

    /**
     * Sets the current status of the setup process
     * @param currentStatus the currentStatus to set
     */
    void setCurrentStatus(String currentStatus);

    /**
     * Provides all setup properties mapped by name
     * @return propertyMap by name
     */
    Map<String, Property> getPropertyMap();

    /**
     * Fetches a named property
     * @param name the name of the property
     * @return property value or null if not set or not exists
     */
    String getProperty(String name);
    
    /**
     * Resets all user input (e.g. restart setup). 
     */
    void resetInput();

    /**
     * Removes an existing property.
     * @param user
     * @param name the name of the property to remove
     */
    void removeProperty(Long user, String name);

    /**
     * Updates a property value. The property name will be added if not exists.
     * @param user
     * @param name the name of the property to update
     * @param value the value to set
     */
    void updateProperty(Long user, String name, String value);
}
