/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 7, 2009 3:14:53 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.dao.Tables;

import com.osserp.core.Item;
import com.osserp.core.ItemPosition;
import com.osserp.core.calc.CalculationConfig;
import com.osserp.core.calc.CalculationConfigGroup;
import com.osserp.core.calc.Partlist;
import com.osserp.core.calc.PartlistReference;
import com.osserp.core.dao.ProductPrices;
import com.osserp.core.dao.CalculationConfigs;
import com.osserp.core.dao.Partlists;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.model.calc.PartlistImpl;
import com.osserp.core.products.Product;

/**
 * @author Rainer Kirchner <rk@osserp.com>
 */
public class PartlistsDao extends AbstractItemLists implements Partlists {
    private static Logger log = LoggerFactory.getLogger(PartlistsDao.class.getName());

    private CalculationConfigs configs = null;
    private ProductPrices productPrices = null;

    protected PartlistsDao(JdbcTemplate jdbcTemplate, Tables tables, SessionFactory sessionFactory, CalculationConfigs configs,
            ProductPrices productPrices) {
        super(jdbcTemplate, tables, sessionFactory, TableKeys.PARTLISTS, TableKeys.PARTLIST_POSITIONS, TableKeys.PARTLIST_ITEMS);
        this.productPrices = productPrices;
        this.configs = configs;
    }

    @Override
    protected Class<PartlistImpl> getItemListClass() {
        return PartlistImpl.class;
    }

    private Partlist create(PartlistReference reference, Product referenceProduct, Long createdBy, CalculationConfig config) {
        Integer number = getCount(reference.getId());
        StringBuilder name = new StringBuilder();
        name.append("pl-").append(reference.getId()).append("-").append(number);
        if (reference.getReference() != null) {
            name.append("-").append(reference.getReference());
        }

        PartlistImpl obj = new PartlistImpl(
                reference,
                referenceProduct,
                config.getId(),
                name.toString(),
                getCount(reference.getId()),
                createdBy);
        save(obj);

        for (Iterator<CalculationConfigGroup> i = config.getGroups().iterator(); i.hasNext();) {
            CalculationConfigGroup grp = i.next();
            obj.addPosition(grp.getName(), grp.getId(), grp.isDiscounts(), false, grp.isPartList(), grp.getPartListId());
        }
        save(obj);

        return obj;
    }

    public Partlist create(PartlistReference reference, Product referenceProduct, Long createdBy, Long groupId) {
        CalculationConfig config = null;
        if (groupId != null) {
            CalculationConfigGroup configGroup = configs.findGroup(groupId);
            if (configGroup != null && configGroup.getPartListId() != null) {
                config = configs.load(configGroup.getPartListId());
            }
        }
        if (config == null) {
            throw new BackendException(ErrorCode.APPLICATION_CONFIG);
        }

        return create(reference, referenceProduct, createdBy, config);
    }

    public Partlist create(PartlistReference reference, Product referenceProduct, Long createdBy, Long configId, Partlist old) {
        CalculationConfig config = null;
        if (configId != null) {
            config = configs.load(configId);
        }
        if (config == null) {
            throw new BackendException(ErrorCode.APPLICATION_CONFIG);
        }

        Partlist created = create(reference, referenceProduct, createdBy, config);
        if (old != null) {
            this.copyExisting(created, old);
        }
        return created;
    }

    private void copyExisting(Partlist target, Partlist source) {
        if (log.isDebugEnabled()) {
            log.debug("copyExisting() invoked [source=" + source.getId()
                    + ", target=" + target.getId()
                    + "]");
        }
        List<ItemPosition> existingPositions = source.getPositions();
        int copied = 0;
        int ignored = 0;
        for (int i = 0, j = existingPositions.size(); i < j; i++) {
            ItemPosition existingPos = existingPositions.get(i);
            ItemPosition newPosition = target.getPositionByGroup(existingPos.getGroupId());
            if (newPosition != null) {
                // old position exists also in new calculation
                List<Item> items = existingPos.getItems();
                for (int k = 0, l = items.size(); k < l; k++) {
                    Item item = existingPos.getItems().get(k);
                    try {
                        BigDecimal partnerPrice = item.getPartnerPrice();
                        if (!item.isPartnerPriceOverridden()) {
                            partnerPrice = new BigDecimal(productPrices.getPartnerPrice(item.getProduct(), source.getInitialCreated(), item.getQuantity()));
                        }

                        target.addItem(
                                newPosition.getId(),
                                item.getProduct(),
                                item.getCustomName(),
                                item.getQuantity(),
                                item.getPrice(),
                                source.getInitialCreated(),
                                item.isPriceOverridden(),
                                (partnerPrice == null ? new BigDecimal(0) : partnerPrice),
                                item.isPartnerPriceEditable(),
                                item.isPartnerPriceOverridden(),
                                new BigDecimal(productPrices.getPurchasePrice(item.getProduct(), item.getProduct().getDefaultStock(),
                                        source.getInitialCreated())),
                                item.getTaxRate(),
                                item.getNote(),
                                item.isIncludePrice(),
                                item.getExternalId());
                        copied++;
                    } catch (ClientException c) {
                        if (log.isInfoEnabled()) {
                            log.info("copyExisting() ignoring failed attempt to copy an item ["
                                    + "id=" + item.getId()
                                    + ", position=" + existingPos.getId()
                                    + ", calculation=" + source.getId()
                                    + ", productId=" + (item.getProduct() == null ? "null" : item.getProduct().getProductId())
                                    + "]");
                        }
                        ignored++;
                    }
                }
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("copyExisting() copied items [count=" + copied + ", ignored=" + ignored + "]");
        }

        save(target);
        if (log.isDebugEnabled()) {
            log.debug("copyExisting() done");
        }
    }

    public Partlist findByProduct(Long referenceId, Long productId) {
        if (referenceId == null || productId == null) {
            log.error("findByProduct() invoked with invalid params [referenceId="
                    + referenceId + ", productId=" + productId + "]");
            throw new IllegalStateException("referenceId and productId must not be null");
        }
        StringBuilder sql = new StringBuilder(96);
        sql
                .append("SELECT id FROM ")
                .append(getItemListsTable())
                .append(" WHERE reference_id = ? AND product_id = ? AND historical = false ORDER BY id DESC");
        final Object[] params = { referenceId, productId };
        final int[] types = { Types.BIGINT, Types.BIGINT };
        List result = (List) jdbcTemplate.query(
                sql.toString(), params, types, getLongRowMapper());
        Long calculationId = (result.isEmpty() ? null : (Long) result.get(0));
        if (log.isDebugEnabled()) {
            if (calculationId != null) {
                log.debug("getActive() found calculation [id=" + calculationId + ", reference=" + referenceId + "]");
            } else {
                log.debug("getActive() no calculation found [reference=" + referenceId + "]");
            }
        }
        return (calculationId == null) ? null : (Partlist) load(calculationId);
    }

}
