/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 22, 2008 6:33:53 PM 
 * 
 */
package com.osserp.core.model;

import java.util.HashMap;
import java.util.Map;

import com.osserp.common.beans.AbstractOption;
import com.osserp.common.util.JsonUtil;
import com.osserp.core.mail.MailTemplate;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class MailTemplateImpl extends AbstractOption implements MailTemplate {
    private String template;
    private String subject;
    private String originator;
    private String recipients;
    private String recipientsCC;
    private String recipientsBCC;
    private boolean hideRecipientsBCC = true;
    private boolean recipientsByAction;
    private boolean recipientsByUser;

    private boolean addAttachments;
    private boolean fixedAttachment;
    private boolean fixedOriginator = true;
    private boolean fixedRecipients;
    private boolean fixedRecipientsCC;
    private boolean fixedRecipientsBCC;
    private boolean fixedText;

    private boolean personalizedHeader;
    private boolean personalizedFooter;
    private boolean addGroup = true;
    private String groupname;
    private boolean addUser = true;
    private String username;
    private boolean sendAsUser;
    private boolean customText;
    private boolean headless;
    private boolean internal;
    private boolean internalOnly;
    private boolean disabled;
    private String parameters;

    protected MailTemplateImpl() {
        super();
    }

    protected MailTemplateImpl(MailTemplate o) {
        super(o);
        this.template = o.getTemplate();
        this.subject = o.getSubject();
        this.originator = o.getOriginator();
        this.recipients = o.getRecipients();
        this.recipientsCC = o.getRecipientsCC();
        this.recipientsBCC = o.getRecipientsBCC();
        this.hideRecipientsBCC = o.isHideRecipientsBCC();
        this.addAttachments = o.isAddAttachments();
        this.addGroup = o.isAddGroup();
        this.groupname = o.getGroupname();
        this.addUser = o.isAddUser();
        this.username = o.getUsername();
        this.fixedAttachment = o.isFixedAttachment();
        this.fixedOriginator = o.isFixedOriginator();
        this.fixedRecipients = o.isFixedRecipients();
        this.fixedRecipientsCC = o.isFixedRecipientsCC();
        this.fixedRecipientsBCC = o.isFixedRecipientsBCC();
        this.fixedText = o.isFixedText();
        this.personalizedHeader = o.isPersonalizedHeader();
        this.personalizedFooter = o.isPersonalizedFooter();
        this.sendAsUser = o.isSendAsUser();
        this.customText = o.isCustomText();
        this.headless = o.isHeadless();
        this.internal = o.isInternal();
        this.internalOnly = o.isInternalOnly();
        this.disabled = o.isDisabled();
        this.parameters = o instanceof MailTemplateImpl ?
                ((MailTemplateImpl) o).getParameters() : null;
    }

    public void update(
            String name,
            String subject,
            boolean sendAsUser,
            String originator,
            boolean fixedOriginator,
            String recipients,
            boolean fixedRecipients,
            String recipientsCC,
            boolean fixedRecipientsCC,
            String recipientsBCC,
            boolean fixedRecipientsBCC,
            boolean hideRecipientsBCC,
            boolean addAttachments,
            boolean fixedAttachment,
            boolean fixedText,
            boolean personalizedHeader,
            boolean personalizedFooter,
            boolean addGroup,
            String groupname,
            boolean addUser,
            String username,
            boolean internal,
            boolean internalOnly) {
        setName(name);
        this.subject = subject;
        this.originator = originator;
        this.recipients = recipients;
        this.recipientsCC = recipientsCC;
        this.recipientsBCC = recipientsBCC;
        this.hideRecipientsBCC = hideRecipientsBCC;
        this.addAttachments = addAttachments;
        this.fixedAttachment = fixedAttachment;
        this.fixedOriginator = fixedOriginator;
        this.fixedRecipients = fixedRecipients;
        this.fixedRecipientsCC = fixedRecipientsCC;
        this.fixedRecipientsBCC = fixedRecipientsBCC;
        this.fixedText = fixedText;
        this.personalizedHeader = personalizedHeader;
        this.personalizedFooter = personalizedFooter;
        this.addGroup = addGroup;
        this.groupname = groupname;
        this.addUser = addUser;
        this.username = username;
        this.sendAsUser = sendAsUser;
        this.internal = internal;
        this.internalOnly = internalOnly;
    }

    public String getTemplate() {
        return template;
    }

    protected void setTemplate(String template) {
        this.template = template;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getOriginator() {
        return originator;
    }

    public void setOriginator(String originator) {
        this.originator = originator;
    }

    public String getRecipients() {
        return recipients;
    }

    public void setRecipients(String recipients) {
        this.recipients = recipients;
    }

    public String getRecipientsCC() {
        return recipientsCC;
    }

    public void setRecipientsCC(String recipientsCC) {
        this.recipientsCC = recipientsCC;
    }

    public String getRecipientsBCC() {
        return recipientsBCC;
    }

    public void setRecipientsBCC(String recipientsBCC) {
        this.recipientsBCC = recipientsBCC;
    }

    public boolean isHideRecipientsBCC() {
        return hideRecipientsBCC;
    }

    public void setHideRecipientsBCC(boolean hideRecipientsBCC) {
        this.hideRecipientsBCC = hideRecipientsBCC;
    }

    public boolean isRecipientsByAction() {
        return recipientsByAction;
    }

    protected void setRecipientsByAction(boolean recipientsByAction) {
        this.recipientsByAction = recipientsByAction;
    }

    public boolean isRecipientsByUser() {
        return recipientsByUser;
    }

    protected void setRecipientsByUser(boolean recipientsByUser) {
        this.recipientsByUser = recipientsByUser;
    }

    public boolean isAddAttachments() {
        return addAttachments;
    }

    public void setAddAttachments(boolean addAttachments) {
        this.addAttachments = addAttachments;
    }

    public boolean isFixedAttachment() {
        return fixedAttachment;
    }

    public void setFixedAttachment(boolean fixedAttachment) {
        this.fixedAttachment = fixedAttachment;
    }

    public boolean isFixedOriginator() {
        return fixedOriginator;
    }

    public void setFixedOriginator(boolean fixedOriginator) {
        this.fixedOriginator = fixedOriginator;
    }

    public boolean isFixedRecipients() {
        return fixedRecipients;
    }

    public void setFixedRecipients(boolean fixedRecipients) {
        this.fixedRecipients = fixedRecipients;
    }

    public boolean isFixedRecipientsCC() {
        return fixedRecipientsCC;
    }

    public void setFixedRecipientsCC(boolean fixedRecipientsCC) {
        this.fixedRecipientsCC = fixedRecipientsCC;
    }

    public boolean isFixedRecipientsBCC() {
        return fixedRecipientsBCC;
    }

    public void setFixedRecipientsBCC(boolean fixedRecipientsBCC) {
        this.fixedRecipientsBCC = fixedRecipientsBCC;
    }

    public boolean isFixedText() {
        return fixedText;
    }

    public void setFixedText(boolean fixedText) {
        this.fixedText = fixedText;
    }

    public boolean isPersonalizedHeader() {
        return personalizedHeader;
    }

    public void setPersonalizedHeader(boolean personalizedHeader) {
        this.personalizedHeader = personalizedHeader;
    }

    public boolean isPersonalizedFooter() {
        return personalizedFooter;
    }

    public void setPersonalizedFooter(boolean personalizedFooter) {
        this.personalizedFooter = personalizedFooter;
    }

    public boolean isAddGroup() {
        return addGroup;
    }

    public void setAddGroup(boolean addGroup) {
        this.addGroup = addGroup;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public boolean isAddUser() {
        return addUser;
    }

    public void setAddUser(boolean addUser) {
        this.addUser = addUser;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isSendAsUser() {
        return sendAsUser;
    }

    public void setSendAsUser(boolean sendAsUser) {
        this.sendAsUser = sendAsUser;
    }

    public boolean isCustomText() {
        return customText;
    }

    protected void setCustomText(boolean customText) {
        this.customText = customText;
    }

    public boolean isHeadless() {
        return headless;
    }

    protected void setHeadless(boolean headless) {
        this.headless = headless;
    }

    public boolean isInternal() {
        return internal;
    }

    public void setInternal(boolean internal) {
        this.internal = internal;
    }

    public boolean isInternalOnly() {
        return internalOnly;
    }

    public void setInternalOnly(boolean internalOnly) {
        this.internalOnly = internalOnly;
    }

    public boolean isDisabled() {
        return disabled;
    }

    protected void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public Map<String, Object> getParameterMap() {
        Map<String, Object> result = isSet(parameters)
                ? JsonUtil.parse(parameters) : new HashMap<>();
        return result;
    }

    protected String getParameters() {
        return parameters;
    }

    protected void setParameters(String parameters) {
        this.parameters = parameters;
    }

    @Override
    public Object clone() {
        return new MailTemplateImpl(this);
    }

}
