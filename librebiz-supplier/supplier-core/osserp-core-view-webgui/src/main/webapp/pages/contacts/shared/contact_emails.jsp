<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<c:set var="communicationView" value="${requestScope.communicationView}" />
<c:choose>
    <c:when test="${!empty requestScope.communicationViewContact}">
        <c:set var="contact" value="${requestScope.communicationViewContact}" />
    </c:when>
    <c:otherwise>
        <c:set var="contact" value="${communicationView.bean}" />
    </c:otherwise>
</c:choose>
<c:set var="readonlyView" value="${requestScope.communicationViewReadonly}" />
<div class="contentBox">
    <div class="contentBoxHeader">
        <div class="contentBoxHeaderLeft">
            <fmt:message key="email" />
        </div>
        <div class="contentBoxHeaderRight">
            <div class="boxnav">
                <c:if test="${empty readonlyView}">
                    <ul>
                        <li><v:ajaxLink url="/contacts/contactEmail/forward?view=${communicationView.name}" linkId="contact_emails">
                                <o:img name="newIcon" />
                            </v:ajaxLink></li>
                    </ul>
                </c:if>
            </div>
        </div>
    </div>
    <div class="contentBoxData">
        <div class="subcolumns">
            <div class="subcolumn">
                <div class="smallSpacer"></div>
                <table class="valueTable first33">
                    <tbody>
                        <c:forEach var="email" items="${contact.emails}">
                            <c:if test="${email.primary or !email.alias}">
                                <tr>
                                    <td>
                                        <oc:options name="emailTypes" value="${email.type}" />
                                        <c:if test="${email.primary}"><fmt:message key="mainAddressShort" /></c:if>
                                        </td>
                                        <td><o:email value="${email.email}" limit="50" /></td>
                                </tr>
                            </c:if>
                        </c:forEach>
                        <c:forEach var="person" items="${communicationView.contactPersons}">
                            <c:forEach var="email" items="${person.emails}">
                                <c:if test="${email.primary or !email.alias}">
                                    <tr>
                                        <td>
                                            <oc:options name="emailTypes" value="${email.type}" />
                                            <c:if test="${email.primary}">
                                                <fmt:message key="mainAddressShort" />
                                            </c:if>
                                        </td>
                                        <td>
                                            <o:email value="${email.email}" limit="50" /> /
                                            <a href="<c:url value="/contactPersons.do?method=load&id=${person.contactId}&exit=contactDisplay"/>">
                                                <c:if test="${!empty person.salutation.name}"><o:out value="${person.salutation.name}"/></c:if>
                                                <c:if test="${!empty person.title.name}"><o:out value="${person.title.name}"/></c:if>
                                                <c:choose>
                                                    <c:when test="${empty person.salutation.name and empty person.title.name}">
                                                        <o:out value="${person.displayName}"/>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <o:out value="${person.lastName}"/>
                                                    </c:otherwise>
                                                </c:choose>
                                            </a>
                                        </td>
                                    </tr>
                                </c:if>
                            </c:forEach>
                        </c:forEach>
                    </tbody>
                </table>
                <div class="smallSpacer"></div>
            </div>
        </div>
    </div>
</div>