/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 16, 2006 10:48:39 AM 
 * 
 */
package com.osserp.gui.client.records;

import java.math.BigDecimal;
import java.util.List;

import org.jdom2.Document;

import com.osserp.common.ClientException;
import com.osserp.common.Option;
import com.osserp.common.PermissionException;
import com.osserp.common.dms.DmsDocument;
import com.osserp.common.web.Form;

import com.osserp.core.BankAccount;
import com.osserp.core.BusinessTemplate;
import com.osserp.core.Item;
import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.contacts.Contact;
import com.osserp.core.dms.Letter;
import com.osserp.core.dms.LetterType;
import com.osserp.core.finance.ItemChangedInfo;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordStatusHistory;
import com.osserp.core.products.Product;
import com.osserp.core.system.BranchOffice;
import com.osserp.gui.client.DomainView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface RecordView extends DomainView {

    /**
     * Loads record for display
     * @param id
     * @param exitTarget
     * @param exitId
     * @param readonly
     * @throws ClientException if record no longer exists
     */
    public void loadRecord(Long id, String exitTarget, String exitId, boolean readonly) throws ClientException;

    /**
     * Indicates that order listing is in item listing mode
     * @return true if so
     */
    public boolean isItemListing();

    /**
     * Enables/disables item listing mode in alternating fashion
     */
    public void changeItemListing();

    /**
     * Provides the current record
     * @return record
     */
    public Record getRecord();

    /**
     * Provides an xml document with current record values
     * @param preview sets preview element value
     * @return document xml
     */
    public Document getRecordDocument(boolean preview);

    /**
     * Provides an xml document with current stylesheet
     * @return document xsl
     */
    public Document getRecordStyleheet() throws ClientException;

    /**
     * Refreshs current record with values from the backend
     */
    public void refresh();

    /**
     * Release historical record
     */
    public void releaseHistorical();

    /**
     * Deletes current record
     * @throws ClientException if record is unchangeable
     * @throws PermissionException if user has no permission
     */
    public void delete() throws ClientException, PermissionException;

    /**
     * Indicates whether record view is in product edit mode
     * @return productEditMode
     */
    public boolean isProductEditMode();

    /**
     * Indicates whether record products may be changed
     * @return productsChangeable
     */
    public boolean isProductsChangeable();

    /**
     * Enables/disables product edit mode
     * @param productEditMode
     */
    public void setProductEditMode(boolean productEditMode);

    /**
     * Indicates whether record view is in salesId edit mode
     * @return salesIdEditMode
     */
    public boolean isSalesIdEditMode();

    /**
     * Enables/disables salesId edit mode
     * @param salesIdEditMode
     */
    public void setSalesIdEditMode(boolean salesIdEditMode);

    /**
     * Updates the sales id of record
     * @param salesId
     * @throws ClientException if validation failed
     */
    public void updateSalesId(Long salesId) throws ClientException;

    /**
     * Provides item backup status
     * @return true if backup available
     */
    public boolean isItemBackupAvailable();

    /**
     * Restores items using latest backup
     */
    public void restoreItems();

    /**
     * Indicates if view supports imports of external records
     * @return true if supported
     */
    public boolean isImportSupported();

    /**
     * Enables item edit for given item
     * @param itemId
     */
    public void setItemEdit(Long itemId);

    /**
     * Indicates whether editor is in item edit mode
     * @return true if so
     */
    public boolean isItemEditMode();

    /**
     * Enables or disables item insert mode
     * @param itemId id or null to disable
     */
    public void setItemPaste(Long itemId);

    /**
     * Indicates if editor is in item insert mode
     * @return true if insert mode enabled
     */
    public boolean isItemPasteMode();

    /**
     * Enables or disables item replace mode
     * @param itemId id or null to disable
     */
    public void setItemReplace(Long itemId);

    /**
     * Indicates whether editor is in item replace mode
     * @return true if so
     */
    public boolean isItemReplaceMode();

    /**
     * Provides the current selected item when editor is in item edit mode
     * @return current selected item
     */
    public Item getCurrentItem();

    /**
     * Updates existing item
     * @param item
     * @param customName
     * @param quantity
     * @param price
     * @param taxRate
     * @param note
     * @throws ClientException if quantity is 0
     */
    public void updateItem(
            Item item,
            String customName,
            Double quantity,
            BigDecimal price,
            Double taxRate,
            String note)
            throws ClientException;

    /**
     * Adds new items to record
     * @param products
     * @param ccustomNames
     * @param quantities
     * @param taxRates
     * @param prices
     * @param notes
     * @throws ClientException if selection is invalid
     */
    public void addItems(
            Product[] products,
            String[] customNames,
            Double[] quantities,
            Double[] taxRates,
            BigDecimal[] prices,
            String[] notes)
            throws ClientException;

    /**
     * Deletes given item from given record
     * @param itemId
     * @throws ClientException if item is not deletable
     */
    public void deleteItem(Long itemId) throws ClientException;

    /**
     * Deletes all items
     * @throws ClientException if validation failed
     */
    public void deleteItems() throws ClientException;

    /**
     * Inserts an item by id
     * @param itemId
     */
    public void pasteItem(Long itemId);

    /**
     * Adds a new item to record
     * @param product
     */
    public void replaceItem(Product product);

    /**
     * Updates an item
     * @param form
     * @throws ClientException if quantity is null or 0
     */
    public void updateItem(Form form) throws ClientException;

    /**
     * Indicates if this record view provides an item changed history
     * @return true if itemChangedHistory is not empty
     */
    public boolean isItemChangedHistoryAvailable();

    /**
     * Provides the item changed history
     * @return itemChangedHistory or empty list if not available
     */
    public List<ItemChangedInfo> getItemChangedHistory();

    /**
     * Moves down an item
     * @param id of the item
     */
    public void moveDownItem(Long id);

    /**
     * Moves up an item
     * @param id of the item
     */
    public void moveUpItem(Long id);

    /**
     * Unlocks locked items if record and config supports
     * this and current user has required permissions
     * @throws ClientException if unlock not possible
     */
    public void unlockItems() throws ClientException;

    /**
     * Toggles ignoreInDocument status for an item
     * @param id the primary key of the item to change
     */
    public void changeItemIgnoreInDocument(Long id);

    /**
     * Indicates if this record view provides a status history
     * @return true if so
     */
    public boolean isProvidingStatusHistory();

    /**
     * Provides the status history of the record
     * @return statusHistory
     */
    public List<RecordStatusHistory> getStatusHistory();

    /**
     * Provides a list of all currently available record info texts
     * @return infos
     */
    public List<Option> getRecordInfos();

    /**
     * Removes all infos
     */
    public void clearInfos();

    /**
     * Removes all infos and sets defaults
     */
    public void setDefaultInfos() throws PermissionException;

    /**
     * Moves an info item down in listing
     * @param id
     */
    public void moveInfoDown(Long id);

    /**
     * Moves an info item up in listing
     * @param id
     */
    public void moveInfoUp(Long id);

    /**
     * Adds an info
     * @param id
     * @throws PermissionException if user has no permissions
     */
    public void addInfo(Long id) throws PermissionException;

    /**
     * Removes an info
     * @param id
     */
    public void removeInfo(Long id);

    /**
     * Indicates if record is deletable by checking permissions and status 
     * @return true if record is deletable
     */
    public boolean isRecordDeletable();

    /**
     * Indicates that view is in confirm delete mode
     * @return true if enabled
     */
    public boolean isConfirmDelete();

    /**
     * Enables/disables confirm delete mode
     * @param confirmDelete
     */
    public void setConfirmDelete(boolean confirmDelete);

    /**
     * List of all employees whose will be provided as selection for left signature
     * @return employees
     */
    public List<Option> getSignatureLeftEmployees();

    /**
     * List of all employees whose will be provided as selection for left signature
     * @return employees
     */
    public List<Option> getSignatureRightEmployees();

    /**
     * Provides contact persons related to record contact
     * @return contactPersons
     */
    public List<Contact> getContactPersons();

    /**
     * Provides base tax rates
     * @return taxRates
     */
    public List<Double> getTaxRates();

    /**
     * Provides reduced tax rates
     * @return reducedTaxRates
     */
    public List<Double> getReducedTaxRates();

    /**
     * Provides the contact person
     * @return contactPerson
     */
    public Contact getContactPerson();

    /**
     * Indicates that default exit is blocked
     * @return exitBlocked
     */
    public boolean isExitBlocked();

    /**
     * Enables/disables default exit
     * @param exitBlocked
     */
    public void setExitBlocked(boolean exitBlocked);

    /**
     * Provides an alternate exit target when default exit is blocked
     * @return exitBlockedTarget
     */
    public String getExitBlockedTarget();

    /**
     * Sets the exit blocked target
     * @param exitBlockedTarget
     */
    public void setExitBlockedTarget(String exitBlockedTarget);

    /**
     * Indicates that description of items should be displayed or hidden
     * @return displayDescription
     */
    public boolean isDisplayDescription();

    /**
     * Enables/disables item description display
     * @param displayDescription
     */
    public void changeDisplayDescriptionState();

    /**
     * Changes related contact
     * @param contact
     * @throws ClientException
     */
    public void changeContact(ClassifiedContact contact) throws ClientException;

    /**
     * Provides all unreleased records of implementing type
     * @param target to forward to
     * @param exitTarget where we go back
     */
    public void loadUnreleased(String target, String exitTarget);

    /**
     * Indicates that view is in unreleased listing mode
     * @return true if so
     */
    public boolean isUnreleasedListMode();

    /**
     * Indicates that prices should be displayed. This is the default
     * @return priceDisplayEnabled
     */
    public boolean isPriceDisplayEnabled();

    /**
     * Indicates that price input is enabled when editing products (a delivery note may not support this for example)
     * @return priceInputEnabled
     */
    public boolean isPriceInputEnabled();

    /**
     * Indicates that view should be kept on exit rather than being destroyed from exit action
     * @return keepViewOnExit
     */
    public boolean isKeepViewOnExit();

    /**
     * Indicates that record view is sales record view
     * @return salesRecordView
     */
    public boolean isSalesRecordView();

    /**
     * Indicates that implementing views record type supports corrections
     * @return providingCorrections
     */
    public boolean isProvidingCorrections();

    /**
     * Indicates if last request was exit request.
     * @return true if previous request was exit request
     */
    public boolean isPreviousRequestExit();
    
    /**
     * Indicates if record supports external document uploads,
     * e.g. record supports switching to historical.
     * @return true if external document importable
     */
    public boolean isDocumentImportable();
    
    /**
     * Indicates custom header support for printout
     * @return true if supported
     */
    public boolean isCustomHeaderSupported();

    /**
     * Activates previousRequestExit status. The status is automatically disabled after invocation of previousRequestExit
     */
    public void previousRequestWasExit();

    /**
     * Provides all bank accounts associated with company of current record
     * @return bankAccounts
     */
    public List<BankAccount> getBankAccounts();
    
    /**
     * Privides the document of the record if supported by type and available
     * @return document or null if not uploaded or supported
     */
	public DmsDocument getDocument();

	/**
	 * Provides customized businessTemplates if available 
	 * @return businessTemplates
	 */
    public List<BusinessTemplate> getBusinessTemplates();

    public LetterType getEmbeddedTextType();

    public Letter getEmbeddedTextAbove();

    public Letter getEmbeddedTextBelow();

    public void enableEmbeddedText();

    public void disableEmbeddedText();

    public String getAccountingPermissions();

    public List<BranchOffice> getBranchOfficeList();

    public boolean isMultipleBranchsAvailable();

}
