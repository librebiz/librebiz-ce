/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 5, 2010 9:56:01 AM 
 * 
 */
package com.osserp.core.service.contacts;

import java.util.List;

import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.PrivateContact;
import com.osserp.core.contacts.PrivateContactManager;
import com.osserp.core.dao.PrivateContacts;
import com.osserp.core.service.impl.AbstractService;
import com.osserp.core.tasks.ContactUpdateSender;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PrivateContactManagerImpl extends AbstractService implements PrivateContactManager {
    private PrivateContacts privateContacts = null;
    private ContactUpdateSender contactUpdateSender = null;

    protected PrivateContactManagerImpl(PrivateContacts users, ContactUpdateSender contactUpdateSender) {
        super();
        this.privateContacts = users;
        this.contactUpdateSender = contactUpdateSender;
    }

    public PrivateContact createPrivate(DomainUser user, Contact contact, Contact company) {
        PrivateContact pc = privateContacts.createPrivateContact(user, contact, company);
        sync(pc);
        return pc;
    }

    public PrivateContact getPrivate(DomainUser user, Contact contact) {
        return privateContacts.getPrivateContact(user, contact);
    }

    public List<PrivateContact> findPrivate(DomainUser user) {
        return privateContacts.getPrivateContacts(user);
    }

    public boolean isPrivate(DomainUser user, Contact contact) {
        return privateContacts.isPrivateContact(user, contact);
    }

    public boolean isPrivateUnused(DomainUser user, Contact contact) {
        return privateContacts.isPrivateContactUnused(user, contact);
    }

    public boolean isPrivateAvailable(DomainUser user) {
        return privateContacts.isPrivateContactAvailable(user);
    }

    public void disable(PrivateContact contact) {
        contact.setUnused(true);
        save(contact);
        sync(contact);
    }

    public PrivateContact update(PrivateContact contact, String type, String info) {
        contact.update(type, info);
        save(contact);
        sync(contact);
        return contact;
    }

    public void sync(PrivateContact contact) {
        contactUpdateSender.send(contact);
    }

    public List<PrivateContact> findPrivate(Long contactId) {
        return privateContacts.getPrivateContacts(contactId);
    }

    private void save(PrivateContact contact) {
        privateContacts.save(contact);
        sync(contact);
    }
}
