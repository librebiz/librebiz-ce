/**
 *
 * Copyright (C) 2022 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License (AGPL)
 * version 3 as published by the Free Software Foundation. In accordance
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code
 * versions must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area
 * or, if not reasonably feasible for technical reason, as a top-level
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute.
 *
 */
package com.osserp.gui.sales

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ActionException
import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.dms.DocumentData
import com.osserp.core.finance.OrderManager
import com.osserp.core.finance.RecordDocument
import com.osserp.core.finance.RecordMailManager
import com.osserp.core.finance.RecordManager
import com.osserp.core.finance.RecordPrintManager
import com.osserp.core.finance.Records
import com.osserp.core.mail.MailMessageContext
import com.osserp.core.sales.SalesOrder
import com.osserp.core.sales.SalesOrderManager
import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class SalesOrderActionView extends CoreView {

    private static Logger logger = LoggerFactory.getLogger(SalesOrderActionView.class.getName())

    SalesOrder salesOrder
    List<RecordDocument> documentVersions = []
    List<String> headerSelection = [
        'salesOrderHeaderConfirmation', 'salesOrderHeader', 'salesOrderHeaderSpecification'
    ]
    Map<String, String> mailopts = [
        successTarget: '/sales/salesOrderAction/reload',
        exitTarget: '/sales/salesOrderAction/reload'
    ]
    Date customDate
    boolean preview

    SalesOrderActionView() {
        super()
        enableAutoreload()
        dependencies = [ 'salesOrderView' ]
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            preview = true
            salesOrder = env.salesOrderView?.record
            if (salesOrder) {
                documentVersions = salesOrderManager.findVersions(salesOrder)
            } else {
                logger.warn('initRequest: Did not find salesOrder')
            }
        }
    }

    DocumentData getDocument() {
        RecordDocument selected = selectedDocument
        if (selected) {
            byte[] doc = salesOrderManager.getVersion(selected)
            if (doc) {
                String filename = "${Records.createNumber(salesOrder, selected)}.pdf"
                return new DocumentData(doc, filename)
            }
        }
        logger.warn('getDocument: Did not find document version')
        return null
    }

    DocumentData getCustomDocument() {
        String selectedHeader = form.getString('customHeader')
        if (!selectedHeader) {
            selectedHeader = form.getString('selectedHeader')
            if (!selectedHeader) {
                throw new ClientException(ErrorCode.VALUES_MISSING)
            }
            selectedHeader = getLocalizedMessage(selectedHeader)
        }
        customDate = form.getDate('customDate')
        preview = form.getBoolean('preview')
        DocumentData result = recordPrintManager.createCustomPdf(
            domainEmployee, salesOrder, preview, selectedHeader, customDate)
        return result
    }

    MailMessageContext getMailMessageContext() throws ClientException {
        RecordDocument selected = selectedDocument
        if (!selected) {
            throw new ActionException()
        }
        if (exitTarget) {
            mailopts.exitTarget = exitTarget
        }
        return recordMailManager.createArchiveContext(
            domainEmployee, salesOrder, selected, mailopts)
    }

    private RecordDocument getSelectedDocument() {
        Long id = form.getLong('id')
        if (id) {
            return documentVersions.find { it.id == id }
        }
        return null
    }

    @Override
    public void reload() {
        if (salesOrder) {
            documentVersions = salesOrderManager.findVersions(salesOrder)
            preview = true
        }
    }

    Object getSalesOrderView() {
        return env[ 'salesOrderView' ]
    }

    protected SalesOrderManager getSalesOrderManager() {
        return getService(SalesOrderManager.class.getName())
    }

    protected RecordPrintManager getRecordPrintManager() {
        return getService(RecordPrintManager.class.getName())
    }

    protected RecordMailManager getRecordMailManager() {
        return getService(RecordMailManager.class.getName())
    }
}
