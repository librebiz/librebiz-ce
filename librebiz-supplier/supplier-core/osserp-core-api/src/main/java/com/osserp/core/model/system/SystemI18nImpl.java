/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 23, 2007 9:00:22 AM 
 * 
 */
package com.osserp.core.model.system;

import com.osserp.common.Option;
import com.osserp.common.beans.AbstractOption;

import com.osserp.core.system.SystemI18n;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SystemI18nImpl extends AbstractOption implements SystemI18n {

    private Long configId = null;
    private String text = null;
    private String context = null;

    protected SystemI18nImpl() {
        super();
    }

    public SystemI18nImpl(Long configId, String name, String text) {
        super(name);
        this.text = text;
        this.configId = configId;
    }

    public SystemI18nImpl(Option vo) {
        super(vo);
        if (vo instanceof SystemI18n) {
            SystemI18n i18n = (SystemI18n) vo;
            this.text = i18n.getText();
            this.context = i18n.getContext();
        }
    }

    public Long getConfigId() {
        return configId;
    }

    protected void setConfigId(Long configId) {
        this.configId = configId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    @Override
    public Object clone() {
        return new SystemI18nImpl(getId(), configId, getName(), text, context);
    }

    private SystemI18nImpl(Long id, Long configId, String name, String text, String context) {
        super(id, name);
        this.text = text;
        this.configId = configId;
        this.context = context;
    }
}
