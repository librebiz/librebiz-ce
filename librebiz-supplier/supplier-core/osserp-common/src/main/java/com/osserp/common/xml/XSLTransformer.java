/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 2, 2006 8:49:28 AM 
 * 
 */
package com.osserp.common.xml;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.fop.apps.Driver;
import org.apache.fop.apps.Options;
import org.apache.fop.image.FopImageFactory;
import org.apache.fop.messaging.MessageHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdom2.Document;
import org.jdom2.transform.JDOMSource;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public final class XSLTransformer {
    private static Logger log = LoggerFactory.getLogger(XSLTransformer.class.getName());

    public static byte[] createPdf(Document xml, Source xsl, String userConfigFile) {

        try {
            FopImageFactory.resetCache();
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            // 	Get a FOP rendering engine.
            Options options = new Options();
            if (userConfigFile != null) {
                options.loadUserconfiguration(userConfigFile);
            }
            Driver pdfDriver = new Driver();
            //org.apache.avalon.framework.logger.Logger logger = new Log4JLogger(org.apache.log4j.Logger.getLogger(XSLTransformer.class));
            pdfDriver.setLogger(AvalonLogger.getLogger(XSLTransformer.class));
            MessageHandler.setScreenLogger(AvalonLogger.getLogger(XSLTransformer.class));
            pdfDriver.setRenderer(Driver.RENDER_PDF);
            pdfDriver.setOutputStream(output);
            // Get a JAXP TrAX Transformer (please use Templates to cache stylesheets).
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer(xsl);
            // Apply XSL Transformation on the JDOM DocumentImplBean, redirecting the
            // result to the FOP rendering engine.
            transformer.transform(new JDOMSource(xml), new SAXResult(pdfDriver.getContentHandler()));
            // Send result to caller.
            return output.toByteArray();
        } catch (Throwable t) {
            log.error("createPdf() failed: " + t.toString(), t);
            throw new XSLException(t.getMessage(), t);
        }
    }
    
    
}
