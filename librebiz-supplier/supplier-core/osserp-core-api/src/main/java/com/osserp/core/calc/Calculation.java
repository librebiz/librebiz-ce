/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 9, 2005 2:57:52 PM 
 * 
 */
package com.osserp.core.calc;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;

import com.osserp.core.ItemList;
import com.osserp.core.ItemPosition;
import com.osserp.core.products.Product;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Calculation extends ItemList, PartlistReference {

    /**
     * Provides the responsible calculator class
     * @return calculatorClass
     */
    String getCalculatorClass();

    /**
     * Sets the responsible calculator class
     * @param calculatorClass
     */
    void setCalculatorClass(String calculatorClass);

    /**
     * Provides all option positions with products
     * @return options
     */
    List<ItemPosition> getOptions();

    /**
     * Fetches an option position
     * @param positionId
     * @return optionPosition
     */
    ItemPosition getOptionPosition(Long positionId);

    /**
     * Indicates that options are available
     * @return optionAvailable, true if at least one item exists under any option
     */
    boolean isOptionAvailable();

    /**
     * Adds a new item
     * @param position
     * @param product
     * @param customName
     * @param quantity
     * @param price
     * @param priceDate
     * @param partnerPrice
     * @param partnerPriceEditable
     * @param purchasePrice
     * @param taxRate
     * @param note
     * @param externalId
     * @throws ClientException if any required value missing or invalid
     */
    void addOptionItem(
            Long position,
            Product product,
            String customName,
            Double quantity,
            BigDecimal price,
            Date priceDate,
            BigDecimal partnerPrice,
            boolean partnerPriceEditable,
            BigDecimal purchasePrice,
            Double taxRate,
            String note,
            Long externalId)
            throws ClientException;

    /**
     * Updates an existing option item
     * @param itemId
     * @param customName
     * @param quantity
     * @param price
     * @param note
     * @throws ClientException if quantity is not set
     */
    void updateOptionItem(
            Long itemId,
            String customName,
            Double quantity,
            BigDecimal price,
            String note) throws ClientException;

    /**
     * Replaces an product on an existing option item
     * @param itemId
     * @param product
     * @param customName
     * @param quantity
     * @param price
     * @param note
     * @throws ClientException if validation failed
     */
    void updateOptionItem(
            Long itemId,
            Product product,
            String customName,
            Double quantity,
            BigDecimal price,
            String note) throws ClientException;

    /**
     * Moves an item up
     * @param itemId
     */
    void moveUpOptionItem(Long itemId);

    /**
     * Moves an item down
     * @param itemId to move
     */
    void moveDownOptionItem(Long itemId);

    /**
     * Removes an optional item from related position
     * @param itemId
     */
    void removeOptionItem(Long itemId);

    /**
     * Adds a discount
     * @param itemToDiscount
     * @param discount
     * @throws ClientException if discount not possible
     */
    void addDiscount(Long itemToDiscount, Product discount) throws ClientException;

    /**
     * Updates all partner prices with current partner prices configured in product. This method must only be used in request context.
     */
    void refreshPartnerPrices();

    /**
     * Updates the price display state of an item
     * @param itemId
     * @throws ClientException if price should be disabled and item does not support this
     */
    void updatePriceDisplayState(Long itemId) throws ClientException;

    /**
     * Enables or disables the price display state of all items
     * @param enable
     * @throws ClientException if price update not supported
     */
    void updatePriceDisplayState(boolean enable) throws ClientException;

    /**
     * Updates the reference
     * @param contextName
     * @param reference
     */
    void updateReference(String contextName, Long reference);

    /**
     * Indicates that calculation is initial
     * @return initial
     */
    boolean isInitial();

    /**
     * Indicates that a package is available
     * @return packageAvailable
     */
    boolean isPackageAvailable();

    /**
     * Updates the package price and total price if package exists. This method is required by item based calculators.
     * @param partnerPrice
     * @param newPrice
     */
    void updatePackagePrice(BigDecimal partnerPrice, BigDecimal newPrice);

    /**
     * Indicates if package price was overridden (updated manually)
     * @return packagePriceOverridden
     */
    boolean isPackagePriceOverridden();

    /**
     * The plant capacity
     * @return plantCapacity
     */
    Double getPlantCapacity();

    /**
     * Validates the calculation against current validation rules
     * @throws ClientException if any existing item does not match current requirements
     */
    void validateCopyAttempt() throws ClientException;

    /**
     * Indicates that project plan documents available for this calculation
     * @return planAvailable
     */
    boolean isPlanAvailable();

    /**
     * Set that project plan documents are available for this calculation
     * @param planAvailable
     */
    void setPlanAvailable(boolean planAvailable);

    /**
     * Provides the minimal margin
     * @return minimalMargin
     */
    Double getMinimalMargin();

    /**
     * Provides the target margin
     * @return targetMargin
     */
    Double getTargetMargin();

    /**
     * Indicates that sales price is locked (calculators should not override an existing salesPrice)
     * @return salesPriceLocked
     */
    boolean isSalesPriceLocked();

    /**
     * Locks/unlocks sales price
     * @param salesPriceLocked
     */
    void setSalesPriceLocked(boolean salesPriceLocked);

    /**
     * Logs all positions and items
     * @return loggingString
     */
    String getLogValues();

    /**
     * Adds an template
     * @param template
     * @param wholeSale
     * @param reducedTax
     * @param normalTax
     * @return
     */
    Long addTemplate(CalculationTemplate template, boolean wholeSale, Double reducedTax, Double normalTax);

    /**
     * Removes a template
     * @param template
     */
    void removeTemplate(CalculationTemplate template);
}
