/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 20, 2008 8:14:44 AM 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.osserp.common.dao.AbstractRowMapper;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
abstract class BaseRecordDisplayRowMapper extends AbstractRowMapper {

    public final Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        Map<String, Object> map = createMap(rs);
        populate(rs, map);
        return map;
    }
    
    protected abstract void populate(ResultSet rs, Map<String, Object> map);
    
    protected Map<String, Object> createMap(ResultSet rs) {
        Map<String, Object> map = new HashMap<>();
        addLong(map, "id", rs, "id");
        addLong(map, "type", rs, "type_id");
        addLong(map, "company", rs, "company_id");
        addLong(map, "reference", rs, "reference_id");
        addLong(map, "contactId", rs, "contact_id");
        addLong(map, "contactType", rs, "contact_type");
        addLong(map, "status", rs, "status");
        addDate(map, "created", rs, "created");
        addLong(map, "createdBy", rs, "created_by");
        addBoolean(map, "taxFree", rs, "tax_free");
        addLong(map, "taxFreeId", rs, "tax_free_id");
        addDouble(map, "netAmount", rs, "net_amount");
        addDouble(map, "taxAmount", rs, "tax_amount");
        addDouble(map, "reducedTaxAmount", rs, "reduced_tax_amount");
        addDouble(map, "grossAmount", rs, "gross_amount");
        addLong(map, "currency", rs, "currency_id");
        addBoolean(map, "internal", rs, "internal");
        addString(map, "lastname", rs, "lastname");
        addString(map, "firstname", rs, "firstname");
        addString(map, "street", rs, "street");
        addString(map, "zipcode", rs, "zipcode");
        addString(map, "city", rs, "city");
        addLong(map, "sales", rs, "sales_id");
        addDate(map, "maturity", rs, "maturity");
        return map;
    }
}
