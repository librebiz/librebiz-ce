/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 9, 2011 11:34:36 AM 
 * 
 */
package com.osserp.gui.admin.events

import com.osserp.core.events.EventConfigManager
import com.osserp.gui.CoreView

/**
 *
 * @author eh <eh@osserp.com>
 *
 */
class PoolMembersView extends CoreView {

    PoolMembersView() {
        super()
        enablePopupView()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        def poolId = form.getLong('id')
        if (poolId) {
            bean = eventConfigManager.findPool(poolId)
        }
    }

    private EventConfigManager getEventConfigManager() {
        getService(EventConfigManager.class.getName())
    }
}

