/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 22, 2016 
 * 
 */
package com.osserp.gui.sales

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode

import com.osserp.core.finance.BillingType
import com.osserp.core.finance.BillingTypeSearch
import com.osserp.core.finance.BookingType
import com.osserp.core.finance.Cancellation
import com.osserp.core.finance.Invoice
import com.osserp.core.finance.Payment
import com.osserp.core.sales.SalesCreditNoteManager
import com.osserp.core.sales.SalesInvoicePaymentManager
import com.osserp.core.users.DomainUser

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class SalesInvoicePaymentView extends AbstractSalesPaymentView {

    private static Logger logger = LoggerFactory.getLogger(SalesInvoicePaymentView.class.getName())

    List<BookingType> creditNoteBookingTypes = []
	private String[] customNumberPerms = ['record_manual_number_input']
	boolean customNumberEnabled = false

    SalesInvoicePaymentView() {
        super()
        providesEditMode()
        dependencies = ['salesInvoiceView', 'salesBillingView']
    }

    @Override
    protected String getDefaultExitLink() {
        "/salesInvoiceDisplay.do?id=${lastRecordId}"
    }

    protected String getPaymentAwareRecordViewName() {
        'salesInvoiceView'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest && record) {
            paymentTypes = billingTypeSearch.getPaymentTypes(domainUser, record)
            customNumberEnabled = getDomainUser().isPermissionGrant(customNumberPerms)
        }
    }

    @Override
    void save() {
        if (clearDecimalAmountMode) {
            record = salesInvoicePaymentManager.clearDecimalAmount(domainUser, record)
            clearDecimalAmountMode = false
        } else if (paymentDeleteMode) {
            Long id = getLong('id')
            if (id) {
                Payment payment = record.payments.find { it.id == id }
                if (payment) {
                    record = salesInvoicePaymentManager.deletePayment(domainUser, record, payment)
                }
            }
            paymentDeleteMode = false
        } else if (selectedPaymentType?.cashDiscount) {
            BigDecimal amount = getDecimal('amount')
            boolean cashDiscount = getBoolean('cashDiscount')
            record = salesInvoicePaymentManager.addDiscount(
                domainUser,
                record,
                selectedPaymentType.id,
                amount,
                cashDiscount)
        } else {
            Date datePaid = getDate('datePaid')
            BigDecimal amount = getDecimal('amount')
            boolean paid = getBoolean('paid')
            boolean cashDiscount = getBoolean('cashDiscount') 
            Long bankAccountId = getLong('bankAccountId')
            if (customPaymentMode) {
                record = salesInvoicePaymentManager.addCustomPayment(
                    domainUser,
                    record,
                    amount,
                    datePaid,
                    paid,
                    getString('customHeader'),
                    getString('note'),
                    bankAccountId)
                customPaymentMode = false
            } else if (selectedPaymentType) {
                record = salesInvoicePaymentManager.addPayment(
                    domainUser,
                    record,
                    bankAccountId,
                    selectedPaymentType.id,
                    amount,
                    datePaid,
                    paid,
                    cashDiscount)
            }
        }
    }

    void selectPaymentType() {
        cancellationMode = false
        clearDecimalAmountMode = false
        creditNoteMode = false
        customPaymentMode = false
        recordCloseMode = false
        paymentDeleteMode = false
        partialPayment = false

        Long id = getLong('id')
        if (!id) {
            selectedPaymentType = null

        } else if (100L == id) {
            recordCloseMode = true

        } else if (-1L == id) {
            paymentDeleteMode = true

        } else if (BillingType.TYPE_CREDIT_NOTE == id) {
            creditNoteMode = true
            creditNoteBookingTypes = salesCreditNoteManager.bookingTypes

        } else if (BillingType.TYPE_CANCELLATION == id) {
            cancellationMode = true

        } else {
            selectedPaymentType = paymentTypes.find { it.id == id }
            if (selectedPaymentType) {
                if (BillingType.TYPE_PARTIAL_PAYMENT == id) {
                    partialPayment = true
                } else if (BillingType.TYPE_CLEARING == id) {
                    clearDecimalAmountMode = true
                } else if (BillingType.TYPE_CUSTOM_PAYMENT == id) {
                    customPaymentMode = true
                }
            }
        }
        if (selectedPaymentType) {
            enableEditMode()
        } else {
            disableEditMode()
        }
    }

    @Override
    public void disableEditMode() {
        super.disableEditMode()
        selectedPaymentType = null
    }

    String createCreditNote() {
        Long id = getLong('id')
        if (!id) {
            throw new ClientException(ErrorCode.TYPE_MISSING)
        }
        resultingRecord = salesInvoicePaymentManager.createCreditNote(
            domainUser, record, id, getBoolean('copyItems'))
        creditNoteMode = false
        return "/salesCreditNote.do?method=display&id=${resultingRecord.id}"
    }

    String createCancellation() throws ClientException {
        resultingRecord = salesInvoicePaymentManager.createCancellation(
            domainUser,
            record,
            form.getDate('cancelDate'),
            form.getLong('recordNumber'))
        cancellationMode = false
        if (resultingRecord instanceof Cancellation) {
            return "/salesCancellation.do?method=forward&id=${resultingRecord.id}&canceledRecordView=salesInvoiceView"
        }
        return defaultExitLink
    }

    String setPaid() {
        record = salesInvoicePaymentManager.setAsPaid(domainUser, record)
        recordCloseMode = false
        return exitTarget
    }

    protected SalesInvoicePaymentManager getSalesInvoicePaymentManager() {
        getService(SalesInvoicePaymentManager.class.getName())
    }

    protected SalesCreditNoteManager getSalesCreditNoteManager() {
        getService(SalesCreditNoteManager.class.getName())
    }

    protected BillingTypeSearch getBillingTypeSearch() {
        getService(BillingTypeSearch.class.getName())
    }
}
