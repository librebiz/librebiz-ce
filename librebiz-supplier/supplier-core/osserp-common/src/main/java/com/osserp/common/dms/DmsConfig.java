/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 14-Feb-2004 12:22:32 
 * 
 */
package com.osserp.common.dms;

import com.osserp.common.Option;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface DmsConfig extends Option {
    
    static final String CUSTOM_TEMPLATE_PATH_PROPERTY = "documentCustomTemplatePath";
    
    static final Long DOCUMENTS = 1L;
    static final Long TEMPLATES = 2L;

    /**
     * Provides the root directory of the dms including path
     * @return root directory
     */
    String getRoot();

    /**
     * Sets the root directory and path
     * @param root directory
     */
    void setRoot(String root);

    /**
     * Provides the direct download url. Note: Direct invocation of the url 
     * should not be allowed for documents with sensible content or public 
     * accessible application deployments.
     * Use printUrl to access document data with authorized accounts or public 
     * access if the controller implementation supports this.  
     * @return url document download
     */
    String getUrl();

    /**
     * Sets the direct download url.
     * @param url
     */
    void setUrl(String url);

    /**
     * Provides the url of the print methods.
     * @return print url
     */
    String getPrintUrl();
    
    /**
     * Sets the print url
     * @param printUrl
     */
    void setPrintUrl(String printUrl);

}
