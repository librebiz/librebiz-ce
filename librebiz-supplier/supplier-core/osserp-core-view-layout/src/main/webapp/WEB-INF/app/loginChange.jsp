<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${sessionScope.portalView}"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="title"><o:displayTitle/></tiles:put>
	<tiles:put name="headline">
		<fmt:message key="changeAccountData"/>
	</tiles:put>
	<tiles:put name="headline_right">
		<v:link url="/index"><o:img name="homeIcon"/></v:link>
	</tiles:put>

	<tiles:put name="content" type="string">
        <div class="content-area" id="loginChangeContent">
            <div class="row">
                <v:form name="userForm" url="/login/authenticationUpdate">
                    <c:set var="userUid" scope="request" value="${view.user.ldapUid}"/>
                    <c:import url="${viewdir}/shared/_passwordChangeForm.jsp" />
                </v:form>
            </div>
        </div>
	</tiles:put>
</tiles:insert>
