/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 1, 2010 1:10:00 PM 
 * 
 */
package com.osserp.core.telephone;

import java.util.List;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public interface TelephoneConfigurationManager {

    /**
     * Creates a new telephoneConfiguration
     * @param employeeId
     * @param uid
     * @param internal
     * @param accountCode
     * @param displayName
     * @param emailAddress
     * @param telephone
     * @return telephoneConfiguration
     */
    TelephoneConfiguration create(Long employeeId, String uid, String internal, String accountCode, String displayName, String emailAddress, Telephone telephone);

    /**
     * Creates a new telephoneConfiguration
     * @param accountCode
     * @param displayName
     * @param emailAddress
     * @return telephoneConfiguration
     */
    TelephoneConfiguration create(String accountCode, String displayName, String emailAddress);

    /**
     * Provides all telephoneConfigurations without templates
     * @return all telephone configurations
     */
    List<TelephoneConfiguration> getAllTelephoneConfigurations();

    /**
     * Provides all telephoneConfiguration templates
     * @return all telephone configuration templates
     */
    List<TelephoneConfiguration> getAllTelephoneConfigurationsTemplates();

    /**
     * Tries to find a telephoneConfiguration by id
     * @param id
     * @return telephoneConfiguration or null if not found
     */
    TelephoneConfiguration find(Long id);

    /**
     * Tries to find telephoneConfigurations by telephone reference id
     * @param id
     * @return configurations or null if not found
     */
    List<TelephoneConfiguration> findByReference(Long id);

    /**
     * Tries to find a telephoneConfiguration by employee id
     * @param employeeId
     * @return telephoneConfiguration or null if not found
     */
    TelephoneConfiguration findByEmployeeId(Long employeeId);

    /**
     * Tries to find telephoneConfigurations by telephone branch id
     * @param branchId
     * @return configurations or null if not found
     */
    List<TelephoneConfiguration> findByBranchId(Long branchId);

    /**
     * Tries to find a telephoneConfiguration by telephone branch id and internal
     * @param internal
     * @param branchId
     * @return telephoneConfiguration or null if not found
     */
    TelephoneConfiguration findByReference(String internal, Long branchId);

    /**
     * Tries to find telephoneConfigurations by telephone group id
     * @param groupId
     * @return configurations or null if not found
     */
    List<TelephoneConfiguration> findByTelephoneGroup(Long groupId);

    /**
     * Tries to find a telephoneConfiguration by branchId and employeeId
     * @param employeeId
     * @param branchId
     * @return telephoneConfiguration or null if not found
     */
    TelephoneConfiguration find(Long employeeId, Long branchId);

    /**
     * Tries to find telephoneConfigurations
     * @param branchId
     * @param includeEmployeeTelephoneSystems
     * @param onlyTelephoneExchanges
     * @return configurations or null if not found
     */
    List<TelephoneConfiguration> search(Long branchId, boolean includeEmployeeTelephoneSystems, boolean onlyTelephoneExchanges);

    /**
     * Tries to update Internal
     * @param userId
     * @param employeeId
     * @param internal
     */
    void resetInternal(Long userId, Long employeeId, String value);

    /**
     * Tries to update Internal
     * @param userId
     * @param id
     * @param internal
     */
    void updateInternal(Long userId, Long id, String internal);

    /**
     * Tries to update AccountCode
     * @param userId
     * @param id
     * @param accountCode
     */
    void updateAccountCode(Long userId, Long id, String accountCode);

    /**
     * Tries to update DisplayName
     * @param userId
     * @param id
     * @param displayName
     */
    void updateDisplayName(Long userId, Long id, String displayName);

    /**
     * Tries to update EmailAddress
     * @param userId
     * @param id
     * @param emailAddress
     */
    void updateEmailAddress(Long userId, Long id, String emailAddress);

    /**
     * Tries to update Telephone
     * @param userId
     * @param id
     * @param telephoneId
     */
    void updateTelephone(Long userId, Long id, Long telephoneId);

    /**
     * Tries to update VoiceMailEnabled
     * @param userId
     * @param id
     * @param voiceMailEnabled
     */
    void updateVoiceMailEnabled(Long userId, Long id, boolean voiceMailEnabled);

    /**
     * Tries to update VoiceMailDeleteAfterEmailSend
     * @param userId
     * @param id
     * @param voiceMailDeleteAfterEmailSend
     */
    void updateVoiceMailDeleteAfterEmailSend(Long userId, Long id, boolean voiceMailDeleteAfterEmailSend);

    /**
     * Tries to update VoiceMailSendAsEmail
     * @param userId
     * @param id
     * @param voiceMailSendAsEmail
     */
    void updateVoiceMailSendAsEmail(Long userId, Long id, boolean voiceMailSendAsEmail);

    /**
     * Tries to update TelephoneExchange
     * @param userId
     * @param id
     * @param telephoneExchange
     */
    void updateTelephoneExchange(Long userId, Long id, boolean telephoneExchange);

    /**
     * Tries to update BusyOnBusy
     * @param userId
     * @param id
     * @param busyOnBusy
     */
    void updateBusyOnBusy(Long userId, Long id, boolean busyOnBusy);

    /**
     * Tries to update Available
     * @param userId
     * @param id
     * @param available
     */
    void updateAvailable(Long userId, Long id, boolean available);

    /**
     * Tries to update VoiceMailDelaySeconds
     * @param userId
     * @param id
     * @param voiceMailDelaySeconds
     */
    void updateVoiceMailDelaySeconds(Long userId, Long id, Integer voiceMailDelaySeconds);

    /**
     * Tries to update VoiceMailEnabledIfBusy
     * @param userId
     * @param id
     * @param voiceMailEnabledIfBusy
     */
    void updateVoiceMailEnabledIfBusy(Long userId, Long id, boolean voiceMailEnabledIfBusy);

    /**
     * Tries to update VoiceMailEnabledIfUnavailable
     * @param userId
     * @param id
     * @param voiceMailEnabledIfUnavailable
     */
    void updateVoiceMailEnabledIfUnavailable(Long userId, Long id, boolean voiceMailEnabledIfUnavailable);

    /**
     * Tries to update ParallelCallTargetPhoneNumber
     * @param userId
     * @param id
     * @param parallelCallTargetPhoneNumber
     */
    void updateParallelCallTargetPhoneNumber(Long userId, Long id, String parallelCallTargetPhoneNumber);

    /**
     * Tries to update ParallelCallEnabled
     * @param userId
     * @param id
     * @param parallelCallEnabled
     */
    void updateParallelCallEnabled(Long userId, Long id, boolean parallelCallEnabled);

    /**
     * Tries to update ParallelCallSetDelaySeconds
     * @param userId
     * @param id
     * @param parallelCallSetDelaySeconds
     */
    void updateParallelCallSetDelaySeconds(Long userId, Long id, Integer parallelCallSetDelaySeconds);

    /**
     * Tries to update CallForwardTargetPhoneNumber
     * @param userId
     * @param id
     * @param callForwardTargetPhoneNumber
     */
    void updateCallForwardTargetPhoneNumber(Long userId, Long id, String callForwardTargetPhoneNumber);

    /**
     * Tries to update CallForwardDelaySeconds
     * @param userId
     * @param id
     * @param callForwardDelaySeconds
     */
    void updateCallForwardDelaySeconds(Long userId, Long id, Integer callForwardDelaySeconds);

    /**
     * Tries to update CallForwardEnabled
     * @param userId
     * @param id
     * @param callForwardEnabled
     */
    void updateCallForwardEnabled(Long userId, Long id, boolean callForwardEnabled);

    /**
     * Tries to update CallForwardIfBusyTargetPhoneNumber
     * @param userId
     * @param id
     * @param callForwardIfBusyTargetPhoneNumber
     */
    void updateCallForwardIfBusyTargetPhoneNumber(Long userId, Long id, String callForwardIfBusyTargetPhoneNumber);

    /**
     * Tries to update CallForwardIfBusyEnabled
     * @param userId
     * @param id
     * @param callForwardIfBusyEnabled
     */
    void updateCallForwardIfBusyEnabled(Long userId, Long id, boolean callForwardIfBusyEnabled);

    /**
     * Tries to update TelephoneGroup
     * @param userId
     * @param id
     * @param telephoneGroupId
     */
    void updateTelephoneGroup(Long userId, Long id, Long telephoneGroupId);
}
