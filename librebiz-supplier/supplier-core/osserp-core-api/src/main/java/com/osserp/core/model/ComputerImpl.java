/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 21, 2007 8:38:33 AM 
 * 
 */
package com.osserp.core.model;

import com.osserp.common.beans.AbstractOption;
import com.osserp.core.clients.Computer;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ComputerImpl extends AbstractOption implements Computer {

    private Long workgroup = null;
    private String workgroupName = null;

    private String type = null;
    private String cpu = null;
    private Integer ram = null;
    private Integer ramInfo = null;
    private Integer hdd = null;
    private String hddInfo = null;
    private String networkAddress = null;
    private Integer networkSpeed = null;

    private Long branchId = null;
    private Long employeeId = null;

    protected ComputerImpl() {
        super();
    }

    public Long getBranchId() {
        return branchId;
    }

    public void setBranchId(Long branchId) {
        this.branchId = branchId;
    }

    public String getCpu() {
        return cpu;
    }

    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public Integer getHdd() {
        return hdd;
    }

    public void setHdd(Integer hdd) {
        this.hdd = hdd;
    }

    public String getHddInfo() {
        return hddInfo;
    }

    public void setHddInfo(String hddInfo) {
        this.hddInfo = hddInfo;
    }

    public String getNetworkAddress() {
        return networkAddress;
    }

    public void setNetworkAddress(String networkAddress) {
        this.networkAddress = networkAddress;
    }

    public Integer getNetworkSpeed() {
        return networkSpeed;
    }

    public void setNetworkSpeed(Integer networkSpeed) {
        this.networkSpeed = networkSpeed;
    }

    public Integer getRam() {
        return ram;
    }

    public void setRam(Integer ram) {
        this.ram = ram;
    }

    public Integer getRamInfo() {
        return ramInfo;
    }

    public void setRamInfo(Integer ramInfo) {
        this.ramInfo = ramInfo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getWorkgroup() {
        return workgroup;
    }

    public void setWorkgroup(Long workgroup) {
        this.workgroup = workgroup;
    }

    public String getWorkgroupName() {
        return workgroupName;
    }

    public void setWorkgroupName(String workgroupName) {
        this.workgroupName = workgroupName;
    }
}
