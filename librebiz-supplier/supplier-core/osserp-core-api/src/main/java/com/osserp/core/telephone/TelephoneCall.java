/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Dec 13, 2010 1:14:27 PM 
 * 
 */
package com.osserp.core.telephone;

import java.util.Date;
import java.util.List;

import com.osserp.common.Entity;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public interface TelephoneCall extends Entity {

    /**
     * Returns the telephoneSystem
     * @return telephoneSystem
     */
    Long getTelephoneSystem();

    /**
     * Start of call (just dialed)
     * @return the start of this call
     */
    Date getStartedAt();

    /**
     * The call was connected (bridged) to the other channel at this Date
     * @return date of call establishment
     */
    Date getEstablishedAt();

    /**
     * Call endet at this Date
     * @return the end
     */
    Date getEndedAt();

    /**
     * How long did they talk? (difference between establishment and end date)
     * @return the talk duration
     */
    Integer getDuration();

    /**
     * Normalized phone number of destination
     * @return the phone number
     */
    Long getDestinationPhoneNumber();

    /**
     * User id of destination
     * @return uid or null if not internal
     */
    String getDestinationUid();

    /**
     * Name to display for destination
     * @return the name or readable phone number of destination
     */
    String getDestinationDisplayName();

    /**
     * Sets the name to display for destination
     * @param destinationDisplayName
     */
    void setDestinationDisplayName(String destinationDisplayName);

    /**
     * Own phone number
     * @return normalized phone number
     */
    Long getPhoneNumber();

    /**
     * Own UID
     * @return the uid
     */
    String getUid();

    /**
     * Own name to display
     * @return name to display
     */
    String getDisplayName();

    /**
     * @return true for incoming calls
     */
    boolean getIncoming();

    /**
     * @return true for clickToCall calls
     */
    boolean getClickToCall();

    /**
     * @return true for internal calls
     */
    boolean getInternal();

    /**
     * @return callContacts or emtpy list
     */
    List<TelephoneCallContact> getCallContacts();
}
