/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 29, 2008 9:17:18 AM 
 * 
 */
package com.osserp.core.service.sales;

import java.util.Iterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.service.Locator;

import com.osserp.core.BusinessCase;
import com.osserp.core.calc.Calculation;
import com.osserp.core.dao.Calculations;
import com.osserp.core.dao.records.SalesRecordQueries;
import com.osserp.core.finance.CalculationAwareRecord;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordSearch;
import com.osserp.core.sales.Sales;
import com.osserp.core.sales.SalesRevenue;
import com.osserp.core.sales.SalesRevenueManager;
import com.osserp.core.sales.SalesSearch;
import com.osserp.core.service.impl.AbstractService;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesRevenueManagerImpl extends AbstractService implements SalesRevenueManager {
    private static Logger log = LoggerFactory.getLogger(SalesRevenueManagerImpl.class.getName());

    private Locator locator = null;
    private Calculations calculations = null;
    private RecordSearch recordSearch = null;
    private SalesSearch salesSearch = null;
    private Map<String, String> calculatorServices = new java.util.HashMap<String, String>();
    private String defaultCalculatorName = null;

    public SalesRevenueManagerImpl(
            Calculations calculations,
            RecordSearch recordSearch,
            SalesSearch salesSearch,
            SalesRecordQueries salesQueries,
            Locator locator,
            Map<String, String> calculatorServices,
            String defaultCalculatorName) {
        super();
        this.calculations = calculations;
        this.recordSearch = recordSearch;
        this.salesSearch = salesSearch;
        this.locator = locator;
        this.calculatorServices = calculatorServices;
        this.defaultCalculatorName = defaultCalculatorName;
    }

    public void reloadConfigs() {
        for (Iterator<String> i = calculatorServices.keySet().iterator(); i.hasNext();) {
            String calculatorName = i.next();
            try {
                SalesRevenueCalculator calculator = (SalesRevenueCalculator) locator.lookupService(calculatorServices.get(calculatorName));
                calculator.reloadConfig();
            } catch (Exception e) {
                log.warn("reloadConfigs() failed [calculatorName=" + calculatorName + ", message=" + e.getMessage() + "]");
            }
        }
    }

    public SalesRevenue getRevenue(Long id) {
        if (calculations.exists(id)) {
            return createByCalculation(id);
        }
        Record record = recordSearch.findSales(id);
        if (record == null) {
            log.warn("getRevenue() invoked with invalid or unreachable record id value [id=" + id + "]");
            return null;
        }
        BusinessCase businessCase = salesSearch.findBusinessCase(record);
        if (businessCase == null) {
            log.warn("getRevenue() invoked with invalid record type [id=" + record.getId()
                    + ", type=" + record.getType().getId() + "]");
            return null;
        }
        return create(businessCase, record);
    }

    private SalesRevenue createByCalculation(Long id) {
        if (log.isDebugEnabled()) {
            log.debug("createByCalculation() invoked [id=" + id + "]");
        }
        try {
            Calculation calculation = (Calculation) calculations.load(id);
            BusinessCase businessCase = salesSearch.findBusinessCase(calculation.getReference());
            return create(businessCase, calculation);
        } catch (Exception e) {
            return null;
        }
    }

    public SalesRevenue getRevenue(Sales sales) {
        Record record = salesSearch.findOrder(sales);
        return create(sales, record);
    }

    protected SalesRevenue create(BusinessCase businessCase, Record record) {
        String calculatorName = defaultCalculatorName;
        if (record instanceof CalculationAwareRecord) {
            CalculationAwareRecord crecord = (CalculationAwareRecord) record;
            if (crecord.getCalculatorName() != null) {
                calculatorName = crecord.getCalculatorName();
                if (log.isDebugEnabled()) {
                    log.debug("create() calculator name assigned by record [businessCase=" + businessCase.getPrimaryKey()
                            + ", calculatorName=" + calculatorName + "]");
                }
            } else if (businessCase.getType().getCalculatorName() != null) {
                calculatorName = businessCase.getType().getCalculatorName();
                if (log.isDebugEnabled()) {
                    log.debug("create() calculator name assigned by businessType [businessCase=" + businessCase.getPrimaryKey()
                            + ", calculatorName=" + calculatorName + "]");
                }

            } else if (log.isDebugEnabled()) {
                log.debug("create() using default calculator name [businessCase=" + businessCase.getPrimaryKey()
                        + ", calculatorName=" + calculatorName + "]");
            }
        }
        SalesRevenueCalculator calculator = (SalesRevenueCalculator) locator.lookupService(calculatorServices.get(calculatorName));
        if (calculator != null) {
            if (log.isDebugEnabled()) {
                log.debug("create() calculator found [class=" + calculator.getClass().getName() + "]");
            }
            return calculator.create(businessCase, record);
        }
        log.warn("create() missing calculator [name=" + calculatorName + "]");
        return null;
    }

    protected SalesRevenue create(BusinessCase businessCase, Calculation calculation) {
        String calculatorName = (calculation.getCalculatorClass() == null) ? defaultCalculatorName : calculation.getCalculatorClass();
        if (log.isDebugEnabled()) {
            log.debug("create() calculator name assigned [businessCase=" + businessCase.getPrimaryKey()
                    + ", calculatorName=" + calculatorName + "]");
        }
        SalesRevenueCalculator calculator = (SalesRevenueCalculator) locator.lookupService(calculatorServices.get(calculatorName));
        if (calculator != null) {
            if (log.isDebugEnabled()) {
                log.debug("create() calculator found [class=" + calculator.getClass().getName() + "]");
            }
            return calculator.create(businessCase, calculation);
        }
        log.warn("create() missing calculator [name=" + calculatorName + "]");
        return null;
    }
    
    public void checkDiscount(DomainUser user, Record record, BusinessCase businessCase) throws ClientException {
        String calculatorName = null;
        if (record instanceof CalculationAwareRecord) {
            CalculationAwareRecord car = (CalculationAwareRecord) record;
            calculatorName = car.getCalculatorName();
            if (isSet(calculatorName)) {
                SalesRevenueCalculator calculator = (SalesRevenueCalculator) locator.lookupService(
                        calculatorServices.get(calculatorName));
                if (calculator != null) {
                    if (log.isDebugEnabled()) {
                        log.debug("checkDiscount() calculator found [class=" + calculator.getClass().getName() + "]");
                    }
                    calculator.checkDiscount(user, car, businessCase);
                }
            }
        }
    }

}
