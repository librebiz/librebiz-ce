<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<c:set var="record" value="${sessionScope.recordView.record}" />
<table class="recordSummary">
    <tr>
        <td class="phead">
            <span>
                <fmt:message key="amount"/>
                <span class="desc">
                    (<fmt:message key="netTurnoverTaxGrossBegin"/>
                    <span> <o:number value="${record.amounts.taxRate}" format="tax"/>%</span>
                    <c:if test="${record.amounts.reducedTaxAmount > 0}">
                    <span> / </span><o:number value="${record.amounts.reducedTaxRate}" format="tax"/>%
                    </c:if>
                    <span> <fmt:message key="netTurnoverTaxGrossEnd"/></span>)
                </span>
             </span>
        </td>
        <c:choose>
            <c:when test="${record.taxFree}">
                <td class="pnet"><o:number value="${record.amounts.amount}" format="currency" /></td>
                <td class="ptax"><o:number value="0.00" format="currency" /></td>
                <td class="pamount"><o:number value="${record.amounts.amount}" format="currency" /></td>
            </c:when>
            <c:otherwise>
                <td class="pnet"><o:number value="${record.amounts.amount}" format="currency" /></td>
                <td class="ptax"><o:number value="${record.amounts.taxAmount + record.amounts.reducedTaxAmount}" format="currency" /></td>
                <td class="pamount"><o:number value="${record.amounts.grossAmount}" format="currency" /></td>
            </c:otherwise>
        </c:choose>
    </tr>
    <c:if test="${!empty record.payments}">
        <tr>
            <td colspan="4" class="pspace"></td>
        </tr>
        <c:forEach var="payment" items="${record.payments}" varStatus="s">
            <tr>
                <c:choose>
                    <c:when test="${s.index == 0}">
                        <td colspan="2" class="payment"><fmt:message key="payments" /></td>
                    </c:when>
                    <c:otherwise>
                        <td colspan="2" class="payment"></td>
                    </c:otherwise>
                </c:choose>
                <td class="pdate" style="text-align: right;"><span> <o:date value="${payment.paid}" />
                </span></td>
                <td class="pamount"><o:number value="${payment.amount}" format="currency" /></td>
            </tr>
        </c:forEach>
        <tr>
            <td colspan="4" class="pspace"></td>
        </tr>
        <tr>
            <td colspan="2" class="payment"><fmt:message key="openAmount" /></td>
            <td class="pdate"><o:date current="true" /></td>
            <td class="pamount"><o:number value="${record.dueAmount}" format="currency" /></td>
        </tr>
    </c:if>
    <tr>
        <td colspan="4" class="plast"></td>
    </tr>
</table>
