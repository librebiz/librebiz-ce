/**
 *
 * Copyright (C) 2017 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 12, 2017 
 * 
 */
package com.osserp.core.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;
import com.osserp.common.mail.EmailAddress;
import com.osserp.common.util.NameUtil;

import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.Phone;
import com.osserp.core.contacts.PhoneType;
import com.osserp.core.contacts.Salutation;
import com.osserp.core.customers.Customer;
import com.osserp.core.dao.BranchOffices;
import com.osserp.core.dao.Contacts;
import com.osserp.core.dao.Customers;
import com.osserp.core.dao.Employees;
import com.osserp.core.dao.SystemCompanies;
import com.osserp.core.dao.SystemSetup;
import com.osserp.core.dao.Users;
import com.osserp.core.employees.Employee;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.system.SystemCompany;
import com.osserp.core.system.SystemConfig;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SystemSetupDao extends AbstractRepository implements SystemSetup {
    private static Logger log = LoggerFactory.getLogger(SystemSetupDao.class.getName());

    private Contacts contacts;
    private Customers customers;
    private Employees employees;
    private SystemCompanies systemCompanies;
    private BranchOffices branchOffices;
    private Users users;
    private OptionsCache optionsCache;

    /**
     * Creates a new system setup dao
     * @param jdbcTemplate
     * @param tables
     * @param sessionFactory
     * @param optionsCache
     * @param contacts
     * @param customers
     * @param employees
     * @param systemCompanies
     * @param branchOffices
     * @param users
     */
    public SystemSetupDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache optionsCache,
            Contacts contacts,
            Customers customers,
            Employees employees,
            SystemCompanies systemCompanies,
            BranchOffices branchOffices,
            Users users) {
        super(jdbcTemplate, tables, sessionFactory);
        this.contacts = contacts;
        this.customers = customers;
        this.employees = employees;
        this.systemCompanies = systemCompanies;
        this.branchOffices = branchOffices;
        this.users = users;
        this.optionsCache = optionsCache;
    }

    public DomainUser getExpectedUser(DomainUser admin) throws ClientException {
        DomainUser user = admin == null ? null : (DomainUser) users.find(admin.getLdapUid());
        if (user == null) {
            throw new ClientException(ErrorCode.INVALID_CONTEXT);
        }
        if (!user.isSetupAdmin()) {
            throw new ClientException(ErrorCode.INVALID_CONTEXT);
        }
        return user;
    }

    public DomainUser initCompany(
            DomainUser admin,
            String loginName,
            SystemCompany company,
            String companyName,
            Salutation salutation,
            Option title,
            String lastName,
            String firstName,
            String street,
            String zipcode,
            String city,
            Long country,
            Long federalState,
            String pcountry,
            String pprefix,
            String pnumber,
            String fcountry,
            String fprefix,
            String fnumber,
            String email) throws ClientException {

        String setupStatus = getPropertyString("appConfigStatus");
        if (!("setupInitial".equals(setupStatus))) {
            log.warn("initCompany() failed [appConfigStatus=" + setupStatus + "]");
            throw new ClientException(ErrorCode.INVALID_CONTEXT);
        }
        DomainUser user = getExpectedUser(admin);
        if (salutation == null || isEmpty(companyName) || isEmpty(lastName)
                || isEmpty(firstName) || isEmpty(street) || isEmpty(zipcode) || isEmpty(city)
                || isEmpty(loginName) || isEmpty(email)) {
            throw new ClientException(ErrorCode.VALUES_MISSING);
        }
        // TODO add some more application state checks (e.g. is system already in use?).

        Contact contact = company.getContact();
        contact.setLastName(companyName);
        contact.getAddress().setStreet(street);
        contact.getAddress().setZipcode(zipcode);
        contact.getAddress().setCity(city);
        contact.getAddress().setCountry(country);
        contact.getAddress().setFederalStateId(federalState);
        if (!isEmpty(pprefix) && !isEmpty(pnumber)) {
            if (!contact.getPhoneDeviceNumbers(PhoneType.PHONE).isEmpty()) {
                Phone existing = contact.getPhoneDeviceNumbers(PhoneType.PHONE).get(0);
                contact.updatePhone(admin.getId(), existing, Phone.BUSINESS, pcountry, pprefix, pnumber, null, true);
            } else {
                contact.addPhone(admin.getId(), PhoneType.PHONE, Phone.BUSINESS, pcountry, pprefix, pnumber, null, true);
            }
        }
        if (!isEmpty(fprefix) && !isEmpty(fnumber)) {
            if (!contact.getPhoneDeviceNumbers(PhoneType.FAX).isEmpty()) {
                Phone existing = contact.getPhoneDeviceNumbers(PhoneType.FAX).get(0);
                contact.updatePhone(admin.getId(), existing, Phone.BUSINESS, fcountry, fprefix, fnumber, null, true);
            } else {
                contact.addPhone(admin.getId(), PhoneType.FAX, Phone.BUSINESS, fcountry, fprefix, fnumber, null, true);
            }
        }
        if (contact.getEmails().isEmpty()) {
            contact.addEmail(admin.getId(), EmailAddress.BUSINESS, email, true, null);
        } else {
            contact.getEmails().get(0).setEmail(email);
        }
        contacts.save(contact);
        company.setName(companyName);
        String managingDirector = firstName + " " + lastName;
        if (!company.getManagingDirectors().isEmpty()) {
            company.getManagingDirectors().get(0).setName(managingDirector);
        }
        systemCompanies.save(company);
        BranchOffice hq = branchOffices.getHeadquarter();
        hq.setEmail(email);
        branchOffices.save(hq);
        user.setLdapUid(loginName);
        user.setLoginName(managingDirector);
        users.save(user);

        Employee md = employees.get(user.getEmployee().getId());
        md.setSalutation(salutation);
        md.setTitle(title);
        md.setFirstName(firstName);
        md.setLastName(lastName);
        md.getAddress().setStreet(street);
        md.getAddress().setZipcode(zipcode);
        md.getAddress().setCity(city);
        md.getAddress().setCountry(country);
        md.getAddress().setFederalStateId(federalState);
        md.setInitials(NameUtil.createInitials(firstName, lastName));
        employees.save(md);

        Customer posAccount = customers.fetchPrimaryPosAccount();
        if (posAccount != null) {
            posAccount.getAddress().setStreet(street);
            posAccount.getAddress().setZipcode(zipcode);
            posAccount.getAddress().setCity(city);
            posAccount.getAddress().setCountry(country);
            posAccount.getAddress().setFederalStateId(federalState);
            customers.save(posAccount);
        }
        optionsCache.refreshAll();
        return (DomainUser) users.find(user.getId());
    }

    public void closeSetup() {
        updatePropertyValue(SystemConfig.APP_CONFIG_STATUS_PROPERTY, Constants.SETUP_CLOSED_STATUS);
    }
}
