<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" scope="page" value="${sessionScope.selectStatusPopupView}"/>

<c:if test="${!empty view}">
	<div class="modalBoxHeader">
		<div class="modalBoxHeaderLeft">
			<fmt:message key="${view.headerName}"/>
		</div>
		<div class="modalBoxHeaderRight">
			<div class="boxnav">
				<ul>
					<c:forEach var="nav" items="${view.navigation}">
						<li>
							<v:ajaxLink url="${nav.link}" targetElement="${view.name}_popup" title="${nav.title}"><o:img name="${nav.icon}"/></v:ajaxLink>
						</li>
					</c:forEach>
				</ul>
			</div>
		</div>
	</div>
	<div class="modalBoxData">
		<div class="subcolumns">
			<div class="subcolumn">
				<c:if test="${!empty sessionScope.error}">
					<div class="spacer"></div>
					<div class="errormessage">
						<fmt:message key="error"/>: <fmt:message key="${sessionScope.error}"/>
					</div>
                    <o:removeErrors/>
				</c:if>
				<div class="spacer"></div>
				<v:ajaxForm name="dynamicForm" targetElement="${view.name}_popup" url="${view.saveLink.link}">
					<table class="valueTable input">
						<tbody>
							<tr>
								<td class="number">
									<fmt:message key="selection"/>
								</td>
								<td>
									<select name="lower" size="1">
										<option value="1" <c:if test="${view.lower}">selected="selected"</c:if>><fmt:message key="lessOrEqual"/></option>
										<option value="" <c:if test="${!view.lower}">selected="selected"</c:if>><fmt:message key="greaterThan"/></option>
									</select>
								</td>
								<td class="name">								
									<select name="value" size="1">
										<c:forEach var="status" items="${view.statusList}">
											<option value="<o:out value="${status}"/>"<c:if test="${status == view.value}"> selected="selected"</c:if>>
												<o:out value="${status}"/><fmt:message key="percent"/>
											</option>
										</c:forEach>
									</select>
								</td>
								<td>
									<input type="image" name="save" src="<o:icon name="saveIcon"/>" value="save" title="<fmt:message key="takeOver"/>"/>
								</td>
							</tr>
						</tbody>
					</table>
				</v:ajaxForm>
				<div class="spacer"></div>
			</div>
		</div>
	</div>
</c:if>