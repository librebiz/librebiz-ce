/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 13, 2016 
 * 
 */
package com.osserp.common;

import java.util.List;
import java.util.Map;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Details extends PersistentObject {

    /**
     * Provides properties as map
     * @return mapped properties
     */
    Map<String, Property> getPropertyMap();

    /**
     * Provides product properties
     * @return properties or empty list
     */
    List<Property> getPropertyList();

    /**
     * Adds or updates a property
     * @param user
     * @param name the logical name of the property
     * @param value
     */
    void addProperty(Long user, String name, String value);

    /**
     * Removes a property by logical name
     * @param user
     * @param property
     */
    void removeProperty(Long user, Property property);
    
    /**
     * Indicates if a property exists
     * @param propertyName 
     * @return true if a property with provided name exists
     */
    boolean isPropertyAvailable(String propertyName);
    
    /**
     * Provides a property by name
     * @param propertyName
     * @return property or null if not exists
     */
    Property getProperty(String propertyName);

}
