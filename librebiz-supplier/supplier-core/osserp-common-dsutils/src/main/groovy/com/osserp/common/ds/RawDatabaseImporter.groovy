/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License (AGPL)
 * version 3 as published by the Free Software Foundation. In accordance
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code
 * versions must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area
 * or, if not reasonably feasible for technical reason, as a top-level
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute.
 *
 */
package com.osserp.common.ds


/**
 * 
 * Implementations of this importer are reading an import file
 * and store all resulting rows in a temporary table created
 * on the fly using sheet and column names for ddl.
 * 
 *
 * @author rk <rk@osserp.com>
 *
 */
abstract class RawDatabaseImporter extends ImporterImpl {

    private final String ALL = "all"
    private final String IMPORT = "import"
    private final String STATS = "stats"
    private final String CLEANUP = "cleanup"
    private final String RENAME = "rename"

    RawDatabase database

    protected RawDatabaseImporter(String selectedMode, String directory) throws ImportException {
        super(selectedMode, directory, true, true)
        database = createDatabase()
    }

    @Override
    protected void checkMode() {
        if (!(ALL == mode
        || IMPORT == mode
        || STATS == mode
        || CLEANUP == mode
        || RENAME == mode)) {
            throw new ImportException("Invalid mode specified: ${mode}")
        }
    }

    Integer run() {
        Integer cnt = 0
        Integer rows = 0
        if (ALL == mode || IMPORT == mode) {

            def importStart = System.currentTimeMillis()
            importDir.eachFile() {
                File file = it
                if (isImportFile(file)) {
                    try {
                        long start = System.currentTimeMillis()
                        println("Importer: transfering file ${file.name}")
                        Sheet2Table importFile = createImportFile(file)
                        rows = rows + database.transfer(importFile)
                        println("Importer: transfer of file ${file.name} done in ${System.currentTimeMillis() - start}ms")
                        cnt++
                    } catch (ImportException e) {
                        println("Importer: error processing file ${file.name} => message=${e.message}")
                    }
                }
            }
            println "Importer: transfered ${cnt} files with ${rows} rows in ${System.currentTimeMillis() - importStart}ms"
        }
        if (ALL == mode || STATS == mode) {
            try {
                database.createTableStats()
            } catch (Throwable t) {
                println("Error creating table stats [message=${e.message}]")
            }
        }
        if (ALL == mode || CLEANUP == mode) {
            try {
                database.cleanupTables()
            } catch (Throwable t) {
                println("Error cleaning up tables [message=${e.message}]")
            }
        }
        if (ALL == mode || RENAME == mode) {
            try {
                database.renameTables()
            } catch (Throwable t) {
                println("Error renaming tables [message=${e.message}]")
            }
        }
        return cnt
    }

    protected abstract RawDatabase createDatabase()
}
