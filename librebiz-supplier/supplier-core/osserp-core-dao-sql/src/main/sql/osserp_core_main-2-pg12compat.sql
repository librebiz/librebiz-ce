SET search_path = osserp, pg_catalog;

DROP FUNCTION get_timestamp_add_days(timestamp without time zone, integer);
DROP FUNCTION get_timestamp_add_days(timestamp with time zone, integer);
DROP FUNCTION get_timestamp_add_minutes(integer);
DROP FUNCTION get_timestamp_add_minutes(timestamp without time zone, integer);
DROP FUNCTION get_timestamp_substract_days(timestamp without time zone, integer);
DROP FUNCTION get_timestamp_substract_days(timestamp with time zone, integer);

CREATE OR REPLACE FUNCTION get_timestamp_add_days(timestamp without time zone, integer) RETURNS timestamp with time zone
    LANGUAGE sql
    AS $_$
 SELECT to_timestamp(trunc(date_part('epoch', $1)) + ($2 * 86400 ));
$_$;

CREATE OR REPLACE FUNCTION get_timestamp_add_days(timestamp with time zone, integer) RETURNS timestamp with time zone
    LANGUAGE sql
    AS $_$
 SELECT to_timestamp(trunc(date_part('epoch', $1)) + ($2 * 86400 ));
$_$;

CREATE OR REPLACE FUNCTION get_timestamp_add_minutes(integer) RETURNS timestamp with time zone
    LANGUAGE sql
    AS $_$
 SELECT to_timestamp(trunc(date_part('epoch', now())) + ($1 * 60));
$_$;

CREATE OR REPLACE FUNCTION get_timestamp_add_minutes(timestamp without time zone, integer) RETURNS timestamp with time zone
    LANGUAGE sql
    AS $_$
 SELECT to_timestamp(trunc(date_part('epoch', $1)) + ($2 * 60));
$_$;

CREATE OR REPLACE FUNCTION get_timestamp_substract_days(timestamp without time zone, integer) RETURNS timestamp with time zone
    LANGUAGE sql
    AS $_$
 SELECT to_timestamp(trunc(date_part('epoch', $1)) - ($2 * 86400 ));
$_$;

CREATE OR REPLACE FUNCTION get_timestamp_substract_days(timestamp with time zone, integer) RETURNS timestamp with time zone
    LANGUAGE sql
    AS $_$
 SELECT to_timestamp(trunc(date_part('epoch', $1)) - ($2 * 86400 ));
$_$;
