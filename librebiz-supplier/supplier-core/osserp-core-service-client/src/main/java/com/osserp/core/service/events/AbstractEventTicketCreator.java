/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 11, 2007 10:27:48 AM 
 * 
 */
package com.osserp.core.service.events;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.core.dao.Employees;
import com.osserp.core.dao.EventConfigs;
import com.osserp.core.dao.Events;
import com.osserp.core.employees.Employee;
import com.osserp.core.events.EventAction;
import com.osserp.core.events.EventTicket;
import com.osserp.core.events.Recipient;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.tasks.EventTicketSender;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class AbstractEventTicketCreator extends AbstractEventTicketHandler {
    private static Logger log = LoggerFactory.getLogger(AbstractEventTicketCreator.class.getName());
    private EventTicketSender eventTicketSender = null;

    protected AbstractEventTicketCreator(
            Events events,
            EventConfigs eventConfigs,
            Employees employees,
            EventTicketSender eventTicketSender) {
        super(events, eventConfigs, employees);
        this.eventTicketSender = eventTicketSender;
    }

    protected void create(EventTicket ticket) {
        Events events = getEvents();
        events.setTicketSent(ticket);
        try {
            eventTicketSender.send(ticket);
        } catch (Throwable t) {
            log.error("create() failed [message=" + t.toString() + "]", t);
        }
    }

    protected final void createTicket(
            EventAction action,
            Long reference,
            String description,
            Long branchId,
            Long managerId,
            Long salesId,
            Long createdBy,
            String message,
            String headline,
            boolean canceled) {

        Events events = getEvents();
        EventTicket eticket = events.createTicket(
                action,
                createdBy,
                reference,
                description,
                message,
                headline,
                canceled);

        boolean send = true;
        if (action.isSendSales() && salesId != null) {
            eticket.addRecipient(salesId);

        } else if (action.isSendManager() && managerId != null) {
            eticket.addRecipient(managerId);

        } else if (action.isSendPool() && action.getPool() != null) {
            log.debug("createTicket() invoked for pool ["
                    + action.getPool().getId() + ":"
                    + action.getPool().getName()
                    + "]");

            for (int i = 0, j = action.getPool().getMembers().size(); i < j; i++) {
                Recipient nextRecipient = action.getPool().getMembers().get(i);
                Long nextId = nextRecipient.getRecipientId();

                if (action.isIgnoringBranch()) {
                    if (log.isDebugEnabled()) {
                        log.debug("createTicket() adding ["
                                + nextId + "] while ignoring branch");
                    }
                    eticket.addRecipient(nextId);

                } else {
                    try {
                        Employee employee = fetchEmployee(nextId);
                        if (branchId == null) {
                            // add only branch ignoring employees
                            if (employee.isIgnoringBranch()) {
                                
                                eticket.addRecipient(employee.getId());
                                
                            } else if (log.isDebugEnabled()) {
                                log.debug("createTicket() business case without branch, " +
                                        "ignoring employee with no enabled ignoring branch flag [id="
                                        + nextId + "]");
                            }

                        } else if (employee.isFromBranch(branchId)) {
                            if (log.isDebugEnabled()) {
                                log.debug("createTicket() adding employee [id=" + employee.getId()
                                        + ", branch=" + branchId + "]");
                            }
                            
                            eticket.addRecipient(employee.getId());

                        } else if (log.isDebugEnabled()) {
                            BranchOffice branch = employee.getBranch();
                            if (log.isDebugEnabled()) {
                                log.debug("createTicket() ignoring employee [id="
                                        + nextId + ", branch="
                                        + (branch == null ? "null" : branch.getId())
                                        + ", businessCaseBranch="
                                        + branchId + "]");
                            }
                        }
                    } catch (Throwable t) {
                        log.warn("createTicket() ignoring exception on attempt to add ["
                                + nextId + "] to recipients of action "
                                + eticket.getConfig().getId());
                    }
                }
            }

        } else if (action.isSendPool()) {
            send = false;
            log.warn("createTicket() ignoring action ["
                    + action.getId()
                    + "] configured as pool while no pool assigned!");
        }
        if (send) {
            eticket.addParameter("id", reference.toString());
            eticket.addParameter("target", "todo");
            create(eticket);
        }
    }

    protected void createTicket(
            EventAction action,
            Long reference,
            String description,
            Long branchId,
            Long recipientId,
            Long createdBy,
            String message) {

        if (recipientId != null) {
            Events events = getEvents();
            EventTicket eticket = events.createTicket(
                    action,
                    createdBy,
                    reference,
                    description,
                    message,
                    null,
                    false);

            eticket.addRecipient(recipientId);
            eticket.addParameter("id", reference.toString());
            eticket.addParameter("target", "todo");
            create(eticket);
        } else {
            log.warn("createTicket() invoked with null arg as recipient [action=" + action.getId() + ", reference=" + reference + "]");
        }
    }

    protected EventTicketSender getEventTicketSender() {
        return eventTicketSender;
    }

    protected void setEventTicketSender(EventTicketSender eventTicketSender) {
        this.eventTicketSender = eventTicketSender;
    }
}
