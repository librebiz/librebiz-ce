/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jul 22, 2014 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.dao.Tables;

import com.osserp.core.dao.impl.AbstractDao;
import com.osserp.core.dao.records.RecordPrintOptionDefaults;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordPrintOption;
import com.osserp.core.model.records.RecordPrintOptionDefaultImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RecordPrintOptionDefaultsDao extends AbstractDao implements RecordPrintOptionDefaults {
    private static Logger log = LoggerFactory.getLogger(RecordPrintOptionDefaultsDao.class.getName());

    protected RecordPrintOptionDefaultsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
    }

    public List<RecordPrintOption> find(Long recordType) {
        StringBuilder hql = new StringBuilder("from ");
        hql
            .append(RecordPrintOptionDefaultImpl.class.getName())
            .append(" o where o.recordType = :typeId");
        return getCurrentSession().createQuery(hql.toString())
                .setParameter("typeId", recordType).list();
    }

    public Map<String, RecordPrintOption> getDefaults(Long recordType) {
        List<RecordPrintOption> list = find(recordType);
        Map<String, RecordPrintOption> result = new HashMap<>();
        for (int i = 0, j = list.size(); i < j; i++) {
            RecordPrintOption next = list.get(i);
            result.put(next.getName(), next);
        }
        return result;
    }

    public Record applyDefaults(Record record) {
        List<RecordPrintOption> options = find(record.getType().getId());
        List<RecordPrintOption> unknownOptions = new ArrayList<>();
        if (log.isDebugEnabled()) {
            log.debug("applyDefaults() invoked [optionCount=" + options.size() + "]");
        }
        for (int i = 0, j = options.size(); i < j; i++) {
            RecordPrintOption next = options.get(i);
            if ("printPaymentTarget".equals(next.getName())) {
                record.setPrintPaymentTarget(next.isEnabled());
            } else if ("printComplimentaryClose".equals(next.getName())) {
                record.setPrintComplimentaryClose(next.isEnabled());
            } else if ("printProjectmanager".equals(next.getName())) {
                record.setPrintProjectmanager(next.isEnabled());
            } else if ("printSalesperson".equals(next.getName())) {
                record.setPrintSalesperson(next.isEnabled());
            } else if ("printBusinessCaseId".equals(next.getName())) {
                record.setPrintBusinessCaseId(next.isEnabled());
            } else if ("printBusinessCaseInfo".equals(next.getName())) {
                record.setPrintBusinessCaseInfo(next.isEnabled());
            } else if ("printRecordDate".equals(next.getName())) {
                record.setPrintRecordDate(next.isEnabled());
            } else if ("printRecordDateByStatus".equals(next.getName())) {
                record.setPrintRecordDateByStatus(next.isEnabled());
            } else if ("printCoverLetter".equals(next.getName())) {
                record.setPrintCoverLetter(next.isEnabled());
            } else {
                unknownOptions.add(next);
            }
        }
        return applyDedicatedDefaults(record, unknownOptions);
    }

    public void saveDefaults(Record record) {
        Map<String, RecordPrintOption> defaults = getDefaults(record.getType().getId());
        RecordPrintOption option = defaults.get("printPaymentTarget");
        if (option == null) {
            // create new option
            option = new RecordPrintOptionDefaultImpl(
                    record.getType().getId(),
                    "printPaymentTarget",
                    record.isPrintPaymentTarget());
        } else {
            option.setBoolean(record.isPrintPaymentTarget());
        }
        save(option);
        option = defaults.get("printComplimentaryClose");
        if (option == null) {
            // create new option
            option = new RecordPrintOptionDefaultImpl(
                    record.getType().getId(),
                    "printComplimentaryClose",
                    record.isPrintComplimentaryClose());
        } else {
            option.setBoolean(record.isPrintComplimentaryClose());
        }
        save(option);
        option = defaults.get("printProjectmanager");
        if (option == null) {
            // create new option
            option = new RecordPrintOptionDefaultImpl(
                    record.getType().getId(),
                    "printProjectmanager",
                    record.isPrintProjectmanager());
        } else {
            option.setBoolean(record.isPrintProjectmanager());
        }
        save(option);
        option = defaults.get("printSalesperson");
        if (option == null) {
            // create new option
            option = new RecordPrintOptionDefaultImpl(
                    record.getType().getId(),
                    "printSalesperson",
                    record.isPrintSalesperson());
        } else {
            option.setBoolean(record.isPrintSalesperson());
        }
        save(option);
        option = defaults.get("printBusinessCaseId");
        if (option == null) {
            // create new option
            option = new RecordPrintOptionDefaultImpl(
                    record.getType().getId(),
                    "printBusinessCaseId",
                    record.isPrintBusinessCaseId());
        } else {
            option.setBoolean(record.isPrintBusinessCaseId());
        }
        save(option);
        option = defaults.get("printBusinessCaseInfo");
        if (option == null) {
            // create new option
            option = new RecordPrintOptionDefaultImpl(
                    record.getType().getId(),
                    "printBusinessCaseInfo",
                    record.isPrintBusinessCaseInfo());
        } else {
            option.setBoolean(record.isPrintBusinessCaseInfo());
        }
        save(option);
        option = defaults.get("printRecordDate");
        if (option == null) {
            // create new option
            option = new RecordPrintOptionDefaultImpl(
                    record.getType().getId(),
                    "printRecordDate",
                    record.isPrintRecordDate());
        } else {
            option.setBoolean(record.isPrintRecordDate());
        }
        save(option);
        option = defaults.get("printRecordDateByStatus");
        if (option == null) {
            // create new option
            option = new RecordPrintOptionDefaultImpl(
                    record.getType().getId(),
                    "printRecordDateByStatus",
                    record.isPrintRecordDateByStatus());
        } else {
            option.setBoolean(record.isPrintRecordDateByStatus());
        }
        save(option);
        option = defaults.get("printCoverLetter");
        if (option == null) {
            // create new option
            option = new RecordPrintOptionDefaultImpl(
                    record.getType().getId(),
                    "printPrintCoverLetter",
                    record.isPrintCoverLetter());
        } else {
            option.setBoolean(record.isPrintCoverLetter());
        }
        save(option);
        saveDedicatedDefaults(record, defaults);
    }
    

    /**
     * Override this method to apply type specific printOptions 
     * @param record
     * @param unknownOptions list of unknown options collected by default impl.
     * @return record
     */
    protected Record applyDedicatedDefaults(Record record, List<RecordPrintOption> unknownOptions) {
        return record;
    }

    /**
     * Override this method to persist type specific printoptions
     * @param record
     * @param defaults
     */
    protected void saveDedicatedDefaults(Record record, Map<String, RecordPrintOption> defaults) {
        // implement your logic in an inherited class if required
    }

    protected void save(RecordPrintOption option) {
        getCurrentSession().saveOrUpdate(RecordPrintOptionDefaultImpl.class.getName(), option);
    }
}
