<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>

<c:set var="view" value="${sessionScope.searchView}" />
<c:set var="url" value="${view.data['searchUrl']}" />
<c:if test="${!empty view}">
    <div class="row" id="productClassification">
        <div class="col-md-2">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-9">
                        <div class="form-group">
                            <input class="form-control" style="text-align:left;" type="text" name="value" id="value" value="<o:out value="${view.value}"/>" onkeyup="javascript:checkCheckbox();" <c:if test="${view.showAllProducts && !view.productSelectionConfigsMode}">disabled="true"</c:if> />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <a href="javascript:if (checkSearchException()) {ojsAjax.ajaxForm('<c:url value="${url}" />', 'ajaxContent', false, $('productSearchForm'), 'value', 'null', 0);}"> <o:img name="replaceIcon" styleClass="bigicon" style="padding-top: 5px;" />
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-2">
            <div class="form-group">
                <select class="form-control" name="method" size="1" id="method" onchange="$('productSearchForm').onkeyup();">
                    <c:forEach var="meth" items="${view.methods}">
                        <c:choose>
                            <c:when test="${view.selectedMethod == meth}">
                                <option value="<o:out value="${meth}"/>" selected="selected"><fmt:message key="${meth}" /></option>
                            </c:when>
                            <c:otherwise>
                                <option value="<o:out value="${meth}"/>"><fmt:message key="${meth}" /></option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
            </div>
        </div>

        <c:choose>
            <c:when test="${view.productSelectionConfigsMode}">
                <div class="col-md-2">
                    <div class="form-group">
                        <select class="form-control" name="filter" size="1" id="filter" onchange="gotoUrl(this.value);">
                            <c:choose>
                                <c:when test="${empty view.selectedProductSelectionConfigItem and empty view.selectedProductSelectionConfig}">
                                    <option value="<c:url value="${url}${'?method=selectProductSelectionConfig'}" />" selected="selected"><fmt:message key="preselectionActivated" /></option>
                                </c:when>
                                <c:otherwise>
                                    <option value="<c:url value="${url}${'?method=selectProductSelectionConfig'}" />"><fmt:message key="loadAllPreselections" /></option>
                                </c:otherwise>
                            </c:choose>
                            <c:forEach var="config" items="${view.productSelectionConfigs}">
                                <c:choose>
                                    <c:when test="${empty view.selectedProductSelectionConfigItem and view.selectedProductSelectionConfig.id == config.id}">
                                        <option style="background: grey; color: white;" value="<c:url value="${url}${'?method=selectProductSelectionConfig&id='}${config.id}" />" selected="selected">A -
                                            <o:out value="${config.name}" /></option>
                                    </c:when>
                                    <c:otherwise>
                                        <option style="background: grey; color: white;" value="<c:url value="${url}${'?method=selectProductSelectionConfig&id='}${config.id}" />">A -
                                            <o:out value="${config.name}" /></option>
                                    </c:otherwise>
                                </c:choose>
                                <c:forEach var="item" items="${config.items}">
                                    <c:choose>
                                        <c:when test="${view.selectedProductSelectionConfigItem.id == item.id}">
                                            <option value="<c:url value="${url}${'?method=selectProductSelectionConfigItem&id='}${item.id}" />" selected="selected">
                                                <o:out value="${item.type.name}" /><c:if test="${!empty item.group}"> / <o:out value="${item.group.name}" />
                                                </c:if><c:if test="${!empty item.category}"> / <o:out value="${item.category.name}" />
                                                </c:if>
                                            </option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="<c:url value="${url}${'?method=selectProductSelectionConfigItem&id='}${item.id}" />">
                                                <o:out value="${item.type.name}" /><c:if test="${!empty item.group}"> / <o:out value="${item.group.name}" />
                                                </c:if><c:if test="${!empty item.category}"> / <o:out value="${item.category.name}" />
                                                </c:if>
                                            </option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </c:forEach>
                        </select>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group"></div>
                </div>

            </c:when>
            <c:otherwise>
                <div class="col-md-2">
                    <div class="form-group">

                        <select class="form-control" name="typeIdFake" size="1" id="typeIdFake" onChange="ojsAjax.ajaxLink(this.value, 'productClassification', false, '', '', function() { $('productSearchForm').onkeyup(); }, this);" <c:if test="${searchView.noClassification}">disabled="disabled"</c:if>>
                            <c:choose>
                                <c:when test="${empty view.selectedType}">
                                    <option value="<c:url value="${url}?method=updateClassification"/>" selected="selected"><fmt:message key="type" />...
                                    </option>
                                </c:when>
                                <c:otherwise>
                                    <option value="<c:url value="${url}?method=updateClassification&typeId=-1"/>" selected="selected"><fmt:message key="noSelection" /></option>
                                </c:otherwise>
                            </c:choose>
                            <c:forEach var="type" items="${view.typeSelections}">
                                <c:choose>
                                    <c:when test="${view.selectedType.id == type.id}">
                                        <option value="<c:url value="${url}?method=updateClassification&typeId=${type.id}"/>" selected="selected"><o:out value="${type.name}" /></option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="<c:url value="${url}?method=updateClassification&typeId=${type.id}"/>"><o:out value="${type.name}" /></option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>

                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        <select class="form-control" name="groupIdFake" size="1" id="groupIdFake" onChange="ojsAjax.ajaxLink(this.value, 'productClassification', false, '', '', function() { $('productSearchForm').onkeyup(); }, this);" <c:if test="${empty view.productGroups or view.noClassification}">disabled="disabled"</c:if>>
                            <c:choose>
                                <c:when test="${empty view.selectedGroup}">
                                    <option value="<c:url value="${url}?method=updateClassification"/>" selected="selected"><fmt:message key="group" />...
                                    </option>
                                </c:when>
                                <c:otherwise>
                                    <option value="<c:url value="${url}?method=updateClassification&groupId=0"/>"><fmt:message key="noSelection" /></option>
                                </c:otherwise>
                            </c:choose>
                            <c:forEach var="grp" items="${view.productGroups}">
                                <c:choose>
                                    <c:when test="${view.selectedGroup.id == grp.id}">
                                        <option value="<c:url value="${url}?method=updateClassification&groupId=${grp.id}"/>" selected="selected"><o:out value="${grp.name}" /></option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="<c:url value="${url}?method=updateClassification&groupId=${grp.id}"/>"><o:out value="${grp.name}" /></option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>

                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        <select class="form-control" name="categoryIdFake" size="1" id="categoryIdFake" onChange="ojsAjax.ajaxLink(this.value, 'productClassification', false, '', '', function() { $('productSearchForm').onkeyup(); }, this);" <c:if test="${empty view.productCategorys or view.noClassification}">disabled="disabled"</c:if>>
                            <c:choose>
                                <c:when test="${empty view.selectedCategory}">
                                    <option value="<c:url value="${url}?method=updateClassification"/>" selected="selected"><fmt:message key="category" />...
                                    </option>
                                </c:when>
                                <c:otherwise>
                                    <option value="<c:url value="${url}?method=updateClassification&categoryId=0"/>"><fmt:message key="noSelection" /></option>
                                </c:otherwise>
                            </c:choose>
                            <c:forEach var="category" items="${view.productCategorys}">
                                <c:choose>
                                    <c:when test="${view.selectedCategory.id == category.id}">
                                        <option value="<c:url value="${url}?method=updateClassification&categoryId=${category.id}"/>" selected="selected"><o:out value="${category.name}" /></option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="<c:url value="${url}?method=updateClassification&categoryId=${category.id}"/>"><o:out value="${category.name}" /></option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group"></div>
                </div>

            </c:otherwise>
        </c:choose>
    </div>
</c:if>