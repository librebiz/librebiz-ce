/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 27.10.2004 
 * 
 */
package com.osserp.common.web.tags;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.web.AbstractPortalView;
import com.osserp.common.web.PageImpl;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.SessionUtil;
import com.osserp.common.web.WebConfig;

/**
 * Displays the title of the application and sets currentPage session scope value
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class DisplayTitleTag extends AbstractConfigDisplayTag {
    private static Logger log = LoggerFactory.getLogger(DisplayTitleTag.class.getName());

    @Override
    protected String getDisplayString(WebConfig config) {
        HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
        String serverString = RequestUtil.getServerURI(request);
        if (log.isDebugEnabled()) {
            StringBuilder buffer = new StringBuilder("getDisplayString() for page ");
            buffer.append(serverString);
            try {
                HttpSession session = request.getSession(true);
                boolean pAvailable = (session.getAttribute("portalView") != null);
                buffer.append(", portalView found: ").append(pAvailable);
            } catch (Throwable t) {
            }
            log.debug(buffer.toString());
        }
        String page = RequestUtil.createPageName(serverString, request.getContextPath());
        SessionUtil.setPage(request.getSession(), new PageImpl(page, serverString));
        return (config.getDisplayTitle() == null) ? ""
                : config.getDisplayTitle();
    }

    @Override
    public int doStartTag() throws JspException {
        int ret = super.doStartTag();
        try {
            AbstractPortalView portalView = (AbstractPortalView) getSession().getAttribute("portalView");
            portalView.resetPermissions();
        } catch (Throwable t) {
            //if there is no portalView, there is nothing to be reseted
        }
        return ret;
    }
}
