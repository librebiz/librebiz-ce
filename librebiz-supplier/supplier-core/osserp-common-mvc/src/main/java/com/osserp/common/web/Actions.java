/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 5, 2004 
 * 
 */
package com.osserp.common.web;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class Actions {

    // keywords
    public static final String ALIASES = "aliases";
    public static final String ARCHIVE = "archive";
    public static final String APPOINTMENT_EDIT = "appointmentEdit";
    public static final String CALENDAR_STARTUP = "calendarStartup";
    public static final String CANCELLATION = "cancellation";
    public static final String CHART = "chart";
    public static final String CHART_ERROR = "chartError";
    public static final String CLOSE_POPUP = "closePopup";
    public static final String COMPANY_SELECTION = "companySelection";
    public static final String CONDITIONS = "conditions";
    public static final String CONFIG = "config";
    public static final String CREATE = "create";
    public static final String CREDIT_NOTE = "creditNote";
    public static final String DISABLED = "disabled";
    public static final String DISPLAY = "display";
    public static final String DISPLAY_DAY = "displayDay";
    public static final String DISPLAY_MONTH = "displayMonth";
    public static final String DISPLAY_WASTE = "displayWaste";
    public static final String ERROR = "error";
    public static final String EDIT = "edit";
    public static final String ENABLED = "enabled";
    public static final String EVENTS = "events";
    public static final String EXIT = "exit";
    public static final String FCS_DISPLAY = "fcsDisplay";
    public static final String FCS_EDIT = "fcsEdit";
    public static final String ID = "id";
    public static final String INDEX = "index";
    public static final String LIST = "list";
    public static final String LOAD = "load";
    public static final String NEXT = "next";
    public static final String PARENT_RELOAD_POPUP = "parentReloadPopup";
    public static final String PAYMENT = "payment";
    public static final String PDF = "pdf";
    public static final String PRINT = "print";
    public static final String PRODUCT = "product";
    public static final String QUERY = "query";
    public static final String RESULT = "result";
    public static final String SALES = "sales";
    public static final String SALES_RECORDS = "salesRecords";
    public static final String SALES_RECORD_SELECTION = "salesRecordSelection";
    public static final String SALUTATION_CONFIG = "salutationConfig";
    public static final String SEARCH = "search";
    public static final String SELECT = "select";
    public static final String SETTINGS = "settings";
    public static final String START = "start";
    public static final String STATUS = "status";
    public static final String SUCCESS = "success";
    public static final String TARGET = "target";
    public static final String TEMPLATE = "template";
    public static final String USER_SELECTION = "userSelection";
    public static final String XLS = "xls";
    public static final String XML = "xml";
}
