/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 2, 2006 10:54:34 AM 
 * 
 */
package com.osserp.gui.client.contacts;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.User;
import com.osserp.common.dms.DmsDocument;
import com.osserp.common.web.Form;
import com.osserp.common.web.ViewName;
import com.osserp.core.employees.EmployeeDisciplinarian;
import com.osserp.core.system.BranchOffice;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("contactView")
public interface EmployeeView extends EmployeeAwareView, RelatedContactView {

    /**
     * Provides the picture if available
     * @return picture or null
     */
    public DmsDocument getPicture();

    /**
     * Puts all activated employees in current list
     */
    public void setActived();

    /**
     * Changes the activation flag of the current selected employee
     */
    public void changeActivationStatus();

    /**
     * Changes the website display flag of the current selected employee
     * @throws ClientException if validation failed
     */
    public void changeWebsiteDisplay() throws ClientException;

    /**
     * Indicates if view is in record phone selection mode
     * @return true if so
     */
    public boolean isRecordPhoneSelectionMode();

    /**
     * Enables/disables record phone selection mode
     * @param recordPhoneSelectionMode
     */
    public void setRecordPhoneSelectionMode(boolean recordPhoneSelectionMode);

    /**
     * Changes the activation flag of the current selected employee
     * @param device id
     */
    public void changePhoneForRecord(Long deviceId);

    /**
     * Indicates if view is in internet phone selection mode
     * @return modeEnabled
     */
    public boolean isInternetPhoneSelectionMode();

    /**
     * Enables/disables internet phone selection mode
     * @param internetPhoneSelectionMode
     */
    public void setInternetPhoneSelectionMode(boolean internetPhoneSelectionMode);

    /**
     * Changes the activation flag of the current selected employee
     * @param phoneId
     */
    public void changePhoneForInternet(Long phoneId);

    /**
     * Indicates that view is in description edit mode
     * @return descriptionEditMode
     */
    public boolean isDescriptionEditMode();

    /**
     * Enables/disables description edit mode
     * @param descriptionEditMode
     */
    public void setDescriptionEditMode(boolean descriptionEditMode);

    /**
     * Updates the current selected description of an employee. Default description will be updated if none description was selected previous
     * @param form with 'description' value
     * @throws ClientException if description missing
     * @throws PermissionException if user has no permission
     */
    public void updateDescription(Form form) throws ClientException, PermissionException;

    /**
     * Provides the disciplinarians of the current activated employee
     * @return disciplinarian list
     */
    public List<EmployeeDisciplinarian> getDisciplinarians();

    /**
     * Provides the count of assigned disciplinarians
     * @return count of assigned disciplinarians
     */
    public int getDisciplinariansCount();

    /**
     * Sets default disciplinarian
     * @param disciplinarianId
     */
    public void setDefaultDisciplinarian(Long disciplinarianId);

    /**
     * Indicates that current selected employee has also an user account
     * @return userAccountAvailable
     */
    public boolean isUserAccountAvailable();

    /**
     * Indicates that domain user is branch executive from current selected employee
     * @return branchExecutiveFromSelected
     */
    public boolean isBranchExecutiveFromSelected();

    /**
     * Indicates that domain user is disciplinarian from current selected employee
     * @return disciplinarianFromSelected
     */
    public boolean isDisciplinarianFromSelected();
    
    /**
     * Provides the userAccount associated with view.employee
     * @return userAccount
     */
    public User getUserAccount();
    

    /**
     * Provides all active branchs required by initial branch selection
     * @return available branch office
     */
    public List<BranchOffice> getAvailableBranchs();
    
}
