<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${requestScope.dmsDocumentView}" />

<v:form url="/sales/salesDocument/save" multipart="true">
    <div class="subcolumns">
        <div class="subcolumn">
            <table class="table table-striped">
                <c:if test="${!empty view.list}">
                    <thead>
                        <tr>
                            <th class="docListName"><fmt:message key="document" /></th>
                            <th class="docListSize"><fmt:message key="size" /></th>
                            <th class="docListDate"><fmt:message key="date" /></th>
                            <th class="docListUser"><fmt:message key="uploadBy" /></th>
                            <th class="action"><fmt:message key="action" /></th>
                        </tr>
                    </thead>
                </c:if>
                <tbody>
                    <c:import url="${viewdir}/dms/_commonDocumentUploadColumn.jsp" />
                    <c:forEach items="${view.list}" var="doc">
                        <tr>
                            <td class="docListName"><o:doc obj="${doc}"><o:out value="${doc.displayName}" /></o:doc></td>
                            <td class="docListSize"><o:number value="${doc.fileSize}" format="bytes" /></td>
                            <td class="docListDate"><o:date value="${doc.displayDate}" /></td>
                            <td class="docListUser">
                                <v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${doc.createdBy}">
                                    <oc:employee value="${doc.createdBy}" />
                                </v:ajaxLink>
                            </td>
                            <td class="action">
                                <o:permission role="executive,delete_sales_documents" info="permissionDocumentsDelete">
                                    <a href="javascript:onclick=confirmLink('<fmt:message key="irrevocableAction"/>:\n<fmt:message key="confirmDeleteDocument"/>','<v:url value="/sales/salesDocument/delete?id=${doc.id}" />');" title="<fmt:message key="deleteDocument"/>">
                                        <o:img name="deleteIcon" />
                                    </a>
                                </o:permission>
                                <o:forbidden role="executive,delete_sales_documents" info="permissionDocumentsDelete">
                                    <c:if test="${doc.createdBy == view.user.id}">
                                        <a href="javascript:onclick=confirmLink('<fmt:message key="irrevocableAction"/>:\n<fmt:message key="confirmDeleteDocument"/>','<v:url value="/sales/salesDocument/delete?id=${doc.id}" />');" title="<fmt:message key="deleteDocument"/>">
                                            <o:img name="deleteIcon" />
                                        </a>
                                    </c:if>
                                </o:forbidden>
                            </td>
                        </tr>
                    </c:forEach>
                    <c:import url="${viewdir}/dms/_commonDocumentImports.jsp" />
                </tbody>
            </table>
        </div>
    </div>
</v:form>