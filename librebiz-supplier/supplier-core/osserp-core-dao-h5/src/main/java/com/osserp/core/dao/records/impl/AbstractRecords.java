/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 14, 2006 3:16:18 PM 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import org.hibernate.SessionFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;
import com.osserp.common.util.CollectionUtil;
import com.osserp.common.util.StringUtil;

import com.osserp.core.BusinessCase;
import com.osserp.core.BusinessType;
import com.osserp.core.Comparators;
import com.osserp.core.Item;
import com.osserp.core.Options;
import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.dao.PaymentConditions;
import com.osserp.core.dao.SystemCompanies;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.dao.TaxRates;
import com.osserp.core.dao.records.Payments;
import com.osserp.core.dao.records.RecordInfoConfigs;
import com.osserp.core.dao.records.RecordMailLogs;
import com.osserp.core.dao.records.RecordPrintOptionDefaults;
import com.osserp.core.dao.records.Records;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.BookingType;
import com.osserp.core.finance.FinanceRecord;
import com.osserp.core.finance.ItemChangedInfo;
import com.osserp.core.finance.ListItem;
import com.osserp.core.finance.Payment;
import com.osserp.core.finance.PaymentAgreement;
import com.osserp.core.finance.PaymentAgreementAwareRecord;
import com.osserp.core.finance.PaymentAwareRecord;
import com.osserp.core.finance.PaymentCondition;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.finance.RecordInfo;
import com.osserp.core.finance.RecordMailLog;
import com.osserp.core.finance.RecordNumberCreator;
import com.osserp.core.finance.RecordStatusHistory;
import com.osserp.core.finance.RecordType;
import com.osserp.core.finance.Stock;
import com.osserp.core.model.DefaultPaymentAgreementImpl;
import com.osserp.core.model.records.AbstractRecord;
import com.osserp.core.model.records.ItemChangedInfoVO;
import com.osserp.core.model.records.RecordStatusInfoImpl;
import com.osserp.core.model.records.RecordTypeImpl;
import com.osserp.core.products.Product;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractRecords extends AbstractFinanceRecords implements Records {
    private static Logger log = LoggerFactory.getLogger(AbstractRecords.class.getName());

    private RecordInfoConfigs infoConfigs = null;
    protected PaymentConditions paymentConditions = null;
    private RecordMailLogs recordMailLogs = null;
    private RecordNumberCreator recordNumberCreator = null;
    private RecordPrintOptionDefaults recordPrintOptionDefaults = null;
    private SystemCompanies companies = null;
    private Payments payments = null;
    private boolean supportingPayments = false;
    private String[] deletePermissions = new String[] { "executive_accounting" };

    protected AbstractRecords(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache cache,
            TaxRates taxRates,
            SystemCompanies companies,
            RecordInfoConfigs infoConfigs,
            PaymentConditions paymentConditions,
            RecordNumberCreator recordNumberCreator,
            RecordPrintOptionDefaults recordPrintOptionDefaults,
            RecordMailLogs recordMailLogs) {

        super(jdbcTemplate, tables, sessionFactory, cache, taxRates);
        this.companies = companies;
        this.infoConfigs = infoConfigs;
        this.paymentConditions = paymentConditions;
        this.recordNumberCreator = recordNumberCreator;
        this.recordPrintOptionDefaults = recordPrintOptionDefaults;
        this.recordMailLogs = recordMailLogs;
    }

    public AbstractRecords(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache cache,
            TaxRates taxRates,
            SystemCompanies companies,
            RecordInfoConfigs infoConfigs,
            PaymentConditions paymentConditions,
            RecordNumberCreator recordNumberCreator,
            RecordPrintOptionDefaults recordPrintOptionDefaults,
            RecordMailLogs recordMailLogs,
            Payments payments) {

        this(jdbcTemplate,
                tables, sessionFactory, cache, taxRates, companies, infoConfigs,
                paymentConditions, recordNumberCreator, recordPrintOptionDefaults,
                recordMailLogs);

        this.payments = payments;
        if (payments != null) {
            supportingPayments = true;
        }
    }
    
    protected abstract PaymentCondition getDefaultCondition();

    protected abstract BusinessType getRequestType(Record record);

    protected abstract Long getDefaultRecordTypeId();

    protected abstract Long getOpenStatus();

    /**
     * Indicates if records with status == Record.STAT_CANCELED should
     * be ignored on search and count
     * @return
     */
    protected boolean isIgnoreCanceledStatusOnSearch() {
        return true;
    }

    public RecordType getRecordType() {
        Long typeId = getDefaultRecordTypeId();
        if (isEmpty(typeId)) {
            throw new BackendException("defaultRecordTypeId not provided by implementing class");
        }
        return getRecordTypeById(typeId);
    }

    protected RecordType getRecordTypeById(Long id) {
        return (RecordType) getCurrentSession().load(RecordTypeImpl.class, id);
    }

    protected final String getQueryContext() {
        RecordType type = getRecordType();
        return type.getTableName();
    }

    public Record load(Long id) {
        AbstractRecord result = (AbstractRecord) getCurrentSession().load(
                getPersistentClass(), id);
        loadAll(result);
        return result;
    }

    public final List<Record> load(List<Long> ids) {
        List<Record> result = new ArrayList<Record>();
        if (!ids.isEmpty()) {
            StringBuilder hql = new StringBuilder(128);
            hql
                    .append("from ")
                    .append(getPersistentClass().getName())
                    .append(" o where o.id in (")
                    .append(StringUtil.createCommaSeparated(ids))
                    .append(") order by o.id");
            result = getCurrentSession().createQuery(hql.toString()).list();
            loadAll(result);
        }
        return result;
    }

    public final void save(Record record) {
        getCurrentSession().saveOrUpdate(getPersistentClass().getName(), record);
    }

    protected final void saveInitial(Record record) {
        getCurrentSession().saveOrUpdate(
                getPersistentClass().getName(),
                recordPrintOptionDefaults.applyDefaults(record));
    }
    
    protected Record saveInitial(Employee user, Record record, Record source, boolean copyNote, boolean copyTerms) {
        if (copyNote && source != null && !isEmpty(source.getNote())) {
            record.setNote(source.getNote());
        }
        getCurrentSession().saveOrUpdate(
                getPersistentClass().getName(),
                recordPrintOptionDefaults.applyDefaults(record));
        if (copyTerms && source != null && !isEmpty(source.getInfos())) {
            List<Long> infos = new ArrayList<>();
            for (int i = 0, j = source.getInfos().size(); i < j; i++) {
                RecordInfo next = source.getInfos().get(i);
                infos.add(next.getInfoId());
            }
            if (!infos.isEmpty()) {
                record.addInfos(user, infos);
                save(record);
            }
        }
        return record;
    }

    public void delete(Record record, Employee employee) {
        Record obj = (Record) getCurrentSession().load(getPersistentClass().getName(), record.getId());
        deleteStatusHistory(obj);
        getCurrentSession().delete(obj);
        if (log.isInfoEnabled()) {
            log.info("delete() done [record=" + record.getId()
                    + ", type=" + (record.getType() != null ? record.getType().getId() : "null")
                    + ", contact=" + (record.getContact() != null ? record.getContact().getContactId() : "null")
                    + ", reference=" + record.getReference()
                    + ", created=" + record.getCreated()
                    + ", sales=" + record.getBusinessCaseId() + "]");
        }
    }

    public Record changeContact(Employee user, Record record, ClassifiedContact contact) {
        Record obj = (Record) getCurrentSession().load(getPersistentClass().getName(), record.getId());
        obj.changeContact(contact, user.getId());
        save(obj);
        if (log.isDebugEnabled()) {
            log.debug("changeCustomer() done [id=" + record.getId() + ", customer=" + obj.getContact().getId() + "]");
        }
        return load(obj.getId());
    }
    
    public final void updatePrintOptionDefaults(Record record) {
    	recordPrintOptionDefaults.saveDefaults(record);
    }

    protected Record updateItems(Record record, List<Item> items) {
        if (record != null) {
            Stock stock = getStockByRecord(record);
            record.getItems().clear();
            for (int i = 0, j = items.size(); i < j; i++) {
                Item next = items.get(i);
                Product product = next.getProduct();
                BigDecimal purchasePrice = product.getEstimatedPurchasePrice() == null 
                        ? new BigDecimal(0) : 
                            new BigDecimal(product.getEstimatedPurchasePrice().longValue());
                BigDecimal partnerPrice = product.getPartnerPrice() == null 
                        ? new BigDecimal(0) : 
                            new BigDecimal(product.getPartnerPrice().longValue());
                record.addItem(
                        stock != null ? stock.getId() : product.getDefaultStock(),
                        product,
                        next.getCustomName(),
                        next.getQuantity(),
                        next.getTaxRate(),
                        next.getPrice(),
                        new Date(),
                        partnerPrice,
                        false, // override pp
                        false, // pp overidden
                        purchasePrice,
                        next.getNote(),
                        true,
                        next.getExternalId());
            }
        }
        return record;
    }

    public List<Record> getOpen(boolean descending) {
        StringBuilder query = new StringBuilder(
                "from ")
                .append(getPersistentClass().getName())
                .append(" o where o.status < ")
                .append(getOpenStatus());
        if (descending) {
            query.append(" order by o.id desc");
        } else {
            query.append(" order by o.id");
        }
        List<Record> result = getCurrentSession().createQuery(query.toString()).list();
        loadAfterFind(result);
        return result;
    }

    public List<Record> getUnreleased() {
        StringBuilder query = new StringBuilder(64);
        query
                .append("from ")
                .append(getPersistentClass().getName())
                .append(" o where o.status < :statusid order by o.id desc");
        List<Record> result = getCurrentSession().createQuery(query.toString())
                .setParameter("statusid", Record.STAT_SENT).list();
        loadAfterFind(result);
        return result;
    }

    public List<Record> getOpenReleased(Product product, boolean descending) {
        StringBuilder query = new StringBuilder(
                "from ")
                .append(getPersistentClass().getName())
                .append(" o where o.status < ")
                .append(getOpenStatus());
        if (descending) {
            query.append(" order by o.id desc");
        } else {
            query.append(" order by o.id");
        }
        List<Record> result = getCurrentSession().createQuery(query.toString()).list();
        loadPayments(result);
        return result;
    }

    public final List<RecordStatusHistory> getStatusHistory(Long recordId, Long recordType) {
        String sql = RecordStatusHistoryRowMapper.createQueryString(
                getTable(TableKeys.RECORD_STATUS_INFO_VIEW), recordId, recordType);
        return (List<RecordStatusHistory>) jdbcTemplate.query(
                sql, RecordStatusHistoryRowMapper.getReader());
    }

    public Long getLastStatusBy(Record record) {
        String table = getTable(TableKeys.RECORD_STATUS_INFOS);
        if (table == null) {
            return null;
        }
        StringBuilder sql = new StringBuilder("select created_by from ");
        sql
                .append(table)
                .append(" where id in (select max(id) from ")
                .append(table)
                .append(" where reference_id = ")
                .append(record.getId())
                .append(" and type_id = ")
                .append(record.getType().getId())
                .append(")");
        List<Long> list = (List<Long>) jdbcTemplate.query(sql.toString(), getLongRowMapper());
        return list.isEmpty() ? null : list.get(0);
    }

    public void createStatusHistory(Record record, Long status, Long createdBy) {
        RecordStatusInfoImpl obj = new RecordStatusInfoImpl(createdBy, record, status);
        getCurrentSession().save(RecordStatusInfoImpl.class.getName(), obj);
    }

    private void deleteStatusHistory(Record record) {
        String hql = "from " + RecordStatusInfoImpl.class.getName() +
                " o where o.reference = :referenceId and o.type = :typeId";
        List<RecordStatusInfoImpl> sl = getCurrentSession().createQuery(hql.toString())
            .setParameter("referenceId", record.getId())
            .setParameter("typeId", record.getType().getId()).list();
        for (Iterator<RecordStatusInfoImpl> i = sl.iterator(); i.hasNext();) {
            getCurrentSession().delete(RecordStatusInfoImpl.class.getName(), i.next());
        }
    }

    public void setDefaultInfos(Employee user, Record record) {
        if (record.getType().isSupportingConditions()) {
            List<Long> defaults = getDefaultInfoConfig(record);
            record.addInfos(user, defaults);
            save(record);
        }
    }

    protected void setDefaultInfos(Employee user, Record record, BusinessType type) {
        if (record.getType().isSupportingConditions()) {
            List<Long> defaults = getDefaultInfoConfig(record, type);
            record.addInfos(user, defaults);
            save(record);
            if (log.isDebugEnabled()) {
                log.debug("setInfos() done [record=" + record.getId()
                        + ", count=" + defaults.size() + "]");
            }
        }
    }

    protected void setInfos(
            Employee user,
            Record record,
            Record other) {
        if (record.getType().isSupportingConditions()) {
            List<Long> configs = new ArrayList<Long>();
            for (int i = 0, j = other.getInfos().size(); i < j; i++) {
                RecordInfo next = other.getInfos().get(i);
                configs.add(next.getInfoId());
            }
            record.addInfos(user, configs);
            save(record);
            if (log.isDebugEnabled()) {
                log.debug("setInfos() done [record=" + record.getId()
                        + ", count=" + configs.size() + "]");
            }
        }
    }

    protected final List<ItemChangedInfo> createChangeset(Employee user, List<Item> currentItems, List<Item> previousItems) {

        List<ItemChangedInfo> result = new ArrayList<ItemChangedInfo>();

        Map<Long, Item> current = new HashMap<Long, Item>();
        for (int i = 0, j = currentItems.size(); i < j; i++) {
            Item next = currentItems.get(i);
            current.put(next.getProduct().getProductId(), next);
        }
        Map<Long, Item> previous = new HashMap<Long, Item>();
        for (int i = 0, j = previousItems.size(); i < j; i++) {
            Item next = previousItems.get(i);
            previous.put(next.getProduct().getProductId(), next);
        }

        for (int i = 0, j = currentItems.size(); i < j; i++) {
            Item currentItem = currentItems.get(i);
            Item previousItem = previous.get(currentItem.getProduct().getProductId());
            if (previousItem != null && !quantityPriceAndStockEquals(currentItem, previousItem)) {
                result.add(new ItemChangedInfoVO(user, currentItem, previousItem));
            } else if (previousItem == null) {
                result.add(new ItemChangedInfoVO(user, currentItem, false));
            }
        }
        for (int i = 0, j = previousItems.size(); i < j; i++) {
            Item previousItem = previousItems.get(i);
            Item currentItem = current.get(previousItem.getProduct().getProductId());
            if (currentItem == null) {
                result.add(new ItemChangedInfoVO(user, previousItem, true));
            }
        }
        return result;
    }

    /**
     * Fetches item changes of a record. Implementaions of records providing this feature must overide {@link #getItemChangedInfoClass()} to get results here.
     * @param id of the record
     * @return all item changed infos of an order
     */
    public final List<ItemChangedInfo> getItemChangedInfos(Long id) {
        Class<? extends ItemChangedInfo> infoClass = getItemChangedInfoClass();
        if (infoClass == null) {
            return new ArrayList<ItemChangedInfo>();
        }
        StringBuilder hql = new StringBuilder();
        hql
            .append("from ")
            .append(infoClass.getName())
            .append(" o where o.reference = :referenceid order by o.id desc");
        return getCurrentSession().createQuery(hql.toString()).setParameter("referenceid", id).list();
    }

    /**
     * Provides the itemChangedInfo class if implementing record supports this
     * @return itemChangedInfoClass or null if unsupported
     */
    protected Class<? extends ItemChangedInfo> getItemChangedInfoClass() {
        return null;
    }

    private boolean quantityPriceAndStockEquals(Item item, Item other) {
        if ((item == null && other == null)
                || (item == null && other != null)
                || (item != null && other == null)) {
            return false;
        }
        if (((item == null || item.getQuantity() == null) && other.getQuantity() != null)
                || ((item != null && item.getQuantity() != null) && other.getQuantity() == null)) {
            return false;
        }
        if ((item != null && item.getQuantity() != null) && other.getQuantity() != null) {
            if (!item.getQuantity().equals(other.getQuantity())) {
                return false;
            }
        }
        if (((item == null || item.getPrice() == null) && other.getPrice() != null)
                || ((item != null && item.getPrice() != null) && other.getPrice() == null)) {
            return false;
        }
        if ((item != null && item.getPrice() != null) && other.getPrice() != null) {
            if (!item.getPrice().equals(other.getPrice())) {
                return false;
            }
        }
        if (((item == null || item.getStockId() == null) && other.getStockId() != null)
                || ((item != null && item.getStockId() != null) && other.getStockId() == null)) {
            return false;
        }
        if ((item != null && item.getStockId() != null) && other.getStockId() != null) {
            if (!item.getStockId().equals(other.getStockId())) {
                return false;
            }
        }
        return true;
    }

    private List<Long> getDefaultInfoConfig(Record record) {
        return getDefaultInfoConfig(record, getRequestType(record));
    }

    private List<Long> getDefaultInfoConfig(Record record, BusinessType type) {
        return (infoConfigs == null ? new ArrayList<Long>() : infoConfigs.getDefaultInfos(
                record.getType().getId(),
                type == null ? null : type.getId()));
    }

    protected List<Long> getExistingInfoConfig(Record record) {
        List<Long> result = new ArrayList<>();
        for (int i = 0, j = record.getInfos().size(); i < j; i++) {
            result.add(record.getInfos().get(i).getInfoId());
        }
        return result;
    }

    /**
     * Override this if implementing record requires additional load operations. Default does nothing.
     * @param record
     */
    protected final void loadAll(Record record) {
    	int unsorted = 0;
    	for (int i = 0, j = record.getItems().size(); i < j; i++) {
			if (unsorted > 1) {
				break;
			}
    		Item next = record.getItems().get(i);
    		if (next.getOrderId() == 0) {
    			unsorted++;
    		}
    	}
    	if (unsorted > 1) {
    		if (record.isUnchangeable()) {
    			CollectionUtil.sort(record.getItems(), Comparators.createRecordItemByIdComparator());
    		} else {
    			for (int i = 0, j = record.getItems().size(); i < j; i++) {
    				Item next = record.getItems().get(i);
    				next.setOrderId(i);
    			}
        		save(record);
    		}
    	}
        if (isSupportingPayments()) {
            if (record instanceof PaymentAwareRecord) {
                PaymentAwareRecord pawr = (PaymentAwareRecord) record;
                if (pawr.getPayments().isEmpty()) {
                    loadPayments(pawr);
                }
            }
        }
        if (recordMailLogs != null) {
            List<RecordMailLog> logs = recordMailLogs.find(record);
            if (!logs.isEmpty() && record instanceof AbstractRecord) {
                ((AbstractRecord) record).setMailLogs(logs);
            }
        }
        loadAfterAll(record);
    }

    /**
     * Override this method to do some work after loadAll.
     * This method is invoked by loadAll.
     * @param record
     */
    protected void loadAfterAll(Record record) {
        // default does nothing
    }

    public Payment addCustomPayment(
            Employee user,
            PaymentAwareRecord record,
            BigDecimal amount,
            Date paid,
            String customHeader,
            String note,
            Long bankAccountId)
            throws ClientException {
        Payment payment = payments.createCustom(
                record,
                amount,
                paid,
                customHeader,
                note,
                bankAccountId,
                user);
        save(record);
        record.getPayments().add(payment);
        return payment;
    }

    public Payment addPayment(Employee user, PaymentAwareRecord record, Long paymentType, BigDecimal amount, Date paid, Long bankAccountId)
            throws ClientException {
        Payment payment = payments.create(paymentType, record, amount, paid, bankAccountId, user);
        save(record);
        record.getPayments().add(payment);
        return payment;
    }

    /**
     * Default does nothing. Override if derived class supports payments.
     * @param record
     */
    protected void loadPayments(PaymentAwareRecord record) {
        // override if required
    }

    protected void loadPayments(List<? extends FinanceRecord> result) {
        if (isSupportingPayments()) {
            for (int i = 0, j = result.size(); i < j; i++) {
                loadPayments((PaymentAwareRecord) result.get(i));
            }
        }
    }
    
    @Override
    protected void loadAfterFind(List<? extends FinanceRecord> resultingList) {
        loadPayments(resultingList);
    }

    public boolean isSupportingPayments() {
        return supportingPayments;
    }

    public void setSupportingPayments(boolean supportingPayments) {
        this.supportingPayments = supportingPayments;
    }

    protected Payments getPayments() {
        return payments;
    }

    protected void setPayments(Payments payments) {
        this.payments = payments;
    }

    /**
     * Invokes loadAll for every record
     * @param records
     */
    protected void loadAll(List records) {
        for (int i = 0, j = records.size(); i < j; i++) {
            loadAll((Record) records.get(i));
        }
    }

    public PaymentCondition loadPaymentCondition(Long id) {
        return paymentConditions.load(id);
    }

    protected PaymentCondition getPaymentConditionDefault(boolean sales) {
        return paymentConditions.getPaymentConditionDefault(sales);
    }

    /**
     * Provides next record id by associated recordNumberCreator
     * @param user
     * @param company
     * @param type
     * @param created
     * @return id
     */
    protected Long getNextRecordId(Employee user, Long company, RecordType type, Date created) {
        return recordNumberCreator.create(user, company, type, created);
    }

    /**
     * Indicates whether given record exists under given table name
     * @param tableName
     * @param recordId
     * @return true if so
     */
    protected boolean exists(String tableName, Long recordId) {
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("SELECT count(*) FROM ")
                .append(tableName)
                .append(" WHERE id = ?");
        final Object[] params = { recordId };
        final int[] types = { Types.BIGINT };
        return (jdbcTemplate.queryForObject(sql.toString(), params, types, Integer.class) > 0);
    }

    /**
     * Counts all records related to given reference
     * @param tableName
     * @param referenceId
     * @return count
     */
    protected int count(String tableName, Long referenceId) {
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("SELECT count(*) FROM ")
                .append(tableName)
                .append(" WHERE reference_id = ?");
        if (!isIgnoreCanceledStatusOnSearch()) {
            sql.append(" AND status <> 99");
        }
        final Object[] params = { referenceId };
        final int[] types = { Types.BIGINT };
        return jdbcTemplate.queryForObject(sql.toString(), params, types, Integer.class);
    }

    /**
     * Provides a list of all id's of implementing record whose status is printed or later.
     * @param tableName
     * @return ids of all printed records
     */
    protected List<Long> getPrinted(String tableName) {
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("SELECT id FROM ")
                .append(tableName)
                .append(" WHERE status >= ?");
        final Object[] params = { Record.STAT_PRINT };
        final int[] types = { Types.BIGINT };
        return (List<Long>) jdbcTemplate.query(sql.toString(), params, types, getLongRowMapper());
    }

    /**
     * Provides the created date of a record
     * @param tableName
     * @param id of the record
     * @return date when record was created
     */
    protected Date getCreated(String tableName, Long id) {
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("SELECT created FROM ")
                .append(tableName)
                .append(" WHERE id = ?");
        final Object[] params = { id };
        final int[] types = { Types.BIGINT };
        List<Date> list = (List<Date>) jdbcTemplate.query(sql.toString(), params, types, getTimestampRowMapper());
        return (list.isEmpty()) ? null : list.get(0);
    }

    protected Long getMostCurrentBySalesId(String tableName, Long businessId) {
        if (tableName == null) {
            return null;
        }
        StringBuilder sql = new StringBuilder("SELECT max(id) FROM ");
        sql
                .append(tableName)
                .append(" WHERE sales_id = ")
                .append(businessId)
                .append(" AND status <> ")
                .append(Record.STAT_CANCELED);
        List<Long> list = (List<Long>) jdbcTemplate.query(sql.toString(), getLongRowMapper());
        return list.isEmpty() ? null : list.get(0);
    }
    
    
    public Long getHighestId(Long company) {
        return getMinOrMax(company, "max");
    }

    public Long getLowestId(Long company) {
        return getMinOrMax(company, "min");
    }
    
    private Long getMinOrMax(Long company, String minOrMax) {
        RecordType type = null;
        try {
            type = getRecordType();
            if (isSet(type.getTableName())) {
                StringBuilder sql = new StringBuilder(64);
                sql
                        .append("SELECT ")
                        .append(minOrMax)
                        .append("(id) FROM ")
                        .append(type.getTableName())
                        .append(" WHERE company_id = ")
                        .append(company);
                return jdbcTemplate.queryForObject(sql.toString(), Long.class);
            }
        } catch (Exception e) {
            log.warn("getMinOrMax() failed [type=" 
                    + (type == null ? "null" : type.getId())
                    + ", company=" + company
                    + ", method=" + minOrMax
                    + ", message=" + e.getMessage() + "]");
        }
        return null;
    }

    /**
     * Provides the items of a record
     * @param tableName
     * @param recordId
     * @return items
     */
    protected List<ListItem> getItemListing(String tableName, Long recordId) {
        List<ListItem> list = new ArrayList<ListItem>();
        if (recordId != null) {
            StringBuilder sql = new StringBuilder(64);
            sql
                    .append(ListItemRowMapper.SELECT)
                    .append(tableName).append(" i,")
                    .append(getTable(TableKeys.PRODUCTS)).append(" a,")
                    .append(getTable(Options.QUANTITY_UNITS)).append(" u WHERE ")
                    .append("i.reference_id = ? AND i.product_id = a.product_id AND" +
                            " a.quantity_unit = u.id order by i.id");
            final Object[] params = { recordId };
            final int[] types = { Types.BIGINT };
            list = (List<ListItem>) jdbcTemplate.query(sql.toString(), params, types, getListItemRowMapper());
        }
        return list;
    }

    protected List<ListItem> getArtcleListing(
            String recordTableName,
            String itemTableName,
            Long productId) {

        if (productId != null) {
            StringBuilder sql = new StringBuilder(64);
            sql
                    .append(RecordProductRowMapper.SELECT_FROM)
                    .append(recordTableName)
                    .append(RecordProductRowMapper.POST_LOCAL_TABLE)
                    .append(itemTableName)
                    .append(RecordProductRowMapper.POST_LOCAL_ITEMS)
                    .append(getAdditionalProductListingAndClauses())
                    .append(getProductListingOrderByClause());
            final Object[] params = { productId };
            final int[] types = { Types.BIGINT };
            return (List<ListItem>) jdbcTemplate.query(
                    sql.toString(),
                    params,
                    types,
                    getRecordProductRowMapper());
        }
        return new ArrayList<ListItem>();
    }

    protected String getAdditionalProductListingAndClauses() {
        return "";
    }

    protected String getProductListingOrderByClause() {
        return RecordProductRowMapper.ORDER_BY_CLAUSE;
    }

    public List<RecordDisplay> getBySalesId(Long salesId) {
        String whereClause = " WHERE sales_id = ?";
        final Object[] params = new Object[] { salesId };
        final int[] types = new int[] { Types.BIGINT };
        return getView(whereClause, params, types, false);
    }

    public final List<RecordDisplay> findUnreleased(Long company) {

        StringBuilder query = new StringBuilder("SELECT * FROM v_");
        query
                .append(getQueryContext())
                .append("_query");
        if (company != null) {
            query
                .append(" WHERE company_id = ? AND status < ")
                .append(Record.STAT_SENT);
        } else {
            query.append(" WHERE status < ").append(Record.STAT_SENT);
        }
        query.append(" ORDER BY id DESC");
        List<Map<String, Object>> result = null;
        if (company != null) {
            result = (List<Map<String, Object>>) jdbcTemplate.query(
                    query.toString(),
                    new Object[] { company },
                    new int[] { Types.BIGINT },
                    RecordDisplayRowMapper.getReader());
        } else {
            result = (List<Map<String, Object>>) jdbcTemplate.query(
                    query.toString(),
                    RecordDisplayRowMapper.getReader());
        }
        return RecordDisplayRowMapper.createDisplayList(getOptionsCache(), result);
    }

    protected List<RecordDisplay> getView(
            String appendWhereClause,
            Object[] params,
            int[] types,
            boolean ignoreDefaultOrder) {
        StringBuilder query = new StringBuilder("SELECT * FROM v_");
        query.append(getQueryContext()).append("_query");
        if (appendWhereClause != null) {
            query.append(appendWhereClause);
        }
        if (!ignoreDefaultOrder) {
            query.append(" ORDER BY id");
        }
        List<Map<String, Object>> result = null;
        if (appendWhereClause != null) {
            result = (List<Map<String, Object>>) jdbcTemplate.query(
                    query.toString(),
                    params,
                    types,
                    RecordDisplayRowMapper.getReader());
        } else {
            result = (List<Map<String, Object>>) jdbcTemplate.query(
                    query.toString(),
                    RecordDisplayRowMapper.getReader());
        }
        return RecordDisplayRowMapper.createDisplayList(getOptionsCache(), result);
    }

    /**
     * Provides a payment agreement by business type and/or existing record. Method first attempts to fetch a valid payment agreement from given source.
     * {@link #getDefaultPaymentAgreement(BusinessType)} is invoked if source is null or source does not provide a valid payment agreement
     * @param type
     * @param source
     * @return paymentAgreement
     */
    protected final PaymentAgreement getPaymentAgreement(BusinessType type, PaymentAgreementAwareRecord source) {
        // TODO add paymentAgreement validation check
        if (source != null
                && source.getPaymentAgreement() != null
                && source.getPaymentAgreement().getDownpaymentPercent() != null
                && source.getPaymentAgreement().getDeliveryInvoicePercent() != null
                && source.getPaymentAgreement().getFinalInvoicePercent() != null
                && source.getPaymentAgreement().getPaymentCondition() != null) {
            return source.getPaymentAgreement();
        }
        return getDefaultPaymentAgreement(type);
    }

    /**
     * Provides the default payment agreement by business type or global default if none defined or provided.
     * @param businessType or null to lookup global default
     * @return paymentAgreement
     */
    protected final PaymentAgreement getDefaultPaymentAgreement(BusinessType businessType) {
        List<PaymentAgreement> result = new ArrayList<PaymentAgreement>();
        StringBuilder query = new StringBuilder();
        if (businessType != null) {
            query.append("from ").append(
                    DefaultPaymentAgreementImpl.class.getName()).append(
                    " o where o.reference = :businessType");
            result = getCurrentSession().createQuery(query.toString())
                    .setParameter("businessType", businessType.getId()).list();

        }
        if (result.isEmpty()) {
            query = new StringBuilder();
            query
                    .append("from ")
                    .append(DefaultPaymentAgreementImpl.class.getName())
                    .append(" o where o.common = true");
            result = getCurrentSession().createQuery(query.toString()).list();
        }
        return (result.isEmpty() ? null : result.get(0));
    }

    private RowMapperResultSetExtractor getListItemRowMapper() {
        return new RowMapperResultSetExtractor(new ListItemRowMapper());
    }

    private RowMapperResultSetExtractor getRecordProductRowMapper() {
        return new RowMapperResultSetExtractor(new RecordProductRowMapper());
    }

    public final boolean deletePermissionGrant(DomainUser user, Record record) {
        assert (record != null && user != null);
        if (!record.isUnchangeable()) {
            if (user.getId().equals(record.getCreatedBy())
                    // by convention: if record is not unchangeable, every
                    // user with permission to change may also delete without
                    // additional permission checking
                    || user.getId().equals(record.getChangedBy())) {
                
                if (log.isDebugEnabled()) {
                    log.debug("deletePermissionGrant() record is deleteable by user.id [record="
                            + record.getId() + ", user=" + user.getId() + "]");
                }
                return true;
            }
            if (isEmpty(deletePermissions) || user.isPermissionGrant(deletePermissions)) {
                
                if (log.isDebugEnabled()) {
                    log.debug("deletePermissionGrant() record is deleteable by permission [record="
                            + record.getId() + ", cause="
                            + (isEmpty(deletePermissions) ? "noconfig" : "assigned") 
                            + ", user=" + user.getId() + "]");
                }
                return true;
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("deletePermissionGrant() record is not deleteable by user.id or permission," 
                    + " checking permission by recordType [record="
                    + record.getId() + ", user=" + user.getId() 
                    + ", type=" + record.getType().getId() 
                    + ", required=" + StringUtil.createCommaSeparated(deletePermissions) + "]");
        }
        return deletePermissionGrantByRecordType(user, record, 
                user.isPermissionGrant(deletePermissions));
    }
    
    /**
     * Override this method with type specific rules to control the result of the
     * deletePermissionGrant method which finally invokes this method with its own result.
     * @param user
     * @param record
     * @param defaultPermissionGrant
     * @return depends on type specific implementation. Default implementation always
     * returns false if record is unchangeable or the value of defaultPermissionGrant.
     */
    protected boolean deletePermissionGrantByRecordType(
            DomainUser user, 
            Record record,
            boolean defaultPermissionGrant) {
        boolean result = record.isUnchangeable() ? false : defaultPermissionGrant;
        if (log.isDebugEnabled()) {
            log.debug("deletePermissionGrantByRecordType() done [record="
                    + record.getId() + ", result=" + result + ", cause="
                    + (record.isUnchangeable() ? "unchangeable" : "defaults") 
                    + ", user=" + user.getId() + "]");
        }
        return result;
    }

    protected String[] getDeletePermissions() {
        return deletePermissions;
    }

    protected void setDeletePermissions(String[] deletePermissions) {
        this.deletePermissions = deletePermissions;
    }

    protected void initializeDeletePermissions(String permissionString) {
        if (!isEmpty(permissionString)) {
            deletePermissions = StringUtil.getTokenArray(permissionString);
        }
    }

    // sub type support

    protected Class<? extends BookingType> getPersistentBookingTypeClass() {
        return null;
    }

    protected Long getDefaultBookingTypeId() {
        return null;
    }

    public List<BookingType> getBookingTypes() {
        return getBookingTypes(getPersistentBookingTypeClass());
    }

    public BookingType getBookingType(Long id) {
        return getBookingType(getPersistentBookingTypeClass(), id);
    }

    public BookingType getDefaultBookingType() {
        return getBookingType(getDefaultBookingTypeId());
    }

    private List<BookingType> getBookingTypes(Class<? extends BookingType> clazz) {
        if (clazz == null) {
            return new ArrayList<BookingType>();
        }
        StringBuilder query = new StringBuilder("from ");
        query
                .append(clazz.getName())
                .append(" o order by o.order");
        return getCurrentSession().createQuery(query.toString()).list();
    }

    private BookingType getBookingType(Class<? extends BookingType> clazz, Long id) {
        BookingType result = null;
        if (clazz != null) {
            List<BookingType> list = getBookingTypes(clazz);
            for (int i = 0, j = list.size(); i < j; i++) {
                BookingType next = list.get(i);
                if (next.getId().equals(id)) {
                    result = next;
                    break;
                }
            }
        }
        return result;
    }

    public Stock getStockByRecord(Record record) {
        return companies.getStockByBranch(record.getCompany(), record.getBranchId());
    }

    protected Stock getStockByBusinessCase(BusinessCase businessCase) {
        return companies.getStockByBranch(businessCase.getBranch().getReference(),
                businessCase.getBranch().getId());
    }
}
