/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 4, 2008 11:11:43 AM 
 * 
 */
package com.osserp.core.model.records;

import java.util.Date;

import com.osserp.common.beans.OptionImpl;

import com.osserp.core.customers.Customer;
import com.osserp.core.products.ProductSelectionConfig;
import com.osserp.core.sales.SalesOrderVolumeExportConfig;
import com.osserp.core.system.SystemCompany;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesOrderVolumeExportConfigImpl extends OptionImpl implements SalesOrderVolumeExportConfig {

    private SystemCompany company = null;
    private Customer customer = null;
    private SystemCompany exportCompany = null;
    private Customer exportCustomer = null;

    private boolean running = false;
    private boolean includeByDeliveryNote = false;
    private boolean salesReferenceBySales = false;
    private boolean calculateSalesPrice = false;
    private String exportProviderName = null;
    private String volumeProcessorName = null;
    private ProductSelectionConfig productSelectionConfig = null;

    private String createPermissions = null;
    private String displayPermissions = null;

    private boolean eol = false;
    private Date exportStart = null;
    private Date exportEnd = null;

    protected SalesOrderVolumeExportConfigImpl() {
        super();
    }

    public SystemCompany getCompany() {
        return company;
    }

    protected void setCompany(SystemCompany company) {
        this.company = company;
    }

    public Customer getCustomer() {
        return customer;
    }

    protected void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public SystemCompany getExportCompany() {
        return exportCompany;
    }

    public void setExportCompany(SystemCompany exportCompany) {
        this.exportCompany = exportCompany;
    }

    public Customer getExportCustomer() {
        return exportCustomer;
    }

    public void setExportCustomer(Customer exportCustomer) {
        this.exportCustomer = exportCustomer;
    }

    public boolean isIncludeByDeliveryNote() {
        return includeByDeliveryNote;
    }

    protected void setIncludeByDeliveryNote(boolean includeByDeliveryNote) {
        this.includeByDeliveryNote = includeByDeliveryNote;
    }

    public boolean isCalculateSalesPrice() {
        return calculateSalesPrice;
    }

    public void setCalculateSalesPrice(boolean calculateSalesPrice) {
        this.calculateSalesPrice = calculateSalesPrice;
    }

    public boolean getSalesReferenceBySales() {
        return salesReferenceBySales;
    }

    protected void setSalesReferenceBySales(boolean salesReferenceBySales) {
        this.salesReferenceBySales = salesReferenceBySales;
    }

    public String getExportProviderName() {
        return exportProviderName;
    }

    public void setExportProviderName(String exportProviderName) {
        this.exportProviderName = exportProviderName;
    }

    public String getVolumeProcessorName() {
        return volumeProcessorName;
    }

    public void setVolumeProcessorName(String volumeProcessorName) {
        this.volumeProcessorName = volumeProcessorName;
    }

    public ProductSelectionConfig getProductSelectionConfig() {
        return productSelectionConfig;
    }

    public void setProductSelectionConfig(ProductSelectionConfig productSelectionConfig) {
        this.productSelectionConfig = productSelectionConfig;
    }

    public String getCreatePermissions() {
        return createPermissions;
    }

    public void setCreatePermissions(String createPermissions) {
        this.createPermissions = createPermissions;
    }

    public String getDisplayPermissions() {
        return displayPermissions;
    }

    public void setDisplayPermissions(String displayPermissions) {
        this.displayPermissions = displayPermissions;
    }

    public Date getExportStart() {
        return exportStart;
    }

    public void setExportStart(Date exportStart) {
        this.exportStart = exportStart;
    }

    public Date getExportEnd() {
        return exportEnd;
    }

    public void setExportEnd(Date exportEnd) {
        this.exportEnd = exportEnd;
    }

    public boolean isEol() {
        return eol;
    }

    public void setEol(boolean eol) {
        this.eol = eol;
    }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }
}
