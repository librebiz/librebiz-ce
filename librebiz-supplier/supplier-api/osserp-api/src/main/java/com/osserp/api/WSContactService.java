/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 12.12.2012 
 * 
 */
package com.osserp.api;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface WSContactService {

    /**
     * Tries to fetch an existing contact by id.
     * @param email address to lookup for
     * @return contact object or null if not exists
     */
    WSContact fetchContactByEmail(String email);

    /**
     * Tries to fetch an existing contact by id.
     * @param contactId id of the contact to fetch
     * @return contact object or null if not exists
     */
    WSContact fetchContactById(Long contactId);

    /**
     * Gets an existing contact by id or throws runtime exception
     * @param contactId id of the ontact to get
     * @return contact object
     * @throws ServiceException if contact does not exist
     */
    WSContact getContactById(Long contactId) throws WSObjectException;

    /**
     * Updates an existing or creates a new contact
     * @param wsContact contact values to persist
     * @return created or updated contact object
     */
    WSContact createOrUpdate(WSContact wsContact);
}
