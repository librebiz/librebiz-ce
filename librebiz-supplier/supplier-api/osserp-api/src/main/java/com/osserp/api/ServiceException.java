/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 12.12.2012 
 * 
 */
package com.osserp.api;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ServiceException extends RuntimeException {
    private static final long serialVersionUID = 4L;

    public static final String INVALID_PRIMARY_KEY = "error.pk";
    
    private String method = null;
    private Class<? extends Object> sourceClass = null;

    /**
     * Creates a new ServiceException
     */
    public ServiceException() {
        super();
    }

    /**
     * Creates a new ServiceException
     * @param message
     */
    public ServiceException(String message) {
        super(message);
    }

    /**
     * Creates a new ServiceException
     * @param message
     * @param source
     */
    public ServiceException(String message, Class<? extends Object> source) {
        super(message);
        this.sourceClass = source;
    }

    /**
     * Creates a new ServiceException
     * @param method
     * @param message
     */
    public ServiceException(String method, String message) {
        super(message);
        this.method = method;
    }

    /**
     * Creates a new ServiceException
     * @param cause
     */
    public ServiceException(Throwable cause) {
        super(cause);
    }

    /**
     * Creates a new ServiceException
     * @param message
     * @param cause
     */
    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public String getMethod() {
        return method;
    }

    public Class<? extends Object> getSourceClass() {
        return sourceClass;
    }
}
