/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Feb 23, 2010 11:02:00 AM 
 * 
 */
package com.osserp.core.model.telephone;

import com.osserp.common.Option;
import com.osserp.common.beans.AbstractEntity;
import com.osserp.core.telephone.Telephone;
import com.osserp.core.telephone.TelephoneSystem;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public class TelephoneImpl extends AbstractEntity implements Telephone {
    private TelephoneSystem telephoneSystem = null;
    private Option telephoneType = null;
    private String mac = null;
    private boolean eol = false;

    protected TelephoneImpl() {
        super();
    }

    public TelephoneImpl(TelephoneSystem telephoneSystem, String mac, Option telephoneType) {
        super();
        this.telephoneSystem = telephoneSystem;
        this.mac = mac;
        this.telephoneType = telephoneType;
    }

    public TelephoneSystem getTelephoneSystem() {
        return telephoneSystem;
    }

    public void setTelephoneSystem(TelephoneSystem telephoneSystem) {
        this.telephoneSystem = telephoneSystem;
    }

    public Option getTelephoneType() {
        return telephoneType;
    }

    public void setTelephoneType(Option telephoneType) {
        this.telephoneType = telephoneType;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public boolean isEol() {
        return eol;
    }

    public void setEol(boolean eol) {
        this.eol = eol;
    }
}
