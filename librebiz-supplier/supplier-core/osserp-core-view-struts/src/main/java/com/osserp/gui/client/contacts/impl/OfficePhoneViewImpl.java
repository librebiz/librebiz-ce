/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 19, 2008 12:06:09 AM 
 * 
 */
package com.osserp.gui.client.contacts.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.util.CollectionUtil;
import com.osserp.common.web.Actions;
import com.osserp.common.web.ViewName;
import com.osserp.core.contacts.OfficePhone;
import com.osserp.core.employees.Employee;
import com.osserp.core.employees.EmployeeManager;
import com.osserp.core.system.BranchOffice;
import com.osserp.gui.client.contacts.EmployeeView;
import com.osserp.gui.client.contacts.OfficePhoneView;
import com.osserp.gui.client.impl.AbstractView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("officePhoneView")
public class OfficePhoneViewImpl extends AbstractView implements OfficePhoneView {
    private static Logger log = LoggerFactory.getLogger(OfficePhoneViewImpl.class.getName());
    private EmployeeView employeeView = null;
    private List<BranchOffice> branchs = new ArrayList<BranchOffice>();
    private BranchOffice selectedBranch = null;

    protected OfficePhoneViewImpl() {
        super();
        setKeepViewWhenExistsOnCreate(true);
    }

    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        setExitTarget(Actions.EXIT);
        this.employeeView = (EmployeeView) fetchView(request, EmployeeView.class);
        if (employeeView != null && employeeView.getBean() != null) {
            Employee selected = employeeView.getEmployee();
            if (log.isDebugEnabled()) {
                log.debug("init() invoked [employee=" + selected.getId() + "]");
            }
            this.branchs = new ArrayList<BranchOffice>();
            for (int i = 0, j = selected.getRoleConfigs().size(); i < j; i++) {
                BranchOffice next = selected.getRoleConfigs().get(i).getBranch();
                this.branchs.add(next);
                if (log.isDebugEnabled()) {
                    log.debug("init() found supported branch [id=" + next.getId() + "]");
                }
            }
            if (branchs.size() == 1) {
                this.selectBranch(branchs.get(0).getId());
            }
        } else if (employeeView == null) {
            log.warn("init() invoked when no employee view exists");
        } else {
            log.warn("init() invoked when employee view without selected employee exists");
        }
    }

    public EmployeeView getEmployeeView() {
        return employeeView;
    }

    protected void setEmployeeView(EmployeeView employeeView) {
        this.employeeView = employeeView;
    }

    public List<BranchOffice> getBranchs() {
        return branchs;
    }

    protected void setBranchs(List<BranchOffice> branchs) {
        this.branchs = branchs;
    }

    public void selectBranch(Long id) {
        if (log.isDebugEnabled()) {
            log.debug("selectBranch() invoked [id=" + id + "]");
        }
        if (id == null) {
            this.selectedBranch = null;
            setList(new ArrayList<OfficePhone>());
        } else {
            this.selectedBranch = (BranchOffice) CollectionUtil.getById(branchs, id);
            List<OfficePhone> phones = getEmployeeManager().getOfficePhones(selectedBranch.getId());
            setList(phones);
            if (log.isDebugEnabled()) {
                log.debug("selectBranch() done [id=" + selectedBranch.getId()
                        + ", phoneCount=" + phones.size() + "]");
            }
        }
    }

    public BranchOffice getSelectedBranch() {
        return selectedBranch;
    }

    protected void setSelectedBranch(BranchOffice selectedBranch) {
        this.selectedBranch = selectedBranch;
    }

    public void removeOfficePhone() {
        Employee employee = employeeView.getEmployee();
        if (employee.getInternalPhone() != null) {
            EmployeeManager manager = getEmployeeManager();
            manager.removeOfficePhone(getUser().getId(), employee);
            this.employeeView.refresh();
        }
    }

    @Override
    public void setSelection(Long id) {
        EmployeeManager manager = getEmployeeManager();
        if (isNotSet(id)) {
            manager.removeOfficePhone(getUser().getId(), employeeView.getEmployee());
        } else {
            OfficePhone selected = (OfficePhone) CollectionUtil.getById(getList(), id);
            manager.addOfficePhone(getUser().getId(), employeeView.getEmployee(), selected);
        }
        this.employeeView.refresh();
    }
}
