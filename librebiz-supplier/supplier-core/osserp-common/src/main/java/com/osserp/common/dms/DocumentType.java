/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 29, 2005 10:22:46 AM 
 * 
 */
package com.osserp.common.dms;

import com.osserp.common.ClientException;
import com.osserp.common.Option;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface DocumentType extends Option {

    /**
     * Documents with this persistence mode are stored in a database.
     */
    static final int DATABASE_STORE = 0;
    
    /**
     * Documents with this persistence mode are stored in the filesystem
     */
    static final int FILESYSTEM_STORE = 1;
    
    /**
     * Documents with this persistence mode are templates stored in the 
     * filesystem and will be used by a framework to render the final document.
     */
    static final int TEMPLATE = 2;

    /**
     * Documents with this persistence format are stored as is
     */
    static final int FORMAT_PLAIN = 0;
    
    /**
     * Documents with this persistence format are stored using default encryptor
     */
    static final int FORMAT_CRYPT = 1;
    
    static final String EMAIL_TEMPLATE_SUFFIX = "EmailTemplate";

    static final String EMAIL_TEMPLATE_TEXT = "EmailText";

    /**
     * Returns the count of this type in current context or null is not set
     * @return count
     */
    Long getCount();

    /**
     * Sets the count of this type in current context
     * @param count
     */
    void setCount(Long count);

    /**
     * Indicates if documentType is locked for changes. You have to enable the
     * lock when performing mass operations on existing documents (e.g. change
     * encoding, renaming, etc.) 
     * @return true if type is locked
     */
    boolean isLocked();

    /**
     * Enabled/disables lock for changes
     * @param locked
     */
    void setLocked(boolean locked);

    /**
     * The prefix for file names of this type. A valid prefix for osserp should something like this: 'xx_'
     * @return prefix
     */
    String getPrefix();

    /**
     * Provides the postfix for filenames
     * @return postfix
     */
    String getPostfix();

    /**
     * The suffix for file names
     * @return suffix
     */
    String getSuffix();

    /**
     * Indicates that documents implementing this type are using the referenceType property to separate different documents with same reference id.
     * @return supportingReferenceType
     */
    boolean isSupportingReferenceType();

    /**
     * Indicates that only a single document is referenced to related object
     * @return overwrite
     */
    boolean isOverwrite();

    /**
     * Provides the context name
     * @return contextName
     */
    String getContextName();

    /**
     * Sets the context name
     * @param contextName
     */
    void setContextName(String contextName);

    /**
     * Provides the context path
     * @return contextPath
     */
    String getContextPath();

    /**
     * Sets the context path
     * @param contextPath
     */
    void setContextPath(String contextPath);

    /**
     * Provides the upload path
     * @return uploadPath
     */
    String getUploadPath();

    /**
     * Provides the full qualified download url
     * @return downloadUrl
     */
    String getDownloadUrl();

    /**
     * Provides the url to access the binary document data
     * @return printUrl or empty string if not provided
     */
    String getPrintUrl();
    
    /**
     * Provides the url to the documents parent entity or context.  
     * @return referenceUrl
     */
    String getReferenceUrl();
    
    /**
     * Provides the naming style to create filenames of type
     * @return namingStyle
     */
    String getNamingStyle();

    /**
     * Provides the persistence mode type
     * 0 - DATABASE_STORE 
     * 1 - FILESYSTEM_STORE
     * 2 - TEMPLATE 
     * @return persistence mode
     */
    int getPersistenceMode();

    /**
     * Provides the persistence format
     * 0 - PLAIN 
     * @return persistence format
     */
    int getPersistenceFormat();
    
    /**
     * Updates peristence format
     * @param format
     */
    void setPersistenceFormat(int format);
    
    /**
     * Indicates if documents of this type are stored in plain format,
     * e.g. default persistenceFormat == 0. 
     * @return true for plain persistence format
     */
    boolean isFormatPlain();

    /**
     * This is a convenience method to ask for dedicated persistence mode
     * @return true if persistenceMode == DATABASE_STORE
     */
    boolean isDatabaseStore();
    
    /**
     * This is a convenience method to ask for dedicated persistence mode
     * @return true if persistenceMode == FILESYSTEM_STORE
     */
    boolean isFilesystemStore();
    
    /**
     * This is a convenience method to ask for dedicated persistence mode
     * @return true if persistenceMode == TEMPLATE
     */
    boolean isTemplate();

    /**
     * Indicates that an additional note is required when creating new documents of implementing type
     * @return noteRequired
     */
    boolean isNoteRequired();

    /**
     * Indicates that suffix is provided by file
     * @return suffixByFile
     */
    boolean isSuffixByFile();

    /**
     * A comma seperated list of file suffix not allowed for upload.
     * @param preventSuffix
     */
    String getPreventSuffix();

    /**
     * Sets a comma seperated list of file suffix not allowed for upload.
     * @param preventSuffix
     */
    void setPreventSuffix(String preventSuffix);
    
    /**
     * Validates file suffix rules
     * @param fileSuffix
     */
    void validateSuffix(String fileSuffix) throws ClientException;

    /**
     * Indicates that documentType supports the selection of one document as reference
     * @return referenceSelection
     */
    boolean isReferenceSelection();
    
    /**
     * Provides the name of the service responsible for documents referencing
     * implementing type (e.g. associated dmsManager)
     * @return serviceName
     */
    String getServiceName();

    /**
     * Use original filename instead of the internal filename for download. 
     * @return
     */
    boolean isUseFilenameForDownload();
    
    /**
     * Toggles useFilenameForDownload setting
     */
    void toggleFilenameForDownload();
    
    /**
     * Indicates if type is configurable via admin gui
     * @return true if configurable
     */
    boolean isConfigurable();
    
    /**
     * Indicates if encryption is supported for this type
     * @return true if encryption supported
     */
    boolean isEncryptionSupported();
    
    /**
     * Indicates if validity period is supported for this type
     * @return true if validity period supported
     */
    boolean isValidityPeriodSupported();
    
    /**
     * Indicates if type is searchable via common search forms
     * @return true if searchable
     */
    boolean isSearchable();

    boolean isCreateThumbmail();

    void setCreateThumbmail(boolean createThumbmail);

    int getThumbmailWidth();

    void setThumbmailWidth(int thumbmailWidth);

    int getThumbmailHeight();

    void setThumbmailHeight(int thumbmailHeight);
}
