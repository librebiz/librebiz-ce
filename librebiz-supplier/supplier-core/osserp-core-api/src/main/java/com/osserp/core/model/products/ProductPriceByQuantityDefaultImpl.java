/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 13, 2011 8:34:39 PM 
 * 
 */
package com.osserp.core.model.products;

import java.util.ArrayList;
import java.util.List;

import com.osserp.common.beans.AbstractOption;

import com.osserp.core.employees.Employee;
import com.osserp.core.products.ProductPriceByQuantityDefault;
import com.osserp.core.products.ProductPriceByQuantityDefaultRange;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProductPriceByQuantityDefaultImpl extends AbstractOption implements ProductPriceByQuantityDefault {

    private Long typeId;
    private Long groupId;
    private Long categoryId;
    private Long productId;
    private List<ProductPriceByQuantityDefaultRange> rangeList = new ArrayList<ProductPriceByQuantityDefaultRange>();

    protected ProductPriceByQuantityDefaultImpl() {
        super();
    }

    public void addRange(Employee user, Double rangeEnd) {
        int rangeListSize = rangeList.size();
        if (rangeListSize == 0) {
            ProductPriceByQuantityDefaultRange initial = new ProductPriceByQuantityDefaultRangeImpl(user, this, 0d, rangeEnd);
            this.rangeList.add(initial);
        } else {
            Double currentMax = rangeListSize == 0 ? 0d : rangeList.get(rangeListSize - 1).getRangeEnd();
            ProductPriceByQuantityDefaultRange range = new ProductPriceByQuantityDefaultRangeImpl(user, this, currentMax, rangeEnd);
            this.rangeList.add(range);
        }
    }

    public void removeRange() {
        if (!rangeList.isEmpty()) {
            this.rangeList.remove(rangeList.size() - 1);
        }
    }

    public List<ProductPriceByQuantityDefaultRange> getRanges() {
        return new ArrayList<ProductPriceByQuantityDefaultRange>(rangeList);
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    protected List<ProductPriceByQuantityDefaultRange> getRangeList() {
        return rangeList;
    }

    protected void setRangeList(List<ProductPriceByQuantityDefaultRange> rangeList) {
        this.rangeList = rangeList;
    }
}
