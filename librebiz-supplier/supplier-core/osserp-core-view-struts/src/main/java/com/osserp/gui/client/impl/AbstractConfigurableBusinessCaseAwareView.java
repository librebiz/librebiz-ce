/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 18, 2008 12:42:59 PM 
 * 
 */
package com.osserp.gui.client.impl;

import com.osserp.common.PermissionException;
import com.osserp.core.BusinessCase;
import com.osserp.core.BusinessCaseManager;
import com.osserp.core.users.DomainUser;
import com.osserp.core.users.Permissions;
import com.osserp.gui.client.BusinessCaseAwareView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractConfigurableBusinessCaseAwareView extends AbstractDomainView
        implements BusinessCaseAwareView {

    /**
     * Default constructor. Sets view name if annotated and initializes action target as 'success'
     */
    protected AbstractConfigurableBusinessCaseAwareView() {
        super();
    }

    public BusinessCase getBusinessCase() {
        Object bean = getBean();
        if (bean != null && bean instanceof BusinessCase) {
            return (BusinessCase) bean;
        }
        return null;
    }

    protected abstract BusinessCaseManager getBusinessCaseManager();

    protected boolean isSalesContext() {
        BusinessCase bc = getBusinessCase();
        return bc != null && bc.isSalesContext();
    }

    // access control

    public boolean isUserRelatedSales() {
        if (isDomainView()) {
            return getDomainUser().isUserRelatedSales(getBusinessCase());
        }
        return false;
    }

    public boolean isUserRelatedCoSales() {
        if (isDomainView()) {
            return getDomainUser().isUserRelatedCoSales(getBusinessCase());
        }
        return false;
    }

    public boolean isUserForeignSales() {
        if (isDomainView()) {
            return getDomainUser().isUserForeignSales(getBusinessCase());
        }
        return false;
    }

    public boolean isUserRelatedManager() {
        if (isDomainView()) {
            return getDomainUser().isUserRelatedManager(getBusinessCase());
        }
        return false;
    }

    public boolean isUserResponsibleManager() {
        if (!isDomainView()) {
            return false;
        }
        return getDomainUser().isResponsibleManager(getBusinessCase());
    }

    /**
     * Method is used by ProjectDisplayForwarder
     * @throws PermissionException
     */
    protected void checkSalesAccess() throws PermissionException {
        DomainUser user = getDomainUser();
        if (user != null) {
            user.checkSalesAccess(getBusinessCase());
        }
    }

    protected boolean isManagerExecutive(DomainUser user) {
        return hasPermission(user, Permissions.EXECUTIVE_TECSTAFF);
    }

    private boolean hasPermission(DomainUser user, String permission) {
        return (user.getPermissions().containsKey(permission));
    }

}
