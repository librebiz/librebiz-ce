<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<c:set var="view" value="${sessionScope.officePhoneView}"/>

<div class="modalBoxHeader">
	<div class="modalBoxHeaderLeft">
		<fmt:message key="office"/>
	</div>
</div>
<div class="modalBoxData">
	<div class="subcolumns">
		<div class="subcolumn">
			<div class="spacer"></div>
			<table class="table">
				<tbody>
					<c:if test="${!empty view}">
						<c:choose>
							<c:when test="${empty view.selectedBranch}">
								<o:logger write="invoked, no branch selected..." level="debug"/>
								<tr>
									<th><fmt:message key="selection"/></th>
									<th><fmt:message key="branch"/></th>
								</tr>
								<c:forEach var="obj" items="${view.branchs}">
									<tr>
										<td><o:out value="${obj.company.name}"/></td>
										<td>
											<o:ajaxLink url="/officePhones.do?method=selectBranch&id=${obj.id}" linkId="branch_selection" targetElement="office_phone_popup">
												<o:out value="${obj.name}"/>
											</o:ajaxLink>
										</td>
									</tr>
								</c:forEach>
							</c:when>
							<c:when test="${!view.editMode}">
								<o:logger write="branch selected, edit mode disabled..." level="debug"/>
								<tr>
									<th><o:out value="${view.selectedBranch.company.name}"/></th>
									<th><o:out value="${view.selectedBranch.name}"/></th>
								</tr>
								<tr>
									<td style="vertical-align:middle;"><fmt:message key="selection"/></td>
									<td>
										<form id="jumpForm" name="jumpForm" onChange="jumpToFormUrl('jumpForm')">
											<select name="jumpUrl" size="1">
												<c:if test="${!empty view.employeeView.bean.internalPhone}">
													<option value="<c:url value="/officePhones.do"><c:param name="method" value="removeOfficePhone"/></c:url>"><fmt:message key="deleteExternelNumber"/></option>
												</c:if>
												<c:set var="phoneSelected" value="false"/>
												<c:forEach var="phone" items="${view.list}">
													<c:choose>
														<c:when test="${!empty view.employeeView.bean.internalPhone and view.employeeView.bean.internalPhone.id == phone.id}">
															<option value="<c:url value="/officePhones.do"><c:param name="method" value="select"/><c:param name="id" value="${phone.id}"/></c:url>" selected="selected"><o:out value="${phone.country}"/> - <o:out value="${phone.prefix}"/> - <o:out value="${phone.number}"/></option>
															<c:set var="phoneSelected" value="true"/>
														</c:when>
														<c:otherwise>
															<option value="<c:url value="/officePhones.do"><c:param name="method" value="select"/><c:param name="id" value="${phone.id}"/></c:url>"><o:out value="${phone.country}"/> - <o:out value="${phone.prefix}"/> - <o:out value="${phone.number}"/></option>
														</c:otherwise>
													</c:choose>
												</c:forEach>
												<c:if test="${!phoneSelected}">
													<option value="0" selected="selected"><fmt:message key="selection"/></option>
												</c:if>
											</select>
										</form>
									</td>
								</tr>
							</c:when>
							<c:otherwise>
							</c:otherwise>
						</c:choose>
						<o:logger write="done, [viewName=${view.name}, exitTarget=${view.exitTarget}]..." level="debug"/>
					</c:if>
                    <tr>
                        <td class="row-submit">
                        </td>
                        <td class="row-submit">
                            <o:ajaxLink url="/officePhones.do?method=selectBranch" linkId="branch_selection" targetElement="office_phone_popup">
                                <input type="button" name="submit" value="<fmt:message key="exit"/>" class="cancel"/>
                            </o:ajaxLink>
                        </td>
                    </tr>
				</tbody>
			</table>
			<div class="spacer"></div>
		</div>
	</div>
</div>