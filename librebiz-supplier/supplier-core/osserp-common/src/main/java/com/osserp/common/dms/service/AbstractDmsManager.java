/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 4, 2009 7:53:55 PM 
 * 
 */
package com.osserp.common.dms.service;

import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.FileObject;
import com.osserp.common.User;
import com.osserp.common.dms.DmsDocument;
import com.osserp.common.dms.DocumentData;
import com.osserp.common.dms.DocumentType;
import com.osserp.common.dms.dao.Documents;
import com.osserp.common.util.FileUtil;
import com.osserp.common.util.StringUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractDmsManager {
    private Documents documents;

    /**
     * Creates a new dms manager supporting a dedicated document type
     * @param documents
     */
    protected AbstractDmsManager(Documents documents) {
        this.documents = documents;
    }

    /**
     * Provides the associated document dao
     * @return documents dao
     */
    protected final Documents getDocumentsDao() {
        return documents;
    }

    public boolean isDedicatedTypeManager() {
        return false;
    }

    public int countByType(DocumentType documentType) {
        return documents.countByType(documentType);
    }

    public List<DmsDocument> findByMessageId(String messageId) {
        return documents.findByMessageId(messageId);
    }

    public List<DmsDocument> findByType(DocumentType documentType) {
        return documents.findByType(documentType);
    }

    public Long getDefaultCategoryId(Long documentType) {
        return documents.getDefaultCategoryId(documentType);
    }

    public DmsDocument findDocument(Long id) {
        return documents.findById(id);
    }

    public DmsDocument getDocument(Long id) throws ClientException {
        try {
            return documents.getById(id);
        } catch (Exception e) {
            // catch runtime exception of dao
            throw new ClientException(ErrorCode.DOCUMENT_NOT_FOUND);
        }
    }

    public final DocumentData getDocumentData(DmsDocument document) {
        return new DocumentData(documents.getDocumentData(document), getDocumentDataFilename(document));
    }

    /**
     * This method will be used by getDocumentData to fetch the filename. 
     * @param document
     * @return filename to set as header on download
     */
    protected String getDocumentDataFilename(DmsDocument document) {
        if (document.getType() != null 
                && document.getType().isUseFilenameForDownload()
                && document.getFileName() != null
                && FileUtil.providesType(document.getFileName())) {
            return StringUtil.deleteBlanks(document.getFileName());
        }
        return document.getRealFileName();
    }

    public final DocumentData getThumbnailData(DmsDocument document) {
        return new DocumentData(documents.getThumbnailData(document), getDocumentDataFilename(document));
    }

    public void setAsReference(Long id) {
        documents.setAsReference(id);
    }

    public void updateDocument(User user, DmsDocument document, FileObject file, String note, Long category) throws ClientException {
        documents.update(user, document, file, note, category);
    }

    public void updateDocument(User user, Long id, Long categoryId) {
        documents.updateCategory(user, id, categoryId);
    }

    public void updateMetadata(User user, DmsDocument document, String note, Date validFrom, Date validTil) throws ClientException {
        documents.updateMetadata(user, document, note, validFrom, validTil);
    }

    public int changeFormat(DocumentType documentType) {
        int result = 0;
        if (documentType != null) {
            List<DmsDocument> all = documents.findByType(documentType);
            int format = documentType.getPersistenceFormat();
            for (int i = 0, j = all.size(); i < j; i++) {
                DmsDocument next = all.get(i);
                documents.changeFormat(next, format);
                result++;
            }
        }
        return result;
    }
}
