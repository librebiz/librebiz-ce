/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 14, 2006 4:30:35 PM 
 * 
 */
package com.osserp.core.dao.records;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;

import com.osserp.core.customers.Customer;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Cancellation;
import com.osserp.core.finance.CreditNote;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.ListItem;
import com.osserp.core.finance.Order;
import com.osserp.core.products.Product;
import com.osserp.core.sales.Sales;
import com.osserp.core.sales.SalesCreditNoteType;
import com.osserp.core.sales.SalesInvoice;
import com.osserp.core.system.BranchOffice;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SalesInvoices extends Invoices, PaymentAwareRecords, SalesRecords {

    int countBySales(Long salesId);

    /**
     * Creates a new invoice by an order
     * @param user creating the invoice
     * @param order new invoice should be based on
     * @param invoiceType
     * @param thirdParty if invoice recipient differs from orders customer
     * @param partialInvoiceAmount
     * @param partialInvoiceAmountGross
     * @param copyNote
     * @param copyTerms
     * @param copyItems
     * @param userDefinedId
     * @param userDefinedDate
     * @return invoice new created
     */
    SalesInvoice create(
            Employee user, 
            Order order, 
            Long invoiceType, 
            Customer thirdParty, 
            BigDecimal partialInvoiceAmount, 
            BigDecimal partialInvoiceAmountGross,
            boolean copyNote,
            boolean copyTerms,
            boolean copyItems,
            Long userDefinedId, 
            Date userDefinedDate) throws ClientException;

    /**
     * Creates a new empty invoice
     * @param user
     * @param office
     * @param customer
     * @param userDefinedId use a user defined record id
     * @param userDefinedDate an optional user defined date. Will be set as created date if provided
     * @param historical indicated if record was created by external app initially
     * @return invoice new created
     */
    SalesInvoice create(Employee user, BranchOffice office, Customer customer, Long userDefinedId, Date userDefinedDate, boolean historical);

    /**
     * Creates a partial invoice
     * @param user
     * @param order
     * @param percentage
     * @param amount
     * @param product
     * @param note
     * @param printOrderItems
     * @param copyNote
     * @param copyTerms
     * @param userDefinedId
     * @param userDefinedDate
     * @return salesInvoice new created
     * @throws ClientException if validation failed
     */
    SalesInvoice create(
            Employee user,
            Order order,
            boolean percentage,
            BigDecimal amount,
            Product product,
            String note,
            boolean printOrderItems,
            boolean copyNote,
            boolean copyTerms,
            Long userDefinedId, 
            Date userDefinedDate) throws ClientException;

    /**
     * Creates a new invoice by copying an existing invoice
     * @param user
     * @param salesInvoice
     * @param copyReferenceId
     * @param userDefinedId
     * @param userDefinedDate
     * @param historical
     * @return invoice
     */
    SalesInvoice create(Employee user, SalesInvoice salesInvoice, boolean copyReferenceId, Long userDefinedId, Date userDefinedDate, boolean historical);

    /**
     * Updates the branch office of all changeable payment depending sales 
     * records with the branch office provided by sales.
     * @param sales
     */
    void changePaymentRecordsBranch(Sales sales);

    /**
     * Changes the number of a sales invoice and direct references. This method ignores any status and other than direct referenced dependencies. Use with care!
     * @param user
     * @param salesInvoice
     * @param id
     * @return
     */
    SalesInvoice changeNumber(Employee user, SalesInvoice salesInvoice, Long id);
    
    /**
     * Changes historical status setting
     * @param invoice
     * @param enable
     * @return invoice with updated status
     */
    SalesInvoice changeHistoricalStatus(SalesInvoice invoice, boolean enable);

    /**
     * Adds a credit note to invoice
     * @param user
     * @param invoice
     * @param bookingType
     * @param copyItems
     * @return creditNote new created
     */
    CreditNote add(Employee user, Invoice invoice, SalesCreditNoteType bookingType, boolean copyItems);

    /**
     * Cancels an invoice
     * @param user
     * @param invoice to cancel
     * @param date
     * @param customId
     * @return cancellation new created
     */
    SalesInvoice cancel(Employee user, SalesInvoice invoice, Date date, Long customId) throws ClientException;

    /**
     * Restores canceled invoice
     * @param user
     * @param cancellation
     * @return restored invoice
     * @throws ClientException if restore not supported or user has no permission grant
     */
    SalesInvoice restoreCanceled(Employee user, Cancellation cancellation) throws ClientException;

    /**
     * Provides all records containing an product
     * @param productId to lookup for
     * @return artcle listing
     */
    List<ListItem> getProductListing(Long productId);

    /**
     * Provides all records of implementing type related to a contact
     * @param contactId
     * @return related
     */
    List<SalesInvoice> getByContact(Long contactId);

    /**
     * Provides all sales invoices related to given sales order
     * @param order
     * @return invoices or empty list
     */
    List<SalesInvoice> getByOrder(Order order);

    /**
     * Provides all sales invoices related to given sales
     * @param sales
     * @return invoices or empty list
     */
    List<SalesInvoice> getBySales(Sales sales);
    
    /**
     * Provides all invoices with existing payments dated between provided
     * start end end date
     * @param startDate
     * @param endDate
     * @return invoices with payments in period
     */
    List<SalesInvoice> getByPayments(Date startDate, Date endDate);

    /**
     * Provides all partial invoices referencing final invoice
     * @param invoice
     * @return partial invoices or empty list
     */
    List<SalesInvoice> getPartialByInvoice(SalesInvoice invoice);

    /**
     * Provides all partial invoices referencing sale
     * @param sales
     * @return partial invoices or empty list
     */
    List<SalesInvoice> getPartialBySales(Sales sales);

    /**
     * Provides all invoices of third party customers
     * @param sales
     * @return third party invoices or empty list
     */
    List<SalesInvoice> getThirdPartyBySales(Sales sales);

    /**
     * Looks up for all sales invoices related to given sales
     * and counts there net amount summary
     * @param sales
     * @return summary amount of all related invoices
     */
    Double getSummary(Sales sales);
}
