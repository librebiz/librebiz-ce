/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Sep 15, 2007 10:30:55 AM 
 * 
 */
package com.osserp.core.model.contacts;

import java.util.Date;

import org.jdom2.Element;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.common.XmlAwareEntity;
import com.osserp.common.beans.AbstractClass;
import com.osserp.common.beans.OptionImpl;
import com.osserp.common.util.DateFormatter;
import com.osserp.common.xml.JDOMUtil;

import com.osserp.core.Address;
import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.ContactUtil;
import com.osserp.core.contacts.ContactType;
import com.osserp.core.contacts.Person;
import com.osserp.core.contacts.Phone;
import com.osserp.core.contacts.Salutation;
import com.osserp.core.model.AddressImpl;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author so <so@osserp.com>
 * 
 */
public abstract class AbstractPerson extends AbstractClass implements Person, XmlAwareEntity {

    private ContactType type;
    private Long status;
    private Salutation salutation;
    private Option title;
    private String lastName;
    private String firstName;
    private boolean firstNamePrefix = false;
    private Salutation spouseSalutation;
    private Option spouseTitle;
    private String spouseFirstName;
    private String spouseLastName;
    private String userSalutation;
    private String userNameAddressField;

    private Date personCreated;
    private Long personCreatedBy;
    private Date personChanged;
    private Long personChangedBy;

    private Address address = new AddressImpl();
    
    private String externalReference;
    private String externalUrl;
    

    protected AbstractPerson() {
        super();
    }

    protected AbstractPerson(Person otherValue) {
        update(otherValue);
    }

    /**
     * Default constructor to create new persons
     * @param createdBy
     * @param type
     * @param salutation
     * @param title
     * @param firstNamePrefix
     * @param firstName
     * @param lastName
     * @param street
     * @param streetAddon
     * @param zipcode
     * @param city
     * @param country
     * @param federalState
     * @param federalStateName
     * @param district
     * @param initialStatus
     * @param validate
     * @param externalReference
     * @param externalUrl
     */
    protected AbstractPerson(
            Long createdBy,
            ContactType type,
            Salutation salutation,
            Option title,
            boolean firstNamePrefix,
            String firstName,
            String lastName,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long country,
            Long federalState,
            String federalStateName,
            Long district,
            Long initialStatus,
            boolean validate,
            String externalReference,
            String externalUrl)
            throws ClientException {

        this.type = type;
        this.status = initialStatus == null ? Contact.STATUS_CREATED : initialStatus;
        this.externalReference = externalReference;
        this.externalUrl = externalUrl;
        this.salutation = salutation;
        this.title = title;
        this.firstNamePrefix = firstNamePrefix;
        if (firstName != null && firstName.length() > 0) {
            this.firstName = firstName.trim();
        }
        if (lastName != null && lastName.length() > 0) {
            this.lastName = lastName.trim();
        } else if (validate) {
            if (type.isBusiness()) {
                throw new ClientException(ErrorCode.COMPANY_MISSING);
            }
            throw new ClientException(ErrorCode.LASTNAME_MISSING);
        }
        personCreated = new Date(System.currentTimeMillis());
        personCreatedBy = createdBy;
        address = new AddressImpl(
                null, // name
                street,
                streetAddon,
                zipcode,
                city,
                country,
                federalState,
                federalStateName,
                district);
    }

    /**
     * Currently invoked by related contacts
     * @param type
     * @param status
     * @param salutation
     * @param title
     * @param firstNamePrefix
     * @param firstName
     * @param lastName
     * @param addressName
     * @param street
     * @param streetAddon
     * @param zipcode
     * @param city
     * @param federalState
     * @param federalStateName
     * @param country
     * @param contactCreated
     * @param contactCreatedBy
     * @param contactChanged
     * @param contactChangedBy
     */
    protected AbstractPerson(
            ContactType type,
            Long status,
            Salutation salutation,
            Option title,
            boolean firstNamePrefix,
            String firstName,
            String lastName,
            String addressName,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long federalState,
            String federalStateName,
            Long country,
            Date contactCreated,
            Long contactCreatedBy,
            Date contactChanged,
            Long contactChangedBy) {

        this(
                type,
                status,
                salutation,
                title,
                firstNamePrefix,
                firstName,
                lastName,
                addressName,
                street,
                streetAddon,
                zipcode,
                city,
                country,
                federalState,
                federalStateName,
                null,
                contactCreated,
                contactCreatedBy,
                contactChanged,
                contactChangedBy);
    }

    /**
     * Currently invoked by interests
     * @param type
     * @param salutation
     * @param title
     * @param firstNamePrefix
     * @param firstName
     * @param lastName
     * @param street
     * @param streetAddon
     * @param zipcode
     * @param city
     * @param federalState
     * @param federalStateName
     * @param country
     */
    protected AbstractPerson(
            ContactType type,
            Salutation salutation,
            Option title,
            boolean firstNamePrefix,
            String firstName,
            String lastName,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long federalState,
            String federalStateName,
            Long country) {

        this(
                type,
                null,
                salutation,
                title,
                firstNamePrefix,
                firstName,
                lastName,
                null,
                street,
                streetAddon,
                zipcode,
                city,
                country,
                federalState,
                federalStateName,
                null,
                new Date(System.currentTimeMillis()),
                Constants.SYSTEM_EMPLOYEE,
                null,
                null);
    }

    /**
     * Currently invoked by related contacts
     * @param type
     * @param status
     * @param salutation
     * @param title
     * @param firstNamePrefix
     * @param firstName
     * @param lastName
     * @param addressName
     * @param street
     * @param streetAddon
     * @param zipcode
     * @param city
     * @param country
     * @param federalState
     * @param federalStateName
     * @param district
     * @param contactCreated
     * @param contactCreatedBy
     * @param contactChanged
     * @param contactChangedBy
     */
    protected AbstractPerson(
            ContactType type,
            Long status,
            Salutation salutation,
            Option title,
            boolean firstNamePrefix,
            String firstName,
            String lastName,
            String addressName,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long country,
            Long federalState,
            String federalStateName,
            Long district,
            Date contactCreated,
            Long contactCreatedBy,
            Date contactChanged,
            Long contactChangedBy) {

        this(type, salutation, title, firstNamePrefix, firstName, lastName);
        this.status = status == null ? Contact.STATUS_CREATED : status;
        this.address = new AddressImpl(
                addressName,
                street,
                streetAddon,
                zipcode,
                city,
                country,
                federalState,
                federalStateName,
                district);
        this.personCreated = contactCreated;
        this.personCreatedBy = contactCreatedBy;
        this.personChanged = contactChanged;
        this.personChangedBy = contactChangedBy;
    }

    /**
     * Invoked by related contact
     * @param type
     * @param salutation
     * @param title
     * @param firstNamePrefix
     * @param firstName
     * @param lastName
     */
    protected AbstractPerson(
            ContactType type,
            Salutation salutation,
            Option title,
            boolean firstNamePrefix,
            String firstName,
            String lastName) {

        this.type = type;
        this.status = STATUS_CREATED;
        this.salutation = salutation;
        this.title = title;
        this.firstNamePrefix = firstNamePrefix;
        this.firstName = firstName != null ? firstName.trim() : null;
        this.lastName = lastName != null ? lastName.trim() : null;
    }

    /**
     * Invoked by related contact
     * @param type
     * @param salutation
     * @param title
     * @param firstNamePrefix
     * @param firstName
     * @param lastName
     * @param street
     * @param zipcode
     * @param city
     */
    protected AbstractPerson(
            ContactType type,
            Salutation salutation,
            Option title,
            boolean firstNamePrefix,
            String firstName,
            String lastName,
            String street,
            String streetAddon,
            String zipcode,
            String city) {

        this(type, salutation, title, firstNamePrefix, firstName, lastName);
        address = new AddressImpl(street, streetAddon, zipcode, city);
    }

    protected AbstractPerson(Element contact) {
        Element addressXml = contact.getChild("address");
        if (addressXml != null) {
            Element typeXml = contact.getChild("contactType");
            if (typeXml != null) {
                type = new ContactTypeImpl(typeXml);
            }
            if (type != null && (
                    Contact.TYPE_PRIVATE.equals(type.getId())
                        || Contact.TYPE_PERSON.equals(type.getId()))) {
                firstName = addressXml.getChildText("firstName");
                lastName = addressXml.getChildText("lastName");
                Long salId = JDOMUtil.fetchLong(addressXml, "salutationId");
                if (salId != null) {
                    salutation = new SalutationImpl(salId, addressXml.getChildText("header"));
                }
                Long titleId = JDOMUtil.fetchLong(addressXml, "titleId");
                if (titleId != null) {
                    title = new OptionImpl(titleId, addressXml.getChildText("title"));
                }
            } else {
                lastName = addressXml.getChildText("lastName");
                firstName = addressXml.getChildText("lastName");
            }
            if (address == null) {
                address = new AddressImpl();
            }
            address.setStreet(addressXml.getChildText("street"));
            address.setZipcode(addressXml.getChildText("zipcode"));
            address.setCity(addressXml.getChildText("cityName"));
            address.setCountry(JDOMUtil.fetchLong(addressXml, "country"));
        }
        createCommunications(contact);
    }

    protected abstract void createCommunications(Element contact);
    

    public String getExternalReference() {
        return externalReference;
    }

    public void setExternalReference(String externalReference) {
        this.externalReference = externalReference;
    }

    public String getExternalUrl() {
        return externalUrl;
    }

    public void setExternalUrl(String externalUrl) {
        this.externalUrl = externalUrl;
    }

    public ContactType getType() {
        return type;
    }

    public void setType(ContactType type) {
        this.type = type;
    }

    public boolean isBusiness() {
        return (type != null && type.isBusiness());
    }

    public boolean isContactPerson() {
        return  (type != null && type.isContactPerson());
    }

    public boolean isFreelance() {
        return  (type != null && type.isFreelance());
    }

    public boolean isPerson() {
        return  (type != null && type.isPerson());
    }

    public boolean isPrivatePerson() {
        return  (type != null && type.isPrivatePerson());
    }

    public boolean isPublicInstitution() {
        return  (type != null && type.isPublicInstitution());
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Salutation getSalutation() {
        return salutation;
    }

    public void setSalutation(Salutation salutation) {
        this.salutation = salutation;
    }

    public Option getTitle() {
        return title;
    }

    public void setTitle(Option title) {
        this.title = title;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public boolean isFirstNamePrefix() {
        return firstNamePrefix;
    }

    public void setFirstNamePrefix(boolean firstNamePrefix) {
        this.firstNamePrefix = firstNamePrefix;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Salutation getSpouseSalutation() {
        return spouseSalutation;
    }

    public void setSpouseSalutation(Salutation spouseSalutation) {
        this.spouseSalutation = spouseSalutation;
    }

    public Option getSpouseTitle() {
        return spouseTitle;
    }

    public void setSpouseTitle(Option spouseTitle) {
        this.spouseTitle = spouseTitle;
    }

    public String getSpouseFirstName() {
        return spouseFirstName;
    }

    public void setSpouseFirstName(String spouseFirstName) {
        this.spouseFirstName = spouseFirstName;
    }

    public String getSpouseLastName() {
        return spouseLastName;
    }

    public void setSpouseLastName(String spouseLastName) {
        this.spouseLastName = spouseLastName;
    }

    public String getSpouseDisplayName() {
        return ContactUtil.createPrivateDisplayName(spouseLastName, spouseFirstName);
    }

    public String getSpouseName() {
        return ContactUtil.createPrivateName(spouseLastName, spouseFirstName);
    }

    public void updateSpouse(Salutation salutation, Option title, String firstName, String lastName) {
        this.spouseSalutation = salutation;
        this.spouseTitle = title;
        this.spouseFirstName = firstName;
        this.spouseLastName = lastName;
    }

    public String getUserSalutation() {
        return userSalutation;
    }

    public void setUserSalutation(String userSalutation) {
        this.userSalutation = userSalutation;
    }

    public String getUserNameAddressField() {
        return userNameAddressField;
    }

    public void setUserNameAddressField(String userNameAddressField) {
        this.userNameAddressField = userNameAddressField;
    }

    public Date getPersonCreated() {
        return personCreated;
    }

    public void setPersonCreated(Date personCreated) {
        this.personCreated = personCreated;
    }

    public Long getPersonCreatedBy() {
        return personCreatedBy;
    }

    public void setPersonCreatedBy(Long personCreatedBy) {
        this.personCreatedBy = personCreatedBy;
    }

    public Date getPersonChanged() {
        return personChanged;
    }

    public void setPersonChanged(Date personChanged) {
        this.personChanged = personChanged;
    }

    public Long getPersonChangedBy() {
        return personChangedBy;
    }

    public void setPersonChangedBy(Long personChangedBy) {
        this.personChangedBy = personChangedBy;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public boolean isDeleted() {
        return getStatus() == null ? false : STATUS_DELETED.equals(status);
    }

    public String getDisplayName() {
        return ContactUtil.createDisplayName(type, lastName, firstName);
    }

    public String getName() {
        return ContactUtil.createName(type, lastName, firstName);
    }

    public String getSalutationDisplay() {
        return getSalutationDisplay(true, false, true, false);
    }

    public String getSalutationDisplayLong() {
        return getSalutationDisplay(true, true, false, false);
    }

    public String getSalutationDisplayShort() {
        return getSalutationDisplay(false, false, false, false);
    }

    public String getSalutationDisplayLetterAddress() {
        return getSalutationDisplay(false, false, false, true);
    }

    private String getSalutationDisplay(boolean addLastName, boolean addFirstName, boolean letterFormat, boolean letterAddressField) {

        if (letterFormat && userSalutation != null)
            return userSalutation;
        if (letterAddressField && userSalutation != null)
            return salutation.getDisplayNameLetterAddress();

        StringBuilder sBuffer = new StringBuilder();
        if (salutation != null) {
            if (letterFormat) {
                sBuffer.append(salutation.getDisplayName());
            } else {
                sBuffer.append(salutation.getName());
            }
        }
        if (salutation != null && salutation.isAddName() && addLastName) {
            if (title != null && isSet(title.getId())) {
                sBuffer.append(Constants.BLANK).append(title.getName());
            }
            if (addFirstName && isSet(firstName)) {
                sBuffer.append(Constants.BLANK).append(firstName);
            }
            if (lastName != null) {
                sBuffer.append(Constants.BLANK).append(lastName);
            }
        }
        return sBuffer.toString();
    }

    protected void update(Person otherValue) {
        type = otherValue.getType();
        status = otherValue.getStatus();
        salutation = otherValue.getSalutation();
        title = otherValue.getTitle();
        firstNamePrefix = otherValue.isFirstNamePrefix();
        firstName = otherValue.getFirstName();
        lastName = otherValue.getLastName();
        personCreated = otherValue.getPersonCreated();
        personCreatedBy = otherValue.getPersonCreatedBy();
        personChanged = otherValue.getPersonChanged();
        personChangedBy = otherValue.getPersonChangedBy();
        externalReference = otherValue.getExternalReference();
        if (otherValue.getAddress() != null) {
            address = otherValue.getAddress();
        } else {
            address = new AddressImpl();
        }
    }

    protected boolean valueChanged(Option src, Option target) {
        if (src == null && target == null) {
            return false;
        }
        if (src == null || target == null) {
            return true;
        }
        return valueChanged(src.getId(), target.getId());
    }

    public Element getXML() {
        return getXML("root");
    }

    public Element getXML(String name) {
        Element root = new Element(name);
        root.addContent(getAddressXML());
        Phone pn = getPhone();
        if (pn == null) {
            root.addContent(new Element("phone"));
        } else {
            root.addContent(new Element("phone").setText(pn.getFormattedNumber()));
            root.addContent(pn.getXml("phoneNumber"));
        }
        Phone mn = getMobile();
        if (mn == null) {
            root.addContent(new Element("mobile"));
        } else {
            root.addContent(new Element("mobile").setText(mn.getFormattedNumber()));
            root.addContent(mn.getXml("mobileNumber"));
        }
        Phone fn = getFax();
        if (fn == null) {
            root.addContent(new Element("fax"));
        } else {
            root.addContent(new Element("fax").setText(fn.getFormattedNumber()));
            root.addContent(fn.getXml("faxNumber"));
        }
        String email = getEmail();
        if (email == null) {
            root.addContent(new Element("email"));
        } else {
            root.addContent(new Element("email").setText(email));
        }
        if (personCreated != null) {
            root.addContent(new Element("created").setText(DateFormatter.getDate(personCreated)));
        }
        return root;
    }

    public Element getAddressXML() {
        Element addressElement = new Element("address");
        String contactTypeId = (type != null && type.getId() != null ? type.getId().toString() : null);
        if (contactTypeId != null) {
            addressElement.addContent(new Element("contactType").setText(contactTypeId));
        }
        if (!isBusiness()) {
            if (isContactPerson()) {
                addressElement.addContent(new Element("contactTypeName").setText("contactPerson"));
            } else {
                addressElement.addContent(new Element("contactTypeName").setText("privateContact"));
            }
            addPersonXML(addressElement);
        } else {
            if (isPublicInstitution()) {
                addressElement.addContent(new Element("contactTypeName").setText("public"));
            } else {
                addressElement.addContent(new Element("contactTypeName").setText("business"));
            }
            addCompanyXML(addressElement);
        }
        Address obj = getAddress();
        addressElement.addContent(new Element("street").setText(obj.getStreet()));
        StringBuffer city = new StringBuffer(64);
        if (obj.getZipcode() != null) {
            city.append(obj.getZipcode()).append(Constants.BLANK);
            addressElement.addContent(new Element("zipcode").setText(obj.getZipcode()));
        }
        if (obj.getCity() != null) {
            city.append(obj.getCity());
            addressElement.addContent(new Element("cityName").setText(obj.getCity()));
        }
        addressElement.addContent(new Element("city").setText(city.toString()));
        if (obj.getCountry() == null) {
            addressElement.addContent(new Element("country"));
        } else {
            addressElement.addContent(new Element("country").setText(obj.getCountry().toString()));
        }
        return addressElement;
    }

    protected void addCompanyXML(Element addressElement) {

        if (firstName != null) {
            if (firstNamePrefix) {
                addressElement.addContent(new Element("header").setText(firstName));
                addressElement.addContent(new Element("headerSub").setText(lastName));
            } else {
                addressElement.addContent(new Element("header").setText(lastName));
                addressElement.addContent(new Element("headerSub").setText(firstName));
            }
        } else {
            addressElement.addContent(new Element("header").setText(lastName));
            addressElement.addContent(new Element("headerSub"));
        }
        if (salutation != null && salutation.getId() != null) {
            addressElement.addContent(
                new Element("salutation").setText(salutation.getDisplayName()));
        }
    }

    protected void addPersonXML(Element addressElement) {
        Boolean married = false;
        if (salutation != null &&
                (salutation.getId().equals(Salutation.FAMILY) ||
                        salutation.getId().equals(Salutation.MARRIED_COUPLE))) {
            married = true;
        }
        addressElement.addContent(new Element("married").setText(married.toString()));

        // name
        StringBuilder name = new StringBuilder();
        if (title != null && title.getName().length() > 0) {
            name.append(title.getName()).append(Constants.BLANK);
        }
        if (firstName != null && firstName.length() > 0) {
            addressElement.addContent(new Element("firstName").setText(firstName));
            name.append(firstName).append(Constants.BLANK);
        } else {
            addressElement.addContent(new Element("firstName"));
        }
        if (lastName != null && lastName.length() > 0) {
            addressElement.addContent(new Element("lastName").setText(lastName));
            name.append(lastName);
        } else {
            addressElement.addContent(new Element("lastName"));
        }
        addressElement
                .addContent(new Element("name").setText((userNameAddressField != null && userNameAddressField.length() > 0) ? userNameAddressField
                        : name.toString()));

        // spouseName
        StringBuilder spouseName = new StringBuilder();
        if (spouseTitle != null && spouseTitle.getName().length() > 0) {
            spouseName.append(spouseTitle.getName()).append(Constants.BLANK);
        }
        if (spouseFirstName != null && spouseFirstName.length() > 0) {
            addressElement.addContent(new Element("spouseFirstName").setText(spouseFirstName));
            spouseName.append(spouseFirstName).append(Constants.BLANK);
        } else {
            addressElement.addContent(new Element("spouseFirstName"));
        }
        if (spouseLastName != null && spouseLastName.length() > 0) {
            addressElement.addContent(new Element("lastName").setText(spouseLastName));
            spouseName.append(spouseLastName);
        } else {
            addressElement.addContent(new Element("spouseLastName"));
        }
        addressElement.addContent(new Element("spouseName").setText(spouseName.toString()));

        // salutation
        Element sElement = new Element("salutation");
        String salutationDisplay = getSalutationDisplay();
        sElement.setText(salutationDisplay);
        addressElement.addContent(sElement);

        // header
        if (salutation != null) {
            if (salutation.getId() != null) {
                addressElement.addContent(new Element("salutationId").setText(salutation.getId().toString()));
            }
            addressElement.addContent(new Element("header").setText(salutation.getDisplayNameLetterAddress()));
        } else {
            addressElement.addContent(new Element("header"));
            addressElement.addContent(new Element("salutationId"));
        }
        if (title != null) {
            addressElement.addContent(new Element("title").setText(title.getName()));
            if (title.getId() != null) {
                addressElement.addContent(new Element("titleId").setText(title.getId().toString()));
            }
        } else {
            addressElement.addContent(new Element("title"));
            addressElement.addContent(new Element("titleId"));
        }

        // spouseHeader
        if (spouseSalutation != null) {
            if (spouseSalutation.getId() != null) {
                addressElement.addContent(new Element("spouseSalutationId").setText(spouseSalutation.getId().toString()));
            }
            addressElement.addContent(new Element("spouseHeader").setText(spouseSalutation.getDisplayNameLetterAddress()));
        } else {
            addressElement.addContent(new Element("spouseHeader"));
            addressElement.addContent(new Element("spouseSalutationId"));
        }
        if (spouseTitle != null) {
            addressElement.addContent(new Element("spouseTitle").setText(spouseTitle.getName()));
            if (spouseTitle.getId() != null) {
                addressElement.addContent(new Element("spouseTitleId").setText(spouseTitle.getId().toString()));
            }
        } else {
            addressElement.addContent(new Element("spouseTitle"));
            addressElement.addContent(new Element("spouseTitleId"));
        }
    }

    public void updateChanged(Long user) {
        if (isSet(user)) {
            setPersonChangedBy(user);
        } else {
            setPersonChangedBy(Constants.SYSTEM_USER);
        }
        setPersonChanged(new Date(System.currentTimeMillis()));
    }

    @Override
    public abstract Object clone();
}
