/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 12.12.2012 
 * 
 */
package com.osserp.api;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class WSObjectException extends ServiceException {
    private static final long serialVersionUID = 4L;

    public WSObjectException() {
        super();
    }

    public WSObjectException(String message, Class<? extends Object> source) {
        super(message, source);
    }

    public WSObjectException(String method, String message) {
        super(method, message);
    }

    public WSObjectException(String message, Throwable cause) {
        super(message, cause);
    }

    public WSObjectException(String message) {
        super(message);
    }

    public WSObjectException(Throwable cause) {
        super(cause);
    }
}
