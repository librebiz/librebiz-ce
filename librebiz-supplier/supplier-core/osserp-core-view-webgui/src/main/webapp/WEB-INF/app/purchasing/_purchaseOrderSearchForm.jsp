<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<v:view viewName="purchaseOrderSearchView"/>
<o:ajaxLookupForm name="searchForm" url="${view.appLink}/save" targetElement="ajaxContent" minChars="0" events="onkeyup" activityElement="value">
    <div class="form-body">

        <div class="row">
        
            <div class="col-md-3">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-group">
                                <input class="form-control" type="text" name="value" id="value" value="<o:out value="${view.searchRequest.pattern}"/>" />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <a href="javascript: $('searchForm').onkeyup();"> 
                                <o:img name="replaceIcon" styleClass="bigicon" style="padding-top: 5px;" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group">
                    <select name="column" size="1" class="form-control" onchange="javascript: $('searchForm').onkeyup();">
                        <option value=""><fmt:message key="all" /></option>
                        <c:forEach var="obj" items="${view.availableColumns}">
                            <option value="<c:url value="${obj.value}"/>" <c:if test="${obj.value == view.searchRequest.columnKey}">selected="selected"</c:if>>
                                <fmt:message key="${obj.name}" />
                            </option>
                        </c:forEach>
                    </select> 
                </div>
            </div>
        
            <div class="col-md-7">
                <div class="form-group">
                    <div class="row">
                    </div>
                </div>
            </div>
            
        </div>

    </div>
</o:ajaxLookupForm>
<div id="ajaxDiv">
    <div id="ajaxContent">
        <c:import url="_purchaseOrderSearch.jsp"/>
    </div>
</div>
