/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Oct 27, 2010 11:18:29 AM 
 * 
 */
package com.osserp.gui.reporting.selectors

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import javax.servlet.http.HttpSession

import com.osserp.common.Entity
import com.osserp.common.PersistentEntity
import com.osserp.groovy.web.ViewContextException
import com.osserp.gui.CoreView

/**
 *
 * @author jg <jg@osserp.com>
 * 
 */
public class SelectCompanyPopupView extends CoreView  {
    private static Logger logger = LoggerFactory.getLogger(SelectCompanyPopupView.class.getName())

    def queryView

    SelectCompanyPopupView() {
        super()
        enablePopupView()
        headerName = 'selector.companyTitle'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (!list) {
            String viewName = form.getString('view')
            if (viewName && !env[viewName]) {
                logger.debug('initRequest: invalid context [message=query view not bound]')
                throw new ViewContextException(this)
            } else {
                queryView = env[viewName]
            }
            list = systemConfigManager.getCompanies()
            if (queryView.values.company) {
                for (obj in list) {
                    if ((obj instanceof Entity && obj.id == queryView.values.company)
                    || (obj instanceof PersistentEntity && obj.getPrimaryKey() == queryView.values.company)) {
                        bean = obj
                        break
                    }
                }
            }
        }
    }

    void createCustomNavigation() {
        nav.beanListNavigation = []
    }

    void save() {
        Long id = form.getLong('company')
        queryView.values.company = id
        queryView.reload()
    }
}
