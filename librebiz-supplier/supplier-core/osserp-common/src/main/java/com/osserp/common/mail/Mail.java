/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 21, 2005 
 * 
 */
package com.osserp.common.mail;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.beans.AbstractEntity;
import com.osserp.common.util.EmailValidator;
import com.osserp.common.util.StringUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class Mail extends AbstractEntity implements SimpleMail, Cloneable {

    public static final String TEXT = "text";
    public static final String TEXT_INVALID = "text.invalid";

    public static final String ATTACHMENT = "attachment";
    public static final String ATTACHMENT_INVALID = "attachment.invalid";

    private Long businessId = null;
    private Long referenceType = null;
    private Integer status = null;
    private Long userId = null;

    private String messageId = null;
    private String originator = null;
    private List<String> recipients = new ArrayList<>();
    private List<String> recipientsCC = new ArrayList<>();
    private List<String> recipientsBCC = new ArrayList<>();
    private String subject = null;
    private String text = null;
    private String html = null;
    private List<Attachment> attachments = new ArrayList<>();

    public Mail() {
        super();
    }

    public Mail(Long id) {
        super(id);
    }

    public Mail(String originator) {
        this.originator = originator;
    }

    public Mail(
            String originator,
            String subject,
            String text) {

        this.originator = originator;
        this.subject = subject;
        this.text = text;
    }

    public Mail(
            String originator,
            String[] recipients,
            String[] recipientsCC,
            String subject,
            String text,
            Attachment attachment) throws ClientException {

        this.originator = originator;
        this.subject = subject;
        this.text = text;
        if (recipients != null) {
            for (int i = 0, j = recipients.length; i < j; i++) {
                addRecipient(recipients[i]);
            }
        }
        if (recipientsCC != null) {
            for (int i = 0, j = recipientsCC.length; i < j; i++) {
                addRecipientCC(recipientsCC[i]);
            }
        }
        if (attachment != null) {
            attachments.add(attachment);
        }
    }

    public Mail(
            String originator,
            String recipient,
            String subject,
            String text) throws ClientException {

        this.originator = originator;
        this.subject = subject;
        this.text = text;
        addRecipient(recipient);
    }

    public Mail(
            Long id,
            String messageId,
            String originator,
            List<String> recipients,
            List<String> recipientsCC,
            List<String> recipientsBCC,
            String subject,
            String text,
            String html,
            List<Attachment> attachments) throws ClientException {
        super(id);
        this.messageId = messageId;
        this.originator = originator;
        this.recipients = recipients;
        this.recipientsCC = recipientsCC;
        this.recipientsBCC = recipientsBCC;
        this.subject = subject;
        this.text = text;
        this.html = html;
        this.attachments = attachments;
        for (int i = 0, j = recipients.size(); i < j; i++) {
            String email = recipients.get(i);
            EmailValidator.validate(email, true);
        }
        for (int i = 0, j = recipientsCC.size(); i < j; i++) {
            String email = recipientsCC.get(i);
            EmailValidator.validate(email, true);
        }
        for (int i = 0, j = recipientsBCC.size(); i < j; i++) {
            String email = recipientsBCC.get(i);
            EmailValidator.validate(email, true);
        }
    }

    public int getMailMessageSize() {
        int sum = (subject == null ? 0 : subject.length()) +
                (text == null ? 0 : text.length());
        for (int i = 0, j = attachments.size(); i < j; i++) {
            Attachment file = attachments.get(i);
            sum = sum + (file.getBytes() == null ? 0 : file.getBytes().length);
        }
        return sum;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getOriginator() {
        return originator;
    }

    public void setOriginator(String originator) {
        this.originator = originator;
    }

    public Long getReferenceType() {
        return referenceType;
    }

    public void setReferenceType(Long referenceType) {
        this.referenceType = referenceType;
    }

    public Long getBusinessId() {
        return businessId;
    }

    public void setBusinessId(Long businessId) {
        this.businessId = businessId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public List<String> getRecipients() {
        return recipients;
    }

    public boolean addRecipient(String recipient) throws ClientException {
        if (!recipientAlreadyAdded(recipients, recipient)) {
            EmailValidator.validate(recipient, true);
            recipients.add(recipient);
            removeRecipient(recipientsCC, recipient);
            removeRecipient(recipientsBCC, recipient);
            return true;
        }
        return false;
    }

    public void addRecipients(String recipientList) throws ClientException {
        String[] array = StringUtil.getTokenArray(recipientList);
        if (array != null) {
            for (int i = 0; i < array.length; i++) {
                addRecipient(array[i]);
            }
        }
    }

    public void resetRecipients() {
        recipients.clear();
    }

    public List<String> getRecipientsCC() {
        return recipientsCC;
    }

    public boolean addRecipientCC(String recipientCC) throws ClientException {
        if (!recipientAlreadyAdded(recipientsCC, recipientCC)) {
            EmailValidator.validate(recipientCC, true);
            recipientsCC.add(recipientCC);
            removeRecipient(recipients, recipientCC);
            removeRecipient(recipientsBCC, recipientCC);
            return true;
        }
        return false;
    }

    public void addRecipientsCC(String recipientsCCList) throws ClientException {
        String[] array = StringUtil.getTokenArray(recipientsCCList);
        if (array != null) {
            for (int i = 0; i < array.length; i++) {
                addRecipient(array[i]);
            }
        }
    }

    public void resetRecipientsCC() {
        recipientsCC.clear();
    }

    public List<String> getRecipientsBCC() {
        return recipientsBCC;
    }

    public boolean addRecipientBCC(String recipientBCC) throws ClientException {
        if (!recipientAlreadyAdded(recipients, recipientBCC)) {
            EmailValidator.validate(recipientBCC, true);
            recipientsBCC.add(recipientBCC);
            removeRecipient(recipients, recipientBCC);
            removeRecipient(recipientsCC, recipientBCC);
            return true;
        }
        return false;
    }

    public void addRecipientsBCC(String recipientsBCCList) throws ClientException {
        String[] array = StringUtil.getTokenArray(recipientsBCCList);
        if (array != null) {
            for (int i = 0; i < array.length; i++) {
                addRecipient(array[i]);
            }
        }
    }

    public void resetRecipientsBCC() {
        recipientsBCC.clear();
    }

    public String getSubject() {
        return subject;
    }

    /**
     * Sets the subject
     * @param subject
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * Returns the text
     * @return text.
     */
    public String getText() {
        return text;
    }

    /**
     * Sets the text
     * @param text
     */
    public void setText(String text) {
        this.text = text;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    /**
     * Indicates that attachments are available
     * @return attachmentSet.
     */
    public boolean isAttachmentSet() {
        return (attachments.size() > 0);
    }

    /**
     * Returns the attachments
     * @return attachments.
     */
    public List<Attachment> getAttachments() {
        return attachments;
    }

    /**
     * Adds an attchment to the email
     * @param attachment
     */
    public void addAttachment(Attachment attachment) {
        if (!fileAlreadyAdded(attachment.getFileName())) {
            attachments.add(attachment);
        }
    }

    /**
     * Removes attachments
     */
    public void resetAttachments() {
        attachments.clear();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        try {
            return new Mail(
                    getId(),
                    messageId,
                    originator,
                    recipients,
                    recipientsCC,
                    recipientsBCC,
                    subject,
                    text,
                    html,
                    attachments);
        } catch (Exception e) {
            // exception should never be thrown 'cause recipients will be checked when added
            throw new CloneNotSupportedException("Mail with invalid addresses is not clonable");
        }
    }

    public String getRecipientsList() {
        StringBuilder buffer = new StringBuilder();
        append(buffer, recipients);
        append(buffer, recipientsCC);
        return buffer.toString();
    }

    public String getRecipientsCCList() {
        StringBuilder buffer = new StringBuilder();
        append(buffer, recipientsCC);
        return buffer.toString();
    }

    private static void append(StringBuilder buffer, List<String> values) {
        if (values != null && !values.isEmpty()) {
            for (int i = 0, j = values.size(); i < j; i++) {
                String next = values.get(i);
                if (buffer.length() > 0) {
                    buffer.append(", ");
                }
                buffer.append(next);
            }
        }
    }

    protected boolean fileAlreadyAdded(String fileName) {
        if (attachments == null) {
            attachments = new ArrayList<Attachment>();
        }
        for (int i = 0, j = attachments.size(); i < j; i++) {
            if (attachments.get(i).getFileName().equals(fileName)) {
                return true;
            }
        }
        return false;
    }

    protected void removeRecipient(List<String> recipientList, String recipient) {
        if (recipient != null) {
            for (Iterator<String> i = recipientList.iterator(); i.hasNext();) {
                if (recipient.equalsIgnoreCase(i.next())) {
                    i.remove();
                }
            }
        }
    }

    protected boolean recipientAlreadyAdded(List<String> recipientList, String recipient) {
        if (recipient == null) {
            return true;
        }
        for (int i = 0; i < recipientList.size(); i++) {
            if (recipient.equalsIgnoreCase(recipientList.get(i))) {
                return true;
            }
        }
        return false;
    }

}
