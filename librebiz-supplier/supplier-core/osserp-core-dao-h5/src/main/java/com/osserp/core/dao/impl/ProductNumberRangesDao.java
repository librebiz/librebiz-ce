/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 7, 2009 8:18:57 AM 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.dao.Tables;

import com.osserp.core.dao.ProductNumberRanges;
import com.osserp.core.employees.Employee;
import com.osserp.core.model.products.ProductNumberRangeImpl;
import com.osserp.core.products.ProductNumberRange;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProductNumberRangesDao extends AbstractDao implements ProductNumberRanges {

    protected ProductNumberRangesDao(JdbcTemplate jdbcTemplate, Tables tables, SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
    }

    public ProductNumberRange createNumberRange(Employee user, String name, Long start, Long end) throws ClientException {
        ProductNumberRange obj = new ProductNumberRangeImpl(
                (user == null ? Constants.SYSTEM_EMPLOYEE : user.getId()), name, start, end);
        getCurrentSession().save(ProductNumberRangeImpl.class.getName(), obj);
        return obj;
    }

    public List<ProductNumberRange> getNumberRanges() {
        StringBuilder hql = new StringBuilder("from ");
        hql.append(ProductNumberRangeImpl.class.getName()).append(" o order by o.name");
        return getCurrentSession().createQuery(hql.toString()).list();
    }

    public ProductNumberRange getNumberRange(Long id) {
        return (ProductNumberRange) getCurrentSession().load(ProductNumberRangeImpl.class.getName(), id);
    }
    
    public ProductNumberRange fetchNumberRange(Long start, Long end) {
        List<ProductNumberRange> list = getNumberRanges();
        for (int i = 0, j = list.size(); i < j; i++) {
            ProductNumberRange range = list.get(i);
            if (range.getRangeStart().equals(start) && range.getRangeEnd().equals(end)) {
                return range;
            }
        }
        return null;
    }

    public void save(ProductNumberRange range) {
        getCurrentSession().saveOrUpdate(ProductNumberRangeImpl.class.getName(), range);
    }
}
