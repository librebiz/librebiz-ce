/**
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 14, 2013 08:23:20 AM
 * 
 */
package com.osserp.core.service.directory

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.OptionsCache
import com.osserp.common.mail.EmailAddress
import com.osserp.core.contacts.Contact
import com.osserp.core.contacts.ContactUtil
import com.osserp.core.contacts.Phone
import com.osserp.core.employees.Employee
import com.osserp.core.employees.EmployeeRoleConfig
import com.osserp.core.system.SystemConfigManager


/**
 * 
 * Maps an employee to a directory map.
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class DefaultEmployeeMapper extends AbstractContactMapper {
	private static Logger logger = LoggerFactory.getLogger(DefaultEmployeeMapper.class.getName())
	
	protected DefaultEmployeeMapper(SystemConfigManager systemConfigManager, OptionsCache optionsCache) {
		super(systemConfigManager, optionsCache)
	}

	Map<String, Object> map(Map<String, Object> target, Object object, Map<String, Object> options) {
		Map result = (target == null) ? [:] : target
		if (object instanceof Employee) {
			Employee employee = object
			result = deployContactValues(result, employee)
			result = deployEmployeeValues(result, employee)
		}
		return result
	}
	
	private Map deployEmployeeValues(Map result, Employee employee) {
		logger.debug("deployEmployeeValues: invoked [id=${employee.id}, contactId=${employee.contactId}]")
		
		// birthDay is an attribute of ox-schema - we don't provide a counterpart for this
		// and think an export of such sensible data to an external directory should be
		// configurable by admin or user itself. First implement this option before export.
		//
		//if (!contact.isEmployee()
		//		|| (contact.isEmployee() && isEnabled('directorySyncEmployeeBirthdate'))) {
		//	result.birthday = contact.birthDate
		//}
		
		result.employeetype = employee.role ?: employee.employeeType?.name
		result.employeenumber = employee.id.toString()
		
		if (employee.address) {
			result.homepostaladdress = employee.address.postalAddressString
		} else {
			result.homepostaladdress = null
		}
		
		EmployeeRoleConfig roleConfig = employee.defaultRoleConfig
		if (roleConfig?.branch?.contact) {
			result.postalcode = roleConfig.branch.contact.address?.zipcode
			result.street = roleConfig.branch.contact.address?.street
			result.l = roleConfig.branch.contact.address?.city
			result.st = fetchFederalState(roleConfig.branch.contact)
			result.userwww = roleConfig.branch.contact.website
			result.facsimiletelephonenumber = roleConfig.branch.contact.fax?.formattedOutlookNumber
			result.o = roleConfig.branch.company?.name
			roleConfig.roles.each {
				if (it.defaultRole) {
					result.ou = it.name
				}
			}
			result.physicaldeliveryofficename = roleConfig.branch.name
		} else {
			result.postalcode = null
			result.street = null
			result.l = null
			result.st = null
			result.userwww = null
			result.facsimiletelephonenumber = null
			result.o = null
			result.ou = null
			result.physicaldeliveryofficename = null
		}
		result.mail = employee.email
		def mailAliasName = getSystemProperty('ldapUserAlias')
		if (result.mail && mailAliasName) {
			List<EmailAddress> emails = employee.emails
			emails.each { EmailAddress address ->
				if (address.isInternal() && address.email == result.mail) {
					List<EmailAddress> aliasList = employee.getEmailAliases(address)
					if (aliasList) {
						def aliasCol = new TreeSet<String>()
						aliasList.each { EmailAddress alias ->
							aliasCol.add(alias.email)
						}
						result[mailAliasName] = aliasCol
					}
				}
			}
		}
		if (employee.defaultLanguage) {
			result.preferredlanguage = employee.defaultLanguage
			result.c = employee.defaultLanguage
		} else {
			result.preferredlanguage = null
			result.c = null
		}  
		
		Phone p = employee.internalPhone
		if (p) {
			result.telephonenumber = p.formattedOutlookNumber
		} else {
			result.telephonenumber = ContactUtil.getBusinessPhone(employee)
		}
		//logger.debug("deployEmployeeValues: done [contactId=${employee?.id}]\nEmployee values:\n${result}")
		return result
	}
}
