<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:choose>
    <c:when test="${!empty view.templates}">
        <c:set var="colClass" value="col-md-4"></c:set>
    </c:when>
    <c:otherwise>
        <c:set var="colClass" value="col-md-6"></c:set>
    </c:otherwise>
</c:choose>
<div class="row">

    <c:if test="${!empty view.templates}">
        <div class="${colClass} panel-area panel-area-default">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>
                        <fmt:message key="templateLabel" />
                    </h4>
                </div>
            </div>
            <div class="panel-body">
                <div class="form-body">
                    <div class="table-responsive table-responsive-default">
                        <table class="table table-striped">
                            <tbody>
                                <c:forEach var="obj" items="${view.templates}">
                                    <tr>
                                        <td><v:link url="/accounting/accountTransaction/selectTemplate?id=${obj.id}">
                                                <o:out value="${obj.templateName}" />
                                            </v:link></td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </c:if>

    <div class="${colClass} panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>
                    <fmt:message key="issuer" />
                </h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-body">
                <div class="table-responsive table-responsive-default">
                    <table class="table table-striped">
                        <tbody>

                            <c:choose>
                                <c:when test="${empty view.selectedContact}">
                                    <c:forEach var="obj" items="${view.contacts}">
                                        <tr>
                                            <td><v:link url="/accounting/accountTransaction/selectContact?id=${obj.id}">
                                                    <o:out value="${obj.displayName}" />
                                                </v:link></td>
                                        </tr>
                                    </c:forEach>
                                </c:when>
                                <c:otherwise>
                                    <tr>
                                        <td><v:link url="/accounting/accountTransaction/selectContact">
                                                <o:out value="${view.selectedContact.displayName}" />
                                            </v:link></td>
                                    </tr>
                                </c:otherwise>
                            </c:choose>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="${colClass} panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>
                    <fmt:message key="bookingTypeSelection" />
                </h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-body">
                <div class="table-responsive table-responsive-default">
                    <table class="table table-striped">
                        <tbody>
                            <c:choose>
                                <c:when test="${empty view.selectedType}">
                                    <c:forEach var="obj" items="${view.billingTypes}">
                                        <tr>
                                            <td><v:link url="/accounting/accountTransaction/selectType?id=${obj.id}">
                                                    <o:out value="${obj.name}" />
                                                </v:link></td>
                                        </tr>
                                    </c:forEach>
                                </c:when>
                                <c:otherwise>
                                    <tr>
                                        <td><v:link url="/accounting/accountTransaction/selectType">
                                                <o:out value="${view.selectedType.name}" />
                                            </v:link></td>
                                    </tr>
                                </c:otherwise>
                            </c:choose>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>