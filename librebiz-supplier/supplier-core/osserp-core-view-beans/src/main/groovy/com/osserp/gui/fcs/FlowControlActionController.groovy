/**
 *
 * Copyright (C) 2006, 2014, 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 15, 2006 
 * Created on Apr 16, 2014 (Groovy implementation) 
 * 
 */
package com.osserp.gui.fcs

import javax.servlet.http.HttpServletRequest

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.groovy.web.ViewController

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class FlowControlActionController extends ViewController {
    private static Logger logger = LoggerFactory.getLogger(FlowControlActionController.class.getName())
    
    @RequestMapping
    def selectAction(HttpServletRequest request) {
        logger.debug("selectAction: invoked [user=${getUserId(request)}]")
        FlowControlActionView view = getView(request)
        String forward
        try {
            forward = view.selectAction()
        } catch (Exception e) {
            logger.debug("selectAction: caught exception [message=${e.message}, class=${e.getClass().getName()}]")
            saveError(request, e.message)
            return defaultPage
        }
        if (forward) {
            return redirect(request, forward)
        }
        defaultPage
    }
    
    @RequestMapping
    def addAction(HttpServletRequest request) {
        logger.debug("addAction: invoked [user=${getUserId(request)}]")
        FlowControlActionView view = getView(request)
        try {
            view.addAction()
        } catch (Exception e) {
            logger.debug("addAction: caught exception [message=${e.message}, class=${e.getClass().getName()}]")
            saveError(request, e.message)
        }
        defaultPage
    }
    
    @RequestMapping
    def selectCancel(HttpServletRequest request) {
        logger.debug("selectCancel: invoked [user=${getUserId(request)}]")
        FlowControlActionView view = getView(request)
        try {
            view.selectCancel()
        } catch (Exception e) {
            logger.debug("selectCancel: caught exception [message=${e.message}, class=${e.getClass().getName()}]")
            saveError(request, e.message)
        }
        defaultPage
    }
    
    @RequestMapping
    def cancelAction(HttpServletRequest request) {
        logger.debug("cancelAction: invoked [user=${getUserId(request)}]")
        FlowControlActionView view = getView(request)
        try {
            view.cancelAction()
        } catch (Exception e) {
            logger.debug("cancelAction: caught exception [message=${e.message}, class=${e.getClass().getName()}]")
            saveError(request, e.message)
        }
        defaultPage
    }
    
    @RequestMapping
    def enableWastebasketMode(HttpServletRequest request) {
        logger.debug("enableWastebasketMode: invoked [user=${getUserId(request)}]")
        FlowControlActionView view = getView(request)
        view.enableWastebasketMode()
        defaultPage
    }

    @RequestMapping
    def disableWastebasketMode(HttpServletRequest request) {
        logger.debug("disableWastebasketMode: invoked [user=${getUserId(request)}]")
        FlowControlActionView view = getView(request)
        view.disableWastebasketMode()
        defaultPage
    }
    
    @RequestMapping
    def throwAction(HttpServletRequest request) {
        logger.debug("throwAction: invoked [user=${getUserId(request)}]")
        FlowControlActionView view = getView(request)
        try {
            view.throwAction()
        } catch (Exception e) {
            logger.debug("throwAction: caught exception [message=${e.message}, class=${e.getClass().getName()}]")
            saveError(request, e.message)
        }
        defaultPage
    }
    
    @RequestMapping
    def restoreAction(HttpServletRequest request) {
        logger.debug("restoreAction: invoked [user=${getUserId(request)}]")
        FlowControlActionView view = getView(request)
        view.restoreAction()
        defaultPage
    }
    
    @RequestMapping
    def selectGroup(HttpServletRequest request) {
        logger.debug("selectGroup: invoked [user=${getUserId(request)}]")
        FlowControlActionView view = getView(request)
        view.selectGroup()
        defaultPage
    }

    @RequestMapping
    def toggleDisplayCanceledMode(HttpServletRequest request) {
        logger.debug("toggleDisplayCanceledMode: invoked [user=${getUserId(request)}]")
        FlowControlActionView view = getView(request)
        view.toggleDisplayCanceledMode()
        defaultPage
    }
    
    @RequestMapping
    def resetCancellation(HttpServletRequest request) {
        logger.debug("resetCancellation: invoked [user=${getUserId(request)}]")
        FlowControlActionView view = getView(request)
        try {
            view.resetCancellation()
        } catch (Exception e) {
            logger.debug("resetCancellation: caught exception [message=${e.message}, class=${e.getClass().getName()}]")
            saveError(request, e.message)
        }
        defaultPage
    }
}
