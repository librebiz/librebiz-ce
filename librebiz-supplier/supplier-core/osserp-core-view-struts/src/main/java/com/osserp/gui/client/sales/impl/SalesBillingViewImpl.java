/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 15, 2007 2:01:18 PM 
 * 
 */
package com.osserp.gui.client.sales.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ActionException;
import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.LockException;
import com.osserp.common.PermissionException;
import com.osserp.common.util.CollectionUtil;
import com.osserp.common.util.NumberUtil;
import com.osserp.common.web.Form;
import com.osserp.common.web.ViewName;

import com.osserp.core.BusinessCase;
import com.osserp.core.BusinessType;
import com.osserp.core.Comparators;
import com.osserp.core.FcsAction;
import com.osserp.core.calc.CalculationManager;
import com.osserp.core.customers.Customer;
import com.osserp.core.customers.CustomerSearch;
import com.osserp.core.finance.BillingType;
import com.osserp.core.finance.BillingTypeSearch;
import com.osserp.core.finance.CreditNote;
import com.osserp.core.finance.Downpayment;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.Payment;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordPaymentAgreement;
import com.osserp.core.finance.RecordPrintManager;
import com.osserp.core.finance.Records;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductSearch;
import com.osserp.core.products.ProductSearchRequest;
import com.osserp.core.products.ProductSelectionConfig;
import com.osserp.core.products.ProductSelectionConfigManager;
import com.osserp.core.projects.FlowControlAction;
import com.osserp.core.projects.ProjectManager;
import com.osserp.core.sales.SalesDownpaymentManager;
import com.osserp.core.sales.SalesInvoice;
import com.osserp.core.sales.SalesInvoiceManager;
import com.osserp.core.sales.SalesOrderManager;
import com.osserp.core.sales.SalesPaymentManager;

import com.osserp.gui.client.impl.AbstractBusinessCaseAwareView;
import com.osserp.gui.client.sales.SalesBillingView;
import com.osserp.gui.sales.SalesFcsView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("salesBillingView")
public class SalesBillingViewImpl extends AbstractBusinessCaseAwareView implements SalesBillingView {
    private static Logger log = LoggerFactory.getLogger(SalesBillingViewImpl.class.getName());
    private static final String SALES_BILLING_PRODUCT_SELECTION = "salesBillingProductSelection";
    protected Order order = null;

    private List<Invoice> invoices = new ArrayList<>();
    private List<Invoice> downpayments = new ArrayList<>();
    private List<Downpayment> canceledDownpayments = new ArrayList<>();
    private List<Invoice> thirdPartyInvoices = new ArrayList<>();
    private List<Payment> payments = new ArrayList<>();
    private ProductSelectionConfig businessCaseSelectionConfig = null;
    private List<Product> businessCases = new ArrayList<>();
    
    private Double accountBalance = null;

    private RecordPaymentAgreement paymentAgreement = null;

    private List<BillingType> invoiceTypes = null;
    private Long billingType = null;
    private String target = null;
    private Customer thirdParty = null;
    
    private BigDecimal invoicedAmount = new BigDecimal(0);
    private BigDecimal creditAmount = new BigDecimal(0);
    private BigDecimal paidAmount = new BigDecimal(0);

    private Map<Long, Long> billingTypeFcsRelations = null;
    private SalesFcsView salesFcsView = null;
    private boolean customNumberEnabled = false;
    private boolean customDateEnabled = false;
    private boolean unpaidDownpaymentsAvailable = false;
    
    protected SalesBillingViewImpl() {
        super(true);
    }

    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        super.init(request);
        billingType = 0L;
        BillingTypeSearch search = (BillingTypeSearch) getService(BillingTypeSearch.class.getName());
        invoiceTypes = search.findInvoiceTypes();
        billingTypeFcsRelations = search.findBillingFcsRelations();
        Long businessCaseProductSelection = NumberUtil.createLong(getSystemProperty(SALES_BILLING_PRODUCT_SELECTION));
        if (businessCaseProductSelection != null) {
            ProductSelectionConfigManager mng = (ProductSelectionConfigManager) getService(ProductSelectionConfigManager.class.getName());
            businessCaseSelectionConfig = mng.getSelectionConfig(businessCaseProductSelection);
        }

        customNumberEnabled = getDomainUser().isPermissionGrant(new String[] { "record_manual_number_input" });
        customDateEnabled = getDomainUser().isPermissionGrant(new String[] { "record_manual_date_input" });
        salesFcsView = (SalesFcsView) request.getSession().getAttribute("salesFcsView");
        reload();
        BusinessCase businessCase = getBusinessCase();
        if (businessCase != null && !businessCase.getType().isSupportingDownpayments()) {
            removeDownpaymentTypes();
        }
        // Downpayments OR partial invoice but not both:
        if (!isSystemPropertyEnabled("salesInvoicePartialOnDownpaymentSupport")) {
            if (!getDownpayments().isEmpty()) {
                for (Iterator<BillingType> i = invoiceTypes.iterator(); i.hasNext();) {
                    BillingType billingType = i.next();
                    if (billingType.getId().equals(Invoice.PARTIAL)) {
                        i.remove();
                    }
                }
            }
            // don't allow to create a downpayment if partial invoice exist
            if (isPartialExisting()) {
                removeDownpaymentTypes();
                if (!isSystemPropertyEnabled("salesInvoiceFinalOnPartialSupport")) {
                    for (Iterator<BillingType> i = invoiceTypes.iterator(); i.hasNext();) {
                        BillingType billingType = i.next();
                        if (billingType.getId().equals(Invoice.FINAL)) {
                            i.remove();
                        }
                    }
                }
            }
        }
    }

    private boolean isPartialExisting() {
        for (int i = 0, j = getInvoices().size(); i < j; i++) {
            Invoice next = getInvoices().get(i);
            if (!next.isCanceled() && next.isPartial() 
                    && next instanceof SalesInvoice) {
                SalesInvoice invoice = (SalesInvoice) next;
                if (!invoice.isCreditedInFull()) {
                    return true;
                }
            }
        }
        return false;
    }
    
    private void removeDownpaymentTypes() {
        for (Iterator<BillingType> i = invoiceTypes.iterator(); i.hasNext();) {
            BillingType billingType = i.next();
            if (billingType.isDownpayment()) {
                i.remove();
            }
        }
    }
    
    public Order getOrder() {
        return order;
    }

    @Override
    public boolean isFlowControlContext() {
        if (salesFcsView != null && salesFcsView.getSelectedAction() instanceof FlowControlAction) {
            FlowControlAction action = (FlowControlAction) salesFcsView.getSelectedAction();
            return (action.isDownpaymentResponse() || action.isDeliveryInvoiceResponse()
                    || action.isInvoiceResponse() || action.isPartialPayment());
        }
        return false;
    }

    public boolean isRecalculationRequired() {
        return (order == null) ? true : order.isRecalculationRequired();
    }

    public boolean isCustomNumberEnabled() {
        return customNumberEnabled;
    }

    public void setCustomNumberEnabled(boolean customNumberEnabled) {
        this.customNumberEnabled = customNumberEnabled;
    }

    public boolean isCustomDateEnabled() {
        return customDateEnabled;
    }

    public void setCustomDateEnabled(boolean customDateEnabled) {
        this.customDateEnabled = customDateEnabled;
    }

    public List<BillingType> getInvoiceTypes() {
        return invoiceTypes;
    }

    public Long getBillingType() {
        return billingType;
    }

    public void setBillingType(Long billingType) throws ClientException {
        BusinessType businessType = getSales().getType();
        if (Invoice.FINAL.equals(billingType)
                && !businessType.isDirectSales()
                && order != null) {
            if (!order.getOpenDeliveries().isEmpty()) {
                throw new ClientException(ErrorCode.DELIVERY_MISSING);
            }
        }
        this.billingType = billingType;
        if (Invoice.PARTIAL.equals(billingType)) {
            List<Product> cases = findBusinessCases();
            if (cases != null) {
                this.businessCases = cases;
            }
        }
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public List<Invoice> getInvoices() {
        return invoices;
    }

    public Invoice getInvoiceByCreditNote(Long creditNoteId) {
        for (int i = 0, j = invoices.size(); i < j; i++) {
            Invoice inv = invoices.get(i);
            if (inv instanceof SalesInvoice) {
                SalesInvoice current = (SalesInvoice) inv;
                for (int k = 0, l = current.getCreditNotes().size(); k < l; k++) {
                    CreditNote note = current.getCreditNotes().get(k);
                    if (note.getId().equals(creditNoteId)) {
                        return current;
                    }
                }
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("getInvoiceByCreditNote() no invoice exists with credit note" + creditNoteId);
        }
        throw new ActionException("getInvoiceByCreditNote() no invoice exists with credit note" + creditNoteId);
    }

    public List<Invoice> getDownpayments() {
        return downpayments;
    }

    public List<Downpayment> getCanceledDownpayments() {
        return canceledDownpayments;
    }

    protected void setCanceledDownpayments(List<Downpayment> canceledDownpayments) {
        this.canceledDownpayments = canceledDownpayments;
    }

    public boolean hasChangeableInvoices() {
        if (downpayments != null) {
            for (int i = 0, j = downpayments.size(); i < j; i++) {
                Invoice inv = downpayments.get(i);
                if (!inv.isUnchangeable()) {
                    return true;
                }
            }
        }
        if (invoices != null) {
            for (int i = 0, j = invoices.size(); i < j; i++) {
                Invoice inv = invoices.get(i);
                if (!inv.isUnchangeable()) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean hasOpenInvoices() {
        if (accountBalance != null) {
            if (accountBalance.doubleValue() > 0.01) {
                if (log.isDebugEnabled()) {
                    log.debug("hasOpenInvoices() reports true for " + accountBalance);
                }
                return true;
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("hasOpenInvoices() reports false for " + accountBalance);
        }
        return false;
    }

    public boolean hasPrepaidInvoice() {
        if (accountBalance != null) {
            // we think an invoice is paid even open amount is 0.01 
            if (!invoices.isEmpty() &&
                    (accountBalance > -0.02
                    || accountBalance < 0.02)) {
                return true;
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("hasPrepaidInvoice() reports false");
        }
        return false;
    }

    public List<Invoice> getAllInvoices() {
        List<Invoice> result = new ArrayList<>(downpayments);
        if (!invoices.isEmpty()) {
            for (int i = 0, j = invoices.size(); i < j; i++) {
                Invoice inv = invoices.get(i);
                result.add(inv);
            }
        }
        return result;
    }

    public List<Invoice> getThirdPartyInvoices() {
        return thirdPartyInvoices;
    }

    public Invoice fetchInvoice(Long id, Long recordType) {
        List<Invoice> all = getAllInvoices();
        for (int i = 0, j = all.size(); i < j; i++) {
            Invoice next = all.get(i);
            if (next.getId().equals(id) && next.getType().getId().equals(recordType)) {
                return next;
            }
        }
        for (int i = 0, j = thirdPartyInvoices.size(); i < j; i++) {
            Invoice next = thirdPartyInvoices.get(i);
            if (next.getId().equals(id) && next.getType().getId().equals(recordType)) {
                return next;
            }
        }
        throw new ActionException("expected to find invoice ["
                + id
                + "] in current billing of sales ["
                + (getBusinessCaseView().getId())
                + "]");
    }

    public RecordPaymentAgreement getPaymentAgreement() {
        return paymentAgreement;
    }

    public Double getAccountBalance() {
        if (accountBalance == null) {
            return 0d;
        }
        return accountBalance;
    }

    public Long getBillingTypeByCurrentFcs() {
        if (isFlowControlContext()) {
            FcsAction current = salesFcsView.getSelectedAction();
            return billingTypeFcsRelations.get(current.getId());
        }
        return null;
    }

    public Invoice addInvoice(Long type, Form form) throws ClientException {
        Invoice newInvoice = null;
        if (Invoice.DELIVERY.equals(type)
                || Invoice.DOWNPAYMENT.equals(type)
                || Invoice.CUSTOM_DOWNPAYMENT.equals(type)
                || Invoice.PARTIAL_DOWNPAYMENT.equals(type)) {

            SalesDownpaymentManager sdm = getDownpaymentManager();
            newInvoice = sdm.create(type, order, getDomainEmployee(),
                    form.getBoolean("copyNote"), form.getBoolean("copyTerms"),
                    form.getBoolean("copyItems"), form.getLong("recordNumber"),
                    form.getDate("recordDate"), form.getDecimal("amount"));

        } else {
            SalesInvoiceManager som = getInvoiceManager();
            BigDecimal partialInvoiceAmount = new BigDecimal(0);
            BigDecimal partialInvoiceAmountGross = new BigDecimal(0);
            List<SalesInvoice> partialInvoices = som.findPartial(getSales());
            for (int i = 0, j = partialInvoices.size(); i < j; i++) {
                SalesInvoice pi = partialInvoices.get(i);
                if (!pi.isCanceled()) {
                    partialInvoiceAmount = partialInvoiceAmount.add(pi.getAmounts().getAmount());
                    partialInvoiceAmountGross = partialInvoiceAmountGross.add(pi.getAmounts().getGrossAmount());
                }
            }
            boolean cancelDownpayments = form.getBoolean("cancelDownpayments");
            if (cancelDownpayments && !downpayments.isEmpty()) {
                RecordPrintManager pm = (RecordPrintManager) getService(RecordPrintManager.class.getName());
                SalesPaymentManager payments = (SalesPaymentManager) getService(SalesPaymentManager.class.getName());
                for (int i = 0, j = downpayments.size(); i < j; i++) {
                    Invoice dp = downpayments.get(i);
                    List paymentList = payments.getByRecord(dp);
                    if (!dp.isCanceled() && paymentList.isEmpty()) {
                        getDownpaymentManager().cancel(getDomainEmployee(), dp, form.getDate("recordDate"));
                        dp = (Invoice) getDownpaymentManager().find(dp.getId());
                        pm.createPdf(getDomainEmployee(), dp, Record.STAT_SENT);
                        if (log.isDebugEnabled()) {
                            log.debug("addInvoice() cancel unpaid downpayment done [id=" + dp.getId() + "]");
                        }
                    }
                }
            }
            newInvoice = som.create(
                    getDomainEmployee(), 
                    order, 
                    type, 
                    thirdParty, 
                    partialInvoiceAmount, 
                    partialInvoiceAmountGross,
                    form.getBoolean("copyNote"),
                    form.getBoolean("copyTerms"),
                    form.getBoolean("copyItems"),
                    form.getLong("recordNumber"), 
                    form.getDate("recordDate"));
            thirdParty = null;
        }
        if (newInvoice == null) {
            throw new IllegalStateException("no invoice created");
        }
        if (log.isDebugEnabled()) {
            log.debug("addInvoice() done [id=" + newInvoice.getId() + "]");
        }
        // reload invoices
        reload();
        // newInvoice is not loaded with all payments
        // returning reloaded invoice
        return fetchInvoice(newInvoice.getId(), newInvoice.getType().getId());
    }

    public Invoice addPartialInvoice(Form form) throws ClientException {
        SalesInvoiceManager som = getInvoiceManager();
        Invoice invoice = som.create(
                getDomainEmployee(),
                order,
                form.getBoolean("percent"),
                form.getDecimal("amount"),
                getBusinessCase(form),
                form.getString("note"),
                form.getBoolean("printOrderItems"),
                form.getBoolean("copyNote"),
                form.getBoolean("copyTerms"), 
                form.getLong("recordNumber"), 
                form.getDate("recordDate"));
        // reload invoices
        reload();
        return invoice;
    }

    private Product getBusinessCase(Form fh) {
        Long id = fh.getLong("productId");
        for (int i = 0, j = businessCases.size(); i < j; i++) {
            Product product = businessCases.get(i);
            if (product.getProductId().equals(id)) {
                return product;
            }
        }
        return null;
    }

    public void deleteInvoice(Invoice invoice) throws ClientException {
        if (invoice instanceof Downpayment) {
            SalesDownpaymentManager aim = getDownpaymentManager();
            aim.delete(invoice, getDomainEmployee());
        } else {
            SalesInvoiceManager som = getInvoiceManager();
            som.delete(invoice, getDomainEmployee());
        }
        // reload invoices
        reload();
    }

    public void deletePayment(Long id) {
        List<Invoice> all = getAllInvoices();
        boolean deleted = false;
        for (int i = 0, j = all.size(); i < j; i++) {
            if (deleted) {
                break;
            }
            Invoice invoice = all.get(i);
            for (int k = 0, l = invoice.getPayments().size(); k < l; k++) {
                Payment next = invoice.getPayments().get(k);
                if (next.getId().equals(id)) {
                    SalesPaymentManager manager = (SalesPaymentManager) getService(SalesPaymentManager.class.getName());
                    manager.delete(next);
                    deleted = true;
                    break;
                }
            }
        }
        reload();
    }

    public List<Product> getBusinessCases() {
        return businessCases;
    }

    public boolean isUnpaidDownpaymentsAvailable() {
        return unpaidDownpaymentsAvailable;
    }

    protected void setUnpaidDownpaymentsAvailable(boolean unpaidDownpaymentsAvailable) {
        this.unpaidDownpaymentsAvailable = unpaidDownpaymentsAvailable;
    }

    public boolean isCancelUnpaidDownpaymentsDefault() {
        return getSystemConfigManager().isSystemPropertyEnabled("salesInvoiceCancelDownpayments");
    }

    public Customer getThirdParty() {
        return thirdParty;
    }

    protected void setThirdParty(Customer thirdParty) {
        this.thirdParty = thirdParty;
    }

    public void addThirdPartyRecipient(Long id) {
        if (id == null) {
            thirdParty = null;
        } else {
            CustomerSearch customerSearch = (CustomerSearch) getService(CustomerSearch.class.getName());
            thirdParty = customerSearch.findById(id);
        }
    }

    public void checkFinalOrderState() throws ClientException {
        if ((getOrder().getAmounts() == null
                || getOrder().getAmounts().getAmount() == null
                || getOrder().getAmounts().getAmount().doubleValue() == 0)
                && !isVirtualProductAvailable()) {
            if (!getSales().getType().isRecordByCalculation()) {
                throw new ClientException(ErrorCode.ORDER_ITEMS_MISSING);
            }
            throw new ClientException(ErrorCode.CALCULATION_REQUIRED);
        }
        if (!getOrder().isUnchangeable()) {
            throw new ClientException(ErrorCode.ORDER_MISSING);
        }
    }

    private boolean isVirtualProductAvailable() {
        for (int i = 0, j = getOrder().getItems().size(); i < j; i++) {
            Product p = getOrder().getItems().get(i).getProduct();
            if (p.isVirtual()) {
                return true;
            }
        }
        return false;
    }

    public void checkLock() throws LockException {
        ((CalculationManager) getService(
                CalculationManager.class.getName())).checkLock(
                getBusinessCase());
    }

    @Override
    public void reload() {
        try {
            load();
        } catch (Throwable e) {
            log.error("reload() failed [message=" + e.getMessage() + "]", e);
            throw new BackendException(e.getMessage(), e);
        }
    }

    protected void load() {
        if (log.isDebugEnabled()) {
            log.debug("load() invoked");
        }
        payments = new ArrayList<>();
        loadOrder();
        loadDownpayments();
        loadInvoices();
        loadThirdPartyInvoices();
        computeAccountBalance();
        if (log.isDebugEnabled()) {
            log.debug("load() done");
        }
    }

    protected void loadOrder() {
        SalesOrderManager som = (SalesOrderManager) getService(SalesOrderManager.class.getName());
        order = som.getBySales(getSales());
        if (log.isDebugEnabled()) {
            log.debug("loadOrder() done [id=" + order.getId() + "]");
        }
        paymentAgreement = order.getPaymentAgreement();
    }

    private void loadDownpayments() {
        SalesDownpaymentManager aim = getDownpaymentManager();
        downpayments = sortInvoices(aim.find(order));
        canceledDownpayments = sortDownpayments(new ArrayList<>(aim.findCanceled(order)));
        if (log.isDebugEnabled()) {
            log.debug("loadDownpayments() search executed [count=" + downpayments.size() + "]");
        }
        for (int i = 0, j = downpayments.size(); i < j; i++) {
            Invoice invoice = downpayments.get(i);
            for (int k = 0, l = invoice.getPayments().size(); k < l; k++) {
                Payment payment = invoice.getPayments().get(k);
                payments.add(payment);
            }
            if (invoice.getPayments().isEmpty()) {
                unpaidDownpaymentsAvailable = true;
            }
        }
    }

    private void loadInvoices() {
        SalesInvoiceManager sim = getInvoiceManager();
        invoices = sortInvoices(sim.find(order));
        if (log.isDebugEnabled()) {
            log.debug("loadInvoices() search executed [count=" + invoices.size() + "]");
        }
        for (int i = 0, j = invoices.size(); i < j; i++) {
            SalesInvoice invoice = (SalesInvoice) invoices.get(i);
            for (int k = 0, l = invoice.getPayments().size(); k < l; k++) {
                Payment payment = invoice.getPayments().get(k);
                if (payment.getRecordId().equals(invoice.getId())) {
                    payments.add(payment);
                }
            }
            for (int k = 0, l = invoice.getCreditNotes().size(); k < l; k++) {
                CreditNote note = invoice.getCreditNotes().get(k);
                for (int m = 0, n = note.getPayments().size(); m < n; m++) {
                    Payment payment = note.getPayments().get(m);
                    payments.add(payment);
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void loadThirdPartyInvoices() {
        SalesInvoiceManager sim = getInvoiceManager();
        thirdPartyInvoices = sortInvoices(new ArrayList(sim.findThirdParty(getSales())));
        if (!thirdPartyInvoices.isEmpty() && log.isDebugEnabled()) {
            log.debug("loadThirdPartyInvoices() done [count=" + thirdPartyInvoices.size() + "]");
        }
    }

    private List<Product> findBusinessCases() {
        List<Product> result = new ArrayList<>();
        if (businessCaseSelectionConfig != null) {
            ProductSearch search = (ProductSearch) getService(ProductSearch.class.getName());
            result = search.find(new ProductSearchRequest(null, true, false, ProductSearchRequest.IGNORE_FETCHSIZE), businessCaseSelectionConfig);
            CollectionUtil.sort(result, Comparators.createProductByNameComparator());
        }
        if (log.isDebugEnabled()) {
            log.debug("findBusinessCases() done [count=" + result.size() + "]");
        }
        return result;
    }
    
    private List<Invoice> sortInvoices(List<Invoice> invoices) {
        Records.sort(invoices, Records.ORDER_BY_CREATED, true);
        return invoices;
    }
    
    private List<Downpayment> sortDownpayments(List<Downpayment> invoices) {
        Records.sort(invoices, Records.ORDER_BY_CREATED, true);
        return invoices;
    }

    private void computeAccountBalance() {
        invoicedAmount = new BigDecimal(0);
        creditAmount = new BigDecimal(0);
        paidAmount = new BigDecimal(0);
        int finalInvoices = 0;
        for (int i = 0, j = invoices.size(); i < j; i++) {
            SalesInvoice inv = (SalesInvoice) invoices.get(i);
            if (!inv.isCanceled()) {
                finalInvoices++;
                if (inv.getAmounts().getGrossAmount() != null) {
                    invoicedAmount = invoicedAmount.add(inv.getAmounts().getGrossAmount());
                    if (log.isDebugEnabled()) {
                        log.debug("computeAccountBalance() adding invoice amount [id="
                                + inv.getId() + ", amount="
                                + inv.getAmounts().getGrossAmount() + "]");
                    }
                    
                }
                if (!inv.getCreditNotes().isEmpty()) {
                    for (int k = 0, l = inv.getCreditNotes().size(); k < l; k++) {
                        CreditNote note = inv.getCreditNotes().get(k);
                        if (!note.isCanceled() && note.getAmounts().getGrossAmount() != null) {
                            creditAmount = creditAmount.add(note.getAmounts().getGrossAmount());
                            if (log.isDebugEnabled()) {
                                log.debug("computeAccountBalance() adding credit amount [id="
                                        + note.getId() + ", amount="
                                        + note.getAmounts().getGrossAmount() + "]");
                            }
                        }
                    }
                }
            }
        }
        if (finalInvoices < 1 && downpayments.size() > 0) {
            for (int i = 0, j = downpayments.size(); i < j; i++) {
                Downpayment inv = (Downpayment) downpayments.get(i);
                if (!inv.isCanceled()) {
                    if (inv.getGrossAmount() != null) {
                        invoicedAmount = invoicedAmount.add(inv.getGrossAmount());
                        if (log.isDebugEnabled()) {
                            log.debug("computeAccountBalance() adding downpayment amount [id="
                                    + inv.getId() + ", amount="
                                    + inv.getGrossAmount() + "]");
                        }
                    }
                }
            }
        }
        for (int i = 0, j = payments.size(); i < j; i++) {
            Payment payment = payments.get(i);
            if (payment.getAmount() != null) {
                paidAmount = paidAmount.add(payment.getAmount());
                if (log.isDebugEnabled()) {
                    log.debug("computeAccountBalance() adding payment amount [id="
                            + payment.getId() + ", amount="
                            + payment.getAmount() + "]");
                }
            }
        }
        BigDecimal _accountBalance = new BigDecimal(invoicedAmount.toString());
        _accountBalance = _accountBalance.subtract(creditAmount);
        _accountBalance = _accountBalance.subtract(paidAmount);
        _accountBalance = NumberUtil.round(_accountBalance, 2); 
        
        ProjectManager projectManager = (ProjectManager) getService(ProjectManager.class.getName());
        accountBalance = projectManager.getAccountBalance(getSales());
        if (log.isDebugEnabled()) {
            log.debug("computeAccountBalance() done [usingFetched=" + accountBalance
                    + ", computed=" + _accountBalance + "]");
        }

        /*
         * below is old code computing account balance by objects instead of a stored db procedure above
         * 
         * if (!invoices.isEmpty()) { if (log.isDebugEnabled()) { log.debug("computeAccountBalance() invoked, computing by final invoices"); } double
         * openAmount = 0; for (int i = 0, j = invoices.size(); i < j; i++) { Invoice invoice = (Invoice) invoices.get(i); if (log.isDebugEnabled()) {
         * log.debug("computeAccountBalance() examining invoice " + invoice.getId()); } if (!invoice.isCanceled()) { if (log.isDebugEnabled()) {
         * log.debug("computeAccountBalance() invoice is not canceled"); } double amount = invoice.getAmounts().getGrossAmount().doubleValue(); if
         * (log.isDebugEnabled()) { log.debug("computeAccountBalance() adding " + amount + " to open amounts"); } openAmount = openAmount + amount; if (invoice
         * instanceof SalesInvoice) { try { List creditNotes = ((SalesInvoice) invoice).getCreditNotes(); for (int k = 0, l = creditNotes.size(); k < l; k++) {
         * CreditNote note = (CreditNote) creditNotes.get(k); if (note.getAmounts() != null && note.getAmounts().getGrossAmount() != null) { openAmount =
         * openAmount - note.getAmounts().getGrossAmount().doubleValue(); } } } catch (Throwable t) {
         * log.warn("computeAccountBalance() failed while attempt to compute credit notes on invoice " + invoice.getId()); } } } } // openAmount => 73.362,81 //
         * paidAdvanceAmount => 50.000,00 (20.000,00 + 30.000,00) // paidAmount => 13.362,81 double paid = 0; for (int k = 0, l = payments.size(); k <
         * l; k++) { Payment payment = (Payment) payments.get(k); double amount = payment.getAmounts().getAmount().doubleValue(); if (log.isDebugEnabled())
         * { log.debug("computeAccountBalance() adding " + amount + " to paid amounts"); } paid = paid + amount; }
         * 
         * this.accountBalance = DecimalUtil.roundDue(openAmount - paid); } else { if (log.isDebugEnabled()) {
         * log.debug("computeAccountBalance() invoked, computing by advance invoices"); } double openAmount = 0; for (int i = 0, j =
         * this.advanceInvoices.size(); i < j; i++) { Invoice inv = (Invoice) advanceInvoices.get(i); double amount = inv.getDueAmount().doubleValue(); if
         * (log.isDebugEnabled()) { log.debug("computeAccountBalance() adding " + amount + " to paid amounts"); } openAmount = openAmount + amount; }
         * this.accountBalance = DecimalUtil.roundDue(openAmount); } } catch (Throwable t) { log.error("computeAccountBalance() failed: " + t.toString(), t); }
         */
    }

    /*
     * private Record getPaymentTarget(Long id) { for (int i = 0, j = downpayments.size(); i < j; i++) { Invoice next = downpayments.get(i); if
     * (next.getId().equals(id)) { return next; } } for (int i = 0, j = invoices.size(); i < j; i++) { Invoice next = invoices.get(i); if
     * (next.getId().equals(id)) { return next; } if (next instanceof SalesInvoice) { SalesInvoice si = (SalesInvoice) next; for (int k = 0, l =
     * si.getCreditNotes().size(); k < l; k++) { CreditNote note = si.getCreditNotes().get(k); if (note.getId().equals(id)) { return note; } } if
     * (si.getCancellation() != null && si.getCancellation().getId().equals(id)) { return si.getCancellation(); } } } return null; }
     */

    private SalesDownpaymentManager getDownpaymentManager() {
        return (SalesDownpaymentManager) getService(SalesDownpaymentManager.class.getName());
    }

    private SalesInvoiceManager getInvoiceManager() {
        return (SalesInvoiceManager) getService(SalesInvoiceManager.class.getName());
    }
}
