<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="productCategoryEditorView" />

<div class="modalBoxHeader">
    <div class="modalBoxHeaderLeft">
        <fmt:message key="${view.headerName}" />
    </div>
</div>
<div class="modalBoxData">

    <div class="subcolumns">
        <div class="subcolumn">
            <div class="spacer"></div>
            <c:import url="/errors/_errors.jsp" />

            <v:ajaxForm name="productCategoryEditorForm" url="/admin/products/productCategoryEditor/save">
                <table class="valueTable inputs230">
                    <tr>
                        <td><fmt:message key="name" /></td>
                        <td><input type="text" name="name" <c:if test="${!empty view.bean}"> value="${view.bean.name}"</c:if> /></td>
                    </tr>
                    <tr>
                        <td class="row-submit"><v:ajaxLink url="/admin/products/productCategoryEditor/exit" targetElement="${view.name}_popup">
                                <input class="cancel" type="button" value="<fmt:message key="exit"/>" />
                            </v:ajaxLink></td>
                        <td class="row-submit"><o:submit /></td>
                    </tr>
                </table>
            </v:ajaxForm>

            <div class="spacer"></div>
        </div>
    </div>
</div>