/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 4, 2012 
 * 
 */
package com.osserp.core.dao.impl

import groovy.sql.Sql

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.dao.QueryResult
import com.osserp.common.util.DateFormatter
import com.osserp.common.util.StringUtil

import com.osserp.core.BusinessCaseFilter
import com.osserp.core.employees.Employee
import com.osserp.core.dao.BusinessCaseMatch
import com.osserp.core.dao.BusinessCaseQueries
import com.osserp.core.dao.SystemConfigs

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public class BusinessCaseQueriesDao extends AbstractCoreDao implements BusinessCaseQueries {
    private static Logger logger = LoggerFactory.getLogger(BusinessCaseQueriesDao.class.getName())
    
    SystemConfigs systemConfigs

    String[] fetchBusinessCaseStatusReportResult() {
        String commaseparated = systemConfigs?.getSystemProperty('businessCaseReportColumns')
        if (commaseparated) {
            String[] result = StringUtil.getTokenArray(commaseparated)
            if (result?.length > 1) {
                logger.debug("fetchBusinessCaseStatusReportResult: done by systemProperty \'businessCaseReportColumns\' {${result}}...")
                return result
            }
        }
        logger.debug('fetchBusinessCaseStatusReportResult: systemProperty \'businessCaseReportColumns\' not found, using defaults...')
        
        return [ 'id', 'action_name', 'origin_name', 'person_sales', 'branchkey', 
            'lastname', 'firstname', 'street', 'zipcode', 'city', 'phone', 'mobile', 'mailaddress', 
            'created', 'action_created', 'accounting_ref', 'is_sales', 'origin', 'initial_created', 
            'contactgrant' ] as String[]
    }
        
    QueryResult getBusinessCaseStatus(BusinessCaseFilter filter, boolean export) {
        String dbview = getSelectedViewName(filter, export)
        StringBuilder query = new StringBuilder("SELECT * FROM ${dbview} bc")
        boolean whereAdded = false
        if (!filter.nothingToFilter) {
            if (filter.businessTypeId) {
                query.append(" WHERE bc.type_id = ${filter.businessTypeId}")
                whereAdded = true
            }
            if (filter.companyId) {
                if (whereAdded) { 
                    query.append(' AND') 
                } else { 
                    query.append(' WHERE')
                    whereAdded = true
                }
                query.append(" bc.company_id = ${filter.companyId}")
            }
            if (filter.branchId) {
                if (whereAdded) { 
                    query.append(' AND') 
                } else { 
                    query.append(' WHERE')
                    whereAdded = true
                }
                query.append(" bc.branch_id = ${filter.branchId}")
            }
            if (!filter.personIgnorable) {
                if (filter.personResponsible) {
                    if (whereAdded) { 
                        query.append(' AND') 
                    } else { 
                        query.append(' WHERE')
                        whereAdded = true
                    }
                    query.append(" bc.manager_id = ${filter.personId}")
                    
                } else if (filter.personSales) {
                    if (whereAdded) { 
                        query.append(' AND') 
                    } else { 
                        query.append(' WHERE')
                        whereAdded = true
                    }
                    query.append(" bc.sales_id = ${filter.personId}")
                } else if (filter.personCreated) {
                    if (whereAdded) { 
                        query.append(' AND') 
                    } else { 
                        query.append(' WHERE')
                        whereAdded = true
                    }
                    query.append(" bc.created_id = ${filter.personId}")
                }
            }
            if (!filter.dateIgnorable) {
                if (filter.dateFrom) {
                    String df = DateFormatter.getComparisonDate(filter.dateFrom)
                    if (whereAdded) {
                        query.append(' AND')
                    } else {
                        query.append(' WHERE')
                        whereAdded = true
                    }
                    if (filter.dateByAction) {
                        query.append(" bc.action_created >= '${df}'")
                    } else if (filter.dateByChanged) {
                        query.append(" bc.changed >= '${df}'")
                    } else {
                        query.append(" bc.created >= '${df}'")
                    }
                }
                if (filter.dateTil) {
                    String df = DateFormatter.getComparisonDate(filter.dateTil)
                    if (whereAdded) {
                        query.append(' AND')
                    } else {
                        query.append(' WHERE')
                        whereAdded = true
                    }
                    if (filter.dateByAction) {
                        query.append(" bc.action_created <= '${df}'")
                    } else if (filter.dateByChanged) {
                        query.append(" bc.changed <= '${df}'")
                    } else {
                        query.append(" bc.created <= '${df}'")
                    }
                }
            }
            if (filter.limitEnabled && !filter.limitUnspecifiedOnly) {
                query.append(" LIMIT ${filter.limit}")
            }
        } else if (filter.limitEnabled) {
            query.append(" LIMIT ${filter.limit}")
        }
        String finalQuery = query.toString()
        logger.debug("getBusinessCaseStatus: query created [sql=${finalQuery}]")
        long start = System.currentTimeMillis()
        List list = sql.rows(finalQuery)
        logger.debug("getBusinessCaseStatus: done [count=${list.size()}, duration=${System.currentTimeMillis() - start}ms]")
        def rows = createResult(list)
        
        new QueryResult(
                name: 'businessCaseReportViewHeader',
                columns: ['id', 'status', 'name', 'created', 'action_date', 'action_name'],
                rows: rows
                )
    }

    List<BusinessCaseMatch> getBusinessCaseNames() {
        List result = []
        sql.rows("SELECT is_sales, id, customer_id, name FROM ${businessCaseView}".toString()).each {
            result << new BusinessCaseMatch(it.is_sales, it.id, it.customer_id, it.name)
        }
        return result
    }

    void reloadCachedViews() {
        logger.debug("refreshCache: invoked")
        long start = System.currentTimeMillis()
        refreshView(businessCaseStatusMaterializedView, true)
        long time = System.currentTimeMillis() - start
        logger.debug("refreshCache: businessCaseStatusView done [duration=${time}ms]")
        start = System.currentTimeMillis()
        refreshView(businessCaseStatusExportMaterializedView, true)
        time = System.currentTimeMillis() - start
        logger.debug("refreshCache: businessCaseStatusExportView done [duration=${time}ms]")
    }
    
    protected List createResult(List raw) {
        List result = []
        raw.each {
            Map map = [:]
            map.putAll(it)
            if (map['origin_id'] instanceof Long) {
                map['origin'] = fetchOptionValue('campaigns', map['origin_id'])
            }
            if (map['installer_id'] instanceof Long) {
                map['installer'] = fetchOptionValue('suppliers', map['installer_id'])
            }
            result << map
        }
        return result
    }
    
    protected String getSelectedViewName(BusinessCaseFilter filter, boolean export) {
        if (filter.cached) {
            return export ? businessCaseStatusExportMaterializedView : 
                businessCaseStatusMaterializedView
        }
        return export ? businessCaseStatusExportView : businessCaseStatusView
    }

    protected String getBusinessCaseView() {
        getTableName('businessCase')
    }

    protected String getBusinessCaseStatusView() {
        getTableName('businessCaseStatus')
    }
    
    protected String getBusinessCaseStatusMaterializedView() {
        getTableName('businessCaseStatusMaterialized')
    }
    
    protected String getBusinessCaseStatusExportView() {
        getTableName('businessCaseStatusExport')
    }
    
    protected String getBusinessCaseStatusExportMaterializedView() {
        getTableName('businessCaseStatusExportMaterialized')
    }

    protected String getOpenRequestFcsView() {
        getTableName('requestListFcs')
    }
    
    protected String getOpenRequestStatusView() {
        getTableName('requestListStatus')
    }
    
    protected String getOpenSalesFcsView() {
        getTableName('salesListFcs')
    }
    
    protected String getOpenSalesStatusView() {
        getTableName('salesListStatus')
    }
    
}
