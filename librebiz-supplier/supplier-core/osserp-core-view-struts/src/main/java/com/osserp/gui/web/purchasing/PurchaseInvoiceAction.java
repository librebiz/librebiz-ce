/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 08-Sep-2006 19:39:00 
 * 
 */
package com.osserp.gui.web.purchasing;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.struts.StrutsForm;
import com.osserp.core.purchasing.PurchaseInvoice;
import com.osserp.gui.client.Actions;
import com.osserp.gui.client.purchasing.PurchaseInvoiceView;
import com.osserp.gui.web.records.AbstractRecordAction;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PurchaseInvoiceAction extends AbstractRecordAction {
    private static Logger log = LoggerFactory.getLogger(PurchaseInvoiceAction.class.getName());

    @Override
    protected Class<PurchaseInvoiceView> getViewClass() {
        return PurchaseInvoiceView.class;
    }

    @Override
    public ActionForward redisplay(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        super.redisplay(mapping, form, request, response);
        PurchaseInvoiceView view = getInvoiceView(request.getSession());
        view.refresh();
        return mapping.findForward(Actions.DISPLAY);
    }

    /**
     * Loads another invoice and forwards to 'display' target
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward loadOther(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("loadOther() request from " + getUser(session).getId());
        }
        PurchaseInvoiceView view = getInvoiceView(session);
        view.loadOther(RequestUtil.getId(request));
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Deletes an invoice
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward delete(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("delete() request from " + getUser(session).getId());
        }
        PurchaseInvoiceView view = getInvoiceView(session);
        try {
            PurchaseInvoice invoice = (PurchaseInvoice) view.getRecord();
            Long targetInvoiceId = null;
            if (invoice.isCreditNote() && invoice.getReference() != null) {
                targetInvoiceId = invoice.getReference();
            }
            view.delete();
            if (targetInvoiceId != null) {
                view.loadOther(targetInvoiceId);
                return mapping.findForward(view.getActionTarget());
            }
            return mapping.findForward(Actions.EXIT);
        } catch (ClientException c) {
            saveError(request, c.getMessage());
            return mapping.findForward(view.getActionTarget());
        }
    }

    /**
     * Releases purchase invoice
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    @Override
    public ActionForward release(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("release() request from " + getUser(session).getId());
        }
        PurchaseInvoiceView view = getInvoiceView(session);
        try {
            view.release();
        } catch (ClientException c) {
            saveError(request, c.getMessage());
        }
        return mapping.findForward(Actions.DISPLAY);
    }

    /**
     * Enables delete closed mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward enableDeleteClosedMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableDeleteClosedMode() request from " + getUser(session).getId());
        }
        PurchaseInvoiceView view = getInvoiceView(session);
        view.setDeleteClosedMode(true);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Disables delete closed mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward disableDeleteClosedMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disableDeleteClosedMode() request from " + getUser(session).getId());
        }
        PurchaseInvoiceView view = getInvoiceView(session);
        view.setDeleteClosedMode(false);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Deletes closed invoice
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward deleteClosed(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("deleteClosed() request from " + getUser(session).getId());
        }
        PurchaseInvoiceView view = getInvoiceView(session);
        try {
            view.deleteClosed(new StrutsForm(form));
            return exit(mapping, form, request, response);
        } catch (ClientException e) {
            return saveErrorAndReturn(request, e.getMessage());
        }
    }

    /**
     * Opens closed invoice
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward openClosed(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("openClosed() request from " + getUser(session).getId());
        }
        PurchaseInvoiceView view = getInvoiceView(session);
        view.openClosed();
        return mapping.findForward(view.getActionTarget());
    }

    private PurchaseInvoiceView getInvoiceView(HttpSession session) throws PermissionException {
        return (PurchaseInvoiceView) getRecordView(session);
    }
}
