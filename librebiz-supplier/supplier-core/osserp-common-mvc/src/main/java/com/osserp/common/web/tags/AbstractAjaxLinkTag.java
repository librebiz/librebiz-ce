/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 5, 2008 3:22:28 PM 
 * 
 */
package com.osserp.common.web.tags;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractAjaxLinkTag extends AbstractAjaxElementTag {
    private static Logger log = LoggerFactory.getLogger(AbstractAjaxLinkTag.class.getName());
    private String linkId = null;

    /**
     * Provides the optional id for the link
     * @return linkId
     */
    public String getLinkId() {
        return linkId;
    }

    /**
     * Sets an optional link id
     * @param linkId
     */
    public void setLinkId(String linkId) {
        this.linkId = linkId;
    }

    @Override
    public int doEndTag() throws JspException {
        try {
            JspWriter out = pageContext.getOut();
            out.print("</a>");
        } catch (IOException ioe) {
            log.error("doEndTag() error getting writer");
        }
        return (EVAL_BODY_INCLUDE);
    }

    @Override
    public void release() {
        this.linkId = null;
    }

    @Override
    public String getTargetElement() {
        String te = super.getTargetElement();
        return (te != null && te != "") ? te : getLinkId() + POPUP_SUFFIX;
    }

    /**
     * Appends title, style and styleClass attributes and closes with the trailing '&gt;'
     * @param buffer
     */
    protected void appendEnd(StringBuilder buffer) {
        addTitle(buffer);
        addStyle(buffer);
        addStyleClass(buffer);
        buffer.append(">");
    }
}
