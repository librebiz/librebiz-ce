/**
 *
 * Copyright (C) 2007, 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jul 17, 2016 
 * 
 */
package com.osserp.core.dao;

import java.util.Date;

import com.osserp.common.ClientException;
import com.osserp.common.Option;

import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.Salutation;
import com.osserp.core.employees.Employee;
import com.osserp.core.employees.EmployeeStatus;
import com.osserp.core.employees.EmployeeType;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.system.SystemCompany;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface EmployeeCreator {

    /**
     * Creates a new employee and corresponding user account
     * @param user logged in user creating new employee
     * @param contact
     * @param type
     * @param branch
     * @param status
     * @param beginDate
     * @param initials
     * @param role
     * @return
     * @throws Exception
     */
    Employee create(
            Employee user,
            Contact contact,
            EmployeeType type,
            BranchOffice branch,
            EmployeeStatus status,
            Date beginDate,
            String initials,
            String role) throws Exception;

    /**
     * Creates the initial admin user.
     * @param setupUser
     * @param systemCompany
     * @param adminId
     * @param salutation
     * @param title
     * @param firstName
     * @param lastName
     * @param emailAddress
     * @param phoneNumber
     * @param mobileNumber
     * @param isExecutiveOfficer
     * @return admin user
     * @throws ClientException
     */
    Employee createAdministrator(
            Employee setupUser,
            SystemCompany systemCompany,
            Long adminId,
            Salutation salutation,
            Option title,
            String firstName,
            String lastName,
            String emailAddress,
            String phoneNumber,
            String mobileNumber,
            boolean isExecutiveOfficer) throws ClientException;

}
