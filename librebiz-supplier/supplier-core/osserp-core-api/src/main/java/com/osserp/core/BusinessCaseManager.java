/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 3, 2007 12:35:25 PM 
 * 
 */
package com.osserp.core;

import com.osserp.common.ClientException;
import com.osserp.common.DetailsManager;
import com.osserp.common.User;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface BusinessCaseManager extends DetailsManager {

    /**
     * Indicates if a business case with a specific id exists
     * @param id
     * @return true if business case exists
     */
    boolean exists(Long id);

    /**
     * Loads business case from persistent storage
     * @param id
     * @return bc
     */
    BusinessCase load(Long id);

    /**
     * Updates the branch office on business case and all references of same context
     * @param businessCase
     * @param id
     */
    BusinessCase updateBranch(BusinessCase bc, Long id);

    /**
     * Sets the manager for this project
     * @param user
     * @param businessCase
     * @param employeeId
     */
    BusinessCase setManager(User user, BusinessCase businessCase, Long employeeId);

    /**
     * Sets the salesman for this project
     * @param user
     * @param businessCase
     * @param employeeId
     */
    BusinessCase setSales(User user, BusinessCase businessCase, Long employeeId);

    /**
     * Creates a new businessCase contract
     * @param user
     * @param businessCase contract reference
     * @return new created contract
     */
    BusinessContract createContract(User user, BusinessCase businessCase);

    /**
     * Provides the current contract referenced by businessCase 
     * @param businessCase
     * @return contract or null if not exists
     */
    BusinessContract getContract(BusinessCase businessCase);

    /**
     * Updates a contract
     * @param user
     * @param contract
     * @return updated contract
     */
    BusinessContract updateContract(User user, BusinessContract contract) throws ClientException;
}
