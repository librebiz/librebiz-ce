/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 07.10.2004 
 * 
 */
package com.osserp.common.dao;

import java.util.Map;

import javax.sql.DataSource;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface DataSourceFactory {

    /** key for the hostname property where the server is running */
    static final String SERVER = "serverName";

    /** key for the name of the database to connect to */
    static final String DATABASE = "databaseName";

    /** key for the TCP-Port of the database server */
    static final String PORT = "portNumber";

    /** key for the name of the database user */
    static final String USER = "user";

    /** key for the password of the database user */
    static final String PASSWORD = "password";

    /** key for the should autocommit set on the connection before it's passed to the app */
    static final String AUTOCOMMIT = "defaultAutoCommit";

    /** key for the name of the DataSource when using pooled connections */
    static final String DATA_SOURCE = "datasourceName";

    /** key for the initial connection pool size */
    static final String INITIAL_CONNECTIONS = "initialConnections";

    /** key for the initial connection pool size */
    static final String MAX_CONNECTIONS = "maxConnections";

    /**
     * gets a new data source
     * @return dataSource
     */
    DataSource getDataSource();

    /**
     * Closes an allocated data source
     */
    void closeDataSource();

    /**
     * Provides the properties used to initialize the datasource
     * @return properties
     */
    Map<String, Object> getProperties();

    /**
     * Sets a datasource property
     * @param name
     * @param value
     */
    void setProperty(String name, String value);
}
