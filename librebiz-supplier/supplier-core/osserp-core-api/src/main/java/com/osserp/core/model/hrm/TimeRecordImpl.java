/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 21, 2007 1:56:02 PM 
 * 
 */
package com.osserp.core.model.hrm;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.util.DateUtil;
import com.osserp.core.employees.Employee;
import com.osserp.core.hrm.TimeRecord;
import com.osserp.core.hrm.TimeRecordCorrection;
import com.osserp.core.hrm.TimeRecordStatus;
import com.osserp.core.hrm.TimeRecordType;
import com.osserp.core.hrm.TimeRecordingPeriod;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class TimeRecordImpl extends AbstractTimeRecord implements TimeRecord {
    private static Logger log = LoggerFactory.getLogger(TimeRecordImpl.class.getName());

    protected TimeRecordImpl() {
        super();
    }

    /**
     * Creates a new time record
     * @param period
     * @param type
     * @param status
     * @param createdBy
     * @param value
     * @param note
     * @param systemTime indicates that value was created by systime
     * @param terminalId
     */
    public TimeRecordImpl(
            TimeRecordingPeriod period,
            TimeRecordType type,
            TimeRecordStatus status,
            Employee createdBy,
            Date value,
            String note,
            boolean systemTime,
            Long terminalId) throws ClientException {
        super(type, status, period.getId(), createdBy, value, note, systemTime);
        if (!type.isStarting()) {
            TimeRecord tr = fetchStarting(period, value);
            if (tr == null) {
                if (log.isDebugEnabled()) {
                    log.debug("throw ErrorCode.BOOKING_START_MISSING [date=" + value + "]");
                }
                throw new ClientException(ErrorCode.BOOKING_START_MISSING);
            }
            setClosing(tr.getId());
        }
        setTerminalId(terminalId);
    }

    /**
     * Creates a new time record
     * @param period
     * @param type
     * @param status
     * @param createdBy
     * @param value
     * @param note
     * @param systemTime indicates that value was created by systime
     * @param terminalId
     */
    public TimeRecordImpl(
            TimeRecordingPeriod period,
            TimeRecordType type,
            TimeRecordStatus status,
            Long createdBy,
            Date value,
            String note,
            boolean systemTime,
            Long terminalId) throws ClientException {
        super(type, status, period.getId(), createdBy, value, note, systemTime);
        if (!type.isStarting()) {
            TimeRecord tr = fetchStarting(period, value);
            if (tr == null) {
                if (log.isDebugEnabled()) {
                    log.debug("throw ErrorCode.BOOKING_START_MISSING [date=" + value + "]");
                }
                throw new ClientException(ErrorCode.BOOKING_START_MISSING);
            }
            setClosing(tr.getId());
        }
        setTerminalId(terminalId);
    }

    private TimeRecord fetchStarting(TimeRecordingPeriod period, Date stopDate) {
        for (int i = 0, j = period.getRecords().size(); i < j; i++) {
            TimeRecord next = period.getRecords().get(i);
            if (next.getClosedBy() == null) {
                if (DateUtil.isSameDay(next.getValue(), stopDate)) {
                    return next;
                }
            }
        }
        return null;
    }

    /**
     * Creates a new time record
     * @param period
     * @param type
     * @param status
     * @param closingRecord
     */
    protected TimeRecordImpl(
            TimeRecordingPeriod period,
            TimeRecordType type,
            TimeRecordStatus status,
            TimeRecord closingRecord) {
        super(
                type,
                status,
                period.getId(),
                Constants.SYSTEM_EMPLOYEE,
                closingRecord.getValue(),
                null,
                false);
        setClosing(closingRecord.getId());
    }

    public TimeRecordCorrection createCorrection(Employee user, Date correction, String note, TimeRecordStatus status) throws ClientException {
        if (correction == null) {
            throw new ClientException(ErrorCode.DATE_MISSING);
        }
        if (isNotSet(note)) {
            throw new ClientException(ErrorCode.ACTION_NOTE_REQUIRED);
        }
        TimeRecordCorrectionImpl obj = new TimeRecordCorrectionImpl(user, this);
        setValue(correction);
        setNote(note);
        setCorrectionAvailable(true);
        setSystemTime(false);
        setStatus(status);
        return obj;
    }

    public void resetCorrection(TimeRecordCorrection correction) {
        if (getClosing() != null) {
            setValue(correction.getStopValue());
        } else {
            setValue(correction.getValue());
        }
        setStatus(correction.getStatus());
        setCorrectionAvailable(false);
        setNote(correction.getNote());
        setSystemTime(correction.isSystemTime());
    }

    @Override
    public Object clone() {
        return new TimeRecordImpl(this);
    }

    /**
     * Creates a new time record by other
     * @param other
     */
    private TimeRecordImpl(TimeRecord other) {
        super(other);
    }
}
