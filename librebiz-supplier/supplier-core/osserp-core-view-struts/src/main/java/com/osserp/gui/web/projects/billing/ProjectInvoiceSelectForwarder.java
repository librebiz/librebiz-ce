/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 26-Jun-2005 10:16:59 
 * 
 */
package com.osserp.gui.web.projects.billing;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.User;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.ViewManager;

import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.RecordType;
import com.osserp.gui.client.Actions;
import com.osserp.gui.client.sales.SalesBillingView;
import com.osserp.gui.client.sales.SalesInvoiceView;
import com.osserp.gui.client.sales.SalesUtil;
import com.osserp.gui.web.struts.AbstractSelectForwarder;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProjectInvoiceSelectForwarder extends AbstractSelectForwarder {
    private static Logger log = LoggerFactory.getLogger(ProjectInvoiceSelectForwarder.class.getName());

    @Override
    protected void setSelection(HttpServletRequest request, Long id) throws ClientException, PermissionException {
        User user = getUser(request);
        if (log.isDebugEnabled()) {
            log.debug("setSelection() request from " + user.getId()
                    + " with id " + id);
        }
        HttpSession session = request.getSession();
        SalesBillingView billingView = SalesUtil.getBillingView(session);
        Long recordType = RequestUtil.getLong(request, "type");
        Invoice invoice = billingView.fetchInvoice(id, recordType);
        SalesInvoiceView invoiceView = (SalesInvoiceView) ViewManager.newInstance(servlet.getServletContext()).createView(request,
                SalesInvoiceView.class.getName());
        if (RecordType.SALES_DOWNPAYMENT.equals(recordType)) {
            invoiceView.loadDownpayment(invoice.getId(), Actions.BILLING, null, false);
        } else {
            invoiceView.loadRecord(invoice.getId(), Actions.BILLING, null, false);
        }
        if (log.isDebugEnabled()) {
            log.debug("execute() view created for selected invoice");
        }
    }
}
