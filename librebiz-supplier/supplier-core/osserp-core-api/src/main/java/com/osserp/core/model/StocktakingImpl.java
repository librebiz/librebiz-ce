/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 20, 2006 9:06:21 PM 
 * 
 */
package com.osserp.core.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import com.osserp.common.Constants;
import com.osserp.common.Option;
import com.osserp.common.beans.AbstractOption;

import com.osserp.core.finance.Stocktaking;
import com.osserp.core.finance.StocktakingGroup;
import com.osserp.core.finance.StocktakingItem;
import com.osserp.core.products.Product;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class StocktakingImpl extends AbstractOption implements Stocktaking {

    private Long company = null;
    private Option type = null;
    private Option status = null;
    private Date recordDate = null;
    private String note;
    private Long writer;
    private Long counter;
    private List<StocktakingItem> items = new ArrayList<StocktakingItem>();

    protected StocktakingImpl() {
        super();
    }

    public StocktakingImpl(Long company, Option type, Option status, String name, Date recordDate, Long createdBy) {
        super(name, createdBy);
        this.company = company;
        this.recordDate = recordDate;
        this.type = type;
        this.status = status;
    }

    public Long getCompany() {
        return company;
    }

    protected void setCompany(Long company) {
        this.company = company;
    }

    public Long getBranchId() {
        // TODO find another solution
        return 1L;
    }

    public Option getType() {
        return type;
    }

    protected void setType(Option type) {
        this.type = type;
    }

    public Option getStatus() {
        return status;
    }

    public void setStatus(Option status) {
        this.status = status;
    }

    public Date getRecordDate() {
        return recordDate;
    }

    public void setRecordDate(Date recordDate) {
        this.recordDate = recordDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getCounter() {
        return counter;
    }

    public void setCounter(Long counter) {
        this.counter = counter;
    }

    public Long getWriter() {
        return writer;
    }

    public void setWriter(Long writer) {
        this.writer = writer;
    }

    public List<StocktakingItem> getItems() {
        return items;
    }

    public SortedSet<StocktakingGroup> getGroupedItems() {
        SortedSet<StocktakingGroup> list = new TreeSet<StocktakingGroup>();
        Map<Long, StocktakingGroup> map = new HashMap<Long, StocktakingGroup>();
        for (int i = 0, j = items.size(); i < j; i++) {
            StocktakingItem next = items.get(i);
            if (map.containsKey(next.getProduct().getGroup().getId())) {
                map.get(next.getProduct().getGroup().getId()).addItem(next);
            } else {
                StocktakingGroup grp = new StocktakingGroupImpl(next.getProduct().getGroup().getId(), next.getProduct().getGroup().getName());
                grp.addItem(next);
                map.put(grp.getId(), grp);
            }
        }
        Collection<StocktakingGroup> col = map.values();
        for (Iterator<StocktakingGroup> i = col.iterator(); i.hasNext();) {
            list.add(i.next());
        }
        return list;
    }

    public void addItem(Product product) {
        StocktakingItemImpl item = new StocktakingItemImpl(getId(), product);
        this.items.add(item);
    }

    public void setCountedAsExpected() {
        for (int i = 0, j = items.size(); i < j; i++) {
            StocktakingItem next = items.get(i);
            next.setCountedQuantity(next.getExpectedQuantity());
        }
    }

    public void resetCounters() {
        for (int i = 0, j = items.size(); i < j; i++) {
            StocktakingItem next = items.get(i);
            next.setCountedQuantity(Constants.DOUBLE_NULL);
        }
    }

    protected void setItems(List<StocktakingItem> items) {
        this.items = items;
    }

    public boolean isCloseable() {
        if (isClosed()) {
            return false;
        }
        for (int i = 0, j = items.size(); i < j; i++) {
            StocktakingItem item = items.get(i);
            if (item.getCountedQuantity() == null) {
                return false;
            }
        }
        return true;
    }

    public boolean isClosed() {
        return (status == null ? false : Stocktaking.CLOSED.equals(status.getId()));
    }
}
