/**
 *
 * Copyright (C) 2017, 2019 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 2, 2019
 * 
 */
package com.osserp.core.service.impl;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.common.PermissionException;
import com.osserp.common.UserAuthenticator;

import com.osserp.core.contacts.Salutation;
import com.osserp.core.dao.SystemSetup;
import com.osserp.core.system.InitialSetupService;
import com.osserp.core.system.SystemCompany;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractSetupService extends AbstractService implements InitialSetupService {

    private SystemSetup systemSetup;
    private UserAuthenticator userAuthenticator;

    public AbstractSetupService(SystemSetup systemSetup, UserAuthenticator userAuthenticator) {
        super();
        this.systemSetup = systemSetup;
        this.userAuthenticator = userAuthenticator;
    }

    protected DomainUser createUser(
            DomainUser admin,
            SystemCompany company,
            String companyName,
            Salutation salutation,
            Option title,
            String lastName,
            String firstName,
            String street,
            String zipcode,
            String city,
            Long country,
            Long federalState,
            String pcountry,
            String pprefix,
            String pnumber,
            String fcountry,
            String fprefix,
            String fnumber,
            String email,
            String loginName,
            String password,
            String confirmPassword,
            String currentPassword)
            throws ClientException, PermissionException {

        if (isNotSet(password) || isNotSet(confirmPassword) || isNotSet(currentPassword)) {
            throw new ClientException(ErrorCode.PASSWORD_MISSING);
        }
        if (!password.equals(confirmPassword)) {
            throw new ClientException(ErrorCode.PASSWORD_MATCH);
        }
        if (password.equals(currentPassword)) {
            throw new ClientException(ErrorCode.USER_PASSWORD_UNCHANGED);
        }
        DomainUser expectedUser = systemSetup.getExpectedUser(admin);
        userAuthenticator.authenticate(expectedUser.getLdapUid(), currentPassword, admin.getLastLoginIp());
        if (loginName == null || loginName.equals(expectedUser.getLdapUid())) {
            throw new ClientException(ErrorCode.USER_LOGINNAME_EXISTS);
        }
        DomainUser result = systemSetup.initCompany(
                admin,
                loginName,
                company,
                companyName,
                salutation,
                title,
                lastName,
                firstName,
                street,
                zipcode,
                city,
                country,
                federalState,
                pcountry,
                pprefix,
                pnumber,
                fcountry,
                fprefix,
                fnumber,
                email);
        return result;
    }

    protected SystemSetup getSystemSetup() {
        return systemSetup;
    }

    protected void setSystemSetup(SystemSetup systemSetup) {
        this.systemSetup = systemSetup;
    }

    protected UserAuthenticator getUserAuthenticator() {
        return userAuthenticator;
    }

    protected void setUserAuthenticator(UserAuthenticator userAuthenticator) {
        this.userAuthenticator = userAuthenticator;
    }

}
