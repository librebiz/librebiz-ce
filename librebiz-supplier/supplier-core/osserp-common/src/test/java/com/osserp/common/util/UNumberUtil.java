/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 12, 2010 12:55:02 AM 
 * 
 */
package com.osserp.common.util;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.junit.Test;

/**
 * @author Rainer Kirchner <rk@osserp.com>
 */
public class UNumberUtil {

    @Test
    public void performBigDecimalScaleAndRoundingModeExample() {
        BigDecimal multipliedValue = new BigDecimal("1.399").multiply(new BigDecimal(45));
        assertTrue("62.955".equals(multipliedValue.toString()));
        assertTrue("62.955".equals(multipliedValue.setScale(3, RoundingMode.HALF_UP).toString()));
        assertTrue("62.95500".equals(multipliedValue.setScale(5, RoundingMode.HALF_UP).toString()));
        assertTrue("62.96".equals(multipliedValue.setScale(2, RoundingMode.HALF_UP).toString()));
    }

    @Test
    public void performBigDecimalDividingWithoutScaleAndRoundingModeMayFailExample() {
        BigDecimal calculationResult = new BigDecimal("6735.3467999999999952542850678582908585667610168457031250");
        try {
            calculationResult.divide(new BigDecimal("5.46"));
            fail("performBigDecimalDividingWithoutScaleAndRoundingModeMayFailExample should throw ArithmeticException");
        } catch (ArithmeticException e) {
            assertTrue(e.getMessage().startsWith("Non-terminating decimal expansion"));
        }
    }

    @Test
    public void performBigDecimalDividingWithScaleAndRoundingModeWorksExample() {
        BigDecimal calculationResult = new BigDecimal("6735.3467999999999952542850678582908585667610168457031250");
        try {
            BigDecimal divisionResult = calculationResult.divide(new BigDecimal("5.46"), 5, RoundingMode.HALF_UP);
            assertTrue("1233.58000".equals(divisionResult.toString()));
        } catch (ArithmeticException e) {
            fail("performBigDecimalDividingWithoutScaleAndRoundingModeMayFailExample should not throw ArithmeticException");
        }
    }

}
