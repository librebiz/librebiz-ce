/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 13, 2008
 * 
 */
package com.osserp.common.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.osserp.common.PropertyAware;
import com.osserp.common.Property;
import com.osserp.common.User;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractPropertyAware extends AbstractEntity implements PropertyAware {

    private List<Property> propertyList = new ArrayList<>();
    private Map<String, Property> _properties = new HashMap<>();

    protected AbstractPropertyAware() {
        super();
    }

    protected AbstractPropertyAware(Long id, Long createdBy) {
        super(id, (Long) null, createdBy);
    }

    protected AbstractPropertyAware(AbstractPropertyAware other) {
        super(other);
        List<Property> proplist = other.propertyList;
        propertyList = new ArrayList<>();
        for (int i = 0, j = proplist.size(); i < j; i++) {
            propertyList.add((Property) proplist.get(i).clone());
        }
    }

    protected abstract Property createProperty(String name, String value);

    public boolean isPropertyEnabled(String propertyName) {
        Map<String, Property> props = getProperties();
        if (propertyName != null && props.containsKey(propertyName)) {
            return props.get(propertyName).isEnabled();
        }
        return false;
    }

    public Map<String, Property> getProperties() {
        if (_properties.isEmpty()) {
            for (int i = 0, j = propertyList.size(); i < j; i++) {
                Property next = propertyList.get(i);
                _properties.put(next.getName(), next);
            }
        }
        return _properties;
    }

    public void setProperty(User user, String name, String value) {
        if (getProperties().containsKey(name)) {
            for (java.util.Iterator<Property> i = propertyList.iterator(); i.hasNext();) {
                Property next = i.next();
                if (next.getName().equals(name)) {
                    if (value == null) {
                        i.remove();
                    } else {
                        next.setValue(value);
                    }
                }
            }
        } else if (value != null) {
            Property p = createProperty(name, value);
            propertyList.add(p);
            _properties.put(name, p);
        }
    }

    public Long propertyAsLong(String name, Long defaultValue) {
        Property prop = getProperties().get(name);
        Long result = prop == null ? null : prop.getAsLong();
        return result == null ? defaultValue : result;
    }

    protected List<Property> getPropertyList() {
        return propertyList;
    }

    protected void setPropertyList(List<Property> propertyList) {
        this.propertyList = propertyList;
    }
}
