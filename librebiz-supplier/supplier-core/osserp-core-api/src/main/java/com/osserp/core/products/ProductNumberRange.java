/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 5, 2009 10:47:56 AM 
 * 
 */
package com.osserp.core.products;

import com.osserp.common.Option;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ProductNumberRange extends Option {

    /**
     * The start of the product number range valid for this group
     * @return the rangeStart.
     */
    Long getRangeStart();

    /**
     * Sets the start of the product number range valid for this group
     * @param the rangeStart to set.
     */
    void setRangeStart(Long rangeStart);

    /**
     * The end of the product number range valid for this group
     * @return the rangeEnd.
     */
    Long getRangeEnd();

    /**
     * Sets the end of the product number range valid for this group
     * @param the rangeEnd to set.
     */
    void setRangeEnd(Long rangeEnd);

}
