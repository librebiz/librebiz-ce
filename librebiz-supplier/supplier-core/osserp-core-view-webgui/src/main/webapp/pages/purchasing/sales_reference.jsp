<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<c:set var="view" value="${sessionScope.recordView}"/>
<c:set var="record" value="${view.record}"/>
<c:set var="url" value="${requestScope.baseUrl}"/>
<c:set var="salesExitTarget" value="${requestScope.salesExitTarget}"/>
<%-- restrict change access if required
<c:if test="${!empty requestScope.changeSalesIdPermissionGrant}">
<c:set var="changePermissionGrant" value="true"/>
</c:if>
--%>
<c:set var="changePermissionGrant" value="true"/>
<c:choose>
    <c:when test="${view.salesIdEditMode}">
        <o:form name="recordForm" url="${url}">
              <input type="hidden" name="method" value="updateSalesId" />
            <div class="recordLabelValue">
            <fmt:message key="customerOrderLabel"/>:<span style="margin-left: 20px;"><input type="text" name="salesId" id="numberValue" value="<o:out value="${record.businessCaseId}"/>" style="width:200px;" /></span>
            <span style="margin-left: 10px;">
                <a href="<c:url value="${url}"><c:param name="method" value="disableSalesIdEditMode"/></c:url>" title="<fmt:message key="exitEdit"/>"><o:img name="backIcon" styleClass="bigicon"/></a>
            </span>
            <input type="image" name="method" src="<c:url value="${applicationScope.saveIcon}"/>" value="updateSalesId" onclick="setMethod('updateSalesId');" style="margin-left: 5px; margin-right: 5px;" class="bigicon" title="<fmt:message key="saveInput"/>"/>
            <o:permission role="purchasing,accounting,executive" info="permissionOrderByPurchase">
            <span style="padding-left:10px; padding-right:10px;"> - </span>
            <v:link url="/sales/orderByPurchase/forward?exit=/purchaseOrderReload.do">
                <fmt:message key="createNewOrder"/>
            </v:link>
            </span>
            </o:permission>
            </div>
        </o:form>
    </c:when>
    <c:otherwise>
        <div class="recordLabelValue">
            <c:choose>
                <c:when test="${changePermissionGrant}">
                    <a href="<c:url value="${url}"><c:param name="method" value="enableSalesIdEditMode"/></c:url>" title="<fmt:message key="change"/>">
                        <fmt:message key="customerOrderLabel"/>:
                    </a>
                </c:when>
                <c:otherwise>
                    <o:permission role="purchasing,accounting,executive" info="permissionChangePurchasingSalesId">
                        <a href="<c:url value="${url}"><c:param name="method" value="enableSalesIdEditMode"/></c:url>" title="<fmt:message key="change"/>">
                            <fmt:message key="customerOrderLabel"/>:
                        </a>
                    </o:permission>
                    <o:forbidden role="purchasing,accounting,executive">
                        <fmt:message key="customerOrderLabel"/>:
                    </o:forbidden>
                </c:otherwise>
            </c:choose>
            <c:choose>
                <c:when test="${recordView.exitTarget == 'sales' or recordView.exitTarget == 'salesOrderDisplay' or recordView.exitTarget == 'salesOrderReload'}">
                    <span style="margin-left: 20px;"><o:out value="${record.businessCaseId}"/></span>
                </c:when>
                <c:otherwise>
                    <span style="margin-left: 20px;">
                        <c:choose>
                            <c:when test="${empty record.businessCaseId}">
                                <fmt:message key="notAssigned"/>
                            </c:when>
                            <c:otherwise>
                                <a href="<c:url value="/loadSales.do?id=${record.businessCaseId}&exit=${salesExitTarget}"/>"><o:out value="${record.businessCaseId}"/></a>
                            </c:otherwise>
                        </c:choose>
                    </span>
                </c:otherwise>
            </c:choose>
            <c:if test="${!empty recordView.referencedSales}">
                <span style="margin-left: 20px;">
                    <o:out value="${recordView.referencedSales.name}"/>
                    <c:if test="${!empty recordView.referencedSales.request.deliveryDate}">
                        - <fmt:message key="deliveryDate"/>: <o:date value="${recordView.referencedSales.request.deliveryDate}"/>
                    </c:if>
                </span>
            </c:if>
        </div>
    </c:otherwise>
</c:choose>
