/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 24-Oct-2005 09:03:05 
 * 
 */
package com.osserp.core.events;

import com.osserp.common.Entity;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface EventConfig extends Entity {

    /**
     * Indicates whether this config is an action or termination
     * @return true if action, otherwise termination
     */
    boolean isAction();

    /**
     * Returns the related type
     * @return EventConfigType type
     */
    EventConfigType getType();

    /**
     * Returns the related caller id
     * @return callerId
     */
    Long getCallerId();

    /**
     * Sets the caller id
     * @param callerId
     */
    void setCallerId(Long callerId);

    /**
     * Indicates that corresponding action is an alert
     * @return true if alert
     */
    boolean isAlert();
    
    /**
     * Provides an optional id of another eventAction of type info.
     * If set, this action is used to create informations about this
     * event to other recipients.  
     * @return infoActionId
     */
    Long getInfoActionId();
    
    /**
     * Sets an optional id of another eventAction of type info.
     * @param infoActionId
     */
    void setInfoActionId(Long infoActionId);
}
