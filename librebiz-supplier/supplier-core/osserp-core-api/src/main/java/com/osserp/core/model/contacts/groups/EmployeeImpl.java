/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.model.contacts.groups;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jdom2.Element;

import com.osserp.common.Constants;
import com.osserp.common.mail.EmailAddress;
import com.osserp.common.util.NameUtil;
import com.osserp.core.Address;
import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.OfficePhone;
import com.osserp.core.contacts.Phone;
import com.osserp.core.contacts.PhoneType;
import com.osserp.core.employees.Employee;
import com.osserp.core.employees.EmployeeDescription;
import com.osserp.core.employees.EmployeeGroup;
import com.osserp.core.employees.EmployeeRole;
import com.osserp.core.employees.EmployeeRoleConfig;
import com.osserp.core.employees.EmployeeSignatures;
import com.osserp.core.employees.EmployeeStatus;
import com.osserp.core.employees.EmployeeType;
import com.osserp.core.model.contacts.AbstractContactAware;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.system.SystemCompany;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class EmployeeImpl extends AbstractContactAware implements Employee {
    private static Logger log = LoggerFactory.getLogger(EmployeeImpl.class.getName());
    private static final String MAIL_ACCOUNT_PREFIX = "mx";
    private EmployeeType employeeType = null;
    private Long accountingId;
    private boolean active;
    private String initials;
    private Date beginDate;
    private Date endDate;
    private Long defaultBranchId = null;
    private Long defaultGroupId = null;
    private Long printPhoneDevice = null;

    private boolean drawsSalary;
    private boolean drawsProvision;
    private boolean timeAndMaterial;
    private boolean serviceContract;
    private boolean consultant;
    private boolean timeWorker;
    private boolean salesPartner;

    private String role = null;
    private boolean emailAccount = false;
    private boolean emailCreated = false;
    private boolean businessCard = false;
    private String businessCardText = null;
    private boolean internationalBusinessCard = false;
    private String internationalBusinessCardText = null;
    private boolean cellularPhone = false;
    private boolean timeRecordingEnabled = false;
    private Long internetPhoneId = null;
    private boolean internetExportEnabled = false;
    private String defaultLanguage = Constants.DEFAULT_COUNTRY;
    private String costCenter = null;
    private String signatureSource = null;
    private Long defaultStock = null;
    private Long defaultDisciplinarian = null;

    private List<OfficePhone> officePhones = new ArrayList<OfficePhone>();
    private List<EmployeeDescription> descriptions = new ArrayList<EmployeeDescription>();
    private List<EmployeeRoleConfig> roleConfigs = new ArrayList<EmployeeRoleConfig>();

    protected EmployeeImpl() {
        setGroupId(Employee.EMPLOYEE);
    }

    /**
     * Creates a new employee with a prefetched id. This is the only valid 
     * method to create new employees. ORM config does not accept null as id. 
     * @param id required primary ky 
     * @param createdBy
     * @param contact
     * @param employeeType
     */
    public EmployeeImpl(Long id, Long createdBy, Contact contact, EmployeeType employeeType) {
        super(id, Employee.EMPLOYEE, createdBy, contact, null);
        this.employeeType = employeeType;
        this.active = true;
        this.initials = NameUtil.createInitials(contact.getFirstName(), contact.getLastName());
    }

    /**
     * Creates a clone of the provided object including id.
     * @param o
     */
    public EmployeeImpl(EmployeeImpl o) {
        super(o);
        if (o.getRc() != null) {
            // super does not clone rc
            setRc((Contact) o.getRc().clone());
        }
        employeeType = o.getEmployeeType();
        accountingId = o.getAccountingId();
        active = o.isActive();
        initials = o.getInitials();
        beginDate = o.getBeginDate();
        endDate = o.getEndDate();
        defaultBranchId = o.getDefaultBranchId();
        defaultGroupId = o.getDefaultGroupId();
        printPhoneDevice = o.getPrintPhoneDevice();
        drawsSalary = o.isDrawsSalary();
        drawsProvision = o.isDrawsProvision();
        timeAndMaterial = o.isTimeAndMaterial();
        serviceContract = o.isServiceContract();
        consultant = o.isConsultant();
        timeWorker = o.isTimeWorker();
        salesPartner = o.isSalesPartner();
        role = o.getRole();
        emailAccount = o.isEmailAccount();
        emailCreated = o.isEmailCreated();
        businessCard = o.isBusinessCard();
        businessCardText = o.getBusinessCardText();
        internationalBusinessCard = o.isInternationalBusinessCard();
        internationalBusinessCardText = o.getInternationalBusinessCardText();
        cellularPhone = o.isCellularPhone();
        timeRecordingEnabled = o.isTimeRecordingEnabled();
        internetPhoneId = o.getInternetPhoneId();
        internetExportEnabled = o.isInternetExportEnabled();
        defaultLanguage = o.getDefaultLanguage();
        costCenter = o.getCostCenter();
        signatureSource = o.getSignatureSource();
        defaultStock = o.getDefaultStock();
        defaultDisciplinarian = o.getDefaultDisciplinarian();

        officePhones = new ArrayList<OfficePhone>();
        if (o.getOfficePhones() != null) {
            for (int i = 0, j = o.getOfficePhones().size(); i < j; i++) {
                OfficePhone next = o.getOfficePhones().get(i);
                officePhones.add((OfficePhone) next.clone());
            }
        }
        descriptions = new ArrayList<EmployeeDescription>();
        if (o.getDescriptions() != null) {
            for (int i = 0, j = o.getDescriptions().size(); i < j; i++) {
                EmployeeDescription next = o.getDescriptions().get(i);
                descriptions.add((EmployeeDescription) next.clone());
            }
        }
        roleConfigs = new ArrayList<EmployeeRoleConfig>();
        if (o.getRoleConfigs() != null) {
            for (int i = 0, j = o.getRoleConfigs().size(); i < j; i++) {
                EmployeeRoleConfig next = o.getRoleConfigs().get(i);
                roleConfigs.add((EmployeeRoleConfig) next.clone());
            }
        }
    }

    @Override
    public Object clone() {
        return new EmployeeImpl(this);
    }

    public EmployeeGroup getDefaultGroup() {
        EmployeeRole defaultRole = getDefaultRole();
        if (defaultRole != null) {
            defaultGroupId = defaultRole.getGroup().getId();
            return defaultRole.getGroup();
        }
        return null;
    }

    public void setEmployeeType(EmployeeType employeeType) {
        this.employeeType = employeeType;
    }

    public void setAccountingId(Long accountingId) {
        this.accountingId = accountingId;
    }

    public Long getAccountingId() {
        return accountingId;
    }

    public EmployeeType getEmployeeType() {
        return employeeType;
    }

    public BranchOffice getBranch() {
        EmployeeRoleConfig cfg = getDefaultRoleConfig();
        return cfg == null ? null : cfg.getBranch();
    }

    public boolean isIgnoringBranch() {
        if (roleConfigs != null) {
            for (int i = 0, j = roleConfigs.size(); i < j; i++) {
                EmployeeRoleConfig cfg = roleConfigs.get(i);
                for (int k = 0, l = cfg.getRoles().size(); k < l; k++) {
                    EmployeeRole nextRole = cfg.getRoles().get(k);
                    if (nextRole.isIgnoringBranch()) {
                        return true;
                    }
                }
            }
        } else if (log.isDebugEnabled()) {
            log.debug("isIgnoringBranch() role configs list is null!");
        }
        return false;
    }

    public boolean isFromBranch(Long branchId) {
        if (roleConfigs != null) {
            for (int i = 0, j = roleConfigs.size(); i < j; i++) {
                EmployeeRoleConfig cfg = roleConfigs.get(i);
                if (cfg.getBranch() != null && cfg.getBranch().getId().equals(branchId)) {
                    return true;
                }
            }
        } else if (log.isDebugEnabled()) {
            log.debug("isFromBranch() role configs list is null!");
        }
        return false;
    }


    public boolean isSupportingBranch(BranchOffice branch) {
        if (branch != null && getRoleConfigs() != null) {
            for (int i = 0, j = getRoleConfigs().size(); i < j; i++) {
                EmployeeRoleConfig next = getRoleConfigs().get(i);
                if (next.getBranch().getId().equals(branch.getId())) {
                    log.debug("isSupportingBranch() grant by matching id [employee="
                            + getId() + ", branch=" + branch.getId() + "]");
                    return true;
                }
                // user not from thirdparty
                if (!next.getBranch().isThirdparty()
                        // and from headquarter or holding 
                        && ((next.getBranch().isHeadquarter()
                                && next.getBranch().getCompany().getId().equals(
                                        branch.getCompany().getId()))
                            || next.getBranch().getCompany().isHolding())) {
                    if (log.isDebugEnabled()) {
                        log.debug("isSupportingBranch() grant by hq or holding [employee="
                                + getId() + ", branch=" + branch.getId() + "]");
                    }
                    return true;
                }
                for (int k = 0, l = next.getRoles().size(); k < l; k++) {
                    EmployeeRole nextRole = next.getRoles().get(k);
                    if (nextRole.isIgnoringBranch()) {
                        if (log.isDebugEnabled()) {
                            log.debug("isSupportingBranch() grant by role [employee="
                                    + getId() + ", name=" + nextRole.getName() + "]"); 
                        }
                        return true;
                    }
                }
                if (log.isDebugEnabled()) {
                    log.debug("isSupportingBranch() found unsupported [employee="
                            + getId() + ", id=" + next.getBranch().getId() + "]"); 
                }
            }
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.employees.Employee#isSupportingCompany(java.lang.Long)
     */
    public boolean isSupportingCompany(Long companyId) {
        if (companyId != null) {
            for (int i = 0, j = getRoleConfigs().size(); i < j; i++) {
                EmployeeRoleConfig next = getRoleConfigs().get(i);
                if (companyId.equals(next.getBranch().getCompany().getId())) {
                    return true;
                }
            }
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.employees.Employee#isCompanyExecutive(java.lang.Long)
     */
    public boolean isCompanyExecutive(Long companyId) {
        if (companyId != null) {
            for (int i = 0, j = getRoleConfigs().size(); i < j; i++) {
                EmployeeRoleConfig next = getRoleConfigs().get(i);
                if (companyId.equals(next.getBranch().getCompany().getId())) {
                    for (int k = 0, l = next.getRoles().size(); k < l; k++) {
                        EmployeeRole nextRole = next.getRoles().get(k);
                        if (nextRole.getGroup() != null && nextRole.getGroup().isExecutiveCompany()) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public boolean isCompanyExecutive() {
        for (int i = 0, j = getRoleConfigs().size(); i < j; i++) {
            EmployeeRoleConfig next = getRoleConfigs().get(i);
            for (int k = 0, l = next.getRoles().size(); k < l; k++) {
                EmployeeRole nextRole = next.getRoles().get(k);
                if (nextRole.getGroup() != null && nextRole.getGroup().isExecutiveCompany()) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isSystemAccount() {
        return Constants.SYSTEM_EMPLOYEE.equals(getId());
    }

    public String getCostCenter() {
        return costCenter;
    }

    public void setCostCenter(String costCenter) {
        this.costCenter = costCenter;
    }

    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public boolean isDrawsSalary() {
        return drawsSalary;
    }

    public void setDrawsSalary(boolean drawsSalary) {
        this.drawsSalary = drawsSalary;
    }

    public boolean isDrawsProvision() {
        return drawsProvision;
    }

    public void setDrawsProvision(boolean drawsProvision) {
        this.drawsProvision = drawsProvision;
    }

    public boolean isTimeAndMaterial() {
        return timeAndMaterial;
    }

    public void setTimeAndMaterial(boolean timeAndMaterial) {
        this.timeAndMaterial = timeAndMaterial;
    }

    public boolean isServiceContract() {
        return serviceContract;
    }

    public void setServiceContract(boolean serviceContract) {
        this.serviceContract = serviceContract;
    }

    public boolean isConsultant() {
        return consultant;
    }

    public void setConsultant(boolean consultant) {
        this.consultant = consultant;
    }

    public boolean isTimeWorker() {
        return timeWorker;
    }

    public void setTimeWorker(boolean timeWorker) {
        this.timeWorker = timeWorker;
    }

    public boolean isSalesPartner() {
        return salesPartner;
    }

    public void setSalesPartner(boolean salesPartner) {
        this.salesPartner = salesPartner;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public boolean isEmailAccount() {
        return emailAccount;
    }

    public void setEmailAccount(boolean emailAccount) {
        this.emailAccount = emailAccount;
    }

    public boolean isEmailCreated() {
        return emailCreated;
    }

    public void setEmailCreated(boolean emailCreated) {
        this.emailCreated = emailCreated;
    }

    public String createMailAccountName() {
        return NameUtil.createAccountName(MAIL_ACCOUNT_PREFIX,
                getFirstName(), getLastName(), getId());
    }

    public boolean isBusinessCard() {
        return businessCard;
    }

    public void setBusinessCard(boolean businessCard) {
        this.businessCard = businessCard;
    }

    public String getBusinessCardText() {
        return businessCardText;
    }

    public void setBusinessCardText(String businessCardText) {
        this.businessCardText = businessCardText;
    }

    public boolean isInternationalBusinessCard() {
        return internationalBusinessCard;
    }

    public void setInternationalBusinessCard(boolean internationalBusinessCard) {
        this.internationalBusinessCard = internationalBusinessCard;
    }

    public String getInternationalBusinessCardText() {
        return internationalBusinessCardText;
    }

    public void setInternationalBusinessCardText(
            String internationalBusinessCardText) {
        this.internationalBusinessCardText = internationalBusinessCardText;
    }

    public boolean isCellularPhone() {
        return cellularPhone;
    }

    public void setCellularPhone(boolean cellularPhone) {
        this.cellularPhone = cellularPhone;
    }

    public boolean isTimeRecordingEnabled() {
        return timeRecordingEnabled;
    }

    public void setTimeRecordingEnabled(boolean timeRecordingEnabled) {
        this.timeRecordingEnabled = timeRecordingEnabled;
    }

    public String getDefaultLanguage() {
        return defaultLanguage;
    }

    public void setDefaultLanguage(String defaultLanguage) {
        this.defaultLanguage = defaultLanguage;
    }

    public Long getDefaultStock() {
        return defaultStock;
    }

    public void setDefaultStock(Long defaultStock) {
        this.defaultStock = defaultStock;
    }

    public Long getDefaultDisciplinarian() {
        return defaultDisciplinarian;
    }

    public void setDefaultDisciplinarian(Long defaultDisciplinarian) {
        this.defaultDisciplinarian = defaultDisciplinarian;
    }

    public String getSignatureSource() {
        return signatureSource;
    }

    public void setSignatureSource(String signatureSource) {
        this.signatureSource = signatureSource;
    }

    public String getSignature() {
        if (EmployeeSignatures.NONE.equalsIgnoreCase(signatureSource)) {
            return "";
        }
        if (EmployeeSignatures.ROLE.equalsIgnoreCase(signatureSource)) {
            if (isNotSet(role)) {
                return "";
            }
            return role;
        }
        if (EmployeeSignatures.GROUP.equalsIgnoreCase(signatureSource)) {
            return getSignatureString();
        }
        if (isSet(role)) {
            return role;
        }
        return getSignatureString();
    }

    private String getSignatureString() {
        EmployeeRole defRole = getDefaultRole();
        /*
        if (log.isDebugEnabled()) {
            log.debug("getSignatureString() invoked [id=" + getId()
                    + ", name=" + getDisplayName()
                    + ", defaultRole=" + (defRole != null)
                    + "]");
        }
        */
        if (defRole != null) {
            if (defRole.getStatus().isPrintStatusIgnoreGroup()) {
                /*
                if (log.isDebugEnabled()) {
                    log.debug("getSignatureString() status has ignoring group enabled [id=" + getId()
                            + ", name=" + getDisplayName()
                            + ", result=" + defRole.getStatus().getName()
                            + "]");
                }
                */
                return defRole.getStatus().getName();

            }
            if (defRole.getStatus().isPrintStatus() && defRole.getStatus().getPrintStatusSeparator() != null) {
                StringBuilder groupDisplay = new StringBuilder();
                if (defRole.getStatus().isPrintStatusFirst()) {
                    groupDisplay
                            .append(defRole.getStatus().getName())
                            .append(defRole.getStatus().getPrintStatusSeparator())
                            .append(defRole.getGroup().getName());
                } else {
                    groupDisplay
                            .append(defRole.getGroup().getName())
                            .append(defRole.getStatus().getPrintStatusSeparator())
                            .append(defRole.getStatus().getName());
                }
                /*
                if (log.isDebugEnabled()) {
                    log.debug("getSignatureString() signature is merged by status [id=" + getId()
                            + ", result=" + groupDisplay.toString()
                            + "]");
                }
                */
                return groupDisplay.toString();
            }
            /*
            if (log.isDebugEnabled()) {
                log.debug("getSignatureString() signature is group [id=" + getId()
                        + ", group=" + defRole.getGroup().getName()
                        + "]");
            }
            */
            return defRole.getGroup().getName();
        }
        /*
        if (log.isDebugEnabled()) {
            log.debug("getSignatureString() no signature found [id=" + getId() + "]");
        }
        */
        return "";
    }

    public Phone getInternetPhone() {
        if (internetPhoneId == null || internetPhoneId == 0) {
            return null;
        }
        List<Phone> numbers = getPhoneNumbers();
        for (int i = 0, j = numbers.size(); i < j; i++) {
            Phone next = numbers.get(i);
            if (next.getId().equals(internetPhoneId)) {
                return next;
            }
        }
        return null;
    }

    protected Long getInternetPhoneId() {
        return internetPhoneId;
    }

    public void setInternetPhoneId(Long internetPhoneId) {
        this.internetPhoneId = internetPhoneId;
    }

    public boolean isInternetExportEnabled() {
        return internetExportEnabled;
    }

    public void setInternetExportEnabled(boolean internetExportEnabled) {
        this.internetExportEnabled = internetExportEnabled;
    }

    public EmployeeDescription getDescription() {
        int size = descriptions == null ? 0 : descriptions.size();
        EmployeeDescription result = null;
        if (size > 0) {
            if (size == 1) {
                result = descriptions.get(0);
            } else {
                for (int i = 0; i < size; i++) {
                    EmployeeDescription next = descriptions.get(i);
                    if (next.isDefaultDescription()) {
                        result = next;
                        break;
                    }
                }
            }
        }
        return result;
    }

    public EmployeeDescription getDescription(Long configId) {
        int size = descriptions == null ? 0 : descriptions.size();
        for (int i = 0; i < size; i++) {
            EmployeeDescription next = descriptions.get(i);
            if (next.getI18nConfigId().equals(configId)) {
                return next;
            }
        }
        return null;
    }

    public void updateDescription(
            Employee user,
            Long configId,
            String name,
            String description,
            boolean defaultDescription) {

        boolean existing = false;
        boolean defaultDescriptionChanged = false;
        for (Iterator<EmployeeDescription> i = descriptions.iterator(); i.hasNext();) {
            EmployeeDescription next = i.next();
            if (next.getI18nConfigId().equals(configId)) {
                existing = true;
                next.setChanged(new Date(System.currentTimeMillis()));
                next.setChangedBy(user.getId());
                next.setDescription(description);
                next.setName(name);
                if (defaultDescription != next.isDefaultDescription()) {
                    defaultDescriptionChanged = true;
                }
                next.setDefaultDescription(defaultDescription);
            } else if (defaultDescription) {
                next.setDefaultDescription(false);
            }
        }
        if (!existing) {
            EmployeeDescriptionImpl obj = new EmployeeDescriptionImpl(
                    user,
                    configId,
                    this,
                    name,
                    description,
                    defaultDescription);
            if (descriptions.size() == 0) {
                obj.setDefaultDescription(true);
            } else if (defaultDescription) {
                for (Iterator<EmployeeDescription> i = descriptions.iterator(); i.hasNext();) {
                    EmployeeDescription next = i.next();
                    next.setDefaultDescription(false);
                }
            }
            this.descriptions.add(obj);
        } else {
            if (!defaultDescription && defaultDescriptionChanged) {
                for (Iterator<EmployeeDescription> i = descriptions.iterator(); i.hasNext();) {
                    EmployeeDescription next = i.next();
                    if (!next.getI18nConfigId().equals(configId)) {
                        next.setDefaultDescription(true);
                        break;
                    }
                }
            }
        }
    }

    public boolean isRoleAssigned(Long groupId) {
        for (int i = 0, j = roleConfigs.size(); i < j; i++) {
            EmployeeRoleConfig roleConfig = roleConfigs.get(i);
            if (roleConfig.getBranch() != null && !roleConfig.getBranch().getGroups().isEmpty()) {
                for (int k = 0, l = roleConfig.getBranch().getGroups().size(); k < l; k++) {
                    EmployeeGroup group = roleConfig.getBranch().getGroups().get(k);
                    if (group.getId().equals(groupId)) {
                        return true;
                    }
                }
            }
            for (int k = 0, l = roleConfig.getRoles().size(); k < l; k++) {
                EmployeeRole next = roleConfig.getRoles().get(k);
                if (next.getGroup().getId().equals(groupId)) {
                    return true;
                }
            }
        }
        return false;
    }

    public List<EmployeeRoleConfig> getRoleConfigs() {
        return roleConfigs;
    }

    protected void setRoleConfigs(List<EmployeeRoleConfig> roleConfigs) {
        this.roleConfigs = roleConfigs;
    }

    public void addRoleConfig(Employee user, BranchOffice branch, String description) {
        boolean alreadyAdded = false;
        for (int i = 0, j = roleConfigs.size(); i < j; i++) {
            EmployeeRoleConfig next = roleConfigs.get(i);
            if (next.getBranch().getId().equals(branch.getId())) {
                alreadyAdded = true;
            }
        }
        if (!alreadyAdded) {
            EmployeeRoleConfigImpl obj = new EmployeeRoleConfigImpl(
                    this, // employee
                    branch,
                    user,
                    roleConfigs.size() == 0);
            if (isSet(description)) {
                obj.setDescription(description);
            }
            roleConfigs.add(obj);
        }
    }

    public void removeRoleConfig(Long id) {
        for (Iterator<EmployeeRoleConfig> i = roleConfigs.iterator(); i.hasNext();) {
            EmployeeRoleConfig next = i.next();
            if (next.getId().equals(id)) {
                i.remove();
            }
        }
    }

    public void addRole(EmployeeRoleConfig config, Employee user, EmployeeGroup group, EmployeeStatus status) {
        for (Iterator<EmployeeRoleConfig> i = roleConfigs.iterator(); i.hasNext();) {
            EmployeeRoleConfig next = i.next();
            if (next.getId().equals(config.getId())) {
                next.addRole(user, group, status);
            }
        }
    }

    public void removeRole(EmployeeRoleConfig config, Employee user, Long id) {
        for (Iterator<EmployeeRoleConfig> i = roleConfigs.iterator(); i.hasNext();) {
            EmployeeRoleConfig next = i.next();
            if (next.getId().equals(config.getId())) {
                next.removeRole(user, id);
            }
        }
    }

    public void setDefaultRole(EmployeeRoleConfig config, Employee user, Long id) {
        for (Iterator<EmployeeRoleConfig> i = roleConfigs.iterator(); i.hasNext();) {
            EmployeeRoleConfig next = i.next();
            if (next.getId().equals(config.getId())) {
                next.setDefaultRole(user, id);
            }
        }
    }

    public EmployeeRoleConfig getDefaultRoleConfig() {
        EmployeeRoleConfig result = null;
        if (roleConfigs.size() == 1) {
            result = roleConfigs.get(0);
        } else {
            for (int i = 0, j = roleConfigs.size(); i < j; i++) {
                EmployeeRoleConfig next = roleConfigs.get(i);
                if (next.isDefaultRole()) {
                    result = next;
                    break;
                }
            }
        }
        return result;
    }

    public EmployeeRole getDefaultRole() {
        EmployeeRoleConfig config = getDefaultRoleConfig();
        if (config != null) {
            for (int i = 0, j = config.getRoles().size(); i < j; i++) {
                EmployeeRole next = config.getRoles().get(i);
                if (next.isDefaultRole()) {
                    return next;
                }
            }
        }
        return null;
    }

    public OfficePhone getInternalPhone() {
        return (officePhones == null
                || officePhones.isEmpty()
                ? null : officePhones.get(0));
    }

    public void setInternalPhone(OfficePhone internalPhone) {
        if (officePhones == null) {
            officePhones = new ArrayList<OfficePhone>();
        }
        if (officePhones.isEmpty()) {
            officePhones.add(internalPhone);
        } else {
            officePhones.set(0, internalPhone);
        }
    }

    public void removeInternalPhone() {
        for (Iterator<OfficePhone> i = officePhones.iterator(); i.hasNext();) {
            i.next();
            i.remove();
        }
    }

    public String getFullname() {
        StringBuffer buffer = new StringBuffer();
        if (getFirstName() != null) {
            buffer.append(getFirstName()).append(" ");
        }
        buffer.append(getLastName());
        return buffer.toString();
    }

    public Long getPrintPhoneDevice() {
        return printPhoneDevice;
    }

    public void setPrintPhoneDevice(Long printPhoneDevice) {
        this.printPhoneDevice = printPhoneDevice;
    }

    public Phone getPhoneForRecord() {
        if (PhoneType.PHONE.equals(printPhoneDevice)) {
            return getPhone();
        } else if (PhoneType.MOBILE.equals(printPhoneDevice)) {
            return getMobile();
        } else if (PhoneType.OFFICE.equals(printPhoneDevice)) {
            return getInternalPhone();
        } else {
            return null;
        }
    }

    public Map<String, Object> getGroupwareValues() {
        Phone businessPhone = getInternalPhone();
        Phone homePhone = null;
        Phone mobilePhone = null;
        for (int i = 0, j = getPhones().size(); i < j; i++) {
            Phone next = getPhones().get(i);
            if (PhoneType.PHONE.equals(next.getDevice())) {
                if (Phone.PRIVATE.equals(next.getType())) {
                    if (homePhone == null || !homePhone.isPrimary()) {
                        homePhone = next;
                    }
                }
            } else if (PhoneType.MOBILE.equals(next.getDevice())) {
                if (mobilePhone == null || !mobilePhone.isPrimary()) {
                    mobilePhone = next;
                }
            }
        }
        if (mobilePhone == null) {
            mobilePhone = getMobile();
        }
        EmployeeDescription description = getDescription();
        Address address = getAddress();
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", getId().toString());
        for (int i = 0, j = getEmails().size(); i < j; i++) {
            EmailAddress next = getEmails().get(i);
            if (next.isInternal()) {
                result.put("email", next.getEmail());
                if (log.isDebugEnabled()) {
                    log.debug("getGroupwareValues() internal mail assigned [email=" + next.getEmail() + "]");
                }
                break;
            }
        }
        if (!getEmails().isEmpty()) {
            List<String> aliases = new ArrayList<String>();
            for (int i = 0, j = getEmails().size(); i < j; i++) {
                EmailAddress next = getEmails().get(i);
                if (next.isAlias()) {
                    aliases.add(next.getEmail());
                }
            }
            result.put("alias", aliases);
        }
        result.put("gecosName", createMailAccountName());
        if (defaultLanguage != null) {
            result.put("mobilePhone", defaultLanguage);
        }
        if (mobilePhone != null) {
            result.put("mobilePhone", mobilePhone.getFormattedOutlookNumber());
        }
        if (homePhone != null) {
            result.put("homePhone", homePhone.getFormattedOutlookNumber());
        }
        if (businessPhone != null) {
            result.put("businessPhone", businessPhone.getFormattedOutlookNumber());
        }
        if (description != null) {
            result.put("description", description.getName());
        }
        if (getPosition() != null) {
            result.put("position", getPosition());
        }
        if (getSalutation() != null) {
            result.put("title", getSalutationDisplayShort());
        }
        if (address != null) {
            if (address.getStreet() != null) {
                result.put("street", address.getStreet());
            }
            if (address.getZipcode() != null) {
                result.put("zipcode", address.getZipcode());
            }
            if (address.getCity() != null) {
                result.put("city", address.getCity());
            }
        }
        String roleString = getRole();
        if (roleString != null) {
            result.put("role", roleString);
        }
        EmployeeRoleConfig rc = getDefaultRoleConfig();
        if (rc != null) {
            BranchOffice bo = rc.getBranch();
            if (bo != null) {
                if (bo.getName() != null) {
                    result.put("branchName", bo.getName());
                }
                SystemCompany company = bo.getCompany();
                if (company != null) {
                    if (company.getName() != null) {
                        result.put("companyName", company.getName());
                    }
                    if (company.getContact() != null) {
                        Contact cpc = company.getContact();
                        if (cpc.getAddress() != null) {
                            if (cpc.getAddress().getStreet() != null) {
                                result.put("companyStreet", cpc.getAddress().getStreet());
                            }
                            if (cpc.getAddress().getZipcode() != null) {
                                result.put("companyZipcode", cpc.getAddress().getZipcode());
                            }
                            if (cpc.getAddress().getCity() != null) {
                                result.put("companyCity", cpc.getAddress().getCity());
                            }
                        }
                    }
                }
            }
        }
        return result;
    }

    public Element getRecordXML(String elementName) {
        Element root = new Element(elementName);
        root.addContent(new Element("name").setText(getFullname()));
        Phone p = getPhoneForRecord();
        if (p == null) {
            root.addContent(new Element("phone"));
        } else {
            root.addContent(new Element("phone").setText(p.getFormattedNumber()));
        }
        EmployeeRole erole = getDefaultRole();
        if (erole == null) {
            root.addContent(new Element("group"));
        } else {
            root.addContent(new Element("group").setText(erole.getGroup().getName()));
        }
        return root;
    }

    @Override
    public Element getXML() {
        Element root = new Element("employee");
        if (getId() != null) {
            root.addContent(new Element("employeeId").setText(getId().toString()));
        }
        if (getFirstName() != null) {
            root.addContent(new Element("firstName").setText(getFirstName()));
        }
        if (getLastName() != null) {
            root.addContent(new Element("lastName").setText(getLastName()));
        }
        root.addContent(getAddressXML());
        Phone p = getPhone();
        if (p != null) {
            root.addContent(new Element("phone").setText(p.getFormattedNumber()));
        }
        Phone f = getPhone();
        if (f != null) {
            root.addContent(new Element("fax").setText(f.getFormattedNumber()));
        }
        Phone m = getMobile();
        if (m != null) {
            root.addContent(new Element("mobile").setText(m.getFormattedNumber()));
        }
        String email = getEmail();
        if (email != null) {
            root.addContent(new Element("email").setText(email));
        }
        OfficePhone op = getInternalPhone();
        if (op != null) {
            root.addContent(new Element("internalPhone").setText(op.getFormattedNumber()));
            root.addContent(new Element("directDial").setText(op.getInternal()));
        }
        Phone internetPhone = getInternetPhone();
        if (internetPhone == null) {
            root.addContent(new Element("internetPhone"));
        } else {
            root.addContent(new Element("internetPhone").setText(internetPhone.getFormattedNumber()));
        }
        EmployeeRoleConfig rc = getDefaultRoleConfig();
        if (rc != null) {
            StringBuffer buffer = new StringBuffer(64);
            for (int i = 0, j = rc.getRoles().size(); i < j; i++) {
                EmployeeRole next = rc.getRoles().get(i);
                buffer.append(next.getGroup().getName());
                if (i < (j - 1)) {
                    buffer.append(", ");
                }
                if (next.getGroup().getId().equals(defaultGroupId)) {
                    root.addContent(new Element("defaultGroup").setText(next.getGroup().getName()));
                }
            }
            root.addContent(new Element("groups").setText(buffer.toString()));

        }
        EmployeeDescription desc = getDescription();
        if (desc != null) {
            Element internetInfo = new Element("internetInfo");
            if (isInternetExportEnabled()) {
                internetInfo.addContent(new Element("display").setText("true"));
            } else {
                internetInfo.addContent(new Element("display").setText("false"));
            }
            if (desc.getName() != null) {
                internetInfo.addContent(new Element("job").setText(desc.getName()));
            }
            if (desc.getDescription() != null) {
                internetInfo.addContent(new Element("description").setText(desc.getDescription()));
            }
            root.addContent(internetInfo);
        }
        return root;
    }

    @Override
    public Long getGroupId() {
        Long id = super.getGroupId();
        return (id != null ? id : Contact.EMPLOYEE);
    }

    protected List<EmployeeDescription> getDescriptions() {
        return descriptions;
    }

    protected void setDescriptions(List<EmployeeDescription> descriptions) {
        this.descriptions = descriptions;
    }

    protected List<OfficePhone> getOfficePhones() {
        return officePhones;
    }

    protected void setOfficePhones(List<OfficePhone> officePhones) {
        this.officePhones = officePhones;
    }

    protected Long getDefaultGroupId() {
        return defaultGroupId;
    }

    protected void setDefaultGroupId(Long defaultGroupId) {
        this.defaultGroupId = defaultGroupId;
    }

    protected Long getDefaultBranchId() {
        return defaultBranchId;
    }

    protected void setDefaultBranchId(Long defaultBranchId) {
        this.defaultBranchId = defaultBranchId;
    }
}
