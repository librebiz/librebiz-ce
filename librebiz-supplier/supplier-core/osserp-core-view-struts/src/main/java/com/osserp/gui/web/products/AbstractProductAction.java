/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 25, 2007 11:48:57 AM 
 * 
 */
package com.osserp.gui.web.products;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;

import com.osserp.gui.client.products.ProductView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class AbstractProductAction extends AbstractLazyIgnoringAction {
    private static Logger log = LoggerFactory.getLogger(AbstractProductAction.class.getName());

    @Override
    protected Class<ProductView> getViewClass() {
        return ProductView.class;
    }

    protected ProductView createProductView(HttpServletRequest request) throws ClientException, PermissionException {
        return (ProductView) createView(request);
    }

    protected ProductView getProductView(HttpSession session) throws PermissionException {
        ProductView view = fetchProductView(session);
        if (view == null) {
            log.warn("getProductView() no view bound!");
            throw new ActionException();
        }
        return view;
    }

    protected ProductView fetchProductView(HttpSession session) throws PermissionException {
        ProductView view = (ProductView) fetchView(session);
        return view;
    }
}
