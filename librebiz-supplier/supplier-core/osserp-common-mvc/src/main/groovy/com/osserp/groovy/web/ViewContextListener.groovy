/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.groovy.web

import javax.servlet.FilterChain
import javax.servlet.ServletContext
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.servlet.http.HttpSession

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.web.context.WebApplicationContext

import com.osserp.common.TimeoutException
import com.osserp.common.web.RequestUtil

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public class ViewContextListener extends AbstractViewContextListener {
    private static Logger logger = LoggerFactory.getLogger(ViewContextListener.class.getName())
    
    @Override
    protected void filterHttpRequest(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain chain) throws IOException, ServletException {

        Map viewparams = [:]
        boolean redirectResult = false
        String remoteAddress = RequestUtil.getRemoteAddress(request)
        String uri = request.getRequestURI()
        def session = request.getSession(true)
        if (!session.getAttributeNames() && !(uri =~ /^\/[A-Za-z0-9]{1,}\//)) {
            logger.info("filterHttpRequest: missing attributes and uri pattern, redirecting to index.jsp [uri=$uri, host=${remoteAddress}]")
            redirect(request, response, '/index.jsp')
        }
        if (!uri || isPotentialViewRequest(uri)) {

            if (logger.isDebugEnabled() && isPotentialViewRequest(uri)) {
                logger.debug("filterHttpRequest: invoked [uri=$uri]")
            }

            viewparams = WebUtil.getViewParams(uri)
            View view = WebUtil.fetchView(request, viewparams.viewName)
            try {
                if (!view || viewparams.method == 'forward') {
                    view = createView(request, viewparams, 'spring')
                    if (view) {
                        checkUser(view)
                        initRequest(view, request)
                        view.generateNavigation()
                        session.setAttribute(viewparams.viewName + "View", view)
                    } else {
                        session.setAttribute('viewparams', viewparams)
                    }
                } else {
                    checkUser(view)
                    view.requestContext = viewparams
                    initRequest(view, request)
                }
                if (view) {
                    View lastRequestedView = session.getAttribute(View.CURRENT)
                    request.setAttribute(View.CURRENT, view)
                    if (!view.popupView) {
                        //no popups, use request attribute in other contexts
                        session.setAttribute(View.CURRENT, view)
                    }
                    Long scrollPosition = RequestUtil.fetchLong(request, 'scrollPosition')
                    if (scrollPosition) {
                        lastRequestedView?.setScrollPosition(scrollPosition)
                        logger.debug("filterHttpRequest: stored scrollPosition in last requested view [view=${lastRequestedView.name}, scrollPosition=${scrollPosition}]")
                    }
                }
                if (logger.isDebugEnabled()) {
                    logger.debug("filterHttpRequest: done [uri=$uri, params=$viewparams]")
                }
            } catch (TimeoutException t) {
                redirect(request, response, timeoutExceptionTarget)
                redirectResult = true
            } catch (ViewContextException e) {
                redirectResult = true
                if (view && authenticationRequired(view)) {
                    redirect(request, response, timeoutExceptionTarget)
                } else {
                    RequestUtil.saveError(request, e.getMessage())
                    if (view?.popupView) {
                        if (logger.isDebugEnabled()) {
                            logger.debug("filterHttpRequest: failed [reloadParent]")
                        }
                        PrintWriter out = response.getWriter()
                        out.println("gotoUrl(\"${request.contextPath}/currentPage\");")
                        out.flush()
                    } else if (view && view.env.lastPage) {
                        if (logger.isDebugEnabled()) {
                            logger.debug("filterHttpRequest: failed [redirect=${view.env.lastPage}]")
                        }
                        redirect(request, response, view.env.lastPage)
                    } else {
                        def servletContext = request.getSession().servletContext
                        def viewActionHome = servletContext.getAttribute('viewActionHome')
                        if (!viewActionHome) {
                            if (logger.isDebugEnabled()) {
                                logger.debug("filterHttpRequest: context param 'viewActionHome' not set, creating default home")
                            }
                            viewActionHome = "${WebUtil.getViewServletPath(servletContext)}/index"
                        }
                        if (logger.isDebugEnabled()) {
                            logger.debug("filterHttpRequest: failed [globalRedirect=${viewActionHome}]")
                        }
                        redirect(request, response, viewActionHome)
                    }
                }
            }
        } else if (logger.isDebugEnabled() && isPotentialViewRequest(uri)) {
            logger.debug("filterHttpRequest: not matching [uri=$uri, host=${remoteAddress}]")
        }
        if (!redirectResult) {
            chain.doFilter(request, response)
            if (logger.isDebugEnabled() && viewparams) {
                logger.debug("filterHttpRequest: filter.chain done [uri=$uri, params=$viewparams]")
            }
        }
    }
}
