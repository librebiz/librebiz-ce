/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 4, 2008 9:52:49 AM 
 * 
 */
package com.osserp.core.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.osserp.common.Option;
import com.osserp.common.PermissionConfig;
import com.osserp.common.beans.OptionImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PermissionConfigImpl extends OptionImpl implements PermissionConfig {

    private List<Option> permissions = new ArrayList<Option>();

    protected PermissionConfigImpl() {
        super();
    }

    public void addPermission(String permission) {
        for (int i = 0, j = permissions.size(); i < j; i++) {
            Option next = permissions.get(i);
            if (next.getName().equals(permission)) {
                // exit function
                return;
            }
        }
        this.permissions.add(new PermissionConfigItemImpl(this, permission));
    }

    public void removePermission(String permission) {
        for (Iterator<Option> i = permissions.iterator(); i.hasNext();) {
            Option next = i.next();
            if (next.getName().equals(permission)) {
                i.remove();
            }
        }
    }

    public List<Option> getPermissions() {
        return permissions;
    }

    protected void setPermissions(List<Option> permissions) {
        this.permissions = permissions;
    }

    public String[] getPermissionValues() {
        String[] result = new String[this.permissions.size()];
        for (int i = 0, j = permissions.size(); i < j; i++) {
            Option next = permissions.get(i);
            result[i] = next.getName();
        }
        return result;
    }

}
