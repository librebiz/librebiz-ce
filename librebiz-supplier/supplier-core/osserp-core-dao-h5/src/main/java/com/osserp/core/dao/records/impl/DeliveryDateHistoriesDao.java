/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.sql.Types;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.dao.Tables;
import com.osserp.common.util.CollectionUtil;

import com.osserp.core.dao.impl.AbstractDao;
import com.osserp.core.dao.records.DeliveryDateHistories;
import com.osserp.core.finance.DeliveryDateChangedInfo;
import com.osserp.core.model.records.PurchaseOrderDeliveryDateChangedInfoImpl;
import com.osserp.core.model.records.SalesOrderDeliveryDateChangedInfoImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class DeliveryDateHistoriesDao extends AbstractDao implements DeliveryDateHistories {

    private String salesOrders;

    /**
     * Creates a new delivery date history dao
     * @param jdbcTemplate
     * @param tables
     * @param sessionFactory
     * @param salesOrdersTable
     */
    public DeliveryDateHistoriesDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            String salesOrdersTable) {
        super(jdbcTemplate, tables, sessionFactory);
        this.salesOrders = getTable(salesOrdersTable);
    }

    public List<DeliveryDateChangedInfo> findByProduct(Long productId) {
        List<DeliveryDateChangedInfo> result = findPurchaseOrderByProduct(productId);
        result.addAll(findSalesOrderByProduct(productId));
        CollectionUtil.sort(result, createCreatedDateComparator());
        return result;
    }

    public List<DeliveryDateChangedInfo> findByRecord(Long id) {
        if (isSalesOrder(id)) {
            return findBySalesOrder(id);
        }
        return findByPurchaseOrder(id);
    }

    private List<DeliveryDateChangedInfo> findByPurchaseOrder(Long id) {
        return findByOrder(PurchaseOrderDeliveryDateChangedInfoImpl.class.getName(), id);
    }

    private List<DeliveryDateChangedInfo> findBySalesOrder(Long id) {
        return findByOrder(SalesOrderDeliveryDateChangedInfoImpl.class.getName(), id);
    }

    private List<DeliveryDateChangedInfo> findPurchaseOrderByProduct(Long productId) {
        return findByProduct(PurchaseOrderDeliveryDateChangedInfoImpl.class.getName(), productId);
    }

    private List<DeliveryDateChangedInfo> findSalesOrderByProduct(Long productId) {
        return findByProduct(SalesOrderDeliveryDateChangedInfoImpl.class.getName(), productId);
    }

    private List<DeliveryDateChangedInfo> findByOrder(String className, Long id) {
        StringBuilder hql = new StringBuilder("from ");
        hql
            .append(className)
            .append(" o where o.reference = :referenceId order by o.created desc");
        return getCurrentSession().createQuery(hql.toString()).setParameter("referenceId", id).list();
    }

    private List<DeliveryDateChangedInfo> findByProduct(String className, Long productId) {
        StringBuilder hql = new StringBuilder("from ");
        hql
            .append(className)
            .append(" o where o.product.productId = :productid order by o.created desc");
        return getCurrentSession().createQuery(hql.toString()).setParameter("productid", productId).list();
    }

    private boolean isSalesOrder(Long id) {
        StringBuilder sql = new StringBuilder("SELECT count(*) FROM ");
        sql
                .append(salesOrders)
                .append(" WHERE id = ?");
        return (jdbcTemplate.queryForObject(
                sql.toString(), 
                new Object[] { id }, 
                new int[] { Types.BIGINT }, 
                Integer.class) > 0);
    }

    private Comparator createCreatedDateComparator() {
        return new Comparator() {
            public int compare(Object a, Object b) {
                DeliveryDateChangedInfo orderA = (DeliveryDateChangedInfo) a;
                DeliveryDateChangedInfo orderB = (DeliveryDateChangedInfo) b;
                Date dateA = orderA.getCreated();
                Date dateB = orderB.getCreated();
                if (dateA == null && dateB == null) {
                    return 0;
                }
                if (dateA == null) {
                    return -1;
                }
                if (dateB == null) {
                    return 1;
                }
                return dateB.compareTo(dateA);
            }
        };
    }
}
