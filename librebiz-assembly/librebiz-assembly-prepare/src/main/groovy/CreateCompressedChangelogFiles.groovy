/**
 *
 * Copyright (C) 2017 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

import java.util.zip.Deflater
import java.util.zip.GZIPOutputStream

import org.librebiz.PackagingHelper

/**
 * This class provides a workaround for creating gzipped changelog files
 * without using any other than maven and jdeb to build the software.
 *
 * It uses 'gzip -9' when running on Debian or other OS having gzip installed
 * and falls back to GZIPOutputStream if gzip is missing.
 *
 * Note: The resulting package when using fallback does not prevent lintian
 * messages since the generated zip does not provide the expected compression
 * level.
 *
 */
class CreateCompressedChangelogFiles extends PackagingHelper {

    def run() {
        if (exec('which gzip > /dev/null') == 0) {
            myProjects.each { String projectName ->
                if (exec("cp -f ${project.basedir}/../../${projectName}/CHANGES.txt ${project.basedir}/../../${projectName}/changelog > /dev/null".toString()) == 0) {
                    if (exec("gzip -n9 ${project.basedir}/../../${projectName}/changelog > /dev/null".toString()) != 0) {
                        return 1
                    }
                } else {
                    createJavaZip("${projectName}".toString())
                }
            }
        } else {
            myProjects.each { String projectName ->
                createJavaZip("${projectName}".toString())
            }
        }
        return 0
    }

    int build(String sourceFile, String targetFile) {
        assert sourceFile && targetFile
        File src = new File(sourceFile)
        if (src.exists()) {
            FileInputStream fis = new FileInputStream(src)
            FileOutputStream fos = new FileOutputStream(targetFile)
            GZIPOutputStream gz = new GZIPOutputStream(fos)
            int readBytes
            while ((readBytes = fis.read()) != -1) {
                gz.write(readBytes)
            }
            gz.close()
            fis.close()
        } else {
            // TODO output message via logger
            println ("FILE DOES NOT EXIST: ${sourceFile}")
        }
        return 0
    }

    private void createJavaZip(String nameOfProject) {
        build("${project.basedir}/../../${nameOfProject}/CHANGES.txt".toString(),
            "${project.basedir}/../../${nameOfProject}/changelog.gz".toString())
    }
}
