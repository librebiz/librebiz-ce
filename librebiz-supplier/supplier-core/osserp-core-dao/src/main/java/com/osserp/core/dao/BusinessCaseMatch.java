package com.osserp.core.dao;

public class BusinessCaseMatch {
    private boolean sales;
    private Long id;
    private Long customerId;
    private String name;
    public BusinessCaseMatch(boolean sales, Long id, Long customerId, String name) {
        this.sales = sales;
        this.id = id;
        this.customerId = customerId;
        this.name = name;
    }
    public boolean isSales() {
        return sales;
    }
    public Long getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    protected void setSales(boolean sales) {
        this.sales = sales;
    }
    protected void setId(Long id) {
        this.id = id;
    }
    protected void setName(String name) {
        this.name = name;
    }
    public Long getCustomerId() {
        return customerId;
    }
    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }
}
