<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<c:set var="view" value="${sessionScope.telephoneConfigurationPopupView}" />
<c:set var="config" value="${sessionScope.telephoneConfigurationPopupView.bean}" />
<c:set var="url" value="/telephones/telephoneConfigurationPopup" />

<c:if test="${!empty sessionScope.error}">
	&nbsp;
	<div class="errormessage">
        <fmt:message key="error" />
        :
        <fmt:message key="${sessionScope.error}" />
    </div>
    <o:removeErrors />
</c:if>
<c:choose>
    <c:when test="${!empty view.lastChangeSetName}">
        <table class="table table-striped" style="width: 280px;">
            <thead>
                <tr>
                    <th colspan="2"><fmt:message key="lastChange" /></th>
                </tr>
            </thead>
            <tbody>
                <c:choose>
                    <c:when test="${empty view.lastChangeSet}">
                        <tr>
                            <td colspan="2"><fmt:message key="noChangeFound" /></td>
                        </tr>
                    </c:when>
                    <c:otherwise>
                        <tr>
                            <td><fmt:message key="changed" /></td>
                            <td><o:date value="${view.lastChangeSet.created}" /></td>
                        </tr>
                        <tr>
                            <td><fmt:message key="changedBy" /></td>
                            <td><oc:employee value="${view.lastChangeSet.createdBy}" /></td>
                        </tr>
                        <tr>
                            <td><fmt:message key="oldValue" /></td>
                            <td><c:choose>
                                    <c:when test="${empty view.lastChangeSet.oldValue}">(<fmt:message key="notSet" />)</c:when>
                                    <c:otherwise>
                                        <o:out value="${view.lastChangeSet.oldValue}" />
                                    </c:otherwise>
                                </c:choose></td>
                        </tr>
                        <tr>
                            <td><fmt:message key="newValue" /></td>
                            <td><c:choose>
                                    <c:when test="${empty view.lastChangeSet.newValue}">(<fmt:message key="notSet" />)</c:when>
                                    <c:otherwise>
                                        <o:out value="${view.lastChangeSet.newValue}" />
                                    </c:otherwise>
                                </c:choose></td>
                        </tr>
                    </c:otherwise>
                </c:choose>
            </tbody>
        </table>
    </c:when>
    <c:otherwise>
        <o:logger write="view not empty" level="debug" />
        <c:choose>
            <c:when test="${view.createMode}">
                <div class="modalBoxHeader">
                    <div class="modalBoxHeaderLeft">
                        <fmt:message key="createTelephoneConfiguration" />
                    </div>
                    <div class="modalBoxHeaderRight">
                        <div class="boxnav">
                            <ul>
                                <c:forEach var="nav" items="${view.navigation}">
                                    <li><v:ajaxLink url="${nav.link}" targetElement="${view.name}_popup" title="${nav.title}">
                                            <o:img name="${nav.icon}" />
                                        </v:ajaxLink></li>
                                </c:forEach>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="modalBoxData">
                    <div class="subcolumns">
                        <div class="subcolumn">
                            <o:logger write="view in createMode" level="debug" />
                            <v:ajaxForm name="dynamicForm" url="${url}/create" activityElement="${view.name}" targetElement="${view.name}" preRequest="if (ojsForms.validateForm(this)) {" postRequest="}">
                                <div class="spacer"></div>
                                <table class="valueTable input" style="width: 500px; table-layout: fixed;">
                                    <thead>
                                        <tr>
                                            <th colspan="2"><fmt:message key="createTelephoneConfiguration" /></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="width: 250px;"><fmt:message key="costCenter" /></td>
                                            <td><input class="mandatory" type="text" name="costCenter" id="costCenter" value="" /></td>
                                        </tr>
                                        <tr>
                                            <td style="width: 250px;"><fmt:message key="nameOnDisplay" /></td>
                                            <td><input class="mandatory" type="text" name="name" id="name" value="" /></td>
                                        </tr>
                                        <tr>
                                            <td style="width: 250px;"><fmt:message key="email" /></td>
                                            <td><input type="text" name="email" id="email" value="" /></td>
                                        </tr>
                                        <tr>
                                            <td class="row-submit"><v:ajaxLink url="${url}/exit" targetElement="${view.name}_popup">
                                                    <input type="button" class="cancel" value="<fmt:message key="exit"/>" />
                                                </v:ajaxLink></td>
                                            <td class="row-submit"><o:submit /></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </v:ajaxForm>
                            <div class="spacer"></div>
                        </div>
                    </div>
                </div>
            </c:when>
            <c:otherwise>
                <c:if test="${!empty config}">
                    <o:logger write="phone configuration not empty" level="debug" />
                    <c:choose>
                        <c:when test="${view.editMode}">
                            <o:logger write="view in editMode" level="debug" />
                            <c:choose>
                                <c:when test="${view.edit == 'internal' || view.edit == 'telephone'}">
                                    <c:set var="activityElement">
                                        <o:out value="${view.name}_popup" />
                                    </c:set>
                                    <c:set var="targetElement">
                                        <o:out value="${view.name}_popup" />
                                    </c:set>
                                </c:when>
                                <c:otherwise>
                                    <c:set var="activityElement">
                                        <o:out value="${view.name}_popup_${config.id}_${view.edit}" />
                                    </c:set>
                                    <c:set var="targetElement">
                                        <o:out value="${view.name}_popup_${config.id}_${view.edit}" />
                                    </c:set>
                                </c:otherwise>
                            </c:choose>
                            <v:ajaxForm name="dynamicForm" url="${url}/save" activityElement="${activityElement}" targetElement="${targetElement}" preRequest="if (ojsForms.validateForm(this)) {" postRequest="}">
                                <c:choose>
                                    <c:when test="${view.edit == 'account_code'}">
                                        <input class="mandatory" type="text" name="value" id="value" value="${config.accountCode}" />
                                    </c:when>
                                    <c:when test="${view.edit == 'display_name'}">
                                        <input class="mandatory" type="text" name="value" id="value" value="${config.displayName}" />
                                    </c:when>
                                    <c:when test="${view.edit == 'telephone_exchange'}">
                                        <input type="checkbox" name="value" id="value" <c:if test="${config.telephoneExchange}">checked="checked"</c:if> />
                                    </c:when>
                                    <c:when test="${view.edit == 'email_address'}">
                                        <input class="mandatory" type="text" name="value" id="value" value="${config.emailAddress}" />
                                    </c:when>
                                    <c:when test="${view.edit == 'telephone'}">
                                        <o:ajaxSelect selectId="${view.name}_${config.id}_telephone_id" activityElement="${view.name}_popup_${config.id}_${view.edit}" targetElement="${view.name}_popup_${config.id}_${view.edit}">
                                            <option value="<v:url value="${url}/select" />?id=&name=${view.edit}" <c:if test="${empty view.telephoneSystemId}">selected="selected"</c:if>><fmt:message key="notSet" /></option>
                                            <c:forEach var="obj" items="${view.telephoneSystems}">
                                                <option value="<v:url value="${url}/select" />?id=${obj.id}&name=${view.edit}" <c:if test="${obj.id == view.telephoneSystemId}">selected="selected"</c:if>><o:out value="${obj.name}" /></option>
                                            </c:forEach>
                                        </o:ajaxSelect> &nbsp;
									<select id="value" name="value" <c:if test="${empty view.telephoneSystemId}">disabled="disabled"</c:if>>
                                            <c:if test="${empty view.telephoneId}">
                                                <option value="" selected="selected"><fmt:message key="selection" /></option>
                                            </c:if>
                                            <c:forEach var="obj" items="${view.telephones}">
                                                <option value="${obj.id}" <c:if test="${obj.id == view.telephoneId}">selected="selected"</c:if>><o:out value="${obj.mac}" /></option>
                                            </c:forEach>
                                        </select>
                                    </c:when>
                                    <c:when test="${view.edit == 'internal'}">
                                        <c:choose>
                                            <c:when test="${config.internalEditable}">
                                                <select id="value" name="value">
                                                    <option value="" <c:if test="${empty config.internal}">selected="selected"</c:if>><fmt:message key="notSet" /></option>
                                                    <c:forEach var="obj" items="${view.internals}">
                                                        <option value="${obj.internal}" <c:if test="${obj.internal == config.internal}">selected="selected"</c:if>><o:out value="${obj.internal}" /></option>
                                                    </c:forEach>
                                                </select>
                                            </c:when>
                                            <c:otherwise>
                                                <o:out value="${config.internal}" />
                                            </c:otherwise>
                                        </c:choose>
                                    </c:when>
                                    <c:when test="${view.edit == 'telephone_group'}">
                                        <select id="value" name="value">
                                            <option value="" <c:if test="${empty config.telephoneGroup}">selected="selected"</c:if>><fmt:message key="notSet" /></option>
                                            <c:forEach var="obj" items="${view.telephoneGroups}">
                                                <option value="${obj.id}" <c:if test="${obj.id == config.telephoneGroup.id}">selected="selected"</c:if>><o:out value="${obj.name}" /></option>
                                            </c:forEach>
                                        </select>
                                    </c:when>
                                    <c:when test="${view.edit == 'voice_mail_send_as_email'}">
                                        <input type="checkbox" name="value" id="value" <c:if test="${config.voiceMailSendAsEmail}">checked="checked"</c:if> />
                                    </c:when>
                                    <c:when test="${view.edit == 'voice_mail_delete_after_email_send'}">
                                        <input type="checkbox" name="value" id="value" <c:if test="${config.voiceMailDeleteAfterEmailSend}">checked="checked"</c:if> />
                                    </c:when>
                                    <c:when test="${view.edit == 'voice_mail_enabled'}">
                                        <input type="checkbox" name="value" id="value" <c:if test="${config.voiceMailEnabled}">checked="checked"</c:if> />
                                    </c:when>
                                    <c:when test="${view.edit == 'voice_mail_delay_seconds'}">
                                        <input class="mandatory integer" type="text" name="value" id="value" value="${config.voiceMailDelaySeconds}" />
                                    </c:when>
                                    <c:when test="${view.edit == 'voice_mail_enabled_if_busy'}">
                                        <input type="checkbox" name="value" id="value" <c:if test="${config.voiceMailEnabledIfBusy}">checked="checked"</c:if> />
                                    </c:when>
                                    <c:when test="${view.edit == 'voice_mail_enabled_if_unavailable'}">
                                        <input type="checkbox" name="value" id="value" <c:if test="${config.voiceMailEnabledIfUnavailable}">checked="checked"</c:if> />
                                    </c:when>
                                    <c:when test="${view.edit == 'parallel_call_target_phone_number'}">
                                        <input class="mandatory integer" type="text" name="value" id="value" value="${config.parallelCallTargetPhoneNumber}" />
                                    </c:when>
                                    <c:when test="${view.edit == 'parallel_call_enabled'}">
                                        <input type="checkbox" name="value" id="value" <c:if test="${config.parallelCallEnabled}">checked="checked"</c:if> />
                                    </c:when>
                                    <c:when test="${view.edit == 'parallel_call_set_delay_seconds'}">
                                        <input class="mandatory integer" type="text" name="value" id="value" value="${config.parallelCallSetDelaySeconds}" />
                                    </c:when>
                                    <c:when test="${view.edit == 'call_forward_target_phone_number'}">
                                        <input class="mandatory integer" type="text" name="value" id="value" value="${config.callForwardTargetPhoneNumber}" />
                                    </c:when>
                                    <c:when test="${view.edit == 'call_forward_delay_seconds'}">
                                        <input class="mandatory integer" type="text" name="value" id="value" value="${config.callForwardDelaySeconds}" />
                                    </c:when>
                                    <c:when test="${view.edit == 'call_forward_enabled'}">
                                        <input type="checkbox" name="value" id="value" <c:if test="${config.callForwardEnabled}">checked="checked"</c:if> />
                                    </c:when>
                                    <c:when test="${view.edit == 'call_forward_if_busy_target_phone_number'}">
                                        <input class="mandatory integer" type="text" name="value" id="value" value="${config.callForwardIfBusyTargetPhoneNumber}" />
                                    </c:when>
                                    <c:when test="${view.edit == 'call_forward_if_busy_enabled'}">
                                        <input type="checkbox" name="value" id="value" <c:if test="${config.callForwardIfBusyEnabled}">checked="checked"</c:if> />
                                    </c:when>
                                    <c:when test="${view.edit == 'busy_on_busy'}">
                                        <input type="checkbox" name="value" id="value" <c:if test="${config.busyOnBusy}">checked="checked"</c:if> />
                                    </c:when>
                                    <c:when test="${view.edit == 'available'}">
                                        <input type="checkbox" name="value" id="value" <c:if test="${config.available}">checked="checked"</c:if> />
                                    </c:when>
                                </c:choose>
                                <v:ajaxLink url="${url}/disableEditMode?name=${view.edit}" activityElement="${view.name}_popup_${config.id}_${view.edit}" targetElement="${view.name}_popup_${config.id}_${view.edit}" styleClass="submitExit">
                                    <o:img name="backIcon" styleClass="bigicon" />
                                </v:ajaxLink>
                                <input type="hidden" name="name" id="name" value="${view.edit}" />
                                <input type="image" name="submit" src="<c:url value="${applicationScope.saveIcon}"/>" class="bigicon" title="<fmt:message key="save"/>" />
                            </v:ajaxForm>
                        </c:when>
                        <c:otherwise>
                            <o:logger write="view not in editMode" level="debug" />
                            <c:choose>
                                <c:when test="${!empty view.edit}">
                                    <c:choose>
                                        <c:when test="${view.edit == 'reboot_phone'}">
                                            <v:ajaxLink url="${url}/sendPhoneAction?name=reboot_phone" targetElement="${view.name}_popup_${config.id}_reboot_phone" activityElement="${view.name}_popup_${config.id}_reboot_phone" title="taskTelephoneReboot">
                                                <fmt:message key="taskTelephoneReboot" />
                                            </v:ajaxLink>
                                        </c:when>
                                        <c:when test="${view.edit == 'send_check_config_to_phone'}">
                                            <v:ajaxLink url="${url}/sendPhoneAction?name=send_check_config_to_phone" targetElement="${view.name}_popup_${config.id}_send_check_config_to_phone" activityElement="${view.name}_popup_${config.id}_send_check_config_to_phone" title="taskReloadTelephoneConfiguration">
                                                <fmt:message key="taskReloadTelephoneConfiguration" />
                                            </v:ajaxLink>
                                        </c:when>
                                        <c:otherwise>
                                            <v:ajaxLink url="${url}/enableEditMode?name=${view.edit}" title="edit" activityElement="${view.name}_popup_${config.id}_${view.edit}" targetElement="${view.name}_popup_${config.id}_${view.edit}">
                                                <c:choose>
                                                    <c:when test="${view.edit == 'account_code'}">
                                                        <c:choose>
                                                            <c:when test="${empty config.accountCode}">(<fmt:message key="notSet" />)</c:when>
                                                            <c:otherwise>
                                                                <o:out value="${config.accountCode}" />
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:when>
                                                    <c:when test="${view.edit == 'display_name'}">
                                                        <c:choose>
                                                            <c:when test="${empty config.displayName}">(<fmt:message key="notSet" />)</c:when>
                                                            <c:otherwise>
                                                                <o:out value="${config.displayName}" />
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:when>
                                                    <c:when test="${view.edit == 'telephone_exchange'}">
                                                        <c:choose>
                                                            <c:when test="${config.telephoneExchange}">
                                                                <fmt:message key="common.yes" />
                                                            </c:when>
                                                            <c:otherwise>
                                                                <fmt:message key="no" />
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:when>
                                                    <c:when test="${view.edit == 'email_address'}">
                                                        <c:choose>
                                                            <c:when test="${empty config.emailAddress}">(<fmt:message key="notSet" />)</c:when>
                                                            <c:otherwise>
                                                                <o:out value="${config.emailAddress}" />
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:when>
                                                    <c:when test="${view.edit == 'telephone'}">
                                                        <c:choose>
                                                            <c:when test="${empty config.telephone}">(<fmt:message key="notSet" />)</c:when>
                                                            <c:otherwise>
                                                                <o:out value="${config.telephone.mac}" />
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:when>
                                                    <c:when test="${view.edit == 'internal'}">
                                                        <c:choose>
                                                            <c:when test="${empty config.internal}">(<fmt:message key="notSet" />)</c:when>
                                                            <c:otherwise>
                                                                <o:out value="${config.internal}" />
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:when>
                                                    <c:when test="${view.edit == 'telephone_group'}">
                                                        <c:choose>
                                                            <c:when test="${empty config.telephoneGroup}">(<fmt:message key="notSet" />)</c:when>
                                                            <c:otherwise>
                                                                <o:out value="${config.telephoneGroup.name}" />
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:when>
                                                    <c:when test="${view.edit == 'voice_mail_send_as_email'}">
                                                        <c:choose>
                                                            <c:when test="${config.voiceMailSendAsEmail}">
                                                                <fmt:message key="activated" />
                                                            </c:when>
                                                            <c:otherwise>
                                                                <fmt:message key="deactivated" />
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:when>
                                                    <c:when test="${view.edit == 'voice_mail_delete_after_email_send'}">
                                                        <c:choose>
                                                            <c:when test="${config.voiceMailDeleteAfterEmailSend}">
                                                                <fmt:message key="activated" />
                                                            </c:when>
                                                            <c:otherwise>
                                                                <fmt:message key="deactivated" />
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:when>
                                                    <c:when test="${view.edit == 'voice_mail_enabled'}">
                                                        <c:choose>
                                                            <c:when test="${config.voiceMailEnabled}">
                                                                <fmt:message key="activated" />
                                                            </c:when>
                                                            <c:otherwise>
                                                                <fmt:message key="deactivated" />
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:when>
                                                    <c:when test="${view.edit == 'voice_mail_delay_seconds'}">
                                                        <c:choose>
                                                            <c:when test="${empty config.voiceMailDelaySeconds}">(<fmt:message key="notSet" />)</c:when>
                                                            <c:otherwise>
                                                                <o:out value="${config.voiceMailDelaySeconds}" />
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:when>
                                                    <c:when test="${view.edit == 'voice_mail_enabled_if_busy'}">
                                                        <c:choose>
                                                            <c:when test="${config.voiceMailEnabledIfBusy}">
                                                                <fmt:message key="activated" />
                                                            </c:when>
                                                            <c:otherwise>
                                                                <fmt:message key="deactivated" />
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:when>
                                                    <c:when test="${view.edit == 'voice_mail_enabled_if_unavailable'}">
                                                        <c:choose>
                                                            <c:when test="${config.voiceMailEnabledIfUnavailable}">
                                                                <fmt:message key="activated" />
                                                            </c:when>
                                                            <c:otherwise>
                                                                <fmt:message key="deactivated" />
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:when>
                                                    <c:when test="${view.edit == 'parallel_call_target_phone_number'}">
                                                        <c:choose>
                                                            <c:when test="${empty config.parallelCallTargetPhoneNumber}">(<fmt:message key="notSet" />)</c:when>
                                                            <c:otherwise>
                                                                <o:out value="${config.parallelCallTargetPhoneNumber}" />
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:when>
                                                    <c:when test="${view.edit == 'parallel_call_enabled'}">
                                                        <c:choose>
                                                            <c:when test="${config.parallelCallEnabled}">
                                                                <fmt:message key="activated" />
                                                            </c:when>
                                                            <c:otherwise>
                                                                <fmt:message key="deactivated" />
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:when>
                                                    <c:when test="${view.edit == 'parallel_call_set_delay_seconds'}">
                                                        <c:choose>
                                                            <c:when test="${empty config.parallelCallSetDelaySeconds}">(<fmt:message key="notSet" />)</c:when>
                                                            <c:otherwise>
                                                                <o:out value="${config.parallelCallSetDelaySeconds}" />
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:when>
                                                    <c:when test="${view.edit == 'call_forward_target_phone_number'}">
                                                        <c:choose>
                                                            <c:when test="${empty config.callForwardTargetPhoneNumber}">(<fmt:message key="notSet" />)</c:when>
                                                            <c:otherwise>
                                                                <o:out value="${config.callForwardTargetPhoneNumber}" />
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:when>
                                                    <c:when test="${view.edit == 'call_forward_delay_seconds'}">
                                                        <c:choose>
                                                            <c:when test="${empty config.callForwardDelaySeconds}">(<fmt:message key="notSet" />)</c:when>
                                                            <c:otherwise>
                                                                <o:out value="${config.callForwardDelaySeconds}" />
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:when>
                                                    <c:when test="${view.edit == 'call_forward_enabled'}">
                                                        <c:choose>
                                                            <c:when test="${config.callForwardEnabled}">
                                                                <fmt:message key="activated" />
                                                            </c:when>
                                                            <c:otherwise>
                                                                <fmt:message key="deactivated" />
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:when>
                                                    <c:when test="${view.edit == 'call_forward_if_busy_target_phone_number'}">
                                                        <c:choose>
                                                            <c:when test="${empty config.callForwardIfBusyTargetPhoneNumber}">(<fmt:message key="notSet" />)</c:when>
                                                            <c:otherwise>
                                                                <o:out value="${config.callForwardIfBusyTargetPhoneNumber}" />
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:when>
                                                    <c:when test="${view.edit == 'call_forward_if_busy_enabled'}">
                                                        <c:choose>
                                                            <c:when test="${config.callForwardIfBusyEnabled}">
                                                                <fmt:message key="activated" />
                                                            </c:when>
                                                            <c:otherwise>
                                                                <fmt:message key="deactivated" />
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:when>
                                                    <c:when test="${view.edit == 'busy_on_busy'}">
                                                        <c:choose>
                                                            <c:when test="${config.busyOnBusy}">
                                                                <fmt:message key="activated" />
                                                            </c:when>
                                                            <c:otherwise>
                                                                <fmt:message key="deactivated" />
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:when>
                                                    <c:when test="${view.edit == 'available'}">
                                                        <c:choose>
                                                            <c:when test="${config.available}">
                                                                <fmt:message key="common.yes" />
                                                            </c:when>
                                                            <c:otherwise>
                                                                <fmt:message key="no" />
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:when>
                                                </c:choose>
                                            </v:ajaxLink>
                                        </c:otherwise>
                                    </c:choose>
                                </c:when>
                                <c:otherwise>
                                    <div class="modalBoxHeader">
                                        <div class="modalBoxHeaderLeft">
                                            <o:out value="${config.uid}" />
                                        </div>
                                        <div class="modalBoxHeaderRight">
                                            <div class="boxnav">
                                                <ul>
                                                    <c:forEach var="nav" items="${view.navigation}">
                                                        <li><v:ajaxLink url="${nav.link}" targetElement="${view.name}_popup" title="${nav.title}">
                                                                <o:img name="${nav.icon}" />
                                                            </v:ajaxLink></li>
                                                    </c:forEach>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modalBoxData">
                                        <div class="subcolumns">
                                            <div class="subcolumn">
                                                <div class="spacer"></div>
                                                <table class="table table-striped" style="margin-right: 10px; width: 500px; table-layout: fixed;">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="2"><o:out value="${config.uid}" /></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td style="width: 250px;"><fmt:message key="costCenter" /> <v:ajaxInfobox id="account_code" url="${url}/getLastChangeset?infos=account_code" iconUrl="${applicationScope.importantIcon}" /></td>
                                                            <td id="${view.name}_popup_${config.id}_account_code"><o:forbidden role="telephone_exchange_edit,telephone_system_admin">
                                                                    <c:choose>
                                                                        <c:when test="${empty config.accountCode}">(<fmt:message key="notSet" />)</c:when>
                                                                        <c:otherwise>
                                                                            <o:out value="${config.accountCode}" />
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </o:forbidden> <o:permission role="telephone_exchange_edit,telephone_system_admin" info="permissionTelephoneExchangeEdit">
                                                                    <v:ajaxLink url="${url}/enableEditMode?name=account_code" targetElement="${view.name}_popup_${config.id}_account_code" activityElement="${view.name}_popup_${config.id}_account_code" title="edit">
                                                                        <c:choose>
                                                                            <c:when test="${empty config.accountCode}">(<fmt:message key="notSet" />)</c:when>
                                                                            <c:otherwise>
                                                                                <o:out value="${config.accountCode}" />
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </v:ajaxLink>
                                                                </o:permission></td>
                                                        </tr>
                                                        <tr>
                                                            <td><fmt:message key="nameOnDisplay" /> <v:ajaxInfobox id="display_name" url="${url}/getLastChangeset?infos=display_name" iconUrl="${applicationScope.importantIcon}" /></td>
                                                            <td id="${view.name}_popup_${config.id}_display_name"><o:forbidden role="telephone_exchange_edit,telephone_system_admin">
                                                                    <c:choose>
                                                                        <c:when test="${empty config.displayName}">(<fmt:message key="notSet" />)</c:when>
                                                                        <c:otherwise>
                                                                            <o:out value="${config.displayName}" />
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </o:forbidden> <o:permission role="telephone_exchange_edit,telephone_system_admin" info="permissionTelephoneExchangeEdit">
                                                                    <v:ajaxLink url="${url}/enableEditMode?name=display_name" targetElement="${view.name}_popup_${config.id}_display_name" activityElement="${view.name}_popup_${config.id}_display_name" title="edit">
                                                                        <c:choose>
                                                                            <c:when test="${empty config.displayName}">(<fmt:message key="notSet" />)</c:when>
                                                                            <c:otherwise>
                                                                                <o:out value="${config.displayName}" />
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </v:ajaxLink>
                                                                </o:permission></td>
                                                        </tr>
                                                        <o:permission role="telephone_system_admin" info="permissionTelephoneExchangeEdit">
                                                            <tr>
                                                                <td><fmt:message key="telephoneExchange" /> <v:ajaxInfobox id="telephone_exchange" url="${url}/getLastChangeset?infos=telephone_exchange" iconUrl="${applicationScope.importantIcon}" /></td>
                                                                <td id="${view.name}_popup_${config.id}_telephone_exchange"><o:forbidden role="telephone_system_admin">
                                                                        <c:choose>
                                                                            <c:when test="${config.telephoneExchange}">
                                                                                <fmt:message key="common.yes" />
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                <fmt:message key="no" />
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </o:forbidden> <o:permission role="telephone_system_admin" info="permissionTelephoneSystemAdmin">
                                                                        <v:ajaxLink url="${url}/enableEditMode?name=telephone_exchange" targetElement="${view.name}_popup_${config.id}_telephone_exchange" activityElement="${view.name}_popup_${config.id}_telephone_exchange" title="edit">
                                                                            <c:choose>
                                                                                <c:when test="${config.telephoneExchange}">
                                                                                    <fmt:message key="common.yes" />
                                                                                </c:when>
                                                                                <c:otherwise>
                                                                                    <fmt:message key="no" />
                                                                                </c:otherwise>
                                                                            </c:choose>
                                                                        </v:ajaxLink>
                                                                    </o:permission></td>
                                                            </tr>
                                                        </o:permission>
                                                        <tr>
                                                            <td><fmt:message key="email" /> <v:ajaxInfobox id="email_address" url="${url}/getLastChangeset?infos=email_address" iconUrl="${applicationScope.importantIcon}" /></td>
                                                            <td id="${view.name}_popup_${config.id}_email_address"><o:forbidden role="telephone_exchange_edit,telephone_system_admin">
                                                                    <c:choose>
                                                                        <c:when test="${empty config.emailAddress}">(<fmt:message key="notSet" />)</c:when>
                                                                        <c:otherwise>
                                                                            <o:out value="${config.emailAddress}" />
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </o:forbidden> <o:permission role="telephone_exchange_edit,telephone_system_admin" info="permissionTelephoneExchangeEdit">
                                                                    <v:ajaxLink url="${url}/enableEditMode?name=email_address" targetElement="${view.name}_popup_${config.id}_email_address" activityElement="${view.name}_popup_${config.id}_email_address" title="edit">
                                                                        <c:choose>
                                                                            <c:when test="${empty config.emailAddress}">(<fmt:message key="notSet" />)</c:when>
                                                                            <c:otherwise>
                                                                                <o:out value="${config.emailAddress}" />
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </v:ajaxLink>
                                                                </o:permission></td>
                                                        </tr>
                                                        <tr>
                                                            <td><fmt:message key="phone" /> <v:ajaxInfobox id="telephone" url="${url}/getLastChangeset?infos=telephone" iconUrl="${applicationScope.importantIcon}" /></td>
                                                            <td id="${view.name}_popup_${config.id}_telephone"><o:forbidden role="telephone_system_admin">
                                                                    <c:choose>
                                                                        <c:when test="${empty config.telephone}">(<fmt:message key="notSet" />)</c:when>
                                                                        <c:otherwise>
                                                                            <o:out value="${config.telephone.mac}" />
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </o:forbidden> <o:permission role="telephone_system_admin" info="permissionTelephoneSystemAdmin">
                                                                    <v:ajaxLink url="${url}/enableEditMode?name=telephone" targetElement="${view.name}_popup_${config.id}_telephone" activityElement="${view.name}_popup_${config.id}_telephone" title="edit">
                                                                        <c:choose>
                                                                            <c:when test="${empty config.telephone}">(<fmt:message key="notSet" />)</c:when>
                                                                            <c:otherwise>
                                                                                <o:out value="${config.telephone.mac}" />
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </v:ajaxLink>
                                                                </o:permission></td>
                                                        </tr>
                                                        <tr>
                                                            <td><fmt:message key="telephoneGroup" /> <v:ajaxInfobox id="telephoneGroup" url="${url}/getLastChangeset?infos=telephone_group" iconUrl="${applicationScope.importantIcon}" /></td>
                                                            <td id="${view.name}_popup_${config.id}_telephone_group"><o:forbidden role="telephone_system_admin">
                                                                    <c:choose>
                                                                        <c:when test="${empty config.telephoneGroup}">(<fmt:message key="notSet" />)</c:when>
                                                                        <c:otherwise>
                                                                            <o:out value="${config.telephoneGroup.name}" />
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </o:forbidden> <o:permission role="telephone_system_admin" info="permissionTelephoneSystemAdmin">
                                                                    <v:ajaxLink url="${url}/enableEditMode?name=telephone_group" targetElement="${view.name}_popup_${config.id}_telephone_group" activityElement="${view.name}_popup_${config.id}_telephone_group" title="edit">
                                                                        <c:choose>
                                                                            <c:when test="${empty config.telephoneGroup}">(<fmt:message key="notSet" />)</c:when>
                                                                            <c:otherwise>
                                                                                <o:out value="${config.telephoneGroup.name}" />
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </v:ajaxLink>
                                                                </o:permission></td>
                                                        </tr>
                                                        <tr>
                                                            <td><fmt:message key="directDial" /> <v:ajaxInfobox id="internal" url="${url}/getLastChangeset?infos=internal" iconUrl="${applicationScope.importantIcon}" /></td>
                                                            <td id="${view.name}_popup_${config.id}_internal"><c:choose>
                                                                    <c:when test="${config.internalEditable}">
                                                                        <o:forbidden role="telephone_system_admin">
                                                                            <c:choose>
                                                                                <c:when test="${empty config.internal}">(<fmt:message key="notSet" />)</c:when>
                                                                                <c:otherwise>
                                                                                    <o:out value="${config.internal}" />
                                                                                </c:otherwise>
                                                                            </c:choose>
                                                                        </o:forbidden>
                                                                        <o:permission role="telephone_system_admin" info="permissionTelephoneSystemAdmin">
                                                                            <v:ajaxLink url="${url}/enableEditMode?name=internal" targetElement="${view.name}_popup_${config.id}_internal" activityElement="${view.name}_popup_${config.id}_internal" title="edit">
                                                                                <c:choose>
                                                                                    <c:when test="${empty config.internal}">(<fmt:message key="notSet" />)</c:when>
                                                                                    <c:otherwise>
                                                                                        <o:out value="${config.internal}" />
                                                                                    </c:otherwise>
                                                                                </c:choose>
                                                                            </v:ajaxLink>
                                                                        </o:permission>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <c:choose>
                                                                            <c:when test="${empty config.internal}">(<fmt:message key="notSet" />)</c:when>
                                                                            <c:otherwise>
                                                                                <o:out value="${config.internal}" />
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </c:otherwise>
                                                                </c:choose></td>
                                                        </tr>
                                                        <tr>
                                                            <td><fmt:message key="activated" /> <v:ajaxInfobox id="available" url="${url}/getLastChangeset?infos=available" iconUrl="${applicationScope.importantIcon}" /></td>
                                                            <td id="${view.name}_popup_${config.id}_available"><o:forbidden role="telephone_exchange_edit,telephone_system_admin,telephone_configuration_own_edit">
                                                                    <c:choose>
                                                                        <c:when test="${config.available}">
                                                                            <fmt:message key="common.yes" />
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <fmt:message key="no" />
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </o:forbidden> <o:permission role="telephone_exchange_edit,telephone_system_admin,telephone_configuration_own_edit" info="permissionTelephoneExchangeEdit">
                                                                    <v:ajaxLink url="${url}/enableEditMode?name=available" targetElement="${view.name}_popup_${config.id}_available" activityElement="${view.name}_popup_${config.id}_available" title="edit">
                                                                        <c:choose>
                                                                            <c:when test="${config.available}">
                                                                                <fmt:message key="common.yes" />
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                <fmt:message key="no" />
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </v:ajaxLink>
                                                                </o:permission></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div class="spacer"></div>
                                                <o:permission role="telephone_exchange_edit,telephone_system_admin" info="permissionTelephoneExchangeEdit">
                                                    <table class="table table-striped" style="margin-right: 10px; width: 500px; table-layout: fixed;">
                                                        <thead>
                                                            <tr>
                                                                <th colspan="2"><fmt:message key="action" /></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td id="${view.name}_popup_${config.id}_reboot_phone" colspan="2"><v:ajaxLink url="${url}/sendPhoneAction?name=reboot_phone" targetElement="${view.name}_popup_${config.id}_reboot_phone" activityElement="${view.name}_popup_${config.id}_reboot_phone" title="taskTelephoneReboot">
                                                                        <fmt:message key="taskTelephoneReboot" />
                                                                    </v:ajaxLink></td>
                                                            </tr>
                                                            <tr>
                                                                <td id="${view.name}_popup_${config.id}_send_check_config_to_phone" colspan="2"><v:ajaxLink url="${url}/sendPhoneAction?name=send_check_config_to_phone" targetElement="${view.name}_popup_${config.id}_send_check_config_to_phone"
                                                                        activityElement="${view.name}_popup_${config.id}_send_check_config_to_phone" title="taskReloadTelephoneConfiguration">
                                                                        <fmt:message key="taskReloadTelephoneConfiguration" />
                                                                    </v:ajaxLink></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <div class="spacer"></div>
                                                </o:permission>

                                                <table class="table table-striped" style="margin-right: 10px; width: 500px; table-layout: fixed;">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="2"><fmt:message key="busy" /></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><fmt:message key="busySignalOnBusy" /> <v:ajaxInfobox id="busy_on_busy" url="${url}/getLastChangeset?infos=busy_on_busy" iconUrl="${applicationScope.importantIcon}" /></td>
                                                            <td id="${view.name}_popup_${config.id}_busy_on_busy"><o:forbidden role="telephone_exchange_edit,telephone_system_admin,telephone_configuration_own_edit">
                                                                    <c:choose>
                                                                        <c:when test="${config.busyOnBusy}">
                                                                            <fmt:message key="activated" />
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <fmt:message key="deactivated" />
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </o:forbidden> <o:permission role="telephone_exchange_edit,telephone_system_admin,telephone_configuration_own_edit" info="permissionTelephoneExchangeEdit">
                                                                    <v:ajaxLink url="${url}/enableEditMode?name=busy_on_busy" targetElement="${view.name}_popup_${config.id}_busy_on_busy" activityElement="${view.name}_popup_${config.id}_busy_on_busy" title="edit">
                                                                        <c:choose>
                                                                            <c:when test="${config.busyOnBusy}">
                                                                                <fmt:message key="activated" />
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                <fmt:message key="deactivated" />
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </v:ajaxLink>
                                                                </o:permission></td>
                                                        </tr>
                                                        <tr>
                                                            <td><fmt:message key="voiceMailEnabledIfBusy" /> <v:ajaxInfobox id="voice_mail_enabled_if_busy" url="${url}/getLastChangeset?infos=voice_mail_enabled_if_busy" iconUrl="${applicationScope.importantIcon}" /></td>
                                                            <td id="${view.name}_popup_${config.id}_voice_mail_enabled_if_busy"><o:forbidden role="telephone_exchange_edit,telephone_system_admin,telephone_configuration_own_edit">
                                                                    <c:choose>
                                                                        <c:when test="${config.voiceMailEnabledIfBusy}">
                                                                            <fmt:message key="activated" />
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <fmt:message key="deactivated" />
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </o:forbidden> <o:permission role="telephone_exchange_edit,telephone_system_admin,telephone_configuration_own_edit" info="permissionTelephoneExchangeEdit">
                                                                    <v:ajaxLink url="${url}/enableEditMode?name=voice_mail_enabled_if_busy" targetElement="${view.name}_popup_${config.id}_voice_mail_enabled_if_busy" activityElement="${view.name}_popup_${config.id}_voice_mail_enabled_if_busy" title="edit">
                                                                        <c:choose>
                                                                            <c:when test="${config.voiceMailEnabledIfBusy}">
                                                                                <fmt:message key="activated" />
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                <fmt:message key="deactivated" />
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </v:ajaxLink>
                                                                </o:permission></td>
                                                        </tr>
                                                        <tr>
                                                            <td><fmt:message key="forwardingIfBusy" /> <v:ajaxInfobox id="call_forward_if_busy_enabled" url="${url}/getLastChangeset?infos=call_forward_if_busy_enabled" iconUrl="${applicationScope.importantIcon}" /></td>
                                                            <td id="${view.name}_popup_${config.id}_call_forward_if_busy_enabled"><o:forbidden role="telephone_exchange_edit,telephone_system_admin,telephone_configuration_own_edit">
                                                                    <c:choose>
                                                                        <c:when test="${config.callForwardIfBusyEnabled}">
                                                                            <fmt:message key="activated" />
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <fmt:message key="deactivated" />
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </o:forbidden> <o:permission role="telephone_exchange_edit,telephone_system_admin,telephone_configuration_own_edit" info="permissionTelephoneExchangeEdit">
                                                                    <v:ajaxLink url="${url}/enableEditMode?name=call_forward_if_busy_enabled" targetElement="${view.name}_popup_${config.id}_call_forward_if_busy_enabled" activityElement="${view.name}_popup_${config.id}_call_forward_if_busy_enabled" title="edit">
                                                                        <c:choose>
                                                                            <c:when test="${config.callForwardIfBusyEnabled}">
                                                                                <fmt:message key="activated" />
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                <fmt:message key="deactivated" />
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </v:ajaxLink>
                                                                </o:permission></td>
                                                        </tr>
                                                        <tr>
                                                            <td><fmt:message key="forwardingIfBusyTo" /> <v:ajaxInfobox id="call_forward_if_busy_target_phone_number" url="${url}/getLastChangeset?infos=call_forward_if_busy_target_phone_number" iconUrl="${applicationScope.importantIcon}" /></td>
                                                            <td id="${view.name}_popup_${config.id}_call_forward_if_busy_target_phone_number"><o:forbidden role="telephone_exchange_edit,telephone_system_admin,telephone_configuration_own_edit">
                                                                    <c:choose>
                                                                        <c:when test="${empty config.callForwardIfBusyTargetPhoneNumber}">(<fmt:message key="notSet" />)</c:when>
                                                                        <c:otherwise>
                                                                            <o:out value="${config.callForwardIfBusyTargetPhoneNumber}" />
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </o:forbidden> <o:permission role="telephone_exchange_edit,telephone_system_admin,telephone_configuration_own_edit" info="permissionTelephoneExchangeEdit">
                                                                    <v:ajaxLink url="${url}/enableEditMode?name=call_forward_if_busy_target_phone_number" targetElement="${view.name}_popup_${config.id}_call_forward_if_busy_target_phone_number" activityElement="${view.name}_popup_${config.id}_call_forward_if_busy_target_phone_number" title="edit">
                                                                        <c:choose>
                                                                            <c:when test="${empty config.callForwardIfBusyTargetPhoneNumber}">(<fmt:message key="notSet" />)</c:when>
                                                                            <c:otherwise>
                                                                                <o:out value="${config.callForwardIfBusyTargetPhoneNumber}" />
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </v:ajaxLink>
                                                                </o:permission></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div class="spacer"></div>

                                                <table class="table table-striped" style="margin-right: 10px; width: 500px; table-layout: fixed;">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="2"><fmt:message key="voiceMail" /></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td style="width: 250px;"><fmt:message key="voiceMail" /> <v:ajaxInfobox id="voice_mail_enabled" url="${url}/getLastChangeset?infos=voice_mail_enabled" iconUrl="${applicationScope.importantIcon}" /></td>
                                                            <td id="${view.name}_popup_${config.id}_voice_mail_enabled"><o:forbidden role="telephone_exchange_edit,telephone_system_admin,telephone_configuration_own_edit">
                                                                    <c:choose>
                                                                        <c:when test="${config.voiceMailEnabled}">
                                                                            <fmt:message key="activated" />
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <fmt:message key="deactivated" />
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </o:forbidden> <o:permission role="telephone_exchange_edit,telephone_system_admin,telephone_configuration_own_edit" info="permissionTelephoneExchangeEdit">
                                                                    <v:ajaxLink url="${url}/enableEditMode?name=voice_mail_enabled" targetElement="${view.name}_popup_${config.id}_voice_mail_enabled" activityElement="${view.name}_popup_${config.id}_voice_mail_enabled" title="edit">
                                                                        <c:choose>
                                                                            <c:when test="${config.voiceMailEnabled}">
                                                                                <fmt:message key="activated" />
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                <fmt:message key="deactivated" />
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </v:ajaxLink>
                                                                </o:permission></td>
                                                        </tr>
                                                        <tr>
                                                            <td><fmt:message key="delay" /> (<fmt:message key="seconds" />) <v:ajaxInfobox id="voice_mail_delay_seconds" url="${url}/getLastChangeset?infos=voice_mail_delay_seconds" iconUrl="${applicationScope.importantIcon}" /></td>
                                                            <td id="${view.name}_popup_${config.id}_voice_mail_delay_seconds"><o:forbidden role="telephone_exchange_edit,telephone_system_admin,telephone_configuration_own_edit">
                                                                    <c:choose>
                                                                        <c:when test="${empty config.voiceMailDelaySeconds}">(<fmt:message key="notSet" />)</c:when>
                                                                        <c:otherwise>
                                                                            <o:out value="${config.voiceMailDelaySeconds}" />
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </o:forbidden> <o:permission role="telephone_exchange_edit,telephone_system_admin,telephone_configuration_own_edit" info="permissionTelephoneExchangeEdit">
                                                                    <v:ajaxLink url="${url}/enableEditMode?name=voice_mail_delay_seconds" targetElement="${view.name}_popup_${config.id}_voice_mail_delay_seconds" activityElement="${view.name}_popup_${config.id}_voice_mail_delay_seconds" title="edit">
                                                                        <c:choose>
                                                                            <c:when test="${empty config.voiceMailDelaySeconds}">(<fmt:message key="notSet" />)</c:when>
                                                                            <c:otherwise>
                                                                                <o:out value="${config.voiceMailDelaySeconds}" />
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </v:ajaxLink>
                                                                </o:permission></td>
                                                        </tr>
                                                        <o:permission role="telephone_system_admin" info="permissionTelephoneSystemAdmin">
                                                            <tr>
                                                                <td><fmt:message key="voiceMailSendAsEmail" /> <v:ajaxInfobox id="voice_mail_send_as_email" url="${url}/getLastChangeset?infos=voice_mail_send_as_email" iconUrl="${applicationScope.importantIcon}" /></td>
                                                                <td id="${view.name}_popup_${config.id}_voice_mail_send_as_email"><v:ajaxLink url="${url}/enableEditMode?name=voice_mail_send_as_email" targetElement="${view.name}_popup_${config.id}_voice_mail_send_as_email" activityElement="${view.name}_popup_${config.id}_voice_mail_send_as_email" title="edit">
                                                                        <c:choose>
                                                                            <c:when test="${config.voiceMailSendAsEmail}">
                                                                                <fmt:message key="activated" />
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                <fmt:message key="deactivated" />
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </v:ajaxLink></td>
                                                            </tr>
                                                            <tr>
                                                                <td><fmt:message key="voiceMailDeleteAfterEmailSend" /> <v:ajaxInfobox id="voice_mail_delete_after_email_send" url="${url}/getLastChangeset?infos=voice_mail_delete_after_email_send" iconUrl="${applicationScope.importantIcon}" /></td>
                                                                <td id="${view.name}_popup_${config.id}_voice_mail_delete_after_email_send"><v:ajaxLink url="${url}/enableEditMode?name=voice_mail_delete_after_email_send" targetElement="${view.name}_popup_${config.id}_voice_mail_delete_after_email_send"
                                                                        activityElement="${view.name}_popup_${config.id}_voice_mail_delete_after_email_send" title="edit">
                                                                        <c:choose>
                                                                            <c:when test="${config.voiceMailDeleteAfterEmailSend}">
                                                                                <fmt:message key="activated" />
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                <fmt:message key="deactivated" />
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </v:ajaxLink></td>
                                                            </tr>
                                                        </o:permission>
                                                        <tr>
                                                            <td><fmt:message key="voiceMailEnabledIfUnavailable" /> <v:ajaxInfobox id="voice_mail_enabled_if_unavailable" url="${url}/getLastChangeset?infos=voice_mail_enabled_if_unavailable" iconUrl="${applicationScope.importantIcon}" /></td>
                                                            <td id="${view.name}_popup_${config.id}_voice_mail_enabled_if_unavailable"><o:forbidden role="telephone_exchange_edit,telephone_system_admin,telephone_configuration_own_edit">
                                                                    <c:choose>
                                                                        <c:when test="${config.voiceMailEnabledIfUnavailable}">
                                                                            <fmt:message key="activated" />
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <fmt:message key="deactivated" />
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </o:forbidden> <o:permission role="telephone_exchange_edit,telephone_system_admin,telephone_configuration_own_edit" info="permissionTelephoneExchangeEdit">
                                                                    <v:ajaxLink url="${url}/enableEditMode?name=voice_mail_enabled_if_unavailable" targetElement="${view.name}_popup_${config.id}_voice_mail_enabled_if_unavailable" activityElement="${view.name}_popup_${config.id}_voice_mail_enabled_if_unavailable" title="edit">
                                                                        <c:choose>
                                                                            <c:when test="${config.voiceMailEnabledIfUnavailable}">
                                                                                <fmt:message key="activated" />
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                <fmt:message key="deactivated" />
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </v:ajaxLink>
                                                                </o:permission></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div class="spacer"></div>

                                                <table class="table table-striped" style="margin-right: 10px; width: 500px; table-layout: fixed;">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="2"><fmt:message key="parallelCall" /></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td style="width: 250px;"><fmt:message key="parallelCall" /> <v:ajaxInfobox id="parallel_call_enabled" url="${url}/getLastChangeset?infos=parallel_call_enabled" iconUrl="${applicationScope.importantIcon}" /></td>
                                                            <td id="${view.name}_popup_${config.id}_parallel_call_enabled"><o:forbidden role="telephone_exchange_edit,telephone_system_admin,telephone_configuration_own_edit">
                                                                    <c:choose>
                                                                        <c:when test="${config.parallelCallEnabled}">
                                                                            <fmt:message key="activated" />
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <fmt:message key="deactivated" />
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </o:forbidden> <o:permission role="telephone_exchange_edit,telephone_system_admin,telephone_configuration_own_edit" info="permissionTelephoneExchangeEdit">
                                                                    <v:ajaxLink url="${url}/enableEditMode?name=parallel_call_enabled" targetElement="${view.name}_popup_${config.id}_parallel_call_enabled" activityElement="${view.name}_popup_${config.id}_parallel_call_enabled" title="edit">
                                                                        <c:choose>
                                                                            <c:when test="${config.parallelCallEnabled}">
                                                                                <fmt:message key="activated" />
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                <fmt:message key="deactivated" />
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </v:ajaxLink>
                                                                </o:permission></td>
                                                        </tr>
                                                        <tr>
                                                            <td><fmt:message key="parallelCallTo" /> <v:ajaxInfobox id="parallel_call_target_phone_number" url="${url}/getLastChangeset?infos=parallel_call_target_phone_number" iconUrl="${applicationScope.importantIcon}" /></td>
                                                            <td id="${view.name}_popup_${config.id}_parallel_call_target_phone_number"><o:forbidden role="telephone_exchange_edit,telephone_system_admin,telephone_configuration_own_edit">
                                                                    <c:choose>
                                                                        <c:when test="${empty config.parallelCallTargetPhoneNumber}">(<fmt:message key="notSet" />)</c:when>
                                                                        <c:otherwise>
                                                                            <o:out value="${config.parallelCallTargetPhoneNumber}" />
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </o:forbidden> <o:permission role="telephone_exchange_edit,telephone_system_admin,telephone_configuration_own_edit" info="permissionTelephoneExchangeEdit">
                                                                    <v:ajaxLink url="${url}/enableEditMode?name=parallel_call_target_phone_number" targetElement="${view.name}_popup_${config.id}_parallel_call_target_phone_number" activityElement="${view.name}_popup_${config.id}_parallel_call_target_phone_number" title="edit">
                                                                        <c:choose>
                                                                            <c:when test="${empty config.parallelCallTargetPhoneNumber}">(<fmt:message key="notSet" />)</c:when>
                                                                            <c:otherwise>
                                                                                <o:out value="${config.parallelCallTargetPhoneNumber}" />
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </v:ajaxLink>
                                                                </o:permission></td>
                                                        </tr>
                                                        <tr>
                                                            <td><fmt:message key="delay" /> (<fmt:message key="seconds" />) <v:ajaxInfobox id="parallel_call_set_delay_seconds" url="${url}/getLastChangeset?infos=parallel_call_set_delay_seconds" iconUrl="${applicationScope.importantIcon}" /></td>
                                                            <td id="${view.name}_popup_${config.id}_parallel_call_set_delay_seconds"><o:forbidden role="telephone_exchange_edit,telephone_system_admin,telephone_configuration_own_edit">
                                                                    <c:choose>
                                                                        <c:when test="${empty config.parallelCallSetDelaySeconds}">(<fmt:message key="notSet" />)</c:when>
                                                                        <c:otherwise>
                                                                            <o:out value="${config.parallelCallSetDelaySeconds}" />
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </o:forbidden> <o:permission role="telephone_exchange_edit,telephone_system_admin,telephone_configuration_own_edit" info="permissionTelephoneExchangeEdit">
                                                                    <v:ajaxLink url="${url}/enableEditMode?name=parallel_call_set_delay_seconds" targetElement="${view.name}_popup_${config.id}_parallel_call_set_delay_seconds" activityElement="${view.name}_popup_${config.id}_parallel_call_set_delay_seconds" title="edit">
                                                                        <c:choose>
                                                                            <c:when test="${empty config.parallelCallSetDelaySeconds}">(<fmt:message key="notSet" />)</c:when>
                                                                            <c:otherwise>
                                                                                <o:out value="${config.parallelCallSetDelaySeconds}" />
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </v:ajaxLink>
                                                                </o:permission></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div class="spacer"></div>

                                                <table class="table table-striped" style="margin-right: 10px; width: 500px; table-layout: fixed;">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="2"><fmt:message key="forwarding" /></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td style="width: 250px;"><fmt:message key="forwarding" /> <v:ajaxInfobox id="costCenter" url="${url}/getLastChangeset?infos=call_forward_enabled" iconUrl="${applicationScope.importantIcon}" /></td>
                                                            <td id="${view.name}_popup_${config.id}_call_forward_enabled"><o:forbidden role="telephone_exchange_edit,telephone_system_admin,telephone_configuration_own_edit">
                                                                    <c:choose>
                                                                        <c:when test="${config.callForwardEnabled}">
                                                                            <fmt:message key="activated" />
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <fmt:message key="deactivated" />
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </o:forbidden> <o:permission role="telephone_exchange_edit,telephone_system_admin,telephone_configuration_own_edit" info="permissionTelephoneExchangeEdit">
                                                                    <v:ajaxLink url="${url}/enableEditMode?name=call_forward_enabled" targetElement="${view.name}_popup_${config.id}_call_forward_enabled" activityElement="${view.name}_popup_${config.id}_call_forward_enabled" title="edit">
                                                                        <c:choose>
                                                                            <c:when test="${config.callForwardEnabled}">
                                                                                <fmt:message key="activated" />
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                <fmt:message key="deactivated" />
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </v:ajaxLink>
                                                                </o:permission></td>
                                                        </tr>
                                                        <tr>
                                                            <td><fmt:message key="forwardingTo" /> <v:ajaxInfobox id="call_forward_target_phone_number" url="${url}/getLastChangeset?infos=call_forward_target_phone_number" iconUrl="${applicationScope.importantIcon}" /></td>
                                                            <td id="${view.name}_popup_${config.id}_call_forward_target_phone_number"><o:forbidden role="telephone_exchange_edit,telephone_system_admin,telephone_configuration_own_edit">
                                                                    <c:choose>
                                                                        <c:when test="${empty config.callForwardTargetPhoneNumber}">(<fmt:message key="notSet" />)</c:when>
                                                                        <c:otherwise>
                                                                            <o:out value="${config.callForwardTargetPhoneNumber}" />
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </o:forbidden> <o:permission role="telephone_exchange_edit,telephone_system_admin,telephone_configuration_own_edit" info="permissionTelephoneExchangeEdit">
                                                                    <v:ajaxLink url="${url}/enableEditMode?name=call_forward_target_phone_number" targetElement="${view.name}_popup_${config.id}_call_forward_target_phone_number" activityElement="${view.name}_popup_${config.id}_call_forward_target_phone_number" title="edit">
                                                                        <c:choose>
                                                                            <c:when test="${empty config.callForwardTargetPhoneNumber}">(<fmt:message key="notSet" />)</c:when>
                                                                            <c:otherwise>
                                                                                <o:out value="${config.callForwardTargetPhoneNumber}" />
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </v:ajaxLink>
                                                                </o:permission></td>
                                                        </tr>
                                                        <tr>
                                                            <td><fmt:message key="delay" /> (<fmt:message key="seconds" />) <v:ajaxInfobox id="call_forward_delay_seconds" url="${url}/getLastChangeset?infos=call_forward_delay_seconds" iconUrl="${applicationScope.importantIcon}" /></td>
                                                            <td id="${view.name}_popup_${config.id}_call_forward_delay_seconds"><o:forbidden role="telephone_exchange_edit,telephone_system_admin,telephone_configuration_own_edit">
                                                                    <c:choose>
                                                                        <c:when test="${empty config.callForwardDelaySeconds}">(<fmt:message key="notSet" />)</c:when>
                                                                        <c:otherwise>
                                                                            <o:out value="${config.callForwardDelaySeconds}" />
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </o:forbidden> <o:permission role="telephone_exchange_edit,telephone_system_admin,telephone_configuration_own_edit" info="permissionTelephoneExchangeEdit">
                                                                    <v:ajaxLink url="${url}/enableEditMode?name=call_forward_delay_seconds" targetElement="${view.name}_popup_${config.id}_call_forward_delay_seconds" activityElement="${view.name}_popup_${config.id}_call_forward_delay_seconds" title="edit">
                                                                        <c:choose>
                                                                            <c:when test="${empty config.callForwardDelaySeconds}">(<fmt:message key="notSet" />)</c:when>
                                                                            <c:otherwise>
                                                                                <o:out value="${config.callForwardDelaySeconds}" />
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </v:ajaxLink>
                                                                </o:permission></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div class="spacer"></div>
                                            </div>
                                        </div>
                                    </div>
                                </c:otherwise>
                            </c:choose>
                        </c:otherwise>
                    </c:choose>
                </c:if>
            </c:otherwise>
        </c:choose>
    </c:otherwise>
</c:choose>