/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 10-Aug-2006 17:31:45 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Comparator;

import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import com.osserp.common.dao.AbstractRowMapper;
import com.osserp.core.finance.RecordExport;
import com.osserp.core.model.records.RecordExportImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RecordExportRowMapper extends AbstractRowMapper {

    public static final String SELECT_VALUES =
            "SELECT id,type_id,created,created_by,start_date,end_date,export_count FROM ";

    public static RowMapperResultSetExtractor getReader() {
        return new RowMapperResultSetExtractor(new RecordExportRowMapper());
    }

    public Object mapRow(ResultSet rs, int arg1) throws SQLException {
        RecordExportImpl export = new RecordExportImpl(
                Long.valueOf(rs.getLong(1)),
                Long.valueOf(rs.getLong(2)),
                rs.getTimestamp(3),
                Long.valueOf(rs.getLong(4)),
                rs.getTimestamp(5),
                rs.getTimestamp(6),
                rs.getInt(7));
        return export;
    }

    public static Comparator createComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    RecordExport recordA = (RecordExport) a;
                    RecordExport recordB = (RecordExport) b;
                    String intervalA = recordA.getExportInterval();
                    String intervalB = recordB.getExportInterval();
                    if (intervalA == null && intervalB == null) {
                        return 0;
                    }
                    if (intervalA == null) {
                        return -1;
                    }
                    if (intervalB == null) {
                        return 1;
                    }
                    return intervalB.compareTo(intervalA);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                RecordExport recordA = (RecordExport) a;
                RecordExport recordB = (RecordExport) b;
                String intervalA = recordA.getExportInterval();
                String intervalB = recordB.getExportInterval();
                if (intervalA == null && intervalB == null) {
                    return 0;
                }
                if (intervalA == null) {
                    return 1;
                }
                if (intervalB == null) {
                    return -1;
                }
                return intervalA.compareTo(intervalB);
            }
        };
    }
}
