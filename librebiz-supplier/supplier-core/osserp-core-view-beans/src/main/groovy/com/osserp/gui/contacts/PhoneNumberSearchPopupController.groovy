/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.osserp.gui.contacts

import javax.servlet.http.HttpServletRequest

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.common.ClientException
import com.osserp.groovy.web.PopupViewController

/**
 * Display phone numbers page layout
 * 
 * @author so <so@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
@RequestMapping("/contacts/phoneNumberSearchPopup/*")
@Controller class PhoneNumberSearchPopupController extends PopupViewController {
    private static Logger logger = LoggerFactory.getLogger(PhoneNumberSearchPopupController.class.getName())

    @RequestMapping
    def search(HttpServletRequest request) {
        logger.debug('search: invoked')
        PhoneNumberSearchPopupView view = getView(request)
        try {
            view?.search()
        } catch (ClientException t) {
            if (logger.debugEnabled) {
                logger.debug("search: caught exception [message=${t.message}]")
            }
            request.getSession().setAttribute('error', t.message)
        }
        pageArea
    }
}
