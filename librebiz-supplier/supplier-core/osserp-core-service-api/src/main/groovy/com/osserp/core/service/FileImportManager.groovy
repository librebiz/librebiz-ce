/**
 *
 * Copyright (C) 2014, 2017 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 2, 2017
 * 
 */
package com.osserp.core.service

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.FileObject
import com.osserp.common.util.FileUtil
import com.osserp.common.util.JsonObject
import com.osserp.common.util.JsonUtil

import com.osserp.core.ImportManager
import com.osserp.core.dao.SyncTasks
import com.osserp.core.dao.SystemConfigs
import com.osserp.core.employees.Employee
import com.osserp.core.system.SyncTask
import com.osserp.core.system.SyncTaskConfig
import com.osserp.core.system.SyncTaskExcecutor

/**
 *
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public abstract class FileImportManager implements ImportManager, SyncTaskExcecutor {
    private static Logger log = LoggerFactory.getLogger(FileImportManager.class.getName())

    private SyncTasks syncTasks
    private SystemConfigs systemConfigs
    private FileImporter fileImporter

    protected FileImportManager(SystemConfigs systemConfigs, SyncTasks syncTasks, FileImporter fileImporter) {
        this.systemConfigs = systemConfigs
        this.syncTasks = syncTasks
        this.fileImporter = fileImporter
    }

    protected abstract String getImportName();

    List<SyncTask> findImports() {
        List<SyncTask> tasks = []
        SyncTaskConfig config = getTaskConfig()
        if (config != null) {
            tasks = syncTasks.find(config)
        }
        return tasks
    }

    String importFile(Employee user, FileObject fileObject, boolean performActions) throws ClientException {
        String filename = FileUtil.generateRandomFileName(fileObject.getFileName(), 18)
        FileUtil.persist(fileObject.getFileData(), createFileUri(filename))
        SyncTaskConfig taskConfig = getTaskConfig()
        if (taskConfig != null) {
            Map data = [
                filename: filename,
                sourcefile: fileObject.getFileName(),
                performActions: performActions,
                user: user?.id
            ]
            createImportTask(taskConfig, JsonUtil.toJsonString(data))
        }
        return 'ok'
    }

    protected void createImportTask(SyncTaskConfig taskConfig, String json) {
        syncTasks.create(taskConfig, importName, 'import', null, json)
    }

    void execute(SyncTask syncTask) throws Exception {
        log.debug("execute() invoked [type=${syncTask.typeName}, action=${syncTask.actionName}, objectData=${syncTask.objectData}]")
        
        if (syncTask.objectData) {
            JsonObject json = JsonUtil.fetchInput(syncTask.objectData)
            boolean performActions = json.getBoolean('performActions')
            String filename = json.getString('filename')
            if (filename) {
                String uri = createFileUri(filename)
                fileImporter.init(uri, performActions)
                Integer i = fileImporter.run()
                json.put("resultCount", i)
                String exceptionReport = fileImporter.exceptionReport
                if (exceptionReport) {
                    json.put("errorReport", exceptionReport)
                }
                syncTasks.update(syncTask, json.toString())
                if (log.isInfoEnabled()) {
                    log.info("execute() import done [type=${syncTask.typeName}, count=${i}]")
                }
            }
        }
    }

    private SyncTaskConfig getTaskConfig() {
        Long taskId = systemConfigs.getSystemPropertyId("${importName}ImportConfig")
        if (taskId) {
            return syncTasks.getConfig(taskId)
        }
        return null
    }

    private String createFileUri(String filename) {
        String importDir = systemConfigs.getSystemProperty("${importName}ImportPath")
        if (importDir) {
            return "${importDir}/${filename}"
        }
        return filename
    }

}
