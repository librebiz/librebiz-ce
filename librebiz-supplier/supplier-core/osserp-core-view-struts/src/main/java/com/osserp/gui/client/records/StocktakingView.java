/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 24-Dec-2006 10:42:50 
 * 
 */
package com.osserp.gui.client.records;

import java.util.List;

import org.jdom2.Document;

import com.osserp.common.ClientException;
import com.osserp.common.Option;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;
import com.osserp.common.web.View;
import com.osserp.common.web.ViewName;
import com.osserp.core.system.SystemCompany;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("stocktakingView")
public interface StocktakingView extends View {

    /**
     * Provides the selected company
     * @return selectedCompany
     */
    public SystemCompany getSelectedCompany();

    /**
     * Provides the stocktaking types
     * @return types
     */
    public List<Option> getTypes();

    /**
     * Provides all active employee names
     * @return employees
     */
    public List<Option> getEmployees();

    /**
     * Indicates that view is in product edit mode
     * @return true if so
     */
    public boolean isProductEditMode();

    /**
     * Enables/disables product edit mode
     * @param productEditMode
     */
    public void setProductEditMode(boolean productEditMode);

    /**
     * Synchronizes counted quantity with expected quentity
     */
    public void synchronizeQuantity();

    /**
     * Updates product counters
     * @param fh
     * @throws ClientException if validation failed
     */
    public void updateProducts(Form fh) throws ClientException;

    /**
     * Closes current activated stocktaking
     * @throws ClientException if any item has count of null
     * @throws PermissionException if user has no permission
     */
    public void close() throws ClientException, PermissionException;

    /**
     * Delete stocktaking object
     * @throws ClientException
     * @throws PermissionException
     */
    public void delete() throws ClientException, PermissionException;

    /**
     * Provides current stocktaking values as xml
     * @param stylesheetName
     * @return doc
     * @throws ClientException if any required values missing
     * @throws PermissionException if user has no permission
     */
    public Document getXml(String stylesheetName) throws ClientException, PermissionException;

}
