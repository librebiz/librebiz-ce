/**
 *
 * Copyright (C) 2008, 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Jan 31, 2008 11:07:52 AM 
 * Created on Jul 11, 2011 12:10:15 PM (Groovy implementation) 
 * 
 */
package com.osserp.gui.reporting

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.util.NumberUtil

import com.osserp.core.employees.Employee
import com.osserp.core.events.EventConfigType
import com.osserp.core.events.EventManager
import com.osserp.core.planning.ProductPlanningManager
import com.osserp.core.sales.SalesMonitoringManager
import com.osserp.core.sales.SalesUtil

import com.osserp.groovy.web.MenuItem

/**
 *
 * @author rk <rk@osserp.com>
 * @author eh <eh@osserp.com>
 * 
 */
public class ProjectMonitoringView extends AbstractSalesListReportingView {
    private static Logger logger = LoggerFactory.getLogger(ProjectMonitoringView.class.getName())
    private static final String IGNORING_BRANCH_PERMISSION_GROUP_NAME = 'salesMonitoringDisplayAllPermissions'

    String[] ignoringBranchPermissions = null
    Double totalCapacity = 0d
    Double averageCapacity = 0d

    ProjectMonitoringView() {
        super()
        selectors.businessType = 'salesMonitoringProjects'
        selectors.branch = 'projectAwareBranchsSelection'
        selectors.status = true
        refreshLink = true
        env.employeeMode = false
    }

    @Override
    protected void init() {
        ignoringBranchPermissions = systemConfigManager.getPermissionConfig(IGNORING_BRANCH_PERMISSION_GROUP_NAME)
        selectors.businessType = form.getString('selection') ?: selectors.businessType
        env.employeeMode = form.getBoolean('employeeMode', env.employeeMode)
        if (employeeMode || !ignoreBranch) {
            selectors.branch = null
        }
        if (forwardRequest) {
            Long minStatus = NumberUtil.createLong(getUserProperty('projectMonitoringStatus'))
            if (minStatus && !form.getLong('statusValue')) {
                values.statusValue = minStatus
            }
        }
    }

    @Override
    void createCustomNavigation() {
        super.createCustomNavigation()
        if (employeeMode) {
            nav.listNavigation = [nav.listNavigation[0] ]+ [startupPageConfigLink ]+ nav.listNavigation[1..-1]
            nav.defaultNavigation = nav.listNavigation
        }
    }

    MenuItem getStartupPageConfigLink() {
        new MenuItem(
            link: "${context.fullPath}/users/userSettings/forward?exit=/reporting/projectMonitoring/reload",
            icon: 'configureIcon',
            title: 'setStartupPage')
    }

    boolean isEmployeeMode() {
        env.employeeMode
    }

    @Override
    void loadList() {
        list = salesMonitoringManager.getSalesMonitoring(selectors.businessType)
        logger.debug("loadList: done [selector=${selectors.businessType}, size=${list.size()}]")
    }

    @Override
    protected void preFilter() {
        if (!ignoreBranch) {
            list = SalesUtil.filter(domainUser, list)
            logger.debug("preFilter: filtered by employee branch [size=${list.size()}]")
        }
        if (employeeMode) {
            Long id = domainEmployee.id
            list = list.findAll { id == it.salesId || id == it.managerId || id == it.managerSubId }
            logger.debug("preFilter: filtered by employees project role [size=${list.size()}]")
        }
    }

    @Override
    protected void postFilter() {
        totalCapacity = 0d
        averageCapacity = 0d
        Map appointments = fetchAppointments(domainEmployee, EventConfigType.SALES_APPOINTMENT)
        list.each {
            if (appointments.containsKey(it.id)) {
                it.setAppointment(appointments[it.id])
            }
            it.setVacant(!productPlanningManager.isAvailableForDelivery(it))
            if (it.capacity) {
                totalCapacity += it.capacity
            }
        }
        if (totalCapacity && list.size() > 0) {
            averageCapacity = totalCapacity / list.size()
        }
    }

    private boolean isIgnoreBranch() {
        domainUser.isPermissionGrant(ignoringBranchPermissions) || domainUser.isFromHeadquarter()
    }

    private Map fetchAppointments(Employee user, Long eventConfigType) {
        Map map = [:]
        List appointments = eventManager.findAppointmentsByRecipient(user.id)
        appointments.each {
            if (eventConfigType == it.action.type.id) {
                if (!map.containsKey(it.referenceId) 
                        || !it.appointmentDate.before(map[it.referenceId]?.appointmentDate)) {
                    map[it.referenceId] = it
                }
            }
        }
        return map
    }

    private ProductPlanningManager getProductPlanningManager() {
        getService(ProductPlanningManager.class.getName())
    }

    private EventManager getEventManager() {
        getService(EventManager.class.getName())
    }

    private SalesMonitoringManager getSalesMonitoringManager() {
        getService(SalesMonitoringManager.class.getName())
    }
}
