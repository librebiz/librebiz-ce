/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 10, 2008 7:55:04 AM 
 * 
 */
package com.osserp.core.dao;

import com.osserp.common.ClientException;
import com.osserp.common.User;
import com.osserp.common.dms.DmsDocument;
import com.osserp.common.dms.dao.ReferencedDocuments;
import com.osserp.core.dms.Letter;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface LetterDocuments extends ReferencedDocuments {

    /**
     * Saves a document for the specified reference
     * @param user creating the document
     * @param letter
     * @param file as byte array
     * @throws ClientException if validation failed
     */
    DmsDocument create(User user, Letter letter, byte[] file) throws ClientException;
}
