/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 12, 2006 3:13:56 AM 
 * 
 */
package com.osserp.core.dao.impl;

import com.osserp.common.Option;
import com.osserp.common.OptionsCache;
import com.osserp.common.dao.AbstractRowMapper;

import com.osserp.core.BusinessType;
import com.osserp.core.Options;
import com.osserp.core.finance.BillingType;
import com.osserp.core.finance.RecordType;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.system.SystemCompany;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class BusinessObjectRowMapper extends AbstractRowMapper {
    private static final String SEPARATOR = ", ";

    public BusinessObjectRowMapper(OptionsCache optionsCache) {
        super(optionsCache);
    }

    protected BusinessType fetchBusinessType(Long id) {
        return (BusinessType) fetchOption(Options.REQUEST_TYPES, id);
    }

    protected StringBuilder createContactName(String firstName, String lastName) {
        StringBuilder name = new StringBuilder(128);
        if (!isEmpty(firstName) && !isEmpty(lastName)) {
            name.append(lastName).append(SEPARATOR).append(firstName);

        } else if (!isEmpty(lastName)) {
            name.append(lastName);
        }
        return name;
    }

    protected String fetchSalutation(Long id) {
        try {
            return fetchOptionValue(Options.SALUTATIONS, id);
        } catch (Exception e) {
            return "";
        }
    }

    protected final RecordType fetchRecordType(Long typeId) {
        RecordType type = null;
        if (com.osserp.core.finance.Records.isPayment(typeId)) {
            type = (BillingType) fetchOption(Options.BILLING_TYPES, typeId);
        } else if (typeId != null) {
            type = (RecordType) fetchOption(Options.RECORD_TYPES, typeId);
        }
        return type;
    }

    protected final SystemCompany fetchCompany(Long id) {
        if (id != null) {
            return (SystemCompany) fetchOption(Options.SYSTEM_COMPANIES, id);
        }
        return null;
    }

    protected final BranchOffice fetchBranch(Long id) {
        if (id != null) {
            return (BranchOffice) fetchOption(Options.BRANCH_OFFICES, id);
        }
        return null;
    }

    protected String fetchEmployeeKey(Long id) {
        Option o = id == null ? null : fetchOption(Options.EMPLOYEE_KEYS, id);
        return o == null ? null : o.getName();
    }
}
