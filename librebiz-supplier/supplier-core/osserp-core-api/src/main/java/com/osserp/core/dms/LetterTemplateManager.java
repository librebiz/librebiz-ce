/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 25, 2008 11:24:34 PM 
 * 
 */
package com.osserp.core.dms;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;

import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface LetterTemplateManager extends LetterContentManager {

    /**
     * Provides all existing letter types
     * @return types
     */
    List<LetterType> getTypes();

    /**
     * Updates a letter type
     * @param letterType
     * @param name
     * @param description
     * @param greetings
     * @param xslName
     */
    void updateType(LetterType letterType, String name, String description, String greetings, String xslName);

    /**
     * Provides all existing templates by type
     * @param type
     * @return letters
     */
    List<LetterTemplate> findTemplates(LetterType type);

    /**
     * Provides standalone or embedded templates
     * @param type
     * @param embedded
     * @return templates by embedded property
     */
    List<LetterTemplate> findTemplates(LetterType type, boolean embedded);

    /**
     * Creates a new template by an existing letter
     * @param user
     * @param letter
     * @return template new created
     */
    LetterTemplate create(DomainUser user, Letter letter);

    /**
     * Creates a new template
     * @param user
     * @param type
     * @param name
     * @param branch
     * @param template
     * @return template new created
     */
    LetterTemplate create(DomainUser user, LetterType type, String name, Long branch, LetterTemplate template);

    /**
     * Deletes an existing template
     * @param user
     * @param template
     */
    void delete(DomainUser user, LetterTemplate template) throws PermissionException;

    /**
     * Provides required permissions to delete a template
     * @return permissions
     */
    String[] getDeletePermissions();

    /**
     * Updates a templates settings
     * @param user
     * @param template
     * @param templateName
     * @param subject
     * @param salutation
     * @param greetings
     * @param language
     * @param signatureLeft
     * @param signatureRight
     * @param ignoreSalutation,
     * @param ignoreGreetings
     * @param ignoreHeader
     * @param ignoreSubject
     * @param embedded
     * @param embeddedAbove
     * @param embeddedSpaceAfter
     * @param contentKeepTogether
     * @param defaultTemplate
     * @throws ClientException
     */
    void update(
            DomainUser user,
            LetterTemplate template,
            String templateName,
            String subject,
            String salutation,
            String greetings,
            String language,
            Long signatureLeft,
            Long signatureRight,
            boolean ignoreSalutation,
            boolean ignoreGreetings,
            boolean ignoreHeader,
            boolean ignoreSubject,
            boolean embedded,
            boolean embeddedAbove,
            String embeddedSpaceAfter,
            String contentKeepTogether,
            boolean defaultTemplate)
            throws ClientException;
}
