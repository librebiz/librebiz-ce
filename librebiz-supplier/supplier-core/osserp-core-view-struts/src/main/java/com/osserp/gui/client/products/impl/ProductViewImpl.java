/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 19, 2006 2:43:17 PM 
 * 
 */
package com.osserp.gui.client.products.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionException;
import com.osserp.common.dms.DmsDocument;
import com.osserp.common.dms.DmsReference;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.ViewName;

import com.osserp.core.BusinessNote;
import com.osserp.core.finance.ListItem;
import com.osserp.core.finance.RecordSearch;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductCategoryConfig;
import com.osserp.core.products.ProductGroupConfig;
import com.osserp.core.products.ProductManager;
import com.osserp.core.products.ProductNoteManager;
import com.osserp.core.products.ProductType;
import com.osserp.core.products.ProductTypeConfig;
import com.osserp.core.products.SerialDisplay;
import com.osserp.core.sales.SalesInvoiceManager;
import com.osserp.core.views.StockReportDisplay;
import com.osserp.gui.client.Actions;
import com.osserp.gui.client.products.ProductView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("productView")
public class ProductViewImpl extends AbstractProductView implements ProductView {
    private static Logger log = LoggerFactory.getLogger(ProductViewImpl.class.getName());
    private boolean salesVolumeMode = false;
    private List<ListItem> salesVolume = null;
    private Double salesVolumeQuantity = 0d;
    private Double salesVolumePriceAverage = 0d;
    private boolean stockReportMode = false;
    private String stockReportExit = null;
    private List<StockReportDisplay> stockReport = null;
    private DmsDocument datasheet = null;
    private DmsDocument picture = null;
    private boolean productReferenced = false;
    private BusinessNote stickyNote = null;

    private List<DmsDocument> docs = new ArrayList<DmsDocument>();

    protected ProductViewImpl() {
        super();
        setActionTarget(Actions.DISPLAY);
        setListTarget(Actions.LIST);
        setForwardTarget(Actions.PRODUCT);
        setCreateBackupWhenExisting(true);
    }

    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        super.init(request);
        Long id = RequestUtil.fetchId(request);
        if (id != null) {
            ProductManager manager = getProductManager();
            Product selected = manager.get(id);
            setBean(selected);
            Long stockId = RequestUtil.fetchLong(request, "stockId");
            if (stockId != null) {
                selectStock(stockId);
            } else {
                if (selected.getDefaultStock() == null) {
                    log.info("init() selected product has no default stock [product=" + selected.getProductId() + "]");
                }
                selectStock(selected.getDefaultStock());
            }
            loadDocuments(selected);
            loadDatasheet(selected);
            loadPicture(selected);
            loadStickyNote(selected);
            String exitTarget = RequestUtil.getExit(request);
            if (isSet(exitTarget)) {
                setExitTarget(exitTarget);
            }
            String exitId = RequestUtil.getExitId(request);
            if (isSet(exitId)) {
                setExitId(exitId);
            }
            if (log.isDebugEnabled()) {
                log.debug("init() view created [product=" + id
                        + ", stock=" + (getSelectedStock() == null ? "null" : getSelectedStock().getId())
                        + ", exitTarget=" + exitTarget
                        + ", exitId=" + exitId
                        + "]");
            }
        }
    }

    public void load(Product obj, String exitTarget, String exitId) {
        Product product = getProductManager().get(obj.getProductId());
        selectStock(product.getDefaultStock());
        loadDocuments(product);
        loadDatasheet(product);
        loadPicture(product);
        loadStickyNote(product);
        setBean(product);
        if (exitTarget != null) {
            setExitTarget(exitTarget);
        }
        if (exitId != null) {
            setExitId(exitId);
        }
        if (log.isDebugEnabled()) {
            log.debug("load() done [product=" + product.getProductId() +
                    ", exit=" + exitTarget + "]");
        }
    }

    @Override
    public void reload() {
        load(getProduct(), getExitTarget(), getExitId());
    }

    public void updateClassification(
            ProductTypeConfig type,
            ProductGroupConfig group,
            ProductCategoryConfig category) throws ClientException {

        Product product = getProduct();
        if (product != null) {
            ProductManager manager = getProductManager();
            if (log.isDebugEnabled()) {
                log.debug("updateClassification() invoked [product="
                        + product.getProductId() + ", type="
                        + (type == null ? "null" : type.getId()) + ", group="
                        + (group == null ? "null" : group.getId()) + ", category="
                        + (category == null ? "null" : category.getId()) + "]");
            }
            Product updated = manager.updateClassification(product, type, group, category);
            setBean(updated);
        }
    }

    public void removeType(ProductType type) throws ClientException {
        Product product = getProduct();
        if (product != null) {
            ProductManager manager = getProductManager();
            if (log.isDebugEnabled()) {
                log.debug("removeType() invoked [product=" + product.getProductId()
                        + ", type=" + (type == null ? "null" : type.getId()) + "]");
            }
            Product updated = manager.removeType(product, type);
            setBean(updated);
        }
    }

    public void updateDocumentFlag(Long docType, boolean available) throws ClientException, PermissionException {
        Product vo = getProduct();
        ProductManager manager = getProductManager();
        manager.updateDocumentFlag(vo, docType, available);
    }

    public void changeSerialnumberFlag() throws ClientException {
        Product vo = getProduct();
        if (vo.isSerialAvailable()) {
            vo.changeSerialFlags(false, false);
        } else {
            vo.changeSerialFlags(true, true);
        }
        ProductManager manager = getProductManager();
        manager.update(vo);
    }

    public boolean isProductReferenced() {
        return productReferenced;
    }

    @Override
    public void setSelection(Long id) {
        if (log.isDebugEnabled()) {
            log.debug("setSelection() invoked [id=" + id + "]");
        }
        if (id == null) {
            setBean(null);
        } else {
            ProductManager manager = getProductManager();
            Product selected = manager.get(id);
            setBean(selected);
            loadDatasheet(selected);
        }
        if (log.isDebugEnabled()) {
            log.debug("setSelection() done [id=" + id + "]");
        }
    }

    public Product getProduct() {
        return getBean() == null ? null : (Product) getBean();
    }

    public boolean isSalesVolumeMode() {
        return salesVolumeMode;
    }

    public void setSalesVolumeMode(boolean enable) {
        if (enable) {
            salesVolume = getSalesInvoiceManager().findProductListing(
                    getProduct());
            int cnt = salesVolume.size();
            salesVolumeQuantity = Constants.DOUBLE_NULL;
            salesVolumePriceAverage = Constants.DOUBLE_NULL;
            if (cnt > 0) {
                for (int i = 0; i < cnt; i++) {
                    ListItem item = salesVolume.get(i);
                    salesVolumeQuantity = salesVolumeQuantity + item.getQuantity();
                    salesVolumePriceAverage = salesVolumePriceAverage + item.getPrice();
                }
                salesVolumePriceAverage = salesVolumePriceAverage / cnt;
            }
            salesVolumeMode = true;
            stockReportMode = false;
        } else {
            salesVolume = null;
            salesVolumeMode = false;
            salesVolumeQuantity = Constants.DOUBLE_NULL;
            salesVolumePriceAverage = Constants.DOUBLE_NULL;
        }
    }

    public List<ListItem> getSalesVolume() {
        return salesVolume;
    }

    public Double getSalesVolumePriceAverage() {
        return salesVolumePriceAverage;
    }

    public Double getSalesVolumeQuantity() {
        return salesVolumeQuantity;
    }

    public boolean isStockReportMode() {
        return stockReportMode;
    }

    public void enableStockReport(String exitTarget) throws ClientException {
        if (getSelectedStock() == null) {
            throw new ClientException(ErrorCode.STOCK_SELECTION_MISSING);
        }
        stockReport = getProductManager().getStockReport(
                getSelectedStock().getId(),
                getProduct().getProductId());
        stockReportMode = true;
        setSalesVolumeMode(false);
        stockReportExit = exitTarget == null ? Actions.DISPLAY : exitTarget;
    }

    public String disableStockReport() {
        stockReport = null;
        stockReportMode = false;
        return stockReportExit;
    }

    public List<StockReportDisplay> getStockReport() {
        return stockReport;
    }

    public DmsDocument getDatasheet() {
        return datasheet;
    }

    public DmsDocument getPicture() {
        return picture;
    }

    public List<SerialDisplay> getSerials() {
        if (getBean() == null) {
            return new ArrayList<SerialDisplay>();
        }
        //Product product = getProduct();
        //return getManager().getSerialDisplay(product.getProductId();
        return null;
    }

    public BusinessNote getStickyNote() {
        return stickyNote;
    }

    protected void setStickyNote(BusinessNote stickyNote) {
        this.stickyNote = stickyNote;
    }

    public void deleteSerial(Long id, boolean out) {
        if (out) {

        } else {

        }
    }

    @Override
    public void selectStock(Long id) {
        if (log.isDebugEnabled()) {
            log.debug("selectStock() invoked [id=" + id + "]");
        }
        super.selectStock(id);
        if (getBean() != null) {
            Product product = getProduct();
            product.enableStock(id);
            try {
                RecordSearch recordSearch = (RecordSearch) getService(RecordSearch.class.getName());
                productReferenced = recordSearch.isProductReferenced(product.getProductId());
            } catch (Exception e) {
                log.error("selectStock() caught ignored exception on attempt to update productReferenced flag [product="
                        + product.getProductId()
                        + ", name=" + product.getName()
                        + ", message=" + e.getMessage(), e);
            }
        }
    }

    @Override
    protected String getSelectPropertyName() {
        return "productId";
    }

    private SalesInvoiceManager getSalesInvoiceManager() {
        return (SalesInvoiceManager) getService(SalesInvoiceManager.class.getName());
    }

    private void loadDocuments(Product product) {
        if (product.isDatasheetAvailable()) {
            docs = getDmsManager().findByReference(new DmsReference(Product.DATASHEET, product.getProductId()));
        }
    }

    public List<DmsDocument> getDocs() {
        return docs;
    }

    private void loadDatasheet(Product product) {
        if (product.isDatasheetAvailable()) {
            List<DmsDocument> list = getDmsManager().findByReference(new DmsReference(Product.DATASHEET, product.getProductId()));
            if (!list.isEmpty()) {
                datasheet = list.get(0);
            }
        }
    }

    private void loadPicture(Product product) {
        if (product.isPictureAvailable()) {
            List<DmsDocument> list = getDmsManager().findByReference(new DmsReference(Product.PICTURE, product.getProductId()));
            if (!list.isEmpty()) {
                picture = list.get(0);
            }
        }
    }

    private void loadStickyNote(Product product) {
        ProductNoteManager noteManager = (ProductNoteManager) getService(ProductNoteManager.class.getName());
        stickyNote = noteManager.getSticky(product.getProductId());
    }
}
