/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 04-Nov-2006 09:52:09 
 * 
 */
package com.osserp.gui.client.sales;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionException;
import com.osserp.common.web.ContextUtil;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.ViewManager;

import com.osserp.core.BusinessCase;
import com.osserp.core.CoreServiceProvider;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordSearch;
import com.osserp.core.requests.Request;
import com.osserp.core.requests.RequestSearch;
import com.osserp.core.sales.Sales;
import com.osserp.core.sales.SalesSearch;
import com.osserp.gui.BusinessCaseView;
import com.osserp.gui.client.requests.RequestView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesUtil {
    private static Logger log = LoggerFactory.getLogger(SalesUtil.class.getName());

    public static BusinessCase load(HttpServletRequest request, Long id) throws ClientException {
        RequestSearch rs = (RequestSearch) getService(request, RequestSearch.class.getName());
        if (rs.exists(id)) {
            return rs.findById(id);
        }
        try {
            return loadSales(request, id);
        } catch (ActionException e) {
            throw new ClientException(ErrorCode.SEARCH);
        }
    }

    public static BusinessCase loadLatest(HttpServletRequest request, Long id) throws ClientException {
        SalesSearch salesSearch = (SalesSearch) getService(request, SalesSearch.class.getName());
        if (salesSearch.exists(id)) {
            return salesSearch.fetchSales(id);
        }
        try {
            return loadRequest(request, id);
        } catch (ActionException e) {
            throw new ClientException(ErrorCode.SEARCH);
        }
    }

    public static Request loadRequest(HttpServletRequest request, Long id) {
        try {
            RequestSearch rs = (RequestSearch) CoreServiceProvider.getService(
                    request, RequestSearch.class.getName());
            return rs.findById(id);
        } catch (ClientException e) {
            throw new ActionException("no such request exists: " + id);
        }
    }

    public static Sales fetchSales(HttpServletRequest request, Long id) {
        SalesSearch salesSearch = (SalesSearch) getService(request, SalesSearch.class.getName());
        try {
            return salesSearch.fetchSales(id);
        } catch (ClientException e) {
            RecordSearch recordSearch = (RecordSearch) getService(request, RecordSearch.class.getName());
            Record record = recordSearch.findSales(id);
            if (record != null) {
                BusinessCase bc = salesSearch.findBusinessCase(record);
                if (bc != null && bc instanceof Sales) {
                    return (Sales) bc;
                }
            }
            log.warn("loadSales() failed, no such sales exists [id=" + id + "]");
            return null;
        }
    }

    public static Sales loadSales(HttpServletRequest request, Long id) {
        Sales sales = fetchSales(request, id);
        if (sales == null) {
            throw new ActionException(ActionException.INVALID_PRIMARY_KEY);
        }
        return sales;
    }

    public static BusinessCaseView createBusinessCaseView(
            ServletContext ctx,
            HttpServletRequest httpRequest,
            Object obj)
            throws ClientException, PermissionException {

        if (obj instanceof Long) {
            BusinessCase bc = load(httpRequest, (Long) obj);
            if (bc instanceof Request) {
                return createRequestView(ctx, httpRequest, (Request) bc);
            }
            return createSalesView(ctx, httpRequest, (Sales) bc);

        }
        if (obj instanceof Request) {
            return createRequestView(ctx, httpRequest, (Request) obj);
        }
        if (obj instanceof Sales) {
            return createSalesView(ctx, httpRequest, (Sales) obj);
        }
        log.warn("createBusinessCaseView() failed, invalid object [" + obj.getClass().getName() + "]");
        throw new ActionException();
    }

    public static RequestView createRequestView(
            ServletContext ctx,
            HttpServletRequest httpRequest,
            Request request)
            throws ClientException, PermissionException {
        RequestView view = (RequestView) ViewManager.newInstance(ctx).createView(httpRequest, RequestView.class.getName());
        view.init(request, RequestUtil.getExit(httpRequest));
        return view;
    }

    public static SalesView createSalesView(
            ServletContext ctx,
            HttpServletRequest httpRequest,
            Sales sales)
            throws ClientException, PermissionException {
        SalesView view = (SalesView) ViewManager.newInstance(ctx).createView(httpRequest, SalesView.class.getName());
        view.init(sales, RequestUtil.getExit(httpRequest));
        return view;
    }

    public static BusinessCaseView fetchBusinessCaseView(HttpSession session) {
        return (BusinessCaseView) ViewManager.fetchView(session, BusinessCaseView.class);
    }

    public static BusinessCaseView getBusinessCaseView(HttpSession session) {
        BusinessCaseView view = (BusinessCaseView) ViewManager.fetchView(session, BusinessCaseView.class);
        if (view == null) {
            log.warn("getBusinessCaseView() failed, view not bound!");
            throw new ActionException();
        }
        return view;
    }

    /**
     * Indicates that the action is invoked in sale context and sale view is available
     * @param view
     * @return true if so
     */
    public static boolean isSaleContext(BusinessCaseView view) {
        if (view != null) {
            return (view instanceof SalesView);
        }
        return false;
    }

    /**
     * Casts existing view to sales view.
     * @param view
     * @return salesView
     */
    public static SalesView getSaleView(BusinessCaseView view) {
        if (view == null) {
            throw new ActionException();
        }
        try {
            return (SalesView) view;
        } catch (ClassCastException e) {
            throw new ActionException("Instance of SaleView expected, found "
                    + view.getClass().getName());
        }
    }

    public static SalesBillingView fetchBillingView(HttpSession session) {
        return (SalesBillingView) ViewManager.fetchView(session, SalesBillingView.class);
    }

    /**
     * Gets billing view. Throws runtime exception if view not bound
     * @param session
     * @return view
     */
    public static SalesBillingView getBillingView(HttpSession session) {
        SalesBillingView view = fetchBillingView(session);
        if (view == null) {
            log.warn("getBillingView() failed, no view bound!");
            throw new ActionException();
        }
        return view;
    }

    public static void removeBillingView(HttpSession session) {
        ViewManager.removeView(session, SalesBillingView.class);
    }

    public static SalesBillingView createBilling(
            ServletContext ctx,
            HttpServletRequest request)
            throws ClientException, PermissionException {
        SalesBillingView view = (SalesBillingView) ViewManager.newInstance(ctx).createView(
                request,
                SalesBillingView.class.getName());
        return view;
    }

    public static SalesBillingView createBillingIfNotExists(
            ServletContext ctx,
            HttpServletRequest request)
            throws ClientException, PermissionException {
        HttpSession session = request.getSession(true);
        SalesBillingView view = fetchBillingView(session);
        if (view == null) {
            view = (SalesBillingView) ViewManager.newInstance(ctx).createView(request, SalesBillingView.class.getName());
        }
        return view;
    }

    protected static Object getService(HttpServletRequest request, String name) {
        return ContextUtil.getService(request, name);
    }
}
