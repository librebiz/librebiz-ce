<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<style type="text/css">
.date {
    text-align: left;
}
</style>

<v:form id="requestCreatorForm" url="${view.baseLink}/save">

    <div class="row">

        <div class="col-md-6 panel-area panel-area-default">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>
                        <fmt:message key="${view.headerName}" />
                    </h4>
                </div>
            </div>
            <div class="panel-body">
                <div class="form-body">

                    <c:if test="${!empty view.bean.type and view.bean.type.externalIdProvided}">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for=""><fmt:message key="businessCaseIdLabel" /></label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <v:text name="businessCaseId" styleClass="form-control" value="${view.businessCaseId}" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:if>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for=""><fmt:message key="requestType" /></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <div class="form-value">
                                    <c:choose>
                                        <c:when test="${!empty view.bean.type and !view.typeSelectionEnabled}">
                                            <o:out value="${view.bean.type.name}" />
                                        </c:when>
                                        <c:otherwise>
                                            <v:link url="${view.baseLink}/selectType" styleClass="boldblue">
                                                <o:out value="${view.bean.type.name}" />
                                            </v:link>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for=""><fmt:message key="salesBy" /></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <c:choose>
                                    <c:when test="${!empty view.bean.salesId && view.bean.salesAssignmentStrict}">
                                        <%-- sales is selected by selector with strict assignment (changing sales not provided on create) --%>
                                        <div class="form-value">
                                            <v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${view.bean.salesId}">
                                                <oc:employee value="${view.bean.salesId}" />
                                            </v:ajaxLink>
                                            <input type="hidden" name="salesId" value="${view.bean.salesId}" />
                                        </div>
                                    </c:when>
                                    <c:otherwise>
                                        <select name="salesId" class="form-control" size="1" onchange="enableUpdateOnly(); sendForm();">
                                            <c:if test="${empty view.bean.salesId}">
                                                <%-- sales is not selected by selector: user can change sales (no strict assignment) --%>
                                                <option value=""><fmt:message key="promptSelect" />:
                                                </option>
                                                <option value="<o:out value="${view.employee.id}"/>" selected="selected"><o:out value="${view.employee.displayName}" /></option>
                                            </c:if>
                                            <c:forEach var="option" items="${view.salesStaff}">
                                                <c:choose>
                                                    <c:when test="${option.id == view.bean.salesId}">
                                                        <option value="<o:out value="${option.id}"/>" selected="selected"><o:out value="${option.displayName}" /></option>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <option value="<o:out value="${option.id}"/>"><o:out value="${option.displayName}" /></option>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:forEach>
                                        </select>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                    <c:if test="${!empty view.bean.salesId && !empty view.bean.salesAssignment}">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for=""><fmt:message key="method" /></label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <div class="form-value">
                                        <fmt:message key="${view.bean.salesAssignment}" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:if>

                    <c:if test="${!empty view.bean and view.bean.type.wholeSale}">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for=""><fmt:message key="coSales" /></label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <select name="salesCoId" class="form-control" size="1" onchange="enableUpdateOnly(); sendForm();">
                                        <option value=""><fmt:message key="selectIfNecessary" />:
                                        </option>
                                        <c:forEach var="option" items="${view.salesStaff}">
                                            <option value="<o:out value="${option.id}"/>"><o:out value="${option.displayName}" /></option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </c:if>
                    <%-- 
                    <v:date label="requestFrom" name="salesTalkDate" styleClass="form-control" picker="true" autoupdate="true" />
                    --%>
                    <v:date label="requestFrom" name="date1" value="${view.date1}" styleClass="form-control" picker="true" autoupdate="true" />
                    <c:choose>
                        <c:when test="${view.bean.type.directSales}">
                            <div class="row next">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for=""><fmt:message key="requestPer" /></label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <oc:select name="originTypeId" styleClass="form-control" options="requestOriginTypes" value="${view.bean.originType}" autoupdate="true" />
                                    </div>
                                </div>
                            </div>
                            <v:date label="orderDate" name="date2" value="${view.date2}" styleClass="form-control" picker="true" autoupdate="true" />
                        </c:when>
                        <c:otherwise>
                            <v:date label="reactionUntil" name="date2" value="${view.date2}" styleClass="form-control" picker="true" autoupdate="true" />
                            <div class="row next">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for=""><fmt:message key="requestPer" /></label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <oc:select name="originTypeId" styleClass="form-control" options="requestOriginTypes" value="${view.bean.originType}" autoupdate="true" />
                                    </div>
                                </div>
                            </div>
                        </c:otherwise>
                    </c:choose>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for=""><fmt:message key="origin" /></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <div class="form-value">
                                    <c:choose>
                                        <c:when test="${empty view.bean.origin}">
                                            <v:ajaxLink linkId="campaignSelectionView" url="/crm/campaignSelection/forward?view=${view.name}&company=${view.bean.type.company}">
                                                <fmt:message key="promptSelect" />
                                            </v:ajaxLink>
                                        </c:when>
                                        <c:otherwise>
                                            <v:ajaxLink linkId="campaignSelectionView" url="/crm/campaignSelection/forward?view=${view.name}&company=${view.bean.type.company}&id=${view.bean.origin}">
                                                <oc:options name="campaigns" value="${view.bean.origin}" />
                                            </v:ajaxLink>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </div>
                        </div>
                    </div>
                    <c:if test="${!view.bean.type.interestContext}">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for=""> </label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <div class="form-value">
                                        <fmt:message key="deliveryAddress" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for=""><fmt:message key="nameIfDeviant" /></label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <v:text name="name" value="${view.bean.address.name}" styleClass="form-control" autoupdate="${view.addressAutocompletionSupport}" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for=""><fmt:message key="street" /></label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <v:text name="street" value="${view.bean.address.street}" styleClass="form-control" autoupdate="${view.addressAutocompletionSupport}" />
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="zipcodeAndCity"> <fmt:message key="zipCodeCity" />
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <v:text name="zipcode" styleClass="form-control" autoupdate="${view.addressAutocompletionSupport}" value="${view.bean.address.zipcode}" objectpath="address.zipcode" />
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <v:text name="city" styleClass="form-control" value="${view.bean.address.city}" objectpath="address.city" />
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for=""><fmt:message key="country" /></label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <oc:select name="country" value="${view.bean.address.country}" options="countryNames" styleClass="form-control" prompt="false" />
                                </div>
                            </div>
                        </div>
                    </c:if>

                    <c:if test="${!empty view.bean}">
                        <div class="row next">
                            <div class="col-md-4"></div>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <c:choose>
                                                <c:when test="${view.typeSelectionEnabled}">
                                                    <v:submitExit url="${view.baseLink}/selectType" />
                                                </c:when>
                                                <c:otherwise>
                                                    <v:submitExit url="${view.baseLink}/exit" />
                                                </c:otherwise>
                                            </c:choose>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <o:submit value="create" styleClass="form-control" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:if>

                </div>
            </div>
        </div>

    </div>
</v:form>
