<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>

<c:set var="recordView" scope="session" value="${sessionScope.purchaseInvoiceView}"/>
<c:set var="record" value="${recordView.record}"/>

<div class="subcolumns">
    <%-- Left column --%>
    <div class="column50l">
        <div class="subcolumnl">

            <div class="spacer"></div>
            
            <c:choose>
            <c:when test="${recordView.deleteClosedMode}">
            
            <div class="contentBox">
                <div class="contentBoxHeader" style="width: 98%;">
                    <div class="contentBoxHeaderLeft">
                        <fmt:message key="confirmRecordDeleteLabel"/>
                    </div>
                </div>

                <div class="contentBoxData">
                    <div class="subcolumns">
                        <div class="subcolumn">
                            <div class="spacer"></div>
                            
                            <c:if test="${record.unchangeable}">
                            <o:permission role="executive,purchase_invoice_delete" info="permissionPurchaseInvoiceDeleteClosed">
                            <div class="recordLabelValue">
                                <o:form name="recordForm" url="/purchaseInvoice.do">
                                    <input type="hidden" name="method" value="deleteClosed" />
                                    <input type="text" name="note" value="" style="margin-left: 10px; margin-bottom: 2px; width: 280px;"/>
                                    <a href="<c:url value="/purchaseInvoice.do"><c:param name="method" value="disableDeleteClosedMode"/></c:url>"><img src="<c:url value="${applicationScope.backIcon}"/>" class="bigicon" style="margin-left: 8px;" /></a>
                                    <input type="image" name="method" src="<c:url value="${applicationScope.deleteIcon}"/>" value="deleteClosed" onclick="setMethod('deleteClosed');" class="bigicon" style="margin-left: 8px;" />
                                </o:form>
                            </div>
                            <c:set var="hadSomethingToDisplay" value="true"/>
                            </o:permission>
                            </c:if>
                            <c:if test="${!hadSomethingToDisplay}">
                            <div class="recordLabelValue">
                                <fmt:message key="noActionsAvailableOnCurrentPermissionState"/>
                            </div>
                            </c:if>

                            <div class="spacer"></div>
                        </div>
                    </div>
                </div>
            </div>
            </c:when>
            <c:otherwise>
            <div class="contentBox">
                <div class="contentBoxHeader" style="width: 98%;">
                    <div class="contentBoxHeaderLeft">
                        <fmt:message key="actions"/>
                    </div>
                </div>

                <div class="contentBoxData">
                    <div class="subcolumns">
                        <div class="subcolumn">
                            <div class="spacer"></div>

                            <c:if test="${record.unchangeable}">
                            <o:permission role="executive,purchase_invoice_change" info="permissionReopenClosedInvoice">
                            <div class="recordLabelValue">
                                <a href="<c:url value="/purchaseInvoice.do?method=openClosed"/>"><fmt:message key="reopenClosedInvoice"/></a>
                            </div>
                            <c:set var="hadSomethingToDisplay" value="true"/>
                            </o:permission>
                            <o:permission role="executive,purchase_invoice_change,accounting" info="permissionChangeSupplierReferenceNumber">
                            <div class="recordLabelValue">
                                <a href="<c:url value="/purchaseInvoice.do?method=enableSupplierReferenceNumberEditMode"/>"><fmt:message key="supplierReferenceNumberChange"/></a>
                            </div>
                            <c:set var="hadSomethingToDisplay" value="true"/>
                            </o:permission>
                            <o:permission role="executive,purchase_invoice_delete" info="permissionEnableDeleteClosedMode">
                            <div class="recordLabelValue">
                                <a href="<c:url value="/purchaseInvoice.do?method=enableDeleteClosedMode"/>"><fmt:message key="irrevocableRecordDeleteAction"/></a>
                            </div>
                            <c:set var="hadSomethingToDisplay" value="true"/>
                            </o:permission>
                            </c:if>
                            <c:if test="${!hadSomethingToDisplay}">
                            <div class="recordLabelValue">
                                <fmt:message key="noActionsAvailableOnCurrentRecordState"/>
                            </div>
                            </c:if>

                            <div class="spacer"></div>
                        </div>
                    </div>
                </div>
            </div>
            </c:otherwise>
            </c:choose>
            
            <div class="spacer"></div>
        </div>
    </div>

    <%-- Right column 
    <div class="column50r">
        <div class="subcolumnr">
            <div class="spacer"></div>
            <div class="contentBox">
                <div class="contentBoxHeader">
                    <div class="contentBoxHeaderLeft">
                                                
                    </div>
                </div>
                <div class="contentBoxData">
                    <div class="subcolumns">
                        <div class="subcolumn">
                            <div class="spacer"></div>
                            
                            <div class="spacer"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="spacer"></div>
        </div>
    </div>
    End right column --%>
</div>
