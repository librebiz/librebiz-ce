<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${requestScope.view}"/>
<c:set var="product" value="${view.product}"/>
<c:set var="datasheetUploadPermissionsGrant" value="false"/>
<c:if test="${requestScope.isPopup}">
    <c:remove var="view" scope="request"/>
</c:if>
<div class="content-area" id="productSalesListContent">
    <div class="row">

        <div class="col-md-8 panel-area panel-area-default">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>
                        <span<c:if test="${product.endOfLife}"> class="canceled"</c:if>>
                            <o:out value="${product.productId}"/>
                            <o:out value="${product.name}"/>
                        </span>
                    </h4>
                </div>
            </div>
            <div class="panel-body panel-left-right">
                <c:if test="${!empty product.matchcode}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="matchcode"/>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <o:out value="${product.matchcode}"/>
                            </div>
                        </div>
                    </div>
                </c:if>
                <c:if test="${!empty product.manufacturer}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="manufacturer"/>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <oc:options name="productManufacturers" value="${product.manufacturer}"/>
                            </div>
                        </div>
                    </div>
                </c:if>
                <c:if test="${!empty product.types}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="typesLabel"/>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <c:forEach var="type" items="${product.types}" varStatus="s">
                                    <c:choose>
                                        <c:when test="${s.index == 0}">
                                            <o:out value="${type.name}"/>
                                        </c:when>
                                        <c:otherwise>
                                            <br /><o:out value="${type.name}"/>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                </c:if>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <c:choose>
                                <c:when test="${requestScope.isPopup}">
                                    <fmt:message key="group"/>
                                </c:when>
                                <c:otherwise>
                                    <o:permission role="product_create,product_edit,executive" info="permissionProductClassificationEdit">
                                        <v:link url="/products/productClassification/forward?exit=/products.do?method=redisplay" title="productClassificationEdit">
                                            <fmt:message key="group"/>
                                        </v:link>
                                    </o:permission>
                                    <o:forbidden role="product_create,product_edit,executive">
                                        <fmt:message key="group"/>
                                    </o:forbidden>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${product.group.name}"/>
                        </div>
                    </div>
                </div>

                <c:if test="${!empty product.category}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="category"/>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <o:out value="${product.category.name}"/>
                            </div>
                        </div>
                    </div>
                </c:if>
                <c:if test="${product.power > 0}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="power"/>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <o:number value="${product.power}" format="decimal"/>
                                <oc:options name="quantityUnits" value="${product.powerUnit}"/>
                            </div>
                        </div>
                    </div>
                </c:if>
                <c:if test="${product.details.measurementsAvailable}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="measurements"/>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <fmt:message key="length"/>/<fmt:message key="width"/>/<fmt:message key="depth"/>:
                                <o:out value="${product.details.width}"/>
                                x
                                <o:out value="${product.details.height}"/>
                                x
                                <o:out value="${product.details.depth}"/>
                                <o:out value="${product.details.measurementUnit}"/>
                            </div>
                        </div>
                    </div>
                </c:if>
                <c:if test="${product.details.weight > 0}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="weight"/>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <o:number value="${product.details.weight}" format="currency"/>
                                <o:out value="${product.details.weightUnit}"/>
                            </div>
                        </div>
                    </div>
                </c:if>
                <c:if test="${product.details.color > 0}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="color"/>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <oc:options name="productColors" value="${product.details.color}"/>
                            </div>
                        </div>
                    </div>
                </c:if>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <c:choose>
                                <c:when test="${requestScope.isPopup}">
                                    <fmt:message key="description"/>
                                </c:when>
                                <c:otherwise>
                                    <o:permission role="product_description_edit,executive" info="permissionProductDescriptionEdit">
                                        <v:ajaxLink url="/products/productDescription/forward" linkId="productDescriptionConfig">
                                            <fmt:message key="description"/>
                                        </v:ajaxLink>
                                    </o:permission>
                                    <o:forbidden role="product_description_edit,executive">
                                        <fmt:message key="description"/>
                                    </o:forbidden>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:choose>
                                <c:when test="${empty product.description}"><fmt:message key="none"/></c:when>
                                <c:otherwise><o:textOut value="${product.description}"/></c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>
                <c:if test="${!empty product.descriptionShort}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="descriptionShort"/>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <o:textOut value="${product.descriptionShort}"/>
                            </div>
                        </div>
                    </div>
                </c:if>
                <c:if test="${!empty product.marketingText}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="marketingText"/>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <o:textOut value="${product.marketingText}"/>
                            </div>
                        </div>
                    </div>
                </c:if>
                <c:if test="${!empty product.purchaseText}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="purchaseText"/>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <o:out value="${product.purchaseText}"/>
                            </div>
                        </div>
                    </div>
                </c:if>
                <c:if test="${product.group.gtin13Available && !empty product.gtin13}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="gtin13"/>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <o:out value="${product.gtin13}"/>
                            </div>
                        </div>
                    </div>
                </c:if>
                <c:if test="${product.group.gtin14Available and !empty product.gtin14}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="gtin14"/>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <o:out value="${product.gtin14}"/>
                            </div>
                        </div>
                    </div>
                </c:if>
                <c:if test="${product.group.gtin8Available and !empty product.gtin8}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="gtin8"/>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <o:out value="${product.gtin8}"/>
                            </div>
                        </div>
                    </div>
                </c:if>
                <c:if test="${product.group.providingDatasheet or product.group.providingPicture}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="documents"/>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <c:if test="${product.group.providingPicture}">
                                    <fmt:message key="picture"/>:
                                    <c:choose>
                                        <c:when test="${product.pictureAvailable}">
                                            <a href="javascript:;" onclick="window.open('<oc:img value="${view.picture}"/>', 'picture', 'width=480,height=600');" title="<fmt:message key="viewImage"/>"><o:img name="printIcon"/></a>
                                            <o:permission role="product_details_edit,product_datasheet_upload,product_datasheet_upload_admin" info="permissionProductDetailsEdit">
                                                <c:if test="${!requestScope.isPopup}"><v:link url="/products/productPicture/forward?exit=/products.do?method=reload" title="changePicture"><o:img name="uploadIcon"/></v:link></c:if>
                                            </o:permission>
                                        </c:when>
                                        <c:otherwise>
                                            <fmt:message key="non"></fmt:message>
                                            <o:permission role="product_details_edit,product_datasheet_upload,product_datasheet_upload_admin" info="permissionProductDetailsEdit">
                                                <c:if test="${!requestScope.isPopup}"><v:link url="/products/productPicture/forward?exit=/products.do?method=reload" title="addPicture"><o:img name="uploadIcon"/></v:link></c:if>
                                            </o:permission>
                                        </c:otherwise>
                                    </c:choose>
                                </c:if>
                                <c:if test="${product.group.providingDatasheet}">
                                    <fmt:message key="datasheet"/>:
                                    <c:choose>
                                        <c:when test="${product.datasheetAvailable}">
                                            <a href="javascript:;" onclick="window.open('<oc:img value="${view.datasheet}"/>', 'datasheet', 'width=480,height=600');" title="<fmt:message key="viewDatasheet"/>"><o:img name="printIcon"/></a>
                                            <o:permission role="product_details_edit,product_datasheet_upload,product_datasheet_upload_admin" info="permissionProductDetailsEdit">
                                                <c:if test="${!requestScope.isPopup}"><v:link url="/products/productDatasheet/forward?exit=/products.do?method=reload" title="changeDatasheet"><o:img name="uploadIcon"/></v:link></c:if>
                                            </o:permission>
                                        </c:when>
                                        <c:otherwise>
                                            <fmt:message key="non"></fmt:message>
                                            <o:permission role="product_details_edit,product_datasheet_upload,product_datasheet_upload_admin" info="permissionProductDetailsEdit">
                                                <c:if test="${!requestScope.isPopup}"><v:link url="/products/productDatasheet/forward?exit=/products.do?method=reload" title="addDatasheet"><o:img name="uploadIcon"/></v:link></c:if>
                                            </o:permission>
                                        </c:otherwise>
                                    </c:choose>
                                </c:if>
                            </div>
                        </div>
                    </div>
                </c:if>
                <c:choose>
                    <c:when test="${!product.detailAvailable}">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <c:choose>
                                        <c:when test="${requestScope.isPopup}">
                                            <fmt:message key="detail"/>
                                        </c:when>
                                        <c:otherwise>
                                            <v:link url="/products/productDetails/forward?exit=/products.do?method=redisplay" title="settings">
                                                <fmt:message key="detail"/>
                                            </v:link>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <fmt:message key="none"/>
                                </div>
                            </div>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <c:choose>
                                        <c:when test="${requestScope.isPopup}">
                                            <fmt:message key="detail"/>
                                        </c:when>
                                        <c:otherwise>
                                            <v:link url="/products/productDetails/forward?exit=/products.do?method=redisplay" title="settings">
                                                <fmt:message key="detail"/>
                                            </v:link>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </div>
                        </div>
                        <c:forEach var="prop" items="${product.propertyList}" varStatus="s">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <fmt:message key="${prop.name}"/>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <o:out value="${prop.value}"/>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
                <c:if test="${!empty view.stickyNote}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="info"/>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <o:out value="${view.stickyNote.note}"/>
                            </div>
                        </div>
                    </div>
                </c:if>

            </div>
        </div>

        <div class="col-md-4 panel-area panel-area-default">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-4">
                            <c:choose>
                                <c:when test="${requestScope.isPopup}">
                                    <h4><fmt:message key="prices"/></h4>
                                </c:when>
                                <c:otherwise>
                                    <o:permission role="product_price_edit,executive" info="permissionEditPrice">
                                        <c:choose>
                                            <c:when test="${product.priceByQuantity}">
                                                <v:link url="/products/productPriceByQuantity/forward?exit=/products.do?method=redisplay"><fmt:message key="prices"/></v:link>
                                            </c:when>
                                            <c:otherwise>
                                                <v:link url="/products/productPrice/forward?exit=/products.do?method=redisplay"><fmt:message key="prices"/></v:link>
                                            </c:otherwise>
                                        </c:choose>
                                    </o:permission>
                                    <o:forbidden role="product_price_edit,executive">
                                        <h4><fmt:message key="prices"/></h4>
                                    </o:forbidden>
                                </c:otherwise>
                            </c:choose>
                        </div>
                        <div class="col-md-8 right">
                            <c:if test="${!requestScope.isPopup}">
                                <o:permission role="product_price_edit,product_sales_volume,executive" info="permissionShowTurnover">
                                    <a href="<c:url value="/products.do?method=enableSalesVolumeDisplay"/>"><fmt:message key="turnover"/></a>
                                </o:permission>
                            </c:if>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body">

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="sale"/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group form-value-right">
                            <o:number value="${product.consumerPrice}" format="currency"/>&nbsp;<fmt:message key="euro"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="trading"/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group form-value-right">
                            <o:number value="${product.resellerPrice}" format="currency"/>&nbsp;<fmt:message key="euro"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="partner"/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group form-value-right">
                            <o:number value="${product.partnerPrice}" format="currency"/>&nbsp;<fmt:message key="euro"/>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-md-4 panel-area panel-area-default">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-4">
                            <h4><fmt:message key="stock"/></h4>
                        </div>
                        <div class="col-md-8 right">
                            <c:choose>
                                <c:when test="${!requestScope.isPopup and product.deliveryNoteAffecting}">
                                    <a href="<c:url value="/products.do?method=enableStockReport&exit=display"/>">
                                        <oc:options name="stockKeys" value="${product.currentStock}"/><span class="tab"><o:number value="${product.summary.stock}" format="integer"/></span>
                                    </a>
                                </c:when>
                                <c:when test="${product.deliveryNoteAffecting}">
                                    <oc:options name="stockKeys" value="${product.summary.stockId}"/>
                                    <span class="tab"><o:number value="${product.summary.stock}" format="integer"/></span>
                                </c:when>
                            </c:choose>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body">

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <c:choose>
                                <c:when test="${!requestScope.isPopup and product.summary.ordered != 0}">
                                    <v:ajaxLink url="/plannings/productPlanning/forward?productId=${product.productId}&stockId=${product.currentStock}&showRecordLinks=true&exit=product" linkId="productPlanningView">
                                        <fmt:message key="orderShortcutReleased"/>
                                    </v:ajaxLink>
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="orderShortcutReleased"/>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group form-value-right">
                            <o:number value="${product.summary.ordered}" format="integer"/>
                        </div>
                    </div>
                </div>
                <c:if test="${product.summary.salesReceipt != 0}">

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group red">
                                <fmt:message key="thereofReturns"/>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group form-value-right">
                                <%-- TODO add link to display receipt
                                <c:choose>
                                    <c:when test="${!requestScope.isPopup and product.summary.ordered > 0}">
                                        <a href="<c:url value="/salesPlanning.do"><c:param name="method" value="forward"/><c:param name="unreleased" value="false"/><c:param name="exit" value="product"/><c:param name="id" value="${product.productId}"/><c:param name="stockId" value="${product.currentStock}"/></c:url>"><o:number value="${product.summary.ordered}" format="integer"/></a>
                                    </c:when>
                                    <c:otherwise>
                                        <o:number value="${product.summary.ordered}" format="integer"/>
                                    </c:otherwise>
                                </c:choose>
                                --%>
                                <o:number value="${product.summary.salesReceipt}" format="integer"/>
                            </div>
                        </div>
                    </div>
                </c:if>
                <c:if test="${product.summary.vacant != 0}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group red">
                                <c:choose>
                                    <c:when test="${!requestScope.isPopup and product.summary.vacant > 0}">
                                        <v:ajaxLink url="/plannings/productPlanning/forward?productId=${product.productId}&vacant=true&stockId=${product.currentStock}&showRecordLinks=true&exit=product" linkId="productPlanningView">
                                            <fmt:message key="orderShortcutUnreleased"/>
                                        </v:ajaxLink>
                                    </c:when>
                                    <c:otherwise>
                                        <fmt:message key="orderShortcutUnreleased"/>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group form-value-right">
                                <o:number value="${product.summary.vacant}" format="integer"/>
                            </div>
                        </div>
                    </div>
                </c:if>
                <c:if test="${product.summary.unreleased != 0}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group red">
                                <c:choose>
                                    <c:when test="${!requestScope.isPopup and product.summary.unreleased != 0}">
                                        <a href="<c:url value="/salesPlanning.do?method=forward&unreleased=true&exit=product&id=${product.productId}&stockId=${product.currentStock}"/>">
                                            <fmt:message key="withoutRelease"/>
                                        </a>
                                    </c:when>
                                    <c:otherwise>
                                        <fmt:message key="withoutRelease"/>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group form-value-right">
                                <o:number value="${product.summary.unreleased}" format="integer"/>
                            </div>
                        </div>
                    </div>
                </c:if>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <c:choose>
                                <c:when test="${!requestScope.isPopup and product.summary.expected != 0}">
                                    <a href="<c:url value="/purchaseOrder.do?method=forwardOpenByProduct&exit=product&id=${product.productId}&stockId=${product.currentStock}"/>">
                                        <fmt:message key="appointed"/>
                                    </a>
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="appointed"/>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group form-value-right">
                            <o:number value="${product.summary.expected}" format="integer"/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <c:choose>
                                <c:when test="${!requestScope.isPopup and product.summary.receipt != 0}">
                                    <a href="<c:url value="/purchaseReceipt.do?method=forward&exit=product&product=${product.productId}&stock=${product.currentStock}&readonly=true"/>">
                                        <fmt:message key="openInArrival"/>
                                    </a>
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="openInArrival"/>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group form-value-right">
                            <o:number value="${product.summary.receipt}" format="integer"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4 panel-area panel-area-default">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4><fmt:message key="properties"/></h4>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <fmt:message key="inventory"/> / <fmt:message key="kanban"/>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group form-value-right">
                            <c:choose><c:when test="${product.affectsStock}"><fmt:message key="bigYes"/></c:when><c:otherwise>-</c:otherwise></c:choose>
                            <span> / </span><c:choose><c:when test="${product.kanban}"><fmt:message key="bigYes"/></c:when><c:otherwise>-</c:otherwise></c:choose>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <fmt:message key="packagingUnit"/> / <fmt:message key="quantityUnitShort"/>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group form-value-right">
                            <o:out value="${product.packagingUnit}"/> <oc:options name="quantityUnits" value="${product.quantityUnit}"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <fmt:message key="taxRate"/>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group form-value-right">
                            <c:choose>
                                <c:when test="${product.reducedTax}"><fmt:message key="reduced"/></c:when>
                                <c:otherwise><fmt:message key="regular"/></c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <fmt:message key="serialNumberLabel"/>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group form-value-right">
                            <o:permission role="product_serialflag_change" info="productSerialFlagChange">
                                <a href="<c:url value="/products.do?method=changeSerialnumberFlags"/>">
                                    <c:choose><c:when test="${product.serialAvailable}"><fmt:message key="bigYes"/></c:when><c:otherwise>-</c:otherwise></c:choose>
                                </a>
                            </o:permission>
                            <o:forbidden value="product_serialflag_change" info="productSerialFlagChange">
                                <c:choose><c:when test="${product.serialAvailable}"><fmt:message key="bigYes"/></c:when><c:otherwise>-</c:otherwise></c:choose>
                            </o:forbidden>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <fmt:message key="textRequired"/>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group form-value-right">
                            <c:choose><c:when test="${product.billingNoteRequired}"><fmt:message key="bigYes"/></c:when><c:otherwise>-</c:otherwise></c:choose>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <fmt:message key="virtual"/> / <fmt:message key="term"/>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group form-value-right">
                            <c:choose><c:when test="${product.virtual}"><fmt:message key="bigYes"/></c:when><c:otherwise>-</c:otherwise></c:choose>
                            <span> / </span><c:choose><c:when test="${product.term}"><fmt:message key="bigYes"/></c:when><c:otherwise>-</c:otherwise></c:choose>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <fmt:message key="service"/>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group form-value-right">
                            <c:choose><c:when test="${product.service}"><fmt:message key="bigYes"/></c:when><c:otherwise>-</c:otherwise></c:choose>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <fmt:message key="bundle"/> / <fmt:message key="plant"/>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group form-value-right">
                            <c:choose><c:when test="${product.bundle}"><fmt:message key="bigYes"/></c:when><c:otherwise>-</c:otherwise></c:choose>
                            <span> / </span><c:choose><c:when test="${product.plant}"><fmt:message key="bigYes"/></c:when><c:otherwise>-</c:otherwise></c:choose>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <fmt:message key="quantityByPlant"/>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group form-value-right">
                            <c:choose><c:when test="${product.quantityByPlant}"><fmt:message key="bigYes"/></c:when><c:otherwise>-</c:otherwise></c:choose>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <fmt:message key="billingInBundlesWithNull"/>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group form-value-right">
                            <c:choose><c:when test="${product.nullable}"><fmt:message key="bigYes"/></c:when><c:otherwise>-</c:otherwise></c:choose>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <fmt:message key="allowedInRecords"/>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group form-value-right">
                            <c:choose><c:when test="${product.offerUsageOnly}"><fmt:message key="offers"/></c:when><c:otherwise><fmt:message key="all"/></c:otherwise></c:choose>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <fmt:message key="syncClientLabel"/>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group form-value-right">
                            <c:choose><c:when test="${product.publicAvailable}"><fmt:message key="bigYes"/></c:when><c:otherwise>-</c:otherwise></c:choose>
                        </div>
                    </div>
                </div>
                <%--
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <h4><fmt:message key="component"/></h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group form-value-right">
                            <c:choose><c:when test="${product.component}"><fmt:message key="bigYes"/></c:when><c:otherwise><fmt:message key="bigNo"/></c:otherwise></c:choose>
                        </div>
                    </div>
                </div>
                --%>
            </div>
        </div>

    </div>
</div>
