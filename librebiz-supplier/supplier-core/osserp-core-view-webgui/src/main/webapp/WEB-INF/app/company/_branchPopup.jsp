<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="branchPopupView"/>
<c:set var="bean" scope="page" value="${view.bean}"/>

<c:if test="${!empty view}">
	<div class="modalBoxHeader">
		<div class="modalBoxHeaderLeft">
			<fmt:message key="systemCompany"/><c:if test="${!empty bean.company}"> <o:out value="${bean.company.id}"/> - <o:out value="${bean.company.name}"/></c:if>
		</div>
		<div class="modalBoxHeaderRight">
			<div class="boxnav">
				<ul>
					<c:forEach var="nav" items="${view.navigation}">
						<o:permission role="${nav.permissions}" info="${nav.permissionInfo}">
							<li>
								<v:ajaxLink url="${nav.link}" targetElement="${view.name}_popup" title="${nav.title}"><o:img name="${nav.icon}"/></v:ajaxLink>
							</li>
						</o:permission>
					</c:forEach>
				</ul>
			</div>
		</div>
	</div>
	<div class="modalBoxData">
		<div>
			<div class="subcolumns">
				<div class="subcolumn">
					<c:if test="${!empty sessionScope.error}">
						<div class="spacer"></div>
						<div class="errormessage">
							<fmt:message key="error"/>: <fmt:message key="${sessionScope.error}"/>
						</div>
                        <o:removeErrors/>
					</c:if>
					<div class="spacer"></div>
						<c:if test="${!empty view.bean}">
							<table class="valueTable">
								<tbody>
									<tr>
										<td><fmt:message key="contact"/></td>
										<td><o:out value="${bean.contact.contactId}"/></td>
									</tr>
									<tr>
										<td> </td>
										<td><o:out value="${bean.contact.displayName}"/></td>
									</tr>
									<tr>
										<td><fmt:message key="name"/></td>
										<td><o:out value="${view.bean.name}"/></td>
									</tr>
									<tr>
										<td><fmt:message key="shortkey"/></td>
										<td><o:out value="${bean.shortkey}"/></td>
									</tr>
									<tr>
										<td><fmt:message key="shortinfo"/></td>
										<td><o:out value="${bean.shortname}"/></td>
									</tr>
									<tr>
										<td><fmt:message key="email"/></td>
										<td><o:out value="${bean.email}"/></td>
									</tr>
									<tr>
										<td><fmt:message key="phone"/></td>
										<td>
											<o:out value="${bean.phoneCountry}"/> <o:out value="${bean.phonePrefix}"/> <o:out value="${bean.phoneNumber}"/>
										</td>
									</tr>
									<tr>
										<td><fmt:message key="headquarter"/></td>
										<td>
											<c:choose>
												<c:when test="${bean.headquarter}">
													<fmt:message key="yes"/>
												</c:when>
												<c:otherwise>
													<fmt:message key="no"/>
												</c:otherwise>
											</c:choose>
										</td>
									</tr>
								</tbody>
							</table>
						</c:if>
					<div class="spacer"></div>
				</div>
			</div>
		</div>
	</div>
</c:if>