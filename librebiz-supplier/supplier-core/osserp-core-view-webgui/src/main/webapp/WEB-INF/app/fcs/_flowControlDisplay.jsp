<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="popup" value="${requestScope.popup}"/>
<c:set var="view" value="${requestScope.view}"/>
<c:set var="businessCase" value="${view.businessCase}"/>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel-body">
        <c:import url="${viewdir}/fcs/_flowControlTableTodo.jsp"/>
    </div>
</div>
<div class="col-md-6 panel-area panel-area-default">
    <div class="panel-body">
        <c:import url="${viewdir}/fcs/_flowControlTableClosed.jsp"/>
    </div>
</div>
