/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 27 2013 10:15:22 
 * 
 */
package com.osserp.core.model;

import java.util.Date;

import com.osserp.common.beans.AbstractOption;
import com.osserp.core.NoteType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class BusinessNoteTypeImpl extends AbstractOption implements NoteType {

    private String serviceName;
    private boolean confirmationSupport;
    private boolean confirmationRequired;
    private String confirmationExpectionResolver;
    private boolean supportingHeadline;
    private boolean supportingMail;
    private boolean mailRecipientsDisabled;
    private boolean mailRecipientsCCDisabled;
    private boolean mailRecipientsBCCDisabled;
    private boolean mailAttachmentsDisabled;
    private String mailOriginator;

    protected BusinessNoteTypeImpl() {
        super();
    }

    public BusinessNoteTypeImpl(
            Long id,
            Long reference,
            String name,
            Date created,
            Long createdBy,
            Date changed,
            Long changedBy,
            boolean endOfLife,
            String serviceName,
            boolean confirmationSupport,
            boolean confirmationRequired,
            String confirmationExpectionResolver,
            boolean supportingHeadline,
            boolean supportingMail,
            boolean mailRecipientsDisabled,
            boolean mailRecipientsCCDisabled,
            boolean mailRecipientsBCCDisabled,
            boolean mailAttachmentsDisabled,
            String mailOriginator) {
        super(id, reference, name, created, createdBy, changed, changedBy, endOfLife);
        this.serviceName = serviceName;
        this.confirmationSupport = confirmationSupport;
        this.confirmationRequired = confirmationRequired;
        this.confirmationExpectionResolver = confirmationExpectionResolver;
        this.supportingHeadline = supportingHeadline;
        this.supportingMail = supportingMail;
        this.mailRecipientsDisabled = mailRecipientsDisabled;
        this.mailRecipientsCCDisabled = mailRecipientsCCDisabled;
        this.mailRecipientsBCCDisabled = mailRecipientsBCCDisabled;
        this.mailAttachmentsDisabled = mailAttachmentsDisabled;
        this.mailOriginator = mailOriginator;
    }

    public BusinessNoteTypeImpl(NoteType other) {
        super(other);
        this.serviceName = other.getServiceName();
        this.confirmationSupport = other.isConfirmationSupport();
        this.confirmationRequired = other.isConfirmationRequired();
        this.confirmationExpectionResolver = other.getConfirmationExpectionResolver();
        this.supportingHeadline = other.isSupportingHeadline();
        this.supportingMail = other.isSupportingMail();
        this.mailRecipientsDisabled = other.isMailRecipientsDisabled();
        this.mailRecipientsCCDisabled = other.isMailRecipientsCCDisabled();
        this.mailRecipientsBCCDisabled = other.isMailRecipientsBCCDisabled();
        this.mailAttachmentsDisabled = other.isMailAttachmentsDisabled();
        this.mailOriginator = other.getMailOriginator();
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public boolean isConfirmationSupport() {
        return confirmationSupport;
    }

    public void setConfirmationSupport(boolean confirmationSupport) {
        this.confirmationSupport = confirmationSupport;
    }

    public boolean isConfirmationRequired() {
        return confirmationRequired;
    }

    public void setConfirmationRequired(boolean confirmationRequired) {
        this.confirmationRequired = confirmationRequired;
    }

    public String getConfirmationExpectionResolver() {
        return confirmationExpectionResolver;
    }

    public void setConfirmationExpectionResolver(String confirmationExpectionResolver) {
        this.confirmationExpectionResolver = confirmationExpectionResolver;
    }

    public boolean isSupportingHeadline() {
        return supportingHeadline;
    }

    public void setSupportingHeadline(boolean supportingHeadline) {
        this.supportingHeadline = supportingHeadline;
    }

    public boolean isSupportingMail() {
        return supportingMail;
    }

    public void setSupportingMail(boolean supportingMail) {
        this.supportingMail = supportingMail;
    }

    public boolean isMailRecipientsDisabled() {
        return mailRecipientsDisabled;
    }

    public void setMailRecipientsDisabled(boolean mailRecipientsDisabled) {
        this.mailRecipientsDisabled = mailRecipientsDisabled;
    }

    public boolean isMailRecipientsCCDisabled() {
        return mailRecipientsCCDisabled;
    }

    public void setMailRecipientsCCDisabled(boolean mailRecipientsCCDisabled) {
        this.mailRecipientsCCDisabled = mailRecipientsCCDisabled;
    }

    public boolean isMailRecipientsBCCDisabled() {
        return mailRecipientsBCCDisabled;
    }

    public void setMailRecipientsBCCDisabled(boolean mailRecipientsBCCDisabled) {
        this.mailRecipientsBCCDisabled = mailRecipientsBCCDisabled;
    }

    public boolean isMailAttachmentsDisabled() {
        return mailAttachmentsDisabled;
    }

    public void setMailAttachmentsDisabled(boolean mailAttachmentsDisabled) {
        this.mailAttachmentsDisabled = mailAttachmentsDisabled;
    }

    public String getMailOriginator() {
        return mailOriginator;
    }

    public void setMailOriginator(String mailOriginator) {
        this.mailOriginator = mailOriginator;
    }

    @Override
    public Object clone() {
        return new BusinessNoteTypeImpl(this);
    }
}
