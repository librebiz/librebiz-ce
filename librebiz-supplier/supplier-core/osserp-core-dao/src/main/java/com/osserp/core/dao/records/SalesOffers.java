/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 14, 2006 3:13:50 PM 
 * 
 */
package com.osserp.core.dao.records;

import java.util.List;

import com.osserp.common.ClientException;

import com.osserp.core.Item;
import com.osserp.core.calc.Calculation;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Offer;
import com.osserp.core.finance.Record;
import com.osserp.core.products.Product;
import com.osserp.core.requests.Request;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SalesOffers extends Records, SalesRecords {

    /**
     * Provides all offers of a request
     * @param request
     * @return offers
     */
    List<Offer> getByRequest(Request request);

    /**
     * Provides the activated offer related to given request
     * @param request
     * @return activated offer or null if none exists
     */
    Offer findActivated(Request request);

    /**
     * Creates a new sales offer
     * @param request
     * @param employee creating the offer
     * @return new created offer
     * @throws ClientException if any requirements missing
     */
    Offer create(Request request, Employee employee) throws ClientException;

    /**
     * Creates a new offer by provided request and items
     * @param request
     * @param items
     * @return new created offer
     */
    Offer createOrUpdate(Request request, List<Item> items);

    /**
     * Creates a new sales offer
     * @param request
     * @param calculation
     * @param employee creating the offer
     * @return new created offer
     * @throws ClientException if any requirements missing
     */
    Offer create(Request request, Calculation calculation, Employee employee)
            throws ClientException;

    /**
     * Create a copy of an existing offer without backed calculation
     * @param employee
     * @param offer
     * @return new created offer
     * @throws ClientException if validation of current offer failed
     */
    Offer create(Employee employee, Offer offer) throws ClientException;

    /**
     * Changes branch office of all open offers
     * @param request
     */
    void changeBranch(Request request);

    /**
     * Resets status if final and sets corresponding calculation as historical
     * @param offer
     */
    void deselectOffer(Offer offer);

    /**
     * Provides the order whose is marked as order
     * @param requestId
     * @return id of the offer marked as order or null of none exists
     */
    Long findActivated(Long requestId);

    /**
     * Provides the count of referenced offers
     * @param requestId
     * @return count
     */
    int getReferencedCount(Long requestId);

    /**
     * Provides the offer id related to given calculation if offer already exists
     * @param calculation
     * @return offer or null if not exists
     */
    Offer findByCalculation(Calculation calculation);

    /**
     * Finds offer without calculation
     * @param requestId
     * @return offer or null if non exists
     */
    Offer findWithoutCalculation(Long requestId);

    /**
     * Provides the calculation linked to given record
     * @param record
     * @return linked calculation or null if none exists
     */
    Calculation getCalculation(Record record);

    /**
     * Provides the average plant power of all sales offers
     * @param reference
     * @return averagePlantPower
     */
    Double getAveragePlantPower(Long reference);

    /**
     * Updates offer item partner and purchase price values
     * @param product
     */
    void updateSalesRevenue(Product product);
}
