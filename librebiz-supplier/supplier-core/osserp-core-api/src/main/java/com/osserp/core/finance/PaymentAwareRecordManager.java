/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 04-Feb-2007 17:19:13 
 * 
 */
package com.osserp.core.finance;

import java.math.BigDecimal;
import java.util.Date;

import com.osserp.common.ClientException;
import com.osserp.core.employees.Employee;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface PaymentAwareRecordManager {

    /**
     * Adds a new payment to a payment aware record
     * @param user adding the payment
     * @param record
     * @param paymentType
     * @param amount
     * @param paid
     * @param customHeader
     * @param note
     * @param bankAccountId
     * @return payment added
     * @throws ClientException if amount is empty
     */
    Payment addCustomPayment(
            Employee user,
            PaymentAwareRecord record,
            BigDecimal amount,
            Date paid,
            String customHeader,
            String note,
            Long bankAccountId)
            throws ClientException;

    /**
     * Adds a new payment to a payment aware record
     * @param user adding the payment
     * @param record
     * @param paymentType
     * @param amount
     * @param paid
     * @param bankAccountId
     * @return payment added
     * @throws ClientException if amount is empty
     */
    Payment addPayment(
            Employee user,
            PaymentAwareRecord record,
            Long paymentType,
            BigDecimal amount,
            Date paid,
            Long bankAccountId)
            throws ClientException;

    /**
     * Adds an address correction to a record
     * @param user
     * @param record
     * @param name
     * @param street
     * @param streetAddon
     * @param zipcode
     * @param city
     * @param country
     * @param note
     * @throws ClientException if any required value missing
     */
    void addCorrection(
            Employee user,
            PaymentAwareRecord record,
            String name,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long country,
            String note) throws ClientException;
}
