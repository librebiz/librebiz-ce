/**
 *
 * Copyright (C) 2023 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License (AGPL)
 * version 3 as published by the Free Software Foundation. In accordance
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code
 * versions must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area
 * or, if not reasonably feasible for technical reason, as a top-level
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute.
 *
 * Created on Nov 23, 2023
 *
 */
package com.osserp.core.service.records;

import java.io.File;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.common.OptionsCache;
import com.osserp.common.dms.DmsManager;
import com.osserp.common.dms.DocumentData;
import com.osserp.common.util.FileUtil;
import com.osserp.core.Options;
import com.osserp.core.finance.RecordExport;
import com.osserp.core.finance.RecordSearch;
import com.osserp.core.finance.RecordType;
import com.osserp.core.service.impl.ExportArchiveService;

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public class RecordExportArchiveImpl extends ExportArchiveService implements RecordExportArchive {

    public RecordExportArchiveImpl(
            OptionsCache optionsCache,
            DmsManager dmsManager,
            RecordSearch recordSearch,
            RecordPrintService recordPrintService) {
        super(optionsCache, dmsManager, recordSearch, recordPrintService);
    }

    public final DocumentData getArchive(RecordExport recordExport) throws ClientException {
        if (recordExport == null || recordExport.getRecords().isEmpty()) {
            throw new ClientException(ErrorCode.NO_DATA_FOUND);
        }
        String name = createArchiveName(recordExport);
        String zipname = name + ".zip";
        String dirname = getArchiveDirName(name);
        File directory = new File(dirname);
        String zipfilename = dirname + ".zip";
        File zipfile = new File(zipfilename);
        if (zipfile.exists()) {
            if (!zipfile.canRead()) {
                throw new ClientException(ErrorCode.FILE_ACCESS);
            }
            return new DocumentData(FileUtil.getBytes(zipfile), zipname);
        }
        if (directory.exists() && (!directory.canWrite() || !directory.canRead())) {
            throw new ClientException(ErrorCode.DIRECTORY_ACCESS);
        }
        if (directory.exists()) {
            FileUtil.delete(directory);
        }
        directory = createDirectory(dirname);
        recordExport.getRecords().forEach(obj -> {
            exportVoucher(dirname, obj.getId(), obj.getType());
        });
        String result = FileUtil.zip(dirname, zipfilename);
        zipfile = new File(result);
        if (zipfile.exists() && zipfile.canRead()) {
            FileUtil.delete(directory);
            return new DocumentData(FileUtil.getBytes(zipfile), zipname);
        }
        throw new ClientException(ErrorCode.UNKNOWN);
    }

    private String getArchiveDirName(String name) {
        return getDmsExportRoot() + "/" + name;
    }

    private String createArchiveName(RecordExport recordExport) {
        String tname = "TP" + recordExport.getType().toString();
        Option opt = getOption(Options.RECORD_TYPES, recordExport.getType());
        if (opt instanceof RecordType) {
            tname = ((RecordType) opt).getNumberPrefix();
        }
        StringBuilder nbuffer = new StringBuilder(recordExport.getExportInterval());
        nbuffer.append("_").append(tname).append("-ID").append(recordExport.getId());
        return nbuffer.toString();
    }

}
