/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 15, 2014 
 * 
 */
package com.osserp.core.system;

import com.osserp.common.ClientException;
import com.osserp.common.service.SysInfo;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SystemSetupManager {
    
    /**
     * Indicates if initial setup is done (e.g. no further action required).
     * @return true if initial setup done
     */
    boolean isInitialSetupDone();
    
    /**
     * Provides the sysInfo object. All methods of this interface except this
     * throw an exception if the osserpSetup runtime property wasn't enabled. 
     * @return sysInfo object required to check if app has startet with the 
     * -DosserpSetup=true property set
     */
    SysInfo getSysInfo();
    
    /**
     * Provides currently activated setup.
     * @return setup configuration
     * @throws ClientException if setup is not enabled or not exists
     */
    SystemSetup getSystemSetup() throws ClientException;
    
    /**
     * Resets previous input
     * @param setup
     * @return cleaned up setup object
     * @throws ClientException if user is not authorized
     */
    SystemSetup resetSetup(SystemSetup setup) throws ClientException;

    /**
     * Updates setup object with current form input data
     * @param setup
     * @param setupData 
     * @return setup object containing latest form input
     * @throws ClientException if validation of required data failed
     */
    SystemSetup updateSetup(SystemSetup setup, Object setupData) throws ClientException;
    
    /**
     * Updates current status of the setup object and adds an enabled setup property.
     * @param setup
     * @param status
     * @return setup object
     * @throws ClientException if validation failed or setup not enabled
     */
    SystemSetup updateSetupStatus(SystemSetup setup, String status) throws ClientException;
    
    /**
     * Updates a setup property
     * @param setup
     * @param name
     * @param value
     * @return updated setup object
     * @throws ClientException if user is not authorized
     */
    SystemSetup updateSetupProperty(SystemSetup setup, String name, String value) throws ClientException;
    
    /**
     * Removes an existing setup property
     * @param setup
     * @param name
     * @return updated setup object
     * @throws ClientException if user is not authorized
     */
    SystemSetup removeSetupProperty(SystemSetup setup, String name) throws ClientException;
    
    /**
     * Activate the administrative user account for initial employee. Do not invoke this method
     * before successful invocation of the setupApplication method. 
     * @param setupData
     * @param uid
     * @param password
     * @param confirmPassword
     * @return closed setup object with contactPasswordComplete enabled on success 
     * @throws ClientException if passwords not match or other validation error.
     */
    SystemSetup setupAccount(Object setupData, String uid, String password, String confirmPassword) throws ClientException;
    
    /**
     * Sets up the application, close setup 
     * @param setupData
     * @return closed setup object
     * @throws ClientException
     */
    SystemSetup setupApplication(Object setupData) throws ClientException;
    
}
