<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${requestScope.productPriceAwareView}"/>
<c:set var="product" value="${view.product}"/>

<div class="table-responsive table-responsive-default">
	<table class="table">
		<tr>
			<th colspan="4" style="width: 788px;"><o:out value="${product.productId}" /> - <o:out value="${product.name}" /></th>
		</tr>
		<o:permission role="purchase_price_view,purchase_price_change" info="permissionPurchasePriceView">
			<tr>
				<td class="col1"><fmt:message key="purchasePriceBase"/></td>
				<td class="col2 right"><o:number value="${product.estimatedPurchasePrice}" format="currency" /></td>
				<c:choose>
					<c:when test="${view.valuationMode}">
						<td class="col3 right">
							<fmt:message key="newProductValuation"/>
						</td>
						<td class="col4 right">
							<v:form name="productPriceForm" url="/products/productPrice/updateValuation">
								<input type="text" class="number" id="valuationInput" name="value" value="<o:number value="${product.summary.lastPurchasePrice}" format="currency"/>" style="text-align: right;"/>
								<input type="image" name="method" src="<c:url value="${applicationScope.saveIcon}"/>" value="updateValuation" style="margin-left: 5px; margin-right: 5px;" class="bigicon" title="<fmt:message key="saveInput"/>"/>
							</v:form>
						</td>
					</c:when>
					<c:otherwise>
						<td class="col3 right" style="text-align: right;">
							<o:permission role="product_valuation_change" info="permissionProductValuationChange">
								<v:link url="/products/productPrice/enableValuationMode" title="permissionProductValuationChange">
									<fmt:message key="purchasePriceLast"/>
								</v:link>
							</o:permission>
							<o:forbidden role="product_valuation_change">
								<fmt:message key="purchasePriceLast"/>
							</o:forbidden>
						</td>
						<td class="col4 right"><o:number value="${product.summary.lastPurchasePrice}" format="currency" /></td>
					</c:otherwise>
				</c:choose>
			</tr>
			<tr>
				<td colspan="2">
					<c:choose>
						<c:when test="${product.ignorePurchasePriceLimit}">
							<fmt:message key="salesPriceUnderPurchasePriceInputEnabled"/>
						</c:when>
						<c:otherwise>
							<fmt:message key="salesPriceUnderPurchasePriceInputDisabled"/>
						</c:otherwise>
					</c:choose>
				</td>
				<td class="col3 right" style="text-align: right;"><fmt:message key="purchasePriceAverage"/></td>
				<td class="col4 right"><o:number value="${product.summary.averagePurchasePrice}" format="currency" /></td>
			</tr>
		</o:permission>
		<tr>
			<td colspan="4">&nbsp;</td>
		</tr>
		<tr>
			<th class="col1"><fmt:message key="targetGroup"/></th>
			<th class="col2 right"><fmt:message key="price"/> &euro;</th>
			<th class="col3 right"><fmt:message key="priceMinimum"/> &euro;</th>
			<th class="col4 right"><fmt:message key="marginMinimum"/> %</th>
		</tr>
		<c:choose>
			<c:when test="${empty view.salesPriceHistory}">
				<tr>
					<td class="col1"><fmt:message key="sale"/></td>
					<td class="col2 right"><o:number value="${product.consumerPrice}" format="currency"/></td>
					<td class="col3 right"><o:number value="${product.consumerPriceMinimum}" format="currency"/></td>
					<td class="col4 right"><o:number value="${product.consumerMinimumMargin * 100}" format="currency"/></td>
				</tr>
				<tr>
					<td class="col1"><fmt:message key="reseller"/></td>
					<td class="col2 right"><o:number value="${product.resellerPrice}" format="currency"/></td>
					<td class="col3 right"><o:number value="${product.resellerPriceMinimum}" format="currency"/></td>
					<td class="col4 right"><o:number value="${product.resellerMinimumMargin * 100}" format="currency"/></td>
				</tr>
				<tr>
					<td class="col1"><fmt:message key="partner"/></td>
					<td class="col2 right"><o:number value="${product.partnerPrice}" format="currency"/></td>
					<td class="col3 right"><o:number value="${product.partnerPriceMinimum}" format="currency"/></td>
					<td class="col4 right"><o:number value="${product.partnerMinimumMargin * 100}" format="currency"/></td>
				</tr>
			</c:when>
			<c:otherwise>
				<c:forEach var="obj" items="${view.salesPriceHistory}" varStatus="status">
    				<tr>
    					<th class="historyHeader" colspan="2">
    						<c:choose>
    							<c:when test="${view.historyEditMode and view.historyEntryId == obj.id}">
									<v:form name="productPriceForm" url="/products/productPrice/updateHistory">
    									<input type="text" id="validFrom" name="validFrom" value="<o:date value="${obj.changed}"/>" style="text-align: right; width: 95px;"/>
    									<input type="image" name="method" src="<c:url value="${applicationScope.saveIcon}"/>" value="updateHistory" style="margin-left: 5px; margin-right: 5px;" class="bigicon" title="<fmt:message key="saveInput"/>"/>
    								</v:form>
    							</c:when>
    							<c:otherwise>
    								<c:choose>
    									<c:when test="${!view.historyEditMode}">
    										<v:link url="/products/productPrice/enableHistoryEditMode?id=${obj.id}">
    											<span title="<fmt:message key="changeDate"/>"><o:date value="${obj.changed}"/></span>
    										</v:link> - <oc:employee value="${obj.changedBy}"/>:
    									</c:when>
    									<c:otherwise>
    										<o:date value="${obj.changed}"/> - <oc:employee value="${obj.changedBy}"/>:
    									</c:otherwise>
    								</c:choose>
    							</c:otherwise>
    						</c:choose>
    					</th>
    					<th class="col3 right"><fmt:message key="purchasePriceLast"/></th>
    					<th class="col4 right"><o:number value="${obj.lastPurchasePrice}" format="currency"/></th>
    				</tr>
    				<tr>
    					<td class="col1"><fmt:message key="sale"/></td>
						<td class="col2 right"><o:number value="${obj.consumerPrice}" format="currency"/></td>
						<td class="col3 right"><o:number value="${obj.consumerPriceMinimum}" format="currency"/></td>
						<td class="col4 right"><o:number value="${obj.consumerMinimumMargin * 100}" format="currency"/></td>
					</tr>
					<tr>
						<td class="col1"><fmt:message key="reseller"/></td>
						<td class="col2 right"><o:number value="${obj.resellerPrice}" format="currency"/></td>
						<td class="col3 right"><o:number value="${obj.resellerPriceMinimum}" format="currency"/></td>
						<td class="col4 right"><o:number value="${obj.resellerMinimumMargin * 100}" format="currency"/></td>
					</tr>
					<tr>
						<td class="col1"><fmt:message key="partner"/></td>
						<td class="col2 right"><o:number value="${obj.partnerPrice}" format="currency"/></td>
						<td class="col3 right"><o:number value="${obj.partnerPriceMinimum}" format="currency"/></td>
						<td class="col4 right"><o:number value="${obj.partnerMinimumMargin * 100}" format="currency"/></td>
					</tr>
				</c:forEach>
			</c:otherwise>
		</c:choose>
	</table>
</div>