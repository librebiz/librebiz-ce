<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${sessionScope.stockReportView}" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="styles" type="string">
        <style type="text/css">
        <c:import url="product_list.css" />
        </style>
    </tiles:put>

    <tiles:put name="headline">
        <fmt:message key="productPlanning" />
        <c:choose>
            <c:when test="${!empty view.selectedProductCategory}">[<fmt:message key="category" />: <o:out value="${view.selectedProductCategory.name}" />]</c:when>
            <c:when test="${!empty view.selectedProductGroup}">[<fmt:message key="group" />: <o:out value="${view.selectedProductGroup.name}" />]</c:when>
            <c:when test="${!empty view.selectedProductType}">[<fmt:message key="type" />: <o:out value="${view.selectedProductType.name}" />]</c:when>
        </c:choose>
        <c:if test="${!empty view.selectedStock}">[<fmt:message key="stock" />: <o:out value="${view.selectedStock.name}" />]</c:if>
    </tiles:put>
    <tiles:put name="headline_right">
        <ul>
            <li><a href="<c:url value="/productStockReport.do?method=exit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon" /></a></li>
            <li><a href="<c:url value="/productStockReport.do?method=refresh"/>" title="<fmt:message key="reloadList"/>"><o:img name="replaceIcon" /></a></li>
            <c:choose>
                <c:when test="${view.listVacantMode}">
                    <li><a href="<c:url value="/productStockReport.do?method=toggleListVacantMode"/>" title="<fmt:message key="displayReleasedOnly"/>"><o:img name="enabledIcon" /></a></li>
                </c:when>
                <c:otherwise>
                    <li><a href="<c:url value="/productStockReport.do?method=toggleListVacantMode"/>" title="<fmt:message key="displayVacantToo"/>"><o:img name="disabledIcon" /></a></li>
                </c:otherwise>
            </c:choose>
            <li><a href="<c:url value="/productStockReport.do?method=toggleLazyIgnoring"/>" title="<fmt:message key="switchFlowless"/>"><o:img name="reloadIcon" /></a></li>
            <li><v:ajaxLink linkId="productClassificationSelection" url="/selections/productClassificationSelection/forward?selectionTarget=/productStockReport.do?method=filter" title="filter">
                    <o:img name="filterIcon" />
                </v:ajaxLink>
            </li>
            <li><v:link url="/index" title="backToMenu">
                    <o:img name="homeIcon" />
                </v:link>
            </li>
        </ul>
    </tiles:put>
    <tiles:put name="content" type="string">

        <c:set var="sortArrow">
            <c:choose>
                <c:when test="${view.comparator.reverse}">&#x25B4;</c:when>
                <c:otherwise>&#x25BE;</c:otherwise>
            </c:choose>
        </c:set>
        <div class="content-area">
            <div class="row">
                <div class="col-md-12 panel-area">
                    <div class="table-responsive table-responsive-default">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>
                                        <a href="<c:url value="/productStockReport.do?method=sort&order=byId" />"><fmt:message key="product" /></a>
                                        <c:if test="${view.sortCriteria == 'byId'}"><c:out escapeXml="false" value="${sortArrow}" /></c:if>
                                    </th>
                                    <th>
                                        <a href="<c:url value="/productStockReport.do?method=sort&order=byName" />"><fmt:message key="name" /></a>
                                        <c:if test="${view.sortCriteria == 'byName'}"><c:out escapeXml="false" value="${sortArrow}" /></c:if>
                                    </th>
                                    <th class="right">
                                        <c:choose>
                                            <c:when test="${view.listVacantMode}">
                                                <a href="<c:url value="/productStockReport.do?method=sort&order=byOrderedAndVacant" />" title="<fmt:message key="stillToDeliverQuantity"/>"><fmt:message key="order" /></a>
                                                <c:if test="${view.sortCriteria == 'byOrderedAndVacant'}">
                                                    <c:out escapeXml="false" value="${sortArrow}" />
                                                </c:if>
                                            </c:when>
                                            <c:otherwise>
                                                <a href="<c:url value="/productStockReport.do?method=sort&order=byOrdered" />" title="<fmt:message key="stillToDeliverQuantity"/>"><fmt:message key="order" /></a>
                                                <c:if test="${view.sortCriteria == 'byOrdered'}">
                                                    <c:out escapeXml="false" value="${sortArrow}" />
                                                </c:if>
                                            </c:otherwise>
                                        </c:choose>
                                    </th>
                                    <th class="right">
                                        <a href="<c:url value="/productStockReport.do?method=sort&order=byAvailableStock" />" title="<fmt:message key="stockAndReceiptTitle"/>"><fmt:message key="stockAndReceiptShort" /></a>
                                        <c:if test="${view.sortCriteria == 'byAvailableStock'}"><c:out escapeXml="false" value="${sortArrow}" /></c:if>
                                    </th>
                                    <th class="right">
                                        <a href="<c:url value="/productStockReport.do?method=sort&order=byExpected" />" title="<fmt:message key="orderedBySupplier"/>"><fmt:message key="purchaseOrderShort" /></a>
                                        <c:if test="${view.sortCriteria == 'byExpected'}"><c:out escapeXml="false" value="${sortArrow}" /></c:if>
                                    </th>
                                    <th class="right">
                                        <c:choose>
                                            <c:when test="${view.listVacantMode}">
                                                <a href="<c:url value="/productStockReport.do?method=sort&order=byVacantAvailability" />" title="<fmt:message key="availability"/>"><fmt:message key="availabilityFormular" /></a>
                                                <c:if test="${view.sortCriteria == 'byVacantAvailability'}">
                                                    <c:out escapeXml="false" value="${sortArrow}" />
                                                </c:if>
                                            </c:when>
                                            <c:otherwise>
                                                <a href="<c:url value="/productStockReport.do?method=sort&order=byAvailability" />" title="<fmt:message key="availability"/>"><fmt:message key="availabilityFormular" /></a>
                                                <c:if test="${view.sortCriteria == 'byAvailability'}">
                                                    <c:out escapeXml="false" value="${sortArrow}" />
                                                </c:if>
                                            </c:otherwise>
                                        </c:choose>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:choose>
                                    <c:when test="${empty view.list}">
                                        <tr>
                                            <td colspan="7"><fmt:message key="noProductsFound" /></td>
                                        </tr>
                                    </c:when>
                                    <c:otherwise>
                                        <c:forEach var="product" items="${view.list}" varStatus="s">
                                            <c:if test="${!view.ignoreLazy or !product.lazy}">
                                                <c:set var="warning" value="${product.summary.purchaseOrdered}" />
                                                <tr id="product${product.productId}" <c:if test="${!warning}"> class="altrow"</c:if>>
                                                    <td><a href="<c:url value="/products.do?method=load&id=${product.productId}&stockId=${view.selectedStock.id}&exit=stockReportDisplay&exitId=product${product.productId}"/>"> <o:out value="${product.productId}" /></a></td>
                                                    <td <c:if test="${warning}"> class="error"</c:if>><o:out value="${product.name}" /></td>
                                                    <td class="right">
                                                        <c:if test="${product.summary.ordered > 0 or (view.listVacantMode and product.summary.vacant > 0)}">
                                                            <a href="<c:url value="/salesPlanning.do?method=forward&exit=stockReportDisplay&id=${product.productId}&stockId=${product.currentStock}&vacant=${view.listVacantMode}&exitId=product${product.productId}"/>">
                                                        </c:if>
                                                        <c:choose>
                                                            <c:when test="${view.listVacantMode}">
                                                                <o:number value="${product.summary.orderedAndVacant}" format="integer" /> (<o:number value="${product.summary.vacant}" format="integer" />)
                                                            </c:when>
                                                            <c:otherwise>
                                                                <o:number value="${product.summary.ordered}" format="integer" />
                                                            </c:otherwise>
                                                        </c:choose>
                                                        <c:if test="${product.summary.ordered > 0 or (view.listVacantMode and product.summary.vacant > 0)}">
                                                            </a>
                                                        </c:if>
                                                    </td>
                                                    <td class="right"><o:number value="${product.summary.availableStock}" format="integer" /></td>
                                                    <td class="right">
                                                        <c:choose>
                                                            <c:when test="${product.summary.expected > 0}">
                                                                <a href="<c:url value="/purchaseOrder.do?method=forwardOpenByProduct&exit=stockReportDisplay&exitId=product${product.productId}&id=${product.productId}"/>"><o:number value="${product.summary.expected}" format="integer" /></a>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <o:number value="${product.summary.expected}" format="integer" />
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </td>
                                                    <td class="right">
                                                        <c:choose>
                                                            <c:when test="${view.listVacantMode}">
                                                                <o:number value="${product.summary.vacantAvailability}" format="integer" />
                                                            </c:when>
                                                            <c:otherwise>
                                                                <o:number value="${product.summary.availability}" format="integer" />
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </td>
                                                </tr>
                                            </c:if>
                                        </c:forEach>
                                    </c:otherwise>
                                </c:choose>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
