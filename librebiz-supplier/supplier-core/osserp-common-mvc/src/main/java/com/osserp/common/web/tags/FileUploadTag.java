/**
 *
 * Copyright (C) 2017 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 3, 2017 
 * 
 */
package com.osserp.common.web.tags;

import javax.servlet.jsp.JspException;

import com.osserp.common.User;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class FileUploadTag extends AbstractOutTag {
    
    private static final String DEFAULT_LABEL = "filechoosing"; 
    
    private String label;
    
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    private boolean multiple;

    public boolean isMultiple() {
        return multiple;
    }

    public void setMultiple(boolean multiple) {
        this.multiple = multiple;
    }

    @Override
    public String getOutput() throws JspException {
        StringBuilder buffer = new StringBuilder();
        if (multiple) {
            buffer.append("<input class=\"fileInput\" type=\"file\" name=\"files\" multiple=\"multiple\">");
        } else if (isUploadButtonNative()) {
            buffer.append("<input type=\"file\" name=\"file\" />");
        } else {
            buffer.append("<div id=\"btn-upload\" onclick=\"getUploadFile()\" "
                    + "class=\"file-upload\">");
            buffer.append(getResourceString(label != null ? label : DEFAULT_LABEL));
            buffer.append("</div>");
            buffer.append("<div style=\"height:0px;width:0px;overflow:hidden;\">"
                    + "<input id=\"uploadFile\" type=\"file\" name=\"file\" "
                    + "onchange=\"submitUploadFile(this)\"/></div>");
        }
        return buffer.toString();
    }
    
    protected boolean isUploadButtonNative() {
        User user = getUser();
        if (user != null && !user.getProperties().isEmpty()
                && user.getProperties().containsKey("uploadButtonNative")) {
            return user.getProperties().get("uploadButtonNative").isEnabled();
        }
        return false;
    }
}
