/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 01-Feb-2007 23:46:08 
 * 
 */
package com.osserp.core.service.records;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.dms.DocumentData;

import com.osserp.core.BusinessCase;
import com.osserp.core.customers.Customer;
import com.osserp.core.dao.PaymentConditions;
import com.osserp.core.dao.Products;
import com.osserp.core.dao.Projects;
import com.osserp.core.dao.records.SalesInvoices;
import com.osserp.core.dao.records.SalesOrderDownpayments;
import com.osserp.core.dao.records.SalesPayments;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Downpayment;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.Payment;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordPaymentAgreement;
import com.osserp.core.model.records.SalesDownpaymentImpl;
import com.osserp.core.sales.SalesDownpaymentManager;
import com.osserp.core.system.SystemConfigManager;
import com.osserp.core.tasks.ProductPlanningCacheSender;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesDownpaymentManagerImpl extends AbstractInvoiceManager
        implements SalesDownpaymentManager {
    private static Logger log = LoggerFactory.getLogger(SalesDownpaymentManagerImpl.class.getName());
    private SalesInvoices salesInvoices = null;
    private SalesPayments payments = null;
    private Projects projects = null;
    private RecordPrintService recordPrintService = null;
    

    public SalesDownpaymentManagerImpl(
            SalesOrderDownpayments records,
            SystemConfigManager systemConfigManager,
            Products products,
            ProductPlanningCacheSender productPlanningTask,
            PaymentConditions paymentConditions,
            SalesPayments payments,
            SalesInvoices salesInvoices,
            Projects projects,
            RecordPrintService recordPrintService) {
        super(records, systemConfigManager, products, productPlanningTask, paymentConditions);
        this.payments = payments;
        this.projects = projects;
        this.salesInvoices = salesInvoices;
        this.recordPrintService = recordPrintService;
    }

    public Downpayment create(
            Long invoiceType,
            Order order,
            Employee user,
            boolean copyNote,
            boolean copyTerms,
            boolean copyItems,
            Long customId,
            Date customDate,
            BigDecimal customAmount) throws ClientException {
        if (invoiceType == null || order == null || user == null) {
            log.error("createInvoice() invoiceType, order or createdBy was null!");
            throw new BackendException(ErrorCode.APPLICATION_CONFIG);
        }
        // check that order is unchangeable, e.g. valid
        if (!order.isUnchangeable()) {
            throw new ClientException(ErrorCode.ORDER_MISSING);
        }
        // check that we have an amount to bill
        if (Downpayment.DELIVERY.equals(invoiceType)
                || Downpayment.DOWNPAYMENT.equals(invoiceType)) {

            RecordPaymentAgreement pa = order.getPaymentAgreement();
            if (pa.isAmountsFixed()) {
                Double amount = getAmount(pa, invoiceType);
                if (amount == null || amount.doubleValue() == 0) {
                    throw new ClientException(ErrorCode.PAYMENT_STEPS_INVALID);
                }
            } else {
                Double percent = getPercent(pa, invoiceType);
                if (percent == null || percent.doubleValue() == 0) {
                    throw new ClientException(ErrorCode.PAYMENT_STEPS_INVALID);
                }
            }
        }
        Downpayment result = getDownpaymentsDao().create(invoiceType, order, user, copyNote,
                copyTerms, copyItems, customId, customDate);
        if (Downpayment.PARTIAL_DOWNPAYMENT.equals(invoiceType)
                && customAmount != null) {
            updateProrateAmount(result, customAmount);
        }
        return result;
    }

    public Downpayment updateProrateAmount(Downpayment invoice, BigDecimal amount) {
        invoice.updateProrate(amount);
        getDao().save(invoice);
        return invoice;
    }

    @Override
    public void validatePrint(Record record, Long nextStatus) throws ClientException {
        super.validatePrint(record, nextStatus);
        if (record instanceof Downpayment && record.isItemsChangeable()) {
            Downpayment dp = (Downpayment) record;
            if (isNotSet(dp.getAmount())) {
                // updateProrateAmount not called
                throw new ClientException(ErrorCode.AMOUNT_MISSING);
            }
        }
    }

    public void cancel(Employee user, Invoice record, Date date) throws ClientException {
        Date now = new Date(System.currentTimeMillis());
        Date cancelDate = date;
        if (cancelDate != null) {
            if (cancelDate.before(record.getCreated())) {
                throw new ClientException(ErrorCode.DATE_PAST);
            } else if (cancelDate.after(now)) {
                throw new ClientException(ErrorCode.DATE_FUTURE);
            }
        } else {
            cancelDate = now;
        }
        List<Payment> existing = payments.getByRecord(record);
        if (!existing.isEmpty()) {
            List<Record> invoices = salesInvoices.getByCustomer(
                    (Customer) record.getContact(),
                    record.getCompany());
            if (invoices.isEmpty()) {
                throw new ClientException(ErrorCode.CANCEL_IMPOSSIBLE_PAYMENT_BOOKED);
            }
        }
        if (record instanceof Downpayment) {
            Downpayment dp = (Downpayment) record;
            archiveSourceDocument(user, dp);
            getDownpaymentsDao().cancel(user, dp, cancelDate);
            try {
                byte[] pdf = null;
                if (isSet(dp.getBusinessCaseId()) && projects.exists(dp.getBusinessCaseId())) {
                    BusinessCase obj = projects.load(dp.getBusinessCaseId());
                    pdf = recordPrintService.createPdf(user, dp, obj, false);
                } else {
                    pdf = recordPrintService.createPdf(user, dp, false);
                }
                recordPrintService.archive(user, dp, pdf);
            } catch (Exception e) {
                log.error("cancel() failed on attempt to create pdf [record="
                    + dp.getId() + ", message=" + e.getMessage() + "]", e);
            }
        }
    }

    private void archiveSourceDocument(Employee user, Downpayment downpayment) {
        if (downpayment instanceof SalesDownpaymentImpl) {
            try {
                DocumentData doc = recordPrintService.getPdf(downpayment);
                if (doc != null) {
                    SalesDownpaymentImpl obj = (SalesDownpaymentImpl) downpayment;
                    obj.setId(obj.getId() * (-1));
                    recordPrintService.archive(user, obj, doc.getBytes());
                    obj.setId(obj.getId() * (-1));
                }
            } catch (Exception e) {
                log.error("archiveSourceDocument() failed on attempt to store pdf [record="
                        + downpayment.getId() + ", message=" + e.getMessage() + "]", e);
            }
        }
    }

    public List<Downpayment> findCanceled(Order order) {
        return getDownpaymentsDao().getCanceledByOrder(order);
    }
    
    public DocumentData printCanceledSource(Employee user, Long id) throws ClientException {
        SalesDownpaymentImpl obj = (SalesDownpaymentImpl) find(id);
        obj.setCanceled(false);
        obj.updateStatus(3L);
        try {
            obj.setId(obj.getId() * (-1));
            DocumentData result = recordPrintService.getPdf(obj);
            if (result != null) {
                log.debug("printCanceledSource() found existing [id=" + obj.getId()
                        + ", filename=" + result.getRealFilename() + "]");
                return result;
            }
        } catch (Exception e) {
            log.info("printCanceledSource() caught exception [message=" + 
                    e.getMessage() + "]", e);
        }
        if (obj.getId() < 0) {
            obj.setId(obj.getId() * (-1));
        }
        byte[] pdf = null;
        if (isSet(obj.getBusinessCaseId()) && projects.exists(obj.getBusinessCaseId())) {
            BusinessCase bc = projects.load(obj.getBusinessCaseId());
            pdf = recordPrintService.createPdf(user, obj, bc, false);
        } else {
            pdf = recordPrintService.createPdf(user, obj, false);
        }
        DocumentData result = new DocumentData(pdf, obj.getNumber() + ".pdf");
        if (obj.getId() > 0) {
            obj.setId(obj.getId() * (-1));
        }
        recordPrintService.archive(user, obj, pdf);
        return result;
    }

    public Downpayment restoreCanceled(Employee user, Downpayment invoice) throws ClientException {
        return getDownpaymentsDao().restoreCanceled(user, invoice);
    }

    private SalesOrderDownpayments getDownpaymentsDao() {
        return (SalesOrderDownpayments) getDao();
    }

    private Double getAmount(RecordPaymentAgreement pa, Long invoiceType) {
        Double result = null;
        if (invoiceType.equals(Invoice.DELIVERY)) {
            result = pa.getDeliveryInvoiceAmount();
        } else {
            result = pa.getDownpaymentAmount();
        }
        return result;
    }

    private Double getPercent(RecordPaymentAgreement pa, Long invoiceType) {
        Double result = null;
        if (invoiceType.equals(Invoice.DELIVERY)) {
            result = pa.getDeliveryInvoicePercent();
        } else {
            result = pa.getDownpaymentPercent();
        }
        return result;
    }

}
