/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 28, 2009 10:13:14 AM 
 * 
 */
package com.osserp.core.service.records;

import java.util.ArrayList;
import java.util.List;

import com.osserp.common.ClientException;

import com.osserp.core.dao.PaymentConditions;
import com.osserp.core.dao.Products;
import com.osserp.core.dao.records.PurchaseInvoiceAwares;
import com.osserp.core.purchasing.PurchaseInvoice;
import com.osserp.core.purchasing.PurchaseInvoiceAwareManager;
import com.osserp.core.purchasing.PurchaseInvoiceType;
import com.osserp.core.system.SystemConfigManager;
import com.osserp.core.tasks.ProductPlanningCacheSender;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractPurchaseInvoiceManager extends AbstractInvoiceManager implements PurchaseInvoiceAwareManager {

    /**
     * Creates a new purchase invoice manager
     * @param records
     * @param productPlanningTask
     * @param paymentConditions
     */
    public AbstractPurchaseInvoiceManager(
            PurchaseInvoiceAwares records,
            SystemConfigManager systemConfigManager,
            Products products,
            ProductPlanningCacheSender productPlanningTask,
            PaymentConditions paymentConditions) {
        super(records, systemConfigManager, products, productPlanningTask, paymentConditions);
    }

    public List<Long> getCreditNoteIds(PurchaseInvoice invoice) {
        PurchaseInvoiceAwares records = getInvoiceAwares();
        return records.getCreditNoteIds(invoice);
    }

    @SuppressWarnings("unchecked")
    public List<PurchaseInvoiceType> getInvoiceTypes() {
        PurchaseInvoiceAwares records = getInvoiceAwares();
        return new ArrayList(records.getBookingTypes());
    }

    public PurchaseInvoiceType getInvoiceType(Long id) {
        PurchaseInvoiceAwares records = getInvoiceAwares();
        return (PurchaseInvoiceType) records.getBookingType(id);
    }

    public void updateSummary(PurchaseInvoice invoice, String taxAmount, String reducedTaxAmount, String grossAmount) throws ClientException {
        invoice.updateSummary(taxAmount, reducedTaxAmount, grossAmount);
        getInvoiceAwares().save(invoice);
    }

    private PurchaseInvoiceAwares getInvoiceAwares() {
        return (PurchaseInvoiceAwares) getDao();
    }

}
