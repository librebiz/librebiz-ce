/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Feb 21, 2011 12:54:04 PM 
 * 
 */
package com.osserp.core.contacts;

import com.osserp.common.Entity;

/**
 * 
 * @author jg <jg@osserp.com>
 */
public interface ZipcodeSearch extends Entity {

    /**
     * Get callFrom
     * @return callFrom
     */
    String getCallFrom();

    /**
     * Get city
     * @return city
     */
    String getCity();

    /**
     * Get contactId
     * @return contactId
     */
    Long getContactId();

    /**
     * Get searchFor
     * @return searchFor
     */
    String getSearchFor();

    /**
     * Get street
     * @return street
     */
    String getStreet();

    /**
     * Get zipcode
     * @return zipcode
     */
    String getZipcode();

    /**
     * Get district
     * @return district
     */
    String getDistrict();
}
