/**
 *
 * Copyright (C) 2011, 2016 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Jul 11, 2011 10:30:08 AM 
 * 
 */
package com.osserp.gui.reporting

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.core.finance.RecordSummary
import com.osserp.core.sales.Sales
import com.osserp.core.sales.SalesListItem

import com.osserp.groovy.web.MenuItem

/**
 *
 * @author eh <eh@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
abstract class AbstractSalesListReportingView extends AbstractReportingView {
    private static Logger logger = LoggerFactory.getLogger(AbstractSalesListReportingView.class.getName())

    boolean exportLink = false
    boolean autosort = true
    boolean closedIgnoreLinkAvailable = false
    boolean ignoreStatus = false
    boolean excludedMode = false
    List excluded = []

    protected AbstractSalesListReportingView() {
        super()
    }

    @Override
    void reload() {
        logger.debug("reload: invoked [values=${values}]")
        loadList()
        scrollPosition = 0
        preFilter()
        if (list) {
            Object fetchedClass = list.get(0)
            logger.debug("reload: load list done [size=${list.size()}, class=${fetchedClass?.getClass()?.getName()}]")
            if (!(fetchedClass instanceof RecordSummary)) {
                List result = []
                list.each {
                    if (filterItem(it)) {
                        result << it
                    }
                }
                excluded = list - result
                list = result
                ignoreStatus = false
            }
        }
        postFilter()
        logger.debug("reload: apply filters done [size=${list.size()}, excluded=${excluded.size()}]")
        if (list && autosort) {
            list.sort{ a, b -> b?.status <=> a?.status }
            actualSortKey = "status"
        }
    }

    protected boolean filterItem(Object it) {
        if (it instanceof SalesListItem) {
            SalesListItem item = it
            boolean branch = values.branch ? values.branch == item.branch?.id : true
            boolean businessType = values.businessType ? values.businessType == item.type?.id  : true
            boolean status = ignoreStatus ? true : (values.closedIgnore && item.closed) ? false :
                (values.statusValue ? (values.statusLower ? item.status <= values.statusValue :
                    item.status > values.statusValue) : true)
            boolean company = values.company ? values.company == item.company?.id : true
            boolean confirmed = ignoreStatus ? true : (values.confirmed != null && !values.confirmedIgnore) ?
                ((values.confirmed && item.status >= Sales.STATUS_CONFIRMED)
                    || (!values.confirmed && item.status < Sales.STATUS_CONFIRMED)) : true
            boolean stopped = values.stopped != null ?
                (values.stopped && item.stopped) || (!values.stopped && !item.stopped) : true
            return (branch && businessType && status && company && confirmed && stopped)
        }
        return true
    }

    @Override
    void createCustomNavigation() {
        super.createCustomNavigation()
        if (exportLink) {
            nav.listNavigation = [nav.listNavigation[0], salesListExportLink ]+ nav.listNavigation[1..-1]
        }
        if (closedIgnoreLinkAvailable) {
            nav.listNavigation = [nav.listNavigation[0], closedIgnoreLink ]+ nav.listNavigation[1..-1]
        }
    }
    
    MenuItem getSalesListExportLink() {
        new MenuItem(
                link: "${context.contextPath}/output/reporting/salesListExport/forward?view=${name}",
                icon: 'tableIcon',
                title: 'displaySheet')
    }
    
    MenuItem getToggleExcluded() {
        if (excludedMode) {
            return navigationLink("/${context.name}/toggleExcluded", 'nosmileIcon', 'listFilteredEntries')
        }
        return navigationLink("/${context.name}/toggleExcluded", 'smileIcon', 'listExcludedEntries')
    }

    MenuItem getClosedIgnoreLink() {
        if (values.closedIgnore) {
            return navigationLink("/${context.name}/toggleClosedIgnore", 'openFolderIcon', 'evenClosed')
        } 
        return navigationLink("/${context.name}/toggleClosedIgnore", 'closedFolderIcon', 'displayOpenOnly')
    }
    
    void toggleClosedIgnore() {
        values.closedIgnore = !values.closedIgnore
        reload()
        nav.listNavigation.each { MenuItem item ->
            if (item.link.indexOf('toggleClosedIgnore') > -1) {
                if (values.closedIgnore) {
                    item.icon = 'openFolderIcon'
                } else {
                    item.icon = 'closedFolderIcon'
                }
            }
        }
    }

    abstract void loadList();

    protected void preFilter() {
        //implement actions before filtering if required
    }

    protected void postFilter() {
        //implement actions after filtering if required
    }
}
