/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 8, 2011 9:34:07 AM 
 * 
 */
package com.osserp.gui.admin.events

import javax.servlet.http.HttpServletRequest

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.common.ClientException

import com.osserp.groovy.web.AdminViewController

/**
 *
 * @author eh <eh@osserp.com>
 * @author rk <rk@osserp.com>
 *
 */
@RequestMapping("/admin/events/eventConfig/*")
@Controller class EventConfigController extends AdminViewController {
    private static Logger logger = LoggerFactory.getLogger(EventConfigController.class.getName())

    @RequestMapping
    def selectEventType(HttpServletRequest request) {
        logger.debug("selectEventType: invoked [user=${getUserId(request)}]")
        EventConfigView view = getView(request)
        view.selectEventType()
        return defaultPage
    }

    @RequestMapping
    def selectRequestType(HttpServletRequest request) {
        logger.debug("selectRequestType: invoked [user=${getUserId(request)}]")
        EventConfigView view = getView(request)
        view.selectRequestType()
        if (!view.form.params.displaySelection && view.form.params.selectionTarget) {
            return view.form.params.selectionTarget
        }
        return defaultPage
    }

    @RequestMapping
    def create(HttpServletRequest request) {
        logger.debug("create: invoked [user=${getUserId(request)}]")
        EventConfigView view = getView(request)
        try {
            view.create()

        } catch (ClientException t) {
            saveError(request, t.message)
        }
        return defaultPage
    }

    @RequestMapping
    def delete(HttpServletRequest request) {
        logger.debug("delete: invoked [user=${getUserId(request)}]")
        EventConfigView view = getView(request)
        view.delete()
        return defaultPage
    }
    
    @RequestMapping
    def synchronizeFcs(HttpServletRequest request) {
        logger.debug("synchronizeFcs: invoked [user=${getUserId(request)}]")
        EventConfigView view = getView(request)
        view.synchronizeFcs()
        return defaultPage
    }

    @RequestMapping
    def togglePaused(HttpServletRequest request) {
        logger.debug("togglePaused: invoked [user=${getUserId(request)}]")
        EventConfigView view = getView(request)
        view.togglePaused()
        return defaultPage
    }
    
}

