/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Apr 21, 2011 11:49:07 AM 
 * 
 */
package com.osserp.groovy.web.tag

import javax.servlet.jsp.JspException
import javax.servlet.jsp.JspWriter
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.util.StringUtil
import com.osserp.groovy.web.WebUtil

/**
 *
 * @author jg <jg@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public class AjaxSearchFormTag extends com.osserp.common.web.tags.AbstractAjaxElementTag {
	private static Logger logger = LoggerFactory.getLogger(AjaxSearchFormTag.class.getName())

	String name
	String events = 'onkeyup'
	int minChars = 2
	String action = 'javascript:;'
	String message = 'pleaseWait'

	@Override
	public int doStartTag() throws JspException {
		String[] eventList = StringUtil.getTokenArray(events)

		if (action != 'javascript:;') {
            action = createAction(action)
		}
		try {
			String functionCall = createFunctionCall()
			StringBuilder buffer = new StringBuilder()
			buffer
            .append('<form')
            .append(' name="').append(name).append('"')
            .append(' id="').append(name).append('"')
            .append(' method="post"')
            .append(' action="').append(action).append('"')
			for (int i=0; i < eventList.length; i++) {
				buffer
                .append(' ')
                .append(eventList[i])
                .append('=')
                .append(functionCall)
				;
			}
			buffer.append('>')
			JspWriter out = pageContext.getOut()
			out.print(buffer.toString())
		} catch (IOException ioe) {
			logger.error("doEndTag: error getting writer [message=${ioe.message}]")
		}
		return (EVAL_BODY_INCLUDE)
	}

	private String createFunctionCall() {
		StringBuilder buffer = new StringBuilder(64)
		buffer.append("\"")
		if(getPreRequest() != null) {
			buffer.append(getPreRequest())
		}
		buffer
        .append(" ojsAjax.ajaxSearchForm('")
        .append(createUrl())
        .append("', '")
        .append(getTargetElement())
        .append("', this, '")
        .append(getActivityElement())
        .append("', '")
        .append(getResourceString(message))
        .append("', ")
        .append(minChars)
        .append(", ")
        .append(getOnSuccess())
        .append("); ")
		if(getPostRequest() != null) {
			buffer.append(getPostRequest())
		}
		buffer.append("\"")
		return buffer.toString()
	}

	@Override
	public int doEndTag() throws JspException {
		try {
			JspWriter out = pageContext.getOut()
			out.print("</form>")
		} catch (IOException ioe) {
			logger.error("doEndTag: error getting writer [message=${ioe.message}]")
		}
		return (EVAL_BODY_INCLUDE)
	}

	@Override
	protected String createUrl() {
		if (url.startsWith(WebUtil.getViewPath(servletContext, request))) {
			return url
		} else {
			return "${WebUtil.getViewPath(servletContext, request)}${url}"
		}
	}

	private String createAction(String action) {
		if (action.startsWith(WebUtil.getViewPath(servletContext, request))) {
			return action
		} else {
			return "${WebUtil.getViewPath(servletContext, request)}${action}"
		}
	}

}

