/**
 *
 * Copyright (C) 2015 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created on 15.05.2015
 *
 */
package com.osserp.api


/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class WsMedia extends WsObject {
    private static final long serialVersionUID = 4L

    Long referenceId
    String referenceContext
    String name
    String mediaType
    String url
}
