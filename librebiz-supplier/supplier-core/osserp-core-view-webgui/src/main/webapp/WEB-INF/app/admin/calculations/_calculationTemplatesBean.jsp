<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="bean" scope="page" value="${view.bean}"/>
<c:set var="calculationTemplate" scope="session" value="${view.bean}"/>

<div class="subcolumns">
	<div class="subcolumn">

		<div class="contentBox">
			<div class="contentBoxData">
				<div class="subcolumns">
					<div class="subcolumn">
						<table class="valueTable">
							<tbody>
								<tr>
									<td><fmt:message key="name"/></td>
									<td><o:out value="${bean.name}"/></td>
								</tr>
                                <tr>
                                    <td><fmt:message key="productNumber"/></td>
                                    <td>
                                        <a href="<c:url value="/products.do?method=load&id=${bean.product.productId}&exit=calculationTemplateDisplay"/>">
                                            <o:out value="${bean.product.productId}"/>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td><fmt:message key="product"/></td>
                                    <td><o:out value="${bean.product.name}"/></td>
                                </tr>
								<tr>
									<td><fmt:message key="businessType"/></td>
									<td><o:out value="${bean.businessType.name}"/></td>
								</tr>
							</tbody>
						</table>
						<div class="spacer"></div>
					</div>
				</div>
			</div>
		</div>

		<div class="spacer"></div>

		<div class="table-responsive table-responsive-default">
			<table class="table table-striped">
				<thead>
					<tr>
						<th class="number right"><fmt:message key="product"/></th>
						<th><fmt:message key="label"/></th>
						<th class="action center"><fmt:message key="option"/></th>
						<th class="action"><fmt:message key="action"/></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="group" items="${view.calculationConfig.groups}">
						<tr>
							<td class="number right"> </td>
							<td class="boldtext">
								<a href="<c:url value="/calculationTemplateProductSearch.do?method=forward&selectedGroup=${group.id}&exit=calculationTemplateDisplay"/>" title="<fmt:message key="productsByPreselection"/>">
									<o:out value="${group.name}"/>
								</a>
							</td>
							<td class="action"> </td>
							<td class="action">
								<a href="<c:url value="/calculationTemplateProductSearch.do?method=forward&selectedGroup=${group.id}&ignorePreselection=true&exit=calculationTemplateDisplay"/>" title="<fmt:message key="productSearch"/>">
									<o:img name="forwardIcon"/>
								</a>
							</td>
						</tr>
						<c:forEach var="item" items="${bean.items}">
							<c:if test="${item.calculationConfigGroupId == group.id}">
								<tr class="lightgrey">
									<td class="number right">
										<a href="<c:url value="/products.do?method=load&id=${item.product.productId}&exit=calculationTemplateDisplay"/>">
											<o:out value="${item.product.productId}"/>
										</a>
									</td>
									<td>
										<o:out value="${item.product.name}"/>
										<c:if test="${!empty item.note}">
											<div class="smalltext"><o:textOut value="${item.note}"/></div>
										</c:if>
									</td>
									<td class="action center">
										<a href="<v:url value="/admin/calculations/calculationTemplates/toggleOptionalItem?id=${item.id}"/>" title="<fmt:message key="toggleOption"/>">
											<c:choose><c:when test="${item.optional}"><o:img name="toggleConfirmedTrueIcon"/></c:when><c:otherwise><o:img name="toggleStoppedFalseIcon"/></c:otherwise></c:choose>
										</a>
									</td>
									<td class="action" >
										<a href="<c:url value="/calculationTemplateProductSearch.do?method=forward&selectedGroup=${group.id}&selectedItemId=${item.id}&exit=calculationTemplateDisplay"/>" title="<fmt:message key="replaceProduct"/>">
											<o:img name="replaceIcon"/>
										</a>
										<a href="<v:url value="/admin/calculations/calculationTemplates/removeItem?id=${item.id}"/>" title="<fmt:message key="deleteProduct"/>">
											<o:img name="deleteIcon"/>
										</a>
									</td>
								</tr>
							</c:if>
						</c:forEach>
					</c:forEach>
				</tbody>
			</table>
		</div>

	</div>
</div>
