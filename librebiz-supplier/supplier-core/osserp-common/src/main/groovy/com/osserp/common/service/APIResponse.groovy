/**
 *
 * Copyright (C) 2020 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package com.osserp.common.service

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class APIResponse implements Serializable {

    Map<String,List<String>> headers = [:]
    int status
    String apiVersion
    String json
    String errorMessage

    boolean isFailed() {
        return status >= 400
    }
}
