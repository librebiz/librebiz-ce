<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>


<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4><fmt:message key="actions"/></h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive table-responsive-default">
            <table class="table table-striped">
                <tbody>
                    <c:forEach var="action" items="${view.list}">
                        <c:if test="${action.status >= 0}">
                            <tr>
                                <td>
                                    <span title="id: ${action.id}">
                                        <v:link url="${view.baseLink}/select?id=${action.id}">
                                            <o:out value="${action.name}" />
                                        </v:link>
                                    </span>
                                </td>
                                <td style="width: 60px; text-align: right;">
                                    <span style="text-align: right; margin-right: 5px;"><o:out value="${action.status}" /></span>
                                </td>
                            </tr>
                        </c:if>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4><fmt:message key="cancellation"/></h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive table-responsive-default">
            <table class="table table-striped">
                <tbody>
                    <c:forEach var="action" items="${view.list}">
                        <c:if test="${action.status < 0}">
                            <tr>
                                <td>
                                    <span title="id: ${action.id}">
                                        <v:link url="${view.baseLink}/select?id=${action.id}">
                                            <o:out value="${action.name}" />
                                        </v:link>
                                    </span>
                                </td>
                                <td style="width: 60px; text-align: right;">
                                    <span style="text-align: right; margin-right: 5px;"><o:out value="${action.status}" /></span>
                                </td>
                            </tr>
                        </c:if>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
