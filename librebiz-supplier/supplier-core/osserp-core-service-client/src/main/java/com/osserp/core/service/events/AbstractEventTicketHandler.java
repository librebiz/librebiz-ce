/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 11, 2007 10:13:24 AM 
 * 
 */
package com.osserp.core.service.events;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.core.dao.Employees;
import com.osserp.core.dao.EventConfigs;
import com.osserp.core.dao.Events;
import com.osserp.core.employees.Employee;
import com.osserp.core.events.EventAction;
import com.osserp.core.events.EventTermination;
import com.osserp.core.events.EventTicket;
import com.osserp.core.service.impl.AbstractService;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class AbstractEventTicketHandler extends AbstractService {
    private static Logger log = LoggerFactory.getLogger(AbstractEventTicketHandler.class.getName());
    private Events events = null;
    private EventConfigs eventConfigs = null;
    private Employees employees = null;

    protected AbstractEventTicketHandler(
            Events events,
            EventConfigs eventConfigs,
            Employees employees) {
        this.employees = employees;
        this.eventConfigs = eventConfigs;
        this.events = events;
    }

    protected List<EventAction> findActionsByCaller(Long eventConfigId, Long callerId) {
        return eventConfigs.findActionsByCaller(eventConfigId, callerId);
    }

    protected List<EventTermination> findTerminationsByCaller(Long eventConfigId, Long callerId) {
        return eventConfigs.findTerminationsByCaller(eventConfigId, callerId);
    }

    protected void executeAction(EventTicket ticket) {
        if (ticket.getConfig() != null) {
            EventAction action = (EventAction) ticket.getConfig();
            if (log.isDebugEnabled()) {
                log.debug("executeAction() invoked [id=" + ticket.getId()
                        + ", action=" + action.getId()
                        + ", reference=" + ticket.getReference()
                        + ", recipientCount="
                        + (ticket.getRecipients() == null ? "null" : ticket.getRecipients().length)
                        + "]");
            }
            if (ticket.getRecipients() != null) {

                for (int i = 0, j = ticket.getRecipients().length; i < j; i++) {
                    Long recipient = ticket.getRecipients()[i];
                    boolean created = events.createEvent(
                            action,
                            ticket.getCreated(),
                            ticket.getCreatedBy(),
                            ticket.getReference(),
                            recipient,
                            ticket.getDescription(),
                            ticket.getMessage(),
                            ticket.getHeadline(),
                            ticket.getParameters());

                    if (!created && log.isDebugEnabled()) {
                        log.debug("executeAction() ignoring already created event [action="
                                + action.getId()
                                + ", reference=" + ticket.getReference()
                                + ", recipient=" + recipient + "]");
                    }
                }
            }
        }
    }

    protected void cancelAction(EventTicket ticket) {
        if (ticket.getConfig() != null) {
            closeEvents(ticket.getCreatedBy(), ticket.getReference(),
                    ticket.getConfig().getId());
            if (log.isDebugEnabled()) {
                log.debug("cancelAction() done [action="
                        + ticket.getConfig().getId()
                        + ", reference="
                        + ticket.getReference()
                        + "]");
            }
        }
    }

    protected void performTerminations(EventTicket ticket) {
        Long callerId = (ticket.getConfig() != null) ? ticket.getConfig().getCallerId() : ticket.getCallerId();
        Long configTypeId = (ticket.getConfig() != null) ? ticket.getConfig().getType().getId() : ticket.getConfigTypeId();
        List<EventTermination> terminations = new java.util.ArrayList<EventTermination>();
        if (configTypeId != null) {
            terminations = eventConfigs.findTerminationsByCaller(configTypeId, callerId);
        }
        if (terminations != null && terminations.size() > 0) {
            if (log.isDebugEnabled()) {
                log.debug("performTerminations() found terminations [count=" + terminations.size() + "]");
            }
            for (int i = 0, j = terminations.size(); i < j; i++) {
                EventTermination termination = terminations.get(i);
                if (log.isDebugEnabled()) {
                    log.debug("performTerminations() terminating [id=" + termination.getId() + "]");
                }
                try {
                    if (ticket.isCanceled()) {
                        restoreClosed(
                                ticket.getReference(),
                                termination.getConfigId());
                    } else {
                        closeEvents(
                                ticket.getCreatedBy(),
                                ticket.getReference(),
                                termination.getConfigId());
                    }
                } catch (Exception e) {
                    log.error("performTerminations() failed [id="
                            + termination.getId()
                            + ", caller="
                            + callerId
                            + ", reference="
                            + ticket.getReference()
                            + "]");
                }
            }
        }
    }

    protected void close(EventTicket ticket) {
        events.closeTicket(ticket);
        if (log.isDebugEnabled()) {
            log.debug("close() done [" + (ticket == null ? "null" : ticket.getId()) + "]");
        }
    }

    protected void closeAllEvents(Long employeeId, Long reference) {
        events.closeAll(employeeId, reference);
    }

    protected void closeEvents(Long employeeId, Long reference, Long config) {
        events.closeEvents(employeeId, reference, config);
    }

    protected void restoreClosed(Long reference, Long config) {
        events.restoreClosed(reference, config);
    }

    protected Employee fetchEmployee(Long id) {
        return employees.get(id);
    }

    protected Events getEvents() {
        return events;
    }

    protected EventConfigs getEventConfigs() {
        return eventConfigs;
    }
}
