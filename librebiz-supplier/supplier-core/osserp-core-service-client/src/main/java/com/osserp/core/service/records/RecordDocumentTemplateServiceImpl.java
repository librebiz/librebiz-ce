/**
 *
 * Copyright (C) 2015 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 12, 2015 
 * 
 */
package com.osserp.core.service.records;

import org.jdom2.Document;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.dms.TemplateDmsManager;

import com.osserp.core.BusinessCase;
import com.osserp.core.BusinessTemplate;
import com.osserp.core.dao.BusinessTemplates;
import com.osserp.core.dao.SystemConfigs;
import com.osserp.core.dms.CoreStylesheetService;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordDocumentTemplateService;
import com.osserp.core.finance.RecordOutputService;
import com.osserp.core.users.DomainUser;
import com.osserp.core.service.doc.AbstractBusinessTemplateManager;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RecordDocumentTemplateServiceImpl extends AbstractBusinessTemplateManager
    implements RecordDocumentTemplateService {

    private static Logger log = LoggerFactory.getLogger(
            RecordDocumentTemplateServiceImpl.class.getName());
    
    private RecordOutputService recordOutputService = null;

    public RecordDocumentTemplateServiceImpl(
            BusinessTemplates businessTemplates, 
            CoreStylesheetService stylesheetService, 
            TemplateDmsManager templateDmsManager, 
            SystemConfigs systemConfigs,
            RecordOutputService recordOutputService) {
        super(businessTemplates, stylesheetService, templateDmsManager, systemConfigs);
        this.recordOutputService = recordOutputService;
    }

    public byte[] createPdf(
            DomainUser domainUser, 
            BusinessTemplate businessTemplate, 
            Record record, 
            BusinessCase businessCase) throws ClientException {
        assert (domainUser != null && businessTemplate != null && record != null);
        if (log.isDebugEnabled()) {
            log.debug("createPdf() invoked [user="
                    + domainUser.getId()
                    + ", template=" + businessTemplate.getId()
                    + ", record=" + record.getId()
                    + ", businessCase=" 
                    + (businessCase == null ? "null" : businessCase.getPrimaryKey())
                    + "]");
        }

        Document document = recordOutputService.createDocument(
                domainUser.getEmployee(), record, false);
        if (businessCase != null) {
            recordOutputService.addBusinessCase(document, businessCase);
        }
        Long branchId = record.getBranchId();
        if (businessCase != null && businessCase.getBranch() != null
                && businessCase.getBranch().getId() != null
                && !businessCase.getBranch().getId().equals(branchId)) {
            
            branchId = businessCase.getBranch().getId();
            if (log.isDebugEnabled()) {
                log.debug("createPdf() overriding branch by businessCase [record="
                        + ", record=" + record.getId()
                        + ", recordBranch=" + record.getId()
                        + ", businessCase=" + businessCase.getPrimaryKey()
                        + ", businessCaseBranch=" + branchId
                        + "]");
            }
        }
        logXML(document);
        return createPdf(
                businessTemplate, 
                document, 
                branchId, 
                Constants.locale(record.getLanguage())); 
    }

}
