<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="action" value="${view.bean}"/>

<v:form name="eventConfigForm" url="/admin/events/eventConfig/save">
    <input type="hidden" name="terminationTime" value="<o:out value="${action.terminationTime}"/>"/>
    <input type="hidden" name="terminationAlert" value="<o:out value="${action.terminationAlert}"/>"/>

    <div class="col-md-6 panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>
                    <o:out value="${action.id}"/> - <o:out value="${action.name}"/>
                </h4>
            </div>
        </div>
        <div class="panel-body">

            <div class="row next">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="jobName"><fmt:message key="jobName" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <input type="text" class="form-control" name="name" value="<o:out value="${action.name}"/>"/>
                    </div>
                </div>
            </div>

            <c:if test="${view.fcsMode}">
                <div class="row next">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="fcsAction"><fmt:message key="fcsAction"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <select class="form-control" name="callerId" size="1">
                                <c:forEach var="caller" items="${view.flowControls}">
                                    <c:choose>
                                        <c:when test="${action.callerId == caller.id}">
                                            <option value="<o:out value="${caller.id}"/>" selected="selected"><o:out value="${caller.name}"/></option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="<o:out value="${caller.id}"/>"><o:out value="${caller.name}"/></option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row next">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="scheduledThrough"><fmt:message key="scheduledThrough"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <select class="form-control" draggable="true" name="terminationEventIds" multiple="multiple" size="3" style="height:72px;">
                                <c:forEach var="terminator" items="${view.flowControls}">
                                    <o:contains list="${action.closings}" value="${terminator.id}" property="callerId">
                                        <option value="<o:out value="${terminator.id}"/>" selected="selected"><o:out value="${terminator.name}"/></option>
                                    </o:contains>
                                    <o:notContains list="${action.closings}" value="${terminator.id}" property="callerId">
                                        <option value="<o:out value="${terminator.id}"/>"><o:out value="${terminator.name}"/></option>
                                    </o:notContains>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </div>
            </c:if>

            <div class="row next">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="manuallySchedulable"><fmt:message key="manuallySchedulable" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${action.closeableByTarget}">
                                <input type="checkbox" name="closeableByTarget" checked="checked" />
                            </c:when>
                            <c:otherwise>
                                <input type="checkbox" name="closeableByTarget" />
                            </c:otherwise>
                        </c:choose>
                        <span class="smalltext">(<fmt:message key="doNotSetWithFcsJobs"/>!)</span>
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="jobIsInformation"><fmt:message key="jobIsInformation" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${action.info}">
                                <input type="checkbox" name="info" checked="checked" />
                            </c:when>
                            <c:otherwise>
                                <input type="checkbox" name="info" />
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="priority"><fmt:message key="priority" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <select class="form-control" name="priority">
                            <c:choose>
                                <c:when test="${action.priority == 1}">
                                    <option value="1" selected="selected"><fmt:message key="high"/></option>
                                </c:when>
                                <c:otherwise>
                                    <option value="1"><fmt:message key="high"/></option>
                                </c:otherwise>
                            </c:choose>
                            <c:choose>
                                <c:when test="${action.priority == 2}">
                                    <option value="2" selected="selected"><fmt:message key="medium"/></option>
                                </c:when>
                                <c:otherwise>
                                    <option value="2"><fmt:message key="medium"/></option>
                                </c:otherwise>
                            </c:choose>
                            <c:choose>
                                <c:when test="${action.priority == 3}">
                                    <option value="3" selected="selected"><fmt:message key="low"/></option>
                                </c:when>
                                <c:otherwise>
                                    <option value="3"><fmt:message key="low"/></option>
                                </c:otherwise>
                            </c:choose>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="typeOfReceiver"><fmt:message key="typeOfReceiver" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${action.sendSales}">
                                <input type="radio" name="sendId" onchange="checkRecipient(this.value);" value="1" checked="checked"/><fmt:message key="salesman"/>
                                <input type="radio" name="sendId" onchange="checkRecipient(this.value);" value="2"/><fmt:message key="technics"/>
                                <input type="radio" name="sendId" onchange="checkRecipient(this.value);" value="3"/><fmt:message key="pool"/>
                            </c:when>
                            <c:when test="${action.sendManager}">
                                <input type="radio" name="sendId" onchange="checkRecipient(this.value);" value="1"/><fmt:message key="salesman"/>
                                <input type="radio" name="sendId" onchange="checkRecipient(this.value);" value="2" checked="checked"/><fmt:message key="technics"/>
                                <input type="radio" name="sendId" onchange="checkRecipient(this.value);" value="3"/><fmt:message key="pool"/>
                            </c:when>

                            <c:when test="${action.sendPool}">
                                <input type="radio" name="sendId" onchange="checkRecipient(this.value);" value="1"/><fmt:message key="salesman"/>
                                <input type="radio" name="sendId" onchange="checkRecipient(this.value);" value="2"/><fmt:message key="technics"/>
                                <input type="radio" name="sendId" onchange="checkRecipient(this.value);" value="3" checked="checked"/><fmt:message key="pool"/>
                            </c:when>

                            <c:otherwise>
                                <input type="radio" name="sendId" onchange="checkRecipient(this.value);" value="1"/><fmt:message key="salesman"/>
                                <input type="radio" name="sendId" onchange="checkRecipient(this.value);" value="2"/><fmt:message key="technics"/>
                                <input type="radio" name="sendId" onchange="checkRecipient(this.value);" value="3"/><fmt:message key="pool"/>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <div class="row next" id="pool" <c:if test="${!action.sendPool}">style="visibility: hidden;"</c:if>>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="recipient"><fmt:message key="recipient" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <select class="form-control" name="poolId" size="1">
                            <option value="0"><fmt:message key="undefined"/>!</option>
                            <c:forEach var="pool" items="${view.pools}">
                                <c:choose>
                                    <c:when test="${empty action.pool}">
                                        <option value="<o:out value="${pool.id}"/>"><o:out value="${pool.name}"/></option>
                                    </c:when>
                                    <c:when test="${action.pool.id == pool.id}">
                                        <option value="<o:out value="${pool.id}"/>" selected="selected"><o:out value="${pool.name}"/></option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="<o:out value="${pool.id}"/>"><o:out value="${pool.name}"/></option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row next" id="poolSettings" <c:if test="${!action.sendPool}">style="visibility: hidden;"</c:if>>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="ignoreBranchoffice"><fmt:message key="ignoreBranchoffice" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${action.ignoringBranch}">
                                <input type="checkbox" name="ignoringBranch" checked="checked" />
                            </c:when>
                            <c:otherwise>
                                <input type="checkbox" name="ignoringBranch" />
                            </c:otherwise>
                        </c:choose>
                        <span class="smalltext"><fmt:message key="sendToAllReceiversInPool"/></span>
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="completionAfter"><fmt:message key="completionAfter" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <input class="inputDays" type="text" name="terminationTimeMillis" value="<o:days value="${action.terminationTime}"/>" onchange="setMsDisplay(this.value, 'terminationTimeMillis'); setMsEdit(this.value, 'eventConfigForm','terminationTime');" />
                        <fmt:message key="days"/> / <span id="terminationTimeMillis"><o:out value="${action.terminationTime}"/></span> ms
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="escalationSupport"><fmt:message key="escalationSupport" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${action.sendAlert}">
                                <input type="checkbox" name="sendAlert" checked="checked" />
                            </c:when>
                            <c:otherwise>
                                <input type="checkbox" name="sendAlert" />
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="escalationAfter"><fmt:message key="escalationAfter" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <input class="inputDays" type="text" name="terminationAlertMillis" value="<o:days value="${action.terminationAlert}"/>" onchange="setMsDisplay(this.value, 'terminationAlertMillis'); setMsEdit(this.value, 'eventConfigForm','terminationAlert');" />
                        <fmt:message key="days"/> / <span id="terminationAlertMillis"><c:choose><c:when test="${empty action.terminationAlert}">0</c:when><c:otherwise><o:out value="${action.terminationAlert}"/></c:otherwise></c:choose></span> ms
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="escalationStrategy"><fmt:message key="escalationStrategy" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <select class="form-control" name="alertPoolId" size="1">
                            <c:if test="${empty action.alertEvent}">
                                <option value="0" selected="selected"><fmt:message key="undefined"/>!</option>
                            </c:if>
                            <c:forEach var="pool" items="${view.pools}">
                                <c:choose>
                                    <c:when test="${action.alertEvent.pool.id == pool.id}">
                                        <option value="<o:out value="${pool.id}"/>" selected="selected"><o:out value="${pool.name}"/></option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="<o:out value="${pool.id}"/>"><o:out value="${pool.name}"/></option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-4"></div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <v:submitExit url="/admin/events/eventConfig/disableEditMode" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <o:submit styleClass="form-control" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

		</div>
	</div>
</v:form>
