<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="salesReceivablesView"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="title"><o:displayTitle/></tiles:put>
	<tiles:put name="styles" type="string">
		<style type="text/css">
			<c:import url="/css/request_list.css"/>
		</style>
	</tiles:put>
	<tiles:put name="headline"><o:listSize value="${view.list}"/> <fmt:message key="ordersWithOpenBillsFoundTotalAmount"/> <o:number value="${view.totalAmount}" format="currency"/></tiles:put>
	<tiles:put name="headline_right">
		<ul>
			<li><v:link url="/sales/salesReceivables/exit"><o:img name="backIcon"/></v:link></li>
			<li><v:link url="/sales/salesReceivables/reload" title="listRefresh"><o:img name="replaceIcon"/></v:link></li>
			<li><v:link url="/sales/salesReceivables/printXls" title="printXls"><o:img name="tableIcon"/></v:link></li>
			<li><v:link url="/sales/salesReceivables/print" title="printPdf"><o:img name="printIcon"/></v:link></li>
			<li><v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link></li>
		</ul>
	</tiles:put>
	<tiles:put name="content" type="string">
        <div class="content-area" id="salesReceivablesContent">
            <div class="row">
                <div class="col-lg-12 panel-area">

                    <div class="table-responsive table-responsive-default">
                        <table class="table table-striped">
                            <thead>
                                <tr>
								    <th><fmt:message key="number"/></th>
								    <th><fmt:message key="type"/></th>
								    <th><fmt:message key="customer"/></th>
								    <th><fmt:message key="from"/></th>
								    <th class="right"><span><fmt:message key="statusPercent"/></span></th>
								    <th><fmt:message key="salesShortcut"/></th>
								    <th><fmt:message key="technicianShortcut"/></th>
								    <th class="right"><fmt:message key="open"/></th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="sales" items="${view.list}"	varStatus="s">
                                    <tr id="sales${sales.id}">
                                        <td><a href="<c:url value="/loadSales.do?id=${sales.id}&exit=salesReceivables&exitId=sales${sales.id}"/>" title="<o:out value="${project.type.name}"/>"><o:out value="${sales.id}"/></a></td>
                                        <td><oc:options name="requestTypeKeys" value="${sales.type}"/></td>
                                        <td><a href="<c:url value="/loadSales.do?id=${sales.id}&exit=salesReceivables&exitId=sales${sales.id}"/>" title="<o:out value="${sales.customerName}"/> / <o:out value="${sales.city}"/>"><o:out value="${sales.name}"/></a></td>
                                        <td><o:date value="${sales.created}" casenull="-" addcentury="false"/></td>
                                        <td class="right"><v:ajaxLink url="/sales/businessCaseDisplay/forward?id=${sales.id}" linkId="businessCaseDisplayView"><o:out value="${sales.status}"/></v:ajaxLink></td>
                                        <td><o:ajaxLink linkId="createdDisplay" url="${'/employeeInfo.do?id='}${sales.salesId}"><oc:employee initials="true" value="${sales.salesId}" /></o:ajaxLink></td>
                                        <td><o:ajaxLink linkId="createdDisplay" url="${'/employeeInfo.do?id='}${sales.managerId}"><oc:employee initials="true" value="${sales.managerId}" /></o:ajaxLink></td>
                                        <td class="right"><o:number value="${sales.amount}" format="currency"/></td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
				</div>
			</div>
		</div>
	</tiles:put>
</tiles:insert>
