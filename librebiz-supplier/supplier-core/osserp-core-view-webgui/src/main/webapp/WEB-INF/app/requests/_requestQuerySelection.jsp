<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="requestQueryIndexView" />

<div class="col-md-6 panel-area panel-area-default">

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <c:choose>
                    <c:when test="${empty view.selectedQueryType}">
                        <fmt:message key="selection" />
                    </c:when>
                    <c:otherwise>
                        <v:link url="/requests/requestQueryIndex/selectQuery">
                            <fmt:message key="${view.selectedQueryType}" />
                        </v:link>
                    </c:otherwise>
                </c:choose>
            </h4>
        </div>
    </div>
    <div class="panel-body">

        <div class="table-responsive table-responsive-default">
            <table class="table">
                <tbody>
                    <c:choose>
                        <c:when test="${empty view.selectedQueryType}">
                            <tr>
                                <td><v:link url="/requests/requestQueryIndex/selectQuery?type=currentRequests"> 
                                        <fmt:message key="currentRequests" /></v:link></td>
                            </tr>
                            <c:forEach var="item" items="${view.availableQueries}">
                                <tr>
                                    <td><v:link url="/requests/requestQueryIndex/selectQuery?type=${item}"> 
                                        <fmt:message key="${item}" /></v:link></td>
                                </tr>
                            </c:forEach>
                        </c:when>
                        <c:otherwise>
                            <c:forEach var="item" items="${view.queryParams}">
                                <tr>
                                    <td><v:link url="/requests/requestQueryIndex/executeQuery?id=${item.id}"> 
                                        <o:out value="${item.name}" /></v:link></td>
                                </tr>
                            </c:forEach>
                        </c:otherwise>
                    </c:choose>
                </tbody>
            </table>
        </div>
    </div>
</div>
