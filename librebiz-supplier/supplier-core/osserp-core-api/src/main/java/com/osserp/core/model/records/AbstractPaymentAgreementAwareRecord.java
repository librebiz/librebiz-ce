/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 12, 2009 1:22:05 PM 
 * 
 */
package com.osserp.core.model.records;

import java.util.ArrayList;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.core.ItemPosition;
import com.osserp.core.contacts.ContactPaymentAgreement;
import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.PaymentAgreementAwareRecord;
import com.osserp.core.finance.PaymentCondition;
import com.osserp.core.finance.RecordDiscount;
import com.osserp.core.finance.RecordPaymentAgreement;
import com.osserp.core.finance.RecordType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractPaymentAgreementAwareRecord extends AbstractRecord implements PaymentAgreementAwareRecord {
    private List<RecordPaymentAgreement> paymentAgreements = new ArrayList<RecordPaymentAgreement>();
    private List<RecordDiscount> discountHistory = new ArrayList<RecordDiscount>();

    /**
     * Default constructor required by Serializable
     */
    protected AbstractPaymentAgreementAwareRecord() {
        super();
    }

    /**
     * Creates a new payment aware record with customized change-item flags
     * @param id
     * @param company
     * @param branchId
     * @param type
     * @param reference
     * @param contact
     * @param createdBy
     * @param sale
     * @param taxRate
     * @param reducedTaxRate
     * @param itemsChangeable
     * @param itemsEditable
     */
    protected AbstractPaymentAgreementAwareRecord(
            Long id,
            Long company,
            Long branchId,
            RecordType type,
            Long reference,
            ClassifiedContact contact,
            Employee createdBy,
            Long sale,
            Double taxRate,
            Double reducedTaxRate,
            boolean itemsChangeable,
            boolean itemsEditable) {
        super(
                id,
                company,
                branchId,
                type,
                reference,
                contact,
                createdBy,
                sale,
                taxRate,
                reducedTaxRate,
                itemsChangeable,
                itemsEditable);
    }

    /**
     * Creates a new payment aware record by calculated items.
     * @param id
     * @param company
     * @param branchId
     * @param type
     * @param reference
     * @param contact
     * @param createdBy
     * @param taxRate
     * @param reducedTaxRate
     * @param sale
     * @param items
     * @param optionalItems
     */
    protected AbstractPaymentAgreementAwareRecord(
            Long id,
            Long company,
            Long branchId,
            RecordType type,
            Long reference,
            ClassifiedContact contact,
            Employee createdBy,
            Double taxRate,
            Double reducedTaxRate,
            Long sale,
            List<ItemPosition> items,
            List<ItemPosition> optionalItems) {
        super(id, company, branchId, type, reference, contact, createdBy, sale,
                taxRate, reducedTaxRate, items, optionalItems);
    }

    protected abstract RecordPaymentAgreement createPaymentAgreement();

    public final RecordPaymentAgreement getPaymentAgreement() {
        return (paymentAgreements.isEmpty() ? null : paymentAgreements.get(0));
    }

    public void updatePaymentAgreement(
            boolean hidePayments,
            boolean amountsFixed,
            Double downpayment,
            Double deliveryInvoice,
            Double finalInvoice,
            Long downpaymentTargetId,
            Long deliveryInvoiceTargetId,
            Long finalInvoiceTargetId,
            PaymentCondition paymentCondition,
            String note,
            Long changedBy)
            throws ClientException {

        if (paymentAgreements.isEmpty()) {
            paymentAgreements.add(createPaymentAgreement());
        }
        paymentAgreements.get(0).update(
                amounts,
                hidePayments,
                amountsFixed,
                downpayment,
                deliveryInvoice,
                finalInvoice,
                downpaymentTargetId,
                deliveryInvoiceTargetId,
                finalInvoiceTargetId,
                paymentCondition,
                note,
                changedBy);
    }

    /**
     * Overrides current payment agreement settings with contact payment agreement settings
     * @param paymentAgreement
     * @throws ClientException if validation failed
     */
    public void updatePaymentAgreement(ContactPaymentAgreement paymentAgreement) throws ClientException {

        if (paymentAgreements.isEmpty()) {
            paymentAgreements.add(createPaymentAgreement());
        }
        RecordPaymentAgreement src = paymentAgreements.get(0);
        boolean amountsFixed = (isSet(paymentAgreement.getDownpaymentPercent())
                || isSet(paymentAgreement.getDeliveryInvoicePercent())
                ? false : true);
        src.update(
                amounts,
                paymentAgreement.isHidePayments(),
                amountsFixed,
                paymentAgreement.getDownpaymentPercent(),
                paymentAgreement.getDeliveryInvoicePercent(),
                paymentAgreement.getFinalInvoicePercent(),
                paymentAgreement.getDownpaymentTargetId(),
                paymentAgreement.getDeliveryInvoiceTargetId(),
                paymentAgreement.getFinalInvoiceTargetId(),
                paymentAgreement.getPaymentCondition(),
                paymentAgreement.getNote(),
                Constants.SYSTEM_EMPLOYEE);
    }

    public void switchPaymentFixedAmountsFlag() {
        if (paymentAgreements.isEmpty()) {
            paymentAgreements.add(createPaymentAgreement());
        }
        RecordPaymentAgreement src = paymentAgreements.get(0);
        src.setAmountsFixed(!src.isAmountsFixed());
    }

    public void switchPaymentHidePaymentsFlag() {
        if (paymentAgreements.isEmpty()) {
            paymentAgreements.add(createPaymentAgreement());
        }
        RecordPaymentAgreement src = paymentAgreements.get(0);
        src.toggleHidePayments();
    }

    protected List<RecordPaymentAgreement> getPaymentAgreements() {
        return paymentAgreements;
    }

    protected void setPaymentAgreements(
            List<RecordPaymentAgreement> paymentAgreements) {
        this.paymentAgreements = paymentAgreements;
    }

    public void addDiscount(Employee employee, Double discount, String note) {
        RecordDiscount obj = createDiscount(employee, discount, note);
        if (obj != null) {
            discountHistory.add(obj);
        }
    }

    /**
     * Override this if implementing class supports discount
     * @param employee
     * @param discount
     * @param note
     * @return null by default
     */
    protected RecordDiscount createDiscount(Employee employee, Double discount, String note) {
        return null;
    }

    public Double getDiscount() {
        if (discountHistory.isEmpty()) {
            return 0d;
        }
        return discountHistory.get(0).getDiscount();
    }

    public List<RecordDiscount> getDiscountHistory() {
        return discountHistory;
    }

    protected void setDiscountHistory(List<RecordDiscount> discountHistory) {
        this.discountHistory = discountHistory;
    }

}
