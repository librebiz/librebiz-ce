<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>


<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="queries" />
                <fmt:message key="and" />
                <fmt:message key="actions" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-striped">
                <tbody>

                    <oc:systemPropertyEnabled name="stockManagement">
                        <o:permission role="purchasing,customer_service,accounting,executive" info="stockOnHand">
                            <tr>
                                <td>
                                    <v:link url="/products/productStockQueries/forward?exit=/accounting/accountingIndex/forward">
                                        <fmt:message key="queryForStock" />
                                    </v:link>
                                </td>
                            </tr>
                        </o:permission>
                        <o:forbidden role="purchasing,customer_service,accounting,executive" info="stockOnHand">
                            <tr>
                                <td><fmt:message key="queryForStock" /></td>
                            </tr>
                        </o:forbidden>
                    </oc:systemPropertyEnabled>

                    <o:permission role="customer_service,accounting,accounting_accountant,executive" info="permissionDisplayAccounting">
                        <tr>
                            <td><a href="<c:url value="/salesInvoiceReminder.do?method=forward"/>"><fmt:message key="outstandingPayments" /></a></td>
                        </tr>
                    </o:permission>
                    <o:forbidden role="customer_service,accounting,accounting_accountant,executive" info="permissionDisplayAccounting">
                        <tr>
                            <td><fmt:message key="outstandingPayments" /></td>
                        </tr>
                    </o:forbidden>

                    <o:permission role="accounting_documents,accounting_bank_documents,executive_accounting,organisation_admin,executive" info="permissionCreateBookingCommon">
                        <tr>
                            <td>
                                <v:link url="/accounting/accountTransaction/forward?exit=/accounting/accountingIndex/forward">
                                    <fmt:message key="createBooking" />
                                </v:link>
                            </td>
                        </tr>
                    </o:permission>
                    <o:forbidden role="accounting_documents,accounting_bank_documents,executive_accounting,organisation_admin,executive" info="permissionCreateBookingCommon">
                        <tr>
                            <td><fmt:message key="createBooking" /></td>
                        </tr>
                    </o:forbidden>

                    <o:permission role="customer_service,accounting_documents,accounting_bank_documents,executive_accounting,organisation_admin,executive" info="permissionCreateBookingCommon">
                        <tr>
                            <td>
                                <v:link url="/dms/documentImport/forward?exit=/accounting/accountingIndex/forward">
                                    <fmt:message key="documentImportLinkLabel" />
                                </v:link>
                            </td>
                        </tr>
                    </o:permission>
                    <o:forbidden role="customer_service,accounting_documents,accounting_bank_documents,executive_accounting,organisation_admin,executive" info="permissionCreateBookingCommon">
                        <tr>
                            <td><fmt:message key="documentImportLinkLabel" /></td>
                        </tr>
                    </o:forbidden>

                    <o:permission role="accounting_documents,accounting_bank_documents,record_export,organisation_admin" info="permissionDisplayAccounting">
                        <o:viewcreate view="com.osserp.gui.client.sales.SalesVolumeInvoiceView" />
                        <tr>
                            <td class="left"><a href="<c:url value="/recordExports.do?method=forward"/>"><fmt:message key="recordExport" /></a></td>
                        </tr>
                        <oc:systemPropertyEnabled name="volumeInvoiceSupport">
                            <c:set var="volumeInvoiceView" value="${sessionScope.salesVolumeInvoiceView}" />
                            <c:forEach var="obj" items="${volumeInvoiceView.configs}">
                                <tr>
                                    <td class="left"><a href="<c:url value="/salesVolumeInvoice.do"><c:param name="method" value="selectConfig"/><c:param name="id" value="${obj.id}"/></c:url>"><o:out value="${obj.name}" /></a></td>
                                </tr>
                            </c:forEach>
                        </oc:systemPropertyEnabled>
                    </o:permission>
                    <o:forbidden role="accounting_documents,accounting_bank_documents,record_export,organisation_admin" info="permissionDisplayAccounting">
                        <tr>
                            <td class="left"><fmt:message key="recordExport" /></td>
                        </tr>
                    </o:forbidden>

                    <c:if test="${view.stockManagementEnabled && view.stocktakingSupportEnabled}">
                        <o:permission role="accounting,purchasing,organisation_admin" info="permissionDisplayStockTaking">
                            <tr>
                                <td>
                                    <a href="<c:url value="/stocktakings.do?method=forward&id=100"/>"><fmt:message key="stockTaking" /></a>
                                </td>
                            </tr>
                        </o:permission>
                    </c:if>

                </tbody>
            </table>
        </div>
    </div>
</div>
