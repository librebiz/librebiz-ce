<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.salesInvoiceView or empty sessionScope.salesInvoiceView.bean}">
<o:logger write="no view bound [salesInvoiceView]" level="warn"/>
<c:redirect url="/errors/error_context.jsp"/>
</c:if>
<c:set var="recordView" scope="session" value="${sessionScope.salesInvoiceView}"/>
<c:set var="recordView" value="${sessionScope.recordView}"/>
<c:set var="record" value="${recordView.record}"/>
<c:set var="baseUrl" scope="request" value="/salesInvoice.do"/>
<c:set var="recipientUrl" scope="request" value="/contacts/contactSearch/forward?exit=/salesInvoiceDisplay.do&selectionTitle=changeInvoiceRecipient&selectionTarget=/salesInvoiceRecipientChange.do&context=customer"/>
<c:set var="recipientUrlTitle" scope="request" value="changeInvoiceRecipient"/>
<c:set var="infoUrl" scope="request" value="/salesInvoiceInfos.do"/>
<c:set var="creditNoteUrl" scope="request" value="/salesCreditNote.do"/>
<c:set var="creditNoteExitTarget" scope="request" value="invoice"/>
<c:set var="productExitTarget" scope="request" value="salesInvoiceReload"/>
<c:set var="productUrl" scope="request" value="/salesInvoiceProducts.do"/>
<c:set var="correctionUrl" scope="request" value="/salesInvoice.do?method=printCorrection"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
<tiles:put name="title"><o:displayTitle/></tiles:put>
<tiles:put name="styles" type="string">
<style type="text/css">
<c:import url="/css/records.css"/>
</style>
</tiles:put>

<tiles:put name="headline">
    <c:if test="${record.canceled}"><span class="error"><fmt:message key="cancelled"/>: </span></c:if>
    <c:import url="/pages/sales/records/voucher_header.jsp"/>
</tiles:put>

<c:choose>
<c:when test="${recordView.setupMode}">
<tiles:put name="headline_right">
<a href="<c:url value="/salesInvoice.do?method=disableSetup"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon" /></a>
<v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link>
</tiles:put>
<tiles:put name="content" type="string">
<c:import url="/pages/sales/records/invoice_settings.jsp"/>
</tiles:put> 
</c:when>

<c:otherwise>
<tiles:put name="headline_right">
<a href="<c:url value="/salesInvoice.do?method=exit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon" /></a>
<o:permission role="${recordView.accountingPermissions}" info="permissionPaymentAdd">
<c:if test="${record.unchangeable}">
<v:link url="/sales/salesInvoicePayment/forward" title="paymentsEdit"><o:img name="moneyIcon" /></v:link>
<v:link url="/sales/salesRecordCreator/forward?record=${record.id}&type=${record.type.id}&exit=${recordView.selectionTarget}&recordExit=${recordView.exitTarget}" title="createByCopy"><o:img name="copyIcon" /></v:link>
</c:if>
</o:permission>
<v:link url="/sales/salesInvoiceDocument/forward?exit=/salesInvoice.do?method=reload" title="documents"><o:img name="docIcon" /></v:link>
<c:choose>
<c:when test="${!record.historical}">
<c:if test="${!record.unchangeable and !empty record.items}">
<v:link url="/records/recordPrint/release?view=salesInvoiceView" confirm="true" message="confirmRecordRelease" title="recordRelease"><o:img name="enabledIcon" /></v:link>
</c:if>
<v:link url="/records/recordPrint/print?view=salesInvoiceView" title="documentPrint"><o:img name="printIcon" /></v:link>
</c:when>
<c:otherwise>
<c:if test="${!empty recordView.document}">
<c:if test="${record.itemsChangeable or record.itemsEditable}">
<a href="<c:url value="/salesInvoice.do?method=release"/>" title="<fmt:message key="release"/>"><o:img name="enabledIcon" /></a>
</c:if>
<v:link url="/records/recordPrint/print?view=salesInvoiceView" title="documentPrint"><o:img name="printIcon" /></v:link>
</c:if>
</c:otherwise>
</c:choose>
<c:if test="${!record.historical and record.unchangeable}">
<v:link url="/records/recordMail/forward?view=salesInvoiceView&exit=/salesInvoice.do?method=reload" title="sendViaEmail"><o:img name="mailIcon" /></v:link>
</c:if>
<c:if test="${!recordView.readOnlyMode}">
<c:if test="${!record.unchangeable or (record.historical and (record.itemsChangeable or record.itemsEditable))}">
<a href="<c:url value="/salesInvoice.do?method=enableEdit"/>" title="<fmt:message key="change"/>"><o:img name="writeIcon" /></a>
</c:if>
<c:if test="${recordView.recordDeletable}">
<a href="<c:url value="/salesInvoice.do?method=forwardDelete"/>" title="<fmt:message key="delete"/>"><o:img name="deleteIcon" /></a>
</c:if>
</c:if>
<c:if test="${record.contact.discountEnabled or record.rebateAvailable 
	or (!record.historical and record.unchangeable)}">
<a href="<c:url value="/salesInvoice.do?method=enableSetup"/>" title="<fmt:message key="settings"/>"><o:img name="configureIcon" /></a>
</c:if>
<v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link>
</tiles:put>

<tiles:put name="content" type="string">
<div class="subcolumns">
    <div class="subcolumn">
        <div class="spacer"></div>
                
        <div class="contentBox">
            <div class="contentBoxData">
                <div class="subcolumns">
                    
                    <div class="contentBoxHeader">
                        <c:import url="/pages/records/record_header.jsp"/>
                    </div>
                    <div class="column66l">
                        <c:set var="recipientChangeEnabled" scope="request" value="${!record.downpayment}"/>
                        <c:import url="/pages/records/record_recipient_display.jsp"/>
                        <c:import url="/pages/records/record_place_of_performance.jsp"/>
                        <c:import url="/pages/records/record_sigs_display.jsp"/>
                        <c:import url="/pages/records/record_shipping_display.jsp"/>
                        <c:import url="/pages/records/invoice_payment_display.jsp"/>
                        <c:import url="/pages/records/record_taxfree_display.jsp"/>
                        <c:import url="/pages/records/record_custom_header_display.jsp"/>
                        <c:import url="/pages/records/record_embedded_text.jsp"/>
                        <c:if test="${!record.type.displayNotesBelowItems}">
                            <c:import url="/pages/records/record_note_display.jsp"/>
                        </c:if>
                        <br />
                    </div>
                    <div class="column33r">
                        <c:import url="/pages/records/record_status.jsp"/>
                        <c:import url="/pages/records/invoice_taxpoint_display.jsp"/>
                        <c:if test="${record.canceled and !empty record.cancellation}">
                            <c:choose>
                                <c:when test="${record.cancellation.unchangeable}">
                                    <div class="recordLabelValue">
                                        <a href="<c:url value="/salesCancellation.do?method=forward&id=${record.id}&exit=salesInvoiceView&canceledRecordView=salesInvoiceView"/>">
                                            <fmt:message key="displayReverseDocument"/>
                                        </a>
                                    </div>
                                    <c:if test="${recordView.canceledCustomerCredit > 0}">
                                        <div class="recordLabelValue">
                                            <fmt:message key="credit"/>: <o:number value="${recordView.canceledCustomerCredit}" format="currency"/>
                                            <o:permission role="accounting,executive" info="permissionPayoutAmount">
                                                <a href="javascript:onclick=confirmLink('<fmt:message key="promptConfirmPayout"/>','<c:url value="/salesInvoice.do"><c:param name="method" value="payoutCanceled"/></c:url>');" title="<fmt:message key="payoutAmount"/>">[<fmt:message key="promptPayout"/>]</a>
                                            </o:permission>
                                        </div>
                                    </c:if>
                                </c:when>
                                <c:otherwise>
                                    <div class="recordLabelValue">
                                        <span class="error"><fmt:message key="notReleasedLabel"/></span>:
                                        <a href="<c:url value="/salesCancellation.do?method=forward&id=${record.id}&exit=salesInvoiceView&canceledRecordView=salesInvoiceView"/>">
                                            <fmt:message key="salesCancellation"/>
                                        </a>
                                    </div>
                                    <c:if test="${recordView.canceledCustomerCredit > 0}">
                                        <div class="recordLabelValue">
                                            <fmt:message key="creditWhenReleased"/>: <o:number value="${recordView.canceledCustomerCredit}" format="currency"/>
                                        </div>
                                    </c:if>
                                </c:otherwise>
                            </c:choose>
                        </c:if>
                        <c:if test="${record.canceled and record.downpayment and empty record.idCanceled}">
                            <div class="recordLabelValue">
                                <v:link url="/sales/downpaymentArchive/print?&id=${record.id}">
                                    <fmt:message key="printRecordSourceDocument"/>
                                </v:link>
                            </div>
                        </c:if>
                        <c:if test="${recordView.exitTarget == 'salesCreditNoteView'}">
                            <c:set var="referenceLinkDisabled" scope="request" value="true" />
                        </c:if>
                        <c:import url="/pages/records/record_credit_notes.jsp"/>
                        <br />
                    </div>
                    <c:import url="/pages/sales/records/invoice_summary.jsp"/>
                </div>
            </div>
        </div>
 
        <c:import url="/pages/records/record_items.jsp"/>
        <c:if test="${record.type.displayNotesBelowItems}">
            <c:import url="/pages/records/record_note_display_below.jsp"/>
        </c:if>
        <c:import url="/pages/records/record_conditions.jsp"/>
    </div>
</div>
</tiles:put> 
</c:otherwise>
</c:choose>
</tiles:insert>
<c:remove var="recordView" scope="session"/>
