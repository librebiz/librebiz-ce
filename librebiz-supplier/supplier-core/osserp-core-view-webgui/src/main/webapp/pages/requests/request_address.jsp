<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<c:set var="view" value="${sessionScope.businessCaseView}" />
<c:set var="plan" scope="request" value="${sessionScope.businessCaseView.request}" />
<c:set var="customer" scope="request" value="${plan.customer}" />
<c:if test="${view.salesContext}">
<c:set var="project" value="${sessionScope.businessCaseView.sales}" />
</c:if>
<c:set var="otherSales" value="${view.userForeignSales}" />

<c:choose>
    <c:when test="${view.addressEditMode}">
        <o:form name="requestForm" url="/requests.do">
            <input type="hidden" name="method" value="updateAddress" />
            <tr>
                <td><fmt:message key="deliveryAddress" /></td>
                <td><input type="text" name="name" value="<c:choose><c:when test="${empty plan.address.name}"><o:out value="${requestScope.customer.displayName}"/></c:when><c:otherwise><o:out value="${plan.address.name}"/></c:otherwise></c:choose>" /></td>
            </tr>
            <tr>
                <td></td>
                <td><input type="text" name="person" value="<o:out value="${plan.address.person}"/>" /></td>
            </tr>
            <tr>
                <td><fmt:message key="street" /></td>
                <td><input type="text" name="street" value="<o:out value="${plan.address.street}"/>" /></td>
            </tr>
            <tr>
                <td><fmt:message key="zipcode" />, <fmt:message key="city" /></td>
                <td><input type="text" name="zipcode" value="<o:out value="${plan.address.zipcode}"/>" class="addressZipcode" /> <input type="text" name="city" value="<o:out value="${plan.address.city}"/>" class="addressCity" /></td>
            </tr>
            <tr>
                <td><fmt:message key="country" /></td>
                <td><oc:select name="country" value="${plan.address.country}" options="countryNames" prompt="false" /></td>
            </tr>
            <tr>
                <td></td>
                <td><input class="cancel" type="button" onclick="gotoUrl('<c:url value="/requests.do?method=disableAddressEditMode"/>');" value="<fmt:message key="exit"/>" style="width: 45%;" /> <span style="margin-left: 22px;"><o:submit style="width:45%;" /></span></td>
            </tr>
        </o:form>
    </c:when>
    <c:otherwise>
        <tr>
            <c:choose>
                <c:when test="${otherSales or plan.cancelled or (!empty project and (project.closed or project.cancelled))}">
                    <td class="top"><fmt:message key="deliveryAddress" /></td>
                </c:when>
                <c:otherwise>
                    <td class="top"><a href="<c:url value="/requests.do?method=enableAddressEditMode"/>" title="<fmt:message key="changeAddress"/>"><fmt:message key="deliveryAddress" /></a></td>
                </c:otherwise>
            </c:choose>
            <td>
                <c:choose>
                    <c:when test="${empty plan.address.name}">
                        <o:out value="${requestScope.customer.displayName}" />
                    </c:when>
                    <c:otherwise>
                        <o:out value="${plan.address.name}" limit="50" />
                    </c:otherwise>
                </c:choose>
            </td>
        </tr>
        <c:if test="${!empty plan.address.person}">
            <tr>
                <td>
                    <c:if test="${view.projectAddressMapSupport}">
                        <o:mapsLink street="${plan.address.street}" zip="${plan.address.zipcode}" city="${plan.address.city}" title="showAddressOnMap">
                            <fmt:message key="map" />
                        </o:mapsLink>
                    </c:if>
                </td>
                <td><o:out value="${plan.address.person}" limit="50" /></td>
            </tr>
        </c:if>
        <tr>
            <td>
                <c:if test="${empty plan.address.person and view.projectAddressMapSupport}">
                    <o:mapsLink street="${plan.address.street}" zip="${plan.address.zipcode}" city="${plan.address.city}" title="showAddressOnMap">
                        <fmt:message key="map" />
                    </o:mapsLink>
                </c:if>
            </td>
            <td><o:out value="${plan.address.street}" /></td>
        </tr>
        <tr>
            <td></td>
            <td><o:out value="${plan.address.zipcode}" /> <o:out value="${plan.address.city}" /></td>
        </tr>
        <c:if test="${!empty plan.shippingId}">
            <tr>
                <td><fmt:message key="shippingLabel" /></td>
                <td><oc:options name="shippingCompanies" value="${plan.shippingId}" /></td>
            </tr>
        </c:if>
    </c:otherwise>
</c:choose>
