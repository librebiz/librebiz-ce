/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 29, 2011 4:12:12 PM 
 * 
 */
package com.osserp.gui.admin.calculations

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode

import com.osserp.core.BusinessType
import com.osserp.core.BusinessTypeManager
import com.osserp.core.calc.CalculationConfig
import com.osserp.core.calc.CalculationConfigManager
import com.osserp.core.calc.CalculationTemplateManager
import com.osserp.core.products.Product
import com.osserp.core.products.ProductManager
import com.osserp.core.products.ProductSearch
import com.osserp.core.products.ProductSearchRequest
import com.osserp.core.products.ProductSelectionConfig
import com.osserp.core.products.ProductSelectionConfigManager

import com.osserp.groovy.web.ViewContextException

import com.osserp.gui.CoreView

/**
 *
 * @author jg <jg@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
class CalculationTemplatesView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(CalculationTemplatesView.class.getName())

    private static final Long PRODUCT_SELECTION_CONFIG = 5L

    CalculationConfig calculationConfig
    List<Product> productList = []
    List<BusinessType> businessTypes = []

    CalculationTemplatesView() {
        super()
        providesCreateMode()
        headerName = 'calculationTemplates'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            list =  calculationTemplateManager.findAll()
            ProductSelectionConfig productSelectionConfig = productSelectionConfigManager.getSelectionConfig(PRODUCT_SELECTION_CONFIG)
            if (!productSelectionConfig) {
                logger.debug("initRequest() can't find product selection config [id=${PRODUCT_SELECTION_CONFIG}]")
                throw new ViewContextException(ErrorCode.PRODUCT_FILTER_CONFIG_MISSING, this)
            }
            productList = productSearch.find(new ProductSearchRequest(), productSelectionConfig)
            if (productList.isEmpty()) {
                logger.warn("initRequest() can't find products by productSelectionConfig [id=${productSelectionConfig.id}]")
                //throw new ViewContextException(ErrorCode.PRODUCT_FILTER_PRODUCTS_MISSING, this)
            }
            businessTypes = []
            List<BusinessType> btl = businessTypeManager.findAll()
            btl.each {
                if (it.recordByCalculation && it.calculationConfig) {
                    businessTypes << it
                }
            }
        }
    }

    @Override
    void select() {
        super.select()
        if (bean) {
            if (!bean.businessType?.calculationConfig) {
                throw new ClientException(ErrorCode.INVALID_ORDER_TYPE_CONFIG)
            }
            calculationConfig = calculationConfigManager.findConfig(bean.businessType)
        } else {
            calculationConfig = null
        }
    }

    @Override
    void save() {

        if (bean) {
            Long directProductId = form.getLong('submit')

            Long[] productIds = form.getIndexedLong('productId')
            Long[] added = form.getIndexedLong('added')
            String[] notes = form.getIndexedStrings('note')
            Long selectedGroup = form.getLong('selectedGroup')
            Long selectedItemId = form.getLong('selectedItemId')

            if(selectedItemId) {
                calculationTemplateManager.removeItem(bean, selectedItemId)
            }

            int length = productIds.length
            for (int i = 0; i < length; i++) {
                if (directProductId) {
                    if (directProductId == productIds[i]) {
                        Product product = productManager.get(productIds[i])
                        if (product && notes[i] != null && selectedGroup) {
                            calculationTemplateManager.addProduct(bean, product, notes[i], selectedGroup)
                            break
                        }
                    }
                } else if (productIds[i] in added) {
                    def product = productManager.get(productIds[i])
                    if (product && notes[i] != null && selectedGroup) {
                        calculationTemplateManager.addProduct(bean, product, notes[i], selectedGroup)
                    }
                }
            }
            list =  calculationTemplateManager.findAll()
        } else if (createMode) {
            String name = form.getString('name')
            Long productId = form.getLong('productId')
            Long businessTypeId = form.getLong('requestTypeId')

            if (!name || !productId || !businessTypeId || name.size() < 3 || name.size() > 100) {
                throw new ClientException(ErrorCode.VALUES_INVALID)
            }
            Product product = productManager.get(productId)
            BusinessType businessType = businessTypeManager.load(businessTypeId)
            if (!product || !businessType) {
                throw new ClientException(ErrorCode.VALUES_INVALID)
            }
            bean = calculationTemplateManager.createTemplate(name, product, businessType)
            calculationConfig = calculationConfigManager.findConfig(bean.businessType)
            list = calculationTemplateManager.findAll()
            disableCreateMode()
        }
    }

    void removeItem() {
        Long id = form.getLong('id')
        if (id) {
            calculationTemplateManager.removeItem(bean, id)
            bean = calculationTemplateManager.load(bean.id)
        }
    }

    void toggleOptionalItem() {
        Long id = form.getLong('id')
        if (id) {
            calculationTemplateManager.toggleOptionalItem(bean, id)
            bean = calculationTemplateManager.load(bean.id)
        }
    }

    void delete() {
        Long id = form.getLong('id')
        if(id) {
            calculationTemplateManager.delete(id)
            list = calculationTemplateManager.findAll()
        }
    }

    private CalculationTemplateManager getCalculationTemplateManager() {
        getService(CalculationTemplateManager.class.getName())
    }

    private ProductSelectionConfigManager getProductSelectionConfigManager() {
        getService(ProductSelectionConfigManager.class.getName())
    }

    private ProductSearch getProductSearch() {
        getService(ProductSearch.class.getName())
    }

    private BusinessTypeManager getBusinessTypeManager() {
        getService(BusinessTypeManager.class.getName())
    }

    private ProductManager getProductManager() {
        getService(ProductManager.class.getName())
    }

    private CalculationConfigManager getCalculationConfigManager() {
        getService(CalculationConfigManager.class.getName())
    }
}

