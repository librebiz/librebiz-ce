package com.osserp.core.sql;


public interface DatabaseTask {

    /**
     * Provides the configured jdbc database url
     * @return dbms url + database name
     */
    String getDatabaseUrl();
    
    /**
     * Tries to establish a database connection using the connection params
     * provided as constructor args.
     * @return true if create connection was successful 
     */
    boolean isDatabaseAvailable();

    /**
     * Expects that create a connection will not fail, tries to create one
     * and performs a query for existing tables in configured database. 
     * @return true if at least one table exists (so be careful with adding
     * own tables before running the initial ddl script).
     */
    boolean isDatabaseInitialized();

    /**
     * Runs the database tasks.
     * @param version
     * @param verbose Logs shell commands to System.out if enabled
     * @return
     */
    boolean runTask(String version, boolean verbose);

}
