<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<v:view viewName="${sessionScope.contactInputViewName}" />

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for=""><fmt:message key="salutation" /> / <fmt:message key="title" /></label>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <oc:select name="salutation" options="salutations" styleClass="form-control" prompt="false" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <oc:select name="title" options="titles" styleClass="form-control" prompt="false" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for=""><fmt:message key="firstName" /></label>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <v:text name="firstName" styleClass="form-control" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for=""><fmt:message key="lastName" /></label>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <v:text name="lastName" styleClass="form-control" />
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for=""><fmt:message key="position" /></label>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <v:text name="position" styleClass="form-control" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for=""><fmt:message key="office" /></label>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <v:text name="office" styleClass="form-control" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for=""><fmt:message key="section" /></label>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <v:text name="section" styleClass="form-control" />
        </div>
    </div>
</div>

<c:import url="_contactAddressInput.jsp" />

<v:date name="birthDate" styleClass="form-control" label="dateOfBirth" picker="true" />
