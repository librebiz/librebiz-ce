/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 6, 2007 5:40:06 PM 
 * 
 */
package com.osserp.gui;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionException;
import com.osserp.common.User;
import com.osserp.common.UserAuthenticator;
import com.osserp.common.UserManager;
import com.osserp.common.gui.MenuProvider;
import com.osserp.common.service.Locator;
import com.osserp.common.service.SharedObjects;
import com.osserp.common.service.UserScope;
import com.osserp.common.util.CollectionUtil;
import com.osserp.common.web.OnlineUsers;
import com.osserp.common.web.PortalView;
import com.osserp.common.web.ViewName;

import com.osserp.core.Comparators;
import com.osserp.core.contacts.PrivateContactManager;
import com.osserp.core.mail.FetchmailInboxManager;
import com.osserp.core.employees.Employee;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("portalView")
public class PortalViewImpl extends CorePortalView implements PortalView {
    private static Logger log = LoggerFactory.getLogger(PortalViewImpl.class.getName());

    private boolean timeRecordingRequired = false;
    private boolean externalAuthentication = false;

    public PortalViewImpl() {
        super();
    }

    @Override
    public void init(HttpServletRequest request) throws PermissionException {
        super.init(request);
    }

    public User login(String loginName, String password, String remoteIp) throws PermissionException {
        if (isNotSet(loginName) || isNotSet(password)) {
            log.warn("login: invocation without username and/or password [ip=" + remoteIp + 
                    (loginName != null ? (", loginName=" + loginName + "]") : "]"));
            throw new PermissionException(ErrorCode.LOGIN_FAILED);
        }
        if (log.isDebugEnabled()) {
            log.debug("login(loginName, password, remoteIp) invoked");
        }
        User user = getUserAuthenticator().authenticate(
                loginName,
                password,
                remoteIp);
        login(user);
        return user;
    }

    public User login(String username, String remoteIp) throws PermissionException {
        if (isNotSet(username)) {
            log.warn("login: invocation without username [ip=" + remoteIp + "]");
            throw new PermissionException(ErrorCode.LOGIN_FAILED);
        }
        if (getWebConfig() == null || !getWebConfig().isSsoEnabled()) {
            log.warn("login: invocation via BasicAuth but SSO not enabled [username=" + username 
                    + ", ip=" + remoteIp + "]");
            throw new PermissionException(ErrorCode.LOGIN_FAILED);
        }
        if (log.isDebugEnabled()) {
            log.debug("login(loginName, remoteIp) invoked");
        }
        User user = getUserAuthenticator().authenticate(username, remoteIp);
        login(user);
        externalAuthentication = true;
        return user;
    }

    private void login(User user) throws PermissionException {
        if (user == null) {
            if (log.isDebugEnabled()) {
                log.debug("login(user) user not found");
            }
            throw new PermissionException();
        }
        if (!user.isActive()) {
            if (log.isDebugEnabled()) {
                log.debug("login(user) user not activated");
            }
            throw new PermissionException();
        }
        if (isSystemSetupRequired() 
                && !user.isAdmin() && !user.isSetupAdmin()) {
            
            if (log.isDebugEnabled()) {
                log.debug("login(user) setup required but user not admin");
            }
            throw new PermissionException();
        }
        
        setUser(user);
        setUserLoggedIn(true);
        if (user instanceof DomainUser) {
            DomainUser domainUser = (DomainUser) user;
            Employee employee = domainUser.getEmployee();

            if (employee != null) {
                if (!employee.isActive()) {
                    if (log.isDebugEnabled()) {
                        log.debug("login(user) employee not active");
                    }
                    setUserLoggedIn(false);
                    throw new PermissionException();
                }
                timeRecordingRequired = employee.isTimeRecordingEnabled();

            } else {
                log.warn("login() domainUser without employee found [id=" + user.getId()
                        + ", contactId=" + user.getContactId() + "]");
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("login() done [user=" + user.getId() + "]");
        }
        updateSharedMenu();
    }

    public void logout() {
        setUserLoggedIn(false);
        setUser(null);
    }

    public void updatePassword(String password, String confirmPassword) throws ClientException {
        UserManager manager = getUserManager();
        if (manager != null) {
            if (isNotSet(password) || isNotSet(confirmPassword)) {
                throw new ClientException(ErrorCode.PASSWORD_MISSING);
            }
            if (!password.equals(confirmPassword)) {
                throw new ClientException(ErrorCode.PASSWORD_MATCH);
            }
            setUser(manager.changePassword(
                    getUser(),
                    password,
                    confirmPassword));
            if (log.isDebugEnabled()) {
                log.debug("updatePassword() done");
            }
        } else if (log.isInfoEnabled()) {
            log.info("updatePassword() ignoring request; userManager not available");
        }
    }

    public void updateUser(User user) throws IllegalAccessException {
        User loggedInUser = getUser();
        if (loggedInUser != null) {
            if (loggedInUser.getId().equals(user.getId())) {
                setUser(user);
            } else {
                throw new IllegalAccessException("permission.denied");
            }
        }
    }
    
    public List<User> getOnlineUsers() {
        List<User> users = new ArrayList<User>();
        OnlineUsers onlineUsers = OnlineUsers.newInstance();
        users.addAll(onlineUsers.getUsers());
        CollectionUtil.sort(users, Comparators.createUserLoginNameComparator());
        return users;
    }

    public boolean isTimeRecordingRequired() {
        return timeRecordingRequired;
    }

    protected void setTimeRecordingRequired(boolean timeRecordingRequired) {
        this.timeRecordingRequired = timeRecordingRequired;
    }

    public boolean isExternalAuthentication() {
        return externalAuthentication;
    }

    protected void setExternalAuthentication(boolean externalAuthentication) {
        this.externalAuthentication = externalAuthentication;
    }

    public boolean isPrivateContactAvailable() {
        try {
            User user = getUser();
            if (user instanceof DomainUser) {
                PrivateContactManager pcm = (PrivateContactManager) getService(PrivateContactManager.class.getName());
                return pcm.isPrivateAvailable((DomainUser) user);
            }
        } catch (Throwable t) {
            // we ignore this
        }
        return false;
    }

    public int getInboxMessageCount() {
        try {
            User user = getUser();
            if (user instanceof DomainUser) {
                int result = getFetchmailInboxManager().getCountByUser((DomainUser) user);
                return result;
            }
        } catch (Throwable t) {
            log.warn("getInboxMessageCount: Failed getting e-mail message count, message: " + t.getMessage(), t);
        }
        return 0;
    }

    protected FetchmailInboxManager getFetchmailInboxManager() {
        return (FetchmailInboxManager) getService(FetchmailInboxManager.class.getName());
    }

    @Override
    protected MenuProvider getMenuProvider() {
        return (MenuProvider) getService(MenuProvider.class.getName());
    }

    @Override
    protected SharedObjects getSharedObjects() {
        return (SharedObjects) getService(SharedObjects.class.getName());
    }

    @Override
    protected UserScope getUserScope() {
        return (UserScope) getService(UserScope.class.getName());
    }

    protected UserAuthenticator getUserAuthenticator() {
        return (UserAuthenticator) getService(UserAuthenticator.class.getName());
    }

    @Override
    public void setLocator(Locator locator) {
        super.setLocator(locator);
        if (locator != null) {
            enableBackendAvailabilityProperty();
        }
    }
}
