/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 23, 2008 11:58:29 AM 
 * 
 */
package com.osserp.core.service.contacts;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.OptionsCache;

import com.osserp.core.Options;
import com.osserp.core.contacts.ContactType;
import com.osserp.core.contacts.Interest;
import com.osserp.core.contacts.InterestManager;
import com.osserp.core.contacts.Person;
import com.osserp.core.dao.Interests;
import com.osserp.core.employees.Employee;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class InterestManagerImpl extends AbstractPersonManager implements InterestManager {
    private Interests interests = null;
    private OptionsCache options = null;

    /**
     * Creates a new InstallerSearch
     */
    public InterestManagerImpl(Interests interests, OptionsCache options) {
        this.interests = interests;
        this.options = options;
    }

    public List<Interest> findOpen() {
        return interests.findOpen();
    }

    public Person find(Long id) {
        return interests.find(id);
    }

    public void changeType(Employee changingUser, Person personToChange, String company) throws ClientException {
        if (personToChange instanceof Interest) {
            Interest interest = (Interest) personToChange;
            interest.setComment(company);
            ContactType type = (ContactType) options.getMapped(Options.CONTACT_TYPES, Person.TYPE_BUSINESS);
            interest.setType(type);
            interests.save(interest);
        }
    }

    public void save(Person person) {
        if (person instanceof Interest) {
            interests.save((Interest) person);
        }
    }

    public void addExternalNote(Person person, String note) {
        if (person instanceof Interest) {
            Interest interest = (Interest) person;
            interest.setDescription(note);
            interests.save(interest);
        }
    }

    public void addInternalNote(Long createdBy, Person person, String note) {
        if (person instanceof Interest) {
            Interest interest = (Interest) person;
            interest.setComment(note);
            interests.save(interest);
        }
    }

    public void delete(Person person) throws ClientException {
        // interest manager does not provide interest delete
    }

}
