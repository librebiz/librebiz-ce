/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 25, 2009 8:21:09 AM 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.hibernate.SessionFactory;

import com.osserp.common.PublicHoliday;
import com.osserp.common.dao.AbstractHibernateDao;

import com.osserp.core.dao.PublicHolidays;
import com.osserp.core.model.PublicHolidayImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PublicHolidaysDao extends AbstractHibernateDao implements PublicHolidays {
    private static Logger log = LoggerFactory.getLogger(PublicHolidaysDao.class.getName());

    public PublicHolidaysDao(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<PublicHoliday> findAll() {
        StringBuilder hql = new StringBuilder("from ");
        hql.append(PublicHolidayImpl.class.getName()).append(" o order by o.country, o.state, o.date");
        return getCurrentSession().createQuery(hql.toString()).list();
    }

    public List<PublicHoliday> find(Long country, Long state) {
        if (log.isDebugEnabled()) {
            log.debug("find() invoked [country=" + country + ", state=" + state + "]");
        }
        if (country == null && state == null) {
            return findAll();
        }
        if (country != null && state == null) {
            StringBuilder hql = new StringBuilder("from ");
            hql.append(PublicHolidayImpl.class.getName()).append(" o where o.country = :countryId order by o.state, o.date");
            return getCurrentSession().createQuery(hql.toString()).setParameter("countryId", country).list();
        }
        if (country == null && state != null) {
            StringBuilder hql = new StringBuilder("from ");
            hql.append(PublicHolidayImpl.class.getName()).append(" o where o.state = :stateId order by o.country, o.date");
            return getCurrentSession().createQuery(hql.toString()).setParameter("stateId", state).list();
        }
        StringBuilder hql = new StringBuilder("from ");
        hql.append(PublicHolidayImpl.class.getName()).append(" o where o.country = :countryId and o.state = :stateId order by o.date");
        return getCurrentSession().createQuery(hql.toString()).setParameter("countryId", country).setParameter("stateId", state).list();
    }
}
