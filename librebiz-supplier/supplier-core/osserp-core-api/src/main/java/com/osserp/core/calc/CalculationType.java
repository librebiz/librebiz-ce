/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 11, 2005 
 * 
 */
package com.osserp.core.calc;

import com.osserp.common.Option;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface CalculationType extends Option {

    /**
     * Sets the name of the calculation type
     * @param name
     */
    void setName(String name);

    /**
     * Provides a description of the calculation type
     * @return description
     */
    String getDescription();

    /**
     * Sets the description of the calculation type
     * @param description
     */
    void setDescription(String description);

    /**
     * Indicates if partlist support is enabled.
     * @return supportsPartlists
     */
    boolean isSupportsPartlists();

    /**
     * Sets partlist availability
     * @param supportsPartlists
     */
    void setSupportsPartlists(boolean supportsPartlists);
}
