/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 14, 2008 3:04:10 PM 
 * 
 */
package com.osserp.gui.client.hrm.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.Month;
import com.osserp.common.PermissionException;
import com.osserp.common.beans.CalendarImpl;
import com.osserp.common.util.DateFormatter;
import com.osserp.common.util.DateUtil;
import com.osserp.common.web.Form;
import com.osserp.common.web.ViewName;
import com.osserp.core.employees.Employee;
import com.osserp.core.hrm.TimeRecord;
import com.osserp.core.hrm.TimeRecordCorrection;
import com.osserp.core.hrm.TimeRecordMarkerType;
import com.osserp.core.hrm.TimeRecordType;
import com.osserp.core.hrm.TimeRecording;
import com.osserp.core.hrm.TimeRecordingConfig;
import com.osserp.core.hrm.TimeRecordingManager;
import com.osserp.core.hrm.TimeRecordingPeriod;
import com.osserp.gui.client.hrm.TimeRecordingView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("timeRecordingView")
public class TimeRecordingViewImpl extends AbstractTimeRecordingView implements TimeRecordingView {
    private static Logger log = LoggerFactory.getLogger(TimeRecordingViewImpl.class.getName());

    private static final String REMOVE_RECORD_PERMISSIONS =
            "time_record_delete";
    private static final String CHANGE_SETUP_PERMISSIONS =
            "time_recording_admin";
    private static final String EDIT_MARKER_PERMISSIONS =
            "time_recording_marker";
    private TimeRecord lastRecord = null;
    private TimeRecordType selectedType = null;
    private boolean manualRecordingMode = false;
    private boolean listDescendentMode = true;
    private boolean listDetailsMode = false;
    private boolean displayJournalMode = false;
    private boolean displayMarkerMode = false;
    private Date selectedDate = null;
    private TimeRecord selectedRecord = null;
    private boolean configChangeMode = false;

    protected TimeRecordingViewImpl() {
        super();
    }

    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        super.init(request);
        TimeRecording recording = getTimeRecording();
        boolean periodTimeRecordingEnabled = false;
        if (recording != null) {
            Month current = DateUtil.createMonth();
            TimeRecordingPeriod period = recording.getSelectedPeriod();
            if (period != null && period.isCurrent()) {
                if (log.isDebugEnabled()) {
                    log.debug("init() found current period [recording=" + period.getReference()
                            + ", period=" + period.getId()
                            + "]");
                }
                period.selectMonth(current);
                if (log.isDebugEnabled()) {
                    log.debug("init() done, selected current month [month="
                            + current.getMonth() + ", year=" + current.getYear() + "]");
                }
                periodTimeRecordingEnabled = period.getConfig().isRecordingHours();

            } else if (period != null) {
                period.selectMonth(null);
                if (log.isDebugEnabled()) {
                    log.debug("init() done, selected latest month from period [month="
                            + period.getSelectedYear().getSelectedMonth().getMonth() + ", year=" + period.getSelectedYear().getYear() + "]");
                }
                periodTimeRecordingEnabled = period.getConfig().isRecordingHours();
            } else if (log.isDebugEnabled()) {
                log.debug("init() done, no month selected [recording=" + recording.getId() + "]");
            }

            if (isCurrentUser() && periodTimeRecordingEnabled) {
                setEditMode(true);
                this.displayJournalMode = true;
                this.listDescendentMode = false;
            }

        }
        if (log.isDebugEnabled()) {
            log.debug("init() done [recording=" + (recording == null ? "null" : recording.getId()) + "]");
        }
    }

    public List<TimeRecordType> getStartTypes() {
        List<TimeRecordType> result = new ArrayList<TimeRecordType>();
        for (int i = 0, j = fetchTypes(true).size(); i < j; i++) {
            TimeRecordType next = fetchTypes(true).get(i);
            if (!next.isInterval()) {
                result.add(next);
            }
        }
        return result;
    }

    public List<TimeRecordingPeriod> getAvailablePeriods() {
        return getTimeRecording().getAvailablePeriods();
    }

    public List<TimeRecordType> getIntervalTypes() {
        List<TimeRecordType> result = new ArrayList<TimeRecordType>();
        for (int i = 0, j = fetchTypes(true).size(); i < j; i++) {
            TimeRecordType next = fetchTypes(true).get(i);
            if (next.isInterval()) {
                result.add(next);
            }
        }
        return result;
    }

    public TimeRecordType getStopType() {
        List<TimeRecordType> types = fetchTypes(false);
        return (types.isEmpty() ? null : types.get(0));
    }

    public List<TimeRecordMarkerType> getMarkerTypes() {
        return getTimeRecordingManager().findMarkerTypes();
    }

    public TimeRecordType getSelectedType() {
        return selectedType;
    }

    protected void setSelectedType(TimeRecordType selectedType) {
        this.selectedType = selectedType;
    }

    public void selectType(Long id) {
        this.selectedType = fetchType(id);
        if (selectedType != null) {
            if (log.isDebugEnabled()) {
                log.debug("selectType() creating defaults [type=" + selectedType.getId() + "]");
            }
            TimeRecordingConfig config = getTimeRecording().getSelectedPeriod().getConfig();
            int startHours = config.getCoreTimeStartHours();
            int startMinutes = config.getCoreTimeStartMinutes();
            Date startDate = (selectedDate != null) ? selectedDate : DateUtil.getCurrentDate();
            if (selectedDate == null && !this.selectedType.isInterval()) {
                Integer[] now = DateUtil.getTime(DateUtil.getCurrentDate());
                startHours = now[3];
                startMinutes = now[4];
            }
            addLocalValue("startValue", DateFormatter.getDate(startDate));

            if (selectedType.isInterval()) {
                if (selectedType.getIntervalStartHours() > 0) {
                    startHours = selectedType.getIntervalStartHours();
                    if (selectedType.getIntervalStartMinutes() != startMinutes) {
                        startMinutes = selectedType.getIntervalStartMinutes();
                    }
                }
                startDate = DateUtil.createDate(startDate, startHours, startMinutes);
                if (log.isDebugEnabled()) {
                    log.debug("selectType() created start date [value=" + DateFormatter.getDateAndTime(startDate) + "]");
                }
                Date stopDate = new Date(startDate.getTime());
                addLocalValue("stopValue", DateFormatter.getDate(stopDate));
                int minutes = 0;
                if (selectedType.isIntervalHoursByConfig() || selectedType.getIntervalHours() == 0) {
                    minutes = Double.valueOf(config.getHoursDaily() * 60).intValue();
                } else {
                    minutes = selectedType.getIntervalHours() * 60 + selectedType.getIntervalMinutes();
                }
                stopDate = DateUtil.addMinutes(stopDate, minutes);
                if (log.isDebugEnabled()) {
                    log.debug("selectType() created stop date [value=" + DateFormatter.getDateAndTime(stopDate) + "]");
                }
                String hrs = DateFormatter.getHours(stopDate);
                addLocalValue("stopValueHours", hrs);
                String min = DateFormatter.getMinutes(stopDate);
                addLocalValue("stopValueMinutes", min);
                if (log.isDebugEnabled()) {
                    log.debug("selectType() stored time [hours=" + hrs + ", min=" + min + "]");
                }
            }
            addLocalValue("startValueHours", DateFormatter.getFormatted(startHours));
            addLocalValue("startValueMinutes", DateFormatter.getFormatted(startMinutes));
            if (selectedType.isManualInputRequired()) {
                this.manualRecordingMode = true;
            }
        }
    }

    public void checkDayOpen() {
        TimeRecording recording = getTimeRecording();
        if (recording != null && recording.getSelectedPeriod() != null) {
            List<TimeRecord> records = recording.getSelectedPeriod().getRecords();
            boolean openDay = false;

            for (int i = 0, j = records.size(); i < j; i++) {
                TimeRecord next = records.get(i);
                if (DateUtil.isToday(next.getValue()) && next.getClosedBy() == null) {
                    openDay = true;
                    break;
                }
            }

            if (openDay) {
                if (getStopType() != null) {
                    this.selectType(getStopType().getId());
                }
            } else {
                if (!getStartTypes().isEmpty()) {
                    this.selectType(getStartTypes().get(0).getId());
                }
            }
        }
    }

    public void displayMarkerList(Month month) {

    }

    public boolean isCurrentlyRecording() {
        if (lastRecord != null) {
            return (lastRecord.isCurrentDay() && lastRecord.getType().isStarting());
        }
        return false;
    }

    public boolean isManualRecordingMode() {
        return manualRecordingMode;
    }

    public void setManualRecordingMode(boolean manualRecordingMode) {
        this.manualRecordingMode = manualRecordingMode;
    }

    public TimeRecord getLastRecord() {
        return lastRecord;
    }

    protected void setLastRecord(TimeRecord lastRecord) {
        this.lastRecord = lastRecord;
    }

    public boolean isListDescendentMode() {
        return listDescendentMode;
    }

    protected void setListDescendentMode(boolean listDescendentMode) {
        this.listDescendentMode = listDescendentMode;
    }

    public void toggleListDescendentMode() {
        this.listDescendentMode = !this.listDescendentMode;
    }

    public boolean isListDetailsMode() {
        return listDetailsMode;
    }

    protected void setListDetailsMode(boolean listDetailsMode) {
        this.listDetailsMode = listDetailsMode;
    }

    public void toggleListDetailsMode() {
        this.listDetailsMode = !this.listDetailsMode;
    }

    public boolean isDisplayJournalMode() {
        return displayJournalMode;
    }

    protected void setDisplayJournalMode(boolean displayJournalMode) {
        this.displayJournalMode = displayJournalMode;
    }

    public boolean isInvokedInApprovalMode() {
        return "timeRecordingApproval".equals(getExitTarget());
    }

    @Override
    public void setExitTarget(String exitTarget) {
        super.setExitTarget(exitTarget);
        if (isInvokedInApprovalMode()) {
            this.displayJournalMode = true;
            this.listDetailsMode = true;
            this.listDescendentMode = false;
        }
    }

    public void toggleDisplayJournalMode() {
        if (!displayJournalMode) {
            this.listDetailsMode = true;
            this.listDescendentMode = false;
        }
        this.displayJournalMode = !this.displayJournalMode;
    }

    public boolean isDisplayMarkerMode() {
        return displayMarkerMode;
    }

    protected void setDisplayMarkerMode(boolean displayMarkerMode) {
        this.displayMarkerMode = displayMarkerMode;
    }

    public void toggleDisplayMarkerMode() {
        this.displayMarkerMode = !this.displayMarkerMode;
    }

    public void removeRecord(Long id) throws PermissionException {
        this.checkPermission(REMOVE_RECORD_PERMISSIONS);
        TimeRecordingManager manager = getTimeRecordingManager();
        manager.removeRecord(getDomainEmployee(), getTimeRecording(), id);
        refresh();
    }

    public void removeMarker(Long id) throws PermissionException {
        this.checkPermission(EDIT_MARKER_PERMISSIONS);
        TimeRecordingManager manager = getTimeRecordingManager();
        manager.removeMarker(getDomainEmployee(), getTimeRecording(), id);
        refresh();
    }

    public boolean selectDate(Integer day, Integer month, Integer year) {
        this.selectedDate = (day == null || month == null || year == null ? DateUtil.getCurrentDate() : new CalendarImpl(day, month, year).getDate());
        addLocalValue("startValue", DateFormatter.getDate(selectedDate));
        addLocalValue("stopValue", DateFormatter.getDate(selectedDate));
        return DateUtil.isToday(selectedDate);
    }

    public List<TimeRecordCorrection> getRecordCorrections() {
        if (selectedRecord == null || !this.selectedRecord.isCorrectionAvailable()) {
            return new ArrayList<TimeRecordCorrection>();
        }
        TimeRecordingManager manager = getTimeRecordingManager();
        return manager.findCorrections(selectedRecord);
    }

    public TimeRecord getSelectedRecord() {
        return selectedRecord;
    }

    public void selectRecord(Long id) {
        if (id != null) {
            TimeRecording recording = getTimeRecording();
            List<TimeRecord> list = recording.getSelectedPeriod().getRecords();
            for (int i = 0, j = list.size(); i < j; i++) {
                TimeRecord next = list.get(i);
                if (next.getId().equals(id)) {
                    this.selectedRecord = next;
                    Date stopDate = (selectedRecord.getClosedBy() == null ? null : selectedRecord.getClosedBy().getValue());
                    this.populateDates(selectedRecord.getValue(), stopDate);
                }
            }
        } else {
            this.selectedRecord = null;
        }
    }

    public void updateRecord(Form form) throws ClientException {
        if (selectedRecord != null) {
            // reset selectedRecord immediately to prevent user from sending form n-times 
            // while long running updateRecord method below  
            TimeRecord selected = (TimeRecord) selectedRecord.clone();
            this.selectedRecord = null;
            TimeRecordingManager manager = getTimeRecordingManager();
            TimeRecording recording = getTimeRecording();
            Date[] startAndStop = populateDates(recording.getSelectedPeriod(), form);
            String correctionNote = form.getString(Form.NOTE);
            manager.updateRecord(
                    getDomainEmployee(),
                    recording,
                    selected,
                    startAndStop[0],
                    startAndStop[1],
                    correctionNote);
        }
        refresh();
    }

    public List<TimeRecordingConfig> getAvailableConfigs() {
        List<TimeRecordingConfig> all = getConfigs();
        TimeRecordingConfig current = getCurrentPeriod().getConfig();
        for (Iterator<TimeRecordingConfig> i = all.iterator(); i.hasNext();) {
            TimeRecordingConfig next = i.next();
            if (next.getId().equals(current.getId())) {
                i.remove();
                break;
            }
        }
        return all;
    }

    public boolean isConfigChangeMode() {
        return configChangeMode;
    }

    protected void setConfigChangeMode(boolean configChangeMode) {
        this.configChangeMode = configChangeMode;
    }

    public void changeConfigMode(boolean configMode) throws PermissionException {
        if (configMode) {
            checkPermission(CHANGE_SETUP_PERMISSIONS);
        }
        configChangeMode = configMode;
    }

    public void changeConfig(Long id) throws ClientException {
        if (isSet(id)) {
            TimeRecordingManager manager = getTimeRecordingManager();
            TimeRecording recording = getTimeRecording();
            manager.changeConfig(getDomainEmployee(), recording, id);
            refresh();
        }
        configChangeMode = false;
    }

    @Override
    public void setEditMode(boolean editMode) throws PermissionException {
        super.setEditMode(editMode);
        if (editMode) {
            try {
                initBooking();
                manualRecordingMode = false;
            } catch (ClientException e) {
                throw new PermissionException(e.getMessage());
            }
        }
    }

    public void createPeriod() throws PermissionException {
        checkPermission(CHANGE_SETUP_PERMISSIONS);
        TimeRecordingManager manager = getTimeRecordingManager();
        manager.createPeriod(getDomainEmployee(), getTimeRecording());
    }

    public void updatePeriod(Form form) throws ClientException, PermissionException {
        if (isSetupMode()) {
            checkPermission(CHANGE_SETUP_PERMISSIONS);
            TimeRecording recording = getTimeRecording();
            getTimeRecordingManager().updatePeriod(
                    getDomainEmployee(),
                    recording,
                    form.getLong("terminalChipNumber"),
                    form.getDate("validFrom"),
                    form.getDate("validTil"),
                    form.getDouble("carryoverLeave"),
                    form.getDouble("carryoverHours"),
                    form.getDouble("extraLeave"),
                    form.getDouble("firstYearLeave"),
                    form.getLong("state"));
            setSetupMode(false);
            refresh();
        }
    }

    public void addMarker(Form form) throws ClientException, PermissionException {
        checkPermission(EDIT_MARKER_PERMISSIONS);
        Date date = form.getDate("markerDate");
        if (date == null) {
            throw new ClientException(ErrorCode.DATE_INVALID);
        }
        TimeRecordingManager manager = getTimeRecordingManager();
        TimeRecordMarkerType type = manager.findMarkerType(form.getLong("markerTyp"));
        if (type == null) {
            throw new ClientException(ErrorCode.TYPE_INVALID);
        }
        Double value = form.getDouble("markerValue");
        if (value == null) {
            throw new ClientException(ErrorCode.VALUES_MISSING);
        }
        manager.addMarker(
                getDomainEmployee(),
                getTimeRecording(),
                type,
                date,
                value,
                form.getString(Form.NOTE));
        refresh();
    }

    public Long getNextPeriodId() {
        TimeRecordingPeriod next = getTimeRecording().getNextPeriod();
        if (next != null) {
            return next.getConfig().getId();
        }
        return null;
    }

    @Override
    public void save(Form form) throws ClientException, PermissionException {
        if (isEditMode()) {
            if (selectedType == null) {
                throw new ClientException(ErrorCode.TYPE_REQUIRED);
            }
            if (log.isDebugEnabled()) {
                log.debug("save() invoked [type=" + selectedType.getId()
                        + "]");
            }
            TimeRecordingPeriod current = getCurrentPeriod();
            if (manualRecordingMode) {
                createByManualInput(current, form);
            } else {
                createBySystemTime(current);
            }
            disableEditMode();
            manualRecordingMode = false;
            selectedDate = null;
            selectedType = null;
            refresh();
            setLastRecord(getCurrentPeriod());
        }
    }

    private void initBooking() throws ClientException {
        TimeRecording recording = getTimeRecording();
        Employee employee = getEmployee();
        if (recording != null) {
            TimeRecordingPeriod period = recording.getSelectedPeriod();
            if (period == null) {
                log.warn("initBooking() failed while employee without active recording period found [id="
                        + (employee == null ? "null" : employee.getId()) + "]");
                throw new ClientException(ErrorCode.EMPLOYEE_CONFIG_INVALID);
            }
            setLastRecord(period);

        } else {
            log.warn("initBooking() failed while employee without active recording found [id="
                    + (employee == null ? "null" : employee.getId()) + "]");
            throw new ClientException(ErrorCode.EMPLOYEE_CONFIG_INVALID);
        }
    }

    private void createBySystemTime(TimeRecordingPeriod current) throws ClientException {
        TimeRecordingManager manager = getTimeRecordingManager();
        manager.addRecord(getDomainEmployee(), getTimeRecording(), selectedType);
    }

    private void createByManualInput(TimeRecordingPeriod current, Form form) throws ClientException {
        Date[] startAndStop = populateDates(current, form);
        TimeRecordingManager manager = getTimeRecordingManager();
        if (selectedType.isInterval()) {
            manager.addRecord(getDomainEmployee(), getTimeRecording(), selectedType, startAndStop[0], startAndStop[1], form.getString(Form.NOTE));
        } else {
            manager.addRecord(getDomainEmployee(), getTimeRecording(), selectedType, startAndStop[0], form.getString(Form.NOTE));
        }
    }

    /**
     * Populates start and stop values to local cache and resulting array
     * @param current
     * @param form
     * @return start and stop date (stop date if exists, result[1] may null)
     * @throws ClientException if start date missing
     */
    private Date[] populateDates(TimeRecordingPeriod current, Form form) throws ClientException {
        Date[] result = new Date[2];
        Date startDate = form.getDate("startValue");
        if (startDate != null) {
            addLocalValue("startValue", DateFormatter.getDate(startDate));
        }
        Date stopDate = form.getDate("stopValue");
        if (stopDate != null) {
            addLocalValue("stopValue", DateFormatter.getDate(stopDate));
        }
        Integer startValueHours = form.getInteger("startValueHours");
        if (startValueHours != null) {
            addLocalValue("startValueHours", DateFormatter.getFormatted(startValueHours));
        }
        Integer startValueMinutes = form.getInteger("startValueMinutes");
        if (startValueMinutes != null) {
            addLocalValue("startValueMinutes", DateFormatter.getFormatted(startValueMinutes));
        } else {
            startValueMinutes = 0;
        }
        Integer stopValueHours = form.getInteger("stopValueHours");
        if (stopValueHours != null) {
            addLocalValue("stopValueHours", DateFormatter.getFormatted(stopValueHours));
        }
        Integer stopValueMinutes = form.getInteger("stopValueMinutes");
        if (stopValueMinutes != null) {
            addLocalValue("stopValueMinutes", DateFormatter.getFormatted(stopValueMinutes));
        }
        if (startDate == null) {
            throw new ClientException(ErrorCode.DATE_START_REQUIRED);
        }
        if (startValueHours == null) {
            startValueHours = current.getConfig().getCoreTimeStartHours();
            startValueMinutes = current.getConfig().getCoreTimeStartMinutes();
        }
        result[0] = DateUtil.createDate(startDate, startValueHours, startValueMinutes);

        if (stopDate != null) {
            if (stopValueHours != null) {
                if (stopValueMinutes == null) {
                    stopValueMinutes = Integer.valueOf(0);
                }
                stopDate = DateUtil.createDate(stopDate, stopValueHours, stopValueMinutes);
            }
            result[1] = stopDate;
        }
        if (log.isDebugEnabled()) {
            log.debug("populateDates() done [startDate="
                    + DateFormatter.getDateAndTime(startDate)
                    + ", stopDate=" + (stopDate == null ? "null" : DateFormatter.getDateAndTime(stopDate))
                    + "]");
        }
        return result;
    }

    private void populateDates(Date startDate, Date stopDate) {
        if (startDate != null) {
            addLocalValue("startValue", DateFormatter.getDate(startDate));
            Integer[] start = DateUtil.getTime(startDate);
            addLocalValue("startValueHours", DateFormatter.getFormatted(start[3]));
            addLocalValue("startValueMinutes", DateFormatter.getFormatted(start[4]));
        }
        if (stopDate != null) {
            addLocalValue("stopValue", DateFormatter.getDate(stopDate));
            Integer[] stop = DateUtil.getTime(stopDate);
            addLocalValue("stopValueHours", DateFormatter.getFormatted(stop[3]));
            addLocalValue("stopValueMinutes", DateFormatter.getFormatted(stop[4]));
        }
    }

    private void setLastRecord(TimeRecordingPeriod period) {
        this.lastRecord = period.getLastRecord();
        if (log.isDebugEnabled()) {
            log.debug("setLastRecord() done [id="
                    + (lastRecord == null ? "null" : lastRecord.getId()) + "]");
        }
    }

    private TimeRecordingPeriod getCurrentPeriod() {
        TimeRecordingPeriod p = getTimeRecording().getSelectedPeriod();
        if (p == null) {
            throw new ActionException("no time record period selected");
        }
        return p;
    }
}
