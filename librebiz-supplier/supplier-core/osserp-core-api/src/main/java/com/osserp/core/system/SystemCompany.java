/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 31-Jul-2006 
 * 
 */
package com.osserp.core.system;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.jdom2.Element;

import com.osserp.common.ClientException;
import com.osserp.common.Mappable;
import com.osserp.common.Option;
import com.osserp.common.Year;

import com.osserp.core.BankAccount;
import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.Legalform;
import com.osserp.core.employees.Employee;
import com.osserp.core.employees.EmployeeGroup;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SystemCompany extends Option, Mappable {

    static final Long COMPANY_LOGO = 5L;
    static final Long COMPANY_REGISTRATION = 6L;

    /**
     * Provides the associated contact
     * @return contact
     */
    Contact getContact();

    /**
     * Provides the legalform type of the company
     * @return legalform
     */
    Legalform getLegalform();

    /**
     * Provides the managing directors of the company
     * @return managingDirectors
     */
    List<ManagingDirector> getManagingDirectors();

    /**
     * @param user
     * @param shortkey
     * @param bankName
     * @param bankNameAddon
     * @param bankAccountNumberIntl
     * @param bankIdentificationCodeIntl
     * @param useInDocumentFooter
     * @param taxAccount
     * @throws ClientException
     */
    void addBankAccount(
            Employee user,
            String shortkey,
            String bankName,
            String bankNameAddon,
            String bankAccountNumberIntl,
            String bankIdentificationCodeIntl,
            boolean useInDocumentFooter,
            boolean taxAccount) throws ClientException;

    /**
     * Adds a managing director
     * @param user
     * @param name
     * @throws ClientException if name already added
     */
    void addManagingDirector(Employee user, String name) throws ClientException;

    /**
     * Provides a list of all years from vatReportsStart til now. 
     * @return years list or list with currentYear only
     */
    List<Year> getAccountingYears();

    /**
     * @return returns a list of bank accounts.
     */
    List<BankAccount> getBankAccounts();

    /**
     * Provides employee groups associated with this company
     * @return groups
     */
    List<EmployeeGroup> getGroups();

    /**
     * Adds a new employee group
     * @param group
     */
    void addGroup(EmployeeGroup group);

    /**
     * Removes group if exists
     * @param id
     * @return removed if true
     */
    boolean removeGroup(Long id);

    /**
     * @return returns the local court.
     */
    String getLocalCourt();

    /**
     * @param localCourt The local court to set.
     */
    void setLocalCourt(String localCourt);

    /**
     * @return returns the shortname
     */
    String getShortname();

    /**
     * @param shortname The shortname to set.
     */
    void setShortname(String shortname);

    /**
     * Provides an optional company name to display in the window of a letter.
     * This may be useful for very long company names.
     * @return letterWindowName
     */
    String getLetterWindowName();

    /**
     * Sets an optional short name for letter windows
     * @param letterWindowName
     */
    void setLetterWindowName(String letterWindowName);

    /**
     * @return returns the trade register entry.
     */
    String getTradeRegisterEntry();

    /**
     * @param tradeRegisterEntry The trade register entry to set.
     */
    void setTradeRegisterEntry(String tradeRegisterEntry);

    /**
     * @return returns the tax id.
     */
    String getTaxId();

    /**
     * @param vatId The tax id to set.
     */
    void setTaxId(String taxId);

    /**
     * @return returns the vat id.
     */
    String getVatId();

    /**
     * @param vatId The vat id to set.
     */
    void setVatId(String vatId);

    /**
     * Provides the default margin added to purchase price in internal invoices
     * @return internalMargin
     */
    Double getInternalMargin();

    /**
     * Sets the default margin added to purchase price in internal invoices
     * @param internalMargin
     */
    void setInternalMargin(Double internalMargin);

    /**
     * Indicates that company is holding (e.g. top level company)
     * @return holding
     */
    boolean isHolding();

    /**
     * Indicates the primary company if more than one defined (e.g. default).
     * Opposed to holding primary does not imply a relation to other companies. 
     * @return primary
     */
    boolean isPrimary();

    /**
     * Indicates that company is a service company, providing internal service capabilities
     * @return providingServices
     */
    boolean isProvidingServices();

    /**
     * Indicates that company is a partner company
     * @return partner
     * 
     * used in record stylesheets in order to decide which header + footer is shown
     */
    boolean isPartner();

    /**
     * Indicates if client logo is available
     * @return true if logo available
     */
    boolean isLogoAvailable();

    /**
     * Enables/disables logo availability flag
     * @param logoAvailable sets that a logo is available or not
     */
    void setLogoAvailable(boolean logoAvailable);

    /**
     * Provides the path to the logo
     * @return logoPath
     */
    String getLogoPath();

    /**
     * Sets the path to the logo
     * @param logoPath
     */
    void setLogoPath(String logoPath);

    /**
     * Indicates if vat reporting is enabled for this company.
     * @return true if vat reporting is enabled
     */
    boolean isVatReports();

    /**
     * Enables/disables vat reporting for this company.
     * @param vatReports
     */
    void setVatReports(boolean vatReports);

    /**
     * Provides the vat reporting start date.
     * @return start date of vat reports
     */
    Date getVatReportsStart();

    /**
     * Sets the vat reporting start date.
     * @param vatReportsStart
     */
    void setVatReportsStart(Date vatReportsStart);

    /**
     * Provides the current logical name for vat reporting period.
     * Possible values are 'month', 'quarter' or 'year' 
     * @return var report period name
     */
    String getVatReportPeriod();

    /**
     * Sets the current logical name for vat reporting period.
     * @param varReportPeriod as 'month', 'quarter' or 'year'
     */
    void setVatReportPeriod(String vatReportPeriod);

    /**
     * Provides the vat reporting method as 'actual' or 'target'
     * @return vat report method
     */
    String getVatReportMethod();

    /**
     * Sets the vat reporting method
     * @param vatReportMethod as 'actual' or 'target'
     */
    void setVatReportMethod(String vatReportMethod);

    /**
     * Indicates if company registration document is available
     * @return true if registration available
     */
    boolean isCompanyRegistrationAvailable();

    /**
     * Enables/disables company registration document availability flag
     * @param companyRegistrationAvailable
     */
    void setCompanyRegistrationAvailable(boolean companyRegistrationAvailable);

    /**
     * Provides the associated registered office if available and required
     * @return registeredOffice
     */
    BranchOffice getRegisteredOffice();

    /**
     * Provides company values as xml
     * @param rootElementName
     * @return xml
     */
    Element getXML(String rootElementName);

    /**
     * Provides a mapped representation of the object graph
     * @param ignoreRegisteredOffice do not add registered office values (avoid recursion)
     * @return mapped
     */
    Map<String, Object> getMapped(boolean ignoreRegisteredOffice);
}
