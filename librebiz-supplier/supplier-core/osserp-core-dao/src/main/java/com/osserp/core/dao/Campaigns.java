/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 20, 2011 3:44:23 PM 
 * 
 */
package com.osserp.core.dao;

import java.util.List;

import com.osserp.common.Option;
import com.osserp.core.crm.Campaign;
import com.osserp.core.crm.CampaignGroup;
import com.osserp.core.crm.CampaignType;

/**
 * @author Rainer Kirchner <rk@osserp.com>
 */
public interface Campaigns {

    /**
     * Provides all activated campaign groups
     * @return groups
     */
    List<CampaignGroup> findGroups();

    /**
     * Provides all campaign groups (including eol)
     * @return groups
     */
    List<CampaignGroup> getGroups();

    /**
     * Provides all activated campaign types
     * @return types
     */
    List<CampaignType> findTypes();

    /**
     * Provides all campaign types (including eol)
     * @return types
     */
    List<CampaignType> getTypes();

    /**
     * Provides all campaigns regarding eol and parent. 
     * @return all campaigns
     */
    List<Campaign> findAllCampaigns();

    /**
     * Provides all selectable campaigns (not eol and not parent)
     * @return selectableCampaigns
     */
    List<Campaign> findSelectableCampaigns();

    /**
     * Provides all currently selected campaigns (assigned as request origin in open requests)
     * @return selectedCampaigns
     */
    List<Campaign> findSelectedCampaigns();

    /**
     * Provides a selection of all available possible parent campaigns (required for new assignments)
     * @param campaign
     * @return availableParents
     */
    List<Option> getAvailableParents(Campaign campaign);

    /**
     * Loads an existing campaign
     * @param id
     * @return campaign
     */
    Campaign getCampaign(Long id);

    /**
     * Provides all top level campaigns (including eol).
     * @return campaigns
     */
    List<Campaign> getCampaigns();

    /**
     * Provides all sub campaigns of another campaign (including eol)
     * @param parent id of parent campaign
     * @return campaigns
     */
    List<Campaign> getCampaigns(Long parent);

    /**
     * Indicates that campaign is referenced (not deletable)
     * @param campaignId
     * @return true if referenced
     */
    boolean isReferenced(Long campaignId);

    /**
     * Deletes a campaign
     * @param campaign
     */
    void delete(Campaign campaign);

    /**
     * Creates or updates a campaign
     * @param campaign
     */
    void save(Campaign campaign);

    /**
     * Creates or updates a campaign group
     * @param campaignGroup
     */
    void save(CampaignGroup campaignGroup);

    /**
     * Creates or updates a campaign type
     * @param campaignType
     */
    void save(CampaignType campaignType);

}
