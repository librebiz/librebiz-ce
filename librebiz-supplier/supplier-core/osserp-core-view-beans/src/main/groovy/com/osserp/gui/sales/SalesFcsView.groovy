/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 16, 2014 
 * 
 */
package com.osserp.gui.sales

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.core.BusinessCase
import com.osserp.core.FcsManager
import com.osserp.core.finance.Order
import com.osserp.core.projects.ProjectFcsManager
import com.osserp.core.sales.SalesOrderManager
import com.osserp.gui.fcs.FlowControlActionView

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class SalesFcsView extends FlowControlActionView {
  
    void checkDelivery() {
        String deliveryType = form.getString('type')
        Order order = salesOrderManager.getBySales(businessCase)
        if (!order) {
            selectedAction = null
            throw new ClientException(ErrorCode.ORDER_MISSING)
        }
        try {
            salesOrderManager.checkDeliveryStatus(order, deliveryType)
        } catch (Exception e) {
            selectedAction = null
            throw new ClientException(e.message)
        }
    }
    
    void checkInvoice() throws ClientException {
        Order order = salesOrderManager.getBySales(businessCase)
        if (!order) {
            selectedAction = null
            throw new ClientException(ErrorCode.ORDER_MISSING)
        }
        if (!order.isUnchangeable()) {
            selectedAction = null
            throw new ClientException(ErrorCode.RECORD_CHANGEABLE)
        }
        try {
            salesOrderManager.checkInvoiceStatus(order)
        } catch (Exception e) {
            selectedAction = null
            throw new ClientException(e.message)
        }
    }
    
    void checkOrder() throws ClientException {
        Order order = salesOrderManager.getBySales(businessCase)
        if (!order) {
            selectedAction = null
            throw new ClientException(ErrorCode.ORDER_MISSING)
        }
        if (!order.isUnchangeable()) {
            selectedAction = null
            throw new ClientException(ErrorCode.RECORD_CHANGEABLE)
        }
        if (order.isRecalculationRequired()) {
            selectedAction = null
            throw new ClientException(ErrorCode.CALCULATION_REQUIRED)
        }
    }
    
    @Override
    protected Map<String, String> createActionTargets() {
        [
            downpaymentResponse:          '/sales/salesInvoicePayment/forward?type=1',
            deliveryInvoiceResponse:      '/sales/salesInvoicePayment/forward?type=2',
            finalInvoiceResponse:         '/sales/salesInvoicePayment/forward?type=3',
            paymentResponse:              '/sales/salesInvoicePayment/forward?type=52',
            orderRequest:                 '/sales/salesFcs/checkOrder',
            invoiceRequest:               '/sales/salesFcs/checkInvoice',
            deliveryRequest:              '/sales/salesFcs/checkDelivery?type=final',
            partialDeliveryRequest:       '/sales/salesFcs/checkDelivery?type=partial',
            mainComponentDeliveryRequest: '/sales/salesFcs/checkDelivery?type=partial'
        ]
    }
    
    @Override
    BusinessCase getBusinessCase() {
        businessCaseView.sales
    }
    
    @Override
    void checkSelectedAction() throws ClientException {
        if (selectedAction?.cancelling && !domainUser.isUserRelatedSales(businessCase)) {
            if (!isPermissionGrantByConfig('salesCancellationPermissions')) {
                throw new ClientException(ErrorCode.PERMISSION_DENIED)
            }
        }
        if (selectedAction?.closing) {
            checkOrder()
        }
    }
    
    @Override
    protected FcsManager getFlowControlManager() {
        getService(ProjectFcsManager.class.getName())
    }
    
    protected SalesOrderManager getSalesOrderManager() {
        getService(SalesOrderManager.class.getName())
    }
}
