<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<c:set var="view" value="${sessionScope.recordView}"/>
<c:set var="record" value="${view.record}"/>
<tr>
    <td><fmt:message key="payment"/></td>
    <td>
        <oc:select name="paymentConditionId" value="${record.paymentCondition}" options="recordPaymentConditions" styleClass="input" prompt="false"/>
    </td>
</tr>
<tr>
    <td><fmt:message key="amendmentLabel"/></td>
    <td>
        <input type="text" name="paymentNote" id="paymentNote" class="recordInput" value="<o:out value="${record.paymentNote}"/>"/>
    </td>
</tr>
