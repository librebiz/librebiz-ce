<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="salesPersonSelectorView"/>
<c:choose>
	<c:when test="${view.salesCoPercentMode}">
	</c:when>
	<c:otherwise>
	</c:otherwise>
</c:choose>

<style type="text/css">
.name {
}
</style>

<div class="modalBoxHeader">
	<div class="modalBoxHeaderLeft">
		<c:choose>
			<c:when test="${view.salesCoMode}"><fmt:message key="assignCoSales"/></c:when>
			<c:when test="${view.salesCoPercentMode}"><fmt:message key="assignCoSalesPercentage"/></c:when>
			<c:otherwise><fmt:message key="assignSales"/></c:otherwise>
		</c:choose>
	</div>
</div>

<div class="modalBoxData">
	<div class="subcolumns">
		<div class="subcolumn">
			<div class="spacer"></div>
				<c:choose>
					<c:when test="${view.salesCoPercentMode}">
						<v:ajaxForm name="dynamicForm" targetElement="${view.name}_popup" url="${view.baseLink}/updateSalesCoPercent">
							<table class="table">
								<thead>
									<tr>
										<th colspan="3"><fmt:message key="assignCoSalesPercentage"/></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="center"><fmt:message key="coSalesRate"/></td>
										<td><input type="text" class="number" name="value" value="<o:number value="${view.request.salesCoPercent}" format="percent"/>"/> <fmt:message key="percent"/></td>
										<td class="center">
											<v:link  url="${view.baseLink}/exit"><o:img name="backIcon"/></v:link>
											<input type="image" name="updateSalesCoPercent" src="<o:icon name="saveIcon"/>" />
										</td>
									</tr>
								</tbody>
							</table>
						</v:ajaxForm>
					</c:when>
					<c:otherwise>
						<c:if test="${view.searchMode}">
							<div id="salesPersonSelectorSearch">
								<o:ajaxLookupForm name="salesPersonSelectorForm" url="${view.appLink}/search" targetElement="salesPersonSelectorResult" minChars="0" events="onkeyup" activityElement="value">
									<input style="background: whitesmoke;" type="text" name="value" id="value" value="<o:out value="${view.searchPattern}"/>" />
								</o:ajaxLookupForm>
							</div>
						</c:if>
						<div id="salesPersonSelectorResult">
							<c:import url="_salesPersonSelectorResult.jsp"/>
						</div>
					</c:otherwise>
				</c:choose>
			<div class="spacer"></div>
		</div>
	</div>
</div>
