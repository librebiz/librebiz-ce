/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 27, 2008 9:34:54 AM 
 * 
 */
package com.osserp.gui.admin.records

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.Option
import com.osserp.core.BusinessType
import com.osserp.core.finance.RecordInfoConfigManager
import com.osserp.core.finance.RecordSearch
import com.osserp.core.finance.RecordType
import com.osserp.core.requests.RequestManager
import com.osserp.groovy.web.MenuItem
import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class RecordInfoConfigView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(RecordInfoConfigView.class.getName())

    List<RecordType> recordTypes = []
    List<BusinessType> requestTypes = []
    Long recordType
    Long requestType
    List recordInfos = []
    List<Option> availableInfos = []

    RecordInfoConfigView() {
        headerName  = 'selectionRecordType'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (forwardRequest) {
            recordTypes = recordSearch.recordTypes
            if (!requestTypes) {
                List<BusinessType> businessTypes = requestManager.requestTypes
                businessTypes.each {
                    if (it.isActive()) {
                        requestTypes.add(it)
                    }
                }
            }
        }
    }

    @Override
    protected void createCustomNavigation() {
        if (recordType) {
            if (requestType) {
                nav.defaultNavigation = [
                    navigationLink('/admin/records/recordInfoConfig/selectRequestType', 'backIcon', 'backToLast'),
                    infoEditorLink,
                    homeLink
                ]
            } else {
                nav.defaultNavigation = [
                    navigationLink('/admin/records/recordInfoConfig/selectRecordType', 'backIcon', 'backToLast'),
                    homeLink
                ]
            }
        } else {
            nav.defaultNavigation = [exitLink, homeLink]
        }
    }

    private MenuItem getInfoEditorLink() {
        new MenuItem(
                link: "${context.fullPath}/admin/records/recordInfoEditor/forward?exit=/admin/records/recordInfoConfig/reload",
                icon: 'configureIcon',
                title: 'recordInfoConfigsEditor')
    }

    void selectRecordType() {
        recordType = form.getLong('id')
        if (recordType) {
            headerName  = 'requestOrderTypeSelection'
        } else {
            headerName  = 'selectionRecordType'
        }
        createCustomNavigation()
    }

    void selectRequestType() {
        loadInfos(form.getLong('id'))
        createCustomNavigation()
    }

    void add() throws ClientException {
        recordInfoConfigManager.addDefaultInfo(recordType, requestType, form.getLong('id'))
        loadInfos(requestType)
    }

    void remove() {
        recordInfoConfigManager.removeDefaultInfo(recordType, requestType, form.getLong('id'))
        loadInfos(requestType)
    }
	
	void moveUp() {
		recordInfoConfigManager.moveInfoUp(recordType, requestType, form.getLong('id'))
        loadInfos(requestType)
	}

	void moveDown() {
		recordInfoConfigManager.moveInfoDown(recordType, requestType, form.getLong('id'))
		loadInfos(requestType)
	}
	
    private void loadInfos(Long type) {
        requestType = type
        if (!requestType) {
            recordInfos.clear()
            if (availableInfos) {
                availableInfos.clear()
            }
            headerName  = 'requestOrderTypeSelection'
        } else {
            recordInfos = recordInfoConfigManager.getDefaultInfos(recordType, requestType)
            Set<Long> addedSet = new HashSet<Long>()

            recordInfos.each { addedSet.add(it) }
            List<Option> all = recordInfoConfigManager.activated
            availableInfos = []
            all.each {
                if (!addedSet.contains(it.id)) {
                    availableInfos.add(it.id)
                }
            }
            headerName  = 'standardSettings'
        }
    }

    private RecordInfoConfigManager getRecordInfoConfigManager() {
        getService(RecordInfoConfigManager.class.getName())
    }

    private RecordSearch getRecordSearch() {
        getService(RecordSearch.class.getName())
    }

    private RequestManager getRequestManager() {
        getService(RequestManager.class.getName())
    }
}
