/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 25-Jun-2005 14:47:24 
 * 
 */
package com.osserp.core.finance;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface BillingType extends RecordType {

    static final Long CLASS_INVOICE = 1L;
    static final Long CLASS_INVOICE_PAYMENT = 2L;

    static final String CLASS_PURCHASE_PAYMENTS = "purchasePayments";
    static final String CLASS_SALES_PAYMENTS = "salesPayments";

    static final String BOOKS_PAYMENT = "booksPayment";
    static final String BOOKS_OUTPAYMENT = "booksOutpayment";

    static final String SIMPLE_BILLING = "simpleBilling";
    static final int SIMPLE_BILLING_NAME_LENGTH_MAX = 22;

    static final Long TYPE_CASH_DISCOUNT = 54L;
    static final Long TYPE_CLEARING = 59L;
    static final Long TYPE_CUSTOM_PAYMENT = 50L;
    static final Long TYPE_PAYMENT = 51L;
    static final Long TYPE_PARTIAL_PAYMENT = 52L;

    static final Long TYPE_CREDIT_NOTE = 53L;
    static final Long TYPE_CANCELLATION = 55L;

    static final Long TYPE_PURCHASE_CASH_DISCOUNT = 58L;
    static final Long TYPE_PURCHASE_PAYMENT = 56L;
    static final Long TYPE_PURCHASE_PARTIAL_PAYMENT = 57L;

    static final Long CLOSE_PAYMENT = 100L;

    static final String CLEARING_LIMIT_PROPERTY = "recordClearDecimalAmountLimit";

    static final Long[] REGULAR_PAYMENTS = {
        TYPE_PURCHASE_PAYMENT,
        TYPE_PURCHASE_PARTIAL_PAYMENT,
        TYPE_PAYMENT,
        TYPE_PARTIAL_PAYMENT
    };


    /**
     * Provides the class of the billing. The relation 
     * replaces the previous type declaration, e.g. 
     * new billingType.billingClass.id == old billingType.type
     * @return billingClass
     */
    BillingClass getBillingClass();

    /**
     * Provides the id of the associated record type if type is fixed.
     * Unused! May be removed in the future.
     * @return record type id
     */
    Long getRecordType();
    
    /**
     * Indicates if billing type represents a record of type invoice
     * @return true if billing type is associated with invoice record type
     */
    boolean isInvoice();

    /**
     * Indicates if payment represents a cash discount
     * @return true if cash discount
     */
    boolean isCashDiscount();
    
    /**
     * Indicates if billing type represents a record of type downpayment
     * @return true if billing type is associated with downpayment record type
     */
    boolean isDownpayment();

    /**
     * Indicates if records of this type should be billed with reduced tax rate
     * @return true if tax rate is reduced 
     */
    boolean isReducedTax();

    /**
     * Indicates if records of this type are tax free
     * @return true if tax free
     */
    boolean isTaxFree();

    /**
     * Provides the id of the legal background list for tax free records 
     * @return taxFreeId
     */
    Long getTaxFreeId();

    /**
     * Indicates if type represents a tax payment
     * @return taxPayment
     */
    boolean isTaxPayment();
    
    /**
     * Indicates if payment is salary itself
     * @return true if salary of employee
     */
    boolean isSalary();
    
    /**
     * Indicates if payment is salary related other costs (e.g. tax, etc.)
     * @return true if non-salary
     */
    boolean isNonSalary();
    
    /**
     * Returns the type of the invoice
     * @return type
     */
    Integer getOrderId();
}
