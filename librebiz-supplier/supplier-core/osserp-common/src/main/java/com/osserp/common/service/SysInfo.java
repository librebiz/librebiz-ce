/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 *
 * Portions are Copyright (C) 2000-2005 JBoss project. See comments below. 
 * 
 * Created on 12-Oct-2005 20:53:20 
 * 
 */
package com.osserp.common.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.Property;
import com.osserp.common.beans.PropertyImpl;
import com.osserp.common.util.NumberFormatter;
import com.osserp.common.util.PropertyUtil;

/**
 * 
 * Portions of this code are inspired by a source form file of the JBoss 2.x
 * software, licensed under the LGPL.
 *  
 *  
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SysInfo {
    
    private static final Logger log = LoggerFactory.getLogger(SysInfo.class.getName());

    private Set<String> osserpProps;
    private Set<String> dedicatedProps;
    private Set<String> ignorableProps;
    
    /** The name for the server. */
    private String hostName;

    /** The host address for the server. */
    private String hostAddress;

    private String javaVersion = null;
    private String javaVendor = null;
    private String jvmName = null;
    private String jvmVendor = null;
    private String jvmVersion = null;
    private String os = null;
    private String osArch = null;
    private String compiler = null;
    
    private List<Property> systemProperties = new ArrayList<>();
    private List<Property> osserpProperties = new ArrayList<>();

    public SysInfo() {
        javaVersion = getJVersion();
        javaVendor = getJVendor();
        jvmName = getJavaVMName();
        jvmVendor = getJavaVMVendor();
        jvmVersion = getJavaVMVersion();
        os = getOSName() + " " + getOSVersion();
        osArch = getOSArch();
        compiler = getManagementCompiler();
        
        dedicatedProps = new HashSet();
        for (int i = 0, j = DEDICATED_PROPS.length; i < j; i++) {
            dedicatedProps.add(DEDICATED_PROPS[i]); 
        }
        
        ignorableProps = new HashSet();
        for (int i = 0, j = IGNORABLE_PROPS.length; i < j; i++) {
            ignorableProps.add(IGNORABLE_PROPS[i]); 
        }
        osserpProps = new HashSet();
        osserpProps.add("osserpConf");
        osserpProps.add("osserpSetup");
    }

    public SysInfo(boolean setupEnabled) {
        this();
        if (setupEnabled && !isSetupEnabled()) {
            boolean found = false;
            List<Property> props = getOsserpProperties();
            for (int i = 0, j = props.size(); i < j; i++) {
                Property next = props.get(i);
                if ("osserpSetup".equals(next.getName())) {
                    found = true;
                    next.setValue("true");
                    if (log.isDebugEnabled()) {
                        log.debug("<init> setup enabled");
                    }
                }
            }
            if (!found) {
                osserpProperties.add(new PropertyImpl("osserpSetup", "true"));
                if (log.isDebugEnabled()) {
                    log.debug("<init> setup enabled");
                }
            }
        }
    }
    
    /**
     * Indicates if setup mode is enabled in the backend.
     * @return true if application startet with -DosserpSetup=true
     */
    public boolean isSetupEnabled() {
        List<Property> props = getOsserpProperties();
        for (int i = 0, j = props.size(); i < j; i++) {
            Property next = props.get(i);
            if ("osserpSetup".equals(next.getName())) {
                boolean result = next.isEnabled(); 
                if (log.isDebugEnabled()) {
                    log.debug("isSetupEnabled() done [result=" + result + "]");
                }
                return result;
            }
        }
        return false;
    }
    
    public List<Property> getOtherProperties() {
        loadIfRequired();
        return systemProperties;
    }

    public List<Property> getOsserpProperties() {
        loadIfRequired();
        return osserpProperties;
    }

    public String getJavaVersion() {
        return javaVersion;
    }

    public String getJavaVendor() {
        return javaVendor;
    }

    public String getJvmName() {
        return jvmName;
    }

    public String getJvmVendor() {
        return jvmVendor;
    }

    public String getJvmVersion() {
        return jvmVersion;
    }

    public String getOs() {
        return os;
    }

    public String getOsArch() {
        return osArch;
    }

    public String getCompiler() {
        return compiler;
    }

    /**
     * Returns <tt>Runtime.getRuntime().totalMemory()</tt> as formatted kilobyte.
     */
    public String getTotalMemory() {
        Double d = Double.valueOf(Runtime.getRuntime().totalMemory() / 1024);
        return NumberFormatter.getIntValue(d);
    }

    /**
     * Returns <tt>Runtime.getRuntime().freeMemory()</tt> as formatted kilobyte.
     */
    public String getFreeMemory() {
        Double d = Double.valueOf(Runtime.getRuntime().freeMemory() / 1024);
        return NumberFormatter.getIntValue(d);
    }

    /**
     * Returns <tt>Runtime.getRuntime().maxMemory()</tt> as formatted kilobyte.
     */
    public String getMaxMemory() {
        Double d = Double.valueOf(Runtime.getRuntime().maxMemory() / 1024);
        return NumberFormatter.getIntValue(d);
    }

    /**
     * Returns <tt>Runtime.getRuntime().availableProcessors()</tt>
     */
    public Integer getAvailableProcessors() {
        return Integer.valueOf(Runtime.getRuntime().availableProcessors());
    }

    /**
     * Returns InetAddress.getLocalHost().getHostName();
     */
    public String getHostName() {
        if (hostName == null) {
            try {
                hostName = java.net.InetAddress.getLocalHost().getHostName();
            } catch (java.net.UnknownHostException e) {
                log.error("Error looking up local hostname", e);
                hostName = "<unknown>";
            }
        }

        return hostName;
    }

    /**
     * Returns InetAddress.getLocalHost().getHostAddress();
     */
    public String getHostAddress() {
        if (hostAddress == null) {
            try {
                hostAddress =
                        java.net.InetAddress.getLocalHost().getHostAddress();
            } catch (java.net.UnknownHostException e) {
                log.error("Error looking up local address", e);
                hostAddress = "<unknown>";
            }
        }

        return hostAddress;
    }

    public Integer getActiveThreadCount() {
        return Integer.valueOf(getRootThreadGroup().activeCount());
    }

    public Integer getActiveThreadGroupCount() {
        return Integer.valueOf(getRootThreadGroup().activeGroupCount());
    }

    private ThreadGroup getRootThreadGroup() {
        ThreadGroup group = Thread.currentThread().getThreadGroup();
        while (group.getParent() != null) {
            group = group.getParent();
        }

        return group;
    }

    private String getJVersion() {
        return System.getProperty("java.version");
    }

    private String getJVendor() {
        return System.getProperty("java.vendor");
    }

    private String getJavaVMName() {
        return System.getProperty("java.vm.name");
    }

    private String getJavaVMVersion() {
        return System.getProperty("java.vm.version");
    }

    private String getJavaVMVendor() {
        return System.getProperty("java.vm.vendor");
    }

    private String getOSArch() {
        return System.getProperty("os.arch");
    }

    private String getOSName() {
        return System.getProperty("os.name");
    }

    private String getOSVersion() {
        return System.getProperty("os.version");
    }

    private String getManagementCompiler() {
        return System.getProperty("sun.management.compiler");
    }
    
    private void loadProperties() {
        systemProperties = new ArrayList<>();
        osserpProperties = new ArrayList<>();
        Properties props = System.getProperties();
        Iterator<Map.Entry<Object, Object>> it = props.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<Object, Object> entry = it.next();
            String key = entry.getKey().toString();
            Object value = entry.getValue();
            if (value != null && !dedicatedProps.contains(key)
                    && !ignorableProps.contains(key)) {
                
                Property prp = new PropertyImpl(key, value.toString());
                if (osserpProps.contains(key)) {
                    osserpProperties.add(prp);
                } else {
                    systemProperties.add(prp);
                }
            }
        }
        systemProperties = PropertyUtil.sort(systemProperties, true, false);
        osserpProperties = PropertyUtil.sort(osserpProperties, true, false);
    }
    
    private static final String[] DEDICATED_PROPS = { 
        "java.version", "java.vendor", "java.vm.name", "java.vm.version", 
        "java.vm.vendor", "os.arch", "os.name", "os.version" };
    
    private static final String[] IGNORABLE_PROPS = { 
        "awt.toolkit",
        "catalina.base",
        "catalina.home",
        "catalina.useNaming",
        "common.loader",
        "file.encoding.pkg", 
        "file.separator",
        "java.awt.graphicsenv",
        "java.awt.headless",
        "java.awt.printerjob",
        "java.endorsed.dirs",
        "java.class.path",
        "java.class.version",
        "java.ext.dirs",
        "java.home",
        "java.io.tmpdir",
        "java.library.path",
        "java.naming.factory.initial",
        "java.naming.factory.url.pkgs",
        "java.runtime.name",
        "java.runtime.version",
        "java.specification.name",
        "java.specification.vendor",
        "java.specification.version",
        "java.util.logging.config.file",
        "java.util.logging.manager",
        "java.vendor.url",
        "java.vendor.url.bug",
        "java.vm.info",
        "java.vm.specification.name",
        "java.vm.specification.vendor",
        "java.vm.specification.version", 
        "line.separator",
        "package.access",
        "package.definition",
        "path.separator",
        "server.loader",
        "shared.loader",
        "sun.arch.data.model",
        "sun.boot.class.path",
        "sun.boot.library.path",
        "sun.cpu.endian",
        "sun.cpu.isalist",
        "sun.io.unicode.encoding",
        "sun.iouser.country",
        "sun.java.command",
        "sun.java.launcher",
        "sun.jnu.encoding",
        "sun.management.compiler",
        "sun.os.patch.level",
        "tomcat.util.buf.StringCache.byte.enabled", 
        "tomcat.util.scan.StandardJarScanFilter.jarsToScan",
        "tomcat.util.scan.StandardJarScanFilter.jarsToSkip",
        "user.country",
        "user.home",
        "user.language",
        "user.name"
        };

    private void loadIfRequired() {
        if (systemProperties.isEmpty()) {
            loadProperties();
        }
    }
}
