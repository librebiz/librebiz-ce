/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 17, 2009 12:23:56 PM 
 * 
 */
package com.osserp.core.model;

import java.util.Date;

import com.osserp.common.beans.AbstractOption;
import com.osserp.common.mail.MailDomain;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class MailDomainImpl extends AbstractOption implements MailDomain {

    private boolean aliasOnly = true;
    private boolean fetchmailDomain = false;

    private MailDomain parentDomain = null;

    private String provider = null;
    private String fetchMode = null;
    private String serverName = null;
    private String serverType = null;
    private String smtpServerName = null;

    private String hostIp = null;
    private String hostMx = null;
    private String hostSecondaryMx = null;
    private String hostMxIp = null;
    private String hostSecondaryMxIp = null;
    private Date domainCreated = null;
    private Date domainExpires = null;
    private String domainWhois = null;

    protected MailDomainImpl() {
        super();
    }

    /**
     * Creates a new domain
     * @param createdBy
     * @param client
     * @param parentDomain if domain is alias domain
     * @param name
     * @param provider
     * @param domainCreated
     * @param domainExpires
     */
    public MailDomainImpl(
            Long createdBy,
            Long client,
            MailDomain parentDomain,
            String name,
            String provider,
            Date domainCreated,
            Date domainExpires) {
        super(name.toLowerCase().trim(), client, createdBy);
        this.provider = provider;
        this.domainCreated = domainCreated;
        this.domainExpires = domainExpires;
        if (parentDomain == null) {
            this.aliasOnly = false;
        } else {
            this.aliasOnly = true;
            this.parentDomain = parentDomain;
        }
    }

    public void update(
            Long user,
            String provider,
            String smtpServerName,
            String hostIp,
            String hostMx,
            String hostSecondaryMx,
            String hostMxIp,
            String hostSecondaryMxIp,
            Date domainCreated,
            Date domainExpires) {
        setChangedBy(user);
        setChanged(new Date(System.currentTimeMillis()));
        this.provider = provider;
        this.smtpServerName = smtpServerName;
        this.hostIp = hostIp;
        this.hostMx = hostMx;
        this.hostSecondaryMx = hostSecondaryMx;
        this.hostMxIp = hostMxIp;
        this.hostSecondaryMxIp = hostSecondaryMxIp;
        this.domainCreated = domainCreated;
        this.domainExpires = domainExpires;
    }

    public boolean isAliasOnly() {
        return aliasOnly;
    }

    public void setAliasOnly(boolean aliasOnly) {
        this.aliasOnly = aliasOnly;
    }

    public boolean isFetchmailDomain() {
        return fetchmailDomain;
    }

    public void setFetchmailDomain(boolean fetchmailDomain) {
        this.fetchmailDomain = fetchmailDomain;
    }

    public boolean isFetchmailConfigured() {
        return (isSet(serverName)
                && isSet(serverType)
                && isSet(fetchMode));
    }

    public MailDomain getParentDomain() {
        return parentDomain;
    }

    public void setParentDomain(MailDomain parentDomain) {
        this.parentDomain = parentDomain;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getFetchMode() {
        return fetchMode;
    }

    public void setFetchMode(String fetchMode) {
        this.fetchMode = fetchMode;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public String getServerType() {
        return serverType;
    }

    public void setServerType(String serverType) {
        this.serverType = serverType;
    }

    public String getSmtpServerName() {
        return smtpServerName;
    }

    public void setSmtpServerName(String smtpServerName) {
        this.smtpServerName = smtpServerName;
    }

    public String getHostIp() {
        return hostIp;
    }

    public void setHostIp(String hostIp) {
        this.hostIp = hostIp;
    }

    public String getHostMx() {
        return hostMx;
    }

    public void setHostMx(String hostMx) {
        this.hostMx = hostMx;
    }

    public String getHostSecondaryMx() {
        return hostSecondaryMx;
    }

    public void setHostSecondaryMx(String hostSecondaryMx) {
        this.hostSecondaryMx = hostSecondaryMx;
    }

    public String getHostMxIp() {
        return hostMxIp;
    }

    public void setHostMxIp(String hostMxIp) {
        this.hostMxIp = hostMxIp;
    }

    public String getHostSecondaryMxIp() {
        return hostSecondaryMxIp;
    }

    public void setHostSecondaryMxIp(String hostSecondaryMxIp) {
        this.hostSecondaryMxIp = hostSecondaryMxIp;
    }

    public Date getDomainCreated() {
        return domainCreated;
    }

    public void setDomainCreated(Date domainCreated) {
        this.domainCreated = domainCreated;
    }

    public Date getDomainExpires() {
        return domainExpires;
    }

    public void setDomainExpires(Date domainExpires) {
        this.domainExpires = domainExpires;
    }

    public String getDomainWhois() {
        return domainWhois;
    }

    public void setDomainWhois(String domainWhois) {
        this.domainWhois = domainWhois;
    }
}
