/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 01.10.2004 
 * 
 */
package com.osserp.common;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ErrorCode {

    static final String ERROR_CODE_PREFIX = "error.";
    static final String ACCOUNT_UNBALANCED = "error.account.unbalanced";
    static final String ACTION_ALREADY_USED = "error.action.already.used";
    static final String ACTION_MISSING = "error.action.missing";
    static final String ACTION_NOTE_REQUIRED = "error.action.note.required";
    static final String ADDRESS_EXISTS = "error.address.exists";
    static final String ADDRESS_INCOMPLETE = "error.address.incomplete";
    static final String ADDRESS_TYPE_INVALID = "error.address.type.invalid";
    static final String ADDRESS_TYPE_MISSING = "error.address.type.missing";
    static final String ALTERNATIVE_SOLUTION_REQUIRED = "error.alternative.solution.required";
    static final String AMOUNT_MISSING = "error.amount.missing";
    static final String APPLICATION_CONFIG = "error.application.config";
    static final String APPOINTMENT_DATE_MISSING = "error.appointment.date.missing";
    static final String APPOINTMENT_DATE_PAST = "error.appointment.date.past";
    static final String APPOINTMENT_HOURS_INVALID = "error.appointment.hours.invalid";
    static final String APPOINTMENT_MINUTES_INVALID = "error.appointment.minutes.invalid";
    static final String APPOINTMENT_TYPE_MISSING = "error.appointment.type.missing";
    static final String ATTACHMENT_MISSING = "error.attachment.missing";
    static final String AT_LEAST_ONE_REQUIRED = "error.at.least.one.required";
    static final String AUTH_REQUIRED = "error.auth.required";
    static final String BANKACCOUNT_NAMES_MISSING = "error.bankaccount.names.missing";
    static final String BANKACCOUNT_MISMATCH = "error.bankaccount.mismatch";
    static final String BANKACCOUNT_MISSING = "error.bankaccount.missing";
    static final String BANKACCOUNTS_UNDEFINED = "error.bankaccounts.undefined";
    static final String BOOKING_AFTER_VALIDTIL = "error.booking.after.validtil";
    static final String BOOKING_BEFORE_VALIDFROM = "error.booking.before.validfrom";
    static final String BOOKING_NOT_IN_PERIOD = "error.booking.not.in.period";
    static final String BOOKING_START_MISSING = "error.booking.start.missing";
    static final String BOOKING_STOP_INVALID = "error.booking.stop.invalid";
    static final String BOOKING_STOP_MISSING = "error.booking.stop.missing";
    static final String BOOKING_TEXT_REQUIRED = "error.booking.text.required";
    static final String BRANCH_ASSIGNMENT = "error.branch.assignment";
    static final String BRANCH_INVALID = "error.branch.invalid";
    static final String BRANCH_MISSING = "error.branch.missing";
    static final String CALCULATION_BY_EVENT = "error.calculation.by.event";
    static final String CALCULATION_CHANGED = "error.calculation.changed";
    static final String CALCULATION_INCOMPLETE = "error.calculation.incomplete";
    static final String CALCULATION_LOCKED = "error.calculation.locked";
    static final String CALCULATION_NOT_NEEDED = "error.calculation.not.needed";
    static final String CALCULATION_NOT_SUPPORTED = "error.calculation.not.supported";
    static final String CALCULATION_PRICE_INVALID = "error.calculation.price.invalid";
    static final String CALCULATION_REQUIRED = "error.calculation.required";
    static final String CAMPAIGN_ALREADY_REFERENCED = "error.campaign.already.referenced";
    static final String CANCEL_IMPOSSIBLE_PAYMENT_BOOKED = "error.cancel.impossible.payment.booked";
    static final String CANCEL_NOTE_MISSING = "error.cancel.note.missing";
    static final String CATEGORY_MISSING = "error.category.missing";
    static final String CHANGE_PASSWORD_CURRENT = "error.change.password.current";
    static final String CHANGE_PASSWORD_MATCH = "error.change.password.match";
    static final String CHART_MISSING = "error.chart.missing";
    static final String CHIP_NUMBER_USED = "error.chip.number.used";
    static final String CITY_LOWER = "error.city.lower";
    static final String CITY_MISSING = "error.city.missing";
    static final String CITY_SPACE = "error.city.space";
    static final String CLICK_TO_CALL_IMPOSSIBLE_WITH_THIS_DEVICE = "error.click.to.call.impossible.with.this.device";
    static final String CLOSED_PROJECT_FOUND = "error.closed.project.found";
    static final String COMMISSION_NOT_SUPPORTED = "error.commission.not.supported";
    static final String COMPANY_AT_PRIVATE = "error.company.at.private";
    static final String COMPANY_OR_LASTNAME = "error.company.or.lastname";
    static final String COMPANY_INVALID = "error.company.invalid";
    static final String COMPANY_MISSING = "error.company.missing";
    static final String COMPANY_SPACE = "error.company.space";
    static final String COMPONENT_MISSING = "error.component.missing";
    static final String CONDITIONS_EXISTING = "error.conditions.existing";
    static final String CONFIG_MISSING = "error.config.missing";
    static final String CONTACT_ALREADY_BRANCH = "error.contact.already.branch";
    static final String CONTACT_ALREADY_CLIENT = "error.contact.already.client";
    static final String CONNECTION_NOT_POSSIBLE_AT_THE_MOMENT_DIAL_MANUALLY = "error.connection.not.possible.at.the.moment.dial.manually";
    static final String COST_CENTER_MISSING = "error.cost.center.missing";
    static final String CONTACTS_MISSING_BOOKING_TYPE = "error.contacts.missing.booking.type";
    static final String CONTACT_EQUAL_EXISTS = "error.contact.equal.exists";
    static final String CONTACT_EXISTS = "error.contact.exists";
    static final String CONTACT_NOT_CLIENT = "error.contact.not.client";
    static final String CONTACT_PERSON_SELECTED = "error.contact.person.selected";
    static final String CONTACT_TYPE_MISSING = "error.contact.type.missing";
    static final String CONTACT_TYPE_INVALID = "error.contact.type.invalid";
    static final String COUNTRY_MISSING = "error.country.missing";
    static final String CORRECTION_EXPECTED = "error.correction.expected";
    static final String CORRECTION_DOCUMENT_MISSING = "error.correction.document.missing";
    static final String CORRECTION_MISSING = "error.correction.missing";
    static final String CORRECTION_NOTE_MISSING = "error.correction.note.missing";
    static final String CUSTOMER_EMAIL_MISSING = "error.customer.email.missing";
    static final String CUSTOMER_ID_NOT_EXISTING = "error.customer.id.not.existing";
    static final String CUSTOMER_PAYMENT_AGREEMENT_MISSING = "error.customer.payment.agreement.missing";
    static final String CUSTOMER_ORIGIN_MISSING = "error.customer.origin.missing";
    static final String CUSTOMER_STATUS_MISSING = "error.customer.status.missing";
    static final String CUSTOMER_TYPE_MISSING = "error.customer.type.missing";
    static final String CUSTOMER_VALUES_MISSING = "error.customer.values.missing";
    static final String CUSTOMER_VALUES_INVALID = "error.customer.values.invalid";
    static final String DATA_ALREADY_SENT = "error.data.already.sent";
    static final String DATA_ALREADY_SYNCHRONIZED = "error.data.already.synchronized";
    static final String DATE_FORMAT = "error.date.format";
    static final String DATE_FROM_MISSING = "error.date.from.missing";
    static final String DATE_FUTURE = "error.date.future";
    static final String DATE_INVALID = "error.date.invalid";
    static final String DATE_OR_TIME_REQUIRED = "error.date.or.time.required";
    static final String DATE_MISSING = "error.date.missing";
    static final String DATE_PAST = "error.date.past";
    static final String DATE_START_AFTER_STOP = "error.date.start.after.stop";
    static final String DATE_START_REQUIRED = "error.date.start.required";
    static final String DATE_STOP_BEFORE_START = "error.date.stop.before.start";
    static final String DATE_STOP_REQUIRED = "error.date.stop.required";
    static final String DATE_UNTIL_MISSING = "error.date.until.missing";
    static final String DECIMAL_FORMAT = "error.decimal.format";
    static final String DEFAULT_CONFIG_HAS_NO_EMPLOYEES = "error.default.config.has.no.employees";
    static final String DEFAULT_STOCK_MISSING = "error.default.stock.missing";
    static final String DELIVERIES_FOUND = "error.deliveries.found";
    static final String DELIVERY_DATE_NOT_SUPPPORTED = "error.delivery.date.not.supported";
    static final String DELIVERY_ITEMS_FOUND = "error.delivery.items.found";
    static final String DELIVERY_ITEMS_MISSING = "error.delivery.items.missing";
    static final String DELIVERY_ITEMS_OVERFLOW = "error.delivery.items.overflow";
    static final String DELIVERY_MISSING = "error.delivery.missing";
    static final String DEPENDING_ADDRESS_FOUND = "error.depending.address.found";
    static final String DESCRIPTION_MISSING = "error.description.missing";
    static final String DEVICE_TYPE_MISSING = "error.device.type.missing";
    static final String DIRECT_BOOKING_SUPPLIERS_MISSING = "error.direct.booking.suppliers.missing";
    static final String DIRECTORY_ACCESS = "error.directory.access";
    static final String DIRECTORY_SERVICE_CONFIG = "error.directory.service.config";
    static final String DIRECTORY_SERVICE_MISSING = "error.directory.service.missing";
    static final String DISCOUNTABLE = "error.discountable";
    static final String DISCOUNT_DISABLED = "error.discount.disabled";
    static final String DISCOUNT_NOT_DISCOUNTABLE = "error.discount.not.discountable";
    static final String DISCOUNT_QUANTITY = "error.discount.quantity";
    static final String DISTRICT_MISSING = "error.district.missing";
    static final String DOCUMENT_LOAD = "error.document.load";
    static final String DOCUMENT_NOT_ACTIVATED = "error.document.not.activated";
    static final String DOCUMENT_NOT_CREATED = "error.document.not.created";
    static final String DOCUMENT_NOT_FOUND = "error.document.not.found";
    static final String DOCUMENT_NOT_CALLED = "error.document.not.called";
    static final String DOCUMENT_SELECTION = "error.document.selection";
    static final String DOCUMENT_TYPE_CONFIG = "error.document.type.config";
    static final String DOCUMENT_TYPE_FORBIDDEN = "error.document.type.forbidden";
    static final String DOCUMENT_TYPE_INVALID = "error.document.type.invalid";
    static final String DOMAIN_INVALID = "error.domain.invalid";
    static final String DOMAIN_MISSING = "error.domain.missing";
    static final String EDITOR_TIMEOUT = "error.editor.timeout";
    static final String EMAIL_EDITOR_INIT = "error.email.editor.init";
    static final String EMAIL_EXISTS = "error.email.exists";
    static final String EMAIL_INVALID = "error.email.invalid";
    static final String EMAIL_MISSING = "error.email.missing";
    static final String EMAIL_OR_PASSWORD_MISSING = "error.email.or.password.missing";
    static final String EMAIL_OR_PHONE_OR_MOBILE_REQUIRED = "error.email.or.phone.or.mobile.required";
    static final String EMAIL_SENDING_NOT_SUPPORTED = "error.email.sending.not.supported";
    static final String EMAIL_UNDELIVERABLE = "error.email.undeliverable";
    static final String EMAIL_USER_INVALID = "error.email.user.invalid";
    static final String EMAIL_USER_UNKNOWN = "error.email.user.unknown";
    static final String EMPLOYEE_CONFIG_INVALID = "error.employee.config.invalid";
    static final String EMPLOYEE_ID_INVALID = "error.employee.id.invalid";
    static final String EMPLOYEE_ID_MISSING = "error.employee.id.missing";
    static final String EMPLOYEE_GROUP = "error.employee.group";
    static final String EMPLOYEE_NOT_DELETABLE = "error.employee.not.deletable";
    static final String EMPLOYEE_TYPE_MISSING = "error.employee.type.missing";
    static final String ENTITY_CREATE = "error.entity.create";
    static final String ENTITY_FIND = "error.entity.find";
    static final String ENTRY_EXISTS = "error.entry.exists";
    static final String ENTRY_MISSING = "error.entry.missing";
    static final String EXECUTIVE_INFO_MISSING = "error.executive.info.missing";
    static final String FAX_INCOMPLETE = "error.fax.incomplete";
    static final String FAX_PREFIX_INVALID = "error.fax.prefix.invalid";
    static final String FAX_PREFIX_MISSING = "error.fax.prefix.missing";
    static final String FAX_NUMBER_INVALID = "error.fax.number.invalid";
    static final String FAX_NUMBER_MISSING = "error.fax.number.missing";
    static final String FEDERAL_STATE_MISSING = "error.federal.state.missing";
    static final String FILE_ACCESS = "error.file.access";
    static final String FILE_EMPTY = "error.file.empty";
    static final String FILE_EXISTS = "error.file.exists";
    static final String FILE_MISSING = "error.file.missing";
    static final String FILE_TYPE_INVALID = "error.file.type.invalid";
    static final String FIRSTNAME_INVALID = "error.firstname.invalid";
    static final String FIRSTNAME_LOWER = "error.firstname.lower";
    static final String FIRSTNAME_MISSING = "error.firstname.missing";
    static final String FIRSTNAME_SPACE = "error.firstname.space";
    static final String FLOW_CONTROL_CONFIG_EMPTY = "error.flowcontrol.config.empty";
    static final String FLOW_CONTROL_EMPTY = "error.flowcontrol.empty";
    static final String FLOW_CONTROL_DISPLAY_EVERYTIME = "error.flowcontrol.display.everytime";
    static final String FLOW_CONTROL_MISSING = "error.flowcontrol.missing";
    static final String FLOW_CONTROL_NOT_THROWABLE = "error.flowcontrol.not.throwable";
    static final String FLOW_CONTROL_STOPPING_EXCLUSIVE = "error.flowcontrol.stopping.exclusive";
    static final String FLOW_CONTROL_WASTEBASKET = "error.flowcontrol.wastebasket";
    static final String FLOW_INCREMENT_MISSING = "error.flowincrement.missing";
    static final String GROUP_ADD_AVAILABLE = "error.group.add.available";
    static final String GROUP_AMBIGUOUS = "error.group.ambiguous";
    static final String GROUP_DELETE_CONTEXT_INVALID = "error.group.delete.context.invalid";
    static final String GROUP_MISSING = "error.group.missing";
    static final String GROUP_SELECTION = "error.group.selection";
    static final String GROUPWARE_DISABLED = "error.groupware.disabled";
    static final String HEADQUARTER_MISSING = "error.headquarter.missing";
    static final String HISTORICAL_DATA = "error.historical.data";
    static final String HOURS_INVALID = "error.hours.invalid";
    static final String ID_EXISTS = "error.id.exists";
    static final String ID_INVALID = "error.id.invalid";
    static final String ID_REQUIRED = "error.id.required";
    static final String INSTALLATION_DATE_INVALID = "error.installation.date.invalid";
    static final String INSTALLATION_DATE_MISSING = "error.installation.date.missing";
    static final String INSTALLATION_DATE_MONTH_MISSING = "error.installation.date.month.missing";
    static final String INSTALLATION_DATE_NOT_SUPPORTED = "error.installation.date.not.supported";
    static final String INSTALLATION_DATE_PAST = "error.installation.date.past";
    static final String INSTALLATION_DATE_YEAR_MISSING = "error.installation.date.year.missing";
    static final String INTERNETEXPORT_DESCRIPTION_MISSING = "error.internetexport.description.missing";
    static final String INTERNETEXPORT_PICTURE_MISSING = "error.internetexport.picture.missing";
    static final String INTERVAL_PERIOD = "error.interval.period";
    static final String INVALID_DURATION = "error.invalid.duration";
    static final String INVALID_GROUP = "error.invalid.group";
    static final String INVALID_CONFIG = "error.invalid.config";
    static final String INVALID_CONTEXT = "error.invalid.context";
    static final String INVALID_DOMAIN_NAME = "error.invalid.domain.name";
    static final String INVALID_MACADDRESS = "error.invalid.macaddress";
    static final String INVALID_ORDER_TYPE_CONFIG = "error.invalid.order.type.config";
    static final String INVALID_PRIMARY_KEY = "error.pk";
    static final String INVALID_STATUS_FOUND = "error.invalid.status.found";
    static final String INVOICE_MISSING = "error.invoice.missing";
    static final String INVOICES_FOUND_CANCEL_DENIED = "error.invoices.found.cancel.denied";
    static final String INVOICES_FOUND_CHANGE_DENIED = "error.invoices.found.change.denied";
    static final String INVOICES_FOUND_CUSTOMER_CHANGE_DENIED = "error.invoices.found.customer.change.denied";
    static final String INVOICES_FOUND_DELETE_DENIED = "error.invoices.found.delete.denied";
    static final String ITEM_EXISTING = "error.item.existing";
    static final String ITEM_MISSING = "error.item.missing";
    static final String JUST_WHOLE_NUMBERS = "error.just.whole.numbers";
    static final String KANBAN_NOT_STOCK_AFFECTING = "error.kanban.not.stock.affecting";
    static final String LASTNAME_LOWER = "error.lastname.lower";
    static final String LASTNAME_MISSING = "error.lastname.missing";
    static final String LASTNAME_SPACE = "error.lastname.space";
    static final String LDAP_CONFIG_INVALID = "error.ldap.config.invalid";
    static final String LEGALFORM_MISSING = "error.legalform.missing";
    static final String LOGIN_ACCOUNT_DISABLED = "error.login.account.disabled";
    static final String LOGIN_FAILED = "error.login.failed";
    static final String LOGIN_NAME_MISSING = "error.login.name.missing";
    static final String LOGIN_NAME_INVALID = "error.login.name.invalid";
    static final String LOGIN_LOCAL_ONLY = "error.login.local.only";
    static final String LOGIN_LOGGED = "error.login.logged";
    static final String LOGIN_PASSWORD_MISSING = "error.login.password.missing";
    static final String LOGIN_PASSWORD_INVALID = "error.login.password.invalid";
    static final String MACADDRESS_EXISTS = "error.macaddress.exists";
    static final String MAILTEXT_MISSING = "error.mailtext.missing";
    static final String MAIL_MESSAGE_SIZE = "error.mail.message.size";
    static final String MAIL_PASSWORD_MATCH = "error.mail.password.match";
    static final String MAIL_SENDING_FAILED = "error.mail.sending.failed";
    static final String MAIL_TEMPLATE_CONFIG = "error.mail.template.config";
    static final String MAIL_TEMPLATE_MISSING = "error.mail.template.missing";
    static final String MAIL_TEMPLATE_MISSING_ORIGINATOR = "error.mail.template.missing.originator";
    static final String MAIL_TEMPLATE_MISSING_SUBJECT = "error.mail.template.missing.subject";
    static final String MAIN_COMPONENTS_MISSING = "error.main.components.missing";
    static final String MANAGER_MISSING = "error.manager.missing";
    static final String CLIENT_NOT_DELETABLE = "error.client.not.deletable";
    static final String CLIENT_NOT_SELECTED = "error.client.not.selected";
    static final String MARKER_NOT_IN_PERIOD = "error.marker.not.in.period";
    static final String MATCHCODE_MAXLENGTH = "error.matchcode.maxlength";
    static final String MINIMUM_OR_RATE_ONLY = "error.minimum.or.rate.only";
    static final String MINUTES_INVALID = "error.minutes.invalid";
    static final String MOBILE_INCOMPLETE = "error.mobile.incomplete";
    static final String MOBILE_PREFIX_INVALID = "error.mobile.prefix.invalid";
    static final String MOBILE_PREFIX_MISSING = "error.mobile.prefix.missing";
    static final String MOBILE_NUMBER_INVALID = "error.mobile.number.invalid";
    static final String MOBILE_NUMBER_MISSING = "error.mobile.number.missing";
    static final String MONTH_MISSING = "error.month.missing";
    static final String MONTH_SELECTION_MISSING = "error.month.selection.missing";
    static final String MULTIPLE_RECORDS_FOUND = "error.multiple.records.found";
    static final String NAME_EXISTS = "error.name.exists";
    static final String NAME_INVALID = "error.name.invalid";
    static final String NAME_MAXLENGTH = "error.name.maxlength";
    static final String NAME_MISSING = "error.name.missing";
    static final String NEWS_MISSING = "error.news.missing";
    static final String NEWS_HEADLINE_LENGTH = "error.news.headline.length";
    static final String NEWS_HEADLINE_MISSING = "error.news.headline.missing";
    static final String NO_CLICK_TO_CALL_ENABLED = "error.no.click.to.call.enabled";
    static final String NO_DATA_AVAILABLE = "error.no.data.available";
    static final String NO_DATA_FOUND = "error.no.data.found";
    static final String NO_TICKETS_AVAILABLE = "error.no.tickets.available";
    static final String NO_TELEPHONE_CONFIGURATION_SELECTED = "error.no.telephoneConfiguration.selected";
    static final String NO_TELEPHONE_CONFIGURATIONS_FOUND = "error.no.telephoneConfigurations.found";
    static final String NO_TELEPHONE_SET = "error.no.telephone.set";
    static final String NO_TELEPHONE_SYSTEM_SET = "error.no.telephone.system.set";
    static final String NO_TELEPHONES_FOUND_IN_BRANCH = "error.no.telephones.found.in.branch";
    static final String NOT_DELETABLE_FINANCE_RECORD_FOUND = "error.not.deletable.finance.record.found";
    static final String NULL_PARAM = "error.null.param";
    static final String NULL_TEXT = "error.null.text";
    static final String NUMBER_EXPECTED = "error.number.expected";
    static final String NUMBER_INVALID = "error.number.invalid";
    static final String OFFER_ACTIVATION = "error.offer.activation";
    static final String OFFER_AMOUNT_MISSING = "error.offer.amount.missing";
    static final String OFFER_MISSING = "error.offer.missing";
    static final String OPEN_PROJECT_FOUND = "error.open.project.found";
    static final String OPEN_REQUEST_FOUND = "error.open.request.found";
    static final String OPEN_TODOS_MISSING = "error.open.todos.missing";
    static final String OPERATION_NOT_SUPPORTED = "error.operation.not.supported";
    static final String ORDER_BILLED = "error.order.billed";
    static final String ORDER_CALCULATION_ONLY_METHOD = "error.order.calculation.only.method";
    static final String ORDER_CLEARED = "error.order.cleared";
    static final String ORDER_CLOSED = "error.order.closed";
    static final String ORDER_CONFIRMATION_CHECKED = "error.order.confirmation.checked";
    static final String ORDER_DATE_MISSING = "error.order.date.missing";
    static final String ORDER_DELIVERIES_FOUND = "error.order.deliveries.found";
    static final String ORDER_DIFFERENCES_MISSING = "error.order.differences.missing";
    static final String ORDER_ITEMS_MISSING = "error.order.items.missing";
    static final String ORDER_MISSING = "error.order.missing";
    static final String ORDER_SIGNED_MISSING = "error.order.signed.missing";
    static final String ORIGIN_MISSING = "error.origin.missing";
    static final String ORIGINATOR_INVALID = "error.originator.invalid";
    static final String ORIGINATOR_MISSING = "error.originator.missing";
    static final String PACKAGE_CONFIG_INVALID = "error.package.config.invalid";
    static final String PACKAGE_MAXCOUNT = "error.package.maxcount";
    static final String PACKAGE_MISSING = "error.package.missing";
    static final String PACKAGING_UNIT_MISSING = "error.packaging.unit.missing";
    static final String PARENT_ACCOUNT_MISSING = "error.parent.account.missing";
    static final String PASSWORD_INVALID = "error.password.invalid";
    static final String PASSWORD_MATCH = "error.password.match";
    static final String PASSWORD_MISSING = "error.password.missing";
    static final String PASSWORD_REQUIRED = "error.password.required";
    static final String PAYMENT_MISSING = "error.payment.missing";
    static final String PAYMENT_CONDITION_MISSING = "error.payment.condition.missing";
    static final String PAYMENT_AGREEMENT_MISSING = "error.payment.agreement.missing";
    static final String PAYMENT_STEPS_INVALID = "error.payment.steps.invalid";
    static final String PAYMENT_STEPS_MISSING = "error.payment.steps.missing";
    static final String PAYMENT_TARGET_INVALID = "error.payment.target.invalid";
    static final String PAYMENT_TARGET_MISSING = "error.payment.target.missing";
    static final String PAYMENTS_FOUND_DELETE_DENIED = "error.payments.found.delete.denied";
    static final String PERIOD_SELECTION_MISSING = "error.period.selection.missing";
    static final String PERIOD_START_ALREADY_BILLED = "error.period.start.already.billed";
    static final String PERIOD_STOP_ALREADY_BILLED = "error.period.stop.already.billed";
    static final String PERMISSION_EXISTS = "error.permission.exists";
    static final String PERMISSION_DENIED = "error.permission.denied";
    static final String PHONE_CONFIG_MISSING = "error.phone.config.missing";
    static final String PHONE_EXISTING = "error.phone.existing";
    static final String PHONE_INCOMPLETE = "error.phone.incomplete";
    static final String PHONE_PREFIX_INVALID = "error.phone.prefix.invalid";
    static final String PHONE_PREFIX_MISSING = "error.phone.prefix.missing";
    static final String PHONE_NUMBER_INVALID = "error.phone.number.invalid";
    static final String PHONE_NUMBER_MISSING = "error.phone.number.missing";
    static final String PLUGIN_DISABLED = "error.plugin.disabled";
    static final String PLUGIN_MISSING = "error.plugin.missing";
    static final String POSITION_UNCHANGEABLE = "error.position.unchangeable";    
    static final String POSACCOUNT_INVALID = "error.posaccount.invalid";
    static final String POSACCOUNT_MISSING = "error.posaccount.missing";
    static final String POWER_UNIT_MISSING = "error.power.unit.missing";
    static final String PREFIX_INVALID = "error.prefix.invalid";
    static final String PREVIEW_NOT_AVAILABLE = "error.preview.not.available";
    static final String PRICE_MISSING = "error.price.missing";
    static final String PRICE_LIMIT = "error.price.limit";
    static final String PRICE_LIMIT_OVERWRITE = "error.price.limit.overwrite";
    static final String PRIMARY_DOMAIN_EXISTS = "error.primary.domain.exists";
    static final String PRIORITY_MISSING = "error.priority.missing";
    static final String PRODUCT_CONFIG_INVALID = "error.product.config.invalid";
    static final String PRODUCT_FILTER_PRODUCTS_MISSING = "error.product.filter.products.missing";
    static final String PRODUCT_FILTER_CONFIG_MISSING = "error.product.filter.config.missing";
    static final String PRODUCT_INCOMPLETE = "error.product.incomplete";
    static final String PRODUCT_GTIN13_MAXLENGTH = "error.product.gtin13.maxlength";
    static final String PRODUCT_GTIN14_MAXLENGTH = "error.product.gtin14.maxlength";
    static final String PRODUCT_GTIN8_MAXLENGTH = "error.product.gtin8.maxlength";
    static final String PRODUCT_HAS_STOCK = "error.product.has.stock";
    static final String PRODUCT_HEIGHT_MISSING = "error.product.height.missing";
    static final String PRODUCT_WIDTH_MISSING = "error.product.width.missing";
    static final String PRODUCT_MISSING = "error.product.missing";
    static final String PRODUCT_SELECTION_INVALID = "error.product.selection.invalid";
    static final String PRODUCT_TYPE_MISSING = "error.product.type.missing";
    static final String PROFILE_SELECTION_MISSING = "error.profile.selection.missing";
    static final String PROJECT_ADDRESS_MISSING = "error.project.address.missing";
    static final String PROJECT_ID_INVALID = "error.project.id.invalid";
    static final String PROJECT_FINISHED = "error.project.finished";
    static final String PROJECT_NOT_FINISHED = "error.project.not.finished";
    static final String PROJECT_SETUP_INVALID = "error.project.setup.invalid";
    static final String PURCHASE_ORDER_CLOSED = "error.purchase.order.closed";
    static final String PURCHASE_PRICE_MISSING = "error.purchase.price.missing";
    static final String PURCHASE_RECEIPT_FOUND = "error.purchase.receipt.found";
    static final String QUANTITY_CORRECTION_REQUIRED = "error.quantity.correction.required";
    static final String QUANTITY_MAX = "error.quantity.max";
    static final String QUANTITY_MISSING = "error.quantity.missing";
    static final String QUANTITY_UNIT_MISSING = "error.quantity.unit.missing";
    static final String QUERY_INVALID = "error.query.invalid";
    static final String QUERY_RESULT_MISSING = "error.query.result.missing";
    static final String RECIPIENT_INVALID = "error.recipient.invalid";
    static final String RECIPIENTS_INVALID = "error.recipients.invalid";
    static final String RECIPIENT_MISSING = "error.recipient.missing";
    static final String RECORD_ALREADY_CANCELLED = "error.record.already.cancelled";
    static final String RECORD_CHANGEABLE = "error.record.changeable";
    static final String RECORD_DATE_MISSING = "error.record.date.missing";
    static final String RECORD_DELETE_FAILED_UNKNOWN = "error.record.delete.failed.unknown";
    static final String RECORD_DOCUMENT_MISSING = "error.record.document.missing";
    static final String RECORD_ISSUER_MISSING = "error.record.issuer.missing";
    static final String RECORD_EXISTING = "error.record.existing";
    static final String RECORD_HISTICAL_NONE_EXISTING = "error.record.historical.none.existing";
    static final String RECORD_ID_MISSING = "error.record.id.missing";
    static final String RECORD_ID_NUMBER = "error.record.id.number";
    static final String RECORD_INVOICE_OPEN_MISSING = "error.record.invoice.open.missing";
    static final String RECORD_ITEMS_MISSING = "error.record.items.missing";
    static final String RECORD_MISSING = "error.record.missing";
    static final String RECORD_NOT_AVAILABLE = "error.record.not.available";
    static final String RECORD_NOT_PRINTED = "error.record.not.printed";
    static final String RECORD_OPERATION_NOT_SUPPORTED = "error.record.operation.not.supported";
    static final String RECORD_UNCHANGEABLE = "error.record.unchangeable";
    static final String RECORD_WAITFORAPPROVAL = "error.record.waitForApproval";
    static final String REFERENCE_PICTURE_MISSING = "error.reference.picture.missing";
    static final String REFERENCED_PRODUCT_EXISTS = "error.referenced.product.exists";
    static final String REGION_SELECTION_MISSING = "error.region.selection.missing";
    static final String REGION_ZIPCODE_EXISTS = "error.region.zipcode.exists";
    static final String REGISTRATION_ALREADY_PERFORMED = "error.registration.already.performed";
    static final String REMINDER_DATE_MISSING = "error.reminder.date.missing";
    static final String REMINDER_DATE_PAST = "error.reminder.date.past";
    static final String REMINDER_HOURS_INVALID = "error.reminder.hours.invalid";
    static final String REMINDER_MINUTES_INVALID = "error.reminder.minutes.invalid";
    static final String REQUEST_TYPE_INVALID = "error.request.type.invalid";
    static final String REQUEST_TYPE_MISSING = "error.request.type.missing";
    static final String ROW_COUNT_EXCEEDS = "error.row.count.exceeds";
    static final String RUNTIME_CLASS_INVALID = "error.runtime.class.invalid";
    static final String SALESEXECUTIVE_REQUIRED = "error.salesexecutive.required";
    static final String SALESPERSON_BRANCH_MISSING = "error.salesperson.branch.missing";
    static final String SALESPERSON_OR_MANAGER_REQUIRED = "error.salesperson.or.manager.required";
    static final String SALESPERSON_MISSING = "error.salesperson.missing";
    static final String SALESPRICE_LOWER_PURCHASEPRICE = "error.salesprice.lower.purchaseprice";
    static final String SALESREGION_CONFIG_INVALID = "error.salesregion.config.invalid";
    static final String SALESTALK_DATE_MISSING = "error.salestalk.date.missing";
    static final String SALESTALK_DATE_FUTURE = "error.salestalk.date.future";
    static final String SALUTATION_FIRSTNAME = "error.salutation.firstname";
    static final String SALUTATION_MISSING = "error.salutation.missing";
    static final String SALUTATION_INVALID = "error.salutation.invalid";
    static final String SEARCH = "error.search";
    static final String SEARCH_PARAM_INVALID = "error.search.param.invalid";
    static final String SELECTIONS_MISSING = "error.selections";
    static final String SELECTION_MISSING = "error.selection";
    static final String SELECTION_INVALID = "error.selection.invalid";
    static final String SELECTION_MULTIPLE_DISABLED = "error.selection.multiple.disabled";
    static final String SEQUENCE_START_MISSING = "error.sequence.start.missing";
    static final String SERIAL_NOT_EXISTING = "error.serial.not.existing";
    static final String SERIAL_REQUIRED = "error.serial.required";
    static final String SERIALS_MISSING = "error.serials.missing";
    static final String SERIALS_OVERFLOW = "error.serials.overflow";
    static final String SERVICE_TEMPORARILY_UNAVAILABLE = "error.service.temporarily.unavailable";
    static final String SERVICE_DEPENDENCY_UNAVAILABLE = "error.service.dependency.unavailable";
    static final String SERVICES_NOT_BILLED = "error.services.not.billed";
    static final String SETUP_DB_COMPANY = "error.setup.db.company";
    static final String SETUP_DB_CONFIG = "error.setup.db.config";
    static final String SETUP_DB_CONTACT = "error.setup.db.contact";
    static final String SETUP_DB_HEADQUARTER = "error.setup.db.headquarter";
    static final String SETUP_DISABLED = "error.setup.disabled";
    static final String SETUP_REQUIRED = "error.setup.required";
    static final String SHORTINFO_MISSING = "error.shortinfo.missing";
    static final String SHORTKEY_EXISTS = "error.shortkey.exists";
    static final String SHORTKEY_MAXLENGTH = "error.shortkey.maxlength";
    static final String SHORTKEY_MISSING = "error.shortkey.missing";
    static final String SHORTNAME_EXISTS = "error.shortname.exists";
    static final String SHORTNAME_MISSING = "error.shortname.missing";
    static final String SHORTNAME_UMLAUT = "error.shortname.umlaut";
    static final String STATUS_MISSING = "error.status.missing";
    static final String STOCK_AVAILABILITY = "error.stock.availability";
    static final String STOCK_EMPTY = "error.stock.empty";
    static final String STOCK_SELECTION_MISSING = "error.stock.selection.missing";
    static final String STOCK_OR_SERVICE = "error.stock.or.service";
    static final String STREET_LOWER = "error.street.lower";
    static final String STREET_MISSING = "error.street.missing";
    static final String STREET_SPACE = "error.street.space";
    static final String STYLESHEET_MISSING = "error.stylesheet.missing";
    static final String SUBJECT_MISSING = "error.subject.missing";
    static final String SUPPLIER_ASSIGNED_TO_PURCHASE = "error.supplier.assigned.to.purchase";
    static final String SUPPLIER_DEACTIVATED = "error.supplier.deactivated";
    static final String SUPPLIER_UNKNOWN = "error.supplier.unknown";
    static final String TARGET_DATE_MISSING = "error.target.date.missing";
    static final String TARGET_DATE_PAST = "error.target.date.past";
    static final String TAX_FREE_REASON_MISSING = "error.tax.free.reason.missing";
    static final String TAX_FREE_REASON_INVALID = "error.tax.free.reason.invalid";
    static final String TEMPLATE_FOOTER_CONFIG_AMBIGUOUS = "error.template.footer.config.ambiguous";
    static final String TEMPLATE_NAME_REQUIRED = "error.template.name.required";
    static final String TEMPLATE_REQUIRED = "error.template.required";
    static final String TEMPLATE_PRODUCT = "error.template.product";
    static final String TERMINATION_POSSIBILITY_MISSING = "error.termination.possibility.missing";
    static final String TEXT_MISSING = "error.text.missing";
    static final String TICKET_SYSTEM_MISSING = "error.ticket.system.missing";
    static final String TIME_ALREADY_BOOKED = "error.time.already.booked";
    static final String TIMEOUT_SESSION = "error.timeout.session";
    static final String TITLE_IS_TOO_LONG = "error.title.is.too.long";
    static final String TITLE_MISSING = "error.title.missing";
    static final String TYPE_INVALID = "error.type.invalid";
    static final String TYPE_MISSING = "error.type.missing";
    static final String TYPE_REQUIRED = "error.type.required";
    static final String UNKNOWN = "error.unknown";
    static final String UNCANCELLED_CREDIT_NOTE_FOUND = "error.uncancelled.credit.note.found";
    static final String UNRELEASED_RECORD = "error.unreleased.record";
    static final String UPLOAD_FAILED = "error.upload.failed";
    static final String USER_ALREADY_EXISTS = "error.user.already.exists";
    static final String USER_LOGINNAME_EXISTS = "error.user.loginname.exists";
    static final String USER_LOGINNAME_MISSING = "error.user.loginname.missing";
    static final String USER_PASSWORD_MISSING = "error.user.password.missing";
    static final String USER_PASSWORD_INVALID = "error.user.password.invalid";
    static final String USER_PASSWORD_UNCHANGED = "error.user.password.unchanged";
    static final String USER_INACTIVE = "error.user.inactive";
    static final String USER_INVALID = "error.user.invalid";
    static final String USERID_INVALID = "error.userid.invalid";
    static final String USERID_MISSING = "error.userid.missing";
    static final String VALUES_INVALID = "error.values.invalid";
    static final String VALUES_DUPLICATE = "error.values.duplicate";
    static final String VALUES_MISSING = "error.values.missing";
    static final String YEAR_UNDEFINED = "error.year.undefined";
    static final String YEAR_SELECTION_MISSING = "error.year.selection.missing";
    static final String ZIPCODE_INVALID = "error.zipcode.invalid";
    static final String ZIPCODE_LENGTH = "error.zipcode.length";
    static final String ZIPCODE_MISSING = "error.zipcode.missing";
    static final String ZIPCODE_SEARCH = "error.zipcode.search";
    static final String ZIPCODE_SEARCH_MAXIMUM_SEARCHES = "error.zipcode.maximumSearches";
    
    /**
     * The error code of this error
     * @return errorCode
     */
    String getErrorCode();

    /**
     * Provides a subject if the error message should be sent via email
     * @return subject for an email error message
     */
    String getSubject();

    /**
     * Provides a human readable error string
     * @return error message or the error code if there is no message for specified error code available
     */
    String getMessage();

    /**
     * Provides the destination address where we send our error to
     * @return destination
     */
    String getDestination();
}
