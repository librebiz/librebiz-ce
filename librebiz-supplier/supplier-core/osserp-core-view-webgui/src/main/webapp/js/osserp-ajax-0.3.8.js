/**
 * osserp-core - osserp-ajax-0.3.8.js - Ajax functions.  
 * Copyright 2001-2021 The original author or authors.
 * Licensed under the Apache License, Version 2.0.
 * 
 * ojsAjax - A collection of AJAX related functions.
 */

var ojsAjax = {};

/**
 * Creates a hash map for ajax request objects. Everytime ajaxRequest() is called, the generated
 * request is inserted into this map (with the url as the key). When a request is finished, it is
 * removed from the map.
 * If ajaxRequest() wants to create an ajax object with a url that already exists in the map, then
 * the "old" request gets aborted and "kicked out" of the map.
 */
try {
	ojsAjax.ajaxRequestObjects = new Hash();
	ojsCommon.consoleDebug("[ojsAjax] <init> done");
} catch(e) {
	ojsCommon.consoleError("[ojsAjax] <init> could not create Hash Object");
}
ojsAjax.counter = 0;

//holds url, params & time of the last initiated request
ojsAjax.lastRequest = {}

/**
 * Calls a given url via Ajax and executes the response
 *
 * @param url: The url to be called.
 */
ojsAjax.loadAndExecute = function(url) {
	new Ajax.Request(url, {
		onSuccess: function(transport) {
			eval(transport.responseText);
		}
	});
}

/**
 * Calls a given url via Ajax and inserts the response into a given HTML element called "targetElement"
 * (unless targetElement is "empty" or "alert").
 * Shows an activity animation (throbber) while the request is in progress (with an optional tooltip).
 *
 * @param url: The url to be called.
 * @param params: The request parameters (as a query string or as any Hash-compatible object).
 * @param method: Should the httpRequest be made via GET or POST?
 * @param targetElement: The ID of the HTML element that the response is rendered into. (special values are "empty" and "alert")
 * @param targetIsPopup: Is the target element a "popup"?
 * @param activityElement: The ID of the HTML element for the activity animation.
 * @param activityMessage: The tooltip message for the activity animation.
 * @param toggleTarget: If true, toggle visibility of targetElement; if false or not set, set visibility to 'visible'.
 * @param successFunction: A function that is inserted into the onSuccess funtion
 * @param sourceElement
 */
ojsAjax.ajaxRequest = function(url, params, method, targetElement, targetIsPopup, activityElement, activityMessage, toggleTarget, successFunction, sourceElement) {
	try {
		var id;
		if (ojsAjax.lastRequest.url != url || ojsAjax.lastRequest.params != params || (new Date().getTime() - ojsAjax.lastRequest.time.getTime() >= 500)) {
			var ajaxObj = new Ajax.Request(url, {
				method: method,
				parameters: params,
				contentType: 'application/x-www-form-urlencoded',
				encoding: 'UTF-8',
				evalJS: true,
				evalJSON: true,
				onCreate: function() {
					//Triggered when the Ajax.Request object is initialized. This is after the parameters
					//and the URL have been processed, but before first using the methods of the XHR object.
					id = ojsAjax.counter;
					ojsAjax.counter++;
					ojsAjax.lastRequest.url = url;
					ojsAjax.lastRequest.params = params;
					ojsAjax.lastRequest.time = new Date();
					ojsAjax.showAjaxThrobber(activityElement, activityMessage);
					ojsCommon.consoleDebug("[ojsAjax] ajaxRequest() request " + id + " INVOKED");
					if (ojsAjax.ajaxRequestObjects.get(url) != null) {
						ojsAjax.ajaxRequestObjects.get(url).transport.abort();
						ojsAjax.ajaxRequestObjects.set(url, null);
					}
				},
				onUninitialized: function() {
				//(Not guaranteed) Invoked when the XHR object was just created.
				// just a dummy for potential future use. DON'T DELETE
				},
				onLoading: function() {
					//(Not guaranteed) Triggered when the underlying XHR object is being setup, and its connection opened.
					ajaxObj.id = id;
					if (ojsAjax.ajaxRequestObjects.get(url) != null) {
						ojsAjax.ajaxRequestObjects.get(url).transport.abort();
						ojsAjax.ajaxRequestObjects.set(url, null);
					}
					ojsAjax.ajaxRequestObjects.set(url, ajaxObj);
				},
				onLoaded: function() {
				//(Not guaranteed) Triggered once the underlying XHR object is setup, the connection open,
				//and ready to send its actual request.
        
				// just a dummy for potential future use. DON'T DELETE
				},
				onSuccess: function(transport) {
					//Invoked when a request completes and its status code is undefined or belongs in the 2xy family.
					//This is skipped if a code-specific callback is defined, and happens before onComplete.
					if (targetElement.endsWith("_popup")) {
						targetIsPopup = true;
					}
					if (transport.status == 0) {
						ojsCommon.consoleDebug("[ojsAjax] ajaxRequest() request " + ajaxObj.id + " ABORTED");
					} else {
						ojsCommon.consoleDebug("[ojsAjax] ajaxRequest() request " + ajaxObj.id + " SUCCESS, status: " + transport.status + " (" + transport.statusText + ")");
					}
					if (transport.responseText.toString().startsWith("redirect:")) {
						location.href = transport.responseText.toString().substring(9);
					} else {
						if (targetElement == 'empty') {
							ojsCommon.consoleDebug("[ojsAjax] ajaxRequest() request " + ajaxObj.id + " not displaying any response");
						} else {
							var responseIsCode = false;
							try {
								eval(transport.responseText);
								var test = transport.responseText.toString();
								if (!test.match("<") && !test.match(">")) {
									responseIsCode = true;
								}
							} catch (e) {
								ojsCommon.consoleDebug(
									"[ojsAjax] ajaxRequest() request " + ajaxObj.id + " caught exception while trying to execute response code: " + e
									+ " (ignore this if the responseText is HTML code)"
									);
							}
							if (!responseIsCode) {
								ojsAjax.displayResponse(transport, targetElement, targetIsPopup, toggleTarget, sourceElement);
							}
						}
						successFunction(transport, targetElement);
						initInfoBox();
					}
				},
				onFailure: function(transport) {
					//Invoked when a request completes and its status code exists but is not in the 2xy family.
					//This is skipped if a code-specific callback is defined, and happens before onComplete.
					if (ojsCommon.debugMode) {
						ojsCommon.consoleError("[ojsAjax] ajaxRequest() request " + ajaxObj.id + " failed");
						ojsAjax.displayResponse(transport, "errorPageDiv", true, false);
					} else {
						alert(ojsI18n.function_not_available);
					}
				},
				onException: function(transport) {
				//Triggered whenever an XHR error arises. Has a custom signature:
				//the first argument is the requester (i.e. an Ajax.Request instance), the second is the exception object.
				
				// just a dummy for potential future use. DON'T DELETE
				},
				onComplete: function(transport) {
					//Triggered at the very end of a request's life-cycle, once the request completed,
					//status-specific callbacks were called, and possible automatic behaviors were processed.
					ojsAjax.hideAjaxThrobber(activityElement);
					ojsAjax.ajaxRequestObjects.set(url, null);
					ojsCommon.consoleDebug("[ojsAjax] ajaxRequest() request " + ajaxObj.id + " DONE");
				}
			});
		}
	} catch(e) {
		alert(ojsI18n.function_not_available);
		ojsCommon.consoleError("[ojsAjax] ajaxRequest(): " + e + "\n is the prototype version outdated?");
	}
}

/**
 * displays the response of an ajax request
 * 
 * @param transport: transport stream of the ajax request
 * @param targetElement
 * @param targetIsPopup
 * @param toggleTarget
 * @param sourceElement
 */
ojsAjax.displayResponse = function(transport, targetElement, targetIsPopup, toggleTarget, sourceElement) {
	try {
		if ($(sourceElement).descendantOf($(targetElement))) {
			toggleTarget = false;
		}
	} catch(e) {
		
	}
	if (targetElement == 'alert') {
		alert(transport.responseText);
	} else {
		if (targetIsPopup) {
			ojsAjax.createPopupDiv(targetElement);
		}
		$(targetElement).innerHTML = transport.responseText;
		if (targetIsPopup) {
			if (toggleTarget) {
				ojsCommon.toggleVisibility(targetElement);
				ojsCommon.toggleDisplay(targetElement);
			} else {
				ojsCommon.setVisibility(targetElement, "visible");
				ojsCommon.setDisplay(targetElement, "block");
			}
			ojsAjax.addCloseButton(targetElement);
		} else {
			ojsCommon.setVisibility(targetElement, "visible");
			if ($(targetElement).tagName != 'TR' && $(targetElement).tagName != 'TD') {
				ojsCommon.setDisplay(targetElement, "block");
			}
		}
	}
}

/**
 * shows or hides an ajax throbber (loading animation)
 * @param activityElement: The ID of the HTML element for the activity animation.
 * @param activityMessage: The tooltip message for the activity animation.
 */
ojsAjax.showAjaxThrobber = function(activityElement, activityMessage) {
	if (activityElement != "" && activityElement != "null" && activityElement != null) {
		if (activityMessage == null || activityMessage == "") {
			activityMessage = ojsI18n.loading;
		}
		try {
			if ($(activityElement).match('input') && $(activityElement).type == "text") {
				$(activityElement).style.background = "url(/" + document.URL.split('/')[3] + "/img/ajaxload_input.gif) no-repeat scroll right top";
			} else {
				$(activityElement).innerHTML = "<div id='ajaxThrobber'><img class='icon' src='/" + document.URL.split('/')[3] + "/img/ajaxload_input.gif' title='" + activityMessage + "' /></div>";
			}
		} catch(e) {
			ojsCommon.consoleWarn("[ojsAjax] showAjaxThrobber() caught error: activityElement '" + activityElement + "' does not exist");
		}
	}
}

/**
 * hides the ajax throbber (loading animation)
 * @param activityElement: The ID of the HTML element for the activity animation.
 */
ojsAjax.hideAjaxThrobber = function(activityElement) {
	if (activityElement != "" && activityElement != "null" && activityElement != null) {
		try {
			if ($(activityElement).match('input') && $(activityElement).type == "text") {
				$(activityElement).style.background = "";
			} else {
				$('ajaxThrobber').remove();
			}
		} catch(e) {
			ojsCommon.consoleWarn("[ojsAjax] hideAjaxThrobber() caught error: activityElement '" + activityElement + "' does not exist");
		}
	}
}

/**
 * creates a "popup" div with the desired id
 * @param id
 */
ojsAjax.createPopupDiv = function(id) {
	var leftBefore = '0px';
	var topBefore = '0px';
	var visibilityBefore = 'hidden';
	var displayBefore = 'none';
	if ($(id)) {
		ojsCommon.consoleDebug("[ojsAjax] createPopupDiv() div with id '" + id + "' already exists, destroying it");
		visibilityBefore = $(id).style.visibility;
		displayBefore = $(id).style.display;
		leftBefore = $(id).style.left;
		topBefore = $(id).style.top;
		$(id).remove();
	}
	var div = document.createElement('div');
	div.id = id;
	div.className = 'modalBox';
	if (visibilityBefore == 'hidden' || displayBefore == 'none') {
		div.style.visibility = 'hidden';
		div.style.display = 'none';
	} else {
		div.style.visibility = visibilityBefore;
		div.style.display = displayBefore;
		div.style.left = leftBefore;
		div.style.top = topBefore;
	}
	var popupArea = document.getElementById('popup-area');
	popupArea.appendChild(div);
}

ojsAjax.alignPopup = function(id, align, left, top) {
	var width = $(id).clientWidth;
	var height = $(id).clientHeight;

	left -= align.startsWith('left') ? (width+10) : 0;
	top -= align.endsWith('up') ? (height+10): 0;

	var hOverhead = (left+width) - window.innerWidth;
	var vOverhead = (top+height+12) - window.innerHeight;

	left -= hOverhead > 0 ? hOverhead : 0;
	top -= vOverhead > 0 ? vOverhead : 0;

	if (ojsCommon.isNumeric(left)) {
		$(id).style.left = left + 'px';
	}
	if (ojsCommon.isNumeric(top)) {
		$(id).style.top = top + 'px';
	}
}

/**
 * adds a "close button" to any given HTML element
 * @param targetElement The ID of the HTML element.
 */
ojsAjax.addCloseButton = function(targetElement) {
	var closeButton = document.createElement('div');
	closeButton.id = targetElement + "_close";
	closeButton.innerHTML = "X";
	closeButton.onclick = function() {
		ojsCommon.toggleVisibility(targetElement);
		ojsCommon.toggleDisplay(targetElement);
	}
	closeButton.className = "modalBoxCloseButton";
	$(targetElement).appendChild(closeButton);
}

/**
 * is used by the <o:ajaxLookupForm> and <o:ajaxFormPopup> tag
 * uses ajaxRequest() to submit an HTML form (via POST)
 * the response is insertet into a "popup"
 * @param requestUrl: The url to be called.
 * @param targetElement: The ID of the HTML element that the response is rendered into.
 * @param targetIsPopup: Is the target element a "popup"?
 * @param form: The form to be submitted.
 * @param activityElement: The ID of the HTML element for the activity animation.
 * @param activityMessage: The tooltip message for the activity animation.
 * @param minChars: The minimal amount of characters the user has to type in.
 * @param successFunction
 */
ojsAjax.ajaxForm = function(requestUrl, targetElement, targetIsPopup, form, activityElement, activityMessage, minChars, successFunction) {
	var send = false;
	var noTextInputs = true;

	for (var i=0; i<form.length; i++) {
		if (form[i].type == "text") {
			noTextInputs = false;
			if (form[i].value.length >= minChars) {
				send = true;
			}
		}
	}
	
	var params = ojsForms.formToHash(form);
	if (send || noTextInputs) {
		ojsAjax.ajaxRequest(requestUrl, params, "POST", targetElement, targetIsPopup, activityElement, activityMessage, false, successFunction, form);
	} else {
		$(targetElement).innerHTML = "<div style='padding: 10px; border: 1px solid #D6DDE6; margin: 10px;'>" + ojsI18n.min_character_count + minChars + "</div>";
	}
}

/**
 * is used by the <v:ajaxSearchForm> tag
 * uses ajaxRequest() to submit an HTML form (via POST)
 * the response is insertet into a "popup"
 * @param requestUrl: The url to be called.
 * @param targetElement: The ID of the HTML element that the response is rendered into.
 * @param form: The form to be submitted.
 * @param activityElement: The ID of the HTML element for the activity animation.
 * @param activityMessage: The tooltip message for the activity animation.
 * @param minChars: The minimal amount of characters the user has to type in.
 * @param successFunction
 */
ojsAjax.ajaxSearchForm = function(requestUrl, targetElement, form, activityElement, activityMessage, minChars, successFunction) {
	var send = false;
	var noTextInputs = true;

	for (var i=0; i<form.length; i++) {
		if (form[i].type == "text") {
			noTextInputs = false;
			if (form[i].value.length >= minChars) {
				send = true;
			}
		}
	}

	var params = ojsForms.formToHash(form);
	if (send || noTextInputs) {
		$(targetElement).innerHTML = "<div class='ajaxSearchFormMessage'><img src='/" + document.URL.split('/')[3] + "/img/ajax_loader.gif' title='" + activityMessage + "' /><br/>"+activityMessage+"</div>";
		ojsAjax.ajaxRequest(requestUrl, params, "POST", targetElement, false, activityElement, activityMessage, false, successFunction, form);
	} else {
		$(targetElement).innerHTML = "<div class='ajaxSearchFormMessage'>" + ojsI18n.min_character_count + minChars + "</div>";
	}
}

/**
 * is used by the <o:ajaxLink> tag
 * uses ajaxRequest() to call a URL from a link
 *
 * @param requestUrl: The url to be called.
 * @param targetElement: The ID of the HTML element that the response is rendered into.
 * (special values are "empty" and "alert")
 * @param targetIsPopup: Is the target element a "popup"?
 * @param activityElement: The ID of the HTML element for the activity animation.
 * @param activityMessage: The tooltip message for the activity animation.
 * @param successFunction
 * @param link The link element itself.
 */
ojsAjax.ajaxLink = function(requestUrl, targetElement, targetIsPopup, activityElement, activityMessage, successFunction, link) {
	ojsAjax.ajaxRequest(requestUrl, "", "GET", targetElement, targetIsPopup, activityElement, activityMessage, true, successFunction, link);
}

/**
 * is used by the <o:ajaxSelect> tag
 * uses ajaxRequest() 
 *
 * @param targetElement: The ID of the HTML element that the response is rendered into.
 * (special values are "empty" and "alert")
 * @param targetIsPopup: Is the target element a "popup"?
 * @param activityElement: The ID of the HTML element for the activity animation.
 * @param activityMessage: The tooltip message for the activity animation.
 * @param successFunction
 * @param select: The select element itself.
 */
ojsAjax.ajaxSelect = function(targetElement, targetIsPopup, activityElement, activityMessage, successFunction, select) {
	var requestUrl = select.value;
	if (requestUrl != "" && requestUrl != null && requestUrl.length > 0) {
		ojsAjax.ajaxRequest(requestUrl, "", "GET", targetElement, targetIsPopup, activityElement, activityMessage, false, successFunction, select);
	}
}

/*
 * Checks if execution of a link "makes sense" by calling a certain url via Ajax.
 */
ojsAjax.checkLink = function(checkUrl, linkUrl) {
	var onSuccess = function(transport) {
		if(transport.responseText != "") {
			ojsAjax.displayResponse(transport, "link_popup", true, false);
		} else {
			ojsCommon.gotoUrl(linkUrl);
		}
	}
	
	ojsAjax.ajaxRequest(checkUrl, "", "GET", "empty", false, "", "", false, onSuccess);
}

/**
 * makes every <div> element disappear whose id ends with "_popup"
 */
ojsAjax.closeAllPopups = function() {
	try {
		var divs = document.getElementsByTagName("div");
		for (var i=0; i<divs.length; i++){
			if (divs[i].id.endsWith("_popup")) {
				divs[i].style.visibility = "hidden";
			}
		}
	} catch(e) {
		ojsCommon.consoleWarn("[ojsAjax] closeAllPopups() something went wrong while trying to close popups, continuing anyway");
	}
}

/**
 * is used by the <o:ajaxInfobox> tag
 * uses ajaxRequest(), and shows an infoBox with the response
 *
 * @param event: e.g. onmousemove
 * @param elementId: The ID of the HTML element that the response is rendered into.
 * @param url: the url for ajaxRequest
 */
ojsAjax.showInfoBox = function(event, elementId, url) {
	
	var infoBoxBorder = $(elementId+'_border');
	var infoBox = $(elementId);
	if (infoBoxBorder && infoBox && url != null) {

		//mouse position
		var x = event.pageX || window.event.x;
		var y = event.pageY || window.event.y;

		if ((infoBox.getDimensions().width+x) <= document.viewport.getWidth()) {
			infoBox.style.left = x + "px";
		} else {
			infoBox.style.left = (document.viewport.getWidth()-infoBox.getDimensions().width) + "px";
		}

		if ((infoBox.getDimensions().height+y) <= document.viewport.getHeight()) {
			infoBox.style.top = y + "px";
		} else {
			infoBox.style.top = (document.viewport.getHeight()-infoBox.getDimensions().height) + "px";
		}

		try {
			ojsAjax.ajaxRequest(url, "", "GET", elementId, false, elementId, "", false);
		} catch(e) {
			ojsCommon.consoleWarn("[ojsAjax] showInfoBox(elementId, url) something went wrong while trying to do the ajaxRequest");
		}
		
		infoBox.style.zIndex = "1010";
		infoBox.style.position = "absolute";
		infoBox.style.visibility = "visible";
		infoBoxBorder.style.display = "";
		infoBox.style.borderLeft = "2px solid #D6DDE6";
		infoBox.style.borderTop = "2px solid #D6DDE6";
		infoBox.style.borderRight = "2px solid black";
		infoBox.style.borderBottom = "2px solid black";
	}
}

/**
 * is used by the <o:ajaxInfobox> tag
 *
 * @param elementId: The ID of the HTML element.
 */
ojsAjax.hideInfoBox = function(elementId) {
	var box = $(elementId+'_border');
	if (box) {
		box.style.display = "none";
	}
}

ojsAjax.isMultipart = function(form) {
	for (var i=0; i<form.length; i++) {
		if (form[i].type == "file") {
			return true;
		}
	}
	return false;
}
