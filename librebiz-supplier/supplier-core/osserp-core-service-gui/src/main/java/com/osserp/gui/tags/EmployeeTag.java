/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 24, 2006 12:53:48 PM 
 * 
 */
package com.osserp.gui.tags;

import javax.servlet.jsp.JspException;

import com.osserp.common.Constants;
import com.osserp.common.web.tags.AbstractOutTag;

import com.osserp.core.CoreServiceProvider;
import com.osserp.core.Options;
import com.osserp.core.employees.Employee;
import com.osserp.core.employees.EmployeeSearch;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class EmployeeTag extends AbstractOutTag {
    private boolean initials = false;

    public boolean getInitials() {
        return initials;
    }

    public void setInitials(boolean initials) {
        this.initials = initials;
    }

    @Override
    public String getOutput() throws JspException {
        Long emp = null;
        try {
            emp = (Long) value;
            if (emp == null || emp == 0) {
                return "";
            }
            if (Constants.SYSTEM_USER.equals(emp)
                    || Constants.SYSTEM_EMPLOYEE.equals(emp)) {
                return initials ? "SYS" : "System";
            }
            String result = null;
            if (initials) {
                result = CoreServiceProvider.getOptionValue(getRequest(), Options.EMPLOYEE_KEYS, emp);
                return (result == null ? "" : result);
            }
            result = CoreServiceProvider.getOptionValue(getRequest(), Options.EMPLOYEES, emp);
            if (result == null) {
                EmployeeSearch search = (EmployeeSearch) CoreServiceProvider.getService(
                        getRequest(), EmployeeSearch.class.getName());
                Employee employee = search.findById(emp);
                if (employee != null) {
                    result = employee.getDisplayName();
                }
            }
            return (result == null ? "" : cut(result));
        } catch (Throwable t) {
            return "";
        }
    }

}
