<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<v:view viewName="campaignSelectionView"/>
<div class="table-responsive table-responsive-default">
<table class="table table-striped" id="campaigns">
	<thead>
		<tr>
			<th class="start"><fmt:message key="start"/></th>
			<th class="name"><fmt:message key="mainCampaign"/></th>
			<th class="name"><fmt:message key="subCampaign"/></th>
			<th class="type">
				<c:choose>
					<c:when test="${view.typeSelectionMode}"><v:ajaxLink url="/crm/campaignSelection/disableTypeSelectionMode" targetElement="${view.name}_popup"><fmt:message key="typeSelection"/></v:ajaxLink></c:when>
					<c:otherwise>
						<c:choose>
							<c:when test="${view.groupSelectionMode or view.branchSelectionMode}"><fmt:message key="type"/></c:when>
							<c:otherwise><v:ajaxLink url="/crm/campaignSelection/enableTypeSelectionMode" targetElement="${view.name}_popup"><fmt:message key="type"/></v:ajaxLink></c:otherwise>
						</c:choose>
					</c:otherwise>
				</c:choose>
			</th>
			<th class="group">
				<c:choose>
					<c:when test="${view.groupSelectionMode}"><v:ajaxLink url="/crm/campaignSelection/disableGroupSelectionMode" targetElement="${view.name}_popup"><fmt:message key="groupSelection"/></v:ajaxLink></c:when>
					<c:otherwise>
						<c:choose>
							<c:when test="${view.typeSelectionMode or view.branchSelectionMode}"><fmt:message key="group"/></c:when>
							<c:otherwise><v:ajaxLink url="/crm/campaignSelection/enableGroupSelectionMode" targetElement="${view.name}_popup"><fmt:message key="group"/></v:ajaxLink></c:otherwise>
						</c:choose>
					</c:otherwise>
				</c:choose>
			</th>
			<th class="company">
				<c:choose>
					<c:when test="${view.branchSelectionMode}"><v:ajaxLink url="/crm/campaignSelection/disableBranchSelectionMode" targetElement="${view.name}_popup"><fmt:message key="branch"/></v:ajaxLink></c:when>
					<c:otherwise>
						<c:choose>
							<c:when test="${view.typeSelectionMode or view.groupSelectionMode}"><fmt:message key="company"/></c:when>
							<c:otherwise><v:ajaxLink url="/crm/campaignSelection/enableBranchSelectionMode" targetElement="${view.name}_popup"><fmt:message key="company"/></v:ajaxLink></c:otherwise>
						</c:choose>
					</c:otherwise>
				</c:choose>
			</th>
		</tr>
	</thead>
	<tbody>
		<c:choose>
			<c:when test="${view.typeSelectionMode}">
				<tr class="deselected">
					<td class="start"> </td>
					<td class="name"> </td>
					<td class="name"> </td>
					<td class="type activated"><v:ajaxLink url="/crm/campaignSelection/selectType" targetElement="${view.name}_popup"><fmt:message key="all"/></v:ajaxLink></td>
					<td class="group"> </td>
					<td class="company"> </td>
				</tr>
				<c:forEach var="item" items="${view.types}">
					<tr class="deselected">
						<td class="start"> </td>
						<td class="name"> </td>
						<td class="name"> </td>
						<td class="type activated"><v:ajaxLink url="/crm/campaignSelection/selectType?id=${item.id}" targetElement="${view.name}_popup"><o:out value="${item.name}"/></v:ajaxLink></td>
						<td class="group"> </td>
						<td class="company"> </td>
					</tr>
				</c:forEach>
				<tr class="deselected">
					<td class="start"> </td>
					<td class="name"> </td>
					<td class="name"> </td>
					<td class="type">&nbsp;</td>
					<td class="group"> </td>
					<td class="company"> </td>
				</tr>
			</c:when>
			<c:when test="${view.groupSelectionMode}">
				<tr class="deselected">
					<td class="start"> </td>
					<td class="name"> </td>
					<td class="name"> </td>
					<td class="type"> </td>
					<td class="group activated"><v:ajaxLink url="/crm/campaignSelection/selectGroup" targetElement="${view.name}_popup"><fmt:message key="all"/></v:ajaxLink></td>
					<td class="company"> </td>
				</tr>
				<c:forEach var="item" items="${view.groups}">
					<tr class="deselected">
						<td class="start"> </td>
						<td class="name"> </td>
						<td class="name"> </td>
						<td class="type"> </td>
						<td class="group activated"><v:ajaxLink url="/crm/campaignSelection/selectGroup?id=${item.id}" targetElement="${view.name}_popup"><o:out value="${item.name}"/></v:ajaxLink></td>
						<td class="company"> </td>
					</tr>
				</c:forEach>
				<tr class="deselected">
					<td class="start"> </td>
					<td class="name"> </td>
					<td class="name"> </td>
					<td class="type"> </td>
					<td class="group">&nbsp;</td>
					<td class="company"> </td>
				</tr>
			</c:when>
			<c:when test="${view.branchSelectionMode}">
				<tr class="deselected">
					<td class="start"> </td>
					<td class="name"> </td>
					<td class="name"> </td>
					<td class="type"> </td>
					<td class="group"> </td>
					<td class="company activated"><v:ajaxLink url="/crm/campaignSelection/selectBranch" targetElement="${view.name}_popup"><fmt:message key="all"/></v:ajaxLink></td>
				</tr>
				<c:forEach var="item" items="${view.branchOfficeList}">
					<tr class="deselected">
						<td class="start"> </td>
						<td class="name"> </td>
						<td class="name"> </td>
						<td class="type"> </td>
						<td class="group"> </td>
						<td class="company activated"><v:ajaxLink url="/crm/campaignSelection/selectBranch?id=${item.id}" targetElement="${view.name}_popup"><o:out value="${item.company.name}"/> - <o:out value="${item.name}"/></v:ajaxLink></td>
					</tr>
				</c:forEach>
				<tr class="deselected">
					<td class="start"> </td>
					<td class="name"> </td>
					<td class="name"> </td>
					<td class="type"> </td>
					<td class="group"> </td>
					<td class="company">&nbsp;</td>
				</tr>
			</c:when>
			<c:otherwise>
				<c:forEach var="item" items="${view.campaigns}">
					<c:if test="${view.displayEol or !item.endOfLife}">
						<c:choose><c:when test="${item.endOfLife}"><c:set var="styleClass" value="eol"/></c:when><c:otherwise><c:set var="styleClass" value="activated"/></c:otherwise></c:choose>
						<tr class="${styleClass}">
							<td class="start"><o:date value="${item.campaignStarts}"/></td>
							<td class="name"><c:choose><c:when test="${item.parent}"><o:out value="${item.name}"/></c:when><c:otherwise><oc:options name="campaigns" value="${item.reference}"/></c:otherwise></c:choose></td>
							<td class="name"><v:ajaxLink url="/crm/campaignSelection/select?id=${item.id}" targetElement="${view.name}_popup"><o:out value="${item.name}"/></v:ajaxLink></td>
							<td class="type"><o:out value="${item.type.name}"/></td>
							<td class="group"><o:out value="${item.group.name}"/></td>
							<td class="company"><oc:options name="systemCompanyKeys" value="${item.company}"/></td>
						</tr>
					</c:if>
				</c:forEach>
			</c:otherwise>
		</c:choose>
	</tbody>
</table>
</div>
