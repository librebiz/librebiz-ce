/**
 *
 * Copyright (C) 2018 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 28, 2018 
 * 
 */
package com.osserp.core.service.records;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.velocity.app.VelocityEngine;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.dms.DmsDocument;
import com.osserp.common.dms.DmsManager;
import com.osserp.common.dms.DmsReference;
import com.osserp.common.dms.DocumentData;
import com.osserp.common.mail.Attachment;
import com.osserp.common.mail.AttachmentImpl;
import com.osserp.common.mail.EmailAddress;
import com.osserp.common.mail.Mail;
import com.osserp.common.service.ResourceLocator;
import com.osserp.common.service.impl.AbstractTemplateAwareService;
import com.osserp.common.util.DateFormatter;
import com.osserp.common.util.StringUtil;

import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.customers.Customer;
import com.osserp.core.dao.Contacts;
import com.osserp.core.dao.records.RecordMailLogs;
import com.osserp.core.dao.records.RecordsArchive;
import com.osserp.core.dao.records.RecordVersionsArchive;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordDocument;
import com.osserp.core.finance.RecordMailManager;
import com.osserp.core.finance.RecordVersionDocument;
import com.osserp.core.mail.MailMessageContext;
import com.osserp.core.mail.MailTemplate;
import com.osserp.core.mail.MailTemplateManager;
import com.osserp.core.products.Product;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RecordMailManagerImpl extends AbstractTemplateAwareService implements RecordMailManager {
    private static Logger log = LoggerFactory.getLogger(RecordMailManagerImpl.class.getName());

    private MailTemplateManager mailTemplateManager;
    private RecordMailLogs mailLogs;
    private RecordsArchive recordsArchive;
    private RecordVersionsArchive recordVersionsArchive;
    private ResourceLocator resourceLocator;
    private Contacts contacts;
    private DmsManager dmsManager;

    protected RecordMailManagerImpl(
            VelocityEngine engine,
            MailTemplateManager mailTemplateManager,
            RecordMailLogs mailLogs,
            RecordsArchive recordsArchive,
            RecordVersionsArchive recordVersionsArchive,
            Contacts contacts,
            ResourceLocator resourceLocator,
            DmsManager dmsManager) {
        super(engine);
        this.mailTemplateManager = mailTemplateManager;
        this.mailLogs = mailLogs;
        this.recordsArchive = recordsArchive;
        this.recordVersionsArchive = recordVersionsArchive;
        this.contacts = contacts;
        this.dmsManager = dmsManager;
        this.resourceLocator = resourceLocator;
    }

    public MailMessageContext createContext(Employee user, Record record, Map<String, String> opts) throws ClientException {
        assert record != null && record.getContact() != null;
        if (!record.isUnchangeable()) {
            throw new ClientException(ErrorCode.RECORD_CHANGEABLE);
        }
        DmsDocument document = getDocument(record);
        return this.createMessageContext(user, record, opts, document, null);
    }

    public MailMessageContext createArchiveContext(Employee user, Record record, RecordDocument document, Map<String, String> opts) throws ClientException {
        assert document != null;
        String customSubject = document.getHeaderName() != null ? document.getHeaderName() : null;
        return createMessageContext(user, record, opts, document, customSubject);
    }

    private MailMessageContext createMessageContext(
            Employee user,
            Record record,
            Map<String, String> opts,
            DmsDocument document,
            String customSubject) throws ClientException {
        assert record != null && record.getContact() != null;
        EmailAddress billingAddress = contacts.findBillingAddress(record.getContact());
        String recipient = billingAddress != null ? billingAddress.getEmail() : record.getContact().getEmail();
        if (record.getContact() instanceof Customer
                && ((Customer) record.getContact()).getInvoiceEmail() != null) {
            recipient = ((Customer) record.getContact()).getInvoiceEmail();
        }
        if (recipient == null) {
            throw new ClientException(ErrorCode.RECIPIENT_MISSING);
        }
        String templateName = (record.getType().getResourceKey() == null ? "record" : record.getType().getResourceKey()) + "EmailTemplate";
        MailTemplate mailTemplate = getTemplate(user, templateName);
        Mail mail = new Mail(
                mailTemplate.getOriginator(),
                new String[] { recipient },
                null,
                customSubject != null ? customSubject : mailTemplate.getSubject(),
                createText(templateName, createValueMap(user, record, mailTemplate)),
                createAttachment(document, record.getNumber()));
        if (isSet(mailTemplate.getRecipientsBCC())) {
            String[] recipients = StringUtil.getTokenArray(mailTemplate.getRecipientsBCC());
            for (int i = 0; i < recipients.length; i++) {
                mail.addRecipientBCC(recipients[i]);
            }
        }
        if (record.getType().isAddProductDatasheet()) {
            for (int i = 0; i < record.getItems().size(); i++) {
                Product product = record.getItems().get(i).getProduct();
                Attachment datasheet = fetchDatasheet(product);
                if (datasheet != null) {
                    mail.addAttachment(datasheet);
                }
            }
        }
        opts.put("viewHeader", record.getType().getName() + " - " + record.getNumber());
        return new MailMessageContext(mailTemplate, mail, opts);
    }

    private MailTemplate getTemplate(Employee user, String templateName) throws ClientException {
        MailTemplate mailTemplate = mailTemplateManager.findTemplate(templateName);
        if (mailTemplate == null) {
            if (log.isInfoEnabled()) {
                log.info("getTemplate: mail template missing [name " + templateName + "]");
            }
            throw new ClientException(ErrorCode.MAIL_TEMPLATE_MISSING);
        }
        if (isNotSet(mailTemplate.getSubject())) {
            throw new ClientException(ErrorCode.MAIL_TEMPLATE_MISSING_SUBJECT);
        }
        if (isNotSet(mailTemplate.getOriginator())) {
            if (!mailTemplate.isSendAsUser()) {
                throw new ClientException(ErrorCode.MAIL_TEMPLATE_MISSING_ORIGINATOR);
            }
            if (user == null || isNotSet(user.getEmail())) {
                throw new ClientException(ErrorCode.ORIGINATOR_MISSING);
            }
            mailTemplate.setOriginator(user.getEmail());
        }
        return mailTemplate;
    }

    private Map<String, Object> createValueMap(Employee user, Record record, MailTemplate mailTemplate) {
        Map<String, Object> model = mailTemplateManager.createParameters(user, mailTemplate);
        Locale locale = isNotSet(record.getLanguage()) ? Constants.DEFAULT_LOCALE_OBJECT :
            new Locale(record.getLanguage());
        model.put("recordId", record.getNumber());
        model.put("recordDate", DateFormatter.getDate(record.getCreated()));

        ClassifiedContact recipient = record.getContact();
        model.put("contactId", recipient.getId());

        String salutation = recipient.getSalutationDisplay();
        if (isNotSet(salutation) || !mailTemplate.isPersonalizedHeader()) {
            salutation = resourceLocator.getMessage("defaultSalutation", locale);
        }
        if (isSet(salutation)) {
            model.put("salutation", salutation);
        }
        return model;
    }

    public void writeLog(
            Employee user,
            Record record,
            String originator,
            String recipients,
            String recipientsCC,
            String recipientsBCC,
            String messageID) {
        mailLogs.create(user, record, originator, recipients, recipientsCC, recipientsBCC, messageID);
    }

    private Attachment createAttachment(DmsDocument doc, String recordNumber) throws ClientException {
        if (doc == null) {
            throw new ClientException(ErrorCode.DOCUMENT_NOT_CREATED);
        }
        byte[] pdfDocument = (doc instanceof RecordVersionDocument)
                ? recordVersionsArchive.getDocumentData(doc)
                        : recordsArchive.getDocumentData(doc);
        if (pdfDocument == null) {
            throw new ClientException(ErrorCode.DOCUMENT_LOAD);
        }
        Attachment obj = new AttachmentImpl(
                recordNumber != null ? recordNumber + ".pdf" : doc.getFileName(),
                pdfDocument,
                Constants.MIME_TYPE_PDF);
        return obj;
    }

    private DmsDocument getDocument(Record record) throws ClientException {
        List<DmsDocument> list = recordsArchive.findByReference(
                new DmsReference(record, record.getType().getId()));
        if (!list.isEmpty()) {
            return list.get(0);
        }
        throw new ClientException(ErrorCode.DOCUMENT_NOT_FOUND);
    }

    private Attachment fetchDatasheet(Product product) {
        DmsDocument dsd = product.isDatasheetAvailable() ? getProductDatasheet(product) : null;
        if (dsd != null) {
            try {
                DocumentData data = dmsManager.getDocumentData(dsd);
                if (data.getBytes() != null) {
                    Attachment obj = new AttachmentImpl(
                            dsd.getFileName(),
                            data.getBytes(),
                            Constants.MIME_TYPE_PDF);
                    return obj;
                }
            } catch (Exception e) {
                log.info("fetchDatasheet: Unable to read " + dsd.getFileName());
            }
        }
        return null;
    }

    private DmsDocument getProductDatasheet(Product product) {
        DmsDocument datasheet = null;
        if (product.isDatasheetAvailable()) {
            List<DmsDocument> list = dmsManager.findByReference(
                    new DmsReference(Product.DATASHEET, product.getProductId()));
            if (!list.isEmpty()) {
                for (int i = 0; i < list.size(); i++) {
                    datasheet = list.get(i);
                    if (datasheet.isReferenceDocument()) {
                        break;
                    }
                }
            }
        }
        return datasheet;
    }
}
