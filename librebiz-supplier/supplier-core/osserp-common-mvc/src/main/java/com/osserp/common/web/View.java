/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 16, 2006 8:27:58 AM 
 * 
 */
package com.osserp.common.web;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.User;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface View extends Serializable {

    /**
     * Provides the name of the view
     * @return name
     */
    String getName();

    /**
     * Provides a copy of views local value cache
     * @return data
     */
    Map<String, Object> getData();

    /**
     * Provides an annotated target element name
     * @return targetElementName or null if not available
     */
    String getTargetElementName();

    /**
     * Initializes the view after configuration
     * @throws ClientException
     * @throws PermissionException if current user has no permission
     */
    void init(HttpServletRequest request) throws ClientException, PermissionException;

    /**
     * Sets current user of the view
     * @param user
     */
    void setCurrentUser(User user);

    /**
     * Provides an error message key after an error has occured
     * @return errorMessage
     */
    String getErrorMessage();

    /**
     * Sets an error message after an error has occured
     * @param errorMessageKey
     */
    void setErrorMessage(String errorMessageKey);

    /**
     * Indicates that the view is in edit mode
     * @return true if so
     */
    boolean isEditMode();

    /**
     * Enables/disables edit mode
     * @param editMode
     * @throws PermissionException if user has no permission to edit
     */
    void setEditMode(boolean editMode) throws PermissionException;

    /**
     * Disables edit mode
     */
    void disableEditMode();

    /**
     * Indicates that the view is in ajax mode
     * @return true if so
     */
    boolean isAjaxMode();

    /**
     * Enables/disables ajax mode
     * @param ajaxMode
     */
    void setAjaxMode(boolean ajaxMode);

    /**
     * Indicates that the view is in create mode
     * @return true if so
     */
    boolean isCreateMode();

    /**
     * Enables/disables create mode
     * @param createMode
     * @throws PermissionException if user has no permission to create
     */
    void setCreateMode(boolean createMode) throws PermissionException;

    /**
     * Indicates that the view is in read only mode which tells the presentation layer not to provide any update methods
     * @return true if read only enabled
     */
    boolean isReadOnlyMode();

    /**
     * Enables/disables read only mode
     * @param readOnlyMode
     */
    void setReadOnlyMode(boolean readOnlyMode);

    /**
     * Indicates that view is in setup mode if implementing view provides setup functionality
     * @return setupMode
     */
    boolean isSetupMode();

    /**
     * Enables/disables setup mode
     * @param setupMode
     */
    void setSetupMode(boolean setupMode);

    /**
     * Indicates that view is a selection view
     * @return selectionView
     */
    boolean isSelectionView();

    /**
     * Enables selection view mode
     */
    void enableSelectionView();

    /**
     * Provides the list if view provides list capabilities
     * @return list of entities to display
     */
    List getList();

    /**
     * Provides the starting element when view iterates over a list
     * @return listStart
     */
    int getListStart();

    /**
     * Provides the last element when view iterates over a list
     * @return listEnd
     */
    int getListEnd();

    /**
     * Provides the current selected step when view iterates page by page over a long list
     * @return listStep
     */
    int getListStep();

    /**
     * Sets the list step and initializes start and end element
     * @param listStep
     */
    void setListStep(int listStep);

    /**
     * Increases start and end element when view iterates over a list to display next page
     */
    void nextList();

    /**
     * Decreases list start and end to display last visited list elements
     */
    void lastList();

    /**
     * Indicates if current listing is first in list
     * @return true if so
     */
    boolean isFirstList();

    /**
     * Indicates if current listing is last in list
     * @return true if so
     */
    boolean isLastList();

    /**
     * Resets list counters. Listing will start with first element after method performed
     */
    void resetListCounters();

    /**
     * Indicates that view is in list selection mode
     * @return listSelectionMode
     */
    boolean isListSelectionMode();

    /**
     * Enables list selection mode
     */
    void enableListSelectionMode();

    /**
     * Disables list selection mode
     */
    void disableListSelectionMode();

    /**
     * Removes current selected bean and removes current bean from list
     */
    void cutList();

    /**
     * Sorts current list by given method.
     * @param method
     */
    void sortList(String method);

    /**
     * Indicates that bean is available
     * @return true if so
     */
    boolean isBeanAvailable();

    /**
     * Provides the current selected object to display/edit
     * @return bean
     */
    Object getBean();

    /**
     * Sets current bean from current list
     * @param id of the selected entity from current list
     */
    void setSelection(Long id);

    /**
     * Indicates if a jumpto target exists wich is by default the last requested id if a current displayed bean was fetched from a list of this view
     * @return true if jumpto target exists
     */
    boolean jumpToExists();

    /**
     * Creates a jump to url to scroll to the last requested id when returning to a list
     * @param jumpToUrl where we should append the target id (e.g. #xxx where xxx is the last selected bean id by default)
     * @return url
     */
    String createJumpTo(String jumpToUrl);

    /**
     * Sets the jumto target (last id property)
     * @param jumpTo
     */
    void setJumpTo(Long jumpTo);

    /**
     * Indicates that an action target is set
     * @return actionTargetAvailable
     */
    boolean isActionTargetAvailable();

    /**
     * Provides a target name for search and select actions
     * @return actionTarget
     */
    String getActionTarget();

    /**
     * Sets a target for select and search actions
     * @param actionTarget
     */
    void setActionTarget(String actionTarget);

    /**
     * Indicates exitId availability
     * @return true if available
     */
    boolean isExitIdAvailable();

    /**
     * Provides the exit id
     * @return exitId
     */
    String getExitId();

    /**
     * Sets the exit id
     * @param exitId
     */
    void setExitId(String exitId);

    /**
     * Indicates that an exit target is set
     * @return true if so
     */
    boolean isExitTargetAvailable();

    /**
     * Provides an exit target for the view
     * @return exitTarget
     */
    String getExitTarget();

    /**
     * Sets an exit target for the view
     * @param exitTarget
     */
    void setExitTarget(String exitTarget);

    /**
     * Indicates that a forward target is set
     * @return true if so
     */
    boolean isForwardTargetAvailable();

    /**
     * Provides the forward target of the view
     * @return forwardTarget
     */
    String getForwardTarget();

    /**
     * Sets the forward target for the view
     * @param forwardTarget
     */
    void setForwardTarget(String forwardTarget);

    /**
     * Provides a list target for the view
     * @return listTarget
     */
    String getListTarget();

    /**
     * Sets a list target for the view
     * @param listTarget
     */
    void setListTarget(String listTarget);

    /**
     * Provides the url if view provides lists where selection depends on other actions
     * @return the selectionUrl
     */
    String getSelectionUrl();

    /**
     * Sets the selection url
     * @param selectionUrl
     */
    void setSelectionUrl(String selectionUrl);

    /**
     * Provides a selection target for the view
     * @return selectionTarget
     */
    String getSelectionTarget();

    /**
     * Sets a selection target for the view
     * @param selectionTarget
     */
    void setSelectionTarget(String selectionTarget);

    /**
     * Provides a selection target id param name for the view
     * @return selectionTargetIdParam
     */
    String getSelectionTargetIdParam();

    /**
     * Sets a selection target id param name for the view
     * @param selectionTargetIdParam
     */
    void setSelectionTargetIdParam(String selectionTargetIdParam);

    /**
     * Provides an action url if jsp includes other pages whose need an url
     * @return actionUrl
     */
    String getActionUrl();

    /**
     * Sets an action url if jsp includes other pages whose need an url
     * @param actionUrl
     */
    void setActionUrl(String actionUrl);

    /**
     * Provides current user of view
     * @return user
     */
    User getUser();

    /**
     * Checks required permissions against current users permissions
     * @param permissions
     * @throws PermissionException if current user has no given permission
     */
    void checkPermission(String[] permissions) throws PermissionException;

    /**
     * Updates the current bean
     * @param form with values to update, see form def. for values
     * @throws ClientException if validation of form values failed
     * @throws PermissionException if current user has no permissions to update
     */
    void save(Form form) throws ClientException, PermissionException;

    /**
     * Provides an object from local cache
     * @param name
     * @return value or null if not exists
     */
    Object getLocalValue(String name);

    /**
     * Indicates that local value exists
     * @param name
     * @return localValueExisting
     */
    boolean isLocalValueExisting(String name);

    /**
     * Sets a local value
     * @param name of the key
     * @param value to set
     */
    void setLocalValue(String name, Object value);

    /**
     * Provides current form
     * @return form
     */
    Form getForm();

    /**
     * Set the associated form values.
     * @param form
     */
    void setForm(Form form);

    /**
     * Provides a list with percentage values from 0 to 100, increment 10
     * @return percentageList
     */
    List<Integer> getPercentageList();

    /**
     * Provides the page header key if available
     * @return pageHeaderKey
     */
    String getPageHeaderKey();

    /**
     * Sets the page header key
     * @param pageHeaderKey
     */
    void setPageHeaderKey(String pageHeaderKey);

    /**
     * Instructs the view creator to create backups of existing views with the same name in session scope
     * @return createBackupWhenExisting
     */
    boolean isCreateBackupWhenExisting();

    /**
     * Indicates that implementing view should not be created if exists
     * @return keepViewWhenExists
     */
    boolean isKeepViewWhenExistsOnCreate();

    /**
     * Enables/disables view keeping behaviour
     * @param keepViewWhenExistsOnCreate
     */
    void setKeepViewWhenExistsOnCreate(boolean keepViewWhenExistsOnCreate);

    /**
     * Indicates that implementing view should be reloaded instead of created if exists
     * @return reloadWhenExistsOnCreate
     */
    boolean isReloadWhenExistsOnCreate();

    /**
     * Indicates that view should immediately exited after save action.
     * @return exitAfterSave
     */
    boolean isExitAfterSave();

    /**
     * Indicates if mighty mode is enabled
     * @return mightyMode
     */
    boolean isMightyMode();

    /**
     * Adds a resource bundle
     * @param locale
     * @param bundle
     */
    void addResourceBundle(Locale locale, ResourceBundle bundle);

    List<String> getScriptsList();

    List<String> getScriptsListBottom();

    List<String> getStylesList();

    /**
     * Reloads the business objects if supported by implementing view.
     */
    void reload();
}
