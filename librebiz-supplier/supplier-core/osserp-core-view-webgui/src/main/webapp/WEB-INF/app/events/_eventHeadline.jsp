<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<c:choose>
	<c:when test="${view.editMode and !empty bean}">
		<c:choose>
			<c:when test="${event.action.type.appointment}">&gt; <fmt:message key="appointment"/></c:when>
			<c:otherwise>&gt; <fmt:message key="jobInformation"/></c:otherwise>
		</c:choose>
	</c:when>
	<c:otherwise>
		<o:listSize value="${view.list}"/> <fmt:message key="openTodos"/>
	</c:otherwise>
</c:choose>
