/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 18, 2011 12:05:52 PM 
 * 
 */
package com.osserp.core.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.osserp.common.Constants;
import com.osserp.common.beans.AbstractOption;
import com.osserp.core.employees.Employee;
import com.osserp.core.sales.SalesRegion;
import com.osserp.core.sales.SalesRegionChange;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 */
public class SalesRegionImpl extends AbstractOption implements SalesRegion {

    private Employee sales;
    private Employee salesDelegate;
    private Employee salesExecutive;
    private List<SalesRegionZipcode> zipcodeList = new ArrayList<SalesRegionZipcode>();
    private List<SalesRegionChange> salesHistory = new ArrayList<SalesRegionChange>();

    protected SalesRegionImpl() {
        super();
    }

    public SalesRegionImpl(
            Employee user,
            String name,
            Employee sales,
            Employee salesExecutive) {
        super(name, (user == null ? Constants.SYSTEM_EMPLOYEE : user.getId()));
        this.sales = sales;
        this.salesExecutive = salesExecutive;
    }

    public Employee getSales() {
        return sales;
    }

    protected void setSales(Employee sales) {
        this.sales = sales;
    }

    public Employee getSalesDelegate() {
        return salesDelegate;
    }

    public void setSalesDelegate(Employee salesDelegate) {
        this.salesDelegate = salesDelegate;
    }

    public Employee getSalesExecutive() {
        return salesExecutive;
    }

    protected void setSalesExecutive(Employee salesExecutive) {
        this.salesExecutive = salesExecutive;
    }

    public void changeSales(Employee user, Employee sales) {
        if (sales != null) {
            setChanged(new Date(System.currentTimeMillis()));
            setChangedBy(user == null ? Constants.SYSTEM_EMPLOYEE : user.getId());
            Long previous = sales.getId();
            this.sales = sales;
            SalesRegionChange change = new SalesRegionChangeImpl(user, this, "sales", sales.getId(), previous);
            this.salesHistory.add(change);
        }
    }

    public void changeSalesExecutive(Employee user, Employee salesExecutive) {
        if (salesExecutive != null) {
            setChanged(new Date(System.currentTimeMillis()));
            setChangedBy(user == null ? Constants.SYSTEM_EMPLOYEE : user.getId());
            Long previous = (this.salesExecutive == null ? null : salesExecutive.getId());
            this.salesExecutive = salesExecutive;
            SalesRegionChange change = new SalesRegionChangeImpl(user, this, "salesExecutive", salesExecutive.getId(), previous);
            salesHistory.add(change);
        }
    }

    public List<String> getZipcodes() {
        List<String> zipcodes = new ArrayList<String>();
        for (int i = 0, j = zipcodeList.size(); i < j; i++) {
            zipcodes.add(zipcodeList.get(i).getZipcode());
        }
        return zipcodes;
    }

    public void addZipcode(String zipcode) {
        SalesRegionZipcode obj = new SalesRegionZipcode(this, zipcode);
        this.zipcodeList.add(obj);
    }

    public void removeZipcode(String zipcode) {
        for (Iterator<SalesRegionZipcode> i = zipcodeList.iterator(); i.hasNext();) {
            SalesRegionZipcode next = i.next();
            if (next.getZipcode().equals(zipcode)) {
                i.remove();
                break;
            }
        }
    }

    public void toggleEol() {
        if (isEndOfLife()) {
            setEndOfLife(false);
        } else {
            setEndOfLife(true);
        }
    }

    public List<SalesRegionChange> getSalesHistory() {
        return salesHistory;
    }

    protected void setSalesHistory(List<SalesRegionChange> salesHistory) {
        this.salesHistory = salesHistory;
    }

    protected List<SalesRegionZipcode> getZipcodeList() {
        return zipcodeList;
    }

    protected void setZipcodeList(List<SalesRegionZipcode> zipcodeList) {
        this.zipcodeList = zipcodeList;
    }
}
