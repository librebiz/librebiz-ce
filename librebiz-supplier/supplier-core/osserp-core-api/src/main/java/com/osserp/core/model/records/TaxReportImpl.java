/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 14, 2014 9:43:34 AM
 * 
 */
package com.osserp.core.model.records;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.EntityRelation;

import com.osserp.core.finance.FinanceRecord;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.Records;
import com.osserp.core.finance.TaxReport;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public class TaxReportImpl extends AbstractFinanceReport implements TaxReport {
    private static Logger log = LoggerFactory.getLogger(TaxReportImpl.class.getName());

    private String taxNote;
    private String taxMethod = VAT_BY_ACTUAL;
    private Long status = 0L;
    
    private List<EntityRelation> items = new ArrayList<EntityRelation>();
    
    private List<FinanceRecord> inflowOfCash = new ArrayList<>();
    private boolean inflowReverse = false;
    private List<FinanceRecord> outflowOfCash = new ArrayList<>();
    private boolean outflowReverse = false;
    
    
    protected TaxReportImpl() {
        super();
    }
    
    /**
     * Creates a new tax report object.
     * @param createdBy
     * @param reference
     * @param taxMethod
     * @param name
     * @param startDate
     * @param stopDate
     */
    public TaxReportImpl(
            Long createdBy, 
            Long reference,
            String taxMethod,
            String name, 
            Date startDate, 
            Date stopDate) {
        super(createdBy, reference, name, startDate, stopDate);
        // use default setting if not provided
        if (taxMethod != null) {
            this.taxMethod = taxMethod;
        }    
    }

    public boolean isClosed() {
        return Record.STAT_SENT.equals(status);
    }

    public void close() {
        status = Record.STAT_SENT;
    }

    public String getTaxMethod() {
        return taxMethod;
    }

    public void setTaxMethod(String taxMethod) {
        this.taxMethod = taxMethod;
    }

    public String getTaxNote() {
        return taxNote;
    }

    public void setTaxNote(String taxNote) {
        this.taxNote = taxNote;
    }

    public Long getStatus() {
        return status;
    }

    protected void setStatus(Long status) {
        this.status = status;
    }
    
    public void changeInflowSortOrder() {
        inflowReverse = !inflowReverse;
    }
    
    public void changeOutflowSortOrder() {
        outflowReverse = !outflowReverse;
    }
    
    public List<FinanceRecord> getInflowOfCash() {
        return inflowOfCash;
    }
    
    public void addInflowOfCash(List<FinanceRecord> records) {
        inflowOfCash.addAll(records);
        Records.sort(inflowOfCash, Records.ORDER_BY_TAXPOINT, inflowReverse);
        if (log.isDebugEnabled()) {
            log.debug("addInflowOfCash() done [count=" 
                    + inflowOfCash.size() + "]");
        }
    }

    protected void setInflowOfCash(List<FinanceRecord> inflowOfCash) {
        this.inflowOfCash = inflowOfCash;
    }

    public List<FinanceRecord> getOutflowOfCash() {
        return outflowOfCash;
    }

    protected void setOutflowOfCash(List<FinanceRecord> outflowOfCash) {
        this.outflowOfCash = outflowOfCash;
    }
    
    public void addOutflowOfCash(List<FinanceRecord> records) {
        outflowOfCash.addAll(records);
        Records.sort(outflowOfCash, Records.ORDER_BY_TAXPOINT, outflowReverse);
        if (log.isDebugEnabled()) {
            log.debug("addOutflowOfCash() done [count=" 
                    + outflowOfCash.size() + "]");
        }
    }

    public List<EntityRelation> getItems() {
        return items;
    }
    
    protected void setItems(List<EntityRelation> items) {
        this.items = items;
    }
}
