/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.osserp.groovy.web

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.common.BackendException
import com.osserp.common.ErrorCode

/**
 *
 * @author eh <eh@osserp.com>
 *
 */
@Controller
abstract class AbstractOutputController {

    @RequestMapping
    def forward(HttpServletRequest request, HttpServletResponse response) {
        OutputView view = getView(request)
        byte[] data = view.getData()
        if (data != null && data.length > 0) {
            response.setContentType(view.getContentType())
            try {
                response.getOutputStream().write(data)
            } catch (IOException e) {
                //do nothing here
            }
            return null
        } else {
            throw new BackendException(ErrorCode.NO_DATA_AVAILABLE)
        }
    }

    /**
     * Gets responsible view by request
     * @return view
     * @throws ViewException if view was not found
     */
    protected final OutputView getView(HttpServletRequest request) throws ViewException {
        WebUtil.getOutputView(request, getClass())
    }
}
