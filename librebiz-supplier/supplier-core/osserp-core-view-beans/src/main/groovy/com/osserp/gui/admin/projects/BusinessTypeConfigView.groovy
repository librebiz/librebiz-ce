/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created on 11.04.2011 17:06:34
 *
 */
package com.osserp.gui.admin.projects

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode

import com.osserp.core.BusinessType
import com.osserp.core.BusinessTypeContext
import com.osserp.core.BusinessTypeManager
import com.osserp.core.ContractType
import com.osserp.core.Options

import com.osserp.core.WorkflowManager
import com.osserp.core.calc.CalculationConfig
import com.osserp.core.calc.CalculationConfigManager
import com.osserp.core.crm.Campaign
import com.osserp.core.system.SystemCompany
import com.osserp.core.dao.BusinessCaseUtilities

import com.osserp.groovy.web.MenuItem

import com.osserp.gui.CoreView

/**
 *
 * @author eh <eh@osserp.com>
 * @author rk <rk@osserp.com>
 *
 */
public class BusinessTypeConfigView extends CoreView {

    private static final Long CALCULATION_CONFIG_TYPE = 1L

    List<CalculationConfig> calculationConfigs = []
    List<SystemCompany> companies = []
    List<BusinessTypeContext> requestContexts = []
    List<BusinessTypeContext> salesContexts = []
    List<ContractType> contractTypes = []
    boolean displayDisabled = false
    CalculationConfig calculationConfig = null

    BusinessTypeConfigView() {
        providesEditMode()
        providesCreateMode()
        providesCustomNavigation()
        headerName = 'orderTypes'
        permissions = 'organisation_admin,runtime_config'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (!list) {
            list = businessTypeManager.findActive()
            requestContexts = businessTypeManager.findByRequestContext()
            salesContexts = businessTypeManager.findBySalesContext()
            contractTypes = businessTypeManager.getContractTypes()
            companies = systemConfigManager.getCompanies()
            calculationConfigs = calculationConfigManager.getConfigs(CALCULATION_CONFIG_TYPE)
            actualSortKey = 'name'
        }
    }

    @Override
    void createCustomNavigation() {
        nav.beanListNavigation = [selectExitLink, editLink, paymentDefaultsLink, homeLink]
        nav.defaultNavigation = nav.listNavigation = [exitLink, toggleDisplayDisabledLink, createLink, homeLink]
    }

    protected MenuItem getPaymentDefaultsLink() {
        MenuItem pdl = navigationLink("/admin/projects/businessTypePaymentDefaults/forward", 'calcIcon', 'defaultPaymentAgreement')
        pdl.ajaxPopup = true
        pdl.ajaxPopupName = 'businessTypePaymentDefaultsView'
        return pdl
    }

    protected MenuItem getToggleDisplayDisabledLink() {
        if (displayDisabled) {
            return navigationLink("/${context.name}/toggleDisplayDisabled", 'nosmileIcon', 'displayActive')
        }
        return navigationLink("/${context.name}/toggleDisplayDisabled", 'smileIcon', 'displayResign')
    }

    @Override
    void reload() {
        List<BusinessType> btlist = businessTypeManager.findAll()
        if (displayDisabled) {
            list = btlist.findAll { BusinessType bt -> !bt.active }
        } else {
            list = btlist.findAll { BusinessType bt -> bt.active }
        }
        actualSortKey = 'name'
        if (bean) {
            bean = list.find { it.id == bean.id }
            calculationConfig = calculationConfigs.find { it.id == bean.calculationConfig }
        }
    }

    @Override
    void select() {
        super.select()
        if (bean) {
            Long[] current = workflowManager.getCurrentSequenceValues(bean.workflow)
            lastId = current[1]
            calculationConfig = calculationConfigs.find { it.id == bean.calculationConfig }
        } else {
            lastId = null
            calculationConfig = null
        }
    }

    Long getIdSuggestion() {
        businessTypeManager.getIdSuggestion()
    }

    @Override
    void save() {
        if (isCreateMode()) {
            Long templateId = form.getLong('templateId')
            if (!templateId) {
                throw new ClientException(ErrorCode.TEMPLATE_REQUIRED)
            }
            BusinessType source = list.find { it.id == templateId }
            bean = businessTypeManager.create(
                    getDomainEmployee(),
                    form.getLong('id'),
                    form.getLong('company'),
                    form.getString('name'),
                    form.getString('key'),
                    source)
            disableCreateMode()
        } else {
            if (bean) {
                BusinessType btype = bean
                boolean recordByCalc = form.getBoolean('recordByCalculation')
                Long defaultOrigin = form.getLong('defaultOrigin')
                Long defaultOriginType = form.getLong('defaultOriginType')
                businessTypeManager.update(
                        bean,
                        form.getString('name'),
                        form.getString('key'),
                        form.getString('requestContext'),
                        form.getString('salesContext'),
                        btype.isActive(),
                        form.getBoolean('requestContextOnly'),
                        form.getBoolean('directSales'),
                        form.getBoolean('trading'),
                        form.getBoolean('packageRequired'),
                        form.getBoolean('interestContext'),
                        form.getBoolean('salesPersonByCollector'),
                        form.getBoolean('supportingDownpayments'),
                        form.getBoolean('supportingPictures'),
                        form.getBoolean('supportingOffers'),
                        btype.isCreateSla(),
                        form.getBoolean('includePriceByDefault'),
                        btype.isDummy(),
                        form.getBoolean('service'),
                        form.getBoolean('subscription'),
                        form.getBoolean('wholeSale'),
                        form.getString('commissionType'),
                        btype.getCapacityFormat(),
                        btype.getCapacityUnit(),
                        btype.getCapacityLabel(),
                        form.getString('language'),
                        recordByCalc,
                        recordByCalc ? form.getLong('calculationConfig') : null,
                        form.getLong('requestLetterType'),
                        form.getLong('salesLetterType'),
                        form.getBoolean('installationDateSupported'),
                        form.getBoolean('startupSupported'),
                        form.getBoolean('externalIdProvided'),
                        form.getString('externalStatusQualifiedRequestKeys'),
                        form.getString('externalStatusSalesKeys'),
                        form.getString('externalStatusSalesClosedKeys'),
                        form.getLong('defaultContractType'),
                        Long.valueOf(0).equals(defaultOrigin) ? null : defaultOrigin,
                        Long.valueOf(0).equals(defaultOriginType) ? null : defaultOriginType)
            }
            disableEditMode()
        }
        reload()
    }

    void toggleDisplayDisabled() {
        displayDisabled = !displayDisabled
        reload()
    }

    void setCampaign(Campaign campaign) {
        bean?.defaultOrigin = campaign?.id
    }

    private BusinessTypeManager getBusinessTypeManager() {
        getService(BusinessTypeManager.class.getName())
    }

    private CalculationConfigManager getCalculationConfigManager() {
        getService(CalculationConfigManager.class.getName())
    }

    private WorkflowManager getWorkflowManager() {
        getService(WorkflowManager.class.getName())
    }
}
