<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${sessionScope.invoiceReminderView}" />

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4><fmt:message key="settings"/></h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive table-responsive-default">
            <table class="table table-striped">
                <tbody>
                    <c:if test="${!empty view.companies}">
                        <tr>
                            <td><fmt:message key="companySelection" /></td>
                            <td><c:forEach var="cmp" items="${view.companies}">
                                    <a href="<c:url value="/salesInvoiceReminder.do?method=removeCompany&id=${cmp.id}"/>"><o:out value="${cmp.name}" /></a>
                                    <br />
                                </c:forEach></td>
                        </tr>
                    </c:if>
                    <c:set var="available" value="${view.availableCompanies}" />
                    <c:if test="${!empty available}">
                        <tr>
                            <td><fmt:message key="availableCompanies" /></td>
                            <td><c:forEach var="cmp" items="${available}">
                                    <a href="<c:url value="/salesInvoiceReminder.do?method=addCompany&id=${cmp.id}"/>"><o:out value="${cmp.name}" /></a>
                                    <br />
                                </c:forEach></td>
                        </tr>
                    </c:if>
                    <tr>
                        <td><fmt:message key="display" /></td>
                        <td><c:choose>
                                <c:when test="${view.includeInternal}">
                                    <a href="<c:url value="/salesInvoiceReminder.do?method=toggleIncludeInternal"/>"><fmt:message key="includeInteriorRecords" /></a>
                                </c:when>
                                <c:otherwise>
                                    <a href="<c:url value="/salesInvoiceReminder.do?method=toggleIncludeInternal"/>"><fmt:message key="withoutInternRecords" /></a>
                                </c:otherwise>
                            </c:choose></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
