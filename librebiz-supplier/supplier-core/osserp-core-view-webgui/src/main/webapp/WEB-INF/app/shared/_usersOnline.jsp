<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>

<div class="table-responsive table-responsive-default">
	<table class="table table-striped">
		<thead>
			<tr>
				<th style="width: 5em;"><fmt:message key="id" /></th>
				<th style="width: 40em;"><fmt:message key="account" /></th>
				<th style="width: 9em;"><fmt:message key="time" /></th>
				<th style="width: 9em;"><fmt:message key="host" /></th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="onlineuser" items="${sessionScope.portalView.onlineUsers}">
			    <c:choose>
                    <c:when test="${!onlineuser.hidden}">
                        <tr>
                            <td style="width: 5em;">
                                <o:ajaxLink linkId="user${onlineuser.id}" url="${'/employeeInfo.do?id='}${onlineuser.id}">
                                    <o:out value="${onlineuser.id}"/>
                                </o:ajaxLink>
                            </td>
                            <td style="width: 40em;">
                                <a href="<c:url value="/contacts.do?method=load&id=${onlineuser.contactId}&exit=onlineUsers"/>">
                                    <o:out value="${onlineuser.loginName}" />
                                </a>
                            </td>
                            <td style="width: 9em;"><o:time value="${onlineuser.loginTime}" addseconds="true" /></td>
                            <td style="width: 9em;"><o:out value="${onlineuser.remoteIp}" /></td>
                        </tr>
                    </c:when>
                    <c:otherwise>
                        <tr>
                            <td style="width: 5em;">ADMIN</td>
                            <td style="width: 40em;">Administrative System Task</td>
                            <td style="width: 9em;"><o:time value="${onlineuser.loginTime}" addseconds="true" /></td>
                            <td style="width: 9em;">localhost</td>
                        </tr>
                    </c:otherwise>
                </c:choose>
			</c:forEach>
		</tbody>
	</table>
</div>
<div class="spacer"></div>