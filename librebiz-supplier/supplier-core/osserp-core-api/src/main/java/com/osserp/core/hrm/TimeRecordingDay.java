/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 30, 2008 9:46:43 AM 
 * 
 */
package com.osserp.core.hrm;

import java.util.Date;
import java.util.List;

import com.osserp.common.Calendar;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface TimeRecordingDay extends Calendar {

    /**
     * Indicates that day is public holiday
     * @return publicHoliday
     */
    boolean isPublicHoliday();

    /**
     * Provides the name of public holiday if available
     * @return public holiday name if day is public holiday and name's available
     */
    String getPublicHolidayName();

    /**
     * Provides total count of minutes of work
     * @return minutesTotal
     */
    int getMinutesTotal();

    /**
     * Provides marker value of this day
     * @return markerValue
     */
    double getMarkerValue();

    /**
     * Indicates that day has a marker
     * @return marker
     */
    boolean isMarker();

    /**
     * Provides net count of minutes of work
     * @return minutesNet
     */
    int getMinutesNet();

    /**
     * Provides breaking minutes
     * @return minutesBreak
     */
    int getMinutesBreak();

    /**
     * Provides minutes of illness
     * @return minutesIllness
     */
    int getMinutesIllness();

    /**
     * Provides leave minutes
     * @return minutesLeave
     */
    int getMinutesLeave();

    /**
     * Provides lost minutes (e.g. minutesNet was greater max minutes per day)
     * @return minutesLost
     */
    int getMinutesLost();

    /**
     * Indicates that this day has a incorrect record (start and stop values equal at one of the days records)
     * @return incorrectRecord
     */
    Date getIncorrectRecord();

    /**
     * Indicates that no bookings at this day and the days is not holiday or weekend
     * @return noBooking
     */
    boolean isNoBooking();

    /**
     * Provides records of day
     * @return records
     */
    List<TimeRecord> getRecords();

    /**
     * Provides markers of day
     * @return dayMarkers
     */
    List<TimeRecordMarker> getDayMarkers();

    /**
     * Provides the recording start date of the day
     * @return startTime wich is first booking of day
     */
    Date getStartTime();

    /**
     * Provides the recording stop date of the day
     * @return stopTime wich is last booking of day <br/>
     * or null if an open start booking found
     */
    Date getStopTime();

    /**
     * Provides the hours and minutes of completed work
     * @return hoursDisplay
     */
    String getHoursDisplay();

    /**
     * Provides minutes expected this day (e.g. daily minutes - leave)
     * @return minutesRequired
     */
    int getMinutesRequired();

    /**
     * Provides the lost hours and minutes of work over daily limit
     * @return lostHoursDisplay
     */
    String getLostHoursDisplay();

    /**
     * Provides the estimated hours to work a day
     * @return owrkingHoursDisplay
     */
    String getWorkingHoursDisplay();

    /**
     * Provides the summary of carryover minutes in month
     * @return minutesCarryoverInMonth
     */
    int getMinutesCarryoverInMonth();

    /**
     * Provides the hours and minutes of carryover minutes in month
     * @return carryoverHoursInMonthDisplay
     */
    String getCarryoverHoursInMonthDisplay();

    /**
     * Indicates that day is beyond range
     * @return beyondRange
     */
    boolean isBeyondRange();

    /**
     * Indicates that an open record found on this day or no booking <br/>
     * (e.g. hours to work == missing hours)
     * @return true if so
     */
    boolean isOpen();

    /**
     * Indicates that day is booking a given date and time
     * @param date
     * @param startBooking
     * @return true if any record of day is booking given date
     */
    boolean isBooking(Date dateAndTime, boolean startBooking);

    /**
     * Indicates that day is booking a given date and time
     * @param startDate
     * @param stopDate
     * @return true if any record of day is booking given date
     */
    boolean isBooking(Date startDate, Date stopDate);

    /**
     * Not necessary yet. When needed some changes are required in this methods
     * 
     * Provides day values as xml
     * @return xml
     * 
     * Element getXml(int completedSummary, int periodSummary);
     */

    /**
     * Provides the hours and minutes of difference time
     * @return differenceHoursDisplay
     */
    String getDifferenceHoursDisplay();

    /**
     * Provides and minutes difference
     * @return minutesDifference
     */
    int getMinutesDifference();

    /**
     * Provides and minutes subtraction
     * @return minutesSubtraction
     */
    int getMinutesSubtraction();

    /**
     * Provides and minutes addition
     * @return minutesAddition
     */
    int getMinutesAddition();

    /**
     * Return true if there is a subtraction booking with the same duration of minutesLost
     * @return lostSubtractionBooking
     */
    boolean isLostSubtractionBooking();

    /**
     * Indicates that a booking at this day wait for an approval
     * @return waitForApproval
     */
    boolean isWaitForApproval();
}
