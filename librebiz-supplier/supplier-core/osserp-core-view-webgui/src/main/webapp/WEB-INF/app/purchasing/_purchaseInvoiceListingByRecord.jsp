<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<o:logger write="rendering records" level="debug" />
<div class="table-responsive table-responsive-default">
    <table class="table table-striped">
        <thead>
            <tr>
                <th class="recordTypeKey"><fmt:message key="type" /></th>
                <th class="recordNumber"><v:sortLink key="id"><fmt:message key="recordNumberShort" /></v:sortLink></th>
                <th class="recordDate"><fmt:message key="from" /></th>
                <th class="recordNumber"><v:sortLink key="referenceNumber"><fmt:message key="operationUnitNumber" /></v:sortLink></th>
                <th class="recordName"><fmt:message key="company" /></th>
                <th class="amount right"><fmt:message key="amount" /></th>
                <th class="amount right taxAmount"><fmt:message key="vat" /></th>
                <th class="amount right"><fmt:message key="total" /></th>
                <th class="center"></th>
            </tr>
        </thead>
        <tbody>
            <c:choose>
                <c:when test="${empty view.list}">
                    <tr>
                        <td colspan="9"><fmt:message key="noRecordsAvailable" /> <v:link url="/purchasing/purchaseInvoiceCreator/forward?exit=${view.baseLink}/reload">
                                [<fmt:message key="createNew" />]
                            </v:link>
                        </td>
                    </tr>
                </c:when>
                <c:otherwise>
                    <c:forEach var="record" items="${view.list}" varStatus="s" begin="${view.listStart}" end="${view.listEnd}">
                        <tr id="record${record.id}">
                            <td class="recordTypeKey"><oc:options name="purchaseInvoiceShorttypes" value="${record.subType}" /></td>
                            <td class="recordNumber">
                                <a href="<c:url value="/purchaseInvoice.do?method=display&id=${record.id}&exit=${view.name}&exitId=record${record.id}"/>">
                                    <o:out value="${record.number}" />
                                </a>
                            </td>
                            <td class="recordDate"><o:date value="${record.created}" /></td>
                            <td class="recordNumber">
                                <c:choose>
                                    <c:when test="${record.bookingType.credit}">
                                        <a href="<c:url value="/purchaseInvoice.do?method=display&id=${record.reference}&exit=${view.name}&exitId=record${record.id}"/>"> <o:out value="${record.referenceNumber}" />
                                        </a>
                                    </c:when>
                                    <c:otherwise>
                                        <a href="<c:url value="/purchaseOrder.do?method=display&exit=${view.name}&exitId=record${record.id}&readonly=true&id=${record.reference}"/>"> <o:out value="${record.referenceNumber}" />
                                        </a>
                                    </c:otherwise>
                                </c:choose>
                            </td>
                            <td class="recordName">
                                <a href="<c:url value="/suppliers.do?method=load&id=${record.contactId}&exit=${view.name}&exitId=record${record.id}"/>">
                                    <o:out value="${record.lastname}" />
                                </a>
                            </td>
                            <td class="amount right"><o:number value="${record.netAmount}" format="currency" /></td>
                            <td class="amount right taxAmount"><o:number value="${record.taxTotal}" format="currency" /></td>
                            <td class="amount right">
                                <span <c:if test="${view.purchasePaymentWarnings and !record.paid}"> class="error"</c:if>>
                                    <o:number value="${record.grossAmount}" format="currency" /><span><o:out value="${record.currencySymbol}" /></span>
                                </span>
                            </td>
                            <td class="icon center">
                                <c:if test="${record.unchangeable and !empty record.typeName}">
                                    <v:link url="/records/recordPrint/print?name=${record.typeName}&id=${record.id}" title="documentPrint">
                                        <o:img name="printIcon" />
                                    </v:link>
                                </c:if>
                            </td>
                        </tr>
                    </c:forEach>
                </c:otherwise>
            </c:choose>
        </tbody>
    </table>
</div>
