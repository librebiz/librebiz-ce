/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 10-Nov-2006 02:22:38 
 * 
 */
package com.osserp.core.service.contacts;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ActionTarget;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.common.Permission;
import com.osserp.common.User;
import com.osserp.common.UserAuthenticator;
import com.osserp.common.util.StringUtil;

import com.osserp.core.contacts.Contact;
import com.osserp.core.dao.Users;
import com.osserp.core.employees.Employee;
import com.osserp.core.employees.EmployeeGroup;
import com.osserp.core.users.DomainUser;
import com.osserp.core.users.DomainUserManager;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class UserManagerImpl implements DomainUserManager {
    private static Logger log = LoggerFactory.getLogger(UserManagerImpl.class.getName());
    private Users users;
    private UserAuthenticator userAuthenticator;
    private boolean localPasswordRequired = true;

    /**
     * Creates a new user manager with local authenticator
     * @param users
     */
    public UserManagerImpl(Users users) {
        this.users = users;
    }

    /**
     * Creates a new user manager with dedicated authenticator
     * @param users
     * @param userAuthenticator
     */
    public UserManagerImpl(Users users, UserAuthenticator userAuthenticator) {
        this.users = users;
        this.userAuthenticator = userAuthenticator;
        if (this.userAuthenticator != null) {
        	// local password is not required in external authentication context
        	localPasswordRequired = false;
        }
    }

    public User disable(User user) {
        user.setActive(false);
        users.save(user);
        if (user.getLdapUid() != null && userAuthenticator != null) {
            userAuthenticator.deactivate(user);
        }
        return user;
    }

    public User enable(User user) {
        user.setActive(true);
        users.save(user);
        if (user.getLdapUid() != null && userAuthenticator != null) {
            userAuthenticator.activate(user);
        }
        return user;
    }

    public User grantPermission(User grantBy, Employee employee, String permission) {
        return users.grantPermission(grantBy, employee, permission);
    }

    public User revokePermission(User revokedBy, Employee employee, String permission) {
        return users.revokePermission(revokedBy, employee, permission);
    }

    public void grantPermissions(User grantBy, Employee employee, EmployeeGroup group) {
        if (grantBy instanceof DomainUser) {
            users.grantPermissions(((DomainUser) grantBy).getEmployee(), employee, group);
        } else {
            log.warn("grantPermissions() nothing to do, current user is not domainUser or not provided");
        }
    }

    public void revokePermissions(User revokedBy, Employee employee, EmployeeGroup group) {
        if (revokedBy instanceof DomainUser) {
            users.revokePermissions(((DomainUser) revokedBy).getEmployee(), employee, group);
        } else {
            log.warn("grantPermissions() nothing to do, current user is not domainUser or not provided");
        }
    }

    public User save(User user) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("save() invoked for object "
                    + user.getClass().getName());
        }
        if (user == null) {
            log.warn("save() nothing to do user was null!");
            return null;
        }
        if (users == null) {
            log.error("save() ignoring application config error: Missing users dao!");
            return user;
        }
        if (user.getLoginName() == null) {
            throw new ClientException(ErrorCode.LOGIN_NAME_MISSING);
        }
        List<Option> existing = users.getLoginNames();
        for (int i = 0, j = existing.size(); i < j; i++) {
            Option next = existing.get(i);
            if (next.getName() != null) {
                if (next.getName().equals(user.getLoginName())
                        && !next.getId().equals(user.getId())) {
                    throw new ClientException(ErrorCode.NAME_EXISTS);
                }
            }
        }
        if (localPasswordRequired && (user.getPassword() == null || user.getPassword().isEmpty())) {
            throw new ClientException(ErrorCode.PASSWORD_MISSING);
        }
        users.save(user);
        return users.find(user.getId());
    }

    public User setProperty(User currentUser, User user, String name, String value) throws ClientException {
        user.setProperty(currentUser, name, value);
        users.save(user);
        return users.find(user.getId());
    }

    public User switchProperty(User currentUser, User user, String name) {
        if (user.isPropertyEnabled(name)) {
            user.setProperty(currentUser, name, "false");
        } else {
            user.setProperty(currentUser, name, "true");
        }
        users.save(user);
        return users.find(user.getId());
    }

    public User changePassword(User user, String newPassword, String confirmPassword) throws ClientException {
        if (newPassword == null
                || confirmPassword == null
                || !newPassword.equals(confirmPassword)) {
            throw new ClientException(ErrorCode.CHANGE_PASSWORD_MATCH);
        }
        if (userAuthenticator == null) {
            // TODO implement local password change
            throw new ClientException(ErrorCode.SERVICE_DEPENDENCY_UNAVAILABLE);
        }
        return userAuthenticator.changePassword(user, newPassword);
    }

    public User changeLocale(User user, String locale) {
        String[] localeArray = StringUtil.getLocaleParts(locale);
        if (localeArray != null && localeArray[0] != null) {
            user.setLocaleLanguage(localeArray[0]);
            if (localeArray[1] != null) {
                user.setLocaleCountry(localeArray[1]);
            }
            users.save(user);
            return findById(user.getId());
        } 
        log.warn("changeLocale() invoked with invalid value [value=" + locale + ", expected=xx_YY]");
        return user;
    }

    public List<User> findActive() {
        return users.findActive();
    }

    public User findById(Long userId) {
        Long uid = users.getUserByEmployee(userId);
        if (uid == null || uid == 0) {
            uid = userId;
        }
        return users.find(uid);
    }

    public User find(Contact contact) {
        return users.find(contact);
    }

    public User find(Employee employee) {
        return users.find(employee);
    }

    public boolean isPermissionGrant(Long userId, String permission) {
        return users.isPermissionGrant(userId, permission);
    }

    public List<Permission> getPermissions() {
        return users.getPermissions();
    }

    public List<Option> getPermissionMembers(String permission) {
        return users.getUsersByPermission(permission);
    }

    public List<ActionTarget> getStartupTargets() {
        return users.getStartupTargets();
    }
}
