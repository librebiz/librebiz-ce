<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="salesProcessView"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title"><o:displayTitle/></tiles:put>

    <tiles:put name="headline">
        <c:choose>
            <c:when test="${view.values.stopped}">
                <fmt:message key="actualStoppedOrderInProcess"/> <c:if test="${!empty view.values.company}">[<oc:options name="systemCompanies" value="${view.values.company}"/>, <fmt:message key="count"/> <o:listSize value="${view.list}"/>]</c:if>:
            </c:when>
            <c:otherwise>
                <fmt:message key="salesMonitoringLabel"/> <c:if test="${!empty view.values.company}">[<oc:options name="systemCompanies" value="${view.values.company}"/>, <fmt:message key="count"/> <o:listSize value="${view.list}"/>]</c:if>:
            </c:otherwise>
        </c:choose>
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation/>
    </tiles:put>
    <tiles:put name="content" type="string">
        <div class="content-area" id="salesProcessContent">
            <div class="row">
                <div class="col-lg-12 panel-area">

                    <div class="table-responsive table-responsive-default">
                        <table class="table table-striped" >
                            <thead>
                                <tr>
                                    <th><v:sortLink key="id"><fmt:message key="number"/></v:sortLink></th>
                                    <th><v:sortLink key="name"><fmt:message key="commission"/></v:sortLink></th>
                                    <th class="center"><v:sortLink key="salesKey"><fmt:message key="salesShortcut"/></v:sortLink></th>
                                    <th class="center"><v:sortLink key="managerKey"><fmt:message key="projectorShort"/></v:sortLink></th>
                                    <th class="center"><v:sortLink key="created"><fmt:message key="order"/></v:sortLink></th>
                                    <th class="center"><v:sortLink key="downpaymentRequest"><fmt:message key="downpaymentInvoiceAt"/></v:sortLink></th>
                                    <th class="center"><v:sortLink key="downpaymentResponse"><fmt:message key="downpaymentInvoicePaid"/></v:sortLink></th>
                                    <th class="center"><v:sortLink key="deliveryInvoiceRequest"><fmt:message key="deliveryInvoiceAt"/></v:sortLink></th>
                                    <th class="center"><v:sortLink key="deliveryInvoiceResponse"><fmt:message key="deliveryInvoicePaidShort"/></v:sortLink></th>
                                    <th class="center"><v:sortLink key="finalInvoiceRequest"><fmt:message key="finalInvoiceAt"/></v:sortLink></th>
                                    <th class="center"><v:sortLink key="partialPaymentDate"><fmt:message key="partialPaymentAtShort"/></v:sortLink></th>
                                    <th class="right"><v:sortLink key="openAmount"><fmt:message key="amount"/></v:sortLink></th>
                                    <th class="status right"><v:sortLink key="status">%</v:sortLink></th>
                                    <th class="center" colspan="3"><fmt:message key="info"/></th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="project" items="${view.list}" varStatus="s">
                                    <tr id="sales${project.id}"<c:if test="${project.vacant}"> class="red"</c:if>>
                                        <td>
                                            <a href="<c:url value="/loadSales.do?id=${project.id}&exit=salesProcess&exitId=sales${project.id}"/>"><o:out value="${project.id}"/></a>
                                        </td>
                                        <td>
                                            <a href="<c:url value="/loadSales.do?id=${project.id}&exit=salesProcess&exitId=sales${project.id}"/>"><o:out value="${project.name}" limit="24" /></a>
                                        </td>
                                        <td class="center">
                                            <c:if test="${!empty project.salesId and project.salesId > 0}">
                                                <v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${project.salesId}"><oc:employee initials="true" value="${project.salesId}" /></v:ajaxLink>
                                            </c:if>
                                        </td>
                                        <td class="center">
                                            <c:if test="${!empty project.managerId and project.managerId > 0}">
                                                <v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${project.managerId}"><oc:employee initials="true" value="${project.managerId}" /></v:ajaxLink>
                                            </c:if>
                                        </td>
                                        <td class="center"><o:date value="${project.created}" casenull="-" addcentury="false"/></td>
                                        <td class="center"><o:date value="${project.downpaymentRequest}" casenull="-" format="noyear"/></td>
                                        <td class="center"><o:date value="${project.downpaymentResponse}" casenull="-" format="noyear"/></td>
                                        <td class="center"><o:date value="${project.deliveryInvoiceRequest}" casenull="-" format="noyear"/></td>
                                        <td class="center"><o:date value="${project.deliveryInvoiceResponse}" casenull="-" format="noyear"/></td>
                                        <td class="center"><o:date value="${project.finalInvoiceRequest}" casenull="-" format="noyear"/></td>
                                        <td class="center">
                                            <c:choose>
                                                <c:when test="${empty project.partialPaymentDate}">-</c:when>
                                                <c:otherwise>x</c:otherwise>
                                            </c:choose>
                                        </td>
                                        <td class="right"><o:number value="${project.openAmount}" format="currency"/></td>
                                        <td class="status right">
                                            <v:ajaxLink url="/sales/flowControlDisplay/forward?view=${view.name}&id=${project.id}" styleClass="boldtext" linkId="flowControlDisplayView">
                                                <o:out value="${project.status}"/>
                                            </v:ajaxLink>
                                        </td>
                                        <td class="icon center">
                                            <o:permission role="notes_sales_deny" info="permissionNotes">
                                                <v:ajaxLink url="/sales/businessCaseDisplay/forward?notes=true&id=${project.id}" linkId="businessCaseDisplayView">
                                                    <o:img name="texteditIcon"/>
                                                </v:ajaxLink>
                                            </o:permission>
                                        </td>
                                        <td class="icon center">
                                            <v:ajaxLink url="/sales/businessCaseDisplay/forward?id=${project.id}" linkId="businessCaseDisplayView">
                                                <o:img name="openFolderIcon"/>
                                            </v:ajaxLink>
                                        </td>
                                        <td class="icon center">
                                            <c:choose>
                                                <c:when test="${!project.released}">
                                                    <span title="<fmt:message key="orderNotReleased"/>"><o:img name="importantDisabledIcon"/></span>
                                                </c:when>
                                                <c:when test="${project.deliveryClosed}">
                                                    <span title="<fmt:message key="materialPlanningFinished"/>"><o:img name="importantDisabledIcon"/></span>
                                                </c:when>
                                                <c:otherwise>
                                                    <v:ajaxLink url="/sales/deliveryStatusDisplay/forward?id=${project.id}" linkId="deliveryStatus" title="materialPlanning">
                                                        <o:img name="importantIcon"/>
                                                    </v:ajaxLink>
                                                </c:otherwise>
                                            </c:choose>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
