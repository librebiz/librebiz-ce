/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 14, 2006 11:21:56 AM 
 * 
 */
package com.osserp.core.dao.records;

import java.util.List;

import com.osserp.common.Option;

import com.osserp.core.Item;
import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.ItemChangedInfo;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.products.Product;
import com.osserp.core.purchasing.PurchaseInvoice;
import com.osserp.core.purchasing.PurchaseOrder;
import com.osserp.core.views.OrderItemsDisplay;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface PurchaseOrders extends Orders, PurchaseRecords, BookingTypeAwareRecords {

    /**
     * Creates a new purchase order as carry over from another order
     * @param user
     * @param order
     * @param carryOver
     * @return new created order
     */
    PurchaseOrder create(Employee user, PurchaseOrder order, List<Item> carryOver);

    /**
     * Creates a new purchase order as copy from another order
     * @param user
     * @param order
     * @return new created order
     */
    PurchaseOrder create(Employee user, PurchaseOrder order);

    /**
     * Creates a new purchase order
     * @param user
     * @param company
     * @param branchId
     * @param contact
     * @param sale
     * @return new created order
     */
    Order create(Employee user, Long company, Long branchId, ClassifiedContact contact, Long sale);

    /**
     * Provides all open purchase orders
     * @param product
     * @param descendant
     * @return open
     */
    List<Record> getOpenReleased(Product product, boolean descendant);

    /**
     * Provides all open purchase orders with confirmed or unconfirmed status
     * @param confirmed
     * @return openByConfirmation
     */
    List<RecordDisplay> findOpenByConfirmation(boolean confirmed);

    /**
     * Provides open records by referenced contact
     * @param contactId id of related contact
     * @return open orders by contact
     */
    List<RecordDisplay> findOpenByContact(Long contactId);

    /**
     * Provides an option list of all suppliers with open orders
     * @return currentSuppliers
     */
    List<Option> findCurrentSuppliers();

    /**
     * Provides the related purchase order if exists
     * @param invoice
     * @return order or null if invoice was directly created
     */
    PurchaseOrder getOrder(PurchaseInvoice invoice);

    /**
     * Closes given order and all related delivery notes
     * @param recordId
     */
    void closeOrder(Long recordId);

    /**
     * Reopens given order
     * @param recordId
     * @return reopenedOrder
     */
    Order reopenOrder(Long recordId);

    /**
     * Provides all open order items
     * @param stockId
     * @param productId or null if all should selected
     * @return openItems
     */
    List<OrderItemsDisplay> getOpen(Long stockId, Long productId);

    /**
     * Provides carryover orders related to an invoice
     * @param invoice
     * @return carryover orders or empty list
     */
    List<PurchaseOrder> getCarryover(PurchaseInvoice invoice);

    /**
     * Persists an item changed info
     * @param notPersistentInfo
     * @return persistent info
     */
    ItemChangedInfo createItemChangedInfo(ItemChangedInfo notPersistentInfo);
}
