/**
 *
 * Copyright (C) 2015 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 18.08.2015 
 * 
 */
package com.osserp.gui.admin.dms

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.dms.DmsDocument
import com.osserp.common.dms.DmsUtil
import com.osserp.common.dms.StylesheetDmsManager

import com.osserp.core.dms.TemplateDocument

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class StylesheetDmsView extends AbstractTemplateDmsView {
    private static Logger logger = LoggerFactory.getLogger(StylesheetDmsView.class.getName())
    
    List<TemplateDocument> printTemplates = []
    List<TemplateDocument> textTemplates = []
    
    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            String templateName = form.getString('template')
            if (templateName) {
                DmsDocument doc = templateDmsManager.findEmailText(templateName)
                if (doc) {
                    bean = new TemplateDocument(doc, null)
                    enableEditMode()
                    enableExternalInvocationMode()
                } else {
                    logger.info("initRequest: template text not found [template=${templateName}]")
                }
            }
            if (!bean) {
                reload()
            }
        }
    }

    @Override
    void reload() {
        // wrap each DmsDocument object into TemplateDocument
        // TemplateDocument provides extended template properties
        if (!externalInvocationMode) {
            list.clear()
            printTemplates.clear()
            textTemplates.clear()
            List tmp = templateDmsManager.findDocuments()
            tmp.each {
                TemplateDocument tpl = new TemplateDocument(it, null)
                if (tpl.printTemplate) {
                    printTemplates << tpl
                } else {
                    textTemplates << tpl
                }
                list << tpl
            }
            DmsUtil.sortByOrder(printTemplates, false)
            DmsUtil.sortByOrder(textTemplates, false)
            logger.debug("reload: done [printTemplates=${printTemplates.size()}, textTemplates=${textTemplates.size()}]")
        }
    }

    protected StylesheetDmsManager getTemplateDmsManager() {
        getService(StylesheetDmsManager.class.getName())
    }
}
