<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${sessionScope.eventEscalationView}" />
<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>

    <tiles:put name="headline">
        <o:listSize value="${view.list}" />
        <fmt:message key="escalatedJobsAfterReceiver" />
    </tiles:put>
    <tiles:put name="headline_right">
        <ul>
            <li><a href="<c:url value="/salesEventEscalations.do?method=exit"/>"><o:img name="backIcon" /></a></li>
            <li><v:link url="/index" title="backToMenu">
                    <o:img name="homeIcon" />
                </v:link>
            </li>
        </ul>
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="employeeProjectsContent">
            <div class="row">
                <div class="col-lg-12 panel-area">
                    <div class="table-responsive table-responsive-default">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th style="width: 33px;" class="center"><fmt:message key="totalEnergy" /></th>
                                    <th style="width: 62px;"><fmt:message key="from" /></th>
                                    <th style="width: 300px;"><fmt:message key="job" /></th>
                                    <th style="width: 50px;"><fmt:message key="order" /></th>
                                    <th style="width: 300px;"><fmt:message key="name" /></th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:choose>
                                    <c:when test="${empty view.list}">
                                        <tr>
                                            <td colspan="5"><fmt:message key="noEscalatedOrdersFound" /></td>
                                        </tr>
                                    </c:when>
                                    <c:otherwise>
                                        <c:forEach var="obj" items="${view.list}" varStatus="s">
                                            <tr>
                                                <td style="width: 33px;"><o:out value="${obj.sourceRecipientName}" /></td>
                                                <td style="width: 62px;"><o:date value="${obj.sourceCreated}" /></td>
                                                <td style="width: 300px;"><o:out value="${obj.eventName}" /></td>
                                                <td style="width: 50px;"><a href="<c:url value="/loadSales.do?exit=${view.name}&id=${obj.id}"/>"><o:out value="${obj.id}" /></a></td>
                                                <td style="width: 300px;"><o:out value="${obj.name}" /></td>
                                            </tr>
                                        </c:forEach>
                                    </c:otherwise>
                                </c:choose>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
