/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.osserp.gui.telephones

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.core.telephone.TelephoneGroupManager
import com.osserp.gui.CoreView

/**
 * 
 * @author so <so@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public class TelephoneGroupEditView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(TelephoneGroupEditView.class.getName())

    private static final String TELEPHONE_GROUP_EDIT_PERMISSIONS = 'telephone_system_admin'
    String edit

    TelephoneGroupEditView() {
        dependencies = ['telephoneGroupView']
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        edit = form.getString('name')
        Long id = form.getLong('id')
        load(id)
    }

    private void load(Long id) {
        if (id) {
            bean = telephoneGroupManager.find(id)
        }
    }

    @Override
    void enableEditMode() {
        this.checkPermission(TELEPHONE_GROUP_EDIT_PERMISSIONS)
        //initRequest tries to load at first, so bean should be given
        if (bean) {
            env.editMode = true
            if (edit != 'name') {
                throw new ClientException(ErrorCode.INVALID_CONTEXT)
            }
        } else {
            throw new ClientException(ErrorCode.INVALID_CONTEXT)
        }
    }

    @Override
    void save() {
        this.checkPermission(TELEPHONE_GROUP_EDIT_PERMISSIONS)
        if (bean?.id) {
            if (edit == 'name') {
                telephoneGroupManager.updateName(user.id, bean.id, form.getString('value'))
            } else {
                throw new ClientException(ErrorCode.INVALID_CONTEXT)
            }
            load(bean.id)
            env?.telephoneGroupView?.reload()
        } else {
            throw new ClientException(ErrorCode.INVALID_CONTEXT)
        }
        env.editMode = false
    }

    private TelephoneGroupManager getTelephoneGroupManager() {
        getService(TelephoneGroupManager.class.getName())
    }
}
