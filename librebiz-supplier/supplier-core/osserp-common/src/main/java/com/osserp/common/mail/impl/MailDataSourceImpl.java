/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 22, 2008 11:58:04 AM 
 * 
 */
package com.osserp.common.mail.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.activation.DataSource;

/**
 * 
 * Usage: MimeBodyPart mbp = new MimeBodyPart(); DataSource mds = new MailDataSourceImpl(bos, "APPLICATION/OCTET-STREAM"); mbp.setDataHandler(new
 * DataHandler(mds)); mbp.setFileName(reportName + ".xls");
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class MailDataSourceImpl implements DataSource {

    public MailDataSourceImpl(byte[] array, String type) {
        this.bytes = array;
        this.type = type;
    }

    public MailDataSourceImpl(ByteArrayOutputStream bos, String type) {
        this.bytes = bos.toByteArray();
        this.type = type;
    }

    public InputStream getInputStream() throws IOException {
        return new ByteArrayInputStream(bytes);
    }

    public String getName() {
        return "";
    }

    public OutputStream getOutputStream() throws IOException {
        throw new IOException("cannot do this");
    }

    public String getContentType() {
        return type;
    }

    //	private ByteArrayOutputStream _bos;
    private byte[] bytes = null;
    private String type; // content-type

}
