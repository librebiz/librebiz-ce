/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 23, 2011 10:32:47 AM 
 * 
 */
package com.osserp.gui.admin.events

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.core.employees.EmployeeSearch

import com.osserp.gui.selections.AbstractSelectionView;

/**
 *
 * @author eh <eh@osserp.com>
 * 
 */
class PoolMemberSelectionView extends AbstractSelectionView {
    private static Logger logger = LoggerFactory.getLogger(PoolMemberSelectionView.class.getName())

    Long id = null

    PoolMemberSelectionView() {
        super()
        headerName = 'employeeSelection'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (!list) {
            list = employeeSearch.findActive()
        }

        def employeeId = form.getLong('id')
        if (employeeId) {
            id = employeeId
        }
    }
}

