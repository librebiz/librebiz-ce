/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 13, 2016 
 * 
 */
package com.osserp.core.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.jdom2.Element;

import com.osserp.common.Property;
import com.osserp.common.beans.AbstractPersistent;
import com.osserp.common.xml.JDOMUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractDetails extends AbstractPersistent {
    
    private List<Property> propertyList = new ArrayList<Property>();
    private Map<String, Property> properties = new HashMap<String, Property>();

    /**
     * Default constructor adding serialializable capability to entities
     */
    protected AbstractDetails() {
        super();
    }

    protected AbstractDetails(Long reference) {
        super(reference);
    }

    protected AbstractDetails(Long reference, List<Property> properties) {
        super(reference);
        propertyList = properties;
    }
    
    protected abstract BusinessPropertyImpl newProperty(
            Long user, 
            String referenceContext,
            String referenceType,
            Long reference, 
            String name, 
            String value);
    
    public Map<String, Property> getPropertyMap() {
        if ((properties.isEmpty() && !propertyList.isEmpty()) || properties.size() != propertyList.size()) {
            resetPropertyMap();
        }
        return properties;
    }

    public void addProperty(Long user, String name, Object value) {
        boolean found = false;
        for (Iterator<Property> i = propertyList.iterator(); i.hasNext();) {
            Property property = i.next();
            if (property.getName().equals(name)) {
                property.setValue(value == null ? null : value.toString());
                found = true;
                break;
            }
        }
        if (!found) {
            Property property = newProperty(user, null, null, getReference(), name, (value == null ? null : value.toString()));
            propertyList.add(property);
        }
        resetPropertyMap();
    }

    public void removeProperty(Long user, Property property) {
        for (Iterator<Property> i = propertyList.iterator(); i.hasNext();) {
            Property next = i.next();
            if (isMatching(next, property)) {
                i.remove();
                break;
            }
        }
        resetPropertyMap();
    }
    
    protected boolean isMatching(Property a, Property b) {
        try {
            BusinessPropertyImpl objA = (BusinessPropertyImpl) a;
            BusinessPropertyImpl objB = (BusinessPropertyImpl) b;
            return objA.getId().equals(objB.getId());
        } catch (Exception e) {
            try {
                return a.getValue().equals(b.getValue());
            } catch (Exception e2) {
                return false;
            }
        }
    }

    /**
     * Provides the product properties
     * @return the propertyList
     */
    public final List<Property> getPropertyList() {
        return propertyList;
    }

    /**
     * Sets the product properties
     * @param propertyList the properties to set
     */
    protected void setPropertyList(List<Property> propertyList) {
        this.propertyList = propertyList;
    }

    /**
     * Resets the property map
     */
    private void resetPropertyMap() {
        if (properties == null) {
            properties = new HashMap<String, Property>();
        } else {
            properties.clear();
        }
        for (int i = 0, j = propertyList.size(); i < j; i++) {
            Property p = propertyList.get(i);
            properties.put(p.getName(), p);
        }
    }

    @Override
    public Element getXML(String rootElementName) {
        Element root = super.getXML(rootElementName);
        Element propElements = new Element(getPropertyXmlElementName());
        for (int i = 0, j = propertyList.size(); i < j; i++) {
            Property property = propertyList.get(i);
            propElements.addContent(JDOMUtil.createElement(property.getName(), property.getValue()));
        }
        root.addContent(propElements);
        return root;
    }
    
    /**
     * Provides the name of properties xml element
     * @return 'properties' by default 
     */
    protected String getPropertyXmlElementName() {
        return "properties";
    }
}
