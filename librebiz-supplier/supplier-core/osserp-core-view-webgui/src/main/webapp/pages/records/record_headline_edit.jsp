<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<c:set var="view" value="${sessionScope.recordView}"/>
<c:set var="record" value="${view.record}"/>
<span class="bolderror" title="<fmt:message key="changeModeActivated"/>">
    <fmt:message key="changeLabel"/>: 
</span>
<span><o:out value="${record.number}"/> / <o:out value="${record.contact.displayName}"/></span>
<c:if test="${!empty record.businessCaseId}">
    <span> - <fmt:message key="salesOrder"/> <o:out value="${record.businessCaseId}"/></span>
</c:if>
