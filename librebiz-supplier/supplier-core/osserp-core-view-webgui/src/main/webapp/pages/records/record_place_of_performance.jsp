<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>

<c:set var="view" value="${sessionScope.recordView}"/>
<c:set var="record" value="${view.record}"/>

<c:if test="${!empty record.placeOfPerformance}">
<div class="recordLabelValue">
    <span>
        <fmt:message key="placeOfPerformance"/>: 
        <o:out value="${record.placeOfPerformance}"/>
    </span>
</div>
</c:if>
