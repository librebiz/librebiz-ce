/**
 *
 * Copyright (C) 2012, 2021 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License (AGPL)
 * version 3 as published by the Free Software Foundation. In accordance
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code
 * versions must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area
 * or, if not reasonably feasible for technical reason, as a top-level
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute.
 *
 */
package com.osserp.core.api.defaults

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.api.WSContact

import com.osserp.common.Constants
import com.osserp.common.Option

import com.osserp.core.contacts.Contact
import com.osserp.core.contacts.Salutation
import com.osserp.core.suppliers.Supplier
import com.osserp.core.dao.ContactCreator
import com.osserp.core.dao.Contacts
import com.osserp.core.dao.Suppliers

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class CoreSupplierServiceProcessor extends AbstractContactServiceProcessor {
    private static Logger logger = LoggerFactory.getLogger(CoreSupplierServiceProcessor.class.getName())

    Suppliers suppliers

    protected CoreSupplierServiceProcessor(
        ContactCreator contactCreator,
        Contacts contacts,
        Suppliers suppliers) {
        super(contactCreator, contacts)
        this.suppliers = suppliers
    }

    Supplier findOrCreate(WSContact wscontact) {
        findOrCreateSupplier(wscontact, Constants.SYSTEM_EMPLOYEE)
    }

    final Supplier findOrCreateSupplier(WSContact wscontact, Long createdBy) {
        Supplier supplier = findByKeys(wscontact)
        if (supplier) {
            return update(supplier, wscontact)
        }

        if (wscontact.externalKey) {
            supplier = (Supplier) suppliers.findByExternalReference(wscontact.externalKey)
            if (supplier?.id) {
                logger.debug("findOrCreateSupplier: Found by externalKey [id=${supplier.id}, key=${supplier.externalReference}]")
                return supplier
            }
        }
        if (wscontact.primaryKey) {
            supplier = (Supplier) suppliers.find(wscontact.primaryKey)
            if (supplier?.id) {
                logger.debug("findOrCreate: supplier found by primaryKey [id=${supplier.id}, key=${supplier.externalReference}]")
                return supplier
            }
            supplier = createSupplierById(wscontact, createdBy)
            if (supplier) {
                return supplier
            }
        }
        return supplier
    }

    protected final Supplier createSupplierById(WSContact wscontact, Long createdBy) {
        Supplier supplier = null
        Contact contact = createContact(wscontact, (String) null, null)
        if (contact) {
            supplier = suppliers.create(createdBy, contact, wscontact.primaryKey)
            if (supplier) {
                logger.debug("createSupplierById: done [id=${supplier?.id}, name=${supplier?.lastName}]")
            }
        }
        return supplier
    }

    /**
     * Tries to find existing supplier by primaryKey, externalKey and email.
     * @param wscontact
     * @return existing supplier or null
     */
    protected final Supplier findByKeys(WSContact wscontact) {
        if (wscontact.externalKey) {
            Supplier supplier = suppliers.findByExternalReference(wscontact.externalKey)
            if (supplier?.id) {
                logger.debug("findByKeys: supplier found by externalKey [id=${supplier.id}, key=${supplier.externalReference}]")
                return supplier
            }
        }
        if (wscontact.primaryKey) {
            Supplier supplier = suppliers.find(wscontact.primaryKey)
            if (supplier?.id) {
                logger.debug("findByKeys: supplier found by primaryKey [id=${supplier.id}, key=${supplier.externalReference}]")
                return supplier
            }
        }
        if (wscontact.businessEmail) {
            Supplier supplier = suppliers.findByEmail(wscontact.businessEmail)
            if (supplier?.id) {
                logger.debug("findByKeys: supplier found by email [id=${supplier.id}, key=${wscontact.businessEmail}]")
                return supplier
            }
        }
        return null
    }

    protected Supplier update(Supplier supplier, WSContact obj) {
        boolean privateContact = supplier.privatePerson
        Salutation salutation = contactCreator.findSalutation(obj.salutationId, obj.salutation, false)
        Option title = contactCreator.findTitle(obj.titleId, obj.title, false)
        return suppliers.update(
                supplier,
                salutation,
                title,
                (!privateContact ? null : obj.firstName),
                ((!privateContact && obj.company) ? obj.company : obj.lastName),
                obj.street,
                obj.streetAddon,
                obj.zipcode,
                obj.city,
                obj.countryId,
                obj.dateOfBirth,
                obj.grantEmail,
                null,
                obj.grantPhone,
                null,
                obj.grantNon,
                null)
    }
}
