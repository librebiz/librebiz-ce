/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 10, 2007 8:25:21 PM 
 * 
 */
package com.osserp.core.service.events;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.core.dao.Employees;
import com.osserp.core.dao.EventConfigs;
import com.osserp.core.dao.Events;
import com.osserp.core.events.EventTicket;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class EventTicketExecutor extends AbstractEventTicketHandler {
    private static Logger log = LoggerFactory.getLogger(EventTicketExecutor.class.getName());

    protected EventTicketExecutor(
            Events events,
            EventConfigs eventConfigs,
            Employees employees) {
        super(events, eventConfigs, employees);
    }

    public void execute(EventTicket ticket) {
        if (log.isDebugEnabled()) {
            log.debug("execute() invoked [" + ticket.getId() + "]");
        }
        if (ticket.getConfig() != null) {
            if (ticket.isCanceled()) {
                cancelAction(ticket);
            } else {
                executeAction(ticket);
            }
        }
        performTerminations(ticket);
        close(ticket);
        if (log.isDebugEnabled()) {
            log.debug("execute() done [" + ticket.getId() + "]");
        }
    }

    /**
     * Provides all open tickets created longer than given minutes away
     * @param minutesAfterCreated
     * @return openTickets
     */
    public List<EventTicket> findOpenTickets(int minutesAfterCreated) {
        return getEvents().findOpenTickets(minutesAfterCreated);
    }
}
