/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 27, 2007 8:58:16 PM 
 * 
 */
package com.osserp.core.dao;

import java.util.List;

import com.osserp.common.Option;

import com.osserp.core.FcsAction;
import com.osserp.core.FcsConfig;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface RequestFcsActions extends FcsActions {

    /**
     * Creates a new status
     * @param id
     * @param name
     */
    void createStatus(Long id, String name);
    
    /**
     * Provides a status object by primary key
     * @param id
     * @return status object 
     */
    Option getStatus(Long id);

    /**
     * Provides all request status
     * @return requestStatus
     */
    List<Option> getStatusList();
    
    /**
     * Provides a flow control configuration
     * @param type
     * @return config referencing provided type 
     */
    FcsConfig findConfig(Long type);
    
    /**
     * Provides a flow control configuration by primary key
     * @param id
     * @return config 
     */
    FcsConfig getConfig(Long id);

    /**
     * Persists a configuration
     * @param config
     */
    void save(FcsConfig config);

    /**
     * Creates a new flow control action
     * @param name
     * @param description
     * @param groupId
     * @param displayEverytime
     * @param status
     * @return new created action
     */
    FcsAction createAction(String name, String description, Long groupId, boolean displayEverytime, Long status);
}
