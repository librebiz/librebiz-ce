/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 19-Jun-2006 13:36:18 
 * 
 */
package com.osserp.core.finance;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.util.CollectionUtil;
import com.osserp.common.util.NumberUtil;
import com.osserp.common.util.StringUtil;

import com.osserp.core.Item;
import com.osserp.core.products.ProductSelectionConfigItem;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public final class Records {
    private static Logger log = LoggerFactory.getLogger(Records.class.getName());

    public static final String ORDER_BY_NAME = "orderByName";
    public static final String ORDER_BY_NUMBER = "orderByNumber";
    public static final String ORDER_BY_CREATED = "orderByCreated";
    public static final String ORDER_BY_TAXPOINT = "orderByTaxPoint";

    private static final int RECORD_ID_LENGTH = 10;
    private static final int RECORD_ID_LENGTH_EXTERNAL = 11;

    public static final Long PAYMENT_TYPES_MINID = 50L;
    public static final Long PAYMENT_TYPES_MAXID = 100L;

    private static Set<Long> paymentAwareTypes = null;

    /**
     * Indicates if provided billingType is a regular payment (excluding
     * all paymentTypes not relating to an invoice or credit note).
     * @param type
     * @return true if invoice or creditNote relating payment 
     */
    public static boolean isRegularPayment(BillingType type) {
        if (paymentAwareTypes == null) {
            paymentAwareTypes = new HashSet<Long>();
            for (int i = 0, j = BillingType.REGULAR_PAYMENTS.length; i < j; i++) {
                paymentAwareTypes.add(BillingType.REGULAR_PAYMENTS[i]);
            }
        }
        return paymentAwareTypes.contains(type.getId());
    }

    /**
     * Indicates assigned payments
     * @param invoice
     * @return true if payments assigned to invoice
     * @throws IllegalArgumentException if invoice is null or downpayment only
     */
    public static boolean isPaymentAssigned(Invoice invoice) {
        if (!invoice.getPayments().isEmpty()) {
            boolean paymentsAssigned = false;
            for (Iterator<Payment> plist = invoice.getPayments().iterator(); plist.hasNext();) {
                Payment payment = plist.next();
                if (invoice.getId().equals(payment.getRecordId())) {
                    paymentsAssigned = true;
                }
            }
            return paymentsAssigned;
        }
        return false;
    }

    /**
     * Indicates if type is payment including outpayments
     * @param typeId
     * @return true if given type is payment type
     */
    public static boolean isPayment(Long typeId) {
        return (typeId == null || (typeId >= PAYMENT_TYPES_MINID && typeId < PAYMENT_TYPES_MAXID));
    }

    /**
     * Indicates if type is outpayment
     * @param typeId
     * @return true if type is outpayment
     */
    public static boolean isOutpayment(Long typeId) {
        return Payment.OUTPAYMENT.equals(typeId);
    }

    /**
     * Creates a formatted record number string
     * @param type
     * @param id
     * @param internal
     * @return recordNumber
     */
    public static String createNumber(RecordType type, Long id, boolean internal) {
        if (type != null && type.isFormatNumber()) {
            return addNumberPrefix(type, Records.createFormattedNumber(id, internal));
        }
        return (id == null ? "" :
                (internal ? ("I" + addNumberPrefix(type, id.toString())) :
                        addNumberPrefix(type, id.toString())));
    }

    /**
     * Creates a number string for archived documents
     * @param record
     * @param version
     * @return recordNumber
     */
    public static String createNumber(Record record, RecordDocument version) {
        assert record != null && version != null;
        if (version.getVersion() < 1) {
            return addNumberPrefix(record.getType(), record.getId().toString());
        }
        return new StringBuilder(addNumberPrefix(record.getType(), record.getId().toString()))
                .append("-").append(version.getVersion()).toString();
    }

    /**
     * Creates a formatted record reference number string
     * @param type
     * @param reference
     * @param internal
     * @return recordNumber
     */
    public static String createReferenceNumber(RecordType type, Long reference, boolean internal) {
        if (reference == null) {
            return "";
        }
        if (type != null) {
            if (RecordType.PURCHASE_ORDER.equals(type.getId())) {
                return addNumberPrefix(type, reference.toString());
            } else if (type.isFormatNumber()) {
                return Records.createFormattedNumber(reference, internal);
            }
        }
        return (internal ? ("I" + reference.toString()) : reference.toString());
    }

    private static String addNumberPrefix(RecordType type, String idstring) {
        if (type == null || type.getNumberPrefix() == null
                || !type.isAddNumberPrefix()) {
            return idstring;
        }
        return type.getNumberPrefix() + idstring;
    }

    /**
     * Creates record id output, e.g.<br/>
     * 1020601015 as 10206-01015<br/>
     * 40100501001 as 401005-01001 for external numbers
     * @param id
     * @return number string as described or id as string if digits less 10
     */
    private static String createFormattedNumber(Long id) {
        String result = "";
        if (id != null && id != 0) {
            result = id.toString();
            if (result.length() == RECORD_ID_LENGTH) {
                int split = RECORD_ID_LENGTH / 2;
                result = new StringBuilder(result.substring(0, split))
                        .append("-")
                        .append(result.substring(split, RECORD_ID_LENGTH)).toString();
            } else if (result.length() == RECORD_ID_LENGTH_EXTERNAL) {
                int split = RECORD_ID_LENGTH / 2 + 1;
                result = new StringBuilder(result.substring(0, split))
                        .append("-")
                        .append(result.substring(split, RECORD_ID_LENGTH_EXTERNAL)).toString();
            }
        }
        return result;
    }

    /**
     * Creates record id output, e.g.<br/>
     * 1020601015 as 10206-01015<br/>
     * 40100501001 as 401005-01001 for external numbers
     * @param id
     * @param internal
     * @return number string as described above or id as string if digits less 10
     */
    private static String createFormattedNumber(Long id, boolean internal) {
        String result = createFormattedNumber(id);
        return (internal) ? ("I" + result) : result;
    }

    /**
     * Creates a record id by given string, e.g. 10206-01015 as 1020601015
     * @param s
     * @return id
     */
    public static Long createNumber(String s) {
        Long result = 0L;
        if (s != null) {
            if (s.indexOf("-") > -1) {
                StringBuffer buffer = new StringBuffer(s);
                StringUtil.deleteMinus(buffer);
                s = buffer.toString();
            }
            try {
                result = Long.valueOf(s);
            } catch (NumberFormatException nfe) {
            }
        }
        return result;
    }

    public static boolean contains(List<Record> list, Item item) {
        for (int i = 0, j = list.size(); i < j; i++) {
            Record record = list.get(i);
            for (int k = 0, l = record.getItems().size(); k < l; k++) {
                Item next = record.getItems().get(k);
                if (next.getProduct().getProductId().equals(item.getProduct().getProductId())) {
                    return true;
                }
            }
        }
        return false;
    }

    public static BigDecimal getAmountWithBaseTax(List<Item> items) {
        BigDecimal result = new BigDecimal(0);
        for (int i = 0, j = items.size(); i < j; i++) {
            Item next = items.get(i);
            if (next.isIncludePrice()
                    && !NumberUtil.isZeroTax(next.getTaxRate())
                    && !next.getProduct().isReducedTax()) {
                result = result.add(next.getAmount());
            }
        }
        return result;
    }

    public static final BigDecimal getAmountWithReducedTax(List<Item> items) {
        BigDecimal result = new BigDecimal(0);
        for (int i = 0, j = items.size(); i < j; i++) {
            Item next = items.get(i);
            if (next.isIncludePrice() && next.getProduct().isReducedTax()) {
                result = result.add(next.getAmount());
            }
        }
        return result;
    }

    public static BigDecimal getAmountWithZeroTax(List<Item> items) {
        BigDecimal result = new BigDecimal(0);
        for (int i = 0, j = items.size(); i < j; i++) {
            Item next = items.get(i);
            if (next.isIncludePrice()
                    && NumberUtil.isZeroTax(next.getTaxRate())) {
                result = result.add(next.getAmount());
            }
        }
        return result;
    }

    /**
     * Filters records by productSelectionItems
     * @param productSelections
     * @param list
     * @return filtered list or unfiltered list if productSelections is null or empty
     */
    public static List<RecordDisplay> filterByRecordDisplay(
            List<ProductSelectionConfigItem> productSelections,
            List<RecordDisplay> list) {

        List<RecordDisplay> result = new ArrayList<RecordDisplay>(list);
        if (log.isDebugEnabled()) {
            log.debug("filter() invoked [filterCount="
                    + (productSelections == null ? "null" : productSelections.size())
                    + ", recordCount=" + result.size() + "]");
        }
        if (productSelections != null && !productSelections.isEmpty()) {
            for (Iterator<RecordDisplay> i = result.iterator(); i.hasNext();) {
                RecordDisplay next = i.next();
                if (!next.matching(productSelections)) {
                    if (log.isDebugEnabled()) {
                        log.debug("filter() removing not matching record [id=" + next.getId() + "]");
                    }
                    i.remove();
                }
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("filter() done [resultCount=" + result.size() + "]");
        }
        sortDisplay(result, ORDER_BY_CREATED, true);
        return result;
    }

    /**
     * Filters records by productSelectionItems
     * @param productSelections
     * @param list
     * @return filtered list or unfiltered list if productSelections is null or empty
     */
    public static List<? extends Record> filterByRecord(
            List<ProductSelectionConfigItem> productSelections,
            List<? extends Record> list) {

        List<Record> result = new ArrayList<Record>(list);
        if (log.isDebugEnabled()) {
            log.debug("filter() invoked [filterCount="
                    + (productSelections == null ? "null" : productSelections.size())
                    + ", recordCount=" + result.size() + "]");
        }
        if (productSelections != null && !productSelections.isEmpty()) {
            for (Iterator<Record> i = result.iterator(); i.hasNext();) {
                Record next = i.next();
                if (!next.matching(productSelections)) {
                    if (log.isDebugEnabled()) {
                        log.debug("filter() removing not matching record [id=" + next.getId() + "]");
                    }
                    i.remove();
                }
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("filter() done [resultCount=" + result.size() + "]");
        }
        sort(result, ORDER_BY_CREATED, true);
        return result;
    }

    public static void sortSummaryByCreated(List<? extends RecordSummary> sortables, boolean reverse) {
        CollectionUtil.sort(sortables, createSummaryByCreatedComparator(reverse));
    }

    private static Comparator createSummaryByCreatedComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    Date dateA = ((RecordSummary) a).getCreated();
                    Date dateB = ((RecordSummary) b).getCreated();
                    int result = dateA.compareTo(dateB);
                    if (result != 0) {
                        return result * (-1);
                    }
                    return result;
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                Date dateA = ((RecordSummary) a).getCreated();
                Date dateB = ((RecordSummary) b).getCreated();
                return dateA.compareTo(dateB);
            }
        };
    }

    public static void sort(List<? extends FinanceRecord> sortables, String method, boolean reverse) {
        if (method != null) {
            if (ORDER_BY_NAME.equals(method)) {
                CollectionUtil.sort(sortables, createNameComparator(reverse));
            } else if (ORDER_BY_NUMBER.equals(method)) {
                CollectionUtil.sort(sortables, createNumberComparator(reverse));
            } else if (ORDER_BY_CREATED.equals(method)) {
                CollectionUtil.sort(sortables, createCreatedComparator(reverse));
            } else if (ORDER_BY_TAXPOINT.equals(method)) {
                CollectionUtil.sort(sortables, createTaxPointComparator(reverse));
            }
        }
    }

    public static void sortDisplay(List<? extends FinanceRecordDisplay> sortables, String method, boolean reverse) {
        if (method != null) {
            if (ORDER_BY_NAME.equals(method)) {
                CollectionUtil.sort(sortables, createNameComparator(reverse));
            } else if (ORDER_BY_NUMBER.equals(method)) {
                CollectionUtil.sort(sortables, createNumberDisplayComparator(reverse));
            } else if (ORDER_BY_CREATED.equals(method)) {
                CollectionUtil.sort(sortables, createCreatedDisplayComparator(reverse));
            } else if (ORDER_BY_TAXPOINT.equals(method)) {
                CollectionUtil.sort(sortables, createTaxPointDisplayComparator(reverse));
            }
        }
    }

    private static Comparator createCreatedComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    Date dateA = ((FinanceRecord) a).getCreated();
                    Date dateB = ((FinanceRecord) b).getCreated();
                    int result = dateA.compareTo(dateB);
                    if (result != 0) {
                        return result * (-1);
                    }
                    return result;
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                Date dateA = ((FinanceRecord) a).getCreated();
                Date dateB = ((FinanceRecord) b).getCreated();
                return dateA.compareTo(dateB);
            }
        };
    }

    private static Comparator createCreatedDisplayComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    Date dateA = ((FinanceRecordDisplay) a).getCreated();
                    Date dateB = ((FinanceRecordDisplay) b).getCreated();
                    int result = dateA.compareTo(dateB);
                    if (result != 0) {
                        return result * (-1);
                    }
                    return result;
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                Date dateA = ((FinanceRecordDisplay) a).getCreated();
                Date dateB = ((FinanceRecordDisplay) b).getCreated();
                return dateA.compareTo(dateB);
            }
        };
    }

    private static Comparator createNumberComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    int result = ((FinanceRecord) a).getId().compareTo(((FinanceRecord) b).getId());
                    if (result != 0) {
                        return result * (-1);
                    }
                    return result;
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                return ((FinanceRecord) a).getId().compareTo(((FinanceRecord) b).getId());
            }
        };
    }

    private static Comparator createNumberDisplayComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    int result = ((FinanceRecordDisplay) a).getId().compareTo(((FinanceRecordDisplay) b).getId());
                    if (result != 0) {
                        return result * (-1);
                    }
                    return result;
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                return ((FinanceRecordDisplay) a).getId().compareTo(((FinanceRecordDisplay) b).getId());
            }
        };
    }

    private static Comparator createNameComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    int result = getName(a).compareTo(getName(b));
                    if (result != 0) {
                        return result * (-1);
                    }
                    return result;
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                return getName(a).compareTo(getName(b));
            }
        };
    }

    private static String getName(Object obj) {
        if (obj != null && obj instanceof FinanceRecord) {
            FinanceRecord record = (FinanceRecord) obj;
            if (record.getContact() != null) {
                String name = record.getContact().getDisplayName();
                return (name == null ? "" : name);
            }
        } else if (obj != null && obj instanceof FinanceRecordDisplay) {
            FinanceRecordDisplay record = (FinanceRecordDisplay) obj;
            if (record.getContactName() != null) {
                String name = record.getContactName();
                return (name == null ? "" : name);
            }
        }
        return "";
    }

    private static Comparator createTaxPointComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    Date dateA = ((FinanceRecord) a).getTaxPointDate();
                    Date dateB = ((FinanceRecord) b).getTaxPointDate();
                    int result = dateA.compareTo(dateB);
                    if (result != 0) {
                        return result * (-1);
                    }
                    return result;
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                Date dateA = ((FinanceRecord) a).getTaxPointDate();
                Date dateB = ((FinanceRecord) b).getTaxPointDate();
                return dateA.compareTo(dateB);
            }
        };
    }

    private static Comparator createTaxPointDisplayComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    Date dateA = ((FinanceRecordDisplay) a).getTaxPoint();
                    Date dateB = ((FinanceRecordDisplay) b).getTaxPoint();
                    int result = dateA.compareTo(dateB);
                    if (result != 0) {
                        return result * (-1);
                    }
                    return result;
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                Date dateA = ((FinanceRecordDisplay) a).getTaxPoint();
                Date dateB = ((FinanceRecordDisplay) b).getTaxPoint();
                return dateA.compareTo(dateB);
            }
        };
    }

}
