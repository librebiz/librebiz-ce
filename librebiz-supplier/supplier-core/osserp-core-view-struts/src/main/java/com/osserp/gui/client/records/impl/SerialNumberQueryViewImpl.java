/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 5, 2008 11:29:27 PM 
 * 
 */
package com.osserp.gui.client.records.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Actions;
import com.osserp.common.web.Form;
import com.osserp.common.web.ViewName;

import com.osserp.core.finance.SerialNumberDisplay;
import com.osserp.core.finance.SerialNumberManager;

import com.osserp.gui.client.impl.AbstractView;
import com.osserp.gui.client.records.SerialNumberQueryView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("serialNumberQueryView")
public class SerialNumberQueryViewImpl extends AbstractView
        implements SerialNumberQueryView {
    private static Logger log = LoggerFactory.getLogger(SerialNumberQueryViewImpl.class.getName());
    private String lastSearchPattern = null;
    private boolean lastStartsWith = false;

    protected SerialNumberQueryViewImpl() {
        super();
        setListTarget(Actions.LIST);
    }

    public void find(Form form) {
        this.lastSearchPattern = form.getString(Form.VALUE);
        this.lastStartsWith = form.getBoolean(Form.STARTS_WITH);
        find(lastSearchPattern, lastStartsWith);
    }

    @Override
    public void setSelection(Long id) {
        super.setSelection(id);
        try {
            if (id == null) {
                setEditMode(false);
            } else {
                setEditMode(true);
            }
        } catch (Exception ignorable) {
            // should not happen
        }
    }

    @Override
    public void save(Form form) throws ClientException, PermissionException {
        SerialNumberDisplay obj = (SerialNumberDisplay) getBean();
        if (obj == null) {
            log.warn("save() invoked when no serial number selected!");
            throw new ActionException();
        }
        String value = form.getString(Form.VALUE);
        if (log.isDebugEnabled()) {
            log.debug("save() invoked [id="
                    + obj.getId()
                    + ",sales=" + obj.isSales()
                    + ",value=" + value + "]");
        }
        SerialNumberManager manager = (SerialNumberManager) getService(SerialNumberManager.class.getName());
        manager.changeSerialNumber(obj, value);
        setEditMode(false);
        setBean(null);
        find(lastSearchPattern, lastStartsWith);
    }

    private void find(String pattern, boolean startsWith) {
        SerialNumberManager manager = (SerialNumberManager) getService(SerialNumberManager.class.getName());
        setList(manager.findSerialNumbers(pattern, startsWith));
    }
}
