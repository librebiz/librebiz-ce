/**
 *
 * Copyright (C) 2017 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 1, 2017 
 * 
 */
package com.osserp.core.api.defaults

import com.osserp.api.WSObjectException
import com.osserp.api.WSProduct

import com.osserp.common.util.StringUtil

import com.osserp.core.api.CoreProductMapper
import com.osserp.core.api.ProductImportProcessor
import com.osserp.core.api.ProductImportVO
import com.osserp.core.dao.Products
import com.osserp.core.products.Product
import com.osserp.core.products.ProductClassificationConfig


/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class CoreProductImportProcessor implements ProductImportProcessor {
    
    Products products
    CoreProductMapper coreProductMapper

    CoreProductImportProcessor(Products products, CoreProductMapper coreProductMapper) {
        this.products = products
        this.coreProductMapper = coreProductMapper
    }

    Product createOrUpdate(WSProduct wsProduct, boolean performActions) throws WSObjectException {
        ProductClassificationConfig productConfig = coreProductMapper.fetchConfig(wsProduct)
        if (performActions) {
            // performActions => create classification only and exit
            return null
        }
        Product existing
        String matchcode = fetchMatchcode(wsProduct)
        Long productId = wsProduct.primaryKey
        if (productId) {
            existing = products.find(productId)
        }
        if (!existing) {
            existing = products.findExisting(wsProduct.name)
        }
        if (existing) {
            if (!productId) {
                wsProduct.primaryKey = existing.productId
            }
            return update(wsProduct, productConfig)
        }
        if (!productId) {
            productId = products.createIdSuggestion(productConfig) 
        }
        Product created = products.create(
            null, // employee
            productId, 
            productConfig, 
            matchcode,
            wsProduct.name,
            wsProduct.description,
            coreProductMapper.fetchQuantityUnit(wsProduct.quantityUnit),
            1, // packaging unit not supported
            coreProductMapper.fetchManufacturer(
                wsProduct.manufacturer, productConfig.group.id),
            null, // planningMode
            null) // source product
        wsProduct.primaryKey = created.productId
        return update(wsProduct, productConfig) ?: created
    }

    private Product update(WSProduct wsProduct, ProductClassificationConfig productConfig) {
        Product product = coreProductMapper.map(wsProduct)
        if (product) {
            products.save(product)
            if (!product.isType(productConfig.type)) {
                product = products.updateClassification(product,
                    productConfig.type, productConfig.group, productConfig.category)
            }
        } 
        return product
    }

    private fetchMatchcode(WSProduct wsProduct) {
        if (wsProduct instanceof ProductImportVO) {
            ProductImportVO vo = wsProduct
            return vo.matchcode
        }
        return StringUtil.cut(wsProduct.name, 127, false)
    }

}
