<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:set var="timeRecordingView" scope="request" value="${sessionScope.timeRecordingView}"/>
<c:set var="popup" scope="request" value="true"/>

<div class="modalBoxHeader">
	<div class="modalBoxHeaderLeft">
		<fmt:message key="summaryLabel"/>
	</div>
</div>
<div class="modalBoxData">
    <div class="col-md-12 panel-area panel-area-default">
        <div class="row">
            <c:import url="/pages/hrm/shared/time_recording_summary.jsp"/>
        </div>
    </div>
</div>
