/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 29, 2010 11:10:06 PM 
 * 
 */
package com.osserp.common.util;

import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Test;

import com.osserp.common.Calendar;
import com.osserp.common.CalendarMonth;
import com.osserp.common.beans.CalendarImpl;

/**
 * @author Rainer Kirchner <rk@osserp.com>
 */
public class UDateUtil {

    @Test
    public void lastDayOfMonth() {
        CalendarMonth month = DateUtil.createMonth(1, 2009);
        Calendar day = new CalendarImpl(month.getDaysOfMonthCount(), 1, 2009);
        assertTrue(DateFormatter.getDate(month.getDate()).equals("01.02.2009"));
        assertTrue(DateFormatter.getDate(day.getDate()).equals("28.02.2009"));

        month = DateUtil.createMonth(1, 2008);
        day = new CalendarImpl(month.getDaysOfMonthCount(), 1, 2008);
        assertTrue(DateFormatter.getDate(month.getDate()).equals("01.02.2008"));
        assertTrue(DateFormatter.getDate(day.getDate()).equals("29.02.2008"));
    }

    @Test
    public void createComparisonDate() {
        String testDate = "22.02.2012 12:12:12";
        String result = null;
        try {
            Date date = DateUtil.createDate(testDate, true);
            result = DateFormatter.getComparisonDate(date);
        } catch (Exception e) {
            // 
        }
        assertTrue("2012-02-22".equals(result));
    }

    @Test
    public void createDate() {
        String testDate = "22.02.2012 12:12:12";
        boolean result = false;
        try {
            Date date = DateUtil.createDate(testDate, true);
            result = (date != null);
        } catch (Exception e) {
            // 
        }
        assertTrue(result);
    }

    @Test
    public void durationDecimalHour() {
        String start = "22.02.2012 12:12:12";
        String end = "22.02.2012 13:12:12";
        try {
            Date startDate = DateUtil.createDate(start, true);
            Date endDate = DateUtil.createDate(end, true);
            Double result = DateUtil.getDurationHours(startDate, endDate);
            assertTrue(Double.valueOf(1.0).equals(result));
        } catch (Exception e) {
            // 
        }
    }

    @Test
    public void durationDecimalQuarter() {
        String start = "22.02.2012 12:12:12";
        String end = "22.02.2012 12:57:12";
        try {
            Date startDate = DateUtil.createDate(start, true);
            Date endDate = DateUtil.createDate(end, true);
            Double result = DateUtil.getDurationHours(startDate, endDate);
            assertTrue(Double.valueOf(0.75).equals(result));
        } catch (Exception e) {
            // 
        }
    }
    
    @Test
    public void getTimeBackConfig() {
        String[] result = DateUtil.getTimeIntervalConfig("D356");
        assertTrue("D".equals(result[0]));
        Integer i = NumberUtil.createInteger(result[1]);
        assertTrue(i != null);
        assertTrue(i.intValue() == 356);
    }
    
    @Test
    public void getTimeBackConfigDefault() {
        String[] result = DateUtil.getTimeIntervalConfig(null);
        assertTrue("Y".equals(result[0]));
        Integer i = NumberUtil.createInteger(result[1]);
        assertTrue(i != null);
        assertTrue(i.intValue() == 1);
    }
}
