/**
 *
 * Copyright (C) 2010, 2017 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License (AGPL)
 * version 3 as published by the Free Software Foundation. In accordance
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code
 * versions must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area
 * or, if not reasonably feasible for technical reason, as a top-level
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute.
 *
 */
package com.osserp.common.service

import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Executes commands on a unix-like shell. Method execution was inspired by
 * Joerg Mueller's excellent blog post
 * http://www.joergm.com/2010/09/executing-shell-commands-in-groovy/
 *
 * Modfied return types, added param to allow silent script execution.
 */
class UnixShell {
    
    private static Logger logger = LoggerFactory.getLogger(UnixShell.class.getName())
    

    static int execute(String command, boolean verbose) {
        return executeOnShell(command,
            new File(System.properties.'user.dir'), verbose)
    }

    private static int executeOnShell(String command, File workingDir, boolean verbose) {
        if (verbose) {
            println command
        }
        def process = new ProcessBuilder(addShellPrefix(command))
                .directory(workingDir)
                .redirectErrorStream(true)
                .start()
        if (verbose) {
            process.inputStream.eachLine { println it }
        } else {
            process.inputStream.eachLine { logger.debug(it) }
        }
        process.waitFor();
        return process.exitValue()
    }

    private static def addShellPrefix(String command) {
        def commandArray = new String[3]
        commandArray[0] = "sh"
        commandArray[1] = "-c"
        commandArray[2] = command
        return commandArray
    }

}
