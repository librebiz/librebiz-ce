/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 23, 2008 11:51:06 AM 
 * 
 */
package com.osserp.gui.client.contacts.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.Option;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;
import com.osserp.common.web.ViewName;
import com.osserp.core.NoteAware;
import com.osserp.core.Options;
import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.ContactManager;
import com.osserp.core.contacts.ContactNoteManager;
import com.osserp.core.contacts.ContactSearch;
import com.osserp.core.contacts.ContactType;
import com.osserp.core.contacts.Interest;
import com.osserp.core.contacts.InterestManager;
import com.osserp.core.contacts.Salutation;
import com.osserp.gui.client.contacts.InterestView;
import com.osserp.gui.client.impl.AbstractView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("interestView")
public class InterestViewImpl extends AbstractView implements InterestView {
    private static Logger log = LoggerFactory.getLogger(InterestViewImpl.class.getName());

    private List<Contact> existingContacts = new ArrayList<Contact>();

    protected InterestViewImpl() {
        super();
    }

    @Override
    public void init(HttpServletRequest request)
            throws ClientException,
            PermissionException {
        super.init(request);
        InterestManager manager = getInterestManager();
        setList(manager.findOpen());
    }

    public List<Contact> getExistingContacts() {
        return existingContacts;
    }

    public Contact closeByCreateContact(Long contactId) throws ClientException {
        Interest interest = getInterest();
        interest.setReference(contactId);
        remove(interest, Interest.STATUS_TRANSFERED);
        return (Contact) getContactManager().find(contactId);
    }

    public Contact closeByExistingContact(Long contactId) {
        Interest interest = getInterest();
        ContactManager manager = getContactManager();
        Contact selected = (Contact) manager.find(contactId);
        StringBuilder note = new StringBuilder();
        if (isSet(interest.getComment())) {
            note.append(interest.getComment());
        }
        if (isSet(interest.getDescription())) {
            if (note.length() > 0) {
                note.append("\n\n");
            }
            note.append("Info:\n").append(interest.getDescription());
        }
        if (note.length() > 0) {
            try {
                ContactNoteManager noteManager = (ContactNoteManager) getService(ContactNoteManager.class.getName());
                NoteAware noteAware = noteManager.create(selected);
                noteManager.addNote(getDomainEmployee(), noteAware, null, note.toString());
            } catch (ClientException e) {
                log.warn("closeByExistingContact() ignoring exception on add external note [message=" + e.getMessage() + "]", e);
            }
        }
        interest.setReference(selected.getContactId());
        this.remove(interest, Interest.STATUS_EXISTING);
        return selected;
    }

    public void deleteCurrent() {
        Interest interest = getInterest();
        this.remove(interest, Interest.STATUS_DELETED);
    }

    @Override
    public void save(Form form) throws ClientException, PermissionException {
        Interest interest = getInterest();
        if (log.isDebugEnabled()) {
            log.debug("save() invoked [interest=" + interest.getId() + "]");
        }
        Long type = form.getLong(Form.TYPE);
        ContactType contactType = null;
        if (isSet(type)) {
            contactType = fetchContactType(type);
            interest.setType(contactType);
        }
        interest.setCompany(form.getString(Form.COMPANY));
        if (isSet(interest.getCompany())) {
            contactType = fetchContactType(Interest.TYPE_BUSINESS);
            interest.setType(contactType);
        } else {
            contactType = fetchContactType(Interest.TYPE_PRIVATE);
            interest.setType(contactType);
        }
        interest.setSalutation(fetchSalutation(form.getLong(Form.SALUTATION)));
        interest.setTitle(fetchTitle(form.getLong(Form.TITLE)));
        String comment = form.getString(Form.NOTE);
        if (isSet(comment)) {
            StringBuilder cb = new StringBuilder();
            if (isNotSet(interest.getComment())) {
                cb.append(getDomainEmployee().getDisplayName()).append(": ");
            }
            cb.append(comment);
            interest.setComment(cb.toString());
        }
        interest.setFirstName(form.getString(Form.FIRSTNAME));
        interest.setLastName(form.getString(Form.LASTNAME));
        interest.getAddress().setStreet(form.getString(Form.STREET));
        interest.getAddress().setZipcode(form.getString(Form.ZIPCODE));
        interest.getAddress().setCity(form.getString(Form.CITY));
        String email = form.getString(Form.EMAIL);
        if (isSet(email)) {
            interest.setEmail(email);
        }
        if (interest.getPhone() == null) {
            interest.addPhone(
                    form.getString(Form.PHONE_COUNTRY),
                    form.getString(Form.PHONE_PREFIX),
                    form.getString(Form.PHONE_NUMBER));
        } else {
            interest.getPhone().setCountry(form.getString(Form.PHONE_COUNTRY));
            interest.getPhone().setPrefix(form.getString(Form.PHONE_PREFIX));
            interest.getPhone().setNumber(form.getString(Form.PHONE_NUMBER));
        }

        if (interest.getFax() == null) {
            interest.addFax(
                    form.getString(Form.FAX_COUNTRY),
                    form.getString(Form.FAX_PREFIX),
                    form.getString(Form.FAX_NUMBER));
        } else {
            interest.getFax().setCountry(form.getString(Form.FAX_COUNTRY));
            interest.getFax().setPrefix(form.getString(Form.FAX_PREFIX));
            interest.getFax().setNumber(form.getString(Form.FAX_NUMBER));
        }

        if (interest.getMobile() == null) {
            interest.addMobile(
                    form.getString(Form.MOBILE_COUNTRY),
                    form.getString(Form.MOBILE_PREFIX),
                    form.getString(Form.MOBILE_NUMBER));
        } else {
            interest.getMobile().setCountry(form.getString(Form.MOBILE_COUNTRY));
            interest.getMobile().setPrefix(form.getString(Form.MOBILE_PREFIX));
            interest.getMobile().setNumber(form.getString(Form.MOBILE_NUMBER));
        }

        InterestManager manager = getInterestManager();
        manager.save(interest);
        setBean(manager.find(interest.getId()));
        findSimilarContacts();
        disableEditMode();
    }

    @Override
    public void setSelection(Long id) {
        super.setSelection(id);
        if (id != null) {
            findSimilarContacts();
        }
    }

    private InterestManager getInterestManager() {
        return (InterestManager) getService(InterestManager.class.getName());
    }

    public Interest getInterest() {
        Interest interest = (Interest) getBean();
        if (interest == null) {
            log.warn("getInterest() failed: No interest selected");
            throw new ActionException("interest not selected");
        }
        return interest;
    }

    private Option fetchTitle(Long id) {
        return getOption(Options.TITLES, id);
    }

    private Salutation fetchSalutation(Long id) {
        return (Salutation) getOption(Options.SALUTATIONS, id);
    }

    private ContactType fetchContactType(Long id) {
        return (ContactType) getOption(Options.CONTACT_TYPES, id);
    }

    private void findSimilarContacts() {
        existingContacts.clear();
        Interest interest = getInterest();
        ContactSearch search = (ContactSearch) getService(ContactSearch.class.getName());
        existingContacts = search.findExisting(interest);
        if (log.isDebugEnabled()) {
            log.debug("findSimilarContacts() done [count=" + existingContacts.size() + "]");
        }
    }

    private void remove(Interest interest, Long removingStatus) {
        interest.setStatus(removingStatus);
        InterestManager manager = getInterestManager();
        manager.save(interest);
        for (Iterator<Interest> i = getList().iterator(); i.hasNext();) {
            Interest next = i.next();
            if (next.getId().equals(interest.getId())) {
                i.remove();
            }
        }
        setBean(null);
    }
    
    private ContactManager getContactManager() {
        return (ContactManager) getService(ContactManager.class.getName());
    }
}
