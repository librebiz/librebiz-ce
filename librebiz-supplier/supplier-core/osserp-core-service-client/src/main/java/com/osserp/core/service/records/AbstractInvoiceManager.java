/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 02-Feb-2007 09:04:41 
 * 
 */
package com.osserp.core.service.records;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.core.dao.PaymentConditions;
import com.osserp.core.dao.Products;
import com.osserp.core.dao.records.Records;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.InvoiceManager;
import com.osserp.core.finance.Order;
import com.osserp.core.system.SystemConfigManager;
import com.osserp.core.tasks.ProductPlanningCacheSender;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractInvoiceManager extends AbstractPaymentAwareRecordManager
        implements InvoiceManager {
    private static Logger log = LoggerFactory.getLogger(AbstractInvoiceManager.class.getName());
    private PaymentConditions paymentConditions = null;

    public AbstractInvoiceManager(
            Records records,
            SystemConfigManager systemConfigManager,
            Products products,
            ProductPlanningCacheSender productPlanningTask,
            PaymentConditions paymentConditions) {
        super(records, systemConfigManager, products, productPlanningTask);
        this.paymentConditions = paymentConditions;
    }

    public List<Invoice> find(Order order) {
        List records = getDao().getByReference(order.getId());
        List<Invoice> result = new ArrayList<Invoice>();
        for (int i = 0, j = records.size(); i < j; i++) {
            result.add((Invoice) records.get(i));
        }
        return result;
    }

    public void updatePaymentCondition(Invoice invoice, Long condition) {
        if (log.isDebugEnabled()) {
            log.debug("updatePaymentCondition() invoked for invoice "
                    + invoice.getId() + ", condition " + condition);
        }
        invoice.setPaymentCondition(paymentConditions.load(condition));
        getDao().save(invoice);
    }

	public void updateTaxPoint(
	        Invoice invoice, Date taxPoint, 
	        String timeOfSupply, String placeOfPerformance,
			boolean printTaxPoint, boolean printTaxPointByItems, 
			boolean printTimeOfSupply, boolean printPlaceOfPerformance) {
		invoice.setPrintTaxPoint(printTaxPoint);
		invoice.setPrintTaxPointByItems(printTaxPointByItems);
		invoice.setPrintTimeOfSupply(printTimeOfSupply);
		invoice.setPrintPlaceOfPerformance(printPlaceOfPerformance);
		invoice.setTaxPoint(taxPoint);
		invoice.setTimeOfSupply(timeOfSupply);
		invoice.setPlaceOfPerformance(placeOfPerformance);
        getDao().save(invoice);
	}

}
