/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 23, 2008 8:12:17 PM 
 * 
 */
package com.osserp.core.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.osserp.common.PublicHoliday;
import com.osserp.common.beans.AbstractOption;
import com.osserp.common.util.NumberUtil;
import com.osserp.common.util.StringUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PublicHolidayImpl extends AbstractOption implements PublicHoliday {

    private Long country = null;
    private Long state = null;
    private Date date = null;
    private String ignoreBranchList = null;

    protected PublicHolidayImpl() {
        super();
    }

    public Long getCountry() {
        return country;
    }

    protected void setCountry(Long country) {
        this.country = country;
    }

    public Long getState() {
        return state;
    }

    protected void setState(Long state) {
        this.state = state;
    }

    public Date getDate() {
        return date;
    }

    protected void setDate(Date date) {
        this.date = date;
    }

    public Set<Long> getIgnorableBranchs() {
        Set<Long> result = new HashSet<Long>();
        if (ignoreBranchList != null) {
            String[] array = StringUtil.getTokenArray(ignoreBranchList, StringUtil.COMMA);
            if (array != null && array.length > 0) {
                for (int i = 0, j = array.length; i < j; i++) {
                    result.add(NumberUtil.createLong(array[i]));
                }
            }
        }
        return result;
    }

    protected String getIgnoreBranchList() {
        return ignoreBranchList;
    }

    protected void setIgnoreBranchList(String ignoreBranchList) {
        this.ignoreBranchList = ignoreBranchList;
    }
}
