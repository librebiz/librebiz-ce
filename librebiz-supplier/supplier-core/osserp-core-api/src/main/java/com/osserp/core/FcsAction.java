/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 26, 2007 1:48:19 PM 
 * 
 */
package com.osserp.core;

import java.util.Set;

import com.osserp.common.Option;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface FcsAction extends Option {

    /**
     * Returns the group id indicating the responsibility for this action
     * @return groupId.
     */
    Long getGroupId();

    /**
     * Sets the group id indicating the responsibility for this action
     * @param groupId.
     */
    void setGroupId(Long groupId);

    /**
     * Returns the status
     * @return status
     */
    Integer getStatus();

    /**
     * Sets the status
     * @param status
     */
    void setStatus(Integer status);

    /**
     * Indicates that this action should be displayed everytime even the action was added before
     * @return displayEverytime
     */
    boolean isDisplayEverytime();

    /**
     * Sets that this action should be displayed everytime even the action was added before
     * @param displayEverytime
     */
    void setDisplayEverytime(boolean displayEverytime);

    /**
     * Indicates that actions of this type are throwable (to wastebasket)
     * @return true if so
     */
    boolean isThrowable();

    /**
     * Indicates that a note is required when using this action
     * @return noteRequired
     */
    boolean isNoteRequired();

    /**
     * Sets that a note is required when using this action
     * @param noteRequired
     */
    void setNoteRequired(boolean noteRequired);

    /**
     * Indicates that additional action(s) should be performed before and/or after executing this fcs action
     * @return performAction
     */
    boolean isPerformAction();

    /**
     * Sets that additional action(s) should be performed before and/or after executing this fcs action
     * @param performAction
     */
    void setPerformAction(boolean performAction);

    /**
     * Indicates that an additional action should be performed before executing fcs action's note
     * @return performOnStart
     */
    boolean isPerformOnStart();

    /**
     * Sets that an additional action should be performed before executing fcs action's note
     * @param performOnStart
     */
    void setPerformOnStart(boolean performOnStart);

    /**
     * The name of the action to be performed on start
     * @return startActionName
     */
    String getStartActionName();

    /**
     * Sets the name of the action to be performed on start
     * @param startActionName
     */
    void setStartActionName(String startActionName);

    /**
     * Indicates that an additional action should be performed after executing fcs action's note
     * @return performAtEnd
     */
    boolean isPerformAtEnd();

    /**
     * Sets that an additional action should be performed after executing fcs action's note
     * @param performAtEnd
     */
    void setPerformAtEnd(boolean performAtEnd);

    /**
     * The name of the action to be performed at end
     * @return endActionName
     */
    String getEndActionName();

    /**
     * Sets the name of the action to be performed at end
     * @param endActionName
     */
    void setEndActionName(String endActionName);

    /**
     * Indicates that event(s) should pe performed when this action was invoked
     * @return performEvents
     */
    boolean isPerformEvents();

    /**
     * Provides the responsible event config id
     * @return eventConfigId
     */
    Long getEventConfigId();

    /**
     * Indicates that this action closes a business transaction
     * @return isClosing
     */
    boolean isClosing();

    /**
     * Allows to close businessCase without checks
     * @return true if enabled
     */
    boolean isClosingUnconditionally();

    /**
     * Indicates that this action cancels a business transaction
     * @return isCancelling
     */
    boolean isCancelling();

    /**
     * Sets that this action cancels a business transaction
     * @param cancelling
     */
    void setCancelling(boolean sendSales);

    /**
     * Indicates that this action stops the business transaction temporarly
     * @return isStopping
     */
    boolean isStopping();

    /**
     * Sets that this action stops a business transaction
     * @param stopping
     */
    void setStopping(boolean stopping);

    /**
     * Indicates that the fcs should be displayed reverse (e.g. note as name and vice versa)
     * @return true if so
     */
    boolean isDisplayReverse();

    /**
     * Sets display reverse
     * @param displayReverse
     */
    void setDisplayReverse(boolean displayReverse);

    /**
     * The action this action depends on
     * @return dependencies
     */
    Set<Long> getDependencies();

    /**
     * Adds a dependecy
     * @param dependsOn
     */
    void addDependency(Long dependsOn);

    /**
     * Removes a dependency
     * @param dependency
     */
    void removeDependency(Long dependency);

    /**
     * Indicates that action is triggered
     * @return triggered
     */
    boolean isTriggered();

    /**
     * Indicates that action is triggered and not manual selectable
     * @return manualSelectionDisabled
     */
    boolean isManualSelectionDisabled();

    /**
     * Indicates dedicated import action
     * @return true if import action
     */
    boolean isImportAction();

    /**
     * Provides a comma separared list of permissions required for action
     * @return permissions or null if no special permission required
     */
    String getPermissions();
}
