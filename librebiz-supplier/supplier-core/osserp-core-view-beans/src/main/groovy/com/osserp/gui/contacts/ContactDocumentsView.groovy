/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.osserp.gui.contacts

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.dms.DmsReference
import com.osserp.core.dms.CoreDocumentType
import com.osserp.groovy.web.ViewContextException
import com.osserp.gui.dms.AbstractDocumentView

/**
 * 
 * @author cf <cf@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public class ContactDocumentsView extends AbstractDocumentView {
    private static Logger logger = LoggerFactory.getLogger(ContactDocumentsView.class.getName())

    def contact

    ContactDocumentsView() {
        super()
        referenceSupport = false
        dependencies = ['contactView']
    }

    @Override
    protected DmsReference getDmsReference() {
        new DmsReference(CoreDocumentType.CONTACT_DOCUMENTS, contact.contactId)
    }


    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (!list) {
            if (!env.contactView?.bean) {
                logger.warn("initRequest: not bound [user=${user?.id}, name=contactView]")
                throw new ViewContextException(this)
            }
            contact = env.contactView?.bean
            reload()
        }
    }
}
