/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 7, 2006 2:41:19 PM 
 * 
 */
package com.osserp.core.model;

import java.util.Date;

import com.osserp.common.beans.AbstractEntity;
import com.osserp.common.util.NumberUtil;

import com.osserp.core.finance.PaymentAgreement;
import com.osserp.core.finance.PaymentCondition;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PaymentAgreementImpl extends AbstractEntity
        implements PaymentAgreement {
    protected Double downpaymentPercent;
    protected Double deliveryInvoicePercent;
    protected Double finalInvoicePercent;
    protected Long downpaymentTargetId;
    protected Long deliveryInvoiceTargetId;
    protected Long finalInvoiceTargetId;
    protected PaymentCondition paymentCondition;
    private boolean hidePayments = true;
    private Long paymentMethodId;
    private Long billingAccountId;

    protected String note;

    protected PaymentAgreementImpl() {
        super();
    }

    protected PaymentAgreementImpl(PaymentAgreement other) {
        super(other);
        this.deliveryInvoicePercent = other.getDeliveryInvoicePercent();
        this.deliveryInvoiceTargetId = other.getDeliveryInvoiceTargetId();
        this.downpaymentPercent = other.getDownpaymentPercent();
        this.downpaymentTargetId = other.getDownpaymentTargetId();
        this.finalInvoicePercent = other.getFinalInvoicePercent();
        this.finalInvoiceTargetId = other.getFinalInvoiceTargetId();
        this.note = other.getNote();
        this.hidePayments = other.isHidePayments();
        this.paymentCondition = other.getPaymentCondition();
    }

    protected PaymentAgreementImpl(Long reference) {
        super(null, reference);
    }

    protected PaymentAgreementImpl(Long reference, Long createdBy) {
        super(null, reference, createdBy);
    }

    protected PaymentAgreementImpl(Long reference, Long createdBy, PaymentCondition paymentCondition) {
        super(null, reference, createdBy);
        this.paymentCondition = paymentCondition;
    }

    public PaymentAgreementImpl(
            Long id,
            Long referenceId,
            Double downpaymentPercent,
            Double deliveryInvoicePercent,
            Double finalInvoicePercent,
            Long downpaymentTargetId,
            Long deliveryInvoiceTargetId,
            Long finalInvoiceTargetId,
            PaymentCondition paymentCondition,
            String note,
            Date created,
            Long createdBy,
            Date changed,
            Long changedBy) {

        super(id, referenceId, created, createdBy, changed, changedBy, false);
        this.downpaymentPercent = downpaymentPercent;
        this.deliveryInvoicePercent = deliveryInvoicePercent;
        this.finalInvoicePercent = finalInvoicePercent;
        this.downpaymentTargetId = downpaymentTargetId;
        this.deliveryInvoiceTargetId = deliveryInvoiceTargetId;
        this.finalInvoiceTargetId = finalInvoiceTargetId;
        this.paymentCondition = paymentCondition;
        this.note = note;
    }

    public void update(PaymentAgreement other) {
        this.downpaymentPercent = other.getDownpaymentPercent();
        this.deliveryInvoicePercent = other.getDeliveryInvoicePercent();
        this.finalInvoicePercent = other.getFinalInvoicePercent();
        this.downpaymentTargetId = other.getDownpaymentTargetId();
        this.deliveryInvoiceTargetId = other.getDeliveryInvoiceTargetId();
        this.finalInvoiceTargetId = other.getFinalInvoiceTargetId();
        this.paymentCondition = other.getPaymentCondition();
        this.hidePayments = other.isHidePayments();
    }

    public Double getDownpaymentPercent() {
        return downpaymentPercent;
    }

    public void setDownpaymentPercent(Double downpaymentPercent) {
        this.downpaymentPercent = downpaymentPercent;
    }

    public Double getDeliveryInvoicePercent() {
        return deliveryInvoicePercent;
    }

    public void setDeliveryInvoicePercent(
            Double deliveryInvoicePercent) {
        this.deliveryInvoicePercent = deliveryInvoicePercent;
    }

    public Double getFinalInvoicePercent() {
        return finalInvoicePercent;
    }

    public void setFinalInvoicePercent(Double finalInvoicePercent) {
        this.finalInvoicePercent = finalInvoicePercent;
    }

    public PaymentCondition getPaymentCondition() {
        return paymentCondition;
    }

    public void setPaymentCondition(PaymentCondition paymentCondition) {
        this.paymentCondition = paymentCondition;
    }

    public Long getDownpaymentTargetId() {
        return downpaymentTargetId;
    }

    public void setDownpaymentTargetId(Long downpaymentTargetId) {
        this.downpaymentTargetId = downpaymentTargetId;
    }

    public Long getDeliveryInvoiceTargetId() {
        return deliveryInvoiceTargetId;
    }

    public void setDeliveryInvoiceTargetId(Long deliveryInvoiceTargetId) {
        this.deliveryInvoiceTargetId = deliveryInvoiceTargetId;
    }

    public Long getFinalInvoiceTargetId() {
        return finalInvoiceTargetId;
    }

    public void setFinalInvoiceTargetId(Long finalInvoiceTargetId) {
        this.finalInvoiceTargetId = finalInvoiceTargetId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public boolean isHidePayments() {
        return hidePayments;
    }

    public void setHidePayments(boolean hidePayments) {
        this.hidePayments = hidePayments;
    }

    public Long getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(Long paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public Long getBillingAccountId() {
        return billingAccountId;
    }

    public void setBillingAccountId(Long billingAccountId) {
        this.billingAccountId = billingAccountId;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.finance.PaymentAgreement#updatePaymentConditions(java.lang.Double, java.lang.Long, java.lang.Double, java.lang.Long, java.lang.Long,
     * com.osserp.core.finance.PaymentCondition)
     */
    public void updatePaymentConditions(
            boolean hidePayments,
            Double downpaymentPercent,
            Long downpaymentTargetId,
            Double deliveryInvoicePercent,
            Long deliveryInvoiceTargetId,
            Long finalInvoiceTargetId,
            PaymentCondition paymentCondition) {

        this.hidePayments = hidePayments;
        this.paymentCondition = paymentCondition;
        this.downpaymentTargetId = downpaymentTargetId;
        this.deliveryInvoiceTargetId = deliveryInvoiceTargetId;
        this.finalInvoiceTargetId = finalInvoiceTargetId;
        this.downpaymentPercent = fetchDouble(downpaymentPercent);
        this.deliveryInvoicePercent = fetchDouble(deliveryInvoicePercent);
        if (this.downpaymentPercent + this.deliveryInvoicePercent > 1) {
            this.deliveryInvoicePercent = 1 - this.downpaymentPercent;
        }
        this.finalInvoicePercent = NumberUtil.round(
                1 - this.downpaymentPercent - this.deliveryInvoicePercent, 2);
    }

    public boolean isPaymentStepsValid() {
        Double result = fetchDouble(getDownpaymentPercent())
                + fetchDouble(getDeliveryInvoicePercent())
                + fetchDouble(getFinalInvoicePercent());
        return (NumberUtil.round(result, 2) == 1d);
    }
}
