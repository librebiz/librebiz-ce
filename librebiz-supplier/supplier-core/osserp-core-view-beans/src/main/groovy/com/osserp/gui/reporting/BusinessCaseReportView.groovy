/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 12, 2012 
 * 
 */
package com.osserp.gui.reporting

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.dao.QueryResult
import com.osserp.common.util.StringUtil
import com.osserp.common.util.DateUtil

import com.osserp.core.BusinessCaseFilter
import com.osserp.core.sales.SalesSearch
import com.osserp.core.system.BranchOffice

import com.osserp.groovy.web.MenuItem

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class BusinessCaseReportView extends AbstractReportingView {
    private static Logger logger = LoggerFactory.getLogger(BusinessCaseReportView.class.getName())
    
    List<BranchOffice> availableBranchs = []
    BranchOffice selectedBranch
    
    BusinessCaseFilter filter = new BusinessCaseFilter()
    QueryResult queryResult
    
    Date businessCaseStatusCacheTime
    
    BusinessCaseReportView() {
        super()
        enableAutoreload()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        businessCaseStatusCacheTime = salesSearch.getBusinessCaseStatusCacheTime()
        logger.debug("initRequest: done [latestCacheRefresh=${businessCaseStatusCacheTime}]")
    }
    
    String getSpreadsheet() {
        salesSearch.getBusinessCaseStatusSheet(filter)
    }
    
    @Override
    void save() {
        selectedBranch = null
        filter.branchId = null
        boolean ignoreBranch = form.getBoolean('ignoreBranch')
        if (ignoreBranch) {
            filter.cached = true
        } else {
            Long branch = form.getLong('branch')
            if (branch) {
                selectedBranch = availableBranchs.find { it.id == branch }
                if (selectedBranch) {
                    filter.branchId = selectedBranch.id
                    filter.cached = false
                } else {
                    filter.cached = true
                }
            }
        }
        filter.dateFrom = form.getDate('dateFrom')
        filter.dateTil = form.getDate('dateTil')
        queryResult = salesSearch.getBusinessCaseStatus(filter)
        createCustomNavigation() 
    }

    @Override
    protected void init() {
        if (forwardRequest) {
            filter = new BusinessCaseFilter()
            //filter.limit = 1000
            filter.dateTil = new Date()
            filter.dateFrom = DateUtil.subtractDays(filter.dateTil, 30)
            availableBranchs = systemConfigManager.getBranchs(domainEmployee)
            
            selectedBranch = branchByCurrentUser
            if (selectedBranch) {
                filter.branchId = selectedBranch.id
            }
        }
    }
    
    @Override
    protected void createCustomNavigation() {
        if (queryResult?.rows) {
            nav.defaultNavigation = [selectExitLink, xlsLink, homeLink]
        } else {
            nav.defaultNavigation = [exitLink, homeLink]
        }
    }

    @Override
    void select() {
        queryResult = null
        createCustomNavigation()
    }

    void reload() {
        if (selectedBranch) {
            
        }
    }
    
    protected SalesSearch getSalesSearch() {
        getService(SalesSearch.class.getName())
    }
}
