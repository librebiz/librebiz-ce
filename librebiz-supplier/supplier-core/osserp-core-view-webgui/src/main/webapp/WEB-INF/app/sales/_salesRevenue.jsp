<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<c:set var="view" value="${sessionScope.salesRevenueView}"/>
<c:if test="${!empty view.bean.name}">
<c:set var="revenue" value="${view.bean}"/>
<style type="text/css">

div.dataEntryForm {
  width: 785px;
}

div.dataEntryForm div.group {
  border: 1px solid #D1D1D1;
  background-color: white;
  margin: 10px 0px 9px 10px;
  overflow: auto;
  width: 762px;
  padding-bottom: 10px;
}

div.group div.header {
  background-color: #DCDCDC;
  margin: 0px 0px 9px 0px;
  border: 1px solid #D1D1D1;
  font-weight: bold;
  width: 760px;
  overflow: auto;
}

div.header div.headerLeft {
  background-color: #DCDCDC;
  margin: 0px;
  padding-left: 9px;
  font-weight: bold;
  float: left;
}

div.header div.headerRight {
  background-color: #DCDCDC;
  margin: 0px;
  text-align: right;
}

td.caption {
	text-align: right;
	width: 240px;
	font-weight: bold;
}

td.a {
	text-align: right;
	width: 120px;
}

.err {
	color: red;
}
</style>
<div class="dataEntryForm">
	<div class="group" style="margin-top: 10px; padding-bottom:10px;">
		<div class="header">
			<div class="headerLeft">
				<fmt:message key="profitMarginCalculation"/>
				<c:if test="${!empty revenue.businessCase}">
					/ <o:out value="${revenue.businessCase.primaryKey}"/> - <o:out value="${revenue.businessCase.name}"/>
				</c:if>
				<c:if test="${!empty revenue.postCalculationRevenue}">
					<c:choose>
						<c:when test="${view.postCalculation}">
							- <v:ajaxLink url="/sales/salesRevenue/togglePostCalculation" targetElement="salesRevenueView_popup"><fmt:message key="postCalculation"/></v:ajaxLink>
						</c:when>
						<c:otherwise>
							- <v:ajaxLink url="/sales/salesRevenue/togglePostCalculation" targetElement="salesRevenueView_popup"><fmt:message key="salesRevenuePlanning"/></v:ajaxLink>
						</c:otherwise>
					</c:choose>
				</c:if>
			</div>
			<div class="headerRight"> </div>
		</div>
		<c:import url="_${revenue.name}.jsp"/>
	</div>
</div>
</c:if>