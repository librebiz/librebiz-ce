/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.osserp.gui.telephones

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.PermissionException
import com.osserp.core.contacts.Contact
import com.osserp.core.contacts.OfficePhone
import com.osserp.core.employees.EmployeeManager
import com.osserp.core.employees.Employee
import com.osserp.core.model.telephone.TelephoneConfigurationImpl
import com.osserp.core.telephone.Telephone
import com.osserp.core.telephone.TelephoneActionManager
import com.osserp.core.telephone.TelephoneConfiguration
import com.osserp.core.telephone.TelephoneConfigurationChangesetManager
import com.osserp.core.telephone.TelephoneConfigurationChangeset
import com.osserp.core.telephone.TelephoneConfigurationManager
import com.osserp.core.telephone.TelephoneGroup
import com.osserp.core.telephone.TelephoneGroupManager
import com.osserp.core.telephone.TelephoneManager
import com.osserp.core.telephone.TelephoneSystem
import com.osserp.core.telephone.TelephoneSystemManager
import com.osserp.gui.CoreView

/**
 * 
 * @author so <so@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public class TelephoneConfigurationPopupView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(TelephoneConfigurationPopupView.class.getName())

    private static final String TELEPHONE_EXCHANGE_EDIT = 'telephone_exchange_edit'
    private static final String TELEPHONE_SYSTEN_ADMIN = 'telephone_system_admin'
    private static final String telephone_configuration_own_edit = 'telephone_configuration_own_edit'
    private static final String TELEPHONE_EDIT_PERMISSIONS = TELEPHONE_EXCHANGE_EDIT+','+TELEPHONE_SYSTEN_ADMIN
    String edit //this tells us which param should be in or not in edit mode
    List<Telephone> telephones = []
    List<TelephoneSystem> telephoneSystems = []
    List<OfficePhone> internals = []
    List<TelephoneGroup> telephoneGroups = []
    Map<String, TelephoneConfigurationChangeset> telConfigChangesets = new HashMap<String, TelephoneConfigurationChangeset>()
    TelephoneConfigurationChangeset lastChangeSet
    String lastChangeSetName
    Long telephoneSystemId
    Long telephoneId

    TelephoneConfigurationPopupView() {
        dependencies = ['contactView', 'telephoneConfigurationSearchView']
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        edit = form.getString('name')
        lastChangeSetName = form.getString('infos')
    }

    void getLastChangeset() {
        logger.debug('getChangeset() invoked...')
        lastChangeSet = null
        if (lastChangeSetName) {
            if (telConfigChangesets.containsKey(lastChangeSetName)) {
                lastChangeSet = telConfigChangesets.get(lastChangeSetName)
            }
        }
    }

    @Override
    void enableEditMode() {
        logger.debug('enableEditMode() invoked...')
        //enables edit mode and tries to load specific lists for selectboxes
        if (isPermissionGrant(bean)) {
            env.editMode = true
            Long branchId
            if (edit == TelephoneConfigurationImpl.TELEPHONE) {
                env.editMode = false
                this.checkPermission(TELEPHONE_SYSTEN_ADMIN)
                env.editMode = true
                TelephoneSystemManager tsm = getService(TelephoneSystemManager.class.getName())
                telephoneSystems = tsm.getAll()
                if (bean?.telephone) {
                    telephoneSystemId = bean.telephone.telephoneSystem?.id
                    telephoneId = bean.telephone?.id
                } else if (env.contactView?.bean?.internalPhone) {
                    branchId = env.contactView.bean.internalPhone?.branchId
                    for (i in telephoneSystems) {
                        if (i.branchId == branchId) {
                            telephoneSystemId = i.id
                            break
                        }
                    }
                    telephoneId = null
                }
                if (telephoneSystemId) {
                    TelephoneSystem telephoneSystem = tsm.find(telephoneSystemId)
                    branchId = telephoneSystem?.branchId
                } else {
                    telephoneId = null
                }
                if (branchId) {
                    TelephoneManager tm = getService(TelephoneManager.class.getName())
                    telephones = tm.findByBranch(branchId)
                }
            }
            if (edit == TelephoneConfigurationImpl.INTERNAL) {
                env.editMode = false
                this.checkPermission(TELEPHONE_SYSTEN_ADMIN)
                env.editMode = true
                if (bean?.telephone?.telephoneSystem?.branchId) {
                    EmployeeManager manager = getService(EmployeeManager.class.getName())
                    internals = manager.getOfficePhones(bean.telephone.telephoneSystem.branchId)
                } else {
                    internals = []
                    throw new PermissionException(ErrorCode.NO_TELEPHONE_SET)
                }
            }
            if (edit == TelephoneConfigurationImpl.TELEPHONE_GROUP) {
                env.editMode = false
                this.checkPermission(TELEPHONE_SYSTEN_ADMIN)
                env.editMode = true
                if (bean?.telephone?.telephoneSystem?.id) {
                    TelephoneGroupManager manager = getService(TelephoneGroupManager.class.getName())
                    telephoneGroups = manager.findByTelephoneSystem(bean.telephone.telephoneSystem.id)
                } else {
                    telephoneGroups = []
                    throw new PermissionException(ErrorCode.NO_TELEPHONE_SET)
                }
            }
        }
    }

    @Override
    void select() {
        logger.debug('select() invoked...')
        //used to set params with values of submited selectbox
        this.checkPermission(TELEPHONE_SYSTEN_ADMIN)
        if (edit == TelephoneConfigurationImpl.TELEPHONE) {
            telephoneSystemId = form.getLong('id')
            if (telephoneSystemId) {
                TelephoneSystemManager tsm = getService(TelephoneSystemManager.class.getName())
                TelephoneSystem telephoneSystem = tsm.find(telephoneSystemId)
                TelephoneManager tm = getService(TelephoneManager.class.getName())
                if (telephoneSystem?.branchId) {
                    telephones = tm.findByBranch(telephoneSystem.branchId)
                }
            } else {
                telephones = []
            }
            telephoneId = null
        }
    }

    void create() {
        logger.debug('create() invoked...')
        //tries to create TelephoneConfiguration just with displayName, accountCode & email
        this.checkPermission(TELEPHONE_SYSTEN_ADMIN)
        String displayName = form.getString('name')
        if (!displayName) {
            throw new ClientException(ErrorCode.NAME_MISSING, this)
        }
        String accountCode = form.getString('costCenter')
        if (!accountCode) {
            throw new ClientException(ErrorCode.COST_CENTER_MISSING, this)
        }
        String email = form.getString('email')

        TelephoneConfigurationManager tcm = getService(TelephoneConfigurationManager.class.getName())
        bean = tcm.create(accountCode, displayName, email)

        if (env.telephoneConfigurationSearchView) {
            env.telephoneConfigurationSearchView.reload()
        }
        disableCreateMode()
    }

    void editOrCreate() {
        logger.debug('editOrCreate() invoked...')
        bean = null
        //tries to load TelephoneConfiguration by employee, creates one for employee if does not exists
        this.checkPermission(TELEPHONE_EDIT_PERMISSIONS+','+telephone_configuration_own_edit)
        Long employeeId = form.getLong('employeeId')
        Long id = form.getLong('id')
        TelephoneConfigurationManager tcm = getService(TelephoneConfigurationManager.class.getName())
        if (employeeId) {
            TelephoneConfiguration tc = tcm.findByEmployeeId(employeeId)
            if (tc) {
                if (isPermissionGrant(tc)) {
                    //edit TelephoneConfiguration
                    bean = tc
                    TelephoneConfigurationChangesetManager tccm = getService(TelephoneConfigurationChangesetManager.class.getName())
                    telConfigChangesets = tccm.getLatestChangesetsByConfigurationId(tc.id)
                }
            } else {
                this.checkPermission(TELEPHONE_SYSTEN_ADMIN)
                //create TelephoneConfiguration
                String accountCode
                String displayName
                String emailAddress
                String internal
                String uid

                if (env.contactView?.bean) {
                    Contact contact = env.contactView.bean
                    displayName = contact.name
                    emailAddress = contact.email
                    if (contact.isEmployee()) {
                        EmployeeManager em = getService(EmployeeManager.class.getName())
                        Employee employee = em.find(contact.contactId)
                        if (employee) {
                            accountCode = employee.costCenter
                            employeeId = employee.id
                            internal = employee.internalPhone?.internal
                        }
                        uid = env.contactView.userAccount?.ldapUid
                    }
                }
                bean = tcm.create(employeeId, uid, internal, accountCode, displayName, emailAddress, null)
                if (env.telephoneConfigurationSearchView) {
                    env.telephoneConfigurationSearchView.reload()
                }
            }
        } else if (id) {
            TelephoneConfiguration tc = tcm.find(id)
            if (isPermissionGrant(tc)) {
                bean = tc
                if (!bean) {
                    throw new ClientException(ErrorCode.NO_DATA_FOUND, this)
                } else {
                    TelephoneConfigurationChangesetManager tccm = getService(TelephoneConfigurationChangesetManager.class.getName())
                    telConfigChangesets = tccm.getLatestChangesetsByConfigurationId(tc.id)
                }
            }
        } else {
            throw new ClientException(ErrorCode.INVALID_CONTEXT, this)
        }
        disableEditMode()
        disableCreateMode()
    }

    @Override
    void reload() {
        logger.debug('reload() invoked...')
        load(bean?.id)
    }

    private void load(Long id) {
        if (id) {
            TelephoneConfigurationManager manager = getService(TelephoneConfigurationManager.class.getName())
            bean = manager.find(id)
            TelephoneConfigurationChangesetManager tccm = getService(TelephoneConfigurationChangesetManager.class.getName())
            telConfigChangesets = tccm.getLatestChangesetsByConfigurationId(bean.id)
        }
    }

    @Override
    void save() {
        logger.debug('save() invoked...')
        this.checkPermission(TELEPHONE_EDIT_PERMISSIONS+','+telephone_configuration_own_edit)
        if (!bean) {
            throw new ClientException(ErrorCode.INVALID_CONTEXT)
        } else {
            if (isPermissionGrant(bean)) {
                TelephoneConfigurationManager tcm = getService(TelephoneConfigurationManager.class.getName())
                if (edit == TelephoneConfigurationImpl.ACCOUNT_CODE) {
                    tcm.updateAccountCode(user.id, bean.id, form.getString('value'))
                } else if (edit == TelephoneConfigurationImpl.DISPLAY_NAME) {
                    tcm.updateDisplayName(user.id, bean.id, form.getString('value'))
                } else if (edit == TelephoneConfigurationImpl.EMAIL_ADDRESS) {
                    tcm.updateEmailAddress(user.id, bean.id, form.getString('value'))
                } else if (edit == TelephoneConfigurationImpl.INTERNAL) {
                    this.checkPermission(TELEPHONE_SYSTEN_ADMIN)
                    tcm.updateInternal(user.id, bean.id, form.getString('value'))
                    edit = null
                } else if (edit == TelephoneConfigurationImpl.TELEPHONE) {
                    this.checkPermission(TELEPHONE_SYSTEN_ADMIN)
                    tcm.updateTelephone(user.id, bean.id, form.getLong('value'))
                    edit = null
                } else if (edit == TelephoneConfigurationImpl.TELEPHONE_GROUP) {
                    this.checkPermission(TELEPHONE_SYSTEN_ADMIN)
                    tcm.updateTelephoneGroup(user.id, bean.id, form.getLong('value'))
                } else if (edit == TelephoneConfigurationImpl.AVAILABLE) {
                    tcm.updateAvailable(user.id, bean.id, form.getBoolean('value'))
                } else if (edit == TelephoneConfigurationImpl.TELEPHONE_EXCHANGE) {
                    this.checkPermission(TELEPHONE_SYSTEN_ADMIN)
                    tcm.updateTelephoneExchange(user.id, bean.id, form.getBoolean('value'))
                } else if (edit == TelephoneConfigurationImpl.BUSY_ON_BUSY) {
                    tcm.updateBusyOnBusy(user.id, bean.id, form.getBoolean('value'))
                } else if (edit == TelephoneConfigurationImpl.VOICE_MAIL_ENABLED) {
                    tcm.updateVoiceMailEnabled(user.id, bean.id, form.getBoolean('value'))
                } else if (edit == TelephoneConfigurationImpl.VOICE_MAIL_SEND_AS_EMAIL) {
                    tcm.updateVoiceMailSendAsEmail(user.id, bean.id, form.getBoolean('value'))
                } else if (edit == TelephoneConfigurationImpl.VOICE_MAIL_DELETE_AFTER_EMAIL_SEND) {
                    tcm.updateVoiceMailDeleteAfterEmailSend(user.id, bean.id, form.getBoolean('value'))
                } else if (edit == TelephoneConfigurationImpl.VOICE_MAIL_DELAY_SECONDS) {
                    tcm.updateVoiceMailDelaySeconds(user.id, bean.id, form.getInteger('value'))
                } else if (edit == TelephoneConfigurationImpl.VOICE_MAIL_ENABLED_IF_BUSY) {
                    tcm.updateVoiceMailEnabledIfBusy(user.id, bean.id, form.getBoolean('value'))
                } else if (edit == TelephoneConfigurationImpl.VOICE_MAIL_ENABLED_IF_UNAVAILABLE) {
                    tcm.updateVoiceMailEnabledIfUnavailable(user.id, bean.id, form.getBoolean('value'))
                } else if (edit == TelephoneConfigurationImpl.PARALLEL_CALL_TARGET_PHONE_NUMBER) {
                    tcm.updateParallelCallTargetPhoneNumber(user.id, bean.id, form.getString('value'))
                } else if (edit == TelephoneConfigurationImpl.PARALLEL_CALL_ENABLED) {
                    tcm.updateParallelCallEnabled(user.id, bean.id, form.getBoolean('value'))
                } else if (edit == TelephoneConfigurationImpl.PARALLEL_CALL_SET_DELAY_SECONDS) {
                    tcm.updateParallelCallSetDelaySeconds(user.id, bean.id, form.getInteger('value'))
                } else if (edit == TelephoneConfigurationImpl.CALL_FORWARD_TARGET_PHONE_NUMBER) {
                    tcm.updateCallForwardTargetPhoneNumber(user.id, bean.id, form.getString('value'))
                } else if (edit == TelephoneConfigurationImpl.CALL_FORWARD_DELAY_SECONDS) {
                    tcm.updateCallForwardDelaySeconds(user.id, bean.id, form.getInteger('value'))
                } else if (edit == TelephoneConfigurationImpl.CALL_FORWARD_ENABLED) {
                    tcm.updateCallForwardEnabled(user.id, bean.id, form.getBoolean('value'))
                } else if (edit == TelephoneConfigurationImpl.CALL_FORWARD_IF_BUSY_TARGET_PHONE_NUMBER) {
                    tcm.updateCallForwardIfBusyTargetPhoneNumber(user.id, bean.id, form.getString('value'))
                } else if (edit == TelephoneConfigurationImpl.CALL_FORWARD_IF_BUSY_ENABLED) {
                    tcm.updateCallForwardIfBusyEnabled(user.id, bean.id, form.getBoolean('value'))
                } else {
                    throw new ClientException(ErrorCode.INVALID_CONTEXT)
                }
                disableEditMode()
                disableCreateMode()
                reload()
                if (env.telephoneConfigurationSearchView) {
                    env.telephoneConfigurationSearchView.reload()
                }
                if (env.telephoneConfigurationSearchListView) {
                    env.telephoneConfigurationSearchListView.search()
                }
            }
        }
    }

    void sendPhoneAction() {
        logger.debug('sendPhoneAction() invoked [edit='+edit+']')
        if (!bean) {
            logger.debug('sendPhoneAction() no bean defined')
            throw new PermissionException(ErrorCode.INVALID_CONTEXT)
        } else if (!bean.telephone) {
            logger.debug('sendPhoneAction() no telephone in bean defined')
            throw new PermissionException(ErrorCode.NO_TELEPHONE_SET)
        } else {
            TelephoneActionManager manager = getService(TelephoneActionManager.class.getName())
            if (edit == TelephoneConfigurationImpl.REBOOT_PHONE) {
                manager.rebootPhone(bean.telephone)
            } else if  (edit == TelephoneConfigurationImpl.SEND_CHECK_CONFIG_TO_PHONE) {
                manager.sendCheckConfigToPhone(bean.telephone)
            }
        }
    }

    private boolean isPermissionGrant(TelephoneConfiguration tc) {
        logger.debug('isPermissionGrant(TelephoneConfiguration tc) invoked...')
        if (tc) {
            //is it the own configuration?
            if (tc.employeeId != null && user?.employee?.id != null && tc.employeeId.equals(user.employee.id)) {
                logger.debug('it is the own telephoneConfiguration')
                if (isPermissionGrant(telephone_configuration_own_edit) || isPermissionGrant(TELEPHONE_SYSTEN_ADMIN)) {
                    logger.debug('employee got permission '+telephone_configuration_own_edit+' or '+TELEPHONE_SYSTEN_ADMIN+', employee is able to edit own telephone configuration')
                    return true
                } else if (tc.isTelephoneExchange() && (isPermissionGrant(TELEPHONE_EXCHANGE_EDIT) || isPermissionGrant(TELEPHONE_SYSTEN_ADMIN))) {
                    logger.debug('it is the own telephoneConfiguration, but also telephoneExchange')
                    logger.debug('employee got permission '+TELEPHONE_EXCHANGE_EDIT+' or '+TELEPHONE_SYSTEN_ADMIN+', employee is able to edit own telephone configuration')
                    return true
                } else {
                    logger.debug('no permission to edit telephone configuration')
                    return false
                }
            } else if (tc.isTelephoneExchange() && (isPermissionGrant(TELEPHONE_EXCHANGE_EDIT) || isPermissionGrant(TELEPHONE_SYSTEN_ADMIN))) {
                logger.debug('employee got permission '+TELEPHONE_EXCHANGE_EDIT+' or '+TELEPHONE_SYSTEN_ADMIN+', employee is able to edit telephone configuration')
                return true
            } else {
                this.checkPermission(TELEPHONE_SYSTEN_ADMIN)
                return true
            }
        } else {
            logger.debug('no telephoneConfiguration given...')
            this.checkPermission(TELEPHONE_SYSTEN_ADMIN)
        }
        return false
    }

}
