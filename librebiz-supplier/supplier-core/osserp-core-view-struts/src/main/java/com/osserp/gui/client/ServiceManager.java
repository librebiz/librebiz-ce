/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 8, 2006 8:47:35 AM 
 * 
 */
package com.osserp.gui.client;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.View;
import com.osserp.common.web.ViewManager;

/**
 * 
 * TODO implement as managed bean (Spring?)
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public final class ServiceManager {

    /**
     * Creates a new view
     * @param ctx
     * @param request
     * @param viewName
     * @return new created view
     * @throws PermissionException if user is no domain user
     */
    public static View createView(
            ServletContext ctx,
            HttpServletRequest request,
            String viewName)
            throws ClientException, PermissionException {

        ViewManager crt = ViewManager.newInstance(ctx);
        return crt.createView(request, viewName);
    }

    /**
     * Creates a new view without invoking init and no storing in any scope
     * @param ctx
     * @param request
     * @param viewName
     * @return new created view
     * @throws PermissionException if user is no domain user
     */
    public static View createViewOnly(
            ServletContext ctx,
            HttpServletRequest request,
            String viewName)
            throws ClientException, PermissionException {

        ViewManager crt = ViewManager.newInstance(ctx);
        return crt.createView(request, viewName);
    }

    /**
     * Tries to fetch view from session scope if exists. Returns null if no view was found
     * @param session
     * @param _class
     * @return view
     */
    public static View fetchView(HttpSession session, Class _class) {
        return ViewManager.fetchView(session, _class);
    }

    /**
     * Removes a view from session scope if exists
     * @param session
     * @param _class
     */
    public static void removeView(HttpSession session, Class _class) {
        ViewManager.removeView(session, _class);
    }
}
