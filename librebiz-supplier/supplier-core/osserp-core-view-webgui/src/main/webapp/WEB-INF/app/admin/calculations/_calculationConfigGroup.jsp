<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="calculationConfigGroupView" />

<div class="modalBoxHeader">
    <div class="modalBoxHeaderLeft">
        <fmt:message key="${view.headerName}" />
    </div>
</div>
<div class="modalBoxData">

    <div class="subcolumns">
        <div class="subcolumn">
            <div class="spacer"></div>
            <c:import url="/errors/_errors.jsp" />

            <v:ajaxForm name="calculationConfigGroupForm" url="/admin/calculations/calculationConfigGroup/save" targetElement="${view.name}_popup">
                <table class="valueTable">
                    <c:choose>
                        <c:when test="${view.assignMode}">
                            <c:set var="promptTitle">
                                <fmt:message key="promptSelect" /> / <fmt:message key="reset" />
                            </c:set>
                            <tr>
                                <td><fmt:message key="selection" /></td>
                                <td>
                                    <oc:select name="configId" styleClass="form-control" options="${view.configs}" value="${view.bean.partListId}" onchange="$('calculationConfigGroupForm').onsubmit();" prompt="false">
                                        <oc:option name="${promptTitle}" value="0" />
                                    </oc:select>
                                </td>
                            </tr>
                        </c:when>
                        <c:otherwise>
                            <tr>
                                <td><fmt:message key="uniqueName" /> / <fmt:message key="title" /></td>
                                <td><v:text name="name" value="${view.bean.name}" styleClass="form-control" /></td>
                            </tr>
                            <tr>
                                <td><fmt:message key="rebate" /></td>
                                <td><v:checkbox name="discounts" value="${view.bean.discounts}" /></td>
                            </tr>
                            <tr>
                                <td><fmt:message key="option" /></td>
                                <td><v:checkbox name="option" value="${view.bean.option}" /></td>
                            </tr>
                            <tr>
                                <td class="row-submit">
                                    <v:ajaxLink url="/admin/calculations/calculationConfigGroup/exit" targetElement="${view.name}_popup">
                                        <input class="cancel" type="button" value="<fmt:message key="exit"/>" />
                                    </v:ajaxLink></td>
                                <td class="row-submit"><o:submit /></td>
                            </tr>
                        </c:otherwise>
                    </c:choose>
                </table>
            </v:ajaxForm>

            <div class="spacer"></div>
        </div>
    </div>
</div>