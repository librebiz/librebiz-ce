/**
 *
 * Copyright (C) 2017 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 5, 2017 
 * 
 */
package com.osserp.core.sql;

import org.springframework.core.io.support.ResourcePatternResolver;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class DatabaseSetupTask extends DatabaseScriptTask implements DatabaseTask {

    public static final String DATABASE_SCRIPT_PATH =
            "classpath:META-INF/sql/osserp_core_main-*.sql";

    public DatabaseSetupTask(
            ResourcePatternResolver pathResolver,
            String dbDriver,
            String datasourceString,
            String dbUser,
            String dbPwd) {
        super(pathResolver, dbDriver, datasourceString, dbUser, dbPwd, false);
    }

    /**
     * The pattern for the path containing the database script(s).
     * @return {@value #DATABASE_SCRIPT_PATH}
     */
    protected String getDatabaseScriptPathPattern(String version) {
        return DATABASE_SCRIPT_PATH;
    }

    /**
     * The name of the systemProperty used to retrieve and increase the 
     * latest script id. 
     * @return {@value DatabaseScriptTask#DATABASE_SCRIPT_DEFAULT_PROPERTY}
     */
    protected String getDatabaseScriptPropertyName() {
        return DATABASE_SCRIPT_DEFAULT_PROPERTY;
    }
}
