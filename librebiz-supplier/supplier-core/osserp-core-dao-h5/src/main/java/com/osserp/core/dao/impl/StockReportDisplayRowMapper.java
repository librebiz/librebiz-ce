/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 25-Sep-2006 18:03:33 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import com.osserp.common.dao.AbstractRowMapper;
import com.osserp.core.views.StockReportDisplay;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class StockReportDisplayRowMapper extends AbstractRowMapper {

    public Object mapRow(ResultSet rs, int arg1) throws SQLException {
        return new StockReportDisplay(
                Long.valueOf(rs.getLong(1)),
                rs.getDate(2),
                Long.valueOf(rs.getLong(3)),
                Long.valueOf(rs.getLong(4)),
                rs.getString(5),
                Long.valueOf(rs.getLong(6)),
                rs.getString(7),
                Double.valueOf(rs.getDouble(8)),
                Long.valueOf(rs.getLong(9)),
                Long.valueOf(rs.getLong(10)));
    }

    public static RowMapperResultSetExtractor getReader() {
        return new RowMapperResultSetExtractor(new StockReportDisplayRowMapper());
    }

}
