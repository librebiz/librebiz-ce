<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="bankAccountDocuments" />
                <c:if test="${!empty view.selectedBankAccount}">
                    -  <o:out value="${view.selectedBankAccount.name}"/>
                </c:if>
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive table-responsive-default">
            <table class="table table-striped">
                <tbody>
                    <c:if test="${empty view.list}">
                        <tr>
                            <td colspan="4"><fmt:message key="thereIsNoDocumentYet" /></td>
                        </tr>
                    </c:if>
                    <c:forEach items="${view.list}" var="doc">
                        <tr>
                            <td class="docName">
                                <o:doc obj="${doc}">
                                    <o:out value="${doc.displayName}" limit="70" />
                                </o:doc>
                            </td>
                            <td class="docDate"><o:date value="${doc.validFrom}" /></td>
                            <td class="docDate"><o:date value="${doc.validTil}" /></td>
                            <td class="doc"><span class="docName"><o:out value="${doc.fileName}" /></span></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
