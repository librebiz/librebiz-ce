package com.osserp.core.model.records;

import com.osserp.core.finance.ItemChangedInfo;

public class PurchaseOrderItemChangedDeleted extends ItemChangedInfoVO {

    public PurchaseOrderItemChangedDeleted() {
        super();
    }

    public PurchaseOrderItemChangedDeleted(ItemChangedInfo other) {
        super(other);
    }

}
