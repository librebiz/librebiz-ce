/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 29-Jan-2007 17:43:18 
 * 
 */
package com.osserp.core.model.records;

import java.util.Date;

import com.osserp.common.Constants;
import com.osserp.common.beans.AbstractEntity;

import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.customers.Customer;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.FinanceRecord;
import com.osserp.core.finance.RecordType;
import com.osserp.core.finance.Records;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractFinanceRecord extends AbstractEntity implements FinanceRecord {

    private RecordType type;
    private Long company;
    private Long branchId;
    private ClassifiedContact contact;
    private Long contactType = null;
    private Long currency = Constants.DEFAULT_CURRENCY;
    private String note;
    private boolean internal = false;
    private Long businessCaseId = null;
    private boolean salesReferenceRequest = false;
    private boolean taxFree = false;
    private Long taxFreeId = null;
    private Date taxPoint = null;
    private Long documentId;
    private Long paymentId;
    private String paymentNote;
    private String customHeader;

    /**
     * Default constructor required by Serializable
     */
    protected AbstractFinanceRecord() {
        super();
        setCreatedBy(Constants.SYSTEM_EMPLOYEE);
    }

    /**
     * Creates a new record by minimum required values. Currency defaults to value of Constants.DEFAULT_CURRENCY.
     * @param id
     * @param company
     * @param branchId
     * @param type
     * @param reference
     * @param contact
     * @param createdBy defaults to {@link com.osserp.common.Constants.SYSTEM_EMPLOYEE} if null
     * @param businessCaseId optional reference to a businessCaseId
     */
    protected AbstractFinanceRecord(
            Long id,
            Long company,
            Long branchId,
            RecordType type,
            Long reference,
            ClassifiedContact contact,
            Employee createdBy,
            Long businessCaseId) {
        super(id, reference, (createdBy == null ? Constants.SYSTEM_EMPLOYEE : createdBy.getId()));
        this.type = type;
        this.company = company;
        this.branchId = branchId;
        this.contact = contact;
        this.contactType = contact.getGroupId();
        this.businessCaseId = businessCaseId;
    }

    /**
     * Creates a new record with additional note.
     * @param id
     * @param company
     * @param branchId
     * @param currency
     * @param type
     * @param reference
     * @param contact
     * @param created
     * @param createdBy defaults to {@link com.osserp.common.Constants.SYSTEM_EMPLOYEE} if null
     * @param businessCaseId optional reference to a businessCaseId
     * @param note optional note
     */
    protected AbstractFinanceRecord(
            Long id,
            Long company,
            Long branchId,
            Long currency,
            RecordType type,
            Long reference,
            ClassifiedContact contact,
            Date created,
            Employee createdBy,
            Long businessCaseId,
            String note) {
        this(id, company, branchId, type, reference, contact, createdBy, businessCaseId);
        if (currency != null) {
            this.currency = currency;
        }
        this.note = note;
    }

    /**
     * Creates a new record with additional note. This constructor does not provide a contact.
     * You should immediately add a contact after initialization to keep the views working properly.
     * @param id
     * @param company
     * @param branchId
     * @param currency
     * @param type
     * @param created
     * @param createdBy defaults to {@link com.osserp.common.Constants.SYSTEM_EMPLOYEE} if null
     * @param note optional note
     * @param taxFree
     * @param taxFreeId
     */
    protected AbstractFinanceRecord(
            Employee user,
            ClassifiedContact contact,
            Long company,
            Long branchId,
            Long currency,
            RecordType type,
            Date created,
            String note,
            boolean taxFree,
            Long taxFreeId) {
        this((Long) null, company, branchId, type, (Long) null, contact, user, (Long) null);
        if (created != null) {
            setCreated(created);
        }
        if (currency != null) {
            this.currency = currency;
        }
        this.note = note;
        this.taxFree = taxFree;
        this.taxFreeId = taxFreeId;
    }

    /**
     * Creates a record by other record. Note: Method copies all properties
     * including the primary key. You have to reset id if object should be used
     * to create a
     * new persistent record.
     * @param record
     */
    protected AbstractFinanceRecord(FinanceRecord record) {
        super(record);
        type = record.getType();
        company = record.getCompany();
        branchId = record.getBranchId();
        contact = record.getContact();
        currency = record.getCurrency();
        note = record.getNote();
        businessCaseId = record.getBusinessCaseId();
        taxFree = record.isTaxFree();
        taxFreeId = record.getTaxFreeId();
        taxPoint = record.getTaxPoint();
        if (record instanceof AbstractFinanceRecord) {
            AbstractFinanceRecord obj = (AbstractFinanceRecord) record;
            contactType = obj.contactType;
            internal = obj.internal;
        }
    }

    public String getNumber() {
        return Records.createNumber(type, getId(), internal);
    }

    public String getReferenceNumber() {
        return Records.createReferenceNumber(type, getReference(), internal);
    }

    public RecordType getType() {
        return type;
    }

    protected void setType(RecordType type) {
        this.type = type;
    }

    public String getTypeName() {
        if (type != null) {
            return type.getResourceKey();
        }
        return null;
    }

    public String getContextName() {
        return getTypeName();
    }

    public Long getCompany() {
        return company;
    }

    public void setCompany(Long company) {
        this.company = company;
    }

    public Long getBranchId() {
        return branchId;
    }

    public void setBranchId(Long branchId) {
        this.branchId = branchId;
    }

    public Long getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }

    public Long getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Long paymentId) {
        this.paymentId = paymentId;
    }

    public String getPaymentNote() {
        return paymentNote;
    }

    public void setPaymentNote(String paymentNote) {
        this.paymentNote = paymentNote;
    }

    public void updateDocumentId(Long changedBy, Long documentId) {
        setChanged(new Date(System.currentTimeMillis()));
        setChangedBy(changedBy != null ? changedBy : Constants.SYSTEM_EMPLOYEE);
        this.documentId = documentId;
    }

    public String getCustomHeader() {
        return customHeader;
    }

    public void setCustomHeader(String customHeader) {
        this.customHeader = customHeader;
    }

    /**
     * @deprecated Exists only for backward compatibility. Will be removed in near future.
     * @return id of the contact object or null if contact is null
     */
    // TODO remove method if all referencing jsp expressions are removed also
    public Long getContactId() {
        return (contact == null ? null : contact.getContactId());
    }

    public ClassifiedContact getContact() {
        return contact;
    }

    protected void setContact(ClassifiedContact contact) {
        this.contact = contact;
    }

    protected void changeContact(ClassifiedContact relatedContact) {
        if (relatedContact != null) {
            this.contact = relatedContact;
            this.contactType = relatedContact.getGroupId();
        }
    }

    public boolean isContactReseller() {
        if (contact != null && contact instanceof Customer) {
            Customer c = (Customer) contact;
            if (c.isReseller()) {
                return true;
            }
        }
        return false;
    }

    /**
     * The contact type is hidden and is only required by queries.
     * The object model does no longer require this property.
     * @return contactType
     */
    protected Long getContactType() {
        return contactType;
    }

    /**
     * The contact type is set by constructor during initial object creation.
     * The value is required for database queries only.
     * @param contactType
     */
    protected void setContactType(Long contactType) {
        this.contactType = contactType;
    }

    public String getCurrencyKey() {
        return Constants.getDefaultCurrency(currency);
    }

    public String getCurrencySymbol() {
        return Constants.getCurrencySymbol(currency);
    }

    public Long getCurrency() {
        return currency;
    }

    public void setCurrency(Long currency) {
        this.currency = currency;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public boolean isInternal() {
        return internal;
    }

    public void setInternal(boolean internal) {
        this.internal = internal;
    }

    public final String getName() {
        return (contact == null ? null : contact.getDisplayName());
    }

    public Long getBusinessCaseId() {
        return businessCaseId;
    }

    protected void setBusinessCaseId(Long businessCaseId) {
        this.businessCaseId = businessCaseId;
    }

    public void deleteBusinessCaseId() {
        setBusinessCaseId(null);
    }

    public boolean isSalesReferenceRequest() {
        return salesReferenceRequest;
    }

    protected void setSalesReferenceRequest(boolean salesReferenceRequest) {
        this.salesReferenceRequest = salesReferenceRequest;
    }

    public Date getTaxPoint() {
        return taxPoint;
    }

    public void setTaxPoint(Date taxPoint) {
        this.taxPoint = taxPoint;
    }

    public Date getTaxPointDate() {
        return taxPoint != null ? taxPoint : getCreated();
    }

    public boolean isTaxFree() {
        return taxFree;
    }

    protected void setTaxFree(boolean taxFree) {
        this.taxFree = taxFree;
    }

    public Long getTaxFreeId() {
        return taxFreeId;
    }

    protected void setTaxFreeId(Long taxFreeId) {
        this.taxFreeId = taxFreeId;
    }

    public void enableTaxFree(Long tfId) {
        taxFree = true;
        taxFreeId = tfId;
    }
}
