/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 9, 2008 8:16:42 AM 
 * 
 */
package com.osserp.core.dms;

import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.Status;

import com.osserp.core.Address;
import com.osserp.core.employees.Employee;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Letter extends LetterContent {

    /**
     * Provides the id of a business case if letter has some association to
     * @return businessId or null if letter is only associated to a contact
     */
    Long getBusinessId();

    /**
     * Provides the status of the letter
     * @return status
     */
    Status getStatus();

    /**
     * Sets a new letter status
     * @param status
     */
    void setStatus(Status status);

    /**
     * Priovides the address letter goes to
     * @return address
     */
    Address getAddress();

    /**
     * Provides all letter infos. Letter info provides some fixed positioned content.
     * @return infos
     */
    List<LetterInfo> getInfos();

    /**
     * Provides the date that should be displayed when printing
     * @return printDate
     */
    Date getPrintDate();

    /**
     * Updates letter settings
     * @param user
     * @param header
     * @param name
     * @param street
     * @param zipcode
     * @param city
     * @param country
     * @param subject
     * @param salutation
     * @param greetings
     * @param language
     * @param signatureLeft
     * @param signatureRight
     * @param printDate
     * @param ignoreSalutation
     * @param ignoreGreetings
     * @param ignoreHeader
     * @param ignoreSubject
     * @param embedded
     * @param embeddedAbove
     * @param embeddedSpaceAfter
     * @param contentKeepTogether
     * @throws ClientException if validation failed
     */
    void update(
            Employee user,
            String header,
            String name,
            String street,
            String zipcode,
            String city,
            Long country,
            String subject,
            String salutation,
            String greetings,
            String language,
            Long signatureLeft,
            Long signatureRight,
            Date printDate,
            boolean ignoreSalutation,
            boolean ignoreGreetings,
            boolean ignoreHeader,
            boolean ignoreSubject,
            boolean embedded,
            boolean embeddedAbove,
            String embeddedSpaceAfter,
            String contentKeepTogether)
            throws ClientException;

    /**
     * Indicates that letter supports manual input of parameters
     * @return manual input supported
     */
    boolean isManualInputSupported();
}
