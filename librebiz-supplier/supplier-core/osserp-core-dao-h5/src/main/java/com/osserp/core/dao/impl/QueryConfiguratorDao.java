/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 26-Jan-2007 23:46:01 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.dao.AbstractHibernateDao;
import com.osserp.common.dao.JdbcQuery;
import com.osserp.common.dao.QueryConfigurator;
import com.osserp.common.dao.QueryContext;

import com.osserp.core.model.query.JdbcQueryContextImpl;
import com.osserp.core.model.query.JdbcQueryImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class QueryConfiguratorDao extends AbstractHibernateDao
        implements QueryConfigurator {

    public QueryConfiguratorDao(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public JdbcQuery create(String name, String title, String query) throws ClientException {
        List<JdbcQuery> existing = findAll();
        for (int i = 0, j = existing.size(); i < j; i++) {
            JdbcQuery next = existing.get(i);
            if (next.getName().equals(name)) {
                throw new ClientException(ErrorCode.NAME_EXISTS);
            }
        }
        JdbcQuery obj = new JdbcQueryImpl(
                getDefaultContext(),
                name,
                title,
                query);
        save(obj);
        return obj;
    }

    public List<JdbcQuery> findAll() {
        return getCurrentSession().createQuery("from " + JdbcQueryImpl.class.getName()
                + " o order by o.name").list();
    }

    public List<JdbcQuery> findActivated() {
        return getCurrentSession().createQuery("from " + JdbcQueryImpl.class.getName()
                + " o where o.deleted = false order by o.outputTitle").list();
    }

    public List<JdbcQuery> findActivatedByContext(String context) {
        if (isEmpty(context) || "none".equalsIgnoreCase(context)) {
            return findActivated();
        }
        List<JdbcQuery> list = getCurrentSession().createQuery("from " + JdbcQueryImpl.class.getName()
                + " o where o.deleted = false and o.context.context = :contextName order by o.name")
                .setParameter("contextName", context).list();
        return list;
    }

    public List<JdbcQuery> findByContext(String context) {
        if (isEmpty(context) || "none".equalsIgnoreCase(context)) {
            return findAll();
        }
        List<JdbcQuery> list = getCurrentSession().createQuery("from " + JdbcQueryImpl.class.getName()
                + " o where o.context.context = :contextName order by o.name")
                .setParameter("contextName", context).list();
        return list;
    }

    public JdbcQuery findById(Long id) {
        if (id == null) {
           return null;
        }
        return (JdbcQuery) getCurrentSession().load(JdbcQueryImpl.class.getName(), id);
    }

    public void save(JdbcQuery query) {
        getCurrentSession().saveOrUpdate(JdbcQueryImpl.class.getName(), query);
    }

    public void delete(JdbcQuery query) {
        query.setDeleted(true);
        save(query);
    }

    private QueryContext getDefaultContext() {
        return (QueryContext) getCurrentSession().load(JdbcQueryContextImpl.class, 1L);
    }
}
