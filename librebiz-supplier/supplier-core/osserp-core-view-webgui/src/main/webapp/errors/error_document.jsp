<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<c:if test="${empty sessionScope.user or empty sessionScope.portalView}">
<c:redirect url="/errors/error_timeout.jsp"/>
</c:if>
<c:set var="errorPageDefaultHeader" scope="request" value="desiredDocumentCouldNotBeLoaded"/>
<c:set var="errorPageDefaultMessage" scope="request" value="documentIsEitherNotAvailableOrThereIsADataBaseProblem"/>
<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/error.jsp" flush="false">
  <tiles:put name="title">
    <o:displayTitle />
  </tiles:put>
  <tiles:put name="headline_right">
    <v:link url="/index" title="backToMenu">
      <o:img name="homeIcon" />
    </v:link>
  </tiles:put>
  <tiles:put name="content">
    <c:import url="/errors/_error_page_message.jsp"/>
  </tiles:put>
</tiles:insert>
