/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 13, 2008 5:26:44 PM 
 * 
 */
package com.osserp.common.service;

import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ResourceLocator {

    /**
     * Provides the default locale for the application using the resource locator
     * @return locale
     */
    Locale getDefaultLocale();

    /**
     * Provides a resource bundle value by resource name and default locale
     * @param code i18n key
     * @return message or value of code if not exists
     */
    String getMessage(String code);

    /**
     * Provides a resource bundle value by resource name and locale
     * @param code i18n key
     * @param locale
     * @return message or value of code if not exists
     */
    String getMessage(String code, Locale locale);

    /**
     * Provides resource bundle
     * @param locale
     * @return bundle
     */
    ResourceBundle getResourceBundle(Locale locale);

    /**
     * Provides all messages for a given locale as map
     * @param locale
     * @return message resources as a map
     */
    Map<String, Object> getResourceBundleMap(Locale locale);

    /**
     * Implementing locators may cache resources after initially fetching them. Invoking reload fetches resources even when they aleady exist for the specified
     * local
     * @param locale optional locale. System wide default locale will be used if missing.
     */
    void reload(Locale locale);

}
