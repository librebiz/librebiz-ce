/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import com.osserp.common.BackendException;


/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class AbstractSpringDao extends AbstractDao {
    private static Logger log = LoggerFactory.getLogger(AbstractSpringDao.class.getName());
    protected JdbcTemplate jdbcTemplate = null;

    public AbstractSpringDao() {
        super();
    }

    public AbstractSpringDao(JdbcTemplate jdbcTemplate) {
        super();
        this.jdbcTemplate = jdbcTemplate;
    }

    protected JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    protected Integer getNextInteger(String sequence) {
        assert sequence != null; 
        StringBuilder sql = new StringBuilder(32);
        sql.append("SELECT nextval('").append(sequence).append("')");
        return jdbcTemplate.queryForObject(sql.toString(), Integer.class);
    }

    protected Long getNextLong(String sequence) {
        assert sequence != null; 
        StringBuilder sql = new StringBuilder(32);
        sql.append("SELECT nextval('").append(sequence).append("')");
        return jdbcTemplate.queryForObject(sql.toString(), Long.class);
    }

    protected Long getNextLongByTable(String tableName) {
        String sequence = sequenceNameByTable(tableName);
        assert sequence != null; 
        return getNextLong(sequence);
    }

    protected String sequenceNameByTable(String tableName) {
        if (isEmpty(tableName)) {
            return null;
        }
        StringBuilder sql = new StringBuilder(tableName.length() + 7);
        sql.append(tableName).append("_id_seq");
        return sql.toString();
    }

    protected boolean createSequence(String name, Long value) {
        assert name != null && name.length() > 0;
        Long startValue = !isEmpty(value) ? value : 1L;
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("CREATE SEQUENCE ")
                .append(name)
                .append(" START WITH ")
                .append(startValue)
                .append(" INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1");
        try {
            boolean result = jdbcTemplate.update(sql.toString()) > 0;
            if (log.isDebugEnabled()) {
                log.debug("createSequence() done [name=" + name
                        + ", value=" + value + ", sucess=" + result + "]");
            }
            return result;
        } catch (Exception e) {
            log.error("createSequence() failed [name=" + name
                    + ", startValue=" + startValue 
                    + ", message=" + e.getMessage() + "]", e);
        }
        return false;
    }
    
    protected boolean sequenceExists(String name) {
        assert name != null && name.length() > 0;
        StringBuilder sql = new StringBuilder(
                "SELECT count(*) FROM information_schema.sequences WHERE sequence_name = '");
        sql.append(name).append("'");
        return jdbcTemplate.queryForObject(sql.toString(), Integer.class).intValue() > 0;
    }

    protected int restartSequence(String sequence, Long value) {
        assert sequence != null && value != null; 
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("ALTER SEQUENCE ")
                .append(sequence)
                .append(" RESTART WITH ")
                .append(value);
        return jdbcTemplate.update(sql.toString());
    }

    protected String getDefaultSequenceName(String tableName) {
        return tableName + "_id_seq";
    }

    protected RowMapperResultSetExtractor getInfoRowMapper() {
        return new RowMapperResultSetExtractor(new InfoRowMapper());
    }

    protected RowMapperResultSetExtractor getOptionRowMapper() {
        return new RowMapperResultSetExtractor(new OptionRowMapper());
    }

    protected RowMapperResultSetExtractor getParameterRowMapper() {
        return new RowMapperResultSetExtractor(new ParameterRowMapper());
    }

    protected RowMapperResultSetExtractor getSelectOptionRowMapper() {
        return new RowMapperResultSetExtractor(new SelectOptionRowMapper());
    }

    protected RowMapperResultSetExtractor getBooleanRowMapper() {
        return new RowMapperResultSetExtractor(new BooleanRowMapper());
    }

    private class BooleanRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int index) throws SQLException {
            return Boolean.valueOf(rs.getBoolean(1));
        }
    }

    protected RowMapperResultSetExtractor getDoubleRowMapper() {
        return new RowMapperResultSetExtractor(new DoubleRowMapper());
    }

    private class DoubleRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int index) throws SQLException {
            return Double.valueOf(rs.getDouble(1));
        }
    }

    protected RowMapperResultSetExtractor getIntegerRowMapper() {
        return new RowMapperResultSetExtractor(new IntegerRowMapper());
    }

    private class IntegerRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int index) throws SQLException {
            return (rs.wasNull()) ? null : Integer.valueOf(rs.getInt(1));
        }
    }

    protected RowMapperResultSetExtractor getLongRowMapper() {
        return new RowMapperResultSetExtractor(new LongRowMapper());
    }

    private class LongRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int index) throws SQLException {
            return (rs.wasNull()) ? null : Long.valueOf(rs.getLong(1));
        }
    }

    protected RowMapperResultSetExtractor getTimestampRowMapper() {
        return new RowMapperResultSetExtractor(new TimestampRowMapper());
    }

    private class TimestampRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int index) throws SQLException {
            return rs.getTimestamp(1);
        }
    }

    protected RowMapperResultSetExtractor getStringRowMapper() {
        return new RowMapperResultSetExtractor(new StringRowMapper());
    }

    private class StringRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int index) throws SQLException {
            return rs.getString(1);
        }
    }

    protected final Double queryForDouble(String sql, Object[] params, int[] types) {
        List<Double> resultList = (List<Double>) jdbcTemplate.query(
                sql,
                params,
                types,
                getDoubleRowMapper());
        return empty(resultList) ? 0d : resultList.get(0);
    }
    
    protected BackendException runtimeException(String guiMessage, String rootMessage) {
        if (isEmpty(guiMessage) && isEmpty(rootMessage)) {
            return new BackendException();
        }
        if (isEmpty(guiMessage)) {
            return new BackendException(new BackendException(rootMessage));
        }
        if (isEmpty(rootMessage)) {
            return new BackendException(guiMessage, new BackendException());
        }
        return new BackendException(guiMessage, new BackendException(rootMessage));
    }
}
