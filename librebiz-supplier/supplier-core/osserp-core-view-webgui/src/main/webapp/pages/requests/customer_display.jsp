<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="customer" value="${requestScope.customer}"/>
<tr>
	<td class="rowLeft">
		<table class="saleDisplayLeft">
			<tr>
				<td class="leftLeft">
					<p>
						<c:choose>
							<c:when test="${!empty sessionScope.contactView.summary}"><fmt:message key="customer"/></c:when>
							<c:when test="${!empty sessionScope.businessCaseView and sessionScope.businessCaseView.salesContext}">
								<a href="<c:url value="${requestScope.contactDisplayUrl}"/>"><fmt:message key="customer"/></a>
								<o:permission role="request_customer_change" info="requestCustomerChange">
									<c:if test="${!empty sessionScope.businessCaseView.sales and sessionScope.businessCaseView.sales.changeable}">
										</p>
										<p>
										<a href="<v:url value="/contacts/contactSearch/forward?exit=/projectDisplay.do&selectionTarget=/salesCustomerChange.do&context=customer"/>"><fmt:message key="customerChange"/></a>
									</c:if>
								</o:permission>
							</c:when>
							<c:otherwise>
								<a href="<c:url value="${requestScope.contactDisplayUrl}"/>"><fmt:message key="potencialBuyer"/></a>
								<o:permission role="request_customer_change" info="requestCustomerChange">
									</p>
									<p>
									<a href="<v:url value="/contacts/contactSearch/forward?exit=/requestDisplay.do&selectionTarget=/requestCustomerChange.do&context=customer"/>"><fmt:message key="potencialBuyerChange"/></a>
								</o:permission>
							</c:otherwise>
						</c:choose>
					</p>
				</td>
				<td class="leftRight">
					<p><o:out value="${customer.displayName}"/></p>
					<p><o:out value="${customer.address.street}"/></p>
					<p><o:out value="${customer.address.zipcode}"/> <o:out value="${customer.address.city}"/></p>
					<p><fmt:message key="customerId"/> <o:out value="${customer.id}"/> <c:if test="${!empty customer.created}"> <fmt:message key="from"/> <o:date value="${customer.created}"/></c:if></p>
				</td>
			</tr>
		</table>
	</td>
	<td class="rowRight">
		<table class="saleDisplayRight">
			<tr>
				<td class="rightLeft">
					<p><fmt:message key="phone"/></p>
					<p><fmt:message key="fax"/></p>
					<p><fmt:message key="mobile"/></p>
					<p><fmt:message key="email"/></p>
				</td>
				<td class="rightRight">
					<p><c:if test="${!empty customer.phone.number}"><oc:phone value="${customer.phone}"/></c:if> &nbsp;</p>
					<p><c:if test="${!empty customer.fax.number}"><oc:phone value="${customer.fax}"/></c:if> &nbsp;</p>
					<p><c:if test="${!empty customer.mobile.number}"><oc:phone value="${customer.mobile}"/></c:if> &nbsp;</p>
					<c:choose>
						<c:when test="${empty customer.email}">
							<p><fmt:message key="common.unknown"/></p>
						</c:when>
						<c:otherwise>
							<p><o:email value="${customer.email}" limit="18"/></p>
						</c:otherwise>
					</c:choose>
				</td>
			</tr>
		</table>
	</td>
</tr>

