/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 31-Jul-2006 
 * 
 */
package com.osserp.core.model.calc;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.osserp.core.calc.CalculationConfig;
import com.osserp.core.calc.CalculationConfigGroup;
import com.osserp.core.calc.CalculationGroupConfig;
import com.osserp.core.model.products.AbstractProductSelection;
import com.osserp.core.products.ProductSelectionConfig;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CalculationConfigGroupImpl extends AbstractProductSelection implements CalculationConfigGroup {

    private int orderId;
    private boolean discounts = false;
    private boolean partList = false;
    private Long partListId = null;
    private List<CalculationGroupConfig> configs = new ArrayList<CalculationGroupConfig>();
    private boolean option = false;

    protected CalculationConfigGroupImpl() {
        super();
    }

    protected CalculationConfigGroupImpl(
            Long id,
            Long referenceId,
            String name,
            int orderId,
            boolean discounts,
            boolean option) {
        super(id, referenceId, name);
        this.orderId = orderId;
        this.discounts = discounts;
        this.option = option;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public boolean isDiscounts() {
        return discounts;
    }

    public void setDiscounts(boolean discounts) {
        this.discounts = discounts;
    }

    public boolean isOption() {
        return option;
    }

    public void setOption(boolean option) {
        this.option = option;
    }

    public boolean isPartList() {
        return partList;
    }

    public void setPartList(boolean partList) {
        this.partList = partList;
    }

    public Long getPartListId() {
        return partListId;
    }

    public void setPartListId(Long partListId) {
        this.partListId = partListId;
    }

    public void updatePartList(CalculationConfig config) {
        if (config == null) {
            this.partList = false;
            this.partListId = null;
        } else {
            this.partList = true;
            this.partListId = config.getId();
        }
    }

    public List<CalculationGroupConfig> getConfigs() {
        return configs;
    }

    protected void setConfigs(List<CalculationGroupConfig> configs) {
        this.configs = configs;
    }

    public List<ProductSelectionConfig> getProductSelections() {
        List<ProductSelectionConfig> list = new ArrayList<ProductSelectionConfig>();
        for (int i = 0, j = configs.size(); i < j; i++) {
            CalculationGroupConfig next = configs.get(i);
            if (next.getProductSelectionConfig() != null) {
                list.add(next.getProductSelectionConfig());
            }
        }
        return list;
    }

    public void addProductSelection(ProductSelectionConfig selectionConfig) {
        CalculationGroupConfigImpl groupConfig = new CalculationGroupConfigImpl(this, selectionConfig);
        configs.add(groupConfig);
    }

    public void removeProductSelection(Long id) {
        for (Iterator<CalculationGroupConfig> i = configs.iterator(); i.hasNext();) {
            CalculationGroupConfig next = i.next();
            if (next.getId().equals(id)) {
                i.remove();
                break;
            }
        }
    }
}
