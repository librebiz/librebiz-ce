/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 6, 2004 
 * 
 */
package com.osserp.core.service.directory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionException;
import com.osserp.common.User;
import com.osserp.common.UserAuthenticator;
import com.osserp.common.directory.DirectoryManager;
import com.osserp.common.directory.DirectoryUser;
import com.osserp.common.service.SysInfo;
import com.osserp.common.util.SecurityUtil;

import com.osserp.core.dao.Users;
import com.osserp.core.system.SystemConfigManager;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class UserAuthenticatorImpl extends AbstractDirectoryService implements UserAuthenticator {
    private static Logger log = LoggerFactory.getLogger(UserAuthenticatorImpl.class.getName());
    private Users users = null;

    public UserAuthenticatorImpl(
            SystemConfigManager systemConfigManager,
            DirectoryManager directoryManager,
            Users users) {
        super(systemConfigManager, directoryManager);
        this.users = users;
    }

    public UserAuthenticatorImpl(Users users) {
        this.users = users;
    }

    public User authenticate(String loginName, String remoteIp) throws PermissionException {
        try {
            User user = users.get(loginName);
            user.setRemoteIp(remoteIp);
            return user;
        } catch (ClientException e) {
            throwPermissionException(ErrorCode.LOGIN_NAME_INVALID);
            return null;
        }
    }

    public User authenticate(String name, String password, String remoteHost) throws PermissionException {
        
        if (password == null || password.length() < 1) {
            log.warn("authenticate() failed [name=" + name
                    + ", message=" + ErrorCode.LOGIN_PASSWORD_MISSING
                    + ", ip=" + remoteHost + "]");
            throw new PermissionException(ErrorCode.LOGIN_FAILED);
        }
        User user = null;
        try {
            user = users.get(name);
            if (user == null || !user.getLdapUid().equals(name)) {
                log.warn("authenticate() failed [name=" + name
                        + ", message=" + ErrorCode.LOGIN_NAME_INVALID
                        + ", ip=" + remoteHost + "]");
                throw new PermissionException(ErrorCode.LOGIN_FAILED);
            }
        } catch (ClientException e) {
            log.warn("authenticate() failed [name=" + name
                    + ", message=" + e.getMessage()
                    + ", ip=" + remoteHost + "]");
            throw new PermissionException(ErrorCode.LOGIN_FAILED);
        }
        
        DirectoryUser ldapUser = getDirectoryManager().findUser(user.getLdapUid(), getDirectoryClientName());
        if (ldapUser == null) {
            if (log.isDebugEnabled()) {
                log.debug("authenticate() login attempt with non-LDAP account [id=" + user.getId()
                        + ", name=" + name
                        + ", uid=" + user.getLdapUid() + "]");
            }
            
            boolean cleartextOK = !SecurityUtil.isPasswordHash(password) 
                    && password.equals(user.getPassword());
            if (!cleartextOK 
                    && !SecurityUtil.checkPassword(password, user.getPassword())) {
                log.warn("authenticate() failed [name=" + name
                        + ", message=" + ErrorCode.LOGIN_PASSWORD_INVALID
                        + ", ip=" + remoteHost + "]");
                throwPermissionException(ErrorCode.LOGIN_FAILED);
            }
            if (cleartextOK) {
                SysInfo sys = new SysInfo();
                if (sys.isSetupEnabled()) {
                    String nuhash = SecurityUtil.createPassword(password);
                    user.setPassword(nuhash);
                    user.setPasswordResetRequired(true);
                    users.save(user);
                    if (log.isDebugEnabled()) {
                        log.debug("authenticate() success via setup password [user="
                                + name + ", remote=" + remoteHost
                                + ", admin=" + user.isAdmin() 
                                + "]");
                    }
                } else {
                    log.warn("authenticate() invalid attempt from " + remoteHost
                            + " to login with cleartext, enable setup to allow this!");
                    throwPermissionException(ErrorCode.SETUP_DISABLED);
                }
            }
            if (log.isDebugEnabled()) {
                log.debug("login() success via local login [id=" + user.getId() 
                    + ", uid=" + user.getLdapUid() + "]");
            }
            user.setRemoteIp(remoteHost);
            return user;
        }
        if (log.isDebugEnabled()) {
            log.debug("login() ldap user found [id=" + user.getId() 
                    + ", uid=" + ldapUser.getValues().get("uid") + "]");
        }
        String userPwd = ldapUser.getUserPasswordDisplay();
        if (!SecurityUtil.checkPassword(password, userPwd)) {
            log.warn("authenticate() failed [name=" + name
                    + ", message=" + ErrorCode.LOGIN_PASSWORD_INVALID
                    + ", ip=" + remoteHost + "]");
            throwPermissionException(ErrorCode.LOGIN_PASSWORD_INVALID);
        }
        user.setRemoteIp(remoteHost);
        try {
            users.updateHistory(user, remoteHost);
        } catch (Exception e) {
            log.warn("authenticate() failed to update history [ip=" + remoteHost
                    + ", message=" + e.getMessage() + "]");
        }
        return user;
    }

    public User changePassword(User user, String password) throws ClientException {
        if (password == null) {
            throw new ClientException(ErrorCode.PASSWORD_MISSING);
        }
        if (user == null || user.getLdapUid() == null || getDirectoryManager() == null) {
            throw new ClientException(ErrorCode.SERVICE_DEPENDENCY_UNAVAILABLE);
        }
        DirectoryUser ldapUser = getDirectoryManager().findUser(user.getLdapUid(), getDirectoryClientName());
        if (ldapUser == null) {
            throw new ClientException(ErrorCode.SERVICE_DEPENDENCY_UNAVAILABLE);
        }
        getDirectoryManager().updatePassword(ldapUser, password);
        user.setPassword(ldapUser.getUserPasswordDisplay());
        users.save(user);
        return user;
    }

    private void throwPermissionException(String reason) throws PermissionException {
        if (verboseLogin()) {
            throw new PermissionException(reason);
        }
        throw new PermissionException();
    }

    private boolean verboseLogin() {
        return isEnabled("loginVerbose");
    }

    public void activate(User user) {
        if (user.getLdapUid() != null) {
            getDirectoryManager().activateUser(user.getLdapUid(), getDirectoryClientName());
        }
    }

    public void deactivate(User user) {
        if (user.getLdapUid() != null) {
            getDirectoryManager().deactivateUser(user.getLdapUid(), getDirectoryClientName());
        }
    }
}
