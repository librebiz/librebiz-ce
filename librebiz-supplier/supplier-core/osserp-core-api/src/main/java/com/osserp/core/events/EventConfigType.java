/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 24-Oct-2005 13:44:58 
 * 
 */
package com.osserp.core.events;

import com.osserp.common.Option;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface EventConfigType extends Option {

    static final Long SALES_FCS_EVENT = 1L;
    static final Long REQUEST_FCS_EVENT = 2L;

    static final Long CONTACT_APPOINTMENT = 3L;
    static final Long REQUEST_APPOINTMENT = 4L;
    static final Long SALES_APPOINTMENT = 5L;

    /**
     * Provides the default recipient type for an event config
     * @return recipientType
     */
    Long getRecipientType();

    /**
     * Provides the sequence name for id of new configs
     * @return sequenceName
     */
    String getSequenceName();

    /**
     * Indicates that related events are appointments
     * @return appointment
     */
    boolean isAppointment();

    /**
     * Indicates that related events are flow control events
     * @return flowControl
     */
    boolean isFlowControl();

    /**
     * Indicates that flow control events are selected by businessType
     * @return flowControlByBusinessType
     */
    boolean isFlowControlByBusinessType();

    /**
     * Provides the logical flow control actions name as used in options
     * @return flowControlActionName logical name of the flow control actions
     */
    String getFlowControlActionsName();

    /**
     * Provides flow control action manager
     * @return flowControlActionMananger name of the responsible fcs action manager
     */
    String getFlowControlActionManager();

    /**
     * Provides flow control action table name
     * @return flowControlActionTable name of the flow control database table
     */
    String getFlowControlActionTable();

    /**
     * Provides the name of the responsible flowControlStartActionView
     * @return flowControlStartActionView
     */
    String getFlowControlStartActionView();

    /**
     * Indicates that corresponding action is an information only (e.g. no appointment, no fcs action, no task)
     * @return true if information
     */
    boolean isInformation();

    /**
     * Indicates that corresponding action is a task (e.g. no appointment, no fcs action, no information)
     * @return true if task
     */
    boolean isTask();

    /**
     * Indicates that corresponding action is configurable
     * @return true if configurable
     */
    boolean isConfigurable();

    /**
     * Provides the default action link as url
     * @return actionLink
     */
    String getActionLink();
    
    /**
     * Provides an optional id of another eventAction of type info.
     * If set, this action is used to create informations about this
     * event to other recipients.  
     * @return infoActionId
     */
    Long getInfoActionId();
    
    /**
     * Sets an optional id of another eventAction of type info.
     * @param infoActionId
     */
    void setInfoActionId(Long infoActionId);
}
