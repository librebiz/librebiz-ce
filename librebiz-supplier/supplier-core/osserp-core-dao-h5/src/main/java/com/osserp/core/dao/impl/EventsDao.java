/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 12, 2006 1:17:54 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import com.osserp.common.Constants;
import com.osserp.common.Parameter;
import com.osserp.common.dao.Tables;
import com.osserp.common.util.CollectionUtil;
import com.osserp.common.util.DateUtil;
import com.osserp.common.util.StringUtil;

import com.osserp.core.BusinessCase;
import com.osserp.core.Comparators;
import com.osserp.core.dao.Events;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.events.Event;
import com.osserp.core.events.EventAction;
import com.osserp.core.events.EventAlertDisplay;
import com.osserp.core.events.EventTicket;
import com.osserp.core.model.events.EventActionImpl;
import com.osserp.core.model.events.EventImpl;
import com.osserp.core.model.events.EventTicketImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class EventsDao extends AbstractDao implements Events {
    private static Logger log = LoggerFactory.getLogger(EventsDao.class.getName());

    private String employeesTable = null;
    private String eventsTable = null;
    private String eventsHistoryTable = null;
    private String eventActionsTable = null;
    private String eventTypesTable = null;
    private String salesTable = null;

    public EventsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory) {

        super(jdbcTemplate, tables, sessionFactory);
        employeesTable = getTable(TableKeys.EMPLOYEES);
        eventsTable = getTable(TableKeys.EVENTS);
        eventsHistoryTable = getTable(TableKeys.EVENTS_HISTORY);
        eventActionsTable = getTable(TableKeys.EVENT_ACTIONS);
        eventTypesTable = getTable(TableKeys.EVENT_TYPES);
        salesTable = getTable(TableKeys.PROJECTS);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.EventConfigs#loadAction(java.lang.Long)
     */
    protected EventAction loadAction(Long id) {
        return (EventAction) getCurrentSession().load(EventActionImpl.class, id);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.Events#createTicket(com.osserp.core.events.EventAction, java.lang.Long, java.lang.Long, java.lang.String, java.lang.String,
     * java.lang.String, boolean)
     */
    public EventTicket createTicket(
            EventAction action,
            Long createdBy,
            Long reference,
            String description,
            String message,
            String headline,
            boolean canceled) {

        EventTicket ticket = new EventTicketImpl(
                action,
                createdBy,
                reference,
                description,
                message,
                headline,
                canceled);
        save(ticket);
        return ticket;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.Events#createTicket(java.lang.Long, java.lang.Long, java.lang.Long, boolean)
     */
    public EventTicket createTicket(
            Long callerId,
            Long configTypeId,
            Long createdBy,
            Long reference,
            boolean canceled) {
        EventTicket ticket = new EventTicketImpl(
                callerId,
                configTypeId,
                createdBy,
                reference,
                canceled);
        if (log.isDebugEnabled()) {
            log.debug("createTicket() ticket object initialized [values=" + ticket.toString() + "]");
        }
        save(ticket);
        return ticket;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.Events#closeTicket(com.osserp.core.events.EventTicket)
     */
    public void closeTicket(EventTicket ticket) {
        getCurrentSession().delete(EventTicketImpl.class.getName(), ticket);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.Events#setTicketSent(com.osserp.core.events.EventTicket)
     */
    public void setTicketSent(EventTicket ticket) {
        ticket.setSent(true);
        save(ticket);
    }

    private void save(EventTicket ticket) {
        getCurrentSession().saveOrUpdate(EventTicketImpl.class.getName(), ticket);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.Events#findOpenTickets(int)
     */
    public List<EventTicket> findOpenTickets(int minutesPastCreated) {
        Date date = DateUtil.subtractMinutes(DateUtil.getCurrentDate(), minutesPastCreated);
        List<EventTicket> list = getCurrentSession().createQuery(
                "from " + EventTicketImpl.class.getName() +
                        " o where o.sent = true " +
                        "and o.closed = false " +
                        "and o.created < :minutesPastCreated")
                .setParameter("minutesPastCreated", date).list();
        return list;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.Events#createEvent(com.osserp.core.events.EventAction, java.lang.Long, java.lang.Long, java.lang.Long, java.lang.String,
     * java.lang.String, java.lang.String, java.util.List)
     */
    public boolean createEvent(
            EventAction action,
            Date created,
            Long createdBy,
            Long reference,
            Long recipient,
            String description,
            String message,
            String headline,
            List<Parameter> parameters) {

        if (!eventExists(action, reference, recipient, message)) {
            EventImpl event = new EventImpl(
                    action,
                    created,
                    createdBy,
                    reference,
                    recipient,
                    description,
                    message,
                    headline,
                    parameters);
            save(event);
            if (log.isDebugEnabled()) {
                log.debug("createEvent() done [event=" + event.getId()
                        + ", action=" + action.getId()
                        + ", reference=" + reference
                        + ", recipient=" + recipient
                        + ", link=" + event.getLink()
                        + "]");
            }
            return true;
        }
        return false;
    }

    /**
     * Indicates already existing event. Method reports true if an event with same action, reference and recipient exists <br/>
     * - and type is flow control or <br/>
     * - type is not flow control and message is equal
     * @param action
     * @param reference
     * @param recipient
     * @param message
     * @return true if event exists
     */
    private boolean eventExists(EventAction action, Long reference, Long recipient, String message) {
        List<Event> list = getCurrentSession().createQuery(
                "from " + EventImpl.class.getName() + " o where o.action.id = :actionId "
                + "and o.referenceId = :reference and o.recipientId = :recipient")
                .setParameter("actionId", action.getId())
                .setParameter("reference", reference)
                .setParameter("recipient", recipient).list();
        Event existing = list.isEmpty() ? null : list.get(0);
        if (existing == null) {
            return false;
        }
        if (log.isDebugEnabled()) {
            log.debug("eventExists() found existing event [id=" + existing.getId()
                    + ", config=" + action.getId()
                    + ", reference=" + reference
                    + ", recipient=" + recipient
                    + ", fcs=" + action.getType().isFlowControl()
                    + "]");
        }
        if (action.getType().isFlowControl()) {
            if (log.isDebugEnabled()) {
                log.debug("eventExists() existing event is fcs [id=" + existing.getId() + ", result=true]");
            }
            return true;
        }
        if (existing.getMessage() != null && existing.getMessage().equals(message)) {
            if (log.isDebugEnabled()) {
                log.debug("eventExists() existing event has same message [id=" + existing.getId() + ", result=true]");
            }
            return true;
        }
        if (log.isDebugEnabled()) {
            log.debug("eventExists() existing event has different message [id=" + existing.getId() + ", result=false]");
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.Events#createEvent(com.osserp.core.events.EventAction, java.lang.Long, java.lang.Long, java.lang.Long, java.lang.String,
     * java.lang.String, java.util.List, java.util.Date, java.util.Date)
     */
    public void createEvent(
            EventAction action,
            Long createdBy,
            Long reference,
            Long recipient,
            String description,
            String message,
            List<Parameter> parameters,
            Date activationDate,
            Date appointmentDate) {

        // create an appointment event
        EventImpl event = new EventImpl(
                action,
                createdBy,
                reference,
                recipient,
                description,
                message,
                parameters,
                activationDate,
                appointmentDate);
        save(event);
        if (log.isDebugEnabled()) {
            log.debug("createEvent() done [id=" + event.getId()
                    + ", action=" + event.getAction().getId() + "]");
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.Events#createAlerts(com.osserp.core.events.Event)
     */
    public void createAlerts(Event sourceEvent) {
        try {
            EventAction alertConfig = sourceEvent.getAction().getAlertEvent();
            if (log.isDebugEnabled()) {
                log.debug("createAlert() invoked [alertEvent="
                        + alertConfig.getId() + ", name=" + alertConfig.getName()
                        + ", source=" + sourceEvent.getId() + "]");
            }
            if (alertConfig.getPool() == null
                    || alertConfig.getPool().getMembers().isEmpty()) {
                log.warn("createAlert() ignoring alert associated with invalid or not configured pool [alertEvent=" 
                        + alertConfig.getId() + ", name=" + alertConfig.getName()
                        + ", source=" + sourceEvent.getId() + "]");

            } else {

                for (int i = 0, j = alertConfig.getPool().getMembers().size(); i < j; i++) {
                    EventImpl event = new EventImpl(
                            alertConfig,
                            sourceEvent.getCreated(),
                            sourceEvent.getReferenceId(),
                            alertConfig.getPool().getMembers().get(i).getRecipientId(),
                            sourceEvent.getDescription(),
                            sourceEvent.getMessage(),
                            sourceEvent.getLink(),
                            sourceEvent.getNotes());
                    save(event);
                }
                this.closeSendAlertFlag(
                        sourceEvent.getReferenceId(),
                        sourceEvent.getAction().getId());
            }
        } catch (Throwable ignorable) {
            log.error("createAlert() ignoring exception [message=" + ignorable.getMessage() + "]", ignorable);
        }
    }

    private void save(Event event) {
        getCurrentSession().saveOrUpdate(event);
    }

    /* (non-Javadoc)
     * @see com.osserp.core.dao.Events#findByRecipient(java.lang.Long, boolean, java.lang.String, boolean, java.lang.String)
     */
    public List<Event> findByRecipient(Long recipient, boolean includeDeactivated, String orderBy, boolean descending, String category) {
        List<Event> result = new ArrayList<>();
        try {
            return findByRecipientAndCategory(recipient, includeDeactivated, orderBy, descending, category);
        } catch (DataAccessException e) {
            if (orderBy != "id") {
                log.warn("findByRecipient() failed, trying again with orderBy='id'");
                try {
                    return findByRecipientAndCategory(recipient, includeDeactivated, "id", false, null);
                } catch (DataAccessException e2) {
                    log.warn("findByRecipient() failed again, giving up [recipient=" + recipient
                            + ", includeDeactivated=" + includeDeactivated
                            + ", orderBy=" + orderBy + ", descending=" + descending
                            + ", category=" + category + "]");
                }
            }
        } 
        return result;
    }

    private List<Event> findByRecipientAndCategory(Long recipient, boolean includeDeactivated, String orderBy, boolean descending, String category) {
        List<Event> result = new ArrayList<>();
        if (orderBy == null || orderBy == "" || orderBy == " ") {
            orderBy = "eventDate";
        }
        String[] names = null;
        Object[] params = null;
        StringBuilder sql = new StringBuilder("from EventImpl event where event.recipientId = :recipient");
        if (includeDeactivated) {
            names = new String[] { "recipient" };
            params = new Object[] { recipient };
        } else {
            sql.append(" and event.activation < :activationDate");
            names = new String[] { "recipient", "activationDate" };
            params = new Object[] { recipient, DateUtil.getCurrentDate() };
        }
        sql.append(" and event.closed = false");
        if (category != null) {
            sql.append(" and event.action.type.").append(category).append(" = true");
        }
        if (log.isDebugEnabled()) {
            log.debug("findByRecipient() query created [sql=" + sql.toString()
                    + ", params=" + StringUtil.createCommaSeparated(params) + "]");
        }
        Query query = getCurrentSession().createQuery(sql.toString());
        for (int i = 0; i < names.length; i++) {
            query = query.setParameter(names[i], params[i]);
        }
        result = query.list();

        if ("referenceId".equalsIgnoreCase(orderBy)) {
            return CollectionUtil.sort(result, Comparators.createEventReferenceComparator(descending));
        }
        if ("created".equalsIgnoreCase(orderBy)) {
            return CollectionUtil.sort(result, Comparators.createEventDateComparator(descending, true));
        }
        return CollectionUtil.sort(result, Comparators.createEventDateComparator(descending, false));
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.Events#findByReference(java.lang.Long)
     */
    public List<Event> findByReference(Long reference) {
        List<Event> events = getCurrentSession().createQuery(
                "from " + EventImpl.class.getName()
                + " event where event.referenceId = :reference"
                + " and event.activation < :activationDate"
                + " and event.closed = false order by event.id desc")
                .setParameter("reference", reference)
                .setParameter("activationDate", DateUtil.getCurrentDate()).list();
        Set<Long> alreadyAdded = new HashSet<Long>();
        for (Iterator<Event> i = events.iterator(); i.hasNext();) {
            Event next = i.next();
            if (alreadyAdded.contains(next.getAction().getId())) {
                i.remove();
            } else {
                alreadyAdded.add(next.getAction().getId());
            }
        }
        return events;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.Events#findAppointments(com.osserp.core.BusinessCase)
     */
    public List<Event> findAppointments(BusinessCase bc) {
        List<Event> events = getCurrentSession().createQuery(
                "from " + EventImpl.class.getName()
                + " event where event.referenceId = :reference"
                + " and event.closed = false"
                + " and event.action.type.appointment = true"
                + " order by event.id desc")
                .setParameter("reference", bc.getPrimaryKey()).list();
        return events;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.Events#findAppointmentsByRecipient(java.lang.Long)
     */
    public List<Event> findAppointmentsByRecipient(Long recipient) {
        List<Event> events = getCurrentSession().createQuery(
                "from " + EventImpl.class.getName()
                + " event where event.recipientId = :recipient"
                + " and event.closed = false"
                + " and event.action.type.appointment = true"
                + " order by event.id")
                .setParameter("recipient", recipient).list();
        return events;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.Events#findEvent(java.lang.Long)
     */
    public Event findEvent(Long eventId) {
        return (Event) getCurrentSession().load(EventImpl.class, eventId);
    }

    /* (non-Javadoc)
     * @see com.osserp.core.dao.Events#addNote(com.osserp.core.events.Event, java.lang.Long, java.lang.String, java.util.Date, java.util.Date, boolean, java.util.List)
     */
    public void addNote(
            Event event,
            Long createdBy,
            String note,
            Date activationDate,
            Date appointmentDate,
            boolean distribute,
            List<Long> informRecipients) {

        if (event == null) {
            throw new IllegalArgumentException("event must not be null");
        }
        if (log.isDebugEnabled()) {
            log.debug("addNote() invoked [event=" + event.getId()
                    + ", reference=" + event.getReferenceId()
                    + ", activationDate=" + activationDate
                    + ", appointmentDate=" + appointmentDate
                    + "]");
        }
        if (activationDate != null) {
            event.setActivation(activationDate);
        }
        if (appointmentDate != null) {
            if (event.getAction().getType().isAppointment()) {
                event.setAppointmentDate(appointmentDate);
            } else {
                event.setExpires(appointmentDate);
            }
        }
        save(event);
        
        Set<Long> informedUsers = new HashSet();
        
        if (!isEmpty(note) || (appointmentDate != null && !event.getAction().getType().isAppointment())) {
            
            if (event.getAction().getType().isAppointment() || !distribute) {
                event.addNote(createdBy, note);
                save(event);
                if (log.isDebugEnabled()) {
                    log.debug("addNote() added new note [event=" + event.getId()
                            + ", recipient=" + createdBy + "]");
                }
            } else {
                List<Event> events = findByCallerAndReference(event.getReferenceId(), event.getAction().getCallerId());
                for (int i = 0, j = events.size(); i < j; i++) {
                    Event e = events.get(i);
                    if (!isEmpty(note)) {
                        e.addNote(createdBy, note);
                    }
                    if (appointmentDate != null && !e.getAction().getType().isAppointment()) {
                        e.setExpires(appointmentDate);
                    }
                    save(e);
                    if (log.isDebugEnabled()) {
                        log.debug("addNote() added new note to " + e.getId());
                    }
                    if (event.getId().equals(e.getId())) {
                        event = e;
                    }
                    informedUsers.add(e.getRecipientId());
                }
            }
        }
        if (!isEmpty(informRecipients) && event.getAction().isInfoActionAvailable()) {
            try {
                EventAction infoAction = loadAction(event.getAction().getInfoAction());
                for (int i = 0, j = informRecipients.size(); i < j; i++) {
                    Long infoRecipient = informRecipients.get(i);
                    
                    if (!informedUsers.contains(infoRecipient) || systemPropertyEnabled("eventInformInformed")) {
                        EventImpl info = new EventImpl(
                            infoAction,
                            event,
                            infoRecipient);
                        save(info);
                        
                        if (log.isDebugEnabled()) {
                            log.debug("addNote() create info event [config=" + infoAction.getId() 
                                    + ", event=" + info.getId()
                                    + ", recipient=" + infoRecipient
                                    + "]");
                        }
                    }
                }
            } catch (Exception e) {
                if (log.isDebugEnabled()) {
                    log.debug("addNote() failed on distributing info events [message="
                        + e.getMessage(), e);
                }
                log.warn("addNote() ignoring info event recipients [message=" + e.getMessage() + "]");
            }
        }
    }

    /**
     * Provides all open events by caller and reference
     * @param referenceId
     * @param callerId
     * @return
     */
    private List<Event> findByCallerAndReference(Long referenceId, Long callerId) {
        // TODO do directly in hibernate when reason for 'ordinal parameter mismatch' is clear
        List<Event> events = new ArrayList<Event>();
        List<Long> ids = findIdsByCallerAndReference(referenceId, callerId);
        for (int i = 0, j = ids.size(); i < j; i++) {
            events.add(findEvent(ids.get(i)));
        }
        return events;
    }

    /**
     * Provides all open event id's related to given reference and caller
     * @param referenceId
     * @param callerId
     * @return ids of open events
     */
    private List<Long> findIdsByCallerAndReference(Long referenceId, Long callerId) {
        StringBuilder query = new StringBuilder();
        query
                .append("SELECT id FROM ")
                .append(eventsTable)
                .append(" WHERE reference_id = ? AND event_activation < ? " +
                        "AND event_config_id IN " +
                        "(SELECT id FROM ")
                .append(eventActionsTable)
                .append(" WHERE caller_id = ?)");
        return (List<Long>) jdbcTemplate.query(
                query.toString(),
                new Object[] { referenceId, DateUtil.getCurrentDate(), callerId },
                new int[] { Types.BIGINT, Types.TIMESTAMP, Types.BIGINT },
                getLongRowMapper());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.Events#closeEvent(java.lang.Long, java.lang.Long)
     */
    public void closeEvent(Long closedBy, Long id) {
        StringBuilder sql = new StringBuilder();
        sql.append("UPDATE ").append(eventsTable).append(
                " SET is_closed = true, closed_on = ?, closed_by = ? WHERE id = ?");
        String update = sql.toString();
        final Object[] params = {
                new Timestamp(System.currentTimeMillis()),
                getClosing(closedBy),
                id };
        final int[] types = {
                Types.TIMESTAMP,
                Types.BIGINT,
                Types.BIGINT };
        jdbcTemplate.update(update, params, types);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.Events#closeEvents(java.lang.Long, java.lang.Long, java.lang.Long)
     */
    public void closeEvents(Long closedBy, Long reference, Long actionId) {
        StringBuilder sql = new StringBuilder();
        sql.append("UPDATE ").append(eventsTable).append(
                " SET is_closed = true, closed_on = ?, closed_by = ? "
                        + "WHERE reference_id = ? AND event_config_id = ?");
        String update = sql.toString();
        final Object[] params = {
                new Timestamp(System.currentTimeMillis()),
                getClosing(closedBy),
                reference,
                actionId
        };
        final int[] types = { Types.TIMESTAMP, Types.BIGINT, Types.BIGINT,
                Types.BIGINT };
        jdbcTemplate.update(update, params, types);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.Events#restoreClosed(java.lang.Long, java.lang.Long)
     */
    public void restoreClosed(Long reference, Long actionId) {
        // restore if closed already in events
        StringBuilder sql = new StringBuilder();
        sql.append("UPDATE ").append(eventsTable).append(
                " SET is_closed = false, closed_on = null, closed_by = null "
                        + "WHERE reference_id = ? AND event_config_id = ?");
        final Object[] params = { reference, actionId };
        final int[] types = { Types.BIGINT, Types.BIGINT };
        jdbcTemplate.update(sql.toString(), params, types);
        // restore if closed are already moved to history
        sql = new StringBuilder();
        sql.append("UPDATE ").append(eventsHistoryTable).append(
                " SET is_closed = false, closed_on = null, closed_by = null "
                        + "WHERE reference_id = ? AND event_config_id = ?");
        jdbcTemplate.update(sql.toString(), params, types);
        jdbcTemplate.queryForObject(Procs.RESTORE_CLOSED_EVENTS, Integer.class);
    }

    /* (non-Javadoc)
     * @see com.osserp.core.dao.Events#closeAll(java.lang.Long, java.lang.Long)
     */
    public int closeAll(Long closedBy, Long reference) {
        StringBuilder sql = new StringBuilder();
        sql.append("UPDATE ").append(eventsTable).append(
                " SET is_closed = true, closed_on = ?, closed_by = ? "
                + "WHERE event_config_id NOT IN (SELECT id FROM ")
            .append(eventActionsTable)
            .append(" WHERE type_id IN (SELECT id FROM ")
            .append(eventTypesTable)
            .append(" WHERE is_appointment)) AND reference_id = ?");
        final Object[] params = { new Timestamp(System.currentTimeMillis()),
                getClosing(closedBy), reference, };
        final int[] types = { Types.TIMESTAMP, Types.BIGINT, Types.BIGINT };
        return jdbcTemplate.update(sql.toString(), params, types);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.Events#moveClosedEvents()
     */
    public int moveClosedEvents() {
        StringBuilder removeDeactivated = new StringBuilder();
        removeDeactivated
                .append("UPDATE ")
                .append(eventsTable)
                .append(" SET is_closed = true, " +
                        "closed_on = CURRENT_TIMESTAMP, " +
                        "closed_by = 1000 " +
                        "WHERE " +
                        "event_config_id IN (SELECT id FROM ")
                .append(eventActionsTable)
                .append(" WHERE closeable_by_target = false) " +
                        "AND reference_id IN " +
                        "(SELECT id FROM ")
                .append(salesTable)
                .append(" where cancelled = true OR status = 100)");
        jdbcTemplate.update(removeDeactivated.toString());
        removeDeactivated = new StringBuilder();
        removeDeactivated
                .append("DELETE FROM ")
                .append(eventsTable)
                .append(" WHERE recipient_id IN (SELECT id FROM ")
                .append(employeesTable)
                .append(" where isactive = false)");
        jdbcTemplate.update(removeDeactivated.toString());
        removeDeactivated = new StringBuilder();
        removeDeactivated
                .append("DELETE FROM ")
                .append(eventsTable)
                .append(" WHERE id IN (SELECT e.id FROM ")
                .append(eventsTable).append(" e,")
                .append(eventActionsTable).append(" c")
                .append(" WHERE e.event_config_id = c.id AND" +
                        " c.pool_id IS NOT null AND e.recipient_id NOT IN " +
                        "(SELECT recipient_id FROM event_recipients WHERE pool_id = c.pool_id))");
        jdbcTemplate.update(removeDeactivated.toString());
        return jdbcTemplate.queryForObject(Procs.MOVE_CLOSED_EVENTS, Integer.class);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.Events#findEscalatedEvents()
     */
    public Long[] findEscalatedEvents() {
        List<Long> result = new ArrayList<Long>();
        StringBuilder query = new StringBuilder();
        query
                .append("SELECT " +
                        "e.id," +
                        "e.reference_id," +
                        "e.event_created," +
                        "c.id," +
                        "c.termination_alert FROM ")
                .append(eventsTable)
                .append(" e, ")
                .append(eventActionsTable)
                .append(" c WHERE e.event_config_id = c.id" +
                        " AND e.is_alert_sent = false AND c.send_alert = true" +
                        " AND c.termination_alert IS NOT NULL");
        List<EventEscalation> all = (List<EventEscalation>) jdbcTemplate.query(
                query.toString(),
                getEscalationReader());

        List<EventEscalation> added = new ArrayList<EventEscalation>();
        Date now = new Date(System.currentTimeMillis());
        for (int i = 0, j = all.size(); i < j; i++) {
            EventEscalation val = all.get(i);
            if (!eventEscalationExists(added, val)) {
                Date escalation = DateUtil.addMillis(val.getCreated(), val.getTerminationAlert());
                if (now.after(escalation)) {
                    result.add(val.getId());
                    added.add(val);
                    if (log.isDebugEnabled()) {
                        log.debug("getEscalatedEvents() added config "
                                + val.getConfigId() + " on reference " + val.getReferenceId());
                    }
                }
            }
        }
        Long[] ids = new Long[result.size()];
        for (int i = 0, j = ids.length; i < j; i++) {
            ids[i] = result.get(i);
        }
        return ids;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.Events#getEventAlertDisplay()
     */
    public List<EventAlertDisplay> getEventAlertDisplay() {
        List<EventAlertDisplay> result = (List<EventAlertDisplay>) jdbcTemplate.query(
                Procs.GET_EVENT_ESCALATIONS_WITH_INITIALS,
                EventAlertDisplayRowMapper.getReader());
        if (log.isDebugEnabled()) {
            log.debug("getEventAlertDisplay() fetched " + result.size() + " escalations");
        }
        return result;
    }

    private boolean eventEscalationExists(List<EventEscalation> list, EventEscalation values) {
        for (int i = 0, j = list.size(); i < j; i++) {
            EventEscalation e = list.get(i);
            if (e.getReferenceId() != null) {
                if (e.getConfigId().equals(values.getConfigId())
                        && e.getReferenceId().equals(values.getReferenceId())) {
                    return true;
                }
            }
        }
        return false;
    }

    private Long getClosing(Long closedBy) {
        if (closedBy == null) {
            return Constants.SYSTEM_USER;
        }
        return closedBy;
    }

    private void closeSendAlertFlag(Long reference, Long action) {
        StringBuilder sql = new StringBuilder();
        sql
                .append("UPDATE ")
                .append(eventsTable)
                .append(" SET is_alert_sent = true WHERE reference_id = ?"
                        + " AND event_config_id = ?");
        final Object[] params = { reference, action };
        final int[] types = { Types.BIGINT, Types.BIGINT };
        jdbcTemplate.update(sql.toString(), params, types);
    }

    private RowMapperResultSetExtractor getEscalationReader() {
        return new RowMapperResultSetExtractor(new EscalationRowMapper());
    }

    private class EscalationRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int index) throws SQLException {
            return new EventEscalation(
                    Long.valueOf(rs.getLong(1)), // id
                    Long.valueOf(rs.getLong(2)), // referenceid
                    rs.getTimestamp(3), // created,
                    Long.valueOf(rs.getLong(4)), // action id
                    Long.valueOf(rs.getLong(5))); // termination target
        }
    }

}
