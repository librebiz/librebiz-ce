<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="calculationConfigView"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="styles" type="string">
        <style type="text/css">
        .name { width: 10%; text-align: right; }
        </style>
    </tiles:put>
    <tiles:put name="headline">
        <fmt:message key="${view.headerName}"/>
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation/>
    </tiles:put>

    <tiles:put name="content" type="string" >
        <div class="content-area" id="calculationConfigContent">
            <div class="row">
                <div class="col-lg-12 panel-area">
                    <div class="table-responsive table-responsive-default">
                        <c:choose>
                            <c:when test="${empty view.bean}">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th><v:sortLink key="type.name"><fmt:message key="purpose"/></v:sortLink></th>
                                            <th><v:sortLink key="name"><fmt:message key="name"/></v:sortLink></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="config" items="${view.list}">
                                            <tr>
                                                <td><o:out value="${config.type.name}"/></td>
                                                <td><v:link url="/admin/calculations/calculationConfig/select" parameters="id=${config.id}"><o:out value="${config.name}"/></v:link></td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </c:when>
                            <c:otherwise>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th class="name"><fmt:message key="name"/></th>
                                            <th><o:out value="${view.bean.type.name}"/> - <o:out value="${view.bean.name}"/></th>
                                            <th class="center" colspan="3"><fmt:message key="action"/></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="name"> </td>
                                            <td><o:out value="${view.bean.type.description}"/></td>
                                            <th class="center" colspan="3"> </th>
                                        </tr>
                                        <tr><td colspan="5">&nbsp;</td></tr>
                                        <c:choose>
                                            <c:when test="${empty view.bean.groups}">
                                                <tr>
                                                    <td class="nane"><fmt:message key="groups"/></td>
                                                    <td><fmt:message key="noGroupsDefined"/>!</td>
                                                    <td class="icon center">
                                                        <o:permission role="executive,calc_config" info="permissionAddNewGroups">
                                                            <v:ajaxLink linkId="calculationConfigGroupView" url="/admin/calculations/calculationConfigGroup/forward" title="addNewGroup">
                                                                <o:img name="newdataIcon"/>
                                                            </v:ajaxLink>
                                                        </o:permission>
                                                    </td>
                                                    <td class="icon"> </td>
                                                    <td class="icon"> </td>
                                                </tr>
                                            </c:when>
                                            <c:otherwise>
                                                <c:forEach var="group" items="${view.bean.groups}" varStatus="s">
                                                    <tr class="altrow">
                                                        <td class="name"><c:if test="${s.index == 0}"><fmt:message key="groups"/></c:if></td>
                                                        <td class="boldtext"><o:out value="${group.name}"/></td>
                                                        <o:forbidden role="executive,calc_config" info="permissionEditGroups">
                                                            <td class="icon" colspan="3"> </td>
                                                        </o:forbidden>
                                                        <o:permission role="executive,calc_config" info="permissionEditGroups">
                                                            <td class="icon center">
                                                                <a href="javascript:onclick=confirmLink('<fmt:message key="confirmation"/>:\n<fmt:message key="confirmGroupDelete"/>','<v:url value="/admin/calculations/calculationConfig/deleteGroup?id=${group.id}"/>');" title="<fmt:message key="groupDelete"/>">
                                                                    <o:img name="deleteIcon"/>
                                                                </a>
                                                            </td>
                                                            <td class="icon center">
                                                                <v:ajaxLink linkId="calculationConfigGroupView" url="/admin/calculations/calculationConfigGroup/forward?id=${group.id}" title="changeGroupName">
                                                                    <o:img name="writeIcon"/>
                                                                </v:ajaxLink>
                                                            </td>
                                                            <td class="icon center">
                                                                <c:if test="${view.bean.type.supportsPartlists}">
                                                                    <c:choose>
                                                                        <c:when test="${!empty group.partListId}">
                                                                            <v:ajaxLink linkId="calculationConfigGroupView" url="${'/admin/calculations/calculationConfigGroup/enableAssignMode?id='}${group.id}" title="changeAssignedPartlist">
                                                                                <o:img name="calcIcon"/>
                                                                            </v:ajaxLink>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <v:ajaxLink linkId="calculationConfigGroupView" url="${'/admin/calculations/calculationConfigGroup/enableAssignMode?id='}${group.id}" title="assignPartlist">
                                                                                <o:img name="rightarrowIcon"/>
                                                                            </v:ajaxLink>
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </c:if>
                                                            </td>
                                                        </o:permission>
                                                    </tr>
                                                    <c:choose>
                                                        <c:when test="${empty group.configs}">
                                                            <tr>
                                                                <td class="name"> </td>
                                                                <td><fmt:message key="noProductSelectionAvailable"/></td>
                                                                <td class="icon center">
                                                                    <o:permission role="executive,calc_config" info="permissionAddProductSelection">
                                                                        <v:link url="/selections/productSelectionConfigsSelection/forward" parameters="context=1&exit=${view.baseLink}/reload&selectionTarget=/admin/calculations/calculationConfig/addSelection?groupId=${group.id}" title="addNewProductSelection">
                                                                            <o:img name="newdataIcon"/>
                                                                        </v:link>
                                                                    </o:permission>
                                                                </td>
                                                                <td class="icon"> </td>
                                                                <td class="icon"> </td>
                                                            </tr>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <c:forEach var="groupConfig" items="${group.configs}">
                                                                <tr>
                                                                    <td class="name"> </td>
                                                                    <td><o:out value="${groupConfig.productSelectionConfig.name}"/></td>
                                                                    <td class="icon center">
                                                                        <o:permission role="executive,calc_config" info="permissionRemoveProductSelection">
                                                                            <v:link url="/admin/calculations/calculationConfig/removeSelection?groupId=${group.id}&id=${groupConfig.id}" title="removeProductSelection">
                                                                                <o:img name="deleteIcon"/>
                                                                            </v:link>
                                                                        </o:permission>
                                                                    </td>
                                                                    <td class="icon center">
                                                                        <o:permission role="executive,calc_config" info="permissionEditProductSelection">
                                                                            <v:link url="/selections/productSelectionConfigsSelection/enableEditMode" parameters="context=1&id=${groupConfig.productSelectionConfig.id}&exit=${view.baseLink}/reload" title="editProductSelection">
                                                                                <o:img name="writeIcon"/>
                                                                            </v:link>
                                                                        </o:permission>
                                                                    </td>
                                                                    <td class="icon center">
                                                                        <v:ajaxLink url="/products/productSelectionList/forward?id=${groupConfig.productSelectionConfig.id}&strutsExit=calculationConfigDisplay" linkId="selectionProductListView" title="displayProductSelection">
                                                                            <o:img name="searchIcon"/>
                                                                        </v:ajaxLink>
                                                                    </td>
                                                                </tr>
                                                            </c:forEach>
                                                            <tr>
                                                                <td class="name"> </td>
                                                                <td><fmt:message key="new"/></td>
                                                                <td class="icon center">
                                                                    <o:permission role="executive,calc_config" info="permissionAddNewProductGroups">
                                                                        <v:link url="/selections/productSelectionConfigsSelection/forward" parameters="context=1&exit=${view.baseLink}/reload&view=calculationConfigView&exclude=includedSelections.group${group.id}&selectionTarget=/admin/calculations/calculationConfig/addSelection?groupId=${group.id}" title="addNewProductSelection">
                                                                            <o:img name="newdataIcon"/>
                                                                        </v:link>
                                                                    </o:permission>
                                                                </td>
                                                                <td class="icon"> </td>
                                                                <td class="icon"> </td>
                                                            </tr>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:forEach>
                                            </c:otherwise>
                                        </c:choose>
                                    </tbody>
                                </table>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
