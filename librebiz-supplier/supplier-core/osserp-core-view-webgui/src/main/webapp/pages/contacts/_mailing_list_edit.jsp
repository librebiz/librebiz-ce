<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${sessionScope.mailingListView}" />

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="addedToDeployer" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive table-responsive-default">
            <table class="table">
                <tbody>
                    <c:forEach var="obj" items="${view.bean.members}">
                        <tr>
                            <td style="width: 291px;"><o:out value="${obj.email}" /></td>
                            <td style="width: 60px;" class="center">
                                <o:permission role="mailing_lists_edit,executive" info="permissionMailingListsRemoveMembers">
                                    <a href="<c:url value="/mailingLists.do?method=removeMember&target=${obj.email}"/>">
                                        <o:img name="deleteIcon" />
                                    </a>
                                </o:permission>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="notAddedToDeployer" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive table-responsive-default">
            <table class="table">
                <tbody>
                    <c:forEach var="obj" items="${view.availableRecipients}">
                        <c:set var="mail" value="${obj.email}" />
                        <c:if test="${!empty mail}">
                            <tr>
                                <td style="width: 291px;">
                                    <a href="<c:url value="/contacts.do?method=load&exit=refreshMailingLists&id=${obj.contactId}"/>"> 
                                        <o:out value="${mail}" />
                                    </a>
                                </td>
                                <td style="width: 60px;" class="center">
                                    <o:permission role="mailing_lists_edit,executive" info="permissionMailingListsAddMembers">
                                        <a href="<c:url value="/mailingLists.do?method=addMember&target=${mail}"/>">
                                            <o:img name="enabledIcon" />
                                        </a>
                                    </o:permission>
                                </td>
                            </tr>
                        </c:if>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
