/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 4, 2010 2:10:02 PM 
 * 
 */
package com.osserp.core.dao.telephone.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.dao.AbstractHibernateAndSpringDao;
import com.osserp.common.dao.Tables;

import com.osserp.core.dao.telephone.SfaTelephoneGroups;
import com.osserp.core.model.telephone.SfaTelephoneGroupImpl;
import com.osserp.core.telephone.SfaTelephoneGroup;
import com.osserp.core.telephone.TelephoneGroup;

/**
 * 
 * @author so <so@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public class SfaTelephoneGroupsDao extends AbstractHibernateAndSpringDao implements SfaTelephoneGroups {
    private static Logger log = LoggerFactory.getLogger(SfaTelephoneGroupsDao.class.getName());

    public SfaTelephoneGroupsDao() {
        super();
    }

    public SfaTelephoneGroupsDao(JdbcTemplate jdbcTemplate, Tables tables, SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
    }

    public void synchronize(TelephoneGroup group) {
        if (log.isDebugEnabled()) {
            log.debug("synchronize() invoked for TelephoneGroup [id=" + group.getId() + "]");
        }
        SfaTelephoneGroup sfatg = fetchSfaTelephoneGroup(group.getId());
        if (sfatg != null) {
            if (group.isEndOfLife()) {
                sfatg.setUnused(true);
            } else {
                sfatg.setUnused(false);
            }
            sfatg.setName(group.getName());
            sfatg.setSynced(false);
        } else {
            sfatg = new SfaTelephoneGroupImpl(group.getTelephoneSystem().getId(), group, group.getName(), group.isEndOfLife());
        }
        getCurrentSession().saveOrUpdate(SfaTelephoneGroupImpl.class.getName(), sfatg);
    }

    public SfaTelephoneGroup findByTelephoneGroup(Long telephoneGroupId) {
        if (log.isDebugEnabled()) {
            log.debug("findByTelephoneGroup() invoked [telephoneGroupId=" + telephoneGroupId + "]");
        }
        if (telephoneGroupId != null) {
            return fetchSfaTelephoneGroup(telephoneGroupId);
        }
        return null;
    }

    private SfaTelephoneGroup fetchSfaTelephoneGroup(Long telephoneGroupId) {
        if (log.isDebugEnabled()) {
            log.debug("fetchSfaTelephoneGroup() invoked [telephoneGroupId=" + telephoneGroupId + "]");
        }
        List<SfaTelephoneGroup> list = getCurrentSession().createQuery(
                "from " + SfaTelephoneGroupImpl.class.getName() 
                + " s where s.telephoneGroup.id = :groupid")
                .setParameter("groupid", telephoneGroupId).list();
        return (list.isEmpty() ? null : list.get(0));
    }
}
