<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="productListClassificationConfiguratorView"/>
<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="headline">
        <fmt:message key="${view.headerName}"/>
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation/>
    </tiles:put>

    <tiles:put name="content" type="string" >
        <div class="content-area" id="purchaseOrderListContent">
            <div class="row">
                <div class="col-md-12 panel-area">
                    <div class="table-responsive table-responsive-default">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th class="id"><fmt:message key="productNo"/></th>
                                    <th class="name"><fmt:message key="name"/></th>
                                    <th class="type"><fmt:message key="type"/></th>
                                    <th class="group"><fmt:message key="group"/></th>
                                    <th class="category"><fmt:message key="category"/></th>
                                    <th class="action center"><fmt:message key="action"/></th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:choose>
                                    <c:when test="${empty view.list}">
                                        <tr><td colspan="4" class="error empty"><fmt:message key="noProductsFound"/></td></tr>
                                    </c:when>
                                    <c:otherwise>
                                        <c:forEach var="product" items="${view.list}">
                                           <tr>
                                               <td class="id"><v:link url="/products.do?method=load&id=${product.productId}&exit=productListClassificationConfiguratorDisplay"><o:out value="${product.productId}"/></v:link></td>
                                               <td class="name">
                                                   <v:link url="/products.do?method=load&id=${product.productId}&exit=productListClassificationConfiguratorDisplay">
                                                       <c:choose><c:when test="${product.endOfLife}"><span style="text-decoration:underline;"><o:out value="${product.name}"/></span></c:when><c:otherwise><o:out value="${product.name}"/></c:otherwise></c:choose>
                                                   </v:link>
                                               </td>
                                               <td class="type"><o:out value="${product.typeDisplay}"/></td>
                                               <td class="group"><o:out value="${product.group.name}"/></td>
                                               <td class="category"><o:out value="${product.category.name}"/></td>
                                               <td class="action center">
                                                   <a href="<c:url value="/products.do?method=load&id=${product.productId}&exit=productClassificationConfigReload"/>">
                                                       <span title="<fmt:message key="removeEntryFromList"/>"><o:img name="writeIcon"/></span>
                                                   </a>
                                               </td>
                                           </tr>
                                        </c:forEach>
                                    </c:otherwise>
                                </c:choose>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
