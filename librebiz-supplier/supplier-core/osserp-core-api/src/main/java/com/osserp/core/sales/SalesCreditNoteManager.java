/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 29, 2006 10:47:05 AM 
 * 
 */
package com.osserp.core.sales;

import java.util.Date;

import com.osserp.core.employees.Employee;
import com.osserp.core.finance.CreditNote;
import com.osserp.core.finance.CreditNoteManager;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SalesCreditNoteManager extends CreditNoteManager, DebitRecordCreator {

    /**
     * Creates a new credit note by existing
     * @param user creating the credit note
     * @param existing credit note to copy from
     * @param copyReference indicates if business case references should also be copied
     * @param userDefinedId
     * @param userDefinedDate
     * @param historical
     * @return creditNote new created
     */
    CreditNote create(Employee user, CreditNote existing, boolean copyReference, Long userDefinedId, Date userDefinedDate, boolean historical);

    /**
     * Creates a relation to existing invoice
     * @param user
     * @param record
     * @param invoiceId
     * @return creditNote with updated relation
     */
    CreditNote createInvoiceRelation(Employee user, CreditNote record, Long invoiceId);

    /**
     * Changes the sales commission status of given record
     * @param creditNote where to change the status
     */
    void changeSalesCommissionStatus(CreditNote creditNote);

    /**
     * Forces create or delete of creditNote deliveryNote booking record
     * @param user
     * @param creditNote with unchangeable status
     */
    void toggleDeliveryBooking(Employee user, CreditNote creditNote);

    /**
     * Toggles credit note requires delivery flag
     * @param creditNote where to delivery required flag should be changed
     */
    void toggleDeliveryRequiredFlag(CreditNote creditNote);
}
