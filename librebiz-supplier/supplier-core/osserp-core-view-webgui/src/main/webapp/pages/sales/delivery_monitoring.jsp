<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.deliveryMonitoringView}">
    <o:logger write="delivery_monitoring.jsp no view bound" level="warn"/>
    <c:redirect url="/errors/error_context.jsp"/>
</c:if>
<c:set var="view" value="${sessionScope.deliveryMonitoringView}"/>
<c:if test="${view.monitoringBySalesMode}">
    <c:redirect url="/pages/sales/delivery_monitoring_by_sales.jsp"/>
</c:if>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title"><o:displayTitle/></tiles:put>
    <tiles:put name="headline"><fmt:message key="monitoringOfDatesDelivery"/></tiles:put>
    <tiles:put name="headline_right">
        <ul>
            <li><a href="<c:url value="/salesDeliveryMonitoring.do?method=exit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a></li>
            <li><a href="<c:url value="/salesDeliveryMonitoring.do?method=refresh"/>" title="<fmt:message key="listRefresh"/>"><o:img name="replaceIcon"/></a></li>
            <c:choose>
                <c:when test="${view.snipListMode}">
                    <li><a href="<c:url value="/salesDeliveryMonitoring.do?method=toggleSnipListMode"/>" title="<fmt:message key="disableSnipFinishedMode"/>"><o:img name="cutIcon"/></a></li>
                </c:when>
                <c:otherwise>
                    <li><a href="<c:url value="/salesDeliveryMonitoring.do?method=toggleSnipListMode"/>" title="<fmt:message key="enableSnipFinishedMode"/>"><o:img name="enabledIcon"/></a></li>
                </c:otherwise>
            </c:choose>
            <li><a href="<c:url value="/salesDeliveryMonitoring.do?method=toggleMonitoringBySalesMode"/>" title="<fmt:message key="listGroupedBySales"/>"><o:img name="reloadIcon"/></a></li>
            <li><v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link></li>
        </ul>
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="deliveryMonitoringContent">
            <div class="row">
                <div class="col-md-12 panel-area">
                    <div class="table-responsive table-responsive-default">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th class="recordNumber"><fmt:message key="order"/></th>
                                    <th class="recordName"><fmt:message key="commission"/></th>
                                    <th class="recordEmployeeKey">T</th>
                                    <th class="recordDate">
                                        <a href="<c:url value="/salesDeliveryMonitoring.do?method=toggleListDescendantMode"/>" title="<fmt:message key="${view.listDescendantMode ? 'sortUp' : 'sortDown'}"/>"><fmt:message key="deliveryDayShort"/></a>
                                    </th>
                                    <th class="recordDate"><fmt:message key="installationAppointmentShort"/></th>
                                    <th class="recordProductName"><fmt:message key="product"/></th>
                                    <th class="recordQuantity"><fmt:message key="numberShort"/></th>
                                    <th class="recordAmount"><fmt:message key="value"/></th>
                                    <th class="recordStatus"><fmt:message key="statusPercent"/></th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="obj" items="${view.list}" varStatus="s">
                                    <tr>
                                        <td class="recordNumber"><a href="<c:url value="/loadSales.do?exit=salesDeliveryMonitoring&id=${obj.id}"/>"><o:out value="${obj.id}"/></a></td>
                                        <td class="recordName">
                                            <c:if test="${view.snipListMode}">
                                                <span style="margin: 0 3px;">
                                                    <a href="<c:url value="/salesDeliveryMonitoring.do"><c:param name="method" value="snipList"/><c:param name="id" value="${obj.id}"/></c:url>" title="<fmt:message key="title.record.delete"/>"><o:img name="deleteIcon" styleClass="bigicon"/></a>
                                                </span>
                                            </c:if>
                                            <o:out value="${obj.name}" limit="45"/>
                                        </td>
                                        <td class="recordEmployeeKey">
                                            <o:ajaxLink linkId="managerDisplay" url="${'/employeeInfo.do?id='}${obj.managerId}">
                                                <oc:employee initials="true" value="${obj.managerId}" />
                                            </o:ajaxLink>
                                        </td>
                                        <td class="recordDate">
                                            <o:date value="${obj.deliveryDate}" addcentury="false"/>
                                        </td>
                                        <td class="recordDate">
                                            <o:date value="${obj.installationDate}" addcentury="false"/>
                                        </td>
                                        <td class="recordProductName">
                                            <a href="<c:url value="/salesPlanning.do?method=forwardProductPlanning&exit=salesDeliveryMonitoring&id=${obj.productId}&stockId=${view.selectedStock}"/>">
                                                <span title="${obj.productId}"><o:out value="${obj.productName}"/></span>
                                            </a>
                                        </td>
                                        <td class="recordQuantity right">
                                            <v:ajaxLink url="/sales/deliveryStatusDisplay/forward?id=${obj.id}" linkId="deliveryStatus" title="deliveryStatus">
                                                <o:number format="integer" value="${obj.outstanding}"/>
                                            </v:ajaxLink>
                                        </td>
                                        <td class="recordAmount">
                                            <o:number format="currency" value="${obj.capacity}"/>
                                        </td>
                                        <td class="recordStatus">
                                            <v:ajaxLink url="/sales/businessCaseDisplay/forward?id=${obj.id}" linkId="businessCaseDisplayView">
                                                <o:out value="${obj.status}"/>
                                            </v:ajaxLink>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
