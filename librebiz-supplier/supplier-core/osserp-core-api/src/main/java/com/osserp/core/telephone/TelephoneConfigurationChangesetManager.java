/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Oct 13, 2010 11:59:27 AM 
 * 
 */
package com.osserp.core.telephone;

import java.util.Map;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public interface TelephoneConfigurationChangesetManager {

    /**
     * Creates a new telephoneConfigurationChangeset
     * @param createdBy
     * @param referenceId
     * @param property
     * @param oldValue
     * @param newValue
     * @return telephoneConfigurationChangeset
     */
    TelephoneConfigurationChangeset create(Long createdBy, Long referenceId, String property, String oldValue, String newValue);

    /**
     * Tries to find telephoneConfigurationChangeset by telephoneConfiguration id
     * @param TelephoneConfigurationId
     * @return map of telephoneConfiguration changesets
     */
    Map<String, TelephoneConfigurationChangeset> getLatestChangesetsByConfigurationId(Long cId);

}
