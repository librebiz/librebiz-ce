/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 3, 2004 
 * 
 */
package com.osserp.common.web.tags;

import java.util.Date;

import javax.servlet.jsp.JspException;

import com.osserp.common.util.DateFormatter;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class DateTag extends AbstractDateTag {

    @Override
    public String getOutput() throws JspException {
        Date toOutput = dateFromValue();
        if (toOutput == null && isDatePartRequest()) {
            return getDatePartRequest(null);
        }
        if (toOutput == null && current) {
            toOutput = new Date(System.currentTimeMillis());
        }
        return getDate(toOutput);
    }

    protected final String getDate(Date date) {
        if (isDatePartRequest()) {
            return getDatePartRequest(date);
        }
        if (date == null) {
            return (casenull == null) ? "" : casenull;
        }
        StringBuilder result = new StringBuilder(getDate(DateFormatter.getDate(date)));
        if (addtime) {
            result.append(" ").append(getTime(date));
        }
        return result.toString();
    }

    private String getDate(String date) {
        if (format != null && format.length() > 0) {

            if (format.equals("noyear")) {
                return getWithoutYear(date);
            }
        }
        if (!addcentury) {
            return getWithoutCentury(date);
        }
        return date;
    }

    private String getWithoutCentury(String date) {
        StringBuilder b = new StringBuilder(date.substring(0, 6)).append(date.substring(8, 10));
        return b.toString();
    }

    private String getWithoutYear(String date) {
        StringBuilder b = new StringBuilder(date.substring(0, 5));
        return b.toString();
    }

    private Date getSaveDate(Date date) {
        return date == null ? new Date(System.currentTimeMillis()) : date;
    }

    private boolean isDatePartRequest() {
        return (format != null && format.length() > 0)
                && (format.equals("year") || format.equals("week") 
                        || format.equals("psyear"));
    }

    private String getDatePartRequest(Date date) {
        if (format != null && format.equals("psyear")) {
            return "2001";
        }
        if (format != null && format.equals("year")) {
            return DateFormatter.getYear(getSaveDate(date));
        }
        if (format != null && format.equals("week")) {
            return DateFormatter.getWeekOfYear(getSaveDate(date));
        }
        return "";
    }
}
