<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="passwordChange"/> - <o:out value="${view.bean.values['uid']}"/>
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">
            <v:form name="dynamicForm" url="${view.baseLink}/save">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for=""><fmt:message key="password"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="password" class="form-control" name="password" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for=""><fmt:message key="repeatPassword"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="password" class="form-control" name="confirmPassword" />
                        </div>
                    </div>
                </div>
                <div class="row next">
                    <div class="col-md-4"></div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <v:submitExit url="${view.baseLink}/disablePasswordEditMode" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <o:submit  styleClass="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </v:form>
        </div>
    </div>
</div>
