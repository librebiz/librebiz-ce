<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="column15l">
    <div class="subcolumnl">
        <div class="spacer"></div>
    </div>
</div>
<div class="column66l">
    <div class="subcolumnc">
        <div class="spacer"></div>
                
        <v:form name="taxReportForm" url="/accounting/taxReport/save">
            <div class="contentBox">
                <div class="contentBoxHeader">
                    <div class="contentBoxHeaderLeft">
                        <fmt:message key="newTaxReport"/>
                    </div>
                </div>
                <div class="contentBoxData">
                    <div class="subcolumns">
                        <div class="subcolumn">
                            <div class="spacer"></div>
                                    
                            <table class="valueTable first33 inputs200">
                                <c:import url="/errors/_errors_table_label_value.jsp"/>
                                <tr>
                                    <td><fmt:message key="company"/></td>
                                    <td><o:out value="${view.selectedCompany.contact.displayName}"/></td>
                                </tr>
                                <tr>
                                    <td><fmt:message key="name"/></td>
                                    <td><v:text name="name" style="width:200px;" /></td>
                                </tr>
                                <tr>
                                    <td><fmt:message key="fromDate"/></td>
                                    <td><v:date name="start" picker="true" /></td>
                                </tr>
                                <tr>
                                    <td><fmt:message key="til"/></td>
                                    <td><v:date name="stop" picker="true" /></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><o:submit name="createReport"/></td>
                                </tr>
                            </table>
                            <div class="spacer"></div>
                        </div>
                    </div>
                </div>
                <div class="spacer"></div>
            </div>
        </v:form>
        <div class="spacer"></div>
    </div> <!-- subcolumnc -->
</div>
<div class="column15r">
    <div class="subcolumnr">
        <div class="spacer"></div>
    </div>
</div>
