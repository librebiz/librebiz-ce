<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>

<c:set var="view" value="${requestScope.interestView}" />
<c:set var="contact" value="${view.bean}" />

<div class="subcolumns">
    <div class="subcolumn">
        <div class="spacer"></div>
        <div class="contentBox">
            <div class="contentBoxData">
                <div class="spacer"></div>
                <o:form name="contactForm" url="/interests.do" onsubmit="return ojsForms.validateContactForm(this);">
                    <input type="hidden" name="method" value="save" />
                    <div class="subcolumns">
                        <div class="column50l">
                            <div class="subcolumnl">
                                <table class="valueTable inputs230">
                                    <tr>
                                        <td><fmt:message key="company" /> / <fmt:message key="institution" /></td>
                                        <td><input type="text" name="company" value="<o:out value="${contact.company}"/>" /></td>
                                    </tr>
                                    <tr>
                                        <td><fmt:message key="salutation" /> / <fmt:message key="title" /></td>
                                        <td><oc:select name="salutation" value="${contact.salutation}" options="salutations" style="width: 80px;" prompt="false" /> <oc:select name="title" value="${contact.title}" options="titles" style="width: 151px;" /></td>
                                    </tr>
                                    <tr>
                                        <td><fmt:message key="firstName" /></td>
                                        <td><input type="text" name="firstName" value="<o:out value="${contact.firstName}"/>" /></td>
                                    </tr>
                                    <tr>
                                        <td><fmt:message key="lastName" /></td>
                                        <td><input type="text" name="lastName" value="<o:out value="${contact.lastName}"/>" /></td>
                                    </tr>
                                    <tr>
                                        <td><fmt:message key="street" /></td>
                                        <td><input type="text" name="street" value="<o:out value="${contact.address.street}"/>" /></td>
                                    </tr>
                                    <tr>
                                        <td><fmt:message key="zipcode" /> / <fmt:message key="city" /></td>
                                        <td><input type="text" name="zipcode" value="<o:out value="${contact.address.zipcode}"/>" style="width: 70px;" /> <input type="text" name="city" value="<o:out value="${contact.address.city}"/>" style="width: 153px;" /></td>
                                    </tr>
                                    <tr>
                                        <td><fmt:message key="country" /></td>
                                        <td><oc:select name="countryId" value="${contact.address.country}" options="countryNames" prompt="false" /></td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                        <div class="column50r">
                            <div class="subcolumnr">
                                <table class="valueTable inputs230">
                                    <c:set var="contact" scope="request" value="${contact}" />
                                    <c:import url="/pages/contacts/shared/contact_data_phones_create.jsp" />
                                    <c:import url="/pages/contacts/shared/contact_data_email_create.jsp" />
                                    <tr>
                                        <td style="vertical-align: top; width: 104px;"><fmt:message key="internalNoteLabel" /></td>
                                        <td><textarea class="form-control" name="note" rows="2"><o:out value="${contact.comment}" /></textarea></td>
                                    </tr>
                                    <tr>
                                        <td class="row-submit"></td>
                                        <td class="row-submit"><o:submit /></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </o:form>
                <div class="spacer"></div>
            </div>
        </div>
    </div>
</div>