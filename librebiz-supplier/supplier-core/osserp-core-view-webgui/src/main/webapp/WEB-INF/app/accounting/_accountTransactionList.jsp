<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-12 panel-area">

    <div class="table-responsive table-responsive-default">
        <table class="table">
            <thead>
                <tr>
				    <th class="recordType"><fmt:message key="type" /></th>
                    <th class="recordDate"><fmt:message key="payment"/></th>
				    <th class="recordName"><fmt:message key="name" /></th>
				    <th class="recordNote"><fmt:message key="description" /></th>
				    <th class="recordAmount"><fmt:message key="amount" /></th>
                    <o:permission role="accounting_documents,accounting_bank_documents,accounting_accountant_deny,executive_accounting,executive" info="permissionEditFinanceRecords">
                    <th colspan="3"> </th>
                    </o:permission>
                    <o:forbidden role="accounting_documents,accounting_bank_documents,accounting_accountant_deny,executive_accounting,executive" info="permissionEditFinanceRecords">
                    <th colspan="2"> </th>
                    </o:forbidden>
                </tr>
            </thead>
            <tbody>
                <c:choose>
				    <c:when test="${empty view.list or view.missingContactByBookingType}">
                        <tr>
                            <td colspan="7">
                                <c:choose>
                                    <c:when test="${!empty sessionScope.errors}">
                                        <span class="error"><o:out value="${sessionScope.errors}"/></span>
                                        <span style="margin-left: 20px;"> 
                                            <v:link url="/contacts/contactSearch/forward?exit=/accounting/accountTransaction/reload">[<fmt:message key="contactSelection" />]</v:link>
                                        </span>
                                        <o:removeErrors/>
                                    </c:when>
                                    <c:otherwise>
                                        <span><fmt:message key="noRecordsAvailable" /></span> 
                                        <span style="margin-left: 20px;"> 
                                            <v:link url="/accounting/accountTransaction/enableCreateMode">[<fmt:message key="create" />]</v:link>
                                        </span>
                                    </c:otherwise>
                                </c:choose>
                            </td>
                        </tr>
                    </c:when>
				    <c:otherwise>
                        <c:forEach var="record" items="${view.list}" varStatus="s">
                            <tr<c:if test="${record.outflowOfCash}"> class="altrow"</c:if>>
                                <td class="recordType"><o:out value="${record.type.name}"/></td>
                                <td class="recordDate">
                                    <c:choose>
                                        <c:when test="${empty record.paid}">
                                            <fmt:message key="open" />
                                        </c:when>
                                        <c:when test="${empty record.amount}">
                                            <span class="error" title="<fmt:message key="dueToThe"/>">
                                                <o:date value="${record.paid}" />
                                            </span>
                                        </c:when>
                                        <c:otherwise>
                                            <o:date value="${record.paid}" />
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td class="recordName">
                                    <c:choose>
                                        <c:when test="${view.supplierMode}">
                                            <o:out value="${record.contact.displayName}" />
                                        </c:when>
                                        <c:when test="${view.filterByContactEnabled}">
                                            <v:link url="/accounting/accountTransaction/filterByContact" title="displayAll">
                                                <o:out value="${record.contact.displayName}" />
                                            </v:link>
                                        </c:when>
                                        <c:otherwise>
                                            <v:link url="/accounting/accountTransaction/filterByContact?id=${record.contactId}" title="displayContactBookingsOnly">
                                                <o:out value="${record.contact.displayName}" />
                                            </v:link>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td class="recordNote">
                                    <v:link url="/accounting/commonPayment/forward?id=${record.id}&exit=/accounting/accountTransaction/reload">
                                        <o:out value="${record.note}" limit="36" />
                                    </v:link>
                                </td>
                                <td class="recordAmount">
                                    <c:choose>
                                        <c:when test="${record.amount == 0}">
                                            <span class="error" title="<fmt:message key="dueAmount"/>">
                                                <o:number value="${record.amountToPay}" format="currency" />
                                            </span>
                                        </c:when>
                                        <c:otherwise>
                                            <o:number value="${record.amount}" format="currency" />
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td class="icon center">
                                    <o:permission role="delete_payments">
                                        <a href="javascript:onclick=confirmLink('<fmt:message key="irrevocableAction"/>:\n<fmt:message key="confirmRecordDelete"/>','<v:url value="/accounting/accountTransaction/delete?id=${record.id}"/>');" title="<fmt:message key="paymentDelete"/>">
                                            <o:img name="deleteIcon"/>
                                        </a>
                                    </o:permission>
                                    <o:forbidden role="delete_payments">
                                        <span title="<fmt:message key="deletePermissionRequired"/>"><o:img name="disabledIcon"/></span>
                                    </o:forbidden>
                                </td>
                                <o:permission role="accounting_documents,accounting_bank_documents,accounting_accountant_deny,executive_accounting,executive" info="permissionEditFinanceRecords">
                                    <td class="icon center">
                                        <v:link url="/accounting/accountTransaction/copy?id=${record.id}" title="copyDataset">
                                            <o:img name="copyIcon"/>
                                        </v:link>
                                    </td>
                                </o:permission>
                                <td class="icon center">
                                    <c:if test="${!empty record.documentId}">
                                        <v:link url="/dms/document/print?id=${record.documentId}" title="documentPrint"><o:img name="printIcon" /></v:link>
                                    </c:if>
                                </td>
                            </tr>
	       				</c:forEach>
			     	</c:otherwise>
                </c:choose>
            </tbody>
        </table>
    </div>
</div>