/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 29-Jan-2007 19:49:03 
 * 
 */
package com.osserp.core.model.records;

import java.math.BigDecimal;
import java.util.Date;

import com.osserp.common.Option;

import com.osserp.core.Item;
import com.osserp.core.customers.Customer;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.BillingType;
import com.osserp.core.finance.CancellableRecord;
import com.osserp.core.finance.Cancellation;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.Payment;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordInfo;
import com.osserp.core.finance.RecordType;
import com.osserp.core.products.Product;
import com.osserp.core.sales.SalesCreditNote;
import com.osserp.core.sales.SalesCreditNoteType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesCreditNoteImpl extends AbstractCreditNote implements SalesCreditNote {

    private boolean reducingCommission = true;
    private Cancellation cancellation = null;

    /**
     * Default constructor required by Serializable
     */
    protected SalesCreditNoteImpl() {
        super();
    }

    /**
     * Create a new, empty and invoice independent credit note
     * @param id
     * @param type
     * @param bookingType
     * @param company
     * @param branchId
     * @param customer
     * @param createdBy
     * @param businessCaseId
     * @param taxRate
     * @param reducedTaxRate
     * @param currency
     */
    public SalesCreditNoteImpl(
            Long id,
            RecordType type,
            SalesCreditNoteType bookingType,
            Long company,
            Long branchId,
            Customer customer,
            Employee createdBy,
            Long businessCaseId,
            Double taxRate,
            Double reducedTaxRate,
            Long currency) {
        super(
                id,
                company,
                branchId,
                type,
                bookingType,
                null,
                customer,
                createdBy,
                businessCaseId,
                taxRate,
                reducedTaxRate,
                currency);
        setInternal(customer.isInternal());
    }

    /**
     * Creates a new credit note by invoice
     * @param id
     * @param type
     * @param bookingType
     * @param createdBy
     * @param cancellable
     * @param copyItems
     */
    protected SalesCreditNoteImpl(
            Long id,
            RecordType type,
            SalesCreditNoteType bookingType,
            Employee createdBy,
            CancellableRecord cancellable,
            boolean copyItems) {
        super(id, cancellable, type, bookingType, createdBy, copyItems);
        setItemsChangeable(true);
        setItemsEditable(true);
    }

    /**
     * Creates a new credit note by another credit note
     * @param id
     * @param other
     * @param createdBy
     * @param itemsChangeable
     */
    public SalesCreditNoteImpl(Long id, SalesCreditNote other, Employee createdBy, boolean copyReference) {
        super(id, other, createdBy);
        setItemsChangeable(true);
        setItemsEditable(true);
        if (copyReference) {
            setReference(other.getReference());
        } else {
            setReference(null);
        }
    }

    public boolean isReducingCommission() {
        return reducingCommission;
    }

    public void setReducingCommission(boolean reducingCommission) {
        this.reducingCommission = reducingCommission;
    }

    public boolean isLinkedWithSale() {
        return (getReference() != null);
    }

    public void updateReference(Invoice invoice) {
        setReference(invoice.getId());
        if (isSet(invoice.getBusinessCaseId())) {
            setBusinessCaseId(invoice.getBusinessCaseId());
        }
    }

    public void cancel(Employee user, Cancellation cancellation) {
        setCanceled(true);
        setStatus(SalesCreditNote.STAT_CANCELED);
        this.cancellation = cancellation;
    }

    public Cancellation getCancellation() {
        return cancellation;
    }

    public void setCancellation(Cancellation cancellation) {
        this.cancellation = cancellation;
    }

    @Override
    public void updateStatus(Long status) {
        Long current = getStatus();
        if (current == null) {
            setStatus(status);
        } else if (Record.STAT_CANCELED.equals(current)
                && Record.STAT_SENT.equals(status)) {
            setStatus(status);
            setCanceled(false);
        } else {
            super.updateStatus(status);
        }
    }

    @Override
    protected Payment createCustomPayment(Employee user, BillingType type, BigDecimal amount, Date paid, String customHeader, String note, Long bankAccountId) {
        throw new IllegalStateException();
    }

    @Override
    protected Payment createPayment(Employee user, BillingType type, BigDecimal amount, Date paid, Long bankAccountId) {
        return new SalesPaymentImpl(
                this,
                type,
                paid,
                user,
                amount.multiply(new BigDecimal(-1)),
                bankAccountId);
    }

    @Override
    protected Item createItem(
            Long stockId,
            Product product,
            String customName,
            Double quantity,
            Double taxRate,
            BigDecimal price,
            Date priceDate,
            BigDecimal partnerPrice,
            boolean partnerPriceEditable,
            boolean partnerPriceOverridden,
            BigDecimal purchasePrice,
            String note,
            boolean includePrice,
            Long externalId) {

        return new SalesCreditNoteItemImpl(
                this,
                stockId,
                product,
                customName,
                quantity,
                price,
                priceDate,
                partnerPrice,
                partnerPriceEditable,
                partnerPriceOverridden,
                purchasePrice,
                taxRate,
                note,
                includePrice,
                getItems().size(),
                externalId);
    }

    @Override
    protected RecordInfo createInfo(Employee user, Option info) {
        return new SalesCreditNoteInfoImpl(this, info, user);
    }

    @Override
    public boolean isSales() {
        return true;
    }
}
