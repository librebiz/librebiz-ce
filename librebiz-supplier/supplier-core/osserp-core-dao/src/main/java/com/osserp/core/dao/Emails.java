/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 12, 2004 
 * 
 */
package com.osserp.core.dao;

import java.util.List;

import com.osserp.common.Option;
import com.osserp.common.Parameter;
import com.osserp.common.mail.EmailAddress;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Emails {

    /**
     * Provides all contact-email address relations
     */
    List<Option> findAll();

    /**
     * Provides a list of all contact-email addresses matching provided contactId
     * @param contactId
     * @return list
     */
    List<EmailAddress> findEmails(Long contactId);

    /**
     * Provides a list of all contact-email address relations matching provided email address
     * @param email
     * @return list
     */
    List<Option> findEqual(String email);

    /**
     * Provides all emails matching provided pattern
     * @param pattern
     * @param startsWith
     * @return list of matching email addresses as parameter values
     */
    List<Parameter> findMatchingEmails(String pattern, boolean startsWith);
}
