<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:form url="${view.baseLink}/save" name="purchaseInvoiceCreatorForm">

    <div class="col-md-6 panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>
                    <fmt:message key="purchaseInvoiceCreatorHeader" />
                </h4>
            </div>
        </div>
        <div class="panel-body">

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="client"><fmt:message key="client" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:out value="${view.selectedBranch.company.name}" />
                    </div>
                </div>
            </div>

            <c:if test="${!empty view.supplier}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="supplier"><fmt:message key="supplier" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${view.supplier.displayName}" />
                        </div>
                    </div>
                </div>
            </c:if>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="recordType"><fmt:message key="recordType" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:out value="${view.selectedType.name}" />
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="recordType"> <span title="<fmt:message key="recordNumberSupplier" />"><fmt:message key="recordNumber" /></span>
                        </label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <v:text name="recordNumberSupplier" styleClass="form-control" title="recordNumberSupplier" />
                    </div>
                </div>
            </div>

            <c:if test="${!empty view.alreadyAvailable}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="supplier"><span title="<fmt:message key="recordNumberSupplier" />" class="error"><fmt:message key="alreadyExistingLabel" /></span></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <a href="<c:url value="/purchaseInvoice.do?method=display&readonly=false&id=${view.alreadyAvailable.id}&exit=${view.recordExitTarget}"/>"> <o:out value="${view.alreadyAvailable.supplierReferenceNumber}" /> - <o:out
                                    value="${view.alreadyAvailable.number}" /> / <o:date value="${view.alreadyAvailable.initialDate}" />
                            </a>
                        </div>
                    </div>
                </div>
            </c:if>

            <v:date name="recordDateSupplier" label="invoiceDate" picker="true" styleClass="form-control" />

            <c:if test="${view.manualNumberInputAvailable or view.manualDateInputAvailable}">
                <div class="row next">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="optionallyDataLabelArea"></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <fmt:message key="optionallyDataLabel" />
                        </div>
                    </div>
                </div>

                <c:if test="${view.manualDateInputAvailable}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="entryDate"><fmt:message key="entryDate" /></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <div class="radio">
                                    <label> <input type="radio" name="recordDateSupplierDate" value="" <c:if test="${view.purchaseInvoiceRecordDateCurrent}">checked="checked"</c:if> /> <fmt:message key="currentDateSelectText" />
                                    </label>
                                </div>
                                <div class="radio">
                                    <label> <input type="radio" name="recordDateSupplierDate" value="true" <c:if test="${!view.purchaseInvoiceRecordDateCurrent}">checked="checked"</c:if> /> <fmt:message key="accordingToInvoiceDate" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <v:date name="recordDate" label="individualDate" picker="true" styleClass="form-control" />

                </c:if>

                <c:if test="${view.manualNumberInputAvailable}">

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="purchaseInvoiceNumberLabel"><fmt:message key="purchaseInvoiceNumberLabel" /></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <v:text name="recordNumber" styleClass="form-control" />
                            </div>
                        </div>
                    </div>
                </c:if>
            </c:if>

            <div class="row next">
                <div class="col-md-4"></div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <v:submitExit url="${view.baseLink}/exit" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <o:submit styleClass="form-control" value="create" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</v:form>
