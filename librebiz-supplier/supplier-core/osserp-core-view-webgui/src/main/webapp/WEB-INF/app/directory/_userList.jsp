<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-12 panel-area">
    <div class="table-responsive table-responsive-default">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th class="uidCol">uid</th>
                    <th class="uidNumberCol">uidNumber</th>
                    <th class="displayNameCol">displayName</th>
                    <th class="mailCol">mail</th>
                    <th class="center">action</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="account" items="${view.list}">
                    <c:if test="${'hidden' != account.values['employeetype']}">
                        <tr>
                            <td>
                                <a href="${view.selectLink.link}?id=${account.id}"><o:out value="${account.values['uid']}"/></a>
                            </td>
                            <td><o:out value="${account.values['uidnumber']}"/></td>
                            <td><o:out value="${account.values['displayname']}"/></td>
                            <td><o:out value="${account.values['mail']}"/></td>
                            <td class="icon center">
                                <a href="${view.selectLink.link}?id=${account.id}"><o:img name="enabledIcon"/></a>
                            </td>
                        </tr>
                    </c:if>
                </c:forEach>
            </tbody>
        </table>
    </div>
</div>
