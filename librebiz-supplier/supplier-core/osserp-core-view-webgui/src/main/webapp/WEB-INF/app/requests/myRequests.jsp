<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="myRequestsView"/>
<c:set var="requestListingView" scope="request" value="${sessionScope.myRequestsView}"/>
<c:set var="requestSelectExitTarget" scope="request" value="requestsByEmployeeReload"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title"><o:displayTitle/></tiles:put>
    <tiles:put name="styles" type="string">
        <style type="text/css">
        </style>
    </tiles:put>
    <tiles:put name="onload" type="string">onload="$('value').focus();"</tiles:put>
    <tiles:put name="headline">
        <fmt:message key="myRequests"/>
        <span> [<o:listSize value="${view.list}"/>]</span>
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation/>
    </tiles:put>
    <tiles:put name="content" type="string">
        <div class="content-area" id="myRequestsContent">
            <div class="row">
                <c:import url="_requestQueryResultFull.jsp"/>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
