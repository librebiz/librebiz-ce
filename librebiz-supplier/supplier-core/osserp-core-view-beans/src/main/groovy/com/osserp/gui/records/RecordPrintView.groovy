/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 29, 2016 
 * 
 */
package com.osserp.gui.records

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ErrorCode
import com.osserp.common.dms.DmsDocument
import com.osserp.common.dms.DmsManager
import com.osserp.common.dms.DmsReference
import com.osserp.common.dms.DocumentData

import com.osserp.core.finance.Record
import com.osserp.core.finance.RecordPrintManager

import com.osserp.groovy.web.ViewContextException

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class RecordPrintView extends AbstractRecordView {
    private static Logger logger = LoggerFactory.getLogger(RecordPrintView.class.getName())

    Record record
    DmsDocument dmsDocument
    DocumentData documentData
    Object recordView
    
    RecordPrintView() {
        super()
    }

    @Override    
    boolean isForwardRequest() {
        requestContext.method == 'print' || requestContext.method == 'release'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest || !recordManager) {

            if (viewReference && !form.getLong('id')) {
                // invocation by 'view' parameter providing existing recordView
                record = viewReference.record
                if (!recordManager) {
                    setRecordManager(viewReference.recordManager)
                }
                if (viewReference.document &&
                        (record?.historical || !record?.type?.supportingPrint)) {
                    dmsDocument = viewReference.document
                }
                
                logger.debug("initRequest: init by view param [record=${record?.id}, name=${viewReferenceName}]")
                
            } else {
                // invocation by 'name' and 'id' parameter  
                record = fetchRecordByRequestId()
                if (record && (record.historical || !record.type.supportingPrint)) {
                    dmsDocument = getReferencedDocument(record)
                    if (!dmsDocument && documentAvailable(record)) {
                        logger.debug('initRequest: document(s) exist but non declared as reference')
                        throw new ViewContextException(ErrorCode.DOCUMENT_NOT_ACTIVATED)
                    }
                } else {
                    // reset existing document  
                    dmsDocument = null
                }
                logger.debug("initRequest: init by id param [record=${record?.id}, historical=${record?.historical}, supportingPrint=${record?.type?.supportingPrint}]")
            }
            // TODO add method to provide archived record document by id and type
            // This replaces /recordPdfDisplay used by event_configs 700003 to display a deleted record
            if (!record) {
                logger.warn('initRequest: record not found')
                throw new ViewContextException(ErrorCode.INVALID_CONTEXT)
            }
        }
    }
    
    DocumentData getDocument() {
        if (dmsDocument) {
            logger.debug("getDocument: imported document found [id=${dmsDocument.id}, reference=${dmsDocument.reference}]")
            return dmsManager.getDocumentData(dmsDocument)
        }
        if (record.unchangeable) {
            logger.debug('getDocument: unchangeable record found, trying to get existing pdf...')
            return recordPrintManager.getPdf(record)
        } 
        logger.debug('getDocument: no document found, creating...')
        DocumentData result = recordPrintManager.createPdf(
            domainEmployee, record, Record.STAT_PRINT)
        reloadViewReference()
        return result
    }
        
    void release() {
        documentData = recordPrintManager.createPdf(
            domainEmployee, record, record?.type?.releasedStatus)
        logger.debug("release: done [size=${documentData?.contentLength}]")
        reloadViewReference()
    }

    protected boolean isReferencedDocumentLookup() {
        (record?.historical || record.bookingType)
    }
    
    protected boolean documentAvailable(Record record) {
        !dmsManager.findByReference(new DmsReference(record.type.documentTypeId, record.id)).empty
    }

    protected DmsDocument getReferencedDocument(Record record) {
        DmsDocument result
        if (record && record?.type?.documentTypeId) {
            result = dmsManager.findReferenced(new DmsReference(record.type.documentTypeId, record.id))
            logger.debug("getReferencedDocument: lookup done [found=" + (result != null) + "]")
        } else {
            logger.debug("getReferencedDocument: no lookup performed [record=${record?.id}, documentType=${record?.type?.documentTypeId}]")
        }
        return result
    }
    
    protected final DmsManager getDmsManager() {
        getService(DmsManager.class.getName())
    }

    protected RecordPrintManager getRecordPrintManager() {
        getService(RecordPrintManager.class.getName())
    }
}
