<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>

<c:if test="${!empty requestScope.timeRecordingConfig}">
    <c:set var="config" value="${requestScope.timeRecordingConfig}"/>
    <div class="modalBoxHeader">
        <div class="modalBoxHeaderLeft">
            <o:out value="${config.id}"/> - <o:out value="${config.name}"/>
        </div>
    </div>
    <div class="modalBoxData">
        <div class="col-md-12 panel-area panel-area-default">
            <div class="panel-body">
                <div class="form-body">

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="name"/>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <o:out value="${config.name}"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="description"/>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <o:out value="${config.description}"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="annualLeave"/>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <o:out value="${config.annualLeave}"/> <fmt:message key="daysLabel"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="hoursPerDayLabel"/>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <o:out value="${config.hoursDaily}"/> <fmt:message key="hourShort"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="maximumCarryoverHoursLabel"/>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <o:out value="${config.carryoverHoursMax}"/> <fmt:message key="hourShort"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="coreTime"/>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <o:out value="${config.coreTimeStartHours}"/>:<o:out value="${config.coreTimeStartMinutes}"/> <fmt:message key="until"/> <o:out value="${config.coreTimeEndHours}"/>:<o:out value="${config.coreTimeEndMinutes}"/> <fmt:message key="clock"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="hoursRecording"/>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <c:choose>
                                    <c:when test="${config.recordingHours}">
                                        <fmt:message key="bigYes"/>
                                    </c:when>
                                    <c:otherwise>
                                        <fmt:message key="bigNo"/>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</c:if>
