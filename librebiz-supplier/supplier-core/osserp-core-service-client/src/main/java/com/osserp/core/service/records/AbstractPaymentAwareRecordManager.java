/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 04-Feb-2007 17:18:07 
 * 
 */
package com.osserp.core.service.records;

import java.math.BigDecimal;
import java.util.Date;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.core.dao.Products;
import com.osserp.core.dao.records.PaymentAwareRecords;
import com.osserp.core.dao.records.Records;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Payment;
import com.osserp.core.finance.PaymentAwareRecord;
import com.osserp.core.finance.PaymentAwareRecordManager;
import com.osserp.core.finance.RecordType;
import com.osserp.core.system.SystemConfigManager;
import com.osserp.core.tasks.ProductPlanningCacheSender;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractPaymentAwareRecordManager extends
        AbstractRecordManager implements PaymentAwareRecordManager {

    public AbstractPaymentAwareRecordManager(
            Records records,
            SystemConfigManager systemConfigManager,
            Products products,
            ProductPlanningCacheSender productPlanningTask) {
        super(records, systemConfigManager, products, productPlanningTask);
    }

    public Payment addCustomPayment(
            Employee user,
            PaymentAwareRecord record,
            BigDecimal amount,
            Date paid,
            String customHeader,
            String note,
            Long bankAccountId) throws ClientException {
        Payment result = getPaymentAwareDao().addCustomPayment(
                user,
                record,
                amount,
                paid,
                customHeader,
                note,
                bankAccountId);
        return result;
    }

    public Payment addPayment(
            Employee user,
            PaymentAwareRecord record,
            Long paymentType,
            BigDecimal amount,
            Date paid,
            Long bankAccountId)
            throws ClientException {
        return getPaymentAwareDao().addPayment(
                user,
                record,
                paymentType,
                amount,
                paid,
                bankAccountId);
    }

    protected PaymentAwareRecords getPaymentAwareDao() {
        Records implementing = getDao();
        if (implementing instanceof PaymentAwareRecords) {
            return (PaymentAwareRecords) implementing;
        }
        throw new BackendException("Implementation of PaymentAwareRecords expected," +
                " found " + implementing.getClass().getName());
    }

    public void addCorrection(
            Employee user,
            PaymentAwareRecord record,
            String name,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long country,
            String note) throws ClientException {

        String addressName = name;
        if (isNotSet(addressName)) {
            addressName = record.getContact().getDisplayName();
        }
        record.addCorrection(user, addressName, street, streetAddon, zipcode, city, country, note);
        getDao().save(record);
    }
    
    protected final void validateCreate(RecordType recordType, Long customId, Date customDate, boolean historical) throws ClientException {
    	if (customId != null && getDao().exists(customId)) {
    		throw new ClientException(ErrorCode.ID_EXISTS);
    	}
    	if (historical) {
        	if (customId == null && (recordType == null || recordType.isNumberCreatorBySequence())) { 
      	        throw new ClientException(ErrorCode.ID_REQUIRED);
        	}
        	if (customDate == null) {
        		throw new ClientException(ErrorCode.DATE_MISSING);
        	}
    	}
    }

}
