/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jul 17, 2007 12:47:31 PM 
 * 
 */
package com.osserp.core.model.records;

import java.util.ArrayList;
import java.util.List;

import com.osserp.core.Item;
import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.CancellableRecord;
import com.osserp.core.finance.Cancellation;
import com.osserp.core.finance.RecordType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractCancellingRecord extends AbstractPaymentAwareRecord implements Cancellation {

    private Long cancelledType = null;

    /**
     * Default constructor required by Serializable
     */
    protected AbstractCancellingRecord() {
        super();
    }

    /**
     * Creates a new cancelling record by cancelled record
     * @param id
     * @param type
     * @param user
     * @param cancellable
     * @param copyItems
     * @param copyInvers
     */
    protected AbstractCancellingRecord(
            Long id,
            RecordType type,
            Employee user,
            CancellableRecord cancellable,
            boolean copyItems,
            boolean copyInvers) {
        super(
                id,
                cancellable.getCompany(),
                cancellable.getBranchId(),
                type,
                cancellable.getId(),
                cancellable.getContact(),
                user,
                cancellable.getBusinessCaseId(),
                cancellable.getAmounts().getTaxRate(),
                cancellable.getAmounts().getReducedTaxRate(),
                false,
                false);
        if (cancellable.getCurrency() != null) {
            setCurrency(cancellable.getCurrency());
        }
        if (cancellable.isTaxFree()) {
            setTaxFree(true);
            setTaxFreeId(cancellable.getTaxFreeId());
        }
        if (copyItems) {
            for (int i = 0, j = cancellable.getItems().size(); i < j; i++) {
                Item next = cancellable.getItems().get(i);
                if (copyInvers) {
                    addItem(next.getStockId(), next.getProduct(), next.getCustomName(),
                            next.getQuantity() * -1, next.getTaxRate(), next.getPrice(),
                            next.getPriceDate(), next.getPartnerPrice(), next.isPartnerPriceEditable(), 
                            next.isPartnerPriceOverridden(), next.getPurchasePrice(), next.getNote(), 
                            next.isIncludePrice(), next.getExternalId());
                } else {
                    addItem(next.getStockId(), next.getProduct(), next.getCustomName(), 
                            next.getQuantity(), next.getTaxRate(), next.getPrice(),
                            next.getPriceDate(), next.getPartnerPrice(), next.isPartnerPriceEditable(), 
                            next.isPartnerPriceOverridden(), next.getPurchasePrice(), next.getNote(), 
                            next.isIncludePrice(), next.getExternalId());
                }
            }
        }
        cancelledType = cancellable.getType().getId();
    }

    /**
     * Creates a new and empty cancelling record
     * @param id
     * @param company
     * @param branchId
     * @param type
     * @param reference
     * @param contact
     * @param createdBy
     * @param sale
     * @param taxRate
     * @param reducedTaxRate
     * @param itemsChangeable
     * @param itemsEditable
     */
    protected AbstractCancellingRecord(
            Long id,
            Long company,
            Long branchId,
            RecordType type,
            Long reference,
            ClassifiedContact contact,
            Employee createdBy,
            Long sale,
            Double taxRate,
            Double reducedTaxRate,
            boolean itemsChangeable,
            boolean itemsEditable) {
        super(id, company, branchId, type, reference, contact, createdBy, sale, taxRate,
                reducedTaxRate, itemsChangeable, itemsEditable);
    }

    public Long getCancelledType() {
        return cancelledType;
    }

    protected void setCancelledType(Long cancelledType) {
        this.cancelledType = cancelledType;
    }

    public boolean isRollinAwareItemAvailable() {
        if (!RecordType.SALES_DOWNPAYMENT.equals(cancelledType)) {
            List<Item> items = getItems();
            for (int i = 0, j = items.size(); i < j; i++) {
                Item next = items.get(i);
                if (next.isDeliveryNoteAffecting()) {
                    return true;
                }
            }
        }
        return false;
    }

    public List<Item> getRollinAwareItems() {
        List<Item> result = new ArrayList<Item>();
        if (!RecordType.SALES_DOWNPAYMENT.equals(cancelledType)) {
            List<Item> items = getItems();
            for (int i = 0, j = items.size(); i < j; i++) {
                Item next = items.get(i);
                if (next.isDeliveryNoteAffecting()) {
                    result.add(next);
                }
            }
        }
        return result;
    }
}
