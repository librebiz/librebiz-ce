<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <c:choose>
                    <c:when test="${!empty view.record.bookingType}"><o:out value="${view.record.bookingType.name}"/></c:when>
                    <c:otherwise><o:out value="${view.record.type.name}"/></c:otherwise>
                </c:choose> 
                <o:out value="${view.record.number}" /><span> / </span><o:date value="${view.record.created}" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        
        <div class="row next">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="invoiceNumberDisplay"><fmt:message key="type" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <v:link url="${view.baseLink}/selectPaymentType" title="selectType">
                        <span class="form-value"><o:out value="${view.selectedPaymentType.name}" /></span>
                    </v:link>
                </div>
            </div>
        </div>
        
        <div class="row next">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="invoiceAmountDisplay"><fmt:message key="invoiceAmountShort" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <span class="form-value"><o:number value="${view.recordAmount}" format="currency" /></span>
                    <o:out value="${view.record.currencyKey}" />
                </div>
            </div>
        </div>
        
        <div class="row next">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="dueAmount">
                        <fmt:message key="outstandingAmount" />
                    </label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <span id="dueAmount" class="form-value"><o:number value="${view.record.dueAmount}" format="currency"/></span>
                    <span> / </span><o:date current="true" />
                </div>
            </div>
        </div>
        
        <div class="row next">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="amount">
                        <fmt:message key="amount" />
                    </label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <v:number name="amount" styleClass="form-control" value="${view.record.dueAmount}" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <p style="padding-top:5px;">(<fmt:message key="includesTaxShort" />)</p>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row next">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="setAsPaid">
                        <fmt:message key="inputIsLabel" />
                    </label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <div class="radio">
                        <label> <input type="radio" name="cashDiscount" value="" checked="checked" /> <fmt:message key="paidAmount" />
                        </label>
                    </div>
                    <div class="radio">
                        <label> <input type="radio" name="cashDiscount" value="true" /> <fmt:message key="cashDiscountAmountDescriptive" />
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <div class="row next">
            <div class="col-md-4"></div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <v:submitExit url="${view.baseLink}/selectPaymentType" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <o:submit styleClass="form-control" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>
