/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 16-Aug-2005 11:24:27 
 * 
 */
package com.osserp.gui.web.struts;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.config.ExceptionConfig;

import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.ViewManager;
import com.osserp.gui.BusinessCaseView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ExceptionHandler extends AbstractExceptionHandler {

    private static Logger log = LoggerFactory.getLogger(ExceptionHandler.class.getName());

    @Override
    public ActionForward execute(
            Exception ex,
            ExceptionConfig ec,
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException {

        if (request.getRequestURI().indexOf("login") == -1) {

            String requestDump = RequestUtil.createRequestDump(request);
            if (log.isInfoEnabled()) {
                log.info("execute() invoked [message=" + ex.getMessage()
                        + ", class=" + ex.getClass().getName() + "]\n"
                        + requestDump + "\ntrying to send error report...", ex);
            }
            HttpSession session = request.getSession();
            StringBuffer buffer = new StringBuffer(512);
            buffer.append("\nDomain specific details:\n");
            try {
                if (session.getAttribute("businessCaseView") != null) {
                    BusinessCaseView businessCaseView = (BusinessCaseView) ViewManager.fetchView(session, BusinessCaseView.class);
                    if (businessCaseView != null && businessCaseView.isBeanAvailable()) {
                        if (businessCaseView.isSalesContext()) {
                            buffer.append("sales context [id=" + businessCaseView.getSales().getId());
                        } else {
                            buffer.append("request context [id=" + businessCaseView.getRequest().getRequestId());
                        }
                    } else {
                        buffer.append("no interesting context found");
                    }
                }
            } catch (Exception ignore) {
            }
            buffer.append("\n\n");
            log.error(buffer.toString());
            buffer
                    .append("Session dump: ").append(RequestUtil.createSessionDump(session))
                    .append("\n\nRequest dump:\n").append(requestDump);
            this.sendError(buffer, ex, ec, mapping, form, request, response);
        }
        return super.execute(ex, ec, mapping, form, request, response);
    }
}
