/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 29, 2009 9:49:05 AM 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.dao.Tables;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.dao.records.SalesInvoices;
import com.osserp.core.dao.records.SalesOrderVolumeExportsByCompany;
import com.osserp.core.dao.records.SalesOrders;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.Record;
import com.osserp.core.sales.SalesOrderVolumeExportConfig;
import com.osserp.core.sales.SalesOrderVolumeExportUtil;

/**
 * 
 * Fetches all not exported sales orders of configured company.
 * 
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesOrderVolumeExportsByCompanyDao extends AbstractSalesOrderVolumeExports implements SalesOrderVolumeExportsByCompany {
    private static Logger log = LoggerFactory.getLogger(SalesOrderVolumeExportsByCompanyDao.class.getName());

    private String customersTable = null;
    private String systemCompaniesTable = null;
    private String salesDeliveryNotesTable = null;

    protected SalesOrderVolumeExportsByCompanyDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            SalesOrders salesOrders,
            SalesInvoices salesInvoices) {
        super(jdbcTemplate, tables, sessionFactory, salesOrders, salesInvoices);
        this.systemCompaniesTable = getTable(TableKeys.SYSTEM_COMPANIES);
        this.customersTable = getTable(TableKeys.CUSTOMERS);
        this.salesDeliveryNotesTable = getTable(TableKeys.SALES_DELIVERY_NOTES);
    }

    @Override
    protected String createAvailabilityQuery(SalesOrderVolumeExportConfig config) throws ClientException {
        if (config.getExportCompany() == null) {
            throw new ClientException(ErrorCode.INVALID_CONFIG);
        }
        StringBuilder query = createSelectFrom(false);
        query
                .append(salesOrdersTable)
                .append(" WHERE company_id = ")
                .append(config.getExportCompany().getId())
                .append(" AND status >= ")
                .append(Record.STAT_SENT)
                .append(" AND id IN (SELECT reference_id FROM ")
                .append(salesDeliveryNotesTable)
                .append(" WHERE created > '")
                .append(config.getExportStart())
                .append("'::timestamp ");

        if (config.getExportEnd() != null) {
            if (log.isDebugEnabled()) {
                log.debug("createAvailabilityQuery() found configured end date");
            }
            query.append("AND created < '").append(config.getExportEnd()).append("'::timestamp ");
        }
        query
                .append("AND status >= ")
                .append(Record.STAT_SENT)
                .append(" AND id NOT IN (SELECT delivery_id FROM ")
                .append(exportDeliveriesTable)
                .append(")) AND contact_id NOT IN (SELECT id FROM ")
                .append(customersTable)
                .append(" WHERE contact_id IN (SELECT contact_id FROM ")
                .append(systemCompaniesTable)
                .append(" WHERE id = ")
                .append(config.getExportCompany().getId())
                .append("))");
        String generatedSql = query.toString();
        if (log.isDebugEnabled()) {
            log.debug("createAvailabilityQuery() done, generated sql:\n" + generatedSql);
        }
        return generatedSql;
    }

    @Override
    protected List<Order> filter(SalesOrderVolumeExportConfig config, List<Order> result) {
        Set<Long> exportedDeliveries = getExportedDeliveries();
        for (Iterator<Order> i = result.iterator(); i.hasNext();) {
            Order next = i.next();
            if (SalesOrderVolumeExportUtil.createUnbilledItems(config, exportedDeliveries, next).isEmpty()) {
                i.remove();
            }
        }
        return result;
    }
}
