/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 26, 2011 at 7:58:04 PM 
 * 
 */
package com.osserp.core.service.requests;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.Constants;
import com.osserp.common.service.ResourceLocator;
import com.osserp.core.dao.Requests;
import com.osserp.core.employees.Employee;
import com.osserp.core.requests.Request;
import com.osserp.core.sales.SalesPersonLocator;
import com.osserp.core.service.events.EventTaskTicketCreator;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RequestCreatedEventTask extends AbstractRequestChangedEventTask {
    private static Logger log = LoggerFactory.getLogger(RequestCreatedEventTask.class.getName());
    private static final String REQUEST_CREATED = "requestCreated";
    private SalesPersonLocator salesPersonLocator;
    private EventTaskTicketCreator ticketCreator;

    public RequestCreatedEventTask(Requests requests, ResourceLocator resourceLocator, EventTaskTicketCreator ticketCreator,
            SalesPersonLocator salesPersonLocator) {
        super(requests, resourceLocator);
        this.salesPersonLocator = salesPersonLocator;
        this.ticketCreator = ticketCreator;
    }

    @Override
    protected void executeTask(Request request, Map<String, Object> values) {
        if (request != null && request.getType().isSalesPersonByCollector()) {
            List<Employee> recipients = salesPersonLocator.findRecipients(request);
            for (int i = 0, j = recipients.size(); i < j; i++) {
                Employee recipient = recipients.get(i);
                ticketCreator.createTicket(
                        REQUEST_CREATED,
                        request.getRequestId(),
                        request.getName(),
                        request.getBranch().getId(),
                        recipient.getId(),
                        Constants.SYSTEM_EMPLOYEE,
                        getResourceString(request.getSalesAssignment()));
                if (log.isDebugEnabled()) {
                    log.debug("executeTask() event ticket created [request=" + request.getRequestId()
                            + ", recipient=" + recipient.getId()
                            + ", salesAssignment=" + request.getSalesAssignment()
                            + "]");
                }
            }
        }
    }
}
