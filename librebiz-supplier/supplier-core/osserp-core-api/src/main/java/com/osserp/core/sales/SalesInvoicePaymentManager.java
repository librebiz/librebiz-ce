/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 22, 2016 
 * 
 */
package com.osserp.core.sales;

import java.math.BigDecimal;
import java.util.Date;

import com.osserp.common.ClientException;

import com.osserp.core.finance.Cancellation;
import com.osserp.core.finance.CreditNote;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.Payment;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SalesInvoicePaymentManager {

    /**
     * Adds cash discount  
     * @param user
     * @param invoice
     * @param paymentType
     * @param amount
     * @param cashDiscount indicates if amount is considered as cash discount
     * or paid amount.  
     * @return invoice with updated payments
     * @throws ClientException if validation failed
     */
    Invoice addDiscount(
            DomainUser user,
            Invoice invoice,
            Long paymentType,
            BigDecimal amount,
            boolean cashDiscount)
            throws ClientException;

    /**
     * Adds a payment  
     * @param user
     * @param invoice
     * @param bankAccountId
     * @param paymentType
     * @param amount
     * @param datePaid
     * @param paid
     * @param cashDiscount to enter remaining amount as discount 
     * @return invoice with updated payments
     * @throws ClientException if validation failed
     */
    Invoice addPayment(
            DomainUser user, 
            Invoice invoice, 
            Long bankAccountId,
            Long paymentType, 
            BigDecimal amount, 
            Date datePaid, 
            boolean paid,
            boolean cashDiscount)
            throws ClientException;

    /**
     * Creates a custom payment
     * @param user
     * @param invoice
     * @param amount
     * @param datePaid
     * @param paid
     * @param customHeader
     * @param note
     * @param bankAccountId
     * @return invoice
     * @throws ClientException if any required value missing
     */
    Invoice addCustomPayment(
            DomainUser user,
            Invoice invoice,
            BigDecimal amount,
            Date datePaid,
            boolean paid,
            String customHeader,
            String note,
            Long bankAccountId)
            throws ClientException;

    /**
     * Creates a cancellation record relating to provided invoice
     * @param user
     * @param invoice
     * @param date
     * @param customId
     * @return cancellation record
     * @throws ClientException if cancellation of invoice is not possible by
     * status or user permissions
     */
    Cancellation createCancellation(DomainUser user, Invoice invoice, Date date, Long customId) throws ClientException;

    /**
     * Creates a creditNote record relating to provided invoice.
     * @param user
     * @param invoice
     * @param bookingType
     * @param copyItems
     * @return new created creditNote or already existing if not unchangeable
     * @throws ClientException if record is not released or user has no permission
     */
    CreditNote createCreditNote(DomainUser user, Invoice invoice, Long bookingType, boolean copyItems) throws ClientException;

    /**
     * Clears decimal amount
     * @param user
     * @param invoice with decimal amount to clear
     * @return invoice with updated payments
     * @throws ClientException if record is not clearable or user has no permission
     */
    Invoice clearDecimalAmount(DomainUser user, Invoice invoice) throws ClientException;

    /**
     * Deletes a payment
     * @param user 
     * @param invoice relating invoice
     * @param payment
     * @return invoice with updated status
     * @throws ClientException if record is not released or user has no permission
     */
    Invoice deletePayment(DomainUser user, Invoice invoice, Payment payment) throws ClientException;

    /**
     * Sets invoice as paid even if invoice is not paid fully. Use with care!
     * @param user
     * @param invoice
     * @return invoice with updated flags
     * @throws ClientException if record is not released or user has no permission
     */
    Invoice setAsPaid(DomainUser user, Invoice invoice) throws ClientException;

}
