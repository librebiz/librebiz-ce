<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<o:resource />
<v:view viewName="productPictureView" />
<c:set var="dmsDocumentView" scope="request" value="${sessionScope.productPictureView}"/>
<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="headline">
        <fmt:message key="pictures" />
        <o:out value="${view.product.productId}" /> - <o:out value="${view.product.name}" />
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>
    <tiles:put name="content" type="string">
        <div class="content-area">
            <div class="row">
                <div class="col-md-12 panel-area">
                    <v:form url="/products/productPicture/save" multipart="true">
                        <div class="table-responsive table-responsive-default">
                            <table class="table table-striped">
                                <c:choose>
                                    <c:when test="${empty view.list}">
                                        <c:import url="${viewdir}/products/_productDocumentUploadColumn.jsp" />
                                    </c:when>
                                    <c:otherwise>
                                        <thead>
                                            <tr>
                                                <th><fmt:message key="picture" /></th>
                                                <th><fmt:message key="date" /></th>
                                                <th><fmt:message key="uploadBy" /></th>
                                                <th<c:if test="${view.syncClientEnabled}"> colspan="2"</c:if> class="center">
                                                    <fmt:message key="action" />
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:import url="${viewdir}/products/_productDocumentUploadColumn.jsp" />
                                            <c:forEach items="${view.list}" var="doc">
                                                <tr>
                                                    <td><o:doc obj="${doc}">
                                                            <o:out value="${doc.displayName}" limit="70" />
                                                        </o:doc></td>
                                                    <td><o:date value="${doc.created}" /></td>
                                                    <td><v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${doc.createdBy}">
                                                            <oc:employee value="${doc.createdBy}" />
                                                        </v:ajaxLink></td>
                                                    <td class="action"><o:permission role="executive,product_datasheet_upload_admin" info="permissionDocumentsDelete">
                                                            <a href="javascript:onclick=confirmLink('<fmt:message key="irrevocableAction"/>:\n<fmt:message key="confirmDeleteDocument"/>','<v:url value="/products/productPicture/delete?id=${doc.id}" />');" title="<fmt:message key="delete"/>"> <o:img name="deleteIcon" />
                                                            </a>
                                                        </o:permission> <o:forbidden role="executive,product_datasheet_upload_admin" info="permissionDocumentsDelete">
                                                            <o:img name="disabledIcon" />
                                                        </o:forbidden></td>
                                                    <c:if test="${view.syncClientEnabled}">
                                                        <td class="action">
                                                            <span style="align-left: 6px;"> <v:link url="/products/productPicture/syncWithClient?id=${doc.id}" title="syncClientActionHint">
                                                                    <o:img name="syncDocumentIcon" />
                                                                </v:link>
                                                            </span>
                                                        </td>
                                                    </c:if>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </c:otherwise>
                                </c:choose>
                            </table>
                        </div>
                    </v:form>
                </div>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
