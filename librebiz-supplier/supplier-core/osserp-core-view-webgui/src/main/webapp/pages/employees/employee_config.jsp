<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<c:set var="contactView" value="${sessionScope.contactView}" />
<c:set var="contact" value="${contactView.bean}" />
<c:if test="${!contactView.detailsEditMode}">
	<div class="contentBox">
		<div class="contentBoxHeader">
			<div class="contentBoxHeaderLeft">
				<p>
					<fmt:message key="settings" />
				</p>
			</div>
			<div class="contentBoxHeaderRight">&nbsp;</div>
		</div>
		<div class="contentBoxData">
			<div class="subcolumns">
				<div class="subcolumn">
					<div class="smallSpacer"></div>
					<table style="width: 100%;">
						<tbody>
							<tr>
								<td>
									<c:choose>
										<c:when test="${contact.emailAccount}">
											<input type="checkbox" name="emailAccount" checked="checked"
												disabled="disabled" />
										</c:when>
										<c:otherwise>
											<input type="checkbox" name="emailAccount"
												disabled="disabled" />
										</c:otherwise>
									</c:choose> 
									<fmt:message key="emailAccount" />
								</td>
								<td>
									<c:choose>
										<c:when test="${contact.cellularPhone}">
											<input type="checkbox" name="cellularPhone"
												disabled="disabled" checked="checked" />
										</c:when>
										<c:otherwise>
											<input type="checkbox" name="cellularPhone"
												disabled="disabled" />
										</c:otherwise>
									</c:choose> 
									<fmt:message key="cellularPhone" />
								</td>
								<td>
									<c:choose>
										<c:when test="${contact.businessCard}">
											<input type="checkbox" name="businessCard" checked="checked"
												disabled="disabled" />
										</c:when>
										<c:otherwise>
											<input type="checkbox" name="businessCard"
												disabled="disabled" />
										</c:otherwise>
									</c:choose> 
									<fmt:message key="businessCard" />
								</td>
							</tr>
							<tr>
								<td>
									<c:choose>
										<c:when test="${contact.timeRecordingEnabled}">
											<input type="checkbox" name="timeRecordingEnabled"
												checked="checked" disabled="disabled" />
										</c:when>
										<c:otherwise>
											<input type="checkbox" name="timeRecordingEnabled"
												disabled="disabled" />
										</c:otherwise>
									</c:choose> 
									<c:choose>
										<c:when	test="${contact.timeRecordingEnabled and (timeRecordingPermission or contactView.userCurrentContact or contactView.disciplinarianFromSelected)}">
											<a href="<c:url value="/timeRecordings.do?method=forward&exit=employeeDisplay"/>">
												<fmt:message key="timeRecording" />
											</a>
										</c:when>
										<c:otherwise>
											<fmt:message key="timeRecording" />
										</c:otherwise>
									</c:choose>
								</td>
								<td>
									<c:choose>
										<c:when test="${contact.internationalBusinessCard}">
											<input type="checkbox" name="internationalBusinessCard"
												checked="checked" disabled="disabled" />
										</c:when>
										<c:otherwise>
											<input type="checkbox" name="internationalBusinessCard"
												disabled="disabled" />
										</c:otherwise>
									</c:choose> 
									<fmt:message key="international" />
								</td>
								<td></td>
							</tr>
						</tbody>
					</table>
					<div class="smallSpacer"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="spacer"></div>
</c:if>
