/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 15, 2012 2:52:23 AM 
 * 
 */
package com.osserp.gui.mail

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.mail.Attachment
import com.osserp.common.mail.Mail
import com.osserp.common.mail.MailSender
import com.osserp.common.util.StringUtil
import com.osserp.groovy.web.ViewContextException

import com.osserp.core.mail.MailMessageContext

import com.osserp.groovy.web.MenuItem

import com.osserp.gui.CoreView
import com.osserp.gui.contacts.EmailSearchView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class MailEditorView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(MailEditorView.class.getName())

    MailMessageContext mailMessageContext
    Mail mail
    Boolean ignoreFixedRecipients
    String subject
    String text
    String recipients = ''
    String recipientsCC = ''
    String recipientsBCC = ''
    Boolean sendMail
    int mailMessageSizeLimit = 10240000

    MailEditorView() {
        super()
        providesCustomNavigation()
        overrideExitTarget = true
        dependencies = [
            MailMessageContext.NAME,
            'emailSearchView',
            'mailAttachmentView'
        ]
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            int maxMsgSize = systemPropertyAsInt('mailMessageSizeLimit')
            if (maxMsgSize > 0) {
                mailMessageSizeLimit = maxMsgSize
            }
            mailMessageContext = env[MailMessageContext.NAME]
            if (mailMessageContext) {
                if (mailMessageContext.mail) {
                    mail = mailMessageContext.mail
                    text = mail.text
                    subject = mail.subject
                }
                if (mailMessageContext.exitTarget) {
                    addExitTarget(mailMessageContext.exitTarget)
                }
                if (mailMessageContext.mail?.recipients) {
                    recipients = StringUtil.createCommaSeparated(mailMessageContext.mail.recipients)
                }
                if (mailMessageContext.mail?.recipientsCC) {
                    recipientsCC = StringUtil.createCommaSeparated(mailMessageContext.mail.recipientsCC)
                }
                if (mailMessageContext.mail?.recipientsBCC) {
                    recipientsBCC = StringUtil.createCommaSeparated(mailMessageContext.mail.recipientsBCC)
                }
                if (!mailMessageContext.recipientSuggestions.empty) {
                    enableRecipientSelectionMode()
                }
                logger.debug("initRequest: done [mailAvailable=${(mail != null)}, dataAvailable=${(mailMessageContext != null)}]")
            }
            if (!mail) {
                // direct invocation
                mail = new Mail(domainEmployee.email)
                logger.debug("initRequest: done [mailAvailable=${(mail != null)}, dataAvailable=${(mailMessageContext != null)}]")
            }
        } else if (!mail && (saveRequest || sendRequest)) {
            throw new ViewContextException()
        }
    }

    @Override
    void save() {
        subject = form.getString('subject')
        text = form.getString('text')

        if (mailMessageContext) {
            if (mailMessageContext.recipientsDisabled && !ignoreFixedRecipients) {
                recipients = form.getString('recipients') ?: ''
            }
            String recipientsCCValue = form.getString('recipientsCC')
            if (ignoreFixedRecipients) {
                if (!recipientsCCValue && !updateOnly) {
                    throw new ClientException(ErrorCode.RECIPIENT_MISSING)
                }
                recipients = recipientsCCValue ?: ''
            } else if (!mailMessageContext.recipientsCCDisabled) {
                recipientsCC = recipientsCCValue ?: ''
            }
            if (!mailMessageContext.recipientsBCCDisabled) {
                recipientsBCC = form.getString('recipientsBCC') ?: ''
            }
        } else {
            // mailEditor was directly invoked
            recipients = form.getString('recipients') ?: ''
            recipientsCC = form.getString('recipientsCC') ?: ''
            recipientsBCC = form.getString('recipientsBCC') ?: ''
            if (!recipients && !recipientsCC && !updateOnly) {
                throw new ClientException(ErrorCode.RECIPIENT_MISSING)
            }
        }
        sendMail = form.getBoolean('sendMail')
    }

    String send() {
        mail.subject = subject
        mail.text = text
        String[] rcpts = StringUtil.getTokenArray(recipients)
        rcpts.each { mail.addRecipient(it) }
        String[] rcptsCC = StringUtil.getTokenArray(recipientsCC)
        rcptsCC.each { mail.addRecipientCC(it) }
        String[] rcptsBCC = StringUtil.getTokenArray(recipientsBCC)
        rcptsBCC.each { mail.addRecipientBCC(it) }
        if (mailMessageContext && !mailMessageContext.allRecipients) {
            throw new ClientException(ErrorCode.RECIPIENT_MISSING)
        }
        if (isSystemPropertyEnabled('mailSenderAlwaysBccFromAddress')) {
            String mailFrom = mail.originator
            if (mailFrom && !mail.getRecipients().contains(mailFrom)
                    && !mail.getRecipientsCC().contains(mailFrom)
                    && !mail.getRecipientsBCC().contains(mailFrom)) {
                 mail.addRecipientBCC(mailFrom)
            }
        }
        if (mail.mailMessageSize > mailMessageSizeLimit) {
            throw new ClientException(ErrorCode.MAIL_MESSAGE_SIZE)
        }

        Exception sendFailed
        try {
            mailSender.send(mail)
        } catch (ClientException ce) {
            throw ce
        } catch (Exception e) {
            sendFailed = e
        }
        if (mailMessageContext) {

            if (sendFailed) {
                mailMessageContext.sendStatus = 'failed'
                mailMessageContext.errorMessage = sendFailed.message
                if (sendFailed.errorObject) {
                    mailMessageContext.errorSource = sendFailed.errorObject.toString()
                }
                return createTarget(mailMessageContext.exitTarget)
            } else {
                mailMessageContext.sendStatus = 'OK'
                return createTarget(mailMessageContext.successTarget)
            }
        }
        return '/index'
    }

    boolean isSendRequest() {
        requestContext.method == 'send'
    }

    /**
     * Adds mail addresses as provided by emailSearchView to recipients
     */
    void addRecipients() {
        EmailSearchView searchView = env['emailSearchView']
        StringBuilder buffer = new StringBuilder(recipients)
        if (searchView) {
            searchView.collectedAddresses.each {
                if (buffer.indexOf(it) < 0) {
                    if (buffer.length() > 0) {
                        buffer.append(',').append(it)
                    } else {
                        buffer.append(it)
                    }
                }
            }
        }
        recipients = buffer.toString()
    }

    /**
     * Adds mail addresses as provided by emailSearchView to recipientsCC
     */
    void addRecipientsCC() {
        EmailSearchView searchView = env['emailSearchView']
        StringBuilder buffer = new StringBuilder(recipientsCC)
        if (searchView) {
            searchView.collectedAddresses.each {
                if (buffer.indexOf(it) < 0) {
                    if (buffer.length() > 0) {
                        buffer.append(',').append(it)
                    } else {
                        buffer.append(it)
                    }
                }
            }
        }
        recipientsCC = buffer.toString()
    }

    /**
     * Adds mail addresses as provided by emailSearchView to recipientsBCC
     */
    void addRecipientsBCC() {
        EmailSearchView searchView = env['emailSearchView']
        StringBuilder buffer = new StringBuilder(recipientsBCC)
        if (searchView) {
            searchView.collectedAddresses.each {
                if (buffer.indexOf(it) < 0) {
                    if (buffer.length() > 0) {
                        buffer.append(',').append(it)
                    } else {
                        buffer.append(it)
                    }
                }
            }
        }
        recipientsBCC = buffer.toString()
    }

    /**
     * Enables/disables ignoreFixedRecipients mode
     */
    void toggleIgnoreFixedRecipients() {
        ignoreFixedRecipients = !ignoreFixedRecipients
    }

    /**
     * Provides the count of files attached to our message
     * @return count of attachments
     */
    int getAttachmentCount() {
        try {
            return mail.attachments.size()
        } catch (NullPointerException e) {
            return 0
        }
    }

    /**
     * Provides the attachment display
     * @return attachments as comma separated list of filenames
     */
    String getAttachments() {
        StringBuilder builder = new StringBuilder()
        int size = attachmentCount
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                Attachment next = mail.attachments.get(i)
                if (builder.length() > 0) {
                    builder.append(", ")
                }
                builder.append(next.fileName)
            }
        }
        return builder.toString()
    }

    @Override
    void createCustomNavigation() {
        if (recipientSelectionMode) {
            nav.defaultNavigation = [
                disableRecipientSelectionLink,
                homeLink
            ]
        } else if (isPermissionGrant('template_admin_email') && mailMessageContext?.mailTemplate
                && !exitTarget.contains('mailTemplateConfig')) {
            if (recipientSelectionAvailable) {
                nav.defaultNavigation = [
                    exitLink,
                    enableRecipientSelectionLink,
                    new MenuItem(link: "${context.fullPath}/admin/mail/mailTemplateConfig/forward?id=${mailMessageContext.mailTemplate.id}&exit=${exitTarget}",
                        icon: 'configureIcon', title: 'mailTemplateConfigHeader'),
                    homeLink
                ]
            } else {
                nav.defaultNavigation = [
                    exitLink,
                    new MenuItem(link: "${context.fullPath}/admin/mail/mailTemplateConfig/forward?id=${mailMessageContext.mailTemplate.id}&exit=${exitTarget}",
                        icon: 'configureIcon', title: 'mailTemplateConfigHeader'),
                    homeLink
                ]
            }
        } else if (recipientSelectionAvailable) {
            nav.defaultNavigation = [
                exitLink,
                enableRecipientSelectionLink,
                homeLink
            ]
        }
    }

    MenuItem getDisableRecipientSelectionLink() {
        new MenuItem(link: "${context.fullPath}/${context.name}/disableRecipientSelectionMode", icon: 'backIcon', title: 'disableSelection')
    }

    MenuItem getEnableRecipientSelectionLink() {
        new MenuItem(link: "${context.fullPath}/${context.name}/enableRecipientSelectionMode", icon: 'peopleIcon', title: 'involvedPartyAddTitle')
    }

    boolean isRecipientSelectionAvailable() {
        !mailMessageContext.recipientSuggestions.empty
    }

    boolean isRecipientSelectionRequired() {
        mailMessageContext.mail.recipients.empty &&
            mailMessageContext.mail.recipientsCC.empty
    }

    boolean isRecipientSelectionMode() {
        env.recipientSelectionMode
    }

    void disableRecipientSelectionMode() {
        env.recipientSelectionMode = false
    }

    void enableRecipientSelectionMode() {
        env.recipientSelectionMode = true
    }

    void addSuggestedRecipient() {
        String emailAddress = form.getString('email')
        if (emailAddress) {
            if (mailMessageContext.addSuggestedRecipient(emailAddress)) {
                List<String> rcpts = StringUtil.getTokenList(recipients)
                boolean exists = false
                rcpts.each { String existing ->
                    if (existing.equalsIgnoreCase(emailAddress)) {
                        exists = true
                    }
                }
                if (!exists) {
                    recipients = !recipients ? "${emailAddress}" : "${recipients},${emailAddress}"
                }
            }
        }
    }

    void addSuggestedRecipientCC() {
        String emailAddress = form.getString('email')
        if (emailAddress) {
            if (mailMessageContext.addSuggestedRecipientCC(emailAddress)) {
                List<String> rcpts = StringUtil.getTokenList(recipientsCC)
                boolean exists = false
                rcpts.each { String existing ->
                    if (existing.equalsIgnoreCase(emailAddress)) {
                        exists = true
                    }
                }
                if (!exists) {
                    recipientsCC = !recipientsCC ? "${emailAddress}" : "${recipientsCC},${emailAddress}"
                }
            }
        }
    }

    protected MailSender getMailSender() {
        getService(MailSender.class.getName())
    }
}
