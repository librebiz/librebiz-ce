<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<c:set var="record" value="${sessionScope.recordView.record}"/>
<c:if test="${!empty record}">
    <o:out value="${record.number}"/> / <o:out value="${record.contact.displayName}"/>
    <c:if test="${!empty record.businessCaseId}">
    <span> - </span>
    <c:choose>
        <c:when test="${record.type.id == 1}"><fmt:message key="request"/> </c:when>
        <c:otherwise><fmt:message key="salesOrder"/> </c:otherwise>
    </c:choose>
     <o:out value="${record.businessCaseId}"/>
    </c:if>
</c:if>
