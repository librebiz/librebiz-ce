/**
*
* Copyright (C) 2016 The original author or authors.
*
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
* 
* Created on May 6, 2016
*  
*/
package com.osserp.core.model.records;

import com.osserp.common.beans.AbstractOption;
import com.osserp.common.util.DateUtil;

import com.osserp.core.finance.RecordNumberConfig;
import com.osserp.core.finance.RecordType;

/**
* 
 * @author Rainer Kirchner <rk@osserp.com>
* 
*/

public class RecordNumberConfigImpl extends AbstractOption implements RecordNumberConfig {

    private Long company;
    private boolean yearly;
    private Long startYear;
    private int digits;
    private boolean dedicated;
    
    private Long defaultNumber;
    private Long highestNumber;
    private Long lowestNumber;


    protected RecordNumberConfigImpl() {
        super();
    }


    public RecordNumberConfigImpl(
            RecordType type, 
            Long company, 
            String name,
            boolean dedicated, 
            int digits, 
            boolean yearly, 
            Long start,
            Long defaultNumber) {
        super(name);
        this.company = company;
        this.dedicated = dedicated;
        this.digits = digits;
        this.yearly = yearly;
        if (yearly) {
            if (isSet(start) && start > 1970L) {
                startYear = start;
            } else {
                startYear = Long.valueOf(DateUtil.getYear(DateUtil.getCurrentDate()));
            }
        }
        this.defaultNumber = defaultNumber;
    }
    

    public Long getCompany() {
        return company;
    }
    
    public void setCompany(Long company) {
        this.company = company;
    }
    
    public boolean isDedicated() {
        return dedicated;
    }

    public void setDedicated(boolean dedicated) {
        this.dedicated = dedicated;
    }

    public boolean isYearly() {
        return yearly;
    }
    
    public void setYearly(boolean yearly) {
        this.yearly = yearly;
    }
    
    public Long getStartYear() {
        return startYear;
    }
    
    public void setStartYear(Long startYear) {
        this.startYear = startYear;
    }
    
    public int getDigits() {
        return digits;
    }
    
    public void setDigits(int digits) {
        this.digits = digits;
    }

    public Long getDefaultNumber() {
        return defaultNumber;
    }


    public void setDefaultNumber(Long defaultNumber) {
        this.defaultNumber = defaultNumber;
    }


    public Long getHighestNumber() {
        return highestNumber;
    }


    public void setHighestNumber(Long highestNumber) {
        this.highestNumber = highestNumber;
    }


    public Long getLowestNumber() {
        return lowestNumber;
    }


    public void setLowestNumber(Long lowestNumber) {
        this.lowestNumber = lowestNumber;
    }

}
