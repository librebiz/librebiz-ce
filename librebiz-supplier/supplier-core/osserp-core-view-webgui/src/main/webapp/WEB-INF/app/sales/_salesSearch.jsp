<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<v:view viewName="salesSearchView"/>
<c:set var="colspanVal" value="8" />
<div class="table-responsive table-responsive-default">
    <table class="table table-striped">
        <thead>
            <tr>
                <th class="businessCaseType"><fmt:message key="type"/></th>
                <th class="businessCaseId"><v:sortLink key="id"><fmt:message key="number"/></v:sortLink></th>
                <th class="businessCaseName"><v:sortLink key="name"><fmt:message key="commission"/></v:sortLink></th>
                <th class="businessCaseDate"><v:sortLink key="created"><fmt:message key="order"/></v:sortLink></th>
                <th class="businessCaseAmount"><v:sortLink key="capacity"><fmt:message key="value"/></v:sortLink></th>
                <th class="businessCaseDate"><v:sortLink key="deliveryDate"><fmt:message key="deliveryDayShort"/></v:sortLink></th>
                <th class="shippingKey" title="<fmt:message key="shippingLabelText"/>"><fmt:message key="shippingLabelShort"/></th>
                <c:if test="${view.renderBranch}">
                <th class="branchKey"><v:sortLink key="branchKey"><fmt:message key="branchOfficeShortcut"/></v:sortLink></th>
                <c:set var="colspanVal" value="${colspanVal + 1}" />
                </c:if>
                <c:if test="${view.renderSalesPerson}">
                <th class="employeeKey" style="width: 35px;"><v:sortLink key="salesKey"><fmt:message key="salesShortcut"/></v:sortLink></th>
                <c:set var="colspanVal" value="${colspanVal + 1}" />
                </c:if>
                <c:if test="${view.renderManager}">
                <th class="employeeKey" style="width: 35px;"><v:sortLink key="managerKey"><fmt:message key="projectorShort"/></v:sortLink></th>
                <c:set var="colspanVal" value="${colspanVal + 1}" />
                </c:if>
                <th class="businessCaseStatus"><v:sortLink key="status">%</v:sortLink></th>
                <o:permission role="notes_sales_deny" info="permissionNotes">
                <th class="center" colspan="3"><fmt:message key="info"/></th>
                </o:permission>
                <o:forbidden role="notes_sales_deny" info="permissionNotes">
                <th class="center" colspan="2"><fmt:message key="info"/></th>
                </o:forbidden>
            </tr>
        </thead>
        <tbody>
            <c:if test="${empty view.list}">
                <tr>
                    <td colspan="${colspanVal}" class="errortext"><fmt:message key="adviceNoActualOrdersFound"/></td>
                </tr>
            </c:if>
            <c:forEach var="sales" items="${view.list}" varStatus="s">
                <tr id="sales${sales.id}"<c:if test="${sales.stopped}"> class="red"</c:if>>
                    <td class="businessCaseType">
                        <o:out value="${sales.type.key}"/>
                    </td>
                    <td class="businessCaseId">
                        <v:link url="/sales/salesSearch/select?id=${sales.id}&exitId=sales${sales.id}" rawtitle="${sales.type.name}">
                            <span<c:if test="${sales.status < 15}"> style="font-style:italic;"</c:if>><o:out value="${sales.id}"/></span>
                        </v:link>
                    </td>
                    <td class="businessCaseName">
                        <v:link url="/sales/salesSearch/select?id=${sales.id}&exitId=sales${sales.id}" rawtitle="${sales.type.name}">
                            <span<c:if test="${sales.status < 15}"> style="font-style:italic;"</c:if>><o:out value="${sales.name}" limit="48"/></span>
                        </v:link>
                    </td>
                    <td class="businessCaseDate"><o:date value="${sales.created}" casenull="-" addcentury="false"/></td>
                    <td class="businessCaseAmount"><o:number value="${sales.capacity}" format="${sales.type.capacityFormat}"/></td>
                    <td class="businessCaseDate"><o:date value="${sales.deliveryDate}" casenull="-" addcentury="false"/></td>
                    <td class="shippingKey"><oc:options name="shippingCompanyKeys" value="${record.shippingId}"/></td>
                    <c:if test="${view.renderBranch}">
                    <td class="branchKey"><o:out limit="4" value="${sales.branchKey}"/></td>
                    </c:if>
                    <c:if test="${view.renderSalesPerson}">
                    <td class="employeeKey">
                        <c:if test="${!empty sales.salesId and sales.salesId > 0}">
                            <v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${sales.salesId}"><oc:employee initials="true" value="${sales.salesId}" /></v:ajaxLink>
                        </c:if>
                    </td>
                    </c:if>
                    <c:if test="${view.renderManager}">
                    <td class="employeeKey">
                        <c:if test="${!empty sales.managerId and sales.managerId > 0}">
                            <v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${sales.managerId}"><oc:employee initials="true" value="${sales.managerId}" /></v:ajaxLink>
                        </c:if>
                    </td>
                    </c:if>
                    <td class="businessCaseStatus">
                        <v:ajaxLink url="/sales/flowControlDisplay/forward?id=${sales.id}" styleClass="boldtext" linkId="flowControlDisplayView">
                            <o:out value="${sales.status}"/>
                        </v:ajaxLink>
                    </td>
                    <o:permission role="notes_sales_deny" info="permissionNotes">
                        <td class="icon center">
                            <v:ajaxLink url="/sales/businessCaseDisplay/forward?notes=true&id=${sales.id}" linkId="businessCaseDisplayView">
                                <o:img name="texteditIcon"/>
                            </v:ajaxLink>
                        </td>
                    </o:permission>
                    <td class="icon center">
                        <v:ajaxLink url="/sales/businessCaseDisplay/forward?id=${sales.id}" linkId="businessCaseDisplayView">
                            <o:img name="openFolderIcon"/>
                        </v:ajaxLink>
                    </td>
                    <td class="icon center">
                        <c:choose>
                            <c:when test="${!sales.released}">
                                <span title="<fmt:message key="orderNotReleased"/>"><o:img name="importantDisabledIcon"/></span>
                            </c:when>
                            <c:when test="${sales.deliveryClosed}">
                                <span title="<fmt:message key="materialPlanningFinished"/>"><o:img name="importantDisabledIcon"/></span>
                            </c:when>
                            <c:otherwise>
                                <v:ajaxLink url="/sales/deliveryStatusDisplay/forward?id=${sales.id}" linkId="deliveryStatus" title="materialPlanning">
                                    <o:img name="importantIcon"/>
                                </v:ajaxLink>
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</div>
<o:removeErrors/>
