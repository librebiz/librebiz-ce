<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.calculationView}">
    <o:logger write="No calculation view found in session context!" level="info"/>
</c:if>
<c:if test="${empty sessionScope.calculationView.bean}">
    <o:logger write="View found but no calculation loaded!" level="info"/>
</c:if>
<c:set var="view" value="${sessionScope.calculationView}"/>
<c:set var="calculation" value="${view.calculation}"/>
<c:set var="businessCaseClosed" value="${view.businessCase.closed}"/>
<c:set var="targetPricePerUnit" value="0"/>
<c:if test="${!empty view.revenue and !empty calculation.plantCapacity and calculation.plantCapacity > 0}">
<c:set var="targetPricePerUnit" value="${view.revenue.targetVolume / calculation.plantCapacity}"/>
</c:if>
<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title"><o:displayTitle/></tiles:put>

    <tiles:put name="headline">
        <fmt:message key="calculation"/> [<o:out value="${calculation.number}"/>]
        <c:if test="${!empty view.businessCase}">
            <o:out value="${view.businessCase.primaryKey}"/> - <o:out value="${view.businessCase.name}" limit="45"/>
        </c:if>
    </tiles:put>

    <tiles:put name="headline_right">
        <ul>
            <li>
                <a href="<c:url value="/calculations.do?method=exit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a>
            </li>
            <c:if test="${!empty calculation.calculatorClass and 'calculatorByItems' == calculation.calculatorClass and !empty calculation.allItems}">
                <o:permission role="executive,executive_sales,sales_revenue" info="permissionPostCalculation">
                    <li><v:ajaxLink linkId="salesRevenueView" url="/sales/salesRevenue/forward?id=${calculation.id}"><o:img name="moneyIcon"/></v:ajaxLink></li>
                    <li>
                        <c:choose>
                            <c:when test="${calculation.salesPriceLocked}">
                                <a href="<c:url value="/calculations.do?method=toggleSalesPriceLock"/>" title="<fmt:message key="toggleSalesPriceLockDisable"/>">
                                    <o:img name="lockIcon"/>
                                </a>
                            </c:when>
                            <c:otherwise>
                                <a href="<c:url value="/calculations.do?method=toggleSalesPriceLock"/>" title="<fmt:message key="toggleSalesPriceLockEnable"/>">
                                    <o:img name="unlockIcon"/>
                                </a>
                            </c:otherwise>
                        </c:choose>
                    </li>
                </o:permission>
            </c:if>
            <c:if test="${view.calculationHistoryAvailable}">
              <li><v:ajaxLink url="/calculations/calculationHistory/forward?id=${view.businessCase.primaryKey}&context=${view.businessCase.contextName}" linkId="calculationHistory" title="calculationHistory"><o:img name="archiveIcon"/></v:ajaxLink></li>
            </c:if>
            <c:if test="${view.calculationChangeable}">
                <o:permission role="executive,customer_service,technics,sales" info="permissionCalculationChange">
                    <li><a href="<c:url value="/calculations.do"><c:param name="method" value="edit"/><c:param name="id" value="${calculation.id}"/></c:url>" title="<fmt:message key="calculationChange"/>"><o:img name="writeIcon"/></a></li>
                </o:permission>
            </c:if>
            <c:if test="${view.calculationCreateable}">
                <li><a href="<c:url value="/calculations.do?method=forwardCreate"/>"><o:img name="newdataIcon" /></a></li>
            </c:if>
            <li><v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link></li>
        </ul>
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="subcolumns">
            <div class="subcolumn">
                <div class="spacer"></div>

                <div class="contentBox">
                    <div class="contentBoxHeader">
                        <div class="contentBoxHeaderLeft">
                            <c:choose>
                                <c:when test="${calculation.initial}">
                                    <fmt:message key="providedForOfferCalculationLabel"/>
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="providedFor"/>
                                    <o:ajaxLink linkId="calculation_detail" url="/calculationDetail.do?method=forward"><o:out value="${calculation.name}"/> </o:ajaxLink>
                                </c:otherwise>
                            </c:choose>
                            <fmt:message key="by"/>
                            <v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${calculation.createdBy}">
                                <oc:employee value="${calculation.createdBy}"/>
                            </v:ajaxLink>
                            / <o:date value="${calculation.created}"/> <fmt:message key="basedOn"/> <fmt:message key="${calculation.calculatorClass}"/>
                        </div>
                    </div>
                </div>
                <div class="spacer"></div>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th style="width: 90px;" class="right"><fmt:message key="product"/></th>
                            <th><fmt:message key="label"/></th>
                            <th class="right"><span title="<fmt:message key="partnerPrice"/>"><fmt:message key="partnerPriceShort"/></span></th>
                            <th class="right"><fmt:message key="quantity"/></th>
                            <th class="center"><span title="<fmt:message key="quantityUnit"/>"><fmt:message key="quantityUnitShort"/></span></th>
                            <th class="right"><fmt:message key="price"/></th>
                            <th class="right"><fmt:message key="total"/></th>
                            <th class="center"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="position" items="${calculation.positions}">
                            <c:if test="${!empty position.items}">
                                <tr>
                                    <td></td>
                                    <td colspan="7" class="boldtext">
                                        <c:choose>
                                            <c:when test="${position.partlist}">
                                                <oc:systemPropertyEnabled name="calculationPartlistSupport">
                                                    <v:link url="/calculations/partlist/forward?positionId=${position.id}&exit=/calculations.do?method=forward">
                                                        <o:out value="${position.name}"/>
                                                    </v:link>
                                                </oc:systemPropertyEnabled>
                                                <oc:systemPropertyDisabled name="calculationPartlistSupport">
                                                    <o:out value="${position.name}"/>
                                                </oc:systemPropertyDisabled>
                                            </c:when>
                                            <c:otherwise>
                                                <o:out value="${position.name}"/>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                </tr>
                                <c:forEach var="item" items="${position.items}">
                                    <tr id="product${item.product.productId}" class="lightgrey">
                                        <td class="right">
                                            <a href="<c:url value="/products.do?method=load&id=${item.product.productId}&exit=calculationDisplay&exitId=product${item.product.productId}" />">
                                                <o:out value="${item.product.productId}"/>
                                            </a>
                                        </td>
                                        <td>
                                            <c:choose>
                                                <c:when test="${empty sessionScope.openSalesOrdersView and !businessCaseClosed and item.product.affectsStock}">
                                                    <div class="recordTextColumn">
                                                        <a href="<c:url value="/calculations.do"><c:param name="method" value="forwardProductPlanning"/><c:param name="id" value="${item.product.productId}"/><c:param name="exit" value="calculationDisplay"/></c:url>" title="<fmt:message key="deliveryStatus"/>">
                                                            <o:out value="${item.productName}" />
                                                        </a>
                                                    </div>
                                                </c:when>
                                                <c:otherwise>
                                                    <div class="recordTextColumn"><o:out value="${item.productName}"/></div>
                                                </c:otherwise>
                                            </c:choose>
                                            <c:if test="${!empty item.note}">
                                                <div class="recordTextColumn smalltext tab"><o:textOut value="${item.note}"/></div>
                                            </c:if>
                                        </td>
                                        <td class="right"><fmt:formatNumber value="${item.partnerPrice}" maxFractionDigits="2" minFractionDigits="2"/></td>
                                        <td class="right"><o:number value="${item.quantity}" format="decimal"/></td>
                                        <td class="center"><oc:options name="quantityUnits" value="${item.product.quantityUnit}"/></td>
                                        <td class="right">
                                            <c:choose>
                                                <c:when test="${item.product.plant and targetPricePerUnit > item.price}">
                                                    <span class="error"><o:number value="${item.price}" format="currency"/></span>
                                                </c:when>
                                                <c:otherwise>
                                                    <o:number value="${item.price}" format="currency"/>
                                                </c:otherwise>
                                            </c:choose>
                                        </td>
                                        <td class="right"><o:number value="${item.amount}" format="currency"/></td>
                                        <td class="center">
                                            <c:choose>
                                                <c:when test="${item.includePrice}"><o:img name="eyesIcon"/></c:when>
                                                <c:otherwise>&nbsp;</c:otherwise>
                                            </c:choose>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </c:if>
                        </c:forEach>
                        <%-- replace, see below --%>
                        <tr>
                            <td colspan="8"></td>
                        </tr>
                        <tr class="lightgrey">
                            <td></td>
                            <td colspan="5" class="boldtext"><fmt:message key="summary"/></td>
                            <td class="right boldtext"><o:number value="${calculation.price}" format="currency"/></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="8"></td>
                        </tr>
                        <%-- re-activate when revenue calculation fixed
                        <c:choose>
                            <c:when test="${empty view.revenue}">
                                <tr>
                                    <td colspan="8"></td>
                                </tr>
                                <tr class="lightgrey">
                                    <td></td>
                                    <td colspan="5" class="boldtext"><fmt:message key="summary"/></td>
                                    <td class="right boldtext"><o:number value="${calculation.price}" format="currency"/></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="8"></td>
                                </tr>
                            </c:when>
                            <c:otherwise>
                                <tr class="lightgrey">
                                    <td></td>
                                    <td class="boldtext"><fmt:message key="summary"/></td>
                                    <td class="right"><o:number value="${view.revenue.netCosts}" format="currency"/></td>
                                    <td class="right"><o:number value="${calculation.plantCapacity}" format="decimal"/></td>
                                    <td class="right">Eh</td>
                                    <td class="right">
                                        <c:choose>
                                            <c:when test="${targetPricePerUnit > 0}">
                                                <o:number value="${targetPricePerUnit}" format="currency"/>
                                            </c:when>
                                            <c:otherwise>
                                                <o:number value="${view.revenue.targetVolume}" format="currency"/>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td class="right boldtext"><o:number value="${calculation.price}" format="currency"/></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="8"></td>
                                </tr>
                            </c:otherwise>
                        </c:choose>
                        --%>
                    </tbody>
                </table>
                <div class="spacer"></div>
                <c:if test="${calculation.optionAvailable}">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th colspan="7"><fmt:message key="options"/></th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="position" items="${calculation.options}">
                                <c:if test="${!empty position.items}">
                                    <tr>
                                        <td></td>
                                        <td colspan="6" class="boldtext"><o:out value="${position.name}"/></td>
                                    </tr>
                                    <c:forEach var="item" items="${position.items}">
                                        <tr id="product${item.product.productId}" class="lightgrey">
                                            <td style="width: 90px;" class="right">
                                                <a href="<c:url value="/products.do?method=load&id=${item.product.productId}&exit=calculationDisplay&exitId=product${item.product.productId}" />">
                                                    <o:out value="${item.product.productId}"/>
                                                </a>
                                            </td>
                                            <td>
                                                <o:out value="${item.product.name}" limit="52"/>
                                                <c:if test="${!empty item.note}">
                                                    <div class="smalltext tab"><o:textOut value="${item.note}"/></div>
                                                </c:if>
                                            </td>
                                            <td class="right"><o:number value="${item.quantity}" format="decimal"/></td>
                                            <td class="center"><oc:options name="quantityUnits" value="${item.product.quantityUnit}"/></td>
                                            <td class="right"><o:number value="${item.price}" format="currency"/></td>
                                            <td class="right"><o:number value="${item.amount}" format="currency"/></td>
                                            <td class="center"><o:img name="eyesIcon"/></td>
                                        </tr>
                                    </c:forEach>
                                </c:if>
                            </c:forEach>
                        </tbody>
                    </table>
                    <div class="spacer"></div>
                </c:if>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
