/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 18, 2008 12:38:09 PM 
 * 
 */
package com.osserp.core.model.contacts;

import java.util.Date;

import org.jdom2.Element;

import com.osserp.common.Constants;
import com.osserp.common.Option;
import com.osserp.common.xml.JDOMUtil;

import com.osserp.core.contacts.ContactReference;
import com.osserp.core.contacts.ContactType;
import com.osserp.core.contacts.ContactUtil;
import com.osserp.core.contacts.Phone;
import com.osserp.core.contacts.PhoneType;
import com.osserp.core.contacts.Salutation;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractContactReference extends AbstractPerson implements ContactReference {
    
    private Long id = null;
    private Long reference = null;
    private String company = null;
    private String email = null;
    private Phone fax = null;
    private Phone mobile = null;
    private Phone phone = null;
    private boolean newsletterAccepted = false;

    protected AbstractContactReference() {
        super();
        fax = new PhoneImpl(PhoneType.FAX, null, null, null, null);
        mobile = new PhoneImpl(PhoneType.MOBILE, null, null, null, null);
        phone = new PhoneImpl(PhoneType.PHONE, null, null, null, null);
    }

    protected AbstractContactReference(
            ContactType type,
            String company,
            Salutation salutation,
            Option title,
            boolean firstNamePrefix,
            String firstName,
            String lastName,
            String street,
            String streetAddon,
            String zipcode,
            String city) {

        super(
                type,
                salutation,
                title,
                firstNamePrefix,
                firstName,
                lastName,
                street,
                streetAddon,
                zipcode,
                city,
                null, // federalState
                null, // federalStateName
                Constants.GERMANY);
        this.company = company;
        fax = new PhoneImpl(PhoneType.FAX, type, null, null, null);
        mobile = new PhoneImpl(PhoneType.MOBILE, type, null, null, null);
        phone = new PhoneImpl(PhoneType.PHONE, type, null, null, null);
    }

    public AbstractContactReference(ContactReference other) {
        super(other);
        id = other.getId();
        email = other.getEmail();
        fax = other.getFax();
        mobile = other.getMobile();
        phone = other.getPhone();
        newsletterAccepted = other.isNewsletterAccepted();
    }

    protected AbstractContactReference(Element contact) {
        super(contact);
        Element addressXml = contact.getChild("address");
        if (addressXml != null) {
            company = addressXml.getChildText("header");
        }
        newsletterAccepted = JDOMUtil.fetchBoolean(contact, "newsletterAccepted");
        id = JDOMUtil.fetchLong(contact, "id");
        reference = JDOMUtil.fetchLong(contact, "reference");
        if (reference == null) {
            reference = JDOMUtil.fetchLong(contact, "contactId");
        }
    }

    public Long getPrimaryKey() {
        return id;
    }

    public Long getPersonId() {
        return id;
    }

    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Long getReference() {
        return reference;
    }

    public void setReference(Long reference) {
        this.reference = reference;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Phone getFax() {
        return fax;
    }

    public void setFax(Phone fax) {
        this.fax = fax;
    }

    public void addFax(String country, String prefix, String number) {
        this.fax = new PhoneImpl(PhoneType.FAX, getType(), country, prefix, number);
    }

    public Phone getMobile() {
        return mobile;
    }

    public void setMobile(Phone mobile) {
        this.mobile = mobile;
    }

    public void addMobile(String country, String prefix, String number) {
        this.fax = new PhoneImpl(PhoneType.MOBILE, getType(), country, prefix, number);
    }

    public Phone getPhone() {
        return phone;
    }

    public void setPhone(Phone phone) {
        this.phone = phone;
    }

    public void addPhone(String country, String prefix, String number) {
        this.fax = new PhoneImpl(PhoneType.PHONE, getType(), country, prefix, number);
    }

    public boolean isNewsletterAccepted() {
        return newsletterAccepted;
    }

    public void setNewsletterAccepted(boolean newsletterAccepted) {
        this.newsletterAccepted = newsletterAccepted;
    }

    @Override
    public String getDisplayName() {
        // company is separate property
        return ContactUtil.createPrivateDisplayName(getLastName(), getFirstName());
    }

    @Override
    public Element getXML(String name) {
        Element xml = super.getXML(name);
        xml.addContent(new Element("newsletterAccepted").setText(Boolean.toString(newsletterAccepted)));
        if (id != null) {
            xml.addContent(new Element("id").setText(id.toString()));
        } else {
            xml.addContent(new Element("id"));
        }
        if (reference != null) {
            xml.addContent(new Element("reference").setText(reference.toString()));
        }
        return xml;
    }

    @Override
    protected void addCompanyXML(Element address) {
        if (company != null) {
            address.addContent(new Element("company").setText(company));
            address.addContent(new Element("header").setText(company));
        } else {
            address.addContent(new Element("header"));
            address.addContent(new Element("company"));
        }
        addPersonXML(address);
    }

    @Override
    protected void createCommunications(Element contact) {
        Element phoneNumber = contact.getChild("phoneNumber");
        if (phoneNumber != null) {
            if (phone == null) {
                phone = new PhoneImpl();
            }
            if (phone instanceof PhoneImpl) {
                ((PhoneImpl) phone).setId(JDOMUtil.fetchLong(phoneNumber, "id"));
            }
            phone.setCountry(phoneNumber.getChildText("country"));
            phone.setPrefix(phoneNumber.getChildText("prefix"));
            phone.setNumber(phoneNumber.getChildText("number"));
        }
        Element faxNumber = contact.getChild("faxNumber");
        if (faxNumber != null) {
            if (fax == null) {
                fax = new PhoneImpl();
            }
            if (fax instanceof PhoneImpl) {
                ((PhoneImpl) fax).setId(JDOMUtil.fetchLong(faxNumber, "id"));
            }
            fax.setCountry(faxNumber.getChildText("country"));
            fax.setPrefix(faxNumber.getChildText("prefix"));
            fax.setNumber(faxNumber.getChildText("number"));
        }
        Element mobileNumber = contact.getChild("mobileNumber");
        if (mobileNumber != null) {
            if (mobile == null) {
                mobile = new PhoneImpl();
            }
            if (mobile instanceof PhoneImpl) {
                ((PhoneImpl) mobile).setId(JDOMUtil.fetchLong(mobileNumber, "id"));
            }
            mobile.setCountry(mobileNumber.getChildText("country"));
            mobile.setPrefix(mobileNumber.getChildText("prefix"));
            mobile.setNumber(mobileNumber.getChildText("number"));
        }
        email = contact.getChildText("email");
    }

    public Date getCreated() {
        return getPersonCreated();
    }

    public Long getCreatedBy() {
        return getPersonCreatedBy();
    }

    public Date getChanged() {
        return getPersonChanged();
    }

    public void setChanged(Date changed) {
        // properties are readonly
    }

    public Long getChangedBy() {
        return getPersonChangedBy();
    }

    public void setChangedBy(Long changedBy) {
        // properties are readonly
    }
}
