/**
 *
 * Copyright (C) 2002 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 03.05.2002 
 * 
 */
package com.osserp.common.util;

import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.Test;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class UPhoneTestData {

    @Test
    public void testCreateTestData() {
        List<String> numbersToTest = getPhoneNumbers();
        assertTrue(!numbersToTest.isEmpty());
    }

    @Test
    public void testLengthExpectsCountry() {
        List<String> numbersToTest = getPhoneNumbers();
        List<String[]> formattedNumbers = new ArrayList<String[]>();
        List<String> ignoredNumbers = new ArrayList<String>();
        List<String> undefinedNumbers = new ArrayList<String>();
        for (int i = 0, j = numbersToTest.size(); i < j; i++) {
            String number = numbersToTest.get(i);
            String[] phone = PhoneUtil.createPhoneNumber(number);
            if (phone == null) {
                ignoredNumbers.add(number);
            } else if (phone.length == 5) {
                formattedNumbers.add(phone);
            } else {
                //System.out.println("undefined: " + number + ", arraysize=" + phone.length);
                undefinedNumbers.add(number);
            }
        }
        assertTrue(undefinedNumbers.isEmpty());
        assertTrue(formattedNumbers.size() == 3490);
        assertTrue(ignoredNumbers.size() == 36);
    }

    
    // testdata
    
    protected List<String> getPhoneNumbers() {
        return _createPhoneNumbers();
    }
    
    private List<String> _testNumbers = new ArrayList<String>();
    private List<String> _createPhoneNumbers() {
        if (_testNumbers.isEmpty()) {
            List<String> raw = FileUtil.readResourceFile("phone_testdata.txt");
            Set<String> tmp = new HashSet<String>(); 
            for (int i = 0, j = raw.size(); i < j; i++) {
                String number = raw.get(i);
                if (number != null && number.length() > 0) {
                    tmp.add(number);
                }
            }
            _testNumbers = new ArrayList<String>(tmp);
        }
        return _testNumbers;
    }
}
