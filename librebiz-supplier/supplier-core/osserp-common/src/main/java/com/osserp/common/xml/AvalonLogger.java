package com.osserp.common.xml;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class AvalonLogger implements org.apache.avalon.framework.logger.Logger {
    private static AvalonLogger factory;
    
    private static Logger logger; 

    private AvalonLogger(Class classToLog) {
        logger = LoggerFactory.getLogger(classToLog); 
    }

    /**
     * @return serviceLocator instance
     */
    public static synchronized AvalonLogger getLogger(Class clazz) {
        if (AvalonLogger.factory == null) {
            AvalonLogger.factory = new AvalonLogger(clazz);
        }
        return AvalonLogger.factory;
    }
    
    /* (non-Javadoc)
     * @see org.apache.avalon.framework.logger.Logger#debug(java.lang.String)
     */
    public void debug(String message) {
        logger.debug(message);
    }

    /* (non-Javadoc)
     * @see org.apache.avalon.framework.logger.Logger#debug(java.lang.String, java.lang.Throwable)
     */
    public void debug(String message, Throwable throwable) {
        logger.debug(message, throwable);
    }

    /* (non-Javadoc)
     * @see org.apache.avalon.framework.logger.Logger#isDebugEnabled()
     */
    public boolean isDebugEnabled() {
        return logger.isDebugEnabled();
    }

    /* (non-Javadoc)
     * @see org.apache.avalon.framework.logger.Logger#info(java.lang.String)
     */
    public void info(String message) {
        logger.info(message);
    }

    /* (non-Javadoc)
     * @see org.apache.avalon.framework.logger.Logger#info(java.lang.String, java.lang.Throwable)
     */
    public void info(String message, Throwable throwable) {
        logger.info(message, throwable);
    }

    /* (non-Javadoc)
     * @see org.apache.avalon.framework.logger.Logger#isInfoEnabled()
     */
    public boolean isInfoEnabled() {
        return logger.isInfoEnabled();
    }

    /* (non-Javadoc)
     * @see org.apache.avalon.framework.logger.Logger#warn(java.lang.String)
     */
    public void warn(String message) {
        logger.warn(message);
    }

    /* (non-Javadoc)
     * @see org.apache.avalon.framework.logger.Logger#warn(java.lang.String, java.lang.Throwable)
     */
    public void warn(String message, Throwable throwable) {
        // TODO Auto-generated method stub
        
    }

    /* (non-Javadoc)
     * @see org.apache.avalon.framework.logger.Logger#isWarnEnabled()
     */
    public boolean isWarnEnabled() {
        // TODO Auto-generated method stub
        return false;
    }

    /* (non-Javadoc)
     * @see org.apache.avalon.framework.logger.Logger#error(java.lang.String)
     */
    public void error(String message) {
        logger.error(message);
    }

    /* (non-Javadoc)
     * @see org.apache.avalon.framework.logger.Logger#error(java.lang.String, java.lang.Throwable)
     */
    public void error(String message, Throwable throwable) {
        // TODO Auto-generated method stub
        
    }

    /* (non-Javadoc)
     * @see org.apache.avalon.framework.logger.Logger#isErrorEnabled()
     */
    public boolean isErrorEnabled() {
        // TODO Auto-generated method stub
        return false;
    }

    /* (non-Javadoc)
     * @see org.apache.avalon.framework.logger.Logger#fatalError(java.lang.String)
     */
    public void fatalError(String message) {
        logger.error(message);
    }

    /* (non-Javadoc)
     * @see org.apache.avalon.framework.logger.Logger#fatalError(java.lang.String, java.lang.Throwable)
     */
    public void fatalError(String message, Throwable throwable) {
        logger.error(message, throwable);
    }

    /* (non-Javadoc)
     * @see org.apache.avalon.framework.logger.Logger#isFatalErrorEnabled()
     */
    public boolean isFatalErrorEnabled() {
        return logger.isErrorEnabled();
    }

    /* (non-Javadoc)
     * @see org.apache.avalon.framework.logger.Logger#getChildLogger(java.lang.String)
     */
    public org.apache.avalon.framework.logger.Logger getChildLogger(String name) {
        return factory;
    }
}
