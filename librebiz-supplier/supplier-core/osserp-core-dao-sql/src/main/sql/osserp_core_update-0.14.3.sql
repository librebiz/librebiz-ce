SET search_path = osserp, pg_catalog;

UPDATE documents SET file_size = 709 WHERE file_name = 'xsl/shared/header_logo_row.vm';

UPDATE system_properties SET order_id = 0 WHERE name like 'document%';
UPDATE system_properties SET order_id = -1 where context_name = 'documentConfig' and is_admin;
UPDATE system_properties SET order_id = 1, displayable = true WHERE name = 'documentPrintFontSizeHeader';
UPDATE system_properties SET order_id = 2, displayable = true WHERE name = 'documentPrintFontSizeHeaderName';
UPDATE system_properties SET order_id = 3, displayable = true WHERE name = 'documentPrintFontSizeRecords';
UPDATE system_properties SET order_id = 4, displayable = true WHERE name = 'documentPrintFontSizeLetter';
UPDATE system_properties SET order_id = 5, displayable = true, pvalue = '7pt' WHERE name = 'documentPrintFontSizeFooter';
UPDATE system_properties SET order_id = 6, displayable = true WHERE name = 'documentPrintHeaderLogo';
UPDATE system_properties SET order_id = 7, displayable = true WHERE name = 'documentPrintHeaderTop';
UPDATE system_properties SET order_id = 8, displayable = true WHERE name = 'documentPrintHeaderLogoWidth';
UPDATE system_properties SET order_id = 9, displayable = true WHERE name = 'documentPrintHeaderLogoHeight';
UPDATE system_properties SET order_id = 11, displayable = true WHERE name = 'documentPrintHeaderLogoRepeat';
UPDATE system_properties SET order_id = 12, displayable = true WHERE name = 'documentPrintHeaderLogoRepeatMarginTop';
UPDATE system_properties SET order_id = 13, displayable = true WHERE name = 'documentPrintHeaderCompanySpace';
UPDATE system_properties SET order_id = 14, displayable = true WHERE name = 'documentPrintHeaderWidthAddress';
UPDATE system_properties SET order_id = 15, displayable = true WHERE name = 'documentPrintHeaderWidthCompany';
UPDATE system_properties SET order_id = 16, displayable = true WHERE name = 'documentPrintHeaderHeightTotal';
UPDATE system_properties SET order_id = 17, displayable = true WHERE name = 'documentPrintHeaderAddressSpace';
UPDATE system_properties SET order_id = 18, displayable = true WHERE name = 'documentPrintHeaderHeight';
UPDATE system_properties SET order_id = 19, displayable = true WHERE name = 'documentPrintHeaderContentSpaceLetter';

UPDATE system_properties SET order_id = 30, displayable = false WHERE name = 'documentPrintFontFamily';
UPDATE system_properties SET order_id = 31, displayable = false WHERE name = 'documentPrintHeaderLastnameFirstname';
UPDATE system_properties SET order_id = 32, displayable = false WHERE name = 'documentPrintFooterLastnameFirstname';
UPDATE system_properties SET order_id = 33, displayable = false WHERE name = 'documentPrintOffice';
UPDATE system_properties SET order_id = 34, displayable = false WHERE name = 'documentPrintAddDefaultCountry';
UPDATE system_properties SET order_id = 35, displayable = false WHERE name = 'documentFormatQuantityByDbms';
UPDATE system_properties SET order_id = 36, displayable = false WHERE name = 'documentPrintDownpaymentPayments';
UPDATE system_properties SET order_id = 37, displayable = false WHERE name = 'documentPrintFooterLayout';

UPDATE system_properties SET order_id = 51, displayable = false WHERE name = 'documentOutputDebugMap';
UPDATE system_properties SET order_id = 52, displayable = false WHERE name = 'documentOutputDebugXSL';
UPDATE system_properties SET order_id = 53, displayable = false WHERE name = 'documentOutputDebugXML';
UPDATE system_properties SET order_id = 54, displayable = false WHERE name = 'documentOutputDownloadMap';
UPDATE system_properties SET order_id = 55, displayable = false WHERE name = 'documentOutputDownloadXSL';
UPDATE system_properties SET order_id = 56, displayable = false WHERE name = 'documentOutputDownloadXML';


INSERT INTO system_properties VALUES (213, 'documentPrintHeaderLogoLeft', 'false', 'java.lang.Boolean', true, false, 'documentConfig', false, 10);
SELECT pg_catalog.setval('system_properties_id_seq', 213, true);

UPDATE system_properties SET pvalue = '0.14.3', ptype = 'java.lang.String' WHERE name = 'dbmsSchemaVersion';
