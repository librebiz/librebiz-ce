/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 4, 2011 2:32:50 PM 
 * 
 */
package com.osserp.core.dao.hrm;

import java.util.Date;
import java.util.List;

import com.osserp.core.employees.Employee;
import com.osserp.core.hrm.TimeRecord;
import com.osserp.core.hrm.TimeRecordType;
import com.osserp.core.hrm.TimeRecording;
import com.osserp.core.hrm.TimeRecordingApproval;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 */
public interface TimeRecordingApprovals {

    public static final String APPROVAL_EVENT_CREATED = "timeRecordingApprovalCreatedEvent";
    public static final String APPROVAL_EVENT_UPDATED = "timeRecordingApprovalUpdatedEvent";

    /**
     * Creates a new time recording approval request for new created records of given type
     * @param user
     * @param recording
     * @param type
     * @param start
     * @param end
     * @param note
     * @return timeRecordingApproval
     */
    TimeRecordingApproval createApproval(Employee user, TimeRecording recording, TimeRecordType type, Date start, Date end, String note);

    /**
     * Creates a new time recording approval request by updated record
     * @param user
     * @param recording
     * @param record
     * @param start
     * @param end
     * @param note
     * @return timeRecordingApproval
     */
    TimeRecordingApproval createApproval(Employee user, TimeRecording recording, TimeRecord record, Date start, Date end, String note);

    /**
     * Loads an existing approval request by id
     * @param id
     * @return timeRecordingApproval. Throws runtime exception if no such approval exists
     */
    TimeRecordingApproval getApproval(Long id);

    /**
     * Loads an approvals for given employee
     * @param employee
     * @return timeRecordingApprovals
     */
    List<TimeRecordingApproval> getApprovals(Employee employee);

    /**
     * Closes an approval request
     * @param user
     * @param approval
     * @param approved
     */
    void closeApproval(Employee user, TimeRecordingApproval approval, boolean approved);

}
