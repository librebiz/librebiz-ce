<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="popup" value="${requestScope.popup}" />
<c:set var="view" value="${requestScope.view}" />
<c:set var="businessCase" value="${view.businessCase}" />

<div class="table-responsive table-responsive-default">
    <table class="table table-striped">
        <thead>
            <tr>
                <th class="actionName"><fmt:message key="closedTodo" /></th>
                <th class="actionDate"><fmt:message key="date" /></th>
                <th class="actionUser"><fmt:message key="employee" /></th>
                <th class="action"><fmt:message key="note" /></th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="fcs" items="${businessCase.flowControlSheet}">
                <c:choose>
                    <c:when test="${empty fcs.canceledBy}">
                        <c:choose>
                            <c:when test="${!fcs.cancels}">
                                <%-- normal operation --%>
                                <tr>
                                    <td class="actionName"><c:choose>
                                            <c:when test="${popup or businessCase.closed or businessCase.cancelled}">
                                                <c:choose>
                                                    <c:when test="${fcs.action.displayReverse}">
                                                        <o:out value="${fcs.headline}" />
                                                    </c:when>
                                                    <c:when test="${fcs.action.cancelling and businessCase.cancelled and view.providingResetCancel}">
                                                        <a href="javascript:onclick=confirmLink('<fmt:message key="confirmResetCancellation"/>','<v:url value="${view.baseLink}/resetCancellation"/>');" title="<fmt:message key="resetCancellation"/>"> <o:out
                                                                value="${fcs.action.name}" />
                                                        </a>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <o:out value="${fcs.action.name}" />
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:when>
                                            <c:otherwise>
                                                <a href="javascript:onclick=confirmLink('<fmt:message key="confirmCancelAction"/>','<v:url value="${view.baseLink}/selectCancel?id=${fcs.id}"/>');" title="<fmt:message key="cancelAction"/>"> <c:choose>
                                                        <c:when test="${fcs.action.displayReverse}">
                                                            <o:out value="${fcs.headline}" />
                                                        </c:when>
                                                        <c:otherwise>
                                                            <o:out value="${fcs.action.name}" />
                                                        </c:otherwise>
                                                    </c:choose>
                                                </a>
                                            </c:otherwise>
                                        </c:choose></td>
                                    <td class="actionDate"><o:date value="${fcs.created}" addcentury="false" /></td>
                                    <td class="actionUser"><oc:employee value="${fcs.employeeId}" /></td>
                                    <td class="action"><v:ajaxLink url="/fcs/flowControlNote/forward?id=${fcs.id}" linkId="flowControlNote">
                                            <c:choose>
                                                <c:when test="${empty fcs.note}">
                                                    <o:img name="closedFolderIcon" />
                                                </c:when>
                                                <c:otherwise>
                                                    <o:img name="openFolderIcon" />
                                                </c:otherwise>
                                            </c:choose>
                                        </v:ajaxLink></td>
                                </tr>
                            </c:when>
                            <c:otherwise>
                                <c:if test="${view.displayCanceledMode}">
                                    <%-- display cancelling action --%>
                                    <tr>
                                        <td class="actionName error"><c:choose>
                                                <c:when test="${fcs.action.displayReverse}">
                                                    <o:out value="${fcs.headline}" />
                                                </c:when>
                                                <c:otherwise>
                                                    <o:out value="${fcs.action.name}" />
                                                </c:otherwise>
                                            </c:choose></td>
                                        <td class="actionDate error"><o:date value="${fcs.created}" addcentury="false" /></td>
                                        <td class="actionUser error"><oc:employee value="${fcs.employeeId}" /></td>
                                        <td class="action"><v:ajaxLink url="/fcs/flowControlNote/forward?id=${fcs.id}" linkId="flowControlNote">
                                                <c:choose>
                                                    <c:when test="${empty fcs.note}">
                                                        <o:img name="closedFolderIcon" />
                                                    </c:when>
                                                    <c:otherwise>
                                                        <o:img name="openFolderIcon" />
                                                    </c:otherwise>
                                                </c:choose>
                                            </v:ajaxLink></td>
                                    </tr>
                                </c:if>
                            </c:otherwise>
                        </c:choose>
                    </c:when>
                    <c:otherwise>
                        <c:if test="${view.displayCanceledMode}">
                            <%-- display cancelled action --%>
                            <tr>
                                <td class="actionName error canceled"><c:choose>
                                        <c:when test="${fcs.action.displayReverse}">
                                            <o:out value="${fcs.headline}" />
                                        </c:when>
                                        <c:otherwise>
                                            <o:out value="${fcs.action.name}" />
                                        </c:otherwise>
                                    </c:choose></td>
                                <td class="actionDate error canceled"><o:date value="${fcs.created}" addcentury="false" /></td>
                                <td class="actionUser error canceled"><oc:employee value="${fcs.employeeId}" /></td>
                                <td class="action"><v:ajaxLink url="/fcs/flowControlNote/forward?id=${fcs.id}" linkId="flowControlNote">
                                        <c:choose>
                                            <c:when test="${empty fcs.note}">
                                                <o:img name="closedFolderIcon" />
                                            </c:when>
                                            <c:otherwise>
                                                <o:img name="openFolderIcon" />
                                            </c:otherwise>
                                        </c:choose>
                                    </v:ajaxLink></td>
                            </tr>
                        </c:if>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </tbody>
    </table>
</div>

