/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 05-Aug-2006 
 * 
 */
package com.osserp.gui.web.records;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionException;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.struts.StrutsForm;
import com.osserp.gui.client.Actions;
import com.osserp.gui.client.records.OrderView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractOrderAction extends AbstractRecordAction {
    private static Logger log = LoggerFactory.getLogger(AbstractOrderAction.class.getName());

    /**
     * Enables open delivery display
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward enableOpenDeliveryDisplay(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableOpenDeliveryDisplay() request from " + getUser(session).getId());
        }
        OrderView view = getOrderView(session);
        view.setOpenDeliveryDisplay(true);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Disables open delivery display
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward disableOpenDeliveryDisplay(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disableOpenDeliveryDisplay() request from " + getUser(session).getId());
        }
        OrderView view = getOrderView(session);
        view.setOpenDeliveryDisplay(false);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Enables delivery date edit mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward enableDeliveryDateEdit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableDeliveryDateEdit() request from " + getUser(session).getId());
        }
        OrderView view = getOrderView(session);
        view.setDeliveryDateEditMode(true);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Disables delivery date edit mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward disableDeliveryDateEdit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disableDeliveryDateEdit() request from " + getUser(session).getId());
        }
        OrderView view = getOrderView(session);
        view.setDeliveryDateEditMode(false);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Updates delivery date
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward updateDeliveryDate(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("updateDeliveryDate() request from " + getUser(session).getId());
        }
        OrderView view = getOrderView(session);
        try {
            view.updateDeliveryDate(new StrutsForm(form));
        } catch (ClientException c) {
            saveError(request, c.getMessage());
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Toggles item delivery date mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward changeItemDeliveryDateMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("changeItemDeliveryDateMode() request from " + getUser(session).getId());
        }
        OrderView view = getOrderView(session);
        view.changeItemDeliveryDateEditMode(RequestUtil.fetchId(request));
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Changes delivery confirmation
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward changeDeliveryConfirmation(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("changeDeliveryConfirmation() request from " + getUser(session).getId());
        }
        OrderView view = getOrderView(session);
        view.changeDeliveryConfirmation(RequestUtil.getId(request));
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Enables stock edit mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward enableStockEditMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableStockEditMode() request from " + getUser(session).getId());
        }
        OrderView view = getOrderView(session);
        view.setStockEditMode(true);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Disables stock edit mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward disableStockEditMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disableStockEditMode() request from " + getUser(session).getId());
        }
        OrderView view = getOrderView(session);
        view.setStockEditMode(false);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Updates stock flag
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward updateStock(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("updateStock() request from " + getUser(session).getId());
        }
        OrderView view = getOrderView(session);
        Long productId = RequestUtil.getLong(request, "productId");
        Long stockId = RequestUtil.getLong(request, "stockId");
        view.updateStock(productId, stockId);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Creates a new version of current sales order
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward createNewVersion(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("createNewVersion() request from " + getUser(session).getId());
        }
        OrderView view = createVersionView(request);
        try {
            view.createNewVersion();
            return mapping.findForward(view.getActionTarget());
        } catch (Exception e) {
            log.warn("createNewVersion() failed [message=" + e.getMessage() + "]", e);
            RequestUtil.saveError(request, e.getMessage());
            return mapping.findForward(Actions.EXIT);
        }
    }

    /**
     * Prints a version of the sales order by document id
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward printVersion(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("printVersion() request from " + getUser(session).getId());
        }
        OrderView view = getOrderView(session);
        try {
            return forwardPdfOutput(mapping, request, view.getVersion(RequestUtil.fetchId(request)));
        } catch (Throwable t) {
            log.warn("printVersion() failed [message=" + t.toString() + "]");
            RequestUtil.saveError(request, ErrorCode.DOCUMENT_NOT_FOUND);
            return mapping.findForward(view.getActionTarget());
        }
    }

    protected abstract OrderView createVersionView(HttpServletRequest request);

    protected OrderView getOrderView(HttpSession session) throws PermissionException {
        return (OrderView) fetchView(session);
    }
}
