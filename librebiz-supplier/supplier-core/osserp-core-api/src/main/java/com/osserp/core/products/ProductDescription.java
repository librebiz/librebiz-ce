/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 26, 2009 11:57:22 AM 
 * 
 */
package com.osserp.core.products;

import org.jdom2.Element;

import com.osserp.common.Option;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ProductDescription extends Option {

    /**
     * Provides the language code of the description, e.g. de, en, etc.
     * @return language
     */
    String getLanguage();

    /**
     * Provides a text for datasheet rendering
     * @return datasheetText
     */
    String getDatasheetText();

    /**
     * Sets a text for datasheet rendering
     * @param datasheetText
     */
    void setDatasheetText(String datasheetText);

    /**
     * A short version of the product description. 
     * @return descriptionShort
     */
    String getDescriptionShort();

    /**
     * Sets the short version of the description
     * @param descriptionShort
     */
    void setDescriptionShort(String descriptionShort);

    /**
     * The marketing text of the product
     * @return marketingText
     */
    String getMarketingText();

    /**
     * Sets marketing text for the product
     * @param marketingText
     */
    void setMarketingText(String marketingText);

    /**
     * Provides an xml representation of the product description
     * @return product description values
     */
    Element getXML();

    /**
     * Provides an xml representation of the product description
     * @param name of the created element
     * @return product description values
     */
    Element getXML(String rootElementName);
}
