/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 21, 2006 6:09:36 PM 
 * 
 */
package com.osserp.gui.client.contacts.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.Option;
import com.osserp.common.PermissionException;
import com.osserp.common.User;
import com.osserp.common.web.Actions;
import com.osserp.common.web.Form;

import com.osserp.core.NoteManager;
import com.osserp.core.Options;
import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.ContactAssignmentManager;
import com.osserp.core.contacts.ContactCountry;
import com.osserp.core.contacts.ContactDeleteManager;
import com.osserp.core.contacts.ContactManager;
import com.osserp.core.contacts.ContactNoteManager;
import com.osserp.core.contacts.ContactSearch;
import com.osserp.core.contacts.Person;
import com.osserp.core.contacts.PersonManager;
import com.osserp.core.contacts.PrivateContact;
import com.osserp.core.contacts.PrivateContactManager;
import com.osserp.core.users.DomainUser;
import com.osserp.core.users.DomainUserManager;

import com.osserp.gui.client.contacts.ContactView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractContactView extends AbstractPersonView implements ContactView {
    private static Logger log = LoggerFactory.getLogger(AbstractContactView.class.getName());

    private boolean assignMode = false;
    private boolean deleteMode = false;
    private boolean typeChangeMode = false;
    private String companyName = null;

    private User userAccount = null;
    private boolean privateContactOfCurrentEmployee = false;
    private List<Contact> contactPersons = new ArrayList<>();
    private boolean contactAddressMapSupport = false;

    /**
     * Sets view name if annotated and initializes action target as 'display' and forward target as 'contactDisplay'
     */
    protected AbstractContactView() {
        super();
        setActionTarget(Actions.DISPLAY);
        setForwardTarget("contactDisplay");
    }

    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        super.init(request);
        contactAddressMapSupport = isSystemPropertyEnabled("contactAddressMapSupport");
    }

    /**
     * Indicates that contact view handles unclassified default contacts or classified related contacts such as customers, suppliers, etc.
     * @return true if implementing view handles classified contacts (a customer view for example)
     */
    public abstract boolean isRelatedContactView();

    @Override
    public void load(Person person) throws ClientException {
        PersonManager manager = getPersonManager();
        if (person instanceof Contact) {
            Contact contact = (Contact) person;
            if (log.isDebugEnabled()) {
                log.debug("load(person) contact found [contact=" + contact.getContactId()
                        + ", client=" + contact.isClient()
                        + ", view=" + getClass().getName()
                        + "]");
            }
            Person updated = manager.find(contact.getContactId());
            setBean(updated);
            NoteManager noteManager = (NoteManager) getService(ContactNoteManager.class.getName());
            setStickyNote(noteManager.getSticky(contact.getContactId()));
            if (!contact.isContactPerson()) {
                ContactSearch search = (ContactSearch) getService(ContactSearch.class.getName());
                contactPersons = search.findContactPersons(contact.getContactId());
            }
        }
    }

    public void delete() throws ClientException {
        Contact contact = getCurrent();
        ContactDeleteManager deleteManager = (ContactDeleteManager) getService(ContactDeleteManager.class.getName());
        deleteManager.deleteContact(getDomainEmployee(), contact);
        disableAllModes();
        setBean(null);
    }

    public boolean isDeleteMode() {
        return deleteMode;
    }

    public void setDeleteMode(boolean deleteMode) {
        this.deleteMode = deleteMode;
    }

    public boolean isCustomFederalState() {
        Contact c = (Contact) getBean();
        if (c != null && c.getAddress() != null && c.getAddress().getCountry() != null) {
            Option o = getOption(Options.COUNTRY_NAMES, c.getAddress().getCountry());
            if (o != null && o instanceof ContactCountry) {
                ContactCountry cc = (ContactCountry) o;
                return cc.isCustomFederalState();
            }
        }
        return false;
    }

    public boolean isAssignModeAvailable() {
        Contact current = getCurrent();
        return (!current.isCustomer()
                || !current.isEmployee()
                || !current.isSupplier());
    }

    public boolean isAssignMode() {
        return assignMode;
    }

    public void setAssignMode(boolean assignMode) {
        disableAllModes();
        this.assignMode = assignMode;
    }

    public void assignContact(Long groupId) throws Exception {
        Contact current = getCurrent();
        ContactAssignmentManager manager = (ContactAssignmentManager) getService(ContactAssignmentManager.class.getName());
        if (log.isDebugEnabled()) {
            log.debug("assignContact() invoked [contact=" + current.getContactId()
                    + ", selectedGroup=" + groupId + "]");
        }

        try {
            if (Contact.CUSTOMER.equals(groupId)) {
                manager.assignCustomer(getDomainEmployee(), current); // status is unknown yet
                if (log.isDebugEnabled()) {
                    log.debug("assignContact() new customer added");
                }

            } else if (Contact.SUPPLIER.equals(groupId)) {
                Long id = manager.assignSupplier(getDomainEmployee(), current);
                if (log.isDebugEnabled()) {
                    log.debug("assignContact() new supplier added: " + id);
                }
            }
            refresh();
            disableAllModes();

        } catch (Exception e) {
            disableAllModes();
            throw e;
        }
    }

    public boolean isTypeChangeMode() {
        return typeChangeMode;
    }

    public void setTypeChangeMode(boolean typeChangeMode) throws PermissionException {
        disableAllModes();
        setEditMode(true);
        this.typeChangeMode = typeChangeMode;
    }

    public void changeType(Form fh) throws ClientException {
        companyName = fh.getString(Form.COMPANY, true);
        PersonManager manager = getPersonManager();
        manager.changeType(
                getDomainEmployee(),
                getCurrent(),
                companyName);
        disableAllModes();
        companyName = null;
    }

    public String getCompanyName() {
        return companyName;
    }

    public User getUserAccount() {
        return userAccount;
    }

    protected void setUserAccount(User userAccount) {
        this.userAccount = userAccount;
    }

    public boolean isUserCurrentContact() {
        try {
            User user = getDomainUser();
            Contact selected = getCurrent();
            return selected.getContactId().equals(user.getContactId());
        } catch (Throwable t) {
        }
        return false;
    }

    public boolean isPrivateContactOfCurrentEmployee() {
        return privateContactOfCurrentEmployee;
    }

    public void setPrivateContactOfCurrentEmployee(Boolean privateContactOfCurrentEmployee) {
        this.privateContactOfCurrentEmployee = privateContactOfCurrentEmployee;
    }

    public final boolean isPrivateContactCreateable() {
        Contact selected = (Contact) getBean();
        if (selected != null && !selected.isClient() && !selected.isGrantContactNone()) {
            DomainUser user = getDomainUser();
            return isPrivateContactCreateable(user, selected);
        }
        return false;

    }

    /**
     * Method is invoked by {@link #isPrivateContactCreateable()} after performing initial checks for enabled groupware and existing selection. <br/>
     * Default implementation always returns true. Override this if implementing view requires additional checks.
     * @param user
     * @return true if not overridden
     */
    protected boolean isPrivateContactCreateable(DomainUser user, Contact selected) {
        return true;
    }

    public final void enablePrivateContact() {
        DomainUser user = getDomainUser();
        Contact selected = getCurrent();
        PrivateContactManager pcm = getPrivateContactManager();
        this.createPrivateContact();
        if (pcm.isPrivate(user, selected)) {
            privateContactOfCurrentEmployee = true;
            if (log.isDebugEnabled()) {
                log.debug("enablePrivateContact() done [employee="
                        + user.getId()
                        + ", contact="
                        + selected.getContactId()
                        + "]");
            }
        }
    }

    /**
     * Method is invoked by by {@link #enablePrivateContact()} to invoke managers create private contact method. <br/>
     * Override if implementing view requires any other than simple create operation.
     * @param manager
     */
    protected void createPrivateContact() {
        DomainUser user = getDomainUser();
        Contact selected = getCurrent();
        getPrivateContactManager().createPrivate(user, selected, null);
    }

    public void disablePrivateContact() {
        DomainUser user = getDomainUser();
        Contact selected = getCurrent();
        PrivateContactManager manager = getPrivateContactManager();
        PrivateContact contact = manager.getPrivate(user, selected);
        if (contact != null) {
            manager.disable(contact);
            privateContactOfCurrentEmployee = false;
            if (log.isDebugEnabled()) {
                log.debug("disablePrivateContact() done [employee="
                        + user.getId()
                        + ", contact="
                        + selected.getContactId()
                        + "]");
            }
        }
    }

    public void disableAllModes() {
        assignMode = false;
        deleteMode = false;
        typeChangeMode = false;
        try {
            setEditMode(false);
            setCreateMode(false);
        } catch (PermissionException e) {
            // should not happen when disabling
        }
        setForm(null);
    }

    /**
     * Casts current bean to contact if available.
     * @return contact
     * @throws ActionException invalid context runtime exception if no such contact exists
     */
    protected Contact getCurrent() {
        if (!(getBean() instanceof Contact)) {
            throw new ActionException("no activated contact!");
        }
        return (Contact) getBean();
    }

    public Long getCurrentFederalState() {
        Long federalState = null;
        if (getBean() != null) {
            Contact contact = (Contact) getBean();
            federalState = contact.getAddress().getFederalStateId();
        }
        return federalState;
    }

    /**
     * Provides 'contactId' as select property name
     * @return contactId
     */
    @Override
    protected String getSelectPropertyName() {
        return "contactId";
    }

    @Override
    protected void setBean(Object bean) {
        if (log.isDebugEnabled()) {
            log.debug("setBean() invoked [" + (bean == null ? "null" : bean.getClass().getName()) + "]");
        }
        super.setBean(bean);
        if (bean != null && bean instanceof Contact) {
            DomainUserManager userManager = (DomainUserManager) getService(com.osserp.common.UserManager.class.getName());
            Contact current = (Contact) bean;
            userAccount = userManager.find(current);
            DomainUser user = getDomainUser();
            privateContactOfCurrentEmployee = !getPrivateContactManager().isPrivateUnused(user, current);
        }
    }

    public boolean isContactPersonWithMobilePhoneExisting() {
        if (!contactPersons.isEmpty()) {
            for (int i = 0, j = contactPersons.size(); i < j; i++) {
                Contact next = contactPersons.get(i);
                if (!next.getMobiles().isEmpty()) {
                    return true;
                }
            }
        }
        return false;
    }

    public List<Contact> getContactPersons() {
        return contactPersons;
    }

    protected void setContactPersons(List<Contact> contactPersons) {
        this.contactPersons = contactPersons;
    }

    public boolean isContactAddressMapSupport() {
        return contactAddressMapSupport;
    }

    protected void setContactAddressMapSupport(boolean contactAddressMapSupport) {
        this.contactAddressMapSupport = contactAddressMapSupport;
    }

    public boolean isCustomerView() {
        return false;
    }

    public boolean isEmployeeView() {
        return false;
    }

    public boolean isInstallerView() {
        return false;
    }

    public boolean isClientView() {
        return false;
    }

    public boolean isSupplierView() {
        return false;
    }
    
    protected ContactManager getCommonContactManager() {
        return (ContactManager) getService(ContactManager.class.getName());
    }

    protected PrivateContactManager getPrivateContactManager() {
        return (PrivateContactManager) getService(PrivateContactManager.class.getName());
    }
}
