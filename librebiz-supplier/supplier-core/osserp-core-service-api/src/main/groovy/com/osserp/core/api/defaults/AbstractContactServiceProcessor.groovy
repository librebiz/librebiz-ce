/**
 *
 * Copyright (C) 2012, 2021 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License (AGPL)
 * version 3 as published by the Free Software Foundation. In accordance
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code
 * versions must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area
 * or, if not reasonably feasible for technical reason, as a top-level
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute.
 *
 */
package com.osserp.core.api.defaults

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.api.WSBusinessCase
import com.osserp.api.WSContact

import com.osserp.common.ClientException
import com.osserp.common.Constants
import com.osserp.common.Option

import com.osserp.core.contacts.Contact
import com.osserp.core.contacts.Salutation
import com.osserp.core.customers.Customer
import com.osserp.core.dao.Customers
import com.osserp.core.dao.ContactCreator
import com.osserp.core.dao.Contacts

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class AbstractContactServiceProcessor {
    private static Logger logger = LoggerFactory.getLogger(AbstractContactServiceProcessor.class.getName())

    private ContactCreator contactCreator
    private Contacts contacts

    protected AbstractContactServiceProcessor(ContactCreator contactCreator, Contacts contacts) {
        super();
        this.contactCreator = contactCreator
        this.contacts = contacts
    }
    
    protected Contact createContact(WSContact obj, String emailAddress, Long salesPersonId) {
        boolean privateContact = obj.privateContact
        Contact contact = null

        Long type = obj.contactTypeId

        Salutation salutation = contactCreator.findSalutation(obj.salutationId, obj.salutation, false)
        Option title = contactCreator.findTitle(obj.titleId, obj.title, false)

        String company = obj.company
        String firstName = obj.firstName
        String lastName = obj.lastName
        String street = obj.street
        String streetAddon = obj.streetAddon
        String zipcode = obj.zipcode
        String city = obj.city
        Long country = obj.countryId

        Date birthDate = obj.dateOfBirth

        String fax = obj.businessFax
        String phone = obj.businessPhone
        String mobile = obj.businessMobile

        String email = emailAddress ?: obj.businessEmail

        if (!lastName && !company && email) {
            // some internet forms do not require personal informations
            // but we require for a not empty lastName property
            lastName = email
        }
        contact = contactCreator.create(
                salesPersonId,
                type,
                company,
                salutation,
                title,
                firstName,
                lastName,
                birthDate,
                street,
                streetAddon,
                zipcode,
                city,
                country,
                email,
                phone,
                mobile,
                fax,
                obj.grantEmail,
                obj.grantPhone,
                obj.grantNon,
                obj.externalKey,
                obj.externalUrl)
        return contact
    }

    protected ContactCreator getContactCreator() {
        return contactCreator
    }

    public void setContactCreator(ContactCreator contactCreator) {
        this.contactCreator = contactCreator
    }

    protected Contacts getContacts() {
        return contacts
    }

    public void setContacts(Contacts contacts) {
        this.contacts = contacts
    }
}
