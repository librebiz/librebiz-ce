/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 17.12.2012 
 * 
 */
package com.osserp.core.api.defaults

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.api.WSBusinessCase
import com.osserp.api.WSContact

import com.osserp.common.ClientException
import com.osserp.common.Constants
import com.osserp.common.Option

import com.osserp.core.BusinessCase
import com.osserp.core.contacts.Contact
import com.osserp.core.contacts.Salutation
import com.osserp.core.customers.Customer
import com.osserp.core.dao.Customers
import com.osserp.core.dao.ContactCreator
import com.osserp.core.dao.Contacts

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class CoreCustomerServiceProcessor extends AbstractContactServiceProcessor {
    private static Logger logger = LoggerFactory.getLogger(CoreCustomerServiceProcessor.class.getName())

    Customers customers

    protected CoreCustomerServiceProcessor(
        ContactCreator contactCreator,
        Contacts contacts,
        Customers customers) {
        super(contactCreator, contacts)
        this.customers = customers
    }

    Customer findOrCreate(WSBusinessCase request) {
        Customer customer = null

        if (request.customerId) {

            customer = customers.find(request.customerId)
        } else if (request?.contact) {

            customer = createOrUpdate(request.contact, request.salesPersonId)
        } else {
            logger.warn("findOrCreate: businessCase.customerId and businessCase.contact missing [id=${request?.primaryKey}]")
        }
        return customer
    }

    Customer override(BusinessCase businessCase, WSContact obj) {
        return update(businessCase.customer, obj)
    }

    Customer createOrUpdate(WSContact wscontact) {
        return createOrUpdate(wscontact, Constants.SYSTEM_EMPLOYEE)
    }

    protected final Customer createOrUpdate(WSContact wscontact, Long salesPersonId) {
        Customer customer = findByKeys(wscontact)
        if (customer) {
            return update(customer, wscontact)
        }

        if (wscontact.externalKey) {
            customer = (Customer) customers.findByExternalReference(wscontact.externalKey)
            if (customer?.id) {
                logger.debug("findOrCreate: customer found by externalKey [id=${customer.id}, key=${customer.externalReference}]")
                return customer
            }
        }
        if (wscontact.primaryKey) {
            customer = (Customer) customers.find(wscontact.primaryKey)
            if (customer?.id) {
                logger.debug("findOrCreate: customer found by primaryKey [id=${customer.id}, key=${customer.externalReference}]")
                return customer
            }
            customer = createCustomerById(wscontact, salesPersonId)
            if (customer) {
                return customer
            }
        }
        if (wscontact.businessEmail) {
            customer = customers.findByEmail(wscontact.businessEmail)
            if (customer == null) {
                customer = createCustomerByEmail(wscontact, wscontact.businessEmail, salesPersonId)
            }
        } else {
            customer = createCustomer(wscontact, salesPersonId)
        }
        return customer
    }

    protected final Customer createCustomer(WSContact wscontact, Long salesPersonId) {
        Customer customer = null
        Contact contact = createContact(wscontact, (String) null, salesPersonId)
        if (contact) {
            boolean reseller = 'reseller' == wscontact.status ? true : false
            customer = customers.create(wscontact.primaryKey, contact, salesPersonId, reseller)
            if (customer) {
                logger.debug("createCustomer: done [id=${customer?.id}, name=${customer?.lastName}]")
            }
        }
        return customer
    }

    protected final Customer createCustomerById(WSContact wscontact, Long salesPersonId) {
        Customer customer = null
        Contact contact = createContact(wscontact, (String) null, salesPersonId)
        if (contact) {
            boolean reseller = 'reseller' == wscontact.status ? true : false
            customer = customers.create(wscontact.primaryKey, contact, salesPersonId, reseller)
            if (customer) {
                logger.debug("createCustomerById: done [id=${customer?.id}, name=${customer?.lastName}]")
            }
        }
        return customer
    }

    protected final Customer createCustomerByEmail(WSContact wscontact, String emailAddress, Long salesPersonId) {
        Customer customer = null
        // We know that another customer with provided address does not exist.
        // But do we know if another contact of other type with same address exists?
        Contact contact = contacts.fetchByEmailAddress(emailAddress)
        if (!contact) {
            contact = createContact(wscontact, emailAddress, salesPersonId)
        }
        if (contact) {
            customer = customers.create(salesPersonId, contact, null)
            if (customer) {
                boolean reseller = 'reseller' == wscontact.status ? true : false
                customer.setTypeId(Customer.TYPE_RESELLER)
                customers.save(customer)
                logger.debug("createCustomerByEmail: done [id=${customer?.id}, email=${customer?.email}]")
            }
        }
        return customer
    }

    /**
     * Tries to find existing customer by primaryKey, externalKey and email.
     * @param wscontact
     * @return existing customer or null
     */
    protected final Customer findByKeys(WSContact wscontact) {
        if (wscontact.externalKey) {
            Customer customer = customers.findByExternalReference(wscontact.externalKey)
            if (customer?.id) {
                logger.debug("findByKeys: customer found by externalKey [id=${customer.id}, key=${customer.externalReference}]")
                return customer
            }
        }
        if (wscontact.primaryKey) {
            Customer customer = customers.find(wscontact.primaryKey)
            if (customer?.id) {
                logger.debug("findByKeys: customer found by primaryKey [id=${customer.id}, key=${customer.externalReference}]")
                return customer
            }
        }
        if (wscontact.businessEmail) {
            Customer customer = customers.findByEmail(wscontact.businessEmail)
            if (customer?.id) {
                logger.debug("findByKeys: customer found by email [id=${customer.id}, key=${wscontact.businessEmail}]")
                return customer
            }
        }
        return null
    }

    protected Customer update(Customer customer, WSContact obj) {
        boolean privateContact = customer.privatePerson
        Salutation salutation = contactCreator.findSalutation(obj.salutationId, obj.salutation, false)
        Option title = contactCreator.findTitle(obj.titleId, obj.title, false)
        return customers.update(
                customer,
                salutation,
                title,
                (!privateContact ? null : obj.firstName),
                ((!privateContact && obj.company) ? obj.company : obj.lastName),
                obj.street,
                obj.streetAddon,
                obj.zipcode,
                obj.city,
                obj.countryId,
                obj.dateOfBirth,
                obj.grantEmail,
                obj.grantPhone,
                obj.grantNon,
                obj.businessFax,
                obj.businessPhone,
                obj.businessMobile)
    }
}
