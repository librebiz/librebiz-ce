<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" scope="page" value="${sessionScope.telephoneView}"/>
<c:set var="sortUrl" scope="page" value="/telephones/telephone/sort"/>

<c:if test="${!empty sessionScope.error}">
	&nbsp;
	<div class="errormessage">
		<fmt:message key="error"/>: <fmt:message key="${sessionScope.error}"/>
	</div>
    <o:removeErrors/>
</c:if>
<c:if test="${!empty view}">
	<table class="table table-striped">
		<thead>
			<tr>
				<th><v:ajaxLink url="${sortUrl}?key=mac" targetElement="ajaxContent"><fmt:message key="macAddress"/></v:ajaxLink></th>
				<th><v:ajaxLink url="${sortUrl}?key=telephoneSystem.name" targetElement="ajaxContent"><fmt:message key="telephoneSystem"/></v:ajaxLink></th>
				<th><v:ajaxLink url="${sortUrl}?key=telephoneType.name" targetElement="ajaxContent"><fmt:message key="type"/></v:ajaxLink></th>
				<th class="scrollbar"></th>
			</tr>
		</thead>
		<tbody class="scrolly" style="height:414px;">
			<c:choose>
				<c:when test="${empty view.list}">
					<tr>
						<td colspan="4"><fmt:message key="error.no.telephones.found"/></td>
					</tr>
				</c:when>
				<c:otherwise>
					<c:forEach var="obj" items="${view.list}">
						<tr id="telephone_${obj.id}">
							<td id="telephone_mac_address_${obj.id}">
								<o:out value="${obj.mac}"/>
							</td>
							<td id="telephone_system_${obj.id}">
								<v:ajaxLink url="/telephones/telephoneEdit/enableEditMode?name=telephone_system&id=${obj.id}" targetElement="telephone_system_${obj.id}" title="edit">
									<c:choose>
										<c:when test="${!empty obj.telephoneSystem.name}"><o:out value="${obj.telephoneSystem.name}"/></c:when>
										<c:otherwise>(<fmt:message key="notSet"/>)</c:otherwise>
									</c:choose>
								</v:ajaxLink>
							</td>
							<td id="telephone_type_${obj.id}">
								<v:ajaxLink url="/telephones/telephoneEdit/enableEditMode?name=telephone_type&id=${obj.id}" targetElement="telephone_type_${obj.id}" title="edit">
									<c:choose>
										<c:when test="${!empty obj.telephoneType.name}"><o:out value="${obj.telephoneType.name}"/></c:when>
										<c:otherwise>(<fmt:message key="notSet"/>)</c:otherwise>
									</c:choose>
								</v:ajaxLink>
							</td>
							<td></td>
						</tr>
					</c:forEach>
						<tr><td colspan="4" style="height:100%;"></td></tr>
				</c:otherwise>
			</c:choose>
		</tbody>
	</table>
</c:if>
