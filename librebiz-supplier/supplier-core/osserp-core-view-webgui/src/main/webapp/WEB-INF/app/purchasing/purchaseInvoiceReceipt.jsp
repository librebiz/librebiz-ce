<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="purchaseInvoiceReceiptView" />
<c:set var="records" value="${view.list}" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="styles" type="string">
        <style type="text/css">
        #purchaseListSearch_popup {
            width:40%;
        }
        </style>
    </tiles:put>
    <tiles:put name="headline">
        <fmt:message key="openStockReceipts" />
        <c:set var="listNavMargin" value="145" />
        <c:if test="${view.listStartDate != null && view.listStopDate != null}">
            <c:set var="listNavMargin" value="45" />
            (<o:date value="${view.listStartDate}" /> <fmt:message key="til" />
            <o:date value="${view.listStopDate}" />)
        </c:if>
        <span style="margin-left: ${listNavMargin}px;"> 
            <v:link url="${view.baseLink}/listPrevious"><span>&lt;&lt;&lt;</span></v:link> 
            [<o:out value="${view.listStart}" />-<o:out value="${view.listEnd}" />]
            <v:link url="${view.baseLink}/listNext"><span>&gt;&gt;&gt;</span></v:link>
        </span>
    </tiles:put>
    <tiles:put name="headline_right">
        <ul>
            <li><v:link url="${view.baseLink}/exit"><o:img name="backIcon" /></v:link></li>
            <li><v:link url="/purchasing/purchaseInvoiceCreator/forward?exit=/purchasing/purchaseInvoiceArchive/reload"><o:img name="newdataIcon" /></v:link></li>
            <li>
                <v:ajaxLink linkId="purchaseListSearch" url="${view.baseLink}/forwardSearch" title="filter">
                    <o:img name="calendarIcon" />
                </v:ajaxLink>
            </li>
            <li><v:link url="/purchasing/purchaseInvoiceSearch/forward?exit=/purchasing/purchaseInvoiceReceipt/reload"><o:img name="searchIcon" /></v:link></li>
            <li><v:link url="${view.baseLink}/toggleListItems" title="productDisplay"><o:img name="reloadIcon" /></v:link></li>
            <li><v:link url="/index" title="backToMenu"><o:img name="homeIcon" /></v:link></li>
        </ul>
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="purchaseInvoiceReceiptContent">
            <div class="row">
                <div class="col-lg-12 panel-area">
                    <c:choose>
                        <c:when test="${view.listItems}">
                            <c:import url="_purchaseInvoiceListingByItem.jsp"/>
                        </c:when>
                        <c:otherwise>
                            <c:import url="_purchaseInvoiceListingByRecord.jsp"/>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
