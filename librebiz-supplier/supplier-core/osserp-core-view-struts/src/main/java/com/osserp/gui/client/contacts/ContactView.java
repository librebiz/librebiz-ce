/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 21, 2006 6:12:03 PM 
 * 
 */
package com.osserp.gui.client.contacts;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;
import com.osserp.common.web.ViewName;

import com.osserp.core.contacts.Contact;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("contactView")
public interface ContactView extends PersonView {

    /**
     * @param deleteMode
     * @return true if delete mode is enabled
     */
    public boolean isDeleteMode();

    /**
     * Enables/disables delete mode
     * @param deleteMode
     */
    public void setDeleteMode(boolean deleteMode);

    /**
     * Indicates if assignMode is available (e.g. all groups not already assigned)
     * @return assignModeAvailable
     */
    public boolean isAssignModeAvailable();

    /**
     * Indicates that this view is in assign mode
     * @return assignMode
     */
    public boolean isAssignMode();

    /**
     * Enables/disables assign mode
     * @param assignMode
     */
    public void setAssignMode(boolean assignMode);

    /**
     * Assigns current contact to given group
     * @param groupId
     * @throws Exception if something goes wrong, see error message
     */
    public void assignContact(Long groupId) throws Exception;

    /**
     * Gets the current federal state of the current contact
     * @return federalStateId
     */
    public Long getCurrentFederalState();

    /**
     * Indicates if view is in type change mode where user is promted to input company name to change type from private to business
     * @return typeChangeMode
     */
    public boolean isTypeChangeMode();

    /**
     * Enables/disables type change mode
     * @param typeChangeMode
     * @throws PermissionException if user has no permission to do that
     */
    public void setTypeChangeMode(boolean typeChangeMode) throws PermissionException;

    /**
     * Changesthe type of contact from
     * @param form
     * @throws ClientException if company name already exists
     */
    public void changeType(Form form) throws ClientException;

    /**
     * Deletes current contact
     * @throws ClientException if contact is not deletable
     */
    public void delete() throws ClientException;

    /**
     * Contact persons of selected contact
     * @return contact persons
     */
    public List<Contact> getContactPersons();

    /**
     * Indicates presence of a contact person with mobile phone
     * @return true if such contact person exists
     */
    public boolean isContactPersonWithMobilePhoneExisting();

    /**
     * Indicates that current selected contact is private contact <br/>
     * of current logged in employee
     * @return true if assigned as private contact
     */
    public boolean isPrivateContactOfCurrentEmployee();

    /**
     * Enabels/disbles privateContactOfCurrentEmployee
     * @param privateContactOfCurrentEmployee
     */
    public void setPrivateContactOfCurrentEmployee(Boolean privateContactOfCurrentEmployee);

    /**
     * Indicates that current logged in employee has permissions <br/>
     * to assign current selected contact as private contact
     * @return true if so
     */
    public boolean isPrivateContactCreateable();

    /**
     * Enables current selected contact as private contact <br/>
     * of current logged in employee
     */
    public void enablePrivateContact();

    /**
     * Disables current selected contact as private contact <br/>
     * of current logged in employee
     */
    public void disablePrivateContact();

    /**
     * Disables all activated modes and frees related objects
     */
    public void disableAllModes();

    /**
     * Indicates availability of map based address search
     * @return contactAddressMapSupport
     */
    public boolean isContactAddressMapSupport();

    /**
     * Indicates that federal state input is custom
     * @return customFederalState
     */
    public boolean isCustomFederalState();

    /**
     * @return true if view is a CustomerView
     */
    public boolean isCustomerView();

    /**
     * @return true if view is an InstallerView
     */
    public boolean isInstallerView();

    /**
     * @return true if view is a ClientCompanyView
     */
    public boolean isClientView();

    /**
     * @return true if view is an EmployeeView
     */
    public boolean isEmployeeView();

    /**
     * @return true if view is a SupplierView
     */
    public boolean isSupplierView();

    /**
     * @return true if user is current contact
     */
    public boolean isUserCurrentContact();
}
