<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="selectUrl" value="${requestScope.selectUrl}"/>
<c:set var="printEnabled" value="${requestScope.printEnabled}"/>
<c:set var="project" value="${sessionScope.businessCaseView.sales}"/>
<c:set var="billing" value="${sessionScope.salesBillingView}"/>

<table class="table table-striped">
    <c:if test="${!empty billing.order}">
    <tr>
        <td class="boldtext recordHead" colspan="3">
            <fmt:message key="priceByOrder"/>
            <a href="<c:url value="/salesOrder.do?method=forward&exit=billing"/>">
                <o:out value="${billing.order.number}"/><span> / </span><o:date value="${billing.order.created}"/>
            </a>
            (<o:out value="${billing.order.currencyKey}"/>)
        </td>
        <td class="amount boldtext"><o:number value="${billing.order.amounts.amount}" format="currency"/></td>
        <c:choose>
        <c:when test="${billing.order.taxFree}">
        <td class="amount boldtext"><o:number value="0.00" format="currency"/></td>
        <td class="amount boldtext"><o:number value="${billing.order.amounts.amount}" format="currency"/></td>
        </c:when>
        <c:otherwise>
        <td class="amount boldtext"><o:number value="${billing.order.amounts.taxAmount}" format="currency"/></td>
        <td class="amount boldtext"><o:number value="${billing.order.amounts.grossAmount}" format="currency"/></td>
        </c:otherwise>
        </c:choose>
    </tr>
    <tr class="altrow">
        <td class="boldtext" colspan="6" style="padding-top: 0px; padding-bottom: 0px;">&nbsp;</td>
    </tr>
    </c:if>
    <tr>
        <th class="boldtext recordId"><fmt:message key="number"/></th>
        <th class="boldtext created"><fmt:message key="date"/></th>
        <th class="boldtext recordName"><fmt:message key="record"/></th>
        <th class="amount boldtext sum"><fmt:message key="amount"/></th>
        <th class="amount boldtext sum"><fmt:message key="turnoverTax"/></th>
        <th class="amount boldtext sum"><fmt:message key="total"/></th>
    </tr>
    <c:choose>
    <c:when test="${(empty billing.downpayments) and (empty billing.invoices)}">
    <tr class="altrow">
        <td></td>
        <td colspan="5"><fmt:message key="noBillsForThisOrder"/></td>
    </tr>
    </c:when>
    <c:otherwise>
    <c:if test="${!empty billing.invoices}">
    <c:forEach var="invoice" items="${billing.invoices}">
    <%-- credit notes --%>
    <c:forEach var="creditNote" items="${invoice.creditNotes}">
    <tr <c:choose><c:when test="${creditNote.canceled}">class="altrow canceled"</c:when><c:otherwise>class="altrow"</c:otherwise></c:choose>>
        <td class="recordId">
            <c:if test="${creditNote.unchangeable and printEnabled}">
            <v:link url="/records/recordPrint/print?name=salesCreditNote&id=${creditNote.id}" title="documentPrint"><o:img name="printIcon" /></v:link>
            </c:if>
            <a href="<c:url value="/salesCreditNote.do?method=display&id=${creditNote.id}&exit=billing"/>"><o:out value="${creditNote.number}"/></a>
        </td>
        <td class="created"><fmt:formatDate value="${creditNote.created}"/></td>
        <c:choose>
        <c:when test="${!empty creditNote.bookingType}">
        <td class="recordName"><o:out value="${creditNote.bookingType.name}"/></td>
        </c:when>
        <c:otherwise>
        <td class="recordName"><fmt:message key="creditNote"/></td>
        </c:otherwise>
        </c:choose>
        <td class="amount sum"><o:number value="${creditNote.amounts.amount}" format="currency" /></td>
        <c:choose>
        <c:when test="${creditNote.taxFree}">
        <td class="amount sum"><o:number value="0.00" format="currency"/></td>
        <td class="amount sum"><o:number value="${creditNote.amounts.amount}" format="currency" /></td>
        </c:when>
        <c:otherwise>
        <td class="amount sum"><o:number value="${creditNote.amounts.taxAmount}" format="currency"/></td>
        <td class="amount sum"><o:number value="${creditNote.amounts.grossAmount}" format="currency"/></td>
        </c:otherwise>
        </c:choose>
    </tr>
    <c:forEach var="payment" items="${creditNote.payments}">
    <tr>
        <td></td>
        <td class="created"><fmt:formatDate value="${payment.paid}"/></td>
        <td colspan="3"><o:out value="${payment.type.name}"/></td>
        <td class="amount"><o:number value="${payment.amount}" format="currency"/></td>
    </tr>
    </c:forEach>
    </c:forEach>
    <tr <c:choose><c:when test="${invoice.canceled}">class="altrow canceled"</c:when><c:otherwise>class="altrow"</c:otherwise></c:choose>>
        <td class="recordId">
        <c:if test="${invoice.unchangeable and printEnabled}">
            <v:link url="/records/recordPrint/print?name=salesInvoice&id=${invoice.id}" title="documentPrint"><o:img name="printIcon" /></v:link>
        </c:if>
        <c:choose>
        <c:when test="${invoice.historical}">
            <a href="<c:url value="${selectUrl}"><c:param name="id" value="${invoice.id}"/><c:param name="type" value="${invoice.type.id}"/></c:url>" style="color: red;"><o:out value="${invoice.number}"/></a>
        </c:when>
        <c:otherwise>
            <a href="<c:url value="${selectUrl}"><c:param name="id" value="${invoice.id}"/><c:param name="type" value="${invoice.type.id}"/></c:url>"><o:out value="${invoice.number}"/></a>
        </c:otherwise>
        </c:choose>
        </td>
        <td class="created"><fmt:formatDate value="${invoice.created}"/></td>
        <c:choose>
        <c:when test="${invoice.partial}">
        <td class="recordName"><fmt:message key="partialInvoice"/></td>
        </c:when>
        <c:otherwise>
        <td class="recordName"><fmt:message key="invoice"/></td>
        </c:otherwise>
        </c:choose>
        <td class="amount"><o:number value="${invoice.amounts.amount}" format="currency"/></td>
        <c:choose>
        <c:when test="${invoice.taxFree}">
        <td class="amount"><o:number value="0.00" format="currency"/></td>
        <td class="amount"><o:number value="${invoice.amounts.amount}" format="currency" /></td>
        </c:when>
        <c:otherwise>
        <td class="amount"><o:number value="${invoice.amounts.taxAmount}" format="currency"/></td>
        <td class="amount"><o:number value="${invoice.amounts.grossAmount}" format="currency"/></td>
        </c:otherwise>
        </c:choose>
    </tr>
    <%-- payments --%>
    <c:forEach var="payment" items="${invoice.payments}">
    <c:if test="${invoice.id == payment.recordId}">
    <tr>
        <td></td>
        <td class="created"><fmt:formatDate value="${payment.paid}"/></td>
        <td colspan="3"><o:out value="${payment.type.name}"/></td>
        <td class="amount"><o:number value="${payment.amount}" format="currency"/></td>
    </tr>
    <c:if test="${!empty payment.note}">
    <tr>
        <td></td>
        <td class="created"> </td>
        <td colspan="3"><o:out value="${payment.note}"/></td>
        <td class="amount"> </td>
    </tr>
    </c:if>
    </c:if>
    </c:forEach>
    </c:forEach>
    </c:if>
    <c:if test="${!empty billing.downpayments}">
    <c:forEach var="invoice" items="${billing.downpayments}">
    <c:if test="${!invoice.canceled}">
    <tr class="altrow">
        <td class="recordId">
        <c:if test="${invoice.unchangeable and !invoice.historical and printEnabled}">
            <v:link url="/records/recordPrint/print?name=salesDownpayment&id=${invoice.id}" title="documentPrint"><o:img name="printIcon" /></v:link>
        </c:if>
        <o:permission role="project_billing" info="permissionViewProjectBillings">
        <c:choose>
        <c:when test="${invoice.historical}">
            <a href="<c:url value="${selectUrl}"><c:param name="id" value="${invoice.id}"/><c:param name="type" value="${invoice.type.id}"/></c:url>" style="color: red;"><o:out value="${invoice.number}"/></a>
        </c:when>
        <c:otherwise>
            <a href="<c:url value="${selectUrl}"><c:param name="id" value="${invoice.id}"/><c:param name="type" value="${invoice.type.id}"/></c:url>"><o:out value="${invoice.number}"/></a>
        </c:otherwise>
        </c:choose>
        </o:permission>
        <o:forbidden role="project_billing">
        <c:choose>
        <c:when test="${invoice.historical}">
            <span style="color: red;"><o:out value="${invoice.number}"/></span>
        </c:when>
        <c:otherwise>
        <c:choose>
        <c:when test="${invoice.unchangeable and !invoice.historical and printEnabled}">
            <v:link url="/records/recordPrint/print?name=salesDownpayment&id=${invoice.id}" title="documentPrint"><o:out value="${invoice.number}"/></v:link>
        </c:when>
        <c:otherwise>
            <o:out value="${invoice.number}"/>
        </c:otherwise>
        </c:choose>
        </c:otherwise>
        </c:choose>
        </o:forbidden>
        </td>
        <td class="created"><fmt:formatDate value="${invoice.created}"/></td>
        <td class="recordName"><fmt:message key="downpaymentInvoice"/></td>
        <c:choose>
        <c:when test="${!invoice.invoiceBehaviour}">
        <%-- this is the default display mode for downpayments where a prorated amount is displayed --%>
        <td class="amount sum"><o:number value="${invoice.amount}" format="currency"/></td>
        <c:choose>
        <c:when test="${invoice.taxFree}">
        <td class="amount sum"><o:number value="0.00" format="currency"/></td>
        <td class="amount sum"><o:number value="${invoice.amount}" format="currency" /></td>
        </c:when>
        <c:otherwise>
        <td class="amount sum"><o:number value="${invoice.taxAmount}" format="currency"/></td>
        <td class="amount sum"><o:number value="${invoice.grossAmount}" format="currency"/></td>
        </c:otherwise>
        </c:choose>
        </c:when>
        <c:otherwise>
        <td class="amount sum"><o:number value="${invoice.amounts.amount}" format="currency"/></td>
        <c:choose>
        <c:when test="${invoice.taxFree}">
        <td class="amount sum"><o:number value="0.00" format="currency"/></td>
        <td class="amount sum"><o:number value="${invoice.amounts.amount}" format="currency" /></td>
        </c:when>
        <c:otherwise>
        <td class="amount sum"><o:number value="${invoice.amounts.taxAmount}" format="currency"/></td>
        <td class="amount sum"><o:number value="${invoice.amounts.grossAmount}" format="currency"/></td>
        </c:otherwise>
        </c:choose>
        </c:otherwise>
        </c:choose>
    </tr>
    <c:forEach var="payment" items="${invoice.payments}">
    <tr>
        <td class="recordId"></td>
        <td class="created"><fmt:formatDate value="${payment.paid}"/></td>
        <td colspan="3"><o:out value="${payment.type.name}"/></td>
        <td class="amount"><o:number value="${payment.amount}" format="currency"/></td>
    </tr>
    </c:forEach>
    </c:if>
    </c:forEach>
    </c:if>
    </c:otherwise>
    </c:choose>
    <c:if test="${!empty billing.canceledDownpayments}">
    <c:forEach var="invoice" items="${billing.canceledDownpayments}">
    <tr class="altrow canceled">
        <td class="recordId">
        <c:if test="${invoice.unchangeable and !invoice.historical and printEnabled}">
            <v:link url="/records/recordPrint/print?name=salesDownpayment&id=${invoice.id}" title="documentPrint"><o:img name="printIcon" styleClass="icon"/></v:link>
        </c:if>
        <o:permission role="project_billing" info="permissionViewProjectBillings">
        <c:choose>
        <c:when test="${invoice.historical}">
            <a href="<c:url value="${selectUrl}"><c:param name="id" value="${invoice.id}"/><c:param name="type" value="${invoice.type.id}"/></c:url>" style="color: red;"><o:out value="${invoice.number}"/></a>
        </c:when>
        <c:otherwise>
            <a href="<c:url value="${selectUrl}"><c:param name="id" value="${invoice.id}"/><c:param name="type" value="${invoice.type.id}"/></c:url>"><o:out value="${invoice.number}"/></a>
        </c:otherwise>
        </c:choose>
        </o:permission>
        <o:forbidden role="project_billing">
        <c:choose>
        <c:when test="${invoice.historical}">
            <span style="color: red;"><o:out value="${invoice.number}"/></span>
        </c:when>
        <c:otherwise>
        <c:choose>
        <c:when test="${invoice.unchangeable and !invoice.historical and printEnabled}">
            <v:link url="/records/recordPrint/print?name=salesDownpayment&id=${invoice.id}" title="documentPrint"><o:out value="${invoice.number}"/></v:link>
        </c:when>
        <c:otherwise>
            <o:out value="${invoice.number}"/>
        </c:otherwise>
        </c:choose>
        </c:otherwise>
        </c:choose>
        </o:forbidden>
        </td>
        <td class="created"><fmt:formatDate value="${invoice.created}"/></td>
        <td class="recordName"><fmt:message key="downpayment"/></td>
        <c:choose>
        <c:when test="${!invoice.invoiceBehaviour}">
        <%-- this is the default display mode for downpayments where a prorated amount is displayed --%>
        <td class="amount sum"><o:number value="${invoice.amount}" format="currency"/></td>
        <c:choose>
        <c:when test="${invoice.taxFree}">
        <td class="amount sum"><o:number value="0.00" format="currency"/></td>
        <td class="amount sum"><o:number value="${invoice.amount}" format="currency" /></td>
        </c:when>
        <c:otherwise>
        <td class="amount sum"><o:number value="${invoice.taxAmount}" format="currency"/></td>
        <td class="amount sum"><o:number value="${invoice.grossAmount}" format="currency"/></td>
        </c:otherwise>
        </c:choose>
        </c:when>
        <c:otherwise>
        <td class="amount sum"><o:number value="${invoice.amounts.amount}" format="currency"/></td>
        <c:choose>
        <c:when test="${invoice.taxFree}">
        <td class="amount sum"><o:number value="0.00" format="currency"/></td>
        <td class="amount sum"><o:number value="${invoice.amounts.amount}" format="currency" /></td>
        </c:when>
        <c:otherwise>
        <td class="amount sum"><o:number value="${invoice.amounts.taxAmount}" format="currency"/></td>
        <td class="amount sum"><o:number value="${invoice.amounts.grossAmount}" format="currency"/></td>
        </c:otherwise>
        </c:choose>
        </c:otherwise>
        </c:choose>
    </tr>
    <c:forEach var="payment" items="${invoice.payments}">
    <tr>
        <td class="recordId"></td>
        <td class="created"><fmt:formatDate value="${payment.paid}"/></td>
        <td colspan="3"><o:out value="${payment.type.name}"/></td>
        <td class="amount"><o:number value="${payment.amount}" format="currency"/></td>
    </tr>
    </c:forEach>
    </c:forEach>
    </c:if>
    <tr>
        <th colspan="5">
            <fmt:message key="balance"/>
            <c:if test="${!empty billing.order}">  (<o:out value="${billing.order.currencyKey}"/>)</c:if>
        </th>
        <th id="total" class="amount"><o:number value="${billing.accountBalance}" format="currency"/></th>
    </tr>
</table>
<c:if test="${!empty billing.thirdPartyInvoices}">
<table class="table table-striped">
    <c:forEach var="invoice" items="${billing.thirdPartyInvoices}">
    <%-- credit notes --%>
    <c:forEach var="creditNote" items="${invoice.creditNotes}">
    <tr <c:choose><c:when test="${creditNote.canceled}">class="altrow canceled"</c:when><c:otherwise>class="altrow"</c:otherwise></c:choose>>
        <td class="recordId">
            <c:if test="${creditNote.unchangeable and printEnabled}">
            <v:link url="/records/recordPrint/print?name=salesCreditNote&id=${creditNote.id}" title="documentPrint"><o:img name="printIcon" /></v:link>
            </c:if>
            <a href="<c:url value="/salesCreditNote.do?method=display&id=${creditNote.id}&exit=billing"/>"><o:out value="${creditNote.number}"/></a>
        </td>
        <td class="created"><fmt:formatDate value="${creditNote.created}"/></td>
        <td class="recordName"><fmt:message key="creditNote"/></td>
        <td class="amount sum"><o:number value="${creditNote.amounts.amount}" format="currency" /></td>
        <c:choose>
        <c:when test="${creditNote.taxFree}">
        <td class="amount sum"><o:number value="0.00" format="currency"/></td>
        <td class="amount sum"><o:number value="${creditNote.amounts.amount}" format="currency" /></td>
        </c:when>
        <c:otherwise>
        <td class="amount sum"><o:number value="${creditNote.amounts.taxAmount}" format="currency"/></td>
        <td class="amount sum"><o:number value="${creditNote.amounts.grossAmount}" format="currency"/></td>
        </c:otherwise>
        </c:choose>
    </tr>
    <c:forEach var="payment" items="${creditNote.payments}">
    <tr>
        <td></td>
        <td class="created"><fmt:formatDate value="${payment.paid}"/></td>
        <td colspan="3"><o:out value="${payment.type.name}"/></td>
        <td class="amount"><o:number value="${payment.amount}" format="currency"/></td>
    </tr>
    </c:forEach>
    </c:forEach>
    <tr <c:choose><c:when test="${invoice.canceled}">class="altrow canceled"</c:when><c:otherwise>class="altrow"</c:otherwise></c:choose>>
        <td class="recordId">
        <c:if test="${invoice.unchangeable and printEnabled}">
            <v:link url="/records/recordPrint/print?name=salesInvoice&id=${invoice.id}" title="documentPrint"><o:img name="printIcon" /></v:link>
        </c:if>
        <c:choose>
        <c:when test="${invoice.historical}">
            <a href="<c:url value="${selectUrl}"><c:param name="id" value="${invoice.id}"/><c:param name="type" value="${invoice.type.id}"/></c:url>" style="color: red;"><o:out value="${invoice.number}"/></a>
        </c:when>
        <c:otherwise>
            <a href="<c:url value="${selectUrl}"><c:param name="id" value="${invoice.id}"/><c:param name="type" value="${invoice.type.id}"/></c:url>"><o:out value="${invoice.number}"/></a>
        </c:otherwise>
        </c:choose>
        </td>
        <td class="created"><fmt:formatDate value="${invoice.created}"/></td>
        <td class="recordName"><fmt:message key="invoice"/></td>
        <td class="amount sum"><o:number value="${invoice.amounts.amount}" format="currency"/></td>
        <c:choose>
        <c:when test="${invoice.taxFree}">
        <td class="amount sum"><o:number value="0.00" format="currency"/></td>
        <td class="amount sum"><o:number value="${invoice.amounts.amount}" format="currency" /></td>
        </c:when>
        <c:otherwise>
        <td class="amount sum"><o:number value="${invoice.amounts.taxAmount}" format="currency"/></td>
        <td class="amount sum"><o:number value="${invoice.amounts.grossAmount}" format="currency"/></td>
        </c:otherwise>
        </c:choose>
    </tr>
    <%-- payments --%>
    <c:forEach var="payment" items="${invoice.payments}">
    <c:if test="${invoice.id == payment.recordId}">
    <tr>
        <td></td>
        <td><fmt:formatDate value="${payment.paid}"/></td>
        <td colspan="3"><o:out value="${payment.type.name}"/></td>
        <td class="amount"><o:number value="${payment.amount}" format="currency"/></td>
    </tr>
    </c:if>
    </c:forEach>
    </c:forEach>
</table>
</c:if>
