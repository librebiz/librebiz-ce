<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<c:set var="obj" value="${sessionScope.discountAware}"/>
<table class="table" style="width: 350px; background-color: #FFFFFF; text-align:left;">
<c:choose>
<c:when test="${empty obj}">
	<tr>
		<th><fmt:message key="unknown"/></th>
	</tr>
	<tr>
		<td>
			<fmt:message key="noDiscountInfoAvailable"/>
		</td>
	</tr>
</c:when> 
<c:otherwise> 
	<tr>
		<th><o:out value="${obj.createdBy}"/></th>
		<th><oc:employee value="${obj.createdBy}"/></th>
	</tr>
	<tr>
		<td style="border: 0px;"><fmt:message key="entryFrom"/></td>
		<td style="border: 0px;"><o:date value="${obj.created}" addtime="true"/></td>
	</tr>
	<tr>
		<td style="border: 0px;"><fmt:message key="salesDiscount"/></td>
		<td style="border: 0px;" class="small">
			<o:number value="${obj.discount}" format="currency"/>
			<c:choose><c:when test="${sessionScope.discountPercent}"> % </c:when><c:otherwise> &euro;/Eh </c:otherwise></c:choose>
		</td>
	</tr>
	<tr>
		<td style="border: 0px;"><fmt:message key="statement"/></td>
		<td style="border: 0px;"><o:out value="${obj.note}"/></td>
	</tr>
</c:otherwise> 
</c:choose> 
</table>
<c:remove var="discountAware" scope="session"/>
