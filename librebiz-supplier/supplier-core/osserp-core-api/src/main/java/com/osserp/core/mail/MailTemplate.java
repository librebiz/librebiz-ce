/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 22, 2008 6:33:01 PM 
 * 
 */
package com.osserp.core.mail;

import java.util.Map;

import com.osserp.common.Option;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface MailTemplate extends Option {

    String getTemplate();

    String getOriginator();

    void setOriginator(String originator);

    String getSubject();

    void setSubject(String subject);

    String getRecipients();

    void setRecipients(String recipients);

    String getRecipientsCC();

    void setRecipientsCC(String recipientsCC);

    String getRecipientsBCC();

    void setRecipientsBCC(String recipientsBCC);

    boolean isHideRecipientsBCC();

    void setHideRecipientsBCC(boolean hideRecipientsBCC);

    boolean isRecipientsByAction();

    boolean isRecipientsByUser();

    boolean isPersonalizedHeader();

    void setPersonalizedHeader(boolean personalizedHeader);

    boolean isPersonalizedFooter();

    void setPersonalizedFooter(boolean personalizedFooter);

    boolean isAddAttachments();

    void setAddAttachments(boolean addAttachments);

    boolean isAddGroup();

    void setAddGroup(boolean addGroup);

    String getGroupname();

    void setGroupname(String groupname);

    boolean isAddUser();

    void setAddUser(boolean addUser);

    String getUsername();

    void setUsername(String username);

    boolean isFixedAttachment();

    void setFixedAttachment(boolean fixedAttachment);

    boolean isFixedOriginator();

    void setFixedOriginator(boolean fixedOriginator);

    boolean isFixedRecipients();

    void setFixedRecipients(boolean fixedRecipients);

    boolean isFixedRecipientsCC();

    void setFixedRecipientsCC(boolean fixedRecipientsCC);

    boolean isFixedRecipientsBCC();

    void setFixedRecipientsBCC(boolean fixedRecipientsBCC);

    boolean isFixedText();

    void setFixedText(boolean fixedText);

    boolean isSendAsUser();

    void setSendAsUser(boolean sendAsUser);

    boolean isCustomText();

    boolean isHeadless();

    boolean isInternal();

    void setInternal(boolean internal);

    boolean isInternalOnly();

    void setInternalOnly(boolean internalOnly);

    boolean isDisabled();

    Map<String, Object> getParameterMap();

    void update(
            String name,
            String subject,
            boolean sendAsUser,
            String originator,
            boolean fixedOriginator,
            String recipients,
            boolean fixedRecipients,
            String recipientsCC,
            boolean fixedRecipientsCC,
            String recipientsBCC,
            boolean fixedRecipientsBCC,
            boolean hideRecipientsBCC,
            boolean addAttachments,
            boolean fixedAttachment,
            boolean fixedText,
            boolean personalizedHeader,
            boolean personalizedFooter,
            boolean addGroup,
            String groupname,
            boolean addUser,
            String username,
            boolean internal,
            boolean internalOnly);

}
