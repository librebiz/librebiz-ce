/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 01-Sep-2006 09:53:34 
 * 
 */
package com.osserp.gui.web.sales;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.ViewManager;
import com.osserp.gui.client.Actions;
import com.osserp.gui.client.sales.SalesVolumeInvoiceView;
import com.osserp.gui.web.struts.AbstractViewDispatcherAction;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class VolumeInvoiceAction extends AbstractViewDispatcherAction {
    private static Logger log = LoggerFactory.getLogger(VolumeInvoiceAction.class.getName());

    /**
     * Forwards to open volume invoice listing
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward forwardInvoiceArchive(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("forwardInvoiceArchive() request from " + getUser(session).getId());
        }
        SalesVolumeInvoiceView view = getVolumeView(session);
        saveViewData(request.getSession(), "salesInvoiceListingView", view.getInvoiceArchiveListing());
        return mapping.findForward("salesInvoiceListingForward");
    }

    /**
     * Forwards to open volume invoice listing
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward forwardUnreleasedInvoices(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("forwardUnreleasedInvoices() request from " + getUser(session).getId());
        }
        SalesVolumeInvoiceView view = getVolumeView(session);
        saveViewData(request.getSession(), "salesInvoiceListingView", view.getUnreleasedInvoiceListing());
        return mapping.findForward("salesInvoiceListingForward");
    }

    /**
     * Forwards to unreleased volume invoice listing
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward forwardUnreleasedOrders(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("forwardUnreleasedOrders() request from " + getUser(session).getId());
        }
        SalesVolumeInvoiceView view = getVolumeView(session);
        saveViewData(request.getSession(), "salesOrderListingView", view.getUnrelasedOrderListing());
        return mapping.findForward("salesOrderListingForward");
    }

    /**
     * Selects volume invoice config. Creates view if not exists
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward selectConfig(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("selectConfig() request from " + getUser(session).getId());
        }
        Long config = RequestUtil.getId(request);
        SalesVolumeInvoiceView view = fetchVolumeView(session);
        if (view == null) {
            view = (SalesVolumeInvoiceView) createView(request);
        }
        try {
            view.selectConfig(config);
            return mapping.findForward(view.getActionTarget());
        } catch (Exception e) {
            return saveErrorAndReturn(request, e.getMessage());
        }
    }

    /**
     * Starts an asynchronous volume creation job, forwards to 'success'
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward createVolume(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("createVolume() request from " + getUser(session).getId());
        }
        SalesVolumeInvoiceView view = getVolumeView(session);
        try {
            view.createVolume();
            this.saveMessage(request, "volumeInvoiceInitialized");
        } catch (ClientException e) {
            saveError(request, e.getMessage());
        } catch (PermissionException e) {
            saveError(request, e.getMessage());
        }
        return mapping.findForward(Actions.SUCCESS);
    }

    /**
     * Refreshs the view after calling create volume and removes message if volume created
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward listExportable(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("listExportable() request from " + getUser(session).getId());
        }
        SalesVolumeInvoiceView view = getVolumeView(session);
        view.setExportableMode(true);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Backups current volume view and forwards to display related sales.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward loadSales(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("loadSales() request from " + getUser(session).getId());
        }
        Long id = RequestUtil.getId(request);
        SalesVolumeInvoiceView view = getVolumeView(session);
        ViewManager.createViewBackup(session, view);
        StringBuilder url = new StringBuilder();
        url.append("/loadSales.do?id=").append(id).append("&exit=volumeInvoice");
        return new ActionForward(url.toString());
    }

    /**
     * Restores and refreshs the view after return from a view outside
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward restore(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("restore() request from " + getUser(session).getId());
        }
        SalesVolumeInvoiceView view = fetchVolumeView(session);
        if (view != null) {
            ViewManager.restoreViewBackup(session, view.getName());
            return mapping.findForward(view.getActionTarget());
        }
        return redisplay(mapping, form, request, response);
    }

    @Override
    public ActionForward redisplay(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("redisplay() request from " + getUser(session).getId());
        }
        SalesVolumeInvoiceView view = fetchVolumeView(session);
        if (view == null) {
            view = (SalesVolumeInvoiceView) createView(request);
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Refreshs available orders
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    public ActionForward refresh(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("refresh() request from " + getUser(session).getId());
        }
        SalesVolumeInvoiceView view = fetchVolumeView(session);
        if (view == null) {
            view = (SalesVolumeInvoiceView) createView(request);
        } else {
            view.refresh();
        }
        return mapping.findForward(view.getActionTarget());
    }

    @Override
    protected Class<SalesVolumeInvoiceView> getViewClass() {
        return SalesVolumeInvoiceView.class;
    }

    private SalesVolumeInvoiceView fetchVolumeView(HttpSession session) {
        return (SalesVolumeInvoiceView) fetchView(session);
    }

    private SalesVolumeInvoiceView getVolumeView(HttpSession session) {
        SalesVolumeInvoiceView view = fetchVolumeView(session);
        if (view == null) {
            log.warn("getVolumeView() no view bound!");
        }
        return view;
    }
}
