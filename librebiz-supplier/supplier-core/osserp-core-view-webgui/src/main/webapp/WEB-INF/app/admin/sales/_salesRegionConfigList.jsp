<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<style type="text/css">

.id {
	width: 27px;
}

.name {
	width: 200px;
}

.employee {
	width: 230px;
}

.action {
	width: 55px;
	text-align: center;
}

.empty {
	width: 780px;
}
</style>

<div class="subcolumn">
	<div class="table-responsive table-responsive-default">
		<table class="table table-striped">
			<thead>
				<tr>
					<th class="id"><fmt:message key="id"/></th>
					<th class="name"><fmt:message key="name"/></th>
					<th class="employee"><fmt:message key="salesRegionManager"/></th>
					<th class="employee"><fmt:message key="salesBy"/></th>
					<th class="action center" colspan="2"><fmt:message key="action"/></th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty view.list}">
						<tr><td colspan="5" class="error empty"><fmt:message key="noSalesRegionsDefined"/></td></tr>
					</c:when>
					<c:otherwise>
						<c:forEach var="item" items="${view.list}">
							<c:if test="${(view.displayEol and item.endOfLife) or (!view.displayEol and !item.endOfLife)}">
								<tr>
									<td class="id"><v:link url="/admin/sales/salesRegionConfig/select?id=${item.id}"><o:out value="${item.id}"/></v:link></td>
									<td class="name"><v:link url="/admin/sales/salesRegionConfig/select?id=${item.id}"><o:out value="${item.name}"/></v:link></td>
									<td class="employee"><o:out value="${item.salesExecutive.displayName}"/></td>
									<td class="employee"><o:out value="${item.sales.displayName}"/></td>
                                    <td class="icon center">
										<c:choose>
											<c:when test="${item.endOfLife}">
												<v:link url="/admin/sales/salesRegionConfig/toggleEol?id=${item.id}" title="activate"><o:img name="nosmileIcon"/></v:link>
											</c:when>
											<c:otherwise>
												<v:link url="/admin/sales/salesRegionConfig/toggleEol?id=${item.id}" title="deactivate"><o:img name="smileIcon"/></v:link>
											</c:otherwise>
										</c:choose>
                                    </td>
                                    <td class="icon center">
										<a href="javascript:onclick=confirmLink('<fmt:message key="irrevocableAction"/>: <fmt:message key="confirmSalesRegionDelete"/> ID <o:out value="${item.id}"/>', '<v:url value="/admin/sales/salesRegionConfig/delete?id=${item.id}"/>');"><o:img name="deleteIcon"/></a>
									</td>
								</tr>
							</c:if>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	</div>
</div>
