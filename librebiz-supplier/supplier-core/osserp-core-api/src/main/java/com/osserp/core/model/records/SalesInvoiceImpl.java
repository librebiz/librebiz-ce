/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 29-Jan-2007 18:36:47 
 * 
 */
package com.osserp.core.model.records;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.Constants;
import com.osserp.common.Option;
import com.osserp.common.util.NumberUtil;

import com.osserp.core.Item;
import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.customers.Customer;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Cancellation;
import com.osserp.core.finance.CreditNote;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.Payment;
import com.osserp.core.finance.RecordCorrection;
import com.osserp.core.finance.RecordInfo;
import com.osserp.core.finance.RecordType;
import com.osserp.core.products.Product;
import com.osserp.core.sales.SalesCreditNoteType;
import com.osserp.core.sales.SalesInvoice;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 */
public class SalesInvoiceImpl extends AbstractSalesInvoice implements SalesInvoice {
    private static Logger log = LoggerFactory.getLogger(SalesInvoiceImpl.class.getName());

    private Cancellation cancellation = null;
    private List<CreditNote> creditNotes = new ArrayList<>();
    private boolean dueCalculated = false;
    private boolean printOrderItems = false;
    private boolean downpaymentsAvailable = false;
    private Long thirdPartyOrder = null;
    private BigDecimal partialInvoiceAmount = new BigDecimal(0);
    private BigDecimal partialInvoiceAmountGross = new BigDecimal(0);
    private String deEst35a = null;
    private String deEst35aTax = null;

    /**
     * Default constructor required by Serializable
     */
    protected SalesInvoiceImpl() {
        super();
    }

    /**
     * Creates a new sales invoice by order
     * @param id
     * @param type
     * @param order
     * @param user
     * @param downpaymentsAvailable
     * @param invoiceType
     * @param copyItems
     * @param thirdPartyRecipient
     * @param partialInvoiceAmount
     * @param partialInvoiceAmountGross
     */
    public SalesInvoiceImpl(
            Long id,
            RecordType type,
            Order order,
            Employee user,
            boolean downpaymentsAvailable,
            Long invoiceType,
            boolean copyItems,
            Customer thirdPartyRecipient,
            BigDecimal partialInvoiceAmount,
            BigDecimal partialInvoiceAmountGross) {
        super(id, type, order, user);

        setPartialInvoiceAmount(partialInvoiceAmount);
        setPartialInvoiceAmountGross(partialInvoiceAmountGross);

        if (SalesInvoice.CUSTOM.equals(invoiceType)) {
            setItemsChangeable(true);
            setItemsEditable(true);
            if (copyItems) {
                updateItems(order);
                for (int i = 0, j = getItems().size(); i < j; i++) {
                    Item next = getItems().get(i);
                    next.setIncludePrice(true);
                }
                calculateSummary();
            }
        } else {
            updateItems(order);
            setItemsChangeable(false);
            setItemsEditable(true);
        }
        if (thirdPartyRecipient != null) {
            setContact(thirdPartyRecipient);
            setReference(null);
            thirdPartyOrder = order.getId();
            setPersonId(null);
        }
    }

    /**
     * Creates a new partial sales invoice by order
     * @param id
     * @param type
     * @param order
     * @param user
     * @param percentage
     * @param price
     * @param product
     * @param customName
     * @param note
     * @param printOrderItems
     */
    public SalesInvoiceImpl(
            Long id,
            RecordType type,
            Order order,
            Employee user,
            boolean percentage,
            BigDecimal price,
            Product product,
            String customName,
            String note,
            boolean printOrderItems) {
        super(id, type, order, user);
        this.printOrderItems = printOrderItems;
        addItem(
                order.getCompany(),
                product,
                customName,
                1d,
                product.isReducedTax() ?
                    getAmounts().getReducedTaxRate() :
                        getAmounts().getTaxRate(),
                price,
                new Date(),
                price,
                false,
                false,
                price,
                note,
                true,
                null);
        setItemsChangeable(false);
        setItemsEditable(false);
        setPartial(true);
    }

    /**
     * Creates a new invoice by other invoice
     * @param id
     * @param other
     * @param createdBy
     * @param taxRate
     * @param reducedTaxRate
     * @param itemsChangeable
     * @param copyReferenceId indicates if id of reference should be set as reference of new record
     */
    public SalesInvoiceImpl(
            Long id,
            SalesInvoice other,
            Employee createdBy,
            Double taxRate,
            Double reducedTaxRate,
            boolean itemsChangeable,
            boolean copyReferenceId) {
        super(id, other.getType(), other, createdBy, copyReferenceId, itemsChangeable);
        printOrderItems = other.isPrintOrderItems();
    }

    /**
     * Creates a new empty invoice
     * @param id
     * @param type
     * @param company
     * @param branchId
     * @param customer
     * @param createdBy
     * @param sale
     * @param taxRate
     * @param reducedTaxRate
     * @param itemsChangeable
     */
    public SalesInvoiceImpl(
            Long id,
            RecordType type,
            Long company,
            Long branchId,
            Customer customer,
            Employee createdBy,
            Long sale,
            Double taxRate,
            Double reducedTaxRate,
            boolean itemsChangeable) {
        this(
                id,
                type,
                company,
                branchId,
                null,
                customer,
                createdBy,
                sale,
                taxRate,
                reducedTaxRate,
                itemsChangeable,
                itemsChangeable);
    }

    /**
     * Creates a new and empty order independant sales invoice
     * @param id
     * @param type
     * @param company
     * @param branchId
     * @param reference
     * @param customer
     * @param createdBy
     * @param sale
     * @param taxRate
     * @param reducedTaxRate
     * @param itemsChangeable
     * @param itemsEditable
     */
    protected SalesInvoiceImpl(
            Long id,
            RecordType type,
            Long company,
            Long branchId,
            Long reference,
            Customer customer,
            Employee createdBy,
            Long sale,
            Double taxRate,
            Double reducedTaxRate,
            boolean itemsChangeable,
            boolean itemsEditable) {
        super(
                id,
                company,
                branchId,
                type,//SalesInvoice.RECORD_TYPE, 
                reference,
                customer,
                createdBy,
                sale,
                taxRate,
                reducedTaxRate,
                itemsChangeable,
                itemsEditable);
    }

    public CreditNote addCreditNote(Employee user, Long id, RecordType type, SalesCreditNoteType bookingType, boolean copyItems) {
        SalesCreditNoteImpl obj = new SalesCreditNoteImpl(
                id,
                type,
                bookingType,
                user,
                this,
                copyItems);
        creditNotes.add(obj);
        return obj;
    }

    public void cancel(Employee user, Cancellation cancellation) {
        setCanceled(true);
        setStatus(SalesInvoice.STAT_CANCELED);
        setChanged(cancellation.getCreated());
        setChangedBy(user == null ?
                Constants.SYSTEM_EMPLOYEE : user.getId());
        setCancellation(cancellation);
    }

    public Cancellation getCancellation() {
        return cancellation;
    }

    public void setCancellation(Cancellation cancellation) {
        this.cancellation = cancellation;
    }

    public List<CreditNote> getCreditNotes() {
        return creditNotes;
    }

    public void setCreditNotes(List<CreditNote> creditNotes) {
        this.creditNotes = creditNotes;
    }

    public Long getThirdPartyOrder() {
        return thirdPartyOrder;
    }

    public void setThirdPartyOrder(Long thirdPartyOrder) {
        this.thirdPartyOrder = thirdPartyOrder;
    }

    public boolean isCreditedInFull() {
        if (!getCreditNotes().isEmpty()
                && getDueAmount().doubleValue() <= 0.01) {
            // credit note(s) exist and invoice is thereby balanced
            // or payout required (due < 0).
            return true;
        }
        return false;
    }

    @Override
    public BigDecimal getDueAmount() {
        if (dueCalculated) {
            return dueAmount;
        }
        BigDecimal result = new BigDecimal(0);
        if (isCanceled()) {
            result = result.subtract(getPaidAmount());
            
        } else if (creditNotes.isEmpty()) {
            if (log.isDebugEnabled()) {
                log.debug("getDueAmount() invoice has no credit notes");
            }
            dueCalculated = true;
            result = super.getDueAmount();
        } else {
            if (log.isDebugEnabled()) {
                log.debug("getDueAmount() credit notes found");
            }
            BigDecimal tmp = new BigDecimal(0);
            for (int i = 0, j = creditNotes.size(); i < j; i++) {
                CreditNote note = creditNotes.get(i);
                if (!note.isCanceled()) {
                    tmp = tmp.add(fetchBigDecimal(note.getAmounts().getGrossAmount()));
                    for (int k = 0, l = note.getPayments().size(); k < l; k++) {
                        Payment p = note.getPayments().get(k);
                        tmp = tmp.add(p.getAmount());
                    }
                } else if (log.isDebugEnabled()) {
                    log.debug("getDueAmount() ignoring canceled credit note [id=" + note.getId() + "]");
                }
            }
            if (tmp.doubleValue() > 0) {
                result = NumberUtil.round(
                        super.getDueAmount().subtract(tmp), 2);
            } else {
                result = super.getDueAmount();
            }
        }
        BigDecimal piag = getPartialInvoiceAmountGross();
        if (piag != null) {
            result = result.add(piag);
        }
        dueAmount = result;
        dueCalculated = true;
        return dueAmount;
    }

    public boolean isPrintOrderItems() {
        return printOrderItems;
    }

    protected void setPrintOrderItems(boolean printOrderItems) {
        this.printOrderItems = printOrderItems;
    }

    public boolean isLinkedWithSale() {
        return (getReference() != null);
    }

    protected boolean isDownpaymentsAvailable() {
        return downpaymentsAvailable;
    }

    protected void setDownpaymentsAvailable(boolean downpaymentsAvailable) {
        this.downpaymentsAvailable = downpaymentsAvailable;
    }

    public String getDeEst35a() {
        return deEst35a;
    }

    public void setDeEst35a(String deEst35a) {
        this.deEst35a = deEst35a;
    }

    public String getDeEst35aTax() {
        return deEst35aTax;
    }

    public void setDeEst35aTax(String deEst35aTax) {
        this.deEst35aTax = deEst35aTax;
    }

    public void addItem(
            Long stockId,
            Product product,
            String customName,
            Double quantity,
            BigDecimal price,
            String note,
            boolean includePrice,
            boolean unchecked,
            Long accountingReferenceId,
            Date accountingReferenceDate) {
        Item created = createItem(
                stockId,
                product,
                customName,
                quantity,
                product.isReducedTax() ?
                        getAmounts().getReducedTaxRate() :
                            getAmounts().getTaxRate(),
                price,
                new Date(),
                price,
                false,
                false,
                price,
                note,
                includePrice,
                null);
        created.setUnchecked(unchecked);
        created.updateAccountingReference(accountingReferenceId, accountingReferenceDate);
        getItems().add(created);
        calculateSummary();
    }

    @Override
    protected RecordCorrection createCorrection(
            Employee user,
            String name,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long country,
            String note) {
        return new SalesInvoiceCorrectionImpl(this, user, name, street, streetAddon, zipcode, city, country, note);
    }

    @Override
    protected Item createItem(
            Long stockId,
            Product product,
            String customName,
            Double quantity,
            Double taxRate,
            BigDecimal price,
            Date priceDate,
            BigDecimal partnerPrice,
            boolean partnerPriceEditable,
            boolean partnerPriceOverridden,
            BigDecimal purchasePrice,
            String note,
            boolean includePrice,
            Long externalId) {

        return new SalesInvoiceItemImpl(
                this,
                stockId,
                product,
                customName,
                quantity,
                price,
                priceDate,
                partnerPrice,
                partnerPriceEditable,
                partnerPriceOverridden,
                purchasePrice,
                taxRate,
                note,
                includePrice,
                getItems().size(),
                externalId);
    }

    @Override
    protected RecordInfo createInfo(Employee user, Option info) {
        return new SalesInvoiceInfoImpl(this, info, user);
    }

    @Override
    public boolean isSales() {
        return true;
    }

    public BigDecimal getPartialInvoiceAmount() {
        return partialInvoiceAmount;
    }

    public void setPartialInvoiceAmount(BigDecimal partialInvoiceAmount) {
        this.partialInvoiceAmount = partialInvoiceAmount;
    }

    public BigDecimal getPartialInvoiceAmountGross() {
        return partialInvoiceAmountGross;
    }

    public void setPartialInvoiceAmountGross(BigDecimal partialInvoiceAmountGross) {
        this.partialInvoiceAmountGross = partialInvoiceAmountGross;
    }
    
    public void changeHistoricalStatus(boolean enable) {
        if (enable) {
            setItemsChangeable(true);
            setItemsEditable(true);
            setStatus(Invoice.STAT_HISTORICAL);
        } else {
            setItemsChangeable(true);
            setItemsEditable(true);
            setStatus(Invoice.STAT_CHANGED);
        }
    }

    @Override
    public void changeContact(ClassifiedContact relatedContact, Long contactChangedBy) {
        super.changeContact(relatedContact, contactChangedBy);
        if (getBusinessCaseId() != null) {
            // reset reference if contact will be changed in order context 
            if (getReference() != null) {
                setThirdPartyOrder(getReference());
                setReference(null);
            }
        }
    }

    @Override
    protected void calculateByBaseTaxOnly() {
        super.calculateByBaseTaxOnly();
        subtractPartialAmount();
    }

    @Override
    protected void calculateByMixedTax() {
        super.calculateByMixedTax();
        subtractPartialAmount();
    }

    private void subtractPartialAmount() {
        if (partialInvoiceAmount != null && !partialInvoiceAmount.equals(new BigDecimal(0))) {
            BigDecimal amount = amounts.getAmount().subtract(partialInvoiceAmount);
            BigDecimal grossAmount = amounts.getGrossAmount().subtract(partialInvoiceAmountGross);
            BigDecimal taxAmount = grossAmount.subtract(amount);
            amounts.setAmount(amount);
            amounts.setGrossAmount(grossAmount);
            amounts.setTaxAmount(taxAmount);
        }
    }
}
