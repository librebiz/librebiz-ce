/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 10, 2006 6:19:44 AM 
 * 
 */
package com.osserp.common.web;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CoreFilter extends AbstractHttpFilter {
    private static Logger logger = LoggerFactory.getLogger(CoreFilter.class.getName());

    @Override
    protected void filterHttpRequest(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain chain) throws IOException, ServletException {
        String uri = request.getRequestURI();
        boolean actionRequest = (uri != null
                && uri.indexOf("png") == -1
                && uri.indexOf("gif") == -1
                && uri.indexOf("bmp") == -1
                && uri.indexOf("jpg") == -1
                && uri.indexOf("css") == -1
                && uri.indexOf("swf") == -1
                && uri.indexOf("svg") == -1
                && uri.indexOf("ttf") == -1
                && uri.indexOf("woff") == -1
                && uri.indexOf("js") == -1);
        long startTime = System.currentTimeMillis();
        if (actionRequest) {
            RequestUtil.log(request);
        }
        chain.doFilter(request, response);
        long totalTime = System.currentTimeMillis() - startTime;
        if (actionRequest) {
            log(request, uri, totalTime);
        }
    }

    protected void log(HttpServletRequest request, String uri, long totalTime) {
        if (logger.isDebugEnabled()) {
            boolean portalAvailable = false;
            HttpSession session = request.getSession(false);
            if (session != null && session.getAttribute("portalView") != null) {
                portalAvailable = true;
            }
            String method = request.getParameter("method");
            logger.debug("REQUEST DONE - [uri=" + uri
                    + (method != null ? ", method=" + method : "")
                    + ", duration=" + totalTime
                    + "ms, serverName=" + request.getServerName()
                    + ", contextPath=" + request.getContextPath()
                    + ", servletPath=" + request.getServletPath()
                    + ", port=" + request.getServerPort()
                    + ", scheme=" + request.getScheme()
                    + ", remote=" + RequestUtil.getRemoteAddress(request)
                    + ", referer=" + request.getHeader("referer")
                    + ", portalView=" + portalAvailable
                    + "]");
        }
    }
}
