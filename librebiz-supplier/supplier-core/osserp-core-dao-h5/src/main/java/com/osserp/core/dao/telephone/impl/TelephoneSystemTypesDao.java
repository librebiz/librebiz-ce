/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.osserp.core.dao.telephone.impl;

import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.dao.Tables;

import com.osserp.core.dao.impl.AbstractDao;
import com.osserp.core.dao.telephone.TelephoneSystemTypes;
import com.osserp.core.model.telephone.TelephoneSystemTypeImpl;
import com.osserp.core.telephone.TelephoneSystemType;

/**
 * 
 * @author so <so@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public class TelephoneSystemTypesDao extends AbstractDao implements TelephoneSystemTypes {

    protected TelephoneSystemTypesDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
    }

    public TelephoneSystemType find(Long id) {
        List<TelephoneSystemType> list = getCurrentSession().createQuery(
                "from " + TelephoneSystemTypeImpl.class.getName() 
                + " st where st.id = :pk").setParameter("pk", id).list();
        if (!list.isEmpty()) {
            return list.get(0);
        }
        return null;
    }

    public List<TelephoneSystemType> getAll() {
        return getCurrentSession().createQuery("from "
                + TelephoneSystemTypeImpl.class.getName() + " st order by st.name").list();
    }
}
