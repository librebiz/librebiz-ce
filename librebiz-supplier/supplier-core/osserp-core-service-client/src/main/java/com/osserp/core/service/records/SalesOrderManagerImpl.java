/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 01-Feb-2007 14:49:20 
 * 
 */
package com.osserp.core.service.records;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;

import com.osserp.core.Item;
import com.osserp.core.calc.Calculation;
import com.osserp.core.customers.Customer;
import com.osserp.core.dao.ProductPrices;
import com.osserp.core.dao.Products;
import com.osserp.core.dao.Projects;
import com.osserp.core.dao.Requests;
import com.osserp.core.dao.SystemConfigs;
import com.osserp.core.dao.Users;
import com.osserp.core.dao.records.RecordBackups;
import com.osserp.core.dao.records.RecordVersionsArchive;
import com.osserp.core.dao.records.SalesDeliveryNotes;
import com.osserp.core.dao.records.SalesInvoices;
import com.osserp.core.dao.records.SalesOrderDownpayments;
import com.osserp.core.dao.records.SalesOrders;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.DeliveryDateChangedInfo;
import com.osserp.core.finance.DeliveryNote;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.ItemChangedInfo;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.OrderItem;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.finance.RecordPaymentAgreement;
import com.osserp.core.products.Product;
import com.osserp.core.requests.Request;
import com.osserp.core.sales.Sales;
import com.osserp.core.sales.SalesInvoice;
import com.osserp.core.sales.SalesOrder;
import com.osserp.core.sales.SalesOrderManager;
import com.osserp.core.sales.SalesRequest;
import com.osserp.core.system.SystemConfigManager;
import com.osserp.core.tasks.ProductPlanningCacheSender;
import com.osserp.core.tasks.SalesMonitoringCacheSender;
import com.osserp.core.tasks.SalesOrderChangedSender;
import com.osserp.core.tasks.SalesOrderReleasedSender;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author cf <cf@osserp.com>
 * 
 */
public class SalesOrderManagerImpl extends AbstractOrderManager implements SalesOrderManager {
    private static Logger log = LoggerFactory.getLogger(SalesOrderManagerImpl.class.getName());
    private ProductPrices productPrices = null;
    private SalesDeliveryNotes deliveryNotes = null;
    private SalesOrderDownpayments salesDownpayments = null;
    private SalesInvoices salesInvoices = null;
    private Projects projects = null;
    private Requests requests = null;
    private SalesOrderChangedSender orderChangedSender = null;
    private SalesOrderReleasedSender orderReleasedSender = null;
    private SalesMonitoringCacheSender salesMonitoringCacheTask = null;
    private SystemConfigs systemConfigs = null;

    public SalesOrderManagerImpl(
            SalesOrders orders,
            SystemConfigManager systemConfigManager,
            ProductPlanningCacheSender productPlanningTask,
            RecordBackups recordBackups,
            RecordVersionsArchive archive,
            Users users,
            SalesDeliveryNotes deliveryNotes,
            Products products,
            ProductPrices productPrices,
            Projects projects,
            Requests requests,
            SystemConfigs systemConfigs,
            SalesOrderDownpayments salesDownpayments,
            SalesInvoices salesInvoices,
            SalesOrderChangedSender orderChangedSender,
            SalesOrderReleasedSender orderReleasedSender,
            SalesMonitoringCacheSender salesMonitoringCacheTask) {

        super(orders, deliveryNotes, systemConfigManager, products,
                productPlanningTask, recordBackups, archive, users);
        this.deliveryNotes = deliveryNotes;
        this.productPrices = productPrices;
        this.projects = projects;
        this.requests = requests;
        this.systemConfigs = systemConfigs;
        this.salesDownpayments = salesDownpayments;
        this.salesInvoices = salesInvoices;
        this.orderChangedSender = orderChangedSender;
        this.orderReleasedSender = orderReleasedSender;
        this.salesMonitoringCacheTask = salesMonitoringCacheTask;
    }

    public List<Order> getByCustomer(Customer customer) {
        if (log.isDebugEnabled()) {
            log.debug("find() invoked [customer="
                    + ((customer == null) ? "null" : customer.getId().toString())
                    + "]");
        }
        List<Order> list = new ArrayList<>();
        if (customer != null) {
            List<SalesOrder> records = getOrdersDao().getByContact(
                    customer.getId());
            for (int i = 0, j = records.size(); i < j; i++) {
                list.add((Order) records.get(i));
            }
        }
        return list;
    }

    public Order getBySales(Sales sales) {
        if (log.isDebugEnabled()) {
            log.debug("find() invoked [sales="
                    + ((sales == null) ? "null" : sales.getId().toString())
                    + "]");
        }
        return getOrdersDao().find(sales);
    }

    public List<Order> findServiceOrders(Sales sales) {
        if (log.isDebugEnabled()) {
            log.debug("findServiceOrders() invoked [sales="
                    + ((sales == null) ? "null" : sales.getId().toString())
                    + "]");
        }
        return getOrdersDao().getServiceOrders(sales);
    }

    public List<RecordDisplay> findByCompany(Long company, Long reference) {
        return getOrdersDao().findByCompanyAndReference(company, reference);
    }

    public List<RecordDisplay> findByCompany(Long company, boolean openOnly) {
        return getOrdersDao().findByCompany(company, openOnly);
    }

    public List<Order> get(List<Long> ids) {
        List<Order> result = new ArrayList<>();
        for (int i = 0, j = ids.size(); i < j; i++) {
            Order order = (Order) find(ids.get(i));
            result.add(order);
        }
        return result;
    }

    public boolean exists(Sales sales) {
        return (getOrdersDao().countBySales(sales.getPrimaryKey()) > 0);
    }

    public Order create(
            Employee user,
            Long company,
            Long branch,
            Customer customer,
            Long bookingType,
            Sales sales) throws ClientException {
        SalesOrders ordersDao = getOrdersDao();

        return ordersDao.create(
                user,
                company,
                branch,
                ordersDao.getBookingType((bookingType == null ? SalesOrder.BOOK_TYPE_SALES : bookingType)),
                customer,
                sales,
                null); // internal orders currently sets sales id as reference and leaves sale blank 
    }

    public Order createByRecord(
            Employee user,
            Sales sales,
            Record other,
            boolean overridePrice) throws ClientException {
        Order salesOrder = getOrdersDao().createByRecord(user, sales, other, overridePrice);
        if (log.isDebugEnabled()) {
            log.debug("createByRecord() done [sales="
                    + ((sales == null) ? "null" : sales.getId().toString())
                    + ", order=" + salesOrder.getId() + "]");
        }
        return salesOrder;
    }

    public Order createMissing(Employee user, Sales sales) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("createMissing() invoked [sales="
                    + ((sales == null) ? "null" : sales.getId().toString())
                    + "]");
        }
        Order order = getOrdersDao().find(sales);
        if (order == null) {
            
            if (!systemConfigs.isSystemPropertyEnabled("orderCreateEmptyOnMissingDefault")) {
                log.warn("createMissing() not supported by config [orderCreateEmptyOnMissingDefault=false]");
                throw new ClientException(ErrorCode.PROJECT_SETUP_INVALID);
            }
            Product defaultProduct = null;
            Long defaultProductId = systemConfigs.getSystemPropertyId("orderCreateEmptyOnMissingProduct");
            if (defaultProductId != null) {
                defaultProduct = getProducts().find(defaultProductId);
            }
            order = getOrdersDao().create(user, sales, defaultProduct);
            if (log.isDebugEnabled()) {
                log.debug("createMissing() done [sales=" + (sales == null ? "null" : sales.getId())
                        + ", order=" + (order == null ? "null" : order.getId()) + "]");
            }
        } else {
            log.warn("createMissing() found existing, parallel request? [sales=" 
                    + (sales == null ? "null" : sales.getId())
                    + ", order=" + order.getId() + "]");
        }
        return order;
    }

    public Order createWarrantyOrder(Employee user, Sales sales, List<Item> itemsToReplace) {
        SalesOrders ordersDao = getOrdersDao();
        Order created = ordersDao.create(
                user,
                sales.getType().getCompany(),
                sales.getRequest().getBranch().getId(),
                ordersDao.getBookingType(SalesOrder.BOOK_TYPE_WARRANTY),
                sales.getRequest().getCustomer(),
                sales,
                null); // warranty orders currently sets sales id as reference and leaves sale blank
        for (int i = 0, j = itemsToReplace.size(); i < j; i++) {
            Item next = itemsToReplace.get(i);
            BigDecimal price = next.getPrice();
            if (price == null || price.doubleValue() == 0) {
                if (sales.getType().isWholeSale() || sales.getCustomer().isReseller()) {
                    price = new BigDecimal(next.getProduct().getResellerPrice());
                } else {
                    price = new BigDecimal(next.getProduct().getConsumerPrice());
                }
            }
            created.addItem(
                    next.getStockId(),
                    next.getProduct(),
                    next.getCustomName(),
                    next.getQuantity() * (-1),
                    next.getTaxRate(),
                    price,
                    created.getCreated(),
                    new BigDecimal(productPrices.getPartnerPrice(next.getProduct(), created.getCreated(), next.getQuantity())),
                    false,
                    false,
                    new BigDecimal(productPrices.getPurchasePrice(next.getProduct(), next.getProduct().getDefaultStock(), created.getCreated())),
                    next.getNote(),
                    true,
                    next.getExternalId());
        }
        persist(created);
        return created;
    }

    public Order createNewVersion(Employee user, Sales sales, boolean backupItems, String backupContext) {
        SalesOrders ordersDao = getOrdersDao();
        SalesOrder order = (SalesOrder) ordersDao.load(sales);
        if (log.isDebugEnabled()) {
            log.debug("createNewVersion() invoked [order=" + order.getId()
                    + ", deliveryDate=" + order.getDelivery() + "]");
        }
        createNewVersion(user, order, backupItems, backupContext);
        order.changeContact(sales.getRequest().getCustomer(), user.getId());
        ordersDao.save(order);
        return ordersDao.load(sales);
    }

    public void updateByCalculation(Employee user, Sales sales, Calculation calculation) {
        if (log.isDebugEnabled()) {
            log.debug("updateByCalculation() invoked [sales="
                    + ((sales.getId() == null) ? "NULL" : sales.getId())
                    + ", calculation="
                    + ((calculation.getId() == null) ? "NULL" : calculation.getId())
                    + "]");
        }
        SalesOrders orders = getOrdersDao();
        SalesOrder order = (SalesOrder) orders.load(sales);
        if (log.isDebugEnabled()) {
            log.debug("updateByCalculation() dumping delivery notes before processing [count=" + order.getDeliveryNotes().size() + "]");
            for (int i = 0, j = order.getDeliveryNotes().size(); i < j; i++) {
                DeliveryNote dn = order.getDeliveryNotes().get(i);
                for (int k = 0, l = dn.getItems().size(); k < l; k++) {
                    Item it = dn.getItems().get(k);
                    log.debug("updateByCalculation() delivery note item [reference=" + it.getReference()
                            + ", id=" + it.getId()
                            + ", product=" + (it.getProduct() == null ? "null" : it.getProduct().getProductId())
                            + ", quantity=" + it.getQuantity()
                            + ", delivered=" + it.getDelivered()
                            + ", price=" + it.getPrice()
                            + (it.getSerials() != null && !it.getSerials().isEmpty() ? (", serials=" + it.getSerialsDisplay() + "]") : "]"));
                }
            }
            log.debug("updateByCalculation() delivery notes before processing dump done");
        }

        List<Item> allItems = new ArrayList<>();
        allItems.addAll(order.getItems());
        List<Item> previousItems = order.getItems();
        orders.clearOrder(order);
        order.updateByCalculation(calculation);
        updateStock(order);
        orders.save(order);
        order.refreshDeliveries();

        if (log.isDebugEnabled()) {
            log.debug("updateByCalculation() dumping delivery notes after processing [count=" + order.getDeliveryNotes().size() + "]");
            for (int i = 0, j = order.getDeliveryNotes().size(); i < j; i++) {
                DeliveryNote dn = order.getDeliveryNotes().get(i);
                for (int k = 0, l = dn.getItems().size(); k < l; k++) {
                    Item it = dn.getItems().get(k);
                    log.debug("updateByCalculation() delivery note item [reference=" + it.getReference()
                            + ", id=" + it.getId()
                            + ", product=" + (it.getProduct() == null ? "null" : it.getProduct().getProductId())
                            + ", quantity=" + it.getQuantity()
                            + ", delivered=" + it.getDelivered()
                            + ", price=" + it.getPrice()
                            + (it.getSerials() != null && !it.getSerials().isEmpty() ? (", serials=" + it.getSerialsDisplay() + "]") : "]"));
                }
            }
            log.debug("updateByCalculation() delivery notes after processing dump done");
        }

        Date nextDeliveryDate = null;
        for (int i = 0, j = order.getItems().size(); i < j; i++) {
            OrderItem next = (OrderItem) order.getItems().get(i);
            OrderItem previous = fetchPrevious(previousItems, next.getProduct().getProductId());
            if (previous != null) {
                if (previous.getDelivery() != null) {
                    next.setDelivery(previous.getDelivery());
                }
                if (previous.getDeliveryNote() != null) {
                    next.setDeliveryNote(previous.getDeliveryNote());
                }
                if (previous.getDeliveryDateBy() != null) {
                    next.setDeliveryDateBy(previous.getDeliveryDateBy());
                }
                if (previous.getFinalDelivery() != null) {
                    next.setFinalDelivery(previous.getFinalDelivery());
                }
                if (nextDeliveryDate == null && next.getDelivery() != null) {
                    nextDeliveryDate = next.getDelivery();
                } else if (nextDeliveryDate != null && next.getDelivery() != null) {
                    if (nextDeliveryDate.after(next.getDelivery())) {
                        nextDeliveryDate = next.getDelivery();
                    }
                }
            } else if (log.isDebugEnabled()) {
                log.debug("updateByCalculation() new item found [id=" + next.getId()
                        + ", product=" + next.getProduct().getProductId()
                        + ", quantity=" + next.getQuantity()
                        + "]");
            }
        }
        order.setDelivery(nextDeliveryDate);
        orders.save(order);
        if (nextDeliveryDate != null && sales.getRequest() != null
                && sales.getRequest() instanceof SalesRequest) {
            SalesRequest request = (SalesRequest) sales.getRequest();
            request.setDeliveryDate(nextDeliveryDate);
            requests.save(request);
        }
        refreshDeliveryBallance(user, order);
        allItems.addAll(order.getItems());
        allItems.addAll(order.getDeliveries());
        createProductPlanningRefreshTask(order, allItems);
        List<DeliveryDateChangedInfo> deliveryDateChangedInfos = orders.createDeliveryChangedHistory(user, order, order.getItems(), previousItems);
        if (orderChangedSender != null && !deliveryDateChangedInfos.isEmpty()) {
            orderChangedSender.sendDeliveryDateChangedInfo(order, deliveryDateChangedInfos);
        }
        List<ItemChangedInfo> orderChangedInfos = orders.createItemChangedHistory(user, order.getItems(), previousItems);
        if (log.isDebugEnabled()) {
            if (orderChangedInfos.isEmpty()) {
                log.debug("updateByCalculation() no items changed by due to previous version");
            } else {
                log.debug("updateByCalculation() items changed by due to previous version [count=" + orderChangedInfos.size() + "]");
            }
        }
        if (orderChangedSender != null && !orderChangedInfos.isEmpty()) {
            orderChangedSender.sendItemChangedInfo(order, orderChangedInfos);
        }
        if (log.isDebugEnabled()) {
            log.debug("updateByCalculation() done [order=" + order.getId()
                    + ", delivery=" + order.getDelivery() + "]");
        }
    }

    private OrderItem fetchPrevious(List<Item> previous, Long productId) {
        for (int i = 0, j = previous.size(); i < j; i++) {
            Item next = previous.get(i);
            if (next.getProduct().getProductId().equals(productId)) {
                return (OrderItem) next;
            }
        }
        return null;
    }

    public void performClosing(Sales sales, boolean closingCanceled) {
        SalesOrder order = (SalesOrder) getBySales(sales);
        if (order != null) {
            performClosing(order, closingCanceled);
        }
    }

    public void close(Employee user, Sales sales) {
        try {
            SalesOrder order = (SalesOrder) getBySales(sales);
            if (sales.isCancelled()) {
                updateStatus(order, user, Record.STAT_CANCELED);
                performClosing(order, true);
                if (log.isDebugEnabled()) {
                    log.debug("close() cancelled [id=" + order.getId() + "]");
                }
            } else if (sales.isClosed()) {
                updateStatus(order, user, Record.STAT_CLOSED);
                performClosing(order, false);
                if (log.isDebugEnabled()) {
                    log.debug("close() done [id=" + order.getId() + "]");
                }
            }
        } catch (Exception e) {
            log.error("close() failed [message=" + e.getMessage() + "]", e);
        }
    }

    public void reopen(Employee user, Sales sales) throws ClientException {
        if (sales.getType().isRecordByCalculation()) {
            throw new ClientException(ErrorCode.ORDER_CALCULATION_ONLY_METHOD);
        }
        SalesOrder order = (SalesOrder) getBySales(sales);
        if (order.isClosed()) {
            throw new ClientException(ErrorCode.INVALID_STATUS_FOUND);
        }
        List<DeliveryNote> dn = deliveryNotes.getByOrder(order);
        if (!dn.isEmpty() && order.isDelivered()) {
            throw new ClientException(ErrorCode.DELIVERIES_FOUND);
        }
        List<SalesInvoice> invoices = salesInvoices.getBySales(sales);
        for (Iterator<SalesInvoice> i = invoices.iterator(); i.hasNext();) {
            SalesInvoice invoice = i.next();
            if (!invoice.isCanceled() && !invoice.isDownpayment()) {
                throw new ClientException(ErrorCode.INVOICES_FOUND_CHANGE_DENIED);
            }
        }
        try {
            Long oldStatus = order.getStatus();
            order.markChanged();
            getDao().save(order);
            getDao().createStatusHistory(order, order.getStatus(), user.getId());
            doAfterStatusUpdate(order, user, oldStatus);
            createProductPlanningRefreshTask(order, null);
            if (log.isDebugEnabled()) {
                log.debug("reopen() re-opened order " + order.getId());
            }
        } catch (Exception e) {
            log.error("reopen() failed [message=" + e.getMessage() + "]", e);
        }
    }

    public void resetDeliveryDate(Sales sales) {
        getOrdersDao().resetDeliveryDate(sales);
    }

    public boolean isBilled(Order order) {
        boolean result = false;
        boolean partialBilling = false;
        BigDecimal billed = new BigDecimal(0);
        Sales sales = (order.getBusinessCaseId() != null &&
                projects.exists(order.getBusinessCaseId()) ?
                        projects.load(order.getBusinessCaseId()) : null);
        List<SalesInvoice> list = salesInvoices.getByContact(order.getContact().getId());
        if (sales != null) {
            list.addAll(salesInvoices.getThirdPartyBySales(sales));
        }
        for (int i = 0, j = list.size(); i < j; i++) {
            SalesInvoice next = (SalesInvoice) list.get(i);
            if (order.getId().equals(next.getReference()) ||
                    (sales != null && sales.getId().equals(next.getBusinessCaseId()))) {

                if (next.isPartial()) {
                    partialBilling = true;
                    if (!next.isCanceled()) {
                        for (Iterator<Item> items = next.getItems().iterator(); items.hasNext();) {
                            Item item = items.next();
                            if (item.getProduct().isAffectsStock()) {
                                return true;
                            }
                        }
                        billed = billed.add(next.getAmounts().getAmount());
                    }
                } else {
                    if (!next.isCanceled()) {
                        if (log.isDebugEnabled()) {
                            log.debug("isBilled() found related not canceled invoice [id=" + next.getId() + "]");
                        }
                        return true;
                    } else if (next.getCancellation() != null && !next.getCancellation().isUnchangeable()) {
                        if (log.isDebugEnabled()) {
                            log.debug("isBilled() found open cancellation [invoice="
                                    + next.getId() + ", cancellation=" + next.getCancellation().getId()
                                    + "]");
                        }
                        return true;
                    }
                }
            }
        }
        if (billed.doubleValue() > 0) {
            double diff = order.getAmounts().getAmount().doubleValue() - billed.doubleValue();
            if (diff <= 0.01 && !partialBilling) {
                return true;
            }
        }
        return result;
    }

    private void refreshDeliveryBallance(Employee user, SalesOrder order) {
        if (log.isDebugEnabled()) {
            log.debug("refreshDeliveryBallance() invoked [order=" + order.getId() + "]");
        }
        Map<Long, Item> ordered = order.getGoods();
        List<Item> delivered = order.getDeliveries();
        //Map<Long, Item> delivered = order.getDeliveredGoods();

        List<Item> canceled = getCanceledDeliveries(ordered, delivered);
        if (!canceled.isEmpty()) {
            DeliveryNote created = deliveryNotes.createRollin(
                    user,
                    order,
                    canceled);
            if (log.isDebugEnabled()) {
                log.debug("refreshDeliveryBallance() created new rollin [id="
                        + ((created == null) ? "null" : created.getId()) + "]");
            }
        } else if (log.isDebugEnabled()) {
            log.debug("refreshDeliveryBallance() nothing to do, no delivered goods canceled");
        }
        for (int i = 0, j = order.getItems().size(); i < j; i++) {
            Item next = order.getItems().get(i);
            Item alreadyDelivered = fetchAlreadyDelivered(delivered, next);
            if (alreadyDelivered != null) {
                if (next.getQuantity() > alreadyDelivered.getQuantity()) {
                    order.resetDelivered();
                    persist(order);
                    break;
                }
            } else {
                order.resetDelivered();
                persist(order);
                break;
            }
        }
    }

    private Item fetchAlreadyDelivered(List<Item> items, Item item) {
        for (int i = 0, j = items.size(); i < j; i++) {
            Item next = items.get(i);
            if (next.getProduct().getProductId().equals(item.getProduct().getProductId())) {
                return next;
            }
        }
        return null;
    }

    private List<Item> getCanceledDeliveries(Map<Long, Item> ordered, Collection<Item> delivered) {
        List<Item> result = new ArrayList<>();
        if (log.isDebugEnabled()) {
            log.debug("getCanceledDeliveries() invoked [orderedCount=" + ordered.size()
                    + ", deliveredCount=" + delivered.size() + "]");
        }
        for (Iterator<Item> i = delivered.iterator(); i.hasNext();) {
            Item out = i.next();
            if (log.isDebugEnabled()) {
                log.debug("getCanceledDeliveries() examining next [id=" + out.getId()
                        + ", product=" + out.getProduct().getProductId()
                        + ", quantity=" + out.getQuantity()
                        + "]");

            }
            if (ordered.containsKey(out.getProduct().getProductId())) {
                if (log.isDebugEnabled()) {
                    log.debug("getCanceledDeliveries() ordered contains product...");
                }
                Item orderedItem = ordered.get(out.getProduct().getProductId());
                if (orderedItem.getQuantity() < out.getQuantity()) {
                    Double diff = out.getQuantity() - orderedItem.getQuantity();
                    if (log.isDebugEnabled()) {
                        log.debug("getCanceledDeliveries() new ordered < delivered [canceled=" + diff + "]");
                    }
                    for (Iterator<Item> j = delivered.iterator(); j.hasNext();) {
                        Item deliveredItem = j.next();
                        if (deliveredItem.getProduct().getProductId().equals(out.getProduct().getProductId())
                                && deliveredItem.getQuantity() < 0) {
                            diff = diff + deliveredItem.getQuantity();
                            if (log.isDebugEnabled()) {
                                log.debug("getCanceledDeliveries() added rollin [quantity=" + deliveredItem.getQuantity()
                                        + ", canceled=" + diff + "]");
                            }
                        }
                    }
                    if (diff > 0) {
                        Item copy = (Item) out.clone();
                        copy.setQuantity(diff);
                        result.add(copy);
                    }
                }
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("getCanceledDeliveries() order no longer contains product...");
                }
                if (out.getQuantity() > 0) {
                    double rolledIn = 0;
                    for (Iterator<Item> j = delivered.iterator(); j.hasNext();) {
                        Item deliveredItem = j.next();
                        if (deliveredItem.getProduct().getProductId().equals(out.getProduct().getProductId())
                                && deliveredItem.getQuantity() < 0) {
                            rolledIn = rolledIn + deliveredItem.getQuantity();
                            if (log.isDebugEnabled()) {
                                log.debug("getCanceledDeliveries() found previous rollin [quantity=" + deliveredItem.getQuantity()
                                        + ", rolledIn=" + rolledIn + "]");
                            }
                        }
                    }
                    double diff = out.getQuantity() + rolledIn;
                    if (diff > 0) {
                        Item requiredRollIn = (Item) out.clone();
                        requiredRollIn.setQuantity(diff);
                        result.add(requiredRollIn);
                        if (log.isDebugEnabled()) {
                            log.debug("getCanceledDeliveries() added required rollin [product=" + out.getProduct().getProductId()
                                    + ", quantity=" + requiredRollIn.getQuantity() + "]");
                        }
                    } else if (log.isDebugEnabled()) {
                        log.debug("getCanceledDeliveries() ignoring already rolled in [product=" + out.getProduct().getProductId() + "]");
                    }
                }
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("getCanceledDeliveries() done [count=" + result.size() + "]");
        }
        return result;
    }

    public RecordPaymentAgreement findPaymentAgreement(Long orderId) {
        return getOrdersDao().findPaymentAgreement(orderId);
    }

    private SalesOrders getOrdersDao() {
        return (SalesOrders) getDao();
    }

    private void performClosing(SalesOrder order, boolean closingCanceled) {
        if (!closingCanceled) {
            for (int i = 0, j = order.getItems().size(); i < j; i++) {
                Item next = order.getItems().get(i);
                if (!next.isDeliveryNoteAffecting()) {
                    //if (!order.isItemDeliveryNoteAffecting(next)) {
                    next.setDelivered(next.getQuantity());
                }
            }
            order.closeDelivery();
            if (log.isDebugEnabled()) {
                log.debug("performClosing() closed deliveries on order " + order.getId());
            }
        } else {
            for (int i = 0, j = order.getItems().size(); i < j; i++) {
                Item next = order.getItems().get(i);
                if (!next.isDeliveryNoteAffecting()) {
                    next.setDelivered(Constants.DOUBLE_NULL);
                }
            }
            order.resetDelivered();
            if (log.isDebugEnabled()) {
                log.debug("performClosing() reset delivery on order " + order.getId());
            }
        }
        persist(order);
    }

    public void update(
            Employee user,
            Order order,
            Date estimatedDelivery,
            String deliveryNote,
            Item item) throws ClientException {
        DeliveryDateChangedInfo changeInfo = getOrdersDao().update(user, order, estimatedDelivery, deliveryNote, item);
        createProductPlanningRefreshTask(order, null);
        updateSalesDeliveryDate(order);
        if (orderChangedSender != null && changeInfo != null) {
            orderChangedSender.sendDeliveryDateChangedInfo(order, changeInfo);
        }
        if (salesMonitoringCacheTask != null) {
            salesMonitoringCacheTask.send(order.getCompany());
        }
    }

    public void update(
            Employee user,
            Order order,
            Date deliveryDate,
            String deliveryNote,
            String itemBehaviour,
            boolean deliveryConfirmed) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("update() invoked, values [user="
                    + user.getId() + ", order="
                    + order.getId() + ", deliveryDate="
                    + deliveryDate + ", deliveryNote="
                    + deliveryNote + ", itemBehaviour="
                    + itemBehaviour + ", deliveryConfirmed="
                    + deliveryConfirmed + "]");
        }
        List<DeliveryDateChangedInfo> changeInfos = getOrdersDao().update(
                user,
                order,
                deliveryDate,
                deliveryNote,
                itemBehaviour,
                deliveryConfirmed);
        createProductPlanningRefreshTask(order, null);
        updateSalesDeliveryDate(order);
        if (orderChangedSender != null && !changeInfos.isEmpty()) {
            orderChangedSender.sendDeliveryDateChangedInfo(order, changeInfos);
        }
        if (salesMonitoringCacheTask != null) {
            salesMonitoringCacheTask.send(order.getCompany());
        }
    }

    public void confirmDiscount(Employee user, Order order, Double discount, String note) {
        if (!(order instanceof SalesOrder)) {
            log.warn("confirmPriceCalculation() expected instanceof SalesOrder, found ["
                    + order.getClass().getName() + "]");
            throw new com.osserp.common.ActionException();
        }
        SalesOrder salesOrder = (SalesOrder) order;
        salesOrder.addDiscount(user, discount, note);
        persist(salesOrder);
        if (log.isDebugEnabled()) {
            log.debug("confirmPriceCalculation() done [order=" + order.getId()
                    + ", employee=" + user.getId()
                    + ", discount=" + discount
                    + ", note=" + note
                    + "]");
        }
    }

    private void updateSalesDeliveryDate(Order order) {
        if (order.getBusinessCaseId() != null) {
            Sales sales = projects.load(order.getBusinessCaseId());
            Request request = sales.getRequest();
            request.setDeliveryDate(order.getDelivery());
            requests.save(request);
            if (log.isDebugEnabled()) {
                log.debug("updateSalesDeliveryDate() done [sales="
                        + order.getBusinessCaseId()
                        + ", delivery=" + order.getDelivery()
                        + "]");
            }
        }
    }

    public void changeDeliveryConfirmation(Employee user, Order order, Long itemId) {
        // Nothing to do for sales orders here
    }

    public void close(Employee user, Order order) {
        try {
            SalesOrder salesOrder = (SalesOrder) order;
            updateStatus(order, user, Record.STAT_CLOSED);
            performClosing(salesOrder, false);
            if (log.isDebugEnabled()) {
                log.debug("close() closed [order=" + order.getId() + "]");
            }
        } catch (Exception e) {
            log.error("close() failed [message=" + e.toString() + "]", e);
        }
    }

    public void changeStock(Employee user, Record record, Long stockId) {
        for (int i = 0, j = record.getItems().size(); i < j; i++) {
            Item next = record.getItems().get(i);
            next.setStockId(stockId);
        }
        persist(record);
        if (log.isDebugEnabled()) {
            log.debug("changeStock() done [record=" + record.getId() + ", stock=" + stockId + "]");
        }
    }

    public void changeStock(Employee user, Record record, Item item, Long stockId) {
        for (int i = 0, j = record.getItems().size(); i < j; i++) {
            Item next = record.getItems().get(i);
            if (next.getId().equals(item.getId())) {
                next.setStockId(stockId);
                persist(record);
                if (log.isDebugEnabled()) {
                    log.debug("changeStock() done [record=" + record.getId()
                            + ", product=" + item.getProduct().getProductId()
                            + ", stock=" + stockId + "]");
                }
                break;
            }
        }
    }

    public void checkDeliveryStatus(Order order, String deliveryType) throws ClientException {
        boolean partialDelivery = false;
        boolean finalDelivery = false;
        if ("partial".equals(deliveryType)) {
            if (log.isDebugEnabled()) {
                log.debug("checkDeliveryStatus() invoked [deliveryType=partial]");
            }
            partialDelivery = true;
        } else if (!"final".equals(deliveryType)) {
            throw new IllegalArgumentException("param must be 'partial' or 'final'");
        } else {
            finalDelivery = true;
            if (log.isDebugEnabled()) {
                log.debug("checkDeliveryStatus() invoked [deliveryType=final]");
            }
        }
        if (!order.isUnchangeable()) {
            if (log.isDebugEnabled()) {
                log.debug("checkDeliveryStatus() failed, order is changeable!");
            }
            throw new ClientException(ErrorCode.RECORD_CHANGEABLE);
        }
        if (log.isDebugEnabled()) {
            log.debug("checkDeliveryStatus() ok, order is unchangeable...");
        }
        if (finalDelivery && order.getOpenDeliveries().isEmpty()) {
            log.debug("checkDeliveryStatus() ok, order has no open deliveries...");
            // nothing more to check
            return;
        }
        if (order.getDeliveryNotes().isEmpty() && order.isNoneKanbanAvailable()) {
            if (log.isDebugEnabled()) {
                log.debug("checkDeliveryStatus() failed, no delivery notes found!");
            }
            throw new ClientException(ErrorCode.DELIVERY_MISSING);
        }
        if (log.isDebugEnabled()) {
            log.debug("checkDeliveryStatus() ok, delivery notes found...");
        }
        if (order.isChangeableDeliveryAvailable()) {
            if (log.isDebugEnabled()) {
                log.debug("checkDeliveryStatus() failed, changeable deliveries found!");
            }
            throw new ClientException(ErrorCode.RECORD_CHANGEABLE);
        }
        if (log.isDebugEnabled()) {
            log.debug("checkDeliveryStatus() ok, no changeable delivery notes found...");
        }
        List<Item> toDeliver = order.getOpenDeliveries();
        if (partialDelivery) {
            if (toDeliver.isEmpty()) {
                if (log.isDebugEnabled()) {
                    log.debug("checkFlowControlDelivery() failed while items to deliver found"
                            + " in partial delivery context");
                }
                throw new ClientException(ErrorCode.DELIVERY_ITEMS_MISSING);
            }
        } else if (!toDeliver.isEmpty()) {
            if (log.isDebugEnabled()) {
                log.debug("checkDeliveryStatus() failed while items to deliver found");
            }
            throw new ClientException(ErrorCode.DELIVERY_ITEMS_FOUND);
        }
    }

    public void checkInvoiceStatus(Order order) throws ClientException {
        boolean anyReleasedInvoiceFound = false;
        List<? extends Invoice> records = salesInvoices.getByOrder(order);
        for (int i = 0, j = records.size(); i < j; i++) {
            Record record = (Record) records.get(i);
            if (!record.isUnchangeable()) {
                if (log.isDebugEnabled()) {
                    log.debug("checkInvoiceStatus() found unreleased record [order=" 
                        + order.getId() + ", invoice=" + record.getId() + "]");
                }
                throw new ClientException(ErrorCode.RECORD_CHANGEABLE);
            }
            if (!record.isCanceled()) {
                anyReleasedInvoiceFound = true;
                
            } else if (log.isDebugEnabled()) {
                log.debug("checkInvoiceStatus() found canceled record [order=" 
                    + order.getId() + ", invoice=" + record.getId() + "]");
            }
        }
        records = salesDownpayments.getByOrder(order);
        for (int i = 0, j = records.size(); i < j; i++) {
            Record record = (Record) records.get(i);
            if (!record.isUnchangeable()) {
                if (log.isDebugEnabled()) {
                    log.debug("checkInvoiceStatus() found unreleased record [order=" 
                        + order.getId() + ", downpayment=" + record.getId() + "]");
                }
                throw new ClientException(ErrorCode.RECORD_CHANGEABLE);
            }
            if (!record.isCanceled()) {
                anyReleasedInvoiceFound = true;
                
            } else if (log.isDebugEnabled()) {
                log.debug("checkInvoiceStatus() found canceled record [order=" 
                    + order.getId() + ", downpayment=" + record.getId() + "]");
            }
        }
        if (!anyReleasedInvoiceFound) {
            if (log.isDebugEnabled()) {
                log.debug("checkInvoiceStatus() did not find any invoice [order=" 
                    + order.getId() + "]");
            }
            throw new ClientException(ErrorCode.INVOICE_MISSING);
        }
    }

    public void sendUpdateEvent(Employee user, Sales sales, String eventName, String message) {
        if (orderChangedSender != null && user != null && sales != null) {
            orderChangedSender.sendSalesUpdateEvent(user, eventName, sales, message);
        }
    }

    @Override
    public void validatePrint(Record record, Long nextStatus) throws ClientException {
        super.validatePrint(record, nextStatus);
        if (isSet(nextStatus) && Record.STAT_SENT >= nextStatus.longValue()
                && isSet(record.getBusinessCaseId()) && getOrdersDao().locked(record.getBusinessCaseId())) {
            throw new ClientException(ErrorCode.CALCULATION_LOCKED);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.service.records.AbstractRecordManager#doAfterStatusUpdate(com.osserp.core.finance.Record, com.osserp.core.employees.Employee,
     * java.lang.Long)
     */
    @Override
    protected void doAfterStatusUpdate(Record record, Employee employee, Long oldStatus) {
        if (Record.STAT_SENT.equals(record.getStatus())
                && !Record.STAT_SENT.equals(oldStatus)
                && record instanceof SalesOrder) {
            if (orderReleasedSender != null) {
                orderReleasedSender.send((Order) record);
            }
        }
        try {
            if (record.getBusinessCaseId() != null && projects.exists(record.getBusinessCaseId())
                    && record.getAmounts() != null 
                    && isSet(record.getAmounts().getAmount())) {
                
                Sales sales = projects.load(record.getBusinessCaseId());
                if (isNotSet(sales.getCapacity())) {
                    sales.setCapacity(record.getAmounts().getAmount().doubleValue());
                    projects.save(sales);
                    // Note: capacity should be already set when passing this method. 
                    // Check if this warning occurs sometime...  
                    log.warn("doAfterStatusUpdate() fixed sales with missing capacity [sales="
                            + record.getBusinessCaseId()
                            + ", capacity=" + sales.getCapacity()
                            + "]");
                }
            }
        } catch (Exception ignorable) {
            log.warn("doAfterStatusUpdate() caught exception on attempt to set capacity [sales="
                            + record.getBusinessCaseId() + ", message=" + ignorable.getMessage()
                            + "]");
        }
    }
}
