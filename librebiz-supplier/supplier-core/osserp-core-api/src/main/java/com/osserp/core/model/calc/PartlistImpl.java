/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 6, 2009 7:13:24 PM 
 * 
 */
package com.osserp.core.model.calc;

import java.math.BigDecimal;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;

import com.osserp.core.Item;
import com.osserp.core.ItemPosition;
import com.osserp.core.calc.Partlist;
import com.osserp.core.calc.PartlistReference;
import com.osserp.core.products.Product;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PartlistImpl extends AbstractItemList implements Partlist {
    private static Logger log = LoggerFactory.getLogger(PartlistImpl.class.getName());
    private Long productId = null;
    private Long configId = null;

    protected PartlistImpl() {
        super();
    }

    public PartlistImpl(
            PartlistReference reference,
            Product referenceProduct,
            Long configId,
            String name,
            Integer number,
            Long createdBy) {
        super(
            reference.getId(), 
            reference.getContextName(), 
            name, 
            number, 
            createdBy, 
            reference.getInitialCreated(), 
            reference.isWholesale());
        
        if (referenceProduct == null) {
            throw new IllegalArgumentException("property 'referenceProduct' must not be null");
        }
        this.productId = referenceProduct.getProductId();
        this.configId = configId;
        if (log.isDebugEnabled()) {
            log.debug("new instance created [reference=" + reference.getId()
                    + ", configId=" + configId
                    + ", product=" + referenceProduct.getProductId()
                    + ", name=" + name
                    + ", createdBy=" + createdBy
                    + "]");
        }
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getConfigId() {
        return configId;
    }

    public void setConfigId(Long configId) {
        this.configId = configId;
    }

    @Override
    protected ItemPosition createPosition(String name, Long groupId, boolean discounts, boolean option, boolean partlist, Long partlistId) {
        return new PartlistPositionImpl(name, getId(), groupId, discounts, option);
    }

    @Override
    protected void refresh() {
        setPrice(new BigDecimal(Constants.DOUBLE_NULL));
        for (int i = 0, j = getPositions().size(); i < j; i++) {
            ItemPosition pos = getPositions().get(i);
            for (int k = 0, l = pos.getItems().size(); k < l; k++) {
                Item next = pos.getItems().get(k);
                if (next.isIncludePrice()) {
                    BigDecimal itemSummary = new BigDecimal(Constants.DOUBLE_NULL);
                    if (next.getPrice() != null && next.getQuantity() != null) {
                        itemSummary = next.getPrice().multiply(new BigDecimal(next.getQuantity()));
                    }
                    setPrice(getPrice().add(itemSummary));
                }
            }
        }
    }

    public void addItem(
            Long positionId,
            Product product,
            String customName,
            Double quantity,
            BigDecimal price,
            Date priceDate,
            boolean priceOverridden,
            BigDecimal partnerPrice,
            boolean partnerPriceEditable,
            boolean partnerPriceOverridden,
            BigDecimal purchasePrice,
            Double taxRate,
            String note,
            boolean includePrice,
            Long externalId)
            throws ClientException {

        if (isNotSet(quantity)) {
            throw new ClientException(ErrorCode.QUANTITY_MISSING);
        }
        if (isAlreadyAdded(product, null)) {
            throw new ClientException(ErrorCode.ITEM_EXISTING);
        }
        PartlistPositionImpl position = (PartlistPositionImpl) getPosition(positionId);
        position.addItem(
                product,
                customName,
                quantity,
                price,
                priceDate,
                priceOverridden,
                partnerPrice,
                partnerPriceEditable,
                partnerPriceOverridden,
                purchasePrice,
                taxRate,
                note,
                (!product.isNullable() ? true : includePrice),
                externalId);
        refresh();
    }

    public void validate() {
        // nothing to do here now
    }

}
