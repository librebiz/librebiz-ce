<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.businessCaseView}">
    <c:redirect url="/errors/error_context.jsp" />
</c:if>
<c:set var="view" value="${sessionScope.businessCaseView}" />
<c:set var="project" value="${sessionScope.businessCaseView.sales}" />
<c:set var="plan" scope="request" value="${project.request}" />
<c:choose>
    <c:when test="${!empty param.lastId}">
        <c:set var="exitUrl" value="/loadSales.do?id=${param.lastId}&exit=${param.lastExit}" />
    </c:when>
    <c:otherwise>
        <c:set var="exitUrl" value="/projects.do?method=exit" />
    </c:otherwise>
</c:choose>
<c:set var="customer" scope="request" value="${plan.customer}" />
<c:set var="customerExitUrl" scope="request" value="${exitUrl}" />
<c:set var="projectContext" value="true" />
<c:set var="contactDisplayUrl" scope="request" value="/sales.do?method=displayCustomer" />
<c:set var="logisticUser" value="${view.user.logisticsOnly}" />

<c:set var="internetConfigPermission" value="false" />
<o:permission role="executive,sales,project_internet_config" info="permissionProjectInternetConfig">
    <c:set var="internetConfigPermission" value="true" />
</o:permission>

<c:set var="installationSupport" value="false" />
<oc:systemPropertyEnabled name="businessCaseInstallationSupport">
    <c:if test="${project.type.installationDateSupported}">
        <c:set var="installationSupport" value="true" />
    </c:if>
</oc:systemPropertyEnabled>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="styles" type="string">
        <style type="text/css">
.red {
	color: #D20707;
}

.statusHeaderMargin {
    margin-left: 8px;
}
.table .action {
    text-align: center;
    width: 100px;
}
.table img {
	padding-left: .25em;
}
.top {
	vertical-align: top !important;
}
</style>
    </tiles:put>

    <tiles:put name="headline">
        <c:choose>
            <c:when test="${project.cancelled}">
                <span class="bolderror">Canceled: <o:out value="${project.type.name}" /> <o:out value="${project.id}" /> / <o:date value="${project.created}" /></span>
            </c:when>
            <c:when test="${project.stopped}">
                <span class="bolderror">Stop =&gt;</span>
                <o:out value="${project.type.name}" />
                <o:out value="${project.id}" /> / <o:date value="${project.created}" />
                <span class="bolderror"> &lt;= Stop</span>
            </c:when>
            <c:otherwise>
                <o:out value="${project.type.name}" />
                <o:out value="${project.id}" /> / <o:date value="${project.created}" />
            </c:otherwise>
        </c:choose>
    </tiles:put>

    <tiles:put name="headline_right">
        <ul>
            <c:choose>
                <c:when test="${view.referenceSetupMode}">
                    <li><a href="<c:url value="/sales.do?method=disableReferenceSetup"/>"><o:img name="backIcon" /></a></li>
                </c:when>
                <c:otherwise>
                    <li><a href="<c:url value="${exitUrl}"/>"><o:img name="backIcon" /></a></li>
                    <c:if test="${!project.cancelled and !logisticUser}">
                        <li><v:link url="/projects/projectContract/forward?exit=/projectDisplay.do" title="furtherSettings">
                                <o:img name="configureIcon" />
                            </v:link></li>
                        <o:permission role="executive,executive_sales,sales_revenue" info="permissionPostCalculation">
                            <li><v:ajaxLink linkId="salesRevenueView" url="/sales/salesRevenue/forward">
                                    <o:img name="moneyIcon" />
                                </v:ajaxLink></li>
                        </o:permission>
                    </c:if>
                    <li><v:ajaxLink url="/events/appointmentCreator/forward?type=5&view=businessCaseView" linkId="appointmentCreator">
                            <o:img name="bellIcon" />
                        </v:ajaxLink></li>
                    <o:permission role="notes_sales_deny" info="permissionNotes">
                        <li><v:link url="/notes/note/forward?id=${project.id}&name=salesNote&exit=/projectDisplay.do" title="displayAndWriteNotes">
                                <o:img name="texteditIcon" />
                            </v:link></li>
                    </o:permission>
                </c:otherwise>
            </c:choose>
            <li><v:link url="/index" title="backToMenu">
                    <o:img name="homeIcon" />
                </v:link></li>
        </ul>
    </tiles:put>

    <tiles:put name="content" type="string">

        <c:import url="/pages/shared/message.jsp" />
        <c:choose>
            <c:when test="${view.referenceSetupMode}">
                <c:import url="project_reference_setup.jsp" />
            </c:when>
            <c:otherwise>
                <div class="subcolumns">
                    <div class="column50l">
                        <div class="subcolumnl">
                            <div class="spacer"></div>
                            <c:import url="/pages/requests/customer_data.jsp" />
                            <div class="spacer"></div>
                            <div class="contentBox">
                                <div class="contentBoxHeader">
                                    <div class="contentBoxHeaderLeft">
                                        <div class="subHeaderLeft"></div>
                                        <div class="subHeaderRight">
                                            <o:out value="${project.type.name}" />
                                            <o:out value="${project.id}" />
                                            <c:if test="${!empty project.request and !project.type.directSales}">
                                                <span> - <fmt:message key="request" />
                                                </span>
                                                <c:if test="${project.id != project.request.requestId}">
                                                    <o:out value="${project.request.requestId}" />
                                                </c:if>
                                                <a href="<c:url value="/requests.do?method=display"/>" title="<fmt:message key="requestData"/>"> <span> <fmt:message key="from" />
                                                </span> <o:date value="${project.request.created}" />
                                                </a>
                                            </c:if>
                                        </div>
                                    </div>
                                    <div class="contentBoxHeaderRight">
                                        <div class="boxnav">
                                            <oc:systemPropertyEnabled name="businessCasePropertySupport">
                                                <o:permission role="customer_service,sales,technics,organisation_admin" info="propertyEdit">
                                                    <ul>
                                                        <li><v:ajaxLink linkId="businessDetails" url="/requests/requestDetails/forward">
                                                                <o:img name="editIcon" />
                                                            </v:ajaxLink></li>
                                                    </ul>
                                                </o:permission>
                                            </oc:systemPropertyEnabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="contentBoxData">
                                    <div class="subcolumns">
                                        <div class="subcolumn" style="overflow-y: auto;">
                                            <div class="spacer"></div>
                                            <table class="valueTable first33">
                                                <oc:systemPropertyEnabled name="businessCaseExternalUrlSupport">
                                                    <c:if test="${!empty project.externalUrl}">
                                                        <tr>
                                                            <td style="vertical-align: top;"><fmt:message key="businessCaseExternalUrl" />
                                                            <td><a href="<o:out value="${project.externalUrl}"/>"><o:out value="${project.externalUrl}" limit="36" /></a></td>
                                                        </tr>
                                                    </c:if>
                                                </oc:systemPropertyEnabled>
                                                <c:import url="/pages/requests/request_accounting_reference.jsp" />
                                                <c:import url="/pages/requests/request_name.jsp" />
                                                <c:import url="/pages/requests/request_address.jsp" />
                                                <c:import url="/pages/requests/request_branch.jsp" />
                                                <c:import url="/pages/requests/request_sales_persons.jsp" />

                                                <c:if test="${!empty project.request.agent}">
                                                    <tr>
                                                        <td class="top"><fmt:message key="agent" /></td>
                                                        <td><o:ajaxLink linkId="agentDisplay" url="/sales.do?method=enableAgentDisplay">
                                                                <o:out value="${project.request.agent.displayName}" />
                                                            </o:ajaxLink></td>
                                                    </tr>
                                                </c:if>

                                                <c:if test="${!empty project.request.tip}">
                                                    <tr>
                                                        <td class="top"><fmt:message key="tipProvider" /></td>
                                                        <td><o:ajaxLink linkId="tipDisplay" url="/sales.do?method=enableTipDisplay">
                                                                <o:out value="${project.request.tip.displayName}" />
                                                            </o:ajaxLink></td>
                                                    </tr>
                                                </c:if>

                                                <c:if test="${!project.type.sales and !empty project.observerId}">
                                                    <tr>
                                                        <td><fmt:message key="projectController" /></td>
                                                        <td><v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${project.observerId}">
                                                                <oc:employee value="${project.observerId}" />
                                                            </v:ajaxLink></td>
                                                    </tr>
                                                </c:if>

                                                <tr>
                                                    <td><o:forbidden role="executive,manager_change,fetch_new_project">
                                                            <fmt:message key="responsible" />
                                                        </o:forbidden> <o:permission role="executive,manager_change,fetch_new_project" info="permissionProjectResponsibleChange">
                                                            <c:choose>
                                                                <c:when test="${project.closed}">
                                                                    <fmt:message key="responsible" />
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <a href="<c:url value="/requests.do?method=enableEmployeeSelection&target=manager"/>"><fmt:message key="responsible" /></a>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </o:permission></td>
                                                    <td><c:choose>
                                                            <c:when test="${!empty project.managerId}">
                                                                <v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${project.managerId}">
                                                                    <oc:employee value="${project.managerId}" />
                                                                </v:ajaxLink>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <fmt:message key="notAssignedLabel" />
                                                            </c:otherwise>
                                                        </c:choose></td>
                                                </tr>

                                                <c:if test="${!project.type.sales and !empty project.managerSubstituteId}">
                                                    <tr>
                                                        <td><fmt:message key="coResponsible" /></td>
                                                        <td><v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${project.managerSubstituteId}">
                                                                <oc:employee value="${project.managerSubstituteId}" />
                                                            </v:ajaxLink></td>
                                                    </tr>
                                                </c:if>

                                                <oc:systemPropertyEnabled name="projectSupplierRelationSupport">
                                                    <tr>
                                                        <td>
                                                            <v:link url="/contacts/contactSearch/forward?selectionExit=/projectDisplay.do&selectionTarget=/projects/projectSupplierConfig/forward&selectionTargetIdParam=supplierId&context=supplier&exit=/projectDisplay.do" title="involvedPartyAddTitle">
                                                                <fmt:message key="involvedPartiesLabel" />
                                                            </v:link>
                                                        </td>
                                                        <td>
                                                            <c:choose>
                                                                <c:when test="${!empty view.suppliers}">
                                                                    <fmt:message key="involvedPartiesHeader" />
                                                                </c:when>
                                                                <c:otherwise><fmt:message key="none" /></c:otherwise>
                                                            </c:choose>
                                                        </td>
                                                    </tr>
                                                    <c:if test="${!empty view.suppliers}">
                                                        <c:forEach var="obj" items="${view.suppliers}" varStatus="s">
                                                            <tr>
                                                                <td>
                                                                    <v:link url="/projects/projectSupplierConfig/forward?id=${obj.id}&exit=/projectDisplay.do">
                                                                        <o:out value="${obj.label}" />
                                                                    </v:link>
                                                                </td>
                                                                <td>
                                                                    <c:choose>
                                                                        <c:when test="${!empty obj.voucherType.numberPrefix}">
                                                                            <o:out value="${obj.voucherType.numberPrefix}"/>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <span title="<fmt:message key="noVoucherAssigned" />">NA</span>
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                    <c:if test="${!empty obj.deliveryDate}">
                                                                        <span> - </span><o:date value="${obj.deliveryDate}"/>
                                                                    </c:if>
                                                                    <span> - </span>
                                                                    <c:choose>
                                                                        <c:when test="${!empty obj.recordId}">
                                                                            <a href="<c:url value="/${obj.voucherType.resourceKey}.do?method=display&id=${obj.recordId}&exit=project"/>">
                                                                                <o:out value="${obj.supplier.displayName}" limit="37" />
                                                                            </a>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <o:out value="${obj.supplier.displayName}" limit="37" />
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </td>
                                                            </tr>
                                                        </c:forEach>
                                                    </c:if>
                                                </oc:systemPropertyEnabled>

                                                <c:if test="${!empty view.stickyNote}">
                                                    <tr>
                                                        <td><fmt:message key="info" /></td>
                                                        <td><o:out value="${view.stickyNote.note}" /></td>
                                                    </tr>
                                                </c:if>
                                                <c:forEach var="prop" items="${project.request.propertyList}" varStatus="s">
                                                    <tr>
                                                        <td><fmt:message key="${prop.name}" /></td>
                                                        <td><o:out value="${prop.value}" /></td>
                                                    </tr>
                                                </c:forEach>
                                                <tr>
                                                    <td style="height: 8px;" colspan="2"></td>
                                                </tr>

                                            </table>
                                            <div class="spacer"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="spacer"></div>
                        </div>
                    </div>
                    <div class="column50r">
                        <div class="subcolumnr">
                            <div class="spacer"></div>
                            <c:import url="/pages/requests/customer_contact_data.jsp" />
                            <div class="spacer"></div>
                            <div class="contentBox">
                                <div class="contentBoxHeader">
                                    <div class="contentBoxHeaderLeft">
                                        <div class="subHeaderLeft"></div>
                                        <div class="subHeaderRight">
                                            <v:ajaxLink linkId="flowControlStatusDisplay" url="/fcs/flowControlStatus/forward?id=${project.id}&selectAction=/sales/salesFcs&exit=/projectDisplay.do">
                                                <span class="statusHeaderMargin"><fmt:message key="status" /></span>
                                            </v:ajaxLink>
                                            <c:set var="projectStatusName">
                                                <oc:options name="salesStatus" value="${project.status}" />
                                            </c:set>
                                            <span <c:if test="${project.cancelled}"> class="red"</c:if>>
                                                <v:link url="/sales/salesFcs/forward?exit=/projectDisplay.do" rawtitle="${projectStatusName}">
                                                    <o:out value="${project.status}" />
                                                </v:link> <fmt:message key="percent" />
                                            </span>
                                            <c:if test="${project.closed and !empty project.closedStatus}">
                                                <span class="statusHeaderMargin" title="${project.closedStatus.description}"><o:out value="${project.closedStatus.name}" /></span>
                                            </c:if>
                                            <c:if test="${!empty view.events and !project.closed and !project.cancelled}">
                                                <span class="statusHeaderMargin">-</span>
                                                <span class="statusHeaderMargin">
                                                    <v:link url="/events/salesEvents/forward?exit=/projectDisplay.do" title="openTodos">
                                                        <fmt:message key="jobs" />
                                                    </v:link>
                                                </span>
                                            </c:if>
                                            <oc:pluginHookAvailable hook="crmEvents">
                                                <oc:pluginHook hook="crmEvents" view="businessCaseView" exit="/projectDisplay.do" />
                                            </oc:pluginHookAvailable>
                                        </div>
                                    </div>
                                </div>
                                <div class="contentBoxData">
                                    <div class="subcolumns">
                                        <div class="subcolumn">
                                            <div class="spacer"></div>
                                            <table class="valueTable first33">

                                                <c:choose>
                                                    <c:when test="${!empty view.sales.parentId or view.serviceSales}">
                                                        <tr>
                                                            <c:choose>
                                                                <c:when test="${!view.parentEditMode}">
                                                                    <td><a href="<c:url value="/sales.do?method=enableParentEdit"/>"><fmt:message key="salesRelationLink" /></a></td>
                                                                    <td>
                                                                        <c:set var="url">
                                                                            <c:choose>
                                                                                <c:when test="${!empty param.lastExit}">
                                                                                    <c:url value="/loadSales.do?id=${view.sales.parentId}&exit=${param.lastExit}" />
                                                                                </c:when>
                                                                                <c:otherwise>
                                                                                    <c:url value="/loadSales.do?id=${view.sales.parentId}&lastId=${project.id}&lastExit=${view.exitTarget}" />
                                                                                </c:otherwise>
                                                                            </c:choose>
                                                                        </c:set>
                                                                        <a href="<o:out value="${url}"/>"><o:out value="${view.sales.parentId}" /></a>
                                                                    </td>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <td><fmt:message key="salesRelationLink" /></td>
                                                                    <td>
                                                                        <o:form name="requestForm" url="/sales.do">
                                                                            <input type="hidden" name="method" value="updateParent" />
                                                                            <input style="width:100px;" type="text" name="parentId" value="<o:out value="${view.sales.parentId}" />" />
                                                                            <a href="<c:url value="/sales.do?method=disableParentEdit"/>"><o:img name="backIcon" styleClass="tab" /></a>
                                                                            <input type="image" src="<o:icon name="saveIcon"/>" class="tab" />
                                                                        </o:form>
                                                                    </td>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </tr>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <c:if test="${!empty view.serviceSalesList}">
                                                            <tr>
                                                                <td><fmt:message key="salesRelationParentLink" /></td>
                                                                <td>
                                                                    <c:forEach var="serviceSales" items="${view.serviceSalesList}" varStatus="s">
                                                                        <c:set var="url">
                                                                            <c:choose>
                                                                                <c:when test="${!empty param.lastExit}">
                                                                                    <c:url value="/loadSales.do?id=${serviceSales.id}&exit=${param.lastExit}" />
                                                                                </c:when>
                                                                                <c:otherwise>
                                                                                    <c:url value="/loadSales.do?id=${serviceSales.id}&lastId=${project.id}&lastExit=${view.exitTarget}" />
                                                                                </c:otherwise>
                                                                            </c:choose>
                                                                        </c:set>
                                                                        <a href="<o:out value="${url}" />"><o:out value="${serviceSales.id}" /></a>
                                                                        <c:if test="${!s.last}">, </c:if>
                                                                    </c:forEach>
                                                                </td>
                                                            </tr>
                                                        </c:if>
                                                    </c:otherwise>
                                                </c:choose>

                                                <%-- PLANT POWER --%>
                                                <c:if test="${project.type.supportingCapacity && project.capacity > 0}">
                                                    <tr>
                                                        <td><fmt:message key="${project.type.capacityLabel}" /></td>
                                                        <td><o:number value="${project.capacity}" format="${project.type.capacityFormat}" /> <o:out value="${project.type.capacityUnit}" /></td>
                                                    </tr>
                                                </c:if>
                                                <tr>
                                                    <td><v:ajaxLink url="/requests/requestOriginSelector/forward?exit=/projectDisplay.do" linkId="requestOriginSelector">
                                                            <fmt:message key="requestPer" />
                                                        </v:ajaxLink></td>
                                                    <td><c:choose>
                                                            <c:when test="${!empty project.request.originType}">
                                                                <oc:options name="requestOriginTypes" value="${project.request.originType}" />
                                                            </c:when>
                                                            <c:otherwise>
                                                                <fmt:message key="unknown" />
                                                            </c:otherwise>
                                                        </c:choose></td>
                                                </tr>
                                                <c:if test="${!empty project.request.origin}">
                                                    <tr>
                                                        <td><o:permission role="executive,executive_branch,sales_change" info="permissionChangeRequestOrigin">
                                                                <v:ajaxLink linkId="campaignSelectionView" url="/crm/campaignSelection/forward?view=businessCaseView&company=${project.request.type.company}&id=${project.request.origin}">
                                                                    <fmt:message key="origin" />
                                                                </v:ajaxLink>
                                                            </o:permission> <o:forbidden role="executive,executive_branch,sales_change">
                                                                <fmt:message key="origin" />
                                                            </o:forbidden></td>
                                                        <td><c:choose>
                                                                <c:when test="${view.originHistoryAvailable}">
                                                                    <v:ajaxLink linkId="originHistoryView" url="/requests/requestOriginHistory/forward" title="changes">
                                                                        <oc:options name="campaigns" value="${project.request.origin}" />
                                                                    </v:ajaxLink>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <oc:options name="campaigns" value="${project.request.origin}" />
                                                                </c:otherwise>
                                                            </c:choose></td>
                                                    </tr>
                                                </c:if>

                                                <%-- INSTALLATION DATE --%>
                                                <c:if test="${installationSupport}">
                                                    <tr>
                                                        <td><c:choose>
                                                                <c:when test="${project.closed}">
                                                                    <fmt:message key="installation" />
                                                                </c:when>
                                                                <c:when test="${logisticUser}">
                                                                    <fmt:message key="installationLabel" />
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <v:ajaxLink url="/projects/installationDate/forward" linkId="installationDate">
                                                                        <fmt:message key="installationLabel" />
                                                                    </v:ajaxLink>
                                                                </c:otherwise>
                                                            </c:choose></td>
                                                        <td><c:choose>
                                                                <c:when test="${empty project.request.installationDate}">
                                                                    <span><fmt:message key="noTargetDate" /></span>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <span title="KW <o:date format="week" value="${project.request.installationDate}"/> / <o:date format="year" value="${project.request.installationDate}"/>"><o:date
                                                                            value="${project.request.installationDate}" /></span>
                                                                </c:otherwise>
                                                            </c:choose></td>
                                                    </tr>
                                                </c:if>

                                                <%-- DELIVERY DATE --%>
                                                <c:if test="${!empty project.request.deliveryDate}">
                                                    <tr>
                                                        <td><c:choose>
                                                                <c:when test="${project.closed or logisticUser}">
                                                                    <fmt:message key="deliveryDate" />
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <a href="<c:url value="/salesOrder.do?method=forward"/>" title="<fmt:message key="processEdit"/>"><fmt:message key="deliveryDate" /></a>
                                                                </c:otherwise>
                                                            </c:choose></td>
                                                        <td><c:choose>
                                                                <c:when test="${empty project.request.deliveryDate}">
                                                                    <fmt:message key="noTargetDate" />
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <span title="KW <o:date format="week" value="${project.request.deliveryDate}"/> / <o:date format="year" value="${project.request.deliveryDate}"/>"> <c:choose>
                                                                            <c:when test="${view.deliveryWarning}">
                                                                                <span class="error top"><o:date value="${project.request.deliveryDate}" /></span>
                                                                                <span class="tab"> <v:ajaxLink url="/sales/deliveryStatusDisplay/forward?id=${project.id}" linkId="deliveryStatus" title="deliveryStatus">
                                                                                        <o:img name="importantIcon" />
                                                                                    </v:ajaxLink>
                                                                                </span>
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                <c:choose>
                                                                                    <c:when test="${project.deliveryClosed}">
                                                                                        <span class="top"><o:date value="${project.request.deliveryDate}" /></span>
                                                                                    </c:when>
                                                                                    <c:otherwise>
                                                                                        <span class="top"><o:date value="${project.request.deliveryDate}" /></span>
                                                                                        <span class="tab"> <v:ajaxLink url="/sales/deliveryStatusDisplay/forward?id=${project.id}" linkId="deliveryStatus" title="deliveryStatus">
                                                                                                <o:img name="importantIcon" />
                                                                                            </v:ajaxLink>
                                                                                        </span>
                                                                                    </c:otherwise>
                                                                                </c:choose>
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </span>
                                                                </c:otherwise>
                                                            </c:choose></td>
                                                    </tr>
                                                </c:if>

                                                <c:if test="${project.type.startupSupported}">
                                                    <tr>
                                                        <td><fmt:message key="startUp" /></td>
                                                        <td><c:choose>
                                                                <c:when test="${empty project.startUp}">
                                                                    <c:choose>
                                                                        <c:when test="${project.status == 100}">
                                                                            <fmt:message key="unknown" />
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <fmt:message key="standsOut" />
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <o:date value="${project.startUp}" />
                                                                </c:otherwise>
                                                            </c:choose></td>
                                                    </tr>
                                                </c:if>


                                                <tr>
                                                    <td><fmt:message key="documents" /></td>
                                                    <td><a href="<c:url value="/salesOrder.do?method=forward"/>"> <%-- order count could be never more than one: --%> <fmt:message key="order" /> <c:if test="${view.recordCount < 1}">[0]</c:if>
                                                    </a></td>
                                                </tr>
                                                <c:if test="${project.type.recordByCalculation and !project.type.directSales and !logisticUser}">
                                                    <tr>
                                                        <td></td>
                                                        <td><a href="<c:url value="/calculations.do?method=forward&exit=project"/>"><fmt:message key="calculation" /></a></td>
                                                    </tr>
                                                </c:if>
                                                <c:if test="${!logisticUser}">
                                                    <tr>
                                                        <td></td>
                                                        <td><a href="<c:url value="/projectBilling.do?method=forward"/>"> <fmt:message key="invoices" /> [<o:out value="${view.invoiceCount}" />]
                                                        </a></td>
                                                    </tr>
                                                </c:if>
                                                <c:if test="${view.contract.trackingRequired}">
                                                    <tr>
                                                        <td></td>
                                                        <td><v:link url="/projects/projectTracking/forward?exit=/projectDisplay.do">
                                                                <fmt:message key="projectTrackingHeader" /> [<o:out value="${view.trackingCount}" />]
                                                                </v:link></td>
                                                    </tr>
                                                </c:if>
                                                <tr>
                                                    <td></td>
                                                    <td><v:link url="/sales/salesDocument/forward?exit=/projectDisplay.do">
                                                            <fmt:message key="documents" /> [<o:out value="${view.salesDocumentCount}" />]
                                                            </v:link></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td><v:link url="/sales/salesPicture/forward?exit=/projectDisplay.do">
                                                            <fmt:message key="pictures" /> [<o:out value="${view.salesPictureCount}" />]
                                                            </v:link></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td><v:link url="/sales/salesLetter/forward?exit=/projectDisplay.do">
                                                            <fmt:message key="letters" /> [<o:out value="${view.letterCount}" />]
                                                            </v:link></td>
                                                </tr>
                                                <c:if test="${view.fetchmailInboxCount > 0}">
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                            <v:link url="/mail/businessCaseInbox/forward?exit=/projectDisplay.do">
                                                                <fmt:message key="inboxLabel" /> [<o:out value="${view.fetchmailInboxCount}" />]
                                                            </v:link>
                                                        </td>
                                                    </tr>
                                                </c:if>
                                            </table>
                                            <div class="spacer"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="spacer"></div>
                        </div>
                    </div>
                </div>
                <c:if test="${!empty view.appointments}">
                    <div class="subcolumns">
                        <div class="subcolumn">
                            <c:import url="project_appointments.jsp" />
                            <div class="spacer"></div>
                        </div>
                    </div>
                </c:if>
            </c:otherwise>
        </c:choose>
    </tiles:put>
</tiles:insert>
