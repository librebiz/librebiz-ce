/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 26, 2007 3:04:53 PM 
 * 
 */
package com.osserp.common.dao;

import org.jdom2.Element;

import com.osserp.common.ClientException;
import com.osserp.common.Option;
import com.osserp.common.OptionsCache;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface JdbcParameter extends Option {

    static final Long CHECKBOX = 1L;
    static final Long SELECT = 2L;
    static final Long TEXT = 3L;

    /**
     * Provides the label
     * @return label
     */
    String getLabel();

    /**
     * Sets the label
     * @param label
     */
    void setLabel(String label);

    /**
     * Provides the type name
     * @return typeName
     */
    String getTypeName();

    /**
     * Sets the type name
     * @param typeName
     */
    void setTypeName(String typeName);

    /**
     * Provides the parameter value
     * @return value
     */
    Object getValue();

    /**
     * Sets the object value by given string depending on parameter type.
     * @param inputValue
     * @throws ClientException if value has an invalid format
     */
    void setParamValue(String inputValue) throws ClientException;

    /**
     * Provides the type as jdbc type
     * @return type
     */
    int getType();

    /**
     * Provides the width of the input field
     * @return width
     */
    Integer getWidth();

    /**
     * Sets the width of the input field
     * @param width
     */
    void setWidth(Integer width);

    /**
     * Indicates that param input is a checkbox
     * @return true if so
     */
    boolean isCheckbox();

    /**
     * Indicates that param input is a select option list
     * @return true bif select
     */
    boolean isSelect();

    /**
     * Provides the name of the cached selection list
     * @return selectNames
     */
    String getSelectNames();

    /**
     * Provides the id of the input type
     * @return inputType
     */
    Option getInputType();

    /**
     * Sets the input type
     * @param type
     */
    void setInputType(Long type, String selectName);

    /**
     * Indicates that param is optional. This is possible for queries executing stored procedures accepting null params
     * @return true if so
     */
    boolean isOptional();

    /**
     * Sets that param is optional and a stored procedure can handle this. Param will we set as null or 0 on query.
     * @param optional
     */
    void setOptional(boolean optional);

    /**
     * Provides a default value for forms
     * @return default value
     */
    String getDefaultValue();

    /**
     * Provides the parameter input config as xml element
     * @param cache
     * @return xml
     */
    Element getXML(OptionsCache cache);
}
