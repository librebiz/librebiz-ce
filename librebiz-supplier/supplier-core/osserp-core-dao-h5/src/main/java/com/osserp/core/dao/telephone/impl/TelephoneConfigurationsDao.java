/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 1, 2010 1:13:00 PM 
 * 
 */
package com.osserp.core.dao.telephone.impl;

import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.Constants;
import com.osserp.common.dao.Tables;

import com.osserp.core.dao.impl.AbstractDao;
import com.osserp.core.dao.telephone.TelephoneConfigurations;
import com.osserp.core.model.telephone.TelephoneConfigurationImpl;
import com.osserp.core.telephone.TelephoneConfiguration;

/**
 * 
 * @author so <so@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public class TelephoneConfigurationsDao extends AbstractDao implements TelephoneConfigurations {

    protected TelephoneConfigurationsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
    }

    public List<TelephoneConfiguration> getAllTelephoneConfigurations() {
        return getCurrentSession().createQuery("from " + TelephoneConfigurationImpl.class.getName()
                + " t where t.defaultConfig = false order by t.uid").list();
    }

    public List<TelephoneConfiguration> getAllTelephoneConfigurationsTemplates() {
        return getCurrentSession().createQuery("from " + TelephoneConfigurationImpl.class.getName()
                + " t where t.defaultConfig = true").list();
    }

    public TelephoneConfiguration find(Long id) {
        List<TelephoneConfiguration> list = getCurrentSession().createQuery(
                "from " + TelephoneConfigurationImpl.class.getName() + " t where t.id = :pk")
                .setParameter("pk", id ).list();
        if (!list.isEmpty()) {
            return list.get(0);
        }
        return null;
    }

    public List<TelephoneConfiguration> findByReference(Long reference) {
        return getCurrentSession().createQuery(
                "from " + TelephoneConfigurationImpl.class.getName() 
                + " t where t.telephone.id = :referenceId order by t.internal")
                .setParameter("referenceId", reference ).list();
    }

    public TelephoneConfiguration findByReference(String internal, Long branchId) {
        List<TelephoneConfiguration> list = getCurrentSession().createQuery(
                "from " + TelephoneConfigurationImpl.class.getName() 
                + " t where t.internal = :intern and t.telephone.telephoneSystem.branchId = :branch")
                .setParameter("intern", internal)
                .setParameter("branch", branchId)
                .list();
        if (!list.isEmpty()) {
            return list.get(0);
        }
        return null;
    }

    public TelephoneConfiguration find(Long employeeId, Long branchId) {
        List<TelephoneConfiguration> list = getCurrentSession().createQuery(
                "from " + TelephoneConfigurationImpl.class.getName() 
                + " t where t.employeeId = :employee and t.telephone.telephoneSystem.branchId = :branch")
                .setParameter("employee", employeeId)
                .setParameter("branch", branchId)
                .list();
        if (!list.isEmpty()) {
            return list.get(0);
        }
        return null;
    }

    public List<TelephoneConfiguration> findByTelephoneGroup(Long telephoneGroupId) {
        return getCurrentSession().createQuery(
                "from " + TelephoneConfigurationImpl.class.getName() 
                + " t where t.telephoneGroup.id = :groupid")
                .setParameter("groupid", telephoneGroupId).list();
    }

    public TelephoneConfiguration findByEmployeeId(Long id) {
        List<TelephoneConfiguration> list = getCurrentSession().createQuery(
                "from " + TelephoneConfigurationImpl.class.getName() 
                + " t where t.employeeId = :employee")
                .setParameter("employee", id).list();
        if (!list.isEmpty()) {
            return list.get(0);
        }
        return null;
    }

    public List<TelephoneConfiguration> findByBranchId(Long branchId) {
        return getCurrentSession().createQuery(
                "from " + TelephoneConfigurationImpl.class.getName() 
                + " t where t.telephone.telephoneSystem.branchId = :branch order by t.internal")
                .setParameter("branch", branchId).list();
    }

    public TelephoneConfiguration getDefaultConfig() {
        List<TelephoneConfiguration> list = getAllTelephoneConfigurationsTemplates();
        if (!list.isEmpty()) {
            return list.get(0);
        }
        return null;
    }

    public void save(TelephoneConfiguration telephoneConfiguration) {
        getCurrentSession().saveOrUpdate(TelephoneConfigurationImpl.class.getName(), telephoneConfiguration);
        if (telephoneConfiguration.getUid() == null) {
            telephoneConfiguration.setUid(getNextUid(telephoneConfiguration.getId()));
            getCurrentSession().saveOrUpdate(TelephoneConfigurationImpl.class.getName(), telephoneConfiguration);
        }
    }

    private String getNextUid(Long id) {
        String prefix = null;
        if (id < 10) {
            prefix = "0000";
        } else if (id < 100) {
            prefix = "000";
        } else if (id < 1000) {
            prefix = "00";
        } else if (id < 10000) {
            prefix = "0";
        }
        return Constants.PHONE_UID_PREFIX + "_" + prefix + id.toString();
    }
}
