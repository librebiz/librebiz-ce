/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 11, 2008 10:51:27 AM 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;
import com.osserp.core.Options;
import com.osserp.core.dao.ProductCategories;
import com.osserp.core.employees.Employee;
import com.osserp.core.model.products.ProductCategoryConfigImpl;
import com.osserp.core.model.products.ProductCategoryImpl;
import com.osserp.core.products.ProductCategoryConfig;
import com.osserp.core.products.ProductClassificationEntity;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProductCategoriesDao extends AbstractProductClassificationEntities implements ProductCategories {

    protected ProductCategoriesDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache optionsCache) {
        super(jdbcTemplate, tables, sessionFactory, optionsCache);
    }

    @Override
    protected Class<ProductCategoryImpl> getImmutableClass() {
        return ProductCategoryImpl.class;
    }

    @Override
    protected Class<ProductCategoryConfigImpl> getMutableClass() {
        return ProductCategoryConfigImpl.class;
    }

    @Override
    protected String getOptionsName() {
        return Options.PRODUCT_CATEGORIES;
    }

    public ProductClassificationEntity create(Employee user, String name, String description) throws ClientException {
        if (exists(name)) {
            throw new ClientException(ErrorCode.NAME_EXISTS);
        }
        ProductCategoryConfigImpl obj = new ProductCategoryConfigImpl(
                name, description, (user == null ? Constants.SYSTEM_EMPLOYEE : user.getId()));
        save(obj);
        return obj;
    }

    public ProductCategoryConfig load(Long id) {
        return (ProductCategoryConfig) getCurrentSession().load(ProductCategoryConfigImpl.class.getName(), id);
    }

    public List<ProductCategoryConfig> getCategories() {
        StringBuilder hql = new StringBuilder(128);
        hql.append("from ").append(ProductCategoryConfigImpl.class.getName()).append(" o order by o.name");
        return getCurrentSession().createQuery(hql.toString()).list();
    }
}
