/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 21, 2011 9:27:31 AM 
 * 
 */
package com.osserp.gui.selections

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.groovy.web.WebUtil
import com.osserp.gui.CoreView

/**
 *
 * @author eh <eh@osserp.com>
 * @author cf <cf@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
class AbstractSelectionView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(AbstractSelectionView.class.getName())

    def targetParams
    String selectionAttribute = 'id'
    Long selectedId
    protected List excludeList = []
    protected List subtractedList = []
    protected boolean checkTargetParams = true
    
    public AbstractSelectionView() {
        super()
        enablePopupView()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        selectionAttribute = form.getString('selectionAttribute') ?: selectionAttribute
        setSelectionTarget(form.getString('selectionTarget') ?: selectionTarget)

        if (!targetParams && checkTargetParams) {
            targetParams = new HashMap(params)
            targetParams.remove('selectionAttribute')
            targetParams.remove('selectionTarget')
            if (selectionTarget && targetParams.exit) {
                String selectionBase = selectionTarget.substring(0, selectionTarget.lastIndexOf('/'))
                String exitBase = targetParams.exit.substring(0, targetParams.exit.lastIndexOf('/'))
                if (selectionBase == exitBase) {
                    targetParams.remove('exit')
                }
            }
        }
        logger.debug("initRequest: done [targetParams=${targetParams}]")
        fetchExcludeList()
    }

    String getUrl() {
        def result = (!WebUtil.isStrutsRequest(selectionTarget) ? context.appPath : '') + selectionTarget
        targetParams.each() { key, value ->
            result += (result?.indexOf('?') > -1 ? '&' : '?') + "${key}=${value}"
        }
        result += (result?.indexOf('?') > -1 ? '&' : '?') + "${selectionAttribute}="
        return result
    }

    List<? extends Object> getList() {
        if (super.getList() && excludeList) {
            def excludeObjects = super.getList().findAll { it.id in excludeList }
            list = super.getList() - excludeObjects
            logger.debug("getList: removed excludes from list [size=${super.getList().size}]")
        }
        return super.getList()
    }

    protected void fetchExcludeList() {
        String excludeViewName = form.getString('view')
        String excludeListName = form.getString('exclude')
        if (excludeViewName) {
            if (!excludeListName) {
                excludeListName = 'list'
            }
            if (excludeListName.contains('.')) {
                List properties = excludeListName.tokenize('.')
                def property = env[excludeViewName]
                properties.each { property = property."${it}" }
                excludeList = property
            } else {
                excludeList = env[excludeViewName]."${excludeListName}"
            }
        }
    }
}
