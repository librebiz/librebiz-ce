<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4><fmt:message key="branchs"/></h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive table-responsive-default">
            <table class="table table-striped">
                <tbody>
                    <c:forEach var="branch" items="${view.branchOfficeList}" varStatus="s">
                        <tr>
                            <td>
                                <v:ajaxLink url="/company/branchPopup/forward?id=${branch.id}" linkId="branchPopupView"><o:out value="${branch.shortkey}"/></v:ajaxLink>
                            </td>
                            <td>
                                <v:link url="/company/branch/forward?id=${branch.id}&exit=/company/clientCompany/reload" title="branchDetails">
                                    <o:out value="${branch.name}"/>
                                </v:link>
                            </td>
                            <td>
                                <c:choose>
                                    <c:when test="${branch.headquarter}"><fmt:message key="headquarterShort"/></c:when>
                                    <c:otherwise><fmt:message key="branchOfficeShortcut"/></c:otherwise>
                                </c:choose>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
             
    </div>
</div>
