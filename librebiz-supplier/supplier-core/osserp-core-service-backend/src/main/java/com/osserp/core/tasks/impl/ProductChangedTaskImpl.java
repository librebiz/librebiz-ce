/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 8, 2012 14:28:39 
 * 
 */
package com.osserp.core.tasks.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.service.impl.AbstractObjectMessageHandler;
import com.osserp.common.util.JsonUtil;

import com.osserp.core.dao.Products;
import com.osserp.core.dao.SyncTasks;
import com.osserp.core.dao.SystemConfigs;
import com.osserp.core.dao.records.SalesOffers;
import com.osserp.core.products.Product;
import com.osserp.core.system.SyncTaskConfig;
import com.osserp.core.tasks.ProductChangedSender;
import com.osserp.core.tasks.Task;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProductChangedTaskImpl extends AbstractObjectMessageHandler implements Task {
    private static Logger log = LoggerFactory.getLogger(ProductChangedTaskImpl.class.getName());
    private static final String API_CLIENT_ENABLED = "syncClientEnabled";
    private static final String API_CLIENT_SYNC_TASK_ID = "syncClientTaskId";
    private Products products;
    private SalesOffers salesOffers;
    private SyncTasks syncTasks;
    private SystemConfigs systemConfigs;

    /**
     * Creates a new productChangedTask
     * @param syncTasks
     * @param systemConfigs
     * @param products
     * @param salesOffers
     */
    protected ProductChangedTaskImpl(
            SyncTasks syncTasks, 
            SystemConfigs systemConfigs, 
            Products products, 
            SalesOffers salesOffers) {
        super();
        this.products = products;
        this.salesOffers = salesOffers;
        this.systemConfigs = systemConfigs;
        this.syncTasks = syncTasks;
    }

    @Override
    protected void execute(Object object, byte[] file) {
        if (object instanceof Map<?, ?>) {
            Map<String, Object> values = (Map<String, Object>) object;
            Long productId = (Long) values.get(ProductChangedSender.PRODUCT_ID);
            String event = (String) values.get(ProductChangedSender.EVENT);
            if (productId != null && event != null) {
                try {
                    Product product = products.load(productId);
                    if (log.isDebugEnabled()) {
                        log.debug("execute() invoked [product=" + product.getProductId() + ", event=" + event + "]");
                    }
                    if ("price".equals(event)) {
                        updatePriceDependencies(product);
                    }
                    executeSyncTasks(event, values, product);
                    
                } catch (Exception e) {
                    log.error("execute() caught exception [class=" + e.getClass().getName() + ", message=" + e.getMessage() + "]", e);
                }
            } else if (ProductChangedSender.EVENT_PRODUCT_CHANGED.equals(event)
                    || ProductChangedSender.EVENT_PRODUCT_MEDIA_CHANGED.equals(event)) {
                // sync all products
                executeSyncTasks(event, values, null);
                
            } else if (event == null) {
                log.warn("execute() message receipt without event. You must add a key '" 
                        + ProductChangedSender.EVENT 
                        + "' providing a known event name");
            } else {
                log.warn("execute() message receipt without product id. You must add a key '" 
                        + ProductChangedSender.PRODUCT_ID 
                        + "' providing an existing product id");
            }
        }
    }
    
    private void executeSyncTasks(String event, Map<String, Object> values, Product product) {
        if (systemConfigs != null && systemConfigs.isSystemPropertyEnabled(API_CLIENT_ENABLED)) {
            
            Long syncTaskId = systemConfigs.getSystemPropertyId(API_CLIENT_SYNC_TASK_ID);
            if (syncTaskId != null) {
                
                if (product != null) {
                    boolean delete = product.isPublicAvailable() ? false : true;
                    createSyncTasks(syncTasks.getConfig(syncTaskId), event, values, product.getProductId(), delete);
                } else {
                    List<Long> tasks = products.getPublicIds();
                    for (int i = 0, j = tasks.size(); i < j; i++) {
                        product = products.load(tasks.get(i));
                        createSyncTasks(
                                syncTasks.getConfig(syncTaskId), 
                                event, 
                                values, 
                                product.getProductId(), 
                                false);
                    }
                }
                
            } else {
                log.warn("execute() client sync enabled but " + API_CLIENT_SYNC_TASK_ID + " not defined. System property check required!");
            }
        }
    }
    
    private void createSyncTasks(SyncTaskConfig config, String event, Map<String, Object> values, Long productId, boolean delete) {
        if (ProductChangedSender.EVENT_PRODUCT_CHANGED.equals(event)) {
            
            // product data changed
            syncTasks.create(config, ProductChangedSender.EVENT_PRODUCT_CHANGED, 
                    (delete ? "delete" : "update"), productId.toString(), null);
            
        } else if (ProductChangedSender.EVENT_PRODUCT_MEDIA_CHANGED.equals(event)) {
            
            Long documentType = (Long) values.get(ProductChangedSender.PRODUCT_DOCUMENT_TYPE);
            Long documentId = (Long) values.get(ProductChangedSender.PRODUCT_DOCUMENT_ID);
            
            if (documentId != null) {
                // change of a known document was recognized
                Map obj = new HashMap<>();
                obj.put("documentId", documentId);
                obj.put("documentType", documentType);
                syncTasks.create(config, ProductChangedSender.EVENT_PRODUCT_MEDIA_CHANGED, 
                        "documentId", productId.toString(), JsonUtil.toJsonString(obj));
            } else if (documentType != null) {
                
                // change was recognized by a change of one or more documents corresponding to a documentType
                // this means that synchronization will be performed for all media of that type. 
                Map obj = new HashMap<>();
                obj.put("documentType", documentType);
                syncTasks.create(config, ProductChangedSender.EVENT_PRODUCT_MEDIA_CHANGED, 
                        "documentType", productId.toString(), JsonUtil.toJsonString(obj));
                
            } else {
                
                    log.warn("createSyncTasks() media event without type. You must add a key '" 
                        + ProductChangedSender.PRODUCT_DOCUMENT_TYPE 
                        + "' providing an existing media type or '"
                        + ProductChangedSender.PRODUCT_DOCUMENT_ID 
                        + "' or both to provide a dedicated document id");
            }
            
        } else if (log.isInfoEnabled()) {
            log.info("createSyncTasks() invoked with unknown event; nothing to do [event=" + event + "]");
        }
    }

    private void updatePriceDependencies(Product product) {
        salesOffers.updateSalesRevenue(product);
    }
}
