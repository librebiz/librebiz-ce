/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 28, 2006 4:06:29 PM 
 * 
 */
package com.osserp.core.purchasing;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;

import com.osserp.core.employees.Employee;
import com.osserp.core.finance.BookingType;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.finance.Stocktaking;
import com.osserp.core.products.Product;
import com.osserp.core.suppliers.Supplier;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface PurchaseInvoiceManager extends PurchaseInvoiceAwareManager {

    /**
     * Looks up for all invoices related to given order
     * @param order
     * @return invoices
     */
    List<Record> getByOrder(Order order);
    
    /**
     * Looks up for a record with the provided supplier reference.
     * @param supplier
     * @param supplierReference suppliers record number
     * @return record or null if no such record exists
     */
    Record getBySupplierReference(Supplier supplier, String supplierReference);

    /**
     * Creates a new purchase invoice based on given order
     * @param user who creates the new record
     * @param order
     * @return new created invoice
     * @throws ClientException if any dependencies not met
     */
    PurchaseInvoice create(Employee user, PurchaseOrder order) throws ClientException;

    /**
     * Creates a new internal inventory booking invoice
     * @param user who creates the new record
     * @param company also used to determine internal supplier id
     * @param branchId
     * @param bookingType
     * @return new created invoice
     * @throws ClientException if inventory booking not configured for company
     */
    PurchaseInvoice create(Employee user, Long company, Long branchId, Long bookingType) throws ClientException;

    /**
     * Creates a new and empty invoice without previous references (order, deliveryNote)
     * @param user who creates the new record
     * @param company also used to determine internal supplier id
     * @param branchId
     * @param bookingType
     * @param supplier
     * @return new created invoice
     */
    PurchaseInvoice create(
            Employee user,
            Long company,
            Long branchId,
            Long bookingType,
            Supplier supplier);

    /**
     * Craetes a new simple billing invoice
     * @param user
     * @param company
     * @param branchId
     * @param bookingType
     * @param supplier
     * @param recordDate
     * @param recordNumber
     * @param invoiceDate
     * @param invoiceNumber
     * @param product
     * @param customName
     * @param productNote
     * @param productQuantity
     * @param productPrice
     * @return new created invoice
     * @throws ClientException if validation of user input failed
     */
    PurchaseInvoice create(
            Employee user,
            Long company, 
            Long branchId, 
            BookingType bookingType, 
            Supplier supplier,
            Date recordDate,
            Long recordNumber,
            Date invoiceDate,
            String invoiceNumber,
            Product product,
            String customName,
            String productNote,
            Double productQuantity,
            BigDecimal productPrice) throws ClientException;
    
    /**
     * Creates a new purchase invoice by existing invoice
     * @param user
     * @param existing
     * @param recordDate
     * @param recordNumber
     * @param invoiceDate
     * @param invoiceNumber
     * @return new created invoice
     * @throws ClientException if validation of user input failed
     */
    PurchaseInvoice create(
        Employee user, 
        PurchaseInvoice existing,
        Date recordDate,
        Long recordNumber,
        Date invoiceDate,
        String invoiceNumber) throws ClientException;

    /**
     * Creates a stocktaking correction record
     * @param user
     * @param st
     * @return new created invoice
     */
    PurchaseInvoice createStocktakingCorrection(Employee user, Stocktaking st);

    /**
     * Creates a new product valuation purchase invoice
     * @param user
     * @param product
     * @param company
     * @param branchId
     * @param valuation
     * @return product with updated valuation
     * @throws ClientException if no product on stock or current stock receipt found
     */
    Product createValuation(Employee user, Product product, Long company, Long branchId, Double valuation) throws ClientException;

    /**
     * Provdides open invoices from date to date
     * @param from
     * @param til
     * @return open records
     */
    List<RecordDisplay> findOpen(Date from, Date til);

    /**
     * Provides all closed invoices
     * @return closed records
     */
    List<RecordDisplay> findClosed(Date from, Date til);

    /**
     * Reopens a closed invoice
     * @param user
     * @param invoice
     */
    void reopenClosed(Employee user, PurchaseInvoice invoice);
}
