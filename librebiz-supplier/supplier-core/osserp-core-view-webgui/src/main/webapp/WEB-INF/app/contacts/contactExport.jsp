<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="contactExportView" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="styles" type="string">
        <style type="text/css">
            .action {
	           text-align: center;
            }
        </style>
    </tiles:put>
    <tiles:put name="headline">
        <fmt:message key="contactExport" /><c:if test="${!empty view.selected}"> - <fmt:message key="${view.selected.name}" /></c:if>
    </tiles:put>

    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="contactExportContent">
            <div class="row">

                <c:choose>
                    <c:when test="${empty view.selected}">
                        <div class="col-md-6 panel-area panel-area-default">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4>
                                        <fmt:message key="groupSelection" />
                                    </h4>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive table-responsive-default">
                                    <table class="table">
                                        <tbody>
                                            <c:forEach var="group" items="${view.selection}">
                                                <tr>
                                                    <td><v:link url="/contacts/contactExport/selectGroup?id=${group.id}">
                                                            <fmt:message key="${group.name}" />
                                                        </v:link></td>
                                                    <td><fmt:message key="${group.description}" /></td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <div class="col-lg-12 panel-area">

                            <div class="table-responsive table-responsive-default">
                                <table class="table table-striped">
                                    <tr>
                                        <th class="name"><fmt:message key="companySlashName" /></th>
                                        <th class="street"><fmt:message key="street" /></th>
                                        <th class="city"><fmt:message key="city" /></th>
                                        <th class="action"></th>
                                    </tr>
                                    <c:forEach var="contact" items="${view.list}" varStatus="s">
                                        <tr>
                                            <td class="name"><a href="<c:url value="/contacts.do?method=load&exit=contactExport&id=${contact.contactId}"/>"> <c:choose>
                                                        <c:when test="${contact.type.privatePerson or contact.type.contactPerson}">
                                                            <c:choose>
                                                                <c:when test="${empty contact.firstName}">
                                                                    <o:out value="${contact.lastName}" />
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <o:out value="${contact.lastName}" />, <o:out value="${contact.firstName}" />
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <o:out value="${contact.lastName}" />
                                                        </c:otherwise>
                                                    </c:choose>
                                            </a></td>
                                            <td class="street"><o:out value="${contact.address.street}" /></td>
                                            <td class="city"><o:out value="${contact.address.zipcode}" /> <o:out value="${contact.address.city}" /></td>
                                            <td class="action"><v:link url="/contacts/contactExport/remove?id=${contact.contactId}">
                                                    <o:img name="deleteIcon" styleClass="bigicon" />
                                                </v:link></td>
                                        </tr>
                                    </c:forEach>
                                </table>
                            </div>
                        </div>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
