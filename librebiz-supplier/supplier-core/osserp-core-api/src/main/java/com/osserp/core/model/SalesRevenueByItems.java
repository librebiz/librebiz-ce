/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 1, 2011 11:11:48 AM 
 * 
 */
package com.osserp.core.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.util.NumberUtil;

import com.osserp.core.BusinessCase;
import com.osserp.core.Item;
import com.osserp.core.calc.Calculation;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Record;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesRevenueByItems extends AbstractSalesRevenue {
    private static Logger log = LoggerFactory.getLogger(SalesRevenueByItems.class.getName());

    private List<Item> hardwareCostsAItems = new ArrayList<>();
    private List<Item> hardwareCostsBItems = new ArrayList<>();
    private List<Item> hardwareCostsCItems = new ArrayList<>();
    private List<Item> otherCostsItems = new ArrayList<>();
    private List<Item> foreignCostsItems = new ArrayList<>();
    private List<Item> developmentCostsItems = new ArrayList<>();
    private List<Item> unknownCostsItems = new ArrayList<>();

    protected SalesRevenueByItems() {
        super();
    }

    /**
     * Creates salesRevenue by items
     * @param businessCase
     * @param salesPerson
     * @param salesRecord
     * @param hardwareCostsADescription
     * @param hardwareCostsBDescription
     * @param hardwareCostsCDescription
     * @param otherCostsDescription
     * @param foreignCostsDescription
     * @param hardwareCostsAItems
     * @param hardwareCostsBItems
     * @param hardwareCostsCItems
     * @param otherCostsItems
     * @param foreignCostsItems
     * @param developmentCostsItems
     * @param unknownCostItemCandidates
     */
    public SalesRevenueByItems(
            BusinessCase businessCase,
            Employee salesPerson,
            Record salesRecord,
            List<Record> salesCreditNotes,
            String hardwareCostsADescription,
            String hardwareCostsBDescription,
            String hardwareCostsCDescription,
            String otherCostsDescription,
            String foreignCostsDescription,
            List<Item> hardwareCostsAItems,
            List<Item> hardwareCostsBItems,
            List<Item> hardwareCostsCItems,
            List<Item> otherCostsItems,
            List<Item> foreignCostsItems,
            List<Item> developmentCostsItems,
            List<Item> unknownCostItemCandidates) {
        super(
                businessCase,
                salesPerson,
                salesRecord.getId(),
                salesRecord.getType().getName(),
                salesRecord.getAmounts().getAmount(),
                businessCase.getType().getMinimalMargin(),
                businessCase.getType().getTargetMargin(),
                salesCreditNotes);
        this.hardwareCostsADescription = hardwareCostsADescription;
        this.hardwareCostsBDescription = hardwareCostsBDescription;
        this.hardwareCostsCDescription = hardwareCostsCDescription;
        this.accessoryCostsDescription = otherCostsDescription;
        this.serviceCostsDescription = foreignCostsDescription;
        this.hardwareCostsAItems = hardwareCostsAItems;
        this.hardwareCostsBItems = hardwareCostsBItems;
        this.hardwareCostsCItems = hardwareCostsCItems;
        this.otherCostsItems = otherCostsItems;
        this.foreignCostsItems = foreignCostsItems;
        this.developmentCostsItems = developmentCostsItems;
        this.unknownCostsItems = createUnknownItems(unknownCostItemCandidates);
        this.calculate();
    }

    /**
     * Creates salesRevenue by items
     * @param businessCase
     * @param salesPerson
     * @param businessCaseId
     * @param hardwareCostsADescription
     * @param hardwareCostsBDescription
     * @param hardwareCostsCDescription
     * @param otherCostsDescription
     * @param foreignCostsDescription
     * @param hardwareCostsAItems
     * @param hardwareCostsBItems
     * @param hardwareCostsCItems
     * @param otherCostsItems
     * @param foreignCostsItems
     * @param developmentCostsItems
     * @param unknownCostItemCandidates
     */
    public SalesRevenueByItems(
            BusinessCase businessCase,
            Employee salesPerson,
            Long businessCaseId,
            String caseName,
            BigDecimal salesPrice,
            Double minimalMargin,
            Double targetMargin,
            List<Record> salesCreditNotes,
            String hardwareCostsADescription,
            String hardwareCostsBDescription,
            String hardwareCostsCDescription,
            String otherCostsDescription,
            String foreignCostsDescription,
            List<Item> hardwareCostsAItems,
            List<Item> hardwareCostsBItems,
            List<Item> hardwareCostsCItems,
            List<Item> otherCostsItems,
            List<Item> foreignCostsItems,
            List<Item> developmentCostsItems,
            List<Item> unknownCostItemCandidates) {
        super(
                businessCase,
                salesPerson,
                businessCaseId,
                caseName,
                salesPrice,
                (minimalMargin != null && minimalMargin.doubleValue() != 0) ?
                        minimalMargin : businessCase.getType().getMinimalMargin(),
                (targetMargin != null && targetMargin.doubleValue() != 0) ?
                        targetMargin : businessCase.getType().getTargetMargin(),
                salesCreditNotes);
        this.hardwareCostsADescription = hardwareCostsADescription;
        this.hardwareCostsBDescription = hardwareCostsBDescription;
        this.hardwareCostsCDescription = hardwareCostsCDescription;
        this.accessoryCostsDescription = otherCostsDescription;
        this.serviceCostsDescription = foreignCostsDescription;
        this.hardwareCostsAItems = hardwareCostsAItems;
        this.hardwareCostsBItems = hardwareCostsBItems;
        this.hardwareCostsCItems = hardwareCostsCItems;
        this.otherCostsItems = otherCostsItems;
        this.foreignCostsItems = foreignCostsItems;
        this.developmentCostsItems = developmentCostsItems;
        this.unknownCostsItems = createUnknownItems(unknownCostItemCandidates);
        this.calculate();
    }

    /**
     * Creates salesRevenue by items
     * @param businessCase
     * @param salesPerson
     * @param calculation
     * @param hardwareCostsADescription
     * @param hardwareCostsBDescription
     * @param hardwareCostsCDescription
     * @param otherCostsDescription
     * @param foreignCostsDescription
     * @param hardwareCostsAItems
     * @param hardwareCostsBItems
     * @param hardwareCostsCItems
     * @param otherCostsItems
     * @param foreignCostsItems
     * @param developmentCostsItems
     * @param unknownCostItemCandidates
     */
    public SalesRevenueByItems(
            BusinessCase businessCase,
            Employee salesPerson,
            Calculation calculation,
            String hardwareCostsADescription,
            String hardwareCostsBDescription,
            String hardwareCostsCDescription,
            String otherCostsDescription,
            String foreignCostsDescription,
            List<Item> hardwareCostsAItems,
            List<Item> hardwareCostsBItems,
            List<Item> hardwareCostsCItems,
            List<Item> otherCostsItems,
            List<Item> foreignCostsItems,
            List<Item> developmentCostsItems,
            List<Item> unknownCostItemCandidates) {
        super(
                businessCase,
                salesPerson,
                calculation.getId(),
                calculation.getName(),
                calculation.getPrice(),
                calculation.getMinimalMargin(),
                calculation.getTargetMargin());
        this.hardwareCostsADescription = hardwareCostsADescription;
        this.hardwareCostsBDescription = hardwareCostsBDescription;
        this.hardwareCostsCDescription = hardwareCostsCDescription;
        this.accessoryCostsDescription = otherCostsDescription;
        this.serviceCostsDescription = foreignCostsDescription;
        this.hardwareCostsAItems = hardwareCostsAItems;
        this.hardwareCostsBItems = hardwareCostsBItems;
        this.hardwareCostsCItems = hardwareCostsCItems;
        this.otherCostsItems = otherCostsItems;
        this.foreignCostsItems = foreignCostsItems;
        this.developmentCostsItems = developmentCostsItems;
        this.unknownCostsItems = createUnknownItems(unknownCostItemCandidates);
        this.calculate();
    }

    private List<Item> createUnknownItems(List<Item> unknownCostItemCandidates) {
        List<Item> result = new ArrayList<>();
        Set<Long> allItems = new HashSet<>();
        addItems(hardwareCostsAItems, allItems);
        addItems(hardwareCostsBItems, allItems);
        addItems(hardwareCostsCItems, allItems);
        addItems(otherCostsItems, allItems);
        addItems(foreignCostsItems, allItems);
        addItems(developmentCostsItems, allItems);
        for (int i = 0, j = unknownCostItemCandidates.size(); i < j; i++) {
            Item item = unknownCostItemCandidates.get(i);
            if (!item.getProduct().isPlant() && !allItems.contains(item.getProduct().getProductId())) {
                result.add(item);
                if (log.isDebugEnabled()) {
                    log.debug("createUnknownItems() found unknown item [id=" + item.getId() +
                            ", product=" + item.getProduct().getProductId() +
                            ", name=" + item.getProduct().getName() + "]");
                }
            }
        }
        return result;
    }

    private void addItems(List<Item> source, Set<Long> target) {
        for (int i = 0, j = source.size(); i < j; i++) {
            Item item = source.get(i);
            target.add(item.getProduct().getProductId());
        }
    }

    @Override
    public String getName() {
        return "salesRevenueByItems";
    }

    @Override
    public boolean isProvidesItems() {
        return true;
    }

    @Override
    protected void calculate() {
        update(
                summarizeCosts(hardwareCostsAItems),
                summarizeCosts(hardwareCostsBItems),
                summarizeCosts(hardwareCostsCItems),
                summarizeCosts(otherCostsItems),
                summarizeCosts(foreignCostsItems),
                summarizeCosts(developmentCostsItems),
                summarizeCosts(unknownCostsItems));

        setCosts(
                getAccessoryCosts() +
                        getHardwareCostsA() +
                        getHardwareCostsB() +
                        getHardwareCostsC() +
                        getServiceCosts());

        setNetCosts(
                getAccessoryCosts() +
                        getAgentCosts() +
                        getHardwareCostsA() +
                        getHardwareCostsB() +
                        getHardwareCostsC() +
                        getServiceCosts() +
                        getProjectCosts() +
                        getUnknownCosts());

        setTotalCosts(
                getAccessoryCosts() +
                        getAgentCosts() +
                        getHardwareCostsA() +
                        getHardwareCostsB() +
                        getHardwareCostsC() +
                        getSalesCosts() +
                        getServiceCosts() +
                        getTipCosts());

        setFinalCosts(
                getAccessoryCosts() +
                        getAgentCosts() +
                        getHardwareCostsA() +
                        getHardwareCostsB() +
                        getHardwareCostsC() +
                        getSalesCosts() +
                        getServiceCosts() +
                        getTipCosts() +
                        getProjectCosts() +
                        getUnknownCosts());

        if (log.isDebugEnabled()) {
            log.debug("calculate() done [mode=byItems, businessCase=" + (getBusinessCase() != null ? getBusinessCase().getPrimaryKey() : "anonymous")
                    + ", calculationName=" + getName()
                    + ", salesVolume=" + getSalesVolume()
                    + ", hardwareCostsA=" + getHardwareCostsA()
                    + ", hardwareCostsADescription=" + getHardwareCostsADescription()
                    + ", hardwareCostsB=" + getHardwareCostsB()
                    + ", hardwareCostsBDescription=" + getHardwareCostsBDescription()
                    + ", hardwareCostsC=" + getHardwareCostsC()
                    + ", hardwareCostsCDescription=" + getHardwareCostsCDescription()
                    + ", accessoryCosts=" + getAccessoryCosts()
                    + ", accessoryCostsDescription=" + getAccessoryCostsDescription()
                    + ", serviceCosts=" + getServiceCosts()
                    + ", serviceCostsDescription=" + getServiceCostsDescription()
                    + ", projectCosts=" + getProjectCosts()
                    + ", unknownCosts=" + getUnknownCosts()
                    + ", agentCosts=" + getAgentCosts()
                    + ", salesCosts=" + getSalesCosts()
                    + ", tipCosts=" + getTipCosts()
                    + ", costs=" + getCosts()
                    + ", netCosts=" + getNetCosts()
                    + ", totalCosts=" + getTotalCosts()
                    + ", finalCosts=" + getFinalCosts()
                    + "]");
        }

    }

    private Double summarizeCosts(List<Item> items) {
        BigDecimal result = new BigDecimal(0);
        for (int i = 0, j = items.size(); i < j; i++) {
            Item item = items.get(i);
            BigDecimal price = NumberUtil.round(item.getPartnerPrice().multiply(new BigDecimal(item.getQuantity())), 5);
            if (log.isDebugEnabled()) {
                log.debug("summarizeCosts() adding item [id=" + item.getId()
                        + ", reference=" + item.getReference()
                        + ", product=" + item.getProduct().getProductId()
                        + ", name=" + item.getProduct().getName()
                        + ", quantity=" + item.getQuantity()
                        + ", partnerPrice=" + item.getPartnerPrice()
                        + ", sum=" + price + "]");

            }
            result = result.add(price);
        }
        if (log.isDebugEnabled()) {
            log.debug("summarizeCosts() done [summary=" + result + "]");
        }
        return NumberUtil.round(result.doubleValue(), 5);
    }

    public List<Item> getHardwareCostsAItems() {
        return hardwareCostsAItems;
    }

    public List<Item> getHardwareCostsBItems() {
        return hardwareCostsBItems;
    }

    public List<Item> getHardwareCostsCItems() {
        return hardwareCostsCItems;
    }

    public List<Item> getOtherCostsItems() {
        return otherCostsItems;
    }

    public List<Item> getForeignCostsItems() {
        return foreignCostsItems;
    }

    public List<Item> getDevelopmentCostsItems() {
        return developmentCostsItems;
    }

    public List<Item> getUnknownCostsItems() {
        return unknownCostsItems;
    }
}
