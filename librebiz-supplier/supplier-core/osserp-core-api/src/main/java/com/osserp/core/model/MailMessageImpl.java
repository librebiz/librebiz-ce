/**
 *
 * Copyright (C) 2021 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License (AGPL)
 * version 3 as published by the Free Software Foundation. In accordance
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code
 * versions must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area
 * or, if not reasonably feasible for technical reason, as a top-level
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute.
 *
 */
package com.osserp.core.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.osserp.common.beans.AbstractEntity;
import com.osserp.core.mail.MailMessage;

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public class MailMessageImpl extends AbstractEntity implements MailMessage {

    private static final long serialVersionUID = 1L;

    private Long referenceType;
    private Long typeId;
    private Long userId;
    private Long accountId;
    private Long businessId;
    private boolean received = false;
    private String messageId;
    private String contentType;
    private String originator;
    private String recipient;
    private String recipients;
    private Date dateReceived;
    private Date dateSent;
    private String subject;
    private String text;
    private String html;
    private Integer status = MailMessage.STATUS_NEW;

    private List<MailAttachment> attachments = new ArrayList<>();
    private List<MailMessageHeader> headers = new ArrayList<>();

    public MailMessageImpl() {
        super();
    }

    public MailMessageImpl(
        Long id,
        Long reference,
        Long referenceType,
        Long accountId,
        Long businessId,
        boolean received,
        String messageId,
        String contentType,
        String originator,
        String recipient,
        String recipients,
        Date dateReceived,
        Date dateSent,
        String subject,
        String text,
        String html,
        Integer status) {
        super(id, reference);
        this.referenceType = referenceType;
        this.accountId = accountId;
        this.businessId = businessId;
        this.received = received;
        this.messageId = messageId;
        this.contentType = contentType;
        this.originator = originator;
        this.recipient = recipient;
        this.recipients = recipients;
        this.dateReceived = dateReceived;
        this.dateSent = dateSent;
        this.subject = subject;
        this.text = text;
        this.html = html;
        this.status = status;
    }

    protected MailMessageImpl(MailMessageImpl other) {
        super(other);
        this.referenceType = other.referenceType;
        this.typeId = other.typeId;
        this.userId = other.userId;
        this.businessId = other.businessId;
        this.accountId = other.accountId;
        this.received = other.received;
        this.messageId = other.messageId;
        this.contentType = other.contentType;
        this.originator = other.originator;
        this.recipient = other.recipient;
        this.recipients = other.recipients;
        this.dateReceived = other.dateReceived;
        this.dateSent = other.dateSent;
        this.subject = other.subject;
        this.text = other.text;
        this.html = other.html;
        this.status = other.status;
    }

    public MailMessageImpl(
            boolean received,
            Long userId,
            Long reference,
            Long referenceType,
            Long businessId,
            String originator,
            String recipient,
            String recipients,
            String messageId,
            Date dateSent,
            Date dateReceived,
            String subject,
            String text,
            String html) {
        setReference(reference);
        this.received = received;
        this.userId = userId;
        this.referenceType = referenceType;
        this.businessId = businessId;
        this.originator = originator;
        this.recipient = recipient;
        this.recipients = recipients;
        this.messageId = messageId;
        this.dateSent = dateSent;
        this.dateReceived = dateReceived;
        this.subject = subject;
        this.text = text;
        this.html = html;
    }

    public Long getReferenceType() {
        return this.referenceType;
    }

    public void setReferenceType(Long referenceType) {
        this.referenceType = referenceType;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getBusinessId() {
        return this.businessId;
    }

    public void setBusinessId(Long businessId) {
        this.businessId = businessId;
    }

    public boolean isReceived() {
        return received;
    }

    public void setReceived(boolean received) {
        this.received = received;
    }

    public String getMessageId() {
        return this.messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getContentType() {
        return this.contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getOriginator() {
        return this.originator;
    }

    public void setOriginator(String originator) {
        this.originator = originator;
    }

    public String getRecipient() {
        return this.recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getRecipients() {
        return this.recipients;
    }

    public void setRecipients(String recipients) {
        this.recipients = recipients;
    }

    public Date getDateReceived() {
        return dateReceived;
    }

    public void setDateReceived(Date dateReceived) {
        this.dateReceived = dateReceived;
    }

    public Date getDateSent() {
        return dateSent;
    }

    public void setDateSent(Date dateSent) {
        this.dateSent = dateSent;
    }

    public String getSubject() {
        return this.subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public Integer getStatus() {
        return this.status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<MailAttachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<MailAttachment> attachments) {
        this.attachments = attachments;
    }

    public List<MailMessageHeader> getHeaders() {
        return headers;
    }

    public void setHeaders(List<MailMessageHeader> headers) {
        this.headers = headers;
    }

}
