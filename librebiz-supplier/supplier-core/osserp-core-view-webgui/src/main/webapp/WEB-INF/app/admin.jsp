<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<o:adminContext/>
<o:resource/>
<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="title"><o:displayTitle/></tiles:put>
	<tiles:put name="headline"><fmt:message key="currentlyLoggedInAccounts"/> [<o:listSize value="${sessionScope.portalView.onlineUsers}"/>]</tiles:put>
	<tiles:put name="headline_right"> </tiles:put>
	<tiles:put name="content" type="string">
        <div class="content-area" id="onlineUsersContent">
            <div class="row">
                <div class="col-lg-12 panel-area">    
                    <c:import url="${viewdir}/shared/_usersOnline.jsp"/>
                </div>
            </div>
        </div>
	</tiles:put>
</tiles:insert>
