/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.groovy.web


import com.osserp.common.ErrorCode

/**
 * 
 * Exception class is used in case of view context errors such as exceptions caused by invalid navigation, etc.
 *  
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public class ViewContextException extends WebContextException {
    private static final long serialVersionUID = 42L

    ViewContextException() {
        super()
    }
    
    ViewContextException(Throwable cause) {
        super(cause);
    }

    ViewContextException(String message) {
        super(message)
    }

    ViewContextException(String message, String target) {
        super(message, target)
    }

    /**
     * Creates a new view context exception and initializes target with last requested page as provided 
     * by an instance of portalView
     * @param message
     * @param view
     */
    ViewContextException(String message, View view) {
        super(message, view?.env?.portalView?.page?.name ?: view?.env?.homeTarget)
    }

    /**
     * Creates a new view context exception and initializes target with last requested page as provided
     * by an instance of portalView. Error message is initialized by ErrorCode.INVALID_CONTEXT
     * @param view
     */
    ViewContextException(View view) {
        super(ErrorCode.INVALID_CONTEXT, view?.env?.portalView?.page?.name ?: view?.env?.homeTarget)
    }
}
