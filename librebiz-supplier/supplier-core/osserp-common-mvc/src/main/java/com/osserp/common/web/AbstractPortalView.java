/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 11, 2007 6:30:33 PM 
 * 
 */
package com.osserp.common.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.User;
import com.osserp.common.gui.HelpLink;
import com.osserp.common.gui.HelpLinkManager;
import com.osserp.common.gui.Menu;
import com.osserp.common.gui.MenuHeader;
import com.osserp.common.gui.MenuLink;
import com.osserp.common.gui.MenuProvider;
import com.osserp.common.service.ResourceLocator;
import com.osserp.common.service.SharedObjects;
import com.osserp.common.service.SysInfo;
import com.osserp.common.service.UserScope;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractPortalView extends AbstractView implements PortalView {

    private static Logger log = LoggerFactory.getLogger(AbstractPortalView.class.getName());
    
    private WebConfig webConfig = null;
    private String applicationName = null;
    private String serverUrl = null;
    
    private Page page = null;
    private Menu menu = null;
    private List<Menu> menus = new ArrayList<>();
    private boolean shareMenu = false;

    private boolean userHasTelephone = false;
    private boolean clickToCallEnabled = false;
    private boolean clickToCallEnabledWizard = false;

    private boolean helpConfigMode = false;
    private String helpServer = null;
    private HelpLink helpLink = null;
    private Map<String, HelpLink> helpPages = new HashMap<>();

    private Map<String, String> permissions = new HashMap<>();
    
    private Map<Locale, ResourceBundle> resourceBundles = new HashMap<>();

    private boolean backendAvailable = false;
    private String backendErrorMessage = null;
    
    private boolean userLoggedIn = false;
    private String loginTarget = "index";
    private String logoutTarget = "login";

    private String remoteAddress = null;
    
    protected AbstractPortalView() {
        super();
    }

    protected AbstractPortalView(String applicationName, String helpServerUrl) {
        super();
        this.applicationName = applicationName;
        this.helpServer = helpServerUrl;
    }
    
    protected abstract MenuProvider getMenuProvider();
    
    public void init(HttpServletRequest request) throws PermissionException {
        if (getLocator() == null) {
            setLocator(ContextUtil.getServiceLocator(request));
        }
        initRequest(request);
        shareMenu = SessionUtil.isShareMenu(request.getSession());
        serverUrl = RequestUtil.getServerURL(request);
        initMenu();
        if (log.isDebugEnabled()) {
            log.debug("init() done [serverUrl=" + serverUrl
                    + ", shareMenu=" + shareMenu + "]");
        }
    }
    
    public void initRequest(HttpServletRequest request) {
        remoteAddress = RequestUtil.getRemoteAddress(request);
        if (getUser() != null) {
            userLoggedIn = true;
            if (log.isDebugEnabled()) {
                log.debug("init() invoked by user [id=" + getUser().getId() + "]");
            }
        } else if (log.isDebugEnabled()) {
            log.debug("init() invoked by anonymous [ip=" + remoteAddress + "]");
        }
    }

    public final ResourceBundle getResourceBundle(Locale locale) {
        if (!resourceBundles.containsKey(locale)) {
            try {
                ResourceLocator resLocator = getResourceLocatorImpl();
                if (resLocator != null) {
                    ResourceBundle bundle = resLocator.getResourceBundle(locale);
                    resourceBundles.put(locale, bundle);
                    if (log.isDebugEnabled()) {
                        log.debug("getResourceBundle() loading bundle from backend");
                    }
                }
            } catch (Throwable t) {
                log.error("getResourceBundle() ignoring failure [message=" + t.getMessage() + ", exception=" + t.getClass().getName() + "]");
            }
        }
        return resourceBundles.get(locale);
    }
    
    protected abstract ResourceLocator getResourceLocatorImpl();

    public boolean isBackendAvailable() {
        return backendAvailable;
    }

    public void setBackendAvailable(boolean backendAvailable) {
        this.backendAvailable = backendAvailable;
    }

    public String getBackendErrorMessage() {
        return backendErrorMessage;
    }

    public void setBackendErrorMessage(String backendErrorMessage) {
        this.backendErrorMessage = backendErrorMessage;
    }
    
    public void enableBackendAvailabilityProperty() {
        backendAvailable = true;
        backendErrorMessage = null;
    }
    
    public void disableBackendAvailabilityProperty() {
        backendAvailable = false;
        backendErrorMessage = "Required resources not available, see logs";
    }
    
    public void disableBackendAvailabilityProperty(String message) {
        backendAvailable = false;
        backendErrorMessage = message;
    }

    public final SysInfo getSysInfo() {
        try {
            return new SysInfo();
        } catch (Throwable t) {
            log.warn("getSysInfo() failed [message=" + t.getMessage() + "]", t);
            return null;
        }
    }

    public boolean isSystemSetupRequired() {
        return false;
    }

    public boolean isSystemSetupMode() {
        return getSysInfo().isSetupEnabled();
    }

    public String getLoginTarget() {
        return loginTarget;
    }

    public void setLoginTarget(String loginTarget) {
        this.loginTarget = loginTarget;
    }

    public String getLogoutTarget() {
        return logoutTarget;
    }

    public void setLogoutTarget(String logoutTarget) {
        this.logoutTarget = logoutTarget;
    }

    public boolean isUserLoggedIn() {
        return userLoggedIn;
    }

    protected void setUserLoggedIn(boolean userLoggedIn) {
        this.userLoggedIn = userLoggedIn;
    }

    public String getRemoteAddress() {
        return remoteAddress;
    }

    protected void setRemoteAddress(String remoteAddress) {
        this.remoteAddress = remoteAddress;
    }

    public String getApplicationName() {
        return applicationName;
    }

    protected void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public Page getPage() {
        return page;
    }

    public final void activatePage(Page pageToActivate) {
        if (pageToActivate != null 
                && pageToActivate.getName().indexOf("/errors/error_") < 0) {
            page = pageToActivate;
            helpLink = getHelpLink(page);
            if (helpLink == null) {
                HelpLinkManager manager = getHelpLinkManager();
                if (manager != null) {
                    helpLink = manager.getLink(page.getName());
                }
            }
            if (helpLink != null) {
                addHelpLink(helpLink);
            } 
            if (log.isDebugEnabled()) {
                log.debug("activatePage() done [page="
                        + (page == null ? "null" : page.toString())
                        + "]");
            }
        } else if (log.isDebugEnabled()) {
            // do not store error page since it wasn't expected by user and
            // should therefore not be used as target for currentPageServlet  
            log.debug("activatePage() ignoring error or non existing [page="
                    + (page == null ? "null" : page.toString())
                    + "]");
        } 
    }

    protected void initMenu() {
        if (shareMenu) {
            MenuProvider menuProvider = null;
            try {
                menuProvider = getMenuProvider();
                menus = menuProvider.getMenus();
                for (int i = 0, j = menus.size(); i < j; i++) {
                    Menu next = menus.get(i);
                    if (next.isDefaultMenu()) {
                        menu = next;
                        if (log.isDebugEnabled()) {
                            log.debug("initMenu() default menu assigned [name=" + menu.getName() + "]");
                        }
                        break;
                    }
                }
            } catch (Exception e) {
                if (menuProvider != null) {
                    log.warn("initMenu() caught exception on attempt to set menu [message=" + e.getMessage() + "]", e);
                }
            }
        } else if (menu == null) {

            try {
                menu = (Menu) getSharedObjects().fetch(getUser().getId(), PortalView.SHARED_MENU);
                if (log.isDebugEnabled()) {
                    if (menu != null) {
                        log.debug("initMenu() shared menu assigned [name=" + menu.getName() + "]");
                    } else {
                        log.debug("initMenu() did not find sharedMenu; menu not available");
                    }
                }
            } catch (Exception e) {
                if (log.isInfoEnabled()) {
                    log.info("initMenu() found neither menuProvider nor sharedMenu; menu not available");
                }
            }
        }
        updateSharedMenu();
    }

    public Menu getMenu() {
        return menu == null ? null : menu.getCustomized(getUser());
    }
    
    protected List<Menu> getMenus() {
        return menus;
    }
    
    protected void setMenus(List<Menu> menus) {
        this.menus = menus;
    }

    public void selectMenu(String name) {
        if (menu != null && name != null && !menu.getName().equals(name)) {
            for (int i = 0, j = menus.size(); i < j; i++) {
                Menu next = menus.get(i);
                if (next.getName().equals(name)) {
                    menu = next;
                    break;
                }
            }
            
        }
        updateSharedMenu();
    }

    public void toggleMenuHeader(String name) {
        if (log.isDebugEnabled()) {
            log.debug("toggleMenuHeader() invoked [name=" + name + "]");
        }
        for (int i = 0, j = menu.getHeaders().size(); i < j; i++) {
            MenuHeader next = menu.getHeaders().get(i);
            if (next.getName().equals(name)) {
                next.setActivated(!next.isActivated());
                break;
            }
        }
        updateSharedMenu();
    }

    public void reloadMenu() {
        String currentName = (menu != null) ? menu.getName() : null;
        List<MenuHeader> mpl = getActivatedMenuHeader();
        initMenu();
        if (isSet(currentName)) {
            selectMenu(currentName);
            if (!mpl.isEmpty() && menu != null) {
                for (int i = 0, j = mpl.size(); i < j; i++) {
                    MenuHeader next = mpl.get(i);
                    for (int k = 0, l = menu.getHeaders().size(); k < l; k++) {
                        MenuHeader pos = menu.getHeaders().get(k);
                        if (next.getId().equals(pos.getId())) {
                            pos.setActivated(true);
                        }
                    }
                }
            }
        }
    }

    private List<MenuHeader> getActivatedMenuHeader() {
        List<MenuHeader> mpl = new ArrayList<>();
        if (menu != null) {
            for (int i = 0, j = menu.getHeaders().size(); i < j; i++) {
                MenuHeader pos = menu.getHeaders().get(i);
                if (pos.isActivated()) {
                    mpl.add(pos);
                }
            }
        }
        return mpl;
    }

    protected void updateSharedMenu() {
        if (shareMenu && getUser() != null && menu != null) {
            Menu clone = (Menu) menu.clone();
            if (serverUrl != null) {
                for (Iterator<MenuHeader> p = clone.getHeaders().iterator(); p.hasNext();) {
                    MenuHeader pos = p.next();
                    for (Iterator<MenuLink> l = pos.getLinks().iterator(); l.hasNext();) {
                        MenuLink link = l.next();
                        if (!link.isDirectLink()) {
                            link.prefixUrl(serverUrl);
                        }
                    }
                }
            }
            try {
                getUserScope().addObject(getUser().getId(), PortalView.SHARED_MENU, clone);
            } catch (NullPointerException npe) {
                log.warn("updateSharedMenu() ignoring npe...");
            } catch (Exception e) {
                log.warn("updateSharedMenu() failed [message=" + e.getMessage() + "]", e);
            }
        }
    }

    protected void setMenu(Menu menu) {
        this.menu = menu;
    }

    public void updateHelp(String name, String value) throws ClientException {
        if (helpLink != null) {
            HelpLinkManager manager = getHelpLinkManager();
            if (manager != null) {
                helpLink.setDescription(name);
                helpLink.setPage(value);
                manager.update(helpLink);
                addHelpLink(helpLink);
            }
        }
    }

    public boolean isHelpAvailable() {
        return (getHelpServerUrl() != null && helpLink != null && helpLink.isActivated());
    }

    public void enableHelp() {
        if (helpLink != null && isSet(helpLink.getPage())) {
            HelpLinkManager manager = getHelpLinkManager();
            if (manager != null) {
                helpLink.setStatus(HelpLink.ACTIVATED);
                try {
                    manager.update(helpLink);
                    addHelpLink(helpLink);
                } catch (ClientException e) {
                    // should not happen here
                    log.warn("enableHelp() ignoring failure [message=" + e.getMessage() + "]");
                }
            }
        }
    }

    public void disableHelp() {
        if (helpLink != null) {
            HelpLinkManager manager = getHelpLinkManager();
            if (manager != null) {
                helpLink.setStatus(HelpLink.DEACTIVATED);
                try {
                    manager.update(helpLink);
                    addHelpLink(helpLink);
                } catch (ClientException e) {
                    // should not happen here
                    log.warn("disableHelp() ignoring failure [message=" + e.getMessage() + "]");
                }
            }
        }
    }

    public HelpLink getHelpLink() {
        return helpLink;
    }

    public String getHelpLinkUrl() {
        if (helpLink == null || isNotSet(helpLink.getPage())) {
            return null;
        }
        StringBuilder url = new StringBuilder();
        if (getHelpServerUrl() != null) {
            if (getHelpServerUrl().endsWith("/")) {
                url.append(getHelpServerUrl()
                        .substring(0, getHelpServerUrl().length() - 1));
            } else {
                url.append(getHelpServerUrl());
            }
        }
        if (helpLink.getPage().indexOf("/") > 0) {
            url.append("/");
        }
        url.append(helpLink.getPage());
        return url.toString();
    }

    public String getHelpLinkParameters() {
        User user = getUser();
        if (user == null) {
            return "";
        }
        StringBuilder p = new StringBuilder(64);
        p.append("?uid=").append(user.getId()).append("&sid=").append(user.getSessionId());
        return p.toString();
    }

    public boolean isHelpConfigMode() {
        return helpConfigMode;
    }

    public void enableHelpConfigMode() {
        this.helpConfigMode = true;
    }

    public void disableHelpConfigMode() {
        this.helpConfigMode = false;
    }

    protected void setHelpConfigMode(boolean helpConfigMode) {
        this.helpConfigMode = helpConfigMode;
    }

    public String getHelpServerUrl() {
        return helpServer;
    }

    public Map<String, String> getPermissions() {
        return permissions;
    }

    public void addPermission(String name, String description) {
        if (permissions.containsKey(name)) {
            String old = permissions.get(name);
            if (old != null) {
                description = old + "; " + description;
            }
        }
        permissions.put(name, description);
    }

    public void resetPermissions() {
        permissions = new HashMap<>();
    }

    protected String getHelpServer() {
        return helpServer;
    }

    protected void setHelpServerUrl(String helpServer) {
        this.helpServer = helpServer;
    }

    protected void addHelpLink(HelpLink helpLinkToAdd) {
        helpPages.put(helpLinkToAdd.getName(), helpLinkToAdd);
    }

    protected HelpLink getHelpLink(Page pageToAdd) {
        if (helpPages != null && helpPages.containsKey(pageToAdd.getName())) {
            return helpPages.get(pageToAdd.getName());
        }
        return null;
    }

    protected void setPage(Page page) {
        this.page = page;
    }

    protected SharedObjects getSharedObjects() {
        return null;
    }

    protected UserScope getUserScope() {
        return null;
    }

    /**
     * Override this method if you want to provide external help system support
     * @return helpLinkManager
     */
    protected HelpLinkManager getHelpLinkManager() {
        return null;
    }

    public WebConfig getWebConfig() {
        return webConfig;
    }

    public void setWebConfig(WebConfig webConfig) {
        this.webConfig = webConfig;
    }

    public boolean isUserHasTelephone() {
        return userHasTelephone;
    }

    public void setUserGotTelephone(boolean userHasTelephone) {
        this.userHasTelephone = userHasTelephone;
    }

    public boolean isClickToCallEnabled() {
        return clickToCallEnabled;
    }

    public void setClickToCallEnabled(boolean clickToCallEnabled) {
        this.clickToCallEnabled = clickToCallEnabled;
    }

    public boolean isClickToCallEnabledWizard() {
        return clickToCallEnabledWizard;
    }

    public void setClickToCallEnabledWizard(boolean clickToCallEnabledWizard) {
        this.clickToCallEnabledWizard = clickToCallEnabledWizard;
    }

    public void setTelephoneStatus(boolean userHasTelephone, boolean clickToCallEnabled, boolean clickToCallEnabledWizard) {
        this.userHasTelephone = userHasTelephone;
        this.clickToCallEnabled = clickToCallEnabled;
        this.clickToCallEnabledWizard = clickToCallEnabledWizard;
    }
}
