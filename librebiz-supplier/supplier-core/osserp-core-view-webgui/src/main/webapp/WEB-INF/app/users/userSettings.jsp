<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<o:resource />
<v:view viewName="userSettingsView" />
<c:set var="portalView" value="${sessionScope.portalView}" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="headline">
        <fmt:message key="myAccount" />
    </tiles:put>

    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="setupContent">
            <div class="row">

                <div class="col-md-6 panel-area panel-area-default">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>
                                <fmt:message key="navigationMenuSettingsLabel" />
                            </h4>
                        </div>
                    </div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <fmt:message key="myStartpage" />
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <select name="targetId" onchange="gotoUrl(this.value);" class="form-control">
                                        <c:forEach var="obj" items="${view.startupTargets}">
                                            <option value="<v:url value="${view.baseLink}/updateStartup?id=${obj.id}"/>"
                                                <c:if test="${view.user.startup.id == obj.id}">selected="selected"</c:if>>
                                                <o:out value="${obj.name}" />
                                            </option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <fmt:message key="navigationMenuLabel" />
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label for="myEvents">
                                            <v:checkboxAction name="events" value="${view.user.properties['events'].enabled}" url="${view.baseLink}/switchProperty?name=events" />
                                            <fmt:message key="myEvents" />
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <c:if test="${!user.admin}">

                            <c:if test="${view.user.sales or view.user.projectManager}">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group"></div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <div class="checkbox">
                                                <label for="requestsByEmployee">
                                                    <v:checkboxAction name="requestsByEmployee" value="${view.user.properties['requestsByEmployee'].enabled}" url="${view.baseLink}/switchProperty?name=requestsByEmployee" />
                                                    <fmt:message key="requestsByEmployee" />
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:if>

                            <c:choose>
                                <c:when test="${view.user.sales and !view.user.projectManager}">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group"></div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <div class="checkbox">
                                                    <label for="salesBySales">
                                                        <v:checkboxAction name="salesBySales" value="${view.user.properties['salesBySales'].enabled}" url="${view.baseLink}/switchProperty?name=salesBySales" />
                                                        <fmt:message key="myOrders" />
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </c:when>
                                <c:when test="${view.user.projectManager and !view.user.sales}">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group"></div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <div class="checkbox">
                                                    <label for="salesByManager">
                                                        <v:checkboxAction name="salesByManager" value="${view.user.properties['salesByManager'].enabled}" url="${view.baseLink}/switchProperty?name=salesByManager" />
                                                        <fmt:message key="myOrders" />
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </c:when>
                            </c:choose>

                            <c:if test="${user.projectManager or user.executive or user.sales or user.accounting}">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="checkbox">
                                                <label for="projectMonitoring">
                                                    <v:checkboxAction name="projectMonitoring" value="${view.user.properties['projectMonitoring'].enabled}" url="${view.baseLink}/switchProperty?name=projectMonitoring" />
                                                    <fmt:message key="projectMonitoring" />
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="projectMonitoringStatus" class="checkbox-label">
                                                <fmt:message key="status" />
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <v:form name="projectMonitoringStatusForm" url="${view.baseLink}/updateProperty" style="display:inline;">
                                                <input type="hidden" name="name" value="projectMonitoringStatus"/>
                                                <v:text styleClass="form-control" name="value"
                                                        value="${view.user.properties['projectMonitoringStatus'].asLong}"
                                                        onchange="document.projectMonitoringStatusForm.submit();" />
                                            </v:form>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group"></div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="checkbox">
                                                <label for="salesMonitoring">
                                                    <v:checkboxAction name="salesMonitoring" value="${view.user.properties['salesMonitoring'].enabled}" url="${view.baseLink}/switchProperty?name=salesMonitoring" />
                                                    <fmt:message key="salesMonitoringLabel" />
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="salesMonitoringStatus" class="checkbox-label">
                                                <fmt:message key="status" />
                                            </label>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <v:form name="salesMonitoringStatusForm" url="${view.baseLink}/updateProperty" style="display:inline;">
                                                <input type="hidden" name="name" value="salesMonitoringStatus"/>
                                                <v:text styleClass="form-control" name="value"
                                                        value="${view.user.properties['salesMonitoringStatus'].asLong}"
                                                        onchange="document.salesMonitoringStatusForm.submit();" />
                                            </v:form>
                                        </div>
                                    </div>
                                </div>
                            </c:if>

                            <c:if test="${view.user.sales or view.user.projectManager or view.user.purchaser or view.user.executive or view.user.accounting}">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group"></div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <div class="checkbox">
                                                <label for="deliveryCalendar">
                                                    <v:checkboxAction name="deliveryCalendar" value="${view.user.properties['deliveryCalendar'].enabled}" url="${view.baseLink}/switchProperty?name=deliveryCalendar" />
                                                    <fmt:message key="deliveryCalendar" />
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group"></div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <div class="checkbox">
                                                <label for="mountingCalendar">
                                                    <v:checkboxAction name="mountingCalendar" value="${view.user.properties['mountingCalendar'].enabled}" url="${view.baseLink}/switchProperty?name=mountingCalendar" />
                                                    <fmt:message key="mountingCalendar" />
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:if>

                        </c:if>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group"></div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label for="documentImports">
                                            <v:checkboxAction name="documentImports" value="${view.user.properties['documentImports'].enabled}" url="${view.baseLink}/switchProperty?name=documentImports" />
                                            <fmt:message key="documentImports" />
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <oc:systemPropertyEnabled name="fetchmailInboxSupport">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group"></div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label for="fetchmailInbox">
                                                <v:checkboxAction name="fetchmailInbox" value="${view.user.properties['fetchmailInbox'].enabled}" url="${view.baseLink}/switchProperty?name=fetchmailInbox" />
                                                <fmt:message key="inboxLabel" />
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </oc:systemPropertyEnabled>

                    </div>
                </div>

                <div class="col-md-6 panel-area panel-area-default">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>
                                <fmt:message key="settings" />
                            </h4>
                        </div>
                    </div>
                    <div class="panel-body">

                        <c:if test="${!user.admin}">

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <fmt:message key="fcsDisplay" />
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <select name="groupId" onchange="gotoUrl(this.value);" class="form-control">
                                            <option value="<v:url value="${view.baseLink}/updateFlowControlGroup"/>"><fmt:message key="displayAll" /></option>
                                            <c:forEach var="obj" items="${view.flowControlGroups}">
                                                <option value="<v:url value="${view.baseLink}/updateFlowControlGroup?id=${obj.id}" />"
                                                    <c:if test="${view.user.defaultFcsGroup == obj.id}">selected="selected"</c:if>>
                                                    <o:out value="${obj.name}" />
                                                </option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </c:if>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group"><fmt:message key="searchLabel" /></div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label for="contactSearchStartsWith">
                                            <v:checkboxAction name="contactSearchStartsWith" value="${view.user.properties['contactSearchStartsWith'].enabled}" url="${view.baseLink}/switchProperty?name=contactSearchStartsWith" />
                                            <fmt:message key="contactSearchStartsWith" />
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group"></div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label for="productSearchKeepAfterAdd">
                                            <v:checkboxAction name="productSearchKeepAfterAdd" value="${view.user.properties['productSearchKeepAfterAdd'].enabled}" url="${view.baseLink}/switchProperty?name=productSearchKeepAfterAdd" />
                                            <fmt:message key="productSearchKeepAfterAdd" />
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <c:if test="${view.user.purchaser}">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group"></div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label for="displayAllPurchasersOrders">
                                                <v:checkboxAction name="displayAllPurchasersOrders" value="${view.user.properties['displayAllPurchasersOrders'].enabled}" url="${view.baseLink}/switchProperty?name=displayAllPurchasersOrders" />
                                                <fmt:message key="displayAllPurchasersOrders" />
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:if>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group"><fmt:message key="appearance" /></div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label for="uploadButtonNative">
                                            <v:checkboxAction name="uploadButtonNative" value="${view.user.properties['uploadButtonNative'].enabled}" url="${view.baseLink}/switchProperty?name=uploadButtonNative" />
                                            <fmt:message key="uploadButtonNative" />
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row next">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <fmt:message key="language" />
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <select name="locale" onchange="gotoUrl(this.value);" class="form-control">
                                        <c:forEach var="lang" items="${sessionScope.portalView.supportedLanguages}">
                                            <option value="<v:url value="${view.baseLink}/updateLocale?value=${lang}"/>"
                                                <c:if test="${userSettingsView.user.localeDisplay == lang}">selected="selected"</c:if>>
                                                <fmt:message key="${lang}" />
                                            </option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <c:set var="mapsProvider" value="${view.user.properties['mapsProvider'].value}" />
                        <div class="row next">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <fmt:message key="mapsProvider" />
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <select name="name" onchange="gotoUrl(this.value);" class="form-control">
                                        <option value="<v:url value="${view.baseLink}/updateProperty?name=mapsProvider&value=osm"/>"
                                            <c:if test="${empty mapsProvider || mapsProvider == 'osm'}">selected="selected"</c:if>>
                                            OpenStreetMap
                                        </option>
                                        <option value="<v:url value="${view.baseLink}/updateProperty?name=mapsProvider&value=ggl"/>"
                                            <c:if test="${!empty mapsProvider && mapsProvider == 'ggl'}">selected="selected"</c:if>>
                                            Google Maps
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group"><fmt:message key="emailSettings" /></div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label for="requestNoteMailAutoCC">
                                            <v:checkboxAction name="requestNoteMailAutoCC" value="${view.user.properties['requestNoteMailAutoCC'].enabled}" url="${view.baseLink}/switchProperty?name=requestNoteMailAutoCC" />
                                            <fmt:message key="requestNoteMailAutoCC" />
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group"> </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label for="requestNoteMailAutoBCC">
                                            <v:checkboxAction name="requestNoteMailAutoBCC" value="${view.user.properties['requestNoteMailAutoBCC'].enabled}" url="${view.baseLink}/switchProperty?name=requestNoteMailAutoBCC" />
                                            <fmt:message key="requestNoteMailAutoBCC" />
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group"> </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label for="salesNoteMailAutoCC">
                                            <v:checkboxAction name="salesNoteMailAutoCC" value="${view.user.properties['salesNoteMailAutoCC'].enabled}" url="${view.baseLink}/switchProperty?name=salesNoteMailAutoCC" />
                                            <fmt:message key="salesNoteMailAutoCC" />
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group"> </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label for="salesNoteMailAutoBCC">
                                            <v:checkboxAction name="salesNoteMailAutoBCC" value="${view.user.properties['salesNoteMailAutoBCC'].enabled}" url="${view.baseLink}/switchProperty?name=salesNoteMailAutoBCC" />
                                            <fmt:message key="salesNoteMailAutoBCC" />
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <c:if test="${!empty portalView and !empty portalView.helpServerUrl}">
                            <o:permission role="help_edit,runtime_config,executive_saas" info="permissionHelpConfiguration">
                                <div class="row next">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <fmt:message key="helpConfiguration" />
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <c:choose>
                                                <c:when test="${portalView.helpConfigMode}">
                                                    <a href="<c:url value="/helpConfig.do?method=disableHelpConfig&target=userSettings"/>" title="<fmt:message key="switchOff"/>"><fmt:message key="switchEnabled" /></a>
                                                </c:when>
                                                <c:otherwise>
                                                    <a href="<c:url value="/helpConfig.do?method=enableHelpConfig&target=userSettings"/>" title="<fmt:message key="switchOn"/>"><fmt:message key="switchDisabled" /></a>
                                                </c:otherwise>
                                            </c:choose>
                                        </div>
                                    </div>
                                </div>
                            </o:permission>
                        </c:if>

                    </div>
                </div>

            </div>
        </div>
    </tiles:put>
</tiles:insert>
