/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 *
 * Created on 03.03.2013
 *
 */
package com.osserp.groovy.web

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.servlet.http.HttpSession

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.common.TimeoutException
import com.osserp.common.web.PortalView
import com.osserp.common.web.RequestUtil
import com.osserp.common.web.RequestForm
import com.osserp.common.web.ResourceUtil
import com.osserp.common.web.SessionUtil

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class AbstractIndexController extends AbstractPortalController {
    private static Logger logger = LoggerFactory.getLogger(AbstractIndexController.class.getName())

    protected String redirectLogin(HttpServletRequest request) {
        redirect(request, '/login/forward')
    }

    @RequestMapping('/')
    def root(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("root: invoked [host=${RequestUtil.getRemoteAddress(request)}]")
        index(request, response)
    }

    @RequestMapping('/admin')
    def admin(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("admin: invoked [host=${RequestUtil.getRemoteAddress(request)}]")
        PortalView view = getPortalView(request)
        if (!view?.user) {
            logger.debug('admin: no user logged in, probably timeout')
            throw new TimeoutException()
        }
        HttpSession session = request.session
        SessionUtil.cleanUp(session)
        'admin'
    }

    @RequestMapping('/adminExit')
    def adminExit(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("adminExit: invoked [host=${RequestUtil.getRemoteAddress(request)}]")
        index(request, response)
    }

    @RequestMapping('/setup')
    def setup(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("setup: invoked [host=${RequestUtil.getRemoteAddress(request)}]")
        PortalView view = getPortalView(request)
        if (!view?.user) {
            logger.debug('setup: no user logged in, probably timeout')
            throw new TimeoutException()
        }
        HttpSession session = request.session
        SessionUtil.cleanUp(session)
        'setup'
    }

    @RequestMapping('/setupExit')
    def setupExit(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("setupExit: invoked [host=${RequestUtil.getRemoteAddress(request)}]")
        index(request, response)
    }

    @RequestMapping('/index')
    def index(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("index: invoked [host=${RequestUtil.getRemoteAddress(request)}]")
        HttpSession session = request.session
        SessionUtil.cleanUp(session)
        PortalView view = getPortalView(request)
        if (!view.user) {
            logger.debug('index: no user found, login required')
            return redirectLogin(request)
        }
        logger.debug('index: user found, performing startupTarget lookup')
        String startup = view.startupTarget
        if (startup) {
            return redirect(request, startup)
        }
        logger.debug('index: did not find any startupTarget [result=index]')
        'index'
    }

    @RequestMapping('/home')
    def home(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("home: invoked [host=${RequestUtil.getRemoteAddress(request)}]")
        index(request, response)
    }

    @RequestMapping('/onlineUsers')
    def onlineUsers(HttpServletRequest request) {
        logger.debug("onlineUsers: invoked [host=${RequestUtil.getRemoteAddress(request)}]")
        if (!getPortalView(request)?.user) {
            return redirectLogin(request)
        }
        'onlineUsers'
    }

    @RequestMapping('/toggleMenu')
    def toggleMenu(HttpServletRequest request) {
        String target = RequestUtil.getTarget(request)
        logger.debug("toggleMenu: invoked [host=${RequestUtil.getRemoteAddress(request)}, target=${target}]")
        PortalView view = getPortalView(request)
        if (view == null) {
            throw new TimeoutException()
        }
        view.toggleMenuHeader(target)
        if (!view.page) {
            logger.debug("toggleMenu: no page found, forwarding to default [target=" + view.getMenu().getName() + "]")

            return redirect(request, "/${view.getMenu().getName()}")
        }
        logger.debug("toggleMenu: page found [name=${view.getPage().getName()}]")
        'redirect:/currentPage'
    }

    @RequestMapping('/dispatch')
    def dispatch(HttpServletRequest request) {
        HttpSession session = request.getSession()
        if (!session || !session.getAttribute('portalView')
                || !session.getAttribute('portalView').user) {
            throw new TimeoutException()
        }
        Map params = new RequestForm(request).getMapped()
        StringBuilder buffer = new StringBuilder(params['url'] ?: '')
        params.each { key, value ->
            if ('url' != key) {
                if (buffer.indexOf('?') > -1) {
                    buffer.append("&${key}=${value}")
                } else {
                    buffer.append("?${key}=${value}")
                }
            }
        }
        def target = buffer.toString()
        logger.debug("dispatch: invoked [host=${RequestUtil.getRemoteAddress(request)}, target=${target}]")
        SessionUtil.cleanUp(request.session)
        redirect(request, target)
    }

}
