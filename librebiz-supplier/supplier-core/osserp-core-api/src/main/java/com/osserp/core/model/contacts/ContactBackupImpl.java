/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 14, 2008 4:47:20 PM 
 * 
 */
package com.osserp.core.model.contacts;

import java.util.Date;

import org.jdom2.Element;

import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.ContactBackup;
import com.osserp.core.contacts.Phone;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ContactBackupImpl extends AbstractContactPerson implements ContactBackup {

    private Long id = null;

    protected ContactBackupImpl() {
        super();
    }

    protected ContactBackupImpl(ContactBackup o) {
        super(o);
        this.id = o.getId();
    }

    public ContactBackupImpl(Long changedBy, Contact otherValue) {
        super(otherValue);
        setPersonCreated(new Date(System.currentTimeMillis()));
        setPersonCreatedBy(changedBy);
    }

    public boolean isChanged(Contact c) {
        return (valueChanged(getTitle(), c.getTitle())
                || valueChanged(getSalutation(), c.getSalutation())
                || valueChanged(getType(), c.getType())
                || valueChanged(getStatus(), c.getStatus())
                || valueChanged(getFirstName(), c.getFirstName())
                || valueChanged(getLastName(), c.getLastName())
                || valueChanged(getBirthDate(), c.getBirthDate())
                || valueChanged(getOffice(), c.getOffice())
                || valueChanged(getSection(), c.getSection())
                || valueChanged(getPosition(), c.getPosition())
                || valueChanged(getAddress().getName(), c.getAddress().getName())
                || valueChanged(getAddress().getStreet(), c.getAddress().getStreet())
                || valueChanged(getAddress().getZipcode(), c.getAddress().getZipcode())
                || valueChanged(getAddress().getCity(), c.getAddress().getCity())
                || valueChanged(getAddress().getCountry(), c.getAddress().getCountry()));
    }

    @Override
    public Long getPrimaryKey() {
        return id;
    }

    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    @Override
    protected void createCommunications(Element contact) {
        // nothing to do for this implementation
    }

    public String getEmail() {
        // Backups currently don't provide this value
        return null;
    }

    public Phone getFax() {
        // Backups currently don't provide this value
        return null;
    }

    public Phone getMobile() {
        // Backups currently don't provide this value
        return null;
    }

    public Phone getPhone() {
        // Backups currently don't provide this value
        return null;
    }

    public Date getCreated() {
        return getPersonCreated();
    }

    public Long getCreatedBy() {
        return getPersonCreatedBy();
    }

    public Date getChanged() {
        return getPersonChanged();
    }

    public void setChanged(Date changed) {
        // properties are readonly
    }

    public Long getChangedBy() {
        return getPersonChangedBy();
    }

    public void setChangedBy(Long changedBy) {
        // properties are readonly
    }

    @Override
    public Object clone() {
        return new ContactBackupImpl(this);
    }
}
