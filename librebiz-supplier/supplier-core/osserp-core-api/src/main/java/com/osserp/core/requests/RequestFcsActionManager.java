/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 29, 2007 2:35:37 PM 
 * 
 */
package com.osserp.core.requests;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.Option;

import com.osserp.core.FcsAction;
import com.osserp.core.FcsActionManager;
import com.osserp.core.FcsConfig;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface RequestFcsActionManager extends FcsActionManager {

    /**
     * Provides a config by type
     * @param type
     * @return config or null if not exists
     */
    FcsConfig getConfig(Long type);
    
    /**
     * Adds an action to provided config
     * @param user
     * @param config
     * @param actionId
     * @return config with updated actions
     */
    FcsConfig addAction(DomainUser user, FcsConfig config, Long actionId);
    
    /**
     * Removes an action from provided config
     * @param user
     * @param config
     * @param actionId
     * @return config with updated actions
     */
    FcsConfig removeAction(DomainUser user, FcsConfig config, Long actionId);
    
    /**
     * Moves the position of an action up or down in list
     * @param config
     * @param actionId
     * @param up
     * @return config with new ordered actions
     */
    FcsConfig moveAction(FcsConfig config, Long actionId, boolean up);

    /**
     * Provides a list of all flow control actions
     * @return available flow control actions
     */
    List<FcsAction> findAll();

    /**
     * Creates a new flow control action
     * @param name
     * @param description
     * @param groupId
     * @param status
     * @param statusName
     * @return all actions including new created
     * @throws ClientException if name missing or already existing
     */
    List<FcsAction> createAction(
            String name, 
            String description, 
            Long groupId,
            Long status,
            String statusName) throws ClientException;

    /**
     * Provides a list of unused id to create new actions
     * @return unused action status list
     */
    List<Option> getFreeActionStatus();
    
    /**
     * Provides a list of unused id to create new cancellation actions
     * @return unused cancellation action status list
     */
    List<Option> getFreeCancellationStatus();
}
