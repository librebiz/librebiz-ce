/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * 
 */
package com.osserp.common.beans

import org.junit.Test

import com.osserp.common.beans.CommonClass

import static org.junit.Assert.*

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CommonClassTest {
	
	@Test
	void createClass() {
		CommonClass impl
		assertNull(impl)
		impl = new CommonClass()
		assertNotNull(impl)
	}
	
	@Test
	void isNumberByNumber() {
		CommonClass impl = new CommonClass()
		assertTrue(impl.isNumber('1'))
	}
	
	@Test
	void isNumberByNoNumber() {
		CommonClass impl = new CommonClass()
		assertFalse(impl.isNumber('a'))
	}
	
	@Test
	void isNumberByNull() {
		CommonClass impl = new CommonClass()
		assertFalse(impl.isNumber((String) null))
	}
	
	
	@Test
	void fetchIntegerByNumber() {
		CommonClass impl = new CommonClass()
		assertEquals(1, impl.fetchInteger('1'))
	}
	
	@Test
	void fetchIntegerByNoNumber() {
		CommonClass impl = new CommonClass()
		assertNull(impl.fetchInteger('a'))
	}
	
	@Test
	void fetchIntegerByNull() {
		CommonClass impl = new CommonClass()
		assertNull(impl.fetchInteger((String) null))
	}
	
	
	@Test
	void fetchIntegerByNumberArray() {
		CommonClass impl = new CommonClass()
		String[] s = new String[1]
		s[0] = '1'
		assertEquals(1, impl.fetchInteger(s))
	}
	
	@Test
	void fetchIntegerByNoNumberArray() {
		CommonClass impl = new CommonClass()
		String[] s = new String[1]
		s[0] = 'a'
		assertNull(impl.fetchInteger(s))
	}
	
	@Test
	void fetchIntegerByNullArray() {
		CommonClass impl = new CommonClass()
		assertNull(impl.fetchInteger((String[]) null))
	}

	
	
	@Test
	void fetchLongByNumber() {
		CommonClass impl = new CommonClass()
		assertEquals(1L, impl.fetchLong('1'))
	}
	
	@Test
	void fetchLongByNoNumber() {
		CommonClass impl = new CommonClass()
		assertNull(impl.fetchLong('a'))
	}
	
	@Test
	void fetchLongByNull() {
		CommonClass impl = new CommonClass()
		assertNull(impl.fetchLong((String) null))
	}
	
	
	@Test
	void fetchLongByNumberArray() {
		CommonClass impl = new CommonClass()
		String[] s = new String[1]
		s[0] = '1'
		assertEquals(1L, impl.fetchLong(s))
	}
	
	@Test
	void fetchLongByNoNumberArray() {
		CommonClass impl = new CommonClass()
		String[] s = new String[1]
		s[0] = 'a'
		assertNull(impl.fetchLong(s))
	}
	
	@Test
	void fetchLongByNullArray() {
		CommonClass impl = new CommonClass()
		assertNull(impl.fetchLong((String[]) null))
	}
}
