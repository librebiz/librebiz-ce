<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<c:set var="view" value="${sessionScope.portalView}" />
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><o:displayName /></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <c:if test="${!empty view and !view.systemSetupOnly}">
                    <c:choose>
                        <c:when test="${view.menu.name == 'admin'}">
                            <li><v:link url="/adminExit">
                                    <fmt:message key="home" />
                                </v:link>
                            </li>
                        </c:when>
                        <c:when test="${view.menu.name == 'setup'}">
                            <li><v:link url="/setupExit">
                                    <fmt:message key="home" />
                                </v:link>
                            </li>
                        </c:when>
                        <c:when test="${view.user.admin}">
                            <li><v:link url="/setup">
                                    <fmt:message key="setup" />
                                </v:link>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <li>
                                <v:link url="/admin" role="menu_admin_deny">
                                    <fmt:message key="admin" />
                                </v:link>
                            </li>
                        </c:otherwise>
                    </c:choose>
                </c:if>
                <li><v:link url="/login/logout" style="padding-right: 4em;">Logout</v:link></li>
            </ul>
            <%--
            <form class="navbar-form navbar-right">
                <input type="text" class="form-control" placeholder="Search...">
            </form>
            --%>
        </div>
    </div>
</nav>
