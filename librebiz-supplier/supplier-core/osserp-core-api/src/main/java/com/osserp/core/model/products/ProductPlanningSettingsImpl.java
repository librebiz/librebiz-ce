/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 30, 2010 11:18:52 AM 
 * 
 */
package com.osserp.core.model.products;

import java.util.Date;

import com.osserp.common.beans.AbstractEntity;
import com.osserp.core.planning.ProductPlanningSettings;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProductPlanningSettingsImpl extends AbstractEntity implements ProductPlanningSettings {

    private Date planningHorizon = null;
    private boolean confirmedPurchaseOnly = false;
    private boolean openPurchaseOnly = false;
    private boolean displayVacant = false;
    private Long stock = null;
    private Long productCategory = null;
    private Long productGroup = null;
    private Long productType = null;

    protected ProductPlanningSettingsImpl() {
        super();
    }

    public ProductPlanningSettingsImpl(Long reference, Long stock) {
        super(null, reference);
        this.stock = stock;
    }

    public Date getPlanningHorizon() {
        return planningHorizon;
    }

    public void setPlanningHorizon(Date planningHorizon) {
        this.planningHorizon = planningHorizon;
    }

    public boolean isConfirmedPurchaseOnly() {
        return confirmedPurchaseOnly;
    }

    public void setConfirmedPurchaseOnly(boolean confirmedPurchaseOnly) {
        this.confirmedPurchaseOnly = confirmedPurchaseOnly;
    }

    public boolean isOpenPurchaseOnly() {
        return openPurchaseOnly;
    }

    public void setOpenPurchaseOnly(boolean openPurchaseOnly) {
        this.openPurchaseOnly = openPurchaseOnly;
    }

    public boolean isDisplayVacant() {
        return displayVacant;
    }

    public void setDisplayVacant(boolean displayVacant) {
        this.displayVacant = displayVacant;
    }

    public Long getStock() {
        return stock;
    }

    public void setStock(Long stock) {
        this.stock = stock;
    }

    public Long getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(Long productCategory) {
        this.productCategory = productCategory;
    }

    public Long getProductGroup() {
        return productGroup;
    }

    public void setProductGroup(Long productGroup) {
        this.productGroup = productGroup;
    }

    public Long getProductType() {
        return productType;
    }

    public void setProductType(Long productType) {
        this.productType = productType;
    }

    public void reset() {
        this.planningHorizon = null;
        this.confirmedPurchaseOnly = false;
        this.openPurchaseOnly = false;
        this.displayVacant = false;
        this.productGroup = null;
    }
}
