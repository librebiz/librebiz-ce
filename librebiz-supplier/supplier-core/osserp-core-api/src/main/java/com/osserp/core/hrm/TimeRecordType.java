/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 21, 2007 12:11:53 PM 
 * 
 */
package com.osserp.core.hrm;

import com.osserp.common.Option;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface TimeRecordType extends Option {

    static final Long CLOSING = 0L;

    /**
     * Indicates that type is starting a record
     * @return starting
     */
    boolean isStarting();

    /**
     * Indicates that type represents an interval meaning that end time will be booked automatically with booking start
     * @return block
     */
    boolean isInterval();

    /**
     * Enables/disables interval flag
     * @param interval
     */
    void setInterval(boolean interval);

    /**
     * Indicates that interval hours should be taken by config
     * @return intervalHoursByConfig
     */
    boolean isIntervalHoursByConfig();

    /**
     * Sets interval by config param
     * @param intervalHoursByConfig
     */
    void setIntervalHoursByConfig(boolean intervalHoursByConfig);

    /**
     * Provides interval length hours
     * @return intervalHours
     */
    int getIntervalHours();

    /**
     * Sets interval length hous
     * @param intervalHours
     */
    void setIntervalHours(int intervalHours);

    /**
     * Provides interval length minutes
     * @return intervalMinutes
     */
    int getIntervalMinutes();

    /**
     * Sets interval length minutes
     * @param intervalMinutes
     */
    void setIntervalMinutes(int intervalMinutes);

    /**
     * Provides interval start time hours
     * @return intervalStartHours
     */
    int getIntervalStartHours();

    /**
     * Sets interval start time hours
     * @param intervalStartHours
     */
    void setIntervalStartHours(int intervalStartHours);

    /**
     * Provides interval start time minutes
     * @return intervalStartMinutes
     */
    int getIntervalStartMinutes();

    /**
     * Sets the interval start time minutes
     * @param intervalStartMinutes
     */
    void setIntervalStartMinutes(int intervalStartMinutes);

    /**
     * Provides the order id of the type
     * @return orderId
     */
    Integer getOrderId();

    /**
     * Sets the order id of the type
     * @param orderId
     */
    void setOrderId(Integer orderId);

    /**
     * Indicates that type is leave
     * @return leave
     */
    boolean isLeave();

    /**
     * Sets that type is leave
     * @param leave
     */
    void setLeave(boolean leave);

    /**
     * Indicates that type is special leave
     * @return specialLeave
     */
    boolean isSpecialLeave();

    /**
     * Sets that type is special leave
     * @param specialLeave
     */
    void setSpecialLeave(boolean specialLeave);

    /**
     * Indicates that type is unpaid leave
     * @return unpaidLeave
     */
    boolean isUnpaidLeave();

    /**
     * Sets that type is unpaid leave
     * @param unpaidLeave
     */
    void setUnpaidLeave(boolean unpaidLeave);

    /**
     * Indicates that type is illness
     * @return illness
     */
    boolean isIllness();

    /**
     * Sets type as illness
     * @param illness
     */
    void setIllness(boolean illness);

    /**
     * Indicates that type is school
     * @return school
     */
    boolean isSchool();

    /**
     * Enables/disables school param
     * @param school
     */
    void setSchool(boolean school);

    /**
     * Indicates that bookings require manual input
     * @return manualInputRequired
     */
    boolean isManualInputRequired();

    /**
     * Enables/disables manual input required flag
     * @param manualInputRequired
     */
    void setManualInputRequired(boolean manualInputRequired);

    /**
     * Indicates that bookings with type ignore break rules
     * @return ignoreBreakRules
     */
    boolean isIgnoreBreakRules();

    /**
     * Enables/disables ignore break rules flag
     * @param ignoreBreakRules
     */
    void setIgnoreBreakRules(boolean ignoreBreakRules);

    /**
     * Indicates that bookings with type ignore name on printing
     * @return ignoreNameOnPrint
     */
    boolean isIgnoreNameOnPrint();

    /**
     * Enables/disables ignore name on print flag
     * @param ignoreNameOnPrint
     */
    void setIgnoreNameOnPrint(boolean ignoreNameOnPrint);

    /**
     * Indicates that type is default type
     * @return defaultType
     */
    boolean isDefaultType();

    /**
     * Indicates that it is possible to book this type in future
     * @return bookingFuture
     */
    boolean isBookingFuture();

    /**
     * Enables/disables booking of this type in future
     * @param bookingFuture
     */
    void setBookingFuture(boolean bookingFuture);

    /**
     * Indicates that it is possible to book this type in past
     * @return bookingPast
     */
    boolean isBookingPast();

    /**
     * Enables/disables booking of this type in past
     * @param bookingPast
     */
    void setBookingPast(boolean bookingPast);

    /**
     * Indicates that it is possible to book this interval over several days
     * @return intervalPeriod
     */
    boolean isIntervalPeriod();

    /**
     * Enables/disables booking of interval over several days
     * @param intervalPeriod
     */
    void setIntervalPeriod(boolean intervalPeriod);

    /**
     * Indicates that this booking is a subtraction
     * @return subtraction
     */
    boolean isSubtraction();

    /**
     * Sets type as subtraction
     * @param subtraction
     */
    void setSubtraction(boolean subtraction);

    /**
     * Indicates that this booking is visible
     * @return visible
     */
    boolean isVisible();

    /**
     * Sets bookings of this type as visible
     * @param visible
     */
    void setVisible(boolean visible);

    /**
     * Indicates that this booking is bookable
     * @return bookable
     */
    boolean isBookable();

    /**
     * Sets bookings of this type as bookable
     * @param bookable
     */
    void setBookable(boolean bookable);

    /**
     * Indicates that this booking is a addition
     * @return addition
     */
    boolean isAddition();

    /**
     * Sets type as addition
     * @param addition
     */
    void setAddition(boolean addition);

    /**
     * Indicates that this type is a month subtraction
     * @return monthSubtraction
     */
    boolean isMonthSubtraction();

    /**
     * Sets type as month subtraction
     * @param monthSubtraction
     */
    void setMonthSubtraction(boolean monthSubtraction);

    /**
     * Indicates that this type is a day subtraction
     * @return daySubtraction
     */
    boolean isDaySubtraction();

    /**
     * Sets type as day subtraction
     * @param daySubtraction
     */
    void setDaySubtraction(boolean daySubtraction);

    /**
     * Provides the permissions to booking this type
     * @return permissions
     */
    String getPermissions();

    /**
     * Sets the permissions needed to booking this type
     * @param permissions
     */
    void setPermissions(String permissions);

    /**
     * Indicates that this type need approval
     * @return needApproval
     */
    boolean isNeedApproval();

    /**
     * Sets type as need approval
     * @param needApproval
     */
    void setNeedApproval(boolean needApproval);

}
