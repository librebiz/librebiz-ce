<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="changeUserData"/> - <o:out value="${view.bean.values['uid']}"/>
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">
            <v:form name="dynamicForm" url="${view.baseLink}/updateProperty">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <select name="selectAction" size="1" class="form-control" onChange="gotoUrl(this.value);">
                                <c:forEach var="obj" items="${view.bean.allAttributeNames}">
                                    <c:choose>
                                        <c:when test="${obj == view.env.propertyName}">
                                            <option value="<v:url value="${view.baseLink}/selectProperty?name=${obj}"/>" selected="selected">${obj}</option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="<v:url value="${view.baseLink}/selectProperty?name=${obj}"/>">${obj}</option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <span>
                                <c:if test="${!empty view.env.propertyName}">
                                    <o:out value="${view.bean.values[view.env.propertyName]}"/>
                                </c:if>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text" name="name" class="form-control" value="${view.env.propertyName}"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text" name="value" class="form-control" value="${view.bean.values[view.env.propertyName]}"/>
                        </div>
                    </div>
                </div>

                <div class="row next">
                    <div class="col-md-6"></div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <v:submitExit url="${view.baseLink}/disableEditMode" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <o:submit styleClass="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </v:form>
        </div>
    </div>
</div>
