/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 31, 2005 5:44:53 PM 
 * 
 */
package com.osserp.common.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jfree.chart.JFreeChart;

/**
 * 
 * @author <a href="mailto:raymond.zeng@hotmail.com">rz</a>
 * 
 */
public abstract class JFreeChartsOutputter {
    private static Logger log = LoggerFactory.getLogger(JFreeChartsOutputter.class.getName());

    private HttpServletRequest request = null;
    private int width;
    private int height;

    public JFreeChartsOutputter(HttpServletRequest httpRequest) {
        if (log.isDebugEnabled()) {
            log.debug("JFreeChartsOutputter(HttpServletRequest) invoked");
        }
        request = httpRequest;
        width = RequestUtil.getInt(httpRequest, "width");
        height = RequestUtil.getInt(httpRequest, "height");
    }

    public JFreeChartsOutputter(HttpServletRequest request, int width, int height) {
        if (log.isDebugEnabled()) {
            log.debug("JFreeChartsOutputter(HttpServletRequest, int, int) invoked");
        }
        this.request = request;
        this.width = width;
        this.height = height;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    protected String getResourceString(String key) {
        return RequestUtil.getResourceString(request, key);
    }

    protected HttpSession getSession() {
        return request.getSession();
    }

    public abstract JFreeChart getChart();
}
