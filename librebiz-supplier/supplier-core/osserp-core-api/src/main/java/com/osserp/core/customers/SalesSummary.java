/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 29.04.2005 14:34:27 
 * 
 */
package com.osserp.core.customers;

import java.util.Date;

import com.osserp.common.Option;

import com.osserp.core.BusinessType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SalesSummary extends Option {

    /**
     * Provides the type
     * @return type
     */
    BusinessType getType();

    /**
     * The id of the sales person
     * @return salesId.
     */
    Long getSalesId();

    /**
     * The id of the project manager
     * @return managerId.
     */
    Long getManagerId();

    /**
     * Provides the status in case of project and last action for plans
     * @return status.
     */
    Integer getStatus();

    /**
     * The last action of the project
     * @return lastAction
     */
    String getLastAction();

    /**
     * The date project or plan was created
     * @return created.
     */
    Date getCreated();

    /**
     * Indicates that the project is canceled
     * @return cancelled
     */
    boolean isCancelled();

    /**
     * Indicates stopped businessCase
     * @return true if stopped
     */
    boolean isStopped();

    /**
     * Indicates that the project is closed
     * @return closed
     */
    boolean isClosed();
}
