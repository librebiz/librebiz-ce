/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Dec 13, 2010 1:17:59 PM 
 * 
 */
package com.osserp.core.service.telephone;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.util.DateUtil;
import com.osserp.core.dao.telephone.TelephoneGroups;
import com.osserp.core.model.telephone.TelephoneGroupImpl;
import com.osserp.core.service.impl.AbstractService;
import com.osserp.core.telephone.TelephoneGroup;
import com.osserp.core.telephone.TelephoneGroupManager;
import com.osserp.core.telephone.TelephoneSystemManager;
import com.osserp.core.tasks.SfaGroupSender;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public class TelephoneGroupManagerImpl extends AbstractService implements TelephoneGroupManager {
    private static Logger log = LoggerFactory.getLogger(TelephoneGroupManagerImpl.class.getName());

    private TelephoneGroups telephoneGroupsDao = null;
    private TelephoneSystemManager telephoneSystemManager = null;
    private SfaGroupSender sfaGroupSender = null;

    public TelephoneGroupManagerImpl(
            TelephoneGroups telephoneGroupsDao,
            TelephoneSystemManager telephoneSystemManager,
            SfaGroupSender sfaGroupSender) {
        super();
        this.telephoneGroupsDao = telephoneGroupsDao;
        this.telephoneSystemManager = telephoneSystemManager;
        this.sfaGroupSender = sfaGroupSender;
    }

    public TelephoneGroup create(Long createdBy, String name, Long telephoneSystemId) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("create() invoked ...");
        }
        this.check(name, null, telephoneSystemId);
        TelephoneGroup group = new TelephoneGroupImpl(name, createdBy, telephoneSystemManager.find(telephoneSystemId));
        group = telephoneGroupsDao.create(group);
        this.sfaGroupSender.send(group.getId());
        return group;
    }

    public void updateName(Long changedBy, Long id, String name) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("updateName() invoked ...");
        }
        TelephoneGroup group = (id != null) ? telephoneGroupsDao.find(id) : null;
        if (group != null) {
            this.check(name, group.getId(), group.getTelephoneSystem().getId());
            group.setName(name);
            group.setChangedBy(changedBy);
            group.setChanged(DateUtil.getCurrentDate());
            this.telephoneGroupsDao.save(group);
            this.sfaGroupSender.send(group.getId());
        }
    }

    public List<TelephoneGroup> getAll() {
        if (log.isDebugEnabled()) {
            log.debug("getAll() invoked ...");
        }
        return telephoneGroupsDao.getAll();
    }

    public TelephoneGroup find(Long id) {
        if (log.isDebugEnabled()) {
            log.debug("find() invoked ...");
        }
        if (id != null) {
            return telephoneGroupsDao.find(id);
        }
        return null;
    }

    public List<TelephoneGroup> findByTelephoneSystem(Long id) {
        if (log.isDebugEnabled()) {
            log.debug("findByTelephoneSystem() invoked ...");
        }
        if (id != null) {
            return telephoneGroupsDao.findByTelephoneSystem(id);
        }
        return null;
    }

    public void delete(Long groupId, Long changedBy) {
        if (log.isDebugEnabled()) {
            log.debug("delete() invoked ...");
        }
        TelephoneGroup group = telephoneGroupsDao.find(groupId);
        if (group != null) {
            group.setEndOfLife(true);
            group.setChangedBy(changedBy);
            this.telephoneGroupsDao.save(group);
            this.sfaGroupSender.send(groupId);
        }
    }

    private void check(String name, Long groupId, Long telephoneSystemId) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("check() invoked ...");
        }
        if (name == null) {
            throw new ClientException(ErrorCode.NAME_INVALID);
        } else if (telephoneSystemId == null || telephoneSystemManager.find(telephoneSystemId) == null) {
            throw new ClientException(ErrorCode.NO_TELEPHONE_SYSTEM_SET);
        } else {
            TelephoneGroup tg = telephoneGroupsDao.find(name, telephoneSystemId);
            if ((tg != null && groupId == null) || (tg != null && groupId != null && !groupId.equals(tg.getId()))) {
                throw new ClientException(ErrorCode.NAME_EXISTS);
            }
        }
    }
}
