/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 12-Jul-2006 
 * 
 */
package com.osserp.core.model;

import com.osserp.common.Constants;
import com.osserp.common.beans.OptionImpl;
import com.osserp.core.finance.PaymentCondition;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PaymentConditionImpl extends OptionImpl implements PaymentCondition {

    private Integer paymentTarget;
    private boolean donation = false;
    private boolean paid = false;
    private boolean withDiscount = false;
    private Double discount1;
    private Integer discount1Target;
    private Double discount2;
    private Integer discount2Target;
    private String language = null;
    private boolean defaultCondition = false;

    protected PaymentConditionImpl() {
        super();
    }

    public PaymentConditionImpl(
            String name,
            Integer paymentTarget,
            boolean donation,
            boolean paid,
            boolean withDiscount,
            Double discount1,
            Integer discount1Target,
            Double discount2,
            Integer discount2Target) {

        super(null, name);
        this.paymentTarget = paymentTarget;
        this.donation = donation;
        this.paid = paid;
        this.withDiscount = withDiscount;
        this.discount1 = discount1;
        this.discount1Target = discount1Target;
        this.discount2 = discount2;
        this.discount2Target = discount2Target;
        this.defaultCondition = false;
        this.language = Constants.DEFAULT_LANGUAGE;
    }

    public PaymentConditionImpl(PaymentCondition otherValue) {
        super(otherValue);
        this.paymentTarget = otherValue.getPaymentTarget();
        this.donation = otherValue.isDonation();
        this.paid = otherValue.isPaid();
        this.withDiscount = otherValue.isWithDiscount();
        this.discount1 = otherValue.getDiscount1();
        this.discount1Target = otherValue.getDiscount1Target();
        this.discount1 = otherValue.getDiscount2();
        this.discount1Target = otherValue.getDiscount2Target();
        this.defaultCondition = otherValue.isDefaultCondition();
        this.language = otherValue.getLanguage();
    }

    public Integer getPaymentTarget() {
        return paymentTarget;
    }

    public void setPaymentTarget(Integer paymentTarget) {
        this.paymentTarget = paymentTarget;
    }

    public boolean isDonation() {
        return donation;
    }

    public void setDonation(boolean donation) {
        this.donation = donation;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public boolean isWithDiscount() {
        return withDiscount;
    }

    public void setWithDiscount(boolean withDiscount) {
        this.withDiscount = withDiscount;
    }

    public Double getDiscount1() {
        return discount1;
    }

    public void setDiscount1(Double discount1) {
        this.discount1 = discount1;
    }

    public Integer getDiscount1Target() {
        return discount1Target;
    }

    public void setDiscount1Target(Integer discount1Target) {
        this.discount1Target = discount1Target;
    }

    public Double getDiscount2() {
        return discount2;
    }

    public void setDiscount2(Double discount2) {
        this.discount2 = discount2;
    }

    public Integer getDiscount2Target() {
        return discount2Target;
    }

    public void setDiscount2Target(Integer discount2Target) {
        this.discount2Target = discount2Target;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public boolean isDefaultCondition() {
        return defaultCondition;
    }

    public void setDefaultCondition(boolean defaultCondition) {
        this.defaultCondition = defaultCondition;
    }

}
