/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 27, 2007 08:11:15 PM 
 * 
 */
package com.osserp.core.hrm;

import com.osserp.common.Option;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface TimeRecordingConfig extends Option {

    /**
     * Indicates that config is eol
     * @return eol
     */
    boolean isEol();

    /**
     * Enables/disables eol flag
     * @param eol
     */
    void setEol(boolean eol);

    /**
     * Indicates that config is default
     * @return defaultConfig
     */
    boolean isDefaultConfig();

    /**
     * Provides the annual leave in days
     * @return annualLeave
     */
    double getAnnualLeave();

    /**
     * Sets annual leave days
     * @param annualLeave
     */
    void setAnnualLeave(double annualLeave);

    /**
     * Provides the working days per week (5=Mon-Friday, 6=Mon-Sat)
     * @return daysPerWeek
     */
    double getDaysPerWeek();

    /**
     * Sets the count of working days per week
     * @param daysPerWeek
     */
    void setDaysPerWeek(double daysPerWeek);

    /**
     * Indicates that recording periods of this type support hour based recordings
     * @return recordingHours
     */
    boolean isRecordingHours();

    /**
     * Enables/disables recording hours flag
     * @param recordingHours
     */
    void setRecordingHours(boolean recordingHours);

    /**
     * Provides the daily hours to work
     * @return hoursDaily
     */
    double getHoursDaily();

    /**
     * Sets daily hours to work
     * @param hoursDaily
     */
    void setHoursDaily(double hoursDaily);

    /**
     * Days per week (unused at the time of this writing)
     * @return daysPerWeek
     */
    double getHoursPerWeek();

    /**
     * Provides the daily minutes to work
     * @return dailyMinutes
     */
    int getDailyMinutes();

    /**
     * Provides start of the core working time (24 hours format!)
     * @return coreTimeStartHours
     */
    int getCoreTimeStartHours();

    /**
     * Sets the core time start
     * @param coreTimeStartHours
     */
    void setCoreTimeStartHours(int coreTimeStartHours);

    /**
     * Provides start of the core working time (24 hours format!)
     * @return coreTimeStartMinutes
     */
    int getCoreTimeStartMinutes();

    /**
     * Sets the core time start
     * @param coreTimeStartMinutes
     */
    void setCoreTimeStartMinutes(int coreTimeStartMinutes);

    /**
     * Provides end of the core working time (24 hours format!)
     * @return coreTimeEndHours
     */
    int getCoreTimeEndHours();

    /**
     * Sets the core time end
     * @param coreTimeEndHours
     */
    void setCoreTimeEndHours(int coreTimeEndHours);

    /**
     * Provides end of the core working time (24 hours format!)
     * @return coreTimeEndMinutes
     */
    int getCoreTimeEndMinutes();

    /**
     * Sets the core time end
     * @param coreTimeEndMinutes
     */
    void setCoreTimeEndMinutes(int coreTimeEndMinutes);

    /**
     * Provides the default carryover hours maximum
     * @return carryoverHoursMax
     */
    double getCarryoverHoursMax();

    /**
     * Sets the default carryover hours maximum
     * @param carryoverHoursMax
     */
    void setCarryoverHoursMax(double carryoverHoursMax);

    /**
     * Provides the default carryover maximum in minutes
     * @return carryoverHoursMaxMinutes
     */
    int getCarryoverHoursMaxMinutes();

    /**
     * Provides core time start as formatted string
     * @return coreTimeStartDisplay
     */
    String getCoreTimeStartDisplay();

    /**
     * Provides core time end as formatted string
     * @return coreTimeEndDisplay
     */
    String getCoreTimeEndDisplay();
}
