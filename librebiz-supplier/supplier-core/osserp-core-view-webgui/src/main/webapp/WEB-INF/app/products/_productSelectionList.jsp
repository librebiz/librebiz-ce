<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="productSelectionListView"/>
<c:set var="exitTarget" value="${view.env.strutsExit}"/>

<div class="row row-modal">
    <div class="col-md-6 panel-area panel-area-modal">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>
                    <fmt:message key="${view.headerName}"/>
                    <c:if test="${!empty view.selectionConfig}"> [<o:out value="${view.selectionConfig.name}"/>]</c:if>
                </h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="table-responsive table-responsive-default">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th><v:ajaxLink url="${view.baseLink}/sort?key=productId" targetElement="${view.name}_popup"><fmt:message key="product"/></v:ajaxLink></th>
                            <th><v:ajaxLink url="${view.baseLink}/sort?key=name" targetElement="${view.name}_popup"><fmt:message key="name"/></v:ajaxLink></th>
                            <th><v:ajaxLink url="${view.baseLink}/sort?key=summary.ordered" targetElement="${view.name}_popup"><fmt:message key="order"/></v:ajaxLink></th>
                            <th><v:ajaxLink url="${view.baseLink}/sort?key=summary.stock" targetElement="${view.name}_popup"><fmt:message key="inventory"/></v:ajaxLink></th>
                            <th><v:ajaxLink url="${view.baseLink}/sort?key=summary.receipt" targetElement="${view.name}_popup"><fmt:message key="stockReceiptShort"/></v:ajaxLink></th>
                            <th><v:ajaxLink url="${view.baseLink}/sort?key=summary.expected" targetElement="${view.name}_popup"><fmt:message key="purchaseOrderShort"/></v:ajaxLink></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:choose>
                            <c:when test="${empty view.list}">
                                <tr>
                                    <td colspan="6" class="center"><fmt:message key="noProductsFound"/>.</td>
                                </tr>
                            </c:when>
                            <c:otherwise>
                                <c:forEach var="product" items="${view.list}">
                                    <c:if test="${!view.ignoreLazy or !product.lazy}">
                                        <c:set var="warning" value="${product.summary.purchaseOrdered}"/>
                                        <tr<c:if test="${!warning}"> class="altrow"</c:if>>
                                            <td><v:link url="/products.do" parameters="method=load&id=${product.productId}&exit=${exitTarget}"><o:out value="${product.productId}"/></v:link></td>
                                            <c:choose>
                                                <c:when test="${warning}">
                                                    <td class="error"><o:out value="${product.name}"/></td>
                                                </c:when>
                                                <c:otherwise>
                                                    <td><o:out value="${product.name}"/></td>
                                                </c:otherwise>
                                            </c:choose>
                                            <c:choose>
                                                <c:when test="${product.summary.ordered > 0}">
                                                    <td class="right"><v:link url="/salesPlanning.do" parameters="method=forward&id=${product.productId}&stockId=${product.currentStock}&unreleased=false&exit=${exitTarget}"><o:number value="${product.summary.ordered}" format="integer"/></v:link></td>
                                                </c:when>
                                                <c:otherwise>
                                                    <td class="right"><o:number value="${product.summary.ordered}" format="integer"/></td>
                                                </c:otherwise>
                                            </c:choose>
                                            <td class="right"><v:link url="/products.do" parameters="method=enableStockReport&id=${product.productId}&exit=${exitTarget}"><o:number value="${product.summary.stock}" format="integer"/></v:link></td>
                                            <td class="right"><o:number value="${product.summary.receipt}" format="integer"/></td>
                                            <c:choose>
                                                <c:when test="${product.summary.expected > 0}">
                                                    <td class="right"><v:link url="/purchaseOrder.do" parameters="method=forwardOpenByProduct&id=${product.productId}&exit=${exitTarget}"><o:number value="${product.summary.expected}" format="integer"/></v:link></td>
                                                </c:when>
                                                <c:otherwise>
                                                    <td class="right"><o:number value="${product.summary.expected}" format="integer"/></td>
                                                </c:otherwise>
                                            </c:choose>
                                        </tr>
                                    </c:if>
                                </c:forEach>
                            </c:otherwise>
                        </c:choose>
                    </tbody>
                </table>
            </div>
                
        </div>
    </div>
</div>
