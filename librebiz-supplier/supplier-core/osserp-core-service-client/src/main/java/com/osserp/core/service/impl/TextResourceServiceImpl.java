/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 2, 2007 
 * 
 */
package com.osserp.core.service.impl;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.service.ResourceLocator;
import com.osserp.common.service.impl.ResourceLocatorImpl;

import com.osserp.core.dao.ResourceProvider;
import com.osserp.core.system.SystemI18n;
import com.osserp.core.system.SystemI18nConfig;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class TextResourceServiceImpl extends ResourceLocatorImpl implements ResourceLocator {
    private static Logger log = LoggerFactory.getLogger(TextResourceServiceImpl.class.getName());
    private ResourceProvider resourceProvider;

    /**
     * Constructor using property files application and application-custom 
     * from com/osserp/common/resources as defined in AbstractResourceLocator
     * along with resources provided by resource provider
     * @param resourceProvider
     */
    protected TextResourceServiceImpl(ResourceProvider resourceProvider) {
        super();
        this.resourceProvider = resourceProvider;
    }

    /**
     * Constructor ignoring property files application and application-custom 
     * using only property files as provided by appResourcesFiles instead along
     * with resources provided by resource provider
     * @param textResourcesFiles Comma separated list with resource files including path
     * @param resourceProvider
     */
    protected TextResourceServiceImpl(String textResourcesFiles, ResourceProvider resourceProvider) {
        super(textResourcesFiles);
        this.resourceProvider = resourceProvider;
    }

    /**
     * Loads default or user defined resource files and adds all properties provided 
     * by Constructor ignoring property files application and application-custom 
     * using only property files as provided by appResourcesFiles instead along
     * with resources provided by resource provider
     * @param locale
     */
    @Override
    protected InputStream getResourceStream(Locale locale) {
        String resourceString = getResourcesByLocalPath(locale);
        if (resourceProvider != null) {
            SystemI18nConfig i18nConfig = resourceProvider.loadConfigByLanguage(locale.getLanguage());
            List<SystemI18n> i18ns = resourceProvider.loadResources(i18nConfig.getId());

            StringBuffer buffer = new StringBuffer(resourceString);
            for (int i = 0, j = i18ns.size(); i < j; i++) {
                SystemI18n i18n = i18ns.get(i);
                buffer.append("\n").append(i18n.getName()).append("=").append(i18n.getText());
            }
            buffer.append("\n");
            if (log.isDebugEnabled()) {
                log.debug("getResourceStream() done. Added " + i18ns.size() + " properties");
            }
            resourceString = buffer.toString();
        }
        return new ByteArrayInputStream(resourceString.getBytes());
    }
}
