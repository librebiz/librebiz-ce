/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 4, 2014 
 * 
 */
package com.osserp.common.ods

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ds.ImportException
import com.osserp.common.ds.ImportFile

/**
 * 
 * Provides an importer instance using a dedicated service to import a row.  
 *  
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class OdsImporterService extends OdsImporter {
    private static Logger logger = LoggerFactory.getLogger(OdsImporterService.class.getName())
    
    String filename
    ImportFile file
    
    StringBuilder errorBuffer = null
    private String _errorReport = null
    
    boolean performActions

    /**
     * Creates a new importer implementation.
     * @param sheetProvidesHeader
     */
    OdsImporterService(Boolean sheetProvidesHeader) throws ImportException {
        super(sheetProvidesHeader)
    }
    
    void init(String file) {
        filename = file
    }
    
    void init(String filename, boolean performActions) {
        init(filename)
        this.performActions = performActions
    }
    
    public String getExceptionReport() {
        errorBuffer?.toString()
    }

    /**
     * Analyzes imported row and creates the final value map to run importRow with.
     * This method may create or initialize dependent objects, remove invalid row, etc.
     * The default implementation does nothing and returns the row as provided.
     * Override to implement required tasks needed by your implementation.
     * @param row
     * @return final map of row to invoke importRow with
     */
    protected Map<String, String> initRow(Map<String, String> row) {
        row
    }
    
    /**
     * Imports a mapped row to the database, Override to implement your logic.
     * @param row
     * @return true if import row was successful
     */
    abstract protected boolean importRow(Map<String, String> row);
    
    /**
     * Imports a sheet from file and invokes importRow on each row map.
     * @return count of rows for whose importRow returns true
     */
    Integer run() throws ImportException {
        Integer result = 0
        int ires = 0
        while (ires >= 0) {
            ires = importNext()
            if (ires > 0) {
                result++
            }
        }
        file = null
        result
    }
    
    protected int importNext() {
        if (!file) {
            long start = System.currentTimeMillis()
            file = createImportFile(createFile(filename))
            logger.debug("importNext: import file created [duration=${(System.currentTimeMillis() - start)}ms]")
        }
        if (!file) {
            // invalid file
            return -1
        } 
        Map<String, String> row = file.getNextMap()
        if (!row) {
            // eof
            return -1
        }
        row = initRow(row)
        if (row && importRow(row)) {
            return 1
        }
        return 0
    }
}
