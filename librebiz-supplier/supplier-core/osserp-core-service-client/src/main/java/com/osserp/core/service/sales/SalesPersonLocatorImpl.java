/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 20, 2011 11:38:42 AM 
 * 
 */
package com.osserp.core.service.sales;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;

import com.osserp.core.BusinessCaseSearch;
import com.osserp.core.customers.CustomerSummary;
import com.osserp.core.customers.SalesSummary;
import com.osserp.core.dao.Employees;
import com.osserp.core.dao.Projects;
import com.osserp.core.dao.SalesRegions;
import com.osserp.core.employees.Employee;
import com.osserp.core.requests.Request;
import com.osserp.core.sales.Sales;
import com.osserp.core.sales.SalesPersonLocator;
import com.osserp.core.sales.SalesPersonLocatorResult;
import com.osserp.core.sales.SalesRegion;
import com.osserp.core.sales.SalesRegionConfig;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 */
public class SalesPersonLocatorImpl implements SalesPersonLocator {
    private static Logger log = LoggerFactory.getLogger(SalesPersonLocatorImpl.class.getName());
    private static final String DONT_SEND = "none";
    private static final String SALES = "sales";
    private static final String SALES_EXECUTIVE = "salesExecutive";
    private static final String SALES_EXECUTIVE_DEFAULT = "salesExecutiveDefault";
    private Employees employees;
    private Projects projects;
    private BusinessCaseSearch businessCaseSearch;
    private SalesRegions salesRegions;
    private Map<String, String[]> recipientMap = new HashMap<String, String[]>();
    private Set<String> recipientsByExistingSales = new HashSet<String>();

    class SummaryResult {
        int activeSalesPersonCount = 0;
        Employee relatedSales;
    }

    protected SalesPersonLocatorImpl(
            Employees employees,
            Projects projects,
            BusinessCaseSearch businessCaseSearch,
            SalesRegions salesRegions) {
        super();
        this.employees = employees;
        this.projects = projects;
        this.businessCaseSearch = businessCaseSearch;
        this.salesRegions = salesRegions;
        this.recipientMap.put(EXISTING_SALES_OF_ACTIVE_EMPLOYEE, new String[] { SALES });
        this.recipientMap.put(EXISTING_SALES_OF_ACTIVE_EMPLOYEE_VIA_AGENT, new String[] { SALES });
        this.recipientMap.put(EXISTING_SALES_OF_ACTIVE_EMPLOYEE_VIA_AGENT_AND_TIP, new String[] { SALES });
        this.recipientMap.put(EXISTING_SALES_OF_ACTIVE_EMPLOYEE_VIA_TIP, new String[] { SALES });
        this.recipientMap.put(EXISTING_SALES_OF_ACTIVE_EMPLOYEES, new String[] { SALES_EXECUTIVE, SALES_EXECUTIVE_DEFAULT });
        this.recipientMap.put(EXISTING_SALES_OF_ACTIVE_EMPLOYEES_VIA_AGENT, new String[] { SALES_EXECUTIVE, SALES_EXECUTIVE_DEFAULT });
        this.recipientMap.put(EXISTING_SALES_OF_ACTIVE_EMPLOYEES_VIA_AGENT_AND_TIP, new String[] { SALES_EXECUTIVE, SALES_EXECUTIVE_DEFAULT });
        this.recipientMap.put(EXISTING_SALES_OF_ACTIVE_EMPLOYEES_VIA_TIP, new String[] { SALES_EXECUTIVE, SALES_EXECUTIVE_DEFAULT });
        this.recipientMap.put(REQUEST_TYPE_WITH_MANUAL_SELECTION, new String[] { DONT_SEND });
        this.recipientMap.put(REQUEST_WITH_DEACTIVATED_SALES_REGION, new String[] { SALES_EXECUTIVE_DEFAULT });
        this.recipientMap.put(REQUEST_WITH_SALES_REGION_WITHOUT_SALES, new String[] { SALES_EXECUTIVE, SALES_EXECUTIVE_DEFAULT });
        this.recipientMap.put(REQUEST_WITH_SALES_REGION_OF_DEACTIVATED_SALES, new String[] { SALES_EXECUTIVE, SALES_EXECUTIVE_DEFAULT });
        this.recipientMap.put(REQUEST_WITH_SALES_REGION_SETTING_SALES, new String[] { SALES });
        this.recipientMap.put(REQUEST_WITHOUT_SALES_REGION_ASSIGNED, new String[] { SALES_EXECUTIVE_DEFAULT });
        this.recipientMap.put(SALES_PERSON_WITHOUT_SALES_REGION, new String[] { SALES_EXECUTIVE, SALES_EXECUTIVE_DEFAULT });

        this.recipientsByExistingSales.add(EXISTING_SALES_OF_ACTIVE_EMPLOYEES);
        this.recipientsByExistingSales.add(EXISTING_SALES_OF_ACTIVE_EMPLOYEES_VIA_AGENT);
        this.recipientsByExistingSales.add(EXISTING_SALES_OF_ACTIVE_EMPLOYEES_VIA_AGENT_AND_TIP);
        this.recipientsByExistingSales.add(EXISTING_SALES_OF_ACTIVE_EMPLOYEES_VIA_TIP);

    }

    public SalesPersonLocatorResult find(DomainUser user, Request request) {

        boolean regionAssigned = salesRegions.isRegionAssigned(user.getEmployee());
        CustomerSummary customerSummary = businessCaseSearch.getCustomerSummary(request.getCustomer().getId());

        if (log.isDebugEnabled()) {
            log.debug("find() invoked [user=" + user.getId()
                    + ", employee=" + user.getEmployee().getId()
                    + ", customer=" + customerSummary.getId()
                    + ", existingOrders=" + customerSummary.getSalesCount()
                    + ", sales=" + user.isSales()
                    + ", salesWithRegion=" + regionAssigned
                    + ", salesOnly=" + user.isSalesOnly()
                    + ", salesExcecutive=" + user.isSalesExecutive()
                    + ", requestType=" + request.getType().getId()
                    + ", salesPersonByCollector=" + request.getType().isSalesPersonByCollector()
                    + "]");
        }

        if (user.isSales()) {

            if (!regionAssigned) {
                if (log.isDebugEnabled()) {
                    log.debug("find() user is sales without region assigned [user=" + user.getId() + "]");
                }
                return new SalesPersonLocatorResult(user.getEmployee(), SalesPersonLocator.SALES_PERSON_WITHOUT_SALES_REGION, false);
            }
            if (!request.getType().isSalesPersonByCollector()) {
                if (log.isDebugEnabled()) {
                    log.debug("find() user is sales, manual sales selection enabled [user=" + user.getId()
                            + ", type=" + request.getType().getId() + "]");
                }
                return new SalesPersonLocatorResult(user.getEmployee(), SalesPersonLocator.REQUEST_TYPE_WITH_MANUAL_SELECTION, false);
            }
        }
        if (request.getType().isSalesPersonByCollector()) {
            if (log.isDebugEnabled()) {
                log.debug("find() user not sales, salesByCollector enabled [user=" + user.getId()
                        + ", type=" + request.getType().getId() + "]");
            }
            return find(user, request, customerSummary);
        }
        if (log.isDebugEnabled()) {
            log.debug("find() user not sales, manual sales selection enabled [user=" + user.getId()
                    + ", type=" + request.getType().getId() + "]");
        }
        return new SalesPersonLocatorResult(user.getEmployee(), SalesPersonLocator.REQUEST_TYPE_WITH_MANUAL_SELECTION, false);
    }

    private SalesPersonLocatorResult find(DomainUser user, Request request, CustomerSummary customerSummary) {
        // fetch config, required if default sales is required
        SalesRegionConfig cfg = salesRegions.getConfig();
        SummaryResult summaryResult = getSummaryResult(customerSummary);
        // one existing order with an activated sales person 
        if (summaryResult.activeSalesPersonCount == 1) {
            return new SalesPersonLocatorResult(summaryResult.relatedSales, SalesPersonLocator.EXISTING_SALES_OF_ACTIVE_EMPLOYEE, true);
        }
        // more than one existing orders with more that one activated sales persons 
        if (summaryResult.activeSalesPersonCount > 1) {
            return createDefault(cfg, user, EXISTING_SALES_OF_ACTIVE_EMPLOYEES);
        }
        // check if sales could be determined by sales agent and/or tip provider 
        SalesPersonLocatorResult resultByAgentAndTip = findByAgentAndTip(user, request, cfg);
        if (resultByAgentAndTip != null) {
            // found agent and/or tip provider with sales of a active employee(s)
            return resultByAgentAndTip;
        }
        // assert that customer has valid adress data before checking for matching region
        if (request.getCustomer() == null
                || request.getCustomer().getAddress() == null
                || request.getCustomer().getAddress().getZipcode() == null) {

            return createDefault(cfg, user, ErrorCode.ZIPCODE_MISSING);
        }

        SalesRegion region = salesRegions.find(request.getCustomer().getAddress().getZipcode());
        if (region != null) {
            if (region.isEndOfLife()) {
                return createDefault(cfg, user, SalesPersonLocator.REQUEST_WITH_DEACTIVATED_SALES_REGION);
            }
            if (region.getSales() != null) {
                if (region.getSales().isActive()) {
                    return new SalesPersonLocatorResult(region.getSales(), SalesPersonLocator.REQUEST_WITH_SALES_REGION_SETTING_SALES, true);
                }
                return createDefault(cfg, user, SalesPersonLocator.REQUEST_WITH_SALES_REGION_OF_DEACTIVATED_SALES);
            }
            return createDefault(cfg, user, SalesPersonLocator.REQUEST_WITH_SALES_REGION_WITHOUT_SALES);
        }
        return createDefault(cfg, user, SalesPersonLocator.REQUEST_WITHOUT_SALES_REGION_ASSIGNED);
    }

    private SalesPersonLocatorResult findByAgentAndTip(DomainUser user, Request request, SalesRegionConfig cfg) {
        if (request.getAgent() != null || request.getTip() != null) {

            CustomerSummary agentSummary = null;
            SummaryResult agentSummaryResult = null;
            CustomerSummary tipSummary = null;
            SummaryResult tipSummaryResult = null;

            if (request.getAgent() != null) {
                agentSummary = businessCaseSearch.getCustomerSummary(request.getAgent().getId());
                agentSummaryResult = getSummaryResult(agentSummary);
            }
            if (request.getTip() != null) {
                tipSummary = businessCaseSearch.getCustomerSummary(request.getTip().getId());
                tipSummaryResult = getSummaryResult(tipSummary);
            }
            if ((agentSummaryResult != null && agentSummaryResult.activeSalesPersonCount > 0) ||
                    (tipSummaryResult != null && tipSummaryResult.activeSalesPersonCount > 0)) {

                // check if both agent and tip are customers with active sales persons 
                if ((agentSummaryResult != null && agentSummaryResult.activeSalesPersonCount > 0) &&
                        (tipSummaryResult != null && tipSummaryResult.activeSalesPersonCount > 0)) {

                    if (agentSummaryResult.activeSalesPersonCount == 1 && tipSummaryResult.activeSalesPersonCount == 1
                            && agentSummaryResult.relatedSales.getId().equals(tipSummaryResult.relatedSales.getId())
                            && agentSummaryResult.relatedSales.isActive()) {
                        return new SalesPersonLocatorResult(
                                agentSummaryResult.relatedSales,
                                SalesPersonLocator.EXISTING_SALES_OF_ACTIVE_EMPLOYEE_VIA_AGENT_AND_TIP, true);
                    }
                    return createDefault(cfg, user, EXISTING_SALES_OF_ACTIVE_EMPLOYEES_VIA_AGENT_AND_TIP);
                }
                if (agentSummaryResult != null && agentSummaryResult.activeSalesPersonCount > 0) {
                    if (agentSummaryResult.activeSalesPersonCount == 1 && agentSummaryResult.relatedSales.isActive()) {
                        return new SalesPersonLocatorResult(
                                agentSummaryResult.relatedSales,
                                SalesPersonLocator.EXISTING_SALES_OF_ACTIVE_EMPLOYEE_VIA_AGENT, true);
                    }
                    return createDefault(cfg, user, EXISTING_SALES_OF_ACTIVE_EMPLOYEES_VIA_AGENT);
                }
                if (tipSummaryResult != null && tipSummaryResult.activeSalesPersonCount > 0) {
                    if (tipSummaryResult.activeSalesPersonCount == 1 && tipSummaryResult.relatedSales.isActive()) {
                        return new SalesPersonLocatorResult(
                                tipSummaryResult.relatedSales,
                                SalesPersonLocator.EXISTING_SALES_OF_ACTIVE_EMPLOYEE_VIA_TIP, true);
                    }
                    return createDefault(cfg, user, EXISTING_SALES_OF_ACTIVE_EMPLOYEES_VIA_TIP);
                }
            }
        }
        return null;
    }

    private SummaryResult getSummaryResult(CustomerSummary customerSummary) {
        SummaryResult result = new SummaryResult();
        if (customerSummary.getSalesCount() > 0) {
            int activeSalesPersonCount = 0;
            Map<Long, Employee> activeSalesPersons = new HashMap<Long, Employee>();
            Employee relatedSales = null;
            List<SalesSummary> existingOrders = customerSummary.getSales();
            for (int i = 0, j = existingOrders.size(); i < j; i++) {
                SalesSummary order = existingOrders.get(i);
                if (order.getSalesId() != null) {
                    Employee employee = employees.get(order.getSalesId());
                    if (employee != null && employee.isActive() && !activeSalesPersons.containsKey(employee.getId())) {
                        activeSalesPersons.put(employee.getId(), employee);
                        relatedSales = employee;
                        activeSalesPersonCount++;
                    }
                }
            }
            result.activeSalesPersonCount = activeSalesPersonCount;
            result.relatedSales = relatedSales;
        }
        return result;
    }

    private SalesPersonLocatorResult createDefault(SalesRegionConfig cfg, DomainUser user, String resolution) {
        if (cfg.getDefaultSales() == null) {
            return new SalesPersonLocatorResult(user.getEmployee(), ErrorCode.SALESREGION_CONFIG_INVALID, false);
        }
        return new SalesPersonLocatorResult(cfg.getDefaultSales(), resolution, true);
    }

    public List<Employee> findRecipients(Request request) {
        List<Employee> result = new ArrayList<Employee>();

        if (request.getType().isSalesPersonByCollector() && request.getSalesAssignment() != null) {
            SalesRegionConfig cfg = salesRegions.getConfig();

            if (recipientsByExistingSales.contains(request.getSalesAssignment())) {
                return findRecipientsByExistingSales(cfg, request);
            }
            Set<Long> added = new HashSet<Long>();
            SalesRegion newRegion = findRegion(request);
            String[] recipientTypes = recipientMap.get(request.getSalesAssignment());
            if (recipientTypes != null) {
                for (int i = 0, j = recipientTypes.length; i < j; i++) {
                    String recipientType = recipientTypes[i];
                    if (!DONT_SEND.equals(recipientType)) {
                        if (SALES.equals(recipientType)) {
                            if (!added.contains(request.getSalesId())) {
                                result.add(employees.get(request.getSalesId()));
                                added.add(request.getSalesId());
                            }
                        } else if (SALES_EXECUTIVE.equals(recipientType)) {
                            if (newRegion.getSalesExecutive() != null && !newRegion.getSalesExecutive().getId().equals(Constants.SYSTEM_EMPLOYEE)
                                    && !added.contains(newRegion.getSalesExecutive().getId())) {
                                result.add(newRegion.getSalesExecutive());
                                added.add(newRegion.getSalesExecutive().getId());
                            }
                        } else if (cfg.getDefaultSalesExecutive() != null && !cfg.getDefaultSalesExecutive().getId().equals(Constants.SYSTEM_EMPLOYEE)
                                && !added.contains(cfg.getDefaultSalesExecutive().getId())) {
                            result.add(cfg.getDefaultSalesExecutive());
                            added.add(cfg.getDefaultSalesExecutive().getId());
                        }
                    }
                }
            }
        }
        return result;
    }

    private List<Employee> findRecipientsByExistingSales(SalesRegionConfig cfg, Request request) {
        List<Employee> result = new ArrayList<Employee>();
        Set<Long> added = new HashSet<Long>();
        SalesRegion newRegion = findRegion(request);
        String[] salesTargets = recipientMap.get(request.getSalesAssignment());
        for (int x = 0; x < salesTargets.length; x++) {
            String target = salesTargets[x];
            if (SALES_EXECUTIVE_DEFAULT.equals(target)) {
                if (cfg.getDefaultSalesExecutive() != null && !cfg.getDefaultSalesExecutive().getId().equals(Constants.SYSTEM_EMPLOYEE)
                        && !added.contains(cfg.getDefaultSalesExecutive().getId())) {
                    result.add(cfg.getDefaultSalesExecutive());
                    added.add(cfg.getDefaultSalesExecutive().getId());
                }

            } else if (SALES_EXECUTIVE.equals(target)) {
                CustomerSummary customerSummary = businessCaseSearch.getCustomerSummary(request.getCustomer().getId());
                for (int i = 0, j = customerSummary.getSales().size(); i < j; i++) {
                    SalesSummary salesSummary = customerSummary.getSales().get(i);
                    Sales sales = projects.load(salesSummary.getId());
                    List<Employee> existingSalesResult = findRecipients(newRegion, sales);
                    for (int k = 0, l = existingSalesResult.size(); k < l; k++) {
                        Employee salesPerson = existingSalesResult.get(k);
                        if (!added.contains(salesPerson.getId())) {
                            result.add(salesPerson);
                            added.add(salesPerson.getId());
                        }
                    }
                }
            }
        }
        return result;
    }

    private List<Employee> findRecipients(SalesRegion newRegion, Sales sales) {
        List<Employee> result = new ArrayList<Employee>();
        Set<Long> added = new HashSet<Long>();
        if (newRegion != null) {
            Employee salesPerson = employees.get(sales.getSalesId());
            if (salesPerson.isActive()) {
                SalesRegion region = findRegion(sales.getRequest());
                if (region != null && !region.getId().equals(newRegion.getId())
                        && !region.getSalesExecutive().getId().equals(Constants.SYSTEM_EMPLOYEE)) {

                    result.add(region.getSalesExecutive());
                    added.add(region.getSalesExecutive().getId());
                    if (region.getSalesDelegate() != null) {
                        result.add(region.getSalesDelegate());
                        added.add(region.getSalesDelegate().getId());
                    }
                    if (log.isDebugEnabled()) {
                        log.debug("findRecipients() added sales executive of region with existing sales [request"
                                + newRegion.getId()
                                + ", existingProject=" + sales.getId()
                                + ", region=" + region.getId()
                                + ", salesExecutive=" + region.getSalesExecutive().getId()
                                + ", salesDelegate=" + (region.getSalesDelegate() == null ? "null" : region.getSalesDelegate().getId())
                                + "]");
                    }
                }
            }
            if (sales.getRequest().getAgent() != null) {
                CustomerSummary agentSummary = businessCaseSearch.getCustomerSummary(sales.getRequest().getAgent().getId());
                if (agentSummary.getSalesCount() > 0) {
                    for (int i = 0, j = agentSummary.getSales().size(); i < j; i++) {
                        SalesSummary existingSales = agentSummary.getSales().get(i);
                        salesPerson = employees.get(existingSales.getSalesId());
                        if (salesPerson.isActive()) {
                            Sales agentSales = projects.load(existingSales.getId());
                            SalesRegion agentRegion = findRegion(agentSales.getRequest());
                            if (agentRegion != null && !agentRegion.getId().equals(newRegion.getId())
                                    && !agentRegion.getSalesExecutive().getId().equals(Constants.SYSTEM_EMPLOYEE)) {
                                if (!added.contains(agentRegion.getSalesExecutive().getId())) {
                                    result.add(agentRegion.getSalesExecutive());
                                    added.add(agentRegion.getSalesExecutive().getId());
                                    if (agentRegion.getSalesDelegate() != null && !added.contains(agentRegion.getSalesDelegate().getId())) {
                                        result.add(agentRegion.getSalesDelegate());
                                        added.add(agentRegion.getSalesDelegate().getId());
                                    }
                                    if (log.isDebugEnabled()) {
                                        log.debug("findRecipients() added sales executive of region with existing agent [request"
                                                + newRegion.getId()
                                                + ", existingProject=" + sales.getId()
                                                + ", region=" + agentRegion.getId()
                                                + ", salesExecutive=" + agentRegion.getSalesExecutive().getId()
                                                + ", salesDelegate="
                                                + (agentRegion.getSalesDelegate() == null ? "null" : agentRegion.getSalesDelegate().getId())
                                                + "]");
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (sales.getRequest().getTip() != null) {
                CustomerSummary tipSummary = businessCaseSearch.getCustomerSummary(sales.getRequest().getTip().getId());
                if (tipSummary.getSalesCount() > 0) {
                    for (int i = 0, j = tipSummary.getSales().size(); i < j; i++) {
                        SalesSummary existingSales = tipSummary.getSales().get(i);
                        salesPerson = employees.get(existingSales.getSalesId());
                        if (salesPerson.isActive()) {
                            Sales tipSales = projects.load(existingSales.getId());
                            SalesRegion tipRegion = findRegion(tipSales.getRequest());
                            if (tipRegion != null && !tipRegion.getId().equals(newRegion.getId())
                                    && !tipRegion.getSalesExecutive().getId().equals(Constants.SYSTEM_EMPLOYEE)) {
                                if (!added.contains(tipRegion.getSalesExecutive().getId())) {
                                    result.add(tipRegion.getSalesExecutive());
                                    added.add(tipRegion.getSalesExecutive().getId());
                                    if (tipRegion.getSalesDelegate() != null && !added.contains(tipRegion.getSalesDelegate().getId())) {
                                        result.add(tipRegion.getSalesDelegate());
                                        added.add(tipRegion.getSalesDelegate().getId());
                                    }
                                    if (log.isDebugEnabled()) {
                                        log.debug("findRecipients() added sales executive of region with existing tip [request"
                                                + newRegion.getId()
                                                + ", existingProject=" + sales.getId()
                                                + ", region=" + tipRegion.getId()
                                                + ", salesExecutive=" + tipRegion.getSalesExecutive().getId()
                                                + ", salesDelegate=" + (tipRegion.getSalesDelegate() == null ? "null" : tipRegion.getSalesDelegate().getId())
                                                + "]");
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return result;
    }

    private SalesRegion findRegion(Request request) {
        SalesRegion region = null;
        if (request != null && request.getCustomer() != null
                && request.getCustomer().getAddress() != null
                && request.getCustomer().getAddress().getZipcode() != null) {
            region = salesRegions.find(request.getCustomer().getAddress().getZipcode());
        }
        return region;
    }
}
