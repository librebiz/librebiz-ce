/**
 *
 * Copyright (C) 2022 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License (AGPL)
 * version 3 as published by the Free Software Foundation. In accordance
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code
 * versions must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area
 * or, if not reasonably feasible for technical reason, as a top-level
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute.
 *
 */
package com.osserp.common.web.tags;

import javax.servlet.jsp.JspException;

import com.osserp.common.User;

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public class MapsLinkTag extends AbstractLinkTag {

    private String street;
    private String zip;
    private String city;

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String getStartTag() throws JspException {
        if (isAddressAvailable()) {
            String mapsUrl = TagUtil.createMapsLink(
                    getMapsProvider(),
                    getStreet(),
                    getZip(),
                    getCity());
            StringBuilder buffer = new StringBuilder(LINK_START);
            buffer.append(mapsUrl).append("\"");
            appendEnd(buffer);
            return buffer.toString();
        }
        return null;
    }

    protected boolean isAddressAvailable() {
        return (street != null && street.length() > 0)
                || (zip != null && zip.length() > 0)
                || (city != null && city.length() > 0);
    }

    protected String getMapsProvider() {
        User user = getUser();
        if (user != null && user.getProperties().containsKey("mapsProvider")) {
            return user.getProperties().get("mapsProvider").getAsString();
        }
        return null;
    }

    @Override
    public String getTarget() {
        return "_blank";
    }

    @Override
    public void release() {
        super.release();
        street = null;
        zip = null;
        city = null;
    }
}
