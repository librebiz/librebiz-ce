<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="telephoneView"/>
<c:set var="sortUrl" scope="page" value="/telephones/telephone/sort"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="title"><o:displayTitle/></tiles:put>
	<tiles:put name="javascript" type="string">
		<script type="text/javascript">
			telephoneList = new Object();
			telephoneList.checkSearchException = function() {
				try {
					if ($('ajaxContent')) {
						$('ajaxContent').innerHTML = "<div style='width: 100%; text-align: center;'><div style='text-align:center; border:1px solid #D6DDE6; padding:10px;'><img src='/osdb/img/ajax_loader.gif' style='margin:5px;'/><br/>"+ ojsI18n.loading+"</div></div>";
					}
					return true;
				} catch (e) {
					return true;
				}
			}
		</script>
	</tiles:put>
	<tiles:put name="headline"><fmt:message key="phonelist"/></tiles:put>
	<tiles:put name="headline_right">
		<v:navigation/>
	</tiles:put>
	<tiles:put name="content" type="string">
		<c:if test="${!empty sessionScope.error}">
			&nbsp;
			<div class="errormessage">
				<fmt:message key="error"/>: <fmt:message key="${sessionScope.error}"/>
			</div>
            <o:removeErrors/>
		</c:if>
		<div class="spacer"></div>
		<div class="subcolumns">
			<div class="subcolumn">
				<div class="contentBox">
					<div class="contentBoxData">
						<div class="spacer"></div>
						<o:ajaxLookupForm name="dynamicForm" url="/app/telephones/telephone/search" targetElement="ajaxContent" events="onkeyup" preRequest="if (telephoneList.checkSearchException()) {" postRequest="}" minChars="0">
							<div class="subcolumns">
								<div class="column33l">
									<div class="subcolumnl">
										<input class="text" type="text" name="macAddress" value="${view.macAddress}"/>
										<a href="javascript:if(telephoneList.checkSearchException()) {$('dynamicForm').onkeyup();}">
											<o:img name="replaceIcon" styleClass="bigicon" style="padding: 0px 0px 0px 10px;" />
										</a>
									</div>
								</div>
								<div class="column33l">
									<div class="subcolumnl">
										<select id="system" name="system" style="width: 210px;" onChange="javascript: $('dynamicForm').onkeyup();">
											<option value="" <c:if test="${empty view.telephoneSystem}">selected="selected"</c:if>><fmt:message key="telephoneSystem"/> ...</option>
											<c:forEach var="obj" items="${view.telephoneSystems}">
												<option value="${obj.id}" <c:if test="${obj.id == view.telephoneSystem}">selected="selected"</c:if>><o:out value="${obj.name}"/></option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="column33l">
									<div class="subcolumnl">
										<select id="type" name="type" style="width: 210px;" onChange="javascript: $('dynamicForm').onkeyup();">
											<option value="" <c:if test="${empty view.type}">selected="selected"</c:if>><fmt:message key="type"/> ...</option>
											<c:forEach var="obj" items="${view.types}">
												<option value="${obj.id}" <c:if test="${obj.id == view.type}">selected="selected"</c:if>><o:out value="${obj.name}"/></option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>
						</o:ajaxLookupForm>
						<div class="spacer"></div>
					</div>
				</div>
				<div class="spacer"></div>
				<div id="ajaxDiv">
					<div id="ajaxContent">
						<c:import url="_telephone.jsp"/>
					</div>
				</div>
			</div>
		</div>
		<div class="spacer"></div>
	</tiles:put>
</tiles:insert>
