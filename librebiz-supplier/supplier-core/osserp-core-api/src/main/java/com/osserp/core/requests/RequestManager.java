/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 5, 2005 3:53:05 PM 
 * 
 */
package com.osserp.core.requests;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.User;

import com.osserp.core.Address;
import com.osserp.core.BusinessCaseManager;
import com.osserp.core.BusinessType;
import com.osserp.core.crm.CampaignAssignmentHistory;
import com.osserp.core.customers.Customer;
import com.osserp.core.employees.Employee;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface RequestManager extends BusinessCaseManager {

    /**
     * Provides all available request types
     * @return requestTypes
     */
    List<BusinessType> getRequestTypes();

    /**
     * Provides an empty request of given type for create actions.
     * @param user
     * @param customer
     * @param type
     * @return request object
     */
    Request createEmpty(User user, Customer customer, BusinessType type);

    /**
     * Validates a required address
     * @param address
     * @throws ClientException if validation failed
     */
    void validateAddress(Address address) throws ClientException;

    /**
     * Validates common values. E.g. sales talk and presentation date, origin, etc. depending on requirements of selected request type
     * @param request
     * @throws ClientException if any required value missing or invalid
     */
    void validateCommons(Request request) throws ClientException;

    /**
     * Creates a new request with values from given request value object
     * @param request
     * @return id of the new created request
     */
    Long createRequest(Request request);

    /**
     * Transfers the given request to another request
     * @param user transfering the request
     * @param request
     * @param type
     */
    Request transferRequest(User user, Request request, BusinessType type);

    /**
     * Synchronizes request values with persistent storage
     * @param request
     * @throws ClientException if validation failed
     */
    void update(Request request) throws ClientException;

    /**
     * Updates request origin
     * @param request
     * @param origin
     */
    void updateOrigin(Request request, Employee user, Long origin);

    /**
     * Updates request origin type
     * @param request
     * @param originType
     */
    void updateOriginType(Request request, Employee user, Long originType);

    /**
     * Provides request origin history
     * @param request
     * @return true if origin history entries available
     */
    boolean isOriginHistoryAvailable(Request request);

    /**
     * Provides request origin history
     * @param request
     * @return originHistory
     */
    List<CampaignAssignmentHistory> getOriginHistory(Request request);

    /**
     * Updates request sales co person
     * @param request
     * @param salesCoId
     */
    void updateSalesCo(Request request, Long salesCoId);

    /**
     * Updates request sales co percent
     * @param request
     * @param salesCoPercent
     */
    void updateSalesCoPercent(Request request, Double salesCoPercent);
}
