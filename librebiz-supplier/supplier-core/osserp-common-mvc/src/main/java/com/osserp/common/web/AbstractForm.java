/**
 *
 * Copyright (C) 2009, 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 18, 2009 10:00:00 AM 
 * 
 */
package com.osserp.common.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.util.StringUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractForm implements Form {
    private static Logger log = LoggerFactory.getLogger(AbstractForm.class.getName());

    public void validateSet(Object o) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("validateSet() invokked [o=" + o + "]");
        }
        if (o == null) {
            throw new ClientException(ErrorCode.VALUES_MISSING);
        } else if (o instanceof String) {
            if (((String) o).length() < 1 || StringUtil.isWhitespace((String) o)) {
                throw new ClientException(ErrorCode.VALUES_MISSING);
            }
        } else if (o instanceof Long) {
            if (((Long) o).equals(Constants.LONG_NULL)) {
                throw new ClientException(ErrorCode.VALUES_MISSING);
            }
        } else if (o instanceof Integer) {
            if (((Integer) o).equals(Constants.INT_NULL)) {
                throw new ClientException(ErrorCode.VALUES_MISSING);
            }
        } else if (o instanceof Double) {
            if (((Double) o).equals(Constants.DOUBLE_NULL)) {
                throw new ClientException(ErrorCode.VALUES_MISSING);
            }
        }
    }
    
    public Integer getHours(String varName) throws ClientException {
        try {
            String h = getString(varName);
            Integer result = Integer.valueOf(h);
            if (result > 23) {
                throw new ClientException(ErrorCode.HOURS_INVALID);
            }
            return result;
        } catch (NullPointerException npe) {
        } catch (NumberFormatException nfe) {
            throw new ClientException(ErrorCode.DATE_FORMAT);
        }
        return 0;
    }

    public Integer getMinutes(String varName) throws ClientException {
        try {
            String m = getString(varName);
            Integer result = Integer.valueOf(m);
            if (result > 59) {
                throw new ClientException(ErrorCode.MINUTES_INVALID);
            }
            return result;
        } catch (NullPointerException npe) {
        } catch (NumberFormatException nfe) {
        }
        return 0;
    }
}
