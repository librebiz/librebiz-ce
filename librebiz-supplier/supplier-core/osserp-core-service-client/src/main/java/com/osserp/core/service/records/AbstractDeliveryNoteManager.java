/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 06-Feb-2007 11:04:04 
 * 
 */
package com.osserp.core.service.records;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;

import com.osserp.core.dao.records.DeliveryNotes;
import com.osserp.core.dao.records.Records;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.DeliveryNote;
import com.osserp.core.finance.DeliveryNoteManager;
import com.osserp.core.finance.DeliveryNoteType;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.Record;
import com.osserp.core.dao.Products;
import com.osserp.core.system.SystemConfigManager;
import com.osserp.core.tasks.ProductPlanningCacheSender;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractDeliveryNoteManager extends AbstractRecordManager
        implements DeliveryNoteManager {
    private static Logger log = LoggerFactory.getLogger(AbstractDeliveryNoteManager.class.getName());
    private Records orders = null;

    /**
     * Creates a new deliveryNoteManager
     * @param deliveryNotes
     * @param systemConfigManager
     * @param productPlanningTask
     * @param orders
     */
    protected AbstractDeliveryNoteManager(
            Records deliveryNotes,
            SystemConfigManager systemConfigManager,
            Products products,
            ProductPlanningCacheSender productPlanningTask,
            Records orders) {
        super(deliveryNotes, systemConfigManager, products, productPlanningTask);
        this.orders = orders;
    }

    public void update(
            DeliveryNote record,
            String note,
            String language,
            Date delivery,
            boolean printBusinessId,
            boolean printBusinessInfo,
            boolean printRecordDate,
            boolean printRecordDateByStatus) throws ClientException {
        record.update(
                note,
                language,
                delivery,
                printBusinessId,
                printBusinessInfo,
                printRecordDate,
                printRecordDateByStatus);
        getDao().save(record);
    }

    @Override
    protected void doAfterStatusUpdate(Record record, Employee employee, Long oldStatus) {
        if (log.isDebugEnabled()) {
            log.debug("doAfterStatusUpdate() invoked [record=" + record.getId()
                    + ", oldStatus=" + oldStatus + ", newStatus=" + record.getStatus() + "]");
        }
        if (Record.STAT_SENT.equals(record.getStatus())
                && !Record.STAT_SENT.equals(oldStatus)
                && (record instanceof DeliveryNote)) {

            if (log.isDebugEnabled()) {
                log.debug("doAfterStatusUpdate() delivery note marked as sent," +
                        " updating deliveries");
            }
            reloadSummary((DeliveryNote) record);
        }
    }

    protected void reloadSummary(DeliveryNote note) {
        if (note != null && !DeliveryNoteType.SALES_INVOICE_BOOKING.equals(note.getBookingType().getId())
                && !DeliveryNoteType.SALES_CREDIT_NOTE_BOOKING.equals(note.getBookingType().getId())) {
            super.reloadSummary(note);
            refreshDeliveries(note);
        }
    }

    protected void refreshDeliveries(DeliveryNote note) {
        Order order = (Order) orders.load(note.getReference());
        if (order.getDeliveryNotes().isEmpty()) {
            order.getDeliveryNotes().addAll(getDeliveryNotes(order));
        }
        order.refreshDeliveries();
        orders.save(order);
        if (log.isDebugEnabled()) {
            log.debug("refreshDeliveries() done");
        }
    }

    protected Records getOrders() {
        return orders;
    }

    protected List<DeliveryNote> getDeliveryNotes(Order order) {
        return getDeliveryNotesDao().getByOrder(order);
    }

    protected DeliveryNotes getDeliveryNotesDao() {
        return (DeliveryNotes) getDao();
    }
}
