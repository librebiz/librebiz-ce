/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jul 19, 2012 1:49:12 PM
 * 
 */
package com.osserp.gui.admin.products

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode

import com.osserp.core.products.Product
import com.osserp.core.products.ProductClassificationConfigManager
import com.osserp.core.products.ProductManager

import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class ProductListClassificationConfiguratorView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(ProductListClassificationConfiguratorView.class.getName())

    String action
    String actionStatus
    Long actionId

    ProductListClassificationConfiguratorView() {
        headerName = 'productListClassificationConfiguratorView'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (forwardRequest) {
            String viewName = form.getString('view')
            if (viewName && env[viewName]) {
                def viewobj = env[viewName]
                list = viewobj.productList
                action = viewobj.removalActive
                actionId = viewobj.removalId
                logger.debug("initRequest: view found [listSize=${list.size()}, action=${action}, actionId=${actionId}]")
            }
        }
    }

    void removeProduct() {
        Long id = form.getLong('id')
        if (id) {

            for (Iterator i = list.iterator(); i.hasNext();) {
                def product = i.next()
                if (product.productId == id) {
                    Product obj = productManager.get(id)
                    if (!obj.types?.isEmpty() && obj.types.size() > 1) {
                        i.remove()
                    } else {
                        logger.debug("removeProduct: missing alternate type")
                        throw new ClientException(ErrorCode.ALTERNATIVE_SOLUTION_REQUIRED)
                    }
                    break
                }
            }
        }
    }

    protected ProductClassificationConfigManager getProductClassificationConfigManager() {
        getService(ProductClassificationConfigManager.class.getName())
    }

    protected ProductManager getProductManager() {
        getService(ProductManager.class.getName())
    }
}

