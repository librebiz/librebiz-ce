/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 7, 2006 9:54:07 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;
import org.hibernate.SessionFactory;

import com.osserp.common.Option;
import com.osserp.common.OptionsCache;
import com.osserp.common.Parameter;
import com.osserp.common.dao.Tables;

import com.osserp.core.Options;
import com.osserp.core.contacts.ContactType;
import com.osserp.core.contacts.Salutation;
import com.osserp.core.dao.Emails;
import com.osserp.core.dao.Persons;
import com.osserp.core.dao.TableKeys;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractContactDao extends AbstractRepository implements Persons {

    protected OptionsCache namesCache = null;
    private Map<Long, Salutation> salutations = null;
    protected Emails emails = null;
    protected String contactsTable = null;
    protected String emailTable = null;
    protected String phoneTable = null;

    @SuppressWarnings("unchecked")
    public AbstractContactDao(
            JdbcTemplate jdbcTemplate, 
            Tables tables, 
            SessionFactory sessionFactory,
            Emails emails, 
            OptionsCache namesCache) {
        super(jdbcTemplate, tables, sessionFactory);
        this.namesCache = namesCache;
        this.emails = emails;
        this.contactsTable = getTable(TableKeys.CONTACTS);
        this.emailTable = getTable(TableKeys.EMAILS);
        this.phoneTable = getTable(TableKeys.PHONES);
        this.salutations = (Map) namesCache.getMap(Options.SALUTATIONS);
    }

    public List<Parameter> findMatchingCities(String pattern, boolean startsWith) {
        return findMatchingColumnEntries("city", pattern, startsWith);
    }

    public List<Parameter> findMatchingStreets(String pattern, boolean startsWith) {
        return findMatchingColumnEntries("street", pattern, startsWith);
    }

    public List<Parameter> findMatchingZipcodes(String pattern, boolean startsWith) {
        return findMatchingColumnEntries("zipcode", pattern, startsWith);
    }

    private List<Parameter> findMatchingColumnEntries(String column, String pattern, boolean startsWith) {
        StringBuilder sql = new StringBuilder(96);
        sql
                .append("SELECT ")
                .append(column)
                .append(" FROM ")
                .append(contactsTable)
                .append(" WHERE ")
                .append(column)
                .append(" ~* '");
        if (startsWith) {
            sql.append("^");
        }
        sql
                .append(pattern)
                .append("' ORDER BY ")
                .append(column);
        List qres = (List) jdbcTemplate.query(sql.toString(), getParameterRowMapper());
        Set<String> existing = new HashSet<String>();
        List<Parameter> result = new ArrayList<Parameter>();
        for (int i = 0, j = qres.size(); i < j; i++) {
            Parameter p = (Parameter) qres.get(i);
            if (!existing.contains(p.getName())) {
                result.add(p);
                existing.add(p.getName());
            }
        }
        return result;
    }

    protected final Long getIdByExternalReference(String externalReference) {
        StringBuilder sql = new StringBuilder(96);
        sql
                .append("SELECT contact_id FROM ")
                .append(contactsTable)
                .append(" WHERE external_ref = ?");
        List<Long> result = (List<Long>) jdbcTemplate.query(
                sql.toString(), 
                new Object[] { externalReference },
                new int[] { Types.VARCHAR }, 
                getLongRowMapper());
        return (result.isEmpty() ? null : result.get(0));
    }

    public final Salutation getDefaultSalutation() {
        return getSalutation(Salutation.UNDEFINED);
    }

    public final Salutation getSalutation(Long id) {
        return (id == null || salutations == null) ? null : (Salutation) salutations.get(id);
    }

    public final Salutation findSalutation(String salutationName) {
        if (!isEmpty(salutationName)) {
            List<Salutation> list = (namesCache == null) ? new ArrayList<>() :
                new ArrayList(namesCache.getList(Options.SALUTATIONS));
            for (int i = 0, j = list.size(); i < j; i++) {
                Salutation next = list.get(i);
                if (salutationName.equals(next.getName()) 
                        || salutationName.equalsIgnoreCase(next.getResourceKey())) {
                    return next;
                }
            }
        }
        return null;
    }

    public final Option getTitle(Long id) {
        return (id == null || namesCache == null) ? null : (Option) namesCache.getMapped(Options.TITLES, id);
    }

    public final Option findTitle(String titleName) {
        if (!isEmpty(titleName)) {
            List<Option> titles = (namesCache == null) ? new ArrayList<>() :
                    namesCache.getList(Options.TITLES);
            for (int i = 0, j = titles.size(); i < j; i++) {
                Option next = titles.get(i);
                if (titleName.equals(next.getName())
                        || titleName.equals(next.getResourceKey())) {
                    return next;
                }
            }
        }
        return null;
    }

    /**
     * Provides the contact type by id
     * @param id
     * @return contactType or null if not exists
     */
    protected ContactType getContactType(Long id) {
        Map types = namesCache.getMap(Options.CONTACT_TYPES);
        if (types.containsKey(id)) {
            return (ContactType) types.get(id);
        }
        return null;
    }

    protected String getCountryCode(Long country) {
        if (country != null) {
            Map<Long, Option> countries = namesCache.getMap(Options.COUNTRY_CODES);
            if (countries.containsKey(country)) {
                return countries.get(country).getName();
            }
        }
        return null;
    }

    protected RowMapperResultSetExtractor getContactReader() {
        return new RowMapperResultSetExtractor(new ContactRowMapper(namesCache));
    }
}
