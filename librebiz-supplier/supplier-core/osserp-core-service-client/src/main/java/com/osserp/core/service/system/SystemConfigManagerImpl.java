/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 13, 2007 10:33:50 PM 
 * 
 */
package com.osserp.core.service.system;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionConfig;
import com.osserp.common.Property;
import com.osserp.common.gui.Menu;
import com.osserp.common.util.CollectionUtil;

import com.osserp.core.AddressLocality;
import com.osserp.core.BankAccount;
import com.osserp.core.Comparators;
import com.osserp.core.Plugins;
import com.osserp.core.PluginManager;
import com.osserp.core.contacts.Contact;
import com.osserp.core.dao.BranchOffices;
import com.osserp.core.dao.Districts;
import com.osserp.core.dao.NocClients;
import com.osserp.core.dao.SystemCompanies;
import com.osserp.core.dao.SystemConfigs;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Stock;
import com.osserp.core.service.impl.AbstractService;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.system.BranchSelection;
import com.osserp.core.system.SystemCompany;
import com.osserp.core.system.SystemConfig;
import com.osserp.core.system.SystemConfigManager;
import com.osserp.core.tasks.OptionsCacheRefreshSender;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SystemConfigManagerImpl extends AbstractService implements SystemConfigManager {
    private static Logger log = LoggerFactory.getLogger(SystemConfigManagerImpl.class.getName());

    private BranchOffices branchsDao = null;
    private Districts districts = null;
    private PluginManager pluginManager = null;
    private SystemCompanies companiesDao = null;
    private SystemConfigs configsDao = null;
    private NocClients nocClients = null;
    private OptionsCacheRefreshSender optionsCacheRefreshSender = null;

    public SystemConfigManagerImpl(
            SystemConfigs configs,
            SystemCompanies companies,
            BranchOffices branchs,
            Districts districts,
            NocClients mailDomains,
            PluginManager pluginManager,
            OptionsCacheRefreshSender optionsCacheRefreshSender) {
        super();
        this.companiesDao = companies;
        this.configsDao = configs;
        this.branchsDao = branchs;
        this.districts = districts;
        this.pluginManager = pluginManager;
        this.nocClients = mailDomains;
        this.optionsCacheRefreshSender = optionsCacheRefreshSender;
    }

    public SystemCompany getCompany(Long id) {
        if (id == null) {
            throw new BackendException("getCompany", "invoked with id param null");
        }
        List<SystemCompany> all = companiesDao.getAll();
        for (int i = 0, j = all.size(); i < j; i++) {
            SystemCompany c = all.get(i);
            if (c.getId().equals(id)) {
                return c;
            }
        }
        return null;
    }

    public List<SystemCompany> getActiveCompanies() {
        return companiesDao.getActive();
    }

    public List<SystemCompany> getCompanies() {
        List<SystemCompany> all = companiesDao.getAll();
        return all;
    }

    public boolean isStockManagementEnabled() {
        return configsDao.isSystemPropertyEnabled(SystemConfig.STOCK_MANAGEMENT_PROPERTY);
    }

    public void toggleStockManagement() {
        configsDao.switchBooleanProperty(SystemConfig.STOCK_MANAGEMENT_PROPERTY);
    }

    public Stock getDefaultStock() {
        return companiesDao.getDefaultStock();
    }

    public Stock createStock(DomainUser user, SystemCompany company, BranchOffice office) {
        return companiesDao.createStock(company, office);
    }

    public List<Stock> getAvailableStocks() {
        return companiesDao.getStocks();
    }

    public Stock updateStock(
            DomainUser user,
            Stock stock,
            String shortKey,
            String name,
            String description,
            boolean active,
            Date activationDate,
            boolean planningAware)
        throws ClientException {

        return companiesDao.updateStock(
                stock,
                shortKey,
                name,
                description,
                active,
                activationDate,
                planningAware);
    }

    public boolean isStocktakingSupportEnabled() {
        return configsDao.isSystemPropertyEnabled(SystemConfig.STOCKTAKING_PROPERTY);
    }

    public void toggleStocktakingSupport() {
        configsDao.switchBooleanProperty(SystemConfig.STOCKTAKING_PROPERTY);
    }

    public BranchOffice getBranch(Long id) {
        return branchsDao.getBranchOffice(id);
    }

    public Map<String, Object> getPrintOptions(Long id) {
        Map<String, Object> result = branchsDao.createValueMap(id);
        result.put("ldapClientName", Constants.DEFAULT_CLIENT_NAME);
        List<Property> properties = configsDao.getProperties();
        for (int i = 0, j = properties.size(); i < j; i++) {
            Property nextProperty = properties.get(i);
            if (nextProperty.getName().startsWith("documentPrint")
                    || nextProperty.getName().equals("ldapClientName")) {

                result.put(nextProperty.getName(), nextProperty.getValue());

                if (log.isDebugEnabled()) {
                    log.debug("getPrintOptions() added option [name=" + nextProperty.getName()
                            + ", value=" + nextProperty.getValue() + "]");
                }
            }
        }
        return result;
    }

    public List<BranchOffice> getActivatedBranchs() {
        List<BranchOffice> list = branchsDao.getBranchOffices();
        List<BranchOffice> result = new ArrayList<>();
        for (int i = 0, j = list.size(); i < j; i++) {
            BranchOffice next = list.get(i);
            if (!next.isEndOfLife() && next.isActivated()) {
                result.add(next);
            }
        }
        return CollectionUtil.sort(result, Comparators.createOptionNameComparator(false));
    }

    public List<BranchOffice> getBranchs() {
        return branchsDao.getBranchOffices();
    }

    public List<BranchOffice> getBranchs(Long company) {
        List<BranchOffice> all = getBranchs();
        List<BranchOffice> related = new ArrayList<>();
        for (int i = 0, j = all.size(); i < j; i++) {
            BranchOffice next = all.get(i);
            if (next.getCompany().getId().equals(company)) {
                related.add(next);
            }
        }
        return related;
    }

    public List<BranchOffice> getBranchs(Employee employee) {
        List<BranchOffice> result = new ArrayList<>();
        List<BranchOffice> all = getActivatedBranchs();
        for (int i = 0, j = all.size(); i < j; i++) {
            BranchOffice next = all.get(i);
            if (employee.isSupportingBranch(next)) {
                result.add(next);
            }
        }
        return result;
    }

    public BranchOffice getHeadquarter(Long company) throws ClientException {
        List<BranchOffice> all = getBranchs(company);
        for (int i = 0, j = all.size(); i < j; i++) {
            BranchOffice next = all.get(i);
            if (next.isHeadquarter()) {
                return next;
            }
        }
        throw new ClientException(ErrorCode.BRANCH_MISSING);
    }

    public BankAccount getBankAccount(Long id) {
        return companiesDao.getBankAccount(id);
    }

    public Set<String> getManagedMailDomainNames() {
        return new HashSet<String>(nocClients.getSupportedDomainNames());
    }

    public Plugins getActivePlugins() {
        return pluginManager.getPlugins();
    }

    public String getSystemProperty(String name) {
        return configsDao.getSystemProperty(name);
    }

    public boolean isSystemPropertyEnabled(String name) {
        return configsDao.isSystemPropertyEnabled(name);
    }

    public List<Property> getProperties() {
        return configsDao.getProperties();
    }

    public void switchBooleanSystemProperty(String name) {
        configsDao.switchBooleanProperty(name);
    }

    public void updateSystemProperty(String name, String value) {
        configsDao.updateProperty(name, value);
    }

    public boolean credentialValueAvailable(String context, String name) {
        return (configsDao.getKeyValue(context, name) != null);
    }

    public void saveCredentials(String context, String name, String value) throws ClientException {
        configsDao.saveKey(context, name, value);
    }

    public BranchSelection getBranchSelection(Long id) {
        return configsDao.getBranchSelection(id);
    }

    public BranchSelection getBranchSelection(String name) {
        return configsDao.getBranchSelection(name);
    }
    
    public List<AddressLocality> getLocalitySelection(String zipcode) {
        return districts.findByZipcode(zipcode);
    }

    public List<Menu> getMenus() {
        return configsDao.getMenus();
    }

    public String[] getPermissionConfig(String name) {
        PermissionConfig cfg = configsDao.getPermissionConfig(name);
        return (cfg == null) ? null : cfg.getPermissionValues();
    }

    public void initOptionsCacheRefreshTask() {
        optionsCacheRefreshSender.send(OptionsCacheRefreshSender.ALL_CACHES);
    }

    public void synchronizeBranchs() {
        List<BranchOffice> branchOffices = branchsDao.getBranchOffices();
        for (Iterator<BranchOffice> eg = branchOffices.iterator(); eg.hasNext();) {
            BranchOffice branchOffice = eg.next();
            Contact contact = branchOffice.getContact();
            // TODO LDAP synchronize branchs
            //groupwareContacts.synchronize(contact);
        }
        if (log.isDebugEnabled()) {
            log.debug("synchronizeBranchs() done [count=" + branchOffices.size() + "]");
        }
    }
}
