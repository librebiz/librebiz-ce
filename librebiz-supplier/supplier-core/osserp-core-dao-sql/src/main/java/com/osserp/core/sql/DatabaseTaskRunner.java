/**
 *
 * Copyright (C) 2017 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 5, 2017 
 * 
 */
package com.osserp.core.sql;

import com.osserp.common.service.ConsoleTask;

/**
 * 
 * Setup task runner
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class DatabaseTaskRunner extends ConsoleTask {
    
    /**
     * Runs a named database task. DatabaseTaskRunner tries to retrieve the
     * service by the provided name using the
     * {@link com.osserp.common.service.ConsoleTask#getService(String, boolean)}
     * method.
     * @param task The logical service name of the database task to run.
     * @param version optional string to run dedicated update script
     * @param silent Do not log to System.out. Logs to logfile instead.
     * @param verbose Restrict logging to error if false
     * @return Exit status 1 on error, 0 if ok.
     */
    public static int runDatabaseTasks(String task, String version, boolean silent, boolean verbose) {
        
        DatabaseTask dbas = null;
        try {
            dbas = (DatabaseTask) getService(task, silent);
            
        } catch (Exception e) {
            log("ERROR: Database setup failed with unexpected exception: " 
                    + e.getMessage() + ", class=" + e.getClass().getName(), 
                    true, silent);
            return 1;
        }
        if (!dbas.isDatabaseAvailable()) {
            log("WARN: Database not bound: " + (dbas.getDatabaseUrl() == null 
                    ? "databaseUrl null" : dbas.getDatabaseUrl()), true, silent);
            return 1;
        }
        if (!dbas.runTask(version, verbose)) {
            // Exit with error if we find a configured database but scriptrunner 
            // does not terminate properly.
            log("ERROR: Database update did not terminate properly. See log for details.", 
                    true, silent);
            return 1;
        }
        return 0;
    }
}
