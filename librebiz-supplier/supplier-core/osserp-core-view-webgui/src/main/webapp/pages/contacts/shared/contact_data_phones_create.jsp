<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<c:set var="contact" value="${requestScope.contact}"/>
<div class="contactData">
	<div class="label"><fmt:message key="phone"/></div>
	<div class="value">
		<oc:selectCountry name="pcountry" styleClass="pcountry" value="${contact.phone.country}" type="prefixes" prompt="false"/>
		<input class="pprefix" type="text" name="pprefix" value="<o:out value="${contact.phone.prefix}"/>" />
		<input class="pnumber" type="text" name="pnumber" value="<o:out value="${contact.phone.number}"/>" /> 
  </div>
</div>  

<div class="contactData">
	<div class="label"><fmt:message key="fax"/></div>
	<div class="value">
		<oc:selectCountry name="fcountry" styleClass="pcountry" value="${contact.fax.country}" type="prefixes" prompt="false"/>
		<input class="pprefix" type="text" name="fprefix" value="<o:out value="${contact.fax.prefix}"/>" />
  	<input class="pnumber" type="text" name="fnumber" value="<o:out value="${contact.fax.number}"/>" /> 
	</div>
</div>  

<div class="contactData">
	<div class="label"><fmt:message key="mobile"/></div>
	<div class="value">
		<oc:selectCountry name="mcountry" styleClass="pcountry" value="${contact.mobile.country}" type="prefixes" prompt="false"/>
		<input class="pprefix" type="text" name="mprefix" value="<o:out value="${contact.mobile.prefix}"/>" />
	 	<input class="pnumber" type="text" name="mnumber" value="<o:out value="${contact.mobile.number}"/>" /> 
  </div>
</div>  
