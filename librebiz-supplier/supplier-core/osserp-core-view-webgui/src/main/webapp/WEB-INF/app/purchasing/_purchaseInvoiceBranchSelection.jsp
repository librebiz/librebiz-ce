<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="clientSelection" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive table-responsive-default">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th class="center"><fmt:message key="shortkey" /></th>
                        <th><fmt:message key="name" /></th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="branch" items="${view.branchOfficeList}">
                        <tr>
                            <td class="center"><o:out value="${branch.shortkey}" /></td>
                            <td><v:link url="/purchasing/purchaseInvoiceCreator/selectBranch?id=${branch.id}">
                                    <o:out value="${branch.company.name}" />
                                </v:link></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
