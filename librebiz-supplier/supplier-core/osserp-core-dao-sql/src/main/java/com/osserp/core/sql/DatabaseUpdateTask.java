/**
 *
 * Copyright (C) 2021 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.sql;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.ResourcePatternResolver;

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public class DatabaseUpdateTask extends DatabaseScriptTask implements DatabaseTask {
    private static Logger log = LoggerFactory.getLogger(DatabaseUpdateTask.class.getName());

    public static final String DATABASE_SCRIPT_PATH = 
            "classpath:META-INF/sql/osserp_core_update-*.sql";

    public DatabaseUpdateTask(
            ResourcePatternResolver pathResolver,
            String dbDriver,
            String datasourceString,
            String dbUser,
            String dbPwd) {
        super(pathResolver, dbDriver, datasourceString, dbUser, dbPwd, true);
    }

    /**
     * The pattern for the path containing the database script(s).
     * @return {@value #DATABASE_SCRIPT_PATH}
     */
    protected String getDatabaseScriptPathPattern(String version) {
        if (version != null) {
            return createFilename(DATABASE_SCRIPT_PATH, "*", version);
        }
        return DATABASE_SCRIPT_PATH;
    }

    /**
     * The name of the systemProperty used to retrieve and increase the 
     * latest script id. 
     * @return {@value DatabaseScriptTask#DATABASE_SCRIPT_DEFAULT_PROPERTY}
     */
    protected String getDatabaseScriptPropertyName() {
        return "dbmsSchemaVersion";
    }

    private String createFilename(String pathPattern, String key, String version) {
        StringBuilder text = new StringBuilder(pathPattern);
        if (version != null && version.length() > 0) {
            int i = -1;
            while ((i = text.indexOf(key)) >= 0) {
                text.replace(i, (i + key.length()), version);
            }
        }
        return text.toString();
    }

    @Override
    protected Object getScriptId(String filename) {
        if (filename != null && filename.indexOf('-') > -1) {
            String p1 = filename.substring(filename.indexOf('-') + 1, filename.length());
            return p1.substring(0, p1.indexOf(".sql"));
        }
        return null;
    }

    @Override
    public boolean runTask(String version, boolean verbose) {
        boolean updateAll = "all".equalsIgnoreCase(version);
        if (version != null && !updateAll) {
            return super.runTask(version, verbose);
        }
        try {
            String currentVersion = getLastScriptId();
            Resource[] files = getPathResolver().getResources(getDatabaseScriptPathPattern(null));
            List<String> versions = new ArrayList<>();
            if (files != null && files.length > 0) {
                for (int i = 0; i < files.length; i++) {
                    Resource res = files[i];
                    String filename = res.getFilename();
                    Object vobj = getScriptId(filename);
                    if (vobj instanceof String) {
                        String nextVersion = (String) vobj;
                        if (currentVersion != null &&
                                !"0".equals(currentVersion) && !"-1".equals(currentVersion)) {
                            if (DatabaseScript.compare(nextVersion, currentVersion) > 0) {
                                versions.add(nextVersion);
                            }
                        }
                    }
                }
                if (versions.isEmpty()) {
                    System.out.println("No update script found. Current DBMS version: " + currentVersion);
                } else {
                    System.out.println("Current DBMS version: " + currentVersion);
                    boolean success = false;
                    Collections.sort(versions);
                    for (int i = 0; i < versions.size(); i++) {
                        String nextVersion = versions.get(i);
                        if (updateAll) {
                            success = super.runTask(nextVersion, verbose);
                            if (!success) {
                                System.out.println("ERROR: Failure running script " + nextVersion);
                                return false;
                            }
                        } else {
                            System.out.println("New update script " + nextVersion);
                        }
                    }
                }
            } else {
                log.info("Did not find any script to run");
            }
        } catch (Exception e) {
            log.error("Database update error: " + e.getMessage(), e);
            return false;
        }
        return true;
    }
}
