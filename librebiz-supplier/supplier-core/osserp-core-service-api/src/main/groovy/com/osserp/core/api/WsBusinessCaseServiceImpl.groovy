/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 16.12.2012 
 * 
 */
package com.osserp.core.api

import com.osserp.api.WSBusinessCase
import com.osserp.api.WSBusinessCaseService
import com.osserp.api.WSObjectException
import com.osserp.core.BusinessCase
import com.osserp.core.BusinessCaseSearch
import com.osserp.core.dao.SystemConfigs

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class WsBusinessCaseServiceImpl implements WSBusinessCaseService {
    
    private SystemConfigs systemConfigs
    private BusinessCaseSearch businessCaseSearch
    private WsBusinessCaseMapper wsBusinessCaseMapper
    private CoreBusinessCaseService coreBusinessCaseService
    
    
    protected WsBusinessCaseServiceImpl(
        SystemConfigs systemConfigs,
        BusinessCaseSearch businessCaseSearch,
        WsBusinessCaseMapper wsBusinessCaseMapper,
        CoreBusinessCaseService coreBusinessCaseService) {
        this.systemConfigs = systemConfigs
        this.businessCaseSearch = businessCaseSearch
        this.wsBusinessCaseMapper = wsBusinessCaseMapper
        this.coreBusinessCaseService = coreBusinessCaseService
    }

    /* (non-Javadoc)
     * @see com.osserp.api.WSBusinessCaseService#fetchBusinessCaseById(java.lang.Long)
     */
    public WSBusinessCase fetchBusinessCaseById(Long businessCaseId) {
        BusinessCase businessCase = businessCaseSearch.findBusinessCase(businessCaseId)
        !businessCase ? null : wsBusinessCaseMapper.map(businessCase)
    }

    /* (non-Javadoc)
     * @see com.osserp.api.WSBusinessCaseService#getBusinessCaseById(java.lang.Long)
     */
    public WSBusinessCase getBusinessCaseById(Long businessCaseId) throws WSObjectException {
        WSBusinessCase result = fetchBusinessCaseById(businessCaseId)
        if (!result) {
            throw new WSObjectException("businessCase.id ${businessCaseId} not exists")
        }
        result
    }

    /* (non-Javadoc)
     * @see com.osserp.api.WSBusinessCaseService#createOrUpdate(com.osserp.api.WSBusinessCase)
     */
    public WSBusinessCase createOrUpdate(WSBusinessCase wsBusinessCase) {
        WSBusinessCase result = wsBusinessCase
        if (systemConfigs.isSystemPropertyEnabled('wsBusinessCaseReceiptEnabled') && coreBusinessCaseService) {
            BusinessCase createdOrUpdated = coreBusinessCaseService.createOrUpdate(wsBusinessCase)
            if (createdOrUpdated) {
                result = wsBusinessCaseMapper.map(createdOrUpdated)
            } 
        }
        result
    }

}
