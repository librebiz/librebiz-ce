<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="productStockQueriesView" />
<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4><o:out value="${view.bean.name}" /></h4>
        </div>
    </div>
    <div class="panel-body">
        <c:set var="listSize"><o:listSize value="${view.list}"/></c:set>
        <c:set var="labelDisplayed" value="false"/>
        <c:if test="${listSize > 1}">
            <c:forEach var="query" items="${view.list}" varStatus="s">
                <c:if test="${view.bean.id != query.id}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="querySelection">
                                    <c:if test="${!labelDisplayed}">
                                        <fmt:message key="querySelection" />
                                        <c:set var="labelDisplayed" value="true"/>
                                    </c:if>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <v:link url="/products/productStockQueries/select?id=${query.id}">
                                    <o:out value="${query.name}"/>
                                </v:link>
                            </div>
                        </div>
                    </div>
                </c:if>
            </c:forEach>
        </c:if>

        <v:form name="queryForm" url="/products/productStockQueries/save">

            <v:queryInput jdbcQuery="${view.bean}" />

            <div class="row next">
                <div class="col-md-4"></div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <o:submit styleClass="form-control" value="search" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </v:form>
    </div>
</div>
