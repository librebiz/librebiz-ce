<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="orderData" />
            </h4>
        </div>
    </div>
    <div class="panel-body">

        <div class="row next">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="customer"><fmt:message key="customer" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <span class="form-value"><o:out value="${view.customer.displayName}" /></span>
                </div>
            </div>
        </div>

    </div>
</div>
