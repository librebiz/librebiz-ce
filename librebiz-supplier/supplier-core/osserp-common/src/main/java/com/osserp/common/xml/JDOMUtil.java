/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 10.09.2004 
 * 
 */
package com.osserp.common.xml;

import java.io.StringReader;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.ConstructorUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import com.osserp.common.XmlAwareEntity;
import com.osserp.common.util.DateUtil;
import com.osserp.common.util.FileUtil;
import com.osserp.common.util.NumberUtil;
import com.osserp.common.util.StringUtil;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author cf <cf@osserp.com>
 * 
 */
public class JDOMUtil {
    private static Logger log = LoggerFactory.getLogger(JDOMUtil.class.getName());

    private static final String ENCODING = "UTF-8";
    public static final boolean COMPACT = false;
    public static final boolean PRETTY = true;

    /**
     * Adds all children of the root element of source document to target document root
     * @param target
     * @param source
     * @return
     */
    public static Document addElements(Document target, Document source) {
        Element srcRoot = source.detachRootElement();
        List<Element> elements = srcRoot.getChildren();
        for (int i = 0, j = elements.size(); i < j; i++) {
            Element next = elements.get(i);
            Element clone = next.clone();
            clone.detach();
            target.getRootElement().addContent(clone);
        }
        return target;
    }
    
    /**
     * Creates an org.jdom.output.XMLOutputter.
     * @param pretty if compact or pretty format should be used
     * @return outputter
     */
    public static XMLOutputter createOutputter(boolean pretty) {
        return new XMLOutputter(createFormat(pretty));
    }

    /**
     * Returns an org.jdom.output.Format
     * @param true for pretty format, false for compact format
     * @return format
     */
    private static Format createFormat(boolean pretty) {
        if (pretty) {
            return getPrettyFormat();
        }
        return getCompactFormat();
    }

    private static Format getCompactFormat() {
        Format format = Format.getCompactFormat();
        format.setEncoding(ENCODING);
        return format;
    }

    private static Format getPrettyFormat() {
        Format format = Format.getPrettyFormat();
        format.setEncoding(ENCODING);
        return format;
    }

    public static String getCompactString(Document doc) {
        if (doc != null) {
            try {
                XMLOutputter xo = createOutputter(JDOMUtil.COMPACT);
                String xml = xo.outputString(doc);
                return xml;
            } catch (Exception e) {
                log.error("getCompactString() failed [ignoredMessage=" + e.toString() + "]", e);
            }
        }
        return null;
    }

    public static String getPrettyString(Document doc) {
        if (doc != null) {
            try {
                XMLOutputter xo = createOutputter(JDOMUtil.PRETTY);
                String xml = xo.outputString(doc);
                return xml;
            } catch (Exception e) {
                log.error("getPrettyString() failed [ignoredMessage=" + e.toString() + "]", e);
            }
        }
        return null;
    }

    public static String getPrettyString(Element element) {
        if (element != null) {
            Document doc = element.getDocument();
            if (doc == null) {
                doc = new Document();
                doc.setRootElement(element);
            }
            try {
                XMLOutputter xo = createOutputter(JDOMUtil.PRETTY);
                String xml = xo.outputString(doc);
                return xml;
            } catch (Exception e) {
                log.error("getPrettyString() failed [ignoredMessage=" + e.toString() + "]", e);
            }
        }
        return null;
    }

    public static String getCompactString(Element element) {
        if (element != null) {
            Document doc = element.getDocument();
            if (doc == null) {
                doc = new Document();
                doc.setRootElement(element);
            }
            try {
                XMLOutputter xo = createOutputter(JDOMUtil.COMPACT);
                String xml = xo.outputString(doc);
                return xml;
            } catch (Exception e) {
                log.error("getCompactString() failed [ignoredMessage=" + e.toString() + "]", e);
            }
        }
        return null;
    }

    /**
     * Splits a text by controls
     * @param rootElementName
     * @param lineElementName
     * @param text to split
     * @return root with added lines if text not empty. Otherwise an empty element will be returned
     */
    public static Element createListByText(
            String rootElementName,
            String lineElementName,
            String text) {
        Element element = new Element(rootElementName);
        if (text != null && text.length() > 0) {
            List lines = StringUtil.divideByControl(text);
            if (!lines.isEmpty()) {
                for (int i = 0, j = lines.size(); i < j; i++) {
                    Element line = new Element(lineElementName).setText(
                            (String) lines.get(i));
                    element.addContent(line);
                }
            }
        }
        return element;
    }

    public static boolean fetchBoolean(Element e, String elementName) {
        String s = e.getChildText(elementName);
        try {
            return s == null ? false : Boolean.valueOf(s);
        } catch (Throwable t) {
            return false;
        }
    }

    public static Date fetchDate(Element e, String elementName) {
        String s = e.getChildText(elementName);
        try {
            return DateUtil.createDate(s, false);
        } catch (Throwable t) {
            return null;
        }
    }

    public static Double fetchDouble(Element e, String elementName) {
        String s = e.getChildText(elementName);
        try {
            return s == null ? null : Double.valueOf(s);
        } catch (Throwable t) {
            return NumberUtil.createDouble(s);
        }
    }

    public static Long fetchLong(Element e, String elementName) {
        String s = e.getChildText(elementName);
        try {
            return s == null ? null : Long.valueOf(s);
        } catch (Throwable t) {
            return null;
        }
    }

    /**
     * Tries to fetch an element child string value.
     * Empty strings will be returned as null. 
     * @param e xml element
     * @param elementName name of the child element to lookup for
     * @return
     */
    public static String fetchString(Element e, String elementName) {
        String s = e.getChildText(elementName);
        try {
            return s == null || s.length() < 1 ? null : s;
        } catch (Throwable t) {
            return null;
        }
    }

    /**
     * returns true, if given string is a valid XML element name
     */
    public static boolean isValidElementName(String name) {
        String nameStartChars = "a-zA-Z:_";
        String nameChars = nameStartChars + "0-9.-";

        return name.matches("[" + nameStartChars + "][" + nameChars + "]*");
    }

    /**
     * returns an XML representation of the given list
     */
    public static Element listToXML(List<Object> list, String elementName) {
        Element root = new Element(elementName);
        for (Object object : list) {
            root.addContent(createElement("item", object));
        }
        return root;
    }

    /**
     * returns an XML representation of the given map
     */
    public static Element mapToXML(Map map, String elementName) {
        Element root = new Element(elementName);
        Iterator i = map.keySet().iterator();

        while (i.hasNext()) {
            Object key = i.next();
            Object value = map.get(key);
            root.addContent(createElement(key.toString(), value));
        }
        return root;
    }

    /**
     * creates an element with the given name and value
     * @param name
     * @param value
     */
    public static Element createElement(String name, Object value) {
        if (!isValidElementName(name)) {
            if (!name.startsWith("_")) {
                log.warn("createElement() \"" + name + "\" is not a valid XML element name, trying to make it valid by prepending a \"_\"-character");
                return createElement("_" + name, value);
            }
            log.warn("createElement() name still invalid, using \"root\"");
            name = "root";
        }

        if (value == null) {
            return new Element(name);
        }
        if (value instanceof Element) {
            return ((Element) value).setName(name);
        }

        Element element = null;
        if (value instanceof List) {
            element = listToXML((List) value, name);
        } else if (value instanceof Map) {
            element = mapToXML((Map) value, name);
        } else if (value instanceof XmlAwareEntity) {
            element = ((XmlAwareEntity) value).getXML(name);
        } else {
            element = new Element(name).setText(value.toString());
        }
        return element.setAttribute("type", value.getClass().getName());
    }

    /**
     * takes an XML representation of an object and tries to reconstruct the original object
     * @param element
     */
    public static Object getObjectFromElement(Element element) {
        String type = element.getAttributeValue("type");
        if (type != null) {
            try {
                Object result = ConstructorUtils.invokeExactConstructor(Class.forName(type), element);
                return result;
            } catch (Exception e) {
                if (type.equals("java.lang.String")) {
                    return element.getValue();
                }
                if (type.equals("java.lang.Double")) {
                    return NumberUtil.createDouble(element.getValue());
                }
                if (type.equals("java.lang.Integer")) {
                    return NumberUtil.createInteger(element.getValue());
                }
                if (type.equals("java.lang.Long")) {
                    return NumberUtil.createLong(element.getValue());
                }
                if (type.equals("java.lang.Bolean")) {
                    return element.getValue().equals("true");
                }
                if (type.equals("java.util.Date") || type.equals("java.sql.Timestamp")) {
                    try {
                        return DateUtil.createDate(element.getValue(), false);
                    } catch (Exception ex) {
                        return null;
                    }
                }
                return null;
            }
        }
        return null;
    }

    /**
     * converts a textual representation of an element (e.g. "&lt;root&gt;test&lt;/root&gt;" into an Element
     * @param elementString
     */
    public static Element createElementFromString(String elementString) {
        StringReader sr = new StringReader(elementString);
        SAXBuilder sb = new SAXBuilder();
        sb.setExpandEntities(false);
        try {
            return sb.build(sr).getRootElement().clone();
        } catch (Exception e) {
            log.error("stringToElement() could not convert\n[message=" + e.getMessage() + "\n string=" + elementString + "]");
            return new Element("empty");
        }
    }

    public static Document parse(String filename) {
        try {
            return parseText(FileUtil.readFilesystemFileAsString(filename));
        } catch (Exception e) {
            log.error("parse() failed [message=" + e.getMessage() + ", class=" + e.getClass().getName() + "]", e);
        }
        return null;
    }

    public static Document parseText(String xml) {
        try {
            return JDOMParser.getDocument(xml);
        } catch (Exception e) {
            log.error("parseText() failed [message=" + e.getMessage() + ", class=" + e.getClass().getName() + "]", e);
        }
        return null;
    }
}
