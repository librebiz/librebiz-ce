/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 13, 2004 
 * 
 */
package com.osserp.common.web;

import javax.servlet.http.HttpSessionBindingEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.User;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class OnlineUsersListener extends AbstractSessionListener {

    private static Logger log = LoggerFactory.getLogger(OnlineUsersListener.class.getName());

    /**
     * Logs the attribute name, session id and add time if debug level activated
     * @see javax.servlet.http.HttpSessionAttributeListener#attributeAdded(javax.servlet.http.HttpSessionBindingEvent)
     */
    @Override
    public void attributeAdded(HttpSessionBindingEvent event) {
        if (log.isDebugEnabled()) {
            if (event.getName().indexOf("org.apache.struts") == -1) {
                Object value = event.getValue();
                if (value instanceof String 
                        && !"xlsDocument".equals(event.getName())
                        && !"xmlDocument".equals(event.getName())) {
                    
                    log.debug("attributeAdded() [name=" + event.getName()
                            + ", class="
                            + value.getClass().getName()
                            + ", value=" + value + "]");
                } else {
                    log.debug("attributeAdded() [name=" + event.getName()
                            + ", class="
                            + (value == null ? "null" : value.getClass().getName())
                            + "]");
                }
            }
        }
        super.attributeAdded(event);
        if (event.getName().equals(Globals.USER)) {
            try {
                User user = (User) event.getValue();
                OnlineUsers.newInstance().add(event.getSession().getId(), user);
            } catch (Exception ignore) {
                // we ignore ClassCastExceptions if another handler uses
                // the same key for objects of a different type
            }
        }
    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent event) {
        super.attributeReplaced(event);
    }

    /**
     * Logs the attribute name, session id and removal time if debug level activated
     * @see javax.servlet.http.HttpSessionAttributeListener#attributeRemoved(javax.servlet.http.HttpSessionBindingEvent)
     */
    @Override
    public void attributeRemoved(HttpSessionBindingEvent event) {
        if (log.isDebugEnabled()) {
            if (event.getName().indexOf("org.apache.struts") == -1) {
                log.debug("attributeRemoved() [" + event.getName() + "]");
            }
        }
        if (event.getName().equals(Globals.USER)) {
            try {
                User user = (User) event.getValue();
                OnlineUsers.newInstance().remove(user);
            } catch (Exception ignorable) {
                // we ignore ClassCastExceptions if another handler uses
                // the same key for objects of a different type
                log.error("attributeRemoved() ignoring failure: " + ignorable.toString());
            }
        }
    }

}
