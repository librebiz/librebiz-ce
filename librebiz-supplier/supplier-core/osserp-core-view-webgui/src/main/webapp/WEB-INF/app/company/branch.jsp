<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="branchView"/>
<c:set var="companyExit" scope="request" value="${view.baseLink}/reload"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title"><o:displayTitle/></tiles:put>

    <tiles:put name="headline">
        <c:choose>
            <c:when test="${view.bean.headquarter}"><fmt:message key="headquarter" /></c:when>
            <c:otherwise><fmt:message key="branchOffice" /></c:otherwise>
        </c:choose>
        <span> - </span><o:out value="${view.bean.contact.displayName}" />
    </tiles:put>

    <tiles:put name="headline_right">
        <v:navigation/>
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="branchContent">
            <c:choose>
                <c:when test="${view.editMode}">
                    <v:form id="branchEditForm" url="${view.baseLink}/save">
                        <c:import url="_branchEdit.jsp"/>
                    </v:form>
                </c:when>
                <c:when test="${!empty view.bean}">
                    <c:import url="_branch.jsp" />
                </c:when>
                <c:otherwise>
                    <c:redirect url="/errors/error_context.jsp" />
                </c:otherwise>
            </c:choose>
        </div>
    </tiles:put>
</tiles:insert>
