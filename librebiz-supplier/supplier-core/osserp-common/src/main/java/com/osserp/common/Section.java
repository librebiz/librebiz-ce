/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 28, 2007 9:44:27 AM 
 * 
 */
package com.osserp.common;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Section extends Option {

    /**
     * Provides the value
     * @return value
     */
    Object getValue();

    /**
     * Sets the value
     * @param value
     */
    void setValue(Object value);

    /**
     * Provides the type of the value
     * @return valueType
     */
    String getValueType();

    /**
     * Sets the value type
     * @param valueType
     */
    void setValueType(String valueType);

    /**
     * Provides the width of the input element
     * @return width
     */
    int getWidth();

    /**
     * Sets the width of the input element
     * @param width
     */
    void setWidth(int width);

    /**
     * Indicates that input element should be rendered dynamically
     * @return dynamic
     */
    boolean isDynamic();

    /**
     * Sets that input element is static
     * @param dynamic
     */
    void setDynamic(boolean dynamic);

    /**
     * Indicates that input element has an array of values
     * @return array
     */
    boolean isArray();

    /**
     * Sets that input element has an array of values
     * @param array
     */
    void setArray(boolean array);

    /**
     * Indicates that input element is checkbox
     * @return checkbox
     */
    boolean isCheckbox();

    /**
     * Sets that input element is checkbox
     * @param checkbox
     */
    void setCheckbox(boolean checkbox);

    /**
     * Indicates that input element is select
     * @return select
     */
    boolean isSelect();

    /**
     * Sets that input element is select
     * @param select
     */
    void setSelect(boolean select);

    /**
     * Provides the selection key if select is true
     * @return selectionKey
     */
    String getSelectionKey();

    /**
     * Sets the selection key if select is true
     * @param selectionKey
     */
    void setSelectionKey(String selectionKey);

    /**
     * Indicates that section is required
     * @return required
     */
    boolean isRequired();

    /**
     * Sets that section is required
     * @param required
     */
    void setRequired(boolean required);

    /**
     * Provides an error message if required and missing
     * @return errorMessage
     */
    String getErrorMessage();

    /**
     * Sets an error message if required and missing
     * @param errorMessage
     */
    void setErrorMessage(String errorMessage);

    /**
     * Provides the orderid of the section
     * @return orderId
     */
    int getOrderId();

    /**
     * Sets the order id
     * @param orderId
     */
    void setOrderId(int orderId);

    Object clone();
}
