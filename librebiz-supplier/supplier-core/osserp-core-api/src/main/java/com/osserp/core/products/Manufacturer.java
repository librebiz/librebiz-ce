/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Feb 3, 2005 
 * 
 */
package com.osserp.core.products;

import org.jdom2.Element;

import com.osserp.common.Option;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author cf <cf@osserp.com>
 * 
 */
public interface Manufacturer extends Option {

    /**
     * Returns the related group id
     * @return groupId
     */
    Long getGroupId();

    /**
     * Provides an xml representation of the manufacturer
     * @return product description values
     */
    Element getXML();

    /**
     * Provides an xml representation of the manufacturer
     * @param name of the created element
     * @return product description values
     */
    Element getXML(String rootElementName);
}
