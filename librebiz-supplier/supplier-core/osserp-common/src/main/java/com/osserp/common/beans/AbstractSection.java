/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 30, 2007 11:30:52 AM 
 * 
 */
package com.osserp.common.beans;

import com.osserp.common.Section;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractSection extends AbstractOption implements Section {

    private Object value = null;
    private String valueType = null;
    private boolean dynamic = false;
    private boolean array = false;
    private boolean select = false;
    private String selectionKey = null;
    private boolean checkbox = false;
    public int width;
    private boolean required = false;
    private String errorMessage = null;
    private int orderId = 0;

    protected AbstractSection() {
        super();
    }

    protected AbstractSection(String name, String description) {
        super(name, description, (String) null);
    }

    protected AbstractSection(
            Long id,
            String name,
            String description,
            String resourceKey,
            String valueType,
            boolean dynamic,
            boolean array,
            boolean select,
            String selectionKey,
            boolean checkbox,
            int width,
            boolean required,
            String errorMessage,
            int orderId,
            Object value) {
        super(name, description, resourceKey);
        this.array = array;
        this.checkbox = checkbox;
        this.dynamic = dynamic;
        this.errorMessage = errorMessage;
        this.orderId = orderId;
        this.required = required;
        this.select = select;
        this.selectionKey = selectionKey;
        this.value = value;
        this.valueType = valueType;
        this.width = width;
    }

    protected AbstractSection(Section src) {
        super(src);
        array = src.isArray();
        checkbox = src.isCheckbox();
        dynamic = src.isDynamic();
        errorMessage = src.getErrorMessage();
        orderId = src.getOrderId();
        required = src.isRequired();
        select = src.isSelect();
        selectionKey = src.getSelectionKey();
        value = src.getValue();
        valueType = src.getValueType();
        width = src.getWidth();
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String getValueType() {
        return valueType;
    }

    public void setValueType(String valueType) {
        this.valueType = valueType;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public boolean isDynamic() {
        return dynamic;
    }

    public void setDynamic(boolean dynamic) {
        this.dynamic = dynamic;
    }

    public boolean isArray() {
        return array;
    }

    public void setArray(boolean array) {
        this.array = array;
    }

    public boolean isCheckbox() {
        return checkbox;
    }

    public void setCheckbox(boolean checkbox) {
        this.checkbox = checkbox;
    }

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }

    public String getSelectionKey() {
        return selectionKey;
    }

    public void setSelectionKey(String selectionKey) {
        this.selectionKey = selectionKey;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }
}
