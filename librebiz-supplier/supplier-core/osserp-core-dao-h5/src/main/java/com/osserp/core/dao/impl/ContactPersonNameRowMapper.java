/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 11-Sep-2006 22:07:26 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import com.osserp.common.OptionsCache;
import com.osserp.common.beans.OptionImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ContactPersonNameRowMapper extends BusinessObjectRowMapper {

    public ContactPersonNameRowMapper(OptionsCache optionsCache) {
        super(optionsCache);
    }

    public static RowMapperResultSetExtractor getReader(OptionsCache optionsCache) {
        return new RowMapperResultSetExtractor(new ContactPersonNameRowMapper(optionsCache));
    }

    public Object mapRow(ResultSet rs, int index) throws SQLException {
        StringBuffer display = new StringBuffer(64);
        Long id = Long.valueOf(rs.getLong(1));
        Long salutation = Long.valueOf(rs.getLong(2));
        String firstName = rs.getString(3);
        String lastName = rs.getString(4);
        if (salutation.longValue() != 0) {
            display.append(fetchSalutation(salutation)).append(" ");
        }
        if (firstName != null) {
            display.append(firstName).append(" ");
        }
        display.append(lastName);
        return new OptionImpl(id, display.toString());
    }

}
