<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="row">
    <div class="col-md-6 panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4><fmt:message key="nocClientAccount"/> - <o:out value="${view.client.name}"/></h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-body">

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="defaultAccount"><fmt:message key="defaultAccountLabel"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:choose>
                                <c:when test="${view.client.defaultClient}"><fmt:message key="bigYes"/></c:when>
                                <c:otherwise><fmt:message key="bigNo"/></c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="directorySupport"><fmt:message key="directorySupportLabel"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:choose>
                                <c:when test="${view.client.directorySupport}"><fmt:message key="bigYes"/></c:when>
                                <c:otherwise><fmt:message key="bigNo"/></c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>

                <div class="row next">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="mailboxDomainSupport"><fmt:message key="mailboxDomainSupport"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:choose>
                                <c:when test="${view.client.maildomainSupport}"><fmt:message key="bigYes"/></c:when>
                                <c:otherwise><fmt:message key="bigNo"/></c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="mailboxDomainSupportLocal"><fmt:message key="mailboxDomainSupportLocal"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:choose>
                                <c:when test="${view.client.maildomainLocal}"><fmt:message key="bigYes"/></c:when>
                                <c:otherwise><fmt:message key="bigNo"/></c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>

                <div class="row next">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="groupwareSupport"><fmt:message key="groupwareSupport"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:choose>
                                <c:when test="${view.client.groupwareSupport}"><fmt:message key="bigYes"/></c:when>
                                <c:otherwise><fmt:message key="bigNo"/></c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="groupwareSupportLocal"><fmt:message key="groupwareSupportLocal"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:choose>
                                <c:when test="${view.client.groupwareLocal}"><fmt:message key="bigYes"/></c:when>
                                <c:otherwise><fmt:message key="bigNo"/></c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
