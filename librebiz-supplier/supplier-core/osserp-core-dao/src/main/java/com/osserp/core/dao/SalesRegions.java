/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 18, 2011 2:23:59 PM 
 * 
 */
package com.osserp.core.dao;

import java.util.List;

import com.osserp.core.employees.Employee;
import com.osserp.core.sales.SalesRegion;
import com.osserp.core.sales.SalesRegionConfig;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 */
public interface SalesRegions {

    /**
     * Provides sales region config
     * @return config
     */
    SalesRegionConfig getConfig();

    /**
     * Creates a new sales region
     * @param user
     * @param name
     * @param sales
     * @param salesExecutive
     * @return salesRegion new created region
     */
    SalesRegion create(Employee user, String name, Employee sales, Employee salesExecutive);

    /**
     * Deletes a region by id
     * @param id
     */
    void delete(Long id);

    /**
     * Finds region by zipcode
     * @param zipcode
     * @return salesRegion or null if no matching zipcode found
     */
    SalesRegion find(String zipcode);

    /**
     * Finds regions by employee
     * @param sales
     * @return assigned regions
     */
    List<SalesRegion> find(Employee sales);

    /**
     * Provides all available regions
     * @return regions available
     */
    List<SalesRegion> findAll();

    /**
     * Provides a region by id
     * @param id
     * @return region
     */
    SalesRegion getRegion(Long id);

    /**
     * Indicates if sales employee is assigned to one or more salesRegions
     * @param sales
     * @return true if salesRegion(s) assigned
     */
    boolean isRegionAssigned(Employee sales);

    /**
     * Persists region values
     * @param region
     */
    void save(SalesRegion region);
}
