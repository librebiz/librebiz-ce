/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 20, 2008 11:23:41 AM 
 * 
 */
package com.osserp.core.dms;

import java.util.Locale;
import java.util.Map;

import javax.xml.transform.Source;

import com.osserp.common.ClientException;
import com.osserp.common.service.StylesheetService;

import com.osserp.core.BusinessTemplate;
import com.osserp.core.finance.Record;
import com.osserp.core.system.BranchOffice;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface CoreStylesheetService extends StylesheetService {

    /**
     * Creates a custom template path for a branch if not already exists.
     * @param office
     */
    void createCustomTemplatePathIfRequired(BranchOffice office);

    /**
     * Creates a stylesheet by informations provided by a record.
     * @param record
     * @param templateOptions
     * @return sheet or null if no such id exists
     * @throws ClientException if branch or company not set or properly setup
     */
    Source getStylesheetSource(Record record, Map<String, Object> templateOptions) throws ClientException;

    /**
     * Provides a stylesheet by informations provided by a record as text
     * @param record
     * @param templateOptions
     * @return sheet as text
     * @throws ClientException if branch or company not set or properly setup
     */
    String getStylesheetText(Record record, Map<String, Object> templateOptions) throws ClientException;

    /**
     * Creates a stylesheet by provided company information.
     * @param name of the stylsheet template
     * @param branchId
     * @param locale (optional; defaults to com.osserp.common.Constants.DEFAULT_LANGUAGE)
     * @return source
     */
    Source getStylesheetSource(String name, Long branchId, Locale locale);

    /**
     * Creates a stylesheet by informations provided by a document.
     * @param template
     * @param branchId the branchId is optional if document itself is related to 
     * a dedicated branch or company 
     * @param locale (optional; defaults to com.osserp.common.Constants.DEFAULT_LANGUAGE)
     * @return source
     */
    Source getStylesheetSource(BusinessTemplate template, Long branchId, Locale locale);
}
