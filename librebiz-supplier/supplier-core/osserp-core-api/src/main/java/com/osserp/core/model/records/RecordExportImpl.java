/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 10-Aug-2006 16:50:45 
 * 
 */
package com.osserp.core.model.records;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.osserp.common.CalendarMonth;
import com.osserp.common.beans.AbstractEntity;
import com.osserp.common.util.DateUtil;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.finance.RecordExport;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RecordExportImpl extends AbstractEntity implements RecordExport {

    private Long type = null;
    private String exportInterval = null;
    private Date startDate = null;
    private CalendarMonth startMonth = null;
    private Date endDate = null;
    private CalendarMonth endMonth = null;
    private int exportCount = 0;

    private Double _summaryNet = null;
    private Double _summaryTax = null;
    private Double _summaryReducedTax = null;
    private Double _summaryGross = null;

    private List<RecordDisplay> records = new ArrayList<RecordDisplay>();

    protected RecordExportImpl() {
        super();
    }

    public RecordExportImpl(
            Long id,
            Long type,
            Date created,
            Long createdBy,
            Date startDate,
            Date endDate,
            int exportCount) {
        super(id, (Long) null, created, createdBy);
        this.type = type;
        this.startDate = startDate;
        this.endDate = endDate;
        this.exportCount = exportCount;
    }

    public RecordExportImpl(RecordDisplay record) {
        super(record.getExportId(), (Long) null, record.getExportDate(), (Long) null);
        records.add(record);
        type = record.getType();
    }

    public List<RecordDisplay> getRecords() {
        return records;
    }

    protected void setRecords(List<RecordDisplay> records) {
        if (records != null) {
            this.records = records;
        }
    }

    public Long getType() {
        return type;
    }

    protected void setType(Long type) {
        this.type = type;
    }

    public Date getStartDate() {
        if (startDate == null) {
            for (int i = 0, j = records.size(); i < j; i++) {
                RecordDisplay next = records.get(i);
                if (startDate == null || startDate.after(next.getCreated())) {
                    startDate = next.getCreated();
                }
            }
        }
        return startDate;
    }

    protected void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public CalendarMonth getStartMonth() {
        if (startMonth == null) {
            Date date = getStartDate();
            if (date != null) {
                startMonth = DateUtil.createMonth(date);
            }
        }
        return startMonth;
    }

    public Date getEndDate() {
        if (endDate == null) {
            for (int i = 0, j = records.size(); i < j; i++) {
                RecordDisplay next = records.get(i);
                if (endDate == null || endDate.before(next.getCreated())) {
                    endDate = next.getCreated();
                }
            }
        }
        return endDate;
    }

    protected void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public CalendarMonth getEndMonth() {
        if (endMonth == null) {
            Date date = getEndDate();
            if (date != null) {
                endMonth = DateUtil.createMonth(date);
            }
        }
        return endMonth;
    }

    public String getExportInterval() {
        if (exportInterval == null) {
            if (getStartMonth() == null || getEndMonth() == null) {
                return "";
            }
            String start = getStartMonth().getShortYearAndMonthDisplay();
            String end = getEndMonth().getShortYearAndMonthDisplay();
            if (start.equals(end)) {
                exportInterval = start;
            } else {
                exportInterval = start + " - " + end;
            }
        }
        return exportInterval;
    }

    public int getExportCount() {
        return exportCount;
    }

    public void setExportCount(int exportCount) {
        this.exportCount = exportCount;
    }

    public Double getNetAmount() {
        if (_summaryNet == null) {
            loadAmounts();
        }
        return _summaryNet;
    }

    public Double getTaxAmount() {
        if (_summaryTax == null) {
            loadAmounts();
        }
        return _summaryTax;
    }

    public Double getReducedTaxAmount() {
        if (_summaryReducedTax == null) {
            loadAmounts();
        }
        return _summaryReducedTax;
    }

    public Double getGrossAmount() {
        if (_summaryGross == null) {
            loadAmounts();
        }
        return _summaryGross;
    }

    private void loadAmounts() {
        for (int i = 0, j = records.size(); i < j; i++) {
            RecordDisplay rd = records.get(i);
            if (rd.isDownpayment()) {
                _summaryNet = (_summaryNet == null ? 0D : _summaryNet) + rd.getProrateNetAmount();
                _summaryTax = (_summaryTax == null ? 0D : _summaryTax) + rd.getProrateTaxAmount();
                _summaryReducedTax = (_summaryReducedTax == null ? 0D : _summaryReducedTax) + rd.getProrateReducedTaxAmount();
                _summaryGross = (_summaryGross == null ? 0D : _summaryGross) + rd.getProrateGrossAmount();
            } else {
                _summaryNet = (_summaryNet == null ? 0D : _summaryNet) + rd.getNetAmount();
                _summaryTax = (_summaryTax == null ? 0D : _summaryTax) + rd.getTaxAmount();
                _summaryReducedTax = (_summaryReducedTax == null ? 0D : _summaryReducedTax) + rd.getReducedTaxAmount();
                _summaryGross = (_summaryGross == null ? 0D : _summaryGross) + rd.getGrossAmount();
            }
        }
    }
}
