/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.ds


/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class ImporterImpl implements Importer {

    File importDir
    String mode
    Boolean modeRequired = false
    Boolean importDirRequired = false
    
    int insertCount = 0
    int overrideCount = 0
    int updateCount = 0
    int exceptionCount = 0
    
    /**
     * Creates a new importer
     * @param selectedMode
     * @param directory
     * @param modeRequired if mode is required
     * @param importDirRequired if importDir required
     * @throws ImportException
     */
    protected ImporterImpl(String selectedMode, String directory, Boolean modeRequired,	Boolean importDirRequired) throws ImportException {
        mode = selectedMode
        if (directory) {
            importDir = new File(directory)
        }
        this.modeRequired = modeRequired
        this.importDirRequired = importDirRequired

        if (modeRequired) {
            checkMode()
        }
        if (importDirRequired && !importDir?.directory) {
            throw new ImportException("Invalid directory specified: ${directory}")
        }
    }

    protected abstract Boolean isImportFile(File file)

    protected abstract ImportFile createImportFile(File file) throws ImportException

    /**
     * Checks if assigned mode is valid.
     * @throws ImportException on invalid mode
     */
    protected void checkMode() throws ImportException {
        if (!mode) {
            throw new ImportException('No mode specified!')
        }
    }

    protected final File createFile(String filename) {
        String file = !importDirRequired ? filename : "${importDir.getAbsolutePath()}/${filename}"
        //logger.debug("createFile: [done=${file}]")
        new File(file)
    }
}
