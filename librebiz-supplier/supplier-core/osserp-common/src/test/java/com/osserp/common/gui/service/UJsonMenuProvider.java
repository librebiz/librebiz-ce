/**
 *
 * Copyright (C) 2015 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 9, 2015 
 * 
 */
package com.osserp.common.gui.service;

import java.util.List;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.osserp.common.AbstractUtil;
import com.osserp.common.gui.Menu;
import com.osserp.common.util.JsonObject;
import com.osserp.common.util.JsonUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class UJsonMenuProvider extends AbstractUtil {
    
    private static final String JSON_FILE = "/com/osserp/common/gui/service/Menu.json";

    @Test
    public void testReadJsonFromResourceFile() {
        JsonObject obj = JsonUtil.parseResourceFile(JSON_FILE);
        Object element = obj.get("menu");
        String className = (element != null) ? element.getClass().getName() : "null";
        assertTrue(!"null".equals(className));
    }

    @Test
    public void testReadJsonMenuFromResourceFile() {
        JsonMenuProvider provider = new JsonMenuProvider(JSON_FILE);
        List<Menu> menuList = provider.getMenus();
        assertTrue(!menuList.isEmpty());
        assertTrue(menuList.size() == 1);
        assertTrue(menuList.get(0).getHeaders().size() == 1);
        assertTrue(menuList.get(0).getHeaders().get(0).getLinks().size() == 3);
    }

}
