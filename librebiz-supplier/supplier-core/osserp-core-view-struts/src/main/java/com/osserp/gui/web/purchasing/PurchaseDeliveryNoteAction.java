/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 25, 2006 10:47:19 AM 
 * 
 */
package com.osserp.gui.web.purchasing;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ClientException;
import com.osserp.common.web.View;
import com.osserp.gui.client.purchasing.PurchaseDeliveryNoteView;
import com.osserp.gui.client.purchasing.PurchaseOrderView;
import com.osserp.gui.client.records.DeliveryNoteView;
import com.osserp.gui.client.records.OrderView;
import com.osserp.gui.web.records.AbstractDeliveryNoteAction;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PurchaseDeliveryNoteAction extends AbstractDeliveryNoteAction {
    private static Logger log = LoggerFactory.getLogger(PurchaseDeliveryNoteAction.class.getName());

    @Override
    protected Class<? extends View> getOrderViewClass() {
        return PurchaseOrderView.class;
    }

    @Override
    protected Class<? extends View> getViewClass() {
        return PurchaseDeliveryNoteView.class;
    }

    /**
     * Exits delivery note display.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    @Override
    public ActionForward exit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exit() request from " + getUser(session).getId());
        }
        DeliveryNoteView view = (DeliveryNoteView) getRecordView(session);
        if (view.isReadOnlyMode()) {
            return super.exit(mapping, form, request, response);
        }
        try {
            if (view.getNote().getItems().isEmpty()) {
                view.delete();
            }
            OrderView orderView = (OrderView) fetchView(session, PurchaseOrderView.class);
            if (orderView != null) {
                orderView.refresh();
            }
        } catch (Throwable t) {
            log.warn("exit() ignoring failure while attempt to refresh related order on exit: "
                    + t.toString());
        }
        return super.exit(mapping, form, request, response);
    }

    /**
     * Releases current selected delivery note
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    @Override
    public ActionForward release(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("release() request from " + getUser(session).getId());
        }
        PurchaseDeliveryNoteView view = (PurchaseDeliveryNoteView) getRecordView(session);
        try {
            view.release();
        } catch (ClientException e) {
            saveError(request, e.getMessage());
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Opens closed invoice
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward openClosed(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("openClosed() request from " + getUser(session).getId());
        }
        PurchaseDeliveryNoteView view = (PurchaseDeliveryNoteView) getRecordView(session);
        view.openClosed();
        return mapping.findForward(view.getActionTarget());
    }
}
