/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 18, 2008 12:45:47 PM 
 * 
 */
package com.osserp.core.service.records;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.Constants;
import com.osserp.common.User;

import com.osserp.core.Item;
import com.osserp.core.dao.Products;
import com.osserp.core.dao.Users;
import com.osserp.core.dao.records.RecordBackups;
import com.osserp.core.dao.records.RecordVersionsArchive;
import com.osserp.core.dao.records.Records;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.DeliveryNote;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.OrderManager;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordBackupItem;
import com.osserp.core.finance.RecordDocument;
import com.osserp.core.system.SystemConfigManager;
import com.osserp.core.tasks.ProductPlanningCacheSender;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractOrderManager extends AbstractPaymentAgreementAwareRecordManager
        implements OrderManager {
    private static Logger log = LoggerFactory.getLogger(AbstractOrderManager.class.getName());
    private Records referenceRecords = null;
    private RecordBackups recordBackups = null;
    private RecordVersionsArchive archive = null;
    private Users users = null;

    public AbstractOrderManager(
            Records records,
            Records referenceRecords,
            SystemConfigManager systemConfigManager,
            Products products,
            ProductPlanningCacheSender productPlanningTask,
            RecordBackups recordBackups,
            RecordVersionsArchive archive,
            Users users) {
        super(records, systemConfigManager, products, productPlanningTask);
        this.referenceRecords = referenceRecords;
        this.recordBackups = recordBackups;
        this.archive = archive;
        this.users = users;
    }

    public void changeStock(Order order, Long productId, Long stockId) {
        for (int i = 0, j = order.getItems().size(); i < j; i++) {
            Item item = order.getItems().get(i);
            if (item.getProduct().getProductId().equals(productId)) {
                item.setStockId(stockId);
                if (log.isDebugEnabled()) {
                    log.debug("changeStock() changed [itemId=" + item.getId() + ", productId="
                            + productId + ", stockId=" + stockId + "]");
                }
            }
        }
        persist(order);
        createProductPlanningRefreshTask(order, null);
        if (log.isDebugEnabled()) {
            log.debug("changeStock() done [order=" + order.getId() + "]");
        }
    }

    public void createNewVersion(Employee user, Order order, boolean backupItems, String backupContext) {
        if (log.isDebugEnabled()) {
            log.debug("createNewVersion() invoked [order=" + order.getId()
                    + ", deliveryDate=" + order.getDelivery() + "]");
        }
        if (order.isUnchangeable()) {
            if (log.isDebugEnabled()) {
                log.debug("createNewVersion() order ["
                        + order.getId() + "] is unchangeable, creating new version");
            }
            order.createVersion((user == null ? Constants.SYSTEM_EMPLOYEE : user.getId()));
            order.markChanged();
            getDao().save(order);
            User dbUser = users.find(user);
            archive.create(dbUser, order);
            if (backupItems) {
                recordBackups.createItemBackup(order, backupContext);
            }
            if (log.isDebugEnabled()) {
                log.debug("createNewVersion() version increased, archive entry created [version="
                        + order.getVersion()
                        + ", deliveryDate="
                        + order.getDelivery() + "]");
            }
        }
    }

    public boolean isItemBackupAvailable(Record record, String backupContext) {
        return recordBackups.isItemBackupAvailable(record, backupContext);
    }

    public Record restoreItems(Employee user, Record record, String backupContext) {
        List<RecordBackupItem> items = recordBackups.getBackupItems(record, backupContext);
        if (items.isEmpty()) {
            return record;
        }
        Record result = deleteItems(user, record);
        for (int i = 0, j = items.size(); i < j; i++) {
            RecordBackupItem next = items.get(i);
            result.addItem(
                    next.getStockId(),
                    next.getProduct(),
                    next.getCustomName(),
                    next.getQuantity(),
                    next.getTaxRate(),
                    next.getPrice(),
                    next.getPriceDate(),
                    next.getPartnerPrice(),
                    next.isPartnerPriceEditable(),
                    next.isPartnerPriceOverridden(),
                    next.getPurchasePrice(),
                    next.getNote(),
                    next.isIncludePrice(),
                    next.getExternalId());
        }
        result.updateStatus(Record.STAT_CHANGED);
        persist(record);
        recordBackups.deleteItemBackup(result, backupContext);
        return load(record.getId());
    }

    public void closeService(Employee user, Order order) {
        boolean changed = false;
        if (order != null) {
            for (int i = 0, j = order.getItems().size(); i < j; i++) {
                Item item = order.getItems().get(i);
                if (item.getProduct().isService()) {
                    item.setDelivered(item.getQuantity());
                    changed = true;
                }
            }
        }
        if (changed) {
            persist(order);
        }
    }

    public void closeService(Employee user, Order order, Item item) {
        boolean changed = false;
        if (order != null && item != null) {
            for (int i = 0, j = order.getItems().size(); i < j; i++) {
                Item nextitem = order.getItems().get(i);
                if (nextitem.getId().equals(item.getId()) && item.getProduct().isService()) {
                    item.setDelivered(item.getQuantity());
                    changed = true;
                    break;
                }
            }
        }
        if (changed) {
            persist(order);
        }
    }

    public List<RecordDocument> findVersions(Order order) {
        return archive.find(order);
    }

    public byte[] getVersion(RecordDocument document) {
        return archive.getDocumentData(document);
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public List<DeliveryNote> getDeliveryNotes(Order order) {
        try {
            return new ArrayList(getReferenced(order));
        } catch (Throwable t) {
            log.warn("getDeliveryNotes() ignoring failure [message=" + t.getMessage() + "]");
            return new ArrayList<>();
        }
    }

    public List<Record> getReferenced(Record record) {
        return new ArrayList(referenceRecords.getByReference(record.getId()));
    }

}
