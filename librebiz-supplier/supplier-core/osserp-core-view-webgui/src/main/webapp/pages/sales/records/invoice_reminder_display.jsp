<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${sessionScope.invoiceReminderView}" />
<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="styles" type="string">
        <style type="text/css">
.amount {
    text-align: right;
}

.reducedTax {
    width: 60px;
}
</style>
    </tiles:put>
    <tiles:put name="headline">
        <o:listSize value="${view.list}" />
        <fmt:message key="invoiceListTotalAmountHeader" />
        <o:number value="${view.grossAmount}" format="currency" />
        <span> - </span>
        <fmt:message key="demandsLabel" />
        <o:number value="${view.dueAmount}" format="currency" />
    </tiles:put>
    <tiles:put name="headline_right">
        <ul>
            <c:choose>
                <c:when test="${view.editMode}">
                    <li><a href="<c:url value="/salesInvoiceReminder.do?method=disableEdit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon" /></a></li>
                </c:when>
                <c:otherwise>
                    <li><a href="<c:url value="/salesInvoiceReminder.do?method=exit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon" /></a></li>
                    <li><a href="<c:url value="/salesInvoiceReminder.do?method=enableEdit"/>" title="<fmt:message key="settings"/>"><o:img name="configureIcon" /></a></li>
                    <li><a href="<c:url value="/salesInvoiceReminder.do?method=xls"/>" title="<fmt:message key="displaySheet"/>"><o:img name="tableIcon" /></a></li>
                </c:otherwise>
            </c:choose>
            <li><v:link url="/index" title="backToMenu">
                    <o:img name="homeIcon" />
                </v:link></li>
        </ul>
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="invoiceReminderContent">
            <div class="row">
                <c:choose>
                    <c:when test="${view.editMode}">
                        <c:import url="invoice_reminder_edit.jsp"/>
                    </c:when>
                    <c:otherwise>
                        <c:import url="invoice_reminder_list.jsp"/>
                    </c:otherwise>
                 </c:choose>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
