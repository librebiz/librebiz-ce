/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 4, 2010 2:10:02 PM 
 * 
 */
package com.osserp.core.dao.telephone.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.dao.AbstractHibernateAndSpringDao;
import com.osserp.common.dao.Tables;

import com.osserp.core.dao.telephone.SfaSubscriberToTelephoneGroups;
import com.osserp.core.dao.telephone.SfaSubscribers;
import com.osserp.core.dao.telephone.SfaTelephoneGroups;
import com.osserp.core.dao.telephone.TelephoneConfigurations;
import com.osserp.core.model.telephone.SfaSubscriberImpl;
import com.osserp.core.telephone.SfaSubscriber;
import com.osserp.core.telephone.TelephoneConfiguration;

/**
 * 
 * @author so <so@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public class SfaSubscribersDao extends AbstractHibernateAndSpringDao implements SfaSubscribers {
    private static Logger log = LoggerFactory.getLogger(SfaSubscribersDao.class.getName());
    private TelephoneConfigurations telephoneConfigs = null;
    private SfaSubscriberToTelephoneGroups sfaSubscriberToTelephoneGroups = null;
    private SfaTelephoneGroups sfaTelephoneGroups = null;

    public SfaSubscribersDao() {
        super();
    }

    public SfaSubscribersDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            TelephoneConfigurations telephoneConfigs,
            SfaSubscriberToTelephoneGroups sfaSubscriberToTelephoneGroups,
            SfaTelephoneGroups sfaTelephoneGroups) {
        super(jdbcTemplate, tables, sessionFactory);
        this.telephoneConfigs = telephoneConfigs;
        this.sfaSubscriberToTelephoneGroups = sfaSubscriberToTelephoneGroups;
        this.sfaTelephoneGroups = sfaTelephoneGroups;
    }

    public void synchronize(Long id) {
        if (log.isDebugEnabled()) {
            log.debug("synchronize() invoked for TelephoneConfiguration [id=" + id + "]");
        }
        TelephoneConfiguration tc = telephoneConfigs.find(id);
        if (tc != null) {
            if (tc.getUid() != null) {
                this.sfaSubscriberToTelephoneGroups.synchronize(
                        this.sfaTelephoneGroups.findByTelephoneGroup((tc.getTelephoneGroup() != null) ? tc.getTelephoneGroup().getId() : null), tc.getUid());
                if (tc.getTelephone() != null) {
                    if (tc.getTelephone().getTelephoneSystem() != null) {
                        if (tc.getTelephone().getTelephoneSystem().getId() != null) {
                            SfaSubscriber sfa = fetchSubscriber(tc);

                            sfa.setSynced(false);
                            sfa.setUnused(tc.getTelephone().isEol());
                            sfa.setTelephoneSystemId(tc.getTelephone().getTelephoneSystem().getId());
                            sfa.setAccountCode(tc.getAccountCode());
                            sfa.setDisplayName(tc.getDisplayName());
                            sfa.setEmailAddress(tc.getEmailAddress());
                            sfa.setUid(tc.getUid());
                            sfa.setPhoneNumber(tc.getInternal());
                            sfa.setMac(tc.getTelephone().getMac());
                            sfa.setAvailable(tc.isAvailable());
                            sfa.setBusyOnBusy(tc.isBusyOnBusy());
                            sfa.setVoiceMailEnabled(tc.isVoiceMailEnabled());
                            sfa.setVoiceMailDelaySeconds(tc.getVoiceMailDelaySeconds());
                            sfa.setVoiceMailSendAsEmail(tc.isVoiceMailSendAsEmail());
                            sfa.setVoiceMailDeleteAfterEmailSend(tc.isVoiceMailDeleteAfterEmailSend());
                            sfa.setVoiceMailEnabledIfBusy(tc.isVoiceMailEnabledIfBusy());
                            sfa.setVoiceMailEnabledIfUnavailable(tc.isVoiceMailEnabledIfUnavailable());
                            sfa.setParallelCallTargetPhoneNumber(tc.getParallelCallTargetPhoneNumber());
                            sfa.setParallelCallEnabled(tc.isParallelCallEnabled());
                            sfa.setParallelCallSetDelaySeconds(tc.getParallelCallSetDelaySeconds());
                            sfa.setCallForwardTargetPhoneNumber(tc.getCallForwardTargetPhoneNumber());
                            sfa.setCallForwardDelaySeconds(tc.getCallForwardDelaySeconds());
                            sfa.setCallForwardEnabled(tc.isCallForwardEnabled());
                            sfa.setCallForwardIfBusyTargetPhoneNumber(tc.getCallForwardIfBusyTargetPhoneNumber());
                            sfa.setCallForwardIfBusyEnabled(tc.isCallForwardIfBusyEnabled());

                            getCurrentSession().saveOrUpdate(SfaSubscriberImpl.class.getName(), sfa);
                        }
                    }
                }
            }
        }
    }

    public SfaSubscriber fetchSubscriber(TelephoneConfiguration tc) {
        if (log.isDebugEnabled()) {
            log.debug("fetchSubscriber() invoked");
        }
        List<SfaSubscriber> list = getCurrentSession().createQuery(
                "from " + SfaSubscriberImpl.class.getName() + " s where s.uid = :uid")
                .setParameter("uid", tc.getUid()).list();
        return (list.isEmpty() ? new SfaSubscriberImpl() : list.get(0));
    }
}
