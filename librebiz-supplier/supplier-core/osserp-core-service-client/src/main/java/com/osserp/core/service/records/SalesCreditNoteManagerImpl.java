/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 04-Feb-2007 15:12:17 
 * 
 */
package com.osserp.core.service.records;

import java.math.BigDecimal;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;

import com.osserp.core.customers.Customer;
import com.osserp.core.dao.Products;
import com.osserp.core.dao.records.SalesCreditNotes;
import com.osserp.core.dao.records.SalesInvoices;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.BookingType;
import com.osserp.core.finance.CreditNote;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.PaymentAwareRecord;
import com.osserp.core.finance.Record;
import com.osserp.core.sales.SalesCreditNote;
import com.osserp.core.sales.SalesCreditNoteManager;
import com.osserp.core.sales.SalesCreditNoteType;
import com.osserp.core.sales.SalesDeliveryNoteManager;
import com.osserp.core.sales.SalesInvoice;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.system.SystemConfigManager;
import com.osserp.core.tasks.ProductPlanningCacheSender;
import com.osserp.core.tasks.SalesVolumeReportSender;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesCreditNoteManagerImpl extends AbstractPaymentAwareRecordManager 
        implements SalesCreditNoteManager {
    private static Logger log = LoggerFactory.getLogger(SalesCreditNoteManagerImpl.class.getName());
    private SalesInvoices invoices = null;
    private SalesDeliveryNoteManager deliveryNoteManager = null;
    private SalesVolumeReportSender salesVolumeReportSender = null;

    public SalesCreditNoteManagerImpl(
            SalesCreditNotes records,
            SystemConfigManager systemConfigManager,
            Products products,
            ProductPlanningCacheSender productPlanningTask,
            SalesInvoices invoices,
            SalesDeliveryNoteManager deliveryNoteManager,
            SalesVolumeReportSender salesVolumeReportSender) {
        super(records, systemConfigManager, products, productPlanningTask);
        this.invoices = invoices;
        this.deliveryNoteManager = deliveryNoteManager;
        this.salesVolumeReportSender = salesVolumeReportSender;
    }

    public CreditNote create(Employee user, Invoice invoice, BookingType bookingType, boolean copyItems) {
        if (!(bookingType instanceof SalesCreditNoteType)) {
            throw new IllegalArgumentException("bookingType must implement "
                    + SalesCreditNoteType.class.getName()
                    + ". Found " + (bookingType == null ? "null" : bookingType.getClass().getName()));
        }
        if (log.isDebugEnabled()) {
            log.debug("create() invoked [invoice="
                    + (invoice == null ? "null" : invoice.getId())
                    + "]");
        }
        return invoices.add(user, invoice, (SalesCreditNoteType) bookingType, copyItems);
    }

    public PaymentAwareRecord create(Employee user, BranchOffice office,
			Customer customer, Long id, Date created, boolean historical)
			throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("create() invoked [company=" + office.getCompany().getId()
                    + ", customer=" + (customer == null ? "null" : customer.getId())
                    + "]");
        }
    	validateCreate(getCreditNotesDao().getRecordType(), id, created, historical);
        return getCreditNotesDao().create(user, office, customer, id, created, historical);
    }

    public CreditNote create(Employee user, CreditNote existing, boolean copyReference, Long userDefinedId, Date userDefinedDate, boolean historical) {
        return getCreditNotesDao().create(user, existing, copyReference, userDefinedId, userDefinedDate, historical);
    }

    public CreditNote createInvoiceRelation(Employee user, CreditNote record, Long invoiceId) {
        assert invoiceId != null && record != null && user != null;
        SalesCreditNote creditNote = (SalesCreditNote) getCreditNotesDao().load(record.getId());
        SalesInvoice invoice = (SalesInvoice) invoices.load(invoiceId);
        creditNote.updateReference(invoice);
        getDao().save(creditNote);
        return creditNote;
    }

    public void changeSalesCommissionStatus(CreditNote creditNote) {
        if (!(creditNote instanceof SalesCreditNote)) {
            throw new BackendException("unsupported type "
                    + creditNote.getClass().getName()
                    + " expected an instanceof SalesCreditNote");
        }
        SalesCreditNote obj = (SalesCreditNote) creditNote;
        obj.setReducingCommission(!obj.isReducingCommission());
        getDao().save(obj);
    }

    public void toggleDeliveryRequiredFlag(CreditNote creditNote) {
        creditNote.toggleDeliveryNoteCreation();
        getDao().save(creditNote);
    }

    public void toggleDeliveryBooking(Employee user, CreditNote creditNote) {
        creditNote.toggleDeliveryNoteCreation();
        getDao().save(creditNote);
        if (creditNote.isDeliveryExisting()) {
        	Record record = deliveryNoteManager.createRollin(user, creditNote);
        	if (record != null) {
        	    try {
        	        deliveryNoteManager.updateStatus(
                		record,
                        user,
                        Record.STAT_SENT);
        	    } catch (ClientException c) {
        	        log.warn("doAfterStatusUpdate() ignoring error: " + c.toString(), c);
        	    }
        	}

        } else {
        	deliveryNoteManager.removeRollin(user, creditNote);
        }
        createProductPlanningRefreshTask(creditNote, null);
    }

    @Override
    public void updateStatus(Record record, Employee employee, Long newStatus)
            throws ClientException {
        if (!Record.STAT_CANCELED.equals(newStatus)) {
            if (!isSet(record.getAmounts().getGrossAmount())) {
                throw new ClientException(ErrorCode.DISCOUNTABLE);
            } else if (record.getAmounts().getGrossAmount().doubleValue() < 0) {
                throw new ClientException(ErrorCode.DISCOUNT_NOT_DISCOUNTABLE);
            }
        }
        if (newStatus.equals(Record.STAT_BOOKED)) {
            if (record.getStatus().longValue() < Record.STAT_SENT.longValue()) {
                throw new ClientException(ErrorCode.RECORD_NOT_PRINTED);
            }
        }
        super.updateStatus(record, employee, newStatus);
    }

    @Override
    protected void doAfterStatusUpdate(Record record, Employee employee, Long oldStatus) {
        if (log.isDebugEnabled()) {
            log.debug("doAfterStatusUpdate() invoked [id="
                    + record.getId() + ", status=" + record.getStatus()
                    + ", reference=" + record.getReference()
                    + "]");
        }
        if (oldStatus < Record.STAT_SENT
                && record.getStatus() == Record.STAT_SENT
                && record.getReference() != null) {
            Invoice invoice = (Invoice) invoices.load(record.getReference());
            BigDecimal due = invoice.getDueAmount();
            if (due == null) {
                log.warn("doAfterStatusUpdate() found invoice " + invoice.getId()
                        + " with due amount NULL!");
                due = new BigDecimal(0);
            }
            if (log.isDebugEnabled()) {
                log.debug("doAfterStatusUpdate() invoice "
                        + record.getId()
                        + " has calculated due "
                        + due);
            }
            if (due.doubleValue() >= -0.01 && due.doubleValue() <= 0.01) {
                invoice.updateStatus(Invoice.STAT_CLOSED);
                this.invoices.save(invoice);
                if (log.isDebugEnabled()) {
                    log.debug("doAfterStatusUpdate() invoice marked as closed");
                }
            }
        }
        if (justReleased(record, oldStatus)) {
            SalesCreditNote note = (SalesCreditNote) record;
            Record dn = deliveryNoteManager.createRollin(employee, note);
            if (dn != null) {
                if (log.isDebugEnabled()) {
                    log.debug("doAfterStatusUpdate() done [user=" + employee.getId()
                            + ", deliveryNote="
                            + dn.getId()
                            + ", creditNote="
                            + record.getId()
                            + "]");
                }
                try {
                    this.deliveryNoteManager.updateStatus(
                            this.deliveryNoteManager.find(dn.getId()),
                            employee,
                            Record.STAT_SENT);
                } catch (ClientException c) {
                    log.warn("doAfterStatusUpdate() ignoring error: " + c.toString(), c);
                }
            }
            if (salesVolumeReportSender != null) {
                salesVolumeReportSender.send(note);
            }

        } else if (log.isDebugEnabled()) {
            log.debug("doAfterStatusUpdate() nothing to do");
        }
    }

    private SalesCreditNotes getCreditNotesDao() {
        return (SalesCreditNotes) getDao();
    }

}
