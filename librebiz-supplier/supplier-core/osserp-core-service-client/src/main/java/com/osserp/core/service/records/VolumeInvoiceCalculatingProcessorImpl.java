/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 30, 2009 1:20:32 AM 
 * 
 */
package com.osserp.core.service.records;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.core.Item;
import com.osserp.core.dao.ProductPrices;
import com.osserp.core.dao.Products;
import com.osserp.core.dao.Projects;
import com.osserp.core.dao.records.SalesInvoices;
import com.osserp.core.dao.records.SalesOrderVolumeExports;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.Records;
import com.osserp.core.finance.VolumeInvoiceProcessor;
import com.osserp.core.sales.SalesInvoice;
import com.osserp.core.sales.SalesInvoiceRebateManager;
import com.osserp.core.sales.SalesOrderManager;
import com.osserp.core.sales.SalesOrderVolumeExport;
import com.osserp.core.sales.SalesOrderVolumeExportUtil;
import com.osserp.core.system.SystemConfigManager;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class VolumeInvoiceCalculatingProcessorImpl extends AbstractVolumeInvoiceProcessor implements VolumeInvoiceProcessor {
    private static Logger log = LoggerFactory.getLogger(VolumeInvoiceDefaultProcessorImpl.class.getName());

    private ProductPrices productPrices;

    protected VolumeInvoiceCalculatingProcessorImpl(
            Products products,
            ProductPrices productPrices,
            Projects projects,
            SalesOrderManager orderManager,
            SalesOrderVolumeExports exportsArchive,
            SalesInvoices invoices,
            SalesInvoiceRebateManager invoiceRebateManager,
            SystemConfigManager systemConfigManager,
            String volumeSeparatorProduct) {

        super(products, projects, orderManager, exportsArchive,
                invoices, invoiceRebateManager, systemConfigManager, volumeSeparatorProduct);
        this.productPrices = productPrices;
    }

    @Override
    protected List<Item> addOrders(SalesOrderVolumeExport export, SalesInvoice invoice, List<Order> orders) {
        SalesOrderVolumeExports archive = getExportsArchive();
        Set<Long> exportedDeliveries = archive.getExportedDeliveries();
        List<Item> result = new ArrayList<Item>();
        for (int i = 0, j = orders.size(); i < j; i++) {
            Order order = orders.get(i);
            List<Item> added = addOrder(archive, export, invoice, order, exportedDeliveries);
            result.addAll(added);
        }
        return result;
    }

    private List<Item> addOrder(SalesOrderVolumeExports archive, SalesOrderVolumeExport export, SalesInvoice invoice, Order order, Set<Long> exportedDeliveries) {
        List<Item> items = SalesOrderVolumeExportUtil.createUnbilledItems(export.getConfig(), exportedDeliveries, order);
        if (!items.isEmpty()) {
            archive.addItem(export, order);
            Set<Long> addedDeliveries = new HashSet<Long>();
            addOrderSeparator(invoice, order, order.getReference());
            for (int i = 0, j = items.size(); i < j; i++) {
                Item item = items.get(i);
                Double pp = productPrices.getPartnerPrice(item.getProduct(), order.getCreated(), item.getQuantity());
                BigDecimal price = pp == 0 ? item.getPrice() : new BigDecimal(pp.toString());
                item.setPrice(price);

                invoice.addItem(
                        invoice.getCompany(),
                        item.getProduct(),
                        item.getCustomName(),
                        item.getQuantity(),
                        item.getPrice(),
                        Records.createNumber(order.getType(), 
                                item.getReference(), order.isInternal()),
                        true,
                        (item.getPrice().doubleValue() == 0 ? true : false),
                        item.getAccountingReferenceId(),
                        item.getAccountingReferenceDate());
                if (!addedDeliveries.contains(item.getReference())) {
                    addedDeliveries.add(item.getReference());
                }
            }
            archive.addDeliveries(export, order, new ArrayList<Long>(addedDeliveries));
        }
        if (log.isDebugEnabled()) {
            log.debug("addOrder() done [order=" + order.getId() + "]");
        }
        persist(invoice);
        return items;
    }
}
