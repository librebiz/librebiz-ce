/**
 * osserp-core - records.js
 * 
 * Copyright 2001-2021 The original author or authors.
 * Licensed under the Apache License, Version 2.0.
 */

function setMethod(name) {
  var form = document.forms[0];
  form.method.value = name;
  return true;
}

function isNumeric(sText) {
  var validChars = "0123456789.";
  var isNumber = true;
  var Char;
  for (var i=0; i < sText.length && isNumber == true; i++) {
    Char = sText.charAt(i);
    if (validChars.indexOf(Char) == -1) {
      isNumber = false;
    }
  }
  return isNumber;
}

function replaceToken(value, token, replaceWith) {
  return value.replace(token, replaceWith);
}

function replaceCommaWithDot(value) {
  var token = /,/gi;
  return replaceToken(value, token, ".");
}

function replaceDotWithComma(value) {
  var token = /\./gi;
  return replaceToken(value, token, ",");
}

function removeAll(value, token) {
  var test = value.indexOf(token);
  while (test > -1) {
    var s = value.substr(0,test);
    var e = value.substring(test + 1, value.length);
    value = s + e;
    test = value.indexOf(token);
  }
  return value;
}


function createDouble(value) {
	var rc = value.lastIndexOf(",");
	var lc = value.indexOf(",");
	var rd = value.lastIndexOf(".");
	var ld = value.indexOf(".");
	
	if (rc < 0 && rd < 0) {           
		return new Number(value);
		
	} else if (rc >= 0 && rd < 0) {   
		if (lc != rc) {           
			return new Number(0);
		} else {
			return new Number(replaceCommaWithDot(value));
		}
	} else if (rc < 0 && rd >= 0) {   
		if (ld != rd) {           
			return new Number(0);
		} else {
			return new Number(value);
		}
	} else {											  	
		if (rc < rd) {            
			return new Number(removeAll(value, ","));
			
		} else {                      	
			var noDot = removeAll(value, ".");
			return new Number(replaceCommaWithDot(noDot));
		}
	} 
	return new Number(0);
}
