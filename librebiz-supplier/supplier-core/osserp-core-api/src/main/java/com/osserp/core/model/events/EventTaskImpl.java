/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.model.events;

import java.util.ArrayList;
import java.util.List;

import com.osserp.common.beans.OptionImpl;
import com.osserp.core.events.EventAction;
import com.osserp.core.events.EventTask;

/**
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class EventTaskImpl extends OptionImpl implements EventTask {

    private List<EventAction> actions = new ArrayList<EventAction>();

    /**
     * Default constructor required to implement serializable
     */
    protected EventTaskImpl() {
        super();
    }

    /**
     * Constructor for new event tasks
     * @param name
     */
    public EventTaskImpl(String name) {
        super(null, name);
    }

    public List<EventAction> getActions() {
        return actions;
    }

    public void addAction(EventAction action) {
        boolean alreadyExists = false;
        for (int i = 0, j = actions.size(); i < j; i++) {
            EventAction next = actions.get(i);
            if (next.getId().equals(action.getId())) {
                alreadyExists = true;
                break;
            }
        }
        if (!alreadyExists) {
            actions.add(action);
        }
    }

    public void removeAction(Long id) {
        for (java.util.Iterator<EventAction> i = actions.iterator(); i.hasNext();) {
            EventAction next = i.next();
            if (next.getId().equals(id)) {
                i.remove();
                break;
            }
        }
    }

    protected void setActions(List<EventAction> actions) {
        this.actions = actions;
    }
}
