/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 2, 2006 10:56:23 AM 
 * 
 */
package com.osserp.gui.client.contacts.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.Constants;
import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionException;
import com.osserp.common.User;
import com.osserp.common.UserManager;
import com.osserp.common.dms.DmsDocument;
import com.osserp.common.dms.DmsManager;
import com.osserp.common.dms.DmsReference;
import com.osserp.common.util.DateUtil;
import com.osserp.common.web.Form;
import com.osserp.common.web.ViewName;

import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.Person;
import com.osserp.core.contacts.PersonManager;
import com.osserp.core.employees.Employee;
import com.osserp.core.employees.EmployeeDescription;
import com.osserp.core.employees.EmployeeDisciplinarian;
import com.osserp.core.employees.EmployeeManager;
import com.osserp.core.employees.EmployeeSearch;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.users.DomainUser;

import com.osserp.gui.client.contacts.EmployeeView;
import com.osserp.gui.client.contacts.PersonView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("contactView")
public class EmployeeViewImpl extends AbstractRelatedContactView implements EmployeeView {
    private static Logger log = LoggerFactory.getLogger(EmployeeViewImpl.class.getName());

    private boolean internetPhoneSelectionMode = false;
    private boolean recordPhoneSelectionMode = false;
    private boolean userAccountAvailable = false;
    private boolean businessCardEditMode = false;
    private DmsDocument picture = null;
    private String pictureHeight = "128px";
    private boolean descriptionEditMode = false;
    private List<EmployeeDisciplinarian> disciplinarians = new ArrayList<EmployeeDisciplinarian>();

    /**
     * Default constructor needed to implement serializable Do not use for normal operation!
     */
    protected EmployeeViewImpl() {
        super();
        if (log.isDebugEnabled()) {
            log.debug("<init> done");
        }
    }

    @Override
    public void load(Person person) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("load() invoked by person");
        }
        super.load(person);
        Employee employee = getEmployee();
        this.init(employee);
    }

    @Override
    public void load(PersonView view) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("load() invoked by view");
        }
        super.load(view);
        Employee employee = getEmployee();
        this.init(employee);
    }

    @Override
    public void load(Long eid) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("load() invoked by id [value=" + eid + "]");
        }
        Long lookup = eid;
        try {
            Employee employee = null;
            if (lookup.longValue() < Constants.SYSTEM_EMPLOYEE) {
                employee = getEmployeeByUser(lookup);
            } else {
                employee = getEmployee(lookup);
            }
            setBean(employee);
            this.init(employee);
        } catch (Throwable t) {
            throw new ActionException(t.getMessage(), t);
        }
    }

    @Override
    public boolean isRelatedContactView() {
        return true;
    }

    @Override
    public boolean isEmployeeView() {
        return true;
    }

    @Override
    public void disableAllModes() {
        super.disableAllModes();
        this.internetPhoneSelectionMode = false;
        this.recordPhoneSelectionMode = false;
        this.descriptionEditMode = false;
    }

    @Override
    public void updateDetails(Form form) throws ClientException, PermissionException {
        setForm(form);
        Employee selected = getEmployee();
        EmployeeManager manager = getEmployeeManager();
        BranchOffice branch = null;
        Long branchId = form.getLong("branchId");
        if (isSet(branchId)) {
            branch = fetchBranchOffice(branchId);
        }
        selected = manager.updateDetails(
                getDomainEmployee(),
                selected,
                branch,
                form.getLong("typeId"),
                form.getDate("beginDate"),
                form.getDate("endDate"),
                form.getString("initials"),
                form.getString("role"),
                form.getString("costCenter"),
                form.getString("signatureSource"),
                form.getBoolean("businessCard"),
                form.getBoolean("internationalBusinessCard"),
                form.getBoolean("cellularPhone"),
                form.getBoolean("emailAccount"),
                form.getBoolean("timeRecordingEnabled"));        
        refresh();
        disableAllModes();
    }

    public boolean isDescriptionEditMode() {
        return descriptionEditMode;
    }

    public void setDescriptionEditMode(boolean descriptionEditMode) {
        this.descriptionEditMode = descriptionEditMode;
    }

    public void updateDescription(Form form) throws ClientException, PermissionException {
        setForm(form);
        Employee selected = getEmployee();
        String description = form.getString(Form.DESCRIPTION);
        String name = form.getString(Form.JOB);
        selected.updateDescription(
                getDomainEmployee(),
                1L,
                name,
                description,
                true);
        EmployeeManager manager = getEmployeeManager();
        manager.save(selected);
        refresh();
        disableAllModes();
        /*
         * if (selected.isInternetExportEnabled()) { if (log.isDebugEnabled()) {
         * log.debug("updateDescription() internet export flag enabled, syncing employee with server"); } manager.synchronizeWebsiteInfo(getEmployee()); }
         */
    }

    public void setActived() {
        EmployeeSearch search = getEmployeeSearch();
        List<Employee> active = search.findAllActive();
        setList(active);
    }

    public void changeActivationStatus() {
        Employee selected = getEmployee();
        selected.setActive(!selected.isActive());
        EmployeeManager manager = getEmployeeManager();
        manager.save(selected);
    }

    public void changeWebsiteDisplay() throws ClientException {
        EmployeeManager manager = getEmployeeManager();
        Employee selected = getEmployee();
        boolean previousInternetExportStatus = selected.isInternetExportEnabled();
        selected.setInternetExportEnabled(!previousInternetExportStatus);
        if (selected.isInternetExportEnabled()) {
            if (picture == null) {
                selected.setInternetExportEnabled(false);
                manager.save(selected);
                refresh();
                throw new ClientException(ErrorCode.INTERNETEXPORT_PICTURE_MISSING);
            }
            EmployeeDescription desc = selected.getDescription();
            if (desc == null || isNotSet(desc.getDescription())) {
                selected.setInternetExportEnabled(false);
                manager.save(selected);
                refresh();
                throw new ClientException(ErrorCode.INTERNETEXPORT_DESCRIPTION_MISSING);
            }
        }
        selected.setChanged(DateUtil.getCurrentDate());
        selected.setChangedBy(getDomainEmployee().getId());
        manager.save(selected);
        if (selected.isInternetExportEnabled() != previousInternetExportStatus) {
            if (log.isDebugEnabled()) {
                log.debug("updateDetails() internet export flag changed");//, syncing employee with server"); 
            }
            //manager.synchronizeWebsiteInfo(getEmployee());
        }
    }

    public boolean isRecordPhoneSelectionMode() {
        return recordPhoneSelectionMode;
    }

    public void setRecordPhoneSelectionMode(boolean recordPhoneSelectionMode) {
        disableAllModes();
        this.recordPhoneSelectionMode = recordPhoneSelectionMode;
    }

    public void changePhoneForRecord(Long deviceId) {
        if (log.isDebugEnabled()) {
            log.debug("changePhoneForRecord() invoked for device " + deviceId);
        }
        Employee employee = getEmployee();
        if (log.isDebugEnabled()) {
            log.debug("changePhoneForRecord() fetched selected employee "
                    + employee.getId());
        }
        EmployeeManager manager = getEmployeeManager();
        employee.setPrintPhoneDevice(deviceId);
        manager.save(employee);
        disableAllModes();
    }

    public boolean isInternetPhoneSelectionMode() {
        return internetPhoneSelectionMode;
    }

    public void setInternetPhoneSelectionMode(boolean internetPhoneSelectionMode) {
        disableAllModes();
        this.internetPhoneSelectionMode = internetPhoneSelectionMode;
    }

    public void changePhoneForInternet(Long phoneId) {
        if (log.isDebugEnabled()) {
            log.debug("changePhoneForInternet() invoked [id=" + phoneId + "]");
        }
        Employee employee = getEmployee();
        if (log.isDebugEnabled()) {
            log.debug("changePhoneForInternet() fetched selected employee "
                    + employee.getId());
        }
        EmployeeManager manager = getEmployeeManager();
        employee.setInternetPhoneId(phoneId);
        manager.save(employee);
        disableAllModes();
    }

    public boolean isBusinessCardEditMode() {
        return businessCardEditMode;
    }

    public void setBusinessCardEditMode(boolean businessCardEditMode) {
        if (businessCardEditMode) {
            // load business card
        }
        this.businessCardEditMode = businessCardEditMode;
    }
    
    public List<BranchOffice> getAvailableBranchs() {
        return getSystemConfigManager().getActivatedBranchs();
    }

    public Employee getEmployee() {
        if (getBean() == null) {
            if (log.isDebugEnabled()) {
                log.debug("getEmployee() failed, no activated employee!");
            }
            throw new ActionException("no activated employee!");
        }
        return (Employee) getBean();
    }

    public List<EmployeeDisciplinarian> getDisciplinarians() {
        return disciplinarians;
    }

    public int getDisciplinariansCount() {
        return disciplinarians.size();
    }

    public void setDefaultDisciplinarian(Long disciplinarianId) {
        Employee employee = getEmployee();
        employee.setDefaultDisciplinarian(disciplinarianId);
        EmployeeManager manager = getEmployeeManager();
        manager.save(employee);
    }

    public DmsDocument getPicture() {
        return picture;
    }

    protected void setPicture(DmsDocument picture) {
        this.picture = picture;
    }

    public String getPictureHeight() {
        return pictureHeight;
    }

    public boolean isUserAccountAvailable() {
        return userAccountAvailable;
    }

    protected void setUserAccountAvailable(boolean userAccountAvailable) {
        this.userAccountAvailable = userAccountAvailable;
    }

    @Override
    public void setSelection(Long id) {
        super.setSelection(id);
        Employee employee = getEmployee();
        this.init(employee);
    }

    private Employee getEmployeeByUser(Long userId) {
        if (log.isDebugEnabled()) {
            log.debug("getEmployeeByUser() invoked for user id " + userId);
        }
        if (userId == null) {
            return null;
        }
        try {
            UserManager manager = getDomainUserManager();
            User user = manager.findById(userId);
            if (user.isUserEmployee()) {
                return ((DomainUser) user).getEmployee();
            }
        } catch (Exception ignorable) {
        }
        return null;
    }

    @Override
    protected PersonManager getPersonManager() {
        return getEmployeeManager();
    }

    @Override
    protected String getDefaultDisplayGroup() {
        return Contact.EMPLOYEE_KEY;
    }

    private void init(Employee employee) {
        setUserAccount(getDomainUserManager().find(employee));
        if (getUserAccount() != null) {
            this.userAccountAvailable = true;
        }
        loadDisciplinarians(employee);
        loadPictures(employee);
    }

    @Override
    public void refresh() {
        super.refresh();
        Employee employee = getEmployee();
        setUserAccount(getDomainUserManager().find(employee));
        if (getUserAccount() != null) {
            this.userAccountAvailable = true;
            if (log.isDebugEnabled()) {
                log.debug("refresh() done [employee=" + employee.getId()
                        + ", user=" + getUserAccount().getId() + "]");
            }
        }
        loadDisciplinarians(employee);
        loadPictures(employee);
    }

    private void loadPictures(Employee employee) {
        if (employee != null) {
            try {
                DmsManager picManager = getPictureManager();
                picture = picManager.getReferenceDocument(new DmsReference(Employee.PICTURE, employee.getId()));
                String thumbHeight = getSystemProperty("dmsListThumbnailHeight");
                if (thumbHeight != null) {
                    pictureHeight = thumbHeight;
                }
            } catch (ClientException t) {
                // existing ref now disabled
                picture = null;
            } catch (Throwable t) {
                log.warn("loadPictures() failed on attempt to load pictures [message=" + t.getMessage() + "]");
            }
        }
    }

    private DmsManager getPictureManager() {
        return (DmsManager) getService(DmsManager.class.getName());
    }

    public boolean isBranchExecutiveFromSelected() {
        if (getDomainUser().isBranchExecutive()) {
            if (getEmployee().getBranch().getId().equals(getDomainEmployee().getBranch().getId())) {
                return true;
            }
            return false;
        }
        return false;
    }

    public boolean isDisciplinarianFromSelected() {
        return getEmployeeManager().isDisciplinarian(getEmployee(), getDomainEmployee());
    }

    private void loadDisciplinarians(Employee employee) {
        if (employee != null) {
            EmployeeManager manager = getEmployeeManager();
            this.disciplinarians = manager.findDisciplinarians(getEmployee());
        }
    }
}
