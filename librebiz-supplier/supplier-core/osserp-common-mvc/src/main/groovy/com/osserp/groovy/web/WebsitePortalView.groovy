/**
 *
 * Copyright (C) 2017 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 *
 * Created on Mar 10, 2017 
 *
 */
package com.osserp.groovy.web

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.PermissionException
import com.osserp.common.User
import com.osserp.common.gui.Menu
import com.osserp.common.gui.MenuProvider
import com.osserp.common.gui.service.JsonMenuProvider
import com.osserp.common.util.StringUtil
import com.osserp.common.web.AbstractPortalView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class WebsitePortalView extends AbstractPortalView {
    private static Logger logger = LoggerFactory.getLogger(WebsitePortalView.class.getName())
    
    Menu dropdownMenu
    Menu topMenu

    @Override
    protected MenuProvider getMenuProvider() {
        new JsonMenuProvider("/${StringUtil.replace(JsonMenuProvider.getName(), ".", "/")}.json")
    }

    /**
     * SSO login support actually not available
     * @param username
     * @param remoteIp
     * @return user
     * @throws PermissionException all the time by now
     */
    User login(String username, String remoteIp) throws PermissionException {
        throw new PermissionException();
    }
    
    User login(String loginName, String password, String remoteIp) throws PermissionException {
        // implement if required
        return null;
    }

    void logout() {
        // implement if required
    }
    
    public List<String> getSupportedLanguages() {
        ['de']
    }

    public String getStartupTarget() {
        null
    }

    public void updatePassword(String password, String confirmPassword) throws ClientException {
        // implement if required
    }

    List<User> getOnlineUsers() {
        []
    }

    boolean isExternalAuthentication() {
        false
    }

    public boolean isSystemSetupOnly() {
        false
    }

    public String getSystemSetupTarget() {
        null
    }

    public String getSystemSetupTargetForward() {
        null
    }

    public boolean isTimeRecordingRequired() {
        false
    }

    public boolean isPrivateContactAvailable() {
        false
    }

    @Override
    protected void initMenu() {
        super.initMenu()
        if (menus) {
            menus.each { Menu next ->
                if ('top' == next.name) {
                    topMenu = next
                    logger.debug('initMenu: top menu assigned')
                } else if ('dropdown' == next.name) {
                    dropdownMenu = next
                    logger.debug('initMenu: dropdown menu assigned')
                }
            }
        }
    }
}
