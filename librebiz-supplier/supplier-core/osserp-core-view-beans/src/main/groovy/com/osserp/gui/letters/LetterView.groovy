/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 26, 2008 12:19:33 AM 
 * 
 */
package com.osserp.gui.letters

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.dms.DocumentData

import com.osserp.core.contacts.ClassifiedContact
import com.osserp.core.dms.Letter
import com.osserp.core.dms.LetterContentManager
import com.osserp.core.dms.LetterManager
import com.osserp.core.dms.LetterParameter
import com.osserp.core.dms.LetterTemplate

import com.osserp.groovy.web.MenuItem

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class LetterView extends AbstractLetterView {
    private static Logger logger = LoggerFactory.getLogger(LetterView.class.getName())

    List<LetterTemplate> availableTemplates = []

    LetterView() {
        super()
        dependencies = ['letterReference']
        providesCreateMode()
        providesEditMode()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            if (letterReference) {
                selectedType = letterManager.getType(letterReference.letterTypeId)
                availableTemplates = letterTemplateManager.findTemplates(selectedType, false)
                reload()
            } else {
                logger.debug("initRequest: did not find letterReference")
            }
        }
    }

    @Override
    void save() {

        if (createMode) {
            logger.debug('save: invoked in create mode...')
            bean = createByForm()
            disableCreateMode()
            form.resetValue('name')
            enableEditMode()
        } else if (editMode) {
            logger.debug('save: invoked in edit mode...')

            String[] infos = form.getIndexedStrings('infos')
            if (infos) {
                letterReference.updateInfos(bean, infos)
            }
            bean = updateByForm()
            disableEditMode()

        } else if (parameterMode) {
            logger.debug('save: invoked in parameter mode...')
            updateParameters()
        } else if (selectedParagraph) {
            logger.debug('save: invoked in paragraph mode...')
            updateParagraph()
        }
        reload()
    }

    protected void updateParameters() {
        Map<String, String> values = []

        for (int i = 0; i < bean.getParameters().size(); i++) {
            LetterParameter parameter = bean.getParameters().get(i)
            String value = form.getString(parameter.getName())
            if (value) {
                values.put(parameter.getName(), value)
            }
        }
        letterManager.updateParameters(domainUser, bean, values)
        disableParameterMode()
    }

    protected Letter updateByForm() {
        letterManager.update(
                domainUser,
                bean,
                form.getString('header'),
                form.getString('name'),
                form.getString('street'),
                form.getString('zipcode'),
                form.getString('city'),
                form.getLong('country'),
                form.getString('subject'),
                form.getString('salutation'),
                form.getString('greetings'),
                form.getString('language'),
                (form.getLong('signatureLeft') ?: null),
                (form.getLong('signatureRight') ?: null),
                form.getDate('printDate'),
                form.getBoolean('ignoreSalutation'),
                form.getBoolean('ignoreGreetings'),
                form.getBoolean('ignoreHeader'),
                form.getBoolean('ignoreSubject'),
                form.getBoolean('embedded'),
                form.getBoolean('embeddedAbove'),
                form.getString('embeddedSpaceAfter'),
                getContentKeepTogetherValue())
        return letterManager.findLetter(bean.id)
    }

    protected Letter createByForm() throws ClientException {
        letterManager.create(
            domainUser,
            selectedType,
            letterReference.getRecipient(),
            form.getString('name'),
            letterReference.getBusinessId(),
            form.getLong('branch'),
            selectedTemplate)
    }

    Long createTemplate() {
        Letter letter = fetchLetter()
        if (letter) {
            LetterTemplate tplNew = letterTemplateManager.create(domainUser, letter)
            logger.debug("createTemplate: done [id=${tplNew?.id}]")
            return tplNew?.id
        }
        return null
    }

    void delete() throws ClientException {
        if (bean) {
            letterManager.delete(domainUser, bean)
            bean = null
            reload()
        } else {
            logger.warn('delete: did not find bean to delete')
        }
    }

    void release() throws ClientException {
        if (bean) {
            letterManager.close(domainUser, bean)
            reload()
        }
    }

    DocumentData getPdf() throws ClientException {
        letterManager.getPdf(domainUser, fetchLetter())
    }

    DocumentData getTemplatePdf() throws ClientException {
        Long id = form.getLong('id')
        if (id) {
            LetterTemplate template = availableTemplates.find { it.id == id }
            if (template) {
                ClassifiedContact contact = letterReference.getRecipient()
                template.addAddress(contact?.address, contact?.salutation?.name, contact?.displayName)
                return letterTemplateManager.getPdf(domainUser, template)
            }
            logger.warn("getTemplatePdf: template not found [id=${id}]")
        } else {
            logger.warn("getTemplatePdf: no id provided")
        }
        return null
    }

    LetterReference getLetterReference() {
        env['letterReference']
    }

    @Override
    void createCustomNavigation() {
        super.createCustomNavigation()
        if (supportingTypeSelection && !selectedType) {
            nav.defaultNavigation = [exitLink, homeLink]
        }
        nav.editNavigation = [disableEditLink, printLink, homeLink]

        if (selectedParagraph) {
            nav.beanListNavigation = [deselectParagraphLink, homeLink]
        } else if (bean?.closed) {
            nav.beanListNavigation = [selectExitLink, printLink, createTemplateLink, homeLink]
        } else {
            nav.beanListNavigation = [
                selectExitLink,
                printLink,
                closeLetterLink,
                createParagraphLink,
                deleteLetterLink,
                editLink,
                homeLink
            ]
        }
    }

    /**
     * Provides the letter release link	
     * @return letterReleaseLink
     */
    protected MenuItem getCloseLetterLink() {
        new MenuItem(link: "${context.fullPath}/${context.name}/release", icon: 'enabledIcon', title: 'recordRelease', confirmLink: true)
    }

    /**
     * Provides the letter release link	
     * @return letterReleaseLink
     */
    protected MenuItem getCreateTemplateLink() {
        new MenuItem(link: "${context.fullPath}/${context.name}/createTemplate", icon: 'copyIcon', title: 'createTemplateByLetter', confirmLink: true)
    }

    /**
     * Provides the deleteLetter link	
     * @return deleteLetterLink
     */
    protected MenuItem getDeleteLetterLink() {
        new MenuItem(link: "${context.fullPath}/${context.name}/delete", icon: 'deleteIcon', title: 'deleteLetter', confirmLink: true)
    }

    @Override
    void reload() {
        if (letterReference) {
            list = letterManager.findLetters(selectedType, letterReference.recipient, letterReference.businessId)
            if (bean) {
                bean = list.find { it.id == bean.id }
            }
        }
        createCustomNavigation()
    }

    @Override
    void select() {
        super.select()
        createCustomNavigation()
    }

    boolean isParameterMode() {
        env.parameterMode
    }

    void disableParameterMode() {
        env.parameterMode = false
    }

    void enableParameterMode() {
        env.parameterMode = true
    }

    @Override
    Long getCurrentBranch() {
        letterReference?.branchId ?: super.getCurrentBranch()
    }

    protected Letter fetchLetter() {
        Letter letter
        Long id = form.getLong('id')
        if (id) {
            letter = letterManager.find(id)
        } else {
            letter = bean
        }
        return letter
    }

    @Override
    protected String getPrintLetterTitle() {
        'printLetterTitle'
    }

    @Override
    protected LetterContentManager getLetterContentManager() {
        letterManager
    }

    protected LetterManager getLetterManager() {
        getService(LetterManager.class.getName())
    }
}
