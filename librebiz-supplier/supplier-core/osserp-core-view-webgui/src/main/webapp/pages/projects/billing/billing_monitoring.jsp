<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.billingMonitoringView}">
    <c:redirect url="/errors/error_context.jsp"/>
</c:if>
<c:set var="view" value="${sessionScope.billingMonitoringView}"/>
<c:set var="selectUrl" scope="request" value="/loadSales.do"/>
<c:set var="selectExitTarget" scope="request" value="salesBillingMonitoring"/>
<c:set var="collection" scope="request" value="${view.list}"/>
<c:set var="sortUrl" scope="request" value="/salesBillingMonitoring.do"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title"><o:displayTitle/></tiles:put>
    <tiles:put name="headline"><o:listSize value="${collection}"/> <fmt:message key="ordersFound"/></tiles:put>
    <tiles:put name="headline_right">
       <ul>
           <li><a href="<c:url value="/salesBillingMonitoring.do?method=exit"/>"><o:img name="backIcon"/></a></li>
           <li><v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link></li>
        </ul>
    </tiles:put>
    <tiles:put name="content" type="string">
        <div class="content-area" id="byActionListContent">
            <div class="row">
                <c:choose>
                    <c:when test="${empty view.list}">
                        <div class="recordheader" style="margin-left: 20px;">
                            <p class="errortext"><fmt:message key="adviceNoActualOrdersFound"/></p>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <c:if test="${!empty sessionScope.errors}">
                            <div class="recordheader">
                                <p class="errortext"><fmt:message key="error"/>: <o:out value="${sessionScope.errors}"/></p>
                            </div>
                            <o:removeErrors/>
                        </c:if>
                        <c:choose>
                            <c:when test="${!empty sessionScope.collection}">
                                <c:set var="list" value="${sessionScope.collection}"/>
                            </c:when>
                            <c:otherwise>
                                <c:set var="list" value="${requestScope.collection}"/>
                            </c:otherwise>
                        </c:choose>
                        <div class="col-lg-12 panel-area">
                            <div class="table-responsive table-responsive-default">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th nowrap="nowrap">
                                                <a href="<c:url value="${requestScope.sortUrl}"><c:param name="method" value="sort"/><c:param name="order" value="byId"/></c:url>"><fmt:message key="number"/></a>
                                            </th>
                                            <th nowrap="nowrap">
                                                <a href="<c:url value="${requestScope.sortUrl}"><c:param name="method" value="sort"/><c:param name="order" value="byName"/></c:url>"><fmt:message key="commission"/></a>
                                            </th>
                                            <th class="right" nowrap="nowrap">
                                                <a href="<c:url value="${requestScope.sortUrl}"><c:param name="method" value="sort"/><c:param name="order" value="byCapacity"/></c:url>">Eh</a>
                                            </th>
                                            <th class="center" nowrap="nowrap">
                                                <a href="<c:url value="${requestScope.sortUrl}"><c:param name="method" value="sort"/><c:param name="order" value="byBranch"/></c:url>"><fmt:message key="branchOfficeShortcut"/></a>
                                            </th>
                                            <th class="center" nowrap="nowrap">
                                                <a href="<c:url value="${requestScope.sortUrl}"><c:param name="method" value="sort"/><c:param name="order" value="bySales"/></c:url>"><fmt:message key="salesShortcut"/></a>
                                            </th>
                                            <th class="center" nowrap="nowrap">
                                                <a href="<c:url value="${requestScope.sortUrl}"><c:param name="method" value="sort"/><c:param name="order" value="byManager"/></c:url>"><fmt:message key="projectorShort"/></a>
                                            </th>
                                            <th class="center" nowrap="nowrap">
                                                <a href="<c:url value="${requestScope.sortUrl}"><c:param name="method" value="sort"/><c:param name="order" value="byCreated"/></c:url>"><fmt:message key="order"/></a>
                                            </th>
                                            <th colspan="4" class="center" nowrap="nowrap">
                                                <a href="<c:url value="${requestScope.sortUrl}"><c:param name="method" value="sort"/><c:param name="order" value="byStatus"/></c:url>"><fmt:message key="status"/></a>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="sales" items="${list}"  varStatus="s">
                                            <c:set var="nextSelectionUrl"><c:url value="${requestScope.selectUrl}"><c:if test="${!empty selectExitTarget}"><c:param name="exit" value="${selectExitTarget}"/></c:if><c:if test="${!empty method}"><c:param name="method" value="${method}"/></c:if><c:param name="id" value="${sales.id}"/></c:url></c:set>
                                            <tr<c:if test="${sales.stopped}"> class="red"</c:if>>
                                                <td>
                                                    <a href="<o:out value="${nextSelectionUrl}"/>" title="<o:out value="${sales.type.name}"/>">
                                                        <span<c:if test="${sales.status < 15}"> style="font-style:italic;"</c:if>><o:out value="${sales.id}"/></span>
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="<o:out value="${nextSelectionUrl}"/>" title="<o:out value="${sales.customerName}"/>">
                                                        <span<c:if test="${sales.status < 15}"> style="font-style:italic;"</c:if>><o:out value="${sales.name}" limit="48"/></span>
                                                    </a>
                                                </td>
                                                <td align="right"><o:number value="${sales.capacity}" format="decimal"/></td>
                                                <td align="center"><o:out value="${sales.branchKey}"/></td>
                                                <td align="center">
                                                    <c:choose>
                                                        <c:when test="${!empty sales.salesId and sales.salesId > 0}">
                                                            <v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${sales.salesId}"><oc:employee initials="true" value="${sales.salesId}" /></v:ajaxLink>
                                                        </c:when>
                                                        <c:otherwise>
                                                            &nbsp;&nbsp;
                                                        </c:otherwise>
                                                    </c:choose>
                                                </td>
                                                <td class="center">
                                                    <c:choose>
                                                        <c:when test="${!empty sales.managerId and sales.managerId > 0}">
                                                            <v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${sales.managerId}"><oc:employee initials="true" value="${sales.managerId}" /></v:ajaxLink>
                                                        </c:when>
                                                        <c:otherwise>
                                                            &nbsp;&nbsp;
                                                        </c:otherwise>
                                                    </c:choose>
                                                </td>
                                                <td class="center"><o:date value="${sales.created}" casenull="-" addcentury="false"/></td>
                                                <td class="status right">
                                                    <v:ajaxLink url="/sales/flowControlDisplay/forward?id=${sales.id}" styleClass="boldtext" linkId="flowControlDisplayView">
                                                        <o:out value="${sales.status}"/>%
                                                    </v:ajaxLink>
                                                </td>
                                                <td class="icon right">
                                                    <o:permission role="notes_sales_deny" info="permissionNotes">
                                                        <v:ajaxLink url="/sales/businessCaseDisplay/forward?notes=true&id=${sales.id}" linkId="businessCaseDisplayView">
                                                            <o:img name="texteditIcon"/>
                                                        </v:ajaxLink>
                                                    </o:permission>
                                                </td>
                                                <td class="icon right">
                                                    <v:ajaxLink url="/sales/businessCaseDisplay/forward?id=${sales.id}" linkId="businessCaseDisplayView">
                                                        <o:img name="openFolderIcon"/>
                                                    </v:ajaxLink>
                                                </td>
                                                <td class="icon right">
                                                    <c:choose>
                                                        <c:when test="${!sales.released}">
                                                            <span title="<fmt:message key="orderNotReleased"/>"><o:img name="importantDisabledIcon"/></span>
                                                        </c:when>
                                                        <c:when test="${sales.deliveryClosed}">
                                                            <span title="<fmt:message key="materialPlanningFinished"/>"><o:img name="importantDisabledIcon"/></span>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <v:ajaxLink url="/sales/deliveryStatusDisplay/forward?id=${sales.id}" linkId="deliveryStatus" title="materialPlanning">
                                                                <o:img name="importantIcon"/>
                                                            </v:ajaxLink>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
