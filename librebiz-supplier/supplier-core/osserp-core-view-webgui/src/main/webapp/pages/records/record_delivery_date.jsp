<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<c:set var="view" value="${sessionScope.recordView}"/>
<c:set var="record" value="${view.record}"/>

<c:choose>
<c:when test="${view.deliveryDateEditMode}">
    <div class="recordLabelValue">
        
		<span class="boldtext"><fmt:message key="newDeliveryDay"/>:</span><br/>
		<o:form name="recordForm" url="${requestScope.baseUrl}">
			<input type="hidden" name="method" value="update" />
			<span><input type="radio" name="deliveryDateOnItems" value="allItems" style="margin-right: 10px;" checked="checked"/><fmt:message key="setForAllProducts"/></span>
			<br/>
			<span><input type="radio" name="deliveryDateOnItems" value="emptyItems" style="margin-right: 10px;"/><fmt:message key="productsWithoutDateOnly"/></span>
			<br/>
			<span><input type="radio" name="deliveryDateOnItems" value="resetItems" style="margin-right: 10px;"/><fmt:message key="deleteAppointments"/></span>
			<br/>
			<br/>
			<c:if test="${record.type.supportingDeliveryAck}">
				<span><input type="checkbox" name="deliveryConfirmed" style="margin-right: 10px;"/><fmt:message key="appointmentIsAcknowledged"/></span>
				<br/><br/>
			</c:if>
			<span title="<fmt:message key="internalLogisticsNote"/>"><textarea class="form-control" style="margin-left: 8px; width: 85%" name="deliveryNote"></textarea></span>
			<br/><br/>
			<span style="margin-left: 8px;"><input type="text" name="delivery" id="deliveryValue" class="rinput" style="width: 105px;" value="<o:date value="${record.delivery}"/>"/></span>&nbsp;
				<o:datepicker input="deliveryValue"/>
			<a href="<c:url value="${requestScope.baseUrl}"><c:param name="method" value="disableDeliveryDateEdit"/></c:url>" title="<fmt:message key="exitEdit"/>"><o:img name="backIcon" styleClass="bigicon"/></a>
			<input type="image" name="method" src="<c:url value="${applicationScope.saveIcon}"/>" value="updateDeliveryDate" onclick="setMethod('updateDeliveryDate');" style="margin: 0px;" class="bigicon" title="<fmt:message key="saveInput"/>"/>
			<br/><br/>
		</o:form>
	</div>
</c:when>
<c:otherwise>
	<c:if test="${!record.closed}">
    <div class="recordLabelValue">
        <c:choose><c:when test="${!empty view.openDeliveries}"><a href="<c:url value="${requestScope.baseUrl}"><c:param name="method" value="enableDeliveryDateEdit"/></c:url>" title="<fmt:message key="change"/>"><fmt:message key="nextDeliveryLabel"/></a></c:when><c:otherwise><fmt:message key="nextDeliveryLabel"/></c:otherwise></c:choose>:<br/>
        <c:choose>
            <c:when test="${empty record.delivery}">
                <fmt:message key="undefined"/>
            </c:when>
            <c:otherwise>
                <span title="KW <o:date format="week" value="${record.delivery}"/> / <o:date format="year" value="${record.delivery}"/>"><o:date value="${record.delivery}"/></span>
            </c:otherwise>
        </c:choose>
	</div>
	</c:if>
	<c:if test="${!empty record.deliveryNotes}">
    <div class="recordLabelValue">
		<span>
			<o:ajaxLink linkId="ajaxpopup" url="/app/records/deliveryDateHistory/forward?record=${record.id}"><fmt:message key="deliveryDatePopup"/></o:ajaxLink>
		</span>
	</div>
	</c:if>
</c:otherwise>
</c:choose>

