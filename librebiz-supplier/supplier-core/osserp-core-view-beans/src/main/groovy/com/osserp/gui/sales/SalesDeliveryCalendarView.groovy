/**
 *
 * Copyright (C) 2007, 2021 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License (AGPL)
 * version 3 as published by the Free Software Foundation. In accordance
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code
 * versions must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area
 * or, if not reasonably feasible for technical reason, as a top-level
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute.
 *
 */
package com.osserp.gui.sales

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.Appointment
import com.osserp.common.Calendar
import com.osserp.common.ClientException
import com.osserp.common.beans.DefaultAppointment
import com.osserp.common.util.CalendarUtil
import com.osserp.common.util.DateUtil
import com.osserp.common.util.StringUtil
import com.osserp.core.employees.EmployeeRole
import com.osserp.core.employees.EmployeeRoleConfig
import com.osserp.core.events.Event
import com.osserp.core.events.EventManager
import com.osserp.core.projects.ProjectSearch
import com.osserp.core.sales.SalesListProductItem
import com.osserp.core.users.DomainUser
import com.osserp.gui.common.CalendarAwareView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class SalesDeliveryCalendarView extends CalendarAwareView {
    private static Logger logger = LoggerFactory.getLogger(SalesDeliveryCalendarView.class.getName())

    SalesDeliveryCalendarView() {
        super()
        headerName = 'salesDeliveryCalendar'
        dayViewAvailable = true
        invocationTargetUrl = '/loadSales.do?id=@@&exit=salesDeliveryCalendarReload'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            daySelectionTargetUrl = "/sales/salesDeliveryCalendarDay/forward?exit=${baseLink}/reload"
        }
    }

    @Override
    protected List<Appointment> fetchAppointments() {
        List<Appointment> result = []
        List<SalesListProductItem> items = projectSearch.findDeliveries()
        items.each { SalesListProductItem item ->

            if (item.getAppointmentDate() != null) {
                String msg = "${item.quantity} - ${item.productId} - ${item.productName}"
                if (!sameDayAlreadyAdded(result, item)) {
                    Appointment ap = item.createAppointment(invocationTargetUrl, null)
                    ap.overrideDefaults(null, null, msg)
                    result.add(ap)
                } else {
                    updateSameDay(result, item, msg)
                }
            }
        }
        return result
    }

    private boolean sameDayAlreadyAdded(List<Appointment> result, SalesListProductItem item) {
        for (Iterator<Appointment> iterator = result.iterator(); iterator.hasNext();) {
            Appointment next = iterator.next();
            if (next.getId().equals(item.getId())) {
                if (DateUtil.isSameDay(next.getDate(), item.getAppointmentDate())) {
                    return true;
                }
            }
        }
        return false;
    }

    private void updateSameDay(List<Appointment> result, SalesListProductItem item, String message) {
        for (Iterator<Appointment> iterator = result.iterator(); iterator.hasNext();) {
            Appointment next = iterator.next();
            if (next.getId().equals(item.getId())) {
                if (DateUtil.isSameDay(next.getDate(), item.getAppointmentDate())) {
                    StringBuilder msg = new StringBuilder(next.message)
                    msg.append('<br/>').append(message)
                    next.overrideDefaults(null, null, msg.toString())
                }
            }
        }
    }

    public boolean isLogisticUserOnly() {
        DomainUser user = getDomainUser()
        List<EmployeeRoleConfig> configs = user.getEmployee().getRoleConfigs()
        boolean inLogistic = false;
        boolean inOtherGroups = false;
        configs.each { EmployeeRoleConfig nextConfig ->
            nextConfig.getRoles().each { EmployeeRole role ->
                if (role.getGroup().isLogistics()) {
                    inLogistic = true;
                } else {
                    inOtherGroups = true;
                }
            }
        }
        return (inOtherGroups ? false : inLogistic);
    }

    private ProjectSearch getProjectSearch() {
        return getService(ProjectSearch.class.getName())
    }
}
