/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 3, 2005 
 * 
 */
package com.osserp.common.web.tags;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import com.osserp.common.util.DateFormatter;
import com.osserp.common.util.DateUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractDateTag extends AbstractOutTag {

    protected String format = null;

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    protected boolean addtime = false;

    /**
     * Indicates wether the time should be added
     * @return returns true if so
     */
    public boolean isAddtime() {
        return addtime;
    }

    /**
     * Sets that the time should be added
     * @param addtime true if time should be added
     */
    public void setAddtime(boolean addtime) {
        this.addtime = addtime;
    }

    protected boolean addseconds = false;

    /**
     * Indicates wether the time should be added with seconds
     * @return returns true if so
     */
    public boolean isAddseconds() {
        return addseconds;
    }

    /**
     * Sets that the time should be added with seconds
     * @param addseconds true if time should be added with seconds
     */
    public void setAddseconds(boolean addseconds) {
        this.addseconds = addseconds;
    }

    protected String casenull = null;

    /**
     * Indicates wether the time should be added with seconds
     * @return returns true if so
     */
    public String getCasenull() {
        return casenull;
    }

    /**
     * Sets that the time should be added with seconds
     * @param casenull true if time should be added with seconds
     */
    public void setCasenull(String casenull) {
        this.casenull = casenull;
    }

    protected boolean addcentury = true;

    /**
     * Indicates wether the year should be displayed too. This is default
     * @return returns true if so
     */
    public boolean isAddcentury() {
        return addcentury;
    }

    /**
     * Sets that the time should be added with seconds
     * @param addcentury true if time should be added with seconds
     */
    public void setAddcentury(boolean addcentury) {
        this.addcentury = addcentury;
    }

    protected boolean current = false;

    /**
     * Indicates wether the year should be displayed too. This is default
     * @return returns true if so
     */
    public boolean isCurrent() {
        return current;
    }

    /**
     * Sets that the time should be added with seconds
     * @param current true if time should be added with seconds
     */
    public void setCurrent(boolean current) {
        this.current = current;
    }

    /**
     * Provides the time
     * @param date
     * @return time string (HH:mm or HH:mm:ss if addseconds is enabled)
     */
    protected final String getTime(Date date) {
        if (addseconds) {
            return DateFormatter.getTotalTime(date);
        }
        return DateFormatter.getTime(date);
    }

    /**
     * Provides the hours of time
     * @param date
     * @return hours string (HH)
     */
    protected final String getHours(Date date) {
        return DateFormatter.getHours(date);
    }

    /**
     * Provides the minutes of time
     * @param date
     * @return minutes string (mm)
     */
    protected final String getMinutes(Date date) {
        return DateFormatter.getMinutes(date);
    }

    protected Date convert(LocalDateTime dateToConvert) {
        return dateToConvert == null ? null :
            Date.from(dateToConvert.atZone(ZoneId.systemDefault())
          .toInstant());
    }

    protected Date dateFromValue() {
        if (value instanceof Date) {
            return (Date) value;
        }
        if (value instanceof LocalDateTime) {
            return convert((LocalDateTime) value);
        }
        if (value instanceof String) {
            return convert(DateUtil.parseIsoDateTime((String) value));
        }
        return null;
    }
}
