/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 28, 2007 10:55:35 AM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import com.osserp.common.OptionsCache;
import com.osserp.common.dao.AbstractTablesAwareSpringDao;
import com.osserp.common.dao.Tables;
import com.osserp.core.FcsItem;
import com.osserp.core.dao.FcsWastebasketRowMapper;
import com.osserp.core.dao.FcsWastebaskets;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractFcsWastebaskets extends AbstractTablesAwareSpringDao implements FcsWastebaskets {

    private String wastebasketTable = null;
    private OptionsCache cache = null;

    public AbstractFcsWastebaskets(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            OptionsCache cache,
            String wastebasketTableKey) {
        super(jdbcTemplate, tables);
        this.wastebasketTable = getTable(wastebasketTableKey);
        this.cache = cache;
    }

    protected abstract RowMapperResultSetExtractor getWastebasketReader();

    public Set<Long> getWaste(Long referenceId) {
        StringBuilder sql = new StringBuilder(96);
        sql
                .append("SELECT template_id FROM ")
                .append(wastebasketTable)
                .append(" WHERE project_id = ?");
        final Object[] params = { referenceId };
        final int[] types = { Types.BIGINT };
        List<Long> list = (List<Long>) jdbcTemplate.query(
                sql.toString(),
                params,
                types,
                getLongRowMapper());
        return new HashSet<Long>(list);
    }

    public void add(Long projectId, Long employeeId, Long templateId) {
        if (!isExisting(projectId, templateId)) {
            StringBuilder sql = new StringBuilder(96);
            sql
                    .append("INSERT INTO ")
                    .append(wastebasketTable)
                    .append(" (project_id,template_id,employee_id) VALUES (?,?,?)");
            final Object[] params = { projectId, templateId, employeeId };
            final int[] types = { Types.BIGINT, Types.BIGINT, Types.BIGINT };
            jdbcTemplate.update(sql.toString(), params, types);
        }
    }

    public void cleanup(Long reference) {
        StringBuilder sql = new StringBuilder(96);
        sql
                .append("DELETE FROM ")
                .append(wastebasketTable)
                .append(" WHERE project_id = ?");
        final Object[] params = { reference };
        final int[] types = { Types.BIGINT };
        jdbcTemplate.update(sql.toString(), params, types);
    }

    public Long remove(Long id) {
        Long tpid = getProjectValues(id);
        StringBuilder sql = new StringBuilder(96);
        sql
                .append("DELETE FROM ")
                .append(wastebasketTable)
                .append(" WHERE id = ?");
        final Object[] params = { id };
        final int[] types = { Types.BIGINT };
        jdbcTemplate.update(sql.toString(), params, types);
        return tpid;
    }

    public List<FcsItem> getWasteItems(Long projectId) {
        StringBuilder sql = new StringBuilder(96);
        sql
                .append(FcsWastebasketRowMapper.SELECT_FROM)
                .append(wastebasketTable)
                .append(" WHERE project_id = ? ORDER BY created");
        final Object[] params = { projectId };
        final int[] types = { Types.BIGINT };
        return (List<FcsItem>) jdbcTemplate.query(
                sql.toString(),
                params,
                types,
                getWastebasketReader());
    }

    private Long getProjectValues(Long id) {
        StringBuilder sql = new StringBuilder(96);
        sql
                .append("SELECT template_id FROM ")
                .append(wastebasketTable)
                .append(" WHERE id = ?");
        final Object[] params = { id };
        final int[] types = { Types.BIGINT };
        List<Long> result = (List<Long>) jdbcTemplate.query(
                sql.toString(),
                params,
                types,
                getLongRowMapper());
        return empty(result) ? null : result.get(0);
    }

    private boolean isExisting(Long projectId, Long templateId) {
        StringBuilder sql = new StringBuilder(96);
        sql
                .append("SELECT id FROM ")
                .append(wastebasketTable)
                .append(" WHERE project_id = ? AND template_id = ?");
        final Object[] params = { projectId, templateId };
        final int[] types = { Types.BIGINT, Types.BIGINT };
        List result = (List) jdbcTemplate.query(
                sql.toString(),
                params,
                types,
                getLongRowMapper());
        return (!empty(result));
    }

    protected RowMapperResultSetExtractor getProjectValuesReader() {
        return new RowMapperResultSetExtractor(new ProjectValuesRowMapper());
    }

    private class ProjectValuesRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int index) throws SQLException {
            return new Long[] {
                    Long.valueOf(rs.getLong(1)),
                    Long.valueOf(rs.getLong(2))
            };
        }
    }

    protected OptionsCache getCache() {
        return cache;
    }
}
