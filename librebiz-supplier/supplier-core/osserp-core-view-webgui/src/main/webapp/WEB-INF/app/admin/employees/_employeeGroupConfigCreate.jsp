<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="employeeGroupConfigView"/>
<v:form name="employeeGroupConfigForm" url="/admin/employees/employeeGroupConfig/save">
<div class="subcolumns">

	<div class="column50l">
		<div class="subcolumnl">

			<div class="spacer"></div>
			<div class="contentBox">
				<div class="contentBoxHeader">
					<div class="contentBoxHeaderLeft">
						<fmt:message key="createNewGroup"/>
					</div>
				</div>
				<div class="contentBoxData">
					<div class="subcolumns">
						<div class="subcolumn">
							<div class="spacer"></div>
							
							<table class="valueTable first33">
							
								<c:if test="${!empty sessionScope.error}">
									<tr>
										<td class="error"><fmt:message key="error"/></td>
										<td class="error"><fmt:message key="${sessionScope.error}"/></td>
									</tr>
                                    <o:removeErrors/>
								</c:if>
								<tr>
									<td><fmt:message key="uniqueName"/></td>
									<td><v:text name="name" value="${view.bean.lastName}"/></td>
								</tr>
								<tr>
									<td><fmt:message key="postalArea"/></td>
									<td><v:text name="key" value="${view.bean.key}" styleClass="width: 20%;"/></td>
								</tr>
								<tr>
									<td> </td>
									<td>
										<v:link url="/admin/employees/employeeGroupConfig/disableCreateMode" title="exitCreate"><o:img name="backIcon"/></v:link>
										<input type="image" name="submit" class="submitIcon" src="<c:url value="${applicationScope.saveIcon}"/>" value="save" title="<fmt:message key='saveInput'/>"/>
									</td>
								</tr>
							</table>

							<div class="spacer"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="spacer"></div>

		</div>
	</div>
</div>
</v:form>