/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Oct 27, 2010 11:18:29 AM 
 * 
 */
package com.osserp.gui.reporting.selectors

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException

import com.osserp.core.BusinessCase
import com.osserp.core.projects.ProjectManager

import com.osserp.groovy.web.View
import com.osserp.groovy.web.ViewContextException

import com.osserp.gui.CoreView

/**
 *
 * @author jg <jg@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public class SelectStatusPopupView extends CoreView  {
    private static Logger logger = LoggerFactory.getLogger(SelectStatusPopupView.class.getName())

    View queryView
    Long value
    boolean lower
    List statusList

    SelectStatusPopupView() {
        super()
        enablePopupView()
        headerName = 'selector.statusTitle'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (!queryView) {
            String viewName = form.getString('view')
            if (viewName && !env[viewName]) {
                logger.debug('initRequest: invalid context [message=query view not bound]')
                throw new ViewContextException(this)
            } else {
                queryView = env[viewName]
            }
            value = queryView.values.statusValue
            lower = queryView.values.statusLower
            statusList = projectManager.statusList
            if (!value) {
                value = BusinessCase.CLOSED
                lower = true
            }
        }
    }

    void save() {
        Long statusLower = form.getLong('lower')
        queryView.values.statusLower = statusLower ? true : false

        Long statusValue = form.getLong('value')
        if (statusValue && (statusValue <= 0 || statusValue > 100)) {
            throw new ClientException("statusNotInRange")
        }
        queryView.values.statusValue = statusValue
        queryView.reload()
    }

    private ProjectManager getProjectManager() {
        getService(ProjectManager.class.getName())
    }
}
