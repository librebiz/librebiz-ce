/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Jan 30, 2010 11:17:48 AM 
 * 
 */
package com.osserp.core.planning;

import java.util.Date;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author cf <cf@osserp.com>
 * 
 */
public interface ProductPlanningSettings {

    /**
     * @return planningHorizon
     */
    Date getPlanningHorizon();

    /**
     * @param planningHorizon
     */
    void setPlanningHorizon(Date planningHorizon);

    /**
     * @return confirmedPurchaseOnly
     */
    boolean isConfirmedPurchaseOnly();

    /**
     * @param confirmedPurchaseOnly
     */
    void setConfirmedPurchaseOnly(boolean confirmedPurchaseOnly);

    /**
     * @return openPurchaseOnly
     */
    boolean isOpenPurchaseOnly();

    /**
     * @param openPurchaseOnly
     */
    void setOpenPurchaseOnly(boolean openPurchaseOnly);

    /**
     * @return displayVacant
     */
    boolean isDisplayVacant();

    /**
     * @param displayVacant
     */
    void setDisplayVacant(boolean displayVacant);

    /**
     * @return stock
     */
    Long getStock();

    /**
     * @param stock
     */
    void setStock(Long stock);

    /**
     * @return productCategory
     */
    Long getProductCategory();

    /**
     * @param productCategory
     */
    void setProductCategory(Long productCategory);

    /**
     * @return productGroup
     */
    Long getProductGroup();

    /**
     * @param productGroup
     */
    void setProductGroup(Long productGroup);

    /**
     * @return productType
     */
    Long getProductType();

    /**
     * @param productType
     */
    void setProductType(Long productType);

    void reset();
}
