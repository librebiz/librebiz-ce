
function checkSearchException() {
	try {
		if ($('value')) {
			minChars = 5;
			if ($('value').value.length < minChars) {
				if ($('ajaxContent')) {
					$('ajaxContent').innerHTML = "<div style='width: 100%;'><div style='border:1px solid #D6DDE6; padding:20px; background:#FFFFFB;'>"+ ojsI18n.min_character_count + minChars +"</div></div>";
				}
				return false;
			}
		}
		if ($('ajaxContent')) {
			$('ajaxContent').innerHTML = "<div style='width: 100%; text-align: center;'><div style='border:1px solid #D6DDE6; padding:20px; background:#FFFFFB;'><img src='/osdb/img/ajax_loader.gif' style='margin:5px;'/><br/>"+ ojsI18n.loading+"</div></div>";
		}
		return true;
	} catch (e) {
		return true;
	}
}
