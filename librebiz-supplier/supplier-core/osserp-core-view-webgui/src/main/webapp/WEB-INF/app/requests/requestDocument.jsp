<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<o:resource />
<v:view viewName="requestDocumentView" />
<c:set var="dmsDocumentView" scope="request" value="${sessionScope.requestDocumentView}" />
<c:set var="dmsDocumentUrl" scope="request" value="/requests/requestDocument" />
<c:set var="dmsDeletePermissions" scope="request" value="delete_sales_documents,delete_sales_pictures,organisation_admin,executive_sales" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="headline">
        <fmt:message key="documentsTo" />
        <o:out value="${view.businessCase.primaryKey}" /> - <o:out value="${view.businessCase.name}" />
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>
    <tiles:put name="content" type="string">
        <div class="content-area">
            <div class="row">
                <div class="col-md-12 panel-area">
                    <v:form url="/requests/requestDocument/save" multipart="true">
                        <div class="table-responsive table-responsive-default">
                            <table class="table table-striped">
                                <c:if test="${!empty view.list}">
                                    <thead>
                                        <tr>
                                            <th class="docListName"><fmt:message key="document" /></th>
                                            <th class="docListSize"><fmt:message key="size" /></th>
                                            <th class="docListDate"><fmt:message key="date" /></th>
                                            <th class="docListUser"><fmt:message key="uploadBy" /></th>
                                            <th class="action"><fmt:message key="action" /></th>
                                        </tr>
                                    </thead>
                                </c:if>
                                <tbody>
                                    <c:import url="${viewdir}/dms/_commonDocumentUploadColumn.jsp" />
                                    <c:forEach items="${view.list}" var="doc">
                                        <tr>
                                            <td class="docListName"><o:doc obj="${doc}"><o:out value="${doc.displayName}" /></o:doc></td>
                                            <td class="docListSize"><o:number value="${doc.fileSize}" format="bytes" /></td>
                                            <td class="docListDate"><o:date value="${doc.displayDate}" /></td>
                                            <td class="docListUser">
                                                <v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${doc.createdBy}" title="uploadBy">
                                                    <oc:employee value="${doc.createdBy}" />
                                                </v:ajaxLink>
                                            </td>
                                            <td class="action">
                                                <o:permission role="delete_sales_documents,organisation_admin,executive_sales" info="permissionDocumentsDelete">
                                                    <a href="javascript:onclick=confirmLink('<fmt:message key="irrevocableAction"/>:\n<fmt:message key="confirmDeleteDocument"/>','<v:url value="/requests/requestDocument/delete?id=${doc.id}" />');" title="<fmt:message key="deleteDocument"/>"><o:img name="deleteIcon" /></a>
                                                </o:permission>
                                                <o:forbidden role="delete_sales_documents,organisation_admin,executive_sales" info="permissionDocumentsDelete">
                                                    <c:if test="${doc.createdBy == view.user.id}">
                                                        <a href="javascript:onclick=confirmLink('<fmt:message key="irrevocableAction"/>:\n<fmt:message key="confirmDeleteDocument"/>','<v:url value="/requests/requestDocument/delete?id=${doc.id}" />');" title="<fmt:message key="deleteDocument"/>"><o:img name="deleteIcon" /></a>
                                                    </c:if>
                                                </o:forbidden>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    <c:import url="${viewdir}/dms/_commonDocumentImports.jsp" />
                                </tbody>
                            </table>
                        </div>
                    </v:form>
                </div>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
