/**
 *
 * Copyright (C) 2021 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License (AGPL)
 * version 3 as published by the Free Software Foundation. In accordance
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code
 * versions must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area
 * or, if not reasonably feasible for technical reason, as a top-level
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute.
 *
 */
package com.osserp.gui.mail

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.mail.Attachment
import com.osserp.common.mail.ReceivedMail
import com.osserp.common.mail.ReceivedMailInfo

import com.osserp.core.BusinessCase
import com.osserp.core.contacts.Contact
import com.osserp.core.mail.FetchmailInboxManager
import com.osserp.gui.mail.AbstractFetchmailInboxView
import com.osserp.groovy.web.ViewContextException

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class BusinessCaseInboxView extends AbstractBusinessCaseMailView {
    private static Logger logger = LoggerFactory.getLogger(BusinessCaseInboxView.class.getName())

    public BusinessCaseInboxView() {
        super()
        providesEditMode()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest || reloadRequest) {
            loadList()
            if (forwardRequest && list.size() == 1) {
                ReceivedMailInfo nfo = list.get(0)
                receivedMail = bean = fetchmailInboxManager.getMessage(nfo.messageId)
                enableEditMode()
            }
        }
    }

    @Override
    protected void loadList() {
        list = fetchmailInboxManager.findByBusinessCase(domainUser, businessCase)
    }

    @Override
    void save() {
        if (receivedMail) {
            Integer action = form.getInteger('attachmentAction')
            String text = form.getString('text')
            fetchmailInboxManager.moveToNotes(
                domainUser,
                receivedMail,
                businessCase,
                action ?: FetchmailInboxManager.MOVE_ATTACHMENTS_BY_TYPE,
                text)
            receivedMail = bean = null
            disableEditMode()
            reload()
        }
    }

    void setBusinessCase() {
        assignBusinessCase(
            businessCase.customer.primaryKey,
            Contact.CUSTOMER,
            businessCase.primaryKey)
        enableEditMode()
        loadList()
    }

    void moveAttachmentToDocuments() {
        Attachment obj = fetchAttachment()
        if (obj) {
            receivedMail = fetchmailInboxManager.moveToDocuments(
                domainUser, receivedMail, obj, businessCase)
        }
    }

    void moveAttachmentToPictures() {
        Attachment obj = fetchAttachment()
        if (obj) {
            receivedMail = fetchmailInboxManager.moveToPictures(
                domainUser, receivedMail, obj, businessCase)
        }
    }

    @Override
    void select() {
        super.select()
        if (receivedMail) {
            enableEditMode()
        } else {
            disableEditMode()
        }
    }

}
