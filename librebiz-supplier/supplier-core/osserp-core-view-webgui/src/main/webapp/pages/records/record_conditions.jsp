<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<c:set var="view" value="${sessionScope.recordView}"/>
<c:set var="record" value="${view.record}"/>
<div class="recordConditions">
<p class="recordConditionsHeader">
<c:choose>
<c:when test="${!record.unchangeable and !view.readOnlyMode}"><a href="<c:url value="${requestScope.infoUrl}?method=forward"/>" title="<fmt:message key="title.conditions.edit"/>"><fmt:message key="generalConditions"/>:</a></c:when>
<c:otherwise><fmt:message key="generalConditions"/>:</c:otherwise>
</c:choose>
<c:if test="${empty record.infos}"><fmt:message key="none"/></c:if> 
</p>
<c:forEach var="info" items="${record.infos}">
<p><oc:options name="recordInfoConfigs" value="${info.infoId}" format="true"/></p>
</c:forEach>
<br/>
</div>
