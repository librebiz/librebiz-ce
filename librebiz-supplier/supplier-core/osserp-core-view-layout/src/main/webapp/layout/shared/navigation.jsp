<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${!empty sessionScope.user}">
  <c:set var="user" value="${sessionScope.user}" />
</c:if>
<c:set var="portalView" value="${sessionScope.portalView}" />
<c:set var="privateContactAvailable" value="${portalView.privateContactAvailable}" />

<ul class="nav nav-sidebar">
  <c:if test="${portalView.menu.modulesEnabled and user.domainUser
      and (privateContactAvailable or user.properties['events'].enabled
      or user.properties['documentImports'].enabled
      or user.properties['deliveryCalendar'].enabled
      or user.properties['fetchmailInbox'].enabled
      or user.properties['mountingCalendar'].enabled
      or user.properties['projectMonitoring'].enabled
      or user.properties['requestsByEmployee'].enabled
      or user.properties['salesBySales'].enabled
      or user.properties['salesMonitoring'].enabled)}">
    <li><v:link url="/users/userSettings/forward" title="settings" styleClass="content-header"><fmt:message key="news" /></v:link>
      <ul>
        <c:if test="${user.properties['events'].enabled}">
          <li><v:link url="/dispatch?url=/events/userEvents/forward" title="myEvents">
              <fmt:message key="todos" />
            </v:link></li>
        </c:if>
        <c:if test="${user.properties['requestsByEmployee'].enabled}">
          <li><v:link url="/dispatch?url=/requests/myRequests/forward" title="myRequests">
              <fmt:message key="requests" />
            </v:link></li>
        </c:if>
        <c:if test="${user.properties['salesByManager'].enabled or user.properties['salesBySales'].enabled}">
          <li><v:link url="/dispatch?url=/reporting/projectsByEmployee/forward" title="myOrders">
              <fmt:message key="orders" />
            </v:link></li>
        </c:if>
        <c:if test="${user.projectManager or user.executive or user.sales or user.accounting}">
          <c:if test="${user.properties['projectMonitoring'].enabled}">
            <li><v:link url="/dispatch?url=/reporting/projectMonitoring/forward?employeeMode=true" title="projectMonitoring">
              <fmt:message key="control" />
            </v:link></li>
          </c:if>
          <c:if test="${user.properties['salesMonitoring'].enabled}">
            <li><v:link url="/dispatch?url=/reporting/salesProcess/forward?company=100">
                <fmt:message key="process" />
            </v:link></li>
          </c:if>
        </c:if>
        <c:if test="${user.properties['deliveryCalendar'].enabled}">
          <li><v:link url="/dispatch?url=/sales/salesDeliveryCalendar/forward" title="deliveryCalendar">
              <fmt:message key="deliveries" />
          </v:link></li>
        </c:if>
        <c:if test="${user.properties['mountingCalendar'].enabled}">
          <li><v:link url="/dispatch?url=/projects/mountingCalendar/forward" title="mountingCalendar">
              <fmt:message key="mountings" />
          </v:link></li>
        </c:if>
        <c:if test="${user.properties['documentImports'].enabled}">
          <li><v:link url="/dispatch?url=/dms/documentImport/forward" title="documentImports">
              <fmt:message key="documents" />
            </v:link></li>
        </c:if>
        <c:if test="${privateContactAvailable}">
          <li><v:link url="/dispatch?url=/privateContacts.do?method=forward" title="myContacts">
              <fmt:message key="contactList" />
            </v:link></li>
        </c:if>
        <c:if test="${user.properties['fetchmailInbox'].enabled}">
          <c:set var="inboxMessageCount" value="${portalView.inboxMessageCount}" />
          <c:if test="${inboxMessageCount > 0}">
            <li><v:link url="/dispatch?url=/users/userFetchmailInbox/forward">
                <o:out value="${inboxMessageCount}"/>
                <c:choose>
                    <c:when test="${inboxMessageCount > 1}"> <fmt:message key="emails" /></c:when>
                    <c:otherwise> <fmt:message key="email" /></c:otherwise>
                </c:choose>
              </v:link></li>
            </c:if>
        </c:if>
        <%-- <li><a href="<v:url value="/system/tickets/ticket/forward"/>"><fmt:message key="myTickets"/></a></li> --%>
      </ul></li>
  </c:if>

  <c:forEach var="position" items="${portalView.menu.headers}">
    <li>
        <v:link url="/toggleMenu?target=${position.name}" styleClass="content-header">
            <fmt:message key="${position.name}" />
        </v:link>
        <c:if test="${position.activated}">
            <ul>
                <c:forEach var="link" items="${position.links}">
                    <li>
                        <v:link url="/dispatch?url=${link.url}">
                            <fmt:message key="${link.name}" />
                        </v:link>
                    </li>
                </c:forEach>
            </ul>
        </c:if>
    </li>
  </c:forEach>

  <c:if test="${portalView.menu.staticLinksEnabled}">
    <li><span class="content-header">Links</span>
      <ul>
        <li><a href="<oc:systemPropertyOut name="internetSite"/>" target="_blank">Internet</a></li>
        <c:if test="${!empty portalView.helpServerUrl}">
          <li><a href="<o:out value="${portalView.helpServerUrl}"/>" target="_blank"><fmt:message key="faqHelp" /></a></li>
        </c:if>
        <%--
          <c:if test="${sessionScope.telephoneIndexView}">
            <li><v:link url="/telephones/telephoneCall/forward"><fmt:message key="phone"/></v:link></li>
            <o:permission role="telephone_exchange_edit,telephone_system_admin" info="permissionTelephoneExchangeEdit">
              <li><v:link url="/telephones/telephoneConfigurationSearch/findTelephoneExchangesByTelephoneSystem"><fmt:message key="telephoneExchange"/></v:link></li>
            </o:permission>
          </c:if>
          --%>
        <c:if test="${portalView.timeRecordingRequired}">
          <li><v:link url="/dispatch?url=/timeRecordings.do?method=forward&exit=index">
              <fmt:message key="timeRecording" />
            </v:link></li>
        </c:if>
      </ul>
    </li>
  </c:if>
</ul>
