/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 26, 2007 9:07:14 PM 
 * 
 */
package com.osserp.gui.client.mail.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionException;
import com.osserp.common.mail.EmailAddress;
import com.osserp.common.util.CollectionUtil;
import com.osserp.common.web.Form;
import com.osserp.common.web.ViewName;
import com.osserp.core.Comparators;
import com.osserp.core.contacts.Contact;
import com.osserp.core.employees.Employee;
import com.osserp.core.mail.MailingList;
import com.osserp.core.mail.MailingListManager;
import com.osserp.gui.client.impl.AbstractView;
import com.osserp.gui.client.mail.MailingListView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("mailingListView")
public class MailingListViewImpl extends AbstractView implements MailingListView {
    private static Logger log = LoggerFactory.getLogger(MailingListViewImpl.class.getName());
    private List<Employee> employees = new ArrayList<Employee>();
    private List<Contact> availableRecipients = new ArrayList<Contact>();

    protected MailingListViewImpl() {
        super();
    }

    @Override
    public void init(HttpServletRequest request) throws PermissionException {
        // only employees have access
        getDomainEmployee();
        MailingListManager manager = getListManager();
        List<MailingList> list = manager.findAll();
        setList(list);
        this.employees = getEmployeeSearch().findAllActive();
        if (log.isDebugEnabled()) {
            log.debug("init() done; found " + list.size() + " lists");
        }
    }

    public List<Contact> getAvailableRecipients() {
        return availableRecipients;
    }

    public void refreshRecipients() {
        MailingList list = getMailingList();
        this.employees = getEmployeeSearch().findAllActive();
        this.refreshAvailableRecipients(list);
    }

    public void toggleActivation() {
        MailingList list = getMailingList();
        list.setActive(!list.isActive());
        getListManager().save(getDomainEmployee(), list);
    }

    public void addMember(String address) {
        MailingList list = getMailingList();
        EmailAddress selected = getAvailableEmailAddress(address);
        if (selected != null) {
            list.getMembers().add(selected);
            getListManager().save(getDomainEmployee(), list);
        }
        this.refreshAvailableRecipients(list);
    }

    public void removeMember(String address) {
        MailingList list = getMailingList();
        for (Iterator<EmailAddress> i = list.getMembers().iterator(); i.hasNext();) {
            EmailAddress next = i.next();
            if (next.getEmail().equals(address)) {
                i.remove();
                getListManager().save(getDomainEmployee(), list);
                break;
            }
        }
        this.refreshAvailableRecipients(list);
    }

    @Override
    public void save(Form form) throws ClientException, PermissionException {
        String name = form.getString(Form.NAME);
        String description = form.getString(Form.DESCRIPTION);
        Long reference = form.getLong(Form.REFERENCE);
        Employee owner = null;
        if (reference != null) {
            owner = (Employee) CollectionUtil.getById(employees, reference);
        }
        MailingListManager manager = getListManager();
        if (isCreateMode()) {
            MailingList newList = manager.create(
                    getDomainEmployee(),
                    owner,
                    name,
                    description);
            if (log.isDebugEnabled()) {
                log.debug("save() created new mailing list "
                        + newList.getId() + " - " + newList.getName());
            }
            setList(manager.findAll());
            setCreateMode(false);
        } else {
            MailingList list = (MailingList) getBean();
            if (list == null || !isEditMode()) {
                if (log.isInfoEnabled()) {
                    log.info("save() invoked in invalid context; " +
                            "mailing list not found or edit mode deactivated");
                }
                throw new ActionException();
            }
            List<MailingList> existing = manager.findAll();
            for (int i = 0, j = existing.size(); i < j; i++) {
                MailingList next = existing.get(i);
                if (next.getName().equals(name)
                        && !next.getId().equals(list.getId())) {
                    throw new ClientException(ErrorCode.NAME_EXISTS);
                }
            }
            list.setName(name);
            list.setDescription(description);
            manager.save(getDomainEmployee(), list);
            setBean(null);
            setEditMode(false);
        }
    }

    @Override
    public void setSelection(Long id) {
        if (id == null) {
            if (log.isDebugEnabled()) {
                log.debug("setSelection() [null] invoked");
            }
            setBean(null);
        } else {
            if (log.isDebugEnabled()) {
                log.debug("setSelection() [" + id + "] invoked");
            }
            super.setSelection(id);
            if (log.isDebugEnabled()) {
                log.debug("setSelection() invoking on superclass done, checking result...");
            }
            MailingList selected = (MailingList) getBean();
            if (selected == null) {
                if (log.isInfoEnabled()) {
                    log.info("setSelection() invoked in invalid context; " +
                            "no mailing list found under id " + id);
                }
                throw new ActionException();
            }
            this.refreshAvailableRecipients(selected);
        }
    }

    @Override
    public void setEditMode(boolean editMode) throws PermissionException {
        super.setEditMode(editMode);
        if (!isEditMode()) {
            setBean(null);
        }
    }

    private void refreshAvailableRecipients(MailingList list) {
        // remove values from previous action 
        this.availableRecipients = new ArrayList<Contact>();
        // sort mailing list members
        list.sortMembers();
        // add all not already added
        for (int i = 0, j = employees.size(); i < j; i++) {
            Employee next = employees.get(i);
            String email = next.getEmail();
            if (email != null) {
                EmailAddress available = (EmailAddress) CollectionUtil.getByProperty(next.getEmails(), "email", email);
                if (!listContainsExistingAddress(list, available)) {
                    this.availableRecipients.add(next);
                }
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("refreshAvailableRecipients() done, size [" + availableRecipients.size() + "]");
        }
        CollectionUtil.sort(availableRecipients, Comparators.createEmailByContactComparator(false));
    }

    private boolean listContainsExistingAddress(MailingList list, EmailAddress address) {
        if (address == null) {
            // don't add contacts without address 
            return true;
        }
        for (int i = 0, j = list.getMembers().size(); i < j; i++) {
            EmailAddress next = list.getMembers().get(i);
            if (next.getEmail().equals(address.getEmail())) {
                return true;
            }
        }
        return false;
    }

    private MailingList getMailingList() {
        MailingList list = (MailingList) getBean();
        if (list == null) {
            if (log.isInfoEnabled()) {
                log.info("getMailingList() invoked in invalid context; " +
                        "no mailing list selected");
            }
            throw new ActionException();
        }
        return list;
    }

    private EmailAddress getAvailableEmailAddress(String address) {
        for (int i = 0, j = availableRecipients.size(); i < j; i++) {
            Contact nextContact = availableRecipients.get(i);
            for (int k = 0, l = nextContact.getEmails().size(); k < l; k++) {
                EmailAddress nextAddress = nextContact.getEmails().get(k);
                if (nextAddress.getEmail().equals(address)) {
                    return nextAddress;
                }
            }
        }
        return null;
    }

    private MailingListManager getListManager() {
        return (MailingListManager) getService(MailingListManager.class.getName());
    }
}
