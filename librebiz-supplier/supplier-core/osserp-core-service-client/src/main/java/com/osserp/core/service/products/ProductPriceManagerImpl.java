/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 26, 2011 10:48:57 AM 
 * 
 */
package com.osserp.core.service.products;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.User;
import com.osserp.common.util.NumberUtil;

import com.osserp.core.dao.ProductPrices;
import com.osserp.core.dao.ProductPricesByQuantity;
import com.osserp.core.dao.Products;
import com.osserp.core.dao.Users;
import com.osserp.core.employees.Employee;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductPriceByQuantity;
import com.osserp.core.products.ProductPriceByQuantityMatrix;
import com.osserp.core.products.ProductPriceManager;
import com.osserp.core.products.ProductSalesPrice;
import com.osserp.core.products.PriceAware;
import com.osserp.core.service.impl.AbstractService;
import com.osserp.core.tasks.ProductChangedSender;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProductPriceManagerImpl extends AbstractService implements ProductPriceManager {
    private static Logger log = LoggerFactory.getLogger(ProductPriceManagerImpl.class.getName());

    private static final String[] SALES_PRICE_LOWER_PURCHASE_PRICE_PERMISSION = { "product_sp_lower_pp" };
    
    private Products products;
    private ProductPrices prices;
    private ProductPricesByQuantity pricesByQuantity;
    private Users users;
    private ProductChangedSender productChangedSender;

    protected ProductPriceManagerImpl(
            Products products,
            ProductPrices prices,
            ProductPricesByQuantity pricesByQuantity,
            Users users,
            ProductChangedSender productChangedSender) {
        super();
        this.products = products;
        this.prices = prices;
        this.pricesByQuantity = pricesByQuantity;
        this.users = users;
        this.productChangedSender = productChangedSender;
    }

    public void createSalesPriceHistory(Employee employee, Product product, Date validFrom) {
        prices.createSalesPriceHistory(employee, product, validFrom);
    }

    public List<ProductSalesPrice> getPriceHistory(Long productId) {
        return prices.loadPriceHistory(productId);
    }

    public List<ProductSalesPrice> updatePriceHistory(Employee employee, Long priceHistoryId, Date validFrom) {
        return prices.updatePriceHistory(employee.getId(), priceHistoryId, validFrom);
    }

    public double findPartnerPrice(Product product, Date when, Double quantity) {
        return prices.getPartnerPrice(product, when, quantity);
    }

    public ProductPriceByQuantityMatrix createMatrix(Employee user, Product product) {
        ProductPriceByQuantityMatrix created = pricesByQuantity.create(user, product);
        // add initial range 0-0 
        pricesByQuantity.addRange(user, created, 0d);
        return created;
    }

    public ProductPriceByQuantityMatrix getMatrix(Product product) {
        return pricesByQuantity.getPriceMatrix(product);
    }

    public void addRange(Employee user, ProductPriceByQuantityMatrix matrix, Double value) {
        pricesByQuantity.addRange(user, matrix, value);
    }

    public void removeRange(Employee user, ProductPriceByQuantityMatrix matrix) {
        pricesByQuantity.removeRange(user, matrix);
    }

    public void updateMargin(
            Employee user,
            ProductPriceByQuantityMatrix matrix,
            ProductPriceByQuantity range,
            Double consumerMinMargin,
            Double resellerMinMargin,
            Double partnerMinMargin) {
        for (Iterator<ProductPriceByQuantity> i = matrix.getRanges().iterator(); i.hasNext();) {
            ProductPriceByQuantity rg = i.next();
            if (rg.getId().equals(range.getId())) {
                rg.updateMinimumMargin(user, consumerMinMargin, resellerMinMargin, partnerMinMargin);
                pricesByQuantity.save(matrix);
                break;
            }
        }
    }

    public PriceAware updateSalesPrice(
            Employee user,
            Product product,
            PriceAware priceAware,
            Date validFrom,
            Double estimatedPurchasePrice,
            Double consumerPrice,
            Double consumerMinimumMargin,
            Double resellerPrice,
            Double resellerMinimumMargin,
            Double partnerPrice,
            Double partnerMinimumMargin) throws ClientException {

        products.loadSummary(product);
        boolean valuesChanged = false;
        double estimatedPurchasePriceValue = estimatedPurchasePrice == null ? 0 : estimatedPurchasePrice;
        double consumerPriceValue = consumerPrice == null ? 0 : consumerPrice;
        double consumerMinimumMarginValue = consumerMinimumMargin == null ? 0 : consumerMinimumMargin;
        double resellerPriceValue = resellerPrice == null ? 0 : resellerPrice;
        double resellerMinimumMarginValue = resellerMinimumMargin == null ? 0 : resellerMinimumMargin;
        double partnerPriceValue = partnerPrice == null ? 0 : partnerPrice;
        double partnerMinimumMarginValue = partnerMinimumMargin == null ? 0 : partnerMinimumMargin;

        product.setEstimatedPurchasePrice(estimatedPurchasePriceValue);

        // set pice and min margin
        if (priceAware.getConsumerPrice() != null && !priceAware.getConsumerPrice().equals(consumerPriceValue)) {
            valuesChanged = true;
        }
        if (priceAware.getConsumerMinimumMargin() != null && !priceAware.getConsumerMinimumMargin().equals(consumerMinimumMarginValue)) {
            valuesChanged = true;
        }

        if (priceAware.getResellerPrice() != null && !priceAware.getResellerPrice().equals(resellerPriceValue)) {
            valuesChanged = true;
        }
        if (priceAware.getResellerMinimumMargin() != null && !priceAware.getResellerMinimumMargin().equals(resellerMinimumMarginValue)) {
            valuesChanged = true;
        }

        if (priceAware.getPartnerPrice() != null && !priceAware.getPartnerPrice().equals(partnerPriceValue)) {
            valuesChanged = true;
        }
        if (priceAware.getPartnerMinimumMargin() != null && !priceAware.getPartnerMinimumMargin().equals(partnerMinimumMarginValue)) {
            valuesChanged = true;
        }

        if (log.isDebugEnabled()) {
            log.debug("updateSalesPrice() checked values [product=" + product.getProductId()
                    + ", user=" + (user == null ? "null" : user.getId())
                    + ", estimatedPurchasePrice=" + product.getEstimatedPurchasePrice()
                    + ", consumerPrice=" + consumerPriceValue
                    + ", consumerMinimumMargin=" + consumerMinimumMarginValue
                    + ", resellerPrice=" + resellerPriceValue
                    + ", resellerMinimumMargin=" + resellerMinimumMarginValue
                    + ", partnerPrice=" + partnerPriceValue
                    + ", partnerMinimumMargin=" + partnerMinimumMarginValue
                    + ", valuesChanged=" + valuesChanged
                    + "]");
        }

        if (valuesChanged) {
            priceAware.setConsumerPrice(consumerPriceValue);
            priceAware.setConsumerMinimumMargin(consumerMinimumMarginValue);
            priceAware.setResellerPrice(resellerPriceValue);
            priceAware.setResellerMinimumMargin(resellerMinimumMarginValue);
            priceAware.setPartnerPrice(partnerPriceValue);
            priceAware.setPartnerMinimumMargin(partnerMinimumMarginValue);
        }

        double purchasePrice = product.getPurchasePriceForMinimumCalculation();
        if (purchasePrice > 0) {
            Double consumerPriceMinimum = NumberUtil.round(
                    purchasePrice / (1 - priceAware.getConsumerMinimumMargin()), 2);
            priceAware.setConsumerPriceMinimum(consumerPriceMinimum);

            Double resellerPriceMinimum = NumberUtil.round(
                    purchasePrice / (1 - priceAware.getResellerMinimumMargin()), 2);
            priceAware.setResellerPriceMinimum(resellerPriceMinimum);

            Double partnerPriceMinimum = NumberUtil.round(
                    purchasePrice / (1 - priceAware.getPartnerMinimumMargin()), 2);
            priceAware.setPartnerPriceMinimum(partnerPriceMinimum);

        } else if (log.isInfoEnabled()) {
            log.info("updateSalesPrice() user tries to update prices of an product " +
                    "without any purchase price and empty estimated pp value");
        }
        if (priceAware.getPartnerPrice() < purchasePrice
                || priceAware.getResellerPrice() < purchasePrice
                || priceAware.getConsumerPrice() < purchasePrice) {
            User account = users.find(user);
            // root access is assumed if account is null
            if (account != null && !account.isPermissionGrant(SALES_PRICE_LOWER_PURCHASE_PRICE_PERMISSION)) {
                // sales price may be lower purchase price if stock is empty
                if (product.isAvailableOnAnyStock()) {
                    throw new ClientException(ErrorCode.SALESPRICE_LOWER_PURCHASEPRICE);
                }
            }
        }
        if (priceAware instanceof Product) {
            // priceAware is product itself
            if (log.isDebugEnabled()) {
                log.debug("updateSalesPrice() priceAware is product");
            }
            Product updated = (Product) priceAware;
            updated.setEstimatedPurchasePrice(estimatedPurchasePriceValue);
            products.save(updated);
            if (valuesChanged) {
                createSalesPriceHistory(user, updated, validFrom);
            }
            if (productChangedSender != null) {
                try {
                    productChangedSender.send(product.getProductId(), "price");
                } catch (Exception e) {
                    log.warn("updateSalesPrice() ignoring exception on attempt to send changed event [message=" + e.getMessage() + "]", e);
                }
            }
            return updated;
        }
        if (priceAware instanceof ProductPriceByQuantity) {
            if (log.isDebugEnabled()) {
                log.debug("updateSalesPrice() priceAware is productPriceByQuantity");
            }
            ProductPriceByQuantity range = (ProductPriceByQuantity) priceAware;
            pricesByQuantity.save(range);
            Double maxConsumerPrice = consumerPriceValue;
            Double maxResellerPrice = resellerPriceValue;
            Double maxPartnerPrice = partnerPriceValue;
            ProductPriceByQuantityMatrix matrix = pricesByQuantity.getPriceMatrix(product);
            for (int i = 0, j = matrix.getRanges().size(); i < j; i++) {
                ProductPriceByQuantity rng = matrix.getRanges().get(i);
                if (rng.getConsumerPrice() > maxConsumerPrice) {
                    maxConsumerPrice = rng.getConsumerPrice();
                }
                if (rng.getResellerPrice() > maxResellerPrice) {
                    maxResellerPrice = rng.getResellerPrice();
                }
                if (rng.getPartnerPrice() > maxPartnerPrice) {
                    maxPartnerPrice = rng.getPartnerPrice();
                }
            }
            product.setConsumerPrice(maxConsumerPrice);
            product.setResellerPrice(maxResellerPrice);
            product.setPartnerPrice(maxPartnerPrice);
            products.save(product);
            return range;
        }
        log.warn("updateSalesPrice() unkown implementation: method has no effect [priceAware=" + priceAware.getClass().getName() + "]");
        return priceAware;
    }

}
