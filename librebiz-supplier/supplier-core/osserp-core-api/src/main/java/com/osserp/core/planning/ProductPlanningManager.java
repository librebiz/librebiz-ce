/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.planning;

import java.util.Date;
import java.util.List;

import com.osserp.core.products.Product;
import com.osserp.core.projects.results.ProjectListItem;
import com.osserp.core.sales.Sales;
import com.osserp.core.sales.SalesListItem;
import com.osserp.core.views.OrderItemsDisplay;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ProductPlanningManager {

    /**
     * Tries to find a planning aware object by id
     * @param id
     * @return planning aware or null if not exists
     */
    ProductPlanningAware find(Long id);

    /**
     * Creates a virtual order item
     * @param planningAware
     * @param product
     * @param quantity
     * @param stockId
     * @return orderItemDisplay
     */
    OrderItemsDisplay createVirtual(
            ProductPlanningAware planningAware,
            Product product,
            Double quantity,
            Long stockId);

    /**
     * Provides persistent product planning settings
     * @param reference
     * @return settings
     */
    ProductPlanningSettings getSettings(Long reference);

    /**
     * Persists settings
     * @param settings
     */
    void save(ProductPlanningSettings settings);

    /**
     * Indicates that stock supports planning
     * @param stock
     * @return planning available for stock
     */
    boolean isPlanningAvailable(Long stock);

    /**
     * Provides current product planning by stock and planning horizon
     * @param stock
     * @param horizon
     * @param confirmedPurchaseOnly
     * @return product planning
     */
    ProductPlanning getPlanning(Long stock, Date horizon, boolean confirmedPurchaseOnly);

    /**
     * Indicates that sales order items are available for delivery
     * @param sales
     * @return available for delivery
     */
    boolean isAvailableForDelivery(Sales sales);

    /**
     * Indicates that sales order items are available for delivery
     * @param sales
     * @return available for delivery
     */
    boolean isAvailableForDelivery(SalesListItem sales);

    /**
     * Indicates that sales order items are available for delivery
     * @param sales
     * @return available for delivery
     */
    boolean isAvailableForDelivery(ProjectListItem sales);

    /**
     * Provides an open orders summary for all stock aware products by sales
     * @param sales
     * @return open order summary items including sales
     */
    List<SalesPlanningSummary> getItems(Sales sales);

}
