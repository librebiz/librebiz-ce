/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 25, 2007 7:29:49 PM 
 * 
 */
package com.osserp.core.model.query;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdom2.CDATA;
import org.jdom2.Document;
import org.jdom2.Element;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.common.OptionsCache;
import com.osserp.common.beans.OptionImpl;
import com.osserp.common.dao.JdbcOutput;
import com.osserp.common.dao.JdbcParameter;
import com.osserp.common.dao.JdbcQuery;
import com.osserp.common.dao.JdbcQueryPermission;
import com.osserp.common.dao.QueryContext;
import com.osserp.common.dao.QueryException;
import com.osserp.common.util.DateFormatter;
import com.osserp.common.util.StringUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class JdbcQueryImpl extends OptionImpl implements JdbcQuery {
    private static Logger log = LoggerFactory.getLogger(JdbcQueryImpl.class.getName());

    private static final String[] TYPES = {
            BOOLEAN,
            DOUBLE,
            INTEGER,
            LONG,
            STRING,
            TIMESTAMP
    };

    private List<Option> inputTypes = new ArrayList<Option>();
    private List<String> typeNames = new ArrayList<String>();

    {
        this.inputTypes.add(new OptionImpl(JdbcParameter.TEXT, "text"));
        this.inputTypes.add(new OptionImpl(JdbcParameter.SELECT, "select"));
        this.inputTypes.add(new OptionImpl(JdbcParameter.CHECKBOX, "checkbox"));

        for (int i = 0, j = TYPES.length; i < j; i++) {
            this.typeNames.add(TYPES[i]);
        }
    }

    private QueryContext context = null;
    private String query;
    private List<JdbcParameter> parameters = new ArrayList<JdbcParameter>();

    private String outputTitle;
    private List<JdbcOutput> outputs = new ArrayList<JdbcOutput>();
    private boolean fullscreen = false;
    private boolean deleted = true;

    // none persistent properties
    private List<String[]> resultValues = new ArrayList<String[]>();
    private Document result = null;

    private List<JdbcQueryPermission> permissionList = new ArrayList<JdbcQueryPermission>();

    /**
     * Constructor for serialization
     */
    protected JdbcQueryImpl() {
        super();
    }

    /**
     * Default constructor
     * @param name
     * @param title
     */
    public JdbcQueryImpl(QueryContext context, String name, String title, String query) throws ClientException {
        super(null, name);
        this.context = context;
        this.outputTitle = title;
        this.query = query;
        this.checkQuery(query);
    }

    public QueryContext getContext() {
        return context;
    }

    public void setContext(QueryContext context) {
        this.context = context;
    }

    public List<String> getAvailableTypes() {
        return typeNames;
    }

    public List<Option> getInputTypes() {
        return inputTypes;
    }

    public boolean isParameterized() {
        return (parameters == null || parameters.isEmpty()) ? false : true;
    }

    public String getQuery() {
        return query;
    }

    protected void setQuery(String query) {
        this.query = query;
    }

    public void update(String name, String title, String queryString) throws ClientException {
        if (isNotSet(name) || isNotSet(title)) {
            throw new ClientException(ErrorCode.VALUES_MISSING);
        }
        queryString = queryString.trim();
        checkQuery(queryString);
        setName(name);
        outputTitle = title;
        query = queryString;
    }

    public List<JdbcParameter> getParameters() {
        return parameters;
    }

    public JdbcParameter fetchParameter(Long id) {
        for (int i = 0, j = parameters.size(); i < j; i++) {
            JdbcParameter next = parameters.get(i);
            if (next.getId().equals(id)) {
                return next;
            }
        }
        throw new ActionException("param " + id + " no longer exists");
    }

    public void addParameter(String label, String name, String type) throws ClientException {
        if (isNotSet(name) || isNotSet(type)) {
            throw new ClientException(ErrorCode.VALUES_MISSING);
        }
        this.parameters.add(new JdbcQueryParameterImpl(label, name, getId(), type));
    }

    public void updateParameter(
            Long id,
            String label,
            String name,
            String type) throws ClientException {
        if (isNotSet(name) || isNotSet(type)) {
            throw new ClientException(ErrorCode.VALUES_MISSING);
        }
        JdbcParameter param = fetchParameter(id);
        param.setLabel(label);
        param.setName(name);
        param.setTypeName(type);
    }

    public void clearParameters() {
        this.parameters.clear();
    }

    public void setParameterValue(Long id, String value) throws ClientException {
        if (isNotSet(value)) {
            throw new ClientException(ErrorCode.VALUES_MISSING);
        }
        JdbcParameter param = fetchParameter(id);
        param.setParamValue(value);
    }

    public Object[] getParameterValues() {
        if (!parameters.isEmpty()) {
            int cnt = parameters.size();
            Object[] parameterValues = new Object[cnt];
            for (int i = 0, j = cnt; i < j; i++) {
                JdbcParameter next = parameters.get(i);
                parameterValues[i] = next.getValue();
                if (log.isDebugEnabled()) {
                    log.debug("getParameterValues() adding [" + i
                            + "] " + next.getValue());
                }
            }
            return parameterValues;
        }
        if (log.isDebugEnabled()) {
            log.debug("getParameterValues() no params found");
        }
        return null;
    }

    public int[] getParameterTypes() {
        if (!parameters.isEmpty()) {
            int cnt = parameters.size();
            int[] types = new int[cnt];
            for (int i = 0, j = cnt; i < j; i++) {
                JdbcParameter next = parameters.get(i);
                types[i] = next.getType();
            }
            return types;
        }
        return null;
    }

    public Document getParameterXML(OptionsCache cache) {
        Document doc = new Document();
        Element root = new Element(JdbcQuery.OUTPUT_ROOT);
        root.setAttribute("type", "form");
        for (int i = 0, j = parameters.size(); i < j; i++) {
            JdbcParameter next = parameters.get(i);
            root.addContent(next.getXML(cache));
        }
        doc.setRootElement(root);
        return doc;
    }

    public String getOutputTitle() {
        return outputTitle;
    }

    protected void setOutputTitle(String title) {
        this.outputTitle = title;
    }

    public List<JdbcOutput> getOutputs() {
        return outputs;
    }

    public JdbcOutput fetchOutput(Long id) {
        for (int i = 0, j = outputs.size(); i < j; i++) {
            JdbcOutput next = outputs.get(i);
            if (next.getId().equals(id)) {
                return next;
            }
        }
        throw new ActionException("output " + id + " no longer exists");
    }

    public void addOutput(
            String name,
            String type,
            Integer width,
            String alignment,
            boolean display) {
        JdbcQueryOutputImpl obj = new JdbcQueryOutputImpl(
                name,
                getId(),
                type,
                width,
                alignment,
                display);
        this.outputs.add(obj);
    }

    public void updateOutput(
            Long id,
            String name,
            String type,
            Integer width,
            String alignment,
            boolean display) {

        JdbcOutput obj = fetchOutput(id);
        obj.setAlignment(alignment);
        obj.setDisplay(display);
        obj.setName(name);
        obj.setTypeName(type);
        obj.setWidth(width);
        if (log.isDebugEnabled()) {
            log.debug("updateOuput() done, values:\n" + obj.toString());
        }
    }

    public void clearOutput() {
        outputs.clear();
    }

    public void addResult(List<String[]> resultList) throws QueryException {
        resultValues = resultList;
        performResult();
    }

    public void performResult() throws QueryException {
        // TODO add checkbox
        result = new Document();
        Element root = new Element(JdbcQuery.OUTPUT_ROOT);
        if (resultValues.isEmpty()) {
            root.setAttribute("type", "error");
            result.setRootElement(root);
            throw new QueryException(result, ErrorCode.SEARCH);
        }
        root.setAttribute("type", "data");
        if (fullscreen) {
            root.setAttribute("fullscreen", "true");
        } else {
            root.setAttribute("fullscreen", "false");
        }
        root.setAttribute("header", createTitle());

        Element headers = new Element(OUTPUT_HEADERS);
        for (int i = 0, j = outputs.size(); i < j; i++) {
            JdbcOutput next = outputs.get(i);
            Element item = new Element(OUTPUT_HEAD);
            if (next.getAlignment() != null) {
                item.setAttribute("align", next.getAlignment());
            }
            if (next.getWidth() != null && next.getWidth() > 0) {
                item.setAttribute("width", next.getWidth().toString());
            }
            item.setAttribute("display", Boolean.toString(next.isDisplay()));
            item.setText(next.getName());
            headers.addContent(item);
        }
        root.addContent(headers);
        for (int i = 0, j = resultValues.size(); i < j; i++) {
            root.addContent(createRow(resultValues.get(i)));
        }
        result.setRootElement(root);
    }

    public Document getResult() {
        return result;
    }

    public String getSpreadSheet() {
        List<String[]> listWithHeader = new ArrayList<>();
        int arrayLength = outputs.size();
        String[] header = new String[arrayLength];
        for (int i = 0; i < arrayLength; i++) {
            JdbcOutput next = outputs.get(i);
            header[i] = next.getName();
        }
        listWithHeader.add(header);
        listWithHeader.addAll(resultValues);
        return StringUtil.createSheet(listWithHeader);
    }

    public Document getSmartResult() {
        Document document = new Document();
        Element root = new Element(JdbcQuery.OUTPUT_ROOT);
        for (int i = 0, j = resultValues.size(); i < j; i++) {
            Element item = new Element(JdbcQuery.OUTPUT_DATA);
            String[] array = resultValues.get(i);
            for (int k = 0, l = array.length; k < l; k++) {
                String next = array[k];
                item.addContent(new Element(JdbcQuery.OUTPUT_ITEM + k).setText(next));
            }
            root.addContent(item);
        }
        document.setRootElement(root);
        return document;
    }

    public boolean isFullscreen() {
        return fullscreen;
    }

    protected void setFullscreen(boolean fullscreen) {
        this.fullscreen = fullscreen;
    }

    public void switchFullscreen() {
        this.fullscreen = !this.fullscreen;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String[] getPermissions() {
        if (isNotSet(permissionList)) {
            return new String[] {};
        }
        String[] array = new String[permissionList.size()];
        for (int i = 0, j = permissionList.size(); i < j; i++) {
            array[i] = permissionList.get(i).getPermission();
        }
        return array;
    }

    // protected and private methods

    protected void setParameters(List<JdbcParameter> params) {
        this.parameters = params;
    }

    protected void setOutputs(List<JdbcOutput> outputs) {
        this.outputs = outputs;
    }

    protected void setResult(Document result) {
        this.result = result;
    }

    protected List<JdbcQueryPermission> getPermissionList() {
        return permissionList;
    }

    protected void setPermissionList(List<JdbcQueryPermission> permissionList) {
        this.permissionList = permissionList;
    }

    // local helpers
    private void checkQuery(String queryString) throws ClientException {
        if (isNotSet(queryString)) {
            throw new ClientException(ErrorCode.QUERY_INVALID);
        }
        String[] array = StringUtil.getTokenArray(queryString, " ");
        if (array != null && array.length > 0) {
            for (int i = 0, j = array.length; i < j; i++) {
                String type = array[i];
                if (type.equalsIgnoreCase("update")
                        || type.equalsIgnoreCase("insert")
                        || type.equalsIgnoreCase("delete")) {
                    throw new ClientException(ErrorCode.QUERY_INVALID);
                }
            }
        }
    }

    private String createTitle() {
        StringBuilder buffer = new StringBuilder(outputTitle);
        if (!parameters.isEmpty()) {
            buffer.append(" [");
            for (int i = 0, j = parameters.size(); i < j; i++) {
                JdbcParameter param = parameters.get(i);
                buffer.append(param.getLabel()).append(" : ");
                if (param.getTypeName().equals(TIMESTAMP)) {
                    buffer.append(DateFormatter.getDate((Date) param.getValue()));
                } else {
                    buffer.append(param.getValue());
                }
                if (i < j - 1) {
                    buffer.append(" / ");
                }
            }
            buffer.append("]");
        }
        return buffer.toString();
    }

    private Element createRow(String[] array) {
        Element item = new Element(JdbcQuery.OUTPUT_ITEM);
        for (int i = 0, j = array.length; i < j; i++) {
            JdbcOutput next = outputs.get(i);
            Element attrib = new Element(JdbcQuery.OUTPUT_ATTRIBUTE);
            if (isNotSet(next.getLink())) {
                attrib.setText(array[i]);
            } else {
                attrib.addContent(new CDATA(createLink(next.getLink(), array[i])));
            }
            if (isSet(next.getAlignment())) {
                attrib.setAttribute("align", next.getAlignment());
            }
            if (isSet(next.getWidth())) {
                attrib.setAttribute("width", next.getWidth().toString());
            }
            item.addContent(attrib);
        }
        return item;
    }

    private String createLink(String link, String value) {
        StringBuilder buffer = new StringBuilder(link);
        StringUtil.replace(buffer, "$$", value);
        return buffer.toString();
    }

    public String getResultTableContent() {
        StringBuilder buffer = new StringBuilder(4096);
        buffer.append("<thead><tr>");
        for (int i = 0, j = outputs.size(); i < j; i++) {
            JdbcOutput next = outputs.get(i);
            if (next.isDisplay()) {
                buffer.append("<th");
                if (isSet(next.getWidth()) || isSet(next.getAlignment())) {
                    buffer.append(" style=\"");
                    if (isSet(next.getWidth())) {
                        buffer.append("width:").append(next.getWidth()).append("px;");
                    }
                    if (isSet(next.getAlignment())) {
                        buffer.append("text-align:").append(next.getAlignment()).append(";");
                    }
                    buffer.append("\"");
                }
                buffer.append(">").append(next.getName()).append("</th>");
            }
        }
        buffer.append("</tr></thead><tbody>").append(getResultTableRows()).append("</tbody>");
        return buffer.toString();
    }

    private String getResultTableRows() {
        StringBuilder buffer = new StringBuilder(2048);
        if (resultValues.isEmpty()) {
            buffer.append("<tr><td colspan=\"").append(outputs.size()).append("\">Nothing found</td></tr>");
            return buffer.toString();
        }
        for (int i = 0; i < resultValues.size(); i++) {
            buffer.append(createTableRow(resultValues.get(i)));
        }
        return buffer.toString();
    }

    private String createTableRow(String[] array) {
        StringBuilder buffer = new StringBuilder(1024);
        buffer.append("<tr>");
        for (int i = 0; i < array.length; i++) {
            JdbcOutput row = outputs.get(i);
            if (row.isDisplay()) {
                buffer.append("<td");
                if (isSet(row.getWidth()) || isSet(row.getAlignment())) {
                    buffer.append(" style=\"");
                    if (isSet(row.getWidth())) {
                        buffer.append("width:").append(row.getWidth()).append("px;");
                    }
                    if (isSet(row.getAlignment())) {
                        buffer.append("text-align:").append(row.getAlignment()).append(";");
                    }
                    buffer.append("\"");
                }
                buffer.append(">");
                String value = array[i];
                if (isSet(value)) {
                    if (row.getLink() != null) {
                        buffer.append(createLink(row.getLink(), value));
                    } else {
                        buffer.append(value);
                    }
                }
                buffer.append("</td>");
            }
        }
        buffer.append("</tr>");
        return buffer.toString();
    }
}
