function checkCustomerSelectBox() {
	if ($('group') && $('group').value == 'customer') {
		//customerType
		if ($('customerType')) {
			$('customerType').disabled = '';
			$('customerType').enabled = 'enabled';
			$('customerType').style.visibility = 'visible';
		}
		//customerStatus
		if ($('customerStatus')) {
			$('customerStatus').disabled = '';
			$('customerStatus').enabled = 'enabled';
			$('customerStatus').style.visibility = 'visible';
		}
	} else {
		//customerType
		if ($('customerType')) {
			$('customerType').disabled = 'disabled';
			$('customerType').style.visibility = 'hidden';
		}
		//customerStatus
		if ($('customerStatus')) {
			$('customerStatus').disabled = 'disabled';
			$('customerStatus').style.visibility = 'hidden';
		}
	}
}

function checkSearchException() {
	try {
		if ($('value')) {
			minChars = 2;
			if ($('value').value.length < minChars) {
				if ($('ajaxContent')) {
					$('ajaxContent').innerHTML = "<div style='width: 100%;'><div style='text-align:center; border:1px solid #D6DDE6; padding:10px; background:#FFFFFB;'>"+ ojsI18n.min_character_count + minChars +"</div></div>";
				}
				return false;
			}
		}
		if ($('ajaxContent')) {
			$('ajaxContent').innerHTML = "<div style='width: 100%; text-align: center;'><div style='text-align:center; border:1px solid #D6DDE6; padding:10px; background:#FFFFFB;'><img src='/osdb/img/ajax_loader.gif' style='margin:5px;'/><br/>"+ ojsI18n.loading+"</div></div>";
		}
		return true;
	} catch (e) {
		return true;
	}
}
