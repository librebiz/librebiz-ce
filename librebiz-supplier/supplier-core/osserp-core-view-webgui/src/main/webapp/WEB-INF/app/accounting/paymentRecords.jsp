<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="paymentRecordsView"/>
<c:set var="bankAccountSelectionUrl" scope="request" value="/accounting/paymentRecords"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="styles" type="string">
  		<style type="text/css">
        </style>
	</tiles:put>
	<tiles:put name="headline">
		<c:if test="${view.paymentCount > 0}"><o:out value="${view.paymentCount}" /> </c:if><fmt:message key="accountTransactionsLabel" /> 
        <c:if test="${!empty view.selectedBankAccount}"> - <o:out value="${view.selectedBankAccount.name}" /></c:if>
	</tiles:put>
	<tiles:put name="headline_right">
		<v:navigation/>
	</tiles:put>

	<tiles:put name="content" type="string" >
        <div class="content-area" id="paymentRecordsContent">
            <c:choose>
                <c:when test="${empty view.selectedBankAccount}">
                    <c:import url="_bankAccountSelectionList.jsp"/>
                </c:when>
                <c:otherwise>
                    <div class="row">
                        <div class="col-md-12 panel-area">
                            <c:import url="_paymentRecordsList.jsp"/>
                        </div>
                    </div>
                </c:otherwise>
            </c:choose>
		</div>
	</tiles:put>
</tiles:insert>
