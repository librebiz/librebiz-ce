<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="requestEditorView"/>

<div class="modalBoxHeader">
    <div class="modalBoxHeaderLeft">
        <fmt:message key="${view.headerName}"/>
    </div>
</div>
<div class="modalBoxData">
    <div class="subcolumns">
        <div class="subcolumn">
            <div class="spacer"></div>
            <v:ajaxForm url="/requests/requestEditor/save" targetElement="requestEditor_popup" onSuccess="" preRequest="" postRequest="">
                <table class="valueTable inputs100">
                    <tr>
                        <td><o:out value="${view.accountingReferenceLabel}"/></td>
                        <td><v:text name="accountingReference" value="${view.selectedRequest.accountingReference}"/></td>
                    </tr>
                    <tr>
                        <td>
                            <v:ajaxLink url="/requests/requestEditor/exit" linkId="requestEditor">
                                <input class="cancel" type="button" value="<fmt:message key="exit"/>"/>
                            </v:ajaxLink>
                        </td>
                        <td>
                            <o:submit/>
                        </td>
                    </tr>
                </table>
            </v:ajaxForm>
            <div class="spacer"></div>
        </div>
    </div>
</div>
