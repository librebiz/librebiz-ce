/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 25, 2008 
 * 
 */
package com.osserp.core.dms;


import com.osserp.common.Option;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface LetterComponent extends Option {

    /**
     * Indicates if clsoing should be ignored.
     * @return ignoreSalutation
     */
    boolean isIgnoreSalutation();

    /**
     * Sets if closing should be ignored.
     * @param ignoreSalutation
     */
    void setIgnoreSalutation(boolean ignoreSalutation);

    /**
     * Provides greetings of the letter
     * @return greetings
     */
    String getGreetings();

    void setGreetings(String greetings);

    /**
     * Indicates if greetings should be ignored.
     * @return ignoreGreetings
     */
    boolean isIgnoreGreetings();

    /**
     * Sets if greetings should be ignored.
     * @param ignoreGreetings
     */
    void setIgnoreGreetings(boolean ignoreGreetings);

    /**
     * Indicates if header / address field should be ignored.
     * @return ignoreHeader
     */
    boolean isIgnoreHeader();

    /**
     * Sets if header should be ignored.
     * @param ignoreHeader
     */
    void setIgnoreHeader(boolean ignoreHeader);

    /**
     * Indicates if subject should be ignored when printing
     * @return
     */
    boolean isIgnoreSubject();

    void setIgnoreSubject(boolean ignoreSubject);

    /**
     * Template provides text to embed in other document
     * @return true if template should be embedded
     */
    boolean isEmbedded();

    void setEmbedded(boolean embedded);

    /**
     * Where should embedded text take place?
     * @return true embed above document content
     */
    boolean isEmbeddedAbove();

    void setEmbeddedAbove(boolean embeddedAbove);

    /**
     * Provides the space after embedded letter in cm
     * @return embeddedSpaceAfter
     */
    String getEmbeddedSpaceAfter();

    void setEmbeddedSpaceAfter(String embeddedSpaceAfter);

    /**
     * Value of keep-together option for the whole document content.<br/>
     * @return auto or always. Default: auto.
     */
    String getContentKeepTogether();

    void setContentKeepTogether(String contentKeepTogether);

}
