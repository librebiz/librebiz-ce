/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 3, 2005 
 * 
 */
package com.osserp.common.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.text.NumberFormat;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class NumberFormatter {
    private static final String DOT = ".";
    private static final String COMMA = ",";
    public static final int COUNT = 0;
    public static final int CURRENCY = 1;
    public static final int DECIMAL = 2;
    public static final int PERCENT = 3;
    public static final int TAX_RATE = 4;
    public static final int BYTES = 5;
    public static final int SHEET = 6;

    private static final String ipattern = "##";
    private static final String bpattern = "###.###.###";

    public static synchronized String getValue(BigDecimal val, int format, int digits) {
        if (val == null) {
            return "";
        }
        if (format == BYTES) {
            return getBytesValue(val.intValue());
        }
        if (format == PERCENT) {
            return getPercentage(val);
        }
        if (format == TAX_RATE) {
            return getTaxRate(val);
        }
        if (format == SHEET) {
            DecimalFormat df = (DecimalFormat)
                    NumberFormat.getNumberInstance(Constants.DEFAULT_LOCALE_OBJECT);
            df.setGroupingUsed(false);
            df.setMaximumFractionDigits(digits);
            return df.format(val.doubleValue());
        }
        StringBuffer value = new StringBuffer(val.toPlainString());
        if (format == CURRENCY) {
            value = formatEuropean(value, 2);
            String tmp = value.toString();
            if (tmp.equals("-0,00")
                    || tmp.equals("0")
                    || tmp.equals("0,0")
                    || tmp.equals("-0,0")) {
                return "0,00";
            }
        } else if (format == DECIMAL) {
            value = formatEuropean(value, digits);
        }
        return value.toString();
    }

    public static synchronized String getValue(BigDecimal val, int format) {
        return getValue(val, format, 3);
    }

    public static synchronized String getValue(double val, int format) {
        return getValue(Double.valueOf(val), format);
    }

    public static synchronized String getValue(double val, int format, int digits) {
        return getValue(Double.valueOf(val), format, digits);
    }

    public static synchronized String getValue(Double value, int format) {
        if (value == null) {
            return "";
        }
        return getValue(new BigDecimal(value.toString()), format);
    }

    public static synchronized String getValue(Double value, int format, int digits) {
        if (value == null) {
            return "";
        }
        return getValue(new BigDecimal(value.toString()), format, digits);
    }

    public static String getIntValue(BigDecimal value) {
        DecimalFormat nf = getIntegerFormat();
        StringBuffer sb = new StringBuffer();
        return (nf.format(value, sb, new FieldPosition(0)).toString());
    }

    public static String getIntValue(double value) {
        return getIntValue(new BigDecimal(value));
    }

    public static String getIntValue(Double value) {
        return getIntValue(new BigDecimal(value));
    }

    public static String getBytesValue(Integer value) {
        DecimalFormat nf = getBytesFormat();
        StringBuffer sb = new StringBuffer();
        return (nf.format(value, sb, new FieldPosition(0)).toString());
    }

    public static Double createDouble(String s, boolean required) throws ClientException {
        BigDecimal bd = createDecimal(s, required);
        return (bd == null ? null : bd.doubleValue());
    }

    public static BigDecimal createDecimal(String s, boolean required) throws ClientException {
        if (s == null || s.length() < 1) {
            if (required) {
                throw new ClientException(ErrorCode.NUMBER_EXPECTED);
            }
            return null;
        }
        StringBuffer b = new StringBuffer(s);
        int rc = b.lastIndexOf(",");
        int lc = b.indexOf(",");
        int rd = b.lastIndexOf(".");
        int ld = b.indexOf(".");
        try {
            if (rc < 0 && rd < 0) { // xxx
                return new BigDecimal(b.toString());

            } else if (rc >= 0 && rd < 0) { // xx,xx
                if (lc != rc) { // xx,xx,xx INV
                    /*
                     * if (log.isDebugEnabled()) { log.debug("createDouble() invalid value " + s); }
                     */
                    throw new ClientException(ErrorCode.NUMBER_INVALID);
                }
                StringUtil.replace(b, ",", ".");
                return createDecimal(b);
            } else if (rc < 0 && rd >= 0) { // xx.xx  
                if (ld != rd) { // xx.xx.xx INV
                    /*
                     * if (log.isDebugEnabled()) { log.debug("createDouble() invalid value " + s); }
                     */
                    throw new ClientException(ErrorCode.NUMBER_INVALID);
                }
                return createDecimal(b);
            } else {//if (rc >= 0 && rd >= 0) {  // xx,xx.xx || xx.xx,xx 
                if (rc < rd) { // xx,xx.xxx
                    StringUtil.removeAll(b, ",");
                    //System.out.println("xx,xx,xx.xx is " + b.toString());
                    return new BigDecimal(b.toString());
                }
                StringUtil.removeAll(b, ".");
                StringUtil.replace(b, ",", ".");
                //System.out.println("xx.xx.xx,xx is " + b.toString());
                return createDecimal(b);
            }
        } catch (NumberFormatException nf) {
            /*
             * if (log.isDebugEnabled()) { log.debug("createDouble() invalid value " + s); }
             */
            throw new ClientException(ErrorCode.NUMBER_INVALID);
        }
    }

    private static BigDecimal createDecimal(StringBuffer b) throws ClientException {
        return createDecimal(b.toString());
    }

    private static BigDecimal createDecimal(String s) throws ClientException {
        try {
            return new BigDecimal(s);
        } catch (NumberFormatException nf) {
            /*
             * if (log.isDebugEnabled()) { log.debug("createDouble() invalid value " + s); }
             */
            throw new ClientException(ErrorCode.NUMBER_INVALID);
        }
    }

    private static String getPercentage(BigDecimal value) {
        String s = getValue(value.doubleValue() * 100, COUNT);
        int i = s.indexOf(",");
        if (i > -1) {
            Character c = s.charAt(i + 1);
            if (c.equals('0')) {
                return getIntValue(value.doubleValue() * 100);
            }
            return s;
        }
        return getIntValue(value.doubleValue() * 100);
    }

    private static String getTaxRate(BigDecimal value) {
        BigDecimal a = value.multiply(new BigDecimal("100"));
        a = a.subtract(new BigDecimal("100"));
        BigDecimal d = a.round(new MathContext(2, RoundingMode.HALF_UP));
        return Integer.valueOf(d.intValue()).toString();
    }

    private static DecimalFormat getIntegerFormat() {
        DecimalFormat nf = new DecimalFormat();
        try {
            nf.applyPattern(ipattern);
        } catch (NullPointerException ne) {
            //log.error("getValue() failed: " + ne.toString());
        } catch (IllegalArgumentException ie) {
            //log.error("getValue() failed: " + ie.toString());
        }
        return nf;
    }

    private static DecimalFormat getBytesFormat() {
        DecimalFormat nf = new DecimalFormat();
        try {
            nf.applyPattern(bpattern);
        } catch (NullPointerException ne) {
            //log.error("getValue() failed: " + ne.toString());
        } catch (IllegalArgumentException ie) {
            //log.error("getValue() failed: " + ie.toString());
        }
        return nf;
    }
    
    private static StringBuffer formatEuropean(StringBuffer buffer, int digits) {
        if (buffer == null) {
            return new StringBuffer();
        }
        buffer = new StringBuffer(
                formatPlain(buffer.toString(), digits).toPlainString());
        if (buffer.indexOf(DOT) == 0) {
            buffer = new StringBuffer("0").append(buffer);
        }
        int splitAt = buffer.indexOf(DOT);
        if (splitAt > -1) {

            StringBuffer suffix = new StringBuffer(buffer.substring(splitAt + 1));
            suffix = completeSuffix(suffix, digits);

            StringBuffer prefix = new StringBuffer(buffer.substring(0, buffer.indexOf(DOT)));
            if (prefix.length() > 3) {
                prefix = formatPrefix(prefix);
            }
            if (suffix.length() == 0) {
                buffer = prefix.append(COMMA).append(completeSuffix(null, digits));
            } else {
                buffer = prefix.append(COMMA).append(suffix);
            }
        } else {
            buffer = formatPrefix(buffer).append(COMMA).append(completeSuffix(null, digits));
        }
        return buffer;
    }

    private static StringBuffer completeSuffix(StringBuffer suffix, int digits) {
        StringBuffer local = suffix;
        if (local == null) {
            local = new StringBuffer();
        }
        if (local.length() < digits) {
            int cnt = digits - local.length();
            for (int i = 0, j = cnt - 1; i <= j; i++) {
                local.append("0");
            }
        }
        return local;
    }

    public static BigDecimal formatPlain(String s, int digits) {
        if (s == null) {
            return new BigDecimal(0);
        }
        StringBuffer buffer = null;
        boolean negative = false;
        if (s.indexOf("-") == 0) {
            negative = true;
            buffer = new StringBuffer(s.substring(1));
        } else {
            buffer = new StringBuffer(s);
        }
        if (buffer.indexOf(DOT) == 0) {
            buffer = new StringBuffer("0").append(buffer);
        }
        int splitAt = buffer.indexOf(DOT);
        if (splitAt > -1) {
            StringBuffer suffix = new StringBuffer(buffer.substring(splitAt + 1));
            Integer overflow = 0;
            if (suffix.length() > digits) {
                Integer op = Integer.parseInt(Character.valueOf(suffix.charAt(digits)).toString());
                if (op > 4) {
                    String cuttedString = suffix.substring(0, digits);
                    int length = cuttedString.length();
                    int cutted = Integer.parseInt(suffix.substring(0, digits));
                    suffix = new StringBuffer(Integer.valueOf(cutted + 1).toString());
                    if (suffix.length() < length) {
                        int cnt = length - suffix.length();
                        StringBuffer tmpBuffer = new StringBuffer();
                        for (int i = 0, j = cnt; i < j; i++) {
                            tmpBuffer.append("0");
                        }
                        suffix = suffix.insert(0, tmpBuffer);
                    } else if (suffix.length() > digits) {
                        overflow = Integer.valueOf(suffix.substring(0, 1));
                        suffix = new StringBuffer(suffix.substring(1));
                    }
                } else {
                    suffix = new StringBuffer(suffix.substring(0, digits));
                }
            } else if (suffix.length() < digits) {
                int cnt = digits - suffix.length();
                for (int i = 0, j = cnt - 1; i <= j; i++) {
                    suffix.append("0");
                }
            }
            StringBuffer prefix = new StringBuffer(buffer.substring(0, buffer.indexOf(DOT)));
            if (overflow > 0) {
                BigInteger bi = new BigInteger(prefix.toString());
                prefix = new StringBuffer(bi.add(new BigInteger(overflow.toString())).toString());
            }
            if (negative) {
                prefix = new StringBuffer("-" + prefix.toString());
            }
            if (suffix.length() == 0) {
                return new BigDecimal(prefix.toString());
            }
            return new BigDecimal(prefix.append(DOT).append(suffix).toString());
        }
        if (negative) {
            buffer = new StringBuffer("-" + buffer.toString());
        }
        return new BigDecimal(buffer.toString());
    }

    private static StringBuffer formatPrefix(StringBuffer prefixToFormat) {
        StringBuffer prefix = prefixToFormat;
        boolean negative = false;
        if (prefix.indexOf("-") > -1) {
            StringUtil.deleteMinus(prefix);
            negative = true;
        }
        if (prefix.length() > 3) {
            prefix = prefix.reverse();
            StringBuffer bf = new StringBuffer();
            for (int i = 0, j = prefix.length(); i < j; i++) {
                if (i == 0 || (i % 3) != 0) {
                    bf.append(prefix.charAt(i));
                } else if (i % 3 == 0) {
                    bf.append(DOT).append(prefix.charAt(i));
                }
            }
            prefix = bf.reverse();
        }
        if (negative) {
            return new StringBuffer("-" + prefix.toString());
        }
        return prefix;
    }
}
