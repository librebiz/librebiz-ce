/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Jun 6, 2007 9:15:45 PM 
 * 
 */
package com.osserp.core.products;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jdom2.Element;

import com.osserp.common.Constants;
import com.osserp.common.XmlAwareEntity;
import com.osserp.common.beans.AbstractClass;
import com.osserp.common.util.CollectionUtil;
import com.osserp.common.util.NumberUtil;
import com.osserp.common.xml.JDOMUtil;

import com.osserp.core.Comparators;
import com.osserp.core.views.OrderItemsDisplay;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author cf <cf@osserp.com>
 * 
 */
public class ProductSummaryImpl extends AbstractClass implements ProductSummary, XmlAwareEntity {

    private Long stockId = 100L;
    private Double stock = 0d;
    private Double receipt = 0d;
    private Double salesReceipt = 0d;
    private Double expected = 0d;
    private Double ordered = 0d;
    private Double vacant = 0d;
    private Double unreleased = 0d;
    private Double averagePurchasePrice = 0d;
    private Double lastPurchasePrice = 0d;
    private Double lastPricePaid = 0d;
    private Date lastPricePaidOn;
    private List<OrderItemsDisplay> items = new ArrayList<OrderItemsDisplay>();

    /**
     * Parameterless constructor required to implement serializable
     */
    protected ProductSummaryImpl() {
        super();
    }

    /**
     * Creates an empty summary
     * @param stockId
     */
    public ProductSummaryImpl(Long stockId) {
        super();
        this.stockId = stockId;
    }

    /**
     * Default constructor initializing stock id with products current stock or default stock if current not set
     * @param product
     */
    public ProductSummaryImpl(Product product) {
        super();
        stockId = product.getCurrentStock() != null ? product.getCurrentStock() : product.getDefaultStock();
    }

    /**
     * Default constructor, readonly object
     * @param currentStock
     * @param currentReceipt
     * @param currentExpected
     * @param currentOrdered
     * @param currentVacant
     * @param currentUnreleased
     * @param averagePurchasePrice
     * @param lastPurchasePrice
     * @param currentSalesReceipt
     */
    public ProductSummaryImpl(
            Long stockId,
            Double currentStock,
            Double currentReceipt,
            Double currentExpected,
            Double currentOrdered,
            Double currentVacant,
            Double currentUnreleased,
            Double averagePurchasePrice,
            Double lastPurchasePrice,
            Double currentSalesReceipt) {
        super();
        this.stockId = stockId;
        this.stock = currentStock == null ? 0d : currentStock;
        this.receipt = currentReceipt == null ? 0d : currentReceipt;
        this.expected = currentExpected == null ? 0d : currentExpected;
        this.ordered = currentOrdered == null ? 0d : currentOrdered;
        this.vacant = currentVacant == null ? 0d : currentVacant;
        this.unreleased = currentUnreleased == null ? 0d : currentUnreleased;
        this.averagePurchasePrice = averagePurchasePrice == null ? 0d : averagePurchasePrice;
        this.lastPurchasePrice = lastPurchasePrice == null ? 0d : lastPurchasePrice;
        this.salesReceipt = currentSalesReceipt == null ? 0d : currentSalesReceipt;

    }

    public Long getStockId() {
        return stockId;
    }

    public void setStockId(Long stockId) {
        this.stockId = stockId;
    }

    public Double getAveragePurchasePrice() {
        return averagePurchasePrice;
    }

    public Double getLastPurchasePrice() {
        return lastPurchasePrice;
    }

    public Double getExpected() {
        return expected;
    }

    public Double getOrdered() {
        return ordered;
    }

    public Double getOrderedAndVacant() {
        return (NumberUtil.getDouble(ordered) + NumberUtil.getDouble(vacant));
    }

    public Double getVacant() {
        return vacant;
    }

    public Double getSalesReceipt() {
        return salesReceipt;
    }

    public Double getReceipt() {
        return receipt;
    }

    public Double getStock() {
        return stock;
    }

    public Double getAvailableStock() {
        return (NumberUtil.getDouble(stock)
        + NumberUtil.getDouble(receipt));
    }

    public Double getAvailability() {
        return (NumberUtil.getDouble(expected)
                + getAvailableStock()
                - NumberUtil.getDouble(ordered));
    }

    public Double getVacantAvailability() {
        return (NumberUtil.getDouble(expected)
                + getAvailableStock()
                - NumberUtil.getDouble(getOrderedAndVacant()));
    }

    public Double getUnreleased() {
        return unreleased;
    }

    public boolean isPurchaseOrdered() {
        if (ordered < 1
                || ordered <= (stock + expected + receipt)) {
            return false;
        }
        return true;
    }

    public List<OrderItemsDisplay> getItems() {
        return items;
    }

    public void addItem(OrderItemsDisplay item) {
        boolean added = false;
        for (int i = 0, j = items.size(); i < j; i++) {
            OrderItemsDisplay next = items.get(i);
            if (next.getId().equals(item.getId())) {
                added = true;
            }
        }
        if (!added) {
            this.items.add(item);
        }
        CollectionUtil.sort(items, Comparators.createOrderItemsDeliveryDateComparator());
    }

    public void markAsKanban() {
        this.ordered = Constants.DOUBLE_NULL;
        this.stock = Constants.DOUBLE_NULL;
        this.unreleased = Constants.DOUBLE_NULL;
        this.salesReceipt = Constants.DOUBLE_NULL;
    }

    // unused protected setters

    protected void setAveragePurchasePrice(Double averagePurchasePrice) {
        this.averagePurchasePrice = averagePurchasePrice;
    }

    protected void setLastPurchasePrice(Double lastPurchasePrice) {
        this.lastPurchasePrice = lastPurchasePrice;
    }

    protected void setExpected(Double expected) {
        this.expected = expected;
    }

    protected void setOrdered(Double ordered) {
        this.ordered = ordered;
    }

    protected void setVacant(Double vacant) {
        this.vacant = vacant;
    }

    protected void setSalesReceipt(Double salesReceipt) {
        this.salesReceipt = salesReceipt;
    }

    protected void setReceipt(Double receipt) {
        this.receipt = receipt;
    }

    protected void setStock(Double stock) {
        this.stock = stock;
    }

    protected void setUnreleased(Double unreleased) {
        this.unreleased = unreleased;
    }

    public Double getLastPricePaid() {
        return lastPricePaid;
    }

    public void setLastPricePaid(Double lastPricePaid) {
        this.lastPricePaid = lastPricePaid;
    }

    public Date getLastPricePaidOn() {
        return lastPricePaidOn;
    }

    public void setLastPricePaidOn(Date lastPricePaidOn) {
        this.lastPricePaidOn = lastPricePaidOn;
    }

    public void setItems(List<OrderItemsDisplay> items) {
        this.items = items;
    }

    public Element getXML() {
        return getXML("root");
    }

    public Element getXML(String rootElementName) {
        Element root = new Element(rootElementName);
        root.addContent(JDOMUtil.createElement("stockId", stockId));
        root.addContent(JDOMUtil.createElement("stock", stock));
        root.addContent(JDOMUtil.createElement("receipt", receipt));
        root.addContent(JDOMUtil.createElement("salesReceipt", salesReceipt));
        root.addContent(JDOMUtil.createElement("expected", expected));
        root.addContent(JDOMUtil.createElement("ordered", ordered));
        root.addContent(JDOMUtil.createElement("vacant", vacant));
        root.addContent(JDOMUtil.createElement("unreleased", unreleased));
        root.addContent(JDOMUtil.createElement("averagePurchasePrice", averagePurchasePrice));
        root.addContent(JDOMUtil.createElement("lastPurchasePrice", lastPurchasePrice));
        root.addContent(JDOMUtil.createElement("items", items));
        return root;
    }
}
