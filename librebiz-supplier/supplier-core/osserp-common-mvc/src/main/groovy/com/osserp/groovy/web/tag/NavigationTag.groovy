/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.osserp.groovy.web.tag

import javax.servlet.jsp.JspException
import javax.servlet.jsp.JspWriter

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.User
import com.osserp.common.web.SessionUtil
import com.osserp.common.web.tags.ImgTag
import com.osserp.common.web.tags.PermissionTag
import com.osserp.groovy.web.MenuItem
import com.osserp.groovy.web.WebUtil
import com.osserp.groovy.web.View

/**
 * @author rk <rk@osserp.com>
 * @author jg <jg@osserp.com>
 *
 */
public class NavigationTag extends AbstractGroovyTag {
    private static Logger logger = LoggerFactory.getLogger(NavigationTag.class.getName())
    private static final String[] REQUIRED_PERMISSION_DISPLAY = { "page_permission_display" };
    private static final String REQUIRED_PERMISSION_DISPLAY_PROPERTY = "permissionDisplay";

    private JspWriter out

    /**
     * Renders current view navigation links
     * @return navigation 
     */
    @Override
    protected String execute() {
        View view = request.getAttribute('view')
        def navigation = view?.getNavigation()
        if (!view || !navigation) {
            if (view) {
                logger.info("execute: view does not provide navigation [name=${view.name}]")
            } else {
                logger.warn('execute: did not find \'view\' in request scope')
            }
            return ''
        }
        logger.debug("execute: rendering navigation [name=${view.name}]")
        out = getJspContext().getOut()
        out.print('<ul>')
        if (pagePermissionLinkEnabled) {
            
        }
        navigation.each { MenuItem currentElement ->

            def body = {
                out.print('<li>')
                if (currentElement.ajaxPopup) {
                    callAjaxLinkTag(currentElement.link, currentElement.ajaxPopupName, currentElement.title) { callImgTag(currentElement.icon) }
                } else if(currentElement.newWindow) {
                    renderNewWindowLink(currentElement.link, currentElement.title) { callImgTag(currentElement.icon) }
                } else if (currentElement.execAction) {
                    callAjaxExecuteTag(currentElement.link, currentElement.ajaxPopupName, currentElement.title) { callImgTag(currentElement.icon) }
                } else if (currentElement.confirmLink) {
                    renderConfirmLink(currentElement.link, currentElement.title) { callImgTag(currentElement.icon) }
                } else if (currentElement.iconOnly) {
                    renderIconOnly(currentElement.title) { callImgTag(currentElement.icon) }
                } else {
                    renderLink(currentElement.link, currentElement.title) { callImgTag(currentElement.icon) }
                }
                out.print('</li>')
            }

            if (currentElement.permissions) {
                callPermissionTag(currentElement.permissions, currentElement.permissionInfo, body)
            } else {
                body()
            }
        }
        out.print('</ul>')
        return
    }

    private void callImgTag(String name) {
        def imgTag = new ImgTag()
        imgTag.setJspContext(jspContext)
        imgTag.setName(name)
        imgTag.doTag()
    }


    private void callAjaxLinkTag(String url, String linkId, String title, Closure body) {
        def ajaxLinkTag = new AjaxLinkTag()
        ajaxLinkTag.setPageContext(jspContext)
        ajaxLinkTag.setUrl(url)
        ajaxLinkTag.setLinkId(linkId)
        ajaxLinkTag.setTitle(title)
        if (ajaxLinkTag.doStartTag() == AjaxLinkTag.EVAL_BODY_INCLUDE) {
            body()
        }
        ajaxLinkTag.doEndTag()
        ajaxLinkTag.release()
    }

    private void callAjaxExecuteTag(String url, String linkId, String title, Closure body) {
        def ajaxExecuteTag = new AjaxExecuteTag()
        ajaxExecuteTag.setPageContext(jspContext)
        ajaxExecuteTag.setUrl(url)
        ajaxExecuteTag.setLinkId(linkId)
        ajaxExecuteTag.setTitle(title)
        if (ajaxExecuteTag.doStartTag() == AjaxExecuteTag.EVAL_BODY_INCLUDE) {
            body()
        }
        ajaxExecuteTag.doEndTag()
        ajaxExecuteTag.release()
    }

    private void callPermissionTag(String role, String info, Closure body) {
        def permissionTag = new PermissionTag()
        permissionTag.setPageContext(jspContext)
        permissionTag.setRole(role)
        permissionTag.setInfo(info)
        if (permissionTag.doStartTag() == AjaxLinkTag.EVAL_BODY_INCLUDE) {
            body()
        }
        permissionTag.doEndTag()
        permissionTag.release()
    }

    private void renderConfirmLink(String link, String title, Closure body) {
        StringBuilder buffer = new StringBuilder()
        buffer.append('<a href="javascript:;" onclick="confirmLink(\'')
        buffer.append(getResourceString('irrevocableAction'))
        buffer.append(':\\n')
        buffer.append(getResourceString(title))
        buffer.append('\',\'')
        buffer.append(link)
        buffer.append('\');" title="')
        buffer.append(getResourceString(title))
        buffer.append('">')
        out.print(buffer.toString())
        body()
        out.print('</a>')
    }

    private void renderNewWindowLink(String link, String title, Closure body) {
        StringBuilder buffer = new StringBuilder()
        buffer.append('<a href="javascript:;" onclick="')
        buffer.append(link)
        buffer.append('" title="')
        buffer.append(getResourceString(title))
        buffer.append('">')
        out.print(buffer.toString())
        body()
        out.print('</a>')
    }
    
    private void renderLink(String link, String title, Closure body) {
        StringBuilder buffer = new StringBuilder()
        buffer.append('<a href="')
        buffer.append(link)
        buffer.append('" title="')
        buffer.append(getResourceString(title))
        buffer.append('">')
        out.print(buffer.toString())
        body()
        out.print('</a>')
    }

    private void renderIconOnly(String title, Closure body) {
        StringBuilder buffer = new StringBuilder()
        if (title) {
            buffer.append('<span title="')
            buffer.append(getResourceString(title))
            buffer.append('">')
            out.print(buffer.toString())
        } else {
            out.print('<span>')
        }
        body()
        out.print('</span>')
    }
    
    protected boolean isPagePermissionLinkEnabled() {
        User user = SessionUtil.lookupUser(getSession())
        if (user != null && user.isPermissionGrant(REQUIRED_PERMISSION_DISPLAY)) {
            return user.isPropertyEnabled(REQUIRED_PERMISSION_DISPLAY_PROPERTY)
        }
        return false
    }
    
    protected String getPagePermissionLink() {
        StringBuilder buffer = new StringBuilder(
            createAjaxPopupLinkStart(
                '/permissions/showPagePermissions/show', 
                'permissionDisplayLink', null))
    }
}
