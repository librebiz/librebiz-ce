/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 13, 2011 8:01:23 PM 
 * 
 */
package com.osserp.core.dao;

import com.osserp.core.employees.Employee;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductPriceByQuantity;
import com.osserp.core.products.ProductPriceByQuantityMatrix;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ProductPricesByQuantity {

    /**
     * Provides current price matrix
     * @param product
     * @return priceMatrix or null if not exists
     */
    ProductPriceByQuantityMatrix getPriceMatrix(Product product);

    /**
     * Creates a new and empty price matrix
     * @param user
     * @param product
     * @return priceMatrix
     */
    ProductPriceByQuantityMatrix create(Employee user, Product product);

    /**
     * Adds a range to a price matrix
     * @param user
     * @param matrix
     * @param value
     */
    void addRange(Employee user, ProductPriceByQuantityMatrix matrix, Double value);

    /**
     * Removes last range
     * @param user
     * @param matrix
     */
    void removeRange(Employee user, ProductPriceByQuantityMatrix matrix);

    /**
     * Saves changed range
     * @param range
     */
    void save(ProductPriceByQuantity range);

    /**
     * Saves changed matrix
     * @param matrix
     */
    void save(ProductPriceByQuantityMatrix matrix);

    /**
     * Updates the prices of a range
     * @param user
     * @param matrix
     * @param priceByQuantity
     * @param consumerPrice
     * @param resellerPrice
     * @param partnerPrice
     */
    void updateRange(Employee user, ProductPriceByQuantityMatrix matrix,
            ProductPriceByQuantity priceByQuantity,
            Double consumerPrice,
            Double resellerPrice,
            Double partnerPrice);
}
