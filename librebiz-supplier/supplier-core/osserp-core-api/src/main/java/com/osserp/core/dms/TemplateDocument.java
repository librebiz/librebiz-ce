/**
 *
 * Copyright (C) 2015 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 6, 2015 
 * 
 */
package com.osserp.core.dms;

import com.osserp.common.dms.DmsDocument;
import com.osserp.common.dms.DocumentType;
import com.osserp.common.dms.model.DmsDocumentImpl;

import com.osserp.core.BusinessTemplate;

/**
 * Provides a dms document wrapper around with a template reference.
 * 
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public class TemplateDocument extends DmsDocumentImpl {
    
    private BusinessTemplate template;

    protected TemplateDocument() {
        super();
    }
    
    /**
     * Creates a new document wrapper
     * @param dmsDocument
     * @param referencedTemplate
     */
    public TemplateDocument(
            DmsDocument dmsDocument, 
            BusinessTemplate referencedTemplate) {
        super(dmsDocument);
        template = referencedTemplate;
    }
    
    public boolean isPrintTemplate() {
        if (isSet(getFileName())) {
            return (getFileName().startsWith("xsl")
                    || getFileName().startsWith("/xsl"));
        }
        return false;
    }

    public boolean isEmailText() {
        return isSet(getContextName()) &&
            getContextName().indexOf(DocumentType.EMAIL_TEMPLATE_TEXT) > 0;
    }

    public BusinessTemplate getTemplate() {
        return template;
    }

    public void setTemplate(BusinessTemplate template) {
        this.template = template;
    }
}
