/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 31-Aug-2006 15:37:28 
 * 
 */
package com.osserp.core.sales;

import java.util.List;

import com.osserp.common.EntityRelation;
import com.osserp.core.finance.DeliveryNote;
import com.osserp.core.finance.Order;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SalesOrderVolumeExportItem extends EntityRelation {

    /**
     * Provides the billed order
     * @return orderId
     */
    Long getOrderId();

    /**
     * Indicates that order is completely billed
     * @return completed
     */
    boolean isCompleted();

    /**
     * Provides all open (unbilled) deliveries by order
     * @param order to check for open deliveries
     * @return open
     */
    List<DeliveryNote> getOpen(Order order);

    /**
     * Adds adds deliveries if not already added
     * @param order
     * @param deliveries
     */
    void addDeliveries(Order order, List<Long> deliveries);

    /**
     * Indicates if delivery note has been already added
     * @param dn
     * @return true if already added
     */
    boolean added(DeliveryNote dn);

    /**
     * Provides the billed delivery notes
     * @return deliveries
     */
    List<SalesOrderVolumeExportDelivery> getDeliveries();
}
