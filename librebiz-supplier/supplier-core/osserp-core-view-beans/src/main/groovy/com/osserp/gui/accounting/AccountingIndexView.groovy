/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 *
 * Created on 13.05.2013
 * 
 */
package com.osserp.gui.accounting

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.Year
import com.osserp.common.util.DateUtil

import com.osserp.core.finance.AnnualReport
import com.osserp.core.finance.AnnualReportManager
import com.osserp.core.finance.TaxReport
import com.osserp.core.finance.TaxReportManager

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class AccountingIndexView extends AbstractAccountingView {
    private static Logger logger = LoggerFactory.getLogger(AccountingIndexView.class.getName())

    List<TaxReport> taxReports = []

    AnnualReport currentReport
    boolean currentYearOnly = true


    AccountingIndexView() {
        super()
        enableCompanyPreselection()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (forwardRequest) {
            bankAccounts = selectedCompany?.bankAccounts
            logger.debug("initRequest: bankAccounts initialized [count=${bankAccounts?.size()}]")
            year = DateUtil.getCurrentYear()
            if (selectedCompany?.accountingYears) {
                selectedCompany.accountingYears.each { Year obj ->
                    if (obj.year != year) {
                        currentYearOnly = false
                    }
                }
            }
        }
        if ((forwardRequest || reloadRequest) && selectedCompany) {
            
            taxReports = taxReportManager.getReports(selectedCompany)
            logger.debug("initRequest: taxReports initialized [count=${taxReports?.size()}]")
            
            currentReport = annualReportManager.getReport(selectedCompany, year)
        }
    }

    boolean isStocktakingSupportEnabled() {
        return systemConfigManager.stocktakingSupportEnabled
    }

    protected AnnualReportManager getAnnualReportManager() {
        getService(AnnualReportManager.class.getName())
    }

    protected TaxReportManager getTaxReportManager() {
        getService(TaxReportManager.class.getName())
    }
}
