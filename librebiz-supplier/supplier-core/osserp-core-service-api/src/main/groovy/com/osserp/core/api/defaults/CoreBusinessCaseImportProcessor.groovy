/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 14, 2016 
 * 
 */
package com.osserp.core.api.defaults

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.api.WSBusinessCase
import com.osserp.api.WSObjectException

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.Option
import com.osserp.common.OptionsCache
import com.osserp.common.util.DateUtil
import com.osserp.common.util.NumberUtil

import com.osserp.core.BusinessCase
import com.osserp.core.BusinessCaseCreator
import com.osserp.core.BusinessType
import com.osserp.core.FcsAction
import com.osserp.core.Item
import com.osserp.core.Options
import com.osserp.core.api.BusinessCaseServiceProcessor
import com.osserp.core.products.Product
import com.osserp.core.crm.Campaign
import com.osserp.core.customers.Customer
import com.osserp.core.dao.Products
import com.osserp.core.dao.BusinessCaseUtilities
import com.osserp.core.model.ItemImpl
import com.osserp.core.system.BranchOffice

/**
 * 
 * CoreBusinessCaseImportProcessor handles businessCase objects provided 
 * by an import file. 
 * BusinessCase objects provided by callers must follow the default format 
 * definitions and content based rules.   
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class CoreBusinessCaseImportProcessor extends AbstractBusinessCaseProcessor implements BusinessCaseServiceProcessor {
    private static Logger logger = LoggerFactory.getLogger(CoreBusinessCaseImportProcessor.class.getName())

    private static final Long INTEREST_TYPE = 1L
    private static final Long SALES_TYPE = 2L

    private BusinessCaseCreator businessCaseCreator
    private CoreCustomerServiceProcessor customerService
    private OptionsCache optionsCache
    private Products products

    protected CoreBusinessCaseImportProcessor(
            OptionsCache optionsCache,
            Products products,
            CoreCustomerServiceProcessor customerService,
            BusinessCaseUtilities businessCaseUtilities,
            BusinessCaseCreator businessCaseCreator) {
        super(businessCaseUtilities)
        this.products = products
        this.optionsCache = optionsCache
        this.customerService = customerService
        this.businessCaseCreator = businessCaseCreator
    }

    /**
     * Takes a businessCase provided by import file and creates or updates
     * the internal businessCase depending on provided values.
     * @param wsBusinessCase
     * @param performActions
     * @return businessCase internal businessCase representation
     * @throws WSObjectException if validation of input failed or create failed
     */
    BusinessCase createOrUpdate(WSBusinessCase wsBusinessCase, boolean performActions) throws WSObjectException {

        if (!wsBusinessCase.contact && !wsBusinessCase.customerId) {
            throw new WSObjectException('wsBusinessCase.contact or wsBusinessCase.customerId required')
        }
        Customer customer = customerService.findOrCreate(wsBusinessCase)
        if (!customer) {
            logger.info("createOrUpdate: no customer found or created...")
            return null
        }

        boolean externalBusinessId = false
        Long businessId = wsBusinessCase.primaryKey
        if (businessId) {
            externalBusinessId = true
            logger.debug("createOrUpdate: businessId provided [id=${businessId}]")
        } else {
            logger.debug("createOrUpdate: no businessId provided [customer=${customer.id}, name=${customer.displayName}]")
        }

        BusinessType businessType = businessCaseUtilities.fetchRequestType(
            wsBusinessCase.businessType, wsBusinessCase.businessTypeName)

        if (!businessType) {
            // not provided, fetch businessType by default
            
            BusinessType salesType = businessCaseUtilities.fetchRequestType(
                businessCaseUtilities.getPropertyId('wsBusinessCaseTypeDefaultSales'), null)

            if (salesType && wsBusinessCase.status) {
                if (salesType.isExternalStatusSales(wsBusinessCase.status)
                    || salesType.isExternalStatusQualifiedRequest(wsBusinessCase.status)) {
                    businessType = salesType
                }
            }
            if (!businessType) {
                // unqualified request (e.g. interest only) 
                Long requestTypeId = businessCaseUtilities.getPropertyId('wsBusinessCaseTypeDefaultRequest')
                if (!requestTypeId) {
                    requestTypeId = INTEREST_TYPE
                }
                businessType = businessCaseUtilities.fetchRequestType(requestTypeId, (String) null)
            }
            if (!businessType) {
                logger.info("createOrUpdate: did not find required businessType [customer=${customer.id}, name=${customer.displayName}, status=${wsBusinessCase.status}]")
                return null
            }
        }
        String businessContext = null
        // wsBusinessCase.context is currently not specified by import format 
        // and is determined by provided status
        if (businessType.isExternalStatusSales(wsBusinessCase.status)) {
            logger.debug("createOrUpdate: businessCase idenfified as sales [type=${businessType.id}, status=${wsBusinessCase.status}]")
            businessContext = 'sales'
        } else {
            logger.debug("createOrUpdate: businessCase idenfified as request [type=${businessType.id}, status=${wsBusinessCase.status}]")
            businessContext = 'request'
        }

        // check for branch
        boolean branchRequired = isEnabled('wsBusinessCaseBranchRequired')
        BranchOffice branchOffice = businessCaseUtilities.fetchBranch(
            wsBusinessCase.branchId, wsBusinessCase.branchKey, branchRequired)
        if (!branchOffice && branchRequired) {
            logger.error('createOrUpdate: branch expected and required but not provided!')
            throw new WSObjectException(ErrorCode.BRANCH_MISSING)
        }

        Campaign campaign = businessCaseUtilities.fetchCampaign(
            wsBusinessCase.campaignId,
            wsBusinessCase.campaignName,
            'wsBusinessCaseCreateMissingCampaign',
            'contactImportOrigin')

        Option origin = businessCaseUtilities.fetchOrigin(
            wsBusinessCase.originId,
            wsBusinessCase.originName,
            'wsBusinessCaseCreateMissingOrigin')

        FcsAction action = businessCaseUtilities.fetchFcsAction(businessContext,
            businessType, wsBusinessCase.statusId, wsBusinessCase.status)

        Date createdDate = getDateCreated(wsBusinessCase)

        String externalRef = wsBusinessCase.externalKey
        if (externalBusinessId) {
            externalRef = businessId.toString()
        }

        BusinessCase createdBusinessCase = businessCaseCreator.createOrUpdate(
            businessId,
            externalRef,
            wsBusinessCase.externalUrl,
            createdDate,
            businessContext,
            businessType,
            branchOffice,
            customer,
            wsBusinessCase.salesPersonId,
            wsBusinessCase.projectManagerId,
            wsBusinessCase.name,
            wsBusinessCase.street,
            wsBusinessCase.streetAddon,
            wsBusinessCase.zipcode,
            wsBusinessCase.city,
            wsBusinessCase.countryId,
            campaign,
            origin,
            wsBusinessCase.note,
            action,
            []) //items)
        return createdBusinessCase
    }

    BusinessCase override(WSBusinessCase wsBusinessCase) throws WSObjectException {
        logger.debug("override: invoked [target=${wsBusinessCase.targetId}]")

        BusinessType businessType = businessCaseUtilities.fetchRequestType(
            wsBusinessCase.businessType, wsBusinessCase.businessTypeName)

        BranchOffice branchOffice = businessCaseUtilities.fetchBranch(
            wsBusinessCase.branchId, wsBusinessCase.branchKey, false)

        Campaign campaign = businessCaseUtilities.fetchCampaign(
            wsBusinessCase.campaignId,
            wsBusinessCase.campaignName,
            null,
            null)

        Option origin = businessCaseUtilities.fetchOrigin(
            wsBusinessCase.originId,
            wsBusinessCase.originName,
            null)

        FcsAction action = businessCaseUtilities.fetchFcsAction('request',
            businessType, wsBusinessCase.statusId, wsBusinessCase.status)

        Date createdDate = getDateCreated(wsBusinessCase)

        String externalRef = wsBusinessCase?.primaryKey?.toString()

        BusinessCase overridden = businessCaseCreator.override(
            wsBusinessCase.targetId,
            wsBusinessCase.primaryKey, // businessId
            externalRef,
            wsBusinessCase.externalUrl,
            createdDate,
            'request', // contextName
            businessType,
            branchOffice,
            wsBusinessCase.salesPersonId,
            wsBusinessCase.projectManagerId,
            wsBusinessCase.name,
            wsBusinessCase.street,
            wsBusinessCase.streetAddon,
            wsBusinessCase.zipcode,
            wsBusinessCase.city,
            wsBusinessCase.countryId,
            campaign,
            origin,
            wsBusinessCase.note,
            action,
            [])
        customerService.override(overridden.customer, wsBusinessCase.contact)
        return businessCaseCreator.findExisting(overridden.primaryKey)
    }

}
