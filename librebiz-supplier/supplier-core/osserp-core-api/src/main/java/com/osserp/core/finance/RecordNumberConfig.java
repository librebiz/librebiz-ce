/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 6, 2016
 *  
 */
package com.osserp.core.finance;

import com.osserp.common.Option;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface RecordNumberConfig extends Option {

    /**
     * A config may provide a company reference. 
     * @return company id
     */
    Long getCompany();

    /**
     * Indicates if a number should change every year 
     * @return yearly
     */
    boolean isYearly();
    
    /**
     * Enables/disables yearly property 
     * @param yearly
     */
    void setYearly(boolean yearly);
    
    /**
     * Provides the start year of a yearly changing sequence
     * @return startYear
     */
    Long getStartYear();
    
    /**
     * Updates the start year setting. This is an optional 
     * setting for admin purpose. Reset if strictly required only. 
     * @param startYear
     */
    void setStartYear(Long startYear);
    
    /**
     * Provides the count of additional digits to append to a number if
     * number is constructed by a pattern plus additional sequence 
     * @return digits
     */
    int getDigits();
    
    /**
     * Updates count of digits to append to a pattern based number.
     * @param digits
     */
    void setDigits(int digits);
    
    /**
     * Indicates if sequence is dedicated to associated type (e.g. not shared
     * by other record types).
     * @return dedicated
     */
    boolean isDedicated();

    /**
     * Enables/disables dedicated property
     * @param dedicated
     */
    void setDedicated(boolean dedicated);

    /**
     * Provides the default number suggestion set by developers / distributor. 
     * @return default number suggestion
     */
    Long getDefaultNumber();

    /**
     * Provides the highest number if set by using workflow. 
     * This property is not persistent and empty until set. 
     * @return highest number if present, null on empty table
     */
    Long getHighestNumber();
    
    /**
     * Sets the highest number. This value must be queried and set manually.
     * @param highestNumber
     */
    void setHighestNumber(Long highestNumber);

    /**
     * Provides the lowest number if set by using workflow. 
     * This property is not persistent and empty until set. 
     * @return lowest number if present, null on empty table
     */
    Long getLowestNumber();
    
    /**
     * Sets the lowest number. This value must be queried and set manually.
     * @param lowestNumber
     */
    void setLowestNumber(Long lowestNumber);
}
