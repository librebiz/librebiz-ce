/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.gui.output;

import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdom2.Document;

import com.osserp.common.xml.JDOMUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class TextServlet extends XmlServlet {
    private static Logger log = LoggerFactory.getLogger(TextServlet.class.getName());

    /**
     * Renders an XML-Document directly to the response object's OutputStream
     */
    @Override
    public void renderXML(
            HttpServletResponse response,
            Document doc) throws ServletException {
        String text = JDOMUtil.getPrettyString(doc);
        if (log.isDebugEnabled()) {
            log.debug("renderXML() called");
            log.debug("renderXML() xml:\n" + text);
        }
        try {
            response.setContentType("text/plain");
            PrintWriter out = response.getWriter();
            out.println(text);
        } catch (Exception ex) {
            throw new ServletException(ex);
        }
    }

}
