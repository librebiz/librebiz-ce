<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="productSelectionConfigsSelectionView"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="title"><o:displayTitle/></tiles:put>

	<tiles:put name="javascript" type="string">
		<script type="text/javascript">

		function checkContext() {
			var select = $('contextId');
			var contextId = select.options[select.selectedIndex].value;
			var contextName = select.options[select.selectedIndex].text;
			var filterName = $('name').value;
			var send = true;
			if (contextId != <o:out value="${view.selectedContext.id}"/>) {
				if (!confirm('<fmt:message key="moveFilterToOtherContext"/>?\n' + filterName + ' -> ' + contextName)) {
					send = false;
				}
			}
			if (send) {
				$('productSelectionConfigForm').submit();
			}
		}

		</script>
	</tiles:put>

	<tiles:put name="headline">
		<fmt:message key="${view.headerName}"/>
	</tiles:put>
	<tiles:put name="headline_right">
		<v:navigation/>
	</tiles:put>

	<tiles:put name="content" type="string">

		<c:choose>
			<c:when test="${view.editMode || view.createMode}">
				<c:import url="_productSelectionConfigsEdit.jsp"/>
			</c:when>
			<c:otherwise>
				<c:import url="_productSelectionConfigsList.jsp"/>
			</c:otherwise>
		</c:choose>
	</tiles:put>
</tiles:insert>