/**
 *
 * Copyright (C) 2024 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License (AGPL)
 * version 3 as published by the Free Software Foundation. In accordance
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code
 * versions must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area
 * or, if not reasonably feasible for technical reason, as a top-level
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute.
 *
 * Created on 04 Apr 2024
 *
 */
package com.osserp.gui.tags;

import javax.servlet.jsp.JspException;

import com.osserp.common.dms.DmsDocument;
import com.osserp.common.web.tags.AbstractOutTag;
import com.osserp.groovy.web.WebUtil;

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public class ThumbnailTag extends AbstractOutTag {

    @Override
    public String getOutput() throws JspException {
        if (value != null && value instanceof DmsDocument) {
            DmsDocument doc = (DmsDocument) value;
            if (doc.isThumbnailAvailable()) {
                StringBuilder buff = new StringBuilder("<img src=\"");
                buff.append(WebUtil.createUrl(getRequest(), url))
                    .append("?id=").append(doc.getId())
                    .append("&type=").append(doc.getType().getId())
                    .append("\"");
                if (getStyleClass() != null) {
                    buff = addStyleClass(buff);
                }
                if (getStyle() == null) {
                    buff.append(" style=\"width:").append(fetchWidth(doc))
                        .append(";height:").append(fetchHeight(doc))
                        .append(";\"");
                } else {
                    buff = addStyle(buff);
                }
                buff.append(" alt=\"").append(doc.getFileName()).append("\" />");
                return buff.toString();
            }
        }
        return "";
    }

    protected String fetchWidth(DmsDocument doc) {
        return width != null ? width :
            doc.getType().getThumbmailWidth() > 0 ?
                    Integer.valueOf(doc.getType().getThumbmailWidth()) + "px" : "auto";
    }

    protected String fetchHeight(DmsDocument doc) {
        return height != null ? height :
            doc.getType().getThumbmailHeight() > 0 ?
                    Integer.valueOf(doc.getType().getThumbmailHeight()) + "px" : "72px";
    }

    private String url = null;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    private String width = null;

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    private String height = null;

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

}
