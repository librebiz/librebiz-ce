<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="portal" value="${sessionScope.portalView}"/>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="systemInfo"/>
                - <o:out value="${portal.sysInfo.hostAddress}" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">
        
            <c:if test="${!empty portal.sysInfo.hostName}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name">Hostname</label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${portal.sysInfo.hostName}" />
                        </div>
                    </div>
                </div>
            </c:if>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="name">Operating System</label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:out value="${portal.sysInfo.os}" />
                    </div>
                </div>
            </div>
            <c:if test="${!empty portal.webConfig.displayDistributor}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name">Distributor</label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${portal.webConfig.displayDistributor}" />
                        </div>
                    </div>
                </div>
            </c:if>
            <c:if test="${!empty portal.sysInfo.osserpProperties}">
                <c:forEach var="prp" items="${portal.sysInfo.osserpProperties}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label><o:out value="${prp.name}" /></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <o:out value="${prp.value}" />
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </c:if>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="name">Java vendor</label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:out value="${portal.sysInfo.javaVendor}" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="name">Java version</label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:out value="${portal.sysInfo.javaVersion}" />
                    </div>
                </div>
            </div>
           <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="name">Java Compiler</label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:out value="${portal.sysInfo.compiler}" />
                    </div>
                </div>
            </div>
            <c:if test="${portal.sysInfo.jvmName != portal.sysInfo.jvmVendor}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="displayName">JVM name</label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${portal.sysInfo.jvmName}" />
                        </div>
                    </div>
                </div>
            </c:if>
            <c:if test="${portal.sysInfo.javaVendor != portal.sysInfo.jvmVendor}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="displayName">JVM vendor</label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${portal.sysInfo.jvmVendor}" />
                        </div>
                    </div>
                </div>
            </c:if>
            <c:if test="${portal.sysInfo.javaVersion != portal.sysInfo.jvmVersion}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="displayName">JVM version</label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${portal.sysInfo.jvmVersion}" />
                        </div>
                    </div>
                </div>
            </c:if>
           <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="name">Number of CPU</label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:out value="${portal.sysInfo.availableProcessors}" />
                        x <o:out value="${portal.sysInfo.osArch}" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="name">Threads / Groups</label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:out value="${portal.sysInfo.activeThreadCount}" />
                        / <o:out value="${portal.sysInfo.activeThreadGroupCount}" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="name">Max.<br />Total<br />Free</label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:out value="${portal.sysInfo.maxMemory}" />  kB<br />
                        <o:out value="${portal.sysInfo.totalMemory}" /> kB<br />
                        <o:out value="${portal.sysInfo.freeMemory}" />  kB
                    </div>
                </div>
            </div>
            <c:forEach var="prp" items="${portal.sysInfo.otherProperties}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label><fmt:message key="${prp.name}" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${prp.value}" />
                        </div>
                    </div>
                </div>
            </c:forEach>

        </div>
    </div>
</div>
