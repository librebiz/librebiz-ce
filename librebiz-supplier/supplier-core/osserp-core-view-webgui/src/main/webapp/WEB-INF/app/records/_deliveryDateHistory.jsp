<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" scope="page" value="${sessionScope.deliveryDateHistoryView}"/>
<c:set var="list" scope="page" value="${view.list}"/>

<c:choose>
	<c:when test="${view.record}">
		<div>
			<span style="padding-left:15px;"><strong><fmt:message key="deliveryDatePopup.order"/> <o:out value="${view.id}"/></strong></span>
			<table class="table table-striped">
				<thead>
					<tr>
						<th><fmt:message key="atLabel"/></th>
						<th><fmt:message key="by"/></th>
						<th><fmt:message key="number"/></th>
						<th><fmt:message key="productName"/></th>
						<th style="text-align: left;"><fmt:message key="deliveryDateOld"/></th>
						<th style="text-align: left;"><fmt:message key="deliveryDateNew"/></th>
						<th style="text-align: right;"><fmt:message key="quantity"/></th>
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${empty list}">
							<tr>
								<td colspan="6"><fmt:message key="deliveryDatePopup.error"/></td>
							</tr>
						</c:when>
						<c:otherwise>
							<c:forEach var="obj" items="${list}">
								<c:if test="${!record.sales or obj.product.affectsStock}">
								<tr>
									<td style="text-align: right;"><fmt:formatDate value="${obj.created}"/></td>
									<td><span title="<oc:employee value="${obj.createdBy}"/>"><oc:employee initials="true" value="${obj.createdBy}" /></span></td>
									<td><o:out value="${obj.product.productId}"/></td>
									<td><o:out value="${obj.product.name}"/></td>
									<td style="text-align: right;"><fmt:formatDate value="${obj.previousDate}"/></td>
									<td style="text-align: right;"><fmt:formatDate value="${obj.newDate}"/></td>
									<td style="text-align: right;"><o:number value="${obj.quantity}" format="integer"/></td>
								</tr>
								</c:if>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</tbody>
			</table>
		</div>
	</c:when>
	<c:when test="${view.product}">
		<div>
			<span style="padding-left:15px;"><strong><fmt:message key="deliveryDatePopup.product"/> <o:out value="${view.id}"/></strong></span>
			<table class="table">
				<thead>
					<tr>
						<th><fmt:message key="atLabel"/></th>
						<th><fmt:message key="by"/></th>
						<th><fmt:message key="order"/>/<fmt:message key="purchaseOrder"/></th>
						<th style="text-align: left;"><fmt:message key="deliveryDateOld"/></th>
						<th style="text-align: left;"><fmt:message key="deliveryDateNew"/></th>
						<th style="text-align: right;"><fmt:message key="quantity"/></th>
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${empty list}">
							<tr>
								<td colspan="6"><fmt:message key="deliveryDatePopup.error"/></td>
							</tr>
						</c:when>
						<c:otherwise>
							<c:forEach var="obj" items="${list}">
								<tr>
									<td style="text-align: right;"><fmt:formatDate value="${obj.created}"/></td>
									<td><span title="<oc:employee value="${obj.createdBy}"/>"><oc:employee initials="true" value="${obj.createdBy}" /></span></td>
									<td><o:out value="${obj.reference}"/></td>
									<td style="text-align: right;"><fmt:formatDate value="${obj.previousDate}"/></td>
									<td style="text-align: right;"><fmt:formatDate value="${obj.newDate}"/></td>
									<td style="text-align: right;"><o:number value="${obj.quantity}" format="integer"/></td>
								</tr>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</tbody>
			</table>
		</div>
	</c:when>
	<c:otherwise>
		<div class="errormessage"><fmt:message key="deliveryDatePopup.error"/></div>
	</c:otherwise>
</c:choose>