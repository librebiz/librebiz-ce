<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<o:resource/>
<v:view viewName="documentImportView"/>
<c:set var="documentListView" scope="request" value="${documentImportView}"/>
<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="title"><o:displayTitle/></tiles:put>
	<tiles:put name="styles" type="string">
		<style type="text/css">
		.docListSize { text-align: right; }
		</style>
	</tiles:put>
	<tiles:put name="headline">
		<fmt:message key="${view.headerName}"/>
        <c:if test="${!empty view.documentType}">
        - <fmt:message key="currentLabel"/> <o:out value="${view.documentCount}"/>
        </c:if>
	</tiles:put>
	<tiles:put name="headline_right">
		<v:navigation/>
	</tiles:put>
	<tiles:put name="content" type="string">
        <div class="content-area" id="documentImportContent">
            <div class="row">
                <div class="col-md-6">
                    <c:import url="_documentListingShort.jsp"/>
                </div>
                <c:import url="_documentImportArea.jsp"/>
            </div>
		</div>
	</tiles:put>
</tiles:insert>
