/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 12, 2007 5:47:40 PM 
 * 
 */
package com.osserp.gui.client.records;

import java.util.List;

import com.osserp.common.web.View;
import com.osserp.core.system.SystemCompany;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface RecordDisplayView extends View {

    /**
     * Provides the available companies for selection
     * @return availableCompanies
     */
    public List<SystemCompany> getAvailableCompanies();

    /**
     * Provides selected companies whose records should be displayed
     * @return companies
     */
    public List<SystemCompany> getCompanies();

    /**
     * Adds a company to selected companies
     * @param id
     */
    public void addCompany(Long id);

    /**
     * Removes a company to selected companies
     * @param id
     */
    public void removeCompany(Long id);

    /**
     * Refreshs current list with current settings
     */
    public void refresh();

    /**
     * Indicates that internal records should be displayed too
     * @return true if so
     */
    public boolean isIncludeInternal();

    /**
     * Toggles internal display flag
     */
    public void toggleIncludeInternal();

    /**
     * Provides the net amount of all current selected records
     * @return netAmount
     */
    public Double getNetAmount();

    /**
     * Provides the net amount of all current selected records
     * @return taxAmount
     */
    public Double getTaxAmount();

    /**
     * Provides the net amount of all current selected records
     * @return reducedTaxAmount
     */
    public Double getReducedTaxAmount();

    /**
     * Provides the gross amount of all current selected records
     * @return grossAmount
     */
    public Double getGrossAmount();

    /**
     * Provides the due amount of all current selected records if value is supported by record type
     * @return dueAmount
     */
    public Double getDueAmount();

}
