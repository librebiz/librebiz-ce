/**
 *
 * Copyright (C) 2020 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 20, 2020
 * 
 */
package com.osserp.core.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.User;

import com.osserp.core.Plugin;
import com.osserp.core.PluginManager;
import com.osserp.core.PluginMapping;
import com.osserp.core.Plugins;
import com.osserp.core.dao.PluginStore;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PluginManagerImpl extends AbstractService implements PluginManager {

    private PluginStore pluginStore;
    private Plugins _plugins;

    public PluginManagerImpl(PluginStore pluginStore) {
        super();
        this.pluginStore = pluginStore;
    }

    @Override
    public Plugins getPlugins() {
        if (_plugins == null) {
           reload();
        }
        return _plugins;
    }

    @Override
    public Plugin fetch(String name) {
        return getPlugins().getPlugin(name);
    }

    @Override
    public Plugin get(String name) throws ClientException {
        Plugin plugin = getPlugins().getPlugin(name);
        if (plugin == null) {
            throw new ClientException(ErrorCode.PLUGIN_MISSING);
        }
        return plugin;
    }

    @Override
    public Plugin update(User user, Plugin plugin, String description, boolean disabled) {
        assert user != null && plugin != null;
        Plugin obj = load(plugin.getId());
        obj.setDescription(description);
        obj.setDisabled(disabled);
        obj.setChanged(new Date(System.currentTimeMillis()));
        obj.setChangedBy(user.getId());
        pluginStore.update(obj);
        reload();
        return load(obj.getId());
    }

    @Override
    public Plugin updateProperty(User user, Plugin plugin, String name, String value) {
        assert user != null && plugin != null;
        Plugin obj = load(plugin.getId());
        obj.setProperty(user, name, value);
        obj.setChanged(new Date(System.currentTimeMillis()));
        obj.setChangedBy(user.getId());
        pluginStore.update(obj);
        reload();
        return load(obj.getId());
    }

    @Override
    public PluginMapping getMapping(Plugin plugin, String name, Long entityId) {
        return pluginStore.getMapping(plugin.getId(), name, entityId);
    }

    @Override
    public PluginMapping createMapping(Plugin plugin, String name, Long mappedId, String objectId, String objectName, String objectType, String objectStatus) {
        return pluginStore.createMapping(plugin.getId(), name, mappedId, objectId, objectName, objectType, objectStatus);
    }

    @Override
    public void removeMapping(Plugin plugin, String name, Long entityId) {
        PluginMapping mapping = getMapping(plugin, name, entityId);
        if (mapping != null) {
            pluginStore.deleteMapping(mapping);
        }
    }

    @Override
    public PluginMapping updateMapping(PluginMapping mapping, String objectId, String objectName, String objectType, String objectStatus) {
        mapping.setObjectId(objectId);
        mapping.setObjectName(objectName);
        mapping.setObjectType(objectType);
        mapping.setObjectStatus(objectStatus);
        return pluginStore.updateMapping(mapping);
    }

    @Override
    public PluginMapping updateMapping(Plugin plugin, String name, Long mappedId, String status) {
        return pluginStore.updateMapping(plugin.getId(), name, mappedId, status);
    }

    protected Plugin load(Long id) {
        Plugin p = pluginStore.findById(id);
        if (p == null) {
            throw new ActionException(ErrorCode.ID_INVALID);
        }
        return p;
    }

    public void reload() {
        List<Plugin> result = new ArrayList<>();
        List<Plugin> all = pluginStore.findAll();
        all.forEach(plugin -> {
            if (!plugin.isDisabled()) {
                result.add(plugin);
            }
        });
        _plugins = new Plugins(result);
    }
}
