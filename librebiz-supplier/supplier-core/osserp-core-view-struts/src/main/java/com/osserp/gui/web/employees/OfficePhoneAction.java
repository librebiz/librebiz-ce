/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 19, 2008 5:29:09 PM 
 * 
 */
package com.osserp.gui.web.employees;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.View;
import com.osserp.gui.client.contacts.OfficePhoneView;
import com.osserp.gui.web.struts.AbstractViewDispatcherAction;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class OfficePhoneAction extends AbstractViewDispatcherAction {
    private static Logger log = LoggerFactory.getLogger(OfficePhoneAction.class.getName());

    @Override
    protected Class<OfficePhoneView> getViewClass() {
        return OfficePhoneView.class;
    }

    /**
     * Selects a branch if employee is associated with more than one. Missing id param resets current selection
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward selectBranch(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("selectBranch() request from " + getUser(session).getId());
        }
        OfficePhoneView view = (OfficePhoneView) fetchView(session);
        view.selectBranch(RequestUtil.fetchId(request));
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Removes current office phone selection
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward removeOfficePhone(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("removeOfficePhone() request from " + getUser(session).getId());
        }
        OfficePhoneView view = (OfficePhoneView) fetchView(session);
        view.removeOfficePhone();
        return exit(mapping, form, request, response);
    }

    @Override
    public ActionForward select(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("select() request from " + getUser(session).getId());
        }
        View view = fetchView(session);
        view.setSelection(RequestUtil.getId(request));
        return exit(mapping, form, request, response);
    }

}
