<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${sessionScope.emailSearchView}" />

<c:if test="${!empty sessionScope.statusMessage}">
	<div class="recordheader">
		<p><o:out value="${sessionScope.statusMessage}" /></p>
	</div>
	<c:remove var="statusMessage" scope="session" />
</c:if>

<table class="table table-striped">
	<thead>
		<c:choose>
			<c:when test="${!empty view.collected}">
				<tr>
					<th id="name"><fmt:message key="emailRecipientSelectedLabel" /></th>
					<th id="email"><fmt:message key="email"/></th>
					<th id="city"><fmt:message key="city" /></th>
					<th id="type"><fmt:message key="typeLabel"/></th>
					<th id="group"><fmt:message key="grp"/></th>
				</tr>
			</c:when>
			<c:otherwise>
				<tr>
					<th id="name"><fmt:message key="emailRecipientSelectionLabel" /></th>
					<th id="email"><fmt:message key="email"/></th>
					<th id="city"><fmt:message key="city" /></th>
					<th id="type"><fmt:message key="typeLabel"/></th>
					<th id="group"><fmt:message key="grp"/></th>
				</tr>
			</c:otherwise>
		</c:choose>
	</thead>
	<tbody style="height: 550px;">
		<c:forEach var="contact" items="${view.collected}" varStatus="s">
			<tr>
				<td valign="top">
					<a href="<v:url value="${view.baseLink}/removeCollected?id=${contact.primaryKey}"/>"><o:out value="${contact.displayName}" /></a>
					<c:if test="${!empty contact.parent}"> (<o:out value="${contact.parent}"/>)</c:if>
				</td>
				<td valign="top"><o:email value="${contact.email}" /></td>
				<td	valign="top"><o:out value="${contact.addressDisplay}" /></td>
				<td valign="top"><span title="${contact.type.description}"><o:out value="${contact.type.shortname}"/></span></td>
				<td valign="top">
					<c:if test="${contact.employee}"><span title="<fmt:message key="employee"/>"><fmt:message key="contact.type.employee"/> </span></c:if>
					<c:if test="${contact.customer}"><span title="<fmt:message key="customer"/>"><fmt:message key="contact.type.customer"/> </span></c:if>
					<c:if test="${contact.supplier}"><span title="<fmt:message key="supplier"/>"><fmt:message key="contact.type.supplier"/> </span></c:if>
				</td>
			</tr>
		</c:forEach>
		<c:if test="${!empty view.collected}">
			<tr>
				<td colspan="5" style="text-align:right;"><input onclick="gotoUrl('<v:url value="${view.baseLink}/select"/>');" type="button" value="<fmt:message key="selectSelected"/>"/></td>
			</tr>
			<tr>
				<th id="name"><fmt:message key="emailRecipientSelectionLabel" /></th>
				<th id="email"><fmt:message key="email"/></th>
				<th id="city"><fmt:message key="city" /></th>
				<th id="type"><fmt:message key="typeLabel"/></th>
				<th id="group"><fmt:message key="grp"/></th>
			</tr>
		</c:if>
		<c:choose>
			<c:when test="${empty view.list}">
				<tr>
					<td valign="top" colspan="5">
						<fmt:message key="searchUnsuccessfulPleaseChangeYourInput" />
					</td>
				</tr>
			</c:when>
			<c:otherwise>
				<c:forEach var="contact" items="${view.list}" varStatus="s">
					<tr>
						<td valign="top">
							<a href="<v:url value="${view.baseLink}/collect?id=${contact.primaryKey}"/>"><o:out value="${contact.displayName}" /></a>
							<c:if test="${!empty contact.parent}"> (<o:out value="${contact.parent}"/>)</c:if>
						</td>
						<td valign="top"><o:email value="${contact.email}" /></td>
						<td	valign="top"><o:out value="${contact.addressDisplay}" /></td>
						<td valign="top"><span title="${contact.type.description}"><o:out value="${contact.type.shortname}"/></span></td>
						<td valign="top">
							<c:if test="${contact.employee}"><span title="<fmt:message key="employee"/>"><fmt:message key="contact.type.employee"/> </span></c:if>
							<c:if test="${contact.customer}"><span title="<fmt:message key="customer"/>"><fmt:message key="contact.type.customer"/> </span></c:if>
							<c:if test="${contact.supplier}"><span title="<fmt:message key="supplier"/>"><fmt:message key="contact.type.supplier"/> </span></c:if>
						</td>
					</tr>
				</c:forEach>
			</c:otherwise>
		</c:choose>

		<tr style="height: 0px; visibility: hidden;">
			<td colspan="5"> </td>
		</tr>
	</tbody>
</table>
