/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 26, 2007 9:12:12 AM 
 * 
 */
package com.osserp.common.web;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.User;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface LoginView {

    /**
     * Indicates if user was logged in when view was created
     * @return userLoggedIn
     */
    public boolean isUserLoggedIn();

    /**
     * Tries to log in a user
     * @param loginName
     * @param password
     * @param remoteIp
     * @return user logged in
     * @throws PermissionException if login failed by invalid credentials
     */
    public User login(String loginName, String password, String remoteIp) throws PermissionException;

    /**
     * Loggs out current user. This avoids wrong userLoggedIn status even when view is not removed by caller
     */
    public void logout();

    /**
     * Changes the password
     * @param password
     * @param confirmPassword
     * @throws ClientException if validation failed
     */
    public void updatePassword(String password, String confirmPassword) throws ClientException;

    /**
     * Provides the user if logged in
     * @return user or null if not logged in
     */
    public User getUser();

}
