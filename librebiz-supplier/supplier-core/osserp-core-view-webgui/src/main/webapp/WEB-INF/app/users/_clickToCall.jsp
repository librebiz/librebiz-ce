<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<c:set var="view" value="${sessionScope.clickToCallView}" />
<c:set var="portalView" value="${sessionScope.portalView}" />
<c:set var="url" value="/users/clickToCall" />

<c:if test="${!empty view}">
    <div class="modalBoxHeader">
        <div class="modalBoxHeaderLeft" style="font-weight: bold;">
            <fmt:message key="${view.headerName}" />
        </div>
    </div>
    <div class="modalBoxData">
        <div class="subcolumns">
            <div class="subcolumn">
                <c:if test="${!empty sessionScope.error}">
					&nbsp;
					<div class="errormessage">
                        <fmt:message key="error" />
                        :
                        <fmt:message key="${sessionScope.error}" />
                    </div>
                    <div class="spacer"></div>
                    <o:removeErrors />
                </c:if>
                <v:ajaxForm name="dynamicForm" url="${url}/doClickToCall" activityElement="click_to_call_popup_content" targetElement="click_to_call_popup" preRequest="if (ojsForms.validateForm(this)) {" postRequest="}">
                    <c:choose>
                        <c:when test="${view.editMode}">
                            <div class="spacer"></div>
                            <div id="click_to_call_popup_content">
                                <table class="valueTable input">
                                    <tbody>
                                        <tr>
                                            <td><fmt:message key="yourPhone" />:</td>
                                            <td><select id="phone" name="phone" <c:if test="${empty view.phones}">disabled="disabled"</c:if>>
                                                    <c:if test="${empty view.phones}">
                                                        <option value="" selected="selected"><fmt:message key="selection" /></option>
                                                    </c:if>
                                                    <c:forEach var="obj" items="${view.phones}">
                                                        <option value="${obj.id}"><o:out value="${obj.displayName}" /> -
                                                            <o:out value="${obj.internal}" /></option>
                                                    </c:forEach>
                                            </select></td>
                                        </tr>
                                        <tr>
                                            <td><fmt:message key="phoneNumber" />:</td>
                                            <td><input class="mandatory integer" type="hidden" name="pnumber" id="pnumber" value="<o:out value="${view.phoneNumber}"/>" /> <o:out value="${view.phoneNumber}" /></td>
                                        </tr>
                                        <tr>
                                            <td class="row-submit"><v:ajaxLink url="${url}/exit" targetElement="${view.name}_popup">
                                                    <input type="button" class="cancel" value="<fmt:message key="exit"/>" />
                                                </v:ajaxLink></td>
                                            <td class="row-submit"><o:submit value="callPhoneNumber" /></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="spacer"></div>
                        </c:when>
                        <c:otherwise>
                            <div class="spacer"></div>
                            <div id="click_to_call_popup_content">
                                <table class="valueTable input">
                                    <tbody>
                                        <tr>
                                            <td><fmt:message key="yourPhone" />:</td>
                                            <td><c:choose>
                                                    <c:when test="${empty view.currentTelephoneConfiguration}">
                                                        <fmt:message key="error.no.telephoneConfiguration.selected" />
                                                        <input type="hidden" name="phone" id="phone" value="" />
                                                    </c:when>
                                                    <c:otherwise>
                                                        <o:out value="${view.currentTelephoneConfiguration.displayName}" /> - <o:out value="${view.currentTelephoneConfiguration.internal}" />
                                                        <input type="hidden" name="phone" id="phone" value="<o:out value="${view.currentTelephoneConfiguration.id}"/>" />
                                                    </c:otherwise>
                                                </c:choose></td>
                                        </tr>
                                        <tr>
                                            <td><fmt:message key="phoneNumber" />:</td>
                                            <td><input class="mandatory integer" type="hidden" name="pnumber" id="pnumber" value="<o:out value="${view.phoneNumber}"/>" /> <o:out value="${view.phoneNumber}" /></td>
                                        </tr>
                                        <tr>
                                            <td class="row-submit"></td>
                                            <td class="row-submit"><o:submit value="redial" /></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="spacer"></div>
                        </c:otherwise>
                    </c:choose>
                </v:ajaxForm>
            </div>
        </div>
    </div>
</c:if>

