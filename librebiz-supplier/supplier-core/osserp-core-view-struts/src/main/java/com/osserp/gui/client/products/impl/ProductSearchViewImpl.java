/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 2, 2008 10:17:28 AM 
 * 
 */
package com.osserp.gui.client.products.impl;

import java.util.Set;

import com.osserp.common.web.ViewName;

import com.osserp.gui.client.products.ProductSearchView;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author so <so@osserp.com>
 * 
 */
@ViewName("productSearchView")
public class ProductSearchViewImpl extends AbstractProductSearchView implements ProductSearchView {

    protected ProductSearchViewImpl() {
        super();
    }

    protected ProductSearchViewImpl(Set<String> methods, String defaultMethod) {
        super(methods, defaultMethod, false);
        setProvidingSerialNumberSearch(true);
    }
}
