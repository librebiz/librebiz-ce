/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 15, 2011 9:27:31 AM 
 * 
 */
package com.osserp.gui.selections

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.core.BusinessType
import com.osserp.core.BusinessTypeManager
import com.osserp.core.BusinessTypeSelection

/**
 *
 * @author eh <eh@osserp.com>
 * @author cf <cf@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
class BusinessTypeSelectionView extends AbstractSelectionView {
    private static Logger logger = LoggerFactory.getLogger(BusinessTypeSelectionView.class.getName())

    String selection = null
    BusinessTypeSelection businessTypeSelection = null

    BusinessTypeSelectionView() {
        super()
        headerName = 'selectionOrderType'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (forwardRequest) {
            selection = form.getString('selection')
            if (!selection) {
                logger.debug('initRequest: forward does not provide \'selection\' param, loading all activated..')
                list = []
                List<BusinessType> types = businessTypeManager.findActive()
                String context = getString('context')
                Long exclude = getLong('exclude')
                if (!context && !exclude) {
                    list = types
                } else {
                    types.each { 
                        if (it.id != exclude
                                && (!context || ('sales' == context && !it.interestContext))) {
                            list << it
                        }
                    }
                }
                logger.debug("initRequest: done [count=${list.size()}, context=${context}, exclude=${exclude}]")
            } else {
                logger.debug("initRequest: forward provides 'selection' param, loading via 'businessTypeManager.findTypeSelection(${selection})'")
                businessTypeSelection = businessTypeManager.findTypeSelection(selection)
                list = businessTypeSelection?.types.findAll { it.active }
            }
        }
    }

    private BusinessTypeManager getBusinessTypeManager() {
        getService(BusinessTypeManager.class.getName())
    }
}
