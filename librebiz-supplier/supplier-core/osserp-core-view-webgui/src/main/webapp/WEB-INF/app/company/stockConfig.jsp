<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="stockConfigView"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title"><o:displayTitle/></tiles:put>

    <tiles:put name="headline">
        <c:choose>
            <c:when test="${view.headquarter}"><fmt:message key="headquarter" /></c:when>
            <c:otherwise><fmt:message key="branchOffice" /></c:otherwise>
        </c:choose>
        <span> - </span><o:out value="${view.stockLocationName}" />
        <span> - </span><fmt:message key="stock" />
    </tiles:put>

    <tiles:put name="headline_right">
        <v:navigation/>
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="stockConfigContent">
            <v:form id="stockConfigForm" url="${view.baseLink}/save">
                <c:import url="_stockEdit.jsp"/>
            </v:form>
        </div>
    </tiles:put>
</tiles:insert>
