<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<c:set var="communicationView" value="${requestScope.communicationView}"/>
<c:choose>
    <c:when test="${!empty requestScope.communicationViewContact}">
        <c:set var="contact" value="${requestScope.communicationViewContact}"/>
    </c:when>
    <c:otherwise>
        <c:set var="contact" value="${communicationView.bean}"/>
    </c:otherwise>
</c:choose>
<c:set var="readonlyView" value="${requestScope.communicationViewReadonly}"/>
<div class="contentBox">
    <div class="contentBoxHeader">
        <div class="contentBoxHeaderLeft">
            <fmt:message key="fax"/>
        </div>
        <div class="contentBoxHeaderRight">
            <div class="boxnav">
                <c:if test="${empty readonlyView}">
                    <ul><li><v:ajaxLink url="/contacts/contactPhone/forward?device=3&view=${communicationView.name}" linkId="contact_phones"><o:img name="newIcon" /></v:ajaxLink></li></ul>
                        </c:if>
            </div>
        </div>
    </div>
    <div class="contentBoxData">
        <div class="subcolumns">
            <div class="subcolumn">
                <div class="smallSpacer"></div>
                <table class="valueTable first33">
                    <tbody>
                        <c:forEach var="phone" items="${contact.faxNumbers}">
                            <tr>
                                <td>
                                    <oc:options name="phoneTypes" value="${phone.type}"/> <c:if test="${phone.primary}"><fmt:message key="mainNumberShort"/></c:if>
                                    </td>
                                    <td>
                                    <c:choose>
                                        <c:when test="${!empty phone.note}"><span title="<o:out value="${phone.note}"/>"><oc:phone value="${phone}"/></span></c:when>
                                        <c:otherwise><oc:phone value="${phone}"/></c:otherwise>
                                    </c:choose>
                                </td>
                            </tr>
                        </c:forEach>
                        <c:forEach var="person" items="${communicationView.contactPersons}">
                            <c:forEach var="phone" items="${person.faxNumbers}">
                                <tr>
                                    <td>
                                        <oc:options name="phoneTypes" value="${phone.type}"/> <c:if test="${phone.primary}"><fmt:message key="mainNumberShort"/></c:if>
                                        </td>
                                        <td>
                                        <c:choose>
                                            <c:when test="${!empty phone.note}"><span title="<o:out value="${phone.note}"/>"><oc:phone value="${phone}"/></span></c:when>
                                            <c:otherwise><oc:phone value="${phone}"/></c:otherwise>
                                        </c:choose>
                                        <span> / </span>
                                        <a href="<c:url value="/contactPersons.do?method=load&id=${person.contactId}&exit=contactDisplay"/>">
                                            <c:if test="${!empty person.salutation.name}"><o:out value="${person.salutation.name}"/></c:if>
                                            <c:if test="${!empty person.title.name}"><o:out value="${person.title.name}"/></c:if>
                                            <c:choose>
                                                <c:when test="${empty person.salutation.name and empty person.title.name}">
                                                    <o:out value="${person.displayName}"/>
                                                </c:when>
                                                <c:otherwise>
                                                    <o:out value="${person.lastName}"/>
                                                </c:otherwise>
                                            </c:choose>
                                        </a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </c:forEach>
                    </tbody>
                </table>
                <div class="smallSpacer"></div>
            </div>
        </div>
    </div>
</div>
