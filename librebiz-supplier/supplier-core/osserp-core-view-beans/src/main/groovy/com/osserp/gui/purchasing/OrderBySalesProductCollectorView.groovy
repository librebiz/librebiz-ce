/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.gui.purchasing

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.core.BusinessCase
import com.osserp.core.Item
import com.osserp.core.finance.RecordDisplayItem
import com.osserp.core.finance.RecordSearch
import com.osserp.core.products.Product
import com.osserp.core.products.ProductSelectionConfig
import com.osserp.core.products.ProductSelectionConfigManager
import com.osserp.core.sales.SalesOrderManager
import com.osserp.core.sales.SalesOrder

import com.osserp.groovy.web.MenuItem
import com.osserp.groovy.web.ViewContextException

import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class OrderBySalesProductCollectorView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(OrderBySalesProductCollectorView.class.getName())

    SalesOrder salesOrder
    List<RecordDisplayItem> purchasedItems = []
    boolean filterPurchased = true
    Long supplierId

    OrderBySalesProductCollectorView() {
        dependencies = ['salesOrderView', 'businessCaseView']
        headerName = 'selectProductsToOrder'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            def selectionTarget = form.getString('selectionTarget')
            if (!selectionTarget) {
                logger.error("initRequest: not found [name=selectionTarget]")
                throw new ViewContextException(this)
            }
            env.selectionTarget = createTarget(selectionTarget)
            logger.debug("initRequest: forward request performed [selectionTarget=${env.selectionTarget}]")

            if (!env.salesOrderView?.record && !env.businessCaseView?.sales) {
                throw new ViewContextException(this)
            }
            if (env.salesOrderView?.record) {
                salesOrder = env.salesOrderView.record
            } else {
                BusinessCase businessCase = env.businessCaseView?.sales
                salesOrder = salesOrderManager.getBySales(businessCase)
            }

            supplierId = form.getLong('supplierId')
            logger.debug("initRequest: salesOrder fetched [id=${salesOrder?.id}]")
            if (salesOrder.businessCaseId) {
                purchasedItems = recordSearch.findPurchaseItems(salesOrder.businessCaseId)
                if (purchasedItems.empty) {
                    filterPurchased = false
                }
            }
            ProductSelectionConfig productSelectionConfig
            def selectionConfigId = form.getLong('selectionConfig')
            if (selectionConfigId) {
                productSelectionConfig = productSelectionConfigManager.getSelectionConfig(selectionConfigId)
                logger.debug("initRequest: product selection found [id=${productSelectionConfig?.id}]")
            }
            list = []
            List<Item> all = salesOrder.getItems()
            all.each {
                Product product = it.getProduct()
                if (!productSelectionConfig || productSelectionConfig.isMatching(product)) {
                    Item toAdd = it.clone()
                    RecordDisplayItem purchased = getPurchasedItem(product.productId)
                    if (purchased) {
                        if (toAdd.quantity > purchased.quantity) {
                            toAdd.quantity = toAdd.quantity - purchased.quantity
                            list << toAdd
                        }
                    } else {
                        list << toAdd
                    }
                }
            }
        }
    }

    private RecordDisplayItem getPurchasedItem(Long productId) {
        RecordDisplayItem result = null
        if (filterPurchased) {
            purchasedItems.each {
                if (it.productId == productId) {
                    result = it
                }
            }
        }
        return result
    }

    private ProductSelectionConfigManager getProductSelectionConfigManager() {
        getService(ProductSelectionConfigManager.class.getName())
    }

    protected RecordSearch getRecordSearch() {
        getService(RecordSearch.class.getName())
    }

    protected SalesOrderManager getSalesOrderManager() {
        getService(SalesOrderManager.class.getName())
    }
}
