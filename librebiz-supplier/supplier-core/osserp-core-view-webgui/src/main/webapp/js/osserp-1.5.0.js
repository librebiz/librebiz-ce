/**
 * osserp-core - osserp-1.5.0.js - Basic functions.
 * Copyright 2001-2021 The original author or authors.
 * Licensed under the Apache License, Version 2.0.
 */

function startUpHelp(helpLinkUrl) {
	osdb_help=window.open(helpLinkUrl,'osserp.com - Support','width=1024,height=725,screenX=0,screenY=0,status=1,menubar=0,scrollbars=no,resizable=0,toolbar=0,top=100,left=200');
}

function initSearch() {
  if (!document.getElementById) {
    return false;
  }
  try {
    var f = document.getElementById('searchForm');
    var u = f.elements[0];
    f.setAttribute("autocomplete", "off");
    u.focus();
  } catch (e) {
    return false;
  }
}

function setSearchFocus() {
  try {
    $('value').focus();
  } catch (e) {
    return false;
  }
}

function setLoginFocus() {
  var focusControl = document.forms["userForm"].elements["loginName"];
	
  if (focusControl.type != "hidden" && !focusControl.disabled) {
    focusControl.focus();
  }
}

function setMethod(name) {
  var form = document.forms[0];
  form.method.value = name;
  return true;
}

function enableUpdateOnly(url) {
  var form = document.forms[0];
  form.updateOnly.value = 'true';
  if (url) {
	  form.updateOnlyTarget.value = url;
  }
  return true;
}

function sendForm() {
  document.forms[0].submit();
}

function getUploadFile(){
  document.getElementById("uploadFile").click();
}

function submitUploadFile(obj) {
  var file = obj.value;
  var fileName = file.split("\\");
  document.getElementById("btn-upload").innerHTML = fileName[fileName.length-1];
  // most file upload forms provide other input fields so we should not
  // submit the form just for file selection. 
  // TODO add a param autosubmit to control submission
  //document.uploadForm.submit();
  //event.preventDefault();
}

function isNumeric(sText) {
  ojsCommon.isNumeric(sText);
  //just a fallback dummy
  //function was moved to osserp-common.js
}

function replaceToken(value, token, replaceWith) {
  return value.replace(token, replaceWith);
}

function replaceCommaWithDot(value) {
  var token = /,/gi;
  return replaceToken(value, token, ".");
}

function replaceDotWithComma(value) {
  var token = /\./gi;
  return replaceToken(value, token, ",");
}

function removeAll(value, token) {
  var test = value.indexOf(token);
  while (test > -1) {
    var s = value.substr(0,test);
    var e = value.substring(test + 1, value.length);
    value = s + e;
    test = value.indexOf(token);
  }
  return value;
}


function createDouble(value) {
  var rc = value.lastIndexOf(",");
  var lc = value.indexOf(",");
  var rd = value.lastIndexOf(".");
  var ld = value.indexOf(".");
	
  if (rc < 0 && rd < 0) {           
    return new Number(value);
		
  } else if (rc >= 0 && rd < 0) {   
    if (lc != rc) {           
      return new Number(0);
    } else {
      return new Number(replaceCommaWithDot(value));
    }
  } else if (rc < 0 && rd >= 0) {   
    if (ld != rd) {           
      return new Number(0);
    } else {
      return new Number(value);
    }
  } else {											  	
    if (rc < rd) {            
      return new Number(removeAll(value, ","));
			
    } else {                      	
      var noDot = removeAll(value, ".");
      return new Number(replaceCommaWithDot(noDot));
    }
  } 
  return new Number(0);
}

function round(value, decimalDigits) {
	if (decimalDigits < 1) {
		return Math.round(value);
	}
	var e = Math.pow(10, decimalDigits);
	var k = (Math.round(value * e) / e).toString();
	if (k.indexOf('.') == -1) k += '.';
	k += e.toString().substring(1);
	return k.substring(0, k.indexOf('.') + decimalDigits+1);
}

function show_info(tagId) {
  var myElement=document.getElementById(tagId);
  myElement.style.display='block';
}

function hide_info(tagId) {
  var myElement=document.getElementById(tagId);
  myElement.style.display='none';
}

function toggleDisplay(elementName) {
	ojsCommon.toggleDisplay(elementName);
	//just a fallback dummy
	//function was moved to osserp-common.js
}

function toggleVisibility(elementName) {
  	ojsCommon.toggleVisibility(elementName);
	//just a fallback dummy
	//function was moved to osserp-common.js
}


function doRequest(url) {
  var ajaxRequest = new AjaxRequest(url);
  ajaxRequest.sendRequest();
}
 
function gotoUrl(target) {
  location.href=target;
}

function confirmLink(question, target) {
  if (confirm(question)) { 
    location.href=target;
  } 
}

function jumpToFormUrl(formName) {
  var form = document.getElementById(formName);
  var jumpUrl = form.jumpUrl.value;
  location.href=jumpUrl;
}

function jumpToElementValue(element) {
  var jumpUrl = element.value;
  location.href=jumpUrl;
}

function showElement(element) {
  document.getElementById(element).style.visibility="visible";
}
 
function hideElement(element) {
  document.getElementById(element).style.visibility="hidden";
}

var mainHelpDisplayState = true;

function changeMainHelpDisplay() {
  if (mainHelpDisplayState == true) {
    mainHelpDisplayState = false;
    hideElement('mainHelp');
  } else {
    mainHelpDisplayState = true;
    showElement('mainHelp');
  }
}


var recordConditionsDisplayed = false;

function changeConditionDisplay() {
  if (recordConditionsDisplayed) {
    this.recordConditionsDisplayed = false;
    document.getElementById("conditions").style.display="none";
  } else {
    this.recordConditionsDisplayed = true;
    document.getElementById("conditions").style.display="inline";
  }
}


function performProjectNameEdit(url) {
  var ajaxRequest = new AjaxRequest(url);
  ajaxRequest.sendRequest();
}

function performProjectNameUpdate(url) {
  var ajaxRequest = new AjaxRequest(url);
  ajaxRequest.addFormElements('requestNameForm');
  ajaxRequest.setUsePOST();
  ajaxRequest.sendRequest();
}


/* HELP DIV ACTIONS */

function performHelpRequest(url) {
  var ajaxRequest = new AjaxRequest(url);
  ajaxRequest.sendRequest();
}

function createHelp(url) {
  var ajaxRequest = new AjaxRequest(url);
  ajaxRequest.addFormElementsById("method", "subject");
  ajaxRequest.sendRequest();
}

function editHelp(url) {
  var ajaxRequest = new AjaxRequest(url);
  ajaxRequest.addFormElementsById("method", "subject", "message");
  ajaxRequest.sendRequest();
}

/* DATE ACTIONS  */

function checkDate(vDate, required) {
  var s = vDate.value;
  if (s.length < 1 && !required) {
    return true;
  }
  var ok = validateDate(vDate);
  if (!ok && !required) {
    return true;
  }
  return ok;
}

function validateDate(vDate) {
	var enteredDate = vDate.value;
  var actualDate = new Date();
  var actualDay = actualDate.getDate();
  var actualMonth = actualDate.getMonth() + 1;

  var day = parseInt(enteredDate.substring(0,2),10);
  var month = parseInt(enteredDate.substring(3,5),10);
  var year = parseInt(enteredDate.substring(6,10),10);
  if (year < 100) {
    year = year + 2000;
  }

  if (month==4 || month==6 || month==9 || month==11) {
    daysInMonth=30;
  }
  else if (month==1 || month==3 || month==5 || month==7 || month==8 || month==10 || month==12) {
    daysInMonth=31;
  }
  else if(month==2 && year%4==0 && year%100!=0 || year%400==0) {
    daysInMonth=29;
  }
  else if(month==2 && year%4!=0 || year%100==0 && year%400!=0) {
    daysInMonth=28;
  }
  else {
    daysInMonth=0;
  }

  if (day >= 1 && day <= daysInMonth) {
    return true;
  } else {
    alert(ojsI18n.enterAValidDate);
    vDate.focus();
    return false;
  }
}


function popup(url, name, width, height) {
  var dimension = 'width=' + width + ',height=' + height;
  window.open(url,name,dimension);
} 
	
function splitUrl(urlToSplit) {
  var result = new Array(2);
  var i = urlToSplit.indexOf('?');
  if (i > -1) {
    result[0] = urlToSplit.substring(0,i);
    result[1] = urlToSplit.substring(i+1,urlToSplit.length);
  } else {
    result[0] = urlToSplit;
    result[1] = '';
  }
  return result;
}

/*
 begin: infoBox
*/
function initInfoBox() {
	var links = document.getElementsByClassName("infoBoxTrigger");

	for (var i=0; i<links.length; i++) {
		var link = links[i];
		$(link).onmouseover = function() {
			var select = $(this.id + "_select");
			var infoBox = $(select.value + "_ib");

			if ((infoBox.getDimensions().width+Element.viewportOffset(this).left+12) <= document.viewport.getWidth()) {
				infoBox.style.left = (Element.viewportOffset(this).left+12) + "px";
			} else {
				infoBox.style.left = (document.viewport.getWidth()-infoBox.getDimensions().width) + "px";
			}

			if ((infoBox.getDimensions().height+Element.viewportOffset(this).top+10) <= document.viewport.getHeight()) {
				infoBox.style.top = (Element.viewportOffset(this).top+10) + "px";
			} else {
				infoBox.style.top = (document.viewport.getHeight()-infoBox.getDimensions().height) + "px";
			}

			infoBox.style.visibility = "visible";
		}
		$(link).onmouseout = function() {
			var select = $(this.id + "_select");
			var infoBox = $(select.value + "_ib");
			infoBox.style.visibility = "hidden";
		}
	}
}
/*
 end: infoBox
 */

/*
 begin: zipcodeSearch
*/
function applyZipcodeSearchValues(zipcode, city, street) {
        document.contactForm.zipcode.value = zipcode;
        document.contactForm.city.value = city;
		if (street != null) {
			document.contactForm.street.value = street;
		}
}
/*
 end: zipcodeSearch
 */
