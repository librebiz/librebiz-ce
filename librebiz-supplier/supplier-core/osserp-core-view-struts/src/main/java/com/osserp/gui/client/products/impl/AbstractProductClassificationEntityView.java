/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 7, 2009 9:50:24 AM 
 * 
 */
package com.osserp.gui.client.products.impl;

import javax.servlet.http.HttpServletRequest;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;

import com.osserp.core.products.ProductClassificationEntity;
import com.osserp.core.products.ProductClassificationEntityManager;

import com.osserp.gui.client.impl.AbstractView;
import com.osserp.gui.client.products.ProductClassificationEntityView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractProductClassificationEntityView extends AbstractView implements ProductClassificationEntityView {

    protected AbstractProductClassificationEntityView() {
        super();
    }

    protected abstract Class<? extends ProductClassificationEntityManager> getManagerClass();

    public void create(Form form) throws ClientException, PermissionException {
        ProductClassificationEntityManager manager = getClassificationManager();
        ProductClassificationEntity newCreated = manager.create(
                getDomainEmployee(),
                form.getString(Form.NAME),
                form.getString(Form.DESCRIPTION));
        setBean(newCreated);
        refresh();
        setEditMode(true);
    }

    /**
     * Loads list from backend
     */
    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        refresh();
    }

    /**
     * Provides the manager for implementing entity
     * @return classificationManager
     */
    protected ProductClassificationEntityManager getClassificationManager() {
        return (ProductClassificationEntityManager) getService(getManagerClass().getName());
    }

    /**
     * Reloads list and current selected entity from persistent storage
     */
    protected void refresh() {
        ProductClassificationEntityManager manager = getClassificationManager();
        setList(manager.getConfigs());
        if (getBean() != null) {
            ProductClassificationEntity obj = (ProductClassificationEntity) getBean();
            for (int i = 0, j = getList().size(); i < j; i++) {
                ProductClassificationEntity next = (ProductClassificationEntity) getList().get(i);
                if (next.getId().equals(obj.getId())) {
                    setBean(next);
                    break;
                }
            }
        }
    }

    protected ProductClassificationEntity getCurrentEntity() {
        ProductClassificationEntity obj = (ProductClassificationEntity) getBean();
        if (obj == null) {
            throw new ActionException(ActionException.NO_OBJECT_BOUND);
        }
        return obj;
    }
}
