/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 28, 2007 11:02:06 AM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import com.osserp.common.Option;
import com.osserp.common.OptionsCache;
import com.osserp.core.FcsAction;
import com.osserp.core.dao.FcsWastebasketRowMapper;
import com.osserp.core.model.FlowControlItemImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractFcsWastebasketRowMapper implements FcsWastebasketRowMapper {
    private Map<Long, Option> actions = null;

    public AbstractFcsWastebasketRowMapper(OptionsCache cache) {
        this.actions = cache.getMap(getOptionsMapKey());
    }

    protected abstract String getOptionsMapKey();

    public Object mapRow(ResultSet rs, int index) throws SQLException {
        return new FlowControlItemImpl(
                Long.valueOf(rs.getLong(1)), //id
                Long.valueOf(rs.getLong(2)), //project_id
                Long.valueOf(rs.getLong(4)), // employee_id
                getAction(rs.getLong(3)),//
                rs.getDate(5));
    }

    private FcsAction getAction(long id) {
        return (FcsAction) actions.get(Long.valueOf(id));
    }

}
