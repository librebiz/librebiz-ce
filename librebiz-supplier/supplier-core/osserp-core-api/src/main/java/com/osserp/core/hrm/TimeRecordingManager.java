/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 22, 2007 3:50:13 PM 
 * 
 */
package com.osserp.core.hrm;

import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.core.employees.Employee;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface TimeRecordingManager {

    /**
     * Provides available time recording configs
     * @return configs
     */
    List<TimeRecordingConfig> findConfigs();

    /**
     * Provides time recording config by id
     * @return config or null if not exists
     */
    TimeRecordingConfig findConfig(Long id);

    /**
     * Provides available time record types
     * @return types
     */
    List<TimeRecordType> findTypes();

    /**
     * Provides a time record type
     * @param id
     * @return type or null if not exists
     */
    TimeRecordType findType(Long id);

    /**
     * Provides available time record marker types
     * @return marker types
     */
    List<TimeRecordMarkerType> findMarkerTypes();

    /**
     * Provides a time record marker type
     * @param id
     * @return marker type or null if not exists
     */
    TimeRecordMarkerType findMarkerType(Long id);

    /**
     * Indicates that a time recording exists
     * @param employee
     * @return true if time recording exists
     */
    boolean exists(Employee employee);

    /**
     * Creates a new time recording
     * @param user creating the new entry
     * @param timeRecording the employee the new created recording belongs to
     * @return timeRecording new created
     */
    TimeRecording create(Employee user, Employee timeRecording);

    /**
     * Creates a new time recording period
     * @param user creating the new entry
     * @param timeRecording
     */
    void createPeriod(Employee user, TimeRecording timeRecording);

    /**
     * Creates automated closings for open bookings from past if required
     * @param recording
     * @return reloaded time recording
     */
    TimeRecording createClosingsIfRequired(TimeRecording recording);

    /**
     * Provides current activated (valid) time recording
     * @param employee
     * @return timeRecording
     */
    TimeRecording find(Employee employee);

    /**
     * Changes config of current selected period
     * @param user
     * @param recording
     * @param id of the config
     * @throws ClientException if no period selected
     */
    void changeConfig(Employee user, TimeRecording recording, Long id) throws ClientException;

    /**
     * Adds a new system time record to current selected period
     * @param user
     * @param recording
     * @param type
     * @throws ClientException
     */
    void addRecord(Employee user, TimeRecording recording, TimeRecordType type) throws ClientException;

    /**
     * Adds a manual record to current selected period
     * @param user
     * @param recording
     * @param type
     * @param date
     * @param note
     * @throws ClientException
     */
    void addRecord(
            Employee user,
            TimeRecording recording,
            TimeRecordType type,
            Date date,
            String note) throws ClientException;

    /**
     * Adds an interval record to current selected period and initiates approval process if required
     * @param user
     * @param recording
     * @param type
     * @param startDate
     * @param stopDate
     * @param note
     * @throws ClientException
     */
    void addRecord(
            Employee user,
            TimeRecording recording,
            TimeRecordType type,
            Date startDate,
            Date stopDate,
            String note) throws ClientException;

    /**
     * Removes a record
     * @param user
     * @param recording
     * @param id
     * @throws PermissionException
     */
    void removeRecord(Employee user, TimeRecording recording, Long id) throws PermissionException;

    /**
     * Adds a marker to current selected period
     * @param user
     * @param recording
     * @param type
     * @param date
     * @param note
     * @throws ClientException
     */
    void addMarker(
            Employee user,
            TimeRecording recording,
            TimeRecordMarkerType type,
            Date date,
            Double value,
            String note) throws ClientException;

    /**
     * Removes a marker
     * @param user
     * @param recording
     * @param id
     * @throws PermissionException
     */
    void removeMarker(Employee user, TimeRecording recording, Long id) throws PermissionException;

    /**
     * Updates a record, creates a correction protocol entry and initiates approval process if required
     * @param user
     * @param recording
     * @param selected
     * @param startDate
     * @param stopDate
     * @param correctionNote
     * @throws ClientException if validation failed
     */
    void updateRecord(
            Employee user,
            TimeRecording recording,
            TimeRecord selected,
            Date startDate,
            Date stopDate,
            String correctionNote) throws ClientException;

    /**
     * Updates period
     * @param user
     * @param recording
     * @param terminalChipNumber
     * @param validFrom
     * @param validTil
     * @param leave
     * @param hours
     * @param extraLeave
     * @param firstYearLeave
     * @param state
     * @throws ClientException
     */
    void updatePeriod(
            Employee user,
            TimeRecording recording,
            Long terminalChipNumber,
            Date validFrom,
            Date validTil,
            Double leave,
            Double hours,
            Double extraLeave,
            Double firstYearLeave,
            Long state) throws ClientException;

    /**
     * Provides available corrections of a record
     * @param record
     * @return record corrections or empty list if none exist
     */
    List<TimeRecordCorrection> findCorrections(TimeRecord record);
}
