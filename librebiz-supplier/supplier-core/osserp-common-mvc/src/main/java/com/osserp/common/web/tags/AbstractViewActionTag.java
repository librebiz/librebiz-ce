/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 24, 2009 3:30:08 PM 
 * 
 */
package com.osserp.common.web.tags;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.BackendException;
import com.osserp.common.web.View;
import com.osserp.common.web.ViewManager;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractViewActionTag extends AbstractActionTag {
    private static Logger log = LoggerFactory.getLogger(AbstractViewActionTag.class.getName());

    private Object view = null;

    public Object getView() {
        return view;
    }

    public void setView(Object view) {
        this.view = view;
    }

    private boolean forceCreate = false;

    public boolean isForceCreate() {
        return forceCreate;
    }

    public void setForceCreate(boolean forceCreate) {
        this.forceCreate = forceCreate;
    }

    /**
     * Creates view as provided by {@link #getViewClass()}.
     * @return view
     * @throws Exception
     */
    protected final View createView() throws Exception {
        ViewManager manager = ViewManager.newInstance(pageContext.getServletContext());
        return manager.createView(getRequest(), getViewClass().getName());
    }

    /**
     * Tries to fetch the view as provided by {@link #getViewClass()}.
     * @return view or null if not exists
     */
    protected final View fetchView() {
        try {
            View existing = ViewManager.fetchView(getSession(), getViewClass());
            return existing;
        } catch (Throwable t) {
            return null;
        }
    }

    /**
     * Tries to find a view by all known strategies. If all lookups failed, method invokes {@link #createView()} if {@link #isForceCreate()} reports true.
     * @return view or null if view not found.
     */
    protected View lookupView() {
        View result = null;
        if (getView() != null) {
            if (getView() instanceof View) {
                result = (View) getView();
            } else if (getView() instanceof String) {
                Object obj = getSession().getAttribute(getView().toString());
                if (obj != null) {
                    if (obj instanceof View) {
                        result = (View) obj;
                    }
                }
            }
        }
        // tag implementation may override getViewClassName so attempts to fetch and create
        // must be performed even if view param is null: 
        if (result == null) {
            result = fetchView();
        }
        if (result == null && isForceCreate()) {
            try {
                result = createView();
            } catch (Exception e) {
                log.warn("lookupView() failed on last attempt to create new [message="
                        + e.getMessage() + "]");
            }
        }
        return result;
    }

    /**
     * Indicates that view provided by {@link #getViewClass()} exists in session scope.
     * @return viewExists
     */
    protected final boolean viewExists() {
        try {
            View existing = ViewManager.fetchView(getSession(), getViewClass());
            if (log.isDebugEnabled()) {
                log.debug("viewExists() done [result=" + (existing != null) + "]");
            }
            return (existing != null);
        } catch (Throwable t) {
            return false;
        }
    }

    /**
     * Provides the view class by invoking classForName on the name provided by {@link #getViewClassName()}.
     * @return viewClass
     */
    @SuppressWarnings("unchecked")
    protected final Class<? extends View> getViewClass() {
        String viewClassName = getViewClassName();
        try {
            Class clazz = Class.forName(viewClassName);
            return clazz;
        } catch (Throwable t) {
            log.error("getViewClass() failed [message=" + t.getMessage() + "]");
            throw new BackendException("class initialization error [name=" + viewClassName + "]");
        }
    }

    /**
     * Provides the full qualified view class name. Derived classes may override this to provide a dedicated instance. Default returns {@link #getView()}
     * property value.
     * @return viewClassName or null if none provided
     */
    protected String getViewClassName() {
        if (view != null) {
            if (view instanceof String) {
                return view.toString();
            } else if (view instanceof View) {
                return ((View) view).getClass().getName();
            }
        }
        return null;
    }

    @Override
    public void release() {
        super.release();
        view = null;
        forceCreate = false;
    }
}
