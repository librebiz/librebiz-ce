/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 14, 2016 
 * 
 */
package com.osserp.core.contacts.impl

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.api.WsBusinessCase
import com.osserp.api.WsContact

import com.osserp.common.ds.ImportException
import com.osserp.common.ods.OdsImporterService
import com.osserp.common.util.StringUtil

import com.osserp.core.BusinessCase
import com.osserp.core.api.BusinessCaseServiceProcessor
import com.osserp.core.api.defaults.CoreCustomerServiceProcessor
import com.osserp.core.api.defaults.CoreSupplierServiceProcessor
import com.osserp.core.contacts.ContactImporter
import com.osserp.core.customers.Customer
import com.osserp.core.suppliers.Supplier

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class ContactImporterImpl extends OdsImporterService implements ContactImporter {
    private static Logger logger = LoggerFactory.getLogger(ContactImporterImpl.class.getName())

    BusinessCaseServiceProcessor businessCaseServiceProcessor
    CoreCustomerServiceProcessor customerServiceProcessor
    CoreSupplierServiceProcessor supplierServiceProcessor

    BusinessCaseImportMapper businessCaseMapper = new BusinessCaseImportMapper()
    ContactImportMapper contactMapper = new ContactImportMapper()

    ContactImporterImpl() throws ImportException {
        super(true)
    }

    @Override
    protected Map<String, String> initRow(Map<String, String> row) {
        if (ContactImportMapper.isMappable(row)) {
            return row
        }
        return null
    }

    /**
     * Inserts or updates a contact
     * @param row mapped contact data
     * @return true if import row was successful
     */
    @Override
    protected boolean importRow(Map<String, String> row) {
        boolean result = false // businessCase insert or update performed

        WsContact obj = contactMapper.map(row)
        if (businessCaseServiceProcessor
            && BusinessCaseImportMapper.isMappable(row)) {

            WsBusinessCase wsb = businessCaseMapper.map(row)
            wsb.contact = obj
            try {
                BusinessCase businessCase

                if (wsb.targetId) {
                    businessCase = businessCaseServiceProcessor.override(wsb)
                    overrideCount++
                    result = true
                } else {
                    businessCase = businessCaseServiceProcessor.createOrUpdate(wsb, performActions)
                    if (businessCase?.primaryKey) {
                        insertCount++
                        result = true
                    } else {
                        logger.info("importRow: businessCase not created [service=${(businessCaseServiceProcessor != null)}, mapper=${(businessCaseMapper != null)}, data=${row}]")
                    }
                }
            } catch (Exception e) {
                logger.warn("importRow: caught exception [message=${e.message}, class=${e.getClass().getName()}]")
                if (errorBuffer == null) {
                    errorBuffer = new StringBuilder()
                } else {
                    errorBuffer.append('\n')
                }
                String message = "[message=${e.message}, contact=${StringUtil.toString(obj)}, businessCase=${StringUtil.toString(wsb)}]"
                errorBuffer.append(message)
                exceptionCount++
            }
        } else if (supplierServiceProcessor && obj && row.contactgroup == 'supplier') {
            try {
                Supplier supplier = supplierServiceProcessor.findOrCreate(obj)
                insertCount++
                result = true
            } catch (Exception e) {
                logger.warn("importRow: caught exception [message=${e.message}, class=${e.getClass().getName()}, data=${row}]")
            }
        } else if (customerServiceProcessor && obj) {
            try {
                Customer customer = customerServiceProcessor.createOrUpdate(obj)
                insertCount++
                result = true
            } catch (Exception e) {
                logger.warn("importRow: caught exception [message=${e.message}, class=${e.getClass().getName()}, data=${row}]")
            }
        }
        return result
    }
}
