<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <c:choose>
                    <c:when test="${!empty view.record.bookingType}"><o:out value="${view.record.bookingType.name}"/></c:when>
                    <c:otherwise><o:out value="${view.record.type.name}"/></c:otherwise>
                </c:choose> 
                <o:out value="${view.record.number}"/><span> / </span><fmt:formatDate value="${view.record.created}"/>
                - <fmt:message key="recordSelection" />:
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive table-responsive-default">
            <table class="table table-striped">
                <tbody>
                    <c:forEach var="type" items="${view.paymentTypes}">
                        <tr>
                            <td class="center"><o:out value="${type.numberPrefix}" /></td>
                            <td><v:link url="${view.baseLink}/selectPaymentType?id=${type.id}">
                                    <o:out value="${type.name}" />
                                </v:link></td>
                        </tr>
                    </c:forEach>
                    <c:if test="${!view.record.closed && !view.record.downpayment and view.record.dueAmount < 0.02}">
                        <tr>
                            <td class="center"></td>
                            <td><v:link url="${view.baseLink}/selectPaymentType?id=100">
                                    <fmt:message key="markAsClosed" />
                                </v:link></td>
                        </tr>
                    </c:if>
                    <c:if test="${!empty view.record.payments}">
                        <tr>
                            <td class="center"></td>
                            <td><v:link url="${view.baseLink}/selectPaymentType?id=-1">
                                    <fmt:message key="paymentDelete" />
                                </v:link></td>
                        </tr>
                    </c:if>
                </tbody>
            </table>
        </div>
    </div>
</div>
