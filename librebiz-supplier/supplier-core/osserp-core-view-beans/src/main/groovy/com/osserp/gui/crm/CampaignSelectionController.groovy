/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.gui.crm

import javax.servlet.http.HttpServletRequest

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.groovy.web.PopupViewController

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@RequestMapping("/crm/campaignSelection/*")
@Controller class CampaignSelectionController extends PopupViewController {
    private static Logger logger = LoggerFactory.getLogger(CampaignSelectionController.class.getName())

    @RequestMapping
    def search(HttpServletRequest request) {
        logger.debug("search: invoked [user=${getUserId(request)}]")
        CampaignSelectionView view = getView(request)
        view.search()
        return 'crm/_campaignSelectionResult'
    }

    @RequestMapping
    def select(HttpServletRequest request) {
        logger.debug("select: invoked [user=${getUserId(request)}]")
        CampaignSelectionView view = getView(request)
        view.select()
        exit(request)
    }

    @RequestMapping
    def enableBranchSelectionMode(HttpServletRequest request) {
        logger.debug("enableBranchSelectionMode: invoked [user=${getUserId(request)}]")
        CampaignSelectionView view = getView(request)
        view.enableBranchSelectionMode()
        return defaultPage
    }

    @RequestMapping
    def disableBranchSelectionMode(HttpServletRequest request) {
        logger.debug("disableBranchSelectionMode: invoked [user=${getUserId(request)}]")
        CampaignSelectionView view = getView(request)
        view.disableBranchSelectionMode()
        return defaultPage
    }

    @RequestMapping
    def selectBranch(HttpServletRequest request) {
        logger.debug("selectBranch: invoked [user=${getUserId(request)}]")
        CampaignSelectionView view = getView(request)
        view.selectBranch()
        return defaultPage
    }

    @RequestMapping
    def enableGroupSelectionMode(HttpServletRequest request) {
        logger.debug("enableGroupSelectionMode: invoked [user=${getUserId(request)}]")
        CampaignSelectionView view = getView(request)
        view.enableGroupSelectionMode()
        return defaultPage
    }

    @RequestMapping
    def disableGroupSelectionMode(HttpServletRequest request) {
        logger.debug("disableGroupSelectionMode: invoked [user=${getUserId(request)}]")
        CampaignSelectionView view = getView(request)
        view.disableGroupSelectionMode()
        return defaultPage
    }

    @RequestMapping
    def selectGroup(HttpServletRequest request) {
        logger.debug("selectGroup: invoked [user=${getUserId(request)}]")
        CampaignSelectionView view = getView(request)
        view.selectGroup()
        return defaultPage
    }

    @RequestMapping
    def enableTypeSelectionMode(HttpServletRequest request) {
        logger.debug("enableTypeSelectionMode: invoked [user=${getUserId(request)}]")
        CampaignSelectionView view = getView(request)
        view.enableTypeSelectionMode()
        return defaultPage
    }

    @RequestMapping
    def disableTypeSelectionMode(HttpServletRequest request) {
        logger.debug("disableTypeSelectionMode: invoked [user=${getUserId(request)}]")
        CampaignSelectionView view = getView(request)
        view.disableTypeSelectionMode()
        return defaultPage
    }

    @RequestMapping
    def selectType(HttpServletRequest request) {
        logger.debug("selectType: invoked [user=${getUserId(request)}]")
        CampaignSelectionView view = getView(request)
        view.selectType()
        return defaultPage
    }
}
