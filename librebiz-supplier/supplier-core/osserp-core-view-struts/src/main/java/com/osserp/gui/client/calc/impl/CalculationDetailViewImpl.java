/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.gui.client.calc.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;
import com.osserp.common.web.ViewName;

import com.osserp.core.BusinessCase;
import com.osserp.core.calc.Calculation;
import com.osserp.core.calc.CalculationManager;
import com.osserp.core.calc.CalculationSearchResult;
import com.osserp.core.sales.SalesOfferManager;

import com.osserp.gui.client.calc.CalculationDetailView;
import com.osserp.gui.client.calc.CalculationView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("calculationDetailView")
public class CalculationDetailViewImpl extends AbstractCalculationAwareView implements CalculationDetailView {

    private CalculationView calculationView = null;

    protected CalculationDetailViewImpl() {
        super();
        setExitAfterSave(true);
    }

    @Override
    public void init(HttpServletRequest request) throws ClientException,
            PermissionException {
        super.init(request);
        calculationView = (CalculationView) fetchView(request, CalculationView.class);
        if (calculationView != null && calculationView.getBean() != null) {
            setBean(calculationView.getCalculation());
        }
    }

    @Override
    public void save(Form form) throws ClientException, PermissionException {
        String name = form.getString(Form.NAME);
        if (isNotSet(name)) {
            throw new ClientException(ErrorCode.NAME_MISSING);
        }
        Calculation selected = getCalculation();
        // check existing names
        CalculationManager calculationManager = getCalculationManager();
        BusinessCase businessCase = getBusinessCase(); 
        if (businessCase != null) {
            List<CalculationSearchResult> existing = calculationManager.findByReference(
                    businessCase.getPrimaryKey(), businessCase.getContextName());
            checkExistingNames(existing, selected, name);
        } else if (!calculationView.getList().isEmpty()) {
            checkExistingNames(calculationView.getList(), selected, name);
        }
        Calculation result = getCalculationManager().changeName(getDomainUser(), selected, name);
        calculationView.refresh();
        SalesOfferManager offerManager = getOfferManager();
        offerManager.updateCalculationInfo(getDomainEmployee(), result);
        setBean(result);
    }

    private void checkExistingNames(List<CalculationSearchResult> existing, Calculation selected, String selectedName) throws ClientException {
        for (int i = 0, j = existing.size(); i < j; i++) {
            CalculationSearchResult next = existing.get(i);
            if (!next.getId().equals(selected.getId())) {
                if (next.getName().equals(selectedName)) {
                    throw new ClientException(ErrorCode.NAME_EXISTS);
                }
            }
        }
    }
}
