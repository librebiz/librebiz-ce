/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 18-Feb-2007 11:25:55 
 * 
 */
package com.osserp.core.system;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.osserp.common.Mappable;
import com.osserp.common.Option;

import com.osserp.core.contacts.Contact;
import com.osserp.core.employees.EmployeeGroup;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author cf <cf@osserp.com>
 * 
 */
public interface BranchOffice extends Option, Mappable {

    /**
     * Provides the contact
     * @return the contact
     */
    Contact getContact();

    /**
     * Provides the company branch belongs to
     * @return company
     */
    SystemCompany getCompany();

    /**
     * Sets the company branch belongs to
     * @param company
     */
    void setCompany(SystemCompany company);

    /**
     * The shortname of the branch office
     * @return shortname
     */
    String getShortname();

    /**
     * Sets the shortname of the branch office
     * @param shortname
     */
    void setShortname(String shortname);

    /**
     * The shortkey of the branch office
     * @return shortkey
     */
    String getShortkey();

    /**
     * Sets the shortkey of the branch office
     * @param shortkey
     */
    void setShortkey(String shortkey);

    /**
     * Indicates if branch is available but not activated to use for sales
     * or other operations.  
     * @return true if activated
     */
    boolean isActivated();
    
    /**
     * Enables / disables activated status
     * @param activated
     */
    void setActivated(boolean activated);

    /**
     * Indicates that this branch is the headquarter
     * @return headquarter
     */
    boolean isHeadquarter();

    /**
     * Sets whether this branch is the headquarter
     * @param headquarter
     */
    void setHeadquarter(boolean headquarter);

    /**
     * Indicates that this branch is the statuaryOffice
     * @return statuaryOffice
     */
    boolean isStatuaryOffice();

    /**
     * Enables statuary office status
     * @param statuaryOffice
     */
    void setStatuaryOffice(boolean statuaryOffice);

    /**
     * Provides the country of the branch if statuary office
     * @return statOfficeCountry
     */
    String getStatOfficeCountry();

    /**
     * Sets the country of the branch if statuary office
     * @param statOfficeCountry
     */
    void setStatOfficeCountry(String statOfficeCountry);

    /**
     * Provides the company number if branch is statuary office
     * @return statOfficeRegisterNumber
     */
    String getStatOfficeRegisterNumber();

    /**
     * Sets the company number if branch is statuary office
     * @param statOfficeRegisterNumber
     */
    void setStatOfficeRegisterNumber(String statOfficeRegisterNumber);

    /**
     * Provides the country registered in value
     * @return statOfficeRegisteredIn
     */
    String getStatOfficeRegisteredIn();

    /**
     * Sets the country registered in value
     * @param statOfficeRegisteredIn
     */
    void setStatOfficeRegisteredIn(String statOfficeRegisteredIn);

    /**
     * Indicates if branch is part of related system company or thirdparty
     * (e.g. branch is used to sell goods in the name of thirdparty).
     * The information is used for header and footer selection on document 
     * print for example.
     * @return true if related company is thirdparty
     */
    boolean isThirdparty();

    /**
     * Indicates if branch uses custom templates for print and text messages.
     * @return true if custom templates should be used for this branch office.
     */
    boolean isCustomTemplates();
    
    /**
     * Indicates if default templates assigned to print documents.
     * @return true if not thirdparty or thirdparty without customTemplates
     */
    boolean isDefaultTemplateSelection();

    /**
     * Indicates if record template processing uses internal naming scheme.
     * @return true if custom template naming is according internal.  
     */
    boolean isRecordTemplatesDefault();

    /**
     * Updates thirdparty settings
     * @param thirdparty enables or disables thirdparty setting
     * @param customTemplates
     * @param recordTemplatesDefault
     */
    void updateThirdparty(boolean thirdparty, boolean customTemplates, boolean recordTemplatesDefault);

    /**
     * Startup date of the branch
     * @return startup
     */
    Date getStartup();

    /**
     * Endup date of the branch
     * @return endup
     */
    Date getEndup();

    /**
     * Indicates that branch is child branch of another branch
     * @return parent branch when childbranch
     */
    Long getBranchOf();

    /**
     * Sets parent branch
     * @param branchOf
     */
    void setBranchOf(Long branchOf);

    /**
     * Provides the branchs email address
     * @return email
     */
    String getEmail();

    /**
     * Sets the email address
     * @param email
     */
    void setEmail(String email);

    /**
     * Provides the branchs phone country, e.g. '49' for germany
     * @return phoneCountry
     */
    String getPhoneCountry();

    /**
     * Sets the phone country
     * @param phoneCountry
     */
    void setPhoneCountry(String phoneCountry);

    /**
     * Provides the phone prefix of the branch
     * @return phonePrefix
     */
    String getPhonePrefix();

    /**
     * Sets the phone prefix
     * @param phonePrefix
     */
    void setPhonePrefix(String phonePrefix);

    /**
     * Provides the phone number of the branch without direct dial, e.g. '888 02-'
     * @return phoneNumber
     */
    String getPhoneNumber();

    /**
     * Sets the phone number
     * @param phoneNumber
     */
    void setPhoneNumber(String phoneNumber);

    /**
     * Provides formatted phone number, e.g. country + prefix + number.
     * @return formatted phone number or null
     */
    String getPhoneDisplay();
    
    /**
     * Indicates if phone number is available, e.g. phoneDisplay not empty 
     * @return
     */
    boolean isPhoneAvailable();
    
    /**
     * Provides the cost center
     * @return costCenter
     */
    Long getCostCenter();

    /**
     * Sets the cost center
     * @param costCenter
     */
    void setCostCenter(Long costCenter);

    /**
     * Indicates if branch provides own logo
     * @return true if logo provided
     */
    boolean isLogoAvailable();

    /**
     * Enables/disables logo availability
     * @param logoAvailable
     */
    void setLogoAvailable(boolean logoAvailable);

    /**
     * Provides the path to the logo
     * @return logoPath
     */
    String getLogoPath();

    /**
     * Sets the path to the logo
     * @param logoPath
     */
    void setLogoPath(String logoPath);

    /**
     * Provides an optional vat id overriding company's vatId or
     * replacing existing depending on referenced businessCase. 
     * @return returns the vat id.
     */
    String getVatId();

    /**
     * Sets an optional vat id.
     * @param vatId The vat id to set.
     */
    void setVatId(String vatId);

    /**
     * Provides an optional string overriding vat label if available
     * @return vatLabel
     */
    String getVatLabel();

    /**
     * Sets an optional vat label
     * @param vatLabel
     */
    void setVatLabel(String vatLabel);

    /**
     * The primary key of the related pos account. The pos account is
     * represented by a customer object declared as pos account by type.  
     * @return pos account id or null if no pos account defined for this branch
     */
    Long getPosAccountId();

    /**
     * Sets the related pos account, e.g. customer of type pos account.
     * @param posAccountId
     */
    void setPosAccountId(Long posAccountId);

    /**
     * Provides the employee groups assoziated with branch.
     * @return groups
     */
    List<EmployeeGroup> getGroups();

    /**
     * Adds a new employee group
     * @param group
     */
    void addGroup(EmployeeGroup group);

    /**
     * Removes group if exists
     * @param id
     * @return removed if true
     */
    boolean removeGroup(Long id);

    /**
     * Provides the company map by 'systemCompany' key of the resulting map. The branch office map itself is mapped by 'branchOffice' key in company map, e.g.
     * use /systemCompany/branchOffice/name path to achieve branch name for ex.
     * @return company map
     */
    Map<String, Object> getCompanyMap();
}
