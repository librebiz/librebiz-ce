/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 1, 2009 10:39:52 AM 
 * 
 */
package com.osserp.common.service.impl;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.velocity.app.VelocityEngine;

import com.osserp.common.mail.MailSender;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractTemplateMailSender extends AbstractTemplateManager {
    private static Logger log = LoggerFactory.getLogger(AbstractTemplateMailSender.class.getName());
    private MailSender mailSender = null;

    /**
     * Creates a new template based mail sender
     * @param sender engine for emails
     * @param engine for templates
     */
    protected AbstractTemplateMailSender(MailSender sender, VelocityEngine engine) {
        super(engine);
        mailSender = sender;
    }

    /**
     * Creates and sends a message by template. An IllegalArgumentException will be thrown if recipients missing. Method never throws an exception for all other
     * missing values or further exceptions that may occur during mail processing and sending. These exceptions will just be logged.
     * @param originator system default will be used if missing
     * @param subject an default info subject will be used if missing
     * @param recipients
     * @param textValues
     */
    protected final void send(String originator, String subject, String recipients[], String templateName, Map textValues) {
        if (recipients == null || recipients.length < 1) {
            log.error("send() invoked with missing recipients!");
            throw new IllegalArgumentException("recipients must not be null");
        }
        try {
            String message = createText(templateName, textValues);

            if (isNotSet(originator) && isSet(subject)) {
                mailSender.send(recipients, subject, message);
            } else if (isNotSet(subject)) {
                mailSender.sendInfo(recipients, message);
            } else {
                mailSender.send(originator, recipients, subject, message);
            }

        } catch (Throwable t) {
            log.error("send() ignoring failed send attempt [message=" + t.getMessage() + "]", t);
        }
    }

    protected MailSender getMailSender() {
        return mailSender;
    }

    protected void setMailSender(MailSender mailSender) {
        this.mailSender = mailSender;
    }
}
