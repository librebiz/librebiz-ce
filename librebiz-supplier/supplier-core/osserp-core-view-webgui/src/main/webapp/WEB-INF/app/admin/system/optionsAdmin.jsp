<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="optionsAdminView" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="styles" type="string">
        <style type="text/css">
            .action { text-align: center; width: 24px; }
        </style>
    </tiles:put>
    <tiles:put name="headline">
        <c:choose>
            <c:when test="${empty view.selectedName}">
                <fmt:message key="selectionListsEdit" />
            </c:when>
            <c:otherwise>
                <fmt:message key="selectionListEdit" /> - <fmt:message key="${view.selectedName}" />
            </c:otherwise>
        </c:choose>
    </tiles:put>

    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="optionsAdminContent">
            <div class="row">

                <c:choose>
                    <c:when test="${empty view.selectedName}">
                        <div class="col-md-6 panel-area panel-area-default">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4>
                                        <fmt:message key="selectionLists" />
                                    </h4>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive table-responsive-default">
                                    <table class="table table-striped">
                                        <tbody>
                                            <c:forEach var="listName" items="${view.availableLists}">
                                                <tr>
                                                    <td><v:link url="/admin/system/optionsAdmin/selectList?name=${listName}">
                                                            <fmt:message key="${listName}" />
                                                        </v:link></td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <v:form url="/admin/system/optionsAdmin/save">
                            <c:choose>
                                <c:when test="${view.createMode}">
                                    <div class="col-md-6 panel-area panel-area-default">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4>
                                                    <fmt:message key="newEntryLabel" />
                                                </h4>
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <v:text name="name" styleClass="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <o:submit styleClass="form-control" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </c:when>
                                <c:when test="${view.editMode}">
                                    <div class="col-md-6 panel-area panel-area-default">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4>
                                                    <fmt:message key="changeEntry" />
                                                </h4>
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <v:text name="name" styleClass="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <o:submit styleClass="form-control" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </c:when>
                                <c:otherwise>
                                    <div class="col-md-6 panel-area panel-area-default">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4>
                                                    <fmt:message key="currentSelection" />
                                                </h4>
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <div class="table-responsive table-responsive-default">
                                                <table class="table table-striped">
                                                    <tbody>
                                                        <c:forEach var="option" items="${view.list}">
                                                            <tr>
                                                                <td class="optionName" <c:if test="${option.endOfLife}"> style="text-decoration:line-through;"</c:if>><o:out value="${option.name}" /></td>
                                                                <o:permission role="executive,option_edit" info="permissionOptionChange">
                                                                    <td class="action"><c:choose>
                                                                            <c:when test="${option.endOfLife}">
                                                                                <v:link title="disableEol" url="/admin/system/optionsAdmin/toggleEol?id=${option.id}">
                                                                                    <o:img name="enableIcon" styleClass="bigicon" />
                                                                                </v:link>
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                <v:link title="enableEol" url="/admin/system/optionsAdmin/toggleEol?id=${option.id}">
                                                                                    <o:img name="enabledIcon" styleClass="bigicon" />
                                                                                </v:link>
                                                                            </c:otherwise>
                                                                        </c:choose></td>
                                                                    <td class="action"><v:link title="changeName" url="/admin/system/optionsAdmin/select?id=${option.id}">
                                                                            <o:img name="writeIcon" styleClass="bigicon" />
                                                                        </v:link></td>
                                                                </o:permission>
                                                            </tr>
                                                        </c:forEach>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </c:otherwise>
                            </c:choose>
                        </v:form>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
