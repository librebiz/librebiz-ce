/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 23, 2004 
 * 
 */
package com.osserp.common.service.impl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.service.UserScope;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class UserScopeImpl implements UserScope {
    private static Logger log = LoggerFactory.getLogger(UserScopeImpl.class.getName());

    private Map<Long, Map<String, Serializable>> dataStore;

    protected UserScopeImpl() {
        super();
        dataStore = new HashMap<Long, Map<String, Serializable>>();
    }

    public void addObject(Long userId, String name, Serializable object) {
        if (userId != null && name != null && object != null) {
            initDataStore(userId);
            if (dataStore.containsKey(userId)) {
                dataStore.get(userId).put(name, object);
                if (log.isDebugEnabled()) {
                    log.debug("addObject() done [userId=" + userId
                            + ", name=" + name
                            + ", object=" + object.getClass().getName()
                            + "]");
                }
            }
        }
    }

    public Serializable getObject(Long userId, String name) {
        if (userId != null && name != null) {
            initDataStore(userId);
            if (dataStore.containsKey(userId)) {
                if (log.isDebugEnabled()) {
                    log.debug("getObject() object exists [name=" + name + "]");
                }
                return dataStore.get(userId).get(name);
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("getObject() object does not exist [name=" + name + "]");
        }
        return null;
    }

    public void removeObject(Long userId, String name) {
        if (userId != null && name != null) {
            initDataStore(userId);
            if (dataStore.containsKey(userId)) {
                dataStore.get(userId).remove(name);
                if (log.isDebugEnabled()) {
                    log.debug("removeObject() done [name=" + name + "]");
                }
            }
        }
    }

    private void initDataStore(Long userId) {
        if (!dataStore.containsKey(userId)) {
            Map<String, Serializable> userDataStore = new HashMap<String, Serializable>();
            dataStore.put(userId, userDataStore);
        }
    }
}
