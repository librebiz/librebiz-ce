/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 10-Feb-2007 17:27:37 
 * 
 */
package com.osserp.core.service.events;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.Parameter;

import com.osserp.core.BusinessCase;
import com.osserp.core.dao.EventQueries;
import com.osserp.core.dao.Events;
import com.osserp.core.dao.SystemConfigs;
import com.osserp.core.events.Event;
import com.osserp.core.events.EventAction;
import com.osserp.core.events.EventAlertDisplay;
import com.osserp.core.events.EventManager;
import com.osserp.core.service.impl.AbstractService;
import com.osserp.core.tasks.EventChangedSender;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class EventManagerImpl extends AbstractService implements EventManager {
    private static Logger log = LoggerFactory.getLogger(EventManagerImpl.class.getName());
    private EventChangedSender eventChangedSender = null;
    private Events events = null;
    private EventQueries eventQueries = null;
    private SystemConfigs systemConfigs = null;

    public EventManagerImpl(
            Events events, 
            EventQueries eventQueries, 
            SystemConfigs systemConfigs, 
            EventChangedSender eventChangedSender) {
        super();
        this.events = events;
        this.eventQueries = eventQueries;
        this.systemConfigs = systemConfigs;
        this.eventChangedSender = eventChangedSender;
    }

    public void createEvent(
            EventAction action,
            Long createdBy,
            Long reference,
            Long recipient,
            String description,
            String message,
            List<Parameter> parameters,
            Date activationDate,
            Date appointmentDate) throws ClientException {
        if (action == null) {
            throw new ClientException(ErrorCode.APPOINTMENT_TYPE_MISSING);
        }
        if (recipient == null) {
            throw new ClientException(ErrorCode.RECIPIENT_MISSING);
        }
        if (activationDate == null && appointmentDate == null) {
            throw new ClientException(ErrorCode.DATE_MISSING);
        }
        Date now = new Date(System.currentTimeMillis());
        if (activationDate != null && activationDate.before(now)) {
            throw new ClientException(ErrorCode.REMINDER_DATE_PAST);
        }
        if (appointmentDate != null && appointmentDate.before(now)) {
            throw new ClientException(ErrorCode.APPOINTMENT_DATE_PAST);
        }
        events.createEvent(
                action,
                createdBy,
                reference,
                recipient,
                description,
                message,
                parameters,
                (activationDate == null ? appointmentDate : activationDate),
                (appointmentDate == null ? activationDate : appointmentDate));
        if (eventChangedSender != null) {
            eventChangedSender.send();
        }
    }

    public Event findEvent(Long id) {
        try {
            return events.findEvent(id);
        } catch (Exception e) {
            return null;
        }
    }

    public List<Event> findEventsByRecipient(
            Long recipient, 
            boolean includeDeactivated, 
            String orderBy, 
            boolean descending, 
            String category) {
        return eventQueries.findByRecipient(
                recipient, includeDeactivated, orderBy, descending, category);
    }

    public List<Event> findEventsByReference(Long reference) {
        return events.findByReference(reference);
    }

    public List<Event> findEvents(BusinessCase businessCase) {
        return events.findByReference(businessCase.getPrimaryKey());
    }

    public List<Event> findAppointments(BusinessCase bc) {
        return events.findAppointments(bc);
    }

    public List<Event> findAppointmentsByRecipient(Long recipient) {
        return events.findAppointmentsByRecipient(recipient);
    }

    public void addNote(
            Event event,
            Long createdBy,
            String note,
            Date activationDate,
            Date appointmentDate,
            boolean distribute,
            List<Long> informRecipients) {
        events.addNote(
                event,
                createdBy,
                note,
                activationDate,
                appointmentDate,
                distribute,
                informRecipients);
        if (eventChangedSender != null) {
            eventChangedSender.send();
        }
    }

    public void checkAndPerformAlerts() {
        int closed = events.moveClosedEvents();
        if (closed > 0 && log.isDebugEnabled()) {
            log.debug("checkAndSendAlerts() moved " + closed + " closed events...");
        }
        Long[] eventIds = events.findEscalatedEvents();
        if (eventIds != null && eventIds.length > 0) {
            if (log.isDebugEnabled()) {
                log.debug("checkAndSendAlerts() trying to send async message for "
                        + eventIds.length + " alerts");
            }
            for (int i = 0, j = eventIds.length; i < j; i++) {
                Long id = eventIds[i];
                Event event = events.findEvent(id);
                events.createAlerts(event);
                if (log.isDebugEnabled()) {
                    log.debug("checkAndSendAlerts() created alert events for id "
                            + event.getId()
                            + " reference "
                            + event.getReferenceId());
                }
            }
            if (eventChangedSender != null) {
                eventChangedSender.send();
            }
        }
    }

    public void close(Long user, Event eventToClose) {
        if (systemConfigs.isSystemPropertyEnabled("eventFcsCloseOther")
                || !eventToClose.getAction().isCloseableByTarget()) {
            events.closeEvents(
                    user,
                    eventToClose.getReferenceId(),
                    eventToClose.getAction().getId());
        } else {
            events.closeEvent(user, eventToClose.getId());
        }
        if (eventChangedSender != null) {
            eventChangedSender.send();
        }
    }

    public List<EventAlertDisplay> getEventAlertDisplay() {
        return events.getEventAlertDisplay();
    }
}
