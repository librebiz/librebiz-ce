/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 28.09.2004 
 * 
 */
package com.osserp.common;

import java.io.Serializable;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Permission extends Serializable {

    /**
     * Gets the permission.
     * @return permission
     */
    String getPermission();

    /**
     * Gets the description for the permission. The description contains a human 
     * readable text in contrast to the name wich is a key only.
     * @return description
     */
    String getDescription();

    /**
     * Indicates administrative permission. An administrative permission should 
     * never be displayed (console use only)
     * @return true if administrative
     */
	boolean isAdministrative();

    /**
     * Indicates displayable permission. Displayables should be displayed on 
     * selection without extended permission.
     * @return displayable
     */
    boolean isDisplayable();

    /**
     * Indicates disabled permission. Disabled permission should never be included 
     * in any query for permission. Disabled permissions are mostly used to be able
     * to add a required permission for an optional but unused or not deployed 
     * software component. 
     * @return true if disabled
     */
    boolean isDisabled();
}
