<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${requestScope.requestListingView}" />
<c:set var="requestList" value="${requestScope.requestListingEssential}" />
<c:set var="requestListName" value="${requestScope.requestListName}" />
<c:if test="${!empty requestScope.requestSelectExitTarget}">
    <c:set var="exitTarget" value="${requestScope.requestSelectExitTarget}" />
</c:if>

<div class="col-md-6 panel-area panel-area-default">

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4><fmt:message key="${requestListName}" /></h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive table-responsive-default">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th class="requestType"><fmt:message key="type" /></th>
                        <th class="requestId"><fmt:message key="number" /></th>
                        <th class="salesInitials" title="<fmt:message key="sales"/>"><fmt:message key="salesShortcut"/></th>
                        <th class="customer"><fmt:message key="interestOrCustomer" /></th>
                        <th class="createdAt"><fmt:message key="from" /></th>
                    </tr>
                </thead>
                <tbody>
                    <c:choose>
                        <c:when test="${empty requestList}">
                            <tr>
                                <td colspan="5"><fmt:message key="noRequestFound" /></td>
                            </tr>
                        </c:when>
                        <c:otherwise>
                            <c:forEach var="obj" items="${requestList}" varStatus="s">
                                <c:set var="nextSelectionUrl">
                                    <c:url value="/loadRequest.do">
                                        <c:if test="${!empty exitTarget}">
                                            <c:param name="exit" value="${exitTarget}" />
                                        </c:if>
                                        <c:param name="id" value="${obj.id}" />
                                        <c:param name="exitId" value="request${obj.id}" />
                                    </c:url>
                                </c:set>
                                <tr id="request${obj.id}"<c:if test="${obj.stopped}"> class="red"</c:if>>
                                    <td class="requestType"><o:out value="${obj.type.key}" /></td>
                                    <td class="requestId"><a href="<o:out value="${nextSelectionUrl}"/>"> <o:out value="${obj.id}" /></a></td>
                                    <td class="salesInitials" title="<fmt:message key="sales"/>">
                                        <v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${obj.salesId}">
                                            <oc:employee initials="true" value="${obj.salesId}" />
                                        </v:ajaxLink>
                                    </td>
                                    <td class="customer">
                                        <a href="<o:out value="${nextSelectionUrl}"/>" title="<o:out value="${obj.id} - ${obj.shortName}"/> / <o:out value="${obj.city}"/>"> <o:out value="${obj.name}" /></a>
                                    </td>
                                    <td class="createdAt"><o:date value="${obj.created}" casenull="-" addcentury="false" /></td>
                                </tr>
                            </c:forEach>
                        </c:otherwise>
                    </c:choose>
                </tbody>
            </table>
        </div>
    </div>
</div>
