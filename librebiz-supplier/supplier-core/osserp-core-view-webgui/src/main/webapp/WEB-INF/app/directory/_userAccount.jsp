<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="directoryUserData" />
                -
                <o:out value="${view.bean.values['uid']}" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">
            <c:forEach var="name" items="${view.bean.attributeNames}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for=""><o:out value="${name}" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${view.bean.values[name]}" />
                        </div>
                    </div>
                </div>
            </c:forEach>
            <c:if test="${view.bean.sambaSchemaAvailable}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">SMB-Schema</label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <span>enabled</span>
                        </div>
                    </div>
                </div>
            </c:if>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for=""><fmt:message key="actions" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${view.bean.resign}">
                                <a href="<v:url value="${view.baseLink}/userReAssign"/>"><fmt:message key="accountReAssign" /></a>
                            </c:when>
                            <c:otherwise>
                                <a href="<v:url value="${view.baseLink}/userResign"/>"><fmt:message key="accountResign" /></a>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>
            <oc:systemPropertyEnabled name="fetchMailSupportEnabled">
                <c:if test="${empty view.fetchmailAccount}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for=""> </label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <a href="${view.createLink.link}"><fmt:message key="createMailbox" /></a>
                            </div>
                        </div>
                    </div>
                </c:if>
            </oc:systemPropertyEnabled>
        </div>
    </div>
</div>
