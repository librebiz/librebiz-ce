/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 25, 2008 10:42:29 PM 
 * 
 */
package com.osserp.core.dao;

import java.util.List;

import com.osserp.core.dms.LetterContent;
import com.osserp.core.dms.LetterParagraph;
import com.osserp.core.dms.LetterType;
import com.osserp.core.employees.Employee;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface LetterContents {

    /**
     * Finds type by context
     * @param contextName
     * @return type or null if not exists
     */
    LetterType findType(String contextName);

    /**
     * Loads type from persistent storage
     * @param id
     * @return type
     */
    LetterType loadType(Long id);

    /**
     * Loads all available types
     * @return types
     */
    List<LetterType> loadTypes();

    /**
     * Updates a letterType on persistent storage
     * @param type
     */
    void save(LetterType type);

    /**
     * Finds a letter by id
     * @param id
     * @return letterContent
     */
    LetterContent find(Long id);

    /**
     * Deletes letter on persistent storage
     * @param letter
     */
    void delete(LetterContent letter);

    /**
     * Updates a letter on persistent storage
     * @param letter
     */
    void save(LetterContent letter);

    /**
     * Creates a new empty paragraph
     * @param letter
     * @param user adding the paragraph
     * @return paragraph
     */
    LetterParagraph createParagraph(LetterContent letter, Employee user);

}
