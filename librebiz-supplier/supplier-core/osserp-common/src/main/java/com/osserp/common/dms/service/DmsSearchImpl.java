/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 12, 2016 
 * 
 */
package com.osserp.common.dms.service;

import java.util.ArrayList;
import java.util.List;

import com.osserp.common.dms.DmsDocument;
import com.osserp.common.dms.DmsManager;
import com.osserp.common.dms.DmsSearch;
import com.osserp.common.dms.DmsUtil;
import com.osserp.common.dms.DocumentData;
import com.osserp.common.dms.DocumentType;
import com.osserp.common.dms.dao.DocumentTypes;
import com.osserp.common.service.Locator;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class DmsSearchImpl implements DmsSearch {

    private DocumentTypes documentTypes;
    private Locator locator;
    
    /**
     * Creates a new dms search service
     */
    public DmsSearchImpl() {
        super();
    }

    /**
     * Creates a new dms search service
     * @param documentTypes
     * @param locator
     */
    public DmsSearchImpl(DocumentTypes documentTypes, Locator locator) {
        super();
        this.documentTypes = documentTypes;
        this.locator = locator;
    }

    public DocumentType getDocumentType(Long id) {
        return documentTypes.get(id);
    }

    public List<DocumentType> findSearchable() {
        List<DocumentType> result = new ArrayList<>();
        List<DocumentType> all = documentTypes.getList();
        for (int i = 0, j = all.size(); i < j; i++) {
            DocumentType next = all.get(i);
            if (next.isSearchable()) {
                result.add(next);
            }
        }
        return result;
    }

    public List<DmsDocument> findByType(DocumentType documentType) {
        List<DmsDocument> result = new ArrayList<>();
        DmsManager dmsManager = getDmsManager(documentType);
        if (dmsManager != null) {
            result = dmsManager.findByType(documentType);
        }
        return result;
    }

    public DmsDocument getDocument(DocumentType type, Long id) {
        DmsManager dmsManager = getDmsManager(type);
        return dmsManager.findDocument(id);
    }

    public DocumentData getDocumentData(DmsDocument document) {
        DmsManager dmsManager = getDmsManager(document.getType());
        return dmsManager.getDocumentData(document);
    }

    protected DmsManager getDmsManager(DocumentType documentType) {
        return DmsUtil.getDmsManager(locator, documentType);
    }

    protected DocumentTypes getDocumentTypes() {
        return documentTypes;
    }

    public void setDocumentTypes(DocumentTypes documentTypes) {
        this.documentTypes = documentTypes;
    }

    protected Locator getLocator() {
        return locator;
    }

    public void setLocator(Locator locator) {
        this.locator = locator;
    }

}
