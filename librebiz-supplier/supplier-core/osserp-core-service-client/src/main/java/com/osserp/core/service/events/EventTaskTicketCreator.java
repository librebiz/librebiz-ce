/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 11, 2007 11:12:21 PM 
 * 
 */
package com.osserp.core.service.events;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.core.dao.Employees;
import com.osserp.core.dao.EventConfigs;
import com.osserp.core.dao.Events;
import com.osserp.core.events.EventAction;
import com.osserp.core.events.EventTask;
import com.osserp.core.tasks.EventTicketSender;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class EventTaskTicketCreator extends AbstractEventTicketCreator {
    private static Logger log = LoggerFactory.getLogger(EventTaskTicketCreator.class.getName());

    /**
     * @param events
     * @param eventConfigs
     * @param employees
     * @param eventTicketSender
     */
    protected EventTaskTicketCreator(
            Events events,
            EventConfigs eventConfigs,
            Employees employees,
            EventTicketSender eventTicketSender) {
        super(events, eventConfigs, employees, eventTicketSender);
    }

    /**
     * Creates an event ticket with fixed recipient
     * @param eventTaskName
     * @param reference
     * @param description
     * @param branchId
     * @param recipientId
     * @param createdBy
     * @param message
     */
    public void createTicket(
            String eventTaskName,
            Long reference,
            String description,
            Long branchId,
            Long recipientId,
            Long createdBy,
            String message) {

        if (log.isDebugEnabled()) {
            log.debug("createTicket() invoked [eventTaskName=" + eventTaskName + ", reference=" + reference + "]");
        }
        List<EventAction> actions = getConfigs(eventTaskName);
        for (int i = 0, j = actions.size(); i < j; i++) {
            EventAction action = actions.get(i);
            super.createTicket(
                    action,
                    reference,
                    description,
                    branchId,
                    recipientId,
                    createdBy,
                    message);
        }
    }

    /**
     * Creates event tickets by task name
     * @param eventTaskName
     * @param reference
     * @param description
     * @param branchId
     * @param managerId
     * @param salesId
     * @param createdBy
     * @param message
     * @param headline
     */
    public void createTickets(
            String eventTaskName,
            Long reference,
            String description,
            Long branchId,
            Long managerId,
            Long salesId,
            Long createdBy,
            String message,
            String headline) {

        if (log.isDebugEnabled()) {
            log.debug("createTickets() invoked [eventTaskName=" + eventTaskName + ", reference=" + reference + "]");
        }
        List<EventAction> actions = getConfigs(eventTaskName);
        for (int i = 0, j = actions.size(); i < j; i++) {
            EventAction action = actions.get(i);
            super.createTicket(
                    action,
                    reference,
                    description,
                    branchId,
                    managerId,
                    salesId,
                    createdBy,
                    message,
                    headline,
                    false);
        }
    }

    private List<EventAction> getConfigs(String eventTaskName) {
        EventTask task = getEventConfigs().getTask(eventTaskName);
        if (task == null || task.isEndOfLife()) {
            
            if (log.isDebugEnabled()) {
                if (task == null) {
                    log.debug("getConfigs() no task found [name=" + eventTaskName + "]");
                } else {
                    log.debug("getConfigs() deactivated task found [name=" + eventTaskName + "]");
                }
            }
            return new java.util.ArrayList<EventAction>();
        }
        if (log.isDebugEnabled()) {
            log.debug("getConfigs() task found [name=" + eventTaskName + ", actionCount=" + task.getActions().size() + "]");
        }
        return task.getActions();
    }

}
