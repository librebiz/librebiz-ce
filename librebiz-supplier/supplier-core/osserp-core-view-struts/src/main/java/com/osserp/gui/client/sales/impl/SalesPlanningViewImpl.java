/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 20-Sep-2006 06:57:26 
 * 
 */
package com.osserp.gui.client.sales.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.util.DateUtil;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.ViewName;

import com.osserp.core.Options;
import com.osserp.core.employees.Employee;
import com.osserp.core.planning.ProductPlanning;
import com.osserp.core.planning.ProductPlanningManager;
import com.osserp.core.planning.ProductPlanningSettings;
import com.osserp.core.planning.SalesPlanningSummary;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductCategoryConfig;
import com.osserp.core.products.ProductClassificationConfigManager;
import com.osserp.core.products.ProductGroupConfig;
import com.osserp.core.products.ProductManager;
import com.osserp.core.products.ProductTypeConfig;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.views.OrderItemsDisplay;

import com.osserp.gui.client.products.impl.AbstractStockSelectingView;
import com.osserp.gui.client.sales.SalesPlanningSummaryComparator;
import com.osserp.gui.client.sales.SalesPlanningView;
import com.osserp.gui.client.sales.OrderItemsDisplayComparator;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author cf <cf@osserp.com>
 */
@ViewName("openSalesOrdersView")
public class SalesPlanningViewImpl extends AbstractStockSelectingView implements SalesPlanningView {
    private static Logger log = LoggerFactory.getLogger(SalesPlanningViewImpl.class.getName());
    private ProductPlanningSettings settings = null;
    private boolean listUnreleased = false;
    private Product product = null;
    private List<ProductTypeConfig> productTypes = new ArrayList<ProductTypeConfig>();
    private List<ProductGroupConfig> productGroups = new ArrayList<ProductGroupConfig>();
    private List<ProductCategoryConfig> productCategories = new ArrayList<ProductCategoryConfig>();
    private List<SalesPlanningSummary> summary = new ArrayList<SalesPlanningSummary>();
    private boolean summaryMode = true;
    private boolean productSummaryMode = false;
    private Long productSummaryDisplayId = null;
    private String productSummaryDisplayName = null;
    private boolean reloadRequired = false;
    private boolean displaySummaryAndExit = false;
    private SalesPlanningSummaryComparator summaryComparator = new SalesPlanningSummaryComparator("byProductId", false);
    private OrderItemsDisplayComparator listComparator = new OrderItemsDisplayComparator("byProductId", false);

    protected SalesPlanningViewImpl() {
        super();
        if (log.isDebugEnabled()) {
            log.debug("<init> initialized product groups [count=" + productGroups.size() + "]");
        }
    }

    protected SalesPlanningViewImpl(String defaultExitTarget) {
        super();
        setExitTarget(defaultExitTarget);
        setCreateBackupWhenExisting(true);
    }

    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        super.init(request);
        productTypes = getProductClassificationConfigManager().getTypes();
        productGroups = getProductClassificationConfigManager().getGroups();
        productCategories = getProductClassificationConfigManager().getCategories();
        // initialize user settings
        ProductPlanningManager plannings = getPlanningManager();
        settings = plannings.getSettings(getDomainEmployee().getId());

        this.listUnreleased = RequestUtil.getBoolean(request, "unreleased");
        if (RequestUtil.isAvailable(request, "vacant")) {
            settings.setDisplayVacant(RequestUtil.getBoolean(request, "vacant"));
        }
        if (RequestUtil.isAvailable(request, "confirmedPurchaseOnly")) {
            settings.setConfirmedPurchaseOnly(RequestUtil.getBoolean(request, "confirmedPurchaseOnly"));
        }
        selectProductType(getSystemPropertyId("productTypeOpenOrders"));
        saveSettings();
        Long productId = RequestUtil.fetchId(request);
        if (productId != null) {
            ProductManager manager = getProductManager();
            product = manager.get(productId);
            summaryMode = false;
        }
        Long stockId = RequestUtil.fetchLong(request, "stockId");
        if (stockId != null) {
            if (log.isDebugEnabled()) {
                log.debug("init() invoked [stock=" + stockId + "]");
            }
            selectStock(stockId);
        } else if (settings.getStock() != null && settings.getStock() != 0) {
            selectStock(settings.getStock());
        } else {
            refresh();
        }
    }

    private ProductClassificationConfigManager getProductClassificationConfigManager() {
        return (ProductClassificationConfigManager) getService(ProductClassificationConfigManager.class.getName());
    }

    @Override
    public void selectStock(Long id) {
        super.selectStock(id);
        if (id != null && id != 0) {
            settings.setStock(id);
            saveSettings();
        }
        refresh();
        if (log.isDebugEnabled()) {
            log.debug("selectStock() done [id=" + id + "]");
        }
    }

    public boolean isListUnreleased() {
        return listUnreleased;
    }

    public boolean isProductMode() {
        return (product != null);
    }

    public Product getProduct() {
        return product;
    }

    public Date getHorizon() {
        return (settings == null ? null : settings.getPlanningHorizon());
    }

    public boolean isSummaryMode() {
        return summaryMode;
    }

    public void changeSummaryMode() {
        summaryMode = !summaryMode;
        if (!summaryMode) {
            productSummaryMode = false;
            productSummaryDisplayId = null;
        }
    }

    public List<SalesPlanningSummary> getSummary() {
        return summary;
    }

    public boolean isProductSummaryMode() {
        return productSummaryMode;
    }

    public Long getProductSummaryDisplayId() {
        return productSummaryDisplayId;
    }

    public String getProductSummaryDisplayName() {
        return productSummaryDisplayName;
    }

    public void setProductSummaryMode(Long productId) {
        if (productId == null) {
            productSummaryMode = false;
            productSummaryDisplayId = null;
            productSummaryDisplayName = null;
        } else {
            productSummaryMode = true;
            productSummaryDisplayId = productId;
            for (int i = 0, j = summary.size(); i < j; i++) {
                SalesPlanningSummary next = summary.get(i);
                if (next.getProductId().equals(productId)) {
                    productSummaryDisplayName = next.getProductName();
                    break;
                }
            }
        }
    }

    @Override
    public void setSetupMode(boolean setupMode) {
        super.setSetupMode(setupMode);
        if (!isSetupMode() && reloadRequired) {
            // refresh with new settings when leaving config 
            try {
                refresh();
                reloadRequired = false;
            } catch (Exception ignorable) {
                // should never happen if user had permission 
                // to enable config before
            }
        }
    }

    public List<ProductTypeConfig> getProductTypes() {
        return productTypes;
    }

    public List<ProductGroupConfig> getProductGroups() {
        return productGroups;
    }

    public List<ProductCategoryConfig> getProductCategories() {
        return productCategories;
    }

    public Long getSelectedProductType() {
        return settings.getProductType();
    }

    public Long getSelectedProductGroup() {
        return settings.getProductGroup();
    }

    public Long getSelectedProductCategory() {
        return settings.getProductCategory();
    }

    public void selectProductType(Long id) {
        if (id != null) {
            ProductTypeConfig type = getProductClassificationConfigManager().getType(id);
            productGroups = type.getGroups();
        } else {
            productGroups = getProductClassificationConfigManager().getGroups();
        }
        settings.setProductType(id);
        settings.setProductGroup(null);
        settings.setProductCategory(null);
        saveSettings();
        refresh();
    }

    public void selectProductGroup(Long id) {
        if (id != null) {
            ProductGroupConfig group = getProductClassificationConfigManager().getGroup(id);
            productCategories = group.getCategories();
        } else {
            productCategories = getProductClassificationConfigManager().getCategories();
        }
        settings.setProductGroup(id);
        settings.setProductCategory(null);
        saveSettings();
        refresh();
    }

    public void selectProductCategory(Long id) {
        settings.setProductCategory(id);
        saveSettings();
        refresh();
    }

    public OrderItemsDisplay fetchItem(Long id) {
        if (id != null) {
            for (int i = 0, j = summary.size(); i < j; i++) {
                SalesPlanningSummary sum = summary.get(i);
                for (int k = 0, l = sum.getItems().size(); k < l; k++) {
                    OrderItemsDisplay next = sum.getItems().get(k);
                    if (next.getItemId().equals(id)) {
                        return next;
                    }
                }
            }
        }
        return null;
    }

    public boolean isOpenPurchaseOnly() {
        return settings.isOpenPurchaseOnly();
    }

    public void togglePurchaseOrderedOnly() {
        settings.setOpenPurchaseOnly(!settings.isOpenPurchaseOnly());
        saveSettings();
    }

    public boolean isConfirmedPurchaseOnly() {
        return settings.isConfirmedPurchaseOnly();
    }

    public void toggleConfirmedPurchaseOrderOnly() {
        settings.setConfirmedPurchaseOnly(!settings.isConfirmedPurchaseOnly());
        saveSettings();
        refresh();
    }

    public boolean isVacancyDisplayMode() {
        return settings.isDisplayVacant();
    }

    public void toggleVacancyDisplay() {
        settings.setDisplayVacant(!settings.isDisplayVacant());
        saveSettings();
        refresh();
    }

    public void setPlanningHorizon(Date horizon) {
        if (horizon != null) {
            horizon = new Date(horizon.getTime() + DateUtil.DAY - 1000);
        }
        settings.setPlanningHorizon(horizon);
        saveSettings();
        refresh();
    }

    public boolean isDisplaySummaryAndExit() {
        return displaySummaryAndExit;
    }

    public void enableSummaryAndExit(Long productId, String exitTarget) {
        product = null;
        productSummaryDisplayId = productId;
        productSummaryMode = true;
        setExitTarget(exitTarget);
        summaryMode = true;
        displaySummaryAndExit = true;
    }

    protected void setDisplaySummaryAndExit(boolean displaySummaryAndExit) {
        this.displaySummaryAndExit = displaySummaryAndExit;
    }

    public void resetSettings() {
        settings.reset();
        ProductPlanningManager planningManager = getPlanningManager();
        planningManager.save(settings);
    }

    public void refresh() {
        Long stock = settings.getStock();
        if (stock != null) {
            if (log.isDebugEnabled()) {
                log.debug("refresh() invoked [stock=" + stock
                        + ", horizon="
                        + (settings == null || settings.getPlanningHorizon() == null ? "disabled" : settings.getPlanningHorizon())
                        + "]");
            }
            ProductPlanningManager planningManager = getPlanningManager();
            ProductPlanning planning = planningManager.getPlanning(
                    stock,
                    settings.getPlanningHorizon(),
                    settings.isConfirmedPurchaseOnly());
            if (planning == null) {
                throw new java.lang.IllegalStateException("unsupported stock [id=" + stock + "]");
            }
            List<OrderItemsDisplay> all = planning.createList(
                    (product == null ? null : product.getProductId()),
                    settings.getProductCategory(),
                    settings.getProductGroup(),
                    settings.getProductType(),
                    listUnreleased,
                    settings.isDisplayVacant());
            List<OrderItemsDisplay> list = new ArrayList<>();
            Employee domainUser = getDomainEmployee();
            for (int i = 0, j = all.size(); i < j; i++) {
                OrderItemsDisplay item = all.get(i);
                if (item.getBranchId() != null && item.getBranchId() != 0) {
                    BranchOffice bo = (BranchOffice) getOption(Options.BRANCH_OFFICES, item.getBranchId());
                    if (domainUser.isSupportingBranch(bo)) {
                        list.add(item);
                    }
                }
            }
            summary = planning.createSummary(list);
            setList(list);
        }
    }

    protected void setListUnreleased(boolean listUnreleased) {
        this.listUnreleased = listUnreleased;
    }

    private ProductPlanningManager getPlanningManager() {
        return (ProductPlanningManager) getService(ProductPlanningManager.class.getName());
    }

    private ProductManager getProductManager() {
        return (ProductManager) getService(ProductManager.class.getName());
    }

    private void saveSettings() {
        ProductPlanningManager planningManager = getPlanningManager();
        planningManager.save(settings);
    }

    @Override
    public void sortList(String method) {
        if (log.isDebugEnabled()) {
            log.debug("sortList() invoked [method=" + method + "]");
        }
        if (productSummaryMode) {
            for (Iterator<SalesPlanningSummary> iterator = getSummary().iterator(); iterator.hasNext();) {
                SalesPlanningSummary next = iterator.next();
                listComparator.sort(method, next.getItems());
            }
            return;
        }
        if (summaryMode) {
            summaryComparator.sort(method, getSummary());
            return;
        }
        listComparator.sort(method, getList());
    }
}
