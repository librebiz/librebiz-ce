/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 23-Oct-2005 15:59:34 
 * 
 */
package com.osserp.core.events;

import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.Parameter;
import com.osserp.core.BusinessCase;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface EventManager {

    /**
     * Creates an appointment event.
     * @param action
     * @param createdBy
     * @param reference
     * @param recipient
     * @param description
     * @param message
     * @param parameters
     * @param activationDate
     * @param appointmentDate
     */
    void createEvent(
            EventAction action,
            Long createdBy,
            Long reference,
            Long recipient,
            String description,
            String message,
            List<Parameter> parameters,
            Date activationDate,
            Date appointmentDate) throws ClientException;

    /**
     * Provides an event by id
     * @param id
     * @return event or null if no longer exists
     */
    Event findEvent(Long id);

    /**
     * Provides all open events of a given recipient
     * @param recipient
     * @param includeDeactivated
     * @param orderBy
     * @param descending
     * @param category
     * @return open events
     */
    List<Event> findEventsByRecipient(Long recipient, boolean includeDeactivated, String orderBy, boolean descending, String category);

    /**
     * Provides all open events of a given reference
     * @param reference
     * @return open events
     */
    List<Event> findEventsByReference(Long reference);

    /**
     * Provides all open events of a given reference
     * @param businessCase
     * @return open events
     */
    List<Event> findEvents(BusinessCase businessCase);

    /**
     * Provides all appointments of a business case
     * @param businessCase
     * @return open appointments
     */
    List<Event> findAppointments(BusinessCase businessCase);

    /**
     * Provides all appointments of a recipient
     * @param recipient
     * @return open appointments
     */
    List<Event> findAppointmentsByRecipient(Long recipient);

    /**
     * Adds a new note to given event
     * @param event where we add the note
     * @param createdBy
     * @param note to add
     * @param activationDate
     * @param appointmentDate
     * @param distribute indicates that all other recipients of this event should also get and see the note
     * @param informRecipients creates info events for all provided employees
     */
    void addNote(
            Event event,
            Long createdBy,
            String note,
            Date activationDate,
            Date appointmentDate,
            boolean distribute,
            List<Long> informRecipients);

    /**
     * Fires alert events on all escalated events
     */
    void checkAndPerformAlerts();

    /**
     * Closes an event. If event is not terminateable by target all related events of same reference and type will also closed
     * @param user who closes the events
     * @param eventToClose
     */
    void close(Long user, Event eventToClose);

    /**
     * Provides a list of all available event escalations
     * @return event alert display list
     */
    List<EventAlertDisplay> getEventAlertDisplay();
}
