/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 20, 2011 5:25:49 PM 
 * 
 */
package com.osserp.core.service.crm;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.common.OptionsCache;
import com.osserp.common.util.CollectionUtil;

import com.osserp.core.Comparators;
import com.osserp.core.Options;
import com.osserp.core.crm.Campaign;
import com.osserp.core.crm.CampaignGroup;
import com.osserp.core.crm.CampaignManager;
import com.osserp.core.crm.CampaignType;
import com.osserp.core.dao.Campaigns;
import com.osserp.core.dao.SystemConfigs;
import com.osserp.core.employees.Employee;
import com.osserp.core.model.crm.CampaignGroupImpl;
import com.osserp.core.model.crm.CampaignImpl;
import com.osserp.core.model.crm.CampaignTypeImpl;
import com.osserp.core.service.impl.AbstractService;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CampaignManagerImpl extends AbstractService implements CampaignManager {
    private static Logger log = LoggerFactory.getLogger(CampaignManagerImpl.class.getName());

    private static final String EMPTY_BRANCH_PUBLIC_PROPERTY = 
            "salesCampaignEmptyBranchPublic";
    private static final String EMPTY_BRANCH_IGNORE_BY_GLOBAL_PROPERTY = 
            "salesCampaignEmptyBranchIgnoreByGlobal";
    
    private Campaigns campaigns;
    private OptionsCache optionsCache;
    private SystemConfigs systemConfigs;

    protected CampaignManagerImpl(
            Campaigns campaigns, 
            OptionsCache optionsCache, 
            SystemConfigs systemConfigs) {
        super();
        this.campaigns = campaigns;
        this.optionsCache = optionsCache;
        this.systemConfigs = systemConfigs;
    }

    public List<CampaignGroup> findGroups() {
        return campaigns.findGroups();
    }

    public void createGroup(Employee user, String name) {
        boolean exists = false;
        List<CampaignGroup> groups = campaigns.getGroups();
        for (int i = 0, j = groups.size(); i < j; i++) {
            CampaignGroup group = groups.get(i);
            if (group.getName().equalsIgnoreCase(name)) {
                exists = true;
                group.setEndOfLife(false);
                campaigns.save(group);
                break;
            }
        }
        if (!exists) {
            CampaignGroupImpl obj = new CampaignGroupImpl(name, 
                    (user == null ? Constants.SYSTEM_EMPLOYEE : user.getId()));
            campaigns.save(obj);
        }
        optionsCache.refresh(Options.CAMPAIGN_GROUPS);
    }

    public List<CampaignType> findTypes() {
        return campaigns.findTypes();
    }

    public void createType(Employee user, String name) {
        boolean exists = false;
        List<CampaignType> types = campaigns.getTypes();
        for (int i = 0, j = types.size(); i < j; i++) {
            CampaignType type = types.get(i);
            if (type.getName().equalsIgnoreCase(name)) {
                exists = true;
                type.setEndOfLife(false);
                campaigns.save(type);
                break;
            }
        }
        if (!exists) {
            CampaignTypeImpl obj = new CampaignTypeImpl(name, (user == null ? Constants.SYSTEM_EMPLOYEE : user.getId()));
            campaigns.save(obj);
        }
        optionsCache.refresh(Options.CAMPAIGN_TYPES);
    }

    public List<Campaign> find() {
        return campaigns.getCampaigns();
    }

    public List<Campaign> find(Long parent) {
        return campaigns.getCampaigns(parent);
    }

    public List<Campaign> findSelectable() {
        return campaigns.findSelectableCampaigns();
    }

    public List<Campaign> findSelected(DomainUser user) {
        List<Campaign> result = new ArrayList<>();
        List<Campaign> all = campaigns.findSelectedCampaigns();
        boolean emptyBranchPublic = isCampaignEmptyBranchPublic();
        boolean emptyBranchIgnoreByGlobal = isCampaignEmptyBranchIgnoreByGlobal();
        if (log.isDebugEnabled()) {
            log.debug("findSelected() invoked [campaigns=" + all.size()
                    + ", user=" + (user == null ? "null" : user.getId())
                    + ", emptyBranchPublic=" + emptyBranchPublic + "]");
        }
        if (user != null) {
            for (int i = 0, j = all.size(); i < j; i++) {
                Campaign next = all.get(i);
                BranchOffice branch = (next.getBranch() == null) ? null : 
                    fetchBranch(next.getBranch());
                if ((branch == null && emptyBranchPublic)
                        || (branch == null 
                                && user.isGlobalAccessGrant()
                                && emptyBranchIgnoreByGlobal)
                        || (branch != null && user.isBranchAccessible(branch))) {
                    result.add(next);
                }
            }
        }
        return CollectionUtil.sort(result, Comparators.createOptionNameComparator(false));
    }

    public List<Option> getAvailableParents(Campaign campaign) {
        return campaigns.getAvailableParents(campaign);
    }

    public Campaign getCampaign(Long id) {
        return campaigns.getCampaign(id);
    }

    public Campaign create(
            Employee user,
            String name,
            String reach,
            String description,
            Campaign parent,
            Long company,
            Long branch,
            CampaignGroup campaignGroup,
            CampaignType campaignType,
            Date startDate,
            Date endDate) {
        if (parent != null) {
            return create(user, reach, campaignGroup, campaignType, parent, branch, name, description, startDate, endDate);
        }
        boolean exists = false;
        Campaign campaign = null;
        List<Campaign> campainList = campaigns.getCampaigns();
        for (int i = 0, j = campainList.size(); i < j; i++) {
            campaign = campainList.get(i);
            if (campaign.getName().equalsIgnoreCase(name)) {
                exists = true;
                campaign.setEndOfLife(false);
                campaigns.save(campaign);
                break;
            }
        }
        if (!exists) {
            campaign = new CampaignImpl(
                    (user == null ? Constants.SYSTEM_EMPLOYEE : user.getId()),
                    name,
                    reach,
                    description,
                    campaignGroup,
                    campaignType,
                    null, // Campaign parent
                    company,
                    branch,
                    startDate,
                    endDate);
            campaigns.save(campaign);
        }
        optionsCache.refresh(Options.CAMPAIGNS);
        return campaign;
    }

    private Campaign create(
            Employee user,
            String reach,
            CampaignGroup campaignGroup,
            CampaignType campaignType,
            Campaign parent,
            Long branch,
            String name,
            String description,
            Date startDate,
            Date endDate) {
        boolean exists = false;
        Campaign campaign = null;
        List<Campaign> campainList = campaigns.getCampaigns(parent.getId());
        for (int i = 0, j = campainList.size(); i < j; i++) {
            campaign = campainList.get(i);
            if (campaign.getName().equalsIgnoreCase(name)) {
                exists = true;
                campaign.setEndOfLife(false);
                campaigns.save(campaign);
                parent.setParent(true);
                campaigns.save(parent);
                break;
            }
        }
        if (!exists) {
            campaign = new CampaignImpl(
                    (user == null ? Constants.SYSTEM_EMPLOYEE : user.getId()),
                    name,
                    reach,
                    description,
                    campaignGroup,
                    campaignType,
                    parent,
                    parent.getCompany(),
                    branch,
                    startDate,
                    endDate);
            parent.setParent(true);
            campaigns.save(parent);
            campaigns.save(campaign);
        }
        optionsCache.refresh(Options.CAMPAIGNS);
        return campaign;
    }

    public Campaign updateParent(Campaign campaign, Long id) {
        Campaign parent = campaigns.getCampaign(id);
        if (campaign.getReference() != null) {
            Campaign currentParent = campaigns.getCampaign(campaign.getReference());
            List<Campaign> children = campaigns.getCampaigns(currentParent.getId());
            boolean hasOtherChildren = false;
            for (int i = 0, j = children.size(); i < j; i++) {
                Campaign child = children.get(i);
                if (!child.getId().equals(campaign.getId())) {
                    hasOtherChildren = true;
                }
            }
            if (!hasOtherChildren) {
                currentParent.setParent(false);
                campaigns.save(currentParent);
            }
        }
        campaign.updateParent(parent);
        campaigns.save(campaign);
        parent.setParent(true);
        campaigns.save(parent);
        return campaigns.getCampaign(campaign.getId());
    }

    public void update(Campaign campaign) {
        campaigns.save(campaign);
        optionsCache.refresh(Options.CAMPAIGNS);
    }

    public boolean isReferenced(Campaign campaign) {
        return campaigns.isReferenced(campaign.getId());
    }

    public void delete(Campaign campaign) throws ClientException {
        if (campaigns.isReferenced(campaign.getId())) {
            throw new ClientException(ErrorCode.CAMPAIGN_ALREADY_REFERENCED);
        }
        campaigns.delete(campaign);
        optionsCache.refresh(Options.CAMPAIGNS);
    }
    
    protected BranchOffice fetchBranch(Long id) {
        return (BranchOffice) optionsCache.getMapped(Options.BRANCH_OFFICES , id);
    }
    
    protected boolean isCampaignEmptyBranchPublic() {
        return systemConfigs.isSystemPropertyEnabled(EMPTY_BRANCH_PUBLIC_PROPERTY);
    }
    
    protected boolean isCampaignEmptyBranchIgnoreByGlobal() {
        return systemConfigs.isSystemPropertyEnabled(EMPTY_BRANCH_IGNORE_BY_GLOBAL_PROPERTY);
    }
}
