/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 10-Feb-2007 09:36:14 
 * 
 */
package com.osserp.core.service.projects;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.Calendar;
import com.osserp.common.OptionsCache;
import com.osserp.common.util.CollectionUtil;
import com.osserp.common.util.DateFormatter;
import com.osserp.common.util.DateUtil;

import com.osserp.core.BusinessCaseRelationStat;
import com.osserp.core.Comparators;
import com.osserp.core.Options;
import com.osserp.core.dao.ProjectQueries;
import com.osserp.core.projects.ProjectSearch;
import com.osserp.core.projects.results.ProjectByActionDate;
import com.osserp.core.sales.SalesListItem;
import com.osserp.core.sales.SalesListItemSorter;
import com.osserp.core.sales.SalesListProductItem;
import com.osserp.core.sales.SalesMonitoringItem;
import com.osserp.core.sales.SalesUtil;
import com.osserp.core.service.impl.AbstractService;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProjectSearchImpl extends AbstractService implements ProjectSearch {
    private static Logger log = LoggerFactory.getLogger(ProjectSearchImpl.class.getName());
    private ProjectQueries queries = null;
    private OptionsCache optionsCache;

    public ProjectSearchImpl(OptionsCache optionsCache, ProjectQueries queries) {
        super();
        this.optionsCache = optionsCache;
        this.queries = queries;
    }

    public List<SalesListItem> findByManager(Long managerId, boolean openOnly) {
        return sortByCreated(queries.getByManager(managerId, openOnly), true);
    }

    public List<SalesListItem> findBySalesperson(Long salesId, boolean openOnly) {
        return sortByCreated(queries.getBySalesperson(salesId, openOnly), true);
    }

    private List<SalesListItem> sortByCreated(List<SalesListItem> list, boolean descendant) {
        if (descendant) {
            SalesListItemSorter.instance().sort("byCreatedReverse", list);
        } else {
            SalesListItemSorter.instance().sort("byCreated", list);
        }
        return new ArrayList(CollectionUtil.removeDups(list));
    }

    public List<BusinessCaseRelationStat> getSubcontractorStats() {
        return queries.getSubcontractors();
    }

    public String getSubcontractorSpreadsheet(Long id, boolean ignoreClosed) {
        List<SalesMonitoringItem> items = subcontractorMonitoring(id, ignoreClosed);
        return SalesUtil.createSpreadSheet(
                new ArrayList(items), 
                optionsCache.getMap(Options.EMPLOYEES), 
                optionsCache.getMap(Options.SUPPLIERS));
    }
    
    private List<SalesMonitoringItem> subcontractorMonitoring(Long contractorId, boolean ignoreClosed) {
        List<SalesMonitoringItem> result = new ArrayList<>();
        if (ignoreClosed) {

            List<SalesMonitoringItem> all = getSubcontractorMonitoring(contractorId);
            for (int i = 0, j = all.size(); i < j; i++) {
                SalesMonitoringItem next = all.get(i);
                if (!next.isClosed()) {
                    result.add(next);
                }
            }
            
        } else {
            result = getSubcontractorMonitoring(contractorId);
        }
        return result;
    }

    public List<SalesMonitoringItem> getSubcontractorMonitoring(Long contractorId) {
        return queries.getSubcontractorMonitoring(contractorId);
    }
        
    public List<SalesListItem> findByExistingInvoice(Date from, Date til) {
        if (log.isDebugEnabled()) {
            log.debug("findByExistingInvoice() invoked [from="
                    + DateFormatter.getDate(from)
                    + ", til="
                    + DateFormatter.getDate(til)
                    + "]");
        }
        return queries.getByExistingInvoice(from, til);
    }

    public List<SalesListItem> findByMissingInvoices() {
        List<SalesListItem> result = queries.getByMissingInvoices();
        if (log.isDebugEnabled()) {
            log.debug("findByMissingInvoices() done [count=" + result.size() + "]");
        }
        return result;
    }

    public List<SalesListItem> findCancelled(Integer year) {
        List<SalesListItem> list = queries.getByCancelledStatus();
        clearByYear(list, year);
        return list;
    }

    public List<SalesListItem> findStopped(Integer year) {
        List<SalesListItem> list = queries.getByStoppedStatus();
        clearByYear(list, year);
        return list;
    }

    public List<SalesListProductItem> findDeliveries() {
        return queries.getProjectsByDelivery();
    }

    public List<SalesListProductItem> findDeliveriesByDay(Calendar day, boolean items) {
        Set<Long> added = new HashSet<Long>();
        List<SalesListProductItem> result = new ArrayList<SalesListProductItem>();
        List<SalesListProductItem> list = queries.getProjectsByDelivery();
        for (int i = 0, j = list.size(); i < j; i++) {
            SalesListProductItem next = list.get(i);
            if (next.getAppointmentDate() != null
                    && DateUtil.isSameDay(next.getAppointmentDate(), day.getDate())) {
                if (items || !added.contains(next.getId())) {
                    result.add(next);
                    added.add(next.getId());
                }
            }
        }
        return CollectionUtil.sort(result, Comparators.createEntityComparator(false));
    }

    public List<SalesListItem> findMountings() {
        return queries.getProjectsByMounting();
    }

    public List<SalesListItem> findSleepingProjects(Integer maxDaysAgo) {
        if (maxDaysAgo == null || maxDaysAgo.intValue() < 1) {
            return queries.getSleepingProjects(7);
        }
        return queries.getSleepingProjects(maxDaysAgo.intValue());
    }

    public List<ProjectByActionDate> findByActionAndDate(
            Long projectType,
            Long actionId,
            Date from,
            Date til,
            boolean withCanceled,
            boolean withClosed,
            String zipcode) {

        if (log.isDebugEnabled()) {
            log.debug("findByActionAndDate() invoked, params:\nrequest type: "
                    + projectType
                    + "\nfcs action: " + actionId
                    + "\nfrom: " + DateFormatter.getDate(from)
                    + "\ntil: " + DateFormatter.getDate(til)
                    + "\nwith canceled: " + withCanceled
                    + "\nwith closed: " + withClosed);
        }
        // ensure that zipcode is not an empty string as a result of 
        // form handling provided by some web frameworks
        if (zipcode == null || zipcode.length() < 1) {
            zipcode = "";
        }
        return queries.getByActionAndDate(
                projectType,
                actionId,
                from,
                til,
                withCanceled,
                withClosed,
                zipcode);
    }

    public List<ProjectByActionDate> findByMissingAction(
            Long projectType,
            Long missingAction,
            Long withAction,
            boolean withCanceled,
            boolean withClosed) {

        List<ProjectByActionDate> list = queries.getByMissingAction(
                projectType,
                missingAction,
                withAction,
                withCanceled,
                withClosed);
        if (log.isDebugEnabled()) {
            log.debug("findByMissingAction() done, found "
                    + ((list == null) ? 0 : list.size()));
        }
        return list;
    }

    public List<SalesListProductItem> getDeliveryMonitoring(Long stockId, boolean descendant) {
        return queries.getDeliveryMonitoring(stockId, descendant);
    }

    private void clearByYear(List<SalesListItem> list, Integer year) {
        if (log.isDebugEnabled()) {
            log.debug("clearByYear() invoked for " + list.size()
                    + " items, year to filter " + year);
        }
        if (year != null && year.intValue() > 2001) {
            for (Iterator<SalesListItem> i = list.iterator(); i.hasNext();) {
                SalesListItem next = i.next();
                int nextYear = DateUtil.getYear(next.getCreated());
                if (nextYear != year) {
                    i.remove();
                }
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("clearByYear() done, "
                    + list.size()
                    + " items after running filter");
        }
    }
}
