/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 05-Feb-2007 15:42:01 
 * 
 */
package com.osserp.core.service.doc;

import java.math.BigDecimal;

import org.jdom2.Document;
import org.jdom2.Element;

import com.osserp.common.PermissionException;

import com.osserp.core.Address;
import com.osserp.core.BusinessCase;
import com.osserp.core.dms.LetterContent;
import com.osserp.core.employees.Employee;
import com.osserp.core.products.ProductDetails;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface XmlService {

    /**
     * Creates a new xml document with element name 'root' as root element,<br/>
     * employee values as 'currentUser' and elements 'currentDate' and 'currentTime' 
     * as children.
     * @param employee
     * @return document
     */
    Document createDocument(Employee employee);

    /**
     * Provides a document with all employees under /root/employees
     * @param user
     * @return document with active employees
     * @throws PermissionException if permission not grant to provided user
     */
    Document getEmployees(Employee user) throws PermissionException;

    /**
     * Provides country name by id for documents if supported by settings
     * @param countryId
     * @return name or null if not exists or print of country disabled
     */
    String getCountryName(Long countryId);

    /**
     * Creates an address element using 'address' as element name
     * @param address
     * @return address element
     */
    Element createAddress(Address address);

    /**
     * Creates an address element 
     * @param elementName
     * @param address
     * @return address element
     */
    Element createAddress(String elementName, Address address);

    /**
     * Creates an product details element
     * @param elementName
     * @param details
     * @return details element
     */
    Element createProductDetails(String elementName, ProductDetails details);
    
    /**
     * Creates a branch office element
     * @param elementName
     * @param id the primary key of the branch office
     * @return branch element
     */
    Element createBranch(String elementName, Long id);

    /**
     * Creates an element by business case
     * @param elementName
     * @param businessCase
     * @return businessCase element
     */
    Element createBusinessCase(String elementName, BusinessCase businessCase);

    /**
     * Creates an employee element by employee id
     * @param elementName
     * @param id primary key of the employee
     * @return employee element
     */
    Element createEmployee(String elementName, Long id);

    /**
     * Creates an employee element
     * @param elementName
     * @param employee
     * @return employee element
     */
    Element createEmployee(String elementName, Employee employee);

    /**
     * Creates an element with content of provided letter
     * @param user
     * @param content
     * @param xmlName
     * @return element
     */
    Element createLetter(Employee user, LetterContent content, String xmlName);

    /**
     * Formats a boolean value as string
     * @param value
     * @return boolean as string
     */
    String createString(boolean value);

    /**
     * Formats a bigDecimal value as string
     * @param value
     * @return boolean as string
     */
    String createString(BigDecimal value);

    /**
     * Formats a double value as string
     * @param value
     * @return double as string
     */
    String createString(Double value);
}
