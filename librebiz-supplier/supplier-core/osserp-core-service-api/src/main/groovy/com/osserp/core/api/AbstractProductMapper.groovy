/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.api

import com.osserp.common.Option
import com.osserp.common.OptionsCache

import com.osserp.core.Options
import com.osserp.core.dao.Products
import com.osserp.core.dao.QuantityUnits

import com.osserp.core.dao.SystemConfigs
import com.osserp.core.model.products.ProductClassificationConfigVO
import com.osserp.core.products.Manufacturer
import com.osserp.core.products.ProductClassificationConfig

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class AbstractProductMapper extends AbstractCoreMapper {

    protected Products products
    protected QuantityUnits quantityUnits

    protected AbstractProductMapper(
            OptionsCache optionsCache,
            SystemConfigs systemConfigs,
            Products products,
            QuantityUnits quantityUnits) {
        super(optionsCache, systemConfigs)
        this.products = products
        this.quantityUnits = quantityUnits
    }

    Long fetchColor(String colorName) {
        if (colorName) {
            Option color = fetchOrCreateOption(Options.PRODUCT_COLORS, colorName)
            return !color ? null : color.id
        }
        return null
    }

    Long fetchManufacturer(String name, Long group) {
        if (name) {
            Manufacturer existing
            List<Manufacturer> list = products.manufacturers
            list.each { Manufacturer manufacturer ->
                if (manufacturer.name == name && manufacturer.groupId == group) {
                    existing = manufacturer
                }
            }
            if (existing) {
                return existing.id
            }
            Manufacturer m = products.addManufacturer(group, name)
            return m.id
        }
        return null
    }

    Long fetchQuantityUnit(String unitName) {
        Long id = null
        if (unitName) {
            Option unit = fetchOption(Options.QUANTITY_UNITS, unitName)
                ?: createQuantityUnit(unitName)
            if (unit) {
                id = unit.id
            }
        }
        return id ?: defaultQuantityUnit
    }

    Long fetchPowerUnit(String unitName) {
        Long id = null
        if (unitName) {
            Option unit = fetchOption(Options.QUANTITY_UNITS, unitName)
                ?: createQuantityUnit(unitName)
            if (unit) {
                id = unit.id
            }
        }
        return id
    }

    protected ProductClassificationConfig fetchClassification(Long typeId, Long groupId, Long categoryId) {
        return new ProductClassificationConfigVO(
            fetchOption('productTypes', typeId),
            fetchOption('productGroups', groupId),
            fetchOption('productCategories', categoryId))
    }

    protected Long getDefaultQuantityUnit() {
        Long id = getSystemPropertyId('productImportQuantityUnitDefault')
        id ?: 1L
    }

    protected Option createQuantityUnit(String unitName) {
        return quantityUnits.create(unitName, 2)
    }
}
