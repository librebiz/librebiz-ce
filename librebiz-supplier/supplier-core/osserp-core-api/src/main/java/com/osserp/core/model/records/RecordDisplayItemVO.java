/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 18, 2008 3:06:58 PM 
 * 
 */
package com.osserp.core.model.records;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.osserp.common.Constants;
import com.osserp.common.beans.AbstractOption;
import com.osserp.core.finance.RecordDisplayItem;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RecordDisplayItemVO extends AbstractOption implements RecordDisplayItem {
    private Long productId = null;
    private String customName = null;
    private Set<Long> typeIds = new HashSet<Long>();
    private Long typeCount = Constants.LONG_NULL;
    private Long groupId = null;
    private Long categoryId = null;
    private boolean affectsStock = false;
    private boolean includePrice = false;
    private boolean virtual = false;
    private boolean plant = false;
    private boolean bundle = false;
    private boolean service = false;
    private Double quantity = Constants.DOUBLE_NULL;
    private Double delivered = Constants.DOUBLE_NULL;
    private BigDecimal price = new BigDecimal(Constants.DOUBLE_NULL);
    private Long stockId = null;
    private Long externalId = null;
    private Date delivery = null;
    private boolean deliveryConfirmed = false;

    protected RecordDisplayItemVO() {
        super();
    }

    protected RecordDisplayItemVO(RecordDisplayItem vo) {
        super(vo);
        productId = vo.getProductId();
        customName = vo.getCustomName();
        groupId = vo.getGroupId();
        affectsStock = vo.isAffectsStock();
        includePrice = vo.isIncludePrice();
        virtual = vo.isVirtual();
        plant = vo.isPlant();
        bundle = vo.isBundle();
        service = vo.isService();
        quantity = vo.getQuantity();
        delivered = vo.getDelivered();
        price = vo.getPrice();
        stockId = vo.getStockId();
        delivery = vo.getDelivery();
        typeIds = vo.getTypeIds();
        deliveryConfirmed = vo.isDeliveryConfirmed();
        externalId = vo.getExternalId();
    }

    protected RecordDisplayItemVO(Map<String, Object> map) {
        super((Long) map.get("itemId"), (String) map.get("productName"));
        productId = (Long) map.get("productId");
        customName = (String) map.get("customName");
        typeCount = (Long) map.get("productTypeCount");
        Long type = (Long) map.get("productType");
        if (type != null) {
            typeIds.add(type);
        }
        groupId = (Long) map.get("productGroup");
        categoryId = (Long) map.get("productCategory");
        affectsStock = fetchBoolean(map, "affectsStock");
        includePrice = fetchBoolean(map, "includePrice");
        virtual = fetchBoolean(map, "virtual");
        plant = fetchBoolean(map, "plant");
        bundle = fetchBoolean(map, "bundle");
        service = fetchBoolean(map, "service");
        quantity = (Double) map.get("quantity");
        delivered = (Double) map.get("delivered");
        if (map.containsKey("price")) {
            price = new BigDecimal((Double) map.get("price"));
        } else {
            price = new BigDecimal(0d);
        }
        stockId = (Long) map.get("stockId");
        delivery = (Date) map.get("delivery");
        deliveryConfirmed = fetchBoolean(map, "deliveryConfirmed");
        externalId = (Long) map.get("externalItemId");
    }

    protected RecordDisplayItemVO(
            Long itemId,
            Long productId,
            String productName,
            String customName,
            Long typeCount,
            Long typeId,
            Long groupId,
            Long categoryId,
            boolean affectsStock,
            boolean includePrice,
            boolean virtual,
            boolean plant,
            boolean bundle,
            boolean service,
            Double quantity,
            Double delivered,
            BigDecimal price,
            Long stockId,
            Date delivery,
            boolean deliveryConfirmed) {
        super(itemId, productName);
        this.productId = productId;
        this.customName = customName;
        this.typeCount = typeCount;
        if (typeId != null) {
            if (typeIds == null) {
                this.typeIds = new HashSet<Long>();
            }
            this.typeIds.add(typeId);
        }
        this.groupId = groupId;
        this.categoryId = categoryId;
        this.affectsStock = affectsStock;
        this.includePrice = includePrice;
        this.virtual = virtual;
        this.plant = plant;
        this.bundle = bundle;
        this.service = service;
        this.quantity = quantity;
        this.delivered = delivered;
        this.price = price;
        this.stockId = stockId;
        this.delivery = delivery;
        this.deliveryConfirmed = deliveryConfirmed;
    }

    @Override
    public Object clone() {
        return new RecordDisplayItemVO(this);
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getCustomName() {
        return customName;
    }

    public void setCustomName(String customName) {
        this.customName = customName;
    }

    public Long getTypeCount() {
        return typeCount;
    }

    public void setTypeCount(Long typeCount) {
        this.typeCount = typeCount;
    }

    public Set<Long> getTypeIds() {
        return typeIds;
    }

    protected void setTypeIds(Set<Long> typeIds) {
        this.typeIds = typeIds;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public boolean isAffectsStock() {
        return affectsStock;
    }

    public void setAffectsStock(boolean affectsStock) {
        this.affectsStock = affectsStock;
    }

    public boolean isIncludePrice() {
        return includePrice;
    }

    public void setIncludePrice(boolean includePrice) {
        this.includePrice = includePrice;
    }

    public boolean isVirtual() {
        return virtual;
    }

    public void setVirtual(boolean virtual) {
        this.virtual = virtual;
    }

    public boolean isPlant() {
        return plant;
    }

    public void setPlant(boolean plant) {
        this.plant = plant;
    }

    public boolean isBundle() {
        return bundle;
    }

    public void setBundle(boolean bundle) {
        this.bundle = bundle;
    }

    public boolean isService() {
        return service;
    }

    public void setService(boolean service) {
        this.service = service;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Double getDelivered() {
        return delivered;
    }

    public void setDelivered(Double delivered) {
        this.delivered = delivered;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Long getStockId() {
        return stockId;
    }

    public void setStockId(Long stockId) {
        this.stockId = stockId;
    }

    public Date getDelivery() {
        return delivery;
    }

    public void setDelivery(Date delivery) {
        this.delivery = delivery;
    }

    public boolean isDeliveryConfirmed() {
        return deliveryConfirmed;
    }

    public void setDeliveryConfirmed(boolean deliveryConfirmed) {
        this.deliveryConfirmed = deliveryConfirmed;
    }

    public Long getExternalId() {
        return externalId;
    }

    public void setExternalId(Long externalId) {
        this.externalId = externalId;
    }
}
