/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 9, 2006 3:56:57 PM 
 * 
 */
package com.osserp.core.finance;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 
 * Represents all amounts of an invoice, calculation, etc.
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Amounts extends Serializable {

    /**
     * Provides the amount (net)
     * @return amount
     */
    BigDecimal getAmount();

    BigDecimal getAmountWithBaseTax();

    BigDecimal getAmountWithReducedTax();

    BigDecimal getAmountWithZeroTax();

    BigDecimal getTaxAmount();

    BigDecimal getReducedTaxAmount();

    BigDecimal getGrossAmount();

    /**
     * Provides the base tax rate if any item had base tax rate
     * @return taxRate or null if no item was charged with base tax
     */
    Double getTaxRate();

    /**
     * Provides the reduced tax rate if any item had reduced tax rate
     * @return reducedTaxRate or null if no item was charged with reduced tax
     */
    Double getReducedTaxRate();

}
