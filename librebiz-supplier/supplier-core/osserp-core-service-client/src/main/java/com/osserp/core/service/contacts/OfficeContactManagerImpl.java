/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 16-Jan-2007 10:59:58 
 * 
 */
package com.osserp.core.service.contacts;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdom2.Element;

import com.osserp.common.ActionException;

import com.osserp.core.BusinessCase;
import com.osserp.core.contacts.OfficeContact;
import com.osserp.core.contacts.OfficeContactManager;
import com.osserp.core.contacts.Phone;
import com.osserp.core.contacts.PhoneType;
import com.osserp.core.dao.Employees;
import com.osserp.core.dao.Phones;
import com.osserp.core.dao.SystemCompanies;
import com.osserp.core.employees.Employee;
import com.osserp.core.service.impl.AbstractService;
import com.osserp.core.system.BranchOffice;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class OfficeContactManagerImpl extends AbstractService implements OfficeContactManager {
    private static Logger log = LoggerFactory.getLogger(OfficeContactManagerImpl.class.getName());

    private static final Long SALES = 1L;
    private static final Long MANAGER = 2L;

    private static final String SPACE = " ";
    private Employees employeesDao = null;
    private Phones phones = null;
    private SystemCompanies companiesDao = null;

    /**
     * Creates a new OfficeContactManager
     * @param employees
     * @param phones
     * @param companies
     */
    public OfficeContactManagerImpl(Employees employees, Phones phones, SystemCompanies companies) {
        this.companiesDao = companies;
        this.employeesDao = employees;
        this.phones = phones;
    }

    public List<OfficeContact> getContacts() {
        return companiesDao.getOfficeContacts();
    }

    public Element getContacts(BusinessCase businessCase) {
        List<OfficeContact> list = getContacts();
        Element cs = new Element("contacts");
        for (Iterator<OfficeContact> i = list.iterator(); i.hasNext();) {
            OfficeContact o = i.next();
            try {
                cs.addContent(createContact(o, businessCase));
            } catch (ActionException e) {
                log.error("getContacts() failed for " + ((o.getId() == null) ? "null" : o.getId()));
            }
        }
        return cs;
    }

    private Element createContact(OfficeContact contact, BusinessCase businessCase) {
        Element c = new Element("contact");
        if (contact.getId().equals(SALES)
                && businessCase.getSalesId() != null) {
            addEmployee(contact, businessCase.getSalesId(), SALES);
        } else if (contact.getId().equals(MANAGER)
                && businessCase.getManagerId() != null) {
            addEmployee(contact, businessCase.getManagerId(), MANAGER);
        }
        c.addContent(new Element("role").setText(contact.getDescription()));
        c.addContent(new Element("name").setText(contact.getName()));
        c.addContent(new Element("phone").setText(contact.getPhone()));
        return c;
    }

    private void addEmployee(OfficeContact oc, Long employeeId, Long type) {
        Employee contact = employeesDao.get(employeeId);
        StringBuffer name = new StringBuffer(64);
        if (contact.getFirstName() != null) {
            name.append(contact.getFirstName()).append(SPACE);
        }
        if (contact.getLastName() != null) {
            name.append(contact.getLastName());
        }
        oc.setName(name.toString());
        String phone = null;
        if (type.equals(MANAGER)) {
            BranchOffice branch = contact.getBranch();
            if (branch != null && branch.getContact() != null) {
                Phone branchPhone = branch.getContact().getPhone();
                if (branchPhone != null) {
                    String fn = branchPhone.getFormattedNumber();
                    if (fn != null) {
                        phone = branchPhone.getFormattedNumber();
                    }
                }
                if (phone == null && branch.getCompany() != null 
                        && branch.getCompany().getContact() != null
                        && branch.getCompany().getContact().getPhone() != null) {
                    
                    phone = branch.getCompany().getContact().getPhone().getFormattedNumber();
                }
            }
            if (phone != null) {
                oc.setPhone(phone);
            }
        } else { // sales
            oc.setPhone(getPrimaryPhoneNumber(contact.getContactId()));
        }
    }

    private String getPrimaryPhoneNumber(Long contactId) {
        String number = phones.getPrimaryNumber(contactId, PhoneType.MOBILE);
        if (number == null) {
            number = phones.getPrimaryNumber(contactId, PhoneType.PHONE);
        }
        return number;
    }

}
