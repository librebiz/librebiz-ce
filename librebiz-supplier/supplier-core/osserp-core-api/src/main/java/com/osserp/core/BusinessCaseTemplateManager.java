/**
 *
 * Copyright (C) 2015 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 3, 2015 
 * 
 */
package com.osserp.core;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.FileObject;
import com.osserp.core.dms.TemplateDocument;
import com.osserp.core.users.DomainUser;


/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface BusinessCaseTemplateManager {
    
    /**
     * Creates a businessCase pdf by preselected template
     * @param domainUser
     * @param businessTemplate
     * @param businessCase
     * @return pdf
     * @throws ClientException if document specific validation failed. 
     */
    byte[] createPdf(DomainUser domainUser, BusinessTemplate businessTemplate, 
            BusinessCase businessCase) throws ClientException;
    
    /**
     * Creates a new template document
     * @param user
     * @param contextName
     * @param reference
     * @param branchId
     * @param name
     * @param description
     * @param filename
     * @param fileObject
     * @param categoryId
     * @return template
     * @throws ClientException
     */
    TemplateDocument createTemplate(
            DomainUser user,
            String contextName,
            Long reference,
            Long branchId,
            String name,
            String description,
            String filename,
            FileObject fileObject,
            Long categoryId) throws ClientException;

    /**
     * Deletes a business template
     * @param user
     * @param document
     * @throws ClientException if user has no permission or file access error 
     * (e.g. readonly, not available, etc.)
     */
    void deleteTemplate(DomainUser user, TemplateDocument document) throws ClientException;
    
    /**
     * Finds templates by context
     * @param contextName
     * @param branch
     * @return templates available by context
     */
    List<BusinessTemplate> findTemplates(String contextName, Long branch);
    
    /**
     * Provides template documents (e.g. document wrapper with template ref).
     * Use this method for administration of templates and related documents.
     * @param contextName
     * @param branch
     * @return templates by documents
     */
    List<TemplateDocument> getTemplates(String contextName, Long branch);

    /**
     * Provides all templates related to dedicated type
     * @param contextName name type.contextName
     * @return
     */
    List<TemplateDocument> getAllTemplates(String contextName);

}
