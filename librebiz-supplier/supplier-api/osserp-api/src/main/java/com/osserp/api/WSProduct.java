/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 12.12.2012 
 * 
 */
package com.osserp.api;


/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface WSProduct extends WSObject {

    Long getTypeId();
    String getType();

    Long getGroupId();
    String getGroup();

    Long getCategoryId();
    String getCategory();

    String getName();
    String getInfo();
    String getDescription();
    String getShortDescription();
    String getMarketingText();
    String getTechNote();

    String getManufacturer();
    String getColor();

    Double getWidth();
    Double getHeight();
    Double getLenght();
    String getMeasurementUnit();

    Double getWeight();
    String getWeightUnit();

    String getQuantityUnit();

    Double getPower();
    String getPowerUnit();

    Double getAvailableStock();

    Double getConsumerPrice();
    Double getResellerPrice();
    Double getPartnerPrice();
    Double getPurchasePrice();

    Double getPrice();
    Double getPriceNet();

    boolean isVatReduced();

    boolean isEndOfLife();
}
