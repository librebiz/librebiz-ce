<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="eventConfigView"/>
<c:set var="action" value="${view.bean}"/>

<v:form name="eventConfigForm" url="/admin/events/eventConfig/create">

    <div class="col-md-6 panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>
                    <fmt:message key="createNewTodoOrInfo" />
                </h4>
            </div>
        </div>
        <div class="panel-body">

            <div class="row next">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="jobName"><fmt:message key="jobName" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <input type="text" class="form-control" name="name" value="<o:out value="${action.name}"/>"/>
                    </div>
                </div>
            </div>

            <c:if test="${view.fcsMode}">
                <div class="row next">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="fcsAction"><fmt:message key="fcsAction"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <select name="callerId" class="form-control" size="1">
                                <c:forEach var="caller" items="${view.flowControls}">
                                    <option value="<o:out value="${caller.id}"/>"><o:out value="${caller.name}"/></option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </div>
            </c:if>

            <div class="row next">
                <div class="col-md-4"></div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <v:submitExit url="/admin/events/eventConfig/disableCreateMode" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <o:submit styleClass="form-control" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</v:form>
