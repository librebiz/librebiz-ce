/**
 *
 * Copyright (C) 2006, 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jul 11, 2006 
 * 
 */
package com.osserp.gui.sales

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.Constants
import com.osserp.common.dms.DocumentData
import com.osserp.common.dms.DocumentDataView

import com.osserp.core.finance.ReceivableDisplay
import com.osserp.core.finance.ReceivablesManager
import com.osserp.core.system.BranchOffice
import com.osserp.core.users.DomainUser

import com.osserp.gui.CoreView

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class SalesReceivablesView extends CoreView implements DocumentDataView {
    private static Logger logger = LoggerFactory.getLogger(SalesReceivablesView.class.getName())

    Double totalAmount = 0

    SalesReceivablesView() {
        super()
        enableAutoreload()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        reload()
    }

    String getSpreadsheet() {
        receivablesManager.createSalesReceivablesSheet(list)
    }

    public DocumentData getDocumentData() throws ClientException {
        byte[] pdf = receivablesManager.createSalesReceivablesPdf(list)
        new DocumentData(pdf, 'sales_receivables', Constants.MIME_TYPE_PDF)
    }

    @Override
    void reload() {

        List<ReceivableDisplay> salesReceivables = receivablesManager.salesReceivables

        logger.debug("reload: sales receivables fetched [count=${salesReceivables.size()}]")

        for (Iterator<ReceivableDisplay> i = salesReceivables.iterator(); i.hasNext();) {
            ReceivableDisplay next = i.next()
            if (next.getAmount() <= 0.01 && next.getAmount() >= -0.02) {
                logger.debug("reload: ignoring sales [id=${next.id}, amount=${next.amount}]")
                i.remove()
            }
        }

        DomainUser domU = domainUser
        List<BranchOffice> branchs = branchOfficeList
        for (Iterator<ReceivableDisplay> i = salesReceivables.iterator(); i.hasNext();) {
            ReceivableDisplay next = i.next()
            if (next.getBranchId()) {
                BranchOffice office = branchs.find { it.id == next.getBranchId() }
                if (office && !domU.isBranchAccessible(office)) {
                    i.remove()
                }
            } else {
                logger.warn("reload: ignoring sales without branch [id=${next.id}]")
            }
        }

        totalAmount = 0
        salesReceivables.each { ReceivableDisplay receivable ->
            totalAmount = totalAmount + receivable.amount
        }
        logger.debug("reload: done [total=${totalAmount}]")
        list = salesReceivables
    }

    protected ReceivablesManager getReceivablesManager() {
        getService(ReceivablesManager.class.getName())
    }
}
