<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.deliveryMonitoringView}">
    <o:logger write="delivery_monitoring.jsp no view bound" level="warn"/>
    <c:redirect url="/errors/error_context.jsp"/>
</c:if>
<c:set var="view" value="${sessionScope.deliveryMonitoringView}"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title"><o:displayTitle/></tiles:put>
    <tiles:put name="headline">
        <c:choose>
            <c:when test="${view.setupMode}">
                <fmt:message key="settings"/>: <fmt:message key="deliveryDatesAfterOrders"/>
            </c:when>
            <c:otherwise>
                <fmt:message key="deliveryDatesAfterOrders"/>
            </c:otherwise>
        </c:choose>
    </tiles:put>
    <tiles:put name="headline_right">
        <ul>
            <c:choose>
                <c:when test="${view.setupMode}">
                    <li><a href="<c:url value="/salesDeliveryMonitoring.do?method=disableSetup"/>"><o:img name="backIcon"/></a></li>
                </c:when>
                <c:otherwise>
                    <li><a href="<c:url value="/salesDeliveryMonitoring.do?method=exit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a></li>
                    <c:choose>
                        <c:when test="${view.snipListMode}">
                            <li><a href="<c:url value="/salesDeliveryMonitoring.do?method=toggleSnipListMode"/>" title="<fmt:message key="disableSnipFinishedMode"/>"><o:img name="cutIcon"/></a></li>
                        </c:when>
                        <c:otherwise>
                            <li><a href="<c:url value="/salesDeliveryMonitoring.do?method=toggleSnipListMode"/>" title="<fmt:message key="enableSnipFinishedMode"/>"><o:img name="enabledIcon"/></a></li>
                        </c:otherwise>
                    </c:choose>
                    <li><a href="<c:url value="/salesDeliveryMonitoring.do?method=refresh"/>" title="<fmt:message key="listRefresh"/>"><o:img name="replaceIcon"/></a></li>
                    <li><a href="<c:url value="/salesDeliveryMonitoring.do?method=enableSetup"/>" title="<fmt:message key="settings"/>"><o:img name="configureIcon"/></a></li>
                    <li><a href="<c:url value="/salesDeliveryMonitoring.do?method=toggleMonitoringBySalesMode"/>" title="<fmt:message key="listByPositions"/>"><o:img name="reloadIcon"/></a></li>
                </c:otherwise>
            </c:choose>
            <li><v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link></li>
        </ul>
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="deliveryMonitoringContent">
            <div class="row">
                <c:choose>
                    <c:when test="${view.setupMode}">
                        <c:import url="delivery_monitoring_settings.jsp"/>
                    </c:when>
                    <c:otherwise>
                        <div class="col-md-12 panel-area">
                            <div class="table-responsive table-responsive-default">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th class="recordNumber"><fmt:message key="number"/></th>
                                            <th class="recordName"><fmt:message key="name"/></th>
                                            <th class="recordEmployeeKey"><fmt:message key="technicianShortcut"/></th>
                                            <th class="recordEmployeeKey"><fmt:message key="salesShortcut"/></th>
                                            <th class="recordDate">
                                                <a href="<c:url value="/salesDeliveryMonitoring.do?method=toggleListDescendantMode"/>" title="<fmt:message key="${view.listDescendantMode ? 'sortUp' : 'sortDown'}"/>">
                                                    <fmt:message key="deliveryDayShort"/>
                                                </a>
                                            </th>
                                            <th class="recordDate"><fmt:message key="installationAppointmentShort"/></th>
                                            <th class="recordAmount"><fmt:message key="value"/></th>
                                            <th class="recordStatus"><fmt:message key="statusPercent"/></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="bySales" items="${view.monitoringBySales}" varStatus="s">
                                            <c:set var="obj" value="${bySales.item}"/>
                                            <tr class="altrow">
                                                <td class="recordNumber"><a href="<c:url value="/loadSales.do?exit=salesDeliveryMonitoring&id=${obj.id}"/>"><o:out value="${obj.id}"/></a></td>
                                                <td class="recordName">
                                                    <c:if test="${view.snipListMode}">
                                                        <span style="margin: 0 3px;">
                                                            <a href="<c:url value="/salesDeliveryMonitoring.do?method=snipList&id=${obj.id}"/>" title="<fmt:message key="title.record.delete"/>"><o:img name="deleteIcon"/></a>
                                                        </span>
                                                    </c:if>
                                                    <o:out value="${obj.name}" limit="40"/>
                                                </td>
                                                <td class="recordEmployeeKey">
                                                    <o:ajaxLink linkId="managerDisplay" url="${'/employeeInfo.do?id='}${obj.managerId}">
                                                        <oc:employee initials="true" value="${obj.managerId}" />
                                                    </o:ajaxLink>
                                                </td>
                                                <td class="recordEmployeeKey">
                                                    <o:ajaxLink linkId="salesDisplay" url="${'/employeeInfo.do?id='}${obj.salesId}">
                                                        <oc:employee initials="true" value="${obj.salesId}" />
                                                    </o:ajaxLink>
                                                </td>
                                                <td class="recordDate">
                                                    <o:date value="${obj.deliveryDate}" addcentury="false"/>
                                                </td>
                                                <td class="recordDate">
                                                    <o:date value="${obj.installationDate}" addcentury="false"/>
                                                </td>
                                                <td class="recordAmount">
                                                    <o:number format="currency" value="${obj.capacity}"/>
                                                </td>
                                                <td class="recordStatus">
                                                    <v:ajaxLink url="/sales/businessCaseDisplay/forward?id=${obj.id}" linkId="businessCaseDisplayView">
                                                        <o:out value="${obj.status}"/>
                                                    </v:ajaxLink>
                                                </td>
                                            </tr>
                                            <c:forEach var="product" items="${bySales.positions}" varStatus="s">
                                                <tr>
                                                <td class="recordNumber"><a href="<c:url value="/products.do"><c:param name="method" value="load"/><c:param name="exit" value="salesDeliveryMonitoring"/><c:param name="id" value="${product.productId}"/></c:url>"><o:out value="${product.productId}"/></a></td>
                                                    <td class="recordPosition" colspan="6">
                                                        <a href="<c:url value="/salesPlanning.do"><c:param name="method" value="forwardProductPlanning"/><c:param name="exit" value="salesDeliveryMonitoring"/><c:param name="id" value="${product.productId}"/><c:param name="stockId" value="${view.selectedStock.id}"/></c:url>"><o:out value="${product.productName}"/></a>
                                                    </td>
                                                    <td class="recordStatus">
                                                        <v:ajaxLink url="/sales/deliveryStatusDisplay/forward?id=${obj.id}" linkId="deliveryStatus" title="deliveryStatus">
                                                            <o:number format="integer" value="${product.outstanding}"/>
                                                        </v:ajaxLink>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
