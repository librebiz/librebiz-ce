/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 5, 2009 10:45:36 AM 
 * 
 */
package com.osserp.gui.client.purchasing.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;

import com.osserp.core.BankAccount;
import com.osserp.core.Item;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.Payment;
import com.osserp.core.finance.Record;
import com.osserp.core.purchasing.PurchaseInvoice;
import com.osserp.core.purchasing.PurchaseInvoiceAwareManager;

import com.osserp.gui.client.purchasing.PurchaseInvoiceAwareView;
import com.osserp.gui.client.records.impl.AbstractRecordView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractPurchaseInvoiceView extends AbstractRecordView implements PurchaseInvoiceAwareView {
    private static Logger log = LoggerFactory.getLogger(AbstractPurchaseInvoiceView.class.getName());
    private List<Long> creditNoteList = new ArrayList<>();

    protected AbstractPurchaseInvoiceView() {
        super();
    }

    public boolean isSupplierReferenceNumberEditModeDisplayedWhenClosed() {
        return false;
    }

    @Override
    protected void deleteItemValidation(Record record, Item selected) throws ClientException {
        PurchaseInvoice invoice = (PurchaseInvoice) record;

        if (!invoice.isCreditNote()) {

            if (invoice.getStatus().equals(Invoice.STAT_CHANGED)) {
                if (log.isInfoEnabled()) {
                    log.info("deleteItemValidation() delete item not supported if status of record reports 'changed' [user="
                            + getUser().getId() + "]");
                }
                throw new ClientException(ErrorCode.OPERATION_NOT_SUPPORTED);
            }
            if (!getRecordManager().deletePermissionGrant(getDomainUser(), invoice)) {
                if (log.isInfoEnabled()) {
                    log.info("deleteItemValidation() failed, deletePermission not grant [user="
                            + getUser().getId() + "]");
                }
                throw new ClientException(ErrorCode.PERMISSION_DENIED);
            }
        }
    }

    public void release() throws ClientException {
        PurchaseInvoiceAwareManager manager = getInvoiceAwareManager();
        Invoice current = (Invoice) getRecord();
        if (current.getItems().isEmpty()) {
            throw new ClientException(ErrorCode.ITEM_MISSING);
        }
        manager.updateStatus(
                getRecord(),
                getDomainUser().getEmployee(),
                Invoice.STAT_CLOSED);
        if (log.isDebugEnabled()) {
            log.debug("release() done [invoice=" + current.getId() + "]");
        }
    }

    @Override
    public void save(Form form) throws ClientException, PermissionException {
        PurchaseInvoice record = (PurchaseInvoice) getRecord();
        PurchaseInvoiceAwareManager manager = getInvoiceAwareManager();
        manager.update(
                record,
                form.getLong("signatureLeft"),
                form.getLong("signatureRight"),
                form.getLong("personId"),
                form.getString("note"),
                form.getBoolean("taxFree"),
                form.getLong("taxFreeId"),
                form.getDouble("taxRate"),
                form.getDouble("reducedTaxRate"),
                form.getLong("currency"),
                form.getString("language"),
                form.getLong("branch"),
                form.getLong("shippingId"),
                form.getString("customHeader"),
                form.getBoolean("printComplimentaryClose"),
                form.getBoolean("printProjectmanager"),
                form.getBoolean("printSalesperson"),
                form.getBoolean("printBusinessCaseId"),
                form.getBoolean("printBusinessCaseInfo"),
                form.getBoolean("printRecordDate"),
                form.getBoolean("printRecordDateByStatus"),
                form.getBoolean("printPaymentTarget"),
                false,
                false);
        if (record.getType().isOverrideCreatedDate()) {
            Date bookingDate = form.getDate("createdDate");
            if (bookingDate != null) {
                PurchaseInvoice updated = (PurchaseInvoice) manager.find(record.getId());
                if (updated != null) {
                    manager.changeCreatedDate(updated, bookingDate);
                }
            }
        }
        if (form.getBoolean("overrideTaxCalculation")) {
            manager.updateSummary(
                    record,
                    form.getString("taxValue"),
                    form.getString("reducedTaxValue"),
                    form.getString("grossValue"));
        }
        refresh();
    }

    public Invoice getInvoice() {
        return (Invoice) getRecord();
    }

    public List<Long> getCreditNoteList() {
        return creditNoteList;
    }

    @Override
    public void setRecord(Record record) {
        super.setRecord(record);
        PurchaseInvoice invoice = (PurchaseInvoice) getRecord();
        PurchaseInvoiceAwareManager manager = getInvoiceAwareManager();
        creditNoteList = manager.getCreditNoteIds(invoice);
    }

    @Override
    public boolean isProvidingStatusHistory() {
        return false;
    }

    @Override
    public boolean isSalesRecordView() {
        return false;
    }

    public String getPaymentInfoDisplay() {
        Invoice invoice = getInvoice();
        if (invoice.getPayments().isEmpty() || isNotSet(invoice.getCompany())) {
            return null;
        }
        List<BankAccount> bankAccounts = getSystemConfigManager().getCompany(invoice.getCompany()).getBankAccounts();
        if (bankAccounts.isEmpty()) {
            return null;
        }
        StringBuilder buffer = new StringBuilder();
        Set<Long> added = new HashSet();
        for (int i = 0, j = invoice.getPayments().size(); i < j; i++) {
            Payment p = invoice.getPayments().get(i);
            if (!added.contains(p.getId()) && p.getBankAccountId() != null) {
                String name = getBankAccountName(bankAccounts, p.getBankAccountId());
                if (isSet(name)) {
                    if (i > 0) {
                        buffer.append(", ");
                    }
                    buffer.append(name);
                    added.add(p.getId());
                }
            }
        }
        return buffer.toString();
    }

    private String getBankAccountName(List<BankAccount> bankAccounts, Long accountId) {
        for (int i = 0, j = bankAccounts.size(); i < j; i++) {
            BankAccount ba = bankAccounts.get(i);
            if (ba.getId().equals(accountId)) {
                return ba.getName();
            }
        }
        return null;
    }

    protected PurchaseInvoiceAwareManager getInvoiceAwareManager() {
        return (PurchaseInvoiceAwareManager) getRecordManager();
    }
}
