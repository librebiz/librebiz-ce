/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Feb 23, 2010 1:14:27 PM 
 * 
 */
package com.osserp.core.telephone;

import com.osserp.common.Entity;
import com.osserp.common.Option;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public interface Telephone extends Entity {

    /**
     * Returns the telephoneSystem
     * @return telephoneSystem
     */
    TelephoneSystem getTelephoneSystem();

    /**
     * Sets the telephoneSystem
     * @param telephoneSystem
     */
    void setTelephoneSystem(TelephoneSystem telephoneSystem);

    /**
     * Returns the telephoneType
     * @return telephoneType
     */
    Option getTelephoneType();

    /**
     * Sets the telephoneType
     * @param telephoneType
     */
    void setTelephoneType(Option telephoneType);

    /**
     * Provides the mac
     * @return mac
     */
    String getMac();

    /**
     * Sets the mac
     * @param mac
     */
    void setMac(String mac);

    /**
     * Indicates that the telephone is eol
     * @return eol
     */
    boolean isEol();

    /**
     * Sets eol
     * @param eol
     */
    void setEol(boolean eol);
}
