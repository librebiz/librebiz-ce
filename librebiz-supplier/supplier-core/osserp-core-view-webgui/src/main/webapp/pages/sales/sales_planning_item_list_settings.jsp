<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>

<c:set var="view" value="${sessionScope.openSalesOrdersView}"/>
<div class="contentBox">
    <div class="contentBoxData" style="height: 550px;">
        <div class="subcolumns">
            <div class="subcolumn">
                <div class="spacer"></div>
                <table>
                    <tr>
                        <td style="width: 25%;">
                            <o:form name="productTypeSelectionForm" url="/salesPlanning.do?method=selectProductType">
                                <oc:select disabled="true" name="id" options="${view.productTypes}" value="${view.selectedProductType}" onchange="document.productTypeSelectionForm.submit();" style="width: 200px;" prompt="false">
                                    <oc:option name="typeWithDots" value="-1"/>
                                </oc:select>
                            </o:form>
                        </td>
                        <td style="width: 25%;">
                            <o:form name="productGroupSelectionForm" url="/salesPlanning.do?method=selectProductGroup">
                                <oc:select name="id" options="${view.productGroups}" value="${view.selectedProductGroup}" onchange="document.productGroupSelectionForm.submit();" style="width: 200px;" prompt="false">
                                    <oc:option name="groupWithDots" value="-1"/>
                                </oc:select>
                            </o:form>
                        </td>
                        <td style="width: 25%;">
                            <o:form name="productCategorySelectionForm" url="/salesPlanning.do?method=selectProductCategory">
                                <oc:select disabled="${empty view.selectedProductGroup}" name="id" options="${view.productCategories}" value="${view.selectedProductCategory}" onchange="document.productCategorySelectionForm.submit();" style="width: 200px;" prompt="false">
                                    <oc:option name="categoryWithDots" value="-1"/>
                                </oc:select>
                            </o:form>
                        </td>
                    </tr>
                    <tr><td>&nbsp;</td></tr>
                    <tr>
                        <td style="width: 25%;">
                            <select name="selectAction" size="1" onChange="gotoUrl(this.value);" style="width: 200px;" title="<fmt:message key="stockSelection"/>">
                                <c:forEach var="obj" items="${view.availableStocks}">
                                    <option value="<c:url value="/salesPlanning.do?method=selectStock&id=${obj.id}"/>" <c:if test="${obj.id == view.selectedStock.id}">selected="selected"</c:if>>
                                        <o:out value="${obj.name}"/>
                                    </option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                </table>

                <div class="spacer"></div>
                <div class="spacer"></div>

                <table class="valueTable">
                    <tr>
                        <td style="width: 24%;"><label style="float:left"><fmt:message key="dispositionState"/></label></td>
                        <td>
                            <c:choose>
                                <c:when test="${view.openPurchaseOnly}">
                                    <p class="bighead"><fmt:message key="displayProductsWithDeficitInPeriodOnly"/></p>
                                    <p><a href="<c:url value="/salesPlanning.do?method=changeOpenPurchaseOnlyFlag"/>"><fmt:message key="displayAllProducts"/></a></p>
                                </c:when>
                                <c:otherwise>
                                    <p class="bighead"><fmt:message key="displayAllProducts"/></p>
                                    <p><a href="<c:url value="/salesPlanning.do?method=changeOpenPurchaseOnlyFlag"/>"><fmt:message key="displayProductsWithDeficitInPeriodOnly"/></a></p>
                                </c:otherwise>
                            </c:choose>
                        </td>
                    </tr>
                    <tr><td colspan="2">&nbsp;</td></tr>
                    <tr>
                        <td><label style="float:left"><fmt:message key="salesStatus"/></label></td>
                        <td>
                            <c:choose>
                                <c:when test="${view.vacancyDisplayMode}">
                                    <p class="bighead"><fmt:message key="displayAllBetweenZeroAndHundred"/></p>
                                    <p><a href="<c:url value="/salesPlanning.do?method=toggleVacancyDisplay"/>"><fmt:message key="displayConfirmedGreater15Only"/></a></p>
                                </c:when>
                                <c:otherwise>
                                    <p class="bighead"><fmt:message key="displayConfirmedGreater15Only"/></p>
                                    <p><a href="<c:url value="/salesPlanning.do?method=toggleVacancyDisplay"/>"><fmt:message key="displayAllBetweenZeroAndHundred"/></a></p>
                                </c:otherwise>
                            </c:choose>
                        </td>
                    </tr>
                    <tr><td colspan="2">&nbsp;</td></tr>
                    <tr>
                        <td><label style="float:left"><fmt:message key="purchaseStatus"/></label></td>
                        <td>
                            <c:choose>
                                <c:when test="${view.confirmedPurchaseOnly}">
                                    <p class="bighead"><fmt:message key="displayConfirmedOnly"/></p>
                                    <p><a href="<c:url value="/salesPlanning.do?method=changeConfirmedPurchaseOnlyFlag"/>"><fmt:message key="displayAll"/></a></p>
                                </c:when>
                                <c:otherwise>
                                    <p class="bighead"><fmt:message key="displayAll"/></p>
                                    <p><a href="<c:url value="/salesPlanning.do?method=changeConfirmedPurchaseOnlyFlag"/>"><fmt:message key="displayConfirmedOnly"/></a></p>
                                </c:otherwise>
                            </c:choose>
                        </td>
                    </tr>
                    <tr><td colspan="2">&nbsp;</td></tr>
                    <tr>
                        <td><label style="float:left"><fmt:message key="planningUntil"/></label></td>
                        <td>
                            <o:form name="horizonForm" url="/salesPlanning.do">
                                <input type="hidden" name="method" value="updateHorizon"/>
                                <input type="text" style="width: 90px; text-align: right;" name="horizon" value="<o:date value="${view.horizon}"/>" id="horizon" /> <o:datepicker input="horizon"/>
                                <input type="image" name="method" src="<o:icon name="saveIcon"/>" value="updateHorizon" onclick="setMethod('updateHorizon');" style="margin-left: 15px" />
                                <a href="<c:url value="/salesPlanning.do?method=resetHorizon"/>" style="margin-left: 15px"><o:img name="deleteIcon" /></a>
                            </o:form>
                        </td>
                    </tr>
                </table>
                <div class="spacer"></div>
            </div>
        </div>
    </div>
</div>
