/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 27 2013 09:05:55 
 * 
 */
package com.osserp.core;

import com.osserp.common.Option;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface NoteType extends Option {

    /**
     * Provides the name of the responsible service
     * @return serviceName
     */
    String getServiceName();

    /**
     * Indictes if notes of this type are providing confirmation support
     * @return confirmationSupport
     */
    boolean isConfirmationSupport();

    /**
     * Indictes if confirmation of notes of this types are required
     * @return confirmationSupport
     */
    boolean isConfirmationRequired();

    /**
     * Provides the confirmation expection resolver (e.g. mechanism name to determine who has to confirm the note)
     * @return confirmationExpectionResolver
     */
    String getConfirmationExpectionResolver();

    /**
     * Indicates if notes of this type are supporting headlines
     * @return true if headline support enabled
     */
    boolean isSupportingHeadline();

    /**
     * Indicates if notes of this type are supporting sending notes as mail
     * @return true if mail support is enabled
     */
    boolean isSupportingMail();

    /**
     * Indicates if mail recipient selection is disabled if mail support available
     * @return true if mail recipient selection is disabled
     */
    boolean isMailRecipientsDisabled();

    /**
     * Enables/disables mail recipient disabled setting for mail support
     * @param mailRecipientsDisabled
     */
    void setMailRecipientsDisabled(boolean mailRecipientsDisabled);

    /**
     * Indicates if mail cc recipient selection is disabled if mail support available
     * @return true if mail cc recipient selection is disabled
     */
    boolean isMailRecipientsCCDisabled();

    /**
     * Enables/disables mail cc recipient disabled setting for mail support
     * @param mailRecipientsCCDisabled
     */
    void setMailRecipientsCCDisabled(boolean mailRecipientsCCDisabled);

    /**
     * Indicates if mail bcc recipient selection is disabled if mail support available
     * @return true if mail bcc recipient selection is disabled
     */
    boolean isMailRecipientsBCCDisabled();

    /**
     * Enables/disables mail bcc recipient disabled setting for mail support
     * @param mailRecipientsBCCDisabled
     */
    void setMailRecipientsBCCDisabled(boolean mailRecipientsBCCDisabled);

    /**
     * Indicates if mail attachments are disabled if mail support available
     * @return true if mail attachments disabled
     */
    boolean isMailAttachmentsDisabled();

    /**
     * Enables/disables mail attachments disabled setting for mail support
     * @param mailAttachmentsDisabled
     */
    void setMailAttachmentsDisabled(boolean mailAttachmentsDisabled);

    /**
     * Provides the mailOriginator if originator should be fixed
     * @return mailOriginator
     */
    String getMailOriginator();

    /**
     * Sets a fixed mailOriginator
     * @param mailOriginator
     */
    void setMailOriginator(String mailOriginator);
}
