/**
 *
 * Copyright (C) 2010, 2013 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Nov 8, 2010 11:00:50 AM 
 * 
 */
package com.osserp.groovy.web.tag

import javax.servlet.ServletContext
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpSession
import javax.servlet.jsp.JspException
import javax.servlet.jsp.JspWriter

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.web.RequestUtil
import com.osserp.common.web.tags.AbstractTagSupport
import com.osserp.groovy.web.WebUtil
import com.osserp.groovy.web.View

/**
 *
 * @author jg <jg@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public abstract class AbstractTag extends AbstractTagSupport {
    private static Logger logger = LoggerFactory.getLogger(AbstractTag.class.getName())

    protected abstract String getStartTag();

    protected abstract String getEndTag();


    int doStartTag() throws JspException {
        def response = startTag
        if (response) {
            writeResponse(response)
        }
        return EVAL_BODY_INCLUDE
    }

    int doEndTag() throws JspException {
        def response = endTag
        if (response) {
            writeResponse(response)
        }
        return EVAL_BODY_INCLUDE
    }

    /**
     * Provides current view if available
     * @return current view
     */
    protected final View getCurrentView() {
        WebUtil.fetchCurrentView(request)
    }

    /**
     * Writes tags response to servlet output stream.
     */
    protected final void writeResponse(String response) {
        try {
            JspWriter out = pageContext.getOut()
            out.print(response)
        } catch (Exception e) {
            logger.error("writeResponse: failed [message=${e.message}]")
        }
    }
    
    protected final String renderCommonAttributes() {
        StringBuilder buffer = new StringBuilder()
        buffer.append(renderStyles())
        appendSpecificAttributes(addTitle(buffer))
    }
    
    protected String appendSpecificAttributes(StringBuilder commonAttributes) {
        return commonAttributes.toString()
    }

    /**
     * Creates an url by adding provided action to contextPath and viewPath (if view path available)
     * @param action
     * @return created url with context and view path (if view path required)
     */
    protected String createUrl(String action) {
        if (WebUtil.isStrutsRequest(action)) {
            //url.contains('.do')) {
            return "${contextPath}${action}"
        } else if (action.startsWith(WebUtil.getViewPath(servletContext, request))) {
            return action
        } else {
            return "${viewContextPath}${action}"
        }
    }

    /**
     * Provides the view context path (e.g. '/ctxname/app')
     * @return viewContextPath
     */
    protected final String getViewContextPath() {
        "${contextPath}${WebUtil.getViewServletPath(servletContext)}"
    }
}
