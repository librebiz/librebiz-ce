<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="recordInfoConfigView" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="styles" type="string">
        <style type="text/css">
        </style>
    </tiles:put>
    <tiles:put name="headline">
        <fmt:message key="${view.headerName}" />
        <c:if test="${!empty view.recordType}">
            - <oc:options name="recordTypes" value="${view.recordType}" />
        </c:if>
        <c:if test="${!empty view.requestType}">
            [<fmt:message key="context" />
            <oc:options name="requestTypes" value="${view.requestType}" />]
        </c:if>
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="recordInfoConfigContent">
            <c:choose>
                <c:when test="${empty view.recordType or empty view.requestType}">
                    <div class="row">
                        <div class="col-lg-12 panel-area">
                            <div class="table-responsive table-responsive-default">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th id="name"><fmt:message key="name" /></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:choose>
                                            <c:when test="${empty view.recordType}">
                                                <c:forEach var="option" items="${view.recordTypes}">
                                                    <tr>
                                                        <td valign="top" class="number">
                                                            <v:link url="/admin/records/recordInfoConfig/selectRecordType?id=${option.id}">
                                                                <o:out value="${option.name}" />
                                                            </v:link>
                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                            </c:when>
                                            <c:otherwise>
                                                <c:forEach var="option" items="${view.requestTypes}">
                                                    <tr>
                                                        <td valign="top" class="number">
                                                            <v:link url="/admin/records/recordInfoConfig/selectRequestType?id=${option.id}">
                                                                <o:out value="${option.name}" />
                                                            </v:link>
                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                            </c:otherwise>
                                        </c:choose>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="row">
                        <div class="col-lg-12 panel-area">
                            <div class="table-responsive table-responsive-default">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th colspan="3" class="center"> </td>
                                            <th><fmt:message key="assignedConditions" /></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="option" items="${view.recordInfos}">
                                            <tr>
                                                <td class="icon center" valign="top">
                                                    <v:link url="/admin/records/recordInfoConfig/remove?id=${option}">
                                                        <o:img name="deleteIcon" styleClass="bigicon" />
                                                    </v:link>
                                                </td>
                                                <td class="icon center" valign="top">
                                                    <v:link url="/admin/records/recordInfoConfig/moveUp?id=${option}">
                                                        <o:img name="upIcon" styleClass="bigicon" />
                                                    </v:link>
                                                </td>
                                                <td class="icon center" valign="top">
                                                    <v:link url="/admin/records/recordInfoConfig/moveDown?id=${option}">
                                                        <o:img name="downIcon" styleClass="bigicon" />
                                                    </v:link>
                                                </td>
                                                <td valign="top"><oc:options name="recordInfoConfigs" value="${option}" format="true" /></td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                    <thead>
                                        <tr>
                                            <th colspan="3" class="center"> </td>
                                            <th><fmt:message key="availableConditions" /></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="option" items="${view.availableInfos}">
                                            <tr>
                                                <td class="icon center"> </td>
                                                <td class="icon center">
                                                    <v:link url="/admin/records/recordInfoConfig/add?id=${option}">
                                                        <o:img name="enabledIcon" styleClass="bigicon" />
                                                    </v:link>
                                                </td>
                                                <td class="icon center"> </td>
                                                <td valign="top"><oc:options name="recordInfoConfigs" value="${option}" format="true" /></td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
    </tiles:put>
</tiles:insert>
