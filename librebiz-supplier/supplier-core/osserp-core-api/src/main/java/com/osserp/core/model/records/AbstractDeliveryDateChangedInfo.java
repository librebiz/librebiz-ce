/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 02-May-2010 
 * 
 */
package com.osserp.core.model.records;

import java.util.Date;

import com.osserp.common.Constants;
import com.osserp.common.beans.AbstractEntity;

import com.osserp.core.employees.Employee;
import com.osserp.core.finance.DeliveryDateChangedInfo;
import com.osserp.core.finance.Order;
import com.osserp.core.products.Product;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author cf <cf@osserp.com>
 * 
 */
public abstract class AbstractDeliveryDateChangedInfo extends AbstractEntity implements DeliveryDateChangedInfo {
    private Product product;
    Long stockId;
    private Double quantity;
    private Date previousDate;
    private Date newDate;
    private String note;

    /**
     * Required for object seriali
     */
    protected AbstractDeliveryDateChangedInfo() {
        super();
    }

    /**
     * Default constructor to create new delivery date changed info
     * @param user
     * @param order
     * @param product
     * @param quantity
     * @param stockId
     * @param previousDate
     * @param newDate
     * @param note
     */
    public AbstractDeliveryDateChangedInfo(
            Employee user,
            Order order,
            Product product,
            Double quantity,
            Long stockId,
            Date previousDate,
            Date newDate,
            String note) {
        super((Long) null, order.getId(), (user == null ? Constants.SYSTEM_EMPLOYEE : user.getId()));
        this.product = product;
        this.quantity = quantity;
        this.stockId = stockId;
        this.previousDate = previousDate;
        this.newDate = newDate;
        this.note = note;
    }

    public AbstractDeliveryDateChangedInfo(DeliveryDateChangedInfo other) {
        super((Long) null, other.getReference(), (other.getCreatedBy() == null ? Constants.SYSTEM_EMPLOYEE : other.getCreatedBy()));
        this.product = other.getProduct();
        this.quantity = other.getQuantity();
        this.stockId = other.getStockId();
        this.previousDate = other.getPreviousDate();
        this.newDate = other.getNewDate();
        this.note = other.getNote();
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Long getStockId() {
        return stockId;
    }

    public void setStockId(Long stockId) {
        this.stockId = stockId;
    }

    public Date getPreviousDate() {
        return previousDate;
    }

    public void setPreviousDate(Date previousDate) {
        this.previousDate = previousDate;
    }

    public Date getNewDate() {
        return newDate;
    }

    public void setNewDate(Date newDate) {
        this.newDate = newDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
