SET search_path = osserp, pg_catalog;

ALTER TABLE system_company_stocks ADD COLUMN branch_id bigint REFERENCES system_company_branchs (id);

UPDATE system_properties SET pvalue = '0.14.8', ptype = 'java.lang.String' WHERE name = 'dbmsSchemaVersion';
