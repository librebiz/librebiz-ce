/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.directory.ldap.impl

import com.osserp.common.directory.ldap.LdapConfig
import com.osserp.common.directory.ldap.dao.Clients
import com.osserp.common.directory.ldap.dao.Groups
import com.osserp.common.directory.ldap.dao.Internetdomains
import com.osserp.common.directory.ldap.dao.Permissions
import com.osserp.common.directory.ldap.dao.Users

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class AbstractLdapManager extends AbstractLdapConfigAware {

    protected static final int MBOX_GECOS_LENGTH = 8

    protected Clients clients
    protected Groups groups
    protected Internetdomains domains
    protected Permissions permissions
    protected Users users

    protected AbstractLdapManager() {
        super()
    }

    protected AbstractLdapManager(String configFile) {
        super(new LdapConfig(configFile))
        clients = new ClientsImpl(config)
        groups = new GroupsImpl(config)
        users = new UsersImpl(config)
        domains = new InternetdomainsImpl(config)
        permissions = new PermissionsImpl(config)
    }

    protected AbstractLdapManager(LdapConfig configInstance) {
        super(configInstance)
        clients = new ClientsImpl(config)
        groups = new GroupsImpl(config)
        users = new UsersImpl(config)
        domains = new InternetdomainsImpl(config)
        permissions = new PermissionsImpl(config)
    }

    protected AbstractLdapManager(AbstractLdapManager otherManager) {
        super(otherManager.config)
        clients = otherManager.clients
        groups = otherManager.groups
        users = otherManager.users
        domains = otherManager.domains
        permissions = otherManager.permissions
    }

    Map lookupClient(String name) {
        clients.find(name)
    }

    Map lookupGroupByDn(String dn) {
        groups.lookupByDn(dn)
    }

    List<Map> lookupUsers(String client) {
        List<Map> all = users.findAll(client)
        List<Map> result = []
        for (user in all) {
            if (!user.gecos || !('Computer' == user.gecos)) {

                result << user
            }
        }
        return result
    }

    Map lookupUser(String uid, String client) {
        users.lookup(uid, client)
    }

    Map lookupUserByDn(String dn) {
        users.lookupByDn(dn)
    }

    Map changeClientPassword(Map client, String password) {
        clients.changePassword(client, password)
    }

    Map createDomain(String name, String mailhost, String client) {
        domains.create(name, mailhost, client)
    }

    void deleteDomain(Map domain) {
        domains.delete(domain)
    }

    List<Map> lookupDomains(String client) {
        domains.findAll(client)
    }

    Map lookupDomain(String name, String client) {
        domains.find(name, client)
    }

    Map changeDomainMailhosts(Map domain, String mailhost, String mailrelay) {
        domains.updateMailhosts(domain, mailhost, mailrelay)
    }

    Map updateDomain(Map domain, String name, String value) {
        def values = [name: value]
        domains.update(domain, values)
    }

    Map switchMaildomain(Map domain) {
        def values = [maildomain : domain.maildomain ]
        if (values.maildomain) {
            values.maildomain = null
        } else {
            values.maildomain = domain.dc
        }
        domains.update(domain, values)
    }
}
