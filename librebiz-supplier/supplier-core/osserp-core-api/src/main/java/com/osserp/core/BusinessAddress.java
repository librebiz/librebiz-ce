/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 30-Oct-2006 09:20:03 
 * 
 */
package com.osserp.core;


/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface BusinessAddress extends Address {

    /**
     * Provides a person name as addon for the name if name is used a company name
     * @return person
     */
    String getPerson();

    /**
     * Sets the person
     * @param person
     */
    void setPerson(String person);

    /**
     * A phone number for usage if project address is an independant contact
     * @return phone
     */
    String getPhone();

    /**
     * Sets the phone number for usage if project address is an independant contact
     * @param phone
     */
    void setPhone(String phone);

    /**
     * A fax number for usage if project address is an independant contact
     * @return fax
     */
    String getFax();

    /**
     * Sets the fax number for usage if project address is an independant contact
     * @param fax
     */
    void setFax(String fax);

    /**
     * A mobile number for usage if project address is an independant contact
     * @return mobile
     */
    String getMobile();

    /**
     * Sets the fax number for usage if project address is an independant contact
     * @param mobile
     */
    void setMobile(String mobile);

    /**
     * An email address for usage if project address is an independant contact
     * @return email
     */
    String getEmail();

    /**
     * Sets the email address for usage if project address is an independant contact
     * @param email
     */
    void setEmail(String email);
}
