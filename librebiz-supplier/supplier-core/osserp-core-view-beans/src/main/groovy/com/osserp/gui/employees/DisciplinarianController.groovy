/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Jun 18, 2010 at 10:44:55 AM 
 * 
 */
package com.osserp.gui.employees

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import javax.servlet.http.HttpServletRequest
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.groovy.web.ViewController

/**
 * 
 * @author jg <jg@osserp.com>
 * @author rk <rk@osserp.com>
 *
 */
@RequestMapping("/employees/disciplinarian/*")
@Controller class DisciplinarianController extends ViewController {
    private static Logger logger = LoggerFactory.getLogger(DisciplinarianController.class.getName())

    @RequestMapping
    def remove(HttpServletRequest request) {
        logger.debug("remove: invoked [user=${getUserId(request)}]")
        DisciplinarianView view = getView(request)
        view.remove()
        defaultPage
    }
}

