/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 10-Aug-2006 19:16:32 
 * 
 */
package com.osserp.gui.client.records.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.jdom2.Document;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.EmptyListException;
import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionException;
import com.osserp.common.TimeoutException;
import com.osserp.common.dms.DmsDocument;
import com.osserp.common.dms.DmsManager;
import com.osserp.common.dms.DmsReference;
import com.osserp.common.dms.DocumentData;
import com.osserp.common.util.CollectionUtil;
import com.osserp.common.web.Form;
import com.osserp.common.web.ViewName;

import com.osserp.core.finance.RecordComparators;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.finance.RecordExport;
import com.osserp.core.finance.RecordExportType;
import com.osserp.core.finance.RecordExportManager;
import com.osserp.core.finance.RecordType;
import com.osserp.core.finance.Records;
import com.osserp.core.system.SystemConfig;

import com.osserp.gui.client.impl.AbstractView;
import com.osserp.gui.client.records.RecordExportView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("recordExportView")
public class RecordExportViewImpl extends AbstractView implements RecordExportView {
    private static Logger log = LoggerFactory.getLogger(RecordExportViewImpl.class.getName());

    private List<RecordExportType> recordTypes = null;
    private Long type = null;
    private RecordExportType exportType = null;

    private List<RecordExport> availableExports = null;
    private Integer availableCount = null;

    private List<RecordDisplay> unexported = new ArrayList<RecordDisplay>();
    private List<RecordDisplay> selected = new ArrayList<RecordDisplay>();

    private boolean selectedDisplayMode = false;
    private boolean unexportedDisplayMode = false;

    private boolean archiveAvailable = false;
    private long archiveCount = 0;
    private boolean archiveMode = false;
    private boolean typeSelectionMode = false;
    private boolean voucherExportSortReverse = true;

    private boolean exportConfirmationMode = false;

    protected RecordExportViewImpl() {
        super();
        typeSelectionMode = true;
    }

    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        super.init(request);
        if (getSystemProperty(SystemConfig.VOUCHER_EXPORT_SORT_REVERSE) != null) {
            voucherExportSortReverse = isSystemPropertyEnabled(SystemConfig.VOUCHER_EXPORT_SORT_REVERSE);
        }
    }

    public void load(Long type, Long id) {
        setType(type);
        try {
            initArchive();
            selectAvailable(id);
        } catch (Exception e) {
            log.warn("load() failed [type=" + type + ", id=" + id + ", message=" + e.getMessage() + "]");
        }
    }

    @Override
    public void save(Form form) throws ClientException, PermissionException, TimeoutException {
        RecordExportManager recordExportManager = getRecordExportManager();
        Date start = form.getDate("start"); 
        Date end = form.getDate("end");
        int expcount = 0;
        if (start != null && end == null) {
            end = new Date(System.currentTimeMillis());
        }
        if (start == null && end != null) {
            start = new Date(0); // TODO use start date of last exported record  
        }
        if (start != null && end != null) {
            if (end.before(start)) {
                throw new ClientException(ErrorCode.DATE_START_AFTER_STOP);
            }
            expcount = recordExportManager.createExport(type, getDomainUser().getEmployee().getId(), start, end);
            if (log.isDebugEnabled()) {
                log.debug("save() export created [count=" + expcount + ", start=" + start + ", stop=" + end + "]");
            }
        } else if (!selected.isEmpty()) {
            expcount = recordExportManager.createExport(type, getDomainUser().getEmployee().getId(), selected);
            if (log.isDebugEnabled()) {
                log.debug("save() export created [count=" + expcount + ", selected=" + selected.size() + "]");
            }
            selected = new ArrayList<RecordDisplay>();
        } else {
            expcount = recordExportManager.createExport(type, getDomainUser().getEmployee().getId());
            if (log.isDebugEnabled()) {
                log.debug("save() export created [count=" + expcount + "]");
            }
        }
        RecordExport export = recordExportManager.getLatest(type);
        if (export != null) {
            setBean(export);
            archiveMode = false;
            recordTypes = recordExportManager.getExportableTypes();
            exportType = (RecordExportType) CollectionUtil.getByProperty(getRecordTypes(), "type", type);
            archiveCount = (exportType != null ? exportType.getArchived() : 0);
            archiveAvailable = (archiveCount > 0);
            
            availableCount = recordExportManager.getCount(type);
            if (log.isDebugEnabled()) {
                log.debug("save() done [id=" + export.getId() + "]");
            }
        }
        exportConfirmationMode = false;
    }

    /**
     * Provides the available record types for exports
     * @return recordTypes
     */
    public List<RecordExportType> getRecordTypes() {
        if (recordTypes == null) {
            loadTypes();
        }
        return recordTypes;
    }

    private void loadTypes() {
        recordTypes = getRecordExportManager().getExportableTypes();
    }

    public boolean isArchiveAvailable() {
        return archiveAvailable;
    }

    protected void setArchiveAvailable(boolean archiveAvailable) {
        this.archiveAvailable = archiveAvailable;
    }

    public long getArchiveCount() {
        return archiveCount;
    }

    protected void setArchiveCount(long archiveCount) {
        this.archiveCount = archiveCount;
    }

    public boolean isArchiveMode() {
        return archiveMode;
    }

    public boolean isTypeSelectionMode() {
        return typeSelectionMode;
    }

    public RecordExportType getExportType() {
        return exportType;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        if (type == null) {
            this.type = null;
            availableCount = null;
            archiveCount = 0;
            archiveAvailable = false;
            exportType = null;
            unexported = new ArrayList<RecordDisplay>();
            selected = new ArrayList<RecordDisplay>();
            typeSelectionMode = true;
        } else {
            this.type = type;
            exportType = (RecordExportType) CollectionUtil.getByProperty(getRecordTypes(), "type", type);
            archiveCount = (exportType != null ? exportType.getArchived() : 0);
            archiveAvailable = (archiveCount > 0);

            availableCount = getRecordExportManager().getCount(type);
            unexported = getRecordExportManager().getRecords(type, voucherExportSortReverse);
            if (exportType.isPayment()) {
                Records.sortDisplay(unexported, Records.ORDER_BY_CREATED, voucherExportSortReverse);
            }
            selected = new ArrayList<RecordDisplay>();
            typeSelectionMode = false;
        }
        exportConfirmationMode = false;
    }

    public boolean isCancellationSelected() {
        return RecordType.SALES_CANCELLATION.equals(type);
    }

    public boolean isCreditNoteSelected() {
        return RecordType.SALES_CREDIT_NOTE.equals(type);
    }

    public boolean isDownpaymentSelected() {
        return RecordType.SALES_DOWNPAYMENT.equals(type);
    }

    public boolean isInvoiceSelected() {
        return (RecordType.SALES_DOWNPAYMENT.equals(type)
        || RecordType.SALES_INVOICE.equals(type));
    }

    public boolean isPurchaseInvoiceSelected() {
        return RecordType.PURCHASE_INVOICE.equals(type);
    }

    public boolean isPurchasePaymentSelected() {
        return RecordType.PURCHASE_PAYMENT.equals(type);
    }

    public boolean isSalesPaymentSelected() {
        return RecordType.SALES_PAYMENT.equals(type);
    }

    public boolean isExportConfirmationMode() {
        return exportConfirmationMode;
    }

    protected void setExportConfirmationMode(boolean exportConfirmationMode) {
        this.exportConfirmationMode = exportConfirmationMode;
    }

    public Integer getAvailableCount() {
        return availableCount;
    }

    public List<RecordExport> getAvailableExports() {
        return availableExports;
    }

    public List<RecordDisplay> getUnexported() {
        return unexported;
    }

    public List<RecordDisplay> getSelected() {
        return selected;
    }

    public boolean isSelectedDisplayMode() {
        return selectedDisplayMode;
    }

    public void setSelectedDisplayMode(boolean selectedDisplayMode) {
        this.selectedDisplayMode = selectedDisplayMode;
        exportConfirmationMode = false;        
    }

    public boolean isUnexportedDisplayMode() {
        return unexportedDisplayMode;
    }

    public void setUnexportedDisplayMode(boolean unexportedDisplayMode) {
        this.unexportedDisplayMode = unexportedDisplayMode;
        exportConfirmationMode = false;
    }

    public void selectUnexported(Form form) {
        List<RecordDisplay> selectedList = selectedDisplayMode ? selected : unexported;
        List<RecordDisplay> targetList = selectedDisplayMode ? unexported : selected;
        Long[] ids = form.getIndexedLong("recordId");

        if (ids == null || ids.length == 0) {
            log.debug("selectUnexported() no ids found");
            Long recordId = form.getLong("recordId");
            if (recordId != null) {
                for (Iterator<RecordDisplay> iterator = selectedList.iterator(); iterator.hasNext();) {
                    RecordDisplay next = iterator.next();
                    if (next.getId().equals(recordId)) {
                        if (log.isDebugEnabled()) {
                            log.debug("selectUnexported() record found [source=" + (selectedDisplayMode ? "selected" : "unexported") 
                                    + ", id=" + next.getId() + "]");
                        }
                        targetList.add(next);
                        iterator.remove();
                        break;
                    }
                }
            }
        } else {
            log.debug("selectUnexported() ids found [count=" + ids.length + "]");
            for (int i = 0, j = ids.length; i < j; i++) {
                Long id = ids[i];
                for (Iterator<RecordDisplay> iterator = selectedList.iterator(); iterator.hasNext();) {
                    RecordDisplay next = iterator.next();
                    if (next.getId().equals(id)) {
                        targetList.add(next);
                        iterator.remove();
                        break;
                    }
                }
            }
        }
        CollectionUtil.sort(targetList, RecordComparators.createRecordDisplayByCreatedComparator(voucherExportSortReverse));
    }

    public void createExport() throws PermissionException {
        if (type == null) {
            log.error("createExport() invoked without type selection");
            throw new ActionException("type must not be null");
        }
        if (!exportConfirmationMode) {
            exportConfirmationMode = true;
        }
    }

    public void initArchive() throws EmptyListException {
        if (type == null) {
            log.error("initArchive() invoked without type selection");
            throw new ActionException("type must not be null");
        }
        RecordExportManager recordExportManager = getRecordExportManager();
        List<RecordExport> exports = recordExportManager.listByType(type);
        if (exports.isEmpty()) {
            throw new EmptyListException();
        }
        availableExports = exports;
        archiveMode = true;
        exportConfirmationMode = false;
    }

    public void removeArchive() {
        archiveMode = false;
        availableExports = null;
        setBean(null);
    }

    public void selectAvailable(Long id) {
        if (id == null) {
            setBean(null);
        } else {
            for (int i = 0, j = availableExports.size(); i < j; i++) {
                RecordExport export = availableExports.get(i);
                if (export.getId().equals(id)) {
                    if (export.getRecords().isEmpty()) {
                        getRecordExportManager().addRecords(export);
                    }
                    setBean(export);
                    break;
                }
            }
        }
        exportConfirmationMode = false;
    }

    public String getXls() {
        RecordExportManager recordExportManager = getRecordExportManager();
        return recordExportManager.createXls(getDomainUser().getEmployee(), (RecordExport) getBean());
    }

    public Document getXml() throws ClientException, PermissionException {
        RecordExportManager recordExportManager = getRecordExportManager();
        return recordExportManager.createXml(getDomainUser().getEmployee(), (RecordExport) getBean());
    }

    public byte[] getPdf() throws ClientException, PermissionException {
        RecordExportManager recordExportManager = getRecordExportManager();
        return recordExportManager.createPdf(getDomainUser().getEmployee(), (RecordExport) getBean());
    }

    public DocumentData getZip() throws ClientException {
        RecordExportManager recordExportManager = getRecordExportManager();
        return recordExportManager.createZip((RecordExport) getBean());
    }

    private RecordExportManager getRecordExportManager() {
        return (RecordExportManager) getService(RecordExportManager.class.getName());
    }

    protected DmsDocument getReferencedDocument(RecordDisplay record) {
        DmsDocument result = null;
        if (exportType != null && exportType.getDocumentType() != null) {
            result = getDmsManager().findReferenced(new DmsReference(exportType.getDocumentType(), record.getId()));
            log.debug("getReferencedDocument() lookup done [found=" + (result != null) + "]");
        }
        return result;
    }

    private final DmsManager getDmsManager() {
        return (DmsManager) getService(DmsManager.class.getName());
    }
}
