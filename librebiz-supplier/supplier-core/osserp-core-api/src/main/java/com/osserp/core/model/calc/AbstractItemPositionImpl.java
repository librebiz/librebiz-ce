/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 14, 2007 12:45:09 PM 
 * 
 */
package com.osserp.core.model.calc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.osserp.common.beans.OptionImpl;

import com.osserp.core.Item;
import com.osserp.core.ItemPosition;
import com.osserp.core.products.Product;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractItemPositionImpl extends OptionImpl
        implements ItemPosition {
    private Long groupId = null;
    private boolean discounts = false;
    private List<Item> items = new ArrayList<Item>();
    private boolean partlist = false;
    private Long partlistId = null;
    private boolean partlistsAvailable = false;
    private boolean option = false;

    /**
     * Default constructor to implement java.io.Serializable interface
     */
    protected AbstractItemPositionImpl() {
        super();
    }

    /**
     * Constructor for creating new persistent objects
     * @param name
     * @param reference
     * @param groupId
     * @param discounts
     * @param option
     */
    protected AbstractItemPositionImpl(String name, Long reference, Long groupId, boolean discounts, boolean option) {
        super(null, reference, name);
        this.groupId = groupId;
        this.discounts = discounts;
        this.option = option;
    }

    public Long getGroupId() {
        return groupId;
    }

    protected void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public boolean isDiscounts() {
        return discounts;
    }

    public void setDiscounts(boolean discounts) {
        this.discounts = discounts;
    }

    public boolean isOption() {
        return option;
    }

    public void setOption(boolean option) {
        this.option = option;
    }

    public boolean isPartlist() {
        return partlist;
    }

    public void setPartlist(boolean partlist) {
        this.partlist = partlist;
    }

    public Long getPartlistId() {
        return partlistId;
    }

    public void setPartlistId(Long partlistId) {
        this.partlistId = partlistId;
    }

    protected void addItem(Item item) {
        this.items.add(item);
    }

    public void removeItem(Long productId) {
        for (Iterator<Item> i = items.iterator(); i.hasNext();) {
            Item item = i.next();
            if (item.getProduct().getProductId().equals(productId)) {
                i.remove();
                break;
            }
        }
    }

    public Item getItem(Long id) {
        for (int i = 0, j = items.size(); i < j; i++) {
            Item next = items.get(i);
            if (next.getId().equals(id)) {
                return next;
            }
        }
        return null;
    }

    public List<Item> getItems() {
        return items;
    }

    /**
     * Sets the items related to this item position
     * @param items
     */
    protected void setItems(List<Item> items) {
        this.items = items;
    }

    /**
     * Adds an item
     * @param product
     * @param customName
     * @param quantity
     * @param price
     * @param priceDate
     * @param priceOverridden
     * @param partnerPrice
     * @param partnerPriceEditable
     * @param partnerPriceOverridden might be true if method is invoked by a copying service
     * @param purchasePrice
     * @param taxRate
     * @param note
     * @param includePrice
     * @param externalId
     */
    public abstract void addItem(
            Product product,
            String customName, 
            Double quantity,
            BigDecimal price,
            Date priceDate,
            boolean priceOverridden,
            BigDecimal partnerPrice,
            boolean partnerPriceEditable,
            boolean partnerPriceOverridden,
            BigDecimal purchasePrice,
            Double taxRate,
            String note,
            boolean includePrice,
            Long externalId);

    public Map<Long, Item> getProductMap() {
        Map<Long, Item> productMap = new HashMap<Long, Item>();
        for (int i = 0, j = items.size(); i < j; i++) {
            Item next = items.get(i);
            productMap.put(next.getProduct().getProductId(), next);
        }
        return productMap;
    }

    public void setPartlistsAvailable(boolean partlistsAvailable) {
        this.partlistsAvailable = partlistsAvailable;
    }

    public boolean isPartlistsAvailable() {
        return partlistsAvailable;
    }
}
