/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 20, 2004 
 * 
 */
package com.osserp.common;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Locale;

import com.osserp.common.util.DateUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
//TODO check if required, replace with dynamic solution
public class Constants {

    /* Application usage and sales entries starting date */
    public static final Date APPLICATION_ACTIVATION = DateUtil.getDate("01.01.2000");

    public static final Long HEADQUARTER = 1L;
    public static final Long GERMANY = 53L;
    public static final String DEFAULT_CLIENT_NAME = "domain0";

    public static final String GERMAN_PREFIX = "49";
    public static final Long COMPANY_SALUTATION = 3L;
    public static final Integer INT_NULL = 0;
    public static final BigDecimal BIG_DECIMAL_NULL = new BigDecimal(0);
    public static final Double DOUBLE_NULL = 0d;
    public static final int DEFAULT_DELIVERY_DAYS = 3;
    public static final String DEFAULT_TICKET_ORIGINATOR = "noreply@osserp.com";
    public static final String DEFAULT_SUPPORT_ADDRESS = "support@osserp.com";
    public static final String MIME_TYPE_DEFAULT = "application/octet-stream";
    public static final String MIME_TYPE_PDF = "application/pdf";
    public static final String BLANK = " ";
    public static final Long LONG_NULL = 0L;
    public static final String SETUP_CLOSED_STATUS = "ok";
    public static final Long SYSTEM_NOBODY = 65534L;
    public static final Long SYSTEM_EMPLOYEE = 6000L;
    public static final Long SYSTEM_USER = 6000L;

    public static final Long CURRENCY_EUR = 1L;
    public static final Long CURRENCY_USD = 2L;
    public static final Long CURRENCY_GBP = 3L;

    public static final String DEFAULT_COUNTRY = "DE";
    public static final Long DEFAULT_CURRENCY = CURRENCY_EUR;
    public static final String DEFAULT_LANGUAGE = "de";
    public static final Locale DEFAULT_LOCALE_OBJECT = new Locale(Constants.DEFAULT_LANGUAGE, Constants.DEFAULT_COUNTRY);
    public static final Long DEFAULT_QUANTITY_UNIT = 1L;
    public static final String DEFAULT_DMS_AUTH_SECRET = "YOUR_DMS_AUTH_SECRET";

    //salutations
    public static final String SALUTATION_MR_LOWER = "salutations.letter.mr.secondPerson";
    public static final String SALUTATION_MRS_LOWER = "salutations.letter.mrs.secondPerson";
    public static final String SALUTATION_MR = "salutations.letter.mr";
    public static final String SALUTATION_MRS = "salutations.letter.mrs";
    public static final String SALUTATION_MR_PARTNER = "salutations.letter.mr.partner";
    public static final String SALUTATION_MRS_PARTNER = "salutations.letter.mrs.partner";

    // search methods
    public static final String SEARCH_BY_CITY = "byCity";
    public static final String SEARCH_BY_CUSTOMER_ID = "byCustomerId";
    public static final String SEARCH_BY_DESCRIPTION = "byDescription";
    public static final String SEARCH_BY_EMAIL = "byEmail";
    public static final String SEARCH_BY_FIRSTNAME = "byFirstName";
    public static final String SEARCH_BY_ID = "byId";
    public static final String SEARCH_BY_LASTNAME = "byLastName";
    public static final String SEARCH_BY_MATCHCODE = "byMatchcode";
    public static final String SEARCH_BY_NAME = "byName";
    public static final String SEARCH_BY_PRODUCT_ID = "byProductId";
    public static final String SEARCH_BY_PRODUCT_NAME = "byProductName";
    public static final String SEARCH_BY_PROJECT_ID = "byProjectId";
    public static final String SEARCH_BY_PROJECT_NAME = "byProjectName";
    public static final String SEARCH_BY_REQUEST_NAME = "byRequestName";
    public static final String SEARCH_BY_STREET = "byStreet";
    public static final String SEARCH_BY_ZIPCODE = "byZipcode";
    // telephone system
    public static final String PHONE_UID_PREFIX = "phone";
    public static final String MACADDRESS_REGEX = "[0-9a-fA-F]{12}";

    public static final String RESOURCE_NOT_AVAILABLE_MESSAGE =
            "Required resources not available. Application logs may provide more details...";

    public static Locale locale(String country) {
        if (country == null || country.length() < 2
                || DEFAULT_LANGUAGE.equalsIgnoreCase(country)) {
            return DEFAULT_LOCALE_OBJECT;
        }
        return new Locale(country);
    }

    public static String getDefaultCurrency(Long id) {
        return CURRENCY_USD.equals(id) ? "USD" :
            (CURRENCY_GBP.equals(id) ? "GBP" : "EUR");
    }

    public static String getCurrencySymbol(Long id) {
        return CURRENCY_USD.equals(id) ? "$" :
            (CURRENCY_GBP.equals(id) ? "&pound;" : "&euro;");
    }
}
