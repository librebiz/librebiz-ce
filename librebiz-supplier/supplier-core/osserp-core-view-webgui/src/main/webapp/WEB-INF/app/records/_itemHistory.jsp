<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>

<c:set var="view" scope="page" value="${sessionScope.itemHistoryView}"/>
<c:set var="list" scope="page" value="${view.list}"/>

<div>
	<span style="padding-left:15px;"><strong><fmt:message key="itemChangedHistory"/></strong></span>
	<table class="table table-striped">
		<thead>
			<tr>
				<th><fmt:message key="atLabel"/></th>
				<th><fmt:message key="by"/></th>
				<th><fmt:message key="number"/></th>
				<th><fmt:message key="productName"/></th>
				<th style="text-align: right;"><fmt:message key="quantity"/></th>
				<th style="text-align: right;"><fmt:message key="new"/></th>
				<th style="text-align: right;"><fmt:message key="price"/></th>
				<th style="text-align: right;"><fmt:message key="new"/></th>
			</tr>
		</thead>
		<tbody>
			<c:choose>
				<c:when test="${empty list}">
					<tr>
						<td colspan="4"><fmt:message key="noChangeFound"/></td>
					</tr>
				</c:when>
				<c:otherwise>
					<c:forEach var="obj" items="${list}">
						<tr>
							<td style="text-align: right;"><fmt:formatDate value="${obj.created}"/></td>
							<td><span title="<oc:employee value="${obj.createdBy}"/>"><oc:employee initials="true" value="${obj.createdBy}" /></span></td>
							<td><o:out value="${obj.product.productId}"/></td>
							<td><o:out value="${obj.product.name}"/></td>
							<td style="text-align: right;"><o:number value="${obj.previousQuantity}" format="decimal"/></td>
							<td style="text-align: right;"><o:number value="${obj.newQuantity}" format="decimal"/></td>
							<td style="text-align: right;"><o:number value="${obj.previousPrice}" format="currency"/></td>
							<td style="text-align: right;"><o:number value="${obj.newPrice}" format="currency"/></td>
						</tr>
					</c:forEach>
				</c:otherwise>
			</c:choose>
		</tbody>
	</table>
</div>
