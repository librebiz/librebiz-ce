/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jul 31, 2007 4:24:24 PM 
 * 
 */
package com.osserp.gui.web.struts;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionException;
import com.osserp.common.PersistentObject;
import com.osserp.common.web.Form;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.View;
import com.osserp.common.web.ViewManager;
import com.osserp.gui.client.Actions;
import com.osserp.gui.client.QueryView;
import com.osserp.gui.client.ServiceManager;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractViewDispatcherAction extends AbstractViewAction {
    private static Logger log = LoggerFactory.getLogger(AbstractViewDispatcherAction.class.getName());

    /**
     * Provides the view that implementing action handles
     * @return viewClass
     */
    protected abstract Class<? extends View> getViewClass();

    /**
     * Creates a new view and forwards to action target which is 'success' by default. If view already exists and implementing view has
     * 'keepViewWhenExistsOnCreate' set, no view will be created.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward forward(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("forward() request from " + getUser(session).getId());
        }
        View existing = fetchView(session);
        Long id = RequestUtil.fetchId(request);
        if (existing != null && existing.getBean() instanceof PersistentObject
                && id != null && id.equals(((PersistentObject) existing.getBean()).getPrimaryKey())) {
            if (log.isDebugEnabled()) {
                log.debug("forward() found existing view with matching bean [name="
                        + existing.getName() + ", primaryKey=" + id + "]");
            }
            existing.reload();
            return mapping.findForward(existing.getActionTarget());
        }
        if (existing != null && existing.isKeepViewWhenExistsOnCreate()) {
            if (log.isDebugEnabled()) {
                log.debug("forward() found existing view, should be kept [name="
                        + existing.getName() + "]");
            }
            if (existing.isReloadWhenExistsOnCreate()) {
                existing.reload();
            }
            return mapping.findForward(existing.getActionTarget());
        }
        View view = createView(request);
        String exitTarget = RequestUtil.getExit(request);
        if (exitTarget != null) {
            if (log.isDebugEnabled()) {
                log.debug("forward() found exit target [name="
                        + exitTarget + "]");
            }
            view.setExitTarget(exitTarget);
        } /*
           * else if (existing != null && existing.getExitTarget() != null) { StringBuilder b = new StringBuilder("forward() overriding exit target " +
           * "with action target from an existing view:\n\n"); b .append(WebUtil.createRequestURIDump(request))
           * .append("\nexistingViewName=").append(existing.getName()) .append("\nexistingViewClass=").append(existing.getClass().getName())
           * .append("\nexistingViewActionTarget=").append(existing.getActionTarget()) .append("\nexistingViewExitTarget=").append(existing.getExitTarget())
           * .append("\ncreatedViewName=").append(view.getName()) .append("\ncreatedViewClass=").append(view.getClass().getName())
           * .append("\nexistingViewActionTarget=").append(existing.getActionTarget()) .append("\ncreatedViewExitTarget=").append(view.getExitTarget())
           * .append("\n\nrequest dump:\n") .append(WebUtil.createRequestDump(request)) .append("\n\nsession dump:\n")
           * .append(WebUtil.createSessionDump(session)); StringBuilder c = new StringBuilder("AbstractViewDispatcherAction."); String s = b.toString();
           * log.warn(s); c.append(s); this.sendDebug(c.toString()); view.setExitTarget(existing.getActionTarget()); }
           */
        String target = RequestUtil.getTarget(request);
        if (target != null) {
            view.setActionTarget(target);
        }
        String selectionTarget = RequestUtil.getString(request, "selectionTarget");
        if (selectionTarget != null) {
            view.setSelectionTarget(selectionTarget);
        }
        boolean enableSelectionView = RequestUtil.getBoolean(request, "enableSelectionView");
        if (enableSelectionView) {
            view.enableSelectionView();
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Destroys the associated view. Forward goes to exit target of the view or 'exit' target if view doesn't provide an explicit exit target.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward exit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exit() request from " + getUser(session).getId());
        }
        String exitId = null;
        String exitTarget = Actions.EXIT;
        View view = fetchView(session);
        if (view != null) {
            if (view.isExitTargetAvailable()) {
                exitTarget = view.getExitTarget();
            }
            exitId = view.getExitId();
            if (isSet(exitId)) {
                RequestUtil.saveExitId(request, exitId);
            }
            removeView(session);
        } else if (log.isDebugEnabled()) {
            log.debug("exit() no view found, nothing to remove [action="
                    + getClass().getName() + ", view="
                    + (getViewClass() == null ? "null" : getViewClass().getName())
                    + "]");
        }
        if (log.isDebugEnabled()) {
            log.debug("exit() done [target=" + exitTarget + ", exitId=" + exitId + "]");
        }
        return mapping.findForward(exitTarget);
    }

    /**
     * Checks if view exists and forwards to views action target. An error message about an invalid context will be saved in session scope if view not exists.
     * The action goes back where we come from in such case.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward redisplay(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("redisplay() request from " + getUser(session).getId());
        }
        View view = fetchView(session);
        if (view == null) {
            log.warn("redisplay() failed while object not bound [view=" + getViewClass().getName() + "]");
            return saveErrorAndReturn(request, ErrorCode.INVALID_CONTEXT);
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Enables create mode on current view
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward enableCreate(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableCreate() request from " + getUser(session).getId());
        }
        View view = fetchView(session);
        if (view == null) {
            view = createView(request);
        }
        if (view.getBean() != null) {
            view.setSelection(null);
        }
        view.setCreateMode(true);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Disables create mode on current view
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward disableCreate(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disableCreate() request from " + getUser(session).getId());
        }
        View view = getView(session);
        view.setCreateMode(false);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Enables edit mode on current view
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward enableEdit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableEdit() request from " + getUser(session).getId());
        }
        View view = getView(session);
        Long id = RequestUtil.getLong(request, "id", false);
        if (id != null) {
            view.setSelection(id);
        }
        try {
            view.setEditMode(true);
        } catch (PermissionException e) {
            RequestUtil.saveError(request, e.getMessage());
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Disables edit mode on current view
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward disableEdit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disableEdit() request from " + getUser(session).getId());
        }
        View view = getView(session);
        view.setEditMode(false);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Enables setup mode on current view
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward enableSetup(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableSetup() request from " + getUser(session).getId());
        }
        View view = getView(session);
        view.setSetupMode(true);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Disables setup mode on current view
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward disableSetup(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disableSetup() request from " + getUser(session).getId());
        }
        View view = getView(session);
        view.setSetupMode(false);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Toggles setup mode between enabled and disabled depending on current state
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward toggleSetup(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("toggleSetup() request from " + getUser(session).getId());
        }
        View view = getView(session);
        view.setSetupMode(!view.isSetupMode());
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Invokes save on current view with values of given form
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward save(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("save() request from " + getUser(session).getId());
        }
        View view = getView(session);
        try {
            Form f = createForm(form, request);
            view.save(f);
            return mapping.findForward(view.getActionTarget());

        } catch (ClientException c) {
            if (log.isDebugEnabled()) {
                log.debug("save() caught client exception [message=" + c.getMessage() + "]");
            }
            return saveErrorAndReturn(request, c.getMessage());

        } catch (PermissionException p) {
            if (log.isDebugEnabled()) {
                log.debug("save() caught permission exception [message=" + p.getMessage() + "]");
            }
            return saveErrorAndReturn(request, p.getMessage());
        }
    }

    /**
     * Invokes select on current view
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward select(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("select() request from " + getUser(session).getId());
        }
        View view = getView(session);
        Long id = RequestUtil.fetchId(request);
        view.setSelection(id);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Enables views list selection mode, forwards to views list target wich provides 'list' as default
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward enableListSelection(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableListSelection() request from " + getUser(session).getId());
        }
        View view = getView(session);
        view.enableListSelectionMode();
        return mapping.findForward(view.getListTarget());
    }

    /**
     * Disables views list selection mode, forwards to views list target wich provides 'list' as default
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward disableListSelection(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disableListSelection() request from " + getUser(session).getId());
        }
        View view = getView(session);
        view.enableListSelectionMode();
        return mapping.findForward(view.getListTarget());
    }

    /**
     * Increases current list start and end index.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward nextList(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("nextList() request from " + getUser(session).getId());
        }
        View view = getView(session);
        view.nextList();
        return mapping.findForward(view.getListTarget());
    }

    /**
     * Decreases current list start and end index.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward lastList(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("lastList() request from " + getUser(session).getId());
        }
        View view = getView(session);
        view.lastList();
        return mapping.findForward(view.getListTarget());
    }

    /**
     * Sorts current list by list item property provided as request param 'propertyName' if view is instance of QueryView or 'order' if view is instance of
     * View.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward reload(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("reload() request from " + getUser(session).getId());
        }
        View view = getView(session);
        view.reload();
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Sorts current list by list item property provided as request param 'propertyName' if view is instance of QueryView or 'order' if view is instance of
     * View.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward sort(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("sort() request from " + getUser(session).getId());
        }
        View view = getView(session);
        if (view instanceof QueryView) {
            QueryView sortableListView = (QueryView) view;
            sortableListView.sort(RequestUtil.getString(request, "propertyName"));
        } else {
            String method = RequestUtil.getString(request, "order");
            if (method != null) {
                view.sortList(method);
            }
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Tries to fetch associated view from session scope
     * @param session
     * @return view or null if not exists
     */
    protected View fetchView(HttpSession session) {
        View view = ViewManager.fetchView(session, getViewClass());
        return view;
    }

    /**
     * Fetches associated view from session scope. Expects that view exists.
     * @param session
     * @return view
     * @throws ActionException if no such view exists
     */
    protected View getView(HttpSession session) throws ActionException {
        View view = fetchView(session);
        if (view == null) {
            throw new ActionException(ActionException.NO_VIEW_BOUND, getViewClass().getName());
        }
        return view;
    }

    /**
     * Creates the associated view
     * @param request
     * @return new created view
     * @throws ClientException if creation failed while view manager invokes views init method
     * @throws PermissionException if current user has no permission to create views of implementing type
     */
    protected View createView(HttpServletRequest request) throws ClientException, PermissionException {
        ViewManager crt = ViewManager.newInstance(servlet.getServletContext());
        return crt.createView(request, getViewClass().getName());
    }

    /**
     * Removes associated view from session context
     * @param session
     */
    protected void removeView(HttpSession session) {
        ServiceManager.removeView(session, getViewClass());
    }
}
