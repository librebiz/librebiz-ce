/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 20, 2007 9:17:14 AM 
 * 
 */
package com.osserp.core.model.calc;

import java.math.BigDecimal;
import java.util.Date;

import com.osserp.common.beans.AbstractOption;
import com.osserp.core.ItemListDisplay;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractItemListDisplay extends AbstractOption implements ItemListDisplay {
    private Integer number = 1;
    private BigDecimal price = null;
    private String contextName = null;

    protected AbstractItemListDisplay() {
        super();
    }

    /**
     * Constructor used by derived classes
     * @param reference
     * @param contextName
     * @param name
     * @param number
     * @param createdBy
     */
    AbstractItemListDisplay(
            Long reference,
            String contextName,
            String name,
            Integer number,
            Long createdBy) {
        super((Long) null, reference, name, (Date) null, createdBy);
        this.contextName = contextName;
        this.number = number;
    }

    /**
     * Constructor for row mappers
     * @param id
     * @param name
     * @param reference
     * @param contextName
     * @param number
     * @param created
     * @param createdBy
     * @param changed
     * @param changedBy
     * @param price
     */
    public AbstractItemListDisplay(
            Long id,
            String name,
            Long reference,
            String contextName,
            Integer number,
            Date created,
            Long createdBy,
            Date changed,
            Long changedBy,
            BigDecimal price) {
        super(id, reference, name, created, createdBy);
        setChanged(changed);
        setChangedBy(changedBy);
        this.contextName = contextName;
        this.number = number;
        this.price = price;
    }

    public String getContextName() {
        return contextName;
    }

    public void setContextName(String contextName) {
        this.contextName = contextName;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public BigDecimal getPrice() {
        return price;
    }

    protected void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public Object clone() {
        return null;
    }
}
