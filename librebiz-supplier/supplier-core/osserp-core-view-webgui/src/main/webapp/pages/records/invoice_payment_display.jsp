<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<c:set var="view" value="${sessionScope.recordView}"/>
<c:set var="record" value="${view.record}"/>
<div class="recordLabelValue">
    <fmt:message key="payment"/>: <oc:options name="recordPaymentConditions" value="${record.paymentCondition.id}"/>
</div>
<c:if test="${!empty record.paymentNote}">
<div class="recordLabelValue">
    <fmt:message key="amendmentLabel"/>: <o:textOut value="${record.paymentNote}"/>
</div>
</c:if>
