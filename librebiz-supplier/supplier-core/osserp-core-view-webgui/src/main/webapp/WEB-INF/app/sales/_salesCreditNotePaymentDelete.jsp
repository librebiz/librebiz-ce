<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="invoiceNumberShort" />
                <o:out value="${view.record.number}" />
                <span> / </span>
                <o:date value="${view.record.created}" />
                <span> - </span>
                <fmt:message key="paymentDelete" />
            </h4>
        </div>
    </div>
    <div class="panel-body">

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for=""> <fmt:message key="payment" />
                    </label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <ul>
                        <c:forEach var="payment" items="${view.record.payments}" varStatus="s">
                            <v:link url="${view.baseLink}/deletePayment?id=${payment.id}" title="paymentDelete" confirm="true" message="confirmPaymentDelete">
                                <li><o:number value="${payment.amount}" format="currency" /> <fmt:message key="from" /> <o:date value="${payment.paid}" /></li>
                            </v:link>
                        </c:forEach>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
