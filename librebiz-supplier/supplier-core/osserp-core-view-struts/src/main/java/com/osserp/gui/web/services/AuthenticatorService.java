/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 30, 2007 2:45:16 AM 
 * 
 */
package com.osserp.gui.web.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.User;
import com.osserp.common.UserAuthenticator;
import com.osserp.common.service.Locator;

import com.osserp.core.service.impl.AbstractWebService;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class AuthenticatorService extends AbstractWebService implements UserAuthenticator {
    private static Logger log = LoggerFactory.getLogger(AuthenticatorService.class.getName());

    public AuthenticatorService(Locator locator) {
        super(locator);
    }

    public User authenticate(String name, String remoteHost) throws PermissionException {
        if (log.isDebugEnabled()) {
            log.debug("authenticate() invoked [name=" + name + ", remoteHost=" + remoteHost + "]");
        }
        try {
            return getValues(getUserAuthenticator().authenticate(name, remoteHost));
        } catch (PermissionException e) {
            throw e;
        }
    }

    public User authenticate(String name, String password, String remoteHost) throws PermissionException {
        if (log.isDebugEnabled()) {
            log.debug("authenticate() invoked [name=" + name + ", password=" + (password == null || password.length() < 1 ? "provided" : "missing")
                    + ", remoteHost=" + remoteHost + "]");
        }
        try {
            return getValues(getUserAuthenticator().authenticate(name, password, remoteHost));
        } catch (PermissionException e) {
            throw e;
        }
    }

    private User getValues(User obj) {
        try {
            User user = (User) obj.clone();
            // reset local startup target
            user.setStartup(null);
            return user;
        } catch (Exception other) {
            log.error("authenticate() failed for unknown reason [message=" + other.getMessage() + "]", other);
            throw new BackendException(other);
        }
    }

    public User changePassword(User user, String password) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("changePassword() invoked [message=not.supported]");
        }
        throw new UnsupportedOperationException();
    }

    public void activate(User user) {
        if (log.isDebugEnabled()) {
            log.debug("activate() invoked [message=not.supported]");
        }
        throw new UnsupportedOperationException();
    }

    public void deactivate(User user) {
        if (log.isDebugEnabled()) {
            log.debug("deactivate() invoked [message=not.supported]");
        }
        throw new UnsupportedOperationException();
    }

    private UserAuthenticator getUserAuthenticator() {
        return (UserAuthenticator) getService(UserAuthenticator.class.getName());
    }
}
