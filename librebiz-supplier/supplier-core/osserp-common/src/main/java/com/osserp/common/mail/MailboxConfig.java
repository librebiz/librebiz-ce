/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 31, 2008 3:18:05 PM 
 * 
 */
package com.osserp.common.mail;

import java.util.List;
import java.util.Properties;

import com.osserp.common.Option;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface MailboxConfig extends Option {

    static final String DEFAULT_IMAP_INBOX = "INBOX";
    static final String DEFAULT_IMAP_INBOX_ARCHIVE = "INBOX.Archive";
    static final String DEFAULT_IMAP_INBOX_DRAFTS = "INBOX.Drafts";
    static final String DEFAULT_IMAP_INBOX_FAILED = "INBOX.Failed";
    static final String DEFAULT_IMAP_INBOX_SENT = "INBOX.Sent";
    static final String DEFAULT_IMAP_INBOX_SPAM = "INBOX.SPAM";
    static final String DEFAULT_IMAP_INBOX_TRASH = "INBOX.Trash";

    static final String[] DEFAULT_FOLDERS = {
        DEFAULT_IMAP_INBOX,
        DEFAULT_IMAP_INBOX_ARCHIVE,
        DEFAULT_IMAP_INBOX_DRAFTS,
        DEFAULT_IMAP_INBOX_FAILED,
        DEFAULT_IMAP_INBOX_SENT,
        DEFAULT_IMAP_INBOX_SPAM,
        DEFAULT_IMAP_INBOX_TRASH
    };

    static final String DEFAULT_IMAP_SSL_PORT = "993";
    static final String DEFAULT_POP3_SSL_PORT = "995";
    static final String DEFAULT_PROTOCOL = "imap";

    /**
     * Provides the default email address of this account
     * @return defaultAlias
     */
    String getDefaultAlias();

    /**
     * Sets the default alias of this account
     * @param defaultAlias
     */
    void setDefaultAlias(String defaultAlias);

    /**
     * Provides a list of all account aliases (e.g. email addresses)
     * @return aliasList
     */
    List<String> getAliasList();

    /**
     * Adds a new email address
     * @param alias
     */
    void addAlias(String alias);

    /**
     * Removes an email address
     * @param alias
     */
    void removeAlias(String alias);

    /**
     * Provides the mailhost name
     * @return host
     */
    String getHost();

    /**
     * Sets the mailhost name
     * @param host
     */
    void setHost(String host);

    /**
     * Provides the port mailhost is listening, default 110
     * @return port
     */
    String getPort();

    /**
     * Sets the mailhost port
     * @param port
     */
    void setPort(String port);

    /**
     * Provides the mailbox user account name
     * @return user
     */
    String getUser();

    /**
     * Sets the mailbox user account name
     * @param user
     */
    void setUser(String user);

    /**
     * Provides the mailbox password
     * @return password
     */
    String getPassword();

    /**
     * Sets the mailbox password
     * @param password
     */
    void setPassword(String password);

    /**
     * Indicates that host uses authorization
     * @return auth
     */
    boolean isAuth();

    /**
     * Sets that mailserver uses authentication
     * @param auth
     */
    void setAuth(boolean auth);

    /**
     * Protocol according to JavaMail spec, e.g. imap/pop3
     * @return protocol
     */
    String getProtocol();

    /**
     * @param protocol
     */
    void setProtocol(String protocol);

    /**
     * Provides the store for attachments
     * @return attachmentsStore
     */
    String getAttachmentsStore();

    /**
     * @param attachmentsStore
     */
    void setAttachmentsStore(String attachmentsStore);

    /**
     * Provides the default folder config for this mailbox
     * @return imapFolders
     */
    String[] getImapFolders();

    /**
     * Provides current settings as required by JavaMail API
     * @return properties
     */
    Properties getMailProperties();


    String getInboxName();

    void setInboxName(String inboxName);

    String getInboxArchiveName();

    void setInboxArchiveName(String inboxArchiveName);

    String getInboxDraftsName();

    void setInboxDraftsName(String inboxDraftsName);

    String getInboxFailedName();

    void setInboxFailedName(String inboxFailedName);

    String getInboxSentName();

    void setInboxSentName(String inboxSentName);

    String getInboxSpamName();

    void setInboxSpamName(String inboxSpamName);

    String getInboxTrashName();

    void setInboxTrashName(String inboxTrashName);

}
