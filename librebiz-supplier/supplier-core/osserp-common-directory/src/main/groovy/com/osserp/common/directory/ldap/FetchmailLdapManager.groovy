/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.directory.ldap


/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
interface FetchmailLdapManager {

    /**
     * Finds users fetchmail account
     * @return fetchmailAccount or null if not exists
     */
    Map lookupFetchmailAccount(Map user)

    /**
     * Finds all available fetchmail accounts
     * @param client if client based dn required
     * @return available accounts
     */
    List<Map> lookupFetchmailAccounts(String client)

    /**
     * Finds all users without an -ldap- fetchmail account
     * @param client if client based dn required
     * @return users without fetchmail
     */
    List<Map> lookupUsersWithoutFetchmail(String client)

    /**
     * Creates a mailbox id suggestion
     * @param client if client based dn required
     * @return next available id
     */
    String createMboxIdSuggestion(String client)

    /**
     * Validates a mailbox id against a mailbox id pattern
     * @return true if id is a valid mailbox id
     */
    boolean validateMboxId(String mboxid)

    /**
     * Creates a new mail and optional fetchmail account.
     * @param user
     * @param mail address
     * @param fetchmailAccount
     * @param fetchmailPassword
     * @return user updated values
     * @throws LdapException if validation of params failed
     */
    Map createMbox(Map user, String mail, String fetchmailAccount, String fetchmailPassword) throws LdapException

    /**
     * Enables a fetchmail box (starts fetchmail to collect messages)
     * @param mbox
     * @return mbox with updated values
     */
    Map enableMbox(Map mbox)

    /**
     * Disables a fetchmail box (stops fetchmail to collect messages)
     * @param mbox
     * @return mbox with updated values
     */
    Map disableMbox(Map mbox)

    /**
     * Deletes a fetchmail box (removes ldap entry)
     * @param mbox
     */
    void deleteMbox(Map mbox)

    /**
     * Updates mbox values
     * @param mbox
     * @param values
     * @return updated mbox
     */
    Map updateMbox(Map mbox, Map values) throws LdapException
}
