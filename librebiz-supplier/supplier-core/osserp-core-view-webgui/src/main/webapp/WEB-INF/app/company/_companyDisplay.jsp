<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="company" value="${requestScope.companyDisplay}" />
<c:set var="companyMultiple" value="${requestScope.companyDisplayMultiple}" />

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <o:out value="${company.id}" />
                -
                <o:out value="${company.name}" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">

            <c:choose>
                <c:when test="${!empty view.companyLogo}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <c:choose>
                                    <c:when test="${companyEditable == 'true'}">
                                        <c:if test="${view.printConfigTargetAvailable}">
                                            <v:link url="/company/companyPrintConfig/forward?exit=/company/clientCompany/reload" title="companyPrintConfigView">
                                                <fmt:message key="printOptions" />
                                            </v:link>
                                            <br />
                                            <br />
                                        </c:if>
                                        <c:choose>
                                            <c:when test="${view.changeCompanyLogoTargetAvailable}">
                                                <v:link url="/company/clientLogo/forward?exit=/company/clientCompany/reload" title="changeLogo">
                                                    <fmt:message key="changeLogo" />
                                                </v:link>
                                            </c:when>
                                            <c:otherwise>
                                                <label for="logo"><fmt:message key="logo" /></label>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:when>
                                    <c:otherwise>
                                        <label for="logo"><fmt:message key="logo" /></label>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <img src="<oc:img value="${view.companyLogo}"/>" style="width: 50%;" />
                            </div>
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <c:if test="${view.printConfigTargetAvailable or view.changeCompanyLogoTargetAvailable}">
                        <c:choose>
                            <c:when test="${view.printConfigTargetAvailable}">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <v:link url="/company/companyPrintConfig/forward?exit=/company/clientCompany/reload" title="companyPrintConfigView">
                                                <fmt:message key="printOptions" />
                                            </v:link>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <fmt:message key="logo" />
                                            <span> - </span>
                                            <c:choose>
                                                <c:when test="${companyEditable == 'true'}">
                                                    <v:link url="/company/clientLogo/forward?exit=/company/clientCompany/reload" title="addLogo">
                                                        <fmt:message key="notAvailable" />
                                                    </v:link>
                                                </c:when>
                                                <c:otherwise>
                                                    <fmt:message key="notAvailable" />
                                                </c:otherwise>
                                            </c:choose>
                                        </div>
                                    </div>
                                </div>
                            </c:when>
                            <c:otherwise>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <fmt:message key="logo" />
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <c:choose>
                                                <c:when test="${companyEditable == 'true'}">
                                                    <v:link url="/company/clientLogo/forward?exit=/company/clientCompany/reload" title="addLogo">
                                                        <fmt:message key="notAvailable" />
                                                    </v:link>
                                                </c:when>
                                                <c:otherwise>
                                                    <fmt:message key="notAvailable" />
                                                </c:otherwise>
                                            </c:choose>
                                        </div>
                                    </div>
                                </div>
                            </c:otherwise>
                        </c:choose>
                    </c:if>
                </c:otherwise>
            </c:choose>

            <br />

            <c:if test="${company.contact.displayName != company.name}">
                <%-- company.name overrides company.contact.displayName --%>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="displayName"><fmt:message key="contact" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${company.contact.displayName}" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name"><fmt:message key="name" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${company.name}" />
                        </div>
                    </div>
                </div>
            </c:if>
            
            <c:if test="${!empty company.localCourt}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="localCourt"><fmt:message key="localCourt" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${company.localCourt}" />
                        </div>
                    </div>
                </div>
            </c:if>
            <c:if test="${!empty company.tradeRegisterEntry}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="tradeRegisterId"><fmt:message key="tradeRegisterId" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${company.tradeRegisterEntry}" />
                        </div>
                    </div>
                </div>
            </c:if>
            <c:if test="${!empty company.taxId}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="taxId"><fmt:message key="taxId" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${company.taxId}" />
                        </div>
                    </div>
                </div>
            </c:if>
            <c:if test="${!empty company.vatId}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="vayId"><fmt:message key="vatId" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${company.vatId}" />
                        </div>
                    </div>
                </div>
            </c:if>
            <c:if test="${companyMultiple}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="internalMargin"><fmt:message key="internalMargin" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${company.internalMargin}" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="holding"><fmt:message key="holding" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:choose>
                                <c:when test="${company.holding}">
                                    <fmt:message key="yes" />
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="no" />
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="services"><fmt:message key="services" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:choose>
                                <c:when test="${company.providingServices}">
                                    <fmt:message key="yes" />
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="no" />
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="partner"><fmt:message key="partner" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:choose>
                                <c:when test="${company.partner}">
                                    <fmt:message key="yes" />
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="no" />
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>
            </c:if>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="management"> <c:choose>
                                <c:when test="${companyEditable == 'true'}">
                                    <v:ajaxLink url="/company/managingDirectors/forward" linkId="managingDirectorsView">
                                        <fmt:message key="management" />
                                    </v:ajaxLink>
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="management" />
                                </c:otherwise>
                            </c:choose>
                        </label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:forEach var="md" items="${company.managingDirectors}" varStatus="status">
                            <c:if test="${status.index > 0}">
                                <br>
                            </c:if>
                            <o:out value="${md.name}" />
                        </c:forEach>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="bankAccounts"> <c:choose>
                                <c:when test="${companyEditable == 'true'}">
                                    <v:ajaxLink url="/company/bankAccounts/forward" linkId="bankAccountsView">
                                        <fmt:message key="bankAccountsLabel" />
                                    </v:ajaxLink>
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="bankAccountsLabel" />
                                </c:otherwise>
                            </c:choose>
                        </label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <ul>
                            <c:forEach var="ba" items="${company.bankAccounts}" varStatus="status">
                                <li><o:out value="${ba.shortkey}" /> - <o:out value="${ba.bankAccountNumberIntl}" /></li>
                            </c:forEach>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
