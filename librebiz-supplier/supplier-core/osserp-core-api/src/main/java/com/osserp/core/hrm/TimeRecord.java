/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 21, 2007 12:04:15 PM 
 * 
 */
package com.osserp.core.hrm;

import java.util.Date;

import com.osserp.common.Calendar;
import com.osserp.common.ClientException;
import com.osserp.common.EntityRelation;
import com.osserp.common.PublicHoliday;
import com.osserp.core.employees.Employee;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface TimeRecord extends EntityRelation, Cloneable {

    /**
     * Provides the type of the record
     * @return type
     */
    TimeRecordType getType();

    /**
     * Provides the status of the record
     * @return status
     */
    TimeRecordStatus getStatus();

    /**
     * Indicates that this record actually waits for a approval
     * @return waitForApproval
     */
    boolean isWaitForApproval();

    /**
     * Indicates that this record is approved
     * @return approved
     */
    boolean isApproved();

    /**
     * Provides the time record value
     * @return value
     */
    Date getValue();

    /**
     * Provides a note for the record
     * @return note
     */
    String getNote();

    /**
     * Indicates that given date is booked by this (starting) time record.
     * @param date
     * @return true if time record is closed start record and date between start and stop
     */
    boolean isBooked(Date date);

    /**
     * Indicates that record is same day
     * @param day
     * @return true if time record is same day
     */
    boolean isDay(Calendar day);

    /**
     * Indicates that time record is from current day
     * @return currentDay
     */
    boolean isCurrentDay();

    /**
     * Indicates that record is member of a year
     * @param year
     * @return true if record values year matches
     */
    boolean isMemberOf(int year);

    /**
     * Indicates that record is member of a month in year
     * @param month
     * @param year
     * @return true if record values month and year matches
     */
    boolean isMemberOf(int month, int year);

    /**
     * Indicates that value is result of a correction so correction is available
     * @return correctionAvailable
     */
    boolean isCorrectionAvailable();

    /**
     * Indicates that record represents a null booking
     * @return nullBooking
     */
    boolean isNullBooking();

    /**
     * Updates value and note and creates a new correction record to preserve previous state.
     * @param user
     * @param correction
     * @param note
     * @return timeRecord correction
     * @throws ClientException
     */
    TimeRecordCorrection createCorrection(Employee user, Date correction, String note, TimeRecordStatus status) throws ClientException;

    /**
     * Returns to previous state
     * @param correction
     */
    void resetCorrection(TimeRecordCorrection correction);

    /**
     * Updates current status and value. Ignores null params.
     * @param status
     * @param value
     */
    void update(TimeRecordStatus status, Date value);

    /**
     * Provides the id of the closed record if type is closing
     * @return closing
     */
    Long getClosing();

    /**
     * Indicates that record was booked by system time
     * @return systemTime
     */
    boolean isSystemTime();

    /**
     * Returns public holiday values
     * @return publicHoliday or null
     */
    PublicHoliday getPublicHoliday();

    /**
     * Sets record as public holiday
     * @param publicHoliday
     */
    void setPublicHoliday(PublicHoliday holiday);

    /**
     * Provides the closing time record for start records
     * @return closedBy
     */
    TimeRecord getClosedBy();

    /**
     * Adds the time record for a starting record
     * @param closedBy
     */
    void setClosedBy(TimeRecord closedBy);

    /**
     * Provides the duration if time record is a closed start record (e.g. closedBy not null)
     * @return duration in minutes
     */
    Long getDuration();

    /**
     * Provides the duration display if time record is a closed start record (e.g. closedBy not null)
     * @return duration in hours and minutes
     */
    String getDurationDisplay();

    /**
     * Indicates that break rules should be ignored for all bookings on same day
     * @return ignoreBreakRules
     */
    boolean isIgnoreBreakRules();

    /**
     * Enables/disables ignore break rules flag
     * @param ignoreBreakRules
     */
    void setIgnoreBreakRules(boolean ignoreBreakRules);

    /**
     * Indicates that upper limit of working hours should be ignored for all bookings on same day
     * @return ignoreUpperLimit
     */
    boolean isIgnoreUpperLimit();

    /**
     * Enables/disables ignore upper limit flag
     * @param ignoreUpperLimit
     */
    void setIgnoreUpperLimit(boolean ignoreUpperLimit);

    /**
     * Provides the terminal id
     * @return terminalId
     */
    Long getTerminalId();

    /**
     * Clones the record
     * @return cloned record
     * @throws CloneNotSupportedException
     */
    Object clone();// throws CloneNotSupportedException;
}
