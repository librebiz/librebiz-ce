/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 28, 2010 11:39:59 AM 
 * 
 */
package com.osserp.common.util;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractComparator implements Serializable {

    private String comparatorMethod = "byName";
    private boolean reverse = false;

    public AbstractComparator() {
        super();
    }

    public AbstractComparator(String comparatorMethod, boolean reverse) {
        super();
        this.comparatorMethod = comparatorMethod;
        this.reverse = reverse;
    }

    /**
     * Sorts list by method. List will be sorted reverse if method equals last selected method.
     * @param method
     * @param list
     */
    public void sort(String method, final List list) {
        if (comparatorMethod.equals(method)) {
            reverse = !reverse;
        } else {
            comparatorMethod = method;
            reverse = false;
        }
        sort(list);
    }

    /**
     * Sorts list by default method or last selected method
     * @param list
     */
    public abstract void sort(final List list);

    public String getComparatorMethod() {
        return comparatorMethod;
    }

    protected void setComparatorMethod(String comparatorMethod) {
        this.comparatorMethod = comparatorMethod;
    }

    public boolean isReverse() {
        return reverse;
    }

    protected void setReverse(boolean reverse) {
        this.reverse = reverse;
    }
}
