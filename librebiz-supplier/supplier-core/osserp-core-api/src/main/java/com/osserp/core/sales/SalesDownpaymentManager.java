/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 25, 2006 5:40:34 AM 
 * 
 */
package com.osserp.core.sales;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.dms.DocumentData;

import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Downpayment;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.InvoiceManager;
import com.osserp.core.finance.Order;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SalesDownpaymentManager extends InvoiceManager {

    /**
     * Creates a new downpayment
     * @param invoiceType we use to lookup for percentage
     * @param order
     * @param user who performs this action
     * @param copyNote indicates if existing order note should be copied to invoice
     * @param copyTerms indicates if terms of order should be copied to invoice
     * @param copyItems indicates if items of order should be copied to invoice
     * @param customId
     * @param customDate
     * @param customAmount
     * @throws ClientException if invoice already exists or amount is null or 0.00
     */
    Downpayment create(
            Long invoiceType,
            Order order,
            Employee user,
            boolean copyNote,
            boolean copyTerms,
            boolean copyItems,
            Long customId,
            Date customDate,
            BigDecimal customAmount) throws ClientException;

    /**
     * Updates the prorate amount of the invoice
     * @param invoice
     * @param amount
     * @return downpayment with updated prorate
     */
    Downpayment updateProrateAmount(Downpayment invoice, BigDecimal amount);

    /**
     * Cancels a downpayment
     * @param user
     * @param record
     * @param date
     * @throws ClientException if payments already booked for this invoice so cancelling is impossible
     */
    void cancel(Employee user, Invoice record, Date date) throws ClientException;

    /**
     * Restores canceled downpayment
     * @param invoice
     * @return restored downpaymnt
     * @throws ClientException if invoice not restorable
     */
    Downpayment restoreCanceled(Employee user, Downpayment invoice) throws ClientException;

    /**
     * Provides canceled downpayments by order
     * @param order
     * @return canceled downpayments or empty list if none exist.
     */
    List<Downpayment> findCanceled(Order order);

    /**
     * Provides source document of a canceled downpayment
     * @param user
     * @param id
     * @return document data
     * @throws ClientException if document not exists and not createable
     */
    DocumentData printCanceledSource(Employee user, Long id) throws ClientException;
}
