/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 2, 2008 1:21:27 PM 
 * 
 */
package com.osserp.core.dao.records.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.ClientException;
import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;

import com.osserp.core.dao.PaymentConditions;
import com.osserp.core.dao.ProductSummaryCache;
import com.osserp.core.dao.Suppliers;
import com.osserp.core.dao.SystemCompanies;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.dao.TaxRates;
import com.osserp.core.dao.records.PurchaseOffers;
import com.osserp.core.dao.records.RecordInfoConfigs;
import com.osserp.core.dao.records.RecordMailLogs;
import com.osserp.core.dao.records.RecordPrintOptionDefaults;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.BookingType;
import com.osserp.core.finance.RecordNumberCreator;
import com.osserp.core.finance.RecordType;
import com.osserp.core.model.records.PurchaseOfferImpl;
import com.osserp.core.model.records.PurchaseOfferTypeImpl;
import com.osserp.core.purchasing.PurchaseOffer;
import com.osserp.core.suppliers.Supplier;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PurchaseOffersDao extends AbstractPurchaseRecords
        implements PurchaseOffers {
    private static Logger log = LoggerFactory.getLogger(PurchaseOffersDao.class.getName());
    private String purchaseOfferView = null;
    private String purchaseOfferQueryView = null;
    private ProductSummaryCache summaryCache = null;
    private RecordMailLogs mailLogs = null;

    public PurchaseOffersDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache namesCache,
            TaxRates taxRates,
            SystemCompanies companies,
            RecordInfoConfigs infoConfigs,
            PaymentConditions paymentConditions,
            RecordNumberCreator recordNumberCreator,
            RecordPrintOptionDefaults recordPrintOptionDefaults,
            RecordMailLogs recordMailLogs,
            Suppliers suppliers,
            ProductSummaryCache summaryCache,
            RecordMailLogs mailLogs,
            String purchaseOfferViewKey,
            String purchaseOfferQueryViewKey) {
        super(
                jdbcTemplate,
                tables,
                sessionFactory,
                namesCache,
                taxRates,
                companies,
                infoConfigs,
                paymentConditions,
                recordNumberCreator,
                recordPrintOptionDefaults,
                recordMailLogs,
                suppliers,
                TableKeys.PURCHASE_OFFERS,
                TableKeys.PURCHASE_OFFER_ITEMS);
        this.purchaseOfferView = getTable(purchaseOfferViewKey);
        this.purchaseOfferQueryView = getTable(purchaseOfferQueryViewKey);
        this.summaryCache = summaryCache;
        this.mailLogs = mailLogs;
    }

    @Override
    protected Long getDefaultRecordTypeId() {
        return RecordType.PURCHASE_OFFER;
    }

    public PurchaseOffer create(
            BookingType bookingType,
            Long company,
            Long branchId,
            Long reference,
            Supplier supplier,
            Employee createdBy,
            Long sales) throws ClientException {
        RecordType recordType = getRecordType();
        PurchaseOfferImpl obj = new PurchaseOfferImpl(
                getNextRecordId(createdBy, company, recordType, null),
                recordType,
                (bookingType == null ? getDefaultBookingType() : bookingType),
                company,
                branchId,
                reference,
                supplier,
                createdBy,
                sales,
                taxRate,
                reducedTaxRate);
        saveInitial(obj);
        return obj;
    }

    @Override
    protected Long getDefaultBookingTypeId() {
        return null;
    }

    @Override
    protected Class<PurchaseOfferTypeImpl> getPersistentBookingTypeClass() {
        return PurchaseOfferTypeImpl.class;
    }

    @Override
    protected Class<PurchaseOfferImpl> getPersistentClass() {
        return PurchaseOfferImpl.class;
    }

}
