/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 28, 2009 
 * 
 */
package com.osserp.gui.products

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.util.CollectionUtil

import com.osserp.core.model.products.ProductClassificationConfigVO
import com.osserp.core.products.ProductCategoryConfig
import com.osserp.core.products.ProductClassificationEntity
import com.osserp.core.products.ProductClassificationConfig
import com.osserp.core.products.ProductGroupConfig
import com.osserp.core.products.ProductTypeConfig
import com.osserp.core.products.ProductClassificationConfigManager

import com.osserp.gui.CoreView

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class AbstractProductClassificationView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(AbstractProductClassificationView.class.getName())

    List<ProductClassificationEntity> typeSelections = []
    ProductCategoryConfig selectedCategory
    ProductGroupConfig selectedGroup
    ProductTypeConfig selectedType

    AbstractProductClassificationView() {
        super()
        env.productViewRequired = false
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            setSelectionTarget(form.getString('selectionTarget'))
            reload()
        }
    }

    
    @Override
    void reload() {
        typeSelections = productClassificationConfigManager.typesDisplay
        if (selectedType) {
            Long t = selectedType.id
            Long g = selectedGroup?.id
            Long c = selectedCategory?.id
            setType(t)
            if (g) {
                setGroup(g)
                if (c) {
                    setCategory(c)
                }
            }
        }
    }

    void selectType() {
        setType(form.getLong('id'))
    }

    protected void setType(Long id) {
        if (!id) {
            selectedType = null
            logger.debug('setType: type reset done')
        } else {
            selectedType = productClassificationConfigManager.getType(id)
            logger.debug("setType: done [id=${id}")
        }
        selectedGroup = null
    }

    void selectGroup() {
        setGroup(form.getLong('id'))
    }

    protected void setGroup(Long id) {
        if (!id || !selectedType) {
            logger.debug('setGroup: group reset done')
            selectedGroup = null
        } else {
            selectedGroup = CollectionUtil.getById(selectedType.groups, id)
            logger.debug("setGroup: done [id=${id}")
        }
        selectedCategory = null
    }

    void selectCategory() {
        setCategory(form.getLong('id'))
    }

    protected void setCategory(Long id) {
        if (!id || !selectedGroup) {
            selectedCategory = null
            logger.debug('setCategory: category reset done')
        } else {
            selectedCategory = CollectionUtil.getById(selectedGroup.categories, id)
            logger.debug("setCategory: done [id=${id}")
        }
    }

    ProductClassificationConfig getSelectedConfig() {
        return new ProductClassificationConfigVO(selectedType, selectedGroup, selectedCategory)
    }

    protected boolean isProductViewRequired() {
        (env.productViewRequired == true)
    }

    protected ProductClassificationConfigManager getProductClassificationConfigManager() {
        getService(ProductClassificationConfigManager.class.getName())
    }
}
