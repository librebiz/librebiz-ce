/**
 *
 * Copyright (C) 2011, 2024 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created on 11.04.2011 17:06:42
 *
 */
package com.osserp.gui.admin.projects

import javax.servlet.http.HttpServletRequest

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.groovy.web.AdminViewController

/**
 *
 * @author eh <eh@osserp.com>
 * @author rk <rk@osserp.com>
 *
 */
@RequestMapping("/admin/projects/businessTypeConfig/*")
@Controller class BusinessTypeConfigController extends AdminViewController {

    @RequestMapping
    def toggleDisplayDisabled(HttpServletRequest request) {
        BusinessTypeConfigView view = getView(request)
        view.toggleDisplayDisabled()
        return defaultPage
    }

    @RequestMapping
    def resetDefaultOrigin(HttpServletRequest request) {
        BusinessTypeConfigView view = getView(request)
        view.setCampaign(null)
        return defaultPage
    }
}
