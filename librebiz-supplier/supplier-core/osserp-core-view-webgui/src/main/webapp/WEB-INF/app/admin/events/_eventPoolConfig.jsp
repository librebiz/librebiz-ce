<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="eventPoolsView" />
<c:set var="changePermission" value="false" />
<o:permission role="event_pool_config,executive">
    <c:set var="changePermission" value="true" />
</o:permission>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel-body">
        <div class="table-responsive table-responsive-default">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th class="number center"><fmt:message key="id" /></th>
                        <th class="name"><fmt:message key="recipient" /></th>
                        <th class="center" colspan="2"> </th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="pool" items="${view.list}">
                        <c:choose>
                            <c:when test="${pool.id == view.bean.id}">
                                <tr class="altrow">
                                    <td class="center"><o:out value="${pool.id}" /></td>
                                    <td class="boldtext"><o:out value="${pool.name}" /></td>
                                    <c:choose>
                                        <c:when test="${changePermission}">
                                            <td class="icon center">
                                                <v:ajaxLink url="/admin/events/poolMemberSelection/forward?view=eventPoolsView&exclude=poolMembers&selectionTarget=/admin/events/eventPools/addEmployee?poolId=${pool.id}">
                                                    <o:img name="openFolderIcon" />
                                                </v:ajaxLink>
                                            </td>
                                            <td class="icon center">
                                                <c:if test="${empty pool.members}">
                                                    <v:link url="/admin/events/eventPools/removePool" parameters="id=${pool.id}">
                                                        <o:img name="deleteIcon" />
                                                    </v:link>
                                                </c:if>
                                            </td>
                                        </c:when>
                                        <c:otherwise>
                                            <td class="icon center">
                                            </td>
                                            <td class="icon center">
                                            </td>
                                        </c:otherwise>
                                    </c:choose>
                                </tr>
                                <c:forEach var="member" items="${pool.members}">
                                    <tr>
                                        <td> </td>
                                        <td>
                                            <v:ajaxLink linkId="recipientId" url="${'/employees/employeePopup/forward?id='}${member.recipientId}">
                                                <oc:employee value="${member.recipientId}" />
                                            </v:ajaxLink>
                                        </td>
                                        <td class="icon center">
                                            <c:if test="${changePermission}">
                                                <v:link url="/admin/events/eventPools/removeEmployee" parameters="id=${pool.id}&value=${member.recipientId}">
                                                    <o:img name="deleteIcon" />
                                                </v:link>
                                            </c:if>
                                        </td>
                                        <td class="icon center">
                                        </td>
                                    </tr>
                                </c:forEach>
                            </c:when>
                            <c:otherwise>
                                <tr class="altrow">
                                    <td class="center"><o:out value="${pool.id}" /></td>
                                    <td class="boldtext">
                                        <v:link url="/admin/events/eventPools/select" parameters="id=${pool.id}" >
                                            <o:out value="${pool.name}" />
                                        </v:link>
                                    </td>
                                    <td class="icon center">
                                        <c:if test="${empty view.bean and changePermission}">
                                            <v:link url="/admin/events/eventPools/enableEditMode" parameters="id=${pool.id}" >
                                                <o:img name="writeIcon" />
                                            </v:link>
                                        </c:if>
                                    </td>
                                    <td class="icon center">
                                    </td>
                                </tr>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
