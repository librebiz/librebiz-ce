<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.businessCaseView || empty sessionScope.salesBillingView}"><c:redirect url="/errors/error_context.jsp"/></c:if>
<c:set var="selectUrl" scope="request" value="/projectInvoiceSelection.do"/>
<c:set var="printEnabled" scope="request" value="true"/>
<c:set var="project" value="${sessionScope.businessCaseView.sales}"/>
<c:set var="billing" value="${sessionScope.salesBillingView}"/>
<c:set var="paymentAgreement" value="${sessionScope.salesBillingView.paymentAgreement}"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
<tiles:put name="title"><o:displayTitle/></tiles:put>
<tiles:put name="styles" type="string">
<style type="text/css">
<c:import url="/pages/projects/billing/billing_list.css"/>
</style>
</tiles:put>

<tiles:put name="headline"><fmt:message key="billsPaymentsTo"/> <o:out value="${project.id}"/> - <o:out value="${project.request.name}" limit="40"/></tiles:put>
<tiles:put name="headline_right">
<a href="<c:url value="/projectBilling.do?method=exitDisplay"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a>
<o:permission role="accounting,project_billing,executive" info="permissionAddProjectInvoices">
	<a href="<c:url value="/projectInvoiceAddForward.do"/>" title="<fmt:message key="createNewInvoice"/>"><o:img name="newdataIcon"/></a>
</o:permission>
<v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link>
</tiles:put>

<tiles:put name="content" type="string"> 
<div class="subcolumns">
    <div class="subcolumn">
        <div class="spacer"></div>
        <c:import url="/pages/shared/lock_handler.jsp"/>
        <c:import url="/pages/projects/billing/billing_list.jsp"/>
        <div class="spacer"></div>
        <table class="table">
            <tr>
                <th colspan="2"><fmt:message key="recordPaymentConditions"/></th>
            </tr>
            <tr>
                <td colspan="2"><fmt:message key="paymentTarget"/>:&nbsp;&nbsp;&nbsp;<span class="boldtext"><o:out value="${paymentAgreement.paymentCondition.name}"/></span>&nbsp;&nbsp;&nbsp;
                    <a href="<c:url value="/salesOrder.do"><c:param name="method" value="display"/><c:param name="exit" value="billing"/><c:param name="id" value="${billing.order.id}"/></c:url>" style="margin-left: 30px;">[<fmt:message key="change"/>]</a>
                </td>
            </tr>
            <c:if test="${!empty paymentAgreement and !paymentAgreement.hidePayments}">
                <c:choose>
                    <c:when test="${paymentAgreement.amountsFixed}">
                        <tr class="altrow">
                            <td colspan="2"><span class="boldtext"><o:number value="${paymentAgreement.downpaymentAmount}" format="currency"/></span> <oc:options name="recordPaymentTargets" value="${paymentAgreement.downpaymentTargetId}"/></td>
                        </tr>
                        <tr class="altrow">
                            <td colspan="2"><span class="boldtext"><o:number value="${paymentAgreement.deliveryInvoiceAmount}" format="currency"/></span> <oc:options name="recordPaymentTargets" value="${paymentAgreement.deliveryInvoiceTargetId}"/></td>
                        </tr>
                        <tr class="altrow">
                            <td colspan="2"><span class="boldtext"><o:number value="${paymentAgreement.finalInvoiceAmount}" format="currency"/></span> <oc:options name="recordPaymentTargets" value="${paymentAgreement.finalInvoiceTargetId}"/></td>
                        </tr>
                    </c:when>
                    <c:otherwise>
                        <tr class="altrow">
                            <td><span class="boldtext"><o:number value="${paymentAgreement.downpaymentPercent}" format="percent"/>%</span> <oc:options name="recordPaymentTargets" value="${paymentAgreement.downpaymentTargetId}"/></td>
                            <td class="amount"><o:number value="${paymentAgreement.downpaymentAmount}" format="currency"/></td>
                        </tr>
                        <tr class="altrow">
                            <td><span class="boldtext"><o:number value="${paymentAgreement.deliveryInvoicePercent}" format="percent"/>%</span> <oc:options name="recordPaymentTargets" value="${paymentAgreement.deliveryInvoiceTargetId}"/></td>
                            <td class="amount"><o:number value="${paymentAgreement.deliveryInvoiceAmount}" format="currency"/></td>
                        </tr>
                        <tr class="altrow">
                            <td><span class="boldtext"><o:number value="${paymentAgreement.finalInvoicePercent}" format="percent"/>%</span> <oc:options name="recordPaymentTargets" value="${paymentAgreement.finalInvoiceTargetId}"/></td>
                            <td class="amount"><o:number value="${paymentAgreement.finalInvoiceAmount}" format="currency"/></td>
                        </tr>
                    </c:otherwise>
                </c:choose>
            </c:if>
        </table>			 
    </div>
</div>
</tiles:put> 
</tiles:insert>
