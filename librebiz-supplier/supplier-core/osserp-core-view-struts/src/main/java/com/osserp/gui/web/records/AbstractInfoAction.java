/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 8, 2006 7:54:02 AM 
 * 
 */
package com.osserp.gui.web.records;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ActionException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.RequestUtil;
import com.osserp.gui.client.Actions;
import com.osserp.gui.client.records.RecordView;
import com.osserp.gui.web.struts.AbstractViewDispatcherAction;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractInfoAction extends AbstractViewDispatcherAction {
    private static Logger log = LoggerFactory.getLogger(AbstractInfoAction.class.getName());

    @Override
    public ActionForward exit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exit() request from " + getUser(session).getId());
        }
        RecordView view = getRecordView(session);
        return mapping.findForward(view.getActionTarget());
    }

    @Override
    public ActionForward forward(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("forward() request from " + getUser(session).getId());
        }
        return mapping.findForward(Actions.SUCCESS);
    }

    /**
     * Adds an info to a record
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward add(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("add() request from " + getUser(session).getId());
        }
        Long id = RequestUtil.getId(request);
        RecordView view = getRecordView(session);
        view.addInfo(id);
        return mapping.findForward(Actions.SUCCESS);
    }

    /**
     * Removes all infos from a record
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward clear(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("clear() request from " + getUser(session).getId());
        }
        RecordView view = getRecordView(session);
        view.clearInfos();
        return mapping.findForward(Actions.SUCCESS);
    }

    /**
     * Removes all infos from a record and adds default infos
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward defaults(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("defaults() request from " + getUser(session).getId());
        }
        RecordView view = getRecordView(session);
        view.setDefaultInfos();
        return mapping.findForward(Actions.SUCCESS);
    }

    /**
     * Adds an info to a record
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward remove(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("remove() request from " + getUser(session).getId());
        }
        Long id = RequestUtil.getId(request);
        if (log.isDebugEnabled()) {
            log.debug("remove() id to remove is " + id);
        }
        RecordView view = getRecordView(session);
        view.removeInfo(id);
        return mapping.findForward(Actions.SUCCESS);
    }

    /**
     * Moves an info list position down
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward moveDown(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("moveDown() request from " + getUser(session).getId());
        }
        Long id = RequestUtil.getId(request);
        if (log.isDebugEnabled()) {
            log.debug("moveDown() id to remove is " + id);
        }
        RecordView view = getRecordView(session);
        view.moveInfoDown(id);
        return mapping.findForward(Actions.SUCCESS);
    }

    /**
     * Moves an info list position up
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward moveUp(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("moveUp() request from " + getUser(session).getId());
        }
        Long id = RequestUtil.getId(request);
        if (log.isDebugEnabled()) {
            log.debug("moveUp() id to remove is " + id);
        }
        RecordView view = getRecordView(session);
        view.moveInfoUp(id);
        return mapping.findForward(Actions.SUCCESS);
    }

    protected RecordView getRecordView(HttpSession session) throws PermissionException {
        RecordView view = (RecordView) fetchView(session);
        if (view == null) {
            log.warn("getRecordView() no view bound!");
            throw new ActionException();
        }
        return view;
    }
}
