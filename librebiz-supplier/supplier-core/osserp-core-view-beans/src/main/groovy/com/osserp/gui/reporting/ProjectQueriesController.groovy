/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Jul 5, 2011 3:04:04 PM 
 * 
 */
package com.osserp.gui.reporting

import javax.servlet.http.HttpServletRequest

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

/**
 *
 * @author eh <eh@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
@RequestMapping("/reporting/projectQueries/*")
@Controller class ProjectQueriesController extends AbstractSalesListReportingController {
    private static Logger logger = LoggerFactory.getLogger(ProjectQueriesController.class.getName())
    
    @RequestMapping
    def forwardList(HttpServletRequest request) {
        logger.debug("forwardList: invoked [user=${getUserId(request)}]")
        ProjectQueriesView view = getView(request)
        view.forwardList()
        return defaultPage
    }

    @RequestMapping
    def forwardListUnreferenced(HttpServletRequest request) {
        logger.debug("forwardListUnreferenced: invoked [user=${getUserId(request)}]")
        ProjectQueriesView view = getView(request)
        view.forwardListUnreferenced()
        return defaultPage
    }

    @RequestMapping
    def exitList(HttpServletRequest request) {
        logger.debug("exitList: invoked [user=${getUserId(request)}]")
        ProjectQueriesView view = getView(request)
        view.exitList()
        return defaultPage
    }

    @RequestMapping
    def loadPreviousMonth(HttpServletRequest request) {
        logger.debug("loadPreviousMonth: invoked [user=${getUserId(request)}]")
        ProjectQueriesView view = getView(request)
        view.loadPreviousMonth()
        return defaultPage
    }

    @RequestMapping
    def loadNextMonth(HttpServletRequest request) {
        logger.debug("loadNextMonth: invoked [user=${getUserId(request)}]")
        ProjectQueriesView view = getView(request)
        view.loadNextMonth()
        return defaultPage
    }
}
