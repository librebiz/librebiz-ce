/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 8, 2006 7:20:02 AM 
 * 
 */
package com.osserp.gui.client.sales.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;
import com.osserp.common.web.ViewName;

import com.osserp.core.customers.CustomerSearch;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.BillingTypeSearch;
import com.osserp.core.finance.BookingType;
import com.osserp.core.finance.CreditNote;
import com.osserp.core.finance.Downpayment;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.InvoiceManager;
import com.osserp.core.finance.Payment;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordManager;
import com.osserp.core.finance.RecordType;
import com.osserp.core.sales.SalesCreditNoteManager;
import com.osserp.core.sales.SalesDownpaymentManager;
import com.osserp.core.sales.SalesInvoice;
import com.osserp.core.sales.SalesInvoiceManager;
import com.osserp.core.sales.SalesPaymentManager;
import com.osserp.core.users.DomainUser;
import com.osserp.core.users.Permissions;

import com.osserp.gui.client.records.impl.AbstractCorrectionAwareRecordView;
import com.osserp.gui.client.sales.SalesInvoiceView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("salesInvoiceView")
public class SalesInvoiceViewImpl extends AbstractCorrectionAwareRecordView implements SalesInvoiceView {
    private static Logger log = LoggerFactory.getLogger(SalesInvoiceViewImpl.class.getName());
    private List<BookingType> creditNoteBookingTypes = new ArrayList<>();
    private boolean advanceInvoiceMode = false;
    private boolean volumeView = false;
    private Double canceledCustomerCredit = 0d;

    protected SalesInvoiceViewImpl() {
        super();
    }

    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        super.init(request);
        initCreditNoteBookingTypes();
    }

    private void initCreditNoteBookingTypes() {
        SalesCreditNoteManager cnm = (SalesCreditNoteManager) getService(SalesCreditNoteManager.class.getName());
        creditNoteBookingTypes = cnm.getBookingTypes();
    }
    
    @Override
    public boolean isDocumentImportable() {
        return !advanceInvoiceMode && isPermissionGrant(Permissions.HISTORICAL_RECORD_IMPORT);
    }

    public void loadDownpayment(Long id, String exitTarget, String exitId, boolean readonly) throws ClientException {
        advanceInvoiceMode = true;
        boolean alreadyLoaded = false;
        if (getBean() != null && getBean() instanceof Record) {
            Record existing = (Record) getBean();
            if (existing.getId().equals(id) && RecordType.SALES_DOWNPAYMENT.equals(existing.getType().getId())) {
                setBean(getDownpaymentManager().load(existing.getId()));
                alreadyLoaded = true;
            }
        }
        if (!alreadyLoaded) {
            super.loadRecord(id, exitTarget, exitId, readonly);
        }
        if (getBean() instanceof Invoice) {
            Invoice invoice = (Invoice) getBean();
            log.debug("loadDownpayment() done [id=" + invoice.getId() + ", payments=" + invoice.getPayments().size() + "]");
        }
    }

    public void restoreDownpayment() throws ClientException {
        if (isSetupMode() && getBean() instanceof Downpayment) {
            Downpayment dp = (Downpayment) getBean();
            if (dp.isCanceled() && isPermissionGrant(getAccountingPermissions())) {
                SalesDownpaymentManager manager = getDownpaymentManager();
                setBean(manager.restoreCanceled(getDomainEmployee(), dp));
                disableSetupMode();
            } else {
                log.warn("restoreDownpayment: record not canceled or unauthorized access [id=" + dp.getId() + "]");
            }
        }
    }

    @Override
    public void loadRecord(Long id, String exitTarget, String exitId, boolean readonly) throws ClientException {
        advanceInvoiceMode = false;
        boolean alreadyLoaded = false;
        if (getBean() != null && getBean() instanceof Record) {
            Record existing = (Record) getBean();
            if (existing.getId().equals(id) && RecordType.SALES_INVOICE.equals(existing.getType().getId())) {
                refresh();
                alreadyLoaded = true;
            } else {
                setBean(null);
            }
        }
        if (!alreadyLoaded) {
            super.loadRecord(id, exitTarget, exitId, readonly);
        }
    }

    public void checkCancellation() throws ClientException {
        Invoice invoice = (Invoice) getRecord();
        if (invoice instanceof SalesInvoice) {
            SalesInvoice salesInvoice = (SalesInvoice) invoice;
            if (!salesInvoice.getCreditNotes().isEmpty()) {
                List<CreditNote> notes = ((SalesInvoice) invoice).getCreditNotes();
                for (int i = 0, j = notes.size(); i < j; i++) {
                    CreditNote next = notes.get(i);
                    if (!next.isCanceled()) {
                        throw new ClientException(ErrorCode.UNCANCELLED_CREDIT_NOTE_FOUND);
                    }
                }
            }
        }
    }

    public void checkDelete() throws PermissionException {
        if (!isRecordDeletable()) {
            throw new PermissionException();
        }
    }

    @Override
    protected boolean isRecordDeletePermissionGrant(boolean defaultChecksResult) {
        if (!defaultChecksResult) {
            Invoice invoice = (Invoice) getRecord();
            DomainUser user = getDomainUser();
            boolean editByUser = (user.getId().equals(invoice.getCreatedBy())
                || user.getId().equals(invoice.getChangedBy()));
            if (!invoice.isUnchangeable() 
                    && !invoice.isDownpayment() 
                    && isNotSet(invoice.getReference())
                    && !invoice.getType().isNumberCreatorBySequence() 
                    && editByUser) {
                return true;
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("isRecordDeletePermissionGrant() returns with defaultChecks [result=" 
                    + defaultChecksResult + "]");
        }
        return defaultChecksResult;
    }

    @Override
    public void save(Form form) throws ClientException, PermissionException {
        Invoice invoice = (Invoice) getRecord();

        if (log.isDebugEnabled()) {
            log.debug("save() invoked [invoice=" + (invoice == null ? "null" : invoice.getId()) + "]");
        }
        if (invoice == null) {
            throw new ActionException("invoice not bound");
        }
        InvoiceManager manager = (InvoiceManager) getRecordManager();
        manager.update(
                invoice,
                form.getLong("signatureLeft"),
                form.getLong("signatureRight"),
                form.getLong("personId"),
                form.getString("note"),
                form.getBoolean("taxFree"),
                form.getLong("taxFreeId"),
                form.getDouble("taxRate"),
                form.getDouble("reducedTaxRate"),
                form.getLong("currency"),
                form.getString("language"),
                form.getLong("branch"),
                form.getLong("shippingId") != null ? form.getLong("shippingId") : invoice.getShippingId(),
                form.getString("customHeader"),
                form.getBoolean("printComplimentaryClose"),
                form.getBoolean("printProjectmanager"),
                form.getBoolean("printSalesperson"),
                form.getBoolean("printBusinessCaseId"),
                form.getBoolean("printBusinessCaseInfo"),
                form.getBoolean("printRecordDate"),
                form.getBoolean("printRecordDateByStatus"),
                form.getBoolean("printPaymentTarget"),
                form.getBoolean("printConfirmationPlaceholder"),
                form.getBoolean("printCoverLetter"));
        manager.updatePaymentCondition(invoice, form.getLong("paymentConditionId"));
        manager.updateTaxPoint(
        	invoice, 
        	form.getDate("taxPoint"), 
            form.getString("timeOfSupply"), 
            form.getString("placeOfPerformance"), 
        	form.getBoolean("printTaxPoint"), 
        	form.getBoolean("printTaxPointByItems"), 
        	form.getBoolean("printTimeOfSupply"),
        	form.getBoolean("printPlaceOfPerformance"));
        String paymentNote = form.getString("paymentNote");
        if (isSet(paymentNote)) {
            invoice.setPaymentNote(paymentNote);
            manager.persist(invoice);
        }
        if (manager instanceof SalesDownpaymentManager) {
            if (log.isDebugEnabled()) {
                log.debug("save() invoked in downpayment context");
            }
            try {
                BigDecimal d = form.getDecimal("amount");
                if (d != null && d.doubleValue() > 0) {
                    SalesDownpaymentManager aim = (SalesDownpaymentManager) manager;
                    setBean(aim.updateProrateAmount((Downpayment) invoice, d));
                }
            } catch (Throwable t) {
                // we ignore this
            }
        } else {
            SalesInvoiceManager salesInvoiceManager = getSalesInvoiceManager();
            if (isDocumentImportable() && invoice instanceof SalesInvoice) {
                boolean historical = form.getBoolean("historical");
                SalesInvoice changedInvoice = (SalesInvoice) invoice;
                if (changedInvoice.isHistorical() && !historical) {
                    invoice = salesInvoiceManager.disableHistoricalStatus(changedInvoice);
                    if (log.isDebugEnabled()) {
                        log.debug("save() disabled historical status [id" + invoice.getId()
                                + ", status=" + invoice.getStatus() + "]");
                    }
                } else if (!changedInvoice.isHistorical() && historical) {
                    invoice = salesInvoiceManager.enableHistoricalStatus(changedInvoice);
                    if (log.isDebugEnabled()) {
                        log.debug("save() enabled historical status [id" + invoice.getId()
                                + ", status=" + invoice.getStatus() + "]");
                    }
                }
            }
            if (isRecordNumberChangeable()) {
                Long newNumber = form.getLong("recordId");
                if (isSet(newNumber) && !newNumber.equals(invoice.getId())) {
                    SalesInvoice changedInvoice = salesInvoiceManager.changeNumber(
                        getDomainEmployee(), (SalesInvoice) invoice, newNumber);
                    setBean(changedInvoice);
                }
            }
            if (isSystemPropertyEnabled("deEst35aSupport")) {
                setBean(salesInvoiceManager.updateDeEst35a(invoice,
                        form.getString("deEst35a"), form.getString("deEst35aTax")));
            }
        }
        refresh();
        savePrintOptionDefaults(form, getRecord());
    }

    public void changeRecordNumber(Form form) throws ClientException {
        SalesInvoice invoice = (SalesInvoice) getBean();
        if (invoice != null && isRecordNumberChangeable()) {
            Long newNumber = form.getLong("recordId");
            if (isSet(newNumber) && !newNumber.equals(invoice.getId())) {
                SalesInvoice changedInvoice = getSalesInvoiceManager().changeNumber(
                        getDomainEmployee(), invoice, newNumber);
                setBean(changedInvoice);
                refresh();
            }
        }
    }

    public List<Option> getPaymentTypes() {
        BillingTypeSearch search = (BillingTypeSearch) getService(BillingTypeSearch.class.getName());
        return new ArrayList(search.findPaymentTypes());
    }

    public void addPayment(Long type, BigDecimal amount, Date paid, Long bankAccountId, boolean closed) throws ClientException, PermissionException {
        if (isNotSet(amount)) {
            throw new ClientException(ErrorCode.AMOUNT_MISSING);
        }
        if (isNotSet(bankAccountId)) {
            throw new ClientException(ErrorCode.BANKACCOUNT_MISSING);
        }
        if (paid == null) {
            paid = new Date(System.currentTimeMillis());
        }
        InvoiceManager manager = (InvoiceManager) getRecordManager();
        Invoice invoice = (Invoice) getRecord();
        Employee employee = getDomainUser().getEmployee();
        manager.addPayment(employee, invoice, type, amount, paid, bankAccountId);
        if (closed) {
            manager.updateStatus(invoice, employee, Record.STAT_CLOSED);
        }
        setRecord(getRecordManager().find(getRecord().getId()));
    }

    public Double getCanceledCustomerCredit() {
        return canceledCustomerCredit;
    }

    public void payoutCancelled() throws ClientException {
        Invoice invoice = (Invoice) getRecord();
        if (invoice.isCanceled() && canceledCustomerCredit != null
                && canceledCustomerCredit > 0) {
            InvoiceManager manager = (InvoiceManager) getRecordManager();
            Employee employee = getDomainUser().getEmployee();
            manager.addPayment(
                    employee,
                    invoice,
                    Payment.OUTPAYMENT,
                    new BigDecimal(canceledCustomerCredit * (-1)),
                    new Date(System.currentTimeMillis()),
                    null); // TODO add bankAccountId
            loadRecord(invoice.getId(), null, null, false);
        }
    }

    public void removePayment(Long id) throws PermissionException {
        if (log.isInfoEnabled()) {
            log.info("removePayment() invoked by " + getUser().getId()
                    + " for payment " + id
                    + " of record "
                    + (getBean() == null ? "null" : getRecord().getId()));
        }
        SalesPaymentManager manager = (SalesPaymentManager) getService(SalesPaymentManager.class.getName());
        Invoice invoice = (Invoice) getRecord();
        Payment selected = getSelectedPayment(id);
        manager.delete(selected);
        RecordManager recordManager = getRecordManager();
        if (Record.STAT_CLOSED.equals(invoice.getStatus())) {
            try {
                recordManager.updateStatus(
                        invoice,
                        getDomainUser().getEmployee(),
                        Record.STAT_SENT);
            } catch (ClientException e) {
                log.error("removePayment() ignoring failed [message=" + e.getMessage() + "]", e);
            }
        }
        setRecord(recordManager.find(getRecord().getId()));
        if (log.isInfoEnabled()) {
            log.info("removePayment() done");
        }
    }

    public void markAsPaid() throws ClientException, PermissionException {
        DomainUser user = getDomainUser();
        if (!user.isAccounting()) {
            throw new PermissionException();
        }
        InvoiceManager manager = (InvoiceManager) getRecordManager();
        Invoice invoice = (Invoice) getRecord();
        if (!invoice.isUnchangeable()) {
            throw new ClientException(ErrorCode.UNRELEASED_RECORD);
        }
        manager.updateStatus(invoice, user.getEmployee(), Record.STAT_CLOSED);
        setRecord(manager.find(getRecord().getId()));
    }

    private Payment getSelectedPayment(Long id) {
        Invoice invoice = (Invoice) getRecord();
        for (int i = 0, j = invoice.getPayments().size(); i < j; i++) {
            Payment next = invoice.getPayments().get(i);
            if (next.getId().equals(id)) {
                return next;
            }
        }
        return null;
    }

    public void changeCustomer(Long id) throws ClientException {
        CustomerSearch customerSearch = (CustomerSearch) getService(CustomerSearch.class.getName());
        changeContact(customerSearch.findById(id));
    }

    public void switchHistorical() throws PermissionException {
        if (!isPermissionGrant(Permissions.HISTORICAL_RECORD_IMPORT)) {
            throw new PermissionException();
        }
        Invoice invoice = (Invoice) getRecord();
        if (invoice instanceof SalesInvoice) {
            if (log.isDebugEnabled()) {
                log.debug("switchHistorical() invoked [id=" + invoice.getId() 
                        + ", historical=" + invoice.isHistorical() + "]");
            }
            SalesInvoiceManager manager = (SalesInvoiceManager) getRecordManager();
            if (invoice.isHistorical()) {
                setBean(manager.disableHistoricalStatus((SalesInvoice) invoice));
            } else {
                setBean(manager.enableHistoricalStatus((SalesInvoice) invoice));
            }
        }
    }

    public List<BookingType> getCreditNoteBookingTypes() {
        return creditNoteBookingTypes;
    }

	/*
     * (non-Javadoc)
     * 
     * @see com.osserp.gui.client.records.impl.AbstractRecordView#checkCreatePdf(java.lang.Long)
     */
    @Override
    public void checkRelease() throws ClientException, PermissionException {
        Invoice invoice = (Invoice) getRecord();
        if (!(invoice instanceof Downpayment)) {
            SalesInvoiceManager manager = (SalesInvoiceManager) getRecordManager();
            if (manager.isStockAffecting(invoice) && !manager.isAvailableOnStock(invoice)) {
                throw new ClientException(ErrorCode.STOCK_AVAILABILITY);
            }
        }
    }

    @Override
    public RecordManager getRecordManager() {
        if (getBean() == null) {
            if (advanceInvoiceMode) {
                return getDownpaymentManager();
            }
            return getInvoiceManager();
        }
        Invoice record = (Invoice) getRecord();
        if (record.isDownpayment()) {
            return getDownpaymentManager();
        }
        return getInvoiceManager();
    }

    @Override
    public boolean isProvidingStatusHistory() {
        return true;
    }

    @Override
	public void setRecord(Record record) {
		super.setRecord(record);
        loadReferencedDocument(record);
        Invoice invoice = (Invoice) getRecord();
        if (invoice.isCanceled()) {
            BigDecimal paid = invoice.getPaidAmount();
            if (paid != null) {
                canceledCustomerCredit = paid.doubleValue();
                log.debug("setRecord() invoice is canceled [customerCredit="
                        + canceledCustomerCredit + "]");
            }
        }
        log.debug("setRecord() done [id=" + invoice.getId() + ", payments=" + invoice.getPayments().size() + "]");
	}

	public boolean isRecordNumberChangeable() {
        boolean changeClosedEnabled = isSystemPropertyEnabled("salesInvoiceClosedIdChange");
        if (getBean() != null) {
            if (!getRecord().isUnchangeable() && isPermissionGrant("sales_invoice_change_id,sales_invoice_change_id_closed")) {
                return true;
            }
            if (changeClosedEnabled && getRecord().isUnchangeable()
                    && isPermissionGrant("sales_invoice_change_id_closed")) {
                return true;
            }
        }
        return false;
    }

    public boolean isVolumeView() {
        return volumeView;
    }

    protected void setVolumeView(boolean volumeView) {
        this.volumeView = volumeView;
    }

    private final InvoiceManager getInvoiceManager() {
        return (InvoiceManager) getService(SalesInvoiceManager.class.getName());
    }

    private final SalesInvoiceManager getSalesInvoiceManager() {
        return (SalesInvoiceManager) getService(SalesInvoiceManager.class.getName());
    }

    private final SalesDownpaymentManager getDownpaymentManager() {
        return (SalesDownpaymentManager) getService(SalesDownpaymentManager.class.getName());
    }
}
