/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 04-May-2005 22:01:04 
 * 
 */
package com.osserp.core.model.projects;

import java.util.Date;
import java.util.Map;

import org.jdom2.Element;

import com.osserp.common.util.DateFormatter;
import com.osserp.common.util.NumberFormatter;
import com.osserp.common.util.StringUtil;

import com.osserp.core.BusinessType;
import com.osserp.core.projects.results.ProjectByActionDate;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProjectByActionDateVO extends ProjectListItemVO
        implements ProjectByActionDate {
    private static final long serialVersionUID = 42L;

    private Long customerId = null;
    private String customerName = null;
    private String street = null;
    private String zipcode = null;
    private String city = null;
    private Long actionId = null;
    private Date actionCreated = null;
    private Double plantCapacity = null;
    private String lastActionName = null;
    private Double pricePerUnit = null;

    public ProjectByActionDateVO() {

    }

    /**
     * Constructor to set all values with one call
     * @param id
     * @param planId
     * @param customerId
     * @param customerName
     * @param street
     * @param zipcode
     * @param city
     * @param projectName
     * @param type
     * @param status
     * @param salesId
     * @param managerId
     * @param actionId
     * @return created
     * @param plantCapacity
     * @param lastActionName
     * @param pricePerUnit
     */
    public ProjectByActionDateVO(
            Long id,
            Long planId,
            Long customerId,
            String customerName,
            String street,
            String zipcode,
            String city,
            String projectName,
            BusinessType type,
            Integer status,
            Long salesId,
            Long managerId,
            Long branchId,
            Double plantCapacity,
            Long actionId,
            Date actionCreated,
            String lastActionName,
            Double pricePerUnit) {

        super(id, planId, projectName, type, status, salesId, managerId, branchId, null, null);
        this.customerId = customerId;
        this.customerName = customerName;
        this.street = street;
        this.zipcode = zipcode;
        this.city = city;
        this.actionId = actionId;
        this.actionCreated = actionCreated;
        this.plantCapacity = plantCapacity;
        this.lastActionName = lastActionName;
        this.pricePerUnit = pricePerUnit;
    }

    public Element getValues(Map employees) {
        Element prj = new Element("project");
        prj.addContent(new Element("prj").setText(String.valueOf(getId().longValue())));
        if (getBranchId() != null) {
            prj.addContent(new Element("branchId").setText(getBranchId().toString()));
        }
        prj.addContent(new Element("customer").setText(customerId.toString()));
        if (customerName == null) {
            prj.addContent(new Element("customerName"));
        } else {
            prj.addContent(new Element("customerName").setText(customerName));
        }
        if (street == null) {
            prj.addContent(new Element("street"));
        } else {
            prj.addContent(new Element("street").setText(street));
        }
        if (street == null) {
            prj.addContent(new Element("zipcode"));
        } else {
            prj.addContent(new Element("zipcode").setText(zipcode));
        }
        if (city == null) {
            prj.addContent(new Element("city"));
        } else {
            prj.addContent(new Element("city").setText(city));
        }
        prj.addContent(new Element("name").setText(StringUtil.cut(getName(), 28, true)));
        prj.addContent(new Element("status").setText(String.valueOf(getStatus().intValue())));
        if (lastActionName == null) {
            prj.addContent(new Element("lastActionName"));
        } else {
            prj.addContent(new Element("lastActionName").setText(lastActionName));
        }
        prj.addContent(new Element("plantPower").setText(
                NumberFormatter.getValue(plantCapacity, NumberFormatter.DECIMAL)));
        prj.addContent(new Element("pricePerUnit").setText(
                NumberFormatter.getValue(pricePerUnit, NumberFormatter.CURRENCY)));
        if (getSalesId() == null) {
            prj.addContent(new Element("sales").setText(""));
        } else {
            prj.addContent(new Element("sales").setText(getEmployee(employees, getSalesId())));
        }
        if (getManagerId() == null) {
            prj.addContent(new Element("manager").setText(""));
        } else {
            prj.addContent(new Element("manager").setText(getEmployee(employees, getManagerId())));
        }
        prj.addContent(new Element("actionDate").setText(
                DateFormatter.getDate(actionCreated)));
        return prj;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    /**
     * The actionId
     * @return actionId.
     */
    public Long getActionId() {
        return actionId;
    }

    /**
     * Sets the actionId
     * @param actionId to set.
     */
    public void setActionId(Long actionId) {
        this.actionId = actionId;
    }

    /**
     * The created
     * @return created.
     */
    public Date getActionCreated() {
        return actionCreated;
    }

    /**
     * Sets the created
     * @return created to set.
     */
    public void setActionCreated(Date created) {
        this.actionCreated = created;
    }

    /**
     * The plantCapacity
     * @return plantCapacity.
     */
    public Double getPlantCapacity() {
        return plantCapacity;
    }

    /**
     * Sets the plantCapacity
     * @param plantCapacity to set.
     */
    public void setPlantCapacity(Double plantCapacity) {
        this.plantCapacity = plantCapacity;
    }

    public String getLastActionName() {
        return lastActionName;
    }

    public void setLastActionName(String lastActionName) {
        this.lastActionName = lastActionName;
    }

    public Double getPricePerUnit() {
        return pricePerUnit;
    }

    public void setPricePerUnit(Double pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }

}
