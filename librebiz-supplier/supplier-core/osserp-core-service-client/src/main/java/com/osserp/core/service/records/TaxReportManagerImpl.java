/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 15, 2014 4:11:54 PM
 * 
 */
package com.osserp.core.service.records;

import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;

import com.osserp.core.dao.records.TaxReports;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.TaxReport;
import com.osserp.core.finance.TaxReportManager;
import com.osserp.core.service.impl.AbstractService;
import com.osserp.core.system.SystemCompany;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class TaxReportManagerImpl extends AbstractService implements TaxReportManager {

    private TaxReports taxReports;

    /**
     * Creates a new taxReport manager
     * @param taxReports
     */
    protected TaxReportManagerImpl(TaxReports taxReports) {
        super();
        this.taxReports = taxReports;
    }

    public List<TaxReport> getReports(SystemCompany company) {
        assert (company != null);
        return taxReports.getReports(company.getId());
    }

    public TaxReport getReport(Long id) {
        return taxReports.getReport(id);
    }

    public TaxReport create(Employee user, SystemCompany company, String name, Date start, Date end) throws ClientException {
        assert (user != null && company != null);
        if (start == null) {
            throw new ClientException(ErrorCode.DATE_START_REQUIRED);
        }
        if (end == null) {
            throw new ClientException(ErrorCode.DATE_STOP_REQUIRED);
        }
        List<TaxReport> existing = taxReports.getReports(company.getId());
        for (int i = 0, j = existing.size(); i < j; i++) {
            TaxReport next = existing.get(i);
            if (start.after(next.getStartDate()) && start.before(next.getStopDate())) {
                throw new ClientException(ErrorCode.PERIOD_START_ALREADY_BILLED);
            }
            if (end.after(next.getStartDate()) && end.before(next.getStopDate())) {
                throw new ClientException(ErrorCode.PERIOD_STOP_ALREADY_BILLED);
            }
        }
        return taxReports.create(user.getId(), company.getId(), name,
                company.getVatReportMethod(), start, end);
    }

    public TaxReport save(TaxReport report) {
        return taxReports.save(report);
    }
    
}
