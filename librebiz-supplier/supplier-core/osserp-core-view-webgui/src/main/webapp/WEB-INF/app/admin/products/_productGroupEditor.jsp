<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="productGroupEditorView" />
<style type="text/css">
.valueTable.first33 select {
    width: 50%;
}

.valueTable.first33 input[type="text"] {
    width: 50%;
    text-align: right;
}
</style>

<div class="modalBoxHeader">
    <div class="modalBoxHeaderLeft">
        <fmt:message key="${view.headerName}" />
    </div>
</div>
<div class="modalBoxData">

    <div class="subcolumns">
        <div class="subcolumn">
            <div class="spacer"></div>
            <c:import url="/errors/_errors.jsp" />

            <v:ajaxForm name="productGroupEditorForm" url="/admin/products/productGroupEditor/save">
                <table class="valueTable first33">
                    <tr>
                        <td><fmt:message key="name" /></td>
                        <td><v:text name="name" value="${view.bean.name}" style="width:99%; text-align:left;" /></td>
                    </tr>
                    <tr>
                        <td><select name="classification" size="1">
                                <c:forEach var="option" items="${applicationScope.productClasses}">
                                    <c:choose>
                                        <c:when test="${view.bean.classification.id == option.id}">
                                            <option value="<o:out value="${option.name}"/>" selected="selected"><o:out value="${option.name}" /></option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="<o:out value="${option.name}"/>"><o:out value="${option.name}" /></option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                        </select></td>
                        <td><fmt:message key="classification" /></td>
                    </tr>
                    <tr>
                        <td><oc:select name="quantityUnit" value="${view.bean.quantityUnit}" options="quantityUnits" prompt="false" /></td>
                        <td><fmt:message key="quantityUnit" /></td>
                    </tr>
                    <tr>
                        <td><oc:select name="powerUnit" value="${view.bean.powerUnit}" options="quantityUnits" prompt="false" /></td>
                        <td><fmt:message key="powerUnit" /></td>
                    </tr>
                    <tr>
                        <td><v:checkbox name="calculateSales" value="${view.bean.calculateSales}" /></td>
                        <td><fmt:message key="salePriceCalculation" /></td>
                    </tr>
                    <tr>
                        <td><v:number format="percent" name="consumerMargin" value="${view.bean.consumerMargin}" /> %</td>
                        <td><fmt:message key="consumerMargin" /></td>
                    </tr>
                    <tr>
                        <td><v:number format="percent" name="resellerMargin" value="${view.bean.resellerMargin}" /> %</td>
                        <td><fmt:message key="resellerMargin" /></td>
                    </tr>
                    <tr>
                        <td><v:number format="percent" name="partnerMargin" value="${view.bean.partnerMargin}" /> %</td>
                        <td><fmt:message key="partnerMargin" /></td>
                    </tr>
                    <tr>
                        <td><v:checkbox name="providingDatasheet" value="${view.bean.providingDatasheet}" /></td>
                        <td><fmt:message key="productDatasheetUploadable" /></td>
                    </tr>
                    <tr>
                        <td><v:checkbox name="providingPicture" value="${view.bean.providingPicture}" /></td>
                        <td><fmt:message key="productPictureUploadable" /></td>
                    </tr>
                    <tr>
                        <td><v:checkbox name="customNameSupport" value="${view.bean.customNameSupport}" /></td>
                        <td><fmt:message key="customNameSupport" /></td>
                    </tr>
                    <tr>
                        <td><v:checkbox name="gtin13Available" value="${view.bean.gtin13Available}" /></td>
                        <td><fmt:message key="gtin13" /></td>
                    </tr>
                    <tr>
                        <td><v:checkbox name="gtin14Available" value="${view.bean.gtin14Available}" /></td>
                        <td><fmt:message key="gtin14" /></td>
                    </tr>
                    <tr>
                        <td><v:checkbox name="gtin8Available" value="${view.bean.gtin8Available}" /></td>
                        <td><fmt:message key="gtin8" /></td>
                    </tr>
                    <tr>
                        <td class="row-submit"><v:ajaxLink url="/admin/products/productGroupEditor/exit" targetElement="${view.name}_popup">
                                <input class="cancel" type="button" value="<fmt:message key="exit"/>" />
                            </v:ajaxLink></td>
                        <td class="row-submit"><o:submit /></td>
                    </tr>
                </table>
            </v:ajaxForm>

            <div class="spacer"></div>
        </div>
    </div>
</div>