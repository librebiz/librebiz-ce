/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 17, 2009 11:09:34 AM 
 * 
 */
package com.osserp.common.mail;

import java.util.Date;

import com.osserp.common.Option;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface MailDomain extends Option {

    /**
     * Indicates that email addresses of this domain must be declared as alias
     * @return aliasOnly
     */
    boolean isAliasOnly();

    /**
     * Indicates that domain is configured for fetchmail
     * @return fetchmailDomain
     */
    boolean isFetchmailDomain();

    /**
     * Checks if fetchmail is configured completely
     * @return true if all required values set
     */
    boolean isFetchmailConfigured();

    /**
     * Provides the parent domain if this is alias domain only
     * @return parentDomain
     */
    MailDomain getParentDomain();

    /**
     * Provides the mail provider
     * @return provider
     */
    String getProvider();

    /**
     * Provides the fetchmail fetch mode for this domain
     * @return fetchMode
     */
    String getFetchMode();

    /**
     * Provides the name of the fetchmail server
     * @return serverName
     */
    String getServerName();

    /**
     * Provides the type of the fetchmail server
     * @return serverType
     */
    String getServerType();

    /**
     * Provides the name of the smtp server if differs from fetchmail server name
     * @return smtpServerName
     */
    String getSmtpServerName();

    /**
     * Provides the IP address of the domain host
     * @return hostIp
     */
    String getHostIp();

    /**
     * Provides the name of the mx host
     * @return hostMx
     */
    String getHostMx();

    /**
     * Provides the name of the secondary mx host
     * @return hostSecondaryMx
     */
    String getHostSecondaryMx();

    /**
     * Provides the IP address of the mx host
     * @return hostMxIp
     */
    String getHostMxIp();

    /**
     * Provides the IP address of the secondary mx host
     * @return hostSecondaryMxIp
     */
    String getHostSecondaryMxIp();

    /**
     * Provides the domain creation date
     * @return domainCreated
     */
    Date getDomainCreated();

    /**
     * Provides the domain expiry date
     * @return domainExpires
     */
    Date getDomainExpires();

    /**
     * Provides the whois entry
     * @return domainWhois
     */
    String getDomainWhois();

    /**
     * Updates domain values
     * @param user
     * @param provider
     * @param smtpServerName
     * @param hostIp
     * @param hostMx
     * @param hostSecondaryMx
     * @param hostMxIp
     * @param hostSecondaryMxIp
     * @param domainCreated
     * @param domainExpires
     */
    void update(
            Long user,
            String provider,
            String smtpServerName,
            String hostIp,
            String hostMx,
            String hostSecondaryMx,
            String hostMxIp,
            String hostSecondaryMxIp,
            Date domainCreated,
            Date domainExpires);
}
