/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 21-Jun-2005 19:27:37 
 * 
 */
package com.osserp.core.finance;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.osserp.common.ClientException;
import com.osserp.common.Option;

import com.osserp.core.Address;
import com.osserp.core.Item;
import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.employees.Employee;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductSelectionConfigItem;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Record extends FinanceRecord {

    static final Long STAT_NEW = 0L;
    static final Long STAT_CHANGED = 1L;
    static final Long STAT_PRINT = 2L;
    static final Long STAT_SENT = 3L;
    static final Long STAT_HISTORICAL = 4L;
    static final Long STAT_BOOKED = 6L;
    static final Long STAT_CLOSED = 10L;
    static final Long STAT_CANCELED = 99L;

    /**
     * Provides the delivery address if implementing record supports this
     * @return deliveryAddress
     */
    Address getDeliveryAddress();

    /**
     * Removes all values from the delivery address
     */
    void resetDeliveryAddress();

    /**
     * Updates the delivery address of the order
     * @param order
     * @param name
     * @param street
     * @param streetAddon
     * @param zipcode
     * @param city
     * @return updated values
     * @throws ClientException if any given value is empty or null
     */
    void updateDeliveryAddress(
            String name,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long country)
            throws ClientException;

    /**
     * Updates common values
     * @param signatureLeft
     * @param signatureRight
     * @param personId
     * @param note
     * @param taxFree
     * @param taxFreeId
     * @param taxRate
     * @param reducedTaxRate
     * @param currency
     * @param language
     * @param branchId
     * @param shippingId
     * @param customHeader
     * @param printComplimentaryClose
     * @param printProjectmanager
     * @param printSalesperson
     * @param printBusinessId
     * @param printBusinessInfo
     * @param printRecordDate
     * @param printRecordDateByStatus
     * @param printPaymentTarget
     * @param printConfirmationPlaceholder
     * @param printCoverLetter
     * @throws ClientException if validation failed
     */
    void update(
            Long signatureLeft,
            Long signatureRight,
            Long personId,
            String note,
            boolean taxFree,
            Long taxFreeId,
            Double taxRate,
            Double reducedTaxRate,
            Long currency,
            String language,
            Long branchId,
            Long shippingId,
            String customHeader,
            boolean printComplimentaryClose,
            boolean printProjectmanager,
            boolean printSalesperson,
            boolean printBusinessId,
            boolean printBusinessInfo,
            boolean printRecordDate,
            boolean printRecordDateByStatus,
            boolean printPaymentTarget,
            boolean printConfirmationPlaceholder,
            boolean printCoverLetter) throws ClientException;

    /**
     * Provides booking type if supported
     * @return bookingType
     */
    BookingType getBookingType();

    /**
     * Returns the status
     * @return status
     */
    Long getStatus();

    /**
     * Provides the last status changed date
     * @return statusDate
     */
    Date getStatusDate();

    /**
     * Updates the status and statusDate if required
     * @param status
     */
    void updateStatus(Long status);

    /**
     * Marks the record as changed by decreasing the status
     */
    void markChanged();

    /**
     * Provides the id of the employee whose signature will be printed left
     * @return signatureLeft
     */
    Long getSignatureLeft();

    /**
     * Provides the id of the employee whose signature will be printed right
     * @return signatureRight
     */
    Long getSignatureRight();

    /**
     * Provides the id of a contact person
     * @return personId
     */
    Long getPersonId();

    /**
     * Provides the shipping company id
     * @return shipping id
     */
    Long getShippingId();

    /**
     * Sets the shipping company id
     * @return shippingId
     */
    void setShippingId(Long shippingId);

    /**
     * Checks if any of relating products affects stock 
     * (e.g. product.deliveryNoteAffecting reports true).
     * @return true if any item affetcts stoc
     */
    boolean isItemsAffectStock();

    /**
     * Determins and indicates if change of items is disabled 
     * (e.g. items are locked). Method analyzes all properties
     * required to answer this (status, item-related flags, etc.)
     * @return true if itemsEditable and changeable are false 
     * and record is released (e.g. unchangeable).
     */
    boolean isItemsLocked();

    /**
     * Indicates that items are changeable by the application/user Default is false in this domain, 'cause items are added by other records or via calculation.
     * @return itemsChangeable
     */
    boolean isItemsChangeable();

    /**
     * Indicates that items are editable, e.g. change quantity and price even items not changeable. If this flag is set and changeable not set an item should be
     * changed but no items could be added or deleted.
     * @return itemsEditable
     */
    boolean isItemsEditable();

    /**
     * Indicates that items could be deleted in edit mode
     * @return itemsDeletable
     */
    boolean isItemsDeletable();

    /**
     * Adds an item
     * @param stockId
     * @param product
     * @param customName
     * @param quantity
     * @param taxRate
     * @param price
     * @param priceDate
     * @param partnerPrice
     * @param partnerPriceEditable
     * @param partnerPriceOverridden
     * @param purchasePrice
     * @param note
     * @param includePrice
     * @param externalId
     */
    void addItem(
            Long stockId,
            Product product,
            String customName,
            Double quantity,
            Double taxRate,
            BigDecimal price,
            Date priceDate,
            BigDecimal partnerPrice,
            boolean partnerPriceEditable,
            boolean partnerPriceOverridden,
            BigDecimal purchasePrice,
            String note,
            boolean includePrice,
            Long externalId);

    /**
     * Deletes an item
     * @param itemToDelete
     */
    void deleteItem(Item itemToDelete);

    /**
     * Disables itemChangeable and itemEditable flags
     */
    void disableItemChangeable();

    /**
     * Inserts item at location of item with itemId
     * @param itemToCut
     * @param itemToPasteToId
     */
    void pasteItem(Item itemToCut, Long itemToPasteToId);

    /**
     * Updates an item
     * @param item to update
     * @param customName
     * @param quantity
     * @param price
     * @param taxRate
     * @param stockId
     * @param note
     * @throws ClientException if quantity is null
     */
    void updateItem(
            Item item,
            String customName,
            Double quantity,
            BigDecimal price,
            Double taxRate,
            Long stockId,
            String note)
        throws ClientException;

    /**
     * Replaces the product of an item
     * @param item to update
     * @param product to set
     */
    void replaceItem(Item item, Product product);

    /**
     * Unlocks locked items by enabling itemEdit and itemChange properties. 
     * @param user
     */
    void unlockItems(Employee user);

    /**
     * Provides of all goods items of this record
     * @return items
     */
    List<Item> getItems();

    /**
     * Moves up an item list position
     * @param itemId
     */
    void moveUpItem(Long itemId);

    /**
     * Moves down an item list position
     * @param itemId
     */
    void moveDownItem(Long itemId);

    /**
     * Indicates whether record is unchangeable
     * @return unchangeable
     */
    boolean isUnchangeable();

    /**
     * Indicates that the record is canceled
     * @return canceled
     */
    boolean isCanceled();

    /**
     * Indicates that the record is closed
     * @return closed
     */
    boolean isClosed();

    /**
     * Indicates that this record is historical, e.g. record is only an informational copy of a record whose original is in a foreign previously used system.
     * @return historical
     */
    boolean isHistorical();

    /**
     * Indicates that this record is historical, e.g. record is only an informational copy of a record whose original is in a foreign previously used system.
     * @return internal
     */
    boolean isInternal();

    /**
     * Indicates that this record is a sales record
     * @return sales
     */
    boolean isSales();

    /**
     * Indicates that +/- item is avaiable
     * @return correctionAvailable
     */
    boolean isCorrectionAvailable();

    /**
     * Indicates that record is matching an product selection
     * @param selection
     * @return true if any item matches any selection
     */
    boolean matching(List<ProductSelectionConfigItem> selection);

    /**
     * Provides the language of the record
     * @return language
     */
    String getLanguage();

    /**
     * Provides a copy of all items as map of product id and items
     * @return goods
     */
    Map<Long, Item> getGoods();
    
    /**
     * Indicates if this record contains any item referenced to an product
     * that is also contained in other record items. 
     * @param other
     * @return true if any product of this record also exists in other.
     */
    boolean containsProductsOf(Record other);
    
    /**
     * Indicates if this record contains a dedicated product
     * @param productId
     * @return true if any item references provided product
     */
    boolean containsProduct(Long productId);

    /**
     * The amounts of the record
     * @return amounts
     */
    Amounts getAmounts();

    /**
     * The summary
     * @return summary
     */
    Amounts getSummary();

    /**
     * Provides the date when record was sent
     * @return sentDate
     */
    Date getDateSent();

    /**
     * Sets the date when record was sent
     * @param date
     */
    void setDateSent(Date dateSent);

    /**
     * Provides the mail log for the record. Uses last log entry if record 
     * was sent multiple times
     * @return mailLog or null if record wasn't sent
     */
    RecordMailLog getMailLog();

    /**
     * Provides all mail log for the record
     * @return mailLogs
     */
    List<RecordMailLog> getMailLogs();

    /**
     * Indicates if a complimentary close should be print on document creation
     * @return printComplimentaryClose
     */
    boolean isPrintComplimentaryClose();

    /**
     * Enables/disables print complimentary close flag
     * @param printComplimentaryClose
     */
    void setPrintComplimentaryClose(boolean printComplimentaryClose);

    /**
     * Indicates if project manager data should be print on document creation
     * @return printProjectmanager
     */
    boolean isPrintProjectmanager();

    /**
     * Enables/disables print project manager flag
     * @param printProjectmanager
     */
    void setPrintProjectmanager(boolean printProjectmanager);

    /**
     * Indicates if salesperson data should be print on document creation
     * @return printSalesperson
     */
    boolean isPrintSalesperson();

    /**
     * Enables/disables print salesperson flag
     * @param printSalesperson
     */
    void setPrintSalesperson(boolean printSalesperson);

    /**
     * Indicates if businessCase id should be print on document creation
     * @return printBusinessCaseId
     */
    boolean isPrintBusinessCaseId();

    /**
     * Enables/disables printBusinessCaseId flag
     * @param printBusinessCaseId
     */
    void setPrintBusinessCaseId(boolean printBusinessCaseId);

    /**
     * Indicates if businessCase info should be print on document creation
     * @return printBusinessCaseInfo
     */
    boolean isPrintBusinessCaseInfo();

    /**
     * Enables/disables printBusinessCaseInfo flag
     * @param printBusinessCaseInfo
     */
    void setPrintBusinessCaseInfo(boolean printBusinessCaseInfo);

    /**
     * Indicates if an confirmation placeholder should be print if available
     * @return printConfirmationPlaceholder
     */
    boolean isPrintConfirmationPlaceholder();

    /**
     * Sets if an confirmation placeholder should be print if available
     * @param printConfirmationPlaceholder
     */
    void setPrintConfirmationPlaceholder(boolean printConfirmationPlaceholder);

    /**
     * Embed existing cover letter on print 
     * @return false by default
     */
    boolean isPrintCoverLetter();

    /**
     * @param printCoverLetter
     */
    void setPrintCoverLetter(boolean printCoverLetter);

    /**
     * Indicates if record date should be print on document creation
     * @return printRecordDate
     */
    boolean isPrintRecordDate();

    /**
     * Enables/disables print record date flag
     * @param printRecordDate
     */
    void setPrintRecordDate(boolean printRecordDate);

    /**
     * Indicates if record date should be determined by status on document creation
     * @return printRecordDateByStatus
     */
    boolean isPrintRecordDateByStatus();

    /**
     * Enables/disables print record date by status flag
     * @param printRecordDateByStatus
     */
    void setPrintRecordDateByStatus(boolean printRecordDateByStatus);

    /**
     * Indicates if automatic payment target should be print. Users may enable this flag if individual notes does not contain a dedicated payment target
     * statement.
     * @return printPaymentTarget
     */
    boolean isPrintPaymentTarget();

    /**
     * Enables/disables automatic payment target printing.
     * @param printPaymentTarget
     */
    void setPrintPaymentTarget(boolean printPaymentTarget);

    /**
     * Indicates if payment note should be print.
     * @return printPaymentNote
     */
    boolean isPrintPaymentNote();

    /**
     * Enables/disables payment note printing.
     * @param printPaymentNote
     */
    void setPrintPaymentNote(boolean printPaymentNote);

    /**
     * Indicates if item with reduced or zero tax exists
     * @return reducedTaxAvailable
     */
    boolean isReducedTaxAvailable();

    /**
     * Changes the related contact
     * @param relatedContact
     * @param contactChangedBy
     */
    void changeContact(ClassifiedContact relatedContact, Long contactChangedBy);

    /**
     * Provides some text infos for print
     * @return infos
     */
    List<RecordInfo> getInfos();

    /**
     * Adds default infos
     * @param user
     * @param infos
     */
    void addInfos(Employee user, List<Long> infos);

    /**
     * Adds a new info
     * @param user
     * @param info
     */
    void addInfo(Employee user, Option info);

    /**
     * Removes all infos
     */
    void clearInfos();

    /**
     * Moves an info list position upwards
     * @param info to move up
     */
    void moveInfoUp(RecordInfo info);
    
    /**
     * Moves an info list position down
     * @param info to move down
     */
    void moveInfoDown(RecordInfo info);

    /**
     * Removes an info
     * @param info
     */
    void removeInfo(RecordInfo info);

    /**
     * Provides the initial created date if created was overridden
     * @return initialDate
     */
    Date getInitialDate();

    /**
     * Updates created date of the record
     * @param date
     */
    void updateCreatedDate(Date date);
}
