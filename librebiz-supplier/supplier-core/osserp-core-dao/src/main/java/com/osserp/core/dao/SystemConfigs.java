/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 12, 2006 2:03:34 PM 
 * 
 */
package com.osserp.core.dao;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionConfig;
import com.osserp.common.Property;
import com.osserp.common.gui.Menu;

import com.osserp.core.system.BranchSelection;
import com.osserp.core.system.CacheStatus;
import com.osserp.core.system.SystemKey;
import com.osserp.core.system.SystemKeyType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SystemConfigs {

    /**
     * Indicates if a system property is enabled
     * @param name
     * @return true if system property is yes or true
     */
    boolean isSystemPropertyEnabled(String name);

    /**
     * Provides value of a system property
     * @param name
     * @return value or null if not exists
     */
    String getSystemProperty(String name);

    /**
     * Provides value of a system property as id value
     * @param name
     * @return value or null if not exists
     */
    Long getSystemPropertyId(String name);

    /**
     * Provides all properties
     * @return properties
     */
    List<Property> getProperties();

    /**
     * Switches the value of a boolean property.
     * @param name
     */
    void switchBooleanProperty(String name);

    /**
     * Updates the value of a string property
     * @param name
     * @param value
     */
    void updateProperty(String name, String value);

    /**
     * Provides the latest status of a system cache
     * @param cacheName
     * @return status
     */
    CacheStatus getCacheStatus(String cacheName);

    /**
     * Creates a new cache status entry
     * @param cacheName
     */
    void createCacheRefresh(String cacheName);

    /**
     * Closes an open cache status
     * @param status
     */
    void closeCacheRefresh(CacheStatus status);

    /**
     * Provides all available menus for the application
     * @return menus
     */
    List<Menu> getMenus();

    /**
     * Provides a branch selection
     * @param id of selection
     * @return branchSelection
     */
    BranchSelection getBranchSelection(Long id);

    /**
     * Provides a branch selection
     * @param name of selection
     * @return branchSelection
     */
    BranchSelection getBranchSelection(String name);

    /**
     * Provides all branch selections
     * @return branchSelections
     */
    List<BranchSelection> getBranchSelections();

    /**
     * Provides a permission config by name
     * @param name
     * @return permissionConfig or null if not exists
     */
    PermissionConfig getPermissionConfig(String name);

    /**
     * Provides available key types
     * @return key types
     */
    List<SystemKeyType> getKeyTypes();

    /**
     * Provides available keys with encoded keydata
     * @return available keys
     */
    List<SystemKey> getSecuredKeys();

    /**
     * Gets a full qualified key with secured keydata 
     * @param contextName
     * @param name
     * @return key by context and name or null if not exists
     */
    SystemKey getSecuredKey(String contextName, String name);

    /**
     * Provides the plain text key
     * @param contextName
     * @param name
     * @return plain key
     */
    String getKeyValue(String contextName, String name);

    /**
     * Creates or updates a key. This method also creates a new key type
     * if no matching type can be found. 
     * @param contextName
     * @param name
     * @param plainkey
     * @throws ClientException if password missing
     */
    void saveKey(String contextName, String name, String plainkey) throws ClientException;

    /**
     * Creates or updates a key by provided type selection.
     * @param type
     * @param name
     * @param plainkey
     * @throws ClientException if password missing
     */
    void saveKey(SystemKeyType type, String name, String plainkey) throws ClientException;
}
