/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 2, 2009 10:35:11 AM 
 * 
 */
package com.osserp.core.model.products;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.osserp.common.beans.AbstractOption;

import com.osserp.core.employees.Employee;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductCategory;
import com.osserp.core.products.ProductGroup;
import com.osserp.core.products.ProductSelectionConfig;
import com.osserp.core.products.ProductSelectionConfigItem;
import com.osserp.core.products.ProductSelectionContext;
import com.osserp.core.products.ProductType;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.system.SystemCompany;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractProductSelectionConfig extends AbstractOption implements ProductSelectionConfig {

    private SystemCompany company = null;
    private BranchOffice branch = null;
    private ProductSelectionContext context = null;
    private boolean exclusion = false;
    private boolean disabled = false;
    private boolean movable = false;
    private List<ProductSelectionConfigItem> items = new ArrayList<ProductSelectionConfigItem>();

    protected AbstractProductSelectionConfig() {
        super();
    }

    protected AbstractProductSelectionConfig(
            Employee user,
            ProductSelectionContext context,
            String name,
            String description) {
        super(name, description, user.getId());
        this.context = context;
    }

    protected AbstractProductSelectionConfig(ProductSelectionConfig config) {
        super(config.getId(), config.getName(), config.getDescription(), (String) null, config.getCreatedBy());
        context = config.getContext();
        branch = config.getBranch();
        company = config.getCompany();
        exclusion = config.isExclusion();
        disabled = config.isDisabled();
    }

    public abstract void addItem(
            Employee user,
            ProductType type,
            ProductGroup group,
            ProductCategory category);

    public void removeItem(Long id) {
        for (Iterator<ProductSelectionConfigItem> i = items.iterator(); i.hasNext();) {
            ProductSelectionConfigItem next = i.next();
            if (next.getId().equals(id)) {
                i.remove();
            }
        }
    }

    public final boolean isMatching(Product product) {
        if (items == null || items.isEmpty()) {
            if (disabled) {
                return false;
            }
            return true;
        }
        boolean matching = false;
        List<ProductSelectionConfigItem> excludes = new ArrayList<ProductSelectionConfigItem>();
        for (int i = 0, j = items.size(); i < j; i++) {
            ProductSelectionConfigItem next = items.get(i);
            if (next.isExclusion()) {
                excludes.add(next);
            } else if (next.isMatching(product)) {
                matching = true;
                // don't break -> complete exclusion collection
            }
        }
        if (matching && !excludes.isEmpty()) {
            for (int i = 0, j = excludes.size(); i < j; i++) {
                ProductSelectionConfigItem next = excludes.get(i);
                if (next.isMatching(product)) {
                    matching = false;
                    break;
                }
            }
        }
        return matching;
    }

    public SystemCompany getCompany() {
        return company;
    }

    public void setCompany(SystemCompany company) {
        this.company = company;
    }

    public BranchOffice getBranch() {
        return branch;
    }

    public void setBranch(BranchOffice branch) {
        this.branch = branch;
    }

    public ProductSelectionContext getContext() {
        return context;
    }

    public void setContext(ProductSelectionContext context) {
        this.context = context;
    }

    public boolean isExclusion() {
        return exclusion;
    }

    public void setExclusion(boolean exclusion) {
        this.exclusion = exclusion;
    }

    public int getItemCount() {
        return items.size();
    }

    public List<ProductSelectionConfigItem> getItems() {
        return items;
    }

    protected void setItems(List<ProductSelectionConfigItem> items) {
        this.items = items;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public boolean isMovable() {
        return movable;
    }

    public void setMovable(boolean movable) {
        this.movable = movable;
    }
}
