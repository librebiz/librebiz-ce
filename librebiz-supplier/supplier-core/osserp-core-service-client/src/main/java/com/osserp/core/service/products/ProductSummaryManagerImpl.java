/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 4, 2010 2:43:43 PM 
 * 
 */
package com.osserp.core.service.products;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.core.dao.Products;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductSummaryManager;
import com.osserp.core.service.impl.AbstractService;
import com.osserp.core.tasks.ProductPlanningCacheSender;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProductSummaryManagerImpl extends AbstractService implements ProductSummaryManager {
    private static Logger log = LoggerFactory.getLogger(ProductSummaryManagerImpl.class.getName());
    private Products products = null;
    private ProductPlanningCacheSender planningCacheTask = null;

    public ProductSummaryManagerImpl(Products products, ProductPlanningCacheSender productPlanningCacheSender) {
        super();
        this.products = products;
        this.planningCacheTask = productPlanningCacheSender;
    }

    public void refreshPlanningCache() {
        if (log.isDebugEnabled()) {
            log.debug("refreshPlanningCache() invoked...");
        }
        planningCacheTask.send();
    }

    public void reloadSummary(Product product) {
        products.loadSummary(product);
    }
}
