/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 29-Jul-2005 08:53:02 
 * 
 */
package com.osserp.core.finance;

import com.osserp.common.Option;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface PaymentCondition extends Option {

    static final Integer DEFAULT_TARGET = 7;
    static final int MAX_TARGET = 30;

    static final Long DEFAULT_SALES_CONDITION = 2L;
    static final Long DEFAULT_PURCHASE_CONDITION = 8L;

    Integer getPaymentTarget();

    void setPaymentTarget(Integer paymentTarget);

    boolean isDonation();

    void setDonation(boolean donation);

    boolean isPaid();

    void setPaid(boolean paid);

    boolean isWithDiscount();

    void setWithDiscount(boolean withDiscount);

    Double getDiscount1();

    void setDiscount1(Double discount1);

    Integer getDiscount1Target();

    void setDiscount1Target(Integer discount1Target);

    Double getDiscount2();

    void setDiscount2(Double discount2);

    Integer getDiscount2Target();

    void setDiscount2Target(Integer discount2Target);

    String getLanguage();

    void setLanguage(String language);

    boolean isDefaultCondition();

    void setDefaultCondition(boolean defaultCondition);

}
