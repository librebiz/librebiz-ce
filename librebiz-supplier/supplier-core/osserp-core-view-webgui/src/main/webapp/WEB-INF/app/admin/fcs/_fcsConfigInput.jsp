<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>


<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for="name"><fmt:message key="actionName" /></label>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <v:text name="name" styleClass="form-control" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for="description"><fmt:message key="description" /></label>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <v:textarea name="description" styleClass="form-control" rows="4" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for="responsible"><fmt:message key="responsible" /></label>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <oc:select name="groupId" styleClass="form-control" options="${view.employeeGroups}" />
        </div>
    </div>
</div>
