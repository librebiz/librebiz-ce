<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="productPriceByQuantityConfiguratorView"/>

<style type="text/css">

.modalBoxHeaderLeft {
        margin-left: 15px;
}
td,th {
        text-align: right;
}
.margin {
        width: 95px;
}
.marginInput {
        width: 60px;
        text-align:right;
}

</style>

<div class="modalBoxHeader">
	<div class="modalBoxHeaderLeft">
		<fmt:message key="productPriceByQuantityConfig"/>
	</div>
</div>

<div class="modalBoxData">

	<div class="subcolumns">
		<div class="subcolumn">
			<div class="spacer"></div>
			<v:ajaxForm name="dynamicForm" targetElement="${view.name}_popup" url="/products/productPriceByQuantityConfigurator/save">
			<table class="table table-striped">
				<thead>
					<tr>
						<th><fmt:message key="id"/></th>
						<th><fmt:message key="rangeStart"/></th>
						<th><fmt:message key="rangeEnd"/></th>
						<th class="margin"><fmt:message key="consumerMinimumMarginTableKey"/></th>
						<th class="margin"><fmt:message key="resellerMinimumMarginTableKey"/></th>
						<th class="margin"><fmt:message key="partnerMinimumMarginTableKey"/></th>
						<th><fmt:message key="action"/></th>
					</tr>
				</thead>
				<tbody>
				<c:choose>
					<c:when test="${!empty view.bean}">
						<tr>
							<td class="center"><o:out value="${view.bean.id}"/></td>
							<td><o:number value="${view.bean.rangeStart}" format="integer"/></td>
							<td><o:number value="${view.bean.rangeEnd}" format="integer"/></td>
							<td><input type="text" class="marginInput" name="consumerMargin" value="<o:number value="${view.bean.consumerMinimumMargin}" format="percent"/>" id="consumerMinimumMargin" /></td>
							<td><input type="text" class="marginInput" name="resellerMargin" value="<o:number value="${view.bean.resellerMinimumMargin}" format="percent"/>" id="resellerMinimumMargin" /></td>
							<td><input type="text" class="marginInput" name="partnerMargin" value="<o:number value="${view.bean.partnerMinimumMargin}" format="percent"/>" id="partnerMinimumMargin" /></td>
							<td>
								<v:ajaxLink url="/products/productPriceByQuantityConfigurator/select" targetElement="${view.name}_popup"><o:img name="backIcon"/></v:ajaxLink>
								<input type="image" name="method" src="<o:icon name="saveIcon"/>" value="save" title="<fmt:message key='saveInput'/>"/>
							</td>
						</tr>
					</c:when>
					<c:otherwise>
					<c:forEach var="range" items="${view.list}">
						<tr>
							<td class="center"><o:out value="${range.id}"/></td>
							<td>
								<v:ajaxLink url="/products/productPriceByQuantityConfigurator/select?id=${range.id}" targetElement="${view.name}_popup">
									<o:number value="${range.rangeStart}" format="integer"/>
								</v:ajaxLink>
							</td>
							<c:choose>
							<c:when test="${range.last}">
							<td>
								<input type="text" class="marginInput" name="range" value="<o:number value="${range.rangeEnd}" format="integer"/>" id="range" />
							</td>
							<td><o:number value="${range.consumerMinimumMargin}" format="percent"/></td>
							<td><o:number value="${range.resellerMinimumMargin}" format="percent"/></td>
							<td><o:number value="${range.partnerMinimumMargin}" format="percent"/></td>
							<td>
								<input type="image" name="method" src="<o:icon name="saveIcon"/>" value="save" title="<fmt:message key='saveInput'/>"/>
								<c:if test="${!range.initial}">
								<v:ajaxLink url="/products/productPriceByQuantityConfigurator/delete" targetElement="${view.name}_popup"><o:img name="deleteIcon"/></v:ajaxLink>
								</c:if>
							</td>
							</c:when>
							<c:otherwise>
							<td><o:number value="${range.rangeEnd}" format="integer"/></td>
							<td><o:number value="${range.consumerMinimumMargin}" format="percent"/></td>
							<td><o:number value="${range.resellerMinimumMargin}" format="percent"/></td>
							<td><o:number value="${range.partnerMinimumMargin}" format="percent"/></td>
							<td> </td>
							</c:otherwise>
							</c:choose>
						</tr>
					</c:forEach>
					</c:otherwise>
				</c:choose>
				</tbody>
			</table>
			</v:ajaxForm>
			<div class="spacer"></div>
		</div>
	</div>
</div>
				