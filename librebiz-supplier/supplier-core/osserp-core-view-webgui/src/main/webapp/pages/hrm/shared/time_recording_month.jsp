<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>

<c:set var="recordingView" value="${requestScope.timeRecordingView}"/>
<c:set var="recording" value="${recordingView.timeRecording}"/>
<c:set var="period" value="${recording.selectedPeriod}"/>
<c:set var="config" value="${period.config}"/>
<c:set var="selectedMonth" value="${period.selectedYear.selectedMonth}"/>
<c:set var="hrmPermissionGrant" value="false"/>

<c:if test="${!empty period.records and !empty selectedMonth.days}">
<o:logger write="${'selected month found '}${selectedMonth.month}" level="debug"/>
<o:permission role="hrm,disciplinarian_view,client_edit">
    <c:set var="hrmPermissionGrant" value="true"/>
</o:permission>

<c:choose>
    <c:when test="${recordingView.listDescendentMode}">
        <c:set var="days" value="${selectedMonth.reverseDays}"/>
    </c:when>
    <c:otherwise>
        <c:set var="days" value="${selectedMonth.days}"/>
    </c:otherwise>
</c:choose>

<div class="table-responsive table-responsive-default">
    <table class="table table-striped">
        <thead>
            <tr>
                <th><fmt:message key="date"/></th>
                <th><fmt:message key="time"/></th>
                <th><fmt:message key="info"/></th>
                <th><fmt:message key="hours"/></th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="day" items="${days}">
                <c:if test="${!empty day.startTime || !empty day.name}">
                    <tr class="altrow">
                        <td>
                            <a href="<c:url value="/timeRecordings.do?method=toggleListDetailsMode"/>"><o:out value="${day.dayDisplayKey}"/></a>
                            <span> </span>
                            <a href="<c:url value="/timeRecordings.do?method=toggleListDescendentMode"/>"><o:date value="${day.date}"/></a>
                        </td>
                        <td>
                            <c:if test="${!empty day.startTime}">
                                <o:time value="${day.startTime}"/><span> - </span>
                                <c:choose>
                                    <c:when test="${empty day.stopTime}">
                                        <fmt:message key="emptyTimeDisplay"/>
                                    </c:when>
                                    <c:otherwise>
                                        <o:time value="${day.stopTime}"/>
                                    </c:otherwise>
                                </c:choose>
                            </c:if>
                        </td>
                        <c:choose>
                            <c:when test="${!empty day.startTime}">
                                <td>
                                    <o:out value="${day.workingHoursDisplay}"/>
                                    <c:if test="${day.minutesBreak > 0}">
                                        <span style="margin-left: 5px;">(PA-<o:out value="${day.minutesBreak}"/>)</span>
                                    </c:if>
                                </td>
                            </c:when>
                            <c:otherwise>
                                <td><o:out value="${day.name}"/></td>
                            </c:otherwise>
                        </c:choose>
                        <c:choose>
                            <c:when test="${day.minutesDifference < 0}">
                                <td class="bolderror">
                                    <c:if test="${!empty day.startTime}">
                                        <o:out value="${day.hoursDisplay}"/>
                                    </c:if>
                                </td>
                            </c:when>
                            <c:otherwise>
                                <td class="boldtext">
                                    <c:if test="${!empty day.startTime}">
                                        <o:out value="${day.hoursDisplay}"/>
                                    </c:if>
                                </td>
                            </c:otherwise>
                        </c:choose>
                    </tr>
                    <c:if test="${recordingView.listDetailsMode}">
                        <c:forEach var="record" items="${day.records}">
                            <c:if test="${record.type.visible }">
                                <tr>
                                    <td></td>
                                    <td>
                                        <o:time value="${record.value}"/><span> - </span>
                                        <c:choose>
                                            <c:when test="${empty record.closedBy}">
                                                <fmt:message key="emptyTimeDisplay"/>
                                            </c:when>
                                            <c:otherwise>
                                                <o:time value="${record.closedBy.value}"/>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td>
                                        <span title="<fmt:message key="type"/> <o:out value="${record.type.id}"/>">
                                            <c:choose>
                                                <c:when test="${empty record.note}">
                                                    <c:choose>
                                                        <c:when test="${record.type.defaultType}">
                                                            <fmt:message key="comingAndGoingLabel"/>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <o:out value="${record.type.name}"/>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:when>
                                                <c:otherwise>
                                                    <a href="javascript:void(0)" onmouseover="Tip('<o:out value="${record.note}"/>',TITLE, '<fmt:message key="comment"/>')" onmouseout="UnTip()">
                                                        <c:choose>
                                                            <c:when test="${record.type.defaultType}">
                                                                <fmt:message key="comingAndGoingLabel"/>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <o:out value="${record.type.name}"/>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </a>
                                                </c:otherwise>
                                            </c:choose>
                                        </span>
                                    </td>
                                    <td><o:out value="${record.durationDisplay}"/></td>
                                </tr>
                            </c:if>
                        </c:forEach>
                    </c:if>
                </c:if>
            </c:forEach>
        </tbody>
    </table>
</div>
</c:if>
