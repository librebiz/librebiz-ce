/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 21, 2007 11:56:12 AM 
 * 
 */
package com.osserp.common.beans;

import java.util.Date;

import org.jdom2.Element;

import com.osserp.common.Note;
import com.osserp.common.xml.JDOMUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class NoteImpl extends AbstractEntity implements Note {

    private String note;

    public NoteImpl() {
        super();
    }

    public NoteImpl(
            Long id,
            Long reference,
            Date created,
            Long createdBy,
            String note) {
        super(id, reference, created, createdBy);
        this.note = note;
    }

    /**
     * Constructor for orm
     * @param note
     * @param createdBy
     */
    public NoteImpl(String note, Long createdBy) {
        super((Long) null, (Long) null, createdBy);
        this.note = note;
    }

    /**
     * Constructor for orm
     * @param note
     * @param createdBy
     */
    public NoteImpl(Long reference, String note, Long createdBy) {
        super((Long) null, reference, createdBy);
        this.note = note;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public void updateNote(Long user, String note) {
        setNote(note);
        updateChanged(user);
    }

    @Override
    public Element getXML() {
        return getXML("root");
    }

    @Override
    public Element getXML(String rootElementName) {
        Element root = super.getXML(rootElementName);
        root.addContent(JDOMUtil.createElement("note", note));
        return root;
    }
}
