/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 13, 2006 4:07:15 PM 
 * 
 */
package com.osserp.common.dms.dao.impl;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.User;
import com.osserp.common.dao.Tables;
import com.osserp.common.dms.DmsDocument;
import com.osserp.common.dms.DmsReference;
import com.osserp.common.dms.DocumentType;
import com.osserp.common.dms.dao.DocumentEncoder;
import com.osserp.common.dms.dao.DocumentReference;
import com.osserp.common.dms.dao.Documents;
import com.osserp.common.service.SecurityService;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class DatabaseDocumentsDao extends AbstractDocumentsDao implements Documents {

    /**
     * Constructor for document daos
     * @param jdbcTemplate for document objects
     * @param tables
     * @param sessionFactory
     * @param documentTypeId
     * @param documentTableKey for document table
     * @param imageTemplate for images
     * @param imageTableKey for image table
     * @param documentEncoder
     * @param authenticationProvider
     */
    public DatabaseDocumentsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            Long documentTypeId,
            String documentTableKey,
            String categoryTableKey,
            JdbcTemplate imageTemplate,
            String imageTableKey,
            DocumentEncoder documentEncoder, 
            SecurityService authenticationProvider) {
        super(jdbcTemplate,
                tables,
                sessionFactory,
                documentTypeId,
                documentTableKey,
                categoryTableKey,
                new DatabaseDocumentStorage(imageTemplate, tables, imageTableKey),
                documentEncoder,
                authenticationProvider);
    }

    public DmsDocument create(User user, DmsReference reference, DmsDocument imported) {
        throw new IllegalStateException("method not supported");
    }

    @Override
    public int countByType(DocumentType documentType) {
        StringBuilder sql = new StringBuilder("SELECT count(*) FROM ");
        sql
                .append(getDocumentTable())
                .append(" WHERE doc_type_id = ")
                .append(documentType.getId());
        return jdbcTemplate.queryForObject(sql.toString(), Integer.class);
    }

    @Override
    protected DocumentReference getDocumentReference(DmsDocument document) {
        return new DatabaseDocumentReference(document.getId());
    }

    @Override
    protected DocumentReference getThumbnailReference(DmsDocument document) {
        return null;
    }

    public void createThumbnails(DocumentType documentType, boolean forceOverride) {
        // default does nothing for this document type
    }
}
