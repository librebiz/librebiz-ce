/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 9, 2005 3:09:18 PM 
 * 
 */
package com.osserp.core.calc;

import java.math.BigDecimal;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.Editor;
import com.osserp.core.BusinessCase;
import com.osserp.core.Item;
import com.osserp.core.ItemPosition;
import com.osserp.core.sales.SalesRevenue;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Calculator extends Editor {

    /**
     * Provides the referenced business case
     * @return businessCase
     */
    BusinessCase getBusinessCase();

    /**
     * Provides the sales revenue if available
     * @return revenue
     */
    SalesRevenue getRevenue();

    /**
     * The calculation
     * @return calculation
     */
    Calculation getCalculation();

    /**
     * Indicates that an item was added or changed
     * @return true if so
     */
    boolean isChanged();

    /**
     * Adds products to the position set by one of the setProducts() methods
     * @param selectedPosition
     * @param selectedItem if an product should be replaced or null to add
     * @param productIds to add
     * @param customNames
     * @param quantities of products
     * @param prices of the products
     * @param notes for the products
     * @throws ClientException if no quantity is set or more than one product with quantity > 0 found in replacement mode
     */
    List<Long> addProducts(
            ItemPosition selectedPosition,
            Item selectedItem,
            Long[] productIds,
            String[] customNames,
            String[] quantities,
            String[] prices,
            String[] notes)
            throws ClientException;

    /**
     * Adds a discount to calculation based on given item
     * @param itemToDiscount
     * @throws ClientException if nothing discountable found
     */
    void addDiscount(Long itemToDiscount) throws ClientException;

    /**
     * Deletes an product from a layout position
     * @param itemId to delete
     */
    void deleteProduct(Long itemId);

    /**
     * Changes selected items price display status
     * @param itemId where to change the status
     * @throws ClientException if price should be disabled and item does not support this
     */
    void changePriceDisplayStatus(Long itemId) throws ClientException;

    /**
     * Enables price display of all nullable items
     * @throws ClientException if operation not supported in current context
     */
    void enablePriceDisplayStatus() throws ClientException;

    /**
     * Disables price display of all nullable items
     * @throws ClientException if operation not supported in current context
     */
    void disablePriceDisplayStatus() throws ClientException;

    /**
     * Gets the previously selected item
     * @return item or null if not found
     */
    Item getSelectedItem();

    /**
     * Selects an item for further edit
     * @param id of the item
     */
    void setSelectedItem(Long id);

    /**
     * Updates the item with the specified id
     * @param customName
     * @param quantity
     * @param partnerPrice
     * @param partnerPriceOverridden
     * @param price
     * @param note
     * @throws ClientException if quantity is not set (null or 0.0)
     */
    void updateItem(String customName, Double quantity, BigDecimal partnerPrice, boolean partnerPriceOverridden, BigDecimal price, String note)
            throws ClientException;

    /**
     * Moves up an item
     * @param itemId
     */
    void moveUpItem(Long itemId);

    /**
     * Moves down an item
     * @param itemId
     */
    void moveDownItem(Long itemId);

    /**
     * Creates option positions
     */
    void createOptions() throws ClientException;

    /**
     * Indicates if we calculate options
     * @return optionMode
     */
    boolean isOptionMode();

    /**
     * Enables or disables option mode to calculate options
     * @param optionMode
     */
    void setOptionMode(boolean optionMode);

    /**
     * Calculates the calculation and unlocks it on success
     * @throws ClientException if calculation is invalid due to validation errors
     */
    void calculate() throws ClientException;

    /**
     * Disables an existing lock
     */
    void disableLock();

    /**
     * Sets calculation as changed
     */
    void markChanged();

    /**
     * Indicates that product listing should include lazy products
     * @return includeLazyProducts
     */
    boolean isIncludeLazyProducts();

    /**
     * Enables/disables lazy product including
     * @param includeLazyProducts
     */
    void setIncludeLazyProducts(boolean includeLazyProducts);

    /**
     * Refresh the calculation in calculator
     */
    void refresh();

    /**
     * Changes currently selected calculator
     * @param name
     * @return calculation with updated values
     */
    Calculation changeCalculator(String name);

    /**
     * Updates calculation if changed outside of calculator
     * @param calc
     */
    void updateCalculation(Calculation calc);
}
