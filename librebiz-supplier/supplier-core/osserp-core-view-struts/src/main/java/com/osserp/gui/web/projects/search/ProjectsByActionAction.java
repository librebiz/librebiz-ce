/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 13 Apr 2007 16:16:04 
 * 
 */
package com.osserp.gui.web.projects.search;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ActionException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.View;
import com.osserp.common.web.struts.StrutsForm;
import com.osserp.gui.client.projects.ProjectsByActionView;
import com.osserp.gui.web.struts.AbstractViewDispatcherAction;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProjectsByActionAction extends AbstractViewDispatcherAction {
    private static Logger log = LoggerFactory.getLogger(ProjectsByActionAction.class.getName());

    /**
     * Sets request type and reloads related actions from backend
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward selectType(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("selectType() request from " + getUser(session).getId());
        }
        ProjectsByActionView view = fetchSearchView(session);
        if (view == null) {
            view = (ProjectsByActionView) createView(request);
        }
        view.selectType(RequestUtil.getId(request));
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Searchs projects by actions
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward searchByAction(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("searchByAction() request from " + getUser(session).getId());
        }
        ProjectsByActionView view = getSearchView(session);
        try {
            view.save(new StrutsForm(form));
            view.executeByAction();
            return mapping.findForward(view.getListTarget());
        } catch (Exception e) {
            saveError(request, e.getMessage());
            return mapping.findForward(view.getActionTarget());
        }
    }

    /**
     * Searchs projects by missing action
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward searchByMissingAction(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("searchByMissingAction() request from " + getUser(session).getId());
        }
        ProjectsByActionView view = getSearchView(session);
        try {
            view.save(new StrutsForm(form));
            view.executeByMissingAction();
            return mapping.findForward(view.getListTarget());
        } catch (Exception e) {
            saveError(request, e.getMessage());
            return mapping.findForward(view.getActionTarget());
        }
    }

    /**
     * Refreshs and redisplays current list
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward refresh(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("refresh() request from " + getUser(request).getId());
        }
        ProjectsByActionView view = getSearchView(session);
        view.refresh();
        return mapping.findForward(view.getListTarget());
    }

    /**
     * Return from action list to search
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward exitList(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exitList() request from " + getUser(session).getId());
        }
        ProjectsByActionView view = getSearchView(session);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Provides xml represantation of customers
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward customerXml(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("customerXml() request from " + getUser(session).getId());
        }
        ProjectsByActionView view = getSearchView(session);
        return forwardXmlOutput(mapping, request, view.createCustomerXml());
    }

    /**
     * Provides xml represantation of project information
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward salesXml(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("salesXml() request from " + getUser(session).getId());
        }
        ProjectsByActionView view = getSearchView(session);
        return forwardXmlOutput(mapping, request, view.createActionXml());
    }

    @Override
    protected Class<? extends View> getViewClass() {
        return ProjectsByActionView.class;
    }

    protected ProjectsByActionView fetchSearchView(HttpSession session) throws PermissionException {
        return (ProjectsByActionView) fetchView(session);
    }

    protected ProjectsByActionView getSearchView(HttpSession session) throws PermissionException {
        ProjectsByActionView view = fetchSearchView(session);
        if (view == null) {
            log.warn("getSearchView() failed, view not bound!");
            throw new ActionException();
        }
        return view;
    }
}
