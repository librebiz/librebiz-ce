/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 18, 2008 3:43:05 PM 
 * 
 */
package com.osserp.gui.client.hrm.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;
import com.osserp.common.web.ViewName;
import com.osserp.core.hrm.TimeRecordType;
import com.osserp.core.hrm.TimeRecordingConfigManager;
import com.osserp.gui.client.hrm.TimeRecordConfigView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("timeRecordConfigView")
public class TimeRecordConfigViewImpl extends AbstractTimeRecordingConfigView implements TimeRecordConfigView {

    protected TimeRecordConfigViewImpl() {
        super();
    }

    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        TimeRecordingConfigManager manager = getTimeRecordingConfigManager();
        setList(manager.findTypes());
    }

    @Override
    public void save(Form form) throws ClientException, PermissionException {
        if (isCreateMode()) {
            this.create(form);
        } else {
            update(form);
        }
    }

    private void create(Form form) throws ClientException, PermissionException {
        Long id = form.getLong(Form.NUMBER);
        String name = form.getString(Form.NAME);
        String description = form.getString(Form.DESCRIPTION);
        TimeRecordingConfigManager manager = getTimeRecordingConfigManager();
        TimeRecordType type = manager.createType(getDomainEmployee(), id, name, description);
        setList(manager.findTypes());
        setSelection(type.getId());
        disableCreateMode();
        setEditMode(true);
    }

    private void update(Form form) throws ClientException, PermissionException {
        TimeRecordType type = getTimeRecordType();
        if (type != null) {
            TimeRecordingConfigManager manager = getTimeRecordingConfigManager();
            String startingName = form.getString(Form.NAME);
            if (isNotSet(startingName)) {
                throw new ClientException(ErrorCode.NAME_MISSING);
            }
            List<TimeRecordType> existing = manager.findTypes();
            if (!type.getName().equals(startingName)) {
                boolean exists = false;
                for (int i = 0, j = existing.size(); i < j; i++) {
                    TimeRecordType next = existing.get(i);
                    if (!next.getId().equals(type.getId())) {
                        if (next.getName().equals(startingName)) {
                            exists = true;
                            break;
                        }
                    }
                }
                if (!exists) {
                    type.setName(startingName);
                }
            }
            type.setDescription(form.getString(Form.DESCRIPTION));
            type.setInterval(form.getBoolean("interval"));
            type.setIntervalHoursByConfig(form.getBoolean("intervalHoursByConfig"));
            type.setIntervalHours(form.getInteger("intervalHours"));
            type.setIntervalMinutes(form.getInteger("intervalMinutes"));
            type.setIntervalStartHours(form.getIntVal("intervalStartHours"));
            type.setIntervalStartMinutes(form.getIntVal("intervalStartMinutes"));
            type.setLeave(form.getBoolean("leave"));
            type.setManualInputRequired(form.getBoolean("manualInputRequired"));
            type.setIgnoreBreakRules(form.getBoolean("ignoreBreakRules"));
            type.setIntervalPeriod(form.getBoolean("intervalPeriod"));
            type.setBookingFuture(form.getBoolean("bookingFuture"));
            type.setBookingPast(form.getBoolean("bookingPast"));
            type.setSubtraction(form.getBoolean("subtraction"));
            type.setVisible(form.getBoolean("visible"));
            type.setSpecialLeave(form.getBoolean("specialLeave"));
            type.setUnpaidLeave(form.getBoolean("unpaidLeave"));
            type.setIllness(form.getBoolean("illness"));
            type.setBookable(form.getBoolean("bookable"));
            type.setAddition(form.getBoolean("addition"));
            type.setNeedApproval(form.getBoolean("needApproval"));
            manager.save(getDomainEmployee(), type);
        }
        setEditMode(false);
    }

    protected TimeRecordType getTimeRecordType() {
        return (TimeRecordType) getBean();
    }
}
