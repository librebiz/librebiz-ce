/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Oct 19, 2006 01:14:24 PM 
 * 
 */
package com.osserp.gui.client.contacts.impl;

import javax.servlet.http.HttpServletRequest;

import com.osserp.common.ActionException;
import com.osserp.common.web.Form;
import com.osserp.common.web.ViewName;
import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.ContactManager;
import com.osserp.core.contacts.PersonManager;
import com.osserp.gui.client.contacts.ContactSettingsView;
import com.osserp.gui.client.contacts.ContactView;

/**
 * 
 * @author so <so@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
@ViewName("contactSettingsView")
public class ContactSettingsViewImpl extends AbstractContactAwareView implements ContactSettingsView {

    private String[] contactDeletePermissions = null;
    
    protected ContactSettingsViewImpl() {
        super((ContactView.class.getAnnotation(ViewName.class)).value());
    }

    @Override
    public void init(HttpServletRequest request) {
        super.init(request);
        if (getContactView() == null || getContactView().getBean() == null) {
            throw new IllegalStateException("contact view not bound or no contact selected");
        }
        contactDeletePermissions = getSystemConfigManager().getPermissionConfig("contactDelete");
        ContactView cv = getContactView();
        setBean(cv.getBean());
    }

    public void updateUserSalutations(Form form) {
        setForm(form);
        Contact contact = getCurrent();
        contact.setUserNameAddressField(form.getString(Form.USER_NAME_ADDRESS_FIELD));
        contact.setUserSalutation(form.getString(Form.USER_SALUTATION));
        getPersonManager().save(contact);
        setBean(contact);
        refresh();
    }
    
    public boolean isContactDeletePermissionGrant() {
        Contact contact = getCurrent();
        if (!contact.isClient() && !contact.isEmployee() && !contact.isContactPerson()) {
            if (contactDeletePermissions != null && isPermissionGrant(contactDeletePermissions)) {
                return true;
            }
            if (contact.getPersonCreatedBy() != null 
                    && contact.getPersonCreatedBy().equals(getDomainEmployee().getId())) {
                return true;
            }
        }
        return false;
    }

    protected Contact getCurrent() {
        if (getBean() == null) {
            throw new ActionException("no activated contact!");
        }
        return (Contact) getBean();
    }

    protected PersonManager getPersonManager() {
        return (PersonManager) getService(ContactManager.class.getName());
    }

}
