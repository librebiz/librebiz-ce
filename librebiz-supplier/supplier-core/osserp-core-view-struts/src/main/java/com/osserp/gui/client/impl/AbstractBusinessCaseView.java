/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 31-Oct-2006 03:02:07 
 * 
 */
package com.osserp.gui.client.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ActionException;
import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionException;
import com.osserp.common.dms.DmsManager;
import com.osserp.common.dms.DmsReference;
import com.osserp.common.util.AddressValidator;
import com.osserp.common.util.CollectionUtil;
import com.osserp.common.web.Form;

import com.osserp.core.BusinessAddress;
import com.osserp.core.BusinessCase;
import com.osserp.core.BusinessCaseManager;
import com.osserp.core.BusinessCaseRelationStat;
import com.osserp.core.BusinessCaseTemplateManager;
import com.osserp.core.BusinessContract;
import com.osserp.core.BusinessNote;
import com.osserp.core.BusinessObjectChangeManager;
import com.osserp.core.BusinessTemplate;
import com.osserp.core.NoteManager;
import com.osserp.core.crm.Campaign;
import com.osserp.core.customers.Customer;
import com.osserp.core.customers.CustomerManager;
import com.osserp.core.customers.CustomerSearch;
import com.osserp.core.dms.LetterManager;
import com.osserp.core.dms.LetterType;
import com.osserp.core.employees.Employee;
import com.osserp.core.employees.EmployeeSearch;
import com.osserp.core.events.Event;
import com.osserp.core.events.EventManager;
import com.osserp.core.mail.FetchmailInboxManager;
import com.osserp.core.projects.Project;
import com.osserp.core.projects.ProjectManager;
import com.osserp.core.projects.ProjectSearch;
import com.osserp.core.projects.ProjectSupplier;
import com.osserp.core.projects.ProjectSupplierManager;
import com.osserp.core.projects.ProjectTrackingManager;
import com.osserp.core.requests.Request;
import com.osserp.core.requests.RequestManager;
import com.osserp.core.requests.RequestSearch;
import com.osserp.core.sales.Sales;
import com.osserp.core.sales.SalesInvoiceManager;
import com.osserp.core.sales.SalesNoteManager;
import com.osserp.core.sales.SalesOfferManager;
import com.osserp.core.sales.SalesOrderManager;
import com.osserp.gui.BusinessCaseView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractBusinessCaseView extends AbstractConfigurableBusinessCaseAwareView
        implements BusinessCaseView {

    private static Logger log = LoggerFactory.getLogger(AbstractBusinessCaseView.class.getName());

    private boolean salesContext = false;
    // name edit mode
    private boolean nameEditMode = false;
    // address edit mode
    private boolean addressEditMode = false;
    // branch edit mode
    private boolean branchEditMode = false;
    // employee selections
    private List employees = new ArrayList<>();

    private boolean coSalesSelectMode = false;
    private boolean coSalesPercentMode = false;
    private boolean managerSelectMode = false;
    private boolean managerSubstituteSelectMode = false;
    private boolean observerSelectMode = false;
    private boolean salesSelectMode = false;

    // flow control
    private boolean flowControlMode = false;

    private int fetchmailInboxCount;
    private int recordCount;
    private int invoiceCount;
    private int requestDocumentCount;
    private int requestPictureCount;
    private int salesDocumentCount;
    private int salesPictureCount;
    private int templateCount;
    private int trackingCount;
    
    private boolean addSelectNote = false;
    private BusinessNote stickyNote = null;

    private Customer selectedAgent = null;
    private boolean agentDeleteMode = false;
    private Customer selectedTipProvider = null;
    private boolean tipProviderDeleteMode = false;

    private LetterType letterType = null;
    private Long letterTypeId = null;
    private boolean readonly = false;
    private boolean projectAddressMapSupport = false;

    private List<BusinessTemplate> documentTemplates = new ArrayList<>();
    private List<ProjectSupplier> suppliers = new ArrayList<>();
    
    // appointments & events

    private List<Event> appointments = new ArrayList<>();
    private List<Event> events = new ArrayList<>();

    private BusinessContract contract;

    protected AbstractBusinessCaseView() {
        super();
    }

    protected AbstractBusinessCaseView(String forwardTarget, Long letterTypeId) {
        super();
        setForwardTarget(forwardTarget);
        if (letterTypeId != null) {
            this.letterTypeId = letterTypeId;
        }
    }

    /**
     * Does nothing. Override if required.
     */
    public void init(HttpServletRequest request) throws PermissionException {
        if (letterTypeId != null) {
            LetterManager lm = getLetterManager();
            letterType = lm.getType(letterTypeId);
        }
        projectAddressMapSupport = isSystemPropertyEnabled("projectAddressMapSupport");
    }

    protected void load(BusinessCase obj, String exitTarget) throws PermissionException {
        setBean(obj);
        if (obj instanceof Sales) {
            if (log.isDebugEnabled()) {
                log.debug("load() invoked in sales context [id="
                        + obj.getPrimaryKey() + "]");
            }
            salesContext = true;
            if (!isUserResponsibleManager()) {
                checkPermissions(obj);
            }
        } else if (obj instanceof Request) {
            if (log.isDebugEnabled()) {
                log.debug("<init> invoked in request context");
            }
            if (!isUserResponsibleManager()) {
                checkPermissions(obj);
            }
            salesContext = false;
        } else {
            throw new BackendException(("expected object of Sales or Request, found ["
                    + obj.getClass().getName())
                    + "]");
        }
        if (exitTarget != null) {
            setExitTarget(exitTarget);
        }
        loadDependencies();
    }

    protected void loadDependencies() {
        reloadEvents();
        loadContract();
        loadDocumentCounters();
        loadFetchmailInboxCounters();
        loadRecordCounters();
        loadTemplateCounters();
        loadStickyNote();
        loadSuppliers();
    }

    @Override
    public void checkSalesAccess() throws PermissionException {
        super.checkSalesAccess();
    }

    @Override
    public BusinessCase getBusinessCase() {
        return (isSalesContext() ? getSales() : getRequest());
    }

    @Override
    public boolean isSalesContext() {
        return salesContext;
    }
    
    public boolean isCustomerContext() {
        return "customer".equals(getExitTarget());
    }

    public boolean isReadonly() {
        return readonly;
    }

    public void setReadonly(boolean readonly) {
        this.readonly = readonly;
    }

    public boolean isProjectAddressMapSupport() {
        return projectAddressMapSupport;
    }

    protected void setProjectAddressMapSupport(boolean projectAddressMapSupport) {
        this.projectAddressMapSupport = projectAddressMapSupport;
    }

    public Long getId() {
        return getBusinessCase().getPrimaryKey();
    }

    public BusinessNote getStickyNote() {
        return stickyNote;
    }

    protected void setStickyNote(BusinessNote stickyNote) {
        this.stickyNote = stickyNote;
    }

    public final Sales getSales() {
        if (getBean() instanceof Sales) {
            return (Sales) getBean();
        }
        return null;
    }

    public Request getRequest() {
        if (salesContext) {
            return getSales().getRequest();
        }
        if (getBean() instanceof Request) {
            return (Request) getBean();
        }
        if (getBean() != null) {
            log.warn("getRequest() salesContext disabled while bean" 
                    + " not instanceof Request [class="
                    + getBean().getClass().getName() + "]");
            throw new ActionException();
        }
        return null;
    }

    // document counters

    public int getRequestDocumentCount() {
        return requestDocumentCount;
    }

    public int getRequestPictureCount() {
        return requestPictureCount;
    }

    public int getSalesDocumentCount() {
        return salesDocumentCount;
    }

    public int getSalesPictureCount() {
        return salesPictureCount;
    }

    public int getTemplateCount() {
        return templateCount;
    }

    public int getTrackingCount() {
        return trackingCount;
    }
    
    // record counters
    

    public int getRecordCount() {
        return recordCount;
    }

    public int getInvoiceCount() {
        return invoiceCount;
    }

    public List<ProjectSupplier> getSuppliers() {
        return suppliers;
    }

    // appointments

    public List<Event> getAppointments() {
        return appointments;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void reloadEvents() {
        BusinessCase bc = getBusinessCase();
        if (bc != null) {
            EventManager eventManager = getEventManager();
            appointments = eventManager.findAppointments(bc);
            events = eventManager.findEvents(bc);
            if (log.isDebugEnabled()) {
                log.debug("reloadEvents() done [appointments=" + appointments.size()
                        + ", events=" + events.size() + "]");
            }
        } else if (log.isDebugEnabled()) {
            log.debug("reloadEvents() ignored, no business case bound");
        }
    }

    protected EventManager getEventManager() {
        return (EventManager) getService(EventManager.class.getName());
    }

    public void updateCustomer(Long customerId) throws ClientException {
        if (customerId == null) {
            throw new ClientException(ErrorCode.ID_REQUIRED);
        }
        if (isSalesContext()) {
            getBusinessObjectChangeManager().changeCustomer(getDomainUser(), getSales(), customerId);
        } else {
            getBusinessObjectChangeManager().changeCustomer(getDomainUser(), getRequest(), customerId);
        }
        reload();
    }

    public void closeAppointment(Long id) {
        Event event = (Event) CollectionUtil.getByProperty(appointments, "id", id);
        if (event != null) {
            getEventManager().close(getUser().getId(), event);
        }
        reloadEvents();
    }

    public BusinessContract getContract() {
        return contract;
    }

    protected void setContract(BusinessContract contract) {
        this.contract = contract;
    }

    protected void loadContract() {
        contract = getBusinessCaseManager().getContract(getBusinessCase());
    }

    // business methods

    public boolean isAddressEditMode() {
        return addressEditMode;
    }

    public void setAddressEditMode(boolean addressEditMode) {
        this.addressEditMode = addressEditMode;
    }

    public void updateAddress(Form fh) throws ClientException {
        Request request = getRequest();
        BusinessAddress address = request.getAddress();
        address.setName(fh.getString(Form.NAME));
        address.setPerson(fh.getString(Form.PERSON));
        address.setStreet(fh.getString(Form.STREET));
        address.setZipcode(fh.getString(Form.ZIPCODE));
        address.setCity(fh.getString(Form.CITY));
        address.setCountry(fh.getLong(Form.COUNTRY));
        AddressValidator.validateAddress(
                address.getStreet(),
                address.getZipcode(),
                address.getCity(),
                address.getCountry(),
                false);
        getRequestManager().update(request);
        disableAllModes();
        if (log.isDebugEnabled()) {
            log.debug("updateAddress() done");
        }
    }

    public boolean isBranchEditMode() {
        return branchEditMode;
    }

    public void setBranchEditMode(boolean branchEditMode) {
        disableAllModes();
        this.branchEditMode = branchEditMode;
    }

    public final void updateBranch(Form form) throws ClientException {
        Request request = getRequest();
        Employee responsible = fetchResponsibleUser(request);
        if (responsible == null) {
            if (salesContext) {
                throw new ClientException(ErrorCode.SALESPERSON_OR_MANAGER_REQUIRED);
            }
            throw new ClientException(ErrorCode.SALESPERSON_MISSING);
        }
        Long branchId = form.getLong(Form.BRANCH);
        if (isNotSet(branchId)) {
            throw new ClientException(ErrorCode.BRANCH_MISSING);
        }
        BusinessCase bc = getBusinessCase();
        getBusinessCaseManager().updateBranch(bc, branchId);
        disableAllModes();
        if (log.isDebugEnabled()) {
            log.debug("updateBranch() done [businessCase=" + bc.getPrimaryKey() + ", branch=" + branchId + "]");
        }
    }
    
    /**
     * Provides the salesPerson of the request or the projectManager if request
     * is closed and resulting project provides an associated projectManager. 
     * @param request
     * @return salesPerson or projectManager or null if non assigne 
     */
    protected Employee fetchResponsibleUser(Request request) {
        Employee result = null;
        Sales sales = getSales();
        if (sales != null) {
            result = (sales.getSalesId() == null) ? null : getEmployee(sales.getSalesId());
            if (result == null) {
                result = (sales.getManagerId() == null) ? null : getEmployee(sales.getManagerId());
            }
        } else {
            result = (request.getSalesId() == null) ? null : getEmployee(request.getSalesId());
        }
        return result;
    }

    public List<Customer> getAgents() {
        return getCustomerSearch().findAgents();
    }

    public Customer getSelectedAgent() {
        return selectedAgent;
    }

    public boolean isAgentDeleteMode() {
        return agentDeleteMode;
    }

    public void setAgentConfig(Long agentId) throws ClientException {
        if (agentId != null && agentId > 0) {
            selectedAgent = (Customer) getCustomerManager().find(agentId);
        } else if (agentId != null && agentId == 0) {
            agentDeleteMode = true;
            selectedAgent = null;
        } else {
            selectedAgent = null;
        }
    }

    public void updateAgentConfig(Form form) throws ClientException {
        Request request = getRequest();
        Long agentId = form.getLong("agentId");
        if (agentId == null || agentId <= 0) {
            if (agentId == null || agentId == 0) {
                request.configureAgent(null, null, false);
            } else {
                return;
            }
        } else {
            Customer agent = (Customer) getCustomerManager().find(agentId);
            request.configureAgent(
                    agent,
                    form.getDouble("agentCommission"),
                    form.getBoolean("agentCommissionPercent"));
        }
        getRequestManager().update(request);
        reload();
        selectedAgent = null;
        agentDeleteMode = false;
    }

    public List<Customer> getTips() {
        return getCustomerSearch().findTips();
    }

    public Customer getSelectedTipProvider() {
        return selectedTipProvider;
    }

    public boolean isTipProviderDeleteMode() {
        return tipProviderDeleteMode;
    }

    public void setTipConfig(Long tipProviderId) throws ClientException {
        if (tipProviderId != null && tipProviderId > 0) {
            selectedTipProvider = (Customer) getCustomerManager().find(tipProviderId);
        } else if (tipProviderId != null && tipProviderId == 0) {
            tipProviderDeleteMode = true;
            selectedTipProvider = null;
        } else {
            selectedTipProvider = null;
        }
    }

    public void updateTipConfig(Form form) throws ClientException {
        Request request = getRequest();
        Long tipId = form.getLong("tipId");
        if (isNotSet(tipId)) {
            request.configureTip(null, null);
        } else {
            Double tc = form.getDouble(Form.TIP_COMMISSION);
            Customer tip = (Customer) getCustomerManager().find(tipId);
            request.configureTip(tip, tc);
        }
        getRequestManager().update(request);
        reload();
        selectedTipProvider = null;
        tipProviderDeleteMode = false;
    }

    public boolean isNameEditMode() {
        return nameEditMode;
    }

    public void setNameEditMode(boolean nameEditMode) {
        disableAllModes();
        this.nameEditMode = nameEditMode;
    }

    public void updateName(Form fh) throws ClientException {
        Request request = getRequest();
        request.setName(fh.getString(Form.VALUE));
        getRequestManager().update(request);
        setNameEditMode(false);
    }

    public boolean isOriginHistoryAvailable() {
        return getRequestManager().isOriginHistoryAvailable(getRequest());
    }

    public void setCampaign(Campaign campaign) {
        if (campaign != null) {
            Request request = getRequest();
            getRequestManager().updateOrigin(request, getDomainEmployee(), campaign.getId());
            request.setOrigin(campaign.getId());
        }
    }

    // employee selections

    public List getEmployees() {
        return employees;
    }

    public void enableCoSalesSelection() throws PermissionException {
        setSalesEmployees();
        disableAllModes();
        coSalesSelectMode = true;
    }

    public void enableManagerSelection() throws PermissionException {
        setTecEmployees();
        disableAllModes();
        managerSelectMode = true;
    }

    public void enableManagerSubstituteSelection() throws PermissionException {
        setTecEmployees();
        disableAllModes();
        managerSubstituteSelectMode = true;
    }

    public void enableObserverSelection() {
        EmployeeSearch search = getEmployeeSearch();
        employees = search.findAllActive();
        disableAllModes();
        observerSelectMode = true;
    }

    public final void enableSalesSelection() throws PermissionException {
        setSalesEmployees();
        disableAllModes();
        salesSelectMode = true;
    }

    private void setSalesEmployees() throws PermissionException {
        employees = findEmployees(false);
        for (Iterator<BusinessCaseRelationStat> i = employees.iterator(); i
                .hasNext();) {
            BusinessCaseRelationStat e = i.next();
            if (!e.isActive()) {
                if (log.isDebugEnabled()) {
                    log.debug("setSalesEmployees() removing deactivated [id=" + e.getId()
                            + ", lastname=" + e.getLastName() + "]");
                }
                i.remove();
            }
        }
    }

    private void setTecEmployees() throws PermissionException {
        employees = findEmployees(true);
        for (Iterator<BusinessCaseRelationStat> i = employees.iterator(); i
                .hasNext();) {
            BusinessCaseRelationStat e = i.next();
            if (!e.isActive()) {
                i.remove();
            }
        }
    }

    private List<BusinessCaseRelationStat> findEmployees(boolean tec) throws PermissionException {

        EmployeeSearch search = getEmployeeSearch();
        if (tec) {
            return search.findTecCapacity(getDomainUser(), null);
        }
        return search.findSalesCapacity(getDomainUser(), null);
    }

    public boolean isEmployeeSelectionMode() {
        return (managerSelectMode || managerSubstituteSelectMode
                || observerSelectMode || salesSelectMode);
    }

    public boolean isObserverSelectMode() {
        return observerSelectMode;
    }

    public boolean isCoSalesSelectMode() {
        return coSalesSelectMode;
    }

    public synchronized void updateEmployee(Long id) throws ClientException {
        if (log.isDebugEnabled() && isSalesContext()) {
            Sales sales = getSales();
            log.debug("updateEmployee() before action [technician="
                    + sales.getManagerId() + ", sales=" + sales.getSalesId()
                    + ", branch="
                    + (sales.getRequest().getBranch() == null ? null : sales.getRequest().getBranch().getId())
                    + "]");
        }
        if (managerSelectMode) {
            updateManager(id);
        } else if (managerSubstituteSelectMode) {
            updateManagerSubstitute(id);
        } else if (observerSelectMode) {
            updateObserver(id);
        } else if (salesSelectMode) {
            updateSalesPerson(id);
        } else if (coSalesSelectMode) {
            updateCoSales(id);
        }
        if (log.isDebugEnabled() && isSalesContext()) {
            Sales sales = getSales();
            log.debug("updateEmployee() after action [technician="
                    + sales.getManagerId() + ", sales=" + sales.getSalesId()
                    + ", branch="
                    + (sales.getRequest().getBranch() == null ? null : sales.getRequest().getBranch().getId())
                    + "]");
        }
    }

    public void removeEmployee(String target) {
        if ("substitute".equals(target)) {
            getSalesManager().setManagerSubstitute(getUser(), getSales(), null);
            disableAllModes();
        } else if ("coSales".equals(target)) {
            if (salesContext) {
                Sales sale = getSales();
                getSalesManager().setCoSales(getUser(), sale, null);
            } else {
                Request request = getRequest();
                getRequestManager().updateSalesCo(request, null);
            }
        }
    }

    public boolean isCoSalesPercentMode() {
        return coSalesPercentMode;
    }

    public void setCoSalesPercentMode(boolean coSalesPercentMode) {
        this.coSalesPercentMode = coSalesPercentMode;
    }

    public void updateCoSalesPercent(Form form) throws ClientException {
        Request request = getRequest();
        getRequestManager().updateSalesCoPercent(request, form.getPercentage("salesCoPercent"));
        reload();
        coSalesPercentMode = false;
    }

    // flow control 
    
    public boolean isFlowControlMode() {
        return flowControlMode;
    }

    public void setFlowControlMode(boolean flowControlMode) {
        this.flowControlMode = flowControlMode;
    }

    public int getLetterCount() {
        if (getBean() != null && getLetterType() != null) {
            LetterManager lm = getLetterManager();
            return lm.getCount(getLetterType(), getBusinessCase().getCustomer(), getBusinessCase().getPrimaryKey());
        }
        return 0;
    }

    public int getFetchmailInboxCount() {
        return fetchmailInboxCount;
    }

    protected void loadFetchmailInboxCounters() {
        try {
            if (getBean() != null) {
                FetchmailInboxManager fm = getFetchmailInboxManager();
                fetchmailInboxCount = fm.getCountByBusinessCase(getDomainUser(), getBusinessCase());
            }
        } catch (Exception e) {
            log.warn("loadFetchmailInboxCounters: Ignoring exception on attempt to load mail messages"
                    + " [message=" + e.getMessage() + "]", e);
        }
    }

    protected void loadDocumentCounters() {
        Request request = getRequest();
        if (request != null) {
            try {

                DmsManager documentManager = (DmsManager) getService(DmsManager.class.getName());
                requestDocumentCount = documentManager.getCountByReference(new DmsReference(Request.DOCUMENTS, request.getPrimaryKey()));
                requestPictureCount = documentManager.getCountByReference(new DmsReference(Request.PICTURES, request.getPrimaryKey()));

                if (isSalesContext() && getSales() != null) {
                    Sales sales = getSales();
                    trackingCount = getProjectTrackingManager().getTrackingCount(sales);
                    salesDocumentCount = documentManager.getCountByReference(new DmsReference(Sales.DOCUMENTS, sales.getId()));
                    salesPictureCount = documentManager.getCountByReference(new DmsReference(Project.PICTURES, sales.getId()));
                }
            } catch (Exception e) {
                log.warn("init() ignoring exception on attempt to load document counters [request=" + request.getPrimaryKey()
                        + ", message=" + e.getMessage() + "]", e);
            }
        }
    }
    
    private ProjectTrackingManager getProjectTrackingManager() {
        return (ProjectTrackingManager) getService(ProjectTrackingManager.class.getName());
    }
    
    protected abstract void loadRecordCounters();
    
    
    public List<BusinessTemplate> getDocumentTemplates() {
        return documentTemplates;
    }

    public byte[] createDocument(Long templateId) throws ClientException {
       for (int i = 0, j = documentTemplates.size(); i < j; i++) {
           BusinessTemplate next = documentTemplates.get(i);
           if (next.getId().equals(templateId)) {
               return getBusinessCaseTemplateManager().createPdf(
                       getDomainUser(), next, getBusinessCase());
           }
       }
       throw new ClientException(ErrorCode.APPLICATION_CONFIG);
    }
    
    protected final void loadTemplateCounters() {
        BusinessCase businessCase = getBusinessCase();
        if (businessCase != null) {
            Long branch = null;
            String contextName = businessCase.isSalesContext() ? "sales" : "request";
            if (businessCase.getBranch() != null) {
                branch = businessCase.getBranch().getId();
            }
            documentTemplates = getBusinessCaseTemplateManager().findTemplates(contextName, branch);
            setTemplateCount(documentTemplates.size());
        } else {
            log.warn("loadTemplateCounters() invoked while businessCase not bound [user" 
                    + (getUser() == null ? "null" : getUser().getId()) + "]");
        }
    }

    protected void loadStickyNote() {
        NoteManager noteManager = (NoteManager) getService(SalesNoteManager.class.getName());
        stickyNote = noteManager.getSticky(getRequest().getPrimaryKey());
    }

    protected void loadSuppliers() {
        suppliers = getProjectSupplierManager().getSuppliers(getBusinessCase());
    }

    // helpers

    public void disableAllModes() {
        addressEditMode = false;
        branchEditMode = false;
        managerSelectMode = false;
        managerSubstituteSelectMode = false;
        nameEditMode = false;
        observerSelectMode = false;
        salesSelectMode = false;
        coSalesSelectMode = false;
        coSalesPercentMode = false;
    }

    // private business methods

    private void updateManager(Long id) throws ClientException {
        if (managerSelectMode) {
            setBean(getBusinessCaseManager().setManager(getUser(), getBusinessCase(), id));
        }
        disableAllModes();
    }

    private void updateManagerSubstitute(Long id) {
        if (salesContext) {
            Sales sale = getSales();
            getSalesManager()
                    .setManagerSubstitute(getUser(), sale, id);
        }
        disableAllModes();
    }

    private void updateObserver(Long id) {
        if (salesContext) {
            Sales sale = getSales();
            sale.setObserverId(id);
            getSalesManager().save(sale);
        }
        disableAllModes();
    }

    private void updateSalesPerson(Long id) {
        setBean(getBusinessCaseManager().setSales(getUser(), getBusinessCase(), id));
        disableAllModes();
    }

    private void updateCoSales(Long id) {
        if (salesContext) {
            Sales sale = getSales();
            getSalesManager().setCoSales(getUser(), sale, id);
        } else {
            getRequestManager().updateSalesCo(getRequest(), id);
        }
        disableAllModes();
    }

    // protected setters for readonly properties

    protected void setSalesContext(boolean salesContext) {
        this.salesContext = salesContext;
    }

    protected void setRequestDocumentCount(int requestDocumentCount) {
        this.requestDocumentCount = requestDocumentCount;
    }

    protected void setRequestPictureCount(int requestPictureCount) {
        this.requestPictureCount = requestPictureCount;
    }

    protected void setSalesDocumentCount(int salesDocumentCount) {
        this.salesDocumentCount = salesDocumentCount;
    }

    protected void setSalesPictureCount(int salesPictureCount) {
        this.salesPictureCount = salesPictureCount;
    }

    protected void setTemplateCount(int templateCount) {
        this.templateCount = templateCount;
    }
    
    protected void setTrackingCount(int trackingCount) {
        this.trackingCount = trackingCount;
    }

    protected void setRecordCount(int recordCount) {
        this.recordCount = recordCount;
    }

    protected void setInvoiceCount(int invoiceCount) {
        this.invoiceCount = invoiceCount;
    }

    protected void setEmployees(List employees) {
        this.employees = employees;
    }

    public boolean isManagerSelectMode() {
        return managerSelectMode;
    }

    protected void setManagerSelectMode(boolean managerSelectMode) {
        this.managerSelectMode = managerSelectMode;
    }

    protected boolean isManagerSubstituteSelectMode() {
        return managerSubstituteSelectMode;
    }

    protected void setManagerSubstituteSelectMode(
            boolean managerSubstituteSelectMode) {
        this.managerSubstituteSelectMode = managerSubstituteSelectMode;
    }

    protected void setObserverSelectMode(boolean observerSelectMode) {
        this.observerSelectMode = observerSelectMode;
    }

    protected boolean isSalesSelectMode() {
        return salesSelectMode;
    }

    protected void setSalesSelectMode(boolean salesSelectMode) {
        this.salesSelectMode = salesSelectMode;
    }

    protected void setAppointments(List<Event> appointments) {
        this.appointments = appointments;
    }

    protected LetterType getLetterType() {
        return letterType;
    }

    // required managers

    protected LetterManager getLetterManager() {
        return (LetterManager) getService(LetterManager.class.getName());
    }

    protected FetchmailInboxManager getFetchmailInboxManager() {
        return (FetchmailInboxManager) getService(FetchmailInboxManager.class.getName());
    }

    protected final RequestManager getRequestManager() {
        return (RequestManager) getService(RequestManager.class.getName());
    }

    protected final RequestSearch getRequestSearch() {
        return (RequestSearch) getService(RequestSearch.class.getName());
    }

    protected final ProjectManager getSalesManager() {
        return (ProjectManager) getService(ProjectManager.class.getName());
    }

    protected final ProjectSupplierManager getProjectSupplierManager() {
        return (ProjectSupplierManager) getService(ProjectSupplierManager.class.getName());
    }

    protected final SalesOfferManager getSalesOfferManager() {
        return (SalesOfferManager) getService(SalesOfferManager.class.getName());
    }

    protected final SalesOrderManager getSalesOrderManager() {
        return (SalesOrderManager) getService(SalesOrderManager.class.getName());
    }

    protected final CustomerManager getCustomerManager() {
        return (CustomerManager) getService(CustomerManager.class.getName());
    }

    protected final CustomerSearch getCustomerSearch() {
        return (CustomerSearch) getService(CustomerSearch.class.getName());
    }

    protected final SalesInvoiceManager getInvoiceManager() {
        return (SalesInvoiceManager) getService(SalesInvoiceManager.class.getName());
    }

    protected final ProjectSearch getSalesSearch() {
        return (ProjectSearch) getService(ProjectSearch.class.getName());
    }
    
    protected final BusinessCaseTemplateManager getBusinessCaseTemplateManager() {
        return (BusinessCaseTemplateManager) getService(BusinessCaseTemplateManager.class.getName());
    }
    
    protected final BusinessObjectChangeManager getBusinessObjectChangeManager() {
        return (BusinessObjectChangeManager) getService(BusinessObjectChangeManager.class.getName());
    }

    @Override
    protected BusinessCaseManager getBusinessCaseManager() {
        if (isSalesContext()) {
            return getSalesManager();
        }
        return getRequestManager();
    }

    private void checkPermissions(BusinessCase bc) throws PermissionException {
        getDomainUser().checkPermissions(bc);
    }

    public boolean isAddSelectNote() {
        return addSelectNote;
    }

    public void setAddSelectNote(boolean addSelectNote) {
        this.addSelectNote = addSelectNote;
    }
}
