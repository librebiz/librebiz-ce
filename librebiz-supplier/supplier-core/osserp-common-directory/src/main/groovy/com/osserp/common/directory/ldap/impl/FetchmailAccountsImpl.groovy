/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.directory.ldap.impl

import org.apache.directory.groovyldap.LDAP
import org.apache.directory.groovyldap.SearchScope

import com.osserp.common.directory.ldap.FetchmailConfig
import com.osserp.common.directory.ldap.LdapConfig
import com.osserp.common.directory.ldap.LdapException
import com.osserp.common.directory.ldap.dao.FetchmailAccounts
import com.osserp.common.util.EmailValidator

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class FetchmailAccountsImpl extends AbstractLdapDao implements FetchmailAccounts {

    FetchmailAccountsImpl(LdapConfig configInstance) {
        super(configInstance)
    }

    List<Map> findAll(String client) {
        LDAP ldap = createConnection()
        List<Map> result = []
        ldap.search('(objectClass=Fetchmail)', createBaseDn(client), SearchScope.SUB).each() { result << addBusinessAttributes(it) }
        def byUsername = { Map a, Map b ->
            try {
                return a.fetchmailusername.compareToIgnoreCase(b.fetchmailusername)
            } catch (Throwable t) {
                return 0
            }
        }
        return result.sort(byUsername)
    }


    Map lookupFetchmailAccount(Map user) {
        //println "lookupFetchmailAccount: invoked [class=${user.getClass().getName()}]"
        LDAP ldap = createConnection()
        return loadFetchmailAccount(ldap, user)
    }

    Map createFetchmailAccount(Map user, String accountName, String accountPassword) throws LdapException {
        LDAP ldap = createConnection()
        FetchmailConfig fetchmailConfig = config.fetchmailConfig
        if (!fetchmailConfig
        || !fetchmailConfig.serverName
        || !fetchmailConfig.serverType
        || !fetchmailConfig.fetchMode) {
            throw new LdapException(LdapException.FEATURE_CONFIG_MISSING)
        }
        if (!user.mail) {
            throw new LdapException(LdapException.MAIL_MISSING)
        }
        if (!accountPassword) {
            throw new LdapException(LdapException.MAIL_PASSWORD_MISSING)
        }
        if (!accountName) {
            throw new LdapException(LdapException.MAIL_ACCOUNT_NAME_MISSING)
        }
        if (fetchmailAccountExists(ldap, user)) {
            throw new LdapException(LdapException.MAIL_ACCOUNT_EXISTS)
        }
        createFetchmailEntry(ldap, user)
        if (fetchmailEntryExists(ldap, user)) {
            def accountDn = createFetchmailAccountDn(user)
            def attrs = [
                objectclass: ['top', 'Fetchmail'],
                fetchmailfetchmode: fetchmailConfig.fetchMode,
                fetchmailservername: fetchmailConfig.serverName,
                fetchmailservertype: fetchmailConfig.serverType,
                // fetchMailAdress is unique for a user
                // (e.g. primary mail address)
                fetchmailmailaddress: user.mail,
                fetchmailusername: accountName,
                fetchmailuserpasswd: accountPassword
            ]
            ldap.add(accountDn, attrs)
        }
        loadFetchmailAccount(ldap, user)
    }

    Map enableAccount(Map account, String serverName) {
        if (account && account.dn && config.fetchmailConfig) {
            FetchmailConfig fetchmailConfig = config.fetchmailConfig
            def newValues = [ fetchmailServerName: (serverName ?: fetchmailConfig.serverName) ]
            LDAP ldap = createConnection()
            ldap.modify(account.dn, 'REPLACE', newValues)
            return findByCompletedDn(ldap, account.dn)
        }
        return account
    }

    Map disableAccount(Map account, String serverName) {
        if (account && account.dn && config.fetchmailConfig) {
            FetchmailConfig fetchmailConfig = config.fetchmailConfig
            LDAP ldap = createConnection()
            ldap.modify(account.dn, 'REPLACE', [ fetchmailServerName: FetchmailAccounts.ACCOUNT_DISABLED ])
            return findByCompletedDn(ldap, account.dn)
        }
        return account
    }


    void deleteAccount(Map account) {
        LDAP ldap = createConnection()
        if (account && account.dn && ldap.exists(account.dn)) {
            try {
                ldap.delete(account.dn)
            } catch (Throwable t) {
                println "deleteAccount: failed [message=${t.message}]"
            }
        }
    }

    protected Map loadFetchmailAccount(LDAP ldap, Map user) {
        if (user && EmailValidator.validate(user.mail) && fetchmailAccountExists(ldap, user)) {
            return findByCompletedDn(ldap, createFetchmailAccountDn(user))
        }
        return null
    }

    @Override
    protected Map addBusinessAttributes(Map ldapEntry) {
        if (FetchmailAccounts.ACCOUNT_DISABLED == ldapEntry.fetchmailservername) {
            ldapEntry.disabled = true
        } else {
            ldapEntry.disabled = false
        }
        return ldapEntry
    }

    private String createFetchmailAccountDn(Map user) {
        String client = fetchClientName(user.dn)
        //cn={firstname.lastname@email.com},ou=fetchmail,uid={useruid},ou=Users,dc=domain,dc=tld
        "cn=${user.mail},ou=fetchmail,uid=${user.uid},ou=Users,${createBaseDn(client)}"
    }

    private boolean fetchmailAccountExists(LDAP ldap, Map user) {
        def uid = user.uid
        def mail = user.mail
        if (mail && uid) {
            return ldap.exists(createFetchmailAccountDn(user))
        }
        return false
    }

    private boolean createFetchmailEntry(LDAP ldap, Map user) {
        if (!fetchmailEntryExists(ldap, user)) {
            def entryDn = createFetchmailEntryDn(uid)
            def attrs = [
                objectclass: [
                    'top',
                    'organizationalUnit']
            ]
            ldap.add(entryDn, attrs)
            return fetchmailEntryExists(ldap, user)
        }
        return false
    }

    private String createFetchmailEntryDn(Map user) {
        String client = fetchClientName(user.dn)
        //ou=fetchmail,uid={useruid},ou=Users,dc=domain,dc=tld
        "ou=fetchmail,uid=${user.uid},ou=Users,${createBaseDn(client)}"
    }

    private boolean fetchmailEntryExists(LDAP ldap, Map user) {
        def dn = createFetchmailEntryDn(user)
        ldap.exists(dn)
    }
}

