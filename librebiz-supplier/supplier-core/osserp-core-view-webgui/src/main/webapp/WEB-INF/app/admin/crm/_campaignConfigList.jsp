<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<style type="text/css">
/*
.name {
	width: 330px;
}

.group, .type {
	width: 184px;
}

.action {
	width: 60px;
	text-align: center;
}

.empty {
	width: 780px;
}

.table .activated {
	background-color: #FFFFFF;
}

.table .eol {
	background-color: #FBC1C5;
}
*/
</style>

<div class="col-lg-12 panel-area">
	<div class="table-responsive table-responsive-default">
		<table class="table">
			<thead>
				<tr>
					<th class="name"><fmt:message key="name"/></th>
					<th class="type"><fmt:message key="type"/></th>
					<th class="group"><fmt:message key="group"/></th>
					<th class="action"><fmt:message key="action"/></th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty view.list and empty view.parent}">
						<tr><td colspan="4" class="error empty"><fmt:message key="noCampaignsAvailable"/></td></tr>
					</c:when>
					<c:when test="${empty view.parent}">
						<c:forEach var="item" items="${view.list}">
							<c:if test="${view.displayEol or !item.endOfLife}">
								<c:choose><c:when test="${item.endOfLife}"><c:set var="styleClass" value="eol"/></c:when><c:otherwise><c:set var="styleClass" value="activated"/></c:otherwise></c:choose>
								<tr class="${styleClass}">
									<td class="name"><v:link url="/admin/crm/campaignConfig/select?id=${item.id}"><o:out value="${item.name}"/></v:link></td>
									<td class="type"><o:out value="${item.type.name}"/></td>
									<td class="group"><o:out value="${item.group.name}"/></td>
									<td class="action">
										<c:choose>
											<c:when test="${item.parent}">
												<v:link url="/admin/crm/campaignConfig/selectParent?id=${item.id}" title="displaySubCampaigns"><o:img name="relationIcon"/></v:link>
											</c:when>
											<c:otherwise>
												<v:link url="/admin/crm/campaignConfig/selectParent?id=${item.id}" title="setupSubCampaigns"><o:img name="configureIcon"/></v:link>
											</c:otherwise>
										</c:choose>
									</td>
								</tr>
							</c:if>
						</c:forEach>
					</c:when>
					<c:otherwise>
						<c:choose><c:when test="${view.parent.endOfLife}"><c:set var="styleClass" value="eol"/></c:when><c:otherwise><c:set var="styleClass" value="activated"/></c:otherwise></c:choose>
						<tr class="${styleClass}">
							<td class="name"><o:out value="${view.parent.name}"/></td>
							<td class="type"><o:out value="${view.parent.type.name}"/></td>
							<td class="group"><o:out value="${view.parent.group.name}"/></td>
							<td class="action"> 
								<v:link url="/admin/crm/campaignConfig/enableCreateMode" title="createSubCampaign"><o:img name="newdataIcon"/></v:link>
							</td>
						</tr>
						<c:forEach var="item" items="${view.list}">
							<c:if test="${view.displayEol or !item.endOfLife}">
								<c:choose><c:when test="${item.endOfLife}"><c:set var="styleClass" value="eol"/></c:when><c:otherwise><c:set var="styleClass" value="activated"/></c:otherwise></c:choose>
								<tr class="${styleClass}">
									<td class="name"><v:link url="/admin/crm/campaignConfig/select?id=${item.id}"><o:out value="${item.name}"/></v:link></td>
									<td class="type"><o:out value="${item.type.name}"/></td>
									<td class="group"><o:out value="${item.group.name}"/></td>
									<td class="action"> </td>
								</tr>
							</c:if>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	</div>
</div>
