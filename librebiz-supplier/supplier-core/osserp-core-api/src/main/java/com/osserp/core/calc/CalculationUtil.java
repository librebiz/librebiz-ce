/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 16-Feb-2007 08:44:56 
 * 
 */
package com.osserp.core.calc;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.util.AbstractValidator;

import com.osserp.core.Item;
import com.osserp.core.products.Product;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CalculationUtil extends AbstractValidator {
    private static Logger log = LoggerFactory.getLogger(CalculationUtil.class.getName());

    public static void validateProduct(Product product, Double quantity, String note) throws ClientException {
        if (!product.isPlant() && !product.isQuantityByPlant() && isNotSet(quantity)) {
            throw new ClientException(ErrorCode.QUANTITY_MISSING);
        }
        if (product.isBillingNoteRequired() && isNotSet(note)) {
            throw new ClientException(ErrorCode.DESCRIPTION_MISSING);
        }
    }

    public static void validateProductPriceInput(Calculation calculation, boolean packagePriceNullable) throws ClientException {
        if (calculation == null) {
            throw new IllegalArgumentException("calculation must not be null");
        }
        if (log.isDebugEnabled()) {
            log.debug("validateProductPriceInput() invoked [calculation=" + calculation.getId() + "]");
        }
        List<Item> items = calculation.getAllItems();
        if (items.isEmpty()) {
            throw new ClientException(ErrorCode.CALCULATION_INCOMPLETE);
        }
        if (calculation.isPackageAvailable()) {
            for (int i = 0, j = items.size(); i < j; i++) {
                Item next = items.get(i);
                if (!next.getProduct().isNullable()
                        && ((next.getPrice() == null || next.getPrice().doubleValue() == 0)
                        || !next.isIncludePrice())) {

                    if (!next.getProduct().isPlant() || !packagePriceNullable) {
                        if (log.isDebugEnabled()) {
                            log.debug("validateProductPriceInput() product must not be billed with 0 [calculation="
                                    + calculation.getId()
                                    + ", product="
                                    + next.getProduct().getProductId()
                                    + "]");
                        }
                        throw new ClientException(
                                ErrorCode.CALCULATION_PRICE_INVALID,
                                next.getProduct().getProductId());
                    }
                }
            }
        }
    }
}
