/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 22-Jun-2006 12:40:08 
 * 
 */
package com.osserp.gui.web.sales;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionException;
import com.osserp.common.web.RequestUtil;

import com.osserp.core.finance.Record;
import com.osserp.gui.client.Actions;
import com.osserp.gui.client.records.CreditNoteView;
import com.osserp.gui.client.sales.SalesBillingView;
import com.osserp.gui.client.sales.SalesCreditNoteView;
import com.osserp.gui.client.sales.SalesInvoiceView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CreditNoteAction extends AbstractSalesRecordAction {
    private static Logger log = LoggerFactory.getLogger(CreditNoteAction.class.getName());

    /**
     * Creates a credit note for current invoice
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward create(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("create() request from " + getUser(session).getId());
        }
        SalesCreditNoteView view = fetchSalesCreditNoteView(session);
        if (view == null) {
            view = (SalesCreditNoteView) createView(request);
        }
        view.create();
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Deletes a credit note
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward delete(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("delete() request from " + getUser(session).getId());
        }
        CreditNoteView view = getSalesCreditNoteView(session);
        if (view.getRecord().isUnchangeable()) {
            return saveErrorAndReturn(request, ErrorCode.RECORD_UNCHANGEABLE);
        }
        try {
            view.delete();
        } catch (ClientException c) {
            return saveErrorAndReturn(request, c.getMessage());
        } catch (PermissionException p) {
            return saveErrorAndReturn(request, p.getMessage());
        }
        return exit(mapping, form, request, response);
    }

    /**
     * Changes sales commission status of credit note
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward changeCommissionStatus(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("changeCommissionStatus() request from " + getUser(session).getId());
        }
        CreditNoteView view = getSalesCreditNoteView(session);
        try {
            view.changeSalesCommissionStatus();
            return mapping.findForward(view.getActionTarget());
        } catch (PermissionException e) {
            return saveErrorAndReturn(request, e.getMessage());
        }
    }

    /**
     * Changes sales commission status of credit note
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward createInvoiceRelation(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("createInvoiceRelation() request from " + getUser(session).getId());
        }
        SalesCreditNoteView view = getSalesCreditNoteView(session);
        view.createInvoiceRelation(RequestUtil.getId(request));
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Changes sales commission status of credit note
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward toggleDeliveryRequiredFlag(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("toggleDeliveryRequiredFlag() request from " + getUser(session).getId());
        }
        CreditNoteView view = getSalesCreditNoteView(session);
        try {
            view.toggleDeliveryRequiredFlag();
            return mapping.findForward(view.getActionTarget());
        } catch (PermissionException e) {
            return saveErrorAndReturn(request, e.getMessage());
        }
    }

    @Override
    public ActionForward exit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exit() request from " + getUser(session).getId());
        }
        SalesCreditNoteView view = getSalesCreditNoteView(session);
        SalesInvoiceView invoiceView = view.getInvoiceView();
        if (invoiceView != null) {
            invoiceView.refresh();
        } else {
            Record record = view.getRecord();
            SalesBillingView billingView = fechBillingView(session);
            if (billingView != null && billingView.getOrder().getBusinessCaseId() != null
                    && billingView.getOrder().getBusinessCaseId().equals(record.getBusinessCaseId())) {
                super.exit(mapping, form, request, response);
                return mapping.findForward(Actions.BILLING);
            }
        }
        return super.exit(mapping, form, request, response);
    }

    @Override
    protected Class<SalesCreditNoteView> getViewClass() {
        return SalesCreditNoteView.class;
    }

    private SalesCreditNoteView fetchSalesCreditNoteView(HttpSession session) {
        return (SalesCreditNoteView) fetchView(session);
    }

    private SalesCreditNoteView getSalesCreditNoteView(HttpSession session) throws PermissionException {
        return (SalesCreditNoteView) getRecordView(session);
    }
}
