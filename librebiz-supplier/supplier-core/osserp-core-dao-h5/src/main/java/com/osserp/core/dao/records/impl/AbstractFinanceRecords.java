/**
 *
 * Copyright (C) 2015 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 14, 2015 7:35:28 PM 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.OptionsCache;
import com.osserp.common.Property;
import com.osserp.common.dao.Tables;
import com.osserp.common.util.DateUtil;

import com.osserp.core.dao.TaxRates;
import com.osserp.core.dao.impl.AbstractDao;
import com.osserp.core.dao.records.FinanceRecords;
import com.osserp.core.finance.FinanceRecord;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractFinanceRecords extends AbstractDao implements FinanceRecords {
    private static Logger log = LoggerFactory.getLogger(AbstractFinanceRecords.class.getName());

    private TaxRates _taxRates = null;
    protected Double taxRate = null;
    protected Double reducedTaxRate = null;

    protected AbstractFinanceRecords(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache cache,
            TaxRates taxRates) {
        super(jdbcTemplate, tables, sessionFactory, cache);
        _taxRates =  taxRates;
        reloadTaxRates();
    }
    
    protected abstract Class<? extends FinanceRecord> getPersistentClass();

    public List<? extends FinanceRecord> getByReference(Long reference) {
        StringBuilder query = new StringBuilder(64);
        query
                .append("from ")
                .append(getPersistentClass().getName())
                .append(" o where o.reference = :referenceId order by o.id desc");
        List<FinanceRecord> result = getCurrentSession().createQuery(query.toString())
                .setParameter("referenceId", reference).list();
        loadAfterFind(result);
        return result;
    }

    public List<? extends FinanceRecord> getByContact(Long contactId) {
        StringBuilder query = new StringBuilder(64);
        query
                .append("from ")
                .append(getPersistentClass().getName())
                .append(" o where o.contact.id = :contactId order by o.id desc");
        List<FinanceRecord> result = getCurrentSession().createQuery(query.toString())
                .setParameter("contactId", contactId).list();
        loadAfterFind(result);
        return result;
    }

    public List<? extends FinanceRecord> getByContact(Long contactId, Long company) {
        StringBuilder query = new StringBuilder(64);
        query
                .append("from ")
                .append(getPersistentClass().getName())
                .append(" o where o.contact.id = :contactId and o.company = :companyId order by o.id desc");
        List<FinanceRecord> result = getCurrentSession().createQuery(query.toString())
                .setParameter("contactId", contactId)
                .setParameter("companyId", company)
                .list();
        loadAfterFind(result);
        return result;
    }
    
    public List<? extends FinanceRecord> getLatest(Date startDate) {
        Date startOfDate = new Date(0);
        if (startDate == null) {
            Property prop = fetchSystemProperty("recordFetchLatestStart");
            if (prop != null) {
                startDate = prop.getAsDate();
                if (startDate == null) {
                    Long l = prop.getAsLong();
                    if (l != null && l <= 600L) {
                        // property value is taken as months, e.g. looks max. 50 years back
                        Date now = new Date(System.currentTimeMillis());
                        startDate = DateUtil.subtractMonths(now, l.intValue());
                    } else {
                        log.warn("findLatest() invalid property found [name=recordFetchLatestStart, "
                                + "value=" + l + "]");
                    }
                }
            }
        }
        if (startDate == null) {
            // 01.01.1970
            startDate = startOfDate;
        }
        List<? extends FinanceRecord> result = getByDate(startDate, null);
        if (result.isEmpty() && startDate.after(startOfDate)) {
            // check for existing records created before startDate  
            result = getByDate(startOfDate, null);
        }
        return result;
    }   
    
    public List<? extends FinanceRecord> getByDate(Date startDate, Date endDate) {
        Date stop = new Date(System.currentTimeMillis());
        if (endDate != null) {
            stop = endDate;
        }
        Date start = DateUtil.subtractDays(stop, 30); 
        if (startDate != null) {
            start = startDate;
        }
        StringBuilder query = new StringBuilder(64);
        query
                .append("from ")
                .append(getPersistentClass().getName())
                .append(" o where o.")
                .append(getByDateProperty())
                .append(" >= :startDate")
                .append(" and o.")
                .append(getByDateProperty())
                .append(" < :endDate order by o.")
                .append(getByDateProperty());
        String hql = query.toString();
        List<FinanceRecord> result = getCurrentSession().createQuery(hql)
                .setParameter("startDate", start)
                .setParameter("endDate", endDate)
                .list();
        if (log.isDebugEnabled()) {
            log.debug("getByDate() done [from=" + start
                    + ", til=" + stop 
                    + ", count=" + result.size()
                    + ", query=" + hql
                    + "]");
        }
        loadAfterFind(result);
        return result;
    }

    public FinanceRecord updateDocumentReference(Long user, FinanceRecord record, Long id) {
        record.updateDocumentId(user, id);
        getCurrentSession().saveOrUpdate(getPersistentClass().getName(), record);
        return record;
    }
    
    protected Double getTaxRate(boolean reduced) {
        return reduced ? reducedTaxRate : taxRate;
    }
    
    protected void reloadTaxRates() {
        taxRate = _taxRates.getCurrentRate();
        reducedTaxRate = _taxRates.getCurrentReducedRate();
    }
    
    protected String getByDateProperty() {
        return "created";
    }
    
    protected void loadAfterFind(List<? extends FinanceRecord> resultingList) {
        // default implementation does nothing
    }
}
