/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 10, 2009 9:23:57 PM 
 * 
 */
package com.osserp.gui.contacts

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.PermissionException
import com.osserp.core.contacts.Contact
import com.osserp.core.contacts.ContactManager
import com.osserp.core.contacts.Phone
import com.osserp.core.contacts.PhoneType
import com.osserp.groovy.web.MenuItem
import com.osserp.groovy.web.ViewContextException

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class ContactPhoneView extends AbstractContactViewAware {
    private static Logger logger = LoggerFactory.getLogger(ContactPhoneView.class.getName())

    Long selectedDevice

    ContactPhoneView() {
        super()
        enablePopupView()
        headerName = 'phone'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            selectedDevice = form.getLong('device')
            headerName = PhoneType.getName(selectedDevice)
            if (contactView) {
                list = contactView.bean?.getPhoneDeviceNumbers(selectedDevice)
                if (!list) {
                    enableCreateMode()
                }
                logger.debug("initRequest: initialized phone numbers [contact=${contactView.bean?.contactId}, device=${selectedDevice}, count=${list.size()}]")
            }
        }
    }

    void delete() {
        Long id = form.getLong('id')
        if (id && contactView) {
            contactManager.deletePhone(domainEmployee, contactView.bean, id)
            reload()
        }
    }

    @Override
    void save() {

        if (createMode) {

            contactManager.addPhone(
                    domainEmployee,
                    contactView.bean,
                    selectedDevice,
                    form.getLong('type'),
                    form.getString('phoneNumber'),
                    form.getString('note'),
                    form.getBoolean('primary'))
        } else if (bean) {

            contactManager.updatePhone(
                    domainEmployee,
                    contactView.bean,
                    bean,
                    form.getLong('type'),
                    form.getString('country'),
                    form.getString('prefix'),
                    form.getString('number'),
                    form.getString('note'),
                    form.getBoolean('primary'))
        }
        disableCreateMode()
        disableEditMode()
        reload()
    }

    @Override
    void reload() {
        contactView?.refresh()
        list = contactView.bean.getPhoneDeviceNumbers(selectedDevice)
        if (bean) {
            bean = list.find { it.id == bean.id }
        }
    }

    @Override
    void select() {
        super.select()
        if (bean) {
            enableEditMode()
        } else {
            disableEditMode()
        }
    }

    protected ContactManager getContactManager() {
        getService(ContactManager.class.getName())
    }
}
