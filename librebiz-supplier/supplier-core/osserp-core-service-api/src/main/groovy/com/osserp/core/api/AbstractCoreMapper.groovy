/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 15.12.2012 
 * 
 */
package com.osserp.core.api

import com.osserp.common.Option
import com.osserp.common.OptionsCache
import com.osserp.common.beans.AbstractClass

import com.osserp.core.dao.SystemConfigs

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class AbstractCoreMapper extends AbstractClass {

    private OptionsCache optionsCache
    protected SystemConfigs systemConfigs

    protected AbstractCoreMapper(OptionsCache optionsCache, SystemConfigs systemConfigs) {
        this.optionsCache = optionsCache
        this.systemConfigs = systemConfigs
    }

    protected Option fetchOrCreateOption(String optionsName, String name) {
        Option result = fetchOption(optionsName, name)
        if (!result) {
            result = optionsCache.add(optionsName, name)
        }
        return result
    }

    protected Option fetchOption(String optionsName, String name) {
        Option existing = null
        if (optionsName && name) {
            List<Option> opts = optionsCache.getList(optionsName)
            opts.each { Option opt ->
                if (opt.name == name) {
                    existing = opt
                }
            }
        }
        return existing
    }

    protected Option fetchOption(String optionsName, Long id) {
        if (id && optionsName) {
            return optionsCache.getMapped(optionsName, id)
        }
        null
    }

    protected String fetchOptionValue(String optionsName, Long id) {
        if (id && optionsName) {
            return optionsCache.getMappedValue(optionsName, id)
        }
        null
    }

    protected boolean isSystemPropertyEnabled(String name) {
        return systemConfigs.isSystemPropertyEnabled(name)
    }

    protected String getSystemProperty(String name) {
        return systemConfigs.getSystemProperty(name)
    }

    protected Long getSystemPropertyId(String name) {
        return systemConfigs.getSystemPropertyId(name)
    }
}
