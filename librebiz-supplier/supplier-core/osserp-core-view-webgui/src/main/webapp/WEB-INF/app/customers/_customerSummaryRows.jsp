<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${!empty view.customerSummary.requests}">
    <c:forEach var="obj" items="${view.customerSummary.requests}" varStatus="s">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="requests">
                        <c:if test="${s.index == 0}">
                            <fmt:message key="request" />
                        </c:if>
                    </label>
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group">
                    <o:out value="${obj.id}" />
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <a href="<c:url value="/loadRequest.do?id=${obj.id}&exit=${requestScope.businessCaseSelectionExit}"/>">
                        <o:out value="${obj.name}" />
                    </a>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group right">
                    <span>
                        <o:number value="${obj.status}" format="percent"/>
                        <fmt:message key="percent"/>
                    </span>
                </div>
            </div>
        </div>
    </c:forEach>
</c:if>
<c:if test="${!empty view.customerSummary.sales}">
    <c:forEach var="obj" items="${view.customerSummary.sales}" varStatus="s">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="requests">
                        <c:if test="${s.index == 0}">
                            <fmt:message key="order" />
                        </c:if>
                    </label>
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group">
                    <o:out value="${obj.id}" />
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <a href="<c:url value="/loadSales.do?id=${obj.id}&exit=${requestScope.businessCaseSelectionExit}"/>">
                        <o:out value="${obj.name}" />
                    </a>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group right">
                    <span>
                        <o:number value="${obj.status}" format="percent"/>
                        <fmt:message key="percent"/>
                    </span>
                </div>
            </div>
        </div>
    </c:forEach>
</c:if>
