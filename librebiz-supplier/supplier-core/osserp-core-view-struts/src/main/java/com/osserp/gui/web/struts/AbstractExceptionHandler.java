/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 16-Aug-2005 09:28:40 
 * 
 */
package com.osserp.gui.web.struts;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ExceptionHandler;
import org.apache.struts.config.ExceptionConfig;

import com.osserp.common.User;
import com.osserp.common.util.ExceptionUtil;

import com.osserp.core.CoreServiceProvider;
import com.osserp.core.users.DomainUser;

import com.osserp.gui.common.UserUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class AbstractExceptionHandler extends ExceptionHandler {

    protected void sendError(
            StringBuffer buffer,
            Throwable ex,
            ExceptionConfig ec,
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {

        //		StringBuffer buffer = new StringBuffer(512);
        buffer
                .append("\nExeption caught: ").append(ex.getClass().getName())
                .append(getUserInfo(request));
        if (form != null && mapping.getInput() != null) {
            buffer.append("\nInput: ").append(mapping.getInput());
        }
        buffer
                .append("\n\nRequest URI: ").append(request.getRequestURI())
                .append("\nRequest params:");

        Enumeration<String> params = request.getParameterNames();
        while (params.hasMoreElements()) {
            String name = (String) params.nextElement();
            String value = request.getParameter(name);
            buffer.append("\n").append(name).append(": ").append(value);
        }
        buffer
                .append("\nStackTrace:\n")
                .append(ExceptionUtil.stackTraceToString(ex));
        CoreServiceProvider.sendError(request, buffer.toString());
    }

    private String getUserInfo(HttpServletRequest request) {
        StringBuffer buffer = new StringBuffer("\nUser: ");
        try {
            User user = UserUtil.getUser(request.getSession());
            buffer.append(user.getId()).append(": ");
            if (user instanceof DomainUser) {
                buffer.append(((DomainUser) user).getEmployee().getName());
            } else {
                buffer.append(user.getLoginName());
            }
        } catch (Exception ignorable) {
            buffer.append("no user logged in, " + ignorable.toString());
        }
        return buffer.toString();
    }
}
