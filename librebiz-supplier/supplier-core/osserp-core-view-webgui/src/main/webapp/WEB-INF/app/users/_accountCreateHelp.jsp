<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4><fmt:message key="accountCreateHeader"/></h4>
        </div>
    </div>
    <div class="panel-body">
        
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="accountCreateInfoText"><fmt:message key="accountCreateInfoHeader"/></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <ol>
                        <li><fmt:message key="accountCreateInfoText1" />.</li>
                        <li><fmt:message key="accountCreateInfoText2" />.</li>
                        <li><fmt:message key="accountCreateInfoText3" />.</li>
                    </ol>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="accountCreateAdvice"><fmt:message key="accountCreateAdviceHeader"/></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <ol>
                        <li><fmt:message key="accountCreateAdviceText1" />.</li>
                        <li><fmt:message key="accountCreateAdviceText2" />.</li>
                        <li><fmt:message key="accountCreateAdviceText3" />.</li>
                    </ol>
                </div>
            </div>
        </div>

    </div>
</div>
