/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 20, 2016 
 * 
 */
package com.osserp.core.service.contacts;

import java.util.Date;

import com.osserp.common.ClientException;
import com.osserp.common.Option;
import com.osserp.common.PermissionException;

import com.osserp.core.contacts.Person;
import com.osserp.core.contacts.PersonManager;
import com.osserp.core.contacts.Salutation;
import com.osserp.core.service.impl.AbstractService;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractPersonManager extends AbstractService implements PersonManager {

    
    public Person updatePerson(
            DomainUser user,
            Person person,
            Salutation salutation,
            Option title,
            boolean firstNamePrefix,
            String firstName,
            String lastName,
            String street,
            String zipcode,
            String city,
            Long federalState,
            String federalStateName,
            Long country) throws ClientException, PermissionException {
        Person result = deploy(
                user, 
                person, 
                salutation, 
                title, 
                firstNamePrefix, 
                firstName, 
                lastName, 
                street, 
                zipcode, 
                city, 
                federalState, 
                federalStateName, 
                country);
        if (result != null) {
            save(result);
        }
        return result;
    }
    
    /**
     * Sets values on provided person and returns updated object. Note:
     * This method is local helper and does not persist the object.
     * @param user
     * @param person
     * @param salutation
     * @param title
     * @param firstNamePrefix
     * @param firstName
     * @param lastName
     * @param street
     * @param zipcode
     * @param city
     * @param federalState
     * @param federalStateName
     * @param country
     * @return person with values set
     * @throws ClientException
     * @throws PermissionException
     */
    protected Person deploy(
            DomainUser user,
            Person person,
            Salutation salutation,
            Option title,
            boolean firstNamePrefix,
            String firstName,
            String lastName,
            String street,
            String zipcode,
            String city,
            Long federalState,
            String federalStateName,
            Long country) throws ClientException, PermissionException {
        if (person != null) {
            person.setSalutation(salutation);
            person.setTitle(title);
            person.setFirstNamePrefix(firstNamePrefix);
            person.setFirstName(firstName);
            person.setLastName(lastName);
            if (person.getAddress() != null) {
                person.getAddress().setStreet(street);
                person.getAddress().setZipcode(zipcode);
                person.getAddress().setCity(city);
                person.getAddress().setFederalStateId(federalState);
                person.getAddress().setFederalStateName(federalStateName);
                person.getAddress().setCountry(country);
            }
            person.setPersonChanged(new Date(System.currentTimeMillis()));
            person.setPersonChangedBy(user.getId());
            return person;
        }
        return person;
    }

}
