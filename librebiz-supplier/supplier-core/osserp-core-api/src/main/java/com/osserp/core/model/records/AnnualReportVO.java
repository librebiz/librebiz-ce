/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 12, 2016
 * 
 */
package com.osserp.core.model.records;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.util.CollectionUtil;
import com.osserp.common.util.DateUtil;
//import com.osserp.common.util.DateFormatter;
import com.osserp.common.util.NumberFormatter;

import com.osserp.core.BankAccount;
import com.osserp.core.finance.AnnualReport;
import com.osserp.core.finance.BillingType;
import com.osserp.core.finance.CommonPaymentSummary;
import com.osserp.core.finance.PaymentDisplay;
import com.osserp.core.finance.RecordSummary;
import com.osserp.core.finance.Records;
import com.osserp.core.purchasing.PurchaseRecordSummary;
import com.osserp.core.sales.SalesRecordSummary;
import com.osserp.core.system.SystemCompany;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public class AnnualReportVO extends AbstractFinanceReport implements AnnualReport {
    private static Logger log = LoggerFactory.getLogger(AnnualReportVO.class.getName());

    private SystemCompany company;
    
    private List<PaymentDisplay> allPayments = new ArrayList<>();
    
    private List<SalesRecordSummary> salesRecords = new ArrayList<>();
    private List<PurchaseRecordSummary> purchaseRecords = new ArrayList<>();
    private List<CommonPaymentSummary> commonRecords = new ArrayList<>();
    
    private List<RecordSummary> supplierRecords = new ArrayList<>();
    private double supplierAmount = 0;
    private double supplierCreditAmount = 0;

    private Double amountOutstanding = 0d;
    private Double amountToPay = 0d;
    
    private Double cashIn = 0d;
    private Double cashOut = 0d;
    
    private List<PaymentDisplay> cashInList = new ArrayList<>();
    private List<PaymentDisplay> cashOutList = new ArrayList<>();
    
    private Double salesAmount = 0d;
    private Double salesGrossAmount = 0d;
    
    private Double purchaseAmount = 0d;
    private Double purchaseGrossAmount = 0d;
    
    private Double costsAmount = 0d;
    private Double costsGrossAmount = 0d;
    private Double costsTaxfreeAmount = 0d;
    private Double salaryAmount = 0d;
    private Double salaryRefundAmount = 0d;
    private Double taxPaymentAmount = 0d;
    private Double taxRefundAmount = 0d;
    
    /**
     * Default constructor required by serialization
     */
    protected AnnualReportVO() {
        super();
    }

    /**
     * Creates a new annual report.
     * @param company
     * @param year
     */
    public AnnualReportVO(SystemCompany company, int year) {
        super(company.getId(), year);
        this.company = company;
    }

    /**
     * Creates reports by provided records 
     * @param payments
     * @param sales
     * @param purchases
     * @param common
     */
    public void createReport(
            List<PaymentDisplay> payments, 
            List<SalesRecordSummary> sales, 
            List<PurchaseRecordSummary> purchases,
            List<CommonPaymentSummary> common) {
        allPayments = payments;
        salesRecords = sales;
        purchaseRecords = purchases;
        commonRecords = common;
        calculate();
    }
    
    public String getBankAccountBySummary(RecordSummary summary) {
        Long bankId = null;
        if (summary instanceof CommonPaymentSummary) {
            bankId = ((CommonPaymentSummary) summary).getBankAccountId();
        } else {
            for (int i = 0, j = allPayments.size(); i < j; i++) {
                PaymentDisplay next = allPayments.get(i);
                if (!next.isAccountStatusOnly() && summary.getId().equals(next.getInvoiceId())) {
                    if (summary instanceof SalesRecordSummary && next.isSalesInvoicePayment()) {
                        log.debug("getBankAccountBySummary() sales payment found [invoice=" 
                            + summary.getId() + ", payment=" + next.getId() + ", bank="
                            + next.getBankAccountId() + "]");
                        bankId = next.getBankAccountId();
                        break;
                    }
                    if (summary instanceof PurchaseRecordSummary && next.isPurchaseInvoicePayment()) {
                        log.debug("getBankAccountBySummary() purchase payment found [invoice=" 
                            + summary.getId() + ", payment=" + next.getId() + ", bank="
                            + next.getBankAccountId() + "]");
                        bankId = next.getBankAccountId();
                        break;
                    }
                    log.debug("getBankAccountBySummary() did not find payment [invoice=" 
                        + next.getInvoiceId() + "payment=" + next.getId() + "]");
                }
            }
        }
        return bankAccountById(bankId);
    }
    
    public String getBankAccountByPayment(PaymentDisplay payment) {
        return bankAccountById(payment == null ? null : payment.getBankAccountId());
    }

    private String bankAccountById(Long bankId) {
        if (isSet(bankId) && company != null && !company.getBankAccounts().isEmpty()) {
            for (int i = 0, j = company.getBankAccounts().size(); i < j; i++) {
                BankAccount next = company.getBankAccounts().get(i);
                if (next.getId().equals(bankId)) {
                    if (isSet(next.getShortkey())) {
                        return next.getShortkey();
                    }
                    return next.getBankAccountNumberIntl();
                }
            }
        }
        return "";
    }
    
    protected void calculate() {
        reset();
        for (int i = 0, j = salesRecords.size(); i < j; i++) {
            SalesRecordSummary next = salesRecords.get(i);
            if (!next.isCanceled() && !next.isCancellation()) {
                if (next.isCreditNote()) {
                    salesAmount = salesAmount - next.getAmount();
                    salesGrossAmount = salesGrossAmount - next.getGross();
                    // TODO handle credit note payments
                
                } else if (!next.isPayment() && !next.isDownpayment()) {
                    salesAmount = salesAmount + next.getAmount();
                    salesGrossAmount = salesGrossAmount + next.getGross();
                    if (next.isAmountOpen()) {
                        amountOutstanding = amountOutstanding + next.getOpenAmount();
                    }
                }
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("calculate() sales done [net=" + NumberFormatter.getValue(salesAmount, NumberFormatter.CURRENCY)
                + ", gross=" + NumberFormatter.getValue(salesGrossAmount, NumberFormatter.CURRENCY)
                + ", outstanding=" + NumberFormatter.getValue(amountOutstanding, NumberFormatter.CURRENCY)
                + "]");
        }
        for (int i = 0, j = purchaseRecords.size(); i < j; i++) {
            PurchaseRecordSummary next = purchaseRecords.get(i);
            if (!next.isCanceled() && !next.isCancellation()) {
                supplierRecords.add(next);
                if (next.isCreditNote()) {
                    purchaseAmount = purchaseAmount - next.getAmount();
                    purchaseGrossAmount = purchaseGrossAmount - next.getGross();
                    //  TODO handle credit note payments
                
                    supplierCreditAmount = supplierCreditAmount + next.getAmount();
                    
                } else if (!next.isPayment() && !next.isDownpayment()) {
                    purchaseAmount = purchaseAmount + next.getAmount();
                    purchaseGrossAmount = purchaseGrossAmount + next.getGross();
                    if (next.isAmountOpen()) {
                        amountToPay = amountToPay + next.getOpenAmount();
                    }
                    supplierAmount = supplierAmount + next.getAmount();
                }
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("calculate() purchases done [net=" + NumberFormatter.getValue(purchaseAmount, NumberFormatter.CURRENCY)
                + ", gross=" + NumberFormatter.getValue(purchaseGrossAmount, NumberFormatter.CURRENCY)
                + ", topay=" + NumberFormatter.getValue(amountToPay, NumberFormatter.CURRENCY)
                + "]");
        }
        for (int i = 0, j = commonRecords.size(); i < j; i++) {
            
            CommonPaymentSummary next = commonRecords.get(i);
            if (!next.isBooksOnly()) {
                BillingType billingType = (BillingType) next.getType();
            
                if (next.isAmountOpen()) {
                    // not paid, required or expected payment
                    if (next.isOutpayment()) {
                        
                        amountToPay = amountToPay + next.getOpenAmount();
                        if (next.isTaxFree()) {
                            supplierAmount = supplierAmount + next.getGross();
                        } else {
                            supplierAmount = supplierAmount + next.getGross() / next.getTaxRate();
                        }
                    } else {
                        amountOutstanding = amountOutstanding + next.getOpenAmount();
                        if (next.isTaxFree()) {
                            supplierCreditAmount = supplierCreditAmount + next.getGross();
                        } else {
                            supplierCreditAmount = supplierCreditAmount + next.getGross() / next.getTaxRate();
                        }
                    }
                    supplierRecords.add(next);
                } else {
                    if (billingType.isSalary()) {
                        applySalary(next);
                    } else if (billingType.isTaxPayment()) {
                        applyTax(next);
                    } else {
                        applyOther(next);
                        supplierRecords.add(next);
                        if (next.isOutpayment()) {
                            if (next.isTaxFree()) {
                                supplierAmount = supplierAmount + next.getGross();
                            } else {
                                supplierAmount = supplierAmount + next.getGross() / next.getTaxRate();
                            }
                        } else {
                            if (next.isTaxFree()) {
                                supplierCreditAmount = supplierCreditAmount + next.getGross();
                            } else {
                                supplierCreditAmount = supplierCreditAmount + next.getGross() / next.getTaxRate();
                            }
                        }
                    }
                }
                /*
                if (log.isDebugEnabled()) {
                    log.debug("calculate() [id=" + next.getId()
                        + ", out=" + next.isOutpayment()
                        + ", open=" + next.isAmountOpen()
                        + ", type=" + next.getTypeId()
                        + ", created=" + DateFormatter.getDate(next.getCreated())
                        + ", gross=" + NumberFormatter.getValue(next.getGross(), NumberFormatter.CURRENCY)
                        + ", paid=" + NumberFormatter.getValue(next.getPaidAmount(), NumberFormatter.CURRENCY)
                        + ", taxfree=" + next.isTaxFree()
                        + ", netcosts=" + NumberFormatter.getValue(costsAmount, NumberFormatter.CURRENCY)
                        + ", costs=" + NumberFormatter.getValue(costsGrossAmount, NumberFormatter.CURRENCY)
                        + ", taxfreecosts=" + NumberFormatter.getValue(costsTaxfreeAmount, NumberFormatter.CURRENCY)
                        + ", tax=" + NumberFormatter.getValue(taxPaymentAmount, NumberFormatter.CURRENCY)
                        + ", taxRefund=" + NumberFormatter.getValue(taxRefundAmount, NumberFormatter.CURRENCY)
                        + ", salary=" + NumberFormatter.getValue(salaryAmount, NumberFormatter.CURRENCY)
                        + ", salaryRefund=" + NumberFormatter.getValue(salaryRefundAmount, NumberFormatter.CURRENCY)
                        + ", name=" + next.getName()
                        + "]");
                }
                */
            }
        }
        
        Records.sortSummaryByCreated(supplierRecords, false);
        
        for (int i = 0, j = allPayments.size(); i < j; i++) {
            PaymentDisplay pd = allPayments.get(i);
            if (!pd.isAccountStatusOnly() && !pd.isBooksOnly() 
                    && inYear(pd.getPaidOn())) {
                
                if (pd.isOutflowOfCash()) {
                    cashOut = cashOut + pd.getGrossAmount();
                    cashOutList.add(pd);
                    /*
                    if (log.isDebugEnabled()) {
                        log.debug("calculate() out [paid=" + DateFormatter.getDate(pd.getPaidOn())
                                + ", id=" + pd.getId()
                                + ", amount=" + NumberFormatter.getValue(pd.getGrossAmount(), NumberFormatter.CURRENCY)
                                + ", cashOut=" + NumberFormatter.getValue(cashOut, NumberFormatter.CURRENCY)
                                + ", name=" + pd.getContactName()
                                + "]");
                    }
                    */
                } else {
                    cashIn = cashIn + pd.getGrossAmount();
                    cashInList.add(pd);
                    /*
                    if (log.isDebugEnabled()) {
                        log.debug("calculate() in [paid=" + DateFormatter.getDate(pd.getPaidOn())
                                + ", id=" + pd.getId()
                                + ", amount=" + NumberFormatter.getValue(pd.getGrossAmount(), NumberFormatter.CURRENCY)
                                + ", cashIn=" + NumberFormatter.getValue(cashIn, NumberFormatter.CURRENCY)
                                + ", name=" + pd.getContactName()
                                + "]");
                    }
                    */
                }
            }
        }
        cashInList = CollectionUtil.reverse(cashInList);
        cashOutList = CollectionUtil.reverse(cashOutList);
        logReport();
    }
    
    protected boolean inYear(Date date) {
        return (date != null && DateUtil.getYear(date) == getYear());
    }
    
    private void applyOther(CommonPaymentSummary payment) {
        if (payment.isOutpayment()) {
            if (payment.isTaxFree()) {
                costsTaxfreeAmount = costsTaxfreeAmount + payment.getPaidAmount();
            } else {
                costsAmount = costsAmount + payment.getPaidAmount() / payment.getTaxRate();
                costsGrossAmount = costsGrossAmount + payment.getPaidAmount();
            }
        } else {
            if (payment.isTaxFree()) {
                costsTaxfreeAmount = costsTaxfreeAmount - payment.getPaidAmount();
            } else {
                costsAmount = costsAmount - payment.getPaidAmount() / payment.getTaxRate();
                costsGrossAmount = costsGrossAmount - payment.getPaidAmount(); 
            }
        }
    }
    
    private void applySalary(CommonPaymentSummary payment) {
        if (payment.isOutpayment()) {
            salaryAmount = salaryAmount + payment.getPaidAmount();
        } else {
            salaryRefundAmount = salaryRefundAmount - payment.getPaidAmount();
        }
    }
    
    private void applyTax(CommonPaymentSummary payment) {
        if (payment.isOutpayment()) {
            taxPaymentAmount = taxPaymentAmount + payment.getPaidAmount();
        } else {
            taxRefundAmount = taxRefundAmount + payment.getPaidAmount();
        }
    }
    
    public SystemCompany getCompany() {
        return company;
    }

    
    public List<SalesRecordSummary> getSalesRecords() {
        return salesRecords;
    }

    public List<PurchaseRecordSummary> getPurchaseRecords() {
        return purchaseRecords;
    }

    public List<CommonPaymentSummary> getCommonRecords() {
        return commonRecords;
    }
    
    public List<RecordSummary> getSupplierRecords() {
        return supplierRecords;
    }

    public Double getAmountOutstanding() {
        return amountOutstanding;
    }

    public Double getAmountToPay() {
        return amountToPay;
    }

    
    public Double getCashIn() {
        return cashIn;
    }
    
    public List<PaymentDisplay> getCashInList() {
        return cashInList;
    }

    public Double getCashOut() {
        return cashOut;
    }

    public List<PaymentDisplay> getCashOutList() {
        return cashOutList;
    }
    
    public Double getSalesAmount() {
        return salesAmount;
    }

    public Double getSalesGrossAmount() {
        return salesGrossAmount;
    }
    
    public Double getSalesTaxAmount() {
        return (salesAmount == null || salesGrossAmount == null)
                ? 0d : salesGrossAmount - salesAmount;
    }

    public Double getPurchaseAmount() {
        return purchaseAmount;
    }

    public Double getPurchaseGrossAmount() {
        return purchaseGrossAmount;
    }
    
    public Double getPurchaseTaxAmount() {
        return (purchaseAmount == null || purchaseGrossAmount == null)
                ? 0d : purchaseGrossAmount - purchaseAmount;
    }

    public Double getCostsAmount() {
        return costsAmount;
    }

    public Double getCostsGrossAmount() {
        return costsGrossAmount;
    }

    public Double getCostsTaxfreeAmount() {
        return costsTaxfreeAmount;
    }

    public Double getSalaryAmount() {
        return salaryAmount;
    }

    public Double getSalaryRefundAmount() {
        return salaryRefundAmount;
    }

    public Double getTaxPaymentAmount() {
        return taxPaymentAmount;
    }

    public Double getTaxRefundAmount() {
        return taxRefundAmount;
    }
    
    public Double getPurchaseTotalAmount() {
        return (purchaseAmount == null ? 0d : purchaseAmount.doubleValue())
                + (costsAmount == null ? 0d : costsAmount.doubleValue())
                + (costsTaxfreeAmount == null ? 0d : costsTaxfreeAmount.doubleValue());
    }

    public double getSupplierAmount() {
        return supplierAmount;
    }

    public double getSupplierCreditAmount() {
        return supplierCreditAmount;
    }

    protected List<PaymentDisplay> getAllPayments() {
        return allPayments;
    }

    protected void setAllPayments(List<PaymentDisplay> allPayments) {
        this.allPayments = allPayments;
    }

    protected void setSalesRecords(List<SalesRecordSummary> salesRecords) {
        this.salesRecords = salesRecords;
    }

    protected void setPurchaseRecords(List<PurchaseRecordSummary> purchaseRecords) {
        this.purchaseRecords = purchaseRecords;
    }

    protected void setCommonRecords(List<CommonPaymentSummary> commonRecords) {
        this.commonRecords = commonRecords;
    }

    protected void setAmountExpected(Double amountExpected) {
        this.amountOutstanding = amountExpected;
    }

    protected void setAmountToPay(Double amountToPay) {
        this.amountToPay = amountToPay;
    }

    protected void setCashIn(Double cashIn) {
        this.cashIn = cashIn;
    }

    protected void setCashInList(List<PaymentDisplay> cashInList) {
        this.cashInList = cashInList;
    }

    protected void setCashOut(Double cashOut) {
        this.cashOut = cashOut;
    }

    protected void setCashOutList(List<PaymentDisplay> cashOutList) {
        this.cashOutList = cashOutList;
    }

    protected void setSalesAmount(Double salesAmount) {
        this.salesAmount = salesAmount;
    }

    protected void setSalesGrossAmount(Double salesGrossAmount) {
        this.salesGrossAmount = salesGrossAmount;
    }

    protected void setPurchaseAmount(Double purchaseAmount) {
        this.purchaseAmount = purchaseAmount;
    }

    protected void setPurchaseGrossAmount(Double purchaseGrossAmount) {
        this.purchaseGrossAmount = purchaseGrossAmount;
    }

    protected void setCostsAmount(Double costsAmount) {
        this.costsAmount = costsAmount;
    }

    protected void setCostsGrossAmount(Double costsGrossAmount) {
        this.costsGrossAmount = costsGrossAmount;
    }

    protected void setCostsTaxfreeAmount(Double costsTaxfreeAmount) {
        this.costsTaxfreeAmount = costsTaxfreeAmount;
    }

    protected void setSalaryRefundAmount(Double salaryRefundAmount) {
        this.salaryRefundAmount = salaryRefundAmount;
    }

    protected void setTaxPaymentAmount(Double taxPaymentAmount) {
        this.taxPaymentAmount = taxPaymentAmount;
    }

    protected void setTaxRefundAmount(Double taxRefundAmount) {
        this.taxRefundAmount = taxRefundAmount;
    }

    protected void setSalaryAmount(Double salaryAmount) {
        this.salaryAmount = salaryAmount;
    }

    private void reset() {
        amountOutstanding = 0d;
        amountToPay = 0d;
        
        cashIn = 0d;
        cashInList = new ArrayList<>();
        cashOut = 0d;
        cashOutList = new ArrayList<>();
        
        purchaseAmount = 0d;
        purchaseGrossAmount = 0d;
        costsAmount = 0d;
        costsGrossAmount = 0d; 
        costsTaxfreeAmount = 0d; 
        salaryAmount = 0d;
        salaryRefundAmount = 0d;
        taxPaymentAmount = 0d;
        taxRefundAmount = 0d;
        
        salesAmount = 0d;
        salesGrossAmount = 0d;
        supplierRecords = new ArrayList<>();
        supplierAmount = 0;
        supplierCreditAmount = 0;
    }
    
    private void logReport() {
        if (log.isDebugEnabled()) {
            StringBuilder buffer = new StringBuilder();
            buffer.append("AnnualReport ").append(getId()).append(":");
            
            buffer.append("\nsalesAmount=");
            buffer.append(NumberFormatter.getValue(salesAmount, NumberFormatter.CURRENCY));
            
            buffer.append("\nsalesGrossAmount=");
            buffer.append(NumberFormatter.getValue(salesGrossAmount, NumberFormatter.CURRENCY));
            
            buffer.append("\npurchaseAmount=");
            buffer.append(NumberFormatter.getValue(purchaseAmount, NumberFormatter.CURRENCY));
            
            buffer.append("\npurchaseGrossAmount=");
            buffer.append(NumberFormatter.getValue(purchaseGrossAmount, NumberFormatter.CURRENCY));
            
            buffer.append("\ncostsAmount=");
            buffer.append(NumberFormatter.getValue(costsAmount, NumberFormatter.CURRENCY));
            
            buffer.append("\ncostsGrossAmount="); 
            buffer.append(NumberFormatter.getValue(costsGrossAmount, NumberFormatter.CURRENCY));
            
            buffer.append("\ncostsTaxfreeAmount="); 
            buffer.append(NumberFormatter.getValue(costsTaxfreeAmount, NumberFormatter.CURRENCY));
            
            buffer.append("\ntaxPaymentAmount=");
            buffer.append(NumberFormatter.getValue(taxPaymentAmount, NumberFormatter.CURRENCY));
            
            buffer.append("\ntaxRefundAmount=");
            buffer.append(NumberFormatter.getValue(taxRefundAmount, NumberFormatter.CURRENCY));
            
            buffer.append("\nsalaryAmount=");
            buffer.append(NumberFormatter.getValue(salaryAmount, NumberFormatter.CURRENCY));
            
            buffer.append("\nsalaryRefundAmount=");
            buffer.append(NumberFormatter.getValue(salaryRefundAmount, NumberFormatter.CURRENCY));
            
            buffer.append("\namountExpected=");
            buffer.append(NumberFormatter.getValue(amountOutstanding, NumberFormatter.CURRENCY));
            
            buffer.append("\namountToPay=");
            buffer.append(NumberFormatter.getValue(amountToPay, NumberFormatter.CURRENCY));
            
            buffer.append("\ncashIn=");
            buffer.append(NumberFormatter.getValue(cashIn, NumberFormatter.CURRENCY));
            
            buffer.append("\ncashOut=");
            buffer.append(NumberFormatter.getValue(cashOut, NumberFormatter.CURRENCY));
            
            buffer.append("\nsupplierAmount=");
            buffer.append(NumberFormatter.getValue(supplierAmount, NumberFormatter.CURRENCY));
            
            buffer.append("\nsupplierCreditAmount=");
            buffer.append(NumberFormatter.getValue(supplierCreditAmount, NumberFormatter.CURRENCY));
            
            buffer.append("\npurchaseTotalAmount=");
            buffer.append(NumberFormatter.getValue(getPurchaseTotalAmount(), NumberFormatter.CURRENCY));
            
            log.debug(buffer.toString());
        }
    }
}
