/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 7, 2006 4:14:50 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;
import org.hibernate.SessionFactory;

import com.osserp.common.ActionTarget;
import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.common.Permission;
import com.osserp.common.User;
import com.osserp.common.beans.OptionImpl;
import com.osserp.common.dao.Tables;
import com.osserp.common.util.DateFormatter;
import com.osserp.common.util.StringUtil;

import com.osserp.core.contacts.Contact;
import com.osserp.core.dao.Employees;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.dao.Users;
import com.osserp.core.employees.Employee;
import com.osserp.core.employees.EmployeeGroup;
import com.osserp.core.employees.EmployeeRole;
import com.osserp.core.employees.EmployeeRoleConfig;
import com.osserp.core.model.DomainUserImpl;
import com.osserp.core.model.PermissionImpl;
import com.osserp.core.model.UserImpl;
import com.osserp.core.model.UserStartupTargetImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class UsersDao extends AbstractRepository implements Users {
    private static Logger log = LoggerFactory.getLogger(UsersDao.class.getName());
    private static final Long DEFAULT_STARTUP = 1L;
    private String usersTable = null;
    private String userPermissionsTable = null;
    private String usersPermissionsView = null;
    private Employees employees = null;

    public UsersDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            Employees employees) {
        super(jdbcTemplate, tables, sessionFactory);
        this.employees = employees;
        this.usersTable = getTable(TableKeys.USERS);
        this.userPermissionsTable = getTable(TableKeys.USER_PERMISSIONS);
        this.usersPermissionsView = getTable(TableKeys.USERS_PERMISSIONS_VIEW);
    }

    public User create(Long createdBy, Employee employee) {
        Set<String> existing = getExistingNames();
        StringBuilder buffer = new StringBuilder();
        buffer.append(employee.getFirstName()).append(" ").append(employee.getLastName());
        String loginName = buffer.toString();
        if (existing.contains(loginName)) {
            StringUtil.deleteBlanks(buffer);
            loginName = buffer.toString();
            if (existing.contains(loginName)) {
                buffer.append(DateFormatter.getCurrentDate());
                loginName = buffer.toString();
            }
        }
        Long nextId = null;
        if (employee.getId() != null) {
            nextId = employee.getId();
        } else {
            nextId = getNextLong(getDefaultSequenceName(usersTable));
        }
        UserImpl newUser = new UserImpl(
                nextId,
                createdBy,
                employee,
                loginName,
                loadDefaultActionTarget());
        newUser.setLocaleCountry(Constants.DEFAULT_COUNTRY);
        newUser.setLocaleLanguage(Constants.DEFAULT_LANGUAGE);
        getCurrentSession().save(UserImpl.class.getName(), newUser);
        return newUser;
    }

    public List<Permission> getPermissions() {
        return getCurrentSession().createQuery(
                "from " + PermissionImpl.class.getName() +
                " o where o.administrative = false and o.disabled = false order by o.description")
                .list();
    }

    public User login(String loginName, String password, String remoteIp) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("login() invoked [user=" + loginName + ", ip=" + remoteIp + "]");
        }
        if (isEmpty(loginName)) {
            throw new ClientException(ErrorCode.LOGIN_NAME_MISSING);
        } else if (isEmpty(password)) {
            throw new ClientException(ErrorCode.PASSWORD_MISSING);
        }
        List<User> list = getCurrentSession().createQuery(
                "from " + UserImpl.class.getName() 
                + " o where o.ldapName = :uid and o.password = :pwd")
                .setParameter("uid", loginName)
                .setParameter("pwd", password)
                .list();
        if (list.isEmpty()) {
            list = getCurrentSession().createQuery(
                    "from " + UserImpl.class.getName() + " o where o.loginName = :uid")
                    .setParameter("uid", loginName)
                    .list();
            if (list.isEmpty()) {
                throw new ClientException(ErrorCode.LOGIN_NAME_INVALID);
            }
            throw new ClientException(ErrorCode.PASSWORD_INVALID);
        }
        User user = list.get(0);
        user.setRemoteIp(remoteIp);
        return loadRelated(user);
    }

    public void save(User user) {
        assert (user != null);
        if (isEmpty(user.getId())) {
            getCurrentSession().saveOrUpdate(UserImpl.class.getName(), user);
        } else {
            getCurrentSession().merge(UserImpl.class.getName(), user);
        }
    }

    public List<User> findActive() {
        return getCurrentSession().createQuery(
                "from " + UserImpl.class.getName() + " o where o.active = true order by loginname")
                .list();
    }

    public User find(Long id) {
        return loadRelated(load(id));
    }

    public User find(String loginName) {
        List<User> users = getCurrentSession().createQuery(
                "from " + UserImpl.class.getName() 
                + " o where o.loginName = :lname or o.ldapUid = :uid")
                .setParameter("lname", loginName)
                .setParameter("uid", loginName )
                .list();
        if (users.isEmpty()) {
            return null;
        }
        return loadRelated(users.get(0));
    }

    public User find(Employee employee) {
        if (employee == null) {
            return null;
        }
        Long id = getUserByEmployee(employee.getId());
        if (id == null || id == 0) {
            return null;
        }
        User user = load(id);
        return new DomainUserImpl(user, employee);
    }

    public User find(Contact contact) {
        if (contact == null) {
            return null;
        }
        if (contact instanceof Employee) {
            Employee employee = (Employee) contact;
            Long id = getUserByEmployee(employee.getId());
            if (id == null || id == 0) {
                return null;
            }
            return loadRelated(load(id));
        }
        List<User> users = getCurrentSession().createQuery(
                "from " + UserImpl.class.getName()
                + " o where o.contactId = :contactid")
                .setParameter("contactid", contact.getContactId())
                .list();
        if (users.isEmpty()) {
            return null;
        }
        return loadRelated(users.get(0));
    }

    public User get(String loginName) throws ClientException {
        List<User> users = getCurrentSession().createQuery(
                "from " + UserImpl.class.getName()
                + " o where o.loginName = :lname or o.ldapUid = :uid")
                .setParameter("lname", loginName)
                .setParameter("uid", loginName)
                .list();
        if (users.isEmpty()) {
            throw new ClientException(ErrorCode.LOGIN_NAME_INVALID);
        }
        return loadRelated(users.get(0));
    }

    public boolean isPermissionGrant(Long userId, String permission) {
        if (userId == null || permission == null) {
            throw new BackendException(ErrorCode.APPLICATION_CONFIG);
        }
        StringBuilder sql = new StringBuilder();
        sql
                .append("SELECT count(*) FROM ")
                .append(userPermissionsTable)
                .append(" WHERE user_id = ? AND permission = ?");
        final Object[] params = { userId, permission };
        final int[] types = { Types.BIGINT, Types.VARCHAR };

        return (jdbcTemplate.queryForObject(sql.toString(), params, types, Integer.class) > 0);
    }

    public List<Option> getLoginNames() {
        StringBuffer sql = new StringBuffer(64);
        sql
                .append("SELECT id, loginname as name, (not isactive) as end_of_life FROM ")
                .append(getTable(TableKeys.USERS))
                .append(" ORDER BY loginname");
        return (List<Option>) jdbcTemplate.query(sql.toString(), getOptionRowMapper());
    }

    public List<Option> getNames() {
        Map<Long, String> map = getEmployees();
        StringBuffer query = new StringBuffer(64);
        query
                .append("SELECT id,contact_id,loginname FROM ")
                .append(TableKeys.USERS)
                .append(" ORDER BY loginname");
        List<Option> result = new ArrayList<Option>();
        List<UserContactVO> users = (List<UserContactVO>) jdbcTemplate.query(
                query.toString(),
                new RowMapperResultSetExtractor(new UserContactRowMapper()));
        for (int i = 0, j = users.size(); i < j; i++) {
            UserContactVO vo = users.get(i);
            if (vo.contact != null && map.containsKey(vo.contact)) {
                result.add(new OptionImpl(vo.user, map.get(vo.contact)));
            } else {
                result.add(new OptionImpl(vo.user, vo.name));
            }
        }
        return result;
    }

    public Long getUserByEmployee(Long employeeId) {
        if (employeeId == null) {
            return null;
        }
        final Object[] params = { employeeId };
        final int[] types = { Types.BIGINT };
        return jdbcTemplate.queryForObject(Procs.USER_BY_EMPLOYEE, params, types, Long.class);
    }

    public List<Option> getUsersByPermission(String permission) {
        StringBuffer query = new StringBuffer(64);
        query
                .append("SELECT id, displayname AS name FROM ")
                .append(usersPermissionsView)
                .append(" WHERE permission = ? AND contact_id NOT IN ")
                .append("(SELECT contact_id FROM contacts WHERE status_id = -10)");
        final Object[] params = { permission };
        final int[] types = { Types.VARCHAR };
        return (List<Option>) jdbcTemplate.query(query.toString(), params, types, getOptionRowMapper());
    }

    public Long getContactId(Long userId) {
        if (userId == null) {
            return null;
        }
        StringBuffer query = new StringBuffer(64);
        query
                .append(" SELECT contact_id FROM ")
                .append(TableKeys.USERS)
                .append(" WHERE id = ?)");
        final Object[] params = { userId };
        final int[] types = { Types.BIGINT };
        return jdbcTemplate.queryForObject(
                query.toString(), params, types, Long.class);
    }

    public User grantPermission(User grantBy, Employee employee, String permission) {
        return grantPermission(grantBy, find(employee), permission);
    }

    public User grantPermission(User grantBy, User user, String permission) {
        if (user != null && permission != null 
                && !user.isPermissionAssigned(permission)
                && grantBy != null) {
            user.addPermission(grantBy.getId(), permission);
            save(user);
            user = find(user.getId());
            if (log.isDebugEnabled()) {
                log.debug("grantPermission() done [user=" + user.getId()
                        + ", permission=" + permission + "]");
            }
            
        } else if (log.isDebugEnabled() && user != null && permission != null) {
            log.debug("grantPermission() already grant [user=" 
                    + user.getId()  + ", permission=" + permission + "]");
        }
        return user;
    }

    public User revokePermission(User revokedBy, Employee employee, String permission) {
        return revokePermission(revokedBy, find(employee), permission);
    }

    public User revokePermission(User revokedBy, User user, String permission) {
        if (user != null && permission != null && revokedBy != null) {
            user.removePermission(permission);
            user.updateChanged(revokedBy.getId());
            save(user);
            user = find(user.getId());
            if (log.isDebugEnabled()) {
                log.debug("revokePermission() done [employee=" + user.getId()
                        + ", permission=" + permission + "]");
            }
        }
        return user;
    }

    public void grantPermissions(Employee grantBy, Employee employee, EmployeeGroup group) {
        if (group != null && !group.getPermissions().isEmpty()) {
            User user = find(employee);
            if (user != null) {
                for (int i = 0, j = group.getPermissions().size(); i < j; i++) {
                    Option perm = group.getPermissions().get(i);
                    if (!user.isPermissionAssigned( perm.getName() )) {
                        user.addPermission(grantBy.getId(), perm.getName());
                        
                        if (log.isDebugEnabled()) {
                            log.debug("grantPermissions() permission assigned [name=" + perm.getName() + "]");
                        }
                        
                    } else if (log.isDebugEnabled()) {
                        log.debug("grantPermissions() permission already assigned [name=" + perm.getName() + "]");
                    }
                }
                save(user);
            } else {
                log.warn("grantPermissions() no user account found [employee="
                        + (employee == null ? "null" : employee.getId()) + "]");
            }
        } else if (log.isDebugEnabled()) {
            log.debug("grantPermissions() group does not provide permissions to set [group="
                    + (group == null ? "null" : group.getId()) + "]");
        }
    }

    public void revokePermissions(Employee revokedBy, Employee employee, EmployeeGroup group) {
        if (group != null && !group.getPermissions().isEmpty()) {
            User user = find(employee);
            if (user != null) {
                if (log.isDebugEnabled()) {
                    log.debug("revokePermissions() invoked [id="
                            + employee.getId() + ", group=" + group.getId()
                            + ", permissionCount=" + group.getPermissions().size()
                            + "]");
                }
                for (int i = 0, j = group.getPermissions().size(); i < j; i++) {
                    Option perm = group.getPermissions().get(i);
                    boolean assignedByOther = assignedByOtherGroup(employee, group, perm.getName());
                    if (!assignedByOther) {
                        user.removePermission(perm.getName());
                    } else if (log.isDebugEnabled()) {
                        log.debug("revokePermissions() ignoring grant by other [permission="
                                + perm.getName() + "]");
                    }
                }
                save(user);
                
            } else if (log.isDebugEnabled()) {
                log.debug("revokePermissions() no user found [employee="
                        + (employee == null ? "null" : employee.getId()) + "]");
            }
        } else if (log.isDebugEnabled()) {
            log.debug("revokePermissions() nothing to do [group="
                    + (group == null ? "null]" : (group.getId() + ", permissionsEmpty=true]")));
        }
    }

    private boolean assignedByOtherGroup(Employee employee, EmployeeGroup group, String permission) {
        for (int i = 0, j = employee.getRoleConfigs().size(); i < j; i++) {
            EmployeeRoleConfig config = employee.getRoleConfigs().get(i);
            for (int k = 0, l = config.getRoles().size(); k < l; k++) {
                EmployeeRole role = config.getRoles().get(k);
                if (!role.getGroup().getId().equals(group.getId())) {
                    EmployeeGroup other = role.getGroup();
                    for (int m = 0, n = other.getPermissions().size(); m < n; m++) {
                        Option perm = other.getPermissions().get(m);
                        if (perm.getName().equals(permission)) {
                            if (log.isDebugEnabled()) {
                                log.debug("grantByOtherGroup() reports true [permission=" + permission
                                        + ", group=" + group.getId()
                                        + ", otherGroup=" + other.getId()
                                        + "]");
                            }
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public List<ActionTarget> getStartupTargets() {
        return getCurrentSession().createQuery(
                "from " + UserStartupTargetImpl.class.getName() + " o order by o.name")
                .list();
    }

    private ActionTarget loadDefaultActionTarget() {
        return (ActionTarget) getCurrentSession().load(UserStartupTargetImpl.class, DEFAULT_STARTUP);
    }

    public void updateHistory(User user, String host) {
        // select id, ldapname, last_login, last_login_ip from users;
        StringBuilder sql = new StringBuilder();
        sql.append("UPDATE ").append(getTable(TableKeys.USERS)).append(
                " SET last_login = ?, last_login_ip = ? WHERE id = ?");
        String update = sql.toString();
        final Object[] params = {
                new Timestamp(System.currentTimeMillis()),
                host,
                user.getId()
        };
        final int[] types = {
                Types.TIMESTAMP,
                Types.VARCHAR,
                Types.BIGINT
        };
        jdbcTemplate.update(update, params, types);
    }

    private Map<Long, String> getEmployees() {
        Map<Long, String> result = new HashMap<Long, String>();
        StringBuffer query = new StringBuffer(64);
        query
                .append("SELECT contact_id,firstname,lastname FROM ")
                .append(getTable(TableKeys.CONTACTS))
                .append(" c WHERE c.contact_id IN (SELECT contact_id FROM ")
                .append(getTable(TableKeys.EMPLOYEES))
                .append(") AND c.contact_id IN (select contact_id FROM ")
                .append(getTable(TableKeys.USERS))
                .append(")");

        List<Option> list = (List<Option>) jdbcTemplate.query(
                query.toString(),
                new RowMapperResultSetExtractor(new NameRowMapper()));
        for (int i = 0, j = list.size(); i < j; i++) {
            Option o = list.get(i);
            result.put(o.getId(), o.getName());
        }
        return result;
    }

    private Set<String> getExistingNames() {
        Set<String> result = new HashSet<String>();
        List<Option> existing = getLoginNames();
        for (int i = 0, j = existing.size(); i < j; i++) {
            Option next = existing.get(i);
            result.add(next.getName());
        }
        return result;
    }

    private class NameRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int index) throws SQLException {
            StringBuilder buffer = new StringBuilder();
            String firstName = rs.getString(2);
            String lastName = rs.getString(3);
            if (lastName == null) {
                //TODO send bug report to aministration
                lastName = "NONE!";
            }
            buffer.append(lastName);
            if (firstName != null) {
                buffer.append(", ").append(firstName);
            }
            return new OptionImpl(rs.getLong(1), buffer.toString());
        }
    }

    private class UserContactRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int index) throws SQLException {
            return new UserContactVO(rs.getLong(1), rs.getLong(2), rs.getString(3));
        }
    }

    private class UserContactVO {
        private Long user = null;
        private Long contact = null;
        private String name = null;

        public UserContactVO(Long user, Long contact, String name) {
            this.user = user;
            this.contact = contact;
            this.name = name;
        }
    }

    private User loadRelated(User user) {
        if (user == null) {
            return null;
        }
        if (user.getContactId() != null) {
            Employee employee = (Employee) employees.findByContact(user.getContactId());
            if (employee != null) {
                return new DomainUserImpl(user, employee);
            }
        }
        return user;
    }

    private User load(Long id) {
        try {
            return (User) getCurrentSession().load(UserImpl.class, id);
        } catch (Throwable t) {
            return null;
        }
    }
}
