/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 20, 2011 4:31:02 PM 
 * 
 */
package com.osserp.core.model;

import com.osserp.common.Constants;

import com.osserp.core.products.PriceAware;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractPriceAware extends AbstractMarginAware implements PriceAware {

    private Double consumerPrice = Constants.DOUBLE_NULL;
    private Double resellerPrice = Constants.DOUBLE_NULL;
    private Double partnerPrice = Constants.DOUBLE_NULL;

    private Double consumerPriceMinimum = Constants.DOUBLE_NULL;
    private Double resellerPriceMinimum = Constants.DOUBLE_NULL;
    private Double partnerPriceMinimum = Constants.DOUBLE_NULL;

    protected AbstractPriceAware() {
        super();
    }

    protected AbstractPriceAware(
            Double consumerPrice,
            Double resellerPrice,
            Double partnerPrice,
            Double consumerPriceMinimum,
            Double resellerPriceMinimum,
            Double partnerPriceMinimum,
            Double consumerMargin,
            Double resellerMargin,
            Double partnerMargin,
            Double consumerMinimumMargin,
            Double resellerMinimumMargin,
            Double partnerMinimumMargin) {
        super(
                consumerMargin,
                resellerMargin,
                partnerMargin,
                consumerMinimumMargin,
                resellerMinimumMargin,
                partnerMinimumMargin);
        if (consumerPrice == null) {
            this.consumerPrice = Constants.DOUBLE_NULL;
        } else {
            this.consumerPrice = consumerPrice;
        }
        if (resellerPrice == null) {
            this.resellerPrice = Constants.DOUBLE_NULL;
        } else {
            this.resellerPrice = resellerPrice;
        }
        if (partnerPrice == null) {
            this.partnerPrice = Constants.DOUBLE_NULL;
        } else {
            this.partnerPrice = partnerPrice;
        }
        this.consumerPriceMinimum = (consumerPriceMinimum == null ? Constants.DOUBLE_NULL : consumerPriceMinimum);
        this.resellerPriceMinimum = (resellerPriceMinimum == null ? Constants.DOUBLE_NULL : resellerPriceMinimum);
        this.partnerPriceMinimum = (partnerPriceMinimum == null ? Constants.DOUBLE_NULL : partnerPriceMinimum);
    }

    protected AbstractPriceAware(PriceAware other) {
        super(other);
        this.consumerPrice = other.getConsumerPrice() != null ? other.getConsumerPrice() : Constants.DOUBLE_NULL;
        this.resellerPrice = other.getResellerPrice() != null ? other.getResellerPrice() : Constants.DOUBLE_NULL;
        this.partnerPrice = other.getPartnerPrice() != null ? other.getPartnerPrice() : Constants.DOUBLE_NULL;
        this.consumerPriceMinimum = (other.getConsumerPriceMinimum() == null ? Constants.DOUBLE_NULL : other.getConsumerPriceMinimum());
        this.resellerPriceMinimum = (other.getResellerPriceMinimum() == null ? Constants.DOUBLE_NULL : other.getResellerPriceMinimum());
        this.partnerPriceMinimum = (other.getPartnerPriceMinimum() == null ? Constants.DOUBLE_NULL : other.getPartnerPriceMinimum());
    }

    public Double getConsumerPrice() {
        return consumerPrice;
    }

    public void setConsumerPrice(Double consumerPrice) {
        this.consumerPrice = consumerPrice;
    }

    public Double getPartnerPrice() {
        return partnerPrice;
    }

    public void setPartnerPrice(Double partnerPrice) {
        this.partnerPrice = partnerPrice;
    }

    public Double getResellerPrice() {
        return resellerPrice;
    }

    public void setResellerPrice(Double resellerPrice) {
        this.resellerPrice = resellerPrice;
    }

    public Double getConsumerPriceMinimum() {
        return consumerPriceMinimum;
    }

    public void setConsumerPriceMinimum(Double consumerPriceMinimum) {
        this.consumerPriceMinimum = consumerPriceMinimum;
    }

    public Double getResellerPriceMinimum() {
        return resellerPriceMinimum;
    }

    public void setResellerPriceMinimum(Double resellerPriceMinimum) {
        this.resellerPriceMinimum = resellerPriceMinimum;
    }

    public Double getPartnerPriceMinimum() {
        return partnerPriceMinimum;
    }

    public void setPartnerPriceMinimum(Double partnerPriceMinimum) {
        this.partnerPriceMinimum = partnerPriceMinimum;
    }

}
