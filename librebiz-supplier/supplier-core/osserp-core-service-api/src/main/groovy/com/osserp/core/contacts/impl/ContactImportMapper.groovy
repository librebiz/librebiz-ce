/**
 *
 * Copyright (C) 2015 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License (AGPL)
 * version 3 as published by the Free Software Foundation. In accordance
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code
 * versions must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area
 * or, if not reasonably feasible for technical reason, as a top-level
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute.
 *
 */
package com.osserp.core.contacts.impl

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.api.WsContact
import com.osserp.api.WSContact

import com.osserp.common.ds.ImportMapper

import com.osserp.core.contacts.Person

/**
 * 
 * @author rk <rk@osserp.com>
 *
 */
class ContactImportMapper extends ImportMapper {

    private static Logger logger = LoggerFactory.getLogger(ContactImportMapper.class.getName())

    private static final String DEFAULT_BRANCH = 'DE'
    private static final Long COUNTRY = 53L

    /**
     * Checks if row exists and contains 'lastname' OR 'company' OR 'privateemail' column.
     * Company name or private email address will be used for lastname if lastname itself
     * is missing. Lastname is stricly required and we should support contacts coming
     * from webforms accepting a valid email address as mandatory and sufficient. 
     * @param row
     * @return true if row contains required column
     */
    static boolean isMappable(Map<String, String> contactRow) {
        contactRow && (contactRow[rowLastname] || contactRow[rowCompany] || contactRow[rowPrivateEmail])
    }

    @Override
    Object map(Map<String, String> contact) {
        WSContact obj = new WsContact()

        String idString = fetchString(contact[rowContactId])
        Long contactId = createLong(idString)
        if (contactId) {
            obj.primaryKey = contactId
        }
        obj.externalKey = fetchString(contact[rowAccountingId])

        obj.status = fetchString(contact[rowStatus]) ?: obj.status

        obj.branch = fetchString(contact[rowBranchKey])

        obj.company = fetchString(contact[rowCompany])
        obj.contactTypeId = obj.company ? Person.TYPE_BUSINESS : Person.TYPE_PRIVATE

        obj.salutation = fetchString(contact[rowSalutation])
        obj.title =  fetchString(contact[rowTitle])

        obj.firstName = fetchString(contact[rowFirstname])
        obj.lastName = fetchString(contact[rowLastname])

        // ADDRESS VALUES
        obj.street = fetchPrivateStreet(contact)
        //String streetAddon = ...
        obj.city = fetchString(contact[rowPrivateCity])
        obj.zipcode = fetchZipcode(contact, rowPrivateZipcode)
        //String federalState = ...
        obj.countryId = createLong(contact[rowPrivateCountry]) ?: COUNTRY

        // BUSINESS VALUES
        if (obj.company && !obj.street && !obj.city && !obj.zipcode) {
            obj.street = fetchBusinessStreet(contact)
            obj.city = fetchString(contact[rowBusinessCity])
            obj.zipcode = fetchZipcode(contact, rowBusinessZipcode)
            obj.countryId = createLong(contact[rowBusinessCountry]) ?: COUNTRY
        }

        if (!obj.street && !obj.city && !obj.zipcode) {
            obj.street = fetchAddressStreet(contact)
            obj.city = fetchString(contact[rowAddressCity])
            obj.zipcode = fetchZipcode(contact, rowAddressZipcode)
            obj.countryId = createLong(contact[rowAddressCountry]) ?: COUNTRY
        }
        logger.debug("map: zipcode is ${obj.zipcode}")
        obj.businessPhone = fetchString(contact[rowBusinessPhone])
        obj.businessMobile = fetchString(contact[rowBusinessMobile])
        obj.businessFax = fetchString(contact[rowBusinessFax])
        obj.businessEmail = fetchEmail(contact[rowBusinessEmail])

        if (!obj.businessPhone) {
            obj.businessPhone = fetchString(contact[rowPrivatePhone])
        }
        if (!obj.businessMobile) {
            obj.businessMobile = fetchString(contact[rowPrivateMobile])
        }
        if (!obj.businessFax) {
            obj.businessFax = fetchString(contact[rowPrivateFax])
        }
        if (!obj.businessEmail) {
            obj.businessEmail = fetchString(contact[rowPrivateEmail])
        }

        if (!obj.lastName) {
            obj.lastName = obj.businessEmail
        }

        obj.grantEmail = fetchBoolean(fetchString(contact[rowAllowedMail]))
        obj.grantPhone = fetchBoolean(fetchString(contact[rowAllowedPhone]))
        obj.grantNon = fetchBoolean(fetchString(contact[rowAllowedNon]))
        //obj.dateOfBirth
        //obj.groupname
        //obj.jobTitle
        //obj.website
        return obj
    }

    String rowContactId = 'contactkey'
    String rowContactGroup = 'contactgroup'
    String rowAccountingId = 'contactthirdparty'
    String rowBranchKey = 'branch'
    String rowSalesStatus = 'sales'
    String rowStatus = 'status'

    static String rowCompany = 'company'
    static String rowSalutation = 'salutation'
    static String rowTitle = 'title'
    static String rowLastname = 'lastname'
    static String rowFirstname = 'firstname'
    static String rowBirthdate = 'dateofbirth'

    static String rowAllowedMail = 'contactmail'
    static String rowAllowedPhone = 'contactphone'
    static String rowAllowedNon = 'contactnone'

    static String rowGroupname = 'groupname'
    static String rowJobTitle = 'jobtitle'

    static String rowAddressStreet = 'street'
    static String rowAddressStreetAddon = 'streetaddon'
    static String rowAddressStreetNumber = 'streetno'
    static String rowAddressZipcode = 'zipcode'
    static String rowAddressCity = 'city'
    static String rowAddressCountry = 'country'

    static String rowBusinessStreet = 'businessstreet'
    static String rowBusinessStreetAddon = 'businessstreetaddon'
    static String rowBusinessStreetNumber = 'businessstreetno'
    static String rowBusinessZipcode = 'businesszipcode'
    static String rowBusinessCity = 'businesscity'
    static String rowBusinessCountry = 'businesscountry'

    static String rowBusinessPhone = 'businessphone'
    static String rowBusinessMobile = 'businessmobile'
    static String rowBusinessFax = 'businessfax'
    static String rowBusinessEmail = 'businessemail'

    static String rowPrivateStreet = 'privatestreet'
    static String rowPrivateStreetAddon = 'privatestreetaddon'
    static String rowPrivateStreetNumber = 'privatestreetno'
    static String rowPrivateZipcode = 'privatezipcode'
    static String rowPrivateCity = 'privatecity'
    static String rowPrivateCountry = 'privatecountry'

    static String rowPrivatePhone = 'privatephone'
    static String rowPrivateMobile = 'privatemobile'
    static String rowPrivateFax = 'privatefax'
    static String rowPrivateEmail = 'privateemail'

    static String rowContactNote = 'contactnote'
    static String rowContactMail = 'contactmail'
    static String rowContactPhone = 'contactphone'
    static String rowNewsletter = 'newsletter'

    static String rowWebsite = 'website'

    String[] knownColumns = [
        rowContactId,
        rowContactGroup,
        rowAccountingId,
        rowBranchKey,
        rowSalesStatus,
        rowStatus,
        rowCompany,
        rowSalutation,
        rowTitle,
        rowLastname,
        rowFirstname,
        rowBirthdate,
        rowGroupname,
        rowJobTitle,
        rowAddressStreet,
        rowAddressStreetAddon,
        rowAddressStreetNumber,
        rowAddressZipcode,
        rowAddressCity,
        rowAddressCountry,
        rowBusinessStreet,
        rowBusinessStreetAddon,
        rowBusinessStreetNumber,
        rowBusinessZipcode,
        rowBusinessCity,
        rowBusinessCountry,
        rowBusinessPhone,
        rowBusinessMobile,
        rowBusinessFax,
        rowBusinessEmail,
        rowPrivateStreet,
        rowPrivateStreetAddon,
        rowPrivateStreetNumber,
        rowPrivateZipcode,
        rowPrivateCity,
        rowPrivateCountry,
        rowPrivatePhone,
        rowPrivateMobile,
        rowPrivateFax,
        rowPrivateEmail,
        rowContactNote,
        rowContactMail,
        rowContactPhone,
        rowNewsletter,
        rowWebsite,
        rowAllowedMail,
        rowAllowedPhone,
        rowAllowedNon
    ]

    private String fetchAddressStreet(Map<String, String> contact) {
        fetchStreet(contact, rowAddressStreet, rowAddressStreetNumber)
    }

    private String fetchBusinessStreet(Map<String, String> contact) {
        fetchStreet(contact, rowBusinessStreet, rowBusinessStreetNumber)
    }

    private String fetchPrivateStreet(Map<String, String> contact) {
        fetchStreet(contact, rowPrivateStreet, rowPrivateStreetNumber)
    }

    private String fetchStreet(Map<String, String> contact, String rowStreet, String rowStreetNumber) {
        String street = fetchString(contact[rowStreet])
        String streetNumber = fetchString(contact[rowStreetNumber])
        if (street && streetNumber && !(street.indexOf(streetNumber) > -1)) {
            street = "${street.trim()} ${streetNumber.trim()}"
        }
        return street
    }

    private String fetchZipcode(Map<String, String> contact, String rowZipcode) {
        String zip = fetchString(contact[rowZipcode])
        if (zip?.indexOf('.') > -1) {
            zip = zip.substring(0, zip.indexOf('.'))
        }
        return (zip != null) ? zip.trim() : null
    }

    private String fetchEmail(String email) {
        String mail = fetchString(email)
        mail ? mail.toLowerCase() : null
    }
}
