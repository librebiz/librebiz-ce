<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.timeRecordingView}"><c:redirect url="/errors/error_context.jsp"/></c:if>
<c:set var="view" value="${sessionScope.timeRecordingView}"/>
<c:set var="timeRecording" value="${view.timeRecording}"/>
<c:set var="period" value="${timeRecording.selectedPeriod}"/>
<c:set var="selectedMonth" value="${period.selectedYear.selectedMonth}"/>
<c:set var="employee" value="${view.employee}"/>
<c:set var="adminPermissionGrant" value="false"/>
<c:set var="approvalAvailable" value="${view.approvalAvailable}"/>

<o:permission role="time_recording_admin" info="permissionTimeRecordingConfiguration">
    <c:set var="adminPermissionGrant" value="true"/>
    <o:logger write="time recording admin permission grant" level="debug"/>
</o:permission>

<c:set var="markerPermissionGrant" value="false"/>
<o:permission role="time_recording_marker" info="permissionTimeRecordingMarker">
    <c:set var="markerPermissionGrant" value="true"/>
</o:permission>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="title"><o:displayTitle/></tiles:put>

    <tiles:put name="headline">
        <script type="text/javascript" src="<c:url value="/js/wz_tooltip.js"/>"></script>
        <fmt:message key="timeRecording"/>
        <c:if test="${!empty employee}">
            <v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${employee.id}">
                <span> - </span>[<o:out value="${employee.id}"/>]
            </v:ajaxLink>
            <span> - </span><o:out value="${employee.name}"/>
        </c:if>
        <c:if test="${!empty selectedMonth}">
            <span> - </span>
            <o:ajaxLink linkId="monthSelection" url="/forward.do?target=timeRecordingMonthSelection">
                [<o:out value="${selectedMonth.monthAndYearDisplay}"/>]
            </o:ajaxLink>
            <c:if test="${selectedMonth.monthMarker || selectedMonth.leaveMarker}">
                <a href="javascript:void(0)" onmouseover="TagToTip('markerMonth',TITLE, '<fmt:message key="info"/>')" onmouseout="UnTip()"><img style="vertical-align:middle;" src="<c:url value="${applicationScope.importantIcon}"/>" class="icon"/></a>
                <span id="markerMonth">
                    <c:if test="${selectedMonth.monthMarker}">
                        <fmt:message key="maximumCarryoverHoursMonth"/> <o:number value="${selectedMonth.monthMarkerValue}" format="hours"/> <fmt:message key="hours"/><br />
                    </c:if>
                    <c:if test="${selectedMonth.leaveMarker}">
                        <fmt:message key="maximumCarryoverLeaveMonth"/> <o:number value="${selectedMonth.leaveMarkerValue}" format="currency"/> <fmt:message key="daysLabel"/><br />
                    </c:if>
                </span>
            </c:if>
        </c:if>
    </tiles:put>

	<tiles:put name="headline_right">
        <ul>
            <c:choose>
                <c:when test="${view.timeRecording.selectedPeriod == null}">
                    <li><a href="<c:url value="/timeRecordings.do?method=exit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a></li>
                </c:when>
                <c:when test="${view.editMode}">
                    <li><a href="<c:url value="/timeRecordings.do?method=exit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a></li>
                    <li><a href="<c:url value="/timeRecordings.do?method=disableEdit"/>" title="<fmt:message key="displayMonthlyJournal"/>"><o:img name="tableIcon"/></a></li>
                </c:when>
                <c:when test="${view.displayMarkerMode}">
                    <li><a href="<c:url value="/timeRecordings.do?method=toggleDisplayMarkerMode"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a></li>
                    <li><o:ajaxLink linkId="addMarker" url="/timeRecordings.do?method=displayAddMarker"><o:img name="newdataIcon"/></o:ajaxLink></li>
                </c:when>
                <c:when test="${view.displayJournalMode}">
                    <c:choose>
                        <c:when test="${view.invokedInApprovalMode}">
                            <li><a href="<c:url value="/timeRecordings.do?method=exit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a></li>
                        </c:when>
                        <c:otherwise>
                            <li><a href="<c:url value="/timeRecordings.do?method=toggleDisplayJournalMode"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a></li>
                            <c:if test="${approvalAvailable}">
                                <li><a href="<v:url value="/hrm/timeRecordingApproval/forward?exit=/timeRecordings.do?method=redisplay"/>" title="<fmt:message key="timeRecordingApprovals"/>"><o:img name="groupIcon"/></a></li>
                            </c:if>
                        </c:otherwise>
                    </c:choose>
                    <li><o:ajaxLink linkId="summaryDisplay" url="/timeRecordings.do?method=displaySummary"><o:img name="summaryIcon"/></o:ajaxLink></li>
                </c:when>
                <c:otherwise>
                    <li><a href="<c:url value="/timeRecordings.do?method=exit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a></li>
                    <li><a href="<c:url value="/timeRecordings.do?method=enableEdit"/>" title="<fmt:message key="newBooking"/>"><o:img name="clockIcon"/></a></li>
                    <c:if test="${adminPermissionGrant}">
                        <li><o:ajaxLink linkId="periodUpdate" url="/timeRecordings.do?method=enableSetup"><o:img name="configureIcon"/></o:ajaxLink></li>
                        <li><o:ajaxLink linkId="showPeriods" url="/timeRecordings.do?method=showPeriods"><o:img name="bellIcon"/></o:ajaxLink></li>
                    </c:if>
                    <c:if test="${markerPermissionGrant}">
                        <li><a href="<c:url value="/timeRecordings.do?method=toggleDisplayMarkerMode"/>" title="<fmt:message key="displayMarker"/>"><o:img name="texteditIcon"/></a></li>
                    </c:if>
                    <c:if test="${view.exitTarget != 'timeRecordingApproval' and approvalAvailable}">
                        <li><a href="<v:url value="/hrm/timeRecordingApproval/forward?exit=/timeRecordings.do?method=redisplay"/>" title="<fmt:message key="timeRecordingApprovals"/>"><o:img name="groupIcon"/></a></li>
                    </c:if>
                    <li><a href="<c:url value="/timeRecordings.do?method=toggleDisplayJournalMode"/>" title="<fmt:message key="displayMonthlyJournal"/>"><o:img name="tableIcon"/></a></li>
                </c:otherwise>
            </c:choose>
            <li><v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link></li>
        </ul>
    </tiles:put>

	<tiles:put name="content" type="string">
        <div class="content-area" id="timeRecordingContent">
            <div class="row">
                <c:choose>
                    <c:when test="${view.editMode}">
                        <c:import url="/pages/hrm/shared/time_record_edit.jsp"/>
                    </c:when>
                    <c:when test="${view.displayMarkerMode}">
                        <c:import url="/pages/hrm/shared/time_recording_marker.jsp"/>
                    </c:when>
                    <c:when test="${view.displayJournalMode}">
                        <c:import url="/pages/hrm/shared/time_recording_journal.jsp"/>
                    </c:when>
                    <c:otherwise>
                        <%-- display mode --%>
                        <c:set var="timeRecordingView" scope="request" value="${sessionScope.timeRecordingView}"/>
                        <div class="col-md-6 panel-area panel-area-default">
                            <c:import url="/pages/hrm/shared/time_recording_summary.jsp"/>
                        </div>
                        <c:if test="${!empty period.records and !empty selectedMonth.days}">
                            <div class="col-md-6 panel-area panel-area-default">
                                <div class="panel-body">
                                    <c:import url="/pages/hrm/shared/time_recording_month.jsp"/>
                                </div>
                            </div>
                        </c:if>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
