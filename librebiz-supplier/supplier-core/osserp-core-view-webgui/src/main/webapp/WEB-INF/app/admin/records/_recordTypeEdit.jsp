<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="recordTo" /> <o:out value="${view.bean.name}" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="name"><fmt:message key="name" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <v:text name="name" styleClass="form-control" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="shortkey"><fmt:message key="shortkey" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <v:text name="numberPrefix" styleClass="form-control" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="shortkeyStatus"><fmt:message key="shortkeyStatus" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="checkbox">
                            <label for="addNumberPrefix">
                                <v:checkbox id="addNumberPrefix" name="addNumberPrefix" />
                                <fmt:message key="shortkeyActivated" />
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="recordTypeNotes"><fmt:message key="recordTypeNotes" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="checkbox">
                            <label for="addNumberPrefix">
                                <v:checkbox id="displayNotesBelowItems" name="displayNotesBelowItems" />
                                <fmt:message key="displayNotesBelowItems" />
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <c:if test="${view.termsAvailable}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label><fmt:message key="printRecord" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <label for="addTermsAndConditionsOnPrint">
                                    <v:checkbox id="addTermsAndConditionsOnPrint" name="addTermsAndConditionsOnPrint" />
                                    <fmt:message key="addTermsOnPrint" />
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </c:if>

            <c:if test="${view.rightToCancelAvailable}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>
                                <c:if test="${!view.termsAvailable}">
                                    <fmt:message key="printRecord" />
                                </c:if>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <label for="addRightToCancelOnPrint">
                                    <v:checkbox id="addRightToCancelOnPrint" name="addRightToCancelOnPrint" />
                                    <fmt:message key="addRightToCancelOnPrint" />
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </c:if>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label><fmt:message key="sendEmail" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="checkbox">
                            <label for="addProductDatasheet">
                                <v:checkbox id="addProductDatasheet" name="addProductDatasheet" />
                                <fmt:message key="addProductSheetsOnEmail" />
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <c:if test="${'date' == view.bean.numberCreatorName}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="numberRange"><fmt:message key="numberRange" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <fmt:message key="${view.bean.numberCreatorName}" />
                            +  <v:text name="uniqueNumberDigitCount" value="${view.bean.uniqueNumberDigitCount}" style="width:35px;"/>
                            <fmt:message key="digits" />
                        </div>
                    </div>
                </div>
            </c:if>

            <div class="row next">
                <div class="col-md-4"> </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <v:submitExit url="${view.baseLink}/disableEditMode"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <o:submit value="save" styleClass="form-control" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
