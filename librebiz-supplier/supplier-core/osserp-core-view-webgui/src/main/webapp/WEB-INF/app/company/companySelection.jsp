<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="companySelectionView"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="title"><o:displayTitle/></tiles:put>

	<tiles:put name="headline">
		<fmt:message key="${view.headerName}"/>
	</tiles:put>

	<tiles:put name="headline_right">
		<v:navigation/>
	</tiles:put>

	<tiles:put name="content" type="string">

		<div class="subcolumns">
			<div class="subcolumn">
				<div class="spacer"></div>
				<table class="table">
					<tr>
						<th><fmt:message key="id"/></th>
						<th><fmt:message key="name"/></th>
						<th><fmt:message key="shortkey"/></th>
						<th><fmt:message key="requestContextLabel"/></th>
						<th><fmt:message key="orderContextLabel"/></th>
					</tr>
					<c:choose>
						<c:when test="${empty view.list}">
							<tr>
								<td colspan="5" class="errortext"><fmt:message key="noOrderTypesAvailable"/></td>
							</tr>
						</c:when>
						<c:otherwise>
							<c:forEach var="obj" items="${view.list}">
								<tr>
									<td><o:out value="${obj.id}"/></td>
									<td><a href="<c:url value="/businessTypeConfig.do"><c:param name="method" value="select"/><c:param name="id" value="${obj.id}"/></c:url>"><o:out value="${obj.name}"/></a></td>
									<td><o:out value="${obj.key}"/></td>
									<td><o:out value="${obj.context}"/></td>
									<td><o:out value="${obj.salesContext}"/></td>
								</tr>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</table>
				<div class="spacer"></div>
			</div>
		</div>
	</tiles:put>
</tiles:insert>
