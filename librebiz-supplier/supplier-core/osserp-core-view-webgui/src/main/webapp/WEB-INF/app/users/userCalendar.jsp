<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<o:userContext/>
<o:resource/>
<v:view viewName="userCalendarView"/>
<c:set var="cal" scope="request" value="${view.calendar}"/>
<c:set var="calUrl" scope="request"><v:url value="/users/userCalendar/setDate?context=userCalendar"/></c:set>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
<tiles:put name="title"><o:displayTitle/></tiles:put>
<tiles:put name="styles" type="string">
<style type="text/css">
<c:import url="/css/ajax_popup.css"/>
<c:import url="/css/calendar.css"/>
</style>
</tiles:put>
<tiles:put name="javascript" type="string"> 
<c:import url="${viewdir}/shared/calendar_js.jsp"/>
</tiles:put>
<tiles:put name="headline">
    <fmt:message key="appointments"/> <oc:options name="months" value="${requestScope.cal.month}"/> <o:out value="${requestScope.cal.year}"/>
</tiles:put>
<tiles:put name="headline_right">
<ul>
    <c:import url="${viewdir}/shared/calendar_navigation.jsp"/>
    <li><v:link url="/users/userSettings/forward?exit=/users/userCalendar/forward" title="setStartupPage"><o:img name="configureIcon"/></v:link></li>
    <li><v:link url="/events/userEvents/forward" title="todosAndInfos"><o:img name="replaceIcon"/></v:link></li>
</ul>
</tiles:put>
<tiles:put name="content" type="string">
<div class="content-area" id="userCalendarContent">
    <div class="row">
        <div class="col-lg-12 panel-area">
            <c:import url="${viewdir}/shared/calendar_infotext.jsp"/>
            <table class="table table-striped">
                <o:calendarRows calendarView="${view}" exitTarget="userCalendar" />
            </table>
        </div>
    </div>
</div>
</tiles:put> 
</tiles:insert>
