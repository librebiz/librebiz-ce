<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:choose>
<c:when test="${!empty view.scriptsListBottom}"><c:set var="scriptsListBottom" value="${view.scriptsListBottom}"/></c:when>
<c:when test="${!empty portalView.scriptsListBottom}"><c:set var="scriptsListBottom" value="${portalView.scriptsListBottom}"/></c:when>
</c:choose>
<c:choose>
<c:when test="${!empty scriptsListBottom}">
<c:forEach var="scriptsName" items="${scriptsListBottom}">
<c:import url="/layout/shared/bottom_${scriptsName}.jsp" />
</c:forEach>
</c:when>
<c:otherwise>
<c:import url="/layout/shared/bottom_bs3.jsp" />
</c:otherwise>
</c:choose>
