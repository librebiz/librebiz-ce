/**
 *
 * Copyright (C) 2017 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 11, 2017 
 * 
 */
package com.osserp.groovy.web

import javax.servlet.ServletContext
import javax.servlet.ServletContextEvent
import javax.servlet.ServletContextListener

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.WebApplicationContext

import com.osserp.common.service.Locator

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public class ResourceLocatorDefaultListener implements ServletContextListener {//extends ContextLoaderListener {
    private static Logger logger = LoggerFactory.getLogger(ResourceLocatorDefaultListener.class.getName())

    public ResourceLocatorDefaultListener() {
        super();
    }

    /**
     * Initialize the root web application context.
     */
    @Override
    public void contextInitialized(ServletContextEvent event) {
        ServletContext ctx = event.getServletContext()
        ctx.setAttribute(WebUtil.VIEW_SERVLET_NAME, getViewServletName(ctx))
        ctx.setAttribute(WebUtil.VIEW_SERVLET_HOME, getViewServletHome(ctx))
        initLegacyLocatorContext(ctx)
        if (logger.isInfoEnabled()) {
            printContextInfo(ctx)
        }
    }

    protected void initLegacyLocatorContext(ServletContext ctx) {
        // test if already initialized by previous listener
        Object locator = ctx.getAttribute(Locator.LOCATOR_CONTEXT_NAME)
        if (!(locator instanceof Locator)) {
            Enumeration<String> attrNames = ctx.getAttributeNames()
            while (attrNames.hasMoreElements() && locator == null) {
                String attrName = attrNames.nextElement()
                Object obj = ctx.getAttribute(attrName)
                if (obj instanceof WebApplicationContext) {
                    WebApplicationContext appctx = (WebApplicationContext) obj
                    String[] names = appctx.getBeanDefinitionNames()
                    for (int i = 0; i < names.length; i++) {
                        Object next = appctx.getBean(names[i])
                        if (next instanceof Locator) {
                            locator = next
                            break
                        }
                    }
                } else if (obj instanceof ApplicationContext) {
                    ApplicationContext appctx = (ApplicationContext) obj
                    String[] names = appctx.getBeanDefinitionNames()
                    for (int i = 0; i < names.length; i++) {
                        Object next = appctx.getBean(names[i])
                        if (next instanceof Locator) {
                            locator = next
                            break
                        }
                    }
                } else if (obj instanceof Locator) {
                    locator = obj
                }
            }
            if (locator != null) {
                ctx.setAttribute(Locator.LOCATOR_CONTEXT_NAME, locator)
            }
        }
    }

    protected void printContextInfo(ServletContext ctx) {
        Enumeration<String> attrNames = ctx.getAttributeNames()
        while (attrNames.hasMoreElements()) {
            String attrName = attrNames.nextElement()
            Object obj = ctx.getAttribute(attrName)
            if (obj instanceof WebApplicationContext) {
                logger.info("found attribute with webApplicationContext [contextName=${attrName}]")
                WebApplicationContext appctx = (WebApplicationContext) obj
                String[] names = appctx.getBeanDefinitionNames()
                for (int i = 0; i < names.length; i++) {
                    logger.info("found bean [name=${names[i]}]")
                }
            } else if (obj instanceof ApplicationContext) {
                logger.info("found attribute with applicationContext [contextName=${attrName}]")
                ApplicationContext appctx = (ApplicationContext) obj
                String[] names = appctx.getBeanDefinitionNames()
                for (int i = 0; i < names.length; i++) {
                    logger.info("found bean [name=${names[i]}]")
                }
            } else if (obj) {
                logger.info("found attribute [name=${attrName}]")
            }
        } 
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        // Default does nothing
    }

    /**
     * Tries to fetch the viewServletName by context attribute or init param
     * @param context
     * @return name or empty string if not found
     */
    protected String getViewServletName(ServletContext context) {
        getViewServletValue(context, WebUtil.VIEW_SERVLET_NAME)
    }

    /**
     * Tries to fetch the viewServletHome url by context attribute or init param
     * @param context
     * @return home url or default url as provided by defaultHome method
     */
    protected final String getViewServletHome(ServletContext context) {
        String home = getViewServletValue(context, WebUtil.VIEW_SERVLET_HOME)
        if (home) {
            logger.debug("getViewServletHome: configured value found [home=${home}]")
            return home
        }
        getDefaultHome(context)
    }

    /**
     * Provides the default home url if not provided by initParam 'viewServletName'.
     * The default is an invocation of redirect:/index or redirect:/$viewServletName/index
     * if viewServletName is found. You may override this method instead of setting an init
     * param wich should be the prefered method doing this.
     */
    protected String getDefaultHome(ServletContext context) {
        String storedViewServletName = getViewServletName(context)
        String result = 'redirect:'
        if (storedViewServletName) {
            result = "${result}/${storedViewServletName}/index"
        } else {
            result = "${result}/index"
        }
        logger.debug("getDefaultHome: done [home=${result}]")
        return result
    }

    /**
     * Attempts to fetch a value from servletContext by context attribute or context init param.
     * @param context
     * @param key
     * @return value provided by attribute or init param or empty string if none exist
     */
    protected final String getViewServletValue(ServletContext context, String key) {
        String value = context.getAttribute(key)
        if (!value) {
            value = context.getInitParameter(key)
        }
        value ?: ''
    }
}
