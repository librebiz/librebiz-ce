<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="bean" scope="page" value="${view.bean}"/>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4><fmt:message key="templateCreate"/></h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <fmt:message key="name" />
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <v:text styleClass="form-control" name="name" value="${bean.name}"/>
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-4">
                    <div class="form-group">
                        <fmt:message key="product" />
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <oc:select options="${view.productList}" name="productId" valueProperty="productId" styleClass="form-control" />
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-4">
                    <div class="form-group">
                        <fmt:message key="businessType" />
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <oc:select options="${view.businessTypes}" name="requestTypeId" styleClass="form-control" />
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-4"></div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <v:submitExit url="/admin/calculations/calculationTemplates/disableCreateMode" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <o:submit styleClass="form-control" value="create" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
