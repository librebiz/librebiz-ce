/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Feb 23, 2010 2:04:42 PM 
 * 
 */
package com.osserp.core.service.telephone;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.common.OptionsCache;
import com.osserp.core.Options;
import com.osserp.core.dao.telephone.TelephoneConfigurations;
import com.osserp.core.dao.telephone.Telephones;
import com.osserp.core.model.telephone.TelephoneImpl;
import com.osserp.core.service.impl.AbstractService;
import com.osserp.core.telephone.Telephone;
import com.osserp.core.telephone.TelephoneConfiguration;
import com.osserp.core.telephone.TelephoneManager;
import com.osserp.core.telephone.TelephoneSystemManager;
import com.osserp.core.tasks.SfaSubscriberSender;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public class TelephoneManagerImpl extends AbstractService implements TelephoneManager {

    private Telephones telephonesDao = null;
    private TelephoneConfigurations telephoneConfigsDao = null;
    private SfaSubscriberSender sfaSubscriberSender = null;
    protected OptionsCache options = null;
    protected TelephoneSystemManager telephoneSystemManager = null;

    public TelephoneManagerImpl(Telephones telephones,
            TelephoneConfigurations telephoneConfigsDao,
            SfaSubscriberSender sfaSubscriberSender,
            OptionsCache options,
            TelephoneSystemManager telephoneSystemManager) {
        super();
        this.telephonesDao = telephones;
        this.telephoneConfigsDao = telephoneConfigsDao;
        this.sfaSubscriberSender = sfaSubscriberSender;
        this.options = options;
        this.telephoneSystemManager = telephoneSystemManager;
    }

    public Telephone create(String mac, Long telephoneSystemId, Long telephoneTypeId) throws ClientException {
        mac = (mac != null) ? mac.toUpperCase() : mac;
        this.checkMacaddress(mac);
        Telephone telephone = new TelephoneImpl(
                this.telephoneSystemManager.find(telephoneSystemId),
                mac,
                getTelephoneType(telephoneTypeId));
        return telephonesDao.createTelephone(telephone);
    }

    public List<Telephone> getAll() {
        return telephonesDao.getAll();
    }

    public void updateTelephoneSystem(Long id, Long telephoneSystemId) {
        Telephone telephone = (id != null) ? telephonesDao.find(id) : null;
        if (telephone != null) {
            telephone.setTelephoneSystem(telephoneSystemManager.find(telephoneSystemId));
            this.telephonesDao.save(telephone);
            this.syncronize(telephoneConfigsDao.findByReference(telephone.getId()));
        }
    }

    public void updateTelephoneType(Long id, Long telephoneTypeId) {
        Telephone telephone = (id != null) ? telephonesDao.find(id) : null;
        if (telephone != null) {
            telephone.setTelephoneType(getTelephoneType(telephoneTypeId));
            this.telephonesDao.save(telephone);
        }
    }

    public Telephone find(Long id) {
        if (id != null) {
            return telephonesDao.find(id);
        }
        return null;
    }

    public List<Telephone> find(String mac, Long telephoneSystemId, Long telephoneTypeId) {
        mac = (mac != null) ? mac.toUpperCase().replaceAll(":", "").replaceAll("-", "") : mac;
        if (mac != null || telephoneSystemId != null || telephoneTypeId != null) {
            List<Telephone> list = (telephoneSystemId != null) ? telephonesDao.findByReference(telephoneSystemId) : getAll();
            if (telephoneTypeId != null) {
                List<Telephone> newList = new ArrayList<Telephone>();
                for (Iterator<Telephone> iterator = list.iterator(); iterator.hasNext();) {
                    Telephone next = iterator.next();
                    if (next.getTelephoneType() != null &&
                            next.getTelephoneType().getId().equals(telephoneTypeId)) {
                        newList.add(next);
                    }
                }
                list = newList;
            }
            if (mac != null) {
                List<Telephone> newList = new ArrayList<Telephone>();
                for (Iterator<Telephone> iterator = list.iterator(); iterator.hasNext();) {
                    Telephone next = iterator.next();
                    if (next.getMac().indexOf(mac) != -1) {
                        newList.add(next);
                    }
                }
                list = newList;
            }
            return list;
        }
        return getAll();
    }

    public List<Telephone> findByBranch(Long id) {
        List<Telephone> list = new ArrayList<Telephone>();
        if (id != null) {
            list = telephonesDao.findByBranch(id);
        }
        return list;
    }

    public void delete(Long id) {
        Telephone telephone = (id != null) ? telephonesDao.find(id) : null;
        if (telephone != null) {
            telephone.setEol(true);
            this.telephonesDao.save(telephone);
            this.syncronize(telephoneConfigsDao.findByReference(telephone.getId()));
        }
    }

    private void syncronize(TelephoneConfiguration tc) {
        if (tc != null) {
            this.sfaSubscriberSender.send(tc.getId());
        }
    }

    private void syncronize(List<TelephoneConfiguration> list) {
        for (Iterator<TelephoneConfiguration> iterator = list.iterator(); iterator.hasNext();) {
            TelephoneConfiguration tc = iterator.next();
            this.syncronize(tc);
        }
    }

    private void checkMacaddress(String mac) throws ClientException {
        if (!mac.matches(Constants.MACADDRESS_REGEX)) {
            throw new ClientException(ErrorCode.INVALID_MACADDRESS);
        } else if (telephonesDao.findByMacaddress(mac) != null) {
            throw new ClientException(ErrorCode.MACADDRESS_EXISTS);
        }
    }

    private Option getTelephoneType(Long telephoneTypeId) {
        if (telephoneTypeId != null) {
            for (Iterator<Option> i = options.getList(Options.TELEPHONE_TYPES).iterator(); i.hasNext();) {
                Option next = i.next();
                if (next.getId() != null) {
                    if (next.getId().equals(telephoneTypeId)) {
                        return next;
                    }
                }
            }
        }
        return null;
    }
}
