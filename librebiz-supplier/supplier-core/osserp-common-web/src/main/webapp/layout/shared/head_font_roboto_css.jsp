<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link rel="stylesheet" href="<c:url value="/layout/fonts/roboto_bold_macroman/stylesheet.css"/>">
<link rel="stylesheet" href="<c:url value="/layout/fonts/roboto_black_macroman/stylesheet.css"/>">
<link rel="stylesheet" href="<c:url value="/layout/fonts/roboto_regular_macroman/stylesheet.css"/>">
<link rel="stylesheet" href="<c:url value="/layout/fonts/roboto_medium_macroman/stylesheet.css"/>">
