/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 08-Feb-2007 12:57:24 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;

import com.osserp.core.dao.PaymentConditions;
import com.osserp.core.dao.SystemCompanies;
import com.osserp.core.dao.TaxRates;
import com.osserp.core.dao.records.RecordInfoConfigs;
import com.osserp.core.dao.records.RecordMailLogs;
import com.osserp.core.dao.records.RecordPrintOptionDefaults;
import com.osserp.core.dao.records.SalesPayments;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.finance.RecordNumberCreator;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractSalesInvoiceRecords extends AbstractSalesRecords {

    protected static final String DELETE_PERMISSION_DEFAULT =
            "executive_accounting,"
            + "project_billing_delete,"
            + "sales_invoice_delete";

    public AbstractSalesInvoiceRecords(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache cache,
            TaxRates taxRates,
            SystemCompanies companies,
            RecordInfoConfigs infoConfigs,
            PaymentConditions paymentConditions,
            RecordNumberCreator recordNumberCreator,
            RecordPrintOptionDefaults recordPrintOptionDefaults,
            RecordMailLogs recordMailLogs,
            SalesPayments payments) {
        super(
                jdbcTemplate,
                tables,
                sessionFactory,
                cache,
                taxRates,
                companies,
                infoConfigs,
                paymentConditions,
                recordNumberCreator,
                recordPrintOptionDefaults,
                recordMailLogs,
                payments);
    }

    public final List<RecordDisplay> findWithPayments(boolean descending) {
        List<Map<String, Object>> result = getWithPayments(descending);
        List<RecordDisplay> invoices = RecordDisplayRowMapper.createDisplayList(getOptionsCache(), result);
        return addPayments(invoices);
    }

    protected List<Map<String, Object>> getWithPayments(boolean descending) {
        StringBuilder query = new StringBuilder("SELECT ");
        query
                .append(RecordDisplayRowMapper.RECORD_VALUES)
                .append(" FROM v_")
                .append(getQueryContext())
                .append("_query WHERE status = ")
                .append(Invoice.STAT_SENT)
                .append(" ORDER BY id");
        if (descending) {
            query.append(" DESC");
        }
        List<Map<String, Object>> result = (List<Map<String, Object>>) jdbcTemplate.query(
                query.toString(),
                RecordDisplayRowMapper.getReader());
        return result;
    }

    protected abstract List<RecordDisplay> addPayments(List<RecordDisplay> invoices);
}
