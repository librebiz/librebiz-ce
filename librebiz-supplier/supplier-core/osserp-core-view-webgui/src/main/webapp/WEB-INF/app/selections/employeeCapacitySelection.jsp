<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="employeeCapacitySelectionView"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="title"><o:displayTitle/></tiles:put>
	<tiles:put name="headline">
		<fmt:message key="${view.headerName}"/>
		<c:choose>
			<c:when test="${view.salesMode}">
				[<fmt:message key="sales"/>]
			</c:when>
			<c:when test="${view.managerMode}">
				[<fmt:message key="projector"/>]
			</c:when>
		</c:choose>
	</tiles:put>
	<tiles:put name="headline_right">
		<v:navigation/>
	</tiles:put>

	<tiles:put name="content" type="string">
        <div class="content-area" id="employeeProjectsContent">
            <div class="row">
                <div class="col-lg-12 panel-area">
                    <c:import url="${viewdir}/common/_selectBusinessRelationByStat.jsp"/>
				</div>
			</div>
		</div>
	</tiles:put>
</tiles:insert>