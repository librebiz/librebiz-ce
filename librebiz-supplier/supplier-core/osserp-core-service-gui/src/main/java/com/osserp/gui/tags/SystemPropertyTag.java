/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jul 21, 2012 
 * 
 */
package com.osserp.gui.tags;

import com.osserp.common.util.StringUtil;
import com.osserp.common.web.tags.AbstractConditionTag;

import com.osserp.core.CoreServiceProvider;
import com.osserp.core.system.SystemConfigManager;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class SystemPropertyTag extends AbstractConditionTag {

    public static String MATCH_ALL = "all";
    public static String MATCH_ANY = "any";

    private String name = null;
    private String match = MATCH_ANY;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMatch() {
        return match;
    }

    public void setMatch(String match) {
        this.match = match;
    }

    protected final boolean isPropertyEnabled() {
        boolean result = false;
        if (name != null || name.length() > 0) {
            String[] array = StringUtil.getTokenArray(name);
            for (int i = 0; i < array.length; i++) {
                String property = array[i];
                boolean last = false;
                try {
                    SystemConfigManager systemConfigManager = getSystemConfigManager();
                    last = systemConfigManager.isSystemPropertyEnabled(property);
                } catch (Throwable e) {
                }
                if (last) {
                    result = true;
                } else if (MATCH_ALL.equalsIgnoreCase(match)) {
                    return false;
                }
            }
        }
        return result;
    }

    protected SystemConfigManager getSystemConfigManager() {
        return (SystemConfigManager) CoreServiceProvider.getService(
                getRequest(), SystemConfigManager.class.getName());
    }
}
