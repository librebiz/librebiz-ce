/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 2, 2008 4:36:49 PM 
 * 
 */
package com.osserp.gui.client.purchasing;

import com.osserp.common.web.View;
import com.osserp.core.suppliers.Supplier;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface PurchaseOrderQueryView extends View {

    /**
     * Indicates that only confirmed or not confirmed records should be displayed
     * @return displayConfirmedMode
     */
    public boolean isDisplayConfirmedMode();

    /**
     * Toggles between display confirmed and display not confirmed mode
     */
    public void toggleDisplayConfirmedMode();

    /**
     * Indicates that all records should be displayed regardless whose created the record
     * @return displayAllMode
     */
    public boolean isDisplayAllMode();

    /**
     * Toggles between display all and display created by current user only
     * @param displayAllMode
     */
    public void toggleDisplayAllMode();

    /**
     * Indicates that only overdue records should be displayed
     * @return displayOverdueMode
     */
    public boolean isDisplayOverdueOnlyMode();

    /**
     * Toggles between display overdue and display all
     * @param displayOverdueOnlyMode
     */
    public void toggleDisplayOverdueOnlyMode();

    /**
     * Provides a selected supplier
     * @return supplier or null if none selected
     */
    public Supplier getSelectedSupplier();

    /**
     * Selects a supplier
     * @param id
     */
    public void selectSupplier(Long id);

    /**
     * Indicates that current user is a purchaser
     * @return userIsPurchaser
     */
    public boolean isUserIsPurchaser();

    /**
     * Refreshs list by current settings
     */
    public void refresh();

}
