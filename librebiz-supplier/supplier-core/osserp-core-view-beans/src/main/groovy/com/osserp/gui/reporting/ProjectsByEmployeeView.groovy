/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Feb 9, 2008 10:55:11 AM 
 * Created on Jul 27, 2011 3:49:37 PM (Groovy implementation) 
 * 
 */
package com.osserp.gui.reporting

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode

import com.osserp.core.BusinessCase
import com.osserp.core.projects.ProjectSearch
import com.osserp.core.users.DomainUser

/**
 *
 * @author rk <rk@osserp.com>
 * @author eh <eh@osserp.com>
 * 
 */
public class ProjectsByEmployeeView extends AbstractSalesListReportingView {
    private static Logger logger = LoggerFactory.getLogger(ProjectsByEmployeeView.class.getName())

    static final String SALES_MODE = 'sales'

    ProjectsByEmployeeView() {
        super()
        refreshLink = true
        selectors.businessType = 'salesMonitoringProjects'
        selectors.status = true
        values.statusValue = BusinessCase.CLOSED
        closedIgnoreLinkAvailable = true
        env.salesMode = true
    }

    @Override
    protected void init() {
        if (forwardRequest) {
            listReferenceId = form.getLong('id')
            String salesMode = form.getString('mode')
            if (salesMode && salesMode != SALES_MODE) {
                env.salesMode = false
            }
            if (!listReferenceId) {
                DomainUser cachedUser = domainUser
                if (cachedUser.projectManager && cachedUser.sales) {
                    if (cachedUser.properties['salesByManager'].enabled
                    && cachedUser.properties['salesBySales'].enabled) {
                        logger.warn("init: failed [user=${cachedUser.id}, salesByManager=true, salesBySales=true]")
                        throw new ClientException(ErrorCode.GROUP_AMBIGUOUS)
                    }
                    if (cachedUser.properties['salesBySales'].enabled) {
                        env.salesMode = false
                    }
                } else if (cachedUser.projectManager) {
                    env.salesMode = false
                }
                listReferenceId = cachedUser.employee.id
                logger.debug("init: init view with domain user [user=${cachedUser.id}]")
            }
        }
    }

    @Override
    void loadList() {
        if (salesMode) {
            list = projectSearch.findBySalesperson(listReferenceId, false)
        } else {
            list = projectSearch.findByManager(listReferenceId, false)
        }
    }

    boolean isSalesMode() {
        env.salesMode
    }

    private ProjectSearch getProjectSearch() {
        getService(ProjectSearch.class.getName())
    }
}
