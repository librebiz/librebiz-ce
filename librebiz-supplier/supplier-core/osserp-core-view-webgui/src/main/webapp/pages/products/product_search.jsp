<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="searchView" scope="session" value="${sessionScope.productSearchView}"/>
<o:viewset view="${searchView}" name="searchUrl" value="/productSearch.do"/>
<o:viewset view="${searchView}" name="searchResultPage" value="/pages/products/product_search_result_ajax.jsp"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title"><o:displayTitle/></tiles:put>
    <tiles:put name="onload" type="string">onload="initSearch(); setSearchFocus();"</tiles:put>
    <tiles:put name="styles" type="string">
        <style type="text/css">
            <c:import url="/pages/products/product_search_result.css"/>
        </style>
    </tiles:put>
    <tiles:put name="javascript" type="string">
        <script type="text/javascript">
            <c:import url="/pages/products/product_search.js"/>
        </script>
    </tiles:put>
    <tiles:put name="headline"><fmt:message key="productSearch"/></tiles:put>
    <tiles:put name="headline_right">
        <ul>
            <li><a href="<c:url value="/productSearch.do?method=toggleLazyIgnoring"/>" title="<fmt:message key="switchFlowless"/>"><o:img name="reloadIcon"/></a></li>
            <c:if test="${searchView.providingSerialNumberSearch}">
                <li><a href="<c:url value="/serialNumbers.do?method=forward&exit=productSearch"/>" title="<fmt:message key="serialNumberSearch"/>"><o:img name="numbersIcon"/></a></li>
            </c:if>
            <li><a href="<c:url value="/productSearch.do?method=enableListSelection"/>" title="<fmt:message key="listEditMode"/>"><o:img name="cutIcon"/></a></li>
            <li><v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link></li>
        </ul>
    </tiles:put>
    <tiles:put name="content" type="string">
        <div class="row">
            <div class="col-lg-12 panel-area">
                <div class="panel-body">
                    <c:import url="/pages/products/_product_search_form.jsp"/>
                </div>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
