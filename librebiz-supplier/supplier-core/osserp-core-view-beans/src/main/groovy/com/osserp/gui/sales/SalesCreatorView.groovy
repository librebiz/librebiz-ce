/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 *
 */
package com.osserp.gui.sales

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode

import com.osserp.core.requests.Request
import com.osserp.core.sales.Sales
import com.osserp.core.sales.SalesCreator

import com.osserp.groovy.web.ViewContextException

import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class SalesCreatorView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(SalesCreatorView.class.getName())

    Request request
    Long salesOfferId
    String target

    SalesCreatorView() {
        dependencies = ['businessCaseView']
        enablePopupView()
    }

    private SalesCreator getSalesCreator() {
        getService(SalesCreator.class.getName())
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            if (!env.businessCaseView?.bean) {
                logger.error('initRequest: businessCaseView.bean must not be null')
                throw new ViewContextException(ErrorCode.INVALID_CONTEXT)
            }
            request = env.businessCaseView.request
            salesOfferId = form.getLong('id')
            if (!salesOfferId) {
                logger.error('initRequest: param id must not be null')
                throw new ViewContextException(ErrorCode.INVALID_CONTEXT)
            }
            logger.debug("initRequest: done [request=${request.requestId}, offer=${salesOfferId}]")
        }
    }

    @Override
    void save() {
        Date date = form.getDate('date')
        String note = form.getString('note')
        logger.debug("save: invoked [date=$date, note=$note]")
        Sales sales = salesCreator.create(domainUser, request, salesOfferId, date, note)
        logger.debug("save: done [sales=${sales?.id}]")
        target = "/loadSales.do?id=${sales.id}"
    }
}
