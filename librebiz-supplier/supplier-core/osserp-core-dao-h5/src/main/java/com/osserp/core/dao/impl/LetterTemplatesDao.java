/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 25, 2008 10:55:32 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.dao.Tables;

import com.osserp.core.dao.LetterTemplates;
import com.osserp.core.dms.Letter;
import com.osserp.core.dms.LetterContent;
import com.osserp.core.dms.LetterTemplate;
import com.osserp.core.dms.LetterType;
import com.osserp.core.employees.Employee;
import com.osserp.core.model.letters.LetterTemplateImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class LetterTemplatesDao extends AbstractLetterContentsDao implements LetterTemplates {
    private static Logger log = LoggerFactory.getLogger(LetterTemplatesDao.class.getName());

    protected LetterTemplatesDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            String lettersTable,
            String lettersSequence) {
        super(jdbcTemplate, tables, sessionFactory, lettersTable, lettersSequence);
    }

    public List<LetterTemplate> find(LetterType type) {
        StringBuilder hql = new StringBuilder("from ");
        hql.append(getPersistentClass().getName()).append(" o");
        if (type != null) {
            hql.append(" where o.type.id = ").append(type.getId());
        }
        hql.append(" order by o.name");
        List<LetterTemplate> result = getCurrentSession().createQuery(hql.toString()).list();
        if (log.isDebugEnabled()) {
            log.debug("find() done [type=" + (type == null ? "null" : type.getId())
                    + ", count=" + result.size() + "]");
        }
        return result;
    }

    public LetterTemplate create(Employee user, Letter letter) {
        LetterTemplateImpl obj = new LetterTemplateImpl(getNextLong(getLettersSequence()), user, letter);
        save(obj);
        return obj;
    }

    public LetterTemplate create(
            Employee user,
            LetterType type,
            String name,
            Long branch,
            LetterTemplate template) {
        LetterTemplateImpl obj = new LetterTemplateImpl(
                getNextLong(getLettersSequence()),
                user, type, name, branch, template);
        if (find(type).isEmpty()) {
            obj.setDefaultTemplate(true);
        }
        save(obj);
        return obj;
    }

    @Override
    protected Class<? extends LetterContent> getPersistentClass() {
        return LetterTemplateImpl.class;
    }
}
