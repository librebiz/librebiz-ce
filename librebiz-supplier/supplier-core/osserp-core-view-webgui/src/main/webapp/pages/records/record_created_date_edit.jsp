<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<c:set var="view" value="${sessionScope.recordView}" />
<c:set var="record" value="${view.record}" />
<tr>
    <td><fmt:message key="recordDate" /></td>
    <td style="text-align: right;"><input type="text" id="createdDate" name="createdDate" value="<o:date value="${record.created}" />" style="width: 100px;" /></td>
</tr>
