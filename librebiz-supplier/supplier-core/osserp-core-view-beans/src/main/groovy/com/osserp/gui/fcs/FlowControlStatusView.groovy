/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 18, 2014 
 * 
 */
package com.osserp.gui.fcs
import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.util.CollectionUtil

import com.osserp.core.BusinessCase
import com.osserp.core.FcsAction
import com.osserp.core.FcsItem
import com.osserp.core.sales.SalesSearch

import com.osserp.groovy.web.ViewContextException
import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class FlowControlStatusView extends CoreView {
    
    BusinessCase businessCase
    FcsAction mostCurrentAction
    String selectAction

    FlowControlStatusView() {
        enablePopupView()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (forwardRequest) {
            Long id = form.getLong('id')
            if (!id) {
                throw new ViewContextException('id of businessCase not provided')
            }
            businessCase = salesSearch.findBusinessCase(id)
            list = salesSearch.getActionStatusInfo(businessCase)
            if (businessCase.flowControlSheet) {
                mostCurrentAction = businessCase.flowControlSheet.get(0).getAction()
            }
            selectAction = form.getString('selectAction')
        }
    }

    protected SalesSearch getSalesSearch() {
        getService(SalesSearch.class.getName())
    }
}
