/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 24-Oct-2006 13:15:42 
 * 
 */
package com.osserp.core.model.products;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.jdom2.Element;

import org.apache.commons.beanutils.BeanUtils;

import com.osserp.common.Constants;
import com.osserp.common.PermissionException;
import com.osserp.common.Property;
import com.osserp.common.XmlAwareEntity;
import com.osserp.common.xml.JDOMUtil;

import com.osserp.core.employees.Employee;
import com.osserp.core.model.AbstractPriceAware;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductCategory;
import com.osserp.core.products.ProductCategoryConfig;
import com.osserp.core.products.ProductClassificationConfig;
import com.osserp.core.products.ProductDetails;
import com.osserp.core.products.ProductGroup;
import com.osserp.core.products.ProductGroupConfig;
import com.osserp.core.products.ProductPlanningMode;
import com.osserp.core.products.ProductPriceByQuantity;
import com.osserp.core.products.ProductSummary;
import com.osserp.core.products.ProductSummaryImpl;
import com.osserp.core.products.ProductType;
import com.osserp.core.products.ProductTypeConfig;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProductImpl extends AbstractPriceAware implements Product, XmlAwareEntity {
    private static final Long EMPTY_QUANTITY = 11L;
    private Long productId;
    private List<ProductType> types = new ArrayList<ProductType>();
    private ProductGroup group;
    private ProductCategory category;
    private Long quantityUnit;
    private boolean quantityUnitTime = false;
    private Long manufacturer;
    private String matchcode;
    private String name;
    private String description;
    private String descriptionShort;
    private String marketingText;
    private String datasheetText;
    private String gtin13;
    private String gtin14;
    private String gtin8;
    private boolean datasheetAvailable = false;
    private boolean pictureAvailable = false;
    private boolean affectsStock = false;
    private boolean endOfLife = false;
    private Date created;
    private Long createdBy;
    private Date changed;
    private Long changedBy;
    private boolean reducedTax = false;
    private boolean serialAvailable = false;
    private boolean serialRequired = false;
    private boolean autoCalculated = false;
    private boolean calculateByGroup = false;
    private Double estimatedPurchasePrice = Constants.DOUBLE_NULL;
    private String purchaseText = null;
    private Integer packagingUnit = 1;
    private boolean virtual = false;
    private boolean plant = false;
    private boolean bundle = false;
    private boolean nullable = false;
    private boolean kanban = false;
    private boolean component = false;
    private boolean priceAware = false;
    private boolean priceByQuantity = false;
    private boolean service = false;
    private boolean planning = false;
    private boolean term = false;
    private boolean publicAvailable = false;
    private boolean custom = false;

    private boolean billingNoteRequired = false;
    private boolean quantityByPlant = false;
    private boolean offerUsageOnly = false;
    private boolean ignorePurchasePriceLimit = false;
    private String defaultLanguage = "de";
    private Long defaultStock = null;
    private ProductPlanningMode planningMode = null;
    private Double power = Constants.DOUBLE_NULL;
    private Long powerUnit = null;

    //private Details details = null;
    private ProductDetailsImpl productDetails = new ProductDetailsImpl(this);
    private Long currentStock = null;
    private Map<Long, ProductSummary> summaries = new HashMap<Long, ProductSummary>();
    private ProductSummary currentSummary = null;
    private List<ProductPriceByQuantity> priceByQuantityList = new ArrayList<ProductPriceByQuantity>();
    private transient List<ProductPriceByQuantity> priceByQuantityMatrix = new ArrayList<ProductPriceByQuantity>();

    protected ProductImpl() {
        super();
    }

    /**
     * Constructor for creating new products
     * @param user
     * @param productId
     * @param config
     * @param matchcode
     * @param name
     * @param description
     * @param quantityUnit
     * @param packagingUnit
     * @param manufacturer
     * @param defaultStock
     * @param planningMode
     * @param other
     */
    public ProductImpl(
            Employee user,
            Long productId,
            ProductClassificationConfig config,
            String matchcode,
            String name,
            String description,
            Long quantityUnit,
            Integer packagingUnit,
            Long manufacturer,
            Long defaultStock,
            ProductPlanningMode planningMode,
            Product other) {

        super();

        this.productId = productId;
        this.matchcode = matchcode;
        this.name = name;
        this.description = description;
        this.quantityUnit = quantityUnit;
        this.packagingUnit = packagingUnit;
        this.defaultStock = defaultStock;
        this.planningMode = planningMode;
        this.manufacturer = manufacturer;
        
        createdBy = user == null ? Constants.SYSTEM_EMPLOYEE : user.getId();
        created = new Date(System.currentTimeMillis());
        
        initDetailsIfNotExist();
        productDetails.setReference(productId);
        group = config.getGroup();
        category = config.getCategory();
        if (config.getType() != null) {
            types.add(config.getType());
        }
        // TODO set custom flag if category or group has enabled this
        
        serialAvailable = group.isSerialAvailable();
        serialRequired = group.isSerialRequired();
        setConsumerMargin(group.getConsumerMargin());
        setConsumerMinimumMargin(getConsumerMargin());
        setResellerMargin(group.getResellerMargin());
        setResellerMinimumMargin(getResellerMargin());
        setPartnerMargin(group.getPartnerMargin());
        setPartnerMinimumMargin(getPartnerMargin());

        if (other != null) {
            // constructor arg always wins
            if (defaultStock == null) {
                this.defaultStock = other.getDefaultStock();
            }
            if (planningMode == null) {
                this.planningMode = other.getPlanningMode();
            }
            descriptionShort = other.getDescriptionShort();
            marketingText = other.getMarketingText();
            // copy existing settings 
            affectsStock = other.isAffectsStock();
            autoCalculated = other.isAutoCalculated();
            billingNoteRequired = other.isBillingNoteRequired();
            calculateByGroup = other.isCalculateByGroup();
            component = other.isComponent();
            ignorePurchasePriceLimit = other.isIgnorePurchasePriceLimit();
            kanban = other.isKanban();
            nullable = other.isNullable();
            offerUsageOnly = other.isOfferUsageOnly();
            planning = other.isPlanning();
            plant = other.isPlant();
            power = other.getPower();
            powerUnit = other.getPowerUnit();
            priceAware = other.isPriceAware();
            purchaseText = other.getPurchaseText();
            quantityByPlant = other.isQuantityByPlant();
            quantityUnitTime = other.isQuantityUnitTime();
            reducedTax = other.isReducedTax();
            serialAvailable = other.isSerialAvailable();
            serialRequired = other.isSerialRequired();
            service = other.isService();
            term = other.isTerm();
            virtual = other.isVirtual();
            custom = other.isCustom();
            
            if (!other.getTypes().isEmpty()) {
                for (int i = 0, j = other.getTypes().size(); i < j; i++) {
                    ProductType type = other.getTypes().get(i);
                    if (!isType(type)) {
                        types.add(type);
                    }
                }
            }
        }
    }

    /**
     * Constructor used for existing product check
     * @param productId
     * @param matchcode
     * @param name
     */
    public ProductImpl(
            Long productId,
            String matchcode,
            String name) {
        this.productId = productId;
        this.matchcode = matchcode;
        this.name = name;
        initDetailsIfNotExist();
        productDetails.setReference(productId);
    }

    public ProductImpl(Element root) {
        for (Object child : root.getChildren()) {
            Element el = (Element) child;
            try {
                if (BeanUtils.getProperty(this, el.getName()) == null) {
                    BeanUtils.setProperty(this, el.getName(), JDOMUtil.getObjectFromElement(el));
                }
            } catch (Exception ex) {
            }
        }
    }

    public Long getPrimaryKey() {
        return productId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public ProductCategory getCategory() {
        return category;
    }

    public void setCategory(ProductCategory category) {
        this.category = category;
    }

    public ProductGroup getGroup() {
        return group;
    }

    public void setGroup(ProductGroup group) {
        this.group = group;
    }

    public boolean isTypeAvailable() {
        return (types != null && !this.types.isEmpty());
    }

    public boolean isType(ProductType type) {
        for (int i = 0, j = types.size(); i < j; i++) {
            ProductType next = types.get(i);
            if (next.getId().equals(type.getId())) {
                return true;
            }
        }
        return false;
    }

    public void removeType(ProductType type) {
        for (Iterator<ProductType> i = types.iterator(); i.hasNext();) {
            ProductType next = i.next();
            if (next.getId().equals(type.getId())) {
                i.remove();
                break;
            }
        }
        if (types.isEmpty()) {
            category = null;
        }
    }

    public List<ProductType> getTypes() {
        return types;
    }

    protected void setTypes(List<ProductType> types) {
        this.types = types;
    }

    public String getTypeDisplay() {
        if (types == null || types.isEmpty()) {
            return "";
        }
        int typeSize = types.size();
        if (typeSize == 1) {
            return types.get(0).getName();
        }
        StringBuilder typeNames = new StringBuilder();
        for (int i = 0; i < typeSize; i++) {
            typeNames.append(types.get(i).getName());
            if (i < typeSize - 1) {
                typeNames.append(", ");
            }
        }
        return typeNames.toString();
    }

    public ProductType getTypeDefault() {
        if (types == null || types.isEmpty()) {
            return null;
        }
        return types.get(0);
    }

    public void updateClassification(
            ProductTypeConfig typeConfig,
            ProductGroupConfig groupConfig,
            ProductCategoryConfig categoryConfig) {

        if (categoryConfig != null) {
            category = categoryConfig.getProductCategory();
        } else {
            category = null;
        }

        if (groupConfig != null) {
            group = groupConfig.getProductGroup();
        }
        if (typeConfig != null) {
            ProductType typeObj = typeConfig.getProductType();
            if (!isType(typeObj)) {
                types.add(typeConfig.getProductType());
            }
        }
    }

    public boolean isPlanning() {
        return planning;
    }

    public void setPlanning(boolean planning) {
        this.planning = planning;
    }

    public ProductPlanningMode getPlanningMode() {
        return planningMode;
    }

    public void setPlanningMode(ProductPlanningMode planningMode) {
        this.planningMode = planningMode;
    }

    public Date getChanged() {
        return changed;
    }

    public void setChanged(Date changed) {
        this.changed = changed;
    }

    public Long getChangedBy() {
        return changedBy;
    }

    public void setChangedBy(Long changedBy) {
        this.changedBy = changedBy;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public boolean isEndOfLife() {
        return endOfLife;
    }

    public void setEndOfLife(boolean endOfLife) {
        this.endOfLife = endOfLife;
    }

    public String getDatasheetText() {
        return datasheetText;
    }

    public void setDatasheetText(String datasheetText) {
        this.datasheetText = datasheetText;
    }

    public boolean isDatasheetAvailable() {
        return datasheetAvailable;
    }

    public void setDatasheetAvailable(boolean datasheetAvailable) {
        this.datasheetAvailable = datasheetAvailable;
    }

    public boolean isPictureAvailable() {
        return pictureAvailable;
    }

    public void setPictureAvailable(boolean pictureAvailable) {
        this.pictureAvailable = pictureAvailable;
    }

    public boolean isAffectsStock() {
        return affectsStock;
    }

    public void setAffectsStock(boolean affectsStock) {
        this.affectsStock = affectsStock;
    }

    public boolean isAvailableOnAnyStock() {
        for (Iterator<Long> i = summaries.keySet().iterator(); i.hasNext();) {
            ProductSummary summary = summaries.get(i.next());
            if (summary != null && summary.getAvailableStock() != null 
                    && summary.getAvailableStock() > 0) {
                return true;
            }
        }
        return false;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionShort() {
        return descriptionShort;
    }

    public void setDescriptionShort(String descriptionShort) {
        this.descriptionShort = descriptionShort;
    }

    public String getMarketingText() {
        return marketingText;
    }

    public void setMarketingText(String marketingText) {
        this.marketingText = marketingText;
    }

    public Long getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(Long manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getMatchcode() {
        return matchcode;
    }

    public void setMatchcode(String matchcode) {
        this.matchcode = matchcode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getQuantityUnit() {
        return quantityUnit;
    }

    public void setQuantityUnit(Long quantityUnit) {
        this.quantityUnit = quantityUnit;
    }

    public boolean isQuantityUnitTime() {
        return quantityUnitTime;
    }

    public void setQuantityUnitTime(boolean quantityUnitTime) {
        this.quantityUnitTime = quantityUnitTime;
    }

    public Double getPower() {
        return power;
    }

    public void setPower(Double power) {
        this.power = power;
    }

    public Long getPowerUnit() {
        return powerUnit;
    }

    public void setPowerUnit(Long powerUnit) {
        this.powerUnit = powerUnit;
    }

    public boolean isReducedTax() {
        return reducedTax;
    }

    public void setReducedTax(boolean reducedTax) {
        this.reducedTax = reducedTax;
    }

    public boolean isSerialAvailable() {
        return serialAvailable;
    }

    public void setSerialAvailable(boolean serialAvailable) {
        this.serialAvailable = serialAvailable;
    }

    public boolean isSerialRequired() {
        return serialRequired;
    }

    public void setSerialRequired(boolean serialRequired) {
        this.serialRequired = serialRequired;
    }

    public void changeSerialFlags(boolean available, boolean required) {
        serialAvailable = available;
        serialRequired = required;
    }

    public boolean isAutoCalculated() {
        return autoCalculated;
    }

    public void setAutoCalculated(boolean autoCalculated) {
        this.autoCalculated = autoCalculated;
    }

    public boolean isCalculateByGroup() {
        return calculateByGroup;
    }

    public void setCalculateByGroup(boolean calculateByGroup) {
        this.calculateByGroup = calculateByGroup;
    }

    public Double getEstimatedPurchasePrice() {
        return estimatedPurchasePrice;
    }

    public void setEstimatedPurchasePrice(Double estimatedPurchasePrice) {
        this.estimatedPurchasePrice = estimatedPurchasePrice;
    }

    public Double getCalculationPurchasePrice() {
        if (estimatedPurchasePrice != null && estimatedPurchasePrice > 0) {
            return estimatedPurchasePrice;
        }
        ProductSummary summary = getSummary();
        return summary.getLastPurchasePrice();
    }

    public String getPurchaseText() {
        return purchaseText;
    }

    public void setPurchaseText(String purchaseText) {
        this.purchaseText = purchaseText;
    }

    public Integer getPackagingUnit() {
        return packagingUnit;
    }

    public void setPackagingUnit(Integer packagingUnit) {
        this.packagingUnit = packagingUnit;
    }

    public boolean isCustom() {
        return custom;
    }

    public void setCustom(boolean custom) {
        this.custom = custom;
    }

    public boolean isVirtual() {
        return virtual;
    }

    public void setVirtual(boolean virtual) {
        this.virtual = virtual;
    }

    public boolean isPlant() {
        return plant;
    }

    public void setPlant(boolean plant) {
        this.plant = plant;
    }

    public boolean isBundle() {
        return bundle;
    }

    public void setBundle(boolean bundle) {
        this.bundle = bundle;
    }

    public boolean isNullable() {
        return nullable;
    }

    public void setNullable(boolean nullable) {
        this.nullable = nullable;
    }

    public boolean isKanban() {
        return kanban;
    }

    public void setKanban(boolean kanban) {
        this.kanban = kanban;
    }

    public boolean isComponent() {
        return component;
    }

    public void setComponent(boolean component) {
        this.component = component;
    }

    public boolean isTerm() {
        return term;
    }

    public void setTerm(boolean term) {
        this.term = term;
    }

    public boolean isPublicAvailable() {
        return publicAvailable;
    }

    public void setPublicAvailable(boolean publicAvailable) {
        this.publicAvailable = publicAvailable;
    }

    public boolean isPriceAware() {
        return priceAware;
    }

    public void setPriceAware(boolean priceAware) {
        this.priceAware = priceAware;
    }

    public boolean isPriceByQuantity() {
        return priceByQuantity;
    }

    public void setPriceByQuantity(boolean priceByQuantity) {
        this.priceByQuantity = priceByQuantity;
    }

    public boolean isBillingNoteRequired() {
        return billingNoteRequired;
    }

    public void setBillingNoteRequired(boolean billingNoteRequired) {
        this.billingNoteRequired = billingNoteRequired;
    }

    public boolean isOfferUsageOnly() {
        return offerUsageOnly;
    }

    public void setOfferUsageOnly(boolean offerUsageOnly) {
        this.offerUsageOnly = offerUsageOnly;
    }

    public boolean isIgnorePurchasePriceLimit() {
        return ignorePurchasePriceLimit;
    }

    protected void setIgnorePurchasePriceLimit(boolean ignorePurchasePriceLimit) {
        this.ignorePurchasePriceLimit = ignorePurchasePriceLimit;
    }

    public void changeIgnorePurchasePriceFlag(DomainUser user, String[] perms) throws PermissionException {
        user.checkPermission(perms);
        this.ignorePurchasePriceLimit = !this.ignorePurchasePriceLimit;
    }

    public boolean isPriceByExternalQuantity() {
        return (priceByQuantity && (quantityUnit == null
        || EMPTY_QUANTITY.equals(quantityUnit)));
    }

    public boolean isQuantityByPlant() {
        return quantityByPlant;
    }

    public void setQuantityByPlant(boolean quantityByPlant) {
        this.quantityByPlant = quantityByPlant;
    }

    public boolean isService() {
        return service;
    }

    public void setService(boolean service) {
        this.service = service;
    }

    public String getDefaultLanguage() {
        return defaultLanguage;
    }

    public void setDefaultLanguage(String defaultLanguage) {
        this.defaultLanguage = defaultLanguage;
    }

    public String getGtin13() {
        return gtin13;
    }

    public void setGtin13(String gtin13) {
        this.gtin13 = gtin13;
    }

    public String getGtin14() {
        return gtin14;
    }

    public void setGtin14(String gtin14) {
        this.gtin14 = gtin14;
    }

    public String getGtin8() {
        return gtin8;
    }

    public void setGtin8(String gtin8) {
        this.gtin8 = gtin8;
    }

    public ProductDetails getDetails() {
        if (isPlant()) {
            return new PackageImpl(productDetails);
        }
        return productDetails;
    }

    public boolean isDetailAvailable() {
        initDetailsIfNotExist();
        List<Property> properties = productDetails.getPropertyList();
        for (int i = 0, j = properties.size(); i < j; i++) {
            Property property = properties.get(i);
            if (property.getAsString().length() > 0) {
                return true;
            }
        }
        return false;
    }

    public List<Property> getPropertyList() {
        initDetailsIfNotExist();
        return productDetails.getPropertyList();
    }

    public Map<String, Property> getPropertyMap() {
        initDetailsIfNotExist();
        return productDetails.getPropertyMap();
    }

    public void addProperty(Long user, String pname, String value) {
        initDetailsIfNotExist();
        productDetails.addProperty(user, pname, value);
    }

    public void removeProperty(Long user, Property property) {
        initDetailsIfNotExist();
        productDetails.removeProperty(user, property);
    }

    public boolean isPropertyAvailable(String propertyName) {
        initDetailsIfNotExist();
        return productDetails.getPropertyMap().get(propertyName) != null;
    }

    public Property getProperty(String propertyName) {
        initDetailsIfNotExist();
        return productDetails.getPropertyMap().get(propertyName);
    }

    public ProductSummary getSummary() {
        if (currentSummary == null) {
            currentSummary = fetchSummary();
        }
        return currentSummary;
    }

    public void addSummary(ProductSummary summary) {
        if (summary != null) {
            if (isKanban() && summary instanceof ProductSummaryImpl) {
                ProductSummaryImpl obj = (ProductSummaryImpl) summary;
                obj.markAsKanban();
            }
            summaries.put(summary.getStockId(), summary);
        }
    }

    public boolean isLazy() {
        ProductSummary summary = getSummary();
        return summary == null || (isNotSet(summary.getExpected())
                && isNotSet(summary.getOrdered())
                && isNotSet(summary.getReceipt())
                && isNotSet(summary.getStock()));
    }

    public Long getCurrentStock() {
        return currentStock;
    }

    public void setCurrentStock(Long currentStock) {
        this.currentStock = currentStock;
    }

    public void enableStock(Long id) {
        currentStock = id;
        currentSummary = fetchSummary();
    }

    public Long getDefaultStock() {
        return defaultStock;
    }

    public void setDefaultStock(Long defaultStock) {
        this.defaultStock = defaultStock;
    }

    public boolean isDeliveryNoteAffecting() {
        if (isBundle()) {
            return true;
        }
        return (isAffectsStock()
                && !isService()
                && !isVirtual());
    }

    public boolean isSpecificPartnerPriceAvailable() {
        // default implementation does not support specific prices
        return false;
    }

    public Double getSpecificPartnerPrice() {
        if (getPartnerPrice() == null) {
            return 0D;
        }
        if (power != null && power > 0) {
            return getPartnerPrice() / power;
        }
        return 0D;
    }

    public final List<ProductPriceByQuantity> getPriceByQuantityMatrix() {
        if (priceByQuantity && priceByQuantityMatrix.isEmpty() && !priceByQuantityList.isEmpty()) {
            for (int i = 0, j = priceByQuantityList.size(); i < j; i++) {
                ProductPriceByQuantity ap = priceByQuantityList.get(i);
                if (!ap.isEndOfLife()) {
                    priceByQuantityMatrix.add(ap);
                }
            }
        }
        return priceByQuantityMatrix;
    }

    public final Double getConsumerPriceByQuantity(Double quantity) {
        Double price = getConsumerPrice();
        if (isPriceByQuantity() && quantity != null) {
            List<ProductPriceByQuantity> matrix = getPriceByQuantityMatrix();
            if (!matrix.isEmpty()) {
                for (int i = 0, j = matrix.size(); i < j; i++) {
                    ProductPriceByQuantity pbq = matrix.get(i);
                    if (quantity >= pbq.getRangeStart() && (quantity < pbq.getRangeEnd() || pbq.isLast())) {
                        if (isSet(pbq.getConsumerPrice())) {
                            price = pbq.getConsumerPrice();
                        }
                        break;
                    }
                }
            }
        }
        return price;
    }

    public final Double getPartnerPriceByQuantity(Double quantity) {
        Double price = getPartnerPrice();
        if (isPriceByQuantity() && quantity != null) {
            List<ProductPriceByQuantity> matrix = getPriceByQuantityMatrix();
            if (!matrix.isEmpty()) {
                for (int i = 0, j = matrix.size(); i < j; i++) {
                    ProductPriceByQuantity pbq = matrix.get(i);
                    if (quantity >= pbq.getRangeStart() && (quantity < pbq.getRangeEnd() || pbq.isLast())) {
                        if (isSet(pbq.getPartnerPrice())) {
                            price = pbq.getPartnerPrice();
                        }
                        break;
                    }
                }
            }
        }
        return price;
    }

    public final Double getResellerPriceByQuantity(Double quantity) {
        Double price = getResellerPrice();
        if (isPriceByQuantity() && quantity != null) {
            List<ProductPriceByQuantity> matrix = getPriceByQuantityMatrix();
            if (!matrix.isEmpty()) {
                for (int i = 0, j = matrix.size(); i < j; i++) {
                    ProductPriceByQuantity pbq = matrix.get(i);
                    if (quantity >= pbq.getRangeStart() 
                            && (quantity < pbq.getRangeEnd() || pbq.isLast())) {
                        
                        if (isSet(pbq.getResellerPrice())) {
                            price = pbq.getResellerPrice();
                        }
                        break;
                    }
                }
            }
        }
        return price;
    }

    public Double getPurchasePriceForMinimumCalculation() {
        if (isSet(getSummary().getLastPurchasePrice())) {
            return getSummary().getLastPurchasePrice();
        }
        if (isSet(getSummary().getAveragePurchasePrice())) {
            return getSummary().getAveragePurchasePrice();
        }
        Double d = getEstimatedPurchasePrice();
        return d == null ? 0 : d;
    }

    protected List<ProductPriceByQuantity> getPriceByQuantityList() {
        return priceByQuantityList;
    }

    protected void setPriceByQuantityList(
            List<ProductPriceByQuantity> priceByQuantityList) {
        this.priceByQuantityList = priceByQuantityList;
    }

    protected ProductDetailsImpl getProductDetails() {
        return productDetails;
    }

    protected void setProductDetails(ProductDetailsImpl productDetails) {
        this.productDetails = productDetails;
    }
    
    private void initDetailsIfNotExist() {
        if (productDetails == null) {
            productDetails = new ProductDetailsImpl(this);
        }
    }

    public ProductImpl(
            Long productId,
            List<ProductType> types,
            ProductGroup group,
            ProductCategory category,
            ProductPlanningMode planningMode,
            Long quantityUnit,
            boolean quantityUnitTime,
            Long manufacturer,
            String matchcode,
            String name,
            String description,
            String gtin13,
            String gtin14,
            String gtin8,
            String descriptionShort,
            String marketingText,
            boolean datasheetAvailable,
            boolean endOfLife,
            Date created,
            Long createdBy,
            Date changed,
            Long changedBy,
            Double consumerPrice,
            Double resellerPrice,
            Double partnerPrice,
            boolean reducedTax,
            boolean affectsStock,
            boolean serialAvailable,
            boolean serialRequired,
            boolean autoCalculated,
            boolean calculateByGroup,
            Double consumerMargin,
            Double resellerMargin,
            Double partnerMargin,
            String purchaseText,
            Integer packagingUnit,
            boolean custom,
            boolean virtual,
            boolean plant,
            boolean bundle,
            boolean nullable,
            boolean kanban,
            Double consumerPriceMinimum,
            Double resellerPriceMinimum,
            Double partnerPriceMinimum,
            Double consumerMinimumMargin,
            Double resellerMinimumMargin,
            Double partnerMinimumMargin,
            Double estimatedPurchasePrice,
            boolean billingNoteRequired,
            boolean quantityByPlant,
            boolean offerUsageOnly,
            boolean ignorePurchasePriceLimit,
            boolean priceAware,
            boolean priceByQuantity,
            boolean component,
            boolean service,
            boolean term,
            boolean publicAvailable,
            String defaultLanguage,
            Long defaultStock) {

        super(
                consumerPrice,
                resellerPrice,
                partnerPrice,
                consumerPriceMinimum,
                resellerPriceMinimum,
                partnerPriceMinimum,
                consumerMargin,
                resellerMargin,
                partnerMargin,
                consumerMinimumMargin,
                resellerMinimumMargin,
                partnerMinimumMargin);
        this.productId = productId;
        this.group = group;
        if (types != null) {
            this.types = types;
        }
        this.planningMode = planningMode;
        this.category = category;
        this.quantityUnit = quantityUnit;
        this.quantityUnitTime = quantityUnitTime;
        this.manufacturer = manufacturer;
        this.matchcode = matchcode;
        this.name = name;
        this.description = description;
        this.descriptionShort = descriptionShort;
        this.gtin13 = gtin13;
        this.gtin14 = gtin14;
        this.gtin8 = gtin8;
        this.marketingText = marketingText;
        this.datasheetAvailable = datasheetAvailable;
        this.affectsStock = affectsStock;
        this.endOfLife = endOfLife;
        this.created = created;
        this.createdBy = createdBy;
        this.changed = changed;
        this.changedBy = changedBy;
        this.reducedTax = reducedTax;
        this.serialAvailable = serialAvailable;
        this.serialRequired = serialRequired;
        this.autoCalculated = autoCalculated;
        this.calculateByGroup = calculateByGroup;
        this.estimatedPurchasePrice = (estimatedPurchasePrice == null ? Constants.DOUBLE_NULL : estimatedPurchasePrice);
        this.purchaseText = purchaseText;
        this.packagingUnit = (packagingUnit == null ? Constants.INT_NULL : packagingUnit);
        this.custom = custom;
        this.virtual = virtual;
        this.plant = plant;
        this.bundle = bundle;
        this.nullable = nullable;
        this.kanban = kanban;
        this.component = component;
        this.priceAware = priceAware;
        this.priceByQuantity = priceByQuantity;
        this.billingNoteRequired = billingNoteRequired;
        this.quantityByPlant = quantityByPlant;
        this.offerUsageOnly = offerUsageOnly;
        this.ignorePurchasePriceLimit = ignorePurchasePriceLimit;
        this.service = service;
        this.term = term;
        this.publicAvailable = publicAvailable;
        this.defaultLanguage = defaultLanguage;
        this.defaultStock = defaultStock;
    }

    private ProductSummary fetchSummary() {
        Long stockToFetch = (currentStock != null ? currentStock : defaultStock);
        ProductSummary summary = (stockToFetch == null ? null : summaries.get(stockToFetch));
        if (summary == null) {
            ProductSummaryImpl obj = new ProductSummaryImpl(this);
            if (isKanban()) {
                obj.markAsKanban();
                summary = obj;
            }
        }
        return summary;
    }

    public Element getXML() {
        return getXML("root");
    }

    public Element getXML(String rootElementName) {
        Element root = new Element(rootElementName);
        root.setAttribute("type", "product");

        root.addContent(JDOMUtil.createElement("productId", productId));
        root.addContent(JDOMUtil.createElement("quantityUnit", quantityUnit));
        root.addContent(JDOMUtil.createElement("quantityUnitTime", quantityUnitTime));
        root.addContent(JDOMUtil.createElement("manufacturer", manufacturer));
        root.addContent(JDOMUtil.createElement("matchcode", matchcode));
        root.addContent(JDOMUtil.createElement("name", name));
        root.addContent(JDOMUtil.createElement("description", description));
        root.addContent(JDOMUtil.createElement("descriptionShort", descriptionShort));
        root.addContent(JDOMUtil.createElement("gtin13", gtin13));
        root.addContent(JDOMUtil.createElement("gtin14", gtin14));
        root.addContent(JDOMUtil.createElement("gtin8", gtin8));
        root.addContent(JDOMUtil.createElement("marketingText", marketingText));
        root.addContent(JDOMUtil.createElement("datasheetText", datasheetText));
        root.addContent(JDOMUtil.createElement("datasheetAvailable", datasheetAvailable));
        root.addContent(JDOMUtil.createElement("affectsStock", affectsStock));
        root.addContent(JDOMUtil.createElement("endOfLife", endOfLife));
        root.addContent(JDOMUtil.createElement("created", created));
        root.addContent(JDOMUtil.createElement("createdBy", createdBy));
        root.addContent(JDOMUtil.createElement("changed", changed));
        root.addContent(JDOMUtil.createElement("changedBy", changedBy));
        root.addContent(JDOMUtil.createElement("reducedTax", reducedTax));
        root.addContent(JDOMUtil.createElement("consumerPrice", getConsumerPrice()));
        root.addContent(JDOMUtil.createElement("resellerPrice", getResellerPrice()));
        root.addContent(JDOMUtil.createElement("partnerPrice", getPartnerPrice()));
        root.addContent(JDOMUtil.createElement("serialAvailable", serialAvailable));
        root.addContent(JDOMUtil.createElement("serialRequired", serialRequired));
        root.addContent(JDOMUtil.createElement("autoCalculated", autoCalculated));
        root.addContent(JDOMUtil.createElement("calculateByGroup", calculateByGroup));
        root.addContent(JDOMUtil.createElement("consumerMargin", getConsumerMargin()));
        root.addContent(JDOMUtil.createElement("resellerMargin", getResellerMargin()));
        root.addContent(JDOMUtil.createElement("partnerMargin", getPartnerMargin()));
        root.addContent(JDOMUtil.createElement("estimatedPurchasePrice", estimatedPurchasePrice));
        root.addContent(JDOMUtil.createElement("purchaseText", purchaseText));
        root.addContent(JDOMUtil.createElement("packagingUnit", packagingUnit));
        root.addContent(JDOMUtil.createElement("custom", custom));
        root.addContent(JDOMUtil.createElement("virtual", virtual));
        root.addContent(JDOMUtil.createElement("plant", plant));
        root.addContent(JDOMUtil.createElement("bundle", bundle));
        root.addContent(JDOMUtil.createElement("nullable", nullable));
        root.addContent(JDOMUtil.createElement("kanban", kanban));
        root.addContent(JDOMUtil.createElement("component", component));
        root.addContent(JDOMUtil.createElement("term", term));
        root.addContent(JDOMUtil.createElement("publicAvailable", publicAvailable));
        root.addContent(JDOMUtil.createElement("priceAware", priceAware));
        root.addContent(JDOMUtil.createElement("priceByQuantity", priceByQuantity));
        root.addContent(JDOMUtil.createElement("service", service));
        root.addContent(JDOMUtil.createElement("consumerPriceMinimum", getConsumerPriceMinimum()));
        root.addContent(JDOMUtil.createElement("resellerPriceMinimum", getResellerPriceMinimum()));
        root.addContent(JDOMUtil.createElement("partnerPriceMinimum", getPartnerPriceMinimum()));
        root.addContent(JDOMUtil.createElement("consumerMinimumMargin", getConsumerMinimumMargin()));
        root.addContent(JDOMUtil.createElement("resellerMinimumMargin", getResellerMinimumMargin()));
        root.addContent(JDOMUtil.createElement("partnerMinimumMargin", getPartnerMinimumMargin()));
        root.addContent(JDOMUtil.createElement("billingNoteRequired", billingNoteRequired));
        root.addContent(JDOMUtil.createElement("quantityByPlant", quantityByPlant));
        root.addContent(JDOMUtil.createElement("offerUsageOnly", offerUsageOnly));
        root.addContent(JDOMUtil.createElement("ignorePurchasePriceLimit", ignorePurchasePriceLimit));
        root.addContent(JDOMUtil.createElement("defaultLanguage", defaultLanguage));
        root.addContent(JDOMUtil.createElement("defaultStock", defaultStock));
        root.addContent(JDOMUtil.createElement("currentStock", currentStock));

        root.addContent(JDOMUtil.createElement("group", group));
        root.addContent(JDOMUtil.createElement("category", category));
        root.addContent(JDOMUtil.createElement("currentSummary", currentSummary));
        root.addContent(JDOMUtil.createElement("details", productDetails));
        root.addContent(JDOMUtil.createElement("types", types));
        root.addContent(JDOMUtil.createElement("summaries", summaries));

        return root;
    }
}
