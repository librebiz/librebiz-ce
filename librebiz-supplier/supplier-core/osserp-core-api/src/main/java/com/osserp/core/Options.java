/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 27.09.2004 
 * 
 */
package com.osserp.core;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Options {

    static final String BILLING_PERCENTAGES = "billingPercentages";
    static final String CALCULATION_NAMES = "calculationNames";
    static final String CALCULATION_TYPES = "calculationTypes";
    static final String CAMPAIGNS = "campaigns";
    static final String CAMPAIGN_GROUPS = "campaignGroups";
    static final String CAMPAIGN_TYPES = "campaignTypes";
    static final String CUSTOMER_STATUS = "customerStatus";
    static final String CUSTOMER_TYPES = "customerTypes";
    static final String EMAIL_TYPES = "emailTypes";
    static final String PHONE_DEVICES = "phoneDevices";
    static final String PHONE_TYPES = "phoneTypes";
    static final String PRODUCT_COLORS = "productColors";
    static final String PROJECT_MANAGER_STATES = "projectManagerStates";
    static final String RECORD_DELIVERY_CONDITIONS = "recordDeliveryConditions";
    static final String RECORD_INFO_CONFIGS = "recordInfoConfigs";
    static final String RECORD_PAYMENT_TARGETS = "recordPaymentTargets";
    static final String RECORD_STATUS = "recordStatus";
    static final String REQUEST_FCS_CLOSINGS = "requestFcsClosings";
    static final String REQUEST_ORIGIN_TYPES = "requestOriginTypes";
    static final String SALES_STATUS = "salesStatus";
    static final String STOCKTAKING_TYPES = "stocktakingTypes";
    static final String TELEPHONE_TYPES = "telephoneTypes";
    static final String TAX_FREE_DEFINITIONS = "taxFreeDefinitions";
    static final String YEARS = "years";

    static final String[] ALL_OPTIONS = {
            BILLING_PERCENTAGES,
            CALCULATION_NAMES,
            CALCULATION_TYPES,
            CAMPAIGNS,
            CAMPAIGN_GROUPS,
            CAMPAIGN_TYPES,
            CUSTOMER_STATUS,
            CUSTOMER_TYPES,
            RECORD_DELIVERY_CONDITIONS,
            EMAIL_TYPES,
            PHONE_DEVICES,
            PHONE_TYPES,
            PRODUCT_COLORS,
            PROJECT_MANAGER_STATES,
            RECORD_INFO_CONFIGS,
            RECORD_PAYMENT_TARGETS,
            RECORD_STATUS,
            REQUEST_FCS_CLOSINGS,
            REQUEST_ORIGIN_TYPES,
            SALES_STATUS,
            TELEPHONE_TYPES,
            STOCKTAKING_TYPES,
            TAX_FREE_DEFINITIONS,
            YEARS
    };

    // names

    static final String ACTIVATED_EMPLOYEES = "activatedEmployees";
    static final String BANK_ACCOUNTS = "bankAccounts";
    static final String BILLING_TYPES = "billingTypes";
    static final String BRANCH_OFFICES = "branchOffices";
    static final String BRANCH_OFFICE_KEYS = "branchOfficeKeys";
    static final String BUSINESS_STATUS = "businessStatus";
    static final String CONTACT_GROUPS = "contactGroups";
    static final String CONTACT_TYPES = "contactTypes";
    static final String COUNTRY_CODES = "countryCodes";
    static final String COUNTRY_NAMES = "countryNames";
    static final String COUNTRY_PREFIXES = "countryPrefixes";
    static final String CURRENCIES = "currencies";
    static final String DELIVERY_NOTE_TYPES = "deliveryNoteTypes";
    static final String DISTRICTS = "districts";
    static final String DOCUMENT_CATEGORIES = "documentCategories";
    static final String DOCUMENT_TYPES = "documentTypes";
    static final String EMPLOYEES = "employees";
    static final String EMPLOYEE_GROUPS = "employeeGroups";
    static final String EMPLOYEE_GROUP_KEYS = "employeeGroupKeys";
    static final String EMPLOYEE_KEYS = "employeeKeys";
    static final String EMPLOYEE_STATUS = "employeeStatus";
    static final String EMPLOYEE_TYPES = "employeeTypes";
    static final String EVENT_ACTIONS = "eventActions";
    static final String FEDERAL_STATES = "federalStates";
    static final String FLOW_CONTROL_ACTIONS = "flowControlActions";
    static final String LEGALFORMS = "legalforms";
    static final String MONTHS = "months";
    static final String PAYMENT_TARGET_NAMES = "paymentTargetNames";
    static final String PAYMENT_TYPES = "paymentTypes";
    static final String PRODUCT_CATEGORIES = "productCategories";
    static final String PRODUCT_CLASSES = "productClasses";
    static final String PRODUCT_GROUPS = "productGroups";
    static final String PRODUCT_MANUFACTURERS = "productManufacturers";
    static final String PRODUCT_TYPES = "productTypes";
    static final String PROJECT_TRACKING_RECORD_TYPES = "projectTrackingRecordTypes";
    static final String PROJECT_TRACKING_TYPES = "projectTrackingTypes";
    static final String PURCHASE_INVOICE_TYPES = "purchaseInvoiceTypes";
    static final String PURCHASE_INVOICE_SHORTTYPES = "purchaseInvoiceShorttypes";
    static final String PURCHASE_ORDER_TYPES = "purchaseOrderTypes";
    static final String QUANTITY_UNITS = "quantityUnits";
    static final String RECORD_PAYMENT_CONDITIONS = "recordPaymentConditions";
    static final String RECORD_TYPES = "recordTypes";
    static final String REQUEST_FCS_ACTIONS = "requestFcsActions";
    static final String REQUEST_STATUS = "requestStatus";
    static final String REQUEST_TYPES = "requestTypes";
    static final String REQUEST_TYPE_KEYS = "requestTypeKeys";
    static final String SALES_CONTRACT_STATUS = "salesContractStatus";
    static final String SALES_CONTRACT_TYPES = "salesContractTypes";
    static final String SALES_CREDIT_NOTE_TYPES = "salesCreditNoteTypes";
    static final String SALES_ORDER_TYPES = "salesOrderTypes";
    static final String SALUTATIONS = "salutations";
    static final String SHIPPING_COMPANIES = "shippingCompanies";
    static final String SHIPPING_COMPANY_KEYS = "shippingCompanyKeys";
    static final String STOCK_KEYS = "stockKeys";
    static final String STOCK_NAMES = "stockNames";
    static final String SUPPLIER_TYPES = "supplierTypes";
    static final String SUPPLIERS = "suppliers";
    static final String SYSTEM_COMPANIES = "systemCompanies";
    static final String SYSTEM_COMPANY_KEYS = "systemCompanyKeys";
    static final String TIME_RECORD_MARKER_TYPES = "timeRecordMarkerTypes";
    static final String TIME_RECORD_STATUS = "timeRecordStatus";
    static final String TIME_RECORD_TYPES = "timeRecordTypes";
    static final String TIME_RECORDING_CONFIGS = "timeRecordingConfigs";
    static final String TITLES = "titles";
    static final String USERS = "users";
    static final String USER_LOGINS = "userLogins";

    static final String[] ALL_NAMES = {
            ACTIVATED_EMPLOYEES,
            BANK_ACCOUNTS,
            BILLING_TYPES,
            BRANCH_OFFICES,
            BRANCH_OFFICE_KEYS,
            BUSINESS_STATUS,
            CAMPAIGN_GROUPS,
            CAMPAIGN_TYPES,
            CAMPAIGNS,
            CONTACT_GROUPS,
            CONTACT_TYPES,
            COUNTRY_CODES,
            COUNTRY_NAMES,
            COUNTRY_PREFIXES,
            CURRENCIES,
            DISTRICTS,
            DELIVERY_NOTE_TYPES,
            DOCUMENT_CATEGORIES,
            DOCUMENT_TYPES,
            EMPLOYEES,
            EMPLOYEE_GROUPS,
            EMPLOYEE_GROUP_KEYS,
            EMPLOYEE_KEYS,
            EMPLOYEE_STATUS,
            EMPLOYEE_TYPES,
            EVENT_ACTIONS,
            FEDERAL_STATES,
            FLOW_CONTROL_ACTIONS,
            LEGALFORMS,
            MONTHS,
            PAYMENT_TARGET_NAMES,
            PAYMENT_TYPES,
            PRODUCT_CATEGORIES,
            PRODUCT_CLASSES,
            PRODUCT_GROUPS,
            PRODUCT_MANUFACTURERS,
            PRODUCT_TYPES,
            PROJECT_TRACKING_RECORD_TYPES,
            PROJECT_TRACKING_TYPES,
            PURCHASE_INVOICE_SHORTTYPES,
            PURCHASE_INVOICE_TYPES,
            PURCHASE_ORDER_TYPES,
            QUANTITY_UNITS,
            RECORD_PAYMENT_CONDITIONS,
            RECORD_TYPES,
            REQUEST_FCS_ACTIONS,
            REQUEST_STATUS,
            REQUEST_TYPES,
            REQUEST_TYPE_KEYS,
            SALES_CONTRACT_STATUS,
            SALES_CONTRACT_TYPES,
            SALES_CREDIT_NOTE_TYPES,
            SALES_ORDER_TYPES,
            SALUTATIONS,
            SHIPPING_COMPANIES,
            SHIPPING_COMPANY_KEYS,
            STOCK_KEYS,
            STOCK_NAMES,
            SUPPLIER_TYPES,
            SUPPLIERS,
            SYSTEM_COMPANIES,
            SYSTEM_COMPANY_KEYS,
            TIME_RECORD_MARKER_TYPES,
            TIME_RECORD_STATUS,
            TIME_RECORD_TYPES,
            TIME_RECORDING_CONFIGS,
            TITLES,
            USERS,
            USER_LOGINS
    };
}
