SET search_path = osserp, pg_catalog;

UPDATE system_menu_headers SET permissions = 'ldap_admin,ldap_password,role_configs_admin,organisation_admin,hrm,client_create,executive_saas,runtime_config' WHERE id = 21;

UPDATE system_properties SET pvalue = '0.16.3', ptype = 'java.lang.String' WHERE name = 'dbmsSchemaVersion';
