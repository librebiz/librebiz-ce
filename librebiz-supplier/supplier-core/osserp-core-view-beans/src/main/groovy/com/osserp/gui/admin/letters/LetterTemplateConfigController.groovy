/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 26, 2008 12:37:28 AM 
 * 
 */
package com.osserp.gui.admin.letters

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.gui.letters.AbstractLetterController

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@RequestMapping("/admin/letters/letterTemplateConfig/*")
@Controller class LetterTemplateConfigController extends AbstractLetterController {
    private static Logger logger = LoggerFactory.getLogger(LetterTemplateConfigController.class.getName())

    @RequestMapping
    def selectType(HttpServletRequest request) {
        logger.debug("selectType: invoked [user=${getUserId(request)}]")
        LetterTemplateConfigView view = getView(request)
        view.selectType()
        defaultPage
    }

    @RequestMapping
    def delete(HttpServletRequest request) {
        logger.debug("delete: invoked [user=${getUserId(request)}]")
        LetterTemplateConfigView view = getView(request)
        view.delete()
        defaultPage
    }

    @Override
    @RequestMapping
    def exit(HttpServletRequest request) {
        LetterTemplateConfigView view = getView(request)
        String exitTarget = view.exitTarget
        if (view.exitTarget) {
            removeView(request, view)
            return exitTarget
        }
        redirect(request, '/admin')
    }

    @Override
    @RequestMapping
    def select(HttpServletRequest request) {
        LetterTemplateConfigView view = getView(request)
        if (view.externalInvocationMode) {
            return exit(request)
        }
        super.select(request)
    }

    @RequestMapping
    def print(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("print: invoked [user=${getUserId(request)}]")
        LetterTemplateConfigView view = getView(request)
        try {
            savePdf(request, view.getPdf())
        } catch (Exception e) {
            saveError(request, e.message)
            return currentPage
        }
        renderPdf(request, response)
    }
}
