/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Oct 20, 2007 10:45:32 PM 
 * 
 */
package com.osserp.common.web.tags;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.util.StringUtil;

/**
 * 
 * Tag renders HTML-Form element calling an ajaxForm-Function as specified below for a set of event handlers
 * 
 * function ajaxForm(requestUrl, targetElement, targetIsPopup, form, activityElement, activityMessage, minChars)
 * 
 * 
 * @author cf <cf@osserp.com>
 * 
 */
public class AjaxLookupFormTag extends AbstractAjaxElementTag {
    private static Logger log = LoggerFactory.getLogger(AjaxLookupFormTag.class.getName());
    private String name = null;
    private String events = null;
    private int minChars = 2;
    private String action = null;

    @Override
    public int doStartTag() throws JspException {
        if (events == null || events == "") {
            this.events = "onkeyup";
        }
        String[] eventList = StringUtil.getTokenArray(events);
        try {
            if (action == null || action == "") {
                action = "javascript:;";
            } else if (action != "javascript:;") {
                action = getContextPath() + action;
            }

            String functionCall = createFunctionCall();

            StringBuilder buffer = new StringBuilder();
            buffer
                    .append("<form")
                    .append(" name=\"").append(name).append("\"")
                    .append(" id=\"").append(name).append("\"")
                    .append(" method=\"post\"")
                    .append(" action=\"").append(action).append("\"");
            for (int i = 0; i < eventList.length; i++) {
                buffer
                        .append(" ")
                        .append(eventList[i])
                        .append("=")
                        .append(functionCall);
            }
            buffer.append(">");
            buffer.append(renderCSRFInput(createUrl()));
            JspWriter out = pageContext.getOut();
            out.print(buffer.toString());
        } catch (IOException ioe) {
            log.error("doStartTag() error getting writer");
        }
        return (EVAL_BODY_INCLUDE);
    }

    private String createFunctionCall() {
        StringBuilder buffer = new StringBuilder(64);
        buffer.append("\"");
        if (getPreRequest() != null) {
            buffer.append(getPreRequest());
        }
        buffer
                .append(" ojsAjax.ajaxForm('")
                .append(createUrl())
                .append("', '")
                .append(getTargetElement())
                .append("', ");
        if (isPopup()) {
            buffer.append("true");
        } else {
            buffer.append("false");
        }
        buffer
                .append(", this, '")
                .append(getActivityElement())
                .append("', '")
                .append(getResourceString("waitPleaseWithDots"))
                .append("', ")
                .append(minChars)
                .append(", ")
                .append(getOnSuccess())
                .append("); ");
        if (getPostRequest() != null) {
            buffer.append(getPostRequest());
        }
        buffer.append("\"");
        return buffer.toString();
    }

    @Override
    public int doEndTag() throws JspException {
        try {
            JspWriter out = pageContext.getOut();
            out.print("</form>");
        } catch (IOException ioe) {
            log.error("doEndTag() error getting writer");
        }
        return (EVAL_BODY_INCLUDE);
    }

    /**
     * @return returns the Name of the form.
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The Name of the form to set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return returns the minimal amount of characters.
     */
    public int getMinChars() {
        return minChars;
    }

    /**
     * @param minChars The minimal amount of characters to set.
     */
    public void setMinChars(int minChars) {
        this.minChars = minChars;
    }

    /**
     * @return returns the list of events.
     */
    public String getEvents() {
        return events;
    }

    /**
     * @param events The list of events to set.
     */
    public void setEvents(String events) {
        this.events = events;
    }

    /**
     * @return action The form action that is called when "submitting" the form.
     */
    public String getAction() {
        return action;
    }

    /**
     * @param action The form action that is called when "submitting" the form.
     */
    public void setAction(String action) {
        this.action = action;
    }

    @Override
    public String getTargetElement() {
        String te = super.getTargetElement();
        return (te != null && te != "") ? te : getName() + "_popup";
    }

}
