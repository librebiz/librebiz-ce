/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 29, 2009 1:31:10 PM 
 * 
 */
package com.osserp.core.model.records;

import java.util.ArrayList;
import java.util.List;

import com.osserp.common.beans.AbstractClass;
import com.osserp.common.util.CollectionUtil;
import com.osserp.core.Comparators;
import com.osserp.core.finance.Order;
import com.osserp.core.sales.SalesOrderVolumeExportConfig;
import com.osserp.core.sales.SalesOrderVolumeExportOperation;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesOrderVolumeExportOperationVO extends AbstractClass implements SalesOrderVolumeExportOperation {

    private SalesOrderVolumeExportConfig config = null;
    private int exportableOrderCount;
    private int invoiceArchiveCount;
    private int unreleasedInvoiceCount;
    private int unreleasedOrderCount;

    private List<Order> availableOrders = new ArrayList<Order>();

    /**
     * Default constructor required by Serializable
     */
    protected SalesOrderVolumeExportOperationVO() {
        super();
    }

    /**
     * Creates a new volume export operation.
     * @param config
     */
    public SalesOrderVolumeExportOperationVO(SalesOrderVolumeExportConfig config) {
        super();
        this.config = config;
    }

    public SalesOrderVolumeExportConfig getConfig() {
        return config;
    }

    public int getAvailableOrderCount() {
        return availableOrders.size();
    }

    public int getExportableOrderCount() {
        return exportableOrderCount;
    }

    public int getInvoiceArchiveCount() {
        return invoiceArchiveCount;
    }

    public void setInvoiceArchiveCount(int invoiceArchiveCount) {
        this.invoiceArchiveCount = invoiceArchiveCount;
    }

    public int getUnreleasedInvoiceCount() {
        return unreleasedInvoiceCount;
    }

    public void setUnreleasedInvoiceCount(int unreleasedInvoiceCount) {
        this.unreleasedInvoiceCount = unreleasedInvoiceCount;
    }

    public int getUnreleasedOrderCount() {
        return unreleasedOrderCount;
    }

    public void setUnreleasedOrderCount(int unreleasedOrderCount) {
        this.unreleasedOrderCount = unreleasedOrderCount;
    }

    public List<Order> getAvailableOrders() {
        return CollectionUtil.sort(availableOrders, Comparators.createEntityByCreatedComparator(true));
    }

    public void populate(
            List<Order> available,
            int invoiceArchiveCount,
            int unreleasedInvoiceCount,
            int unreleasedOrderCount) {

        if (config == null) {
            throw new IllegalStateException("config must not be null");
        }
        this.unreleasedOrderCount = unreleasedOrderCount;
        this.invoiceArchiveCount = invoiceArchiveCount;
        this.unreleasedInvoiceCount = unreleasedInvoiceCount;
        this.availableOrders = available;
        this.exportableOrderCount = availableOrders.size();
    }
}
