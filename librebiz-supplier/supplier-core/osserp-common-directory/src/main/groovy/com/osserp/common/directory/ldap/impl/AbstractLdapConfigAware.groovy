/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.directory.ldap.impl

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.beans.CommonClass
import com.osserp.common.directory.ldap.LdapConfig
import com.osserp.common.directory.ldap.LdapUtils
import com.osserp.common.directory.ldap.Schema

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class AbstractLdapConfigAware extends CommonClass {
    private static Logger logger = LoggerFactory.getLogger(AbstractLdapConfigAware.class.getName())

    LdapConfig config

    protected AbstractLdapConfigAware() {
        // nothing to do
    }

    /**
     * Initializes a new ldap dependent object  
     * @param configObj
     */
    protected AbstractLdapConfigAware(LdapConfig configObj) {
        config = configObj
    }

    /**
     * Adds the object classes of provided schema to objectclass property of ldapObject
     * @param schema with objectClasses to add
     * @param ldapObject where to add the object classes
     * @return modified ldapObject
     */
    protected final Map addObjectClasses(Schema schema, Map ldapObject) {
        LdapUtils.addObjectClasses(schema, ldapObject)
    }

    /**
     * Adds the object classes of provided schema to objectclass property of ldapObject
     * @param schema with objectClasses to add
     * @param ldapObject where to add the object classes
     * @return modified ldapObject
     */
    protected final Map updateObjectClasses(Schema schema, Map values, Map ldapObject) {
        LdapUtils.updateObjectClasses(schema, values, ldapObject)
    }

    /**
     * Checks if a schema is supported by the objectclass property of a value map.
     * The schema should be a subschema with objectclasses unique for that schema. 	
     * @param schema to check for. 
     * @param values with objectclass property (e.g. ldapObject)
     * @return true if any schema of schema.objectClasses was found in values.objectclass 
     */
    protected final boolean schemaActivated(Schema schema, Map values) {
        LdapUtils.schemaActivated(schema, values)
    }

    /**
     * Removes all keys with null values
     * @param values
     * @return map without null values
     */
    protected final Map clearNull(Map values) {
        LdapUtils.clearNull(values)
    }

    /**
     * Fetches the client name from dn if exists.
     * @param dn
     * @return client name or empty string
     */
    protected String fetchClientName(String dn) {
        LdapUtils.fetchClientName(dn)
    }
}
