/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.dao;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class TableKeys {

    public static final String BANK_ACCOUNTS = "bankAccounts";
    public static final String BILLING_TYPE_FCS_RELATIONS = "billingTypeFcsRelations";
    public static final String BILLING_TYPES = "billingTypes";
    public static final String CALCULATION_CONFIGS = "calculationConfigs";
    public static final String CALCULATION_ITEMS = "calculationItems";
    public static final String CALCULATION_OPTION_POSITIONS = "calculationOptionPositions";
    public static final String CALCULATION_POSITIONS = "calculationPositions";
    public static final String CALCULATION_GROUPS = "calculationGroups";
    public static final String CALCULATION_TEMPLATES = "calculationTemplates";
    public static final String CALCULATIONS = "calculations";
    public static final String CAMPAIGN_ASSIGNMENT_HISTORY = "campaignAssignmentHistory";
    public static final String CONTACTS = "contacts";
    public static final String CONTACT_ZIPCODES = "contactZipcodes";
    public static final String CONTACT_MAILING_LIST_MEMBERS = "contactMailingListMembers";
    public static final String COUNTRIES = "countries";
    public static final String CUSTOMER_SEQUENCE = "customerSequence";
    public static final String CUSTOMERS = "customers";
    public static final String CUSTOMERS_VIEW = "customersView";
    public static final String DELIVERY_MONITORING_VIEW = "deliveryMonitoringView";
    public static final String EMAILS = "emails";
    public static final String EMPLOYEE_CONTACTS = "employeeContacts";
    public static final String EMPLOYEE_GROUPS = "employeeGroups";
    public static final String EMPLOYEE_GROUP_PERMISSIONS = "employeeGroupPermissions";
    public static final String EMPLOYEE_ROLES = "employeeRoles";
    public static final String EMPLOYEE_ROLE_CONFIGS = "employeeRoleConfigs";
    public static final String EMPLOYEE_ROLE_DISPLAY = "employeeRoleDisplay";
    public static final String EMPLOYEES = "employees";
    public static final String EVENTS = "events";
    public static final String EVENTS_HISTORY = "eventsHistory";
    public static final String EVENT_ACTIONS = "eventConfigs";
    public static final String EVENT_ACTIONS_SEQUENCE = "eventConfigsSequence";
    public static final String EVENT_TERMINATIONS = "eventTerminations";
    public static final String EVENT_TYPES = "eventTypes";
    public static final String FLOW_CONTROLS = "flowControls";
    public static final String FLOW_CONTROL_ACTIONS = "flowControlActions";
    public static final String FLOW_CONTROL_WASTEBASKETS = "flowControlWastebaskets";
    public static final String FLOW_CONTROL_WASTEBASKET_DEFAULTS = "flowControlWastebasketDefaults";
    public static final String INVOICES = "invoices";
    public static final String MAIL_DOMAINS = "mailDomains";
    public static final String OPEN_SERVICE_SALES = "openServiceSales";
    public static final String PARTLISTS = "partlists";
    public static final String PARTLIST_ITEMS = "partlistItems";
    public static final String PARTLIST_POSITIONS = "partlistPositions";
    public static final String PAYMENTS = "payments";
    public static final String PERMISSIONS = "permissions";
    public static final String PLANS = "plans";
    public static final String PHONES = "phones";
    public static final String PRODUCTS = "products";
    public static final String PRODUCT_CLASSIFICATION_CONFIGS = "productClassificationConfigs";
    public static final String PRODUCT_DESCRIPTIONS = "productDescriptions";
    public static final String PRODUCT_GROUPS = "productGroups";
    public static final String PRODUCT_TYPE_RELATIONS = "productTypeRelations";
    public static final String PRODUCT_PLANNING_TASKS = "productPlanningTasks";
    public static final String PRODUCT_PLANNING_TASKS_HISTORY = "productPlanningTasksHistory";
    public static final String PRODUCT_SELECTION_CONFIGS = "productSelectionConfigs";
    public static final String PRODUCT_SELECTION_CONFIG_ITEMS = "productSelectionConfigItems";
    public static final String PROJECTS = "projects";
    public static final String PROJECT_FCS = "projectFcs";
    public static final String PROJECT_FCS_ACTIONS = "projectFcsActions";
    public static final String PROJECT_INSTALLATION_DATES = "projectInstallationDates";
    public static final String PROJECT_INSTALLATIONS_DISPLAY = "projectInstallationsDisplay";
    public static final String PROJECT_MOUNTINGS_VIEW = "projectMountingsView";
    public static final String PROJECT_SUPPLIERS = "projectSuppliers";
    public static final String PURCHASE_DELIVERY_NOTES = "purchaseDeliveryNotes";
    public static final String PURCHASE_DELIVERY_NOTE_ITEMS = "purchaseDeliveryNoteItems";
    public static final String PURCHASE_DELIVERY_NOTE_SERIALS = "purchaseDeliveryNoteSerials";
    public static final String PURCHASE_INVOICE_CANCELLATIONS = "purchaseInvoiceCancellations";
    public static final String PURCHASE_INVOICE_CANCELLATION_ITEMS = "purchaseInvoiceCancellationItems";
    public static final String PURCHASE_INVOICES = "purchaseInvoices";
    public static final String PURCHASE_INVOICE_ITEMS = "purchaseInvoiceItems";
    public static final String PURCHASE_OFFERS = "purchaseOffers";
    public static final String PURCHASE_OFFER_ITEMS = "purchaseOfferItems";
    public static final String PURCHASE_ORDERS = "purchaseOrders";
    public static final String PURCHASE_ORDER_ITEMS = "purchaseOrderItems";
    public static final String PURCHASE_ORDER_QUERY_VIEW = "purchaseOrderQueryView";
    public static final String RECORD_DOCUMENT_VERSION_SEQ = "recordDocumentVersionSequence";
    public static final String RECORD_EXPORTS = "recordExports";
    public static final String RECORD_EXPORT_ITEMS = "recordExportItems";
    public static final String RECORD_EXPORT_SUMMARY = "recordExportSummary";
    public static final String RECORD_INFO_CONFIG_DEFAULTS = "recordInfoConfigDefaults";
    public static final String RECORD_PAYMENT_TARGET_DEFAULTS = "recordPaymentTargetDefaults";
    public static final String RECORD_PAYMENT_CONDITION_DEFAULTS = "recordPaymentConditionDefaults";
    public static final String RECORD_PERCENTAGES = "recordPercentages";
    public static final String RECORD_SEQUENCES = "recordSequences";
    public static final String RECORD_STATUS_INFOS = "recordStatusInfos";
    public static final String RECORD_STATUS_INFO_VIEW = "recordStatusInfoView";
    public static final String REQUESTS = "requests";
    public static final String REQUEST_LIST = "requestList";
    public static final String REQUEST_TYPES = "requestTypes";
    public static final String REQUEST_SEARCH_VIEW = "requestSearchView";
    public static final String SALES_CANCELLATIONS = "salesCancellations";
    public static final String SALES_CREDIT_NOTES = "salesCreditNotes";
    public static final String SALES_DELIVERY_LIST = "salesDeliveryList";
    public static final String SALES_DELIVERY_NOTE_SERIALS = "salesDeliveryNoteSerials";
    public static final String SALES_DELIVERY_NOTES = "salesDeliveryNotes";
    public static final String SALES_DELIVERY_NOTE_ITEMS = "salesDeliveryNoteItems";
    public static final String SALES_INVOICES = "salesInvoices";
    public static final String SALES_INVOICE_CORRECTIONS = "salesInvoiceCorrections";
    public static final String SALES_INVOICE_INFOS = "salesInvoiceInfos";
    public static final String SALES_INVOICE_ITEMS = "salesInvoiceItems";
    public static final String SALES_LIST_VIEW = "salesListView";
    public static final String SALES_MONITORING_VIEW = "salesMonitoringView";
    public static final String SALES_MONITORING_CACHE_TASKS = "salesMonitoringCacheTasks";
    public static final String SALES_OFFERS = "salesOffers";
    public static final String SALES_OFFER_ITEMS = "salesOfferItems";
    public static final String SALES_OFFER_CALCULATIONS = "salesOfferCalculations";
    public static final String SALES_ORDERS = "salesOrders";
    public static final String SALES_ORDER_DOWNPAYMENTS = "salesDownpayments";
    public static final String SALES_ORDER_ITEMS = "salesOrderItems";
    public static final String SALES_ORDER_QUERY_VIEW = "salesOrderQueryView";
    public static final String SALES_ORDER_VOLUME_EXPORT_DELIVERIES = "salesOrderVolumeExportDeliveries";
    public static final String SALES_ORDER_VOLUME_EXPORT_ITEMS = "salesOrderVolumeExportItems";
    public static final String SALES_ORDER_VOLUME_EXPORTS = "salesOrderVolumeExports";
    public static final String SALES_PAYMENTS = "salesPayments";
    public static final String SALES_RECEIVABLES_VIEW = "salesReceivablesView";
    public static final String SALES_RECORD_IDS_VIEW = "salesRecordIdsView";
    public static final String SALES_SEARCH_VIEW = "salesSearchView";
    public static final String SALES_VOLUME_REPORT_TASKS = "salesVolumeReportTasks";
    public static final String SALES_WITH_MISSING_INVOICE = "salesWithMissingInvoice";
    public static final String SALUTATIONS = "salutations";
    public static final String SELECTION_ITEMS = "selectionItems";
    public static final String SERIAL_NUMBER_VIEW = "serialNumberView";
    public static final String SFA_PHONE_BOOK_ITEMS = "sfaPhoneBookItems";
    public static final String STOCK_REPORT = "stockReport";
    public static final String STOCKTAKING_PRODUCTS_VIEW = "stocktakingProductsView";
    public static final String STOCKTAKINGS = "stocktakings";
    public static final String SUPPLIER_PRODUCT_GROUPS = "supplierProductGroups";
    public static final String SUPPLIER_SEQUENCE = "supplierSequence";
    public static final String SUPPLIERS = "suppliers";
    public static final String SYSTEM_CACHE_STATUS = "systemCacheStatus";
    public static final String SYSTEM_COMPANIES = "systemCompanies";
    public static final String SYSTEM_COMPANY_BRANCHS = "systemCompanyBranchs";
    public static final String SYSTEM_COMPANY_GROUPS = "systemCompanyGroups";
    public static final String SYSTEM_SERIALS = "systemSerials";
    public static final String SYSTEM_TELEPHONE_TELEPHONE_CONFIGURATIONS = "systemTelephoneTelephoneConfigurations";
    public static final String TAX_RATES = "taxRates";
    public static final String TAX_REPORT_ITEMS = "taxReportItems";
    public static final String TAX_REPORTS = "taxReports";
    public static final String USER_PERMISSIONS = "permissionsUser";
    public static final String USERS = "users";
    public static final String USERS_PERMISSIONS_VIEW = "usersPermissionsView";
    public static final String WORKFLOWS = "workflows";
    public static final String ZIPCODE_SEARCHES = "zipcodeSearches";
}
