/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 1, 2009 2:13:06 PM 
 * 
 */
package com.osserp.gui.client.products;

import java.util.List;

import com.osserp.common.web.View;

import com.osserp.core.products.ProductCategoryConfig;
import com.osserp.core.products.ProductClassificationConfig;
import com.osserp.core.products.ProductClassificationEntity;
import com.osserp.core.products.ProductGroupConfig;
import com.osserp.core.products.ProductTypeConfig;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ProductClassificationAwareView extends View {

    /**
     * Indicates that eol classifications should be displayed
     * @return includeEol
     */
    public boolean isIncludeEol();

    /**
     * Enables/disables eol include flag
     * @param includeEol
     */
    public void setIncludeEol(boolean includeEol);

    /**
     * Indicates that view provides eol selection
     * @return includeEolAvailable
     */
    public boolean isIncludeEolAvailable();

    /**
     * Provides available types
     * @return types
     */
    public List<ProductClassificationEntity> getTypeSelections();

    /**
     * Selects/deselects a type
     * @param id or null to reset a previous selection
     */
    public void selectType(Long id);

    /**
     * Provides current selected type
     * @return selectedType or null if none selected
     */
    public ProductTypeConfig getSelectedType();

    /**
     * Selects/deselects a type
     * @param id or null to reset a previous selection
     */
    public void selectGroup(Long id);

    /**
     * Provides current selected group
     * @return selectedGroup or null if none selected
     */
    public ProductGroupConfig getSelectedGroup();

    /**
     * Selects a category from current selected group
     * @param id
     */
    public void selectCategory(Long id);

    /**
     * Provides current selected category
     * @return selectedCategory
     */
    public ProductCategoryConfig getSelectedCategory();

    /**
     * Provides selected classification
     * @return selectedConfig
     */
    public ProductClassificationConfig getSelectedConfig();

}
