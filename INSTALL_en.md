
# Build & Installation

This guide covers all steps required to build and install the software
using Debian GNU/Linux 10 (Buster).

See [documentation](https://librebiz.org/manuals) for more detailed
instructions and other OS.

## Building

Become root and install all packages required to build the software:

```
~$ sudo su - root
~# apt update && apt upgrade
~# apt install default-jdk-headless maven dpkg-dev git postfix
```

The build script should always run as a non-privileged user:

```
~# adduser librebiz-dev
~# su - librebiz-dev
```

Checkout the source, prepare build environment and build the packages:

```
~$ git clone https://codeberg.org/librebiz/librebiz-ce.git
~$ cd librebiz-ce
~$ ./build all
~$ exit
```

Running the build script creates the directory `./dist/debian` containing the
.deb files along with their corresponding .changes and Packages.bz2 file.

## Deployment 

The installation must run as root.

```
~# echo "deb [ trusted=yes ] file:/home/librebiz-dev/librebiz-ce/dist/debian ./" > /etc/apt/sources.list.d/librebiz.list
~# apt update
~# apt install librebiz
```

## Configuration


#### ActiveMQ Setup

```
~# systemctl stop activemq
~# ln -s /etc/activemq/instances-available/main /etc/activemq/instances-enabled/main
~# systemctl start activemq
```

#### PostgreSQL Setup

```
~# su - postgres
~$ createuser --encrypted --pwprompt osserp
~$ createdb --owner=osserp --template=template0 --encoding=UTF8 librebiz_main
~$ exit
```

Open the file `/etc/librebiz/conf/custom.properties` and change the 
database password for both `dms.database.pwd` and `root.database.pwd` 
properties.

Initialize the database running the admin script (as root)

````
~# librebiz-admin update
```` 

#### Apache Webserver (optional)

```
~# apt-get install apache2
~# a2enmod ssl
~# a2ensite default-ssl
~# a2enmod proxy_http
~# cp /usr/share/doc/librebiz-common/examples/httpd-tomcat.conf /etc/apache2/conf-available/librebiz.conf
~# a2enconf librebiz
~# systemctl restart apache2
```

#### Start Service

```
~# librebiz-admin enable
~# systemctl restart librebiz
```

Point your browser to `https://yourhost.example.com/osdb/` for secure access.

----

## Updating

To update the software, pull latest changes from repository, run build
command and reinstall the packages using librebiz-admin command:

```
~# su - librebiz-dev
~$ cd librebiz-ce
~$ git pull
~$ ./build fresh
~$ exit
~# librebiz-admin reinstall
```

----


## Debugging

To change the logging-level for specific packages, create a custom log4j2
config and enable reading of this file on application startup:

* Open the file `/etc/default/librebiz` and remove the `#` from the line
starting with `#LOG4J_OPTS`. Save the file.
* Copy default logging config to `/etc/librebiz/conf`:

```
~# cp /var/lib/librebiz/webapps/osdb/WEB-INF/classes/log4j2.xml /etc/librebiz/conf/
```

* Open `/etc/librebiz/conf/log4j2.xml` to add, remove and change packages.
* Restart application after changing:

```
systemctl restart librebiz
```
