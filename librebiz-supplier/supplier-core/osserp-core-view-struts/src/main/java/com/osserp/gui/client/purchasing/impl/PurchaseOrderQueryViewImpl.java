/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 2, 2008 4:35:10 PM 
 * 
 */
package com.osserp.gui.client.purchasing.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.util.CollectionUtil;
import com.osserp.common.web.ViewName;

import com.osserp.core.employees.Employee;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.finance.Records;
import com.osserp.core.products.ProductSelection;
import com.osserp.core.products.ProductSelectionConfigItem;
import com.osserp.core.products.ProductSelectionManager;
import com.osserp.core.purchasing.PurchaseOrderManager;
import com.osserp.core.suppliers.Supplier;
import com.osserp.core.suppliers.SupplierSearch;

import com.osserp.gui.client.impl.AbstractView;
import com.osserp.gui.client.purchasing.PurchaseOrderQueryView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("purchaseOrderQueryView")
public class PurchaseOrderQueryViewImpl extends AbstractView implements PurchaseOrderQueryView {
    private static Logger log = LoggerFactory.getLogger(PurchaseOrderQueryViewImpl.class.getName());

    private List<RecordDisplay> records = new ArrayList<RecordDisplay>();

    private boolean displayConfirmedMode = true;
    private boolean displayAllMode = true;
    private boolean displayOverdueOnlyMode = false;
    private boolean userIsPurchaser = false;
    private Supplier selectedSupplier = null;
    private ProductSelection config = null;
    private boolean sortByDescendent = false;
    private String sortBy;
    private List<ProductSelectionConfigItem> productSelections = new ArrayList<ProductSelectionConfigItem>();
    private boolean initialInvocation = true;

    protected PurchaseOrderQueryViewImpl() {
        super();
        setReloadWhenExistsOnCreate(true);
    }

    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        super.init(request);
        sortBy = getSystemProperty("purchaseOrderQueryOrderByColumn");
        if (sortBy == null) {
            sortBy = "created";
            sortByDescendent = true;
        } else {
            sortByDescendent = isSystemPropertyEnabled("purchaseOrderQueryOrderByDescendent");
        }
        userIsPurchaser = getDomainUser().isPurchaser();
        if (userIsPurchaser && !isUserPropertyEnabled("displayAllPurchasersOrders")) {
            displayAllMode = false;
        }
        // TODO replace with better solution (code duplication! See other Purchase*ViewImpl)  
        ProductSelectionManager manager = (ProductSelectionManager) getService(ProductSelectionManager.class.getName());
        config = manager.getSelection(getDomainEmployee(), true);
        if (config != null) {
            productSelections = config.getItems();
            if (log.isDebugEnabled()) {
                log.debug("init() config found [id=" + config.getId() + "]");
            }
        }
        refresh();
    }

    public boolean isDisplayConfirmedMode() {
        return displayConfirmedMode;
    }

    public void toggleDisplayAllMode() {
        displayAllMode = !displayAllMode;
        refresh();
    }

    public boolean isDisplayAllMode() {
        return displayAllMode;
    }

    public void toggleDisplayConfirmedMode() {
        displayConfirmedMode = !displayConfirmedMode;
        refresh();
    }

    public boolean isDisplayOverdueOnlyMode() {
        return displayOverdueOnlyMode;
    }

    public void toggleDisplayOverdueOnlyMode() {
        displayOverdueOnlyMode = !displayOverdueOnlyMode;
        refresh();
    }

    public Supplier getSelectedSupplier() {
        return selectedSupplier;
    }

    protected void setSelectedSupplier(Supplier selectedSupplier) {
        this.selectedSupplier = selectedSupplier;
    }

    public void selectSupplier(Long id) {
        if (id == null) {
            selectedSupplier = null;
        } else {
            SupplierSearch search = (SupplierSearch) getService(SupplierSearch.class.getName());
            try {
                selectedSupplier = search.findById(id);
            } catch (ClientException e) {
                log.warn("selectSupplier() ignoring failure on attempt to fetch supplier [id=" + id
                        + ", message=" + e.getMessage() + "]");
                selectedSupplier = null;
            }
        }
    }

    public boolean isUserIsPurchaser() {
        return userIsPurchaser;
    }

    public void refresh() {
        records = fetchRecordsFromBackend();
        if (log.isDebugEnabled()) {
            log.debug("refresh() invoked [displayAll=" + displayAllMode
                    + ", displayOverdueOnly=" + displayOverdueOnlyMode
                    + ", displayConfirmed=" + displayConfirmedMode
                    + ", purchaser=" + userIsPurchaser
                    + ", fetchedRecordCount=" + records.size()
                    + "]");
        }
        if (initialInvocation && records.isEmpty() && displayConfirmedMode) {
            displayConfirmedMode = false;
            records = fetchRecordsFromBackend();
            initialInvocation = false;
        }
        List<RecordDisplay> displayable = new ArrayList<RecordDisplay>();
        if (displayAllMode || !userIsPurchaser) {
            if (log.isDebugEnabled()) {
                log.debug("refresh() adding all records...");
            }
            displayable.addAll(records);
        } else {
            if (log.isDebugEnabled()) {
                log.debug("refresh() filtering by purchaser...");
            }
            Employee purchaser = getDomainEmployee();
            for (int i = 0, j = records.size(); i < j; i++) {
                RecordDisplay next = records.get(i);
                if (purchaser.getId().equals(next.getCreatedBy())) {
                    displayable.add(next);
                }
            }
            if (displayable.isEmpty()) {
                if (log.isDebugEnabled()) {
                    log.debug("refresh() filter result was empty, adding all...");
                }
                displayable.addAll(records);
            }
        }
        if (displayOverdueOnlyMode && !displayable.isEmpty()) {
            if (log.isDebugEnabled()) {
                log.debug("refresh() filtering by overdue only...");
            }
            for (Iterator<RecordDisplay> i = displayable.iterator(); i.hasNext();) {
                RecordDisplay next = i.next();
                if (!next.isOverdue()) {
                    i.remove();
                }
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("refresh() done [resultingRecordCount=" + displayable.size() + "]");
        }
        if (config != null && config.isIgnoreSelections()) {
            setList(CollectionUtil.sort(displayable, sortBy, sortByDescendent));
        } else {
            setList(CollectionUtil.sort(Records.filterByRecordDisplay(productSelections, displayable), sortBy, sortByDescendent));
        }
    }

    protected void setDisplayConfirmedMode(boolean displayConfirmed) {
        this.displayConfirmedMode = displayConfirmed;
    }

    protected void setDisplayAllMode(boolean displayAllMode) {
        this.displayAllMode = displayAllMode;
    }

    private List<RecordDisplay> fetchRecordsFromBackend() {
        PurchaseOrderManager manager = getOrderManager();
        return manager.findOpenByConfirmation(displayConfirmedMode);
    }

    private PurchaseOrderManager getOrderManager() {
        return (PurchaseOrderManager) getService(PurchaseOrderManager.class.getName());
    }
}
