/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 10-Aug-2006 19:12:47 
 * 
 */
package com.osserp.core.finance;

import java.util.Date;
import java.util.List;

import org.jdom2.Document;

import com.osserp.common.ClientException;
import com.osserp.common.dms.DocumentData;
import com.osserp.core.employees.Employee;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface RecordExportManager {

    /**
     * Provides exportable record types and counters
     * @return exportable types
     */
    List<RecordExportType> getExportableTypes();

    /**
     * Provides all available exports of given type
     * @param recordType
     * @return exports
     */
    List<RecordExport> findByType(Long recordType);

    /**
     * Provides a list of all available exports.
     * @param recordType
     * @return list of available exports without records
     */
    List<RecordExport> listByType(Long recordType);

    /**
     * Adds all records to a given export
     * @param export
     */
    void addRecords(RecordExport export);

    /**
     * Creates a new export
     * @param recordType to export
     * @param employeeId creating the export
     * @return number of exported records
     */
    int createExport(Long recordType, Long employeeId);

    /**
     * Creates a new export for given record type by preselected records
     * @param recordType
     * @param employeeId
     * @param records
     * @return count of records exported or 0 if nothing to do
     */
    int createExport(Long recordType, Long employeeId, List<RecordDisplay> records);

    /**
     * Creates a new export by all unexported between start and end date.
     * @param recordType type to export
     * @param employeeId id of the exporting user
     * @param start date
     * @param end date excluded
     * @return count of records exported or 0 if nothing to do
     */
    int createExport(Long recordType, Long employeeId, Date start, Date end);

    /**
     * Provides the latest export of given type
     * @param recordType
     * @return latest export or null if none exists
     */
    RecordExport getLatest(Long recordType);

    /**
     * Provides the count of exportable records
     * @param recordType
     * @return exportable count
     */
    Integer getCount(Long recordType);

    /**
     * Provides all unexported records of a type
     * @param type
     * @param reverse
     * @return unexported records
     */
    List<RecordDisplay> getRecords(Long type, boolean reverse);

    /**
     * Creates an xml representation of given record export
     * @param employee requesting the document
     * @param export with records
     * @return document
     */
    Document createXml(Employee employee, RecordExport export) throws ClientException;

    /**
     * Creates a sheet representation of an export 
     * @param employee
     * @param export
     * @return sheet as string with rows of columns separated by tabs
     */
    String createXls(Employee employee, RecordExport export);

    /**
     * Creates a pdf representation of given record export
     * @param employee requesting the document
     * @param export with records
     * @return pdf
     */
    byte[] createPdf(Employee employee, RecordExport export) throws ClientException;

    /**
     * Creates a zipped archive containing all pdf documents.
     * @param export with vouchers to zip
     * @return zip archive
     */
    DocumentData createZip(RecordExport recordExport) throws ClientException;

}
