<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>

<v:view viewName="productPlanningView"/>
<c:set var="url" value="/plannings/productPlanning"/>

<c:if test="${!empty view}">
	<c:set var="bean" scope="page" value="${view.bean}"/>

	<div class="modalBoxHeader">
		<div class="modalBoxHeaderLeft">
			<fmt:message key="${view.headerName}"/>
			<c:choose>
				<c:when test="${view.setupMode}">
					: <fmt:message key="settings"/>
				</c:when>
				<c:when test="${view.product != null}">
					: <o:out value="${view.product.name}"/>
				</c:when>
			</c:choose>
		</div>
		<div class="modalBoxHeaderRight">
			<div class="boxnav">
				<ul>
					<c:forEach var="nav" items="${view.navigation}">
						<li>
							<v:ajaxLink url="${nav.link}" targetElement="${view.name}_popup" title="${nav.title}"><o:img name="${nav.icon}"/></v:ajaxLink>
						</li>
					</c:forEach>
				</ul>
			</div>
		</div>
	</div>
	<div class="modalBoxData">
		<div>
			<div class="subcolumns">
				<div class="subcolumn">
					<c:if test="${!empty sessionScope.error}">
						<div class="spacer"></div>
						<div class="errormessage">
							<fmt:message key="error"/>: <fmt:message key="${sessionScope.error}"/>
						</div>
                        <o:removeErrors/>
					</c:if>
					<div class="spacer"></div>
					<c:choose>
						<c:when test="${view.setupMode}">
							<table class="valueTable input table-striped">
								<tbody>
									<tr>
										<td>
											<fmt:message key="stock"/>
										</td>
										<td>
											<o:ajaxSelect selectId="stockId" targetElement="${view.name}_popup">
												<c:forEach var="obj" items="${view.availableStocks}">
													<option value="<v:url value="${url}/selectStock"/>?id=<o:out value="${obj.id}"/>" <c:if test="${obj.id == view.selectedStock.id}">selected="selected"</c:if> ><oc:options name="systemCompanies" value="${obj.id}"/></option>
												</c:forEach>
											</o:ajaxSelect>
										</td>
									</tr>
									<tr>
										<td>
											<fmt:message key="searchFor"/>
										</td>
										<td>
											<c:choose>
												<c:when test="${view.settings.openPurchaseOnly}">
													<div><fmt:message key="displayToOrderOnly"/></div>
													<div><v:ajaxLink url="${url}/toggleOpenPurchaseOnlyFlag" targetElement="${view.name}_popup"><fmt:message key="displayAllOpenOrders"/></v:ajaxLink></div>
												</c:when>
												<c:otherwise>
													<div><fmt:message key="displayAllOpenOrders"/></div>
													<div><v:ajaxLink url="${url}/toggleOpenPurchaseOnlyFlag" targetElement="${view.name}_popup"><fmt:message key="displayToOrderOnly"/></v:ajaxLink></div>
												</c:otherwise>
											</c:choose>
										</td>
									</tr>
									<tr>
										<td>
											<fmt:message key="salesStatus"/>
										</td>
										<td>
											<c:choose>
												<c:when test="${view.vacancyDisplayMode}">
													<div><fmt:message key="displayAllBetweenZeroAndHundred"/></div>
													<div><v:ajaxLink url="${url}/toggleVacancyDisplay" targetElement="${view.name}_popup"><fmt:message key="displayConfirmedGreater15Only"/></v:ajaxLink></div>
												</c:when>
												<c:otherwise>
													<div><fmt:message key="displayConfirmedGreater15Only"/></div>
													<div><v:ajaxLink url="${url}/toggleVacancyDisplay" targetElement="${view.name}_popup"><fmt:message key="displayAllBetweenZeroAndHundred"/></v:ajaxLink></div>
												</c:otherwise>
											</c:choose>
										</td>
									</tr>
									<tr>
										<td>
											<fmt:message key="purchaseStatus"/>
										</td>
										<td>
											<c:choose>
												<c:when test="${view.settings.confirmedPurchaseOnly}">
													<div><fmt:message key="displayConfirmedOnly"/></div>
													<div><v:ajaxLink url="${url}/toggleConfirmedPurchaseOnlyFlag" targetElement="${view.name}_popup"><fmt:message key="displayAll"/></v:ajaxLink></div>
												</c:when>
												<c:otherwise>
													<div><fmt:message key="displayAll"/></div>
													<div><v:ajaxLink url="${url}/toggleConfirmedPurchaseOnlyFlag" targetElement="${view.name}_popup"><fmt:message key="displayConfirmedOnly"/></v:ajaxLink></div>
												</c:otherwise>
											</c:choose>
										</td>
									</tr>
									<tr>
										<td>
											<fmt:message key="planningUntil"/>
										</td>
										<td>
											<v:ajaxForm name="horizonForm" targetElement="${view.name}_popup" url="${url}/updateHorizon">
												<input type="text" style="width: 90px; text-align: right;" name="horizon" value="<o:date value="${view.settings.planningHorizon}"/>" id="horizon" />
												<o:datepicker input="horizon"/>
												<input type="image" name="updateHorizon" src="<o:icon name="saveIcon"/>" value="updateHorizon" />
											</v:ajaxForm>
										</td>
									</tr>
								</tbody>
							</table>
						</c:when>
						<c:when test="${view.bean != null}">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>
											<span title="<fmt:message key="productNumber"/>"><fmt:message key="product"/></span>
										</th>
										<th style="width: 100%;">
											<span title="<fmt:message key="productName"/>"><fmt:message key="name"/></span>
										</th>
										<th>
											<span title="<fmt:message key="stillToDeliverQuantity"/>"><fmt:message key="order"/></span>
										</th>
										<th>
											<span title="<fmt:message key="currentlyInInventory"/>"><fmt:message key="inventory"/></span>
										</th>
										<th>
											<span title="<fmt:message key="locatedInStockReceipt"/>"><fmt:message key="stockReceiptShort"/></span>
										</th>
										<th>
											<span title="<fmt:message key="orderedBySupplier"/>"><fmt:message key="appointedShortcut"/></span>
										</th>
									</tr>
									<tr class="row-alt">
										<td>
											<o:out value="${bean.productId}"/>
										</td>
										<td>
											<o:out value="${bean.productName}"/>
										</td>
										<td>
											<o:number value="${bean.quantity}" format="integer"/>
											<c:if test="${view.vacancyDisplayMode}">
											(<o:number value="${bean.vacant}" format="integer"/>)
											</c:if>
										</td>
										<td>
											<o:number value="${bean.stock}" format="integer"/>
										</td>
										<td>
											<o:number value="${bean.receipt}" format="integer"/>
										</td>
										<td>
											<o:number value="${bean.expected}" format="integer"/>
										</td>
									</tr>
								</thead>
							</table>
							<div class="spacer"></div>
							<div style="max-height:495px; overflow: auto;">
								<table class="table table-striped">
									<thead>
										<tr>
											<th><fmt:message key="orderNumberShort"/></th>
											<th style="width: 100%;"><fmt:message key="name"/></th>
											<th><fmt:message key="order"/></th>
											<th><fmt:message key="inventory"/></th>
											<th><fmt:message key="deliveryDate"/></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="item" items="${bean.items}" varStatus="s">
											<c:choose>
												<c:when test="${item.salesItem}">
													<c:if test="${!item.vacant or view.vacancyDisplayMode}">
														<c:choose>
															<c:when test="${view.itemSelected && item.itemId == view.selectedItem}">
																<tr class="yellow">
															</c:when>
															<c:when test="${item.vacant}">
																<tr class="red">
															</c:when>
															<c:otherwise>
																<tr>
															</c:otherwise>
														</c:choose>
														<c:choose>
															<c:when test="${view.showRecordLinks}">
																<td><v:link url="/loadSales.do?id=${item.salesId}&exit=${view.exitTarget}"><o:out value="${item.salesId}"/></v:link></td>
															</c:when>
															<c:otherwise>
																<td><o:out value="${item.salesId}"/></td>
															</c:otherwise>
														</c:choose>
														<td><o:out value="${item.salesName}"/></td>
														<td class="right"><o:number value="${item.outstanding * (-1)}" format="integer"/></td>
														<td class="right"><o:number value="${item.expectedStock}" format="integer"/></td>
														<c:choose>
															<c:when test="${view.editId != null && view.editId == item.itemId}">
																<td>
																	<v:ajaxForm name="deliveryForm" targetElement="${view.name}_popup" url="${url}/updateDelivery">
																		<input type="text" style="width: 65px; text-align: right;" name="delivery" value="<o:date value="${item.delivery}"/>" id="delivery" />
																		<o:datepicker input="delivery"/>
																		<input type="image" name="updateDelivery" src="<o:icon name="saveIcon"/>" value="updateDelivery" />
																	</v:ajaxForm>
																</td>
															</c:when>
															<c:otherwise>
																<td class="right<c:if test="${item.lazy}"> canceled</c:if>">
																	<v:ajaxLink url="${url}/editDelivery?id=${item.itemId}" targetElement="${view.name}_popup"><o:date value="${item.delivery}"/></v:ajaxLink>
																</td>
															</c:otherwise>
														</c:choose>
														</tr>
													</c:if>
												</c:when>
												<c:otherwise>
													<c:choose>
														<c:when test="${item.deliveryConfirmed}">
															<tr>
														</c:when>
														<c:otherwise>
															<tr class="red">
														</c:otherwise>
													</c:choose>
													<c:choose>
														<c:when test="${view.showRecordLinks}">
															<td><v:link url="/purchaseOrder.do?method=display&exit=product&id=${item.id}"><o:out value="${item.id}"/></v:link></td>
														</c:when>
														<c:otherwise>
															<td><o:out value="${item.id}"/></td>
														</c:otherwise>
													</c:choose>
													<td><o:out value="${item.salesName}"/></td>
													<td class="right"><o:number value="${item.outstanding}" format="integer"/></td>
													<td class="right"><o:number value="${item.expectedStock}" format="integer"/></td>
													<c:choose>
														<c:when test="${view.editId != null && view.editId == item.itemId}">
															<td>
																<v:ajaxForm name="deliveryForm" targetElement="${view.name}_popup" url="${url}/updateDelivery">
																	<input type="text" style="width: 65px; text-align: right;" name="delivery" value="<o:date value="${item.delivery}"/>" id="delivery" />
																	<o:datepicker input="delivery"/>
																	<input type="image" name="updateDelivery" src="<o:icon name="saveIcon"/>" value="updateDelivery" />
																</v:ajaxForm>
															</td>
														</c:when>
														<c:otherwise>
															<td class="right">
																<v:ajaxLink url="${url}/editDelivery?id=${item.itemId}" targetElement="${view.name}_popup"><o:date value="${item.delivery}"/></v:ajaxLink>
															</td>
														</c:otherwise>
													</c:choose>
													</tr>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</c:when>
						<c:otherwise>
							<table class="table table-striped">
								<thead>
									<tr>
										<th>
											<span title="<fmt:message key="productNumber"/>"><fmt:message key="product"/></span>
										</th>
										<th style="width: 100%;">
											<span title="<fmt:message key="productName"/>"><fmt:message key="name"/></span>
										</th>
										<th>
											<span title="<fmt:message key="stillToDeliverQuantity"/>"><fmt:message key="order"/></span>
										</th>
										<th>
											<span title="<fmt:message key="currentlyInInventory"/>"><fmt:message key="inventory"/></span>
										</th>
										<th>
											<span title="<fmt:message key="locatedInStockReceipt"/>"><fmt:message key="stockReceiptShort"/></span>
										</th>
										<th>
											<span title="<fmt:message key="orderedBySupplier"/>"><fmt:message key="appointedShortcut"/></span>
										</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td colspan="6" class="error center"><fmt:message key="productPlanning.noPlanning"/></td>
									</tr>
								</tbody>
							</table>
						</c:otherwise>
					</c:choose>
					<div class="spacer"></div>
				</div>
			</div>
		</div>
	</div>
</c:if>