/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 10-Aug-2006 19:19:11 
 * 
 */
package com.osserp.gui.client.records;

import java.util.List;

import org.jdom2.Document;

import com.osserp.common.ClientException;
import com.osserp.common.EmptyListException;
import com.osserp.common.PermissionException;
import com.osserp.common.dms.DocumentData;
import com.osserp.common.web.Form;
import com.osserp.common.web.View;
import com.osserp.common.web.ViewName;

import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.finance.RecordExport;
import com.osserp.core.finance.RecordExportType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("recordExportView")
public interface RecordExportView extends View {
    
    /**
     * Selects record type and export bean as provided by params.
     * View is in bean display mode after invoking this method.
     * @param type
     * @param id
     */
    public void load(Long type, Long id);

    /**
     * Provides the available record types for exports
     * @return recordTypes
     */
    public List<RecordExportType> getRecordTypes();

    /**
     * Indicates if an archive is available
     * @return true if recordExport count of selected type is greater than zero
     */
    public boolean isArchiveAvailable();

    /**
     * Provides the count of available exports of selected type
     * @return archive count
     */
    public long getArchiveCount();

    /**
     * Indicates if view is in archive mode
     * @return archive mode
     */
    public boolean isArchiveMode();

    /**
     * Indicates if view is in type selection mode
     * @return true if so
     */
    public boolean isTypeSelectionMode();

    /**
     * Provides the id of the export type after successfully type selection mode
     * @return record type
     */
    public Long getType();

    /**
     * Sets the record type, disables type selection mode and computes count of records available to perform an export of this type
     * @param type
     */
    public void setType(Long type);

    /**
     * Provides the selected export type object
     * @return exportType
     */
    public RecordExportType getExportType();

    /**
     * Indicates if cancellation export is selected
     * @return true if so
     */
    public boolean isCancellationSelected();

    /**
     * Indicates if credit note export is selected
     * @return true if so
     */
    public boolean isCreditNoteSelected();

    /**
     * Indicates if invoice export is selected
     * @return true if so
     */
    public boolean isInvoiceSelected();

    /**
     * Indicates if invoice export is selected
     * @return true if so
     */
    public boolean isDownpaymentSelected();

    /**
     * Indicates if purchase invoice export is selected
     * @return
     */
    public boolean isPurchaseInvoiceSelected();

    /**
     * Indicates if purchase invoice payment export is selected
     * @return true if purchase invoice payment
     */
    public boolean isPurchasePaymentSelected();

    /**
     * Indicates if sales invoice payment export is selected
     * @return true if sales invoice payment
     */
    public boolean isSalesPaymentSelected();

    /**
     * Provides the available count of records ready to export
     * @return available count
     */
    public Integer getAvailableCount();

    /**
     * Provides all unexported records of selected type
     * @return list of unexported records of current selected type 
     * or empty list if no type selected or non available
     */
    public List<RecordDisplay> getUnexported();
    
    /**
     * Indicates if unexported list display is activated
     * @return
     */
    public boolean isUnexportedDisplayMode();
    
    /**
     * Enables/disables unexported display mode 
     * @param unexportedDisplayMode
     */
    public void setUnexportedDisplayMode(boolean unexportedDisplayMode);

    /**
     * Provides all previously selected and unexported records of current selected type
     * @return list of selected unexported records of current selected type 
     */
    public List<RecordDisplay> getSelected();
    
    /**
     * Indicates if selected list display is activated
     * @return
     */
    public boolean isSelectedDisplayMode();
    
    /**
     * Enables/disables selected display mode 
     * @param selectedDisplayMode
     */
    public void setSelectedDisplayMode(boolean selectedDisplayMode);

    /**
     * Adds unexported records to selected list
     * @param form
     */
    public void selectUnexported(Form form);

    /**
     * Indicates if export confirmation mode is enabled
     * @return true if export confirmation mode enabled
     */
    public boolean isExportConfirmationMode();

    /**
     * Initiales the export archive of a previously selected type
     * @throws EmptyListException if no exports available
     */
    public void initArchive() throws EmptyListException;

    /**
     * Provides the available historical exports after archive initialization
     * @return available exports
     */
    public List<RecordExport> getAvailableExports();

    /**
     * Creates a new export of a previously selected type
     * @throws PermissionException if user is no domain user
     */
    public void createExport() throws PermissionException;

    /**
     * Removes a previously selected archive
     */
    public void removeArchive();

    /**
     * Selects available export from loaded archive
     * @param id of the export
     */
    public void selectAvailable(Long id);

    /**
     * Provides an xml representation of the current selected export
     * @return xml
     * @throws ClientException if any required value missing
     * @throws PermissionException if user has no permission
     */
    public Document getXml() throws ClientException, PermissionException;

    /**
     * Provides an xls sheet representation of the current selected export
     * @return sheet
     * @throws ClientException if any required value missing
     * @throws PermissionException if user has no permission
     */
    public String getXls() throws ClientException, PermissionException;

    /**
     * Provides a pdf representation of the current selected export
     * @return byte[] pdf
     * @throws ClientException if any required value missing
     * @throws PermissionException if user has no permission
     */
    public byte[] getPdf() throws ClientException, PermissionException;

    /**
     * Creates a zipped archive containing all pdf documents.
     * @return zip archive
     */
    DocumentData getZip() throws ClientException;

}
