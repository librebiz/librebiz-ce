/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 28, 2007 7:13:26 PM 
 * 
 */
package com.osserp.common.web;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.FileException;
import com.osserp.common.FileObject;
import com.osserp.common.User;
import com.osserp.common.util.DateUtil;
import com.osserp.common.util.NumberFormatter;
import com.osserp.common.util.NumberUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RequestForm extends AbstractForm implements Form {
    private static Logger log = LoggerFactory.getLogger(RequestForm.class.getName());
    private HttpServletRequest request = null;
    private Map<String, String> params = new HashMap<>();
    private List<FileObject> uploads = new ArrayList<>();
    private int uploadReadBufferSize = 8192;
    private String uploadDir = null;

    public RequestForm(HttpServletRequest request) {
        super();
        this.request = request;
        if (ServletFileUpload.isMultipartContent(request)) {
            try {
                String urbs = (String) request.getSession().getServletContext().getAttribute("uploadReadBufferSize");
                if (NumberUtil.isInteger(urbs)) {
                    uploadReadBufferSize = NumberUtil.createInteger(urbs);
                }
            } catch (Throwable t) {
                if (log.isInfoEnabled()) {
                    log.info("<init> caught exception on attempt to initialize upload buffer size [message="
                            + t.getMessage() + "]");
                }
            }
            parseMultipart();
        } else {
            parseForm();
        }
    }

    private void parseMultipart() {
        if (log.isDebugEnabled()) {
            log.debug("parseMultipart() invoked [uploadReadBufferSize=" + uploadReadBufferSize + "]");
        }
        ServletFileUpload fileUpload = new ServletFileUpload();
        try {
            FileItemIterator i = fileUpload.getItemIterator(request);
            while (i.hasNext()) {
                FileItemStream item = i.next();
                String name = item.getFieldName();
                InputStream stream = item.openStream();
                if (item.isFormField()) {
                    String value = Streams.asString(stream);
                    params.put(name, value);
                } else {
                    User user = SessionUtil.lookupUser(request.getSession());
                    long start = System.currentTimeMillis();
                    if (log.isDebugEnabled()) {
                        log.debug("parseMultipart() detected upload  [ip=" + RequestUtil.getRemoteAddress(request)
                                + ", user=" + (user == null ? "none" : user.getId())
                                + ", file=" + item.getName() + "]");
                    }
                    FileObject obj = new FileObject(item.getName(), stream, uploadReadBufferSize, uploadDir);
                    uploads.add(obj);
                    if (log.isDebugEnabled()) {
                        log.debug("parseMultipart() read upload done [ip=" + RequestUtil.getRemoteAddress(request)
                                + ", user=" + (user == null ? "none" : user.getId())
                                + ", file=" + item.getName()
                                + ", size=" + (obj.getFileData() == null ? 0 : obj.getFileData().length)
                                + ", duration=" + (System.currentTimeMillis() - start)
                                + "ms]");
                    }
                }
            }
        } catch (Exception ux) {
            logError("parseMultipart()", ux);
        }
    }

    private void logError(String method, Exception e) {
        User user = SessionUtil.lookupUser(request.getSession());
        long duration = 0;
        int bytesRead = 0;
        if (e instanceof FileException) {
            FileException fe = (FileException) e;
            duration = fe.getDuration();
            bytesRead = fe.getBytesRead();
        }
        if (e.getCause() != null) {
            log.error(method + " caught exception [ip=" + RequestUtil.getRemoteAddress(request)
                    + ", user=" + (user == null ? "none" : user.getId())
                    + ", duration=" + duration + ", bytesRead=" + bytesRead
                    + ", message="
                    + e.getCause().getMessage()
                    + ", exception=" + e.getCause().getClass().getName()
                    + "]", e.getCause());

        } else {
            log.error(method + " caught exception [ip=" + RequestUtil.getRemoteAddress(request)
                    + ", user=" + (user == null ? "none" : user.getId())
                    + ", duration=" + duration + ", bytesRead=" + bytesRead
                    + ", message=" + e.getMessage()
                    + ", exception=" + e.getClass().getName()
                    + "]");
        }
    }

    private void parseForm() {
        Enumeration e = request.getParameterNames();
        while (e.hasMoreElements()) {
            String param = (String) e.nextElement();
            String value = request.getParameter(param);
            if (value != null) {
                params.put(param, value);
                if (log.isDebugEnabled()) {
                    log.debug("parseForm() added param [name=" + param + ", value=" + value + "]");
                }
            }
        }
    }

    public Object getValue(String key) {
        try {
            return params.get(key);
        } catch (Throwable ignore) {
            log.error("getValue(" + key + ") ignoring of exception:\n"
                    + ignore.toString());
            return null;
        }
    }

    public String getString(String key) {
        try {
            String s = params.get(key);
            if (s == null || s.length() < 1) {
                return null;
            }
            return s;
        } catch (Exception ignore) {
            log.error("getString(" + key + ") ignoring of exception:\n"
                    + ignore.toString());
            return null;
        }
    }

    /**
     * Tries to extract a String from the form variable key.
     * @param key the name of the variable holding the value
     * @param required indicates if an exception should be thrown if no value under key found
     * @return value or null if variable not exists AND required is false
     * @throws ClientException if required is true and no string set
     */
    public String getString(String key, boolean required) throws ClientException {
        String res = getString(key);
        if (required && (res == null || res.length() < 1)) {
            throw new ClientException(ErrorCode.VALUES_MISSING);
        }
        return res;
    }

    /**
     * Provides a decimal percentage representation of a percentage value expected as string under given key
     * @param key
     * @return percentage representation or 0
     */
    public Double getPercentage(String key) {
        try {
            String s = getString(key);
            if (s == null) {
                return null;
            }
            if (log.isDebugEnabled()) {
                log.debug("getPercentage() retrieved " + s);
            }
            BigDecimal bd = new BigDecimal(s).divide(new BigDecimal("100"));
            Double result = Double.valueOf(bd.toPlainString());
            if (log.isDebugEnabled()) {
                log.debug("getPercentage() calulated percentage as value is "
                        + bd.doubleValue() + "\nresult as plain string is "
                        + bd.toPlainString() + "\ngenerated double is "
                        + result);
            }
            return result;
        } catch (NumberFormatException nfe) {
            return 0d;
        }
    }

    /**
     * Tries to extract an Integer from the form variable key.
     * @param key the name of the variable holding the value
     * @return value or null if variable not exists
     */
    public Integer getInteger(String key) {
        try {
            return NumberUtil.createInteger(getString(key));
        } catch (Throwable ignore) {
            log.error("getInteger(" + key + ") ignoring of exception:\n"
                    + ignore.toString());
            return null;
        }
    }

    /**
     * Tries to get an int from a form property defined as Integer
     * @param key
     * @return value
     * @throws ClientException if no int found
     */
    public int getIntVal(String key) throws ClientException {
        try {
            return getInteger(key).intValue();
        } catch (Throwable ex) {
            log.error("getInt(" + key + ") failed: " + ex.toString());
            throw new ClientException("int.expected");
        }
    }

    /**
     * Tries to get an object specified as Long in form definition
     * @param key of the form field
     * @return value or null if not found
     */
    public Long getLong(String key) {
        try {
            return NumberUtil.createLong(getString(key));
        } catch (Throwable ignore) {
            return null;
        }
    }

    /**
     * Tries to get the long value of an object specified as Long in form definition
     * @param key of the form field
     * @return value or -1 if not found
     */
    public long getLongVal(String key) {
        Long l = getLong(key);
        return (l == null) ? -1 : l.longValue();
    }

    public Long getSelection(String key) {
        Long l = getLong(key);
        return l == null || l == 0 ? null : l;
    }

    /**
     * Tries to get a boolean value of an object specified as Boolean in form definition
     * @param key of the form field
     * @return true if set, false if not set or found
     */
    public boolean getBoolean(String key) {
        try {
            String bool = getString(key);
            if (bool == null || bool.length() == 0) {
                return false;
            }
            if (bool.equalsIgnoreCase("on") || bool.equalsIgnoreCase("checked")) {
                return true;
            }
            return Boolean.valueOf(bool);
        } catch (Exception ignore) {
            log.error("getBoolean(" + key + ") ignoring of exception:\n"
                    + ignore.toString());
            return false;
        }
    }

    /**
     * Tries to retrieve an array of String objects specified as String[] in the form definition.
     * @param key of the form field
     * @return array or null if not set or found
     */
    public String[] getIndexedStrings(String key) {
        try {
            return request.getParameterValues(key);
        } catch (Exception e) {
            if (log.isDebugEnabled()) {
                log.debug("getIndexedStrings(" + key
                        + ") throws exception: " + e.toString(), e);
            }
            return null;
        }
    }

    /**
     * Tries to retrieve an array of Integer objects specified as Integer[] in the form definition.
     * @param key of the form field
     * @return array or null if not set or found
     */
    @SuppressWarnings("null")
    public Integer[] getIndexedIntegers(String key) {
        try {
            Integer[] result = new Integer[] {};
            String[] values = getIndexedStrings(key);
            int ln = (values == null ? 0 : values.length);
            if (ln > 0) {
                result = new Integer[ln];
                for (int i = 0, j = ln; i < j; i++) {
                    result[i] = NumberUtil.createInteger(values[i]);
                }
            }
            return result;
        } catch (Exception e) {
            if (log.isDebugEnabled()) {
                log.debug("getIndexedIntegers(" + key
                        + ") throws exception: " + e.toString(), e);
            }
            return null;
        }
    }

    /**
     * Tries to retrieve an array of Long objects specified as Long[] in the form definition.
     * @param key of the form field
     * @return array or null if not set or found
     */
    @SuppressWarnings("null")
    public Long[] getIndexedLong(String key) {
        try {
            Long[] result = new Long[] {};
            String[] values = getIndexedStrings(key);
            int ln = (values == null ? 0 : values.length);
            if (ln > 0) {
                result = new Long[ln];
                for (int i = 0, j = ln; i < j; i++) {
                    result[i] = NumberUtil.createLong(values[i]);
                }
            }
            return result;
        } catch (Exception e) {
            if (log.isDebugEnabled()) {
                log.debug("getIndexedLong(" + key
                        + ") throws exception: " + e.toString(), e);
            }
            return null;
        }
    }

    /**
     * Retrieves indexed double value
     * @param key of form field
     * @return array or null if none exist
     * @throws ClientException if any value has an invalid format
     */
    public Double[] getIndexedDouble(String key) throws ClientException {
        String[] sarray = getIndexedStrings(key);
        if (sarray == null || sarray.length == 0) {
            return null;
        }
        int l = sarray.length;
        Double[] res = new Double[l];
        for (int i = 0, j = l; i < j; i++) {
            res[i] = NumberFormatter.createDouble(sarray[i], true);
        }
        return res;
    }

    /**
     * Retrieves a request parameter string as date
     * @param key request param value of key
     * @return date parsed from param value string or null if not set
     * @throws ClientException if value was set but illegal format
     */
    public Date getDate(String key) throws ClientException {
        return DateUtil.createDate(getString(key), false);
    }

    /**
     * Tries to extract a Double value from the form variable key.
     * @param key the name of the variable holding the value
     * @return value or null if variable not exists
     * @throws ClientException if value exists and is no number
     */
    public BigDecimal getDecimal(String key) throws ClientException {
        try {
            return NumberFormatter.createDecimal(getString(key), false);
        } catch (ClientException e) {
            if (log.isDebugEnabled()) {
                log.debug("getDecimal() invalid value under key " + key);
            }
            throw e;
        }
    }

    /**
     * Tries to extract a Double value from the form variable key.
     * @param key the name of the variable holding the value
     * @return value or null if variable not exists
     * @throws ClientException if value exists and is no number
     */
    public Double getDouble(String key) throws ClientException {
        try {
            return NumberFormatter.createDouble(getString(key), false);
        } catch (ClientException e) {
            if (log.isDebugEnabled()) {
                log.debug("getDouble() invalid value under key " + key);
            }
            throw e;
        }
    }

    public String getDoubleAsString(String key) throws ClientException {
        Object value = getValue(key);
        if (value == null) {
            return null;
        }
        Double result = NumberFormatter.createDouble(value.toString(), false);
        return (result == null ? null : result.toString());
    }

    /**
     * Provides a decimal percentage representation of a percentage value expected as string under given key
     * @param key
     * @param required
     * @return percentage representation or null if required is false and no value found
     * @throws ClientException if string found under key represents no number
     */
    public Double getPercentage(String key, boolean required) throws ClientException {
        try {
            String s = getString(key);
            if (s == null) {
                if (required) {
                    throw new ClientException(ErrorCode.VALUES_MISSING);
                }
                return null;
            }
            if (log.isDebugEnabled()) {
                log.debug("getPercentage() retrieved " + s);
            }
            BigDecimal bd = new BigDecimal(s).divide(new BigDecimal("100"));
            Double result = Double.valueOf(bd.toPlainString());
            if (log.isDebugEnabled()) {
                log.debug("getPercentage() calulated percentage as value is "
                        + bd.doubleValue() + "\nresult as plain string is "
                        + bd.toPlainString() + "\ngenerated double is "
                        + result);
            }
            return result;
        } catch (NumberFormatException nfe) {
            throw new ClientException(ErrorCode.NUMBER_EXPECTED);
        }
    }

    public void setValue(String key, Object value) {
        request.setAttribute(key, value);
    }

    public void resetValue(String key) {
        params.remove(key);
    }

    public Object getForm() {
        return request;
    }

    public FileObject getUpload() {
        if (uploads.isEmpty()) {
            if (log.isInfoEnabled()) {
                log.info("getUpload() no uploads available...");
            }
            return null;
        }
        return uploads.get(0);
    }

    public List<FileObject> getUploads() {
        return uploads == null ? new ArrayList<>() : uploads;
    }

    public Map<String, String> getMapped() {
        return params;
    }
}
