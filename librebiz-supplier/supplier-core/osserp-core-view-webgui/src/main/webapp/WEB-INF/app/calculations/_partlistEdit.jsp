<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" scope="page" value="${sessionScope.partlistView}"/>
<c:set var="partlist" scope="session" value="${view.bean}"/>

<div class="contentBox">
	<div class="contentBoxHeader">
		<div class="contentBoxHeaderLeft">
			<fmt:message key="providedFor"/>
			<o:out value="${view.product.name}"/>
			<fmt:message key="by"/>
			<v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${partlist.createdBy}">
				<oc:employee value="${partlist.createdBy}"/>
			</v:ajaxLink>
							/ <o:date value="${partlist.created}"/>
		</div>
	</div>
</div>

<div class="spacer"></div>

<table class="table table-striped">
	<thead>
		<tr>
			<th style="width: 90px;" class="right"><fmt:message key="productid"/></th>
			<th><fmt:message key="label"/></th>
			<th class="right"><fmt:message key="quantity"/></th>
			<th><fmt:message key="quantityUnitShort"/></th>
			<th class="right"><fmt:message key="price"/></th>
			<th class="right"><fmt:message key="total"/></th>
			<th style="width: 120px;"></th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="position" items="${partlist.positions}">
			<tr>
				<td></td>
				<td colspan="5" class="boldtext">
					<a href="<c:url value="/partlistProductSearch.do"><c:param name="method" value="forward"/><c:param name="positionId" value="${position.id}"/></c:url>" title="<fmt:message key="productsByPreselection"/>">
						<o:out value="${position.name}"/>
					</a>
				</td>
				<td>
					<a href="<c:url value="/partlistProductSearch.do"><c:param name="method" value="forward"/><c:param name="positionId" value="${position.id}"/><c:param name="ignorePreselection" value="true"/></c:url>" title="<fmt:message key="productSearch"/>">
						<o:img name="forwardIcon"/>
					</a>
				</td>
			</tr>
			<c:forEach var="item" items="${position.items}">
				<tr class="lightgrey">
					<td class="right">
						<a href="<c:url value="/products.do"><c:param name="method" value="load"/><c:param name="id" value="${item.product.productId}"/><c:param name="exit" value="partlist"/></c:url>">
							<o:out value="${item.product.productId}"/>
						</a>
					</td>
					<td>
						<o:out value="${item.product.name}" limit="52"/>
						<c:if test="${!empty item.note}">
							<div class="smalltext tab"><o:textOut value="${item.note}"/></div>
						</c:if>
					</td>
					<td class="right">
						<a href="<v:url value="/calculations/partlist/enableItemChangeMode?itemId=${item.id}"/>">
							<o:number value="${item.quantity}" format="decimal"/>
						</a>
					</td>
					<td class="center"><oc:options name="quantityUnits" value="${item.product.quantityUnit}"/></td>
					<td class="right"><o:number value="${item.price}" format="currency"/></td>
					<td class="right"><o:number value="${item.amount}" format="currency"/></td>
					<td style="width: 120px;">
						<a href="<v:url value="/calculations/partlist/moveItemUp?itemId=${item.id}"/>" title="<fmt:message key="moveUp"/>">
							<o:img name="upIcon"/>
						</a>
						<a href="<v:url value="/calculations/partlist/moveItemDown?itemId=${item.id}"/>" title="<fmt:message key="moveDown"/>">
							<o:img name="downIcon"/>
						</a>
						<a href="<c:url value="/partlistProductSearch.do"><c:param name="method" value="forward"/><c:param name="positionId" value="${position.id}"/><c:param name="itemId" value="${item.id}"/></c:url>" title="<fmt:message key="replaceProduct"/>">
							<o:img name="replaceIcon"/>
						</a>
						<a href="<v:url value="/calculations/partlist/removeItem?itemId=${item.id}"/>" title="<fmt:message key="deleteProduct"/>">
							<o:img name="deleteIcon"/>
						</a>
					</td>
				</tr>
			</c:forEach>
		</c:forEach>
		<tr>
			<td colspan="7"></td>
		</tr>
		<tr class="lightgrey">
			<td></td>
			<td colspan="4" class="boldtext"><fmt:message key="summary"/></td>
			<td class="right boldtext"><o:number value="${partlist.price}" format="currency"/></td>
			<td></td>
		</tr>
	</tbody>
</table>