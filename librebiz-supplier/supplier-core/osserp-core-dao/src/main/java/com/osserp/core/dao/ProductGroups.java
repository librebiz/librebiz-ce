/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 17.09.2004 
 * 
 */
package com.osserp.core.dao;

import java.util.List;

import com.osserp.core.products.ProductClass;
import com.osserp.core.products.ProductGroup;
import com.osserp.core.products.ProductGroupConfig;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ProductGroups extends ProductClassificationEntities {

    /**
     * Loads an immutable product group from persistent storage
     * @param id
     * @return group
     */
    ProductGroup get(Long id);

    /**
     * Loads a mutable group from persistent storage
     * @param id
     * @return group
     */
    ProductGroupConfig load(Long id);

    /**
     * Provides stock aware product groups
     * @return stockAware
     */
    List<ProductGroup> findStockAware();

    /**
     * Provides the product groups count
     * @return count
     */
    int getCount();

    /**
     * Provides a list of available product group configs
     * @return groups
     */
    List<ProductGroupConfig> getGroups();

    /**
     * Loads an product class by id
     * @param id
     * @return classification
     */
    ProductClass loadClassification(Long id);

    /**
     * Provides a list of available product classifications
     * @return classifications
     */
    List<ProductClass> getClassifications();
}
