<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${requestScope.productPriceAwareView}"/>
<c:set var="product" value="${view.product}"/>
<c:set var="priceAware" value="${view.bean}"/>

<style type="text/css">

	.col1 {
		width: 300px;
	}

	.col3, .col4 {
		width: 160px;
	}


</style>


<script type="text/javascript">

	function calculateMinimum(inputElement, minimumDisplayInput, priceInput) {
		var lpp = createDouble(document.getElementById("lastPurchasePrice").firstChild.nodeValue);
		if (lpp <= 0) {
			lpp = createDouble(document.getElementById("estimatedPurchasePrice").firstChild.nodeValue);
		}
		if (lpp > 0) {
			var margin = createDouble(inputElement.value) / 100;
			var price = Math.round((lpp/(1-margin)) * 100)/100;
			document.getElementById(minimumDisplayInput).value = price;
			var priceInputField = createDouble(document.getElementById(priceInput).value);
			if (priceInputField == 0) {
				document.getElementById(priceInput).value = price;
			}
		}
	}

</script>

<v:form name="productPriceForm" url="${view.baseLink}/save">
	<table class="table">
		<tr>
			<th colspan="4" style="width: 775px;"><fmt:message key="priceEditingProduct"/></th>
		</tr>
		<o:permission role="purchase_price_view,purchase_price_change" info="permissionPurchasePriceView">
		<tr>
			<td colspan="4"> </td>
		</tr>
		<tr>
			<td class="right col1"><fmt:message key="purchasePriceBase"/></td>
			<td class="right col2">
				<o:permission role="purchase_price_change" info="permissionPurchasePriceChange">
					<input type="text" class="number" id="estimatedPurchasePrice" name="estimatedPurchasePrice" value="<o:number value="${product.calculationPurchasePrice}" format="currency"/>" tabindex="1"/>
				</o:permission>
				<o:forbidden role="purchase_price_change">
					<span id="estimatedPurchasePrice"><o:number value="${product.calculationPurchasePrice}" format="currency"/></span>
				</o:forbidden>
			</td>
			<td class="right col3"><fmt:message key="purchasePriceLast"/></td>
			<td class="right col4" id="lastPurchasePrice"><o:number value="${product.summary.lastPurchasePrice}" format="currency" /></td>
		</tr>
		<tr>
			<td colspan="2" class="right">
				<o:permission role="product_pp_ignore_change" info="permissionPriceUnderPurchasePriceInput">
					<a href="<v:url value="${view.baseLink}/changePurchasePriceLimitFlag"/>">
						<c:choose>
							<c:when test="${product.ignorePurchasePriceLimit}">
								<fmt:message key="salesPriceUnderPurchasePriceInputEnabled"/>
							</c:when>
							<c:otherwise>
								<fmt:message key="salesPriceUnderPurchasePriceInputDisabled"/>
							</c:otherwise>
						</c:choose>
					</a>
				</o:permission>
				<o:forbidden role="product_pp_ignore_change">
					<c:choose>
						<c:when test="${product.ignorePurchasePriceLimit}">
							<fmt:message key="salesPriceUnderPurchasePriceInputEnabled"/>
						</c:when>
						<c:otherwise>
							<fmt:message key="salesPriceUnderPurchasePriceInputDisabled"/>
						</c:otherwise>
					</c:choose>
				</o:forbidden>
			</td>
			<td class="right col3"><fmt:message key="purchasePriceAverage"/></td>
			<td class="right col4"><o:number value="${product.summary.averagePurchasePrice}" format="currency" /></td>
		</tr>
		<tr>
			<td colspan="4">&nbsp;</td>
		</tr>
		</o:permission>
		<o:forbidden role="purchase_price_view,purchase_price_change" info="permissionPurchasePriceView">
		<tr>
			<td colspan="4">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4">
				<input type="hidden" id="estimatedPurchasePrice" name="estimatedPurchasePrice" value="<o:number value="${product.calculationPurchasePrice}" format="currency"/>" />
			</td>
		</tr>
		</o:forbidden>
		<tr>
			<th class="right"><fmt:message key="targetGroup"/></th>
			<th class="right"><fmt:message key="price"/> &euro;</th>
			<th class="right"><fmt:message key="priceMinimum"/> &euro;</th>
			<th class="right"><fmt:message key="marginMinimum"/> %</th>
		</tr>
		<tr>
			<td colspan="4">&nbsp;</td>
		</tr>
		<tr>
			<td class="right col1"><fmt:message key="sale"/></td>
			<td class="right col2"><input class="number" name="consumerPrice" id="consumerPrice" type="text" value="<o:number value="${priceAware.consumerPrice}" format="currency"/>" tabindex="2"/></td>
			<td class="right col3"><input class="number" name="consumerPriceMinimum" id="consumerPriceMinimum" type="text" readonly="readonly" value="<o:number value="${priceAware.consumerPriceMinimum}" format="currency"/>"/></td>
			<td class="right col4"><input class="number" name="consumerMinimumMargin" onblur="calculateMinimum(this, 'consumerPriceMinimum','consumerPrice');" type="text" value="<o:number value="${priceAware.consumerMinimumMargin}" format="percent"/>" tabindex="3"/></td>
		</tr>
		<tr>
			<td class="right col1"><fmt:message key="reseller"/></td>
			<td class="right col2"><input class="number" name="resellerPrice" id="resellerPrice" type="text" value="<o:number value="${priceAware.resellerPrice}" format="currency"/>" tabindex="4"/></td>
			<td class="right col3"><input class="number" name="resellerPriceMinimum" id="resellerPriceMinimum" type="text" readonly="readonly" value="<o:number value="${priceAware.resellerPriceMinimum}" format="currency"/>"/></td>
			<td class="right col4"><input class="number" name="resellerMinimumMargin" onblur="calculateMinimum(this, 'resellerPriceMinimum','resellerPrice');" type="text" value="<o:number value="${priceAware.resellerMinimumMargin}" format="percent"/>" tabindex="5"/></td>
		</tr>
		<tr>
			<td class="right col1"><fmt:message key="partner"/></td>
			<td class="right col2"><input class="number" name="partnerPrice" id="partnerPrice" type="text" value="<o:number value="${priceAware.partnerPrice}" format="currency"/>" tabindex="6"/></td>
			<td class="right col3"><input class="number" name="partnerPriceMinimum" id="partnerPriceMinimum" type="text" readonly="readonly" value="<o:number value="${priceAware.partnerPriceMinimum}" format="currency"/>"/></td>
			<td class="right col4"><input class="number" name="partnerMinimumMargin" onblur="calculateMinimum(this, 'partnerPriceMinimum','partnerPrice');" type="text" value="<o:number value="${priceAware.partnerMinimumMargin}" format="percent"/>" tabindex="7"/></td>
		</tr>
		<tr>
			<td colspan="4">&nbsp;</td>
		</tr>
		<tr>
			<td class="right col1">
				<input type="button" onclick="gotoUrl('<v:url value="${view.baseLink}/disableEditMode"/>');" value="<fmt:message key="exit"/>" class="submit submitExit"/>
			</td>
			<td class="right" colspan="3">
				<o:submit/>
			</td>
		</tr>
		<tr>
			<td colspan="4"> </td>
		</tr>
	</table>
</v:form>
