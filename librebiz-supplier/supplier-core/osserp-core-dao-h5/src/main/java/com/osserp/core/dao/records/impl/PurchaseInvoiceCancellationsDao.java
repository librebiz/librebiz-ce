/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 31, 2008 10:43:52 AM 
 * 
 */
package com.osserp.core.dao.records.impl;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;

import com.osserp.core.BusinessType;
import com.osserp.core.dao.PaymentConditions;
import com.osserp.core.dao.Suppliers;
import com.osserp.core.dao.SystemCompanies;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.dao.TaxRates;
import com.osserp.core.dao.records.PurchaseInvoiceCancellations;
import com.osserp.core.dao.records.RecordMailLogs;
import com.osserp.core.dao.records.RecordPrintOptionDefaults;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordNumberCreator;
import com.osserp.core.finance.RecordType;
import com.osserp.core.model.records.PurchaseInvoiceCancellationImpl;
import com.osserp.core.purchasing.PurchaseInvoice;
import com.osserp.core.purchasing.PurchaseInvoiceCancellation;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PurchaseInvoiceCancellationsDao extends AbstractPurchaseRecords implements PurchaseInvoiceCancellations {

    public PurchaseInvoiceCancellationsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache cache,
            TaxRates taxRates,
            SystemCompanies companies,
            PaymentConditions paymentConditions,
            RecordNumberCreator recordNumberCreator,
            RecordPrintOptionDefaults recordPrintOptionDefaults,
            RecordMailLogs recordMailLogs,
            Suppliers suppliers) {
        super(
                jdbcTemplate,
                tables,
                sessionFactory,
                cache,
                taxRates,
                companies,
                null,
                paymentConditions,
                recordNumberCreator,
                recordPrintOptionDefaults,
                recordMailLogs,
                suppliers,
                TableKeys.PURCHASE_INVOICE_CANCELLATIONS,
                TableKeys.PURCHASE_INVOICE_CANCELLATION_ITEMS);
    }

    @Override
    protected Long getDefaultRecordTypeId() {
        return RecordType.PURCHASE_INVOICE;
    }

    public PurchaseInvoiceCancellation create(
            Employee user,
            PurchaseInvoice invoice,
            String note) throws ClientException {
        if (isEmpty(note)) {
            throw new ClientException(ErrorCode.ACTION_NOTE_REQUIRED);
        }
        PurchaseInvoiceCancellationImpl obj = new PurchaseInvoiceCancellationImpl(user, invoice, note);
        saveInitial(obj);
        return obj;
    }

    public PurchaseInvoiceCancellation find(Long id) {
        return (PurchaseInvoiceCancellation) load(id);
    }

    @Override
    protected Class<PurchaseInvoiceCancellationImpl> getPersistentClass() {
        return PurchaseInvoiceCancellationImpl.class;
    }

    @Override
    protected BusinessType getRequestType(Record record) {
        return null;
    }

}
