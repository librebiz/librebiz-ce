/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 13, 2006 4:15:56 PM 
 * 
 */
package com.osserp.common.dms.dao;

import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.FileObject;
import com.osserp.common.Option;
import com.osserp.common.User;
import com.osserp.common.dms.DmsConfig;
import com.osserp.common.dms.DmsDocument;
import com.osserp.common.dms.DocumentType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Documents {
    
    /**
     * Provides the primary and activated dms configuration object.
     * @param templates indicates if the templateDmsConfig object
     * should be fetched instead of the dmsConfig which is the default.
     * @return default dms config or template dms config
     */
    DmsConfig getDmsConfig(boolean templates);

    /**
     * Provides a document type by id
     * @param id
     * @return documentType
     */
    DocumentType getDocumentType(Long id);

    /**
     * Provides the document with given primary key
     * @param id
     * @return document or null if not found
     */
    DmsDocument findById(Long id);

    /**
     * Provides the document with given primary key
     * @param id
     * @return document or throws runtime exception if not found
     */
    DmsDocument getById(Long id);

    /**
     * @param messageId
     * @return documents by message id
     */
    List<DmsDocument> findByMessageId(String messageId);

    /**
     * Provides all documents referencing provided type
     * @param type
     * @return documents or empty list if non exists
     */
    List<DmsDocument> findByType(DocumentType type);
    
    /**
     * Provides the count of all documents referencing a type
     * @param documentType
     * @return count of available documents
     */
    int countByType(DocumentType documentType);
    
    /**
     * Provides all available categories
     * @return categories or empty list if non available
     */
    List<Option> getCategories();
    
    /**
     * Provides the default category 
     * @return default category or null if none configured
     */
    Long getDefaultCategoryId(Long documentType);

    /**
     * Provides document data as byte array
     * @param document
     * @return documentData
     */
    byte[] getDocumentData(DmsDocument document);

    /**
     * Provides thumbnail data as byte array
     * @param document
     * @return thumbnailData
     */
    byte[] getThumbnailData(DmsDocument document);

    /**
     * Updates an existing document
     * @param user the updating user
     * @param document
     * @param file
     * @param note to the document
     * @param category optional category selection
     * @throws ClientException if validation failed
     */
    void update(User user, DmsDocument document, FileObject file, String note, Long category) throws ClientException;

    /**
     * Updates the category of an existing document
     * @param user updating the document
     * @param id of an existing document to update
     * @param categoryId or null to reset
     */
    void updateCategory(User user, Long id, Long categoryId);

    /**
     * Updates the metadata of an existing document
     * @param user
     * @param document
     * @param note
     * @param validFrom
     * @param validTil
     * @throws ClientException
     */
    void updateMetadata(User user, DmsDocument document, String note, Date validFrom, Date validTil) throws ClientException;

    /**
     * Sets the document with given id as reference
     * @param id of the document
     */
    void setAsReference(Long id);
    
    /**
     * Changes persistent format if differs
     * @param document
     * @param format
     */
    void changeFormat(DmsDocument document, int format);
    
    /**
     * Exports a document to an external location.
     * @param document the document to export
     * @param exportFile the path and filename to export to
     * @throws ClientException if export failed for any reason, see message
     */
    void export(DmsDocument document, String exportFile) throws ClientException;
    
    /**
     * Deletes a previously created export file
     * @param exportFile the file including pathname
     * @throws ClientException
     */
    void deleteExport(String exportFile) throws ClientException;
    
}
