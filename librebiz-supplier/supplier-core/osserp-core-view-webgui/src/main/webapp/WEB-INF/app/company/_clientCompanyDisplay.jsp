<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="companyDisplay" scope="request" value="${view.bean}"/>
<c:set var="companyDisplayMultiple" scope="request" value="${view.multipleClientsAvailable}"/>
<c:if test="${view.companyEditable == 'true'}">
<c:set var="companyEditable" scope="request" value="true"/>
</c:if>
<c:set var="clientLogoExit" scope="request" value="/company/clientCompany/reload"/>

<div class="row">

    <c:import url="_companyDisplay.jsp"/>
    
    <c:choose>
        <c:when test="${!view.multipleBranchsAvailable && !empty view.selectedBranch}">
            <c:set var="branchDisplay" scope="request" value="${view.selectedBranch}"/>
            <c:set var="branchForwardExit" scope="request" value="${clientLogoExit}"/>
            <c:import url="_branchDisplay.jsp"/>
        </c:when>
        <c:otherwise>
            <c:import url="_branchListDisplay.jsp"/>
        </c:otherwise>
    </c:choose>
        
</div>
