/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 20, 2016 
 * 
 */
package com.osserp.gui.common

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.core.BusinessCase
import com.osserp.core.BusinessCaseSearch
import com.osserp.core.requests.Request
import com.osserp.core.requests.RequestManager
import com.osserp.core.sales.Sales

import com.osserp.groovy.web.ViewContextException

import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class BusinessCaseAwareView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(BusinessCaseAwareView.class.getName())

    Boolean businessCaseRequired = false
    BusinessCase businessCase
    String businessId = 'businessId'

    BusinessCaseAwareView() {
        super()
        dependencies = ['businessCaseView']
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (forwardRequest) {
            Long id = form.getLong(businessId)
            if (id) {
                businessCase = businessCaseSearch.findBusinessCase(id)
                 
            } else if (env.businessCaseView?.businessCase) {
                businessCase = env.businessCaseView.businessCase
            }
            if (businessCaseRequired && !businessCase) {
                logger.warn("initRequest: did not find businessCase by param ${businessId} nor businessCaseView")
                throw new ViewContextException(this)
            }

            if (businessCase) {
                logger.debug("initRequest: done [id=${businessCase.primaryKey}, type=${businessCase.type.id}, sales=${businessCase.salesContext}]")
            }
        }
    }

    Request getRequest() {
        (businessCase instanceof Sales) ? sales.request : businessCase
    }

    Sales getSales() {
        (businessCase instanceof Sales) ? businessCase : null
    }

    boolean isUserRelatedManager() {
        if (!businessCase) {
            return false
        }
        domainUser.isUserRelatedManager(businessCase)
    }

    protected BusinessCaseSearch getBusinessCaseSearch() {
        getService(BusinessCaseSearch.class.getName())
    }

    protected final RequestManager getRequestManager() {
        getService(RequestManager.class.getName())
    }
}
