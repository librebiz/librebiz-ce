/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.model.projects;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.Status;
import com.osserp.common.beans.AbstractOption;

import com.osserp.core.projects.ProjectTracking;
import com.osserp.core.projects.ProjectTrackingRecord;
import com.osserp.core.projects.ProjectTrackingRecordType;
import com.osserp.core.projects.ProjectTrackingType;
import com.osserp.core.requests.Request;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProjectTrackingImpl extends AbstractOption implements ProjectTracking {

    private Date startDate;
    private Date endDate;
    private Status status;
    private Double totalTime = 0d;
    private Long productId = null;
    private boolean productFixed = true;
    private ProjectTrackingType type;
    private boolean printReverse = false;
    private boolean printWorker = false;
    private Long worker = null;
    
    private List<ProjectTrackingRecord> records = new java.util.ArrayList<ProjectTrackingRecord>();

    protected ProjectTrackingImpl() {
        super();
    }

    /**
     * Creates a new tracking object
     * @param initialStatus 
     * @param user
     * @param reference
     * @param trackingType
     * @param name
     * @param start
     * @param end
     */
    public ProjectTrackingImpl(Status initialStatus, Long user, Request reference, ProjectTrackingType trackingType, String name, Date start, Date end) {
        super((Long) null, reference.getRequestId(), name, start, user);
        type = trackingType;
        startDate = start;
        endDate = end;
        status = initialStatus;
    }

    public void update(Long user, ProjectTrackingType type, String name, Date startDate, Date endDate) {
        if (startDate != null) {
            this.startDate = startDate;
        }
        // tracking may have open end... 
        this.endDate = endDate;
        
        if (type != null) {
            this.type = type;
        }
        if (isSet(name)) {
            setName(name);
        }
        updateChanged(user);
    }

    public void update(Long user, Status status) {
        if (status != null) {
            this.status = status;
            updateChanged(user);
        }
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public ProjectTrackingType getType() {
        return type;
    }

    public void setType(ProjectTrackingType type) {
        this.type = type;
    }

    public List<ProjectTrackingRecord> getRecords() {
        return records;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public boolean isProductFixed() {
        return productFixed;
    }

    public void setProductFixed(boolean productFixed) {
        this.productFixed = productFixed;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Double getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(Double totalTime) {
        this.totalTime = totalTime;
    }
    
    public boolean isPrintReverse() {
        return printReverse;
    }
    
    public void setPrintReverse(boolean printReverse) {
        this.printReverse = printReverse;
    }

    public boolean isPrintWorker() {
        return printWorker;
    }

    public void setPrintWorker(boolean printWorker) {
        this.printWorker = printWorker;
    }

    public Long getWorker() {
        return worker;
    }

    public void setWorker(Long worker) {
        this.worker = worker;
    }

    public boolean isClosed() {
        return status != null && status.getId() >= ProjectTracking.STATUS_CLOSED;
    }

    protected void setRecords(List<ProjectTrackingRecord> records) {
        this.records = records;
    }

    public void toggleIgnore(Long user, Long record) {
        for (Iterator<ProjectTrackingRecord> i = getRecords().iterator(); i.hasNext();) {
            ProjectTrackingRecord next = i.next();
            if (next.getId().equals(record)) {
                next.changeIgnore(user, !next.isIgnore());
                break;
            }
        }
        createTotalTime();
    }

    public void addRecord(
            Long user, 
            ProjectTrackingRecordType recordType, 
            String name, 
            String description,
            String internalNote,
            Date startDate, 
            Date endDate, 
            Double duration) throws ClientException {
        
        validateRecord(name, description, startDate, endDate, duration);
        
        ProjectTrackingRecord record = new ProjectTrackingRecordImpl(
            user, this, recordType, name, description, internalNote, 
            startDate, endDate, duration);
        records.add(record);
        createTotalTime();
    }

    public void updateRecord(
            Long user, 
            ProjectTrackingRecord record, 
            ProjectTrackingRecordType recordType, 
            String name, 
            String description, 
            String internalNote,
            Date startDate, 
            Date endDate, 
            Double time) throws ClientException {
        
        if (record == null) {
            addRecord(user, recordType, name, description, internalNote, startDate, endDate, time);
        } else {
            validateRecord(name, description, startDate, endDate, time);
            
            for (Iterator<ProjectTrackingRecord> i = records.iterator(); i.hasNext();) {
                ProjectTrackingRecord next = i.next();
                if (next.getId().equals(record.getId())) {
                    next.update(user, recordType, name, description, internalNote, startDate, endDate, time);
                    break;
                }
            }
            createTotalTime();
        }
    }

    private void validateRecord(String name, String description, Date startDate, Date endDate, Double time) throws ClientException {
        if (isNotSet(description)) {
            throw new ClientException(ErrorCode.TEXT_MISSING);
        }
        if (startDate == null && endDate == null && isNotSet(time)) {
            throw new ClientException(ErrorCode.DATE_OR_TIME_REQUIRED);
        }
        if (startDate == null) {
            throw new ClientException(ErrorCode.DATE_MISSING);
        }
    }

    public void removeRecord(Long recordId) {
        for (Iterator<ProjectTrackingRecord> i = records.iterator(); i.hasNext();) {
            ProjectTrackingRecord next = i.next();
            if (next.getId().equals(recordId)) {
                i.remove();
                break;
            }
        }
        createTotalTime();
    }
    
    private void createTotalTime() {
        totalTime = 0d;
        for (int i = 0, j = records.size(); i < j; i++) {
            ProjectTrackingRecord next = records.get(i);
            if (!next.isIgnore()) {
                totalTime = totalTime + 
                    (next.getDuration() == null ? 0 : next.getDuration());
            }
        }
    }
}
