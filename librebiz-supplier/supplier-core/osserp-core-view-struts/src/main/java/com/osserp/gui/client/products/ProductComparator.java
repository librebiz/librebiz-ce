/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 28, 2010 11:39:59 AM 
 * 
 */
package com.osserp.gui.client.products;

import java.util.List;

import com.osserp.common.util.AbstractComparator;

import com.osserp.core.products.ProductSorter;

/**
 * 
 * @author cf <cf@osserp.com>
 */
public class ProductComparator extends AbstractComparator {

    public ProductComparator() {
        super();
    }

    public ProductComparator(String comparatorMethod, boolean reverse) {
        super(comparatorMethod, reverse);
    }

    @Override
    public void sort(final List list) {
        ProductSorter.instance().sort(isReverse() ? getComparatorMethod() + "Reverse" : getComparatorMethod(), list);
    }

}
