/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jul 21, 2008 
 * 
 */
package com.osserp.core.employees;

import java.util.List;

import com.osserp.common.Role;
import com.osserp.core.system.BranchOffice;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface EmployeeRoleConfig extends Role {

    /**
     * Provides the branch of the role
     * @return branch
     */
    BranchOffice getBranch();

    /**
     * Provides the roles
     * @return roles
     */
    List<EmployeeRole> getRoles();

    /**
     * Adds a new role if not already exists
     * @param user whose adding the new role
     * @param group of the role
     * @param status of the employee in group
     * @return role
     */
    EmployeeRole addRole(Employee user, EmployeeGroup group, EmployeeStatus status);

    /**
     * Removes a role
     * @param user whose removing the role
     * @param id
     */
    void removeRole(Employee user, Long id);

    /**
     * Marks selected role as default
     * @param user whose changing the role
     * @param id
     */
    void setDefaultRole(Employee user, Long id);

}
