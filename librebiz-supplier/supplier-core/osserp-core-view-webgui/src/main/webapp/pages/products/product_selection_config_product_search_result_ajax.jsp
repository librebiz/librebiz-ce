<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${sessionScope.searchView}"/>
<c:set var="exitTarget" value="productSelectionConfigProductSearch"/>
<div class="table-responsive table-responsive-default">
    <table class="table table-striped">
        <thead>
            <tr>
                <th class="productNumber">
                    <a href="<c:url value="/productSelectionConfigProductSearch.do?method=sort&order=byId" />"><fmt:message key="product"/></a>
                </th>
                <th class="productName">
                    <a href="<c:url value="/productSelectionConfigProductSearch.do?method=sort&order=byName" />"><fmt:message key="name"/></a>
                </th>
                <th class="productOrdered">
                    <a href="<c:url value="/productSelectionConfigProductSearch.do?method=sort&order=byOrdered" />" title="<fmt:message key="stillToDeliverQuantity"/>"><fmt:message key="order"/></a>
                </th>
                <th class="productStock">
                    <a href="<c:url value="/productSelectionConfigProductSearch.do?method=sort&order=byStock" />" title="<fmt:message key="currentlyInInventory"/>"><fmt:message key="inventory"/></a>
                </th>
                <th class="productReceipt">
                    <a href="<c:url value="/productSelectionConfigProductSearch.do?method=sort&order=byReceipt" />" title="<fmt:message key="locatedInStockReceipt"/>"><fmt:message key="stockReceiptShort"/></a>
                </th>
                <th class="productExpected">
                    <a href="<c:url value="/productSelectionConfigProductSearch.do?method=sort&order=byExpected" />" title="<fmt:message key="orderedBySupplier"/>"><fmt:message key="purchaseOrderShort"/></a>
                </th>
                <th class="productPrice">
                    <a href="<c:url value="/productSelectionConfigProductSearch.do?method=sort&order=byConsumerPrice" />" title="<fmt:message key="orderedByConsumerPrice"/>"><fmt:message key="sale"/></a>
                </th>
                <th class="productPrice">
                    <a href="<c:url value="/productSelectionConfigProductSearch.do?method=sort&order=byResellerPrice" />" title="<fmt:message key="orderedByResellerPrice"/>"><fmt:message key="trading"/></a>
                </th>
            </tr>
        </thead>
        <tbody style="height:530px;">
            <c:choose>
                <c:when test="${empty view.list}">
                    <tr>
                        <td colspan="6"><fmt:message key="searchUnsuccessfulPleaseChangeYourInput"/>.</td>
                    </tr>
                </c:when>
                <c:otherwise>
                    <c:forEach var="product" items="${view.list}" varStatus="s">
                        <c:if test="${!view.ignoreLazy or !product.lazy}">
                            <c:set var="warning" value="${product.summary.purchaseOrdered}"/>
                            <tr<c:if test="${!warning}"> class="altrow"</c:if>>
                                <td class="productNumber">
                                    <a href="<c:url value="/products.do?method=load&id=${product.productId}&exit=${exitTarget}"/>"><o:out value="${product.productId}"/></a>
                                </td>
                                <td class="productName<c:if test="${warning}"> errortext</c:if>">
                                    <v:link url="/selections/productSelectionConfigsSelection/addProduct?id=${product.productId}">
                                        <o:out value="${product.name}"/>
                                    </v:link>
                                    
                                </td>
                                <td class="productOrdered">
                                    <c:choose>
                                        <c:when test="${product.summary.ordered > 0}">
                                            <a href="<c:url value="/salesPlanning.do?method=forward&unreleased=false&exit=${exitTarget}&id=${product.productId}&stockId=${product.currentStock}"/>">
                                                <o:number value="${product.summary.ordered}" format="integer"/>
                                            </a>
                                        </c:when>
                                        <c:otherwise>
                                            <o:number value="${product.summary.ordered}" format="integer"/>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td class="productStock">
                                    <a href="<c:url value="/products.do?method=enableStockReport&exit=${exitTarget}&id=${product.productId}"/>"><o:number value="${product.summary.stock}" format="integer"/></a>
                                </td>
                                <td class="productReceipt"><o:number value="${product.summary.receipt}" format="integer"/></td>
                                <td class="productExpected">
                                    <c:choose>
                                        <c:when test="${product.summary.expected > 0}">
                                            <a href="<c:url value="/purchaseOrder.do?method=forwardOpenByProduct&exit=${exitTarget}&id=${product.productId}"/>"><o:number value="${product.summary.expected}" format="integer"/></a>
                                        </c:when>
                                        <c:otherwise>
                                            <o:number value="${product.summary.expected}" format="integer"/>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td class="productPrice"><o:number value="${product.consumerPrice}" format="currency"/></td>
                                <td class="productPrice"><o:number value="${product.resellerPrice}" format="currency"/></td>
                            </tr>
                        </c:if>
                    </c:forEach>
                </c:otherwise>
            </c:choose>
        </tbody>
    </table>
</div>
