/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 27, 2006 12:27:11 PM 
 * 
 */
package com.osserp.gui.client.contacts.impl;

import com.osserp.common.ClientException;
import com.osserp.common.web.ViewName;

import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.ContactManager;
import com.osserp.core.contacts.ContactSearch;
import com.osserp.core.contacts.Person;
import com.osserp.core.contacts.PersonManager;
import com.osserp.core.contacts.Search;
import com.osserp.core.users.DomainUser;

import com.osserp.gui.client.contacts.ContactPersonView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("contactPersonView")
public class ContactPersonViewImpl extends AbstractContactView implements ContactPersonView {

    private Contact related = null;

    protected ContactPersonViewImpl() {
        super();
    }

    @Override
    public void load(Person person) throws ClientException {
        if (person instanceof Contact) {
            Contact contact = (Contact) person;
            ContactSearch search = getContactSearch();
            if (contact.getType().isChild()) {
                related = search.find(contact.getReference());
                setBean(contact);
            } else {
                related = contact;
            }
        }
    }

    @Override
    public void load(Long id) throws ClientException {
        ContactSearch search = getContactSearch();
        Contact person = search.find(id);
        load(person);
    }


    @Override
    protected void createPrivateContact() {
        DomainUser user = getDomainUser();
        Contact selected = getCurrent();
        getPrivateContactManager().createPrivate(user, selected, related);
    }

    @Override
    public boolean isRelatedContactView() {
        return false;
    }

    public Contact getRelated() {
        return related;
    }

    @Override
    protected PersonManager getPersonManager() {
        return (PersonManager) getService(ContactManager.class.getName());
    }

    protected ContactManager getContactManager() {
        return (ContactManager) getService(ContactManager.class.getName());
    }

    protected ContactSearch getContactSearch() {
        return (ContactSearch) getSearch();
    }

    private Search getSearch() {
        return (ContactSearch) getService(ContactSearch.class.getName());
    }
}
