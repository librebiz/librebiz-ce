/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 22, 2007 5:18:48 AM 
 * 
 */
package com.osserp.core.finance;

import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;

import com.osserp.core.Item;
import com.osserp.core.employees.Employee;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface OrderManager extends PaymentAgreementAwareRecordManager {

    /**
     * Updates the estimated delivery date
     * @param user
     * @param order
     * @param deliveryDate
     * @param deliveryNote
     * @param defines what to to with order item delivery dates, <br>
     * 'noItems' indicates that nothing should be done, <br>
     * 'allItems' updates the delivery date on all items <br>
     * 'emptyItems' updates all items with current empty date
     * @param deliveryConfirmed indicates that delivery is confirmed by supplier
     * @throws ClientException if date is in the past
     */
    void update(
            Employee user,
            Order order,
            Date deliveryDate,
            String deliveryNote,
            String itemBehaviour,
            boolean deliveryConfirmed) throws ClientException;

    /**
     * Updates the estimated delivery date for a specified item
     * @param user
     * @param order
     * @param estimatedDelivery
     * @param deliveryNote as internal note
     * @param item to update
     * @throws ClientException if date is in the past
     */
    void update(
            Employee user,
            Order order,
            Date estimatedDelivery,
            String deliveryNote,
            Item item) throws ClientException;

    /**
     * Changes the delivery date confirmation flag for an item
     * @param user
     * @param order
     * @param itemId
     */
    void changeDeliveryConfirmation(Employee user, Order order, Long itemId);

    /**
     * Changes the stock for an (product) item
     * @param order
     * @param productId
     * @param stockId
     */
    void changeStock(Order order, Long productId, Long stockId);

    /**
     * Creates a new version of the order
     * @param user
     * @param order
     * @param backupItems
     * @param backupContext
     */
    void createNewVersion(Employee user, Order order, boolean backupItems, String backupContext);

    /**
     * Indicates item backup availability
     * @param record
     * @param backupContext
     * @return true if item backup available
     */
    boolean isItemBackupAvailable(Record record, String backupContext);

    /**
     * Restores previous items by backup
     * @param user
     * @param record
     * @param backupContext
     * @return record
     */
    Record restoreItems(Employee user, Record record, String backupContext);

    /**
     * Closes all service deliveries
     * @param user
     * @param order to close
     */
    void closeService(Employee user, Order order);

    /**
     * Close a dedicated service delivery
     * @param user
     * @param order
     * @param item to close
     */
    void closeService(Employee user, Order order, Item item);

    /**
     * Provides all previous print versions of a sales order
     * @param order
     * @return versions
     */
    List<RecordDocument> findVersions(Order order);

    /**
     * Provides the pdf document of a version
     * @param document
     * @return version
     */
    byte[] getVersion(RecordDocument document);
}
