/**
 *
 * Copyright (C) 2008, 2017 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jul 7, 2020 
 * 
 */
package com.osserp.gui.requests

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.Option
import com.osserp.common.util.CollectionUtil

import com.osserp.core.requests.RequestListItem
import com.osserp.core.requests.RequestListItemComparators
import com.osserp.core.requests.RequestListManager
import com.osserp.core.users.DomainUser
import com.osserp.gui.CoreView

/**
 *
 * @author <a href="mailto:rk@osserp.com">rk</a>
 *
 */
class MyRequestsView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(MyRequestsView.class.getName())

    boolean includeCreatedBy = false

    MyRequestsView() {
        super()
        enableCompanyPreselection()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            executeQuery()
        }
    }

    @Override
    public void reload() {
        executeQuery()
    }

    void executeQuery() {
        List<RequestListItem> items = requestSearch.getCurrentByUser(domainUser)
        logger.debug("executeQuery: Fetched items. Count " + items.size())
        List tmpList = []
        items.each { RequestListItem next ->
            if ( (domainEmployee.id == next.getSalesId()) || (domainEmployee.id == next.getManagerId())
                ||  (includeCreatedBy && domainUser.id == next.getCreatedBy())) {
                tmpList << next
            }
        }
        logger.debug("executeQuery: Filter result done. Count " + tmpList.size())
        list = CollectionUtil.sort(tmpList,
            RequestListItemComparators.createRequestListItemByCreatedComparator(true));
        logger.debug("executeQuery: Done. Resulting list count " + list.size())
    }

    protected RequestListManager getRequestSearch() {
        getService(RequestListManager.class.getName())
    }
}
