/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 9, 2006 11:11:39 PM 
 * 
 */
package com.osserp.common.web.tags;

import java.math.BigDecimal;

import javax.servlet.jsp.JspException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.util.DateFormatter;
import com.osserp.common.util.NumberFormatter;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class NumberTag extends AbstractOutTag {
    private static Logger log = LoggerFactory.getLogger(NumberTag.class.getName());
    private static final String BYTES = "bytes";
    private static final String COUNT = "count";
    private static final String CURRENCY = "currency";
    private static final String DECIMAL = "decimal";
    private static final String INTEGER = "integer";
    private static final String HOURS = "hours";
    private static final String PERCENT = "percent";
    private static final String TAX = "tax";

    private boolean replaceNull = false;
    private String format = null;

    /**
     * The format
     * @return format.
     */
    public String getFormat() {
        return format;
    }

    /**
     * Sets the format
     * @param format to set.
     */
    public void setFormat(String format) {
        this.format = format;
    }

    /**
     * Indicates if null value should be replaced by number 0. Defaults to false so tag returns an empty string.
     * @return true if null should be replaced by formatted 0.00
     */
    public boolean isReplaceNull() {
        return replaceNull;
    }

    /**
     * Enable replacement of null
     * @param replaceNull
     */
    public void setReplaceNull(boolean replaceNull) {
        this.replaceNull = replaceNull;
    }

    @Override
    public String getOutput() throws JspException {
        if (format == null) {
            format = CURRENCY;
        }
        if (value == null) {
            return defaultValue();
        }
        try {
            if (format.equalsIgnoreCase(HOURS) && value instanceof Double) {
                return DateFormatter.getHoursAndMinutes((Double) value);
            }
            if (format.equalsIgnoreCase(BYTES)) {
                if (value instanceof Integer) {
                    return NumberFormatter.getBytesValue((Integer) value);
                }
                if (value instanceof Long) {
                    return NumberFormatter.getBytesValue(((Long) value).intValue());
                }
            }
            BigDecimal d = null;
            if (value instanceof BigDecimal) {
                d = (BigDecimal) value;
            } else if (value instanceof Double) {
                d = new BigDecimal(((Double) value).toString());
            } else if (value instanceof String) {
                d = new BigDecimal((String) value);
            }
            if (d != null) {
                return formattedValue(d);
            }
            return value.toString();
        } catch (Exception e) {
            return defaultValue();
        }
    }

    private String defaultValue() {
        return replaceNull ? formattedValue(new BigDecimal(0d)) : "";
    }

    private String formattedValue(BigDecimal d) {
        assert (d != null);
        if (format.equalsIgnoreCase(CURRENCY)) {
            return NumberFormatter.getValue(d, NumberFormatter.CURRENCY);
        } else if (format.equalsIgnoreCase(COUNT)) {
            return NumberFormatter.getValue(d, NumberFormatter.COUNT);
        } else if (format.equalsIgnoreCase(INTEGER)) {
            return NumberFormatter.getIntValue(d);
        } else if (format.equalsIgnoreCase(DECIMAL)) {
            return NumberFormatter.getValue(d, NumberFormatter.DECIMAL);
        } else if (format.equalsIgnoreCase(PERCENT)) {
            return NumberFormatter.getValue(d, NumberFormatter.PERCENT);
        } else if (format.equalsIgnoreCase(TAX)) {
            return NumberFormatter.getValue(d, NumberFormatter.TAX_RATE);
        }
        log.warn("formattedValue() found undefined format " + format);
        return d.toPlainString();
    }
}
