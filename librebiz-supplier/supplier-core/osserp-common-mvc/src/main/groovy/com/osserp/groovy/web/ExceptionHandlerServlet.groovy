/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on May 5, 2010 at 8:31:55 AM 
 * 
 */
package com.osserp.groovy.web

import javax.servlet.ServletException
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

import groovy.xml.XmlParser

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.User
import com.osserp.common.mail.Mail
import com.osserp.common.mail.MailSender
import com.osserp.common.service.Context
import com.osserp.common.service.Locator
import com.osserp.common.util.ExceptionUtil
import com.osserp.common.util.FileUtil
import com.osserp.common.web.ContextUtil
import com.osserp.common.web.RequestUtil

/**
 * 
 * @author jg <jg@osserp.com>
 * @author rk <rk@osserp.com>
 *
 */
public class ExceptionHandlerServlet extends HttpServlet {
    private static Logger logger = LoggerFactory.getLogger(ExceptionHandlerServlet.class.getName())
    
    private static final String ERRORS_FILE = 'errors.xml'
    
    def globalExceptions = []
    def defaultValues = null
    def ignorables = ['whitespace in the label text', 'error.timeout.session']

    void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        doGet(request, response)
    }

    boolean isIgnorable(String errorText) {
        boolean result = false
        if (errorText) {
            ignorables.each {
                if (errorText.contains(it)) {
                    result = true
                }
            }
        }
        result
    }

    void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        try {
            InputStream streamSource
            
            def errorConfigPath = getErrorConfigFile()
            if (errorConfigPath) {
                streamSource = new FileInputStream(new File(errorConfigPath))
            } else {
                try {
                    streamSource = FileUtil.class.getClassLoader().getResourceAsStream(ERRORS_FILE)
                    logger.debug("doGet: custom config not provided, found default ${ERRORS_FILE} file")
                } catch (Exception e) {
                    logger.error("doGet: exception reading ${ERRORS_FILE} as stream [message=${e.toString()}]", e)
                }
            }
            if (streamSource) {
                globalExceptions = []
                defaultValues = null
                parseErrorConfig(streamSource)

                if (defaultValues) {
                    Exception ex = null
                    Object obj = request.getAttribute('javax.servlet.error.exception')
                    if (!obj) {
                        obj = request.getAttribute('javax.servlet.jsp.jspException')
                    }
                    if (obj && obj instanceof Exception) {
                        ex = (Exception) obj
                    }
                    if (ex) {
                        
                        request.getSession(true).setAttribute('errorSource', ex)
                        logger.debug('doGet: caused by ' + ex.toString() + ' [requestUri=' + request.getAttribute("javax.servlet.error.request_uri") + ']')

                        def exceptionDefinition = globalExceptions.find { it.type?.equals(ex.class.getName()) }
                        def page = defaultValues.page
                        if (exceptionDefinition) {
                            page = handleException(exceptionDefinition, ex, request)
                        } else {
                            page = handleException(defaultValues, ex, request)
                        }
                        response.sendRedirect(request.getAttribute('javax.servlet.forward.context_path') + page)
                    } else {
                        logger.debug('doGet: no exception found, send redirect to default page')
                        response.sendRedirect(request.getAttribute('javax.servlet.forward.context_path') + defaultValues.page)
                    }
                }
            } else {
                logger.error('doGet: init param errorConfigPath not set in servlet config')
            }
        } catch (Exception ex) {
            // Throwing exceptions from this method can result in request
            // going in to an infinite loop forwarding to the error servlet recursively.
            logger.error('doGet: error while handling exception')
            logger.error(ExceptionUtil.stackTraceToString(ex))
        }
    }

    def handleException(ExceptionDefinition definition, Exception ex, HttpServletRequest request) {
        if (definition.type.indexOf('imeout') < 0) {
            logger.debug("handleException: invoked [type=$definition.type,level=$definition.level,mail=$definition.mail,page=$definition.page]", ex)
            String stackTraceString = ExceptionUtil.stackTraceToString(ex)
            if (definition.level.equals('log') || definition.level.equals('mail')) {
                logger.error(stackTraceString)
            }
            if (!isIgnorable(stackTraceString) && (definition.level.equals('mail') || definition.level.equals('mailonly'))) {
                Locator serviceLocator = ContextUtil.getServiceLocator(request)
                if (serviceLocator) {
                    MailSender mailSender = serviceLocator.lookupService(MailSender.class.getName())
                    if (mailSender) {
                        StringBuffer buffer = new StringBuffer(2048)
                        buffer.append("To: ${definition.mail}\n")
                        buffer.append('Session dump: ').append(RequestUtil.createSessionDump(request.session)).append('\n\n')
                        buffer.append('Exeption caught: ').append(ex.getClass().getName())
                        buffer.append(getUserInfo(request)).append('\n\n')
                        buffer.append("Request URI: ").append(request.getAttribute("javax.servlet.error.request_uri"))
                        buffer.append("\nRequest params:")
                        request.parameterNames.each() {
                            buffer.append("\n").append(it).append(": ").append(request.getParameter(it))
                        }
                        buffer.append("\n\nStackTrace:\n").append(stackTraceString)

                        Mail mail = new Mail('info@osserp.com', "Subject: Error-Info ($definition.type)", buffer.toString())
                        def mailAddresses = definition.mail.tokenize(',')
                        mailAddresses.each() {
                            it = it.trim()
                            try {
                            } catch (Exception e) {
                                logger.warn("handleException: found invalid recipient [address=$it]")
                            }
                        }
                        if (mail.recipients) {
                            try {
                                mailSender.send(mail)
                            } catch (Exception e) {
                                logger.warn("handleException: failed sending error report [exception=${e.getMessage()}]")
                            }
                        }
                    } else {
                        logger.error('handleException: mailSender not bound')
                    }
                } else {
                    logger.error('handleException: serviceLocator not available')
                }
            }
        }
        return definition.page
    }

    def parseErrorConfig(InputStream streamSource) {
        def errorConfig = new XmlParser().parse(streamSource)
        def errorDefaults = errorConfig.default[0]
        if (!errorDefaults) {
            logger.error('parseErrorConfig: required section default is missing')
        } else {
            defaultValues = new ExceptionDefinition(errorDefaults.type.text(), errorDefaults.level.text(), errorDefaults.mail.text(), errorDefaults.page.text())
            if (!defaultValues.level || (!defaultValues.level.equals('ignore') && !defaultValues.level.equals('mail') && !defaultValues.level.equals('mailonly') && !defaultValues.level.equals('log'))) {
                logger.error('parseErrorConfig: required default parameter level is missing or has a incorrect value')
                defaultValues = null
            } else if (!defaultValues.mail) {
                logger.error('parseErrorConfig: required default parameter mail is missing')
                defaultValues = null
            } else if (!defaultValues.page) {
                logger.error('parseErrorConfig: required default parameter page is missing')
                defaultValues = null
            } else {
                errorConfig.exception.each() {
                    if (it.type.text()) {
                        def level
                        if (!it.level.text() || (it.level.text().equals('ignore') && it.level.text().equals('mail') && it.level.text().equals('mailonly') && it.level.text().equals('log'))) {
                            level = defaultValues.level
                        } else {
                            level = it.level.text()
                        }
                        def mail = it.mail.text() ?: defaultValues.mail
                        def page = it.page.text() ?: defaultValues.page
                        globalExceptions <<  new ExceptionDefinition(it.type.text(), level, mail, page)
                    }
                }
            }
        }
    }

    private String getUserInfo(HttpServletRequest request) {
        StringBuffer buffer = new StringBuffer('\nUser: ')
        User user = request.session.getAttribute('user')
        if (user) {
            buffer.append(user.id).append(', ')
            buffer.append(user.loginName)
            buffer.append('\nEmployee: ')
            buffer.append(user.employee?.name?:null)
        } else {
            buffer.append('no user logged in')
        }
        return buffer.toString()
    }

    private String getErrorConfigFile() {
        def errorConfig = getServletConfig().getInitParameter('errorConfigPath')
        if (!errorConfig) {
            String osserpConf = Context.getConfigLocation()
            errorConfig = "${osserpConf}/${ERRORS_FILE}"
        }
        File cfgFile = new File(errorConfig)
        if (cfgFile.exists()) {
            logger.debug("getErrorConfigFile: done [file=$errorConfig]")
            return errorConfig
        }
        return null
    }
}

