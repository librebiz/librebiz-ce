/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 16-Jan-2007 11:17:30 
 * 
 */
package com.osserp.core.service.contacts;

import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.ContactAssignmentManager;
import com.osserp.core.customers.Customer;
import com.osserp.core.dao.Customers;
import com.osserp.core.dao.Suppliers;
import com.osserp.core.employees.Employee;
import com.osserp.core.service.impl.AbstractService;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ContactAssignmentManagerImpl extends AbstractService implements ContactAssignmentManager {

    private Customers customers = null;
    private Suppliers suppliers = null;

    /**
     * Creates a new ContactAssignmentManager
     * @param customers
     * @param suppliers
     */
    public ContactAssignmentManagerImpl(Customers customers, Suppliers suppliers) {

        this.customers = customers;
        this.suppliers = suppliers;
    }

    public Customer assignCustomer(Employee createdBy, Contact contact) {

        if (contact.isCustomer()) {
            ClassifiedContact cus = customers.findByContact(contact.getContactId());
            if (cus != null) {
                return (Customer) cus;
            }
        }
        return (Customer) customers.create((createdBy == null ? null : createdBy.getId()), contact, null);
    }

    public Long assignSupplier(Employee createdBy, Contact contact) {
        if (contact.isSupplier()) {
            ClassifiedContact cus = suppliers.findByContact(contact.getContactId());
            if (cus != null) {
                return cus.getId();
            }
        }
        return suppliers.create((createdBy == null ? null : createdBy.getId()), contact, null).getId();
    }
}
