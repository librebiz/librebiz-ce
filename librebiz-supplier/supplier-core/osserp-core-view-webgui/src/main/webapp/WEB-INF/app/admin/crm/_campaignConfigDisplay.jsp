<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <o:out value="${view.bean.id}" />
                -
                <o:out value="${view.bean.name}" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
    
        <c:if test="${!empty view.selectedCompany && view.multipleClientsAvailable}">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="company"><fmt:message key="company" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <oc:options name="systemCompanies" value="${view.bean.company}"/>
                    </div>
                </div>
            </div>
        </c:if>

        <c:if test="${!empty view.bean.reference}">
            <c:choose>
                <c:when test="${view.changeParentMode}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="name"><fmt:message key="subCampaignOf" /></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <select name="id" onchange="gotoUrl(this.value);" class="form-control">
                                    <option value="<v:url value="/admin/crm/campaignConfig/disableChangeParentMode"/>"><fmt:message key="selection"/></option>
                                    <c:forEach var="obj" items="${view.availableParents}">
                                        <option value="<v:url value="/admin/crm/campaignConfig/changeParent?id=${obj.id}"/>"><o:out value="${obj.name}"/></option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="subCampaignOf"><v:link url="/admin/crm/campaignConfig/enableChangeParentMode"><fmt:message key="subCampaignOf"/></v:link></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <o:out value="${view.parent.name}" />
                            </div>
                        </div>
                    </div>
                </c:otherwise>
             </c:choose>
        </c:if>
        
        <c:if test="${!empty view.bean.branch}">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="branch"><fmt:message key="branch" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <oc:options name="branchOffices" value="${view.bean.branch}"/>
                    </div>
                </div>
            </div>
        </c:if>
        
        <c:if test="${!empty view.bean.group}">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="branch"><fmt:message key="group" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:out value="${view.bean.group.name}"/>
                    </div>
                </div>
            </div>
        </c:if>
        
        <c:if test="${!empty view.bean.type}">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="type"><fmt:message key="type" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:out value="${view.bean.type.name}"/>
                    </div>
                </div>
            </div>
        </c:if>
        
        <c:if test="${!empty view.bean.reach}">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="reach"><fmt:message key="reachLabel" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <fmt:message key="${view.bean.reach}"/>
                    </div>
                </div>
            </div>
        </c:if>
        
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="parentCampaign"><fmt:message key="parentCampaignLabel" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <c:choose><c:when test="${view.bean.parent}"><fmt:message key="bigYes"/></c:when><c:otherwise><fmt:message key="bigNo"/></c:otherwise></c:choose>
                </div>
            </div>
        </div>
        
        <c:if test="${!empty view.bean.description}">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="description"><fmt:message key="description" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:out value="${view.bean.description}"/>
                    </div>
                </div>
            </div>
        </c:if>

        <c:choose>
            <c:when test="${empty view.bean.campaignStarts and empty view.bean.campaignEnds}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="campaignStart">
                                <fmt:message key="runtimeLabel"/>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                           <fmt:message key="unlimited"/>
                        </div>
                    </div>
                </div>
            </c:when>
            <c:when test="${!empty view.bean.campaignStarts}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="campaignStart">
                                <fmt:message key="campaignStart" />
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:date value="${view.bean.campaignStarts}"/>
                        </div>
                    </div>
                </div>
            </c:when>
        </c:choose>
        
        <c:if test="${!empty view.bean.campaignEnds or !empty view.bean.campaignStarts}">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="campaignEnd"><fmt:message key="campaignEnd" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${empty view.bean.campaignEnds}">
                                <fmt:message key="undefinedEndDate"/>
                            </c:when>
                            <c:otherwise>
                                <o:date value="${view.bean.campaignEnds}"/>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>
        </c:if>
        
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="deactivatedStatusDisplay"><fmt:message key="deactivatedLabel" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <c:choose>
                        <c:when test="${view.bean.endOfLife}">
                            <fmt:message key="bigYes"/>
                        </c:when>
                        <c:otherwise>
                            <fmt:message key="bigNo"/>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>
        
        <c:if test="${view.campaignMovable}">
            <c:choose>
                <c:when test="${view.changeParentMode}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="name"><fmt:message key="subCampaignOf" /></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <select name="id" class="form-control" onchange="gotoUrl(this.value);">
                                    <option value="<v:url value="/admin/crm/campaignConfig/disableChangeParentMode"/>"><fmt:message key="selection"/></option>
                                    <c:forEach var="obj" items="${view.availableParents}">
                                        <c:if test="${view.bean.id != obj.id}">
                                            <option value="<v:url value="/admin/crm/campaignConfig/changeParent?id=${obj.id}"/>"><o:out value="${obj.name}"/></option>
                                        </c:if>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="row">
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <v:link url="/admin/crm/campaignConfig/enableChangeParentMode">
                                    <fmt:message key="moveCampaign"/>
                                </v:link>
                            </div>
                        </div>
                    </div>
                </c:otherwise>
             </c:choose>
        </c:if>

    </div>
</div>
