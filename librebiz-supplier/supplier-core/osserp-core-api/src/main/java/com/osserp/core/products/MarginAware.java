/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 26, 2011 11:57:50 AM 
 * 
 */
package com.osserp.core.products;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface MarginAware {

    /**
     * Provides the consumer margin for auto calculation
     * @return consumer margin
     */
    Double getConsumerMargin();

    /**
     * Sets the consumer margin
     * @param consumerMargin
     */
    void setConsumerMargin(Double consumerMargin);

    /**
     * Provides the consumer minimum margin
     * @return consumerMinimumMargin
     */
    Double getConsumerMinimumMargin();

    /**
     * Sets the consumer minimum margin
     * @param consumerMinimumMargin
     */
    void setConsumerMinimumMargin(Double consumerMinimumMargin);

    /**
     * Provides the partner margin for auto calculation
     * @return partner margin
     */
    Double getPartnerMargin();

    /**
     * Sets the partner margin
     * @param partnerMargin
     */
    void setPartnerMargin(Double partnerMargin);

    /**
     * Provides the partner minimum margin
     * @return partnerMinimumMargin
     */
    Double getPartnerMinimumMargin();

    /**
     * Sets the partner minimum margin
     * @param partnerMinimumMargin
     */
    void setPartnerMinimumMargin(Double partnerMinimumMargin);

    /**
     * Provides the reseller margin for auto calculation
     * @return reseller margin
     */
    Double getResellerMargin();

    /**
     * Sets the reseller margin
     * @param resellerMargin
     */
    void setResellerMargin(Double resellerMargin);

    /**
     * Provides the reseller minimum margin
     * @return resellerMinimumMargin
     */
    Double getResellerMinimumMargin();

    /**
     * Sets the reseller minimum margin
     * @param resellerMinimumMargin
     */
    void setResellerMinimumMargin(Double resellerMinimumMargin);

}
