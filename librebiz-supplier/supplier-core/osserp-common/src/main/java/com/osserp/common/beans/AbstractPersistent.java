/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 2, 2007 
 * 
 */
package com.osserp.common.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import org.jdom2.Element;

import com.osserp.common.xml.JDOMUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractPersistent extends AbstractClass implements Serializable {

    private Long reference;

    private Date created;
    private Long createdBy;

    private Date changed;
    private Long changedBy;

    private boolean endOfLife = false;

    /**
     * Default constructor adding serialializable capability to entities
     */
    protected AbstractPersistent() {
        super();
        created = new Date(System.currentTimeMillis());
    }

    /**
     * Id assigning constructor
     * @param created date
     */
    protected AbstractPersistent(Date created) {
        super();
        this.created = created;
    }

    /**
     * Creates a new entity assigning id and reference.
     * @param reference
     */
    protected AbstractPersistent(Long reference) {
        this();
        this.reference = reference;
    }

    /**
     * Creates a new entity assigning id, reference and creator.
     * @param reference
     * @param createdBy
     */
    protected AbstractPersistent(Long reference, Long createdBy) {
        this();
        this.reference = reference;
        this.createdBy = createdBy;
    }

    /**
     * Creates a new entity assigning id, reference and creator.
     * @param reference
     * @param created
     * @param createdBy
     */
    protected AbstractPersistent(Long reference, Date created, Long createdBy) {
        this();
        this.reference = reference;
        if (created != null) {
            this.created = created;
        }
        this.createdBy = createdBy;
    }

    /**
     * Creates a new entity assigning all properties.
     * @param id
     * @param reference
     * @param created
     * @param createdBy
     * @param changed
     * @param changedBy
     * @param endOfLife
     */
    protected AbstractPersistent(
            Long id,
            Long reference,
            Date created,
            Long createdBy,
            Date changed,
            Long changedBy,
            boolean endOfLife) {
        this();
        this.reference = reference;
        if (created != null) {
            this.created = created;
        }
        this.createdBy = createdBy;
    }

    /**
     * Creates a new instance with all values copied by other
     * @param other
     */
    protected AbstractPersistent(AbstractPersistent other) {
        super();
        created = other.getCreated();
        createdBy = other.getCreatedBy();
        changed = other.getChanged();
        changedBy = other.getChangedBy();
        endOfLife = other.isEndOfLife();
        reference = other.getReference();
    }

    /**
     * Constructs a new instance by xml element. 
     * Values will be retrieved by child elements.
     * @param xml
     */
    protected AbstractPersistent(Element xml) {
        reference = JDOMUtil.fetchLong(xml, "reference");
        created = JDOMUtil.fetchDate(xml, "created");
        createdBy = JDOMUtil.fetchLong(xml, "createdBy");
        changed = JDOMUtil.fetchDate(xml, "changed");
        changedBy = JDOMUtil.fetchLong(xml, "changedBy");
        endOfLife = JDOMUtil.fetchBoolean(xml, "endOfLife");
    }

    /**
     * Creates a new instance with all values copied found in the map
     * @param map
     */
    protected AbstractPersistent(Map<String, Object> map) {
        super();
        reference = fetchLong(map, "reference");
        created = fetchDate(map, "created");
        createdBy = fetchLong(map, "createdBy");
        changed = fetchDate(map, "changed");
        changedBy = fetchLong(map, "changedBy");
        endOfLife = fetchBoolean(map, "endOfLife");
    }

    @Override
    protected Map<String, Object> getMapped() {
        Map<String, Object> map = super.getMapped();
        map.put("reference", reference);
        putFormatted(map, "created", created);
        map.put("createdBy", createdBy);
        putFormatted(map, "changed", changed);
        map.put("changedBy", changedBy);
        map.put("endOfLife", endOfLife);
        return map;
    }

    public Long getReference() {
        return reference;
    }

    public void setReference(Long reference) {
        this.reference = reference;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Date getChanged() {
        return changed;
    }

    public void setChanged(Date changed) {
        this.changed = changed;
    }

    public Long getChangedBy() {
        return changedBy;
    }

    public void setChangedBy(Long changedBy) {
        this.changedBy = changedBy;
    }

    public final boolean isEndOfLife() {
        return endOfLife;
    }

    public final void setEndOfLife(boolean endOfLife) {
        this.endOfLife = endOfLife;
    }
    
    public final void updateChanged(Long user) {
        this.changedBy = user;
        this.changed = new Date(System.currentTimeMillis());
    }

    protected void update(AbstractPersistent other) {
        created = other.getCreated();
        createdBy = other.getCreatedBy();
        changed = other.getChanged();
        changedBy = other.getChangedBy();
        endOfLife = other.isEndOfLife();
    }

    public Element getXML() {
        return getXML("root");
    }

    public Element getXML(String rootElementName) {
        Element root = new Element(rootElementName);
        root.addContent(JDOMUtil.createElement("reference", reference));
        root.addContent(JDOMUtil.createElement("created", created));
        root.addContent(JDOMUtil.createElement("createdBy", createdBy));
        root.addContent(JDOMUtil.createElement("changed", changed));
        root.addContent(JDOMUtil.createElement("changedBy", changedBy));
        root.addContent(JDOMUtil.createElement("endOfLife", endOfLife));
        return root;
    }
}


