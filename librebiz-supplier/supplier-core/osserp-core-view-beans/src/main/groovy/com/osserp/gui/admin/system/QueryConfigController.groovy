/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 11.05.2011 09:42:37 
 * 
 */
package com.osserp.gui.admin.system

import javax.servlet.http.HttpServletRequest

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.groovy.web.AdminViewController

/**
 *
 * @author eh <eh@osserp.com>
 * @author rk <rk@osserp.com>
 *
 */
@RequestMapping("/admin/system/queryConfig/*")
@Controller class QueryConfigController extends AdminViewController {
    private static Logger logger = LoggerFactory.getLogger(QueryConfigController.class.getName())

    @RequestMapping
    def clearParameters(HttpServletRequest request) {
        logger.debug("clearParameters: invoked [user=${getUserId(request)}]")
        QueryConfigView view = getView(request)
        view.clearParameters()
        return defaultPage
    }

    @RequestMapping
    def clearOutput(HttpServletRequest request) {
        logger.debug("clearOutput: invoked [user=${getUserId(request)}]")
        QueryConfigView view = getView(request)
        view.clearOutput()
        return defaultPage
    }

    @RequestMapping
    def toggleFullscreen(HttpServletRequest request) {
        logger.debug("toggleFullscreen: invoked [user=${getUserId(request)}]")
        QueryConfigView view = getView(request)
        view.toggleFullscreen()
        return defaultPage
    }

    @RequestMapping
    def toggleActivation(HttpServletRequest request) {
        logger.debug("toggleActivation: invoked [user=${getUserId(request)}]")
        QueryConfigView view = getView(request)
        view.toggleActivation()
        return defaultPage
    }
}
