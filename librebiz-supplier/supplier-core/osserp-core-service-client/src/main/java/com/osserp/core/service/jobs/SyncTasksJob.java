/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 22.12.2013 
 * 
 */
package com.osserp.core.service.jobs;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.BackendException;
import com.osserp.common.service.Locator;
import com.osserp.common.service.impl.AbstractJob;

import com.osserp.core.dao.SyncTasks;
import com.osserp.core.system.SyncTask;
import com.osserp.core.system.SyncTaskExcecutor;

/**
 * 
 * {@code SyncTasksJob} reads open syncTask objects and executes required synchronization services.
 * 
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SyncTasksJob extends AbstractJob {
    private static Logger log = LoggerFactory.getLogger(SyncTasksJob.class.getName());
    private SyncTasks syncTasks;

    protected SyncTasksJob(Locator locator, SyncTasks syncTasks) {
        super(locator);
        this.syncTasks = syncTasks;
    }

    @Override
    protected void execute() {
        List<SyncTask> openTasks = syncTasks.findOpen();
        if (!openTasks.isEmpty()) {
            for (int i = 0, j = openTasks.size(); i < j; i++) {
                SyncTask syncTask = openTasks.get(i);
                boolean runningExists = syncTasks.runningTaskExists(syncTask.getConfig());
                if (syncTask.getConfig().isSyncEnabled() && syncTask.isOpen() && !runningExists) {
                    if (log.isDebugEnabled()) {
                        log.debug("execute() found open task [id=" + syncTask.getId() 
                                + ", type=" + syncTask.getConfig().getName() + "]");
                    }
                    try {
                        syncTasks.lock(syncTask);
                        execute(syncTask);
                        syncTasks.close(syncTask);
                    } catch (Exception e) {
                        String message = e.getMessage();
                        Throwable t = e;
                        if (message == null && e.getCause() != null) {
                            t = e.getCause();
                            message = t.getMessage();
                        }
                        log.error("execute() failed [message=" + message
                                + ", exception=" + t.getClass().getName() + "]", e);
                        syncTasks.stop(syncTask, message);
                    }
                } else if (runningExists && log.isInfoEnabled()) { 
                    log.info("execute() ignoring open 'cause running task exists [ignoring=" + syncTask.getId()
                            + ", config=" + syncTask.getConfig().getId() + "]");
                }
            }
        }
    }

    private void execute(SyncTask syncTask) throws Exception {
        SyncTaskExcecutor executor = getSyncTaskExcecutor(syncTask);
        executor.execute(syncTask);
    }

    private SyncTaskExcecutor getSyncTaskExcecutor(SyncTask syncTask) {
        try {
            return (SyncTaskExcecutor) getService(syncTask.getConfig().getServiceName());
        } catch (Throwable e) {
            log.error("getSyncTaskExecutor() failed [message=" + e.getMessage() + "]", e);
            throw new BackendException("service not available: " + syncTask.getConfig().getServiceName());
        }
    }
}
