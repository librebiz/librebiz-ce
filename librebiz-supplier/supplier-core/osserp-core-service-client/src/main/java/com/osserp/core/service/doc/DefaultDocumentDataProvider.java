/**
 *
 * Copyright (C) 2015 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 5, 2015 
 * 
 */
package com.osserp.core.service.doc;

import org.jdom2.Document;
import org.jdom2.Element;

import com.osserp.common.ClientException;
import com.osserp.common.Entity;

import com.osserp.core.BusinessCase;
import com.osserp.core.BusinessTemplate;
import com.osserp.core.dms.Stylesheet;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class DefaultDocumentDataProvider implements CoreDocumentDataProvider {
    
    private XmlService xmlService;
    
    protected DefaultDocumentDataProvider(XmlService xmlService) {
        super();
        this.xmlService = xmlService;
    }
    
    public Document getData(BusinessTemplate template, Entity object, Document existing) throws ClientException {
        if (object instanceof BusinessCase) {
            return getData(template, (BusinessCase) object, existing);
        }
        return null;
    }

    public Document getData(DomainUser user, BusinessTemplate template, Entity object) throws ClientException {
        assert user != null;
        Document document = xmlService.createDocument(user.getEmployee());
        return getData(template, object, document);
    }

    public Document getData(DomainUser user, BusinessTemplate template, BusinessCase businessCase) throws ClientException {
        assert user != null;
        Document document = xmlService.createDocument(user.getEmployee());
        return getData(template, businessCase, document);
    }

    public Document getData(BusinessTemplate template, BusinessCase businessCase, Document existing) throws ClientException {
        Document result = existing != null ? existing : new Document();
        Element root = result.getRootElement();
        root.addContent(getBusinessCaseElement(businessCase, true));
        return result;
    }
    
    public Document getData(DomainUser user, String documentProviderName) throws ClientException {
        try {
            if (Stylesheet.PHONE_LIST.equals(documentProviderName)) {
                return xmlService.getEmployees(user.getEmployee());
            }
        } catch (Exception e) {
            
        }
        return new Document(new Element("root"));
    }
    
    protected final Element getBusinessCaseElement(BusinessCase businessCase, boolean addContact) {
        Element root = xmlService.createBusinessCase("businessCase", businessCase);
        if (addContact && businessCase.getCustomer() != null) {
            root.addContent(businessCase.getCustomer().getXML());
        }
        return addCustomData(root, businessCase);
    }

    protected Element addCustomData(Element businessCaseElement, BusinessCase businessCase) {
        return businessCaseElement;
    }
}
