/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 3, 2008 8:40:39 AM 
 * 
 */
package com.osserp.gui.web.purchasing;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ActionException;
import com.osserp.common.web.RequestUtil;
import com.osserp.gui.client.purchasing.PurchaseOrderQueryView;
import com.osserp.gui.web.struts.AbstractViewDispatcherAction;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PurchaseOrderQueryAction extends AbstractViewDispatcherAction {
    private static Logger log = LoggerFactory.getLogger(PurchaseOrderQueryAction.class.getName());

    @Override
    protected Class<PurchaseOrderQueryView> getViewClass() {
        return PurchaseOrderQueryView.class;
    }

    /**
     * Refreshs and displays list by current settings
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward display(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("display() request from " + getUser(session).getId());
        }
        PurchaseOrderQueryView view = getPurchaseOrderQueryView(session);
        view.refresh();
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Toggles between display confirmed and display not confirmed records
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward toggleDisplayAllMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("toggleDisplayAllMode() request from " + getUser(session).getId());
        }
        PurchaseOrderQueryView view = getPurchaseOrderQueryView(session);
        view.toggleDisplayAllMode();
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Toggles between display confirmed and display not confirmed records
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward toggleDisplayConfirmedMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("toggleDisplayConfirmedMode() request from " + getUser(session).getId());
        }
        PurchaseOrderQueryView view = getPurchaseOrderQueryView(session);
        view.toggleDisplayConfirmedMode();
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Toggles between display confirmed and display not confirmed records
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward toggleDisplayOverdueOnlyMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("toggleDisplayOverdueOnlyMode() request from " + getUser(session).getId());
        }
        PurchaseOrderQueryView view = getPurchaseOrderQueryView(session);
        view.toggleDisplayOverdueOnlyMode();
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Selects/Deselects a supplier
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward selectSupplier(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("selectSupplier() request from " + getUser(session).getId());
        }
        PurchaseOrderQueryView view = getPurchaseOrderQueryView(session);
        view.selectSupplier(RequestUtil.fetchId(request));
        return mapping.findForward(view.getActionTarget());
    }

    private PurchaseOrderQueryView getPurchaseOrderQueryView(HttpSession session) {
        PurchaseOrderQueryView view = (PurchaseOrderQueryView) fetchView(session);
        if (view == null) {
            throw new ActionException(ActionException.NO_VIEW_BOUND);
        }
        return view;
    }
}
