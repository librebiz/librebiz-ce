<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" scope="page" value="${sessionScope.telephoneGroupView}"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="title"><o:displayTitle/></tiles:put>
	<tiles:put name="javascript" type="string">
		<script type="text/javascript">
			telephoneGroups = new Object();
			telephoneGroups.checkSearchException = function() {
				try {
					if ($('ajaxContent')) {
						$('ajaxContent').innerHTML = "<div style='width: 100%; text-align: center;'><div style='text-align:center; border:1px solid #D6DDE6; padding:10px;'><img src='/osdb/img/ajax_loader.gif' style='margin:5px;'/><br/>"+ ojsI18n.loading+"</div></div>";
					}
					return true;
				} catch (e) {
					return true;
				}
			}
		</script>
	</tiles:put>
	<tiles:put name="headline"><fmt:message key="telephoneGroups"/></tiles:put>
	<tiles:put name="headline_right">
		<c:set var="navigation" scope="request" value="${view.navigation}"/>
		<c:import url="../../shared/_navigation.jsp"/>
	</tiles:put>
	<tiles:put name="content" type="string">
		<c:if test="${!empty sessionScope.error}">
			&nbsp;
			<div class="errormessage">
				<fmt:message key="error"/>: <fmt:message key="${sessionScope.error}"/>
			</div>
            <o:removeErrors/>
		</c:if>
		<div class="spacer"></div>
		<div class="subcolumns">
			<div class="subcolumn">
				<div class="contentBox">
					<div class="contentBoxData">
						<div class="spacer"></div>
						<o:ajaxLookupForm name="dynamicForm" url="/app/telephones/telephoneGroup/search" targetElement="ajaxContent" events="onkeyup" preRequest="if (telephoneGroups.checkSearchException()) {" postRequest="}">
							<div class="subcolumns">
								<div class="column33l">
									<div class="subcolumnl">
										<select id="system" name="system" style="width: 210px;" onChange="javascript: $('dynamicForm').onkeyup();">
											<option value="" <c:if test="${empty view.telephoneSystem}">selected="selected"</c:if>><fmt:message key="allTelephoneGroups"/></option>
											<c:forEach var="obj" items="${view.telephoneSystems}">
												<option value="${obj.id}" <c:if test="${obj.id == view.telephoneSystem}">selected="selected"</c:if>><o:out value="${obj.name}"/></option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>
						</o:ajaxLookupForm>
						<div class="spacer"></div>
					</div>
				</div>
				<div class="spacer"></div>
				<div id="ajaxDiv">
					<div id="ajaxContent">
						<c:import url="_telephoneGroup.jsp"/>
					</div>
				</div>
			</div>
		</div>
		<div class="spacer"></div>
	</tiles:put>
</tiles:insert>
