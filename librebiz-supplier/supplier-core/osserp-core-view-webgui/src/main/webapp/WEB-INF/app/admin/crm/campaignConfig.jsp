<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="campaignConfigView"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="headline">
		<c:choose>
			<c:when test="${!empty view.parent}">
				<fmt:message key="subCampaignConfig"/>
			</c:when>
			<c:otherwise>
				<fmt:message key="campaignConfig"/>
			</c:otherwise>
		</c:choose>
	</tiles:put>
	<tiles:put name="headline_right">
		<v:navigation/>
	</tiles:put>

	<tiles:put name="content" type="string" >
        <div class="content-area" id="setupDetailsContent">
            <div class="row">
                <c:choose>
                    <c:when test="${view.createMode}">
                        <v:form id="campaignConfigForm" url="/admin/crm/campaignConfig/save">
                            <c:import url="_campaignConfigCreate.jsp"/>
                        </v:form>
                    </c:when>
                    <c:when test="${view.editMode}">
                        <v:form id="campaignConfigForm" url="/admin/crm/campaignConfig/save">
                            <c:import url="_campaignConfigEdit.jsp"/>
                        </v:form>
                    </c:when>
                    <c:when test="${view.bean != null}">
                        <c:import url="_campaignConfigDisplay.jsp"/>
                    </c:when>
                    <c:otherwise>
                        <c:import url="_campaignConfigList.jsp"/>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
	</tiles:put>
</tiles:insert>
