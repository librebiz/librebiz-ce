/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 28, 2008 12:59:56 AM 
 * 
 */
package com.osserp.common.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdom2.Document;
import org.jdom2.Element;

import com.osserp.common.service.Message;
import com.osserp.common.service.MessageSender;
import com.osserp.common.xml.JDOMUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class AbstractMessageInitiator {
    private static Logger log = LoggerFactory.getLogger(AbstractMessageInitiator.class.getName());

    private MessageSender sender = null;
    private String destinationQueue = null;
    private String serviceName = null;

    public AbstractMessageInitiator(MessageSender sender, String destinationQueue, String serviceName) {
        super();
        this.sender = sender;
        this.destinationQueue = destinationQueue;
        this.serviceName = serviceName;
        if (log.isDebugEnabled()) {
            log.debug("<init> done [destinationQueue=" + destinationQueue + ", serviceName=" + serviceName + "]");
        }
    }

    protected void send(Element root, byte[] file) {
        Document doc = new Document();
        doc.setRootElement(root);
        String xml = JDOMUtil.getCompactString(doc);
        send(xml, file);
    }

    protected void send(Document doc, byte[] file) {
        String xml = JDOMUtil.getCompactString(doc);
        send(xml, file);
    }

    protected void send(String xml, byte[] file) {
        Message message = new Message(serviceName, xml, file);
        sender.send(destinationQueue, message);
        if (log.isDebugEnabled()) {
            log.debug("send(xml, file) done [service=" + serviceName
                    + (file == null ? "]" : (", filesize=" + file.length) + "]"));
        }
    }

    protected void send(String string) {
        Message message = new Message(serviceName, string);
        sender.send(destinationQueue, message);
        if (log.isDebugEnabled()) {
            log.debug("send(string) done [service=" + serviceName + ", string=" + string + "]");
        }
    }

    protected void send(Object data) {
        Message message = new Message(serviceName, data);
        sender.send(destinationQueue, message);
        if (log.isDebugEnabled()) {
            log.debug("send(data) done [service=" + serviceName
                    + ", dataClass=" + data.getClass().getName() + "]");
        }
    }

    protected void send(Object data, byte[] file) {
        Message message = new Message(serviceName, data, file);
        sender.send(destinationQueue, message);
        if (log.isDebugEnabled()) {
            log.debug("send(data, file) done [service=" + serviceName
                    + ", dataClass=" + data.getClass().getName() + "]");
        }
    }
}
