/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 13, 2006 3:30:22 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import com.osserp.common.OptionsCache;

import com.osserp.core.model.SalesListItemVO;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesListItemRowMapper extends BusinessObjectRowMapper {
//    public static final String SELECT_ALL = "SELECT * ";
    public static final String SELECT_ALL_FROM = "SELECT * FROM ";
    private boolean supportsLastAction = false;

    protected SalesListItemRowMapper(OptionsCache optionsCache, boolean supportsLastAction) {
        super(optionsCache);
        this.supportsLastAction = supportsLastAction;
    }

    public Object mapRow(ResultSet rs, int index) throws SQLException {
        return createItem(rs, index);
    }

    protected SalesListItemVO createItem(ResultSet rs, int index) throws SQLException {
        //Long salutation = Long.valueOf(rs.getLong(1));
        String firstName = fetchString(rs, "firstname");
        String lastName = fetchString(rs, "lastname");
        Long contactType = fetchLong(rs, "contact_type");
        String shortName = lastName;
        StringBuilder customerName = createContactName(
                // salutation,
                firstName,
                lastName);
        Long managerId = fetchLong(rs, "manager_id");
        Long salesId = fetchLong(rs, "sales_id");
        Long branchId = fetchLong(rs, "branch_id");
        return new SalesListItemVO(
                customerName.toString(),
                shortName,
                contactType,
                fetchLong(rs, "id"),
                fetchLong(rs, "plan_id"),
                fetchBusinessType(fetchLong(rs, "type_id")),
                fetchLong(rs, "customer_id"),
                getLong(rs, "status"), 
                fetchDate(rs, "created"),
                fetchLong(rs, "created_by"),
                managerId,
                fetchEmployeeKey(managerId),
                fetchLong(rs, "manager_sub_id"),
                salesId,
                fetchEmployeeKey(salesId),
                fetchBranch(branchId),
                fetchString(rs, "name"),
                fetchString(rs, "street"),
                fetchString(rs, "zipcode"),
                fetchString(rs, "city"),
                fetchDouble(rs, "plant_capacity"),
                fetchBoolean(rs, "stopped"),
                fetchBoolean(rs, "cancelled"),
                fetchLong(rs, "installer_id"),
                (supportsLastAction ? fetchDate(rs, "last_action") : null),
                fetchLong(rs, "origin_id"),
                fetchLong(rs, "origin_type_id"),
                fetchString(rs, "accounting_ref"),
                fetchLong(rs, "shipping_id"),
                fetchLong(rs, "payment_id"),
                fetchDate(rs, "delivery_date"),
                fetchDate(rs, "confirmation_date"));
    }

    public static RowMapperResultSetExtractor getReader(
            OptionsCache cache, boolean supportsLastAction) {
        return new RowMapperResultSetExtractor(
                new SalesListItemRowMapper(cache, supportsLastAction));
    }

    /**
     * Creates a buffered query string selecting all values declared in provided viewName.
     * @param viewName required
     * @param optionalSelects if available
     * @return query
     */
    protected static StringBuilder createQueryForAll(String viewName) {
        StringBuilder query = new StringBuilder(SELECT_ALL_FROM);
        query.append(viewName);
        return query;
    }
}
