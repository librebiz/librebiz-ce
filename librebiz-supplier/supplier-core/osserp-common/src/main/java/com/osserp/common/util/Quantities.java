/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 21, 2008 6:22:48 AM 
 * 
 */
package com.osserp.common.util;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public final class Quantities {

    public static final Long PCS = 1L;
    public static final Long MILLIMETER = 2L;
    public static final Long METER = 3L;
    public static final Long SQUARE_METER = 4L;
    public static final Long CUBIC_METER = 5L;
    public static final Long KILOWATT = 6L;
    public static final Long KILOWATT_PEAK = 7L;
    public static final Long WATT_PEAK = 8L;
    public static final Long KILOGRAM = 9L;
    public static final Long LITER = 10L;
    public static final Long BLANK = 11L;
    public static final Long HOURS = 12L;
    public static final Long KILOMETER = 13L;
    public static final Long TONS = 14L;
    public static final Long WATT = 15L;
    public static final Long YEAR = 16L;
    public static final Long MONTH = 17L;
    public static final Long MAN_DAY = 18L;
    public static final Long DAY = 19L;

    // 12, 16, 17, 18, 19
    public static final Long[] TIME_AWARE = { 
        HOURS, YEAR, MONTH, MAN_DAY, DAY
    };

    public static final boolean isTimeAware(Long quantityUnit) {
        for (int i = 0, j = TIME_AWARE.length; i < j; i++) {
            if (TIME_AWARE[i].equals(quantityUnit)) {
                return true;
            }
        }
        return false;
    }

    public static final String format(Double value, Long quantityUnit) {
        if (value != null) {
            if (MAN_DAY.equals(quantityUnit)) {
                return NumberFormatter.getValue(value, NumberFormatter.DECIMAL, 1);
            }
            if (PCS.equals(quantityUnit)) {
                return NumberFormatter.getIntValue(value);
            }
            if (MILLIMETER.equals(quantityUnit)) {
                return NumberFormatter.getValue(value, NumberFormatter.CURRENCY);
            }
            if (METER.equals(quantityUnit)) {
                return NumberFormatter.getValue(value, NumberFormatter.CURRENCY);
            }
            if (HOURS.equals(quantityUnit)) {
                return NumberFormatter.getValue(value, NumberFormatter.CURRENCY);
            }
            if (SQUARE_METER.equals(quantityUnit)) {
                return NumberFormatter.getValue(value, NumberFormatter.CURRENCY);
            }
            if (CUBIC_METER.equals(quantityUnit)) {
                return NumberFormatter.getValue(value, NumberFormatter.CURRENCY);
            }
            if (KILOWATT.equals(quantityUnit)) {
                return NumberFormatter.getValue(value, NumberFormatter.DECIMAL);
            }
            if (KILOWATT_PEAK.equals(quantityUnit)) {
                return NumberFormatter.getValue(value, NumberFormatter.DECIMAL);
            }
            if (WATT.equals(quantityUnit)) {
                return NumberFormatter.getValue(value, NumberFormatter.DECIMAL);
            }
            if (WATT_PEAK.equals(quantityUnit)) {
                return NumberFormatter.getIntValue(value);
            }
            if (KILOGRAM.equals(quantityUnit)) {
                return NumberFormatter.getValue(value, NumberFormatter.CURRENCY);
            }
            if (LITER.equals(quantityUnit)) {
                return NumberFormatter.getValue(value, NumberFormatter.CURRENCY);
            }
            return NumberFormatter.getIntValue(value);
        }
        return "";
    }
}
