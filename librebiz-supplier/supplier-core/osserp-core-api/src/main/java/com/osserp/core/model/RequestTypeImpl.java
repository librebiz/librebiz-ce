/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 27-Jul-2006 
 * 
 */
package com.osserp.core.model;

import java.util.Map;

import com.osserp.common.beans.OptionImpl;
import com.osserp.common.util.StringUtil;

import com.osserp.core.BusinessType;
import com.osserp.core.Workflow;
import com.osserp.core.sales.SalesUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RequestTypeImpl extends OptionImpl implements BusinessType {
    protected static final String CLASS_CONTACT = "contact";
    protected static final String CLASS_SALES = "sales";
    protected static final String CLASS_PROJECT = "project";

    protected static final String SALES_CONTEXT_PROJECT = "generic";
    protected static final String SALES_CONTEXT_SALES = "sales";

    private Workflow workflow = null;
    private String key;
    private String context = "sales";
    private String salesContext = "sales";
    private Long company;
    private boolean active = false;
    private boolean requestContextOnly = false;
    private boolean directSales = false;
    private boolean trading = false;
    private boolean wholeSale = false;
    private boolean packageRequired = false;
    private boolean interestContext = false;
    private boolean salesPersonByCollector = false;
    private boolean supportingConfig = false;
    private boolean supportingDownpayments = false;
    private boolean supportingCustomDelivery = false;
    private boolean supportingPictures = false;
    private boolean supportingOffers = false;
    private boolean createSla = false;
    private boolean includePriceByDefault = false;
    private boolean dummy = false;
    private boolean service = false;
    private boolean subscription = false;
    private String commissionType = null;
    private String configPermissions = null;
    private String capacityFormat = null;
    private String capacityUnit = null;
    private String capacityLabel = null;
    private String capacityCalculationMethod = "amount";
    private String language = null;
    private boolean recordByCalculation = false;
    private Long calculationConfig = null;
    private String calculatorName = null;
    private boolean calculationByEvent = false;
    private Long calculationByEventConfig = null;
    private boolean closeOrderOnClose = false;
    private Double minimalMargin = SalesUtil.getDefaultMinimalMargin();
    private Double targetMargin = SalesUtil.getDefaultTargetMargin();
    private Long defaultOrigin = null;
    private Long defaultOriginType = null;
    private Long defaultBranch = null;
    private boolean forceDefaultBranch = false;
    private boolean installationDateSupported = false;
    private boolean startupSupported = false;
    private boolean updateBranchOnSalesPersonChange = false;
    private boolean workflowOnly = false;
    private Long defaultContractType = null;
    private Long customDeliveryNoteType = null;

    private boolean externalIdProvided = false;
    private String externalStatusQualifiedRequestKeys = null; 
    private String externalStatusSalesKeys = null;
    private String externalStatusSalesClosedKeys = null; 
    
    private Long requestLetterType = null;
    private Long salesLetterType = null;

    protected RequestTypeImpl() {
        super();
    }

    public RequestTypeImpl(
            Long id,
            Workflow workflow,
            String name,
            String key,
            String context,
            String salesContext,
            Long company,
            boolean active,
            boolean requestContextOnly,
            boolean directSales,
            boolean trading,
            boolean packageRequired,
            boolean interestContext,
            boolean supportingDownpayments,
            boolean supportingPictures,
            boolean supportingOffers,
            boolean createSla,
            boolean includePriceByDefault,
            boolean dummy,
            boolean service,
            String commissionType,
            String capacityFormat,
            String capacityUnit,
            String capacityLabel,
            Long calculationConfig) {
        super(id, name);
        this.workflow = workflow;
        this.key = key;
        this.context = context;
        this.salesContext = salesContext;
        this.company = company;
        this.active = active;
        this.requestContextOnly = requestContextOnly;
        this.directSales = directSales;
        this.trading = trading;
        this.packageRequired = packageRequired;
        this.interestContext = interestContext;
        this.supportingDownpayments = supportingDownpayments;
        this.supportingPictures = supportingPictures;
        this.supportingOffers = supportingOffers;
        this.createSla = createSla;
        this.includePriceByDefault = includePriceByDefault;
        this.dummy = dummy;
        this.service = service;
        this.commissionType = commissionType;
        this.capacityFormat = capacityFormat;
        this.capacityLabel = capacityLabel;
        this.capacityUnit = capacityUnit;
        this.calculationConfig = calculationConfig;
        // types created by user are always configurable
        this.supportingConfig = true;
    }

    public void update(
            String name,
            String key,
            String context,
            String salesContext,
            boolean active,
            boolean requestContextOnly,
            boolean directSales,
            boolean trading,
            boolean packageRequired,
            boolean interestContext,
            boolean salesPersonByCollector,
            boolean supportingDownpayments,
            boolean supportingPictures,
            boolean supportingOffers,
            boolean createSla,
            boolean includePriceByDefault,
            boolean dummy,
            boolean service,
            boolean subscription,
            boolean wholeSale,
            String commissionType,
            String capacityFormat,
            String capacityUnit,
            String capacityLabel,
            String language,
            boolean recordByCalculation,
            Long calculationConfig,
            Long requestLetterType,
            Long salesLetterType,
            boolean installationDateSupported,
            boolean startupSupported,
            boolean externalIdProvided,
            String externalStatusQualifiedRequestKeys, 
            String externalStatusSalesKeys,
            String externalStatusSalesClosedKeys,
            Long defaultContractType,
            Long defaultOrigin,
            Long defaultOriginType) {

        setName(name);
        this.key = key;
        this.context = context;
        this.salesContext = salesContext;
        this.active = active;
        this.requestContextOnly = requestContextOnly;
        this.directSales = directSales;
        this.trading = trading;
        this.packageRequired = packageRequired;
        this.interestContext = interestContext;
        this.salesPersonByCollector = salesPersonByCollector;
        this.supportingDownpayments = supportingDownpayments;
        this.supportingPictures = supportingPictures;
        this.supportingOffers = supportingOffers;
        this.createSla = createSla;
        this.includePriceByDefault = includePriceByDefault;
        this.dummy = dummy;
        this.service = service;
        this.subscription = subscription;
        this.commissionType = commissionType;
        this.capacityFormat = capacityFormat;
        this.capacityLabel = capacityLabel;
        this.capacityUnit = capacityUnit;
        this.wholeSale = wholeSale;
        this.language = language;
        this.recordByCalculation = recordByCalculation;
        this.calculationConfig = calculationConfig;
        this.requestLetterType = requestLetterType;
        this.salesLetterType = requestLetterType;
        this.installationDateSupported = installationDateSupported;
        this.startupSupported = startupSupported;
        this.externalIdProvided = externalIdProvided;
        this.externalStatusQualifiedRequestKeys = externalStatusQualifiedRequestKeys;
        this.externalStatusSalesKeys = externalStatusSalesKeys;
        this.externalStatusSalesClosedKeys = externalStatusSalesClosedKeys;
        this.defaultContractType = defaultContractType;
        this.defaultOrigin = defaultOrigin;
        this.defaultOriginType = defaultOriginType;
    }

    @Override
    public Map<String, Object> getMapped() {
        Map<String, Object> map = super.getMapped();
        // TODO add workflow property
        putIfExists(map, "key", key);
        putIfExists(map, "context", context);
        putIfExists(map, "salesContext", salesContext);
        putIfExists(map, "active", active);
        putIfExists(map, "requestContextOnly", requestContextOnly);
        putIfExists(map, "directSales", directSales);
        putIfExists(map, "trading", trading);
        putIfExists(map, "packageRequired", packageRequired);
        putIfExists(map, "interestContext", interestContext);
        putIfExists(map, "supportingConfig", supportingConfig);
        putIfExists(map, "supportingDownpayments", supportingDownpayments);
        putIfExists(map, "supportingPictures", supportingPictures);
        putIfExists(map, "supportingOffers", supportingOffers);
        putIfExists(map, "createSla", createSla);
        putIfExists(map, "includePriceByDefault", includePriceByDefault);
        putIfExists(map, "dummy", dummy);
        putIfExists(map, "service", service);
        putIfExists(map, "subscription", subscription);
        putIfExists(map, "commissionType", commissionType);
        putIfExists(map, "configPermissions", configPermissions);
        putIfExists(map, "capacityFormat", capacityFormat);
        putIfExists(map, "capacityLabel", capacityLabel);
        putIfExists(map, "capacityUnit", capacityUnit);
        putIfExists(map, "wholeSale", wholeSale);
        putIfExists(map, "language", language);
        putIfExists(map, "calculationConfig", calculationConfig);
        putIfExists(map, "defaultOrigin", defaultOrigin);
        putIfExists(map, "defaultOriginType", defaultOriginType);
        putIfExists(map, "updateBranchOnSalesPersonChange", updateBranchOnSalesPersonChange);
        putIfExists(map, "installationDateSupported", installationDateSupported);
        putIfExists(map, "startupSupported", startupSupported);
        putIfExists(map, "externalIdProvided", externalIdProvided);
        putIfExists(map, "externalStatusQualifiedRequestKeys", externalStatusQualifiedRequestKeys);
        putIfExists(map, "externalStatusSalesKeys", externalStatusSalesKeys);
        putIfExists(map, "externalStatusSalesClosedKeys", externalStatusSalesClosedKeys);
        return map;
    }

    public Workflow getWorkflow() {
        return workflow;
    }

    protected void setWorkflow(Workflow workflow) {
        this.workflow = workflow;
    }

    public String getKey() {
        return key;
    }

    protected void setKey(String key) {
        this.key = key;
    }

    public boolean isActive() {
        return active;
    }

    protected void setActive(boolean active) {
        this.active = active;
    }

    public boolean isDummy() {
        return dummy;
    }

    protected void setDummy(boolean dummy) {
        this.dummy = dummy;
    }

    public boolean isService() {
        return service;
    }

    public void setService(boolean service) {
        this.service = service;
    }

    public boolean isSubscription() {
        return subscription;
    }

    public void setSubscription(boolean subscription) {
        this.subscription = subscription;
    }

    public String getContext() {
        return context;
    }

    protected void setContext(String context) {
        this.context = context;
    }

    public boolean isCommissionSupported() {
        return (commissionType != null && !BusinessType.NONE_COMMISSION.equals(commissionType));
    }

    public String getCommissionType() {
        return commissionType;
    }

    public void setCommissionType(String commissionType) {
        this.commissionType = commissionType;
    }

    public String getConfigPermissions() {
        return configPermissions;
    }

    public void setConfigPermissions(String configPermissions) {
        this.configPermissions = configPermissions;
    }

    public String getSalesContext() {
        return salesContext;
    }

    protected void setSalesContext(String salesContext) {
        this.salesContext = salesContext;
    }

    public String getLanguage() {
        return language;
    }

    protected void setLanguage(String language) {
        this.language = language;
    }

    public Long getCompany() {
        return company;
    }

    protected void setCompany(Long company) {
        this.company = company;
    }

    public Long getDefaultBranch() {
        return defaultBranch;
    }

    protected void setDefaultBranch(Long defaultBranch) {
        this.defaultBranch = defaultBranch;
    }

    public boolean isForceDefaultBranch() {
        return forceDefaultBranch;
    }

    protected void setForceDefaultBranch(boolean forceDefaultBranch) {
        this.forceDefaultBranch = forceDefaultBranch;
    }

    public boolean isRequestContextOnly() {
        return requestContextOnly;
    }

    protected void setRequestContextOnly(boolean requestContextOnly) {
        this.requestContextOnly = requestContextOnly;
    }

    public boolean isSales() {
        return SALES_CONTEXT_SALES.equals(salesContext);
    }

    public boolean isStandardProject() {
        return SALES_CONTEXT_PROJECT.equals(salesContext);
    }

    public boolean isProjectRequest() {
        return CLASS_PROJECT.equals(context);
    }

    public boolean isDirectSales() {
        return directSales;
    }

    protected void setDirectSales(boolean directSales) {
        this.directSales = directSales;
    }

    public boolean isWholeSale() {
        return wholeSale;
    }

    protected void setWholeSale(boolean wholeSale) {
        this.wholeSale = wholeSale;
    }

    public boolean isPackageRequired() {
        return packageRequired;
    }

    protected void setPackageRequired(boolean packageRequired) {
        this.packageRequired = packageRequired;
    }

    public boolean isTrading() {
        return trading;
    }

    protected void setTrading(boolean trading) {
        this.trading = trading;
    }

    public boolean isInterestContext() {
        return interestContext;
    }

    protected void setInterestContext(boolean interestContext) {
        this.interestContext = interestContext;
    }

    public boolean isSalesPersonByCollector() {
        return salesPersonByCollector;
    }

    protected void setSalesPersonByCollector(boolean salesPersonByCollector) {
        this.salesPersonByCollector = salesPersonByCollector;
    }

    public boolean isSupportingConfig() {
        return supportingConfig;
    }

    protected void setSupportingConfig(boolean supportingConfig) {
        this.supportingConfig = supportingConfig;
    }

    public boolean isSupportingDownpayments() {
        return supportingDownpayments;
    }

    public void setSupportingDownpayments(boolean supportingDownpayments) {
        this.supportingDownpayments = supportingDownpayments;
    }

    public boolean isSupportingOffers() {
        return supportingOffers;
    }

    protected void setSupportingOffers(boolean supportingOffers) {
        this.supportingOffers = supportingOffers;
    }

    public boolean isSupportingCustomDelivery() {
        return supportingCustomDelivery;
    }

    public void setSupportingCustomDelivery(boolean supportingCustomDelivery) {
        this.supportingCustomDelivery = supportingCustomDelivery;
    }

    public boolean isSupportingPictures() {
        return supportingPictures;
    }

    protected void setSupportingPictures(boolean supportingPictures) {
        this.supportingPictures = supportingPictures;
    }

    public boolean isWorkflowOnly() {
        return workflowOnly;
    }

    protected void setWorkflowOnly(boolean workflowOnly) {
        this.workflowOnly = workflowOnly;
    }

    public boolean isCreateSla() {
        return createSla;
    }

    protected void setCreateSla(boolean createSla) {
        this.createSla = createSla;
    }

    public Long getDefaultContractType() {
        return defaultContractType;
    }

    public void setDefaultContractType(Long defaultContractType) {
        this.defaultContractType = defaultContractType;
    }

    public Long getCustomDeliveryNoteType() {
        return customDeliveryNoteType;
    }

    public void setCustomDeliveryNoteType(Long customDeliveryNoteType) {
        this.customDeliveryNoteType = customDeliveryNoteType;
    }

    public boolean isExternalIdProvided() {
        return externalIdProvided;
    }

    protected void setExternalIdProvided(boolean externalIdProvided) {
        this.externalIdProvided = externalIdProvided;
    }

    public String getExternalStatusQualifiedRequestKeys() {
        return externalStatusQualifiedRequestKeys;
    }

    protected void setExternalStatusQualifiedRequestKeys(String externalStatusQualifiedRequestKeys) {
        this.externalStatusQualifiedRequestKeys = externalStatusQualifiedRequestKeys;
    }

    public String getExternalStatusSalesKeys() {
        return externalStatusSalesKeys;
    }

    protected void setExternalStatusSalesKeys(String externalStatusSalesKeys) {
        this.externalStatusSalesKeys = externalStatusSalesKeys;
    }

    public String getExternalStatusSalesClosedKeys() {
        return externalStatusSalesClosedKeys;
    }

    protected void setExternalStatusSalesClosedKeys(String externalStatusSalesClosedKeys) {
        this.externalStatusSalesClosedKeys = externalStatusSalesClosedKeys;
    }

    public boolean isMatchingExternalStatus(String externalStatus) {
        return isExternalStatusQualifiedRequest(externalStatus)
                || isExternalStatusSales(externalStatus) 
                || isExternalStatusSalesClosed(externalStatus);
    }

    public boolean isExternalStatusQualifiedRequest(String externalStatus) {
        return isExternalStatusMatching(externalStatusQualifiedRequestKeys, externalStatus);
    }

    public boolean isExternalStatusSales(String externalStatus) {
        return isExternalStatusMatching(externalStatusSalesKeys, externalStatus);
    }

    public boolean isExternalStatusSalesClosed(String externalStatus) {
        return isExternalStatusMatching(externalStatusSalesClosedKeys, externalStatus);
    }
    
    private boolean isExternalStatusMatching(String existingStatusList, String externalStatus) {
        String[] stats = StringUtil.getTokenArray(existingStatusList);
        if (stats != null && isSet(externalStatus)) {
            for (int i = 0, j = stats.length; i < j; i++) {
                if (externalStatus.startsWith(stats[i])) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isIncludePriceByDefault() {
        return includePriceByDefault;
    }

    protected void setIncludePriceByDefault(boolean includePriceByDefault) {
        this.includePriceByDefault = includePriceByDefault;
    }

    public boolean isSupportingCapacity() {
        return capacityUnit != null;
    }

    public String getCapacityFormat() {
        return capacityFormat;
    }

    public void setCapacityFormat(String capacityFormat) {
        this.capacityFormat = capacityFormat;
    }

    public String getCapacityUnit() {
        return capacityUnit;
    }

    public void setCapacityUnit(String capacityUnit) {
        this.capacityUnit = capacityUnit;
    }

    public String getCapacityLabel() {
        return capacityLabel;
    }

    public void setCapacityLabel(String capacityLabel) {
        this.capacityLabel = capacityLabel;
    }

    public String getCapacityCalculationMethod() {
        return capacityCalculationMethod;
    }

    public void setCapacityCalculationMethod(String capacityCalculationMethod) {
        this.capacityCalculationMethod = capacityCalculationMethod;
    }

    public boolean isRecordByCalculation() {
        return recordByCalculation;
    }

    public void setRecordByCalculation(boolean recordByCalculation) {
        this.recordByCalculation = recordByCalculation;
    }

    public Long getCalculationConfig() {
        return calculationConfig;
    }

    public void setCalculationConfig(Long calculationConfig) {
        this.calculationConfig = calculationConfig;
    }

    public String getCalculatorName() {
        return calculatorName;
    }

    public void setCalculatorName(String calculatorName) {
        this.calculatorName = calculatorName;
    }

    public boolean isCalculationByEvent() {
        return calculationByEvent;
    }

    public void setCalculationByEvent(boolean calculationByEvent) {
        this.calculationByEvent = calculationByEvent;
    }

    public Long getCalculationByEventConfig() {
        return calculationByEventConfig;
    }

    public void setCalculationByEventConfig(Long calculationByEventConfig) {
        this.calculationByEventConfig = calculationByEventConfig;
    }

    public Double getMinimalMargin() {
        return minimalMargin;
    }

    public void setMinimalMargin(Double minimalMargin) {
        this.minimalMargin = minimalMargin;
    }

    public Double getTargetMargin() {
        return targetMargin;
    }

    public void setTargetMargin(Double targetMargin) {
        this.targetMargin = targetMargin;
    }

    public boolean isCloseOrderOnClose() {
        return closeOrderOnClose;
    }

    public void setCloseOrderOnClose(boolean closeOrderOnClose) {
        this.closeOrderOnClose = closeOrderOnClose;
    }

    public Long getDefaultOrigin() {
        return defaultOrigin;
    }

    public void setDefaultOrigin(Long defaultOrigin) {
        this.defaultOrigin = defaultOrigin;
    }

    public Long getDefaultOriginType() {
        return defaultOriginType;
    }

    public void setDefaultOriginType(Long defaultOriginType) {
        this.defaultOriginType = defaultOriginType;
    }

    public Long getRequestLetterType() {
        return requestLetterType;
    }

    public void setRequestLetterType(Long requestLetterType) {
        this.requestLetterType = requestLetterType;
    }

    public Long getSalesLetterType() {
        return salesLetterType;
    }

    public void setSalesLetterType(Long salesLetterType) {
        this.salesLetterType = salesLetterType;
    }

    public boolean isInstallationDateSupported() {
        return installationDateSupported;
    }

    public void setInstallationDateSupported(boolean installationDateSupported) {
        this.installationDateSupported = installationDateSupported;
    }

    public boolean isStartupSupported() {
        return startupSupported;
    }

    public void setStartupSupported(boolean startupSupported) {
        this.startupSupported = startupSupported;
    }

    public boolean isUpdateBranchOnSalesPersonChange() {
        return updateBranchOnSalesPersonChange;
    }

    public void setUpdateBranchOnSalesPersonChange(boolean updateBranchOnSalesPersonChange) {
        this.updateBranchOnSalesPersonChange = updateBranchOnSalesPersonChange;
    }
    
}
