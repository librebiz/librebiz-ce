/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 1 Apr 2007 15:24:35 
 * 
 */
package com.osserp.core.model.records;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.dms.DmsReference;
import com.osserp.common.dms.DocumentType;

import com.osserp.core.finance.Record;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RecordDocumentImpl extends AbstractRecordDocument {
    private static Logger log = LoggerFactory.getLogger(RecordDocumentImpl.class.getName());

    protected RecordDocumentImpl() {
        super();
    }

    public RecordDocumentImpl(
            DmsReference reference,
            DocumentType type,
            String fileName,
            long fileSize,
            Long createdBy,
            String note) {
        super(reference.getReferenceId(), type, fileName, fileSize, createdBy, note);
        if (reference.getReference() instanceof Record) {
            Record record = (Record) reference.getReference();
            setReferenceType(record.getType().getId());
        } else if (reference.getReferenceType() != null) {
            setReferenceType(reference.getReferenceType());
        } else {
            log.warn("<init> dmsReference does not provide known object as reference [object="
                    + (reference.getReference() == null ? "null" : reference.getReference().getClass().getName()) + "]");
        }
    }

}
