/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.osserp.common.util;

import java.lang.reflect.InvocationTargetException;
import java.util.Comparator;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections.comparators.ComparableComparator;

public class CommonComparator extends BeanComparator {

    public CommonComparator() {
        this(null);
    }

    public CommonComparator(final String property) {
        this(property, ComparableComparator.getInstance());
    }

    public CommonComparator(final String property, final Comparator<?> comparator) {
        super(property, comparator);
    }

    public int compare( final Object o1, final Object o2 ) {
        if (o1 == null && o2 == null) {
            return 0;
        }
        if (o1 == null) {
            return -1;
        }
        if (o2 == null) {
            return 1;
        }
        if (getProperty() != null) {
            String prop = getProperty();
            try {
                final Object value1 = PropertyUtils.getProperty(o1, prop);
                final Object value2 = PropertyUtils.getProperty(o2, prop);
                if (value1 == null && value2 == null) {
                    return 0;
                }
                if (value1 == null) {
                    return -1;
                }
                if (value2 == null) {
                    return 1;
                }
            } catch ( final IllegalAccessException iae ) {
                throw new RuntimeException( "IllegalAccessException: " + iae.toString() );
            } catch ( final InvocationTargetException ite ) {
                throw new RuntimeException( "InvocationTargetException: " + ite.toString() );
            } catch ( final NoSuchMethodException nsme ) {
                throw new RuntimeException( "NoSuchMethodException: " + nsme.toString() );
            }
        }
        try {
            return super.compare(o1, o2);
        }
        catch (NullPointerException npe) {
            return 0;
        }
    }
}
