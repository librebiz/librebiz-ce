<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${requestScope.customerListingView}" />

<div class="col-md-12 panel-area">
    <div class="table-responsive table-responsive-default">
        <c:choose>
            <c:when test="${view.selectedQueryType == 'listWithoutAction' || view.selectedQueryType == 'listWithoutOrder'}">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th><fmt:message key="companySlashName" /></th>
                            <th><fmt:message key="city" /></th>
                            <th class="right"><fmt:message key="ofDate" /></th>
                            <th class="right"><fmt:message key="request" /></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:choose>
                            <c:when test="${empty view.list}">
                                <tr>
                                    <td colspan="4"><fmt:message key="noDataAvailable" /></td>
                                </tr>
                            </c:when>
                            <c:otherwise>
                                <c:forEach var="contact" items="${view.list}" varStatus="s">
                                    <tr id="customer${contact.id}">
                                        <td><a href="<c:url value="/loadCustomer.do?exit=${customerSelectExitTarget}&id=${contact.id}&exitId=customer${contact.id}"/>"> <o:out value="${contact.displayName}" /></a></td>
                                        <td><o:out value="${contact.addressDisplay}" /></td>
                                        <td class="right"><o:date value="${contact.created}" /></td>
                                        <td class="right">
                                            <span<c:if test="${contact.allRequestsCancelled}"> class="error" title="<fmt:message key="requestCanceled" />"</c:if>>
                                                <o:date value="${contact.lastRequestDate}" />
                                            </span>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </c:otherwise>
                        </c:choose>
                    </tbody>
                </table>
            </c:when>
            <c:when test="${view.selectedQueryType == 'listWithoutSales'}">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th><fmt:message key="companySlashName" /></th>
                            <th><fmt:message key="city" /></th>
                            <th class="right"><fmt:message key="ofDate" /></th>
                            <th class="right"><fmt:message key="currentOrders" /></th>
                            <th class="right"><fmt:message key="lastOrder" /></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:choose>
                            <c:when test="${empty view.list}">
                                <tr>
                                    <td colspan="4"><fmt:message key="noDataAvailable" /></td>
                                </tr>
                            </c:when>
                            <c:otherwise>
                                <c:forEach var="contact" items="${view.list}" varStatus="s">
                                    <tr id="customer${contact.id}">
                                        <td><a href="<c:url value="/loadCustomer.do?exit=${customerSelectExitTarget}&id=${contact.id}&exitId=customer${contact.id}"/>"> <o:out value="${contact.displayName}" /></a></td>
                                        <td><o:out value="${contact.addressDisplay}" /></td>
                                        <td class="right"><o:date value="${contact.created}" /></td>
                                        <td class="right"><o:out value="${contact.orderCount}" /></td>
                                        <td class="right"><o:date value="${contact.lastOrderDate}" /></td>
                                    </tr>
                                </c:forEach>
                            </c:otherwise>
                        </c:choose>
                    </tbody>
                </table>
            </c:when>
            <c:otherwise>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th><fmt:message key="companySlashName" /></th>
                            <th><fmt:message key="city" /></th>
                            <th class="right"><fmt:message key="lastRequest" /></th>
                            <th class="right"><fmt:message key="lastSales" /></th>
                            <th class="right"><fmt:message key="ofDate" /></th>
                            <th class="right"><fmt:message key="total" /></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:choose>
                            <c:when test="${empty view.list}">
                                <tr>
                                    <td colspan="6"><fmt:message key="noDataAvailable" /></td>
                                </tr>
                            </c:when>
                            <c:otherwise>
                                <c:forEach var="contact" items="${view.list}" varStatus="s">
                                    <tr id="customer${contact.id}">
                                        <td><a href="<c:url value="/loadCustomer.do?exit=${customerSelectExitTarget}&id=${contact.id}&exitId=customer${contact.id}"/>"> <o:out value="${contact.displayName}" /></a></td>
                                        <td><o:out value="${contact.addressDisplay}" /></td>
                                        <td class="right"><o:date value="${contact.lastRequestDate}" /></td>
                                        <td class="right"><o:number value="${contact.lastSales}" format="currency" /></td>
                                        <td class="right"><o:date value="${contact.lastSalesDate}" /></td>
                                        <td class="right"><o:number value="${contact.salesTotal}" format="currency" /></td>
                                    </tr>
                                </c:forEach>
                            </c:otherwise>
                        </c:choose>
                    </tbody>
                </table>
            </c:otherwise>
        </c:choose>
    </div>
</div>
