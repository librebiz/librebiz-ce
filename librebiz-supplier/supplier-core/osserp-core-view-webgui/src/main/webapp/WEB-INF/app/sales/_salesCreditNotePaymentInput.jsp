<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <c:choose>
                    <c:when test="${!empty view.record.bookingType}">
                        <o:out value="${view.record.bookingType.name}" />
                    </c:when>
                    <c:otherwise>
                        <o:out value="${view.record.type.name}" />
                    </c:otherwise>
                </c:choose>
                <o:out value="${view.record.number}" />
                <span> / </span>
                <o:date value="${view.record.created}" />
            </h4>
        </div>
    </div>
    <div class="panel-body">

        <c:if test="${view.record.dueAmount > 0}">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="bankAccount"> <fmt:message key="bankAccount" />
                        </label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <oc:select name="bankAccountId" options="${view.bankAccounts}" prompt="false" styleClass="form-control" />
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="creditNoteAmount"><fmt:message key="creditNoteAmount" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <span class="form-value"><o:number value="${view.recordAmount}" format="currency" /> </span>
                        <o:out value="${view.record.currencyKey}" />
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="amount"> <fmt:message key="paymentAmount" />
                        </label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <v:number name="amount" styleClass="form-control" value="${view.record.dueAmount}" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <p style="padding-top: 5px;">
                                (
                                <fmt:message key="includesTaxShort" />
                                )
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <v:date name="datePaid" label="paidOn" picker="true" styleClass="form-control" />

            <div class="row next">
                <div class="col-md-4"></div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <o:submit styleClass="form-control" value="paymentCreate" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group"></div>
                        </div>
                    </div>
                </div>
            </div>

            <hr />
        </c:if>

        <c:choose>

            <c:when test="${!empty view.record.payments}">

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="deletePaymentsForward"><fmt:message key="actions" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:link url="${view.baseLink}/forwardDeletePayment">
                                <fmt:message key="paymentDelete" />
                            </v:link>
                        </div>
                    </div>
                </div>

            </c:when>
            <c:when test="${view.record.canceled}">

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="resetCancellation"><fmt:message key="actions" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:link url="${view.baseLink}/resetCancellation" confirm="true" message="confirmResetCancellation" >
                                <fmt:message key="resetCancellation" /> <o:out value="${view.record.cancellation.id}" />
                            </v:link>
                        </div>
                    </div>
                </div>

            </c:when>
            <c:otherwise>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="cancelCreditNote"><fmt:message key="actions" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:link url="${view.baseLink}/createCancellation" confirm="true" message="confirmCreditNoteCancel">
                                <fmt:message key="creditNoteCancel" />
                            </v:link>
                        </div>
                    </div>
                </div>

            </c:otherwise>
        </c:choose>
        
        <c:if test="${view.record.itemsAffectStock}">

            <div class="row next">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="toggleDeliveryBooking"> </label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <v:link url="${view.baseLink}/toggleDeliveryBooking">
                            <c:choose>
                                <c:when test="${view.record.deliveryExisting}">
                                    <fmt:message key="removeStockBookingOnly" />
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="addStockBookingOnly" />
                                </c:otherwise>
                            </c:choose>
                        </v:link>
                    </div>
                </div>
            </div>
        </c:if>

    </div>
</div>
