/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 19.04.2011 17:50:59 
 * 
 */
package com.osserp.gui.products

import com.osserp.core.products.ProductSearch
import com.osserp.core.products.ProductSearchRequest
import com.osserp.core.products.ProductSelectionConfig
import com.osserp.core.products.ProductSelectionConfigManager

import com.osserp.gui.CoreView

/**
 *
 * @author eh <eh@osserp.com>
 * 
 */
public class ProductSelectionListView extends CoreView {

    ProductSelectionConfig selectionConfig = null
    boolean ignoreLazy = false

    ProductSelectionListView() {
        enablePopupView()
        headerName = 'productSelection'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (forwardRequest) {
            Long id = form.getLong('id')
            if (id) {
                selectionConfig = productSelectionConfigManager.getSelectionConfig(id)
                list = productSearch.find(new ProductSearchRequest(), selectionConfig)
            }
            env.strutsExit = form.getString('strutsExit')
        }
    }

    private ProductSearch getProductSearch() {
        getService(ProductSearch.class.getName())
    }

    private ProductSelectionConfigManager getProductSelectionConfigManager() {
        getService(ProductSelectionConfigManager.class.getName())
    }
}
