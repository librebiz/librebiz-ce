<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-12 panel-area panel-area-default">
    <div class="panel-body">
        <div class="table-responsive table-responsive-default">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th><fmt:message key="receivedLabel" /></th>
                        <c:if test="${view.adminMode}">
                            <th><fmt:message key="employeeShort" /></th>
                        </c:if>
                        <th><fmt:message key="incident" /></th>
                        <th><fmt:message key="mailSubject" /></th>
                        <th><fmt:message key="originator" /></th>
                        <th><fmt:message key="recipient" /></th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="obj" items="${view.list}" varStatus="s">
                        <c:if test="${!view.adminMode or (empty view.selectedUser or view.selectedUser == obj.userId)}">
                            <tr>
                                <td>
                                    <v:link url="${view.baseLink}/select?id=${obj.id}" title="displayEmail">
                                        <o:date value="${obj.receivedDate}" addcentury="false" addtime="true" />
                                    </v:link>
                                </td>
                                <c:if test="${view.adminMode}">
                                    <td>
                                        <c:choose>
                                            <c:when test="${!empty view.selectedUser}">
                                                <v:link url="${view.baseLink}/selectUser" title="displayAll">
                                                    <oc:employee initials="true" value="${obj.userId}" />
                                                </v:link>
                                            </c:when>
                                            <c:otherwise>
                                                <v:link url="${view.baseLink}/selectUser?userId=${obj.userId}" title="employeeSelection">
                                                    <oc:employee initials="true" value="${obj.userId}" />
                                                </v:link>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                </c:if>
                                <td>
                                    <c:choose>
                                        <c:when test="${!empty obj.businessId}">
                                            <a href="<c:url value="/loadBusinessCase.do?exit=${fetchmailInboxExitTarget}&id=${obj.businessId}"/>">
                                                <o:out value="${obj.businessId}" />
                                            </a>
                                        </c:when>
                                        <c:otherwise>
                                            <fmt:message key="unknown" />
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td><o:out value="${obj.subject}" /></td>
                                <td><o:out value="${obj.originator}" /></td>
                                <td><o:out value="${obj.recipient}" /></td>
                            </tr>
                        </c:if>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
