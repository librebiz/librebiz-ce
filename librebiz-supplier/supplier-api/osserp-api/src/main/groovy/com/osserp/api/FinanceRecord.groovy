/**
 *
 * Copyright (C) 2017 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 17.12.2017 
 * 
 */
package com.osserp.api

import javax.persistence.Column
import javax.persistence.MappedSuperclass

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
@MappedSuperclass
abstract class FinanceRecord extends WsObject implements WSObject {
    
    String currency
    
    @Column(name='payment_id')
    Long paymentId
    
    @Column(name='payment_status')
    String paymentStatus

    @Column(name='shipping_id')
    Long shippingId

    BigDecimal net = new BigDecimal(0)
    @Column(name='rtax')
    BigDecimal reducedTax = new BigDecimal(0)
    BigDecimal tax = new BigDecimal(0)
    BigDecimal gross = new BigDecimal(0)
    
    BigDecimal getTaxTotal() {
        return (tax != null ? tax.doubleValue() : 0)
            + (reducedTax != null ? reducedTax.doubleValue() : 0)
    }
    
    void setSummary(BigDecimal net, BigDecimal reducedTax, BigDecimal tax, BigDecimal gross) {
        this.net = net
        this.reducedTax = reducedTax
        this.tax = tax
        this.gross = gross
    }
}
