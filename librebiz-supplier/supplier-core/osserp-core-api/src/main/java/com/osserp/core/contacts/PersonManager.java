/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 23, 2008 1:58:37 PM 
 * 
 */
package com.osserp.core.contacts;

import com.osserp.common.ClientException;
import com.osserp.common.Option;
import com.osserp.common.PermissionException;

import com.osserp.core.employees.Employee;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface PersonManager {

    /**
     * Tries to find a person by primary key
     * @param id
     * @return person or null if no such entity exists
     */
    Person find(Long id);

    /**
     * Changes the type of a private contact into business contact
     * @param changingUser
     * @param personToChange
     * @param company name
     * @throws ClientException if such a name already exists
     */
    void changeType(Employee changingUser, Person personToChange, String company) throws ClientException;

    /**
     * Persists current persons values
     * @param person
     */
    void save(Person person);
    
    /**
     * Updates values of an existing person
     * @param user
     * @param contact
     * @param salutation
     * @param title
     * @param firstNamePrefix
     * @param firstName
     * @param lastName
     * @param street
     * @param zipcode
     * @param city
     * @param federalState
     * @param federalStateName
     * @param country
     * @return person with updated values
     * @throws ClientException
     * @throws PermissionException
     */
    Person updatePerson(
            DomainUser user,
            Person contact,
            Salutation salutation,
            Option title,
            boolean firstNamePrefix,
            String firstName,
            String lastName,
            String street,
            String zipcode,
            String city,
            Long federalState,
            String federalStateName,
            Long country) throws ClientException, PermissionException;

}
