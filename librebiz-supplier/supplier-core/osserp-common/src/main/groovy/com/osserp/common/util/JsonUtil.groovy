/**
 *
 * Copyright (C) 2015 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 12, 2015 
 * 
 */
package com.osserp.common.util

import groovy.json.JsonBuilder
import groovy.json.JsonOutput
import groovy.json.JsonSlurper

import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class JsonUtil {
    private static Logger log = LoggerFactory.getLogger(JsonUtil.class.getName())

    /**
     * Parses json formatted string into internal jsonObject
     * @param jsonString
     * @return jsonObject or null if parsing failed
     */
    static JsonObject parse(String jsonString) {
        JsonSlurper jsonObject = new JsonSlurper()
        def json = jsonObject.parseText(jsonString)
        if (json instanceof Map) {
            return new JsonObject(json)
        }
        log.warn("parse unsupported result type [class=${json?.getClass().getName()}]")
        return null
    }

    /**
     * Creates a json formatted string by a map
     * @param map
     * @return json string
     */
    static String toJsonString(Map map) {
        try {
            JsonBuilder jsonBuilder = new JsonBuilder(map)
            return jsonBuilder.toString()
        } catch (Exception e) {
            log.warn("toJsonString caught exception [message=${e.message}]", e)
        }
        return null;
    }

    /**
     * Creates a json object by filesystem file with json formatted content
     * @param filename file path and name of an ascii file with json string inside
     * @return json object or null on parse errors, see application log if enabled
     */
    static JsonObject parseFile(String filename) {
        String input = null
        try {
            input = FileUtil.readFilesystemFileAsString(filename)
            return fetchInput(input)
        } catch (Exception e) {
            log.warn("parseFile caught exception [file=${filename}, message=${e.message}]", e)
        }
        return null
    }

    /**
     * Creates a json object by resource path file with json formatted content
     * @param filename filename of file on resource path
     * @return json object or null on parse errors, see application log if enabled
     */
    static JsonObject parseResourceFile(String filename) {
        String input = null
        try {
            input = FileUtil.readResourceFileAsString(filename)
            return fetchInput(input)
        } catch (Exception e) {
            log.warn("parseResourceFile caught exception [message=${e.message}]", e)
        }
        return null
    }

    /**
     * Invokes parseFile and parseResourceFile if parseFile returns null.
     * @param filename
     * @return json object or null if file does not exist or parsing error.
     */
    static JsonObject parseFileAndResource(String filename) {
        JsonObject obj = parseFile(filename)
        if (obj == null) {
            obj = parseResourceFile(filename)
        }
        return obj
    }

    static String format(String jsonString) {
        String result = ''
        if (jsonString) {
            result = JsonOutput.prettyPrint(jsonString)
        }
        return result
    }

    private static JsonObject fetchInput(String input) {
        JsonObject obj = (input != null && input.length() > 0) ? JsonUtil.parse(input) : null
        return obj
    }
}
