/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on May 7, 2010 at 9:07:19 AM 
 * 
 */
package com.osserp.gui.plannings

import javax.servlet.http.HttpServletResponse
import javax.servlet.http.HttpServletRequest

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.common.ClientException

import com.osserp.groovy.web.PopupViewController

/**
 * 
 * @author jg <jg@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
@RequestMapping("/plannings/productPlanning/*")
@Controller class ProductPlanningController extends PopupViewController {
    private static Logger logger = LoggerFactory.getLogger(ProductPlanningController.class.getName())

    @RequestMapping
    def selectStock(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("selectStock: invoked [user=${getUserId(request)}]")

        ProductPlanningView view = getView(request)
        view.selectStock()
        defaultPage
    }

    @RequestMapping
    def toggleOpenPurchaseOnlyFlag(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("toggleOpenPurchaseOnlyFlag: invoked [user=${getUserId(request)}]")

        ProductPlanningView view = getView(request)
        view.togglePurchaseOrderedOnly()
        defaultPage
    }

    @RequestMapping
    def toggleVacancyDisplay(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("toggleVacancyDisplay: invoked [user=${getUserId(request)}]")

        ProductPlanningView view = getView(request)
        view.toggleVacancyDisplay()
        defaultPage
    }

    @RequestMapping
    def toggleConfirmedPurchaseOnlyFlag(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("toggleConfirmedPurchaseOnlyFlag: invoked [user=${getUserId(request)}]")

        ProductPlanningView view = getView(request)
        view.toggleConfirmedPurchaseOnlyFlag()
        defaultPage
    }

    @RequestMapping
    def updateHorizon(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("updateHorizon: invoked [user=${getUserId(request)}]")

        ProductPlanningView view = getView(request)
        view.updateHorizon()
        defaultPage
    }

    @RequestMapping
    def updateDelivery(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("updateDelivery: invoked [user=${getUserId(request)}]")

        ProductPlanningView view = getView(request)
        try {
            view.updateDelivery()
        } catch (ClientException c) {
            saveError(request, c.message)
        }
        defaultPage
    }

    @RequestMapping
    def editDelivery(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("editDelivery: invoked [user=${getUserId(request)}]")

        ProductPlanningView view = getView(request)
        view.editDelivery()
        defaultPage
    }

    @RequestMapping
    def refresh(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("refresh: invoked [user=${getUserId(request)}]")

        ProductPlanningView view = getView(request)
        view.refresh()
        defaultPage
    }
}

