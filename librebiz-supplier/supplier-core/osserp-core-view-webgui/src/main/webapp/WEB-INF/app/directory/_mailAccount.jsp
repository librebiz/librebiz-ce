<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<o:permission role="ldap_mail_admin" info="permissionLdapMailAdmin">
    <c:if test="${!empty view.fetchmailAccount}">
        <div class="col-md-6 panel-area panel-area-default">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>
                        <fmt:message key="directoryMailbox" />
                        <span> - </span>
                        <c:choose>
                            <c:when test="${view.mailAccountEditMode}">
                                <a href="<v:url value="${view.baseLink}/disableMailAccountEditMode"/>"><o:out value="${view.fetchmailAccount.values['cn']}" /></a>
                            </c:when>
                            <c:otherwise>
                                <a href="<v:url value="${view.baseLink}/enableMailAccountEditMode"/>"><o:out value="${view.fetchmailAccount.values['cn']}" /></a>
                            </c:otherwise>
                        </c:choose>
                    </h4>
                </div>
            </div>
            <div class="panel-body">
                <c:choose>
                    <c:when test="${view.mailAccountEditMode}">
                        <v:form name="dynamicForm" url="${view.baseLink}/save">
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                        <c:forEach var="name" items="${view.fetchmailAccount.editableAttributeNames}">
                                            <tr>
                                                <td style="text-align: left;"><o:out value="${name}" /></td>
                                                <td><input type="text" name="${name}" value="${view.fetchmailAccount.values[name]}" /></td>
                                            </tr>
                                        </c:forEach>
                                        <tr>
                                            <td style="text-align: left;"><input onclick="gotoUrl('<v:url value="${view.baseLink}/disableMailAccountEditMode"/>');" type="button" value="<fmt:message key="exit"/>" /></td>
                                            <td><o:submit /></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </v:form>
                    </c:when>
                    <c:otherwise>
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <c:forEach var="name" items="${view.fetchmailAccount.attributeNames}">
                                        <tr>
                                            <td style="text-align: left;"><o:out value="${name}" /></td>
                                            <td><o:out value="${view.fetchmailAccount.values[name]}" /></td>
                                        </tr>
                                    </c:forEach>
                                    <tr>
                                        <td style="text-align: left;"><fmt:message key="status" /></td>
                                        <td>
                                            <c:choose>
                                                <c:when test="${view.fetchmailAccount.disabled}">
                                                    <a href="<v:url value="${view.baseLink}/enableMailAccount"/>"> <fmt:message key="deactivated" /></a>
                                                </c:when>
                                                <c:otherwise>
                                                    <a href="<v:url value="${view.baseLink}/disableMailAccount"/>"> <fmt:message key="active" /></a>
                                                </c:otherwise>
                                            </c:choose>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </c:if>
</o:permission>
