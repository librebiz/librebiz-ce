/**
 *
 * Copyright (C) 2015 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 30.06.2015 
 * 
 */
package com.osserp.api

import javax.persistence.Entity
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@Entity
@Table(name='ws_business_case_item')
class WsBusinessCaseItem extends WsItem {

    private static final long serialVersionUID = 4L

    WsBusinessCaseItem() {
        super()
    }

    WsBusinessCaseItem(
        WsBusinessCase wsBusinessCase, 
        WsProduct product, 
        Double quantity, 
        BigDecimal price, 
        String note) {
        super(quantity, price, note)
        this.wsBusinessCase = wsBusinessCase
        this.product = product
    }

    @ManyToOne
    @JoinColumn(name = "business_case_id")
    WsBusinessCase wsBusinessCase

    @ManyToOne
    @JoinColumn(name = "product_id")
    WsProduct product
}
