/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 12, 2016
 * 
 */
package com.osserp.gui.accounting


import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.gui.CoreController

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
@RequestMapping("/accounting/annualReport/*")
@Controller class AnnualReportController extends CoreController {
    private static Logger logger = LoggerFactory.getLogger(AnnualReportController.class.getName())
    
    @RequestMapping
    def selectBankAccountDocuments(HttpServletRequest request) {
        logger.debug("selectBankAccountDocuments: invoked [user=${getUserId(request)}]")
        AnnualReportView view = getView(request)
        try {
            view.selectBankAccountDocuments()
        } catch (Exception e) {
            saveError(request, e.message)
            return defaultPage
        }
        defaultPage
    }
    
    @RequestMapping
    def enableCashInListMode(HttpServletRequest request) {
        logger.debug("enableCashInListMode: invoked [user=${getUserId(request)}]")
        AnnualReportView view = getView(request)
        try {
            view.enableCashInListMode()
        } catch (Exception e) {
            saveError(request, e.message)
            return defaultPage
        }
        defaultPage
    }

    @RequestMapping
    def enableCashOutListMode(HttpServletRequest request) {
        logger.debug("enableCashOutListMode: invoked [user=${getUserId(request)}]")
        AnnualReportView view = getView(request)
        try {
            view.enableCashOutListMode()
        } catch (Exception e) {
            saveError(request, e.message)
            return defaultPage
        }
        defaultPage
    }

    @RequestMapping
    def enableSalesListMode(HttpServletRequest request) {
        logger.debug("enableSalesListMode: invoked [user=${getUserId(request)}]")
        AnnualReportView view = getView(request)
        try {
            view.enableSalesListMode()
        } catch (Exception e) {
            saveError(request, e.message)
            return defaultPage
        }
        defaultPage
    }

    @RequestMapping
    def enableSupplierListMode(HttpServletRequest request) {
        logger.debug("enableSupplierListMode: invoked [user=${getUserId(request)}]")
        AnnualReportView view = getView(request)
        try {
            view.enableSupplierListMode()
        } catch (Exception e) {
            saveError(request, e.message)
            return defaultPage
        }
        defaultPage
    }
    
    @RequestMapping
    def renderXls(HttpServletRequest request) {
        logger.debug("renderXls: invoked [user=${getUserId(request)}]")
        AnnualReportView view = getView(request)
        redirectXls(request, view.spreadsheet)
    }
    
    @RequestMapping
    def downloadArchive(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("downloadArchive: invoked [user=${getUserId(request)}]")
        AnnualReportView view = getView(request)
        try {
            downloadDocumentData(request, response, view.reportArchive)
        } catch (Exception e) {
            logger.debug("downloadArchive: failed [message=${e.message}]", e)
            saveError(request, e.message)
            return defaultPage
        }
    }
    
}
