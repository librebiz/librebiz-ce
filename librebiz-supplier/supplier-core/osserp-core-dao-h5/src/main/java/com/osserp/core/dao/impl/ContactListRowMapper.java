/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 7, 2006 8:22:46 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import com.osserp.common.OptionsCache;

import com.osserp.core.model.contacts.ContactListEntryVO;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ContactListRowMapper extends BusinessObjectRowMapper {
    
    public ContactListRowMapper(OptionsCache optionsCache) {
        super(optionsCache);
    }

    public static RowMapperResultSetExtractor getReader(OptionsCache optionsCache) {
        return new RowMapperResultSetExtractor(new ContactListRowMapper(optionsCache));
    }

    public Object mapRow(ResultSet rs, int index) throws SQLException {
        Long id = Long.valueOf(rs.getLong(1));
        StringBuilder name = createContactName(
                rs.getString(3),
                rs.getString(4));
        ContactListEntryVO vo = new ContactListEntryVO(id, Long.valueOf(rs.getLong(2)), null, name.toString());
        vo.setStreet(rs.getString(5));
        String zipcode = rs.getString(6);
        if (zipcode != null) {
            vo.setCity(zipcode + " " + rs.getString(7));
        } else {
            vo.setCity(rs.getString(7));
        }
        return vo;
    }

    public static StringBuilder getQuery(String contactsTable) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT contact_id,salutation,firstname,lastname," +
                "street,zipcode,city FROM ").append(contactsTable);
        return sql;
    }

}
