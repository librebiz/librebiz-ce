/**
 *
 * Copyright (C) 2015 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 18.08.2015 
 * 
 */
package com.osserp.gui.admin.dms

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.FileObject
import com.osserp.common.dms.DmsDocument
import com.osserp.common.dms.TemplateDmsManager

import com.osserp.groovy.web.ViewContextException

import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class AbstractTemplateDmsView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(AbstractTemplateDmsView.class.getName())

    protected AbstractTemplateDmsView() {
        super()
    }

    void reload() {
        list = templateDmsManager.findDocuments()
        logger.debug("reload: done [count=${list?.size()}]")
    }

    @Override
    void save() {
        DmsDocument doc = bean
        if (!doc) {
            throw new ViewContextException(ErrorCode.OPERATION_NOT_SUPPORTED)
        }
        String note = form.getString('note')
        Long categoryId = form.getLong('categoryId')
        if (!categoryId && categoryRequired) {
            throw new ClientException(ErrorCode.CATEGORY_MISSING)
        }
        FileObject upload = form.getUpload()
        logger.debug("save: invoked [upload=${upload?.fileName}, note=${note}, categoryId=${categoryId}]")
        if (!upload) {
            throw new ClientException(ErrorCode.FILE_ACCESS)
        }
        templateDmsManager.updateDocument(domainUser, doc, upload, note, categoryId)
        bean = null
        disableEditMode()
        reload()
    }

    @Override
    void select() {
        super.select()
        if (bean) {
            enableEditMode()
        } else {
            disableEditMode()
        }
    }

    boolean isCustomTemplate() {
        false
    }

    boolean isDocumentDeleteSupported() {
        false
    }
    
    protected boolean isCategoryRequired() {
        false
    }
    
    protected abstract TemplateDmsManager getTemplateDmsManager();
}
