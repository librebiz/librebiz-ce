/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 28, 2013 7:55:22 AM 
 * 
 */
package com.osserp.common.web.tags;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.web.RequestUtil;


/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class FormTag extends AbstractTagSupport {
    private static Logger log = LoggerFactory.getLogger(FormTag.class.getName());

    private String name;
    private String url;
    private boolean disableAutoupdate;
    private String onsubmit;
    private boolean updateonly;
    

    public boolean isDisableAutoupdate() {
        return disableAutoupdate;
    }

    public void setDisableAutoupdate(boolean disableAutoupdate) {
        this.disableAutoupdate = disableAutoupdate;
    }

    public String getName() {
        return (name);
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public String getOnsubmit() {
        return onsubmit;
    }

    public void setOnsubmit(String onsubmit) {
        this.onsubmit = onsubmit;
    }

    public boolean isUpdateonly() {
        return updateonly;
    }

    public void setUpdateonly(boolean updateonly) {
        this.updateonly = updateonly;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public int doStartTag() throws JspException {
        try {
            String formUrl = createUrl(url);
            String[] csrfToken = RequestUtil.getCSRFToken(getRequest(), formUrl);
            JspWriter out = pageContext.getOut();
            out.print(TagUtil.createForm(
                    id,
                    name,
                    formUrl,
                    appendOnsubmit(new StringBuilder(renderStyles())),
                    disableAutoupdate,
                    updateonly,
                    csrfToken[0],
                    csrfToken[1]));
        } catch (IOException ioe) {
            log.error("doStartTag: error getting writer");
        }
        return (EVAL_BODY_INCLUDE);
    }

    @Override
    public int doEndTag() throws JspException {
        try {
            JspWriter out = pageContext.getOut();
            out.print("</form>");
        } catch (IOException ioe) {
            log.error("doEndTag: error getting writer");
        }
        return (EVAL_BODY_INCLUDE);
    }
    
    protected String appendOnsubmit(StringBuilder commonAttributes) {
        if (onsubmit != null) {
            commonAttributes.append(" onsubmit=\"").append(onsubmit).append("\"");
        }
        return commonAttributes.toString();
    }
}
