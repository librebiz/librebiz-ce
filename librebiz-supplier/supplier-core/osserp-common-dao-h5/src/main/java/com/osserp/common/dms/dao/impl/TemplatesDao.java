/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 14, 2006 
 * 
 */
package com.osserp.common.dms.dao.impl;

import java.util.Date;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.ClientException;
import com.osserp.common.FileObject;
import com.osserp.common.User;
import com.osserp.common.dao.Tables;
import com.osserp.common.dms.DmsDocument;
import com.osserp.common.dms.DmsReference;
import com.osserp.common.dms.DocumentType;
import com.osserp.common.dms.dao.DocumentEncoder;
import com.osserp.common.dms.dao.Templates;
import com.osserp.common.dms.model.DmsDocumentImpl;
import com.osserp.common.service.SecurityService;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class TemplatesDao extends AbstractFilesystemDocumentsDao implements Templates {

    protected TemplatesDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            String documentTableKey,
            String categoryTableKey, 
            DocumentEncoder documentEncoder, 
            SecurityService authenticationProvider) {
        super(
                jdbcTemplate,
                tables,
                sessionFactory,
                null,
                documentTableKey,
                categoryTableKey,
                documentEncoder,
                authenticationProvider);
    }

    public DmsDocument create(
            User user, 
            DocumentType docType, 
            String filename, 
            FileObject fileObject, 
            String note, 
            Long categoryId) throws ClientException {
        return create(user, docType, filename, fileObject, note,
                categoryId, null, null, null);
    }

    @Override
    protected DmsDocument createObject(
            User user,
            DocumentType documentType,
            DmsReference reference,
            String fileName,
            long fileSize,
            String note,
            Date sourceDate,
            Long categoryId,
            Date validFrom,
            Date validTil,
            String messageId) {
        return new DmsDocumentImpl(
                documentType,
                fileName,
                fileSize,
                user.getId(),
                note,
                categoryId);
    }

    @Override
    protected Class<DmsDocumentImpl> getPersistentClass() {
        return DmsDocumentImpl.class;
    }

}
