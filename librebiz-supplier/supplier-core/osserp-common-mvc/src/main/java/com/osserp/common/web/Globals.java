/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 8, 2004 
 * 
 */
package com.osserp.common.web;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class Globals {

    public static final String PAGE_SCOPE = "page";
    public static final String REQUEST_SCOPE = "request";
    public static final String SESSION_SCOPE = "session";
    public static final String APPLICATION_SCOPE = "application";

    public static final String PORTAL_VIEW = "portalView";

    // application, session and request properties
    public static final String ACTION = "action";
    public static final String COLLECTION = "collection";
    public static final String CURRENT_PAGE_NAME = "currentPageName";
    
    public static final String ERRORS = "errors";
    public static final String ERRORS_BACKEND = "errorsBackend";
    public static final String ERROR_DETAILS = "errorDetails";
    public static final String ERROR_ID = "errorId";
    public static final String LAST_REQUESTED_ID = "lastRequestedId";
    public static final String LOCALE_KEY = "org.apache.struts.action.LOCALE";
    public static final String LOCALE_FALLBACK_REQUEST = "com.osserp.common.web.LOCALE_FALLBACK_REQUEST";
    public static final String LOCALIZATION_CONTEXT_SESSION = "javax.servlet.jsp.jstl.fmt.localizationContext.session";
    public static final String MAIL_EXIT = "mailExit";
    public static final String MAIL_MESSAGE = "mailMessage";
    public static final String MAX_COUNT_PER_PAGE = "maxCountPerPage";
    public static final String MENU = "menu";
    public static final String MESSAGE = "message";
    public static final String OBJECT_LOCK = "objectLock";
    public static final String ONLINE_USERS = "onlineUsers";
    public static final String SPRING_WEB = "org.springframework.web";
    public static final String STATUS_MESSAGE = "statusMessage";
    public static final String STYLESHEET_LANGUAGE = "stylesheetLanguage";
    public static final String TABINDEX = "tabindex";
    public static final String TARGET = "target";
    public static final String USER = "user";
    public static final String WEB_APP_CTX_PREFIX = SPRING_WEB + ".context.WebApplicationContext"; 
    public static final String WEB_APP_CTX_ROOT = WEB_APP_CTX_PREFIX + ".ROOT";
    public static final String WEB_CONFIG = "webConfig";
    public static final String XLS_DOCUMENT = "xlsDocument";
    public static final String XML_DOCUMENT = "xmlDocument";
    public static final String XSL_STYLE_SHEET = "xslStylesheet";

}
