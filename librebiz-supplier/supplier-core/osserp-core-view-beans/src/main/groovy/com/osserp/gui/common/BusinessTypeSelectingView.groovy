/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 16, 2016 
 * 
 */
package com.osserp.gui.common

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.core.BusinessType
import com.osserp.core.BusinessTypeManager

import com.osserp.groovy.web.MenuItem

import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class BusinessTypeSelectingView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(BusinessTypeSelectingView.class.getName())
    
    List<BusinessType> businessTypes = []
    BusinessType selectedType = null

    BusinessTypeSelectingView() {
        super()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (forwardRequest) {
            businessTypes = fetchBusinessTypes()
            logger.debug("initRequest: businessTypes fetched [count=${businessTypes.size()}]")
        }
    }
    
    /**
     * Selects businessType by request param 'id'. Resets selectedType
     * if id not provided.
     */
    void selectBusinessType() {
        Long id = form.getLong('id')
        if (id) {
            selectedType = businessTypeManager.load(id)
        } else {
            selectedType = null
        }
    }
    
    MenuItem getBusinessTypeDeselectLink() {
        navigationLink("/${context.name}/selectBusinessType", 'backIcon', 'backToLast')
    }

    /**
     * Provides the business types to load the selection list. Override this
     * method to provide an individual selection.
     * The default implementation provides all active types.
     * @return businessTypes required to load the selection list
     */
    protected List<BusinessType> fetchBusinessTypes() {
        businessTypeManager.findActive()
    }

    protected BusinessTypeManager getBusinessTypeManager() {
        getService(BusinessTypeManager.class.getName())
    }
}
