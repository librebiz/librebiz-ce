/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 1, 2010 11:14:27 AM 
 * 
 */
package com.osserp.core.telephone;

import com.osserp.common.Entity;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public interface AbstractTelephoneConfiguration extends Entity {

    /**
     * Returns the accountCode
     * @return accountCode
     */
    public String getAccountCode();

    /**
     * Sets the accountCode
     * @param accountCode
     */
    public void setAccountCode(String accountCode);

    /**
     * Returns the displayName
     * @return displayName
     */
    public String getDisplayName();

    /**
     * Sets the displayName
     * @param displayName
     */
    public void setDisplayName(String displayName);

    /**
     * Returns the emailAddress
     * @return emailAddress
     */
    public String getEmailAddress();

    /**
     * Sets the emailAddress
     * @param emailAddress
     */
    public void setEmailAddress(String emailAddress);

    /**
     * Returns the uid
     * @return uid
     */
    public String getUid();

    /**
     * Sets the uid
     * @param uid
     */
    public void setUid(String uid);

    /**
     * Indicates that voiceMail is enabled
     * @return voiceMailEnabled
     */
    public boolean isVoiceMailEnabled();

    /**
     * Sets the voiceMailEnabled
     * @param voiceMailEnabled
     */
    public void setVoiceMailEnabled(boolean voiceMailEnabled);

    /**
     * Indicates that telephoneExchange is enabled
     * @return telephoneExchange
     */
    public boolean isTelephoneExchange();

    /**
     * Sets the telephoneExchange
     * @param telephoneExchange
     */
    public void setTelephoneExchange(boolean telephoneExchange);

    /**
     * Returns the telephoneGroup
     * @return telephoneGroup
     */
    public TelephoneGroup getTelephoneGroup();

    /**
     * Sets the telephoneGroup
     * @param telephoneGroup
     */
    public void setTelephoneGroup(TelephoneGroup telephoneGroup);

    /**
     * Indicates that voiceMailSendAsEmail is Enabled
     * @return voiceMailSendAsEmail
     */
    public boolean isVoiceMailSendAsEmail();

    /**
     * Sets the voiceMailSendAsEmail
     * @param voiceMailSendAsEmail
     */
    public void setVoiceMailSendAsEmail(boolean voiceMailSendAsEmail);

    /**
     * Indicates that voiceMailDeleteAfterEmailSend is Enabled
     * @return voiceMailDeleteAfterEmailSend
     */
    public boolean isVoiceMailDeleteAfterEmailSend();

    /**
     * Sets the voiceMailDeleteAfterEmailSend
     * @param voiceMailDeleteAfterEmailSend
     */
    public void setVoiceMailDeleteAfterEmailSend(boolean voiceMailDeleteAfterEmailSend);

    /**
     * Indicates that busyOnBusy is Enabled
     * @return busyOnBusy
     */
    public boolean isBusyOnBusy();

    /**
     * Sets the busyOnBusy
     * @param busyOnBusy
     */
    public void setBusyOnBusy(boolean busyOnBusy);

    /**
     * Indicates that available is Enabled
     * @return available
     */
    public boolean isAvailable();

    /**
     * Sets the available
     * @param available
     */
    public void setAvailable(boolean available);

    /**
     * Returns the voiceMailDelaySeconds
     * @return voiceMailDelaySeconds
     */
    public Integer getVoiceMailDelaySeconds();

    /**
     * Sets the voiceMailDelaySeconds
     * @param voiceMailDelaySeconds
     */
    public void setVoiceMailDelaySeconds(Integer voiceMailDelaySeconds);

    /**
     * Indicates that voiceMail is enabled IfBusy
     * @return voiceMailEnabledIfBusy
     */
    public boolean isVoiceMailEnabledIfBusy();

    /**
     * Sets the voiceMailEnabledIfBusy
     * @param voiceMailEnabledIfBusy
     */
    public void setVoiceMailEnabledIfBusy(boolean voiceMailEnabledIfBusy);

    /**
     * Indicates that voiceMail is enabled IfUnavailable
     * @return voiceMailEnabledIfUnavailable
     */
    public boolean isVoiceMailEnabledIfUnavailable();

    /**
     * Sets the voiceMailEnabledIfUnavailable
     * @param voiceMailEnabledIfUnavailable
     */
    public void setVoiceMailEnabledIfUnavailable(
            boolean voiceMailEnabledIfUnavailable);

    /**
     * Returns the parallelCallTargetPhoneNumber
     * @return parallelCallTargetPhoneNumber
     */
    public String getParallelCallTargetPhoneNumber();

    /**
     * Sets the parallelCallTargetPhoneNumber
     * @param parallelCallTargetPhoneNumber
     */
    public void setParallelCallTargetPhoneNumber(
            String parallelCallTargetPhoneNumber);

    /**
     * Indicates that ParallelCall is enabled
     * @return parallelCallEnabled
     */
    public boolean isParallelCallEnabled();

    /**
     * Sets the telephone
     * @param parallelCallEnabled
     */
    public void setParallelCallEnabled(boolean parallelCallEnabled);

    /**
     * Returns the parallelCallSetDelaySeconds
     * @return parallelCallSetDelaySeconds
     */
    public Integer getParallelCallSetDelaySeconds();

    /**
     * Sets the parallelCallSetDelaySeconds
     * @param parallelCallSetDelaySeconds
     */
    public void setParallelCallSetDelaySeconds(Integer parallelCallSetDelaySeconds);

    /**
     * Returns the callForwardTargetPhoneNumber
     * @return callForwardTargetPhoneNumber
     */
    public String getCallForwardTargetPhoneNumber();

    /**
     * Sets the callForwardTargetPhoneNumber
     * @param callForwardTargetPhoneNumber
     */
    public void setCallForwardTargetPhoneNumber(String callForwardTargetPhoneNumber);

    /**
     * Returns the callForwardDelaySeconds
     * @return callForwardDelaySeconds
     */
    public Integer getCallForwardDelaySeconds();

    /**
     * Sets the callForwardDelaySeconds
     * @param callForwardDelaySeconds
     */
    public void setCallForwardDelaySeconds(Integer callForwardDelaySeconds);

    /**
     * Indicates that callForward is enabled
     * @return callForwardEnabled
     */
    public boolean isCallForwardEnabled();

    /**
     * Sets the callForwardEnabled
     * @param callForwardEnabled
     */
    public void setCallForwardEnabled(boolean callForwardEnabled);

    /**
     * Returns the callForwardIfBusyTargetPhoneNumber
     * @return callForwardIfBusyTargetPhoneNumber
     */
    public String getCallForwardIfBusyTargetPhoneNumber();

    /**
     * Sets the callForwardIfBusyTargetPhoneNumber
     * @param callForwardIfBusyTargetPhoneNumber
     */
    public void setCallForwardIfBusyTargetPhoneNumber(
            String callForwardIfBusyTargetPhoneNumber);

    /**
     * Indicates that callForwardIfBusy is Enabled
     * @return callForwardIfBusyEnabled
     */
    public boolean isCallForwardIfBusyEnabled();

    /**
     * Sets the callForwardIfBusyEnabled
     * @param callForwardIfBusyEnabled
     */
    public void setCallForwardIfBusyEnabled(boolean callForwardIfBusyEnabled);

    /**
     * Indicates that config is default
     * @return defaultConfig
     */
    public boolean isDefaultConfig();
}
