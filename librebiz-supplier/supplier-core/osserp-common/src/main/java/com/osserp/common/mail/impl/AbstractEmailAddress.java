/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 06.12.2004 
 * 
 */
package com.osserp.common.mail.impl;

import com.osserp.common.beans.AbstractEntity;
import com.osserp.common.mail.EmailAddress;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractEmailAddress extends AbstractEntity implements EmailAddress {
    private static final long serialVersionUID = 42L;

    private Long contactId;
    private Long type = null;
    private boolean primary = false;
    private String email = null;
    private Long aliasOf = null;
    private String validationStatus = EmailAddress.VALIDATED_NOTHING;

    protected AbstractEmailAddress() {
        super();
    }

    protected AbstractEmailAddress(String email) {
        super();
        this.email = email;
    }

    protected AbstractEmailAddress(
            Long createdBy,
            Long contactId,
            Long type,
            String email,
            boolean primary,
            Long aliasOf) {
        super((Long) null, (Long) null, createdBy);
        this.contactId = contactId;
        this.type = type;
        this.email = email;
        this.aliasOf = aliasOf;
        this.primary = primary;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isPrimary() {
        return primary;
    }

    public void setPrimary(boolean primary) {
        this.primary = primary;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public Long getContactId() {
        return contactId;
    }

    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }

    public Long getAliasOf() {
        return aliasOf;
    }

    public void setAliasOf(Long aliasOf) {
        this.aliasOf = aliasOf;
    }

    public String getValidationStatus() {
        return validationStatus;
    }

    public void setValidationStatus(String validationStatus) {
        this.validationStatus = validationStatus;
    }

    public boolean isAlias() {
        return (type == null ? false : (EmailAddress.ALIAS.equals(type)));
    }

    public boolean isBusiness() {
        return (type == null ? false : (EmailAddress.BUSINESS.equals(type)));
    }

    public boolean isInternal() {
        return (type == null ? false : (EmailAddress.INTERNAL.equals(type)));
    }

    public boolean isPrivate() {
        return (type == null ? true : (EmailAddress.PRIVATE.equals(type)));
    }
}
