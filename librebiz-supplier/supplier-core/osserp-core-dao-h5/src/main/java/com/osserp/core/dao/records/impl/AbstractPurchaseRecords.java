/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 30-Jan-2007 13:53:40 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.sql.Types;
import java.util.Date;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;

import com.osserp.core.BusinessType;
import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.dao.PaymentConditions;
import com.osserp.core.dao.Suppliers;
import com.osserp.core.dao.SystemCompanies;
import com.osserp.core.dao.TaxRates;
import com.osserp.core.dao.records.PurchasePayments;
import com.osserp.core.dao.records.PurchaseRecords;
import com.osserp.core.dao.records.RecordInfoConfigs;
import com.osserp.core.dao.records.RecordMailLogs;
import com.osserp.core.dao.records.RecordPrintOptionDefaults;
import com.osserp.core.finance.PaymentCondition;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordNumberCreator;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractPurchaseRecords extends AbstractRecords implements PurchaseRecords {

    private Suppliers suppliers = null;
    private String recordTableName = null;
    private String recordItemTableName = null;

    public AbstractPurchaseRecords(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache cache,
            TaxRates taxRates,
            SystemCompanies companies,
            RecordInfoConfigs infoConfigs,
            PaymentConditions paymentConditions,
            RecordNumberCreator recordNumberCreator,
            RecordPrintOptionDefaults recordPrintOptionDefaults,
            RecordMailLogs recordMailLogs,
            Suppliers suppliers,
            String recordTableKey,
            String recordItemTableKey) {
        super(
                jdbcTemplate,
                tables,
                sessionFactory,
                cache,
                taxRates,
                companies,
                infoConfigs,
                paymentConditions,
                recordNumberCreator,
                recordPrintOptionDefaults,
                recordMailLogs);
        recordTableName = getTable(recordTableKey);
        if (recordTableName == null) {
            throw new IllegalStateException("recordTableName must not be null");
        }
        recordItemTableName = getTable(recordItemTableKey);
        if (recordItemTableName == null) {
            throw new IllegalStateException("recordItemTableName must not be null");
        }
        this.suppliers = suppliers;
    }

    public AbstractPurchaseRecords(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache cache,
            TaxRates taxRates,
            SystemCompanies companies,
            RecordInfoConfigs infoConfigs,
            PaymentConditions paymentConditions,
            RecordNumberCreator recordNumberCreator,
            RecordPrintOptionDefaults recordPrintOptionDefaults,
            RecordMailLogs recordMailLogs,
            Suppliers suppliers,
            String recordTableKey,
            String recordItemTableKey,
            PurchasePayments purchasePayments) {
        super(
                jdbcTemplate,
                tables,
                sessionFactory,
                cache,
                taxRates,
                companies,
                infoConfigs,
                paymentConditions,
                recordNumberCreator,
                recordPrintOptionDefaults,
                recordMailLogs,
                purchasePayments);
        recordTableName = getTable(recordTableKey);
        if (recordTableName == null) {
            throw new IllegalStateException("recordTableName must not be null");
        }
        recordItemTableName = getTable(recordItemTableKey);
        if (recordItemTableName == null) {
            throw new IllegalStateException("recordItemTableName must not be null");
        }
        this.suppliers = suppliers;
    }

    public boolean isContactInvolved(ClassifiedContact contact) {
        StringBuilder sql = new StringBuilder("SELECT count(*) FROM ");
        sql.append(recordTableName).append(" WHERE contact_id = ").append(contact.getId());
        return jdbcTemplate.queryForObject(sql.toString(), Integer.class) > 0;
    }

    public final boolean exists(Long recordId) {
        return exists(recordTableName, recordId);
    }

    public Record getBySupplierReference(Long supplier, String supplierReference) {
        StringBuilder query = new StringBuilder("SELECT id FROM ");
        query
                .append(getRecordTableName())
                .append(" WHERE contact_id = ? AND supplier_reference = ?");
        Object[] params = { supplier, supplierReference };
        int[] types = { Types.BIGINT, Types.VARCHAR };
        List<Long> resultList = (List<Long>) jdbcTemplate.query(query.toString(), params, types, getLongRowMapper());
        return (resultList.isEmpty() ? null : load(resultList.get(0)));
    }

    public final int count(Long referenceId) {
        return count(recordTableName, referenceId);
    }

    public final List<Long> getPrinted() {
        return getPrinted(recordTableName);
    }

    public final Date getCreated(Long id) {
        return getCreated(recordTableName, id);
    }

    public String getRecordTableName() {
        return recordTableName;
    }

    public String getRecordItemTableName() {
        return recordItemTableName;
    }

    protected Suppliers getSuppliers() {
        return suppliers;
    }

    @Override
    protected PaymentCondition getDefaultCondition() {
        return getPaymentConditionDefault(false);
    }

    @Override
    protected Long getOpenStatus() {
        return Record.STAT_CLOSED;
    }

    @Override
    protected BusinessType getRequestType(Record record) {
        // purchase records do not support business types
        return null;
    }
}
