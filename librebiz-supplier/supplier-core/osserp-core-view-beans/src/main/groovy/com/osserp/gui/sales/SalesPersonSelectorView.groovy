/**
 *
 * Copyright (C) 2009, 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.gui.sales

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode

import com.osserp.core.BusinessCase
import com.osserp.core.employees.Employee
import com.osserp.core.requests.Request
import com.osserp.core.requests.RequestManager

import com.osserp.groovy.web.ViewContextException

import com.osserp.gui.employees.EmployeeSelectionView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class SalesPersonSelectorView extends EmployeeSelectionView {
    private static Logger logger = LoggerFactory.getLogger(SalesPersonSelectorView.class.getName())
    Request request
    Boolean coSales = false

    SalesPersonSelectorView() {
        enablePopupView()
        dependencies = ['businessCaseView']
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (!request) {
            if (!env.businessCaseView?.bean) {
                throw new ViewContextException(ErrorCode.INVALID_CONTEXT)
            }
            request = env.businessCaseView.request
            if (isSystemPropertyEnabled('salesPersonSelectorByGroup')) {
                list = salesPersonList
            } else {
                list = activatedEmployeeList
            }
            if (forwardRequest) {
                coSales = form.getBoolean('coSales')
                logger.debug("initRequest: done [request=${request.requestId}, coSales=${coSales}]")
            }
        }
    }

    @Override
    void select() {
        Long id = form.getLong('id')
        if (id) {
            if (coSales) {
                requestManager.updateSalesCo(request, id)
            } else {
                requestManager.setSales(user, request, id)
            }
            env.businessCaseView.reload()
        }
    }

    void selectType() {
        if ('coSales' == form.getString('type')) {
            coSales = true
        } else {
            coSales = false
        }
    }

    boolean isSalesCoMode() {
        coSales
    }

    boolean isSalesCoPercentMode() {
        env.salesCoPercentMode
    }

    def disableSalesCoPercentMode() {
        env.salesCoPercentMode = false
    }

    def enableSalesCoPercentMode() {
        env.salesCoPercentMode = true
    }

    def updateSalesCoPercent() {
        Double value = form.getPercentage('value')
        if (value) {
            requestManager.updateSalesCoPercent(request, value)
            env.businessCaseView.reload()
            disableSalesCoPercentMode()
        }
    }
    
    protected RequestManager getRequestManager() {
        getService(RequestManager.class.getName())
    }
}
