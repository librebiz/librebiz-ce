/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 1, 2006 11:11:43 AM 
 * 
 */
package com.osserp.gui.client.sales;

import com.osserp.common.ClientException;
import com.osserp.common.web.Form;
import com.osserp.common.web.ViewName;
import com.osserp.core.calc.Calculation;
import com.osserp.gui.BusinessCaseView;
import com.osserp.gui.client.records.DiscountAwareRecordView;
import com.osserp.gui.client.records.RebateAwareRecordView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("salesOfferView")
public interface SalesOfferView extends DiscountAwareRecordView, RebateAwareRecordView {

    /**
     * Indicates that associated business case is closed. View layer must not perform any offer changing actions in this case
     * @return businessCaseClosed
     */
    public boolean isBusinessCaseClosed();

    /**
     * Provides the business case view
     * @return businessCaseView
     */
    public BusinessCaseView getBusinessCaseView();

    /**
     * Creates a new offer with given calculation
     * @param calc
     * @throws ClientException
     */
    public void createOffer(Calculation calc, boolean copy) throws ClientException;

    /**
     * Creates a new offer by currently selected offer (free-form offer without backed calculation)
     * @param form
     * @throws ClientException if validation or create copy failed
     */
    public void createCopy(Form form) throws ClientException;

    /**
     * Loads offers by business case
     * @param businessCaseView
     * @throws ClientException if no offer available wich indicates that no calculation has been created before
     */
    public void load(BusinessCaseView businessCaseView) throws ClientException;

    /**
     * Loads offers by business case, selects offer for given calculation
     * @param businessCaseView
     * @param calc
     * @throws ClientException
     */
    public void load(BusinessCaseView businessCaseView, Calculation calc) throws ClientException;

    /**
     * Initializes activated offer if exists.
     * @return true if exists and initialized
     */
    public boolean initializeActivated();

    /**
     * Indicates that an unreleased offer is available
     * @return true if so
     */
    public boolean isUnreleasedOfferAvailable();

    /**
     * Indicates that only one offer is available
     * @return onlyOneOfferAvailable
     */
    public boolean isOnlyOneOfferAvailable();

    /**
     * Indicates if feature 'copy selected offer without calculation' is available
     * @return true if free-form offer could be copied
     */
    public boolean isCreateCopyAvailable();

    /**
     * Indicates if feature 'copy selected offer by calculation' is available (e.g. requestType.rcordByCalculation reports true and selected offer is released)
     * @return true if creating new offer by backed calculation is
     */
    public boolean isCreateCopyByCalculationAvailable();

    /**
     * Indicates that view is in copy mode
     */
    public boolean isCopyMode();

    /**
     * Enables/disables copy mode
     * @param copyMode
     */
    public void setCopyMode(boolean copyMode);

    /**
     * Indicatews availability of status reset method
     * @return true if current offer is latest and user has required permission
     */
    public boolean isStatusResetAvailable();

    /**
     * Reset released status if statusReset is available
     */
    public void resetStatus();

    /**
     * Provides the calculation of current selected offer
     * @return calculation
     */
    public Calculation getCalculation();
}
