/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 28, 2009 9:45:26 AM 
 * 
 */
package com.osserp.core.products;

import java.util.List;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ProductTypeConfig extends ProductType {

    /**
     * Provides all associated groups
     * @return groups
     */
    List<ProductGroupConfig> getGroups();

    /**
     * Adds a group if not already exists
     * @param group
     */
    void addGroup(ProductGroupConfig group);

    /**
     * Moves a group from current to next list position
     * @param group
     */
    void moveNext(ProductGroupConfig group);

    /**
     * Moves a group from current to previous list position
     * @param group
     */
    void movePrevious(ProductGroupConfig group);

    /**
     * Removes a group
     * @param group
     */
    void removeGroup(ProductGroupConfig group);

    /**
     * Fetches a group from groups
     * @param id
     * @return group or null if not exists
     */
    ProductGroupConfig fetchGroup(Long id);

    /**
     * Indicates that a group is supported by type (e.g. fetchGroup != null)
     * @param group
     * @return true if group is member of groups
     */
    boolean supportsGroup(ProductClassificationEntity group);

    /**
     * Provides assignable product type value object
     * @return productType
     */
    ProductType getProductType();
}
