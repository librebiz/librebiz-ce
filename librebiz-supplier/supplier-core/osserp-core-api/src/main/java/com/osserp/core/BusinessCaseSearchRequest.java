/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 11-Jul-2005 12:59:43 
 * 
 */
package com.osserp.core;

import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class BusinessCaseSearchRequest extends CoreSearchRequest {

    private Long businessTypeId = 0L;

    protected BusinessCaseSearchRequest() {
        super();
    }

    /**
     * Creates a new project search request
     * @param domainUser
     * @param columnKey
     * @param pattern
     * @param startsWith
     * @param openOnly
     */
    public BusinessCaseSearchRequest(
            DomainUser domainUser, 
            String columnKey, 
            String pattern, 
            boolean startsWith, 
            boolean openOnly) {
        super(domainUser, columnKey, pattern, startsWith, openOnly);
    }

    /**
     * Creates a new sales search request
     * @param domainUser
     * @param columnKey
     * @param pattern
     * @param startsWith
     * @param openOnly
     * @param businessTypeId
     */
    public BusinessCaseSearchRequest(
            DomainUser domainUser,
            String columnKey,
            String pattern,
            boolean startsWith,
            boolean openOnly,
            Long businessTypeId) {

        super(domainUser, columnKey, pattern, startsWith, openOnly);
        this.businessTypeId = businessTypeId;
    }

    /**
     * Creates a new sales search request
     * @param domainUser
     * @param columnKey
     * @param pattern
     * @param startsWith
     * @param includeCanceled
     * @param openOnly
     * @param branchId
     * @param businessTypeId
     */
    public BusinessCaseSearchRequest(
            DomainUser domainUser,
            String columnKey,
            String pattern,
            boolean startsWith,
            boolean includeCanceled,
            boolean openOnly,
            Long branchId,
            Long businessTypeId) {

        super(domainUser, columnKey, pattern, startsWith, openOnly, branchId);
        setIncludeCanceled(includeCanceled);
        this.businessTypeId = businessTypeId;
    }

    /**
     * Creates a new sales search request
     * @param domainUser
     * @param columnKey
     * @param pattern
     * @param startsWith
     * @param includeCanceled
     * @param openOnly
     * @param branchId
     * @param businessTypeId
     */
    public BusinessCaseSearchRequest(
            BusinessCaseSearchRequest previous,
            String columnKey,
            String pattern,
            boolean startsWith,
            boolean includeCanceled,
            boolean openOnly,
            Long branchId,
            Long businessTypeId) {

        super(previous, columnKey, pattern, startsWith, openOnly, branchId);
        if (previous != null && (isChanged(isIncludeCanceled(), includeCanceled)
                || isChanged(this.businessTypeId, businessTypeId))) {
            setChanged(true);
        }
        setIncludeCanceled(includeCanceled);
        this.businessTypeId = businessTypeId;
    }

    /**
     * Provides selected business type
     * @return businessType or null if non selected
     */
    public Long getBusinessTypeId() {
        return businessTypeId;
    }

    /**
     * Sets selected business type id
     * @param businessTypeId
     */
    public void setBusinessTypeId(Long businessTypeId) {
        this.businessTypeId = businessTypeId;
    }

}
