/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 24-Dec-2006 09:51:47 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.sql.Types;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.common.dao.Tables;
import com.osserp.common.util.CollectionUtil;
import com.osserp.common.util.DateFormatter;
import com.osserp.common.util.DateUtil;

import com.osserp.core.Options;
import com.osserp.core.dao.ProductSelectionConfigs;
import com.osserp.core.dao.Products;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.dao.impl.AbstractDao;
import com.osserp.core.dao.records.PurchaseInvoices;
import com.osserp.core.dao.records.Stocktakings;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.Stocktaking;
import com.osserp.core.model.StocktakingImpl;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductSelectionConfig;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class StocktakingsDao extends AbstractDao implements Stocktakings {
    private static Logger log = LoggerFactory.getLogger(StocktakingsDao.class.getName());
    private static final Long VIRTUAL_TYPE = 2L;
    private static final Long DEFAULT_PRODUCT_SELECTION_CONFIG = 2L;
    private Products products = null;
    private String stocktakings = null;
    private String stocktakingTypes = null;
    private String stocktakingProductsView = null;
    private PurchaseInvoices purchaseInvoices = null;
    private ProductSelectionConfigs productSelectionConfigs = null;

    public StocktakingsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            Products products,
            PurchaseInvoices purchaseInvoices,
            ProductSelectionConfigs productSelectionConfigs) {
        super(jdbcTemplate, tables, sessionFactory);
        this.products = products;
        this.stocktakingProductsView = getTable(TableKeys.STOCKTAKING_PRODUCTS_VIEW);
        this.stocktakingTypes = getTable(Options.STOCKTAKING_TYPES);
        this.stocktakings = getTable(TableKeys.STOCKTAKINGS);
        this.purchaseInvoices = purchaseInvoices;
        this.productSelectionConfigs = productSelectionConfigs;
    }

    public Map<Long, Option> getStatus() {
        List<Option> list = getCurrentSession().createQuery(
                "from StocktakingStatusImpl t order by t.id").list();
        return CollectionUtil.createEntityMap(list);
    }

    public List<Option> getTypes() {
        return getCurrentSession().createQuery(
                "from StocktakingTypeImpl t order by t.id").list();
    }

    public List<Option> getClosed(Long company) {
        StringBuilder sql = new StringBuilder("SELECT s.id,s.name,t.name FROM ");
        sql
                .append(stocktakings)
                .append(" s, ")
                .append(stocktakingTypes)
                .append(" t WHERE s.type_id = t.id AND s.status_id = 10 AND s.company_id = ? ORDER BY id DESC");
        return (List<Option>) jdbcTemplate.query(
                sql.toString(),
                new Object[] { company },
                new int[] { Types.BIGINT },
                getInfoRowMapper());
    }

    public Stocktaking find(Long id) {
        return (Stocktaking) getCurrentSession().load(StocktakingImpl.class, id);
    }

    public Stocktaking getOpen(Long company) {
        List<Stocktaking> existing = getCurrentSession().createQuery(
                "from " + StocktakingImpl.class.getName()
                + " s where s.status.id < 10 and s.company = :companyId order by s.id desc")
                .setParameter("companyId", company).list();
        return existing.isEmpty() ? null : existing.get(0);
    }

    public void validate(
            List<Option> availableClosed,
            Stocktaking currentOpen,
            Option typeToValidate,
            String nameToValidate) throws ClientException {

        if (typeToValidate == null
                || typeToValidate.getId().equals(Constants.LONG_NULL)) {
            throw new ClientException(ErrorCode.TYPE_REQUIRED);
        }
        if (isEmpty(nameToValidate)) {
            throw new ClientException(ErrorCode.NAME_MISSING);
        }
        for (int i = 0, j = availableClosed.size(); i < j; i++) {
            Option next = availableClosed.get(i);
            if (next.getName().equals(nameToValidate)) {
                throw new ClientException(ErrorCode.NAME_EXISTS);
            }
        }
        if (currentOpen != null) {
            throw new ClientException(ErrorCode.RECORD_CHANGEABLE);
        }
    }

    public Stocktaking create(Long company, Option type, String name, Date recordDate, Long createdBy) throws ClientException {
        StocktakingImpl obj = new StocktakingImpl(
                company,
                type,
                getStatus(Stocktaking.CREATED),
                name,
                recordDate,
                createdBy);
        save(obj);
        refreshItems(obj);
        return obj;
    }

    public Stocktaking createVirtual(Long company, Long createdBy) {
        Date now = DateUtil.getCurrentDate();
        StocktakingImpl obj = new StocktakingImpl(
                company,
                getVirtualType(),
                getStatus(Stocktaking.CREATED),
                DateFormatter.getDate(now),
                now,
                createdBy);
        refreshItems(obj);
        return obj;
    }

    public void delete(Stocktaking st) {
        if (!st.isClosed()) {
            getCurrentSession().delete(StocktakingImpl.class.getName(), st);
        }
    }

    public void refreshItems(Stocktaking st) {
        if (!st.getItems().isEmpty()) {
            st.getItems().clear();
        }
        addItems(st);
        save(st);
    }

    private void addItems(Stocktaking st) {
        ProductSelectionConfig cfg = fetchProductSelectionConfig();
        List<Product> productList = products.getStockAware(true);
        Set<Long> referencedProducts = getStocktakingProducts();
        for (int i = 0, j = productList.size(); i < j; i++) {
            Product next = productList.get(i);
            if ((cfg == null || cfg.isMatching(next))
                    && referencedProducts.contains(next.getProductId())) {
                next.enableStock(st.getCompany());
                st.addItem(next);
            }
        }
    }
    
    private Set<Long> getStocktakingProducts() {
        Set<Long> result = new HashSet<Long>();
        StringBuilder sql = new StringBuilder("SELECT product_id FROM ");
        sql.append(stocktakingProductsView);
        List<Long> l = (List<Long>) jdbcTemplate.query(sql.toString(), getLongRowMapper());
        for (int i = 0, j = l.size(); i<j; i++) {
            result.add(l.get(i));
        }
        return result;
    }
    
    private ProductSelectionConfig fetchProductSelectionConfig() {
        try {
            return productSelectionConfigs.load(getProductSelectionConfig());
        } catch (Exception e) {
            // we ignore this
            log.warn("fetchProductSelectionConfig() ignoring exception [message=" + e.getMessage() + "]");
        }
        return null;
    }
    
    private Long getProductSelectionConfig() {
        Long result = getPropertyId("stocktakingProductSelectionConfig");
        if (isEmpty(result)) {
            if (log.isInfoEnabled()) {
                log.info("getProductSelectionConfig() system property missing, "
                        + "using hardcoded declaration from class [id="
                        + DEFAULT_PRODUCT_SELECTION_CONFIG + "]");
            }
            result = DEFAULT_PRODUCT_SELECTION_CONFIG;
        }
        return result;
    }

    public void setCountedAsExpected(Stocktaking st) {
        st.setCountedAsExpected();
        save(st);
    }

    public void resetCounters(Stocktaking st) {
        st.resetCounters();
        save(st);
    }

    public void markListPrinted(Stocktaking st) {
        st.setStatus(getStatus(Stocktaking.PRINTED));
        save(st);
    }

    public Record close(Employee employee, Stocktaking st) {
        if (log.isDebugEnabled()) {
            log.debug("close() invoked by " + employee.getId());
        }
        Record inv = purchaseInvoices.create(employee, st);
        if (log.isDebugEnabled()) {
            log.debug("close() created new invoice " + inv.getId());
        }
        inv.updateStatus(Record.STAT_CLOSED);
        purchaseInvoices.save(inv);
        purchaseInvoices.createStatusHistory(inv, Record.STAT_CLOSED, employee.getId());
        if (log.isDebugEnabled()) {
            log.debug("close() closed new invoice " + inv.getId());
        }
        st.setStatus(getStatus(Stocktaking.CLOSED));
        save(st);
        if (log.isDebugEnabled()) {
            log.debug("close() done");
        }
        return inv;
    }

    public void save(Stocktaking st) {
        getCurrentSession().saveOrUpdate(StocktakingImpl.class.getName(), st);
    }

    private Option getStatus(Long status) {
        Map<Long, Option> available = getStatus();
        return available.get(status);
    }

    private Option getVirtualType() {
        List<Option> types = getTypes();
        for (int i = 0, j = types.size(); i < j; i++) {
            Option next = types.get(i);
            if (next.getId().equals(VIRTUAL_TYPE)) {
                return next;
            }
        }
        return null;
    }
}
