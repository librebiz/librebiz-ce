/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Jun 18, 2010 at 10:45:08 AM 
 * 
 */
package com.osserp.gui.employees

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.core.employees.Employee
import com.osserp.core.employees.EmployeeDisciplinarian
import com.osserp.core.employees.EmployeeManager

import com.osserp.groovy.web.MenuItem
import com.osserp.groovy.web.ViewContextException

import com.osserp.gui.CoreView

/**
 * 
 * @author jg <jg@osserp.com>
 *
 */
class DisciplinarianView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(DisciplinarianView.class.getName())

    DisciplinarianView() {
        super()
        providesCustomNavigation()
        dependencies = ['contactView']
        headerName = 'disciplinarian'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (!env.contactView.employeeView || !env.contactView.bean) {
            logger.warn("initRequest: not bound [user=${user?.id}, name=employeeView]")
            throw new ViewContextException(this)
        }
        if (!list) {
            list = employeeManager.findDisciplinarians(env.contactView.bean)
        }
    }

    @Override
    protected void createCustomNavigation() {
        nav.listNavigation = [exitLink, addDisciplinarianLink, homeLink]
        nav.defaultNavigation = [exitLink, addDisciplinarianLink, homeLink]
    }

    MenuItem getAddDisciplinarianLink() {
        new MenuItem(link: "${context.fullPath}/contacts/contactSearch/forward?exit=/employees/disciplinarian/reload&selectionExit=${exitTarget}&selectionTarget=/employees/disciplinarian/save&context=employee", icon: 'newdataIcon', title: 'create')
    }

    @Override
    void save() {
        Long id = form.getLong('id')
        if (id) {
            Employee disciplinarian = employeeManager.find(id)
            if (disciplinarian) {
                if (!list.find { id.equals(it.disciplinarianId) }) {
                    employeeManager.addDisciplinarian(domainEmployee, env.contactView.bean, disciplinarian)
                    list = employeeManager.findDisciplinarians(env.contactView.bean)
                }
            }
        }
    }

    void remove() {
        Long id = form.getLong('id')
        if (id) {
            EmployeeDisciplinarian disciplinarian = list.find { id.equals(it.disciplinarianId) }
            if (disciplinarian) {
                employeeManager.removeDisciplinarian(domainEmployee, disciplinarian)
                list = employeeManager.findDisciplinarians(env.contactView.bean)
            }
        }
    }

    protected EmployeeManager getEmployeeManager() {
        return getService(EmployeeManager.class.getName())
    }
}

