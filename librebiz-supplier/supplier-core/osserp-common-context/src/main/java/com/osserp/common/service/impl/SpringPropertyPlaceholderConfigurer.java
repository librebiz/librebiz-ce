/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

import com.osserp.common.service.Context;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SpringPropertyPlaceholderConfigurer extends PropertyPlaceholderConfigurer {
    private static Logger log = LoggerFactory.getLogger(SpringPropertyPlaceholderConfigurer.class.getName());

    private boolean ignoreCustomProperties = false;
    
    public void setIgnoreCustomProperties(boolean ignoreCustomProperties) {
        this.ignoreCustomProperties = ignoreCustomProperties;
    }

    @Override
    public void setLocations(Resource... locations) {
        String configLocation = Context.getConfigLocation();
        int locationsSize = locations.length;
        List<Resource> resolvedLocations = initResolvedLocations();
        for (int i = 0; i < locationsSize; i++) {
            Resource next = locations[i];

            Resource clpResourceWithLocation = fetchResourceAsClasspathResource(next);
            if (clpResourceWithLocation != null) {
                resolvedLocations.add(clpResourceWithLocation);
            }
            Resource fsResourceWithLocation = fetchResourceAsExternalFile(configLocation, next);
            if (fsResourceWithLocation != null) {
                resolvedLocations.add(fsResourceWithLocation);
            }
        }
        if (!ignoreCustomProperties) {
            Resource customOverrides = fetchResourceAsExternalFile(
                    configLocation, Context.CUSTOM_PROPERTIES);
            if (customOverrides != null) {
                resolvedLocations.add(customOverrides);
            }
        } else if (log.isDebugEnabled()) {
            log.debug("Ignoring disabled custom.properties lookup");
        }
        Resource distributionOverrides = fetchResourceAsClasspathResource(
                Context.DIST_PROPERTIES);
        if (distributionOverrides != null) {
            resolvedLocations.add(distributionOverrides);
        }
        locationsSize = resolvedLocations.size();
        Resource[] finalLocations = new Resource[locationsSize];
        for (int i = 0; i < locationsSize; i++) {
            finalLocations[i] = resolvedLocations.get(i);
        }
        super.setLocations(finalLocations);
    }

    private Resource fetchResourceAsExternalFile(String configLocation, Resource raw) {
        Resource resourceWithLocation = null;
        if (raw != null) {
            try {
                resourceWithLocation = fetchResourceAsExternalFile(configLocation, raw.getFile().getPath());
            } catch (Throwable t) {
                log.warn("fetchResourceAsExternalFile() failed [configLocation=" 
                        + configLocation + ", message=" + t.getMessage() + "]");
            }
        }
        return resourceWithLocation;
    }

    private Resource fetchResourceAsExternalFile(String configLocation, String propertyFilename) {
        Resource resourceWithLocation = null;
        if (configLocation != null && propertyFilename != null) {
            try {
                File file = new File(configLocation + "/" + propertyFilename);
                if (file.exists() && file.canRead()) {
                    resourceWithLocation = new FileSystemResource(file);
                } else if (file.exists()) {
                    log.warn("fetchResourceAsExternalFile() not readable config file found [file=" 
                            + configLocation + "/" + propertyFilename + "]");
                }
            } catch (Throwable t) {
                log.warn("fetchResourceAsExternalFile() failed [configLocation=" + configLocation
                        + ", name=" + propertyFilename + ", message=" + t.getMessage() + "]");
            }
        }
        return resourceWithLocation;
    }

    private Resource fetchResourceAsClasspathResource(Resource raw) {
        Resource resourceWithLocation = null;
        String propertyFilename = getResourceName(raw);
        if (propertyFilename != null) {
            resourceWithLocation = getClasspathResource("META-INF/" + propertyFilename);
            if (resourceWithLocation == null) {
                resourceWithLocation = getClasspathResource(propertyFilename);
            }
        }
        return resourceWithLocation;
    }

    private Resource fetchResourceAsClasspathResource(String propertyFilename) {
        Resource resourceWithLocation = null;
        if (propertyFilename != null) {
            resourceWithLocation = getClasspathResource("META-INF/" + propertyFilename);
            if (resourceWithLocation == null) {
                resourceWithLocation = getClasspathResource(propertyFilename);
            }
        }
        return resourceWithLocation;
    }

    private Resource getClasspathResource(String fullPath) {
        try {
            Resource result = new ClassPathResource(fullPath);
            if (result.exists()) {
                return result;
            } 
        } catch (Exception e) {
            log.warn("getClasspathResource() failed [resource=" + fullPath + ", message=" + e.getMessage() + "]");
        }
        return null;
    }

    private String getResourceName(Resource raw) {
        try {
            String propertyFilename = raw.getFile().getPath();
            return propertyFilename;
        } catch (Throwable t) {
            log.warn("getResourceName() failed [message=" + t.getMessage() + "]");
            return null;
        }
    }

    private List<Resource> initResolvedLocations() {
        List<Resource> resolvedLocations = new ArrayList<>();
        Resource defaults = getClasspathResource(
                "META-INF/" + Context.OSSERP_PROPERTIES);
        if (defaults != null) {
            resolvedLocations.add(defaults);
        }
        return resolvedLocations;
    }
}
