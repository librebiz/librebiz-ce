package com.osserp.core.mail;

import com.osserp.common.mail.SimpleMail;

public interface MailMessage extends SimpleMail {

    public static final Integer STATUS_NEW = 0;
    public static final Integer STATUS_RESTORED = 1;
    public static final Integer STATUS_ASSIGNED = 5;
    public static final Integer STATUS_IMPORTED = 9;
    public static final Integer STATUS_REMOVED = -1;
    public static final Integer STATUS_RESET = -2;
    public static final Integer STATUS_UNAVAILABLE = -3;

}
