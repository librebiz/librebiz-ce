/**
 *
 * Copyright (C) 2007, 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 21, 2007 8:16:32 PM 
 * 
 */
package com.osserp.gui.events

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.util.DateFormatter
import com.osserp.common.util.DateUtil
import com.osserp.common.util.NumberUtil

import com.osserp.core.employees.Employee
import com.osserp.core.events.Event
import com.osserp.core.events.EventManager

import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class AbstractEventView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(AbstractEventView.class.getName())

    String sortBy
    boolean sortDescending = true
    List<Employee> recipients = []
    List<Long> recipientIds = []

    protected void refreshEvents() {
        // override if required
    }

    protected void reloadDependencies() {
        // override if required
    }
    
    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest && isSystemPropertyEnabled('eventDistributionEnabled')) {
            recipients = activatedEmployeeList
            if (isSystemPropertyEnabled('appointmentDefaults')) {
                form.setValue('reminderDate', DateFormatter.currentDate)
                form.setValue('reminderHours', '0')
                form.setValue('reminderMinutes', '00')
                form.setValue('appointmentHours', NumberUtil.createInteger(
                    getSystemProperty('appointmentDefaultsHours'))?.toString()  ?: '9')
                form.setValue('appointmentMinutes', NumberUtil.createInteger(
                    getSystemProperty('appointmentDefaultsMinutes'))  ?: '00')
            }
        }
    }
    
    @Override
    void reload() {
        refreshEvents()
    }

    @Override
    void enableEditMode() {
        select()
        super.enableEditMode()
    }

    @Override
    void disableEditMode() {
        super.disableEditMode()
        bean = null
        createCustomNavigation()
    }

    @Override
    void save() {
        if (bean) {
            String note = form.getString('note')
            boolean distribute = form.getBoolean('distribute')
            Event event = bean
            eventManager.addNote(
                    event,
                    domainEmployee.id,
                    note,
                    createDate("reminder"),
                    createDate("appointment"),
                    distribute,
                    recipientIds)
            disableEditMode()
            reloadDependencies()
            refreshEvents()
            if (list) {
                for (int i = 0; i < list.size(); i++) {
                    Event next = list.get(i)
                    if (next.id == event.id) {
                        Event updated = eventManager.findEvent(next.id)
                        if (updated) {
                            list.set(i, updated)
                        }
                    }
                }
                bean = null
            }
        }
    }
    
    void addRecipient() {
        Long id = form.getLong('recipient')
        if (id) {
            if (recipientAlreadyAdded(id)) {
                removeRecipientId(id)
            } else {
                recipientIds << id
            }
            form.resetValue('recipient')
        }
    }
    
    void removeRecipient() {
        Long id = form.getLong('id')
        if (id) {
            removeRecipientId(id)
            form.resetValue('id')
        }
    }
    
    protected boolean recipientAlreadyAdded(Long id) {
        (recipientIds.find { it == id })
    }
    
    protected void removeRecipientId(Long id) {
        if (id) {
            for (Iterator i = recipientIds.iterator(); i.hasNext();) {
                Long rcpt = i.next()
                if (rcpt == id) {
                    i.remove()
                }
            }
        }
    }

    @Override
    void sort() {
        String sortByTmp = form.getString('by')
        if (sortByTmp == sortBy) {
            sortDescending = !sortDescending
        } else {
            sortDescending = true
        }
        sortBy = sortByTmp
        refreshEvents()
    }
    
    boolean isCloseMode() {
        env.closeMode
    }

    void disableCloseMode() {
        env.closeMode = false
        createCustomNavigation()
    }

    void enableCloseMode() {
        env.closeMode = true
        createCustomNavigation()
    }
    
    void closeEvents() {
        Long[] ids = form.getIndexedLong('id')
        if (ids) {
            def closeList = list.findAll { it.id in ids }
            closeList.each {
                eventManager.close(domainUser.employee.id, it)
                logger.debug("closeEvents: closing next [event=${it.id}]")
            }
            refreshEvents()
            closeList.each { Event closed ->
                
                for (Iterator i = list.iterator(); i.hasNext();) {
                    Event next = i.next()
                    if (next.id == closed.id) {
                        i.remove()
                        logger.debug("closeEvents: remove next [event=${next.id}]")
                        break
                    }
                }
            }
        }
        disableCloseMode()
    }

    protected Date createDate(String varName) throws ClientException {
        Date date = form.getDate("${varName}Date")
        if (date) {
            date = DateUtil.addHours(date, getHours(varName))
            date = DateUtil.addMinutes(date, getMinutes(varName))
        }
        return date
    }

    private int getHours(String varName) throws ClientException {
        try {
            Integer result = form.getHours("${varName}Hours")
            if (!result) {
                result = NumberUtil.createInteger(getSystemProperty('appointmentDefaultsHours'))  ?: 9
            }
            if (result > 23) {
                throw new ClientException(ErrorCode.HOURS_INVALID)
            }
            return result
        } catch (NullPointerException npe) {
            logger.warn("getHours: ignoring illegal exception [var=${varName}, message=${npe.message}]")
        } catch (NumberFormatException nfe) {
            logger.debug("getHours: caught exception [var=${varName}, message=${nfe.message}]")
            throw new ClientException(ErrorCode.DATE_FORMAT)
        }
        return 0
    }

    private int getMinutes(String varName) throws ClientException {
        try {
            Integer result = form.getMinutes("${varName}Minutes")
            if (!result) {
                result = NumberUtil.createInteger(getSystemProperty('appointmentDefaultsMinutes'))  ?: 0
            }
            if (result > 59) {
                throw new ClientException(ErrorCode.MINUTES_INVALID)
            }
            return result
        } catch (NullPointerException npe) {
            logger.warn("getMinutes: ignoring illegal exception [var=${varName}, message=${npe.message}]")
        } catch (NumberFormatException nfe) {
            logger.debug("getMinutes: caught exception [var=${varName}, message=${nfe.message}]")
            throw new ClientException(ErrorCode.DATE_FORMAT)
        }
        return 0
    }
    
    protected EventManager getEventManager() {
        getService(EventManager.class.getName())
    }
}
