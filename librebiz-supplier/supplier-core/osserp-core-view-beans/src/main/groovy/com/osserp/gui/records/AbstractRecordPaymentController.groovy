/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 26, 2016 
 * 
 */
package com.osserp.gui.records

import javax.servlet.http.HttpServletRequest

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.common.ClientException
import com.osserp.groovy.web.ViewController

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class AbstractRecordPaymentController extends ViewController {
    private static Logger logger = LoggerFactory.getLogger(AbstractRecordPaymentController.class.getName())
    
    @RequestMapping
    def selectPaymentType(HttpServletRequest request) {
        logger.debug("selectPaymentType: invoked [user=${getUserId(request)}]")
        AbstractRecordPaymentView view = getView(request)
        view.selectPaymentType()
        defaultPage
    }

    @RequestMapping
    def createCancellation(HttpServletRequest request) {
        logger.debug("createCancellation: invoked [user=${getUserId(request)}]")
        AbstractRecordPaymentView view = getView(request)
        String targetPage = defaultPage
        try {
            targetPage = redirect(request, view.createCancellation())
        } catch (ClientException c) {
            logger.error("createCancellation failed: ${c.message}")
            saveError(request, c.message)
        } catch (Exception e) {
            logger.error("createCancellation failed: ${e.message}", e)
            saveError(request, e.message)
        }
        targetPage
    }

    @RequestMapping
    def createCreditNote(HttpServletRequest request) {
        logger.debug("createCreditNote: invoked [user=${getUserId(request)}]")
        AbstractRecordPaymentView view = getView(request)
        String targetPage = defaultPage
        try {
            targetPage = redirect(request, view.createCreditNote())
        } catch (Exception e) {
            saveError(request, e.message)
        }
        targetPage
    }

    @Override
    @RequestMapping
    def save(HttpServletRequest request) {
        logger.debug("save: invoked [user=${getUserId(request)}]")
        AbstractRecordPaymentView view = getView(request)
        executeSave(request)
        if (!errorAvailable(request)) {
            return super.executeExit(request)
        }
        defaultPage
    }

    @Override
    @RequestMapping
    def exit(HttpServletRequest request) {
        logger.debug("exit: invoked [user=${getUserId(request)}]")
        AbstractRecordPaymentView view = getView(request)
        String target = view.defaultExitTarget
        removeView(request, view)
        return target
    }

}
