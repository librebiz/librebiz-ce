/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 15, 2005 
 * 
 */
package com.osserp.core.dao;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.core.calc.CalculationConfig;
import com.osserp.core.calc.CalculationConfigGroup;
import com.osserp.core.calc.CalculationType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface CalculationConfigs {

    /**
     * Loads a config from persistent storage
     * @param id
     * @return calculationConfig
     */
    CalculationConfig load(Long id);

    /**
     * Provides all calculation configs
     * @return configs
     */
    List<CalculationConfig> findAll();

    /**
     * Provides all calculation configs by type
     * @param type
     * @return configs
     */
    List<CalculationConfig> findByType(Long type);

    /**
     * Creates a new calculation config
     * @param type
     * @param name
     * @throws ClientException if config already exists
     */
    CalculationConfig createConfig(CalculationType type, String name) throws ClientException;

    /**
     * Persists a calculation config
     * @param config
     */
    void save(CalculationConfig config);

    /**
     * Provides a group by primary key
     * @param id
     * @return group or null if not exists
     */
    CalculationConfigGroup findGroup(Long id);

    /**
     * Adds a new group to calculation config
     * @param config
     * @param name
     * @param discounts
     * @param option
     * @return new created group
     * @throws ClientException if name already exists
     */
    CalculationConfigGroup addGroup(CalculationConfig config, String name, boolean discounts, boolean option) throws ClientException;

    /**
     * Adds an product selection
     * @param config
     * @param groupId
     * @param configId
     * @return updated config
     */
    CalculationConfig addProductSelection(CalculationConfig config, Long groupId, Long configId);

    /**
     * Removes an product selection
     * @param config
     * @param groupId
     * @param configId
     * @return updated config
     */
    CalculationConfig removeProductSelection(CalculationConfig config, Long groupId, Long configId);

    /**
     * Adds or removes a part list config to a group
     * @param config
     * @param groupId
     * @param partListConfig
     * @return updated config
     */
    CalculationConfig configurePartList(CalculationConfig config, Long groupId, CalculationConfig partListConfig);

    /**
     * Creates a new calculation type
     * @param name
     * @param description
     * @return new created calculationType
     */
    CalculationType createType(String name, String description) throws ClientException;

    /**
     * Updates an existing type
     * @param type
     * @param name
     * @param description
     * @throws ClientException if name already exists on another config
     */
    CalculationType updateType(CalculationType type, String name, String description) throws ClientException;

    /**
     * Loads a known type from persistent storage
     * @param id
     * @return calculationType
     */
    CalculationType loadType(Long id);

    /**
     * Provides all calculation config types
     * @return types
     */
    List<CalculationType> getTypes();

    /**
     * Updates the order of an existing group
     * @param groupid
     * @param configid
     * @param oldorder
     * @param neworder
     */
    void updateGroupOrder(
            long groupid,
            long configid,
            int oldorder,
            int neworder);
}
