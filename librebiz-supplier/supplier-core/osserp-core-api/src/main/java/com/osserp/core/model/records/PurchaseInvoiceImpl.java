/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 30-Jan-2007 10:36:28 
 * 
 */
package com.osserp.core.model.records;

import java.math.BigDecimal;
import java.util.Date;

import com.osserp.core.Item;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.RecordType;
import com.osserp.core.products.Product;
import com.osserp.core.purchasing.PurchaseInvoice;
import com.osserp.core.purchasing.PurchaseInvoiceType;
import com.osserp.core.purchasing.PurchaseOrder;
import com.osserp.core.suppliers.Supplier;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PurchaseInvoiceImpl extends AbstractPurchaseInvoice implements PurchaseInvoice {

    /**
     * Serializable constructor
     */
    protected PurchaseInvoiceImpl() {
        super();
    }

    /**
     * Creates a new purchase invoice by order
     * @param id
     * @param type
     * @param bookingType
     * @param order
     * @param createdBy
     */
    public PurchaseInvoiceImpl(
            Long id,
            RecordType type,
            PurchaseInvoiceType bookingType,
            PurchaseOrder order,
            Employee createdBy) {
        super(id, type, bookingType, order, createdBy);
    }

    /**
     * Default constructor for order independent purchase invoices
     * @param id
     * @param type
     * @param bookingType
     * @param company
     * @param branchId
     * @param reference
     * @param supplier
     * @param createdBy
     * @param sale
     * @param taxRate
     * @param reducedTaxRate
     */
    public PurchaseInvoiceImpl(
            Long id,
            RecordType type,
            PurchaseInvoiceType bookingType,
            Long company,
            Long branchId,
            Supplier supplier,
            Employee createdBy,
            Long sale,
            Double taxRate,
            Double reducedTaxRate) {
        super(
                id,
                type,
                bookingType,
                company,
                branchId,
                null,
                supplier,
                createdBy,
                sale,
                taxRate,
                reducedTaxRate,
                true,
                true);
        if (supplier.getPaymentAgreement() != null) {
            setTaxFree(supplier.getPaymentAgreement().isTaxFree());
            if (isTaxFree()) {
                setTaxFreeId(supplier.getPaymentAgreement().getTaxFreeId());
                calculateSummary();
            }
        }
    }

    /**
     * Constructor for invoices with individual input params
     * @param id
     * @param type
     * @param bookingType
     * @param company
     * @param branchId
     * @param reference
     * @param supplier
     * @param createdBy
     * @param taxRate
     * @param reducedTaxRate
     * @param recordDate
     * @param invoiceDate
     * @param invoiceNumber
     * @param productStockId
     * @param product
     * @param customName
     * @param productNote
     * @param productQuantity
     * @param productPrice
     */
    public PurchaseInvoiceImpl(
            Long id,
            RecordType type,
            PurchaseInvoiceType bookingType,
            Long company,
            Long branchId,
            Supplier supplier,
            Employee createdBy,
            Double taxRate,
            Double reducedTaxRate,
            Date recordDate,
            Date invoiceDate,
            String invoiceNumber,
            Long productStockId,
            Product product,
            String customName,
            String productNote,
            Double productQuantity,
            BigDecimal productPrice) {
        super(
                id,
                type,
                bookingType,
                company,
                branchId,
                null,
                supplier,
                createdBy,
                null,
                taxRate,
                reducedTaxRate,
                true,
                true);

        if (recordDate != null) {
            updateCreatedDate(recordDate);
        }

        setSupplierReferenceDate(invoiceDate);
        setSupplierReferenceNumber(invoiceNumber);
        if (invoiceDate != null) {
            setTaxPoint(invoiceDate);
        } else {
            setTaxPoint(getCreated());
        }

        if (product != null
                && isSet(productStockId)
                && isSet(productQuantity)
                && isSet(productPrice)) {

            addItem(
                productStockId,
                product,
                customName,
                productQuantity,
                product.isReducedTax() ? reducedTaxRate : taxRate,
                productPrice, 
                null,  //priceDate 
                null,  //partnerPrice
                false, //partnerPriceEditable
                false, //partnerPriceOverridden 
                null,  //purchasePrice 
                productNote, 
                true,  // includePrice
                null); // externalId
        }
    }
    
    /**
     * Creates a new invoice by existing invoice
     * @param id
     * @param existing
     * @param createdBy
     * @param recordDate
     * @param invoiceDate
     * @param invoiceNumber
     */
    public PurchaseInvoiceImpl(
            Long id,
            PurchaseInvoice existing,
            Employee createdBy,
            Date recordDate,
            Date invoiceDate,
            String invoiceNumber) {
        super(id, existing, createdBy, recordDate, invoiceDate, invoiceNumber);
    }

    /**
     * Constructor for purchase invoice creditNotes
     * @param id
     * @param bookingType
     * @param invoice
     * @param createdBy
     */
    public PurchaseInvoiceImpl(
            Long id,
            PurchaseInvoiceType bookingType,
            PurchaseInvoice invoice,
            Employee createdBy) {
        super(
                id,
                bookingType,
                invoice,
                createdBy);
    }

    public void reopen(Employee user) {
        setStatus(PurchaseInvoice.STAT_CHANGED);
    }

    @Override
    protected Item createItem(
            Long stockId,
            Product product,
            String customName,
            Double quantity,
            Double taxRate,
            BigDecimal price,
            Date priceDate,
            BigDecimal partnerPrice,
            boolean partnerPriceEditable,
            boolean partnerPriceOverridden,
            BigDecimal purchasePrice,
            String note,
            boolean includePrice,
            Long externalId) {

        return new PurchaseInvoiceItemImpl(
                this,
                stockId,
                product,
                customName,
                quantity,
                price,
                priceDate,
                partnerPrice,
                partnerPriceEditable,
                partnerPriceOverridden,
                purchasePrice,
                taxRate,
                note,
                includePrice,
                getItems().size(),
                externalId);
    }
}
