<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<o:userContext/>
<o:resource/>
<v:view viewName="salesDeliveryCalendarView"/>
<c:set var="cal" scope="request" value="${view.calendar}"/>
<c:set var="calUrl" scope="request"><v:url value="${view.baseLink}/setDate?context=${view.name}"/></c:set>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
<tiles:put name="title"><o:displayTitle/></tiles:put>
<tiles:put name="styles" type="string">
<style type="text/css">
<c:import url="/css/ajax_popup.css"/>
<c:import url="/css/calendar.css"/>
</style>
</tiles:put>
<tiles:put name="javascript" type="string"> 
<c:import url="${viewdir}/shared/calendar_js.jsp"/>
</tiles:put>
<tiles:put name="headline">
    <fmt:message key="${view.headerName}"/> <oc:options name="months" value="${requestScope.cal.month}"/> <o:out value="${requestScope.cal.year}"/>
</tiles:put>
<tiles:put name="headline_right">
<ul>
    <c:import url="${viewdir}/shared/calendar_navigation.jsp"/>
    <li><v:link url="${view.baseLink}/exit" title="backToLast"><o:img name="backIcon"/></v:link></li>
    <li><v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link></li>
</ul>
</tiles:put>
<tiles:put name="content" type="string">
<div class="content-area" id="salesDeliveryCalendarContent">
    <div class="row">
        <div class="col-lg-12 panel-area">
            <c:import url="${viewdir}/shared/calendar_infotext.jsp"/>
            <table class="table table-striped">
                <o:calendarRows calendarView="${view}" />
            </table>
        </div>
    </div>
</div>
</tiles:put> 
</tiles:insert>
