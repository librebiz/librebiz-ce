/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 26-May-2005 19:18:12 
 * 
 */
package com.osserp.common.util;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.FileNameMap;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.nio.file.FileSystem;
import java.nio.file.FileSystemAlreadyExistsException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.io.IOUtils;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.FileException;

/**
 * 
 * Class provides some static convenience methods for file handling
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class FileUtil {
    private static Logger log = LoggerFactory.getLogger(FileUtil.class.getName());

    private static final int DEFAULT_FILE_LENGTH = 48; // 2MB

    /**
     * Generates a random 48-character-long file name with suffix from source file name
     * @param sourceFileName
     * @return random file name
     */
    public static String generateRandomFileName(String sourceFileName) {
        return generateRandomFileName(sourceFileName, DEFAULT_FILE_LENGTH);
    }

    /**
     * Generates a random 48-character-long file name with suffix from source file name
     * @param sourceFileName
     * @param length of the random filename
     * @return random file name
     */
    public static String generateRandomFileName(String sourceFileName, int length) {
        if (log.isDebugEnabled()) {
            log.debug("generateRandomFileName() invoked [sourceFileName="
                    + sourceFileName + ", length=" + length + "]");
        }
        StringBuilder buffer = new StringBuilder(SecurityUtil.createString(length, false));
        buffer.append('.').append(getFileType(sourceFileName));
        return buffer.toString();
    }

    /**
     * Generates a random 48-character-long file name in the given upload directory and with suffix from source file name
     * @param uploadDir
     * @param sourceFileName
     * @return random file name
     */
    public static String generateRandomFileName(String uploadDir, String sourceFileName) {
        return generateRandomFileName(uploadDir, sourceFileName, DEFAULT_FILE_LENGTH);
    }

    /**
     * Generates a random file name in the given upload directory and with suffix from source file name
     * @param uploadDir
     * @param sourceFileName
     * @param length of the random filename
     * @return random file name
     */
    public static String generateRandomFileName(String uploadDir, String sourceFileName, int length) {
        if (log.isDebugEnabled()) {
            log.debug("createFileName() invoked [uploadDir=" + uploadDir
                    + ", sourceFileName=" + sourceFileName + ", length=" + length + "]");
        }
        StringBuilder buffer = new StringBuilder(uploadDir);
        buffer
                .append("/")
                .append(SecurityUtil.createString(length, false))
                .append('.')
                .append(getFileType(sourceFileName));
        return buffer.toString();
    }

    public static String createFileName(String root, String file) {
        StringBuilder result = new StringBuilder(root);
        if (!root.endsWith("/")) {
            if (file.startsWith("/")) {
                result.append(file);
                // '/root' + '/file'
                return result.toString();
            }
            // '/root' + '/' + 'file'
            result.append("/").append(file);
            return result.toString();
        }
        if (file.startsWith("/")) {
            result.append(file.substring(1, file.length()));
            // '/root/' + ('/file' -> 'file')
            return result.toString();
        }
        // '/root/' + 'file'
        result.append(file);
        return result.toString();
    }

    /**
     * Provides suffix from the given file name
     * @param fileName
     * @return
     */
    public static boolean providesPath(String fileName) {
        if (fileName == null) {
            return false;
        }
        if (fileName.indexOf("/") == -1) {
            return false;
        }
        return true;
    }

    /**
     * Removes path from provided filename.
     * @param filename
     * @return filename without path
     */
    public static String stripPath(String filename) {
        if (!providesPath(filename)) {
            return filename;
        }
        return filename.substring(
                filename.lastIndexOf("/") + 1,
                filename.length()).toLowerCase();
    }

    /**
     * Provides suffix from the given file name
     * @param filename
     * @return
     */
    public static boolean providesType(String filename) {
        if (filename == null) {
            return false;
        }
        if (filename.indexOf(".") == -1) {
            return false;
        }
        return true;
    }

    /**
     * Provides suffix from the given file name
     * @param filename
     * @return
     */
    public static String getFileType(String filename) {
        if (!providesType(filename)) {
            return "";
        }
        return filename.substring(filename.lastIndexOf(".") + 1, filename.length()).toLowerCase();
    }

    /**
     * Indicates whether a file exists
     * @param filename
     * @return true if file exists
     */
    public static boolean exists(String filename) {
        if (filename != null) {
            File file = new File(filename);
            return file.exists();
        }
        return false;
    }

    /**
     * Provides the file size.
     * @param filename
     * @return size if file exists and is readable by user
     */
    public static long size(String filename) {
        if (filename != null) {
            File file = new File(filename);
            if (file.exists() && file.canRead()) {
                return file.length();
            }
        }
        return 0;
    }

    /**
     * Creates a directory named by path if not exists.
     * @param path
     * @return true if new dir was created.
     */
    public static boolean createDir(String path) {
        if (path != null) {
            File file = new File(path);
            if (!file.exists()) {
                try {
                    return file.mkdir();
                } catch (Exception e) {
                    log.warn("createDir() failed on mkdir invocation [message="
                            + e.getMessage() + ", path=" + path + "]", e);
                }
            }
        }
        return false;
    }

    /**
     * Reads filename and creates an file object.
     * @param filename
     * @return file
     * @throws ClientException if filename is empty, file does not exist or file is not readable
     */
    public static File getFile(String filename) throws ClientException {
        if (filename == null || filename.length() < 1) {
            throw new ClientException(ErrorCode.VALUES_MISSING);
        }
        File file = new File(filename);
        if (!file.exists()) {
            throw new ClientException(ErrorCode.FILE_MISSING);
        }
        if (!file.canRead()) {
            throw new ClientException(ErrorCode.FILE_ACCESS);
        }
        return file;
    }

    /**
     * Creates an input stream by filename
     * @param fileName
     * @return input stream
     * @throws ClientException if file does not exist
     */
    public static InputStream getStream(String fileName) throws ClientException {
        try {
            File file = new File(fileName);
            FileInputStream fis = new FileInputStream(file);
            return fis;
        } catch (FileNotFoundException e) {
            throw new ClientException(ErrorCode.FILE_MISSING, e);
        }
    }

    /**
     * Creates an input stream by binary object
     * @param object
     * @return input stream
     */
    public static InputStream getStream(byte[] object) {
        return new ByteArrayInputStream(object);
    }

    /**
     * Attempt to read the given file and get it's data in byte array form. Tries to read the entire file
     * @param file to read
     * @throws ClientException if file not exists, io or access problem occured
     */
    public static byte[] getBytes(File file) throws ClientException {

        byte[] bytes = new byte[(int) file.length()];
        try {
            FileInputStream fis = new FileInputStream(file);
            fis.read(bytes);
            fis.close();
            return bytes;
        } catch (FileNotFoundException e) {
            throw new ClientException(ErrorCode.FILE_MISSING, e);
        } catch (IOException e) {
            throw new ClientException(ErrorCode.FILE_ACCESS, e);
        }
    }

    /**
     * Attempt to read the given file and get it's data in byte array form. Tries to read buffered
     * @param file to read
     * @param bufferSize
     * @throws ClientException if file not exists, io or access problem occured
     */
    public static byte[] getBytes(File file, int bufferSize) throws ClientException {

        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        try {
            FileInputStream fis = new FileInputStream(file);
            int readLength = 0;
            int offset = 0;
            byte[] bytes = new byte[bufferSize];

            while ((readLength = fis.read(bytes, offset, bufferSize)) != -1) {
                byteStream.write(bytes, offset, bufferSize);
                offset += readLength;
            }
            bytes = byteStream.toByteArray();
            fis.close();
            byteStream.close();
            return bytes;
        } catch (FileNotFoundException e) {
            throw new ClientException(ErrorCode.FILE_MISSING, e);
        } catch (IOException e) {
            throw new ClientException(ErrorCode.FILE_ACCESS, e);
        }
    }

    /**
     * Attempt to read the given file and get it's data in byte array form. Tries to read buffered
     * @param fis file input stream to read
     * @param bufferSize
     * @throws FileException if io or access problem occured
     */
    public static byte[] getBytes(InputStream fis, int bufferSize) throws FileException {

        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        byte[] bytes = new byte[bufferSize];
        long start = System.currentTimeMillis();
        try {
            int readBytes;
            while ((readBytes = fis.read(bytes)) > 0) {
                byteStream.write(bytes, 0, readBytes);
            }
            bytes = byteStream.toByteArray();
            fis.close();
            byteStream.close();
            return bytes;
        } catch (Exception exception) {
            throw new FileException(
                    ErrorCode.FILE_ACCESS,
                    exception,
                    System.currentTimeMillis() - start,
                    (bytes == null ? 0 : bytes.length));
        }
    }

    /**
     * Persists an input stream to a file
     * @param stream
     * @param fileName where to persist the stream
     * @return size of uploaded file
     * @throws ClientException if file could not be written
     */
    public static long persist(InputStream stream, String fileName) throws ClientException {
        try {
            //retrieve the file data
            //write the file to the file specified
            OutputStream bos = new FileOutputStream(fileName);
            long fileSize = 0;
            int bytesRead = 0;
            byte[] buffer = new byte[8192];
            while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
                bos.write(buffer, 0, bytesRead);
                fileSize = fileSize + bytesRead;
            }
            bos.close();
            stream.close();
            if (log.isDebugEnabled()) {
                log.debug("persist() wrote file [name=" + fileName + "]");
            }
            return fileSize;
        } catch (FileNotFoundException fnfe) {
            log.error("persist() failed: " + fnfe.toString(), fnfe);
            throw new ClientException(ErrorCode.FILE_MISSING, fnfe);
        } catch (IOException ioe) {
            log.error("persist() failed: " + ioe.toString(), ioe);
            throw new ClientException(ErrorCode.FILE_ACCESS, ioe);
        }
    }

    /**
     * Persists a byte stream to a file
     * @param stream
     * @param fileName to persist the stream to
     * @throws ClientException if file could not be written
     */
    public static void persist(byte[] stream, String fileName) throws ClientException {
        try {
            //retrieve the file data
            //write the file to the file specified
            OutputStream bos = new FileOutputStream(fileName);
            bos.write(stream);
            bos.close();
            if (log.isDebugEnabled()) {
                log.debug("persist() wrote file [name=" + fileName + "]");
            }
        } catch (FileNotFoundException fnfe) {
            log.error("persist() failed: " + fnfe.toString(), fnfe);
            throw new ClientException(ErrorCode.FILE_MISSING, fnfe);
        } catch (IOException ioe) {
            log.error("persist() failed: " + ioe.toString(), ioe);
            throw new ClientException(ErrorCode.FILE_ACCESS, ioe);
        }
    }

    /**
     * Deletes a file by name
     * @param fileName
     * @throws ClientException if file could not be deleted
     */
    public static void delete(String fileName) throws ClientException {
        try {
            File file = new File(fileName);
            if (file.exists() && file.canWrite()) {
                file.delete();
            }
            if (log.isDebugEnabled()) {
                log.debug("delete() done [name=" + fileName + "]");
            }
        } catch (Exception e) {
            log.error("delete() failed [message=" + e.getMessage() + "]");
            throw new ClientException(ErrorCode.FILE_ACCESS, e);
        }
    }

    /**
     * Deletes file or directory
     * @param file
     * @throws ClientException
     */
    public static void delete(File file) throws ClientException {
        try {
            if (file.isDirectory()) {
                if (file.list().length == 0) {
                    file.delete();
                } else {
                    String files[] = file.list();
                    for (String temp : files) {
                        File fileDelete = new File(file, temp);
                        delete(fileDelete);
                    }
                    if (file.list().length == 0) {
                        file.delete();
                    }
                }
            } else {
                file.delete();
            }
        } catch (Exception e) {
            log.error("delete() failed [message=" + e.getMessage() + "]");
            throw new ClientException(ErrorCode.FILE_ACCESS, e);
        }
    }

    /**
     * Determines MIME type according to file name
     * @param fileName
     * @return mime type
     */
    public static String determineContentType(String fileName) {
        FileNameMap fileNameMap = URLConnection.getFileNameMap();
        String result = fileNameMap.getContentTypeFor(fileName);
        return (result != null) ? result : "application/x-file-download";
    }

    /**
     * Reads all lines of a resource path file.
     * @param fileName
     * @return list of lines or empty list if failed or empty
     */
    public static List<String> readResourceFile(String fileName) {
        List<String> result = new ArrayList<>();
        try {
            final Path path = createPath(fileName); //fs.getPath(array[1]);            
            result = Files.readAllLines(path, Charset.defaultCharset());
        } catch (Exception e) {
            try {
                result = readResourceFileClassic(fileName);
            } catch (Exception e2) {
                log.warn("readResourceFile() failed [fileName=" + fileName + ", message=" + e2.getMessage() + "]");
            }
        }
        return result;
    }

    /**
     * Reads all lines of a resource path file.
     * @param fileName
     * @return list of lines or empty list if failed or empty
     */
    public static String readResourceFileAsString(String fileName) {
        List<String> strings = readResourceFile(fileName);
        StringBuilder buffer = new StringBuilder(256);
        for (int i = 0, j = strings.size(); i < j; i++) {
            buffer.append(strings.get(i));
        }
        return buffer.toString();
    }

    private static Path createPath(String fileName) throws IOException, URISyntaxException {
        URI uri = FileUtil.class.getResource(fileName).toURI();
        final Map<String, String> env = new HashMap<>();
        Path path = null;
        String[] array = null;
        if (uri.toString().indexOf("!") > -1) {
            array = uri.toString().split("!");
            if (array != null) {

                try {
                    final FileSystem fs = FileSystems.newFileSystem(URI.create(array[0]), env);
                    path = fs.getPath(array[1]);
                    try {
                        fs.close();
                    } catch (Exception e) {
                        log.error("createPath() failed closing fs [message=" + e.getMessage() + "]"); //, e);
                    }
                    return path;
                } catch (FileSystemAlreadyExistsException e) {
                    log.error("createPath() failed creating fs [message=" + e.getMessage() + "]");//, e);
                    try {
                        final FileSystem fs = FileSystems.getFileSystem(URI.create(array[0]));
                        path = fs.getPath(array[1]);
                        try {
                            fs.close();
                        } catch (Exception e2) {
                            log.error("createPath() failed closing fs [message=" + e2.getMessage() + "]"); //, e);
                        }
                        return path;
                    } catch (Exception unknown) {
                        log.error("createPath() failed getting fs [message=" + unknown.getMessage() + "]", e);
                    }
                }
            }
            //return null;
        } else if (log.isDebugEnabled()) {
            log.info("createPath() uri created [uri=" + uri.toString() + "]");
        }
        return Paths.get(uri);
    }

    public static List<String> readResourceFileClassic(String filename) throws Exception {
        List<String> result = new ArrayList<>();
        try {
            InputStream stream = FileUtil.class.getClassLoader().getResourceAsStream(filename);
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
            String line = null;
            while ((line = reader.readLine()) != null) {
                result.add(line);
            }
            reader.close();
        } catch (Exception e) {
            // ignore
        }
        return result;
    }

    public static List<String> readFilesystemFile(String filename) throws Exception {
        List<String> result = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(filename));
            String line = null;
            while ((line = reader.readLine()) != null) {
                result.add(line);
            }
            reader.close();
        } catch (Exception e) {
            log.info("readFilesystemFile() failed [message=" + e.getMessage() + "]");
        }
        return result;
    }

    public static String readFilesystemFileAsString(String filename) throws Exception {
        List<String> strings = readFilesystemFile(filename);
        StringBuffer buffer = new StringBuffer(256);
        for (int i = 0, j = strings.size(); i < j; i++) {
            buffer.append(strings.get(i));
        }
        return buffer.toString();
    }

    public static void writeToFile(String filename, List<String> list, boolean append) throws Exception {
        try {
            File file = new File(filename);
            if ((file.exists() && file.canWrite()) ||
                    (!file.exists() && file.createNewFile())) {
                PrintWriter writer = new PrintWriter(new FileWriter(file, append));
                for (int i = 0, j = list.size(); i < j; i++) {
                    writer.println(list.get(i));
                }
                writer.close();
                if (log.isDebugEnabled()) {
                    log.debug("writeToFile: done [name=" + filename + "]");
                }
            }
        } catch (Exception iox) {
            log.error("writeToFile: failed: " + iox.toString(), iox);
            throw new ClientException(ErrorCode.FILE_ACCESS, iox);
        }
    }

    /**
     * Zips the source dir to archive name
     * @param sourceDir
     * @param archiveName complete path and name of the archive to create, with or without zip extensions
     * @return zipfile path and name
     * @throws ClientException if something failed
     */
    public static String zip(String sourceDir, String archiveName) throws ClientException {
        try {

            String zipname = archiveName;
            String tmp = zipname.toLowerCase();
            if (tmp.indexOf(".zip") < 0) {
                zipname = zipname + ".zip";
            }
            File directory = new File(sourceDir);
            if (directory.isDirectory() && directory.canRead()) {
                FileOutputStream zipout = null;
                BufferedOutputStream buffer = null;
                ZipArchiveOutputStream zipfile = null;
                try {
                    zipout = new FileOutputStream(new File(zipname));
                    buffer = new BufferedOutputStream(zipout);
                    zipfile = new ZipArchiveOutputStream(buffer);
                    addFileToZip(zipfile, sourceDir, "");
                    log.debug("zip() done [archive=" + zipname + "]");
                    return zipname;
                } finally {
                    if (zipfile != null) {
                        zipfile.finish();
                        zipfile.close();
                    }
                    if (buffer != null) {
                        buffer.close();
                    }
                    if (zipout != null) {
                        zipout.close();
                    }
                }

            }
            log.warn("zip() nothing to do [directoryFound=" + directory.isDirectory()
                    + ", canRead=" + directory.canRead() + "]");
            throw new ClientException(ErrorCode.DIRECTORY_ACCESS);

        } catch (ClientException e) {
            throw e;
        } catch (Exception e) {
            log.error("zip() failed [message=" + e.getMessage() + "]", e);
            throw new BackendException();
        }
    }

    private static void addFileToZip(ZipArchiveOutputStream archive, String path, String base) throws IOException {
        File f = new File(path);
        String entryName = base + f.getName();
        System.out.println("addFile() invoked, file '" + f.getName()
                + "', entryName '" + entryName + "'");

        ZipArchiveEntry zipEntry = new ZipArchiveEntry(f, entryName);

        archive.putArchiveEntry(zipEntry);

        if (f.isFile()) {
            FileInputStream inputStream = null;
            try {
                inputStream = new FileInputStream(f);
                IOUtils.copy(inputStream, archive);
                archive.closeArchiveEntry();
            } finally {
                IOUtils.closeQuietly(inputStream);
            }

        } else {
            archive.closeArchiveEntry();
            File[] children = f.listFiles();
            if (children != null) {
                for (File child : children) {
                    addFileToZip(archive, child.getAbsolutePath(), entryName + "/");
                }
            }
        }
    }

    public static boolean createThumbnail(File file, int height, int width) {
        if (file != null && file.exists() && !file.isDirectory()
                && FileUtil.isThumbnailSupported(
                        FileUtil.getFileType(file.getName()))) {
            try {
                Thumbnails.of(file).size(width, height).asFiles(Rename.PREFIX_DOT_THUMBNAIL);
                return true;
            } catch (Exception e) {
                log.warn("createThumbnail: failed [file=" + file.getAbsolutePath() +
                        ", message=" + e.getMessage() + "]");
            }
        }
        return false;
    }

    public static boolean isThumbnailSupported(String suffix) {
        return suffix != null && ("jpg".equalsIgnoreCase(suffix)
                || "jpeg".equalsIgnoreCase(suffix)
                || "png".equalsIgnoreCase(suffix));
    }
}
