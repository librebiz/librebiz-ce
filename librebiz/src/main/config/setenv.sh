# Librebiz ERP and CRM - Tomcat runtime settings

CATALINA_OPTS="-server -Xms1024m -Xmx1024m"

LIBREBIZ_OPTS="-Dlog4j.path=/var/log/librebiz -DosserpConf=/etc/librebiz/conf"
ESAPI_OPTS="-Dorg.owasp.esapi.resources=/etc/librebiz/conf"

JAVA_OPTS="-Djava.awt.headless=true ${LIBREBIZ_OPTS} ${ESAPI_OPTS}"
