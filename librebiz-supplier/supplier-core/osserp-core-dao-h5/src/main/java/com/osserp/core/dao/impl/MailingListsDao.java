/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 26, 2007 9:10:14 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.Iterator;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.dao.Tables;
import com.osserp.common.mail.EmailAddress;

import com.osserp.core.dao.MailingLists;
import com.osserp.core.mail.MailingList;
import com.osserp.core.model.MailingListImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class MailingListsDao extends AbstractDao
        implements MailingLists {
    private String mailingListMembersTable = null;

    protected MailingListsDao(JdbcTemplate jdbcTemplate, Tables tables, SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
        this.mailingListMembersTable = getTable("contactMailingListMembers");
    }

    public boolean exists(Long mailId) {
        StringBuilder sql = new StringBuilder("SELECT count(*) FROM ");
        sql.append(mailingListMembersTable).append(" WHERE mail_id = ").append(mailId);
        return jdbcTemplate.queryForObject(sql.toString(), Integer.class) > 0;
    }

    public List<MailingList> findAll() {
        return getCurrentSession().createQuery(
                "from " + MailingListImpl.class.getName() + " o order by o.name").list();
    }

    public List<MailingList> findActivated() {
        return getCurrentSession().createQuery(
                "from " + MailingListImpl.class.getName() 
                + " o where o.active = true order by o.name").list();
    }

    public MailingList create(Long createdBy, Long owner, String name, String description)
            throws ClientException {
        if (isEmpty(name)) {
            throw new ClientException(ErrorCode.NAME_MISSING);
        }
        List<MailingList> existing = findAll();
        for (int i = 0, j = existing.size(); i < j; i++) {
            MailingList next = existing.get(i);
            if (next.getName().equals(name)) {
                throw new ClientException(ErrorCode.NAME_EXISTS);
            }
        }
        MailingList list = new MailingListImpl(name, description, owner, createdBy);
        save(list);
        return list;
    }

    public void removeAddress(Long emailAddressId) {
        List<MailingList> lists = findAll();
        for (int i = 0, j = lists.size(); i < j; i++) {
            MailingList next = lists.get(i);
            for (Iterator<EmailAddress> it = next.getMembers().iterator(); it.hasNext();) {
                EmailAddress address = it.next();
                if (address.getId().equals(emailAddressId)) {
                    it.remove();
                    save(next);
                }
            }
        }
    }

    public void save(MailingList list) {
        getCurrentSession().saveOrUpdate(MailingListImpl.class.getName(), list);
    }
}
