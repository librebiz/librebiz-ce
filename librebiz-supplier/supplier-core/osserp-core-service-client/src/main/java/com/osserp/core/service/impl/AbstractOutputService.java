/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 26, 2007 11:59:15 AM 
 * 
 */
package com.osserp.core.service.impl;

import java.util.Locale;

import javax.xml.transform.Source;

import org.jdom2.Document;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.common.OptionsCache;
import com.osserp.common.service.ResourceLocator;
import com.osserp.common.service.impl.AbstractResourceAwareService;

import com.osserp.core.BusinessTemplate;
import com.osserp.core.dms.CoreStylesheetService;
import com.osserp.core.service.doc.XmlService;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractOutputService extends AbstractResourceAwareService {

    private OptionsCache optionsCache = null;
    protected CoreStylesheetService stylesheetService = null;
    protected XmlService xmlService = null;

    protected AbstractOutputService(
            ResourceLocator resourceLocator,
            OptionsCache optionsCache,
            CoreStylesheetService stylesheetService,
            XmlService xmlService) {
        super(resourceLocator);
        this.stylesheetService = stylesheetService;
        this.xmlService = xmlService;
        this.optionsCache = optionsCache;
    }

    /**
     * Checks provided params and calls stylesheetService to creates a pdf. 
     * @param xsl
     * @param doc
     * @return created document
     * @throws ClientException if validation of provided documents failed
     */
    protected byte[] createPdf(Source xsl, Document doc) throws ClientException {
        if (xsl == null) {
            throw new ClientException(ErrorCode.STYLESHEET_MISSING);
        }
        return stylesheetService.createPdf(doc, xsl);
    }

    /**
     * Provides a stylesheet source by name and branch
     * @param name
     * @param branchId
     * @param locale
     * @return stylesheet
     */
    protected Source getXsl(String name, Long branchId, Locale locale) {
        return stylesheetService.getStylesheetSource(name, branchId, locale);
    }

    /**
     * Creates a stylesheet by informations provided by a document.
     * @param template
     * @param branchId the branchId is optional if document itself is related to 
     * a dedicated branch or company 
     * @param locale (optional; defaults to com.osserp.common.Constants.DEFAULT_LANGUAGE)
     * @return source
     */
    protected Source getXsl(BusinessTemplate template, Long branchId, Locale locale) {
        return stylesheetService.getStylesheetSource(template, branchId, locale);
    }
    
    /**
     * Fetches an option value by id.
     * @param optionsName name of the collection of options
     * @param optionId id of the option to lookup for
     * @return option value or null if service not available or option not exists
     */
    protected Option getOption(String optionsName, Long optionId) {
        return optionsCache == null ? null : optionsCache.getMapped(optionsName, optionId);
    }
    
    /**
     * Fetches an option value by id.
     * @param optionsName name of the collection of options
     * @param optionId id of the option to lookup for
     * @return option value or null if service not available or option not exists
     */
    protected String getOptionName(String optionsName, Long optionId) {
        return optionsCache == null ? null : optionsCache.getMappedValue(optionsName, optionId);
    }

    /**
     * Provides the options cache. 
     * @return
     */
    protected OptionsCache getOptionsCache() {
        return optionsCache;
    }
}
