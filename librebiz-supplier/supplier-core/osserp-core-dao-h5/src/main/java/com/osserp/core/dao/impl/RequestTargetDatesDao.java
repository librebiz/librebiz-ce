/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 10, 2006 4:39:29 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import com.osserp.common.dao.AbstractTablesAwareSpringDao;
import com.osserp.common.dao.Tables;
import com.osserp.core.TargetDate;
import com.osserp.core.dao.RequestTargetDates;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.model.TargetDateImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RequestTargetDatesDao extends AbstractTablesAwareSpringDao
        implements RequestTargetDates {

    private String installationDatesTable = null;

    public RequestTargetDatesDao(JdbcTemplate jdbcTemplate, Tables tables) {
        super(jdbcTemplate, tables);
        this.installationDatesTable = getTable(TableKeys.PROJECT_INSTALLATION_DATES);
    }

    public TargetDate getInstallationDate(Long planId) {
        return getDate(installationDatesTable, planId);
    }

    /**
     * Provides the installation date
     * @param planId
     * @return date
     */
    private TargetDate getDate(String table, Long planId) {
        if (planId == null) {
            return null;
        }
        StringBuilder sql = new StringBuilder(96);
        sql
                .append("SELECT id,plan_id,target_date,created,created_by FROM ")
                .append(table)
                .append(" WHERE id IN (SELECT max(id) FROM ")
                .append(table)
                .append(" WHERE plan_id = ?)");
        final Object[] params = { planId };
        final int[] types = { Types.BIGINT };
        List list = (List) jdbcTemplate.query(sql.toString(), params, types, getReader());
        return (empty(list) ? null : (TargetDate) list.get(0));
    }

    private RowMapperResultSetExtractor getReader() {
        return new RowMapperResultSetExtractor(new TargetDateRowMapper());
    }

    private class TargetDateRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int index) throws SQLException {
            return new TargetDateImpl(
                    Long.valueOf(rs.getLong(1)),
                    Long.valueOf(rs.getLong(2)),
                    rs.getTimestamp(3),
                    rs.getTimestamp(4),
                    Long.valueOf(rs.getLong(5)));
        }
    }

}
