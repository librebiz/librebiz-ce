/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 30-Aug-2005 08:22:42 
 * 
 */
package com.osserp.core.finance;

import java.util.Date;
import java.util.List;

import com.osserp.core.Item;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Order extends DeliveryReferenceRecord, PaymentAgreementAwareRecord {

    /**
     * Provides the confirmation date. Defaults to created.
     * @return confirmationNote
     */
    Date getConfirmationDate();

    /**
     * Provides an optional confirmation note
     * @return confirmationNote
     */
    String getConfirmationNote();

    /**
     * Provides all delivery notes resulting from the order
     * @return deliveryNotes
     */
    List<DeliveryNote> getDeliveryNotes();

    /**
     * Provides all delivered items of this order by fetching all delivered or rolled in items.
     * @return deliveries
     */
    List<Item> getDeliveries();

    /**
     * Provides all not delivered items of this order.
     * 
     * Fetches delivered items as map of productId/item values. Iterates over ordered items and fetches all items whose stockAffecting flag is true and kanban
     * is false. Any of that selected items will be copied to result whose delivered count is less than ordered count.
     * @return openDeliveries
     */
    List<Item> getOpenDeliveries();

    /**
     * Refreshs delivered counter of order items
     */
    void refreshDeliveries();

    /**
     * Indicates that the order has delivery notes whose are not released
     * @return changeableDeliveryAvailable
     */
    boolean isChangeableDeliveryAvailable();

    /**
     * @return notDeliveryNoteAffectingItemsOnly
     */
    boolean isNotDeliveryNoteAffectingItemsOnly();

    /**
     * Indicates that products are available in order whose are none kanban products
     * @return noneKanbanAvailable
     */
    boolean isNoneKanbanAvailable();

    /**
     * Indicates that delivery notes created for this order should list kanban products also
     * @return kanbanDeliveryEnabled
     */
    boolean isKanbanDeliveryEnabled();

    /**
     * Enables/disbales kanban support in related delivery notes
     * @param kanbanDeliveryEnabled
     */
    void setKanbanDeliveryEnabled(boolean kanbanDeliveryEnabled);

    /**
     * Provides the current version of the sales order
     * @return version
     */
    Integer getVersion();

    /**
     * Creates a new version of the order
     * @param employeeId
     */
    void createVersion(Long employeeId);

    /**
     * Provides the date the order was changed
     * @return changed
     */
    Date getChanged();

    /**
     * Provides the id of the changing user
     * @return changedBy
     */
    Long getChangedBy();

    /**
     * Indicates that a recalculation of this order is required
     * @return recalculationRequired
     */
    boolean isRecalculationRequired();
}
