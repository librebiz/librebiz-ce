/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 2, 2016 
 * 
 */
package com.osserp.common.dms;

import java.io.Serializable;

import com.osserp.common.Constants;
import com.osserp.common.beans.AbstractClass;
import com.osserp.common.util.FileUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class DocumentData extends AbstractClass implements Serializable {

    private byte[] bytes;
    private String type = "pdf";
    private String filename;
    private String contentType = Constants.MIME_TYPE_DEFAULT;

    protected DocumentData() {
        super();
    }

    /**
     * Creates document data using default settings for pdf documents
     * @param bytes
     * @param filename
     */
    public DocumentData(byte[] bytes, String filename) {
        this(bytes, filename, null);
    }

    /**
     * Creates document data using provided contentType
     * @param bytes
     * @param filename
     * @param contentType
     */
    public DocumentData(byte[] bytes, String filename, String contentType) {
        super();
        this.bytes = bytes;
        if (FileUtil.providesPath(filename)) {
            this.filename = FileUtil.stripPath(filename);
        } else {
            this.filename = filename;
        }
        if (contentType != null) {
            this.contentType = contentType;
            if (filename != null) {
                this.type = FileUtil.getFileType(filename);
            }
        } else if (FileUtil.providesType(filename)) {
            String t = FileUtil.getFileType(filename);
            if ("pdf".equalsIgnoreCase(t)) {
                contentType = Constants.MIME_TYPE_PDF;
            } else {
                contentType = FileUtil.determineContentType(filename);
            }
            type = null;
        }
    }

    /**
     * Provides the document data as byte array
     * @return binary data
     */
    public byte[] getBytes() {
        return bytes;
    }

    /**
     * Sets the document data.
     * @param bytes
     */
    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

    /**
     * Convenience method for bytes.length 
     * @return content length
     */
    public int getContentLength() {
        return bytes == null ? 0 : bytes.length;
    }

    /**
     * Provides the HTTP reponse.contentType
     * @return contentType 
     */
    public String getContentType() {
        return contentType;
    }

    protected void setContentType(String contentType) {
        this.contentType = contentType;
    }

    /**
     * Provides the content-disposition http header 
     * (e.g. attachment; filename=realFilename)
     * @return content disposition header
     */
    public String getContentDisposition() {
        return "attachment; filename=" + getRealFilename();
    }

    /**
     * Provides the resulting filename depending on documentType config and
     * content of filename property.
     * @return filename to use for download or filesystem filename.
     */
    public String getRealFilename() {
        StringBuilder result = new StringBuilder();
        if (isNotSet(filename)) {
            result.append(System.currentTimeMillis());
            if (isSet(type)) {
                result.append(".").append(type);
            }
            return result.toString();
        }
        if (isNotSet(type) || FileUtil.providesType(filename)) {
            result.append(filename);
            return result.toString();
        }
        if (isSet(type)) {
            result.append(filename).append(".").append(type);
            return result.toString();
        }
        return filename;
    }

    /**
     * Provides the type of file (e.g. pdf, jpg, etc.). Default setting is pdf. 
     * @return type or null if provided by filename
     */
    public String getType() {
        return type;
    }

    protected void setType(String type) {
        this.type = type;
    }

    /**
     * The filename is provided on initialization and is used by getRealFilename
     * as primary lookup target. Depending on type, the filename may be a logical 
     * name without an extension or null if a simple timestamp based name should 
     * be provided as real filename.
     * @return filename or null
     */
    public String getFilename() {
        return filename;
    }

    protected void setFilename(String filename) {
        this.filename = filename;
    }
}
