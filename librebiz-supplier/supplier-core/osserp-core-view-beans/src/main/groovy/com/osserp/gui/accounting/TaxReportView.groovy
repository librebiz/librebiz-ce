/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 15, 2014 5:26:22 PM
 * 
 */
package com.osserp.gui.accounting

import com.osserp.core.finance.FinanceRecord
import com.osserp.core.finance.TaxReport
import com.osserp.core.finance.TaxReportManager

import com.osserp.gui.CoreView


/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class TaxReportView extends CoreView {
    
    List<FinanceRecord> selectedRecords = []
    
    boolean inflowSelectionMode
    boolean outflowSelectionMode
    
    TaxReportView() {
        super()
        enableAutoreload()
        enableCompanyPreselection()
        enableBranchPreselection()
        providesCreateMode()
    }
    
    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            reload()
            select()
            if (bean) {
                enableExternalInvocationMode()
            }
        }
    }
    
    @Override
    void enableCreateMode() {
        super.enableCreateMode()
        if (!selectedCompany) {
            preselectSelectedCompany()
            enableExternalInvocationMode()
        }
    }
    
    @Override
    void reload() {
        if (selectedCompany) {
            list = taxReportManager.getReports(selectedCompany)
            if (bean) {
                TaxReport report = list.find { it.id == bean.id }
                if (report) {
                    report = taxReportManager.getReport(report.id)
                    if (inflowSelectionMode) {
                        selectedRecords = report.inflowOfCash
                    } else if (outflowSelectionMode) {
                        selectedRecords = report.outflowOfCash
                    }
                    bean = report
                } else {
                    // previous report was deleted
                    bean = null
                }
            }
        }
    }
    
    @Override
    void save() {
        if (createMode) {
            bean = taxReportManager.create(
                domainEmployee,
                selectedCompany,
                form.getString('name'),
                form.getDate('start'),
                form.getDate('stop'))
            disableCreateMode()
            reload()
        }
    }
    
    @Override
    void select() {
        if (!form.getLong('id')) {
            // !id == klicked navigation back-button in bean mode
            if (inflowSelectionMode) {
                inflowSelectionMode = false
            } else if (outflowSelectionMode) {
                outflowSelectionMode = false
            } else {
                super.select()
            }
        } else {
            super.select()
            if (bean) {
                // list elements don't have in- and outflow lists load
                bean = taxReportManager.getReport(bean.id)
            }
        }
    }
    
    void enableInflowSelectionMode() {
        if (bean?.inflowOfCash) {
            selectedRecords = bean.inflowOfCash
            inflowSelectionMode = true
        }
    }

    void enableOutflowSelectionMode() {
        if (bean?.outflowOfCash) {
            selectedRecords = bean.outflowOfCash
            outflowSelectionMode = true
        }
    }
    
    void disableFlowSelectionMode() {
        inflowSelectionMode = false
        outflowSelectionMode = false
    }
    
    protected TaxReportManager getTaxReportManager() {
        getService(TaxReportManager.class.getName())
    }
}
