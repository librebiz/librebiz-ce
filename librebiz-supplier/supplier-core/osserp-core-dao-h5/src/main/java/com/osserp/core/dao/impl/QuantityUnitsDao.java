/**
 *
 * Copyright (C) 2020 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 2, 2020 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.Iterator;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.dao.AbstractHibernateAndSpringDao;
import com.osserp.common.dao.Tables;

import com.osserp.core.QuantityUnit;
import com.osserp.core.dao.QuantityUnits;
import com.osserp.core.model.products.QuantityUnitImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class QuantityUnitsDao extends AbstractHibernateAndSpringDao implements QuantityUnits {

    public QuantityUnitsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory) {

        super(jdbcTemplate, tables, sessionFactory);
    }

    public List<QuantityUnit> getList() {
        return getCurrentSession().createQuery(
                "from " + QuantityUnitImpl.class.getName() + " obj order by obj.name")
                .list();
    }

    public QuantityUnit create(String name, int digits) {
        QuantityUnit result = null;
        List<QuantityUnit> existing = getList();
        for (Iterator<QuantityUnit> i = existing.iterator(); i.hasNext();) {
            QuantityUnit next = i.next();
            if (next.getName().equalsIgnoreCase(name)) {
                result = next;
                break;
            }
        }
        if (result == null) {
            QuantityUnitImpl obj = new QuantityUnitImpl(name, digits);
            getCurrentSession().save(QuantityUnitImpl.class.getName(), obj);
            result = obj;
        }
        return result;
    }

}
