<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<div class="col-md-12 panel-area panel-area-default">
    <div class="modalBoxHeader">
        <div class="modalBoxLeft">
            <p class="boldtext"><fmt:message key="createBookingCorrection"/></p>
        </div>
    </div>
    <div class="modalBoxData">
        <c:import url="/pages/hrm/shared/time_record_update.jsp"/>
    </div>
</div>
