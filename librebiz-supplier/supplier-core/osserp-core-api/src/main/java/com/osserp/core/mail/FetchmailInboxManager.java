/**
 *
 * Copyright (C) 2021 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License (AGPL)
 * version 3 as published by the Free Software Foundation. In accordance
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code
 * versions must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area
 * or, if not reasonably feasible for technical reason, as a top-level
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute.
 *
 */
package com.osserp.core.mail;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.mail.Attachment;
import com.osserp.common.mail.ReceivedMail;
import com.osserp.common.mail.ReceivedMailInfo;

import com.osserp.core.BusinessCase;
import com.osserp.core.customers.Customer;
import com.osserp.core.suppliers.Supplier;
import com.osserp.core.users.DomainUser;

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public interface FetchmailInboxManager {

    static final Integer MOVE_ATTACHMENTS_BY_TYPE = 1;
    static final Integer MOVE_ATTACHMENTS_BY_TYPE_DELETE_TEXT = 2;
    static final Integer MOVE_MESSAGE_DELETE_ATTACHMENTS = 3;
    static final String[] INBOX_ADMIN_PERMISSIONS = {"fetchmail_admin"};

    List<ReceivedMailInfo> findByBusinessCase(DomainUser user, BusinessCase bc);

    List<ReceivedMailInfo> findByCustomer(Customer customer);

    List<ReceivedMailInfo> findByStatus(DomainUser user, Integer status);

    List<ReceivedMailInfo> findBySupplier(Supplier supplier);

    List<ReceivedMailInfo> findByUser(DomainUser user);

    int getCountByBusinessCase(DomainUser user, BusinessCase bc);

    int getCountByUser(DomainUser user);

    ReceivedMail getMessage(String messageId);

    void delete(ReceivedMail mail);

    ReceivedMail setBusinessCase(ReceivedMail mail, Long reference, Long referenceType, Long businessId);

    ReceivedMail setUser(ReceivedMail mail, Long userId);

    ReceivedMail resetReferences(ReceivedMail mail);

    ReceivedMail deleteAttachment(ReceivedMail mail, Attachment attachment);

    ReceivedMail ignoreAttachment(ReceivedMail mail, Attachment attachment);

    ReceivedMail moveToDocuments(
            DomainUser user,
            ReceivedMail mail,
            Attachment attachment,
            BusinessCase bc) throws ClientException;

    ReceivedMail moveToPictures(
            DomainUser user,
            ReceivedMail mail,
            Attachment attachment,
            BusinessCase bc) throws ClientException;

    void moveToNotes(
            DomainUser user,
            ReceivedMail mail,
            BusinessCase bc,
            Integer attachmentAction,
            String updatedText) throws ClientException;

    void restoreMoved(DomainUser user, ReceivedMail mail) throws ClientException;

}
