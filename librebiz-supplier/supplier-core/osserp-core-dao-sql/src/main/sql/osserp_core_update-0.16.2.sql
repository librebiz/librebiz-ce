SET search_path = osserp, pg_catalog;

ALTER TABLE document_types ADD COLUMN tb_width integer DEFAULT 0;
ALTER TABLE document_types ADD COLUMN tb_height integer DEFAULT 0;

ALTER TABLE documents ADD COLUMN tb_avail boolean DEFAULT false;

INSERT INTO system_properties VALUES (237, 'dmsListThumbnailHeight', '128px', 'java.lang.String', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (238, 'dmsGalleryThumbnailCols', 'col-sm-6 col-md-3', 'java.lang.String', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (239, 'dmsGalleryThumbnailHeight', '240px', 'java.lang.String', true, false, NULL, false, 0);
INSERT INTO system_properties VALUES (240, 'dmsGalleryImageHeight', '720px', 'java.lang.String', true, false, NULL, false, 0);

ALTER TABLE record_exports ADD COLUMN start_date timestamp without time zone;
ALTER TABLE record_exports ADD COLUMN end_date timestamp without time zone;
ALTER TABLE record_exports ADD COLUMN export_count integer DEFAULT 0;

CREATE OR REPLACE FUNCTION osserp.init_voucher_exports() RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
    obj      RECORD;
    dates    RECORD;
    v_cnt    integer := 0;
    i_cnt    integer := 0;
BEGIN
    for obj in select * from osserp.record_exports where start_date is null order by id loop
        if (obj.type_id = 8) then
            select min(p.paid), max(p.paid) into dates from osserp.sales_payments p where p.id in (select record_id from osserp.v_record_exports where id = obj.id);
            update osserp.record_exports set start_date = dates.min, end_date = dates.max where id = obj.id;
            select count(*) into v_cnt from osserp.record_export_items where export_id = obj.id;
            update osserp.record_exports set export_count = v_cnt where id = obj.id;
            i_cnt := i_cnt + 1;
        else
            if (obj.type_id = 108) then
                select min(p.paid), max(p.paid) into dates from osserp.purchase_payments p where p.id in (select record_id from osserp.v_record_exports where id = obj.id);
                update osserp.record_exports set start_date = dates.min, end_date = dates.max where id = obj.id;
                select count(*) into v_cnt from osserp.record_export_items where export_id = obj.id;
                update osserp.record_exports set export_count = v_cnt where id = obj.id;
                i_cnt := i_cnt + 1;
            else
                select min(q.created), max(q.created) into dates from osserp.v_record_exports e, osserp.v_record_query q where e.type_id = q.type_id and e.record_id = q.id and e.id = obj.id ;
                update osserp.record_exports set start_date = dates.min, end_date = dates.max where id = obj.id;
                select count(*) into v_cnt from osserp.record_export_items where export_id = obj.id;
                update osserp.record_exports set export_count = v_cnt where id = obj.id;
                i_cnt := i_cnt + 1;
            end if;
        end if;
    end loop;
    return i_cnt;
END;
$$;

SELECT osserp.init_voucher_exports();

DROP FUNCTION osserp.init_voucher_exports();

UPDATE system_properties SET order_id = order_id + 1 WHERE context_name = 'documentConfig' AND displayable AND NOT is_admin AND order_id > 6;
INSERT INTO system_properties VALUES (241, 'documentPrintFontSizeTerms', '8pt', 'java.lang.String', true, false, 'documentConfig', false, 7);
INSERT INTO system_properties VALUES (242, 'documentPrintVoucherNoteSpaceBefore', '1pt', 'java.lang.String', true, false, 'documentConfig', false, 25);
INSERT INTO system_properties VALUES (243, 'documentPrintVoucherTermsSpaceBefore', '5pt', 'java.lang.String', true, false, 'documentConfig', false, 26);
INSERT INTO system_properties VALUES (244, 'documentPrintVoucherTermsSpaceRight', '0.5cm', 'java.lang.String', true, false, 'documentConfig', false, 27);
SELECT pg_catalog.setval('system_properties_id_seq', 244, true);

INSERT INTO user_property_defaults VALUES (11, 'salesNoteMailAutoCC', 'false', 'java.lang.Boolean');
INSERT INTO user_property_defaults VALUES (12, 'salesNoteMailAutoBCC', 'false', 'java.lang.Boolean');
SELECT pg_catalog.setval('user_property_defaults_id_seq', 12, true);

ALTER TABLE documents ADD COLUMN shasum text;
ALTER TABLE mail_attachments ADD COLUMN tb_avail boolean DEFAULT false;
ALTER TABLE mail_attachments ADD COLUMN shasum text DEFAULT NULL;

UPDATE system_properties SET pvalue = '0.16.2', ptype = 'java.lang.String' WHERE name = 'dbmsSchemaVersion';
