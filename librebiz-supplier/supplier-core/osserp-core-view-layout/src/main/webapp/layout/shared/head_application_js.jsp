<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:import url="/layout/shared/head_htmx_js.jsp" />
<c:if test="${!empty view.scriptsList}">
<c:forEach var="scriptName" items="${view.scriptsList}">
<c:import url="/layout/shared/head_${scriptName}.jsp" />
</c:forEach>
</c:if>
