/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 04.09.2004 
 * 
 */
package com.osserp.common.beans;

import java.util.Date;
import java.util.Map;

import com.osserp.common.Option;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class OptionImpl extends AbstractOption {

    public OptionImpl() {
        super();
    }

    protected OptionImpl(Long id) {
        super(id);
    }

    protected OptionImpl(String name) {
        super(name);
    }

    public OptionImpl(Long id, String name) {
        super(id, name);
    }

    public OptionImpl(Long id, String name, boolean endOfLife) {
        super(id, name, endOfLife);
    }

    public OptionImpl(Long id, String name, String resourceKey) {
        super(id, name, resourceKey);
    }

    public OptionImpl(Long id, String name, String description, String resourceKey) {
        super(id, name, description, resourceKey);
    }

    public OptionImpl(Long id, Long reference, String name) {
        super(id, name);
        setReference(reference);
    }

    public OptionImpl(Long id, Long reference, String name, Long createdBy) {
        this(id, reference, name);
        setCreatedBy(createdBy);
    }

    public OptionImpl(Long id, Long reference, String name, Date created, Long createdBy) {
        this(id, reference, name);
        setCreated(created);
        setCreatedBy(createdBy);
    }

    public OptionImpl(Option o) {
        super(o);
    }

    protected OptionImpl(Map<String, Object> map) {
        super(map);
    }

    @Override
    public Object clone() {
        return new OptionImpl(this);
    }
}
