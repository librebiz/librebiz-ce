/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 15, 2008 11:27:07 AM 
 * 
 */
package com.osserp.core.planning;

import java.util.Date;
import java.util.List;

import com.osserp.common.Entity;
import com.osserp.core.finance.Stock;
import com.osserp.core.views.OrderItemsDisplay;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ProductPlanning extends Entity {

    /**
     * Provides the planning horizon. Default horizon is null for unlimited planning.
     * @return horizon
     */
    Date getHorizon();

    /**
     * Provides the related stock
     * @return stock
     */
    Stock getStock();

    /**
     * Provides all items open for delivery
     * @return openItems
     */
    List<OrderItemsDisplay> getOpenItems();

    List<OrderItemsDisplay> getOpenPurchaseItems();

    List<OrderItemsDisplay> getUnreleasedItems();

    List<OrderItemsDisplay> getVacantItems();

    List<OrderItemsDisplay> createList(
            Long selectedProduct,
            Long selectedProductCategory,
            Long selectedProductGroup,
            Long selectedProductType,
            boolean listUnreleased,
            boolean listVacant);

    List<SalesPlanningSummary> createSummary(List<OrderItemsDisplay> list);

    boolean isAvailableForDelivery(Long sales);

    List<SalesPlanningSummary> getItems(Long sales, boolean listVacant);

    ProductPlanning getCopy(boolean confirmedPurchaseOrdersOnly);

    ProductPlanning getCopy(boolean confirmedPurchaseOrdersOnly, Date horizon);

    void refresh(
            List<OrderItemsDisplay> openItems,
            List<OrderItemsDisplay> openPurchaseItems,
            List<OrderItemsDisplay> unreleasedItems,
            List<OrderItemsDisplay> vacantItems);

}
