/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 16, 2006 9:12:50 AM 
 * 
 */
package com.osserp.gui.client.records.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jdom2.Document;
import org.jdom2.Element;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.Entity;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.common.PermissionException;
import com.osserp.common.dms.DmsDocument;
import com.osserp.common.dms.DmsManager;
import com.osserp.common.dms.DmsReference;
import com.osserp.common.util.CollectionUtil;
import com.osserp.common.util.DateFormatter;
import com.osserp.common.util.NumberFormatter;
import com.osserp.common.util.NumberUtil;
import com.osserp.common.web.Actions;
import com.osserp.common.web.Form;
import com.osserp.common.xml.JDOMUtil;

import com.osserp.core.BankAccount;
import com.osserp.core.BusinessCase;
import com.osserp.core.BusinessCaseSearch;
import com.osserp.core.BusinessCaseTemplateManager;
import com.osserp.core.BusinessTemplate;
import com.osserp.core.Item;
import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.ContactSearch;
import com.osserp.core.dms.Letter;
import com.osserp.core.dms.LetterManager;
import com.osserp.core.dms.LetterType;
import com.osserp.core.employees.EmployeeSearch;
import com.osserp.core.finance.ItemChangedInfo;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.OrderItem;
import com.osserp.core.finance.PaymentAwareRecord;
import com.osserp.core.finance.RebateAwareRecord;
import com.osserp.core.finance.RebateManager;
import com.osserp.core.finance.RebateManagerProvider;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordInfo;
import com.osserp.core.finance.RecordInfoConfigManager;
import com.osserp.core.finance.RecordManager;
import com.osserp.core.finance.RecordOutputService;
import com.osserp.core.finance.RecordPrintManager;
import com.osserp.core.finance.RecordStatusHistory;
import com.osserp.core.finance.RecordType;
import com.osserp.core.finance.Stock;
import com.osserp.core.finance.TaxRateManager;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductSelection;
import com.osserp.core.products.ProductSelectionManager;
import com.osserp.core.products.ProductSummaryManager;
import com.osserp.core.purchasing.PurchaseRecord;
import com.osserp.core.sales.Sales;
import com.osserp.core.sales.SalesSearch;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.system.SystemCompany;
import com.osserp.core.system.SystemConfigManager;
import com.osserp.core.users.DomainUser;

import com.osserp.gui.client.impl.AbstractView;
import com.osserp.gui.client.records.RecordView;

/**
 * 
 * Provides a common view for record actions. Default action target is 'display'
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractRecordView extends AbstractView implements RecordView {
    private static Logger log = LoggerFactory.getLogger(AbstractRecordView.class.getName());

    private static final String CUSTOM_HEADER_SUPPORT_PROPERTY = "recordCustomHeaderSupport";

    private List<Option> recordInfos = new ArrayList<>();
    private List<Option> allInfos = new ArrayList<>();
    private List<Option> signatureLeftEmployees = null;
    private List<Option> signatureRightEmployees = null;
    private List<Double> taxRates = null;
    private List<Double> reducedTaxRates = null;
    private boolean confirmDelete = false;
    private Item currentItem = null;
    private boolean productEditMode = false;
    private boolean itemEditMode = false;
    private boolean itemPasteMode = false;
    private boolean itemReplaceMode = false;
    private boolean salesIdEditMode = false;
    private boolean exitBlocked = false;
    private String exitBlockedTarget = null;
    private boolean displayDescription = true;
    private boolean itemListing = false;
    private boolean unreleasedListMode = false;
    private List<Contact> contactPersons = new ArrayList<>();
    private Contact contactPerson = null;
    private boolean priceDisplayEnabled = true;
    private boolean priceInputEnabled = true;
    private boolean providingCorrections = false;
    private boolean previousRequestExit = false;
    private Long selectedStock = null;
    private ProductSelection employeeProductFilter = null;
    private List<ItemChangedInfo> itemChangedHistory = new ArrayList<>();

    private boolean deliveryDateEditMode = false;
    private Item selectedItem = null;
    private boolean itemDeliveryDateEditMode = false;
    private boolean stockEditMode = false;
    private DmsDocument document = null;
    private LetterType embeddedTextType = null;
    private Letter embeddedTextAbove = null;
    private Letter embeddedTextBelow = null;

    private boolean deleteAllItemsSupport = false;
    private boolean multipleBranchsAvailable = false;
    private List<BranchOffice> branchOfficeList = new ArrayList<>();

    protected void deleteAllItemsEnabled() {
        deleteAllItemsSupport = true;
    }

    // purchase records only
    private boolean supplierReferenceNumberEditMode = false;

    private List<BusinessTemplate> businessTemplates = new ArrayList<>();

    /**
     * Initializes action target as 'display' initializes list target as 'list', invokes {@link #init()}
     */
    protected AbstractRecordView() {
        super();
        setActionTarget(Actions.DISPLAY);
        setListTarget(Actions.LIST);
    }

    /**
     * Initializes product view with current user.
     * @param request
     */
    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        super.init(request);
        initLists();
        ProductSelectionManager manager = (ProductSelectionManager) getService(ProductSelectionManager.class.getName());
        employeeProductFilter = manager.getSelection(getDomainEmployee(), true);
        if (log.isDebugEnabled()) {
            if (employeeProductFilter == null) {
                log.debug("init() product selection not found [employee=" + getDomainEmployee().getId() + "]");
            } else if (isNotSet(employeeProductFilter.getId())) {
                log.debug("init() default product selection found [employee=" + getDomainEmployee().getId() + "]");
            } else {
                log.debug("init() product selection found [employee=" + getDomainEmployee().getId()
                        + ", id=" + employeeProductFilter.getId() + "]");
            }
        }
    }

    public String getAccountingPermissions() {
        return "accounting,customer_service,executive";
    }

    public void loadRecord(Long id, String exitTarget, String exitId, boolean readonly) throws ClientException {
        setReadOnlyMode(readonly);
        if (!readonly) {
            initLists();
        }
        if (!getRecordManager().exists(id)) {
            if (log.isDebugEnabled()) {
                log.debug("loadRecord() throwing exception while record not exists [id=" + id + "]");
            }
            throw new ClientException(ErrorCode.RECORD_NOT_AVAILABLE);
        }
        Record record = loadRecord(id);
        initContactPerson(record);
        setRecord(record);
        if (exitTarget != null) {
            setExitTarget(exitTarget);
        }
        if (exitId != null) {
            setExitId(exitId);
        }
        if (log.isDebugEnabled()) {
            log.debug("loadRecord() done [id=" + id + ", exitTarget=" + exitTarget + ", exitId=" + exitId + "]");
        }
    }

    /**
     * Loads a record from persistent storage. Method is used by {@link #loadRecord(Long, String, boolean)}, {@link #refresh()} and {@link #getRecord()}. <br/>
     * Override this if implementing record view handles different record types with different managers.
     * @param id
     * @return
     */
    protected Record loadRecord(Long id) {
        Record record = getRecordManager().find(id);
        return record;
    }

    @Override
    public void setEditMode(boolean editMode) throws PermissionException {
        super.setEditMode(editMode);
        if (editMode) {
            this.supplierReferenceNumberEditMode = false;
            this.initContactPersons(getRecord().getContact());
        } else if (contactPersons == null) {
            this.contactPersons = new ArrayList<>();
        } else {
            this.contactPersons.clear();
        }
    }

    protected void savePrintOptionDefaults(Form form, Record record) {
        boolean update = form.getBoolean("printOptionTypeDefaults");
        if (update) {
            try {
                getRecordManager().updatePrintOptionDefaults(getDomainEmployee(), record);
            } catch (Exception e) {
                log.warn("savePrintOptionDefaults() ignoring failure [record=" + record.getId()
                        + ", message=" + e.getMessage() + "]");
            }
        }
    }

    public boolean isItemListing() {
        return itemListing;
    }

    public void changeItemListing() {
        this.itemListing = !itemListing;
    }

    /**
     * Provides the current record
     * @return record
     */
    public Record getRecord() {
        Object obj = getBean();
        if (obj == null) {
            throw new ActionException("bean was null");
        }
        if (obj instanceof Record) {
            return (Record) obj;
        }
        if (obj instanceof Entity) {
            setBean(loadRecord(((Entity) obj).getId()));
            return (Record) getBean();
        }
        throw new ActionException("entity expected, found " + obj.getClass().getName());
    }

    @Override
    public void setSelection(Long id) {
        super.setSelection(id);
        try {
            if (id != null) {
                refresh();
            }
        } catch (Exception e) {
            log.warn("setSelection() ignoring failed refresh: " + e.toString(), e);
        }
    }

    public void setRecord(Record record) {
        if (record != null) {
            if (log.isDebugEnabled()) {
                StringBuilder buffer = new StringBuilder("setRecord() invoked [id=");
                buffer.append(record.getId());
                if (record instanceof PaymentAwareRecord) {
                    PaymentAwareRecord par = (PaymentAwareRecord) record;
                    buffer.append(", message=").append(par.getPayments().size());
                }
                buffer.append("]");
            }
            reloadProductSummary(record);
            if (getList() != null && !getList().isEmpty()) {
                try {
                    CollectionUtil.replaceByProperty(getList(), "id", record);
                } catch (Exception ignorable) {
                    // we ignore this, should never happen
                }
            }
            if (!isReadOnlyMode() && record.getType().isSupportingConditions()) {
                List<RecordInfo> existingInfos = record.getInfos();
                Set<Long> set = new HashSet<>();
                for (int i = 0, j = existingInfos.size(); i < j; i++) {
                    RecordInfo inf = existingInfos.get(i);
                    set.add(inf.getInfoId());
                }
                List<Option> available = new ArrayList<>();
                for (int i = 0, j = allInfos.size(); i < j; i++) {
                    Option o = allInfos.get(i);
                    if (!set.contains(o.getId())) {
                        available.add(o);
                    }
                }
                recordInfos = available;
            }
            initContactPerson(record);
            itemChangedHistory = getRecordManager().getItemChangedInfos(record.getId());
            embeddedTextAbove = embeddedTextBelow = null;
            embeddedTextType = fetchEmbeddedTextType(record.getType());
            if (embeddedTextType != null) {
                List<Letter> embeddedTextList = getLetterManager().findLetters(
                        embeddedTextType, record.getContact(), record.getId());
                for (int i = 0, j = embeddedTextList.size(); i < j; i++) {
                    Letter text = embeddedTextList.get(i);
                    if (text.isEmbedded()) {
                        if (text.isEmbeddedAbove()) {
                            embeddedTextAbove = text;
                        } else {
                            embeddedTextBelow = text;
                        }
                    }
                }
            } else if (log.isDebugEnabled()) {
                log.debug("setRecord() embedded text not supported [recordType="
                        + record.getType().getId().toString()
                        + ", name=" + record.getType().getResourceKey() + "]");
            }
        }
        super.setBean(record);
        if (log.isDebugEnabled()) {
            log.debug("setRecord() done [record="
                    + ((record == null) ? "null]" : record.getId().toString() + "]"));
        }
    }

    public boolean isItemChangedHistoryAvailable() {
        return !itemChangedHistory.isEmpty();
    }

    public List<ItemChangedInfo> getItemChangedHistory() {
        return itemChangedHistory;
    }

    public void changeItemIgnoreInDocument(Long id) {
        Record record = getRecord();
        for (Iterator<Item> i = record.getItems().iterator(); i.hasNext();) {
            Item next = i.next();
            if (next.getId().equals(id)) {
                next.setIgnoreInDocument(!next.isIgnoreInDocument());
                break;
            }
        }
        getRecordManager().persist(record);
        refresh();
    }

    public void releaseHistorical() {
        Record record = getRecord();
        record.disableItemChangeable();
        getRecordManager().persist(record);
        refresh();
    }

    /**
     * Refreshs current record with values from the backend. This method should be called if related record was updated outside this view
     */
    public void refresh() {
        Record updated = loadRecord(getRecord().getId());
        setRecord(updated);
        if (log.isDebugEnabled()) {
            log.debug("refresh() done [record=" + updated.getId() + "]");
        }
    }

    @Override
    public final void reload() {
        refresh();
    }

    public List<Option> getSignatureLeftEmployees() {
        return signatureLeftEmployees;
    }

    public List<Option> getSignatureRightEmployees() {
        return signatureRightEmployees;
    }

    public List<Contact> getContactPersons() {
        return contactPersons;
    }

    public void changeContact(ClassifiedContact contact) throws ClientException {
        Record record = getRecord();
        if (record != null && record.getContact() != null &&
                contact != null && contact.getId() != null &&
                !contact.getId().equals(record.getContact().getId())) {
            RecordManager manager = getRecordManager();
            setBean(manager.changeContact(getDomainEmployee(), record, contact));
        } else if (log.isDebugEnabled()) {
            log.debug("changeContact() ignoring selection [id=" +
                    (contact == null ? "null]"
                            : (contact.getId() + ", record="
                                    + (record == null ? "null]" : (record.getId() + "]")))));
        }
    }

    public List<Double> getTaxRates() {
        return taxRates;
    }

    public List<Double> getReducedTaxRates() {
        return reducedTaxRates;
    }

    public Contact getContactPerson() {
        return contactPerson;
    }

    public final boolean isRecordDeletable() {
        DomainUser user = getDomainUser();
        Record record = getRecord();
        boolean editByUser = (user.getId().equals(record.getCreatedBy())
                || user.getId().equals(record.getChangedBy()));
        if (!record.isUnchangeable() && editByUser) {
            return true;
        }
        boolean result = getRecordManager().deletePermissionGrant(user, record);
        return isRecordDeletePermissionGrant(result);
    }

    /**
     * Override this method to override the result of default permission check
     * @param defaultChecksResult
     * @return true if delete permission grant. The default implementation returns the value of the defaultChecksResult param
     */
    protected boolean isRecordDeletePermissionGrant(boolean defaultChecksResult) {
        return defaultChecksResult;
    }

    public void delete() throws ClientException, PermissionException {
        DomainUser user = getDomainUser();
        Record record = getRecord();
        RecordManager recordManager = getRecordManager();
        if (recordManager.deletePermissionGrant(user, record)) {
            recordManager.delete(record, user.getEmployee());
            confirmDelete = false;
        } else {
            throw new PermissionException();
        }
    }

    public boolean isProductEditMode() {
        return productEditMode;
    }

    public void setProductEditMode(boolean productEditMode) {
        this.productEditMode = productEditMode;
    }

    public boolean isProductsChangeable() {
        Record record = getRecord();
        return (!record.isUnchangeable() || (record.isHistorical() && record.isItemsChangeable()));
    }

    public boolean isItemBackupAvailable() {
        return false;
    }

    public boolean isImportSupported() {
        return false;
    }

    public void restoreItems() {
        // Default does nothing. Implement if required.
    }

    public void setItemEdit(Long itemId) {
        if (itemId == null) {
            currentItem = null;
            itemEditMode = false;
        } else {
            currentItem = getSelectedItem(getRecord(), itemId);
            itemEditMode = true;
        }
    }

    public boolean isItemEditMode() {
        return itemEditMode;
    }

    public void setItemPaste(Long itemId) {
        if (itemId == null) {
            currentItem = null;
            itemPasteMode = false;
        } else {
            currentItem = getSelectedItem(getRecord(), itemId);
            itemPasteMode = true;
        }
    }

    public boolean isItemPasteMode() {
        return itemPasteMode;
    }

    public void setItemReplace(Long itemId) {
        if (itemId == null) {
            currentItem = null;
            itemReplaceMode = false;
        } else {
            currentItem = getSelectedItem(
                    getRecord(),
                    itemId);
            itemReplaceMode = true;
        }
    }

    public boolean isItemReplaceMode() {
        return itemReplaceMode;
    }

    public Item getCurrentItem() {
        return currentItem;
    }

    public void updateItem(
            Item item,
            String customName,
            Double quantity,
            BigDecimal price,
            Double taxRate,
            String note)
        throws ClientException {
        Record record = getRecord();
        RecordManager manager = getRecordManager();
        manager.updateItem(
                getDomainEmployee(),
                record,
                item,
                customName,
                quantity,
                price,
                taxRate,
                item.getStockId(),
                note);
    }

    public void addItems(
            Product[] products,
            String[] customNames,
            Double[] quantities,
            Double[] taxRates,
            BigDecimal[] prices,
            String[] notes) throws ClientException {

        if (products == null || quantities == null || prices == null
                || products.length != quantities.length || products.length != prices.length) {
            throw new ClientException(ErrorCode.SELECTIONS_MISSING);
        }
        /*
         * if (log.isDebugEnabled()) { log.debug("addItems() invoked [productsCount=" + products.length + ", notesCount=" + (notes == null ? "null]" :
         * notes.length + "]")); }
         */
        Record record = getRecord();
        RecordManager manager = getRecordManager();
        Stock defaultStock = manager.getStockByRecord(record);
        for (int i = 0, j = products.length; i < j; i++) {
            if (quantities[i] != null && quantities[i].doubleValue() != 0) {
                Product product = products[i];
                if (product != null) {
                    product.setCurrentStock(selectedStock);
                    if (log.isDebugEnabled()) {
                        log.debug("addItems() adding item [product="
                                + product.getProductId()
                                + ", quantity=" + quantities[i]
                                + ", price=" + prices[i]
                                + "]");

                    }
                    String cname = (!product.isAffectsStock()
                            && (product.getGroup().isCustomNameSupport()
                                    || (product.getCategory() != null
                                            && product.getCategory().isCustomNameSupport()))) ? customNames[i] : null;
                    if (isPriceInputEnabled()) {
                        manager.addItem(
                                getDomainEmployee(),
                                record,
                                product,
                                defaultStock.getId(),
                                cname,
                                quantities[i],
                                taxRates[i],
                                prices[i],
                                notes == null ? null : notes[i]);
                    } else {
                        manager.addItem(
                                getDomainEmployee(),
                                record,
                                product,
                                defaultStock.getId(),
                                cname,
                                taxRates[i],
                                quantities[i],
                                null,
                                notes == null ? null : notes[i]);
                    }
                }
            }
        }
    }

    public synchronized final void deleteItem(Long itemId) throws ClientException {
        Record record = getRecord();
        for (int i = 0, j = record.getItems().size(); i < j; i++) {
            Item current = record.getItems().get(i);
            if (current.getId().equals(itemId)) {
                deleteItemValidation(record, current);
                RecordManager manager = getRecordManager();
                manager.deleteItem(getDomainEmployee(), record, current);
                // explicit reloading of record to prevent side effects when using several windows/tabs
                setRecord(manager.load(record.getId()));
                break;
            }
        }
    }

    public synchronized final void deleteItems() throws ClientException {
        if (!deleteAllItemsSupport) {
            throw new ClientException(ErrorCode.OPERATION_NOT_SUPPORTED);
        }
        Record record = getRecord();
        RecordManager manager = getRecordManager();
        manager.deleteItems(getDomainEmployee(), record);
        // explicit reloading of record to prevent side effects when using several windows/tabs
        setRecord(manager.load(record.getId()));
    }

    public synchronized final void replaceItem(Product product) {
        if (currentItem == null) {
            throw new ActionException("currentItem not bound");
        }
        Record record = getRecord();
        RecordManager manager = getRecordManager();
        if (log.isDebugEnabled()) {
            log.debug("replaceItem() invocation [record=" + record.getId() + "]");
        }
        setRecord(manager.replaceItem(getDomainEmployee(), record, currentItem, product));
        record = getRecord();
        for (int i = 0, j = record.getItems().size(); i < j; i++) {
            Item current = record.getItems().get(i);
            if (current.getId().equals(currentItem.getId())) {
                currentItem = current;
                itemEditMode = true;
                break;
            }
        }
        itemReplaceMode = false;
    }

    public synchronized final void pasteItem(Long itemId) {
        if (currentItem == null) {
            throw new ActionException("currentItem not bound");
        }
        Record record = getRecord();
        RecordManager manager = getRecordManager();
        if (log.isDebugEnabled()) {
            log.debug("pasteItem() invocation [record=" + record.getId() + "]");
        }
        setRecord(manager.pasteItem(getDomainEmployee(), record, currentItem, itemId));
        itemPasteMode = false;
    }

    public Item getSelectedItem() {
        return selectedItem;
    }

    /**
     * Implementing class may provide extended validation for item deletion
     * @param record
     * @param selected
     * @throws ClientException if validation failed
     */
    protected void deleteItemValidation(Record record, Item selected) throws ClientException {
        // default implementation does nothing
    }

    public void updateItem(Form fh) throws ClientException {
        if (itemEditMode && currentItem != null) {
            RecordManager manager = getRecordManager();
            manager.updateItem(
                    getDomainEmployee(),
                    getRecord(),
                    currentItem,
                    fh.getString("customName"),
                    fh.getDouble("quantity"),
                    fh.getDecimal("price"),
                    fh.getDouble("taxRate"),
                    fh.getLong("stockId"),
                    fh.getString("note"));
            itemEditMode = false;
            currentItem = null;
        }
    }

    public void moveDownItem(Long id) {
        RecordManager manager = getRecordManager();
        Record rec = getRecord();
        manager.moveDownItem(getDomainEmployee(), rec, id);
        setRecord(manager.load(rec.getId()));
    }

    public void moveUpItem(Long id) {
        RecordManager manager = getRecordManager();
        Record rec = getRecord();
        manager.moveUpItem(getDomainEmployee(), rec, id);
        setRecord(manager.load(rec.getId()));
    }

    public void unlockItems() throws ClientException {
        RecordManager manager = getRecordManager();
        Record rec = getRecord();
        setRecord(manager.unlockItems(getDomainEmployee(), rec));
    }

    public List<Option> getRecordInfos() {
        return recordInfos;
    }

    public void setRecordInfos(List<Option> recordInfos) {
        this.recordInfos = recordInfos;
    }

    public void clearInfos() {
        RecordManager manager = getRecordManager();
        manager.clearInfos(getRecord());
        refresh();
        if (log.isDebugEnabled()) {
            log.debug("clearInfos() done");
        }
    }

    public void setDefaultInfos() throws PermissionException {
        RecordManager manager = getRecordManager();
        manager.setDefaultInfos(
                getDomainUser().getEmployee(),
                getRecord());
        refresh();
        if (log.isDebugEnabled()) {
            log.debug("setDefaultInfos() done");
        }
    }

    public void addInfo(Long id) throws PermissionException {
        RecordManager manager = getRecordManager();
        for (int i = 0, j = recordInfos.size(); i < j; i++) {
            Option o = recordInfos.get(i);
            if (o.getId().equals(id)) {
                manager.addInfo(
                        getDomainUser().getEmployee(),
                        getRecord(),
                        o);
                refresh();
                if (log.isDebugEnabled()) {
                    log.debug("addInfo() added [id=" + id + "]");
                }
                break;
            }
        }
    }

    public void removeInfo(Long id) {
        if (log.isDebugEnabled()) {
            log.debug("removeInfo() invoked [id=" + id + "]");
        }
        RecordManager manager = getRecordManager();
        List<RecordInfo> existing = getRecord().getInfos();
        for (int i = 0, j = existing.size(); i < j; i++) {
            RecordInfo next = existing.get(i);
            if (next.getId().equals(id)) {
                Record current = getRecord();
                manager.removeInfo(current, next);
                if (log.isDebugEnabled()) {
                    log.debug("removeInfo() removed [id=" + id + "]");
                }
                refresh();
                break;
            }
        }
    }

    public void moveInfoDown(Long id) {
        if (log.isDebugEnabled()) {
            log.debug("moveInfoDown() invoked [id=" + id + "]");
        }
        RecordManager manager = getRecordManager();
        List<RecordInfo> existing = getRecord().getInfos();
        for (int i = 0, j = existing.size(); i < j; i++) {
            RecordInfo next = existing.get(i);
            if (next.getId().equals(id)) {
                Record current = getRecord();
                manager.moveInfoDown(current, next);
                if (log.isDebugEnabled()) {
                    log.debug("moveInfoDown() info moved [id=" + id + "]");
                }
                refresh();
                break;
            }
        }
    }

    public void moveInfoUp(Long id) {
        if (log.isDebugEnabled()) {
            log.debug("moveInfoUp() invoked [id=" + id + "]");
        }
        RecordManager manager = getRecordManager();
        List<RecordInfo> existing = getRecord().getInfos();
        for (int i = 0, j = existing.size(); i < j; i++) {
            RecordInfo next = existing.get(i);
            if (next.getId().equals(id)) {
                Record current = getRecord();
                manager.moveInfoUp(current, next);
                if (log.isDebugEnabled()) {
                    log.debug("moveInfoUp() info moved [id=" + id + "]");
                }
                refresh();
                break;
            }
        }
    }

    public LetterType getEmbeddedTextType() {
        return embeddedTextType;
    }

    public void setEmbeddedTextType(LetterType embeddedTextType) {
        this.embeddedTextType = embeddedTextType;
    }

    public Letter getEmbeddedTextAbove() {
        return embeddedTextAbove;
    }

    public void setEmbeddedTextAbove(Letter embeddedTextAbove) {
        this.embeddedTextAbove = embeddedTextAbove;
    }

    public Letter getEmbeddedTextBelow() {
        return embeddedTextBelow;
    }

    public void setEmbeddedTextBelow(Letter embeddedTextBelow) {
        this.embeddedTextBelow = embeddedTextBelow;
    }

    public boolean isProvidingCorrections() {
        return providingCorrections;
    }

    public void setProvidingCorrections(boolean providingCorrections) {
        this.providingCorrections = providingCorrections;
    }

    public void createStatusHistoryList() {
        // overwriting class should implement this if required
    }

    public boolean isConfirmDelete() {
        return confirmDelete;
    }

    public void setConfirmDelete(boolean confirmDelete) {
        this.confirmDelete = confirmDelete;
    }

    public final boolean isCustomHeaderSupported() {
        return isSystemPropertyEnabled(CUSTOM_HEADER_SUPPORT_PROPERTY);
    }

    public List<RecordStatusHistory> getStatusHistory() {
        return getRecordManager().getStatusHistory(getRecord());
    }

    public abstract boolean isProvidingStatusHistory();

    public boolean isExitBlocked() {
        return exitBlocked;
    }

    public void setExitBlocked(boolean exitBlocked) {
        this.exitBlocked = exitBlocked;
    }

    public String getExitBlockedTarget() {
        return exitBlockedTarget;
    }

    public void setExitBlockedTarget(String exitBlockedTarget) {
        this.exitBlockedTarget = exitBlockedTarget;
    }

    public boolean isDisplayDescription() {
        return displayDescription;
    }

    public void changeDisplayDescriptionState() {
        displayDescription = !displayDescription;
    }

    public void loadUnreleased(String target, String exitTarget) {
        List<Record> list = getRecordManager().getUnreleased();
        setList(list);
        setActionTarget(target);
        setExitTarget(exitTarget);
        unreleasedListMode = true;
    }

    public boolean isUnreleasedListMode() {
        return unreleasedListMode;
    }

    public boolean isPriceDisplayEnabled() {
        return priceDisplayEnabled;
    }

    protected void setPriceDisplayEnabled(boolean priceDisplayEnabled) {
        this.priceDisplayEnabled = priceDisplayEnabled;
    }

    public boolean isPriceInputEnabled() {
        return priceInputEnabled;
    }

    protected void setPriceInputEnabled(boolean priceInputEnabled) {
        this.priceInputEnabled = priceInputEnabled;
    }

    // delivery date edit modes

    public boolean isDeliveryDateEditMode() {
        return deliveryDateEditMode;
    }

    public void setDeliveryDateEditMode(boolean deliveryDateEditMode) {
        this.deliveryDateEditMode = deliveryDateEditMode;
    }

    public boolean isItemDeliveryDateEditMode() {
        return itemDeliveryDateEditMode;
    }

    public void setItemDeliveryDateEditMode(boolean itemDeliveryDateEditMode) {
        this.itemDeliveryDateEditMode = itemDeliveryDateEditMode;
    }

    public boolean changeItemDeliveryDateEditMode(Long itemId) {
        if (itemId == null) {
            selectedItem = null;
            itemDeliveryDateEditMode = false;
        } else {
            Record record = getRecord();
            selectedItem = (Item) CollectionUtil.getById(record.getItems(), itemId);
            itemDeliveryDateEditMode = true;
            deliveryDateEditMode = false;
        }
        return itemDeliveryDateEditMode;
    }

    public boolean isStockEditMode() {
        return stockEditMode;
    }

    public void setStockEditMode(boolean stockEditMode) {
        this.stockEditMode = stockEditMode;
    }

    public boolean isKeepViewOnExit() {
        return false;
    }

    /**
     * Indicates if record supports external document uploads disabling internal pdf document with an external document source.
     * @return false by default. Override if implementing view supports this.
     */
    public boolean isDocumentImportable() {
        return false;
    }

    /**
     * Checks that current user has required permissions and other requirements to create pdf version. <br/>
     * Method does nothing by default. Override to implement record specific validation
     * @throws ClientException if not all requirements except user permissions are fullfilled
     * @throws PermissionException if user has not required permissions
     */
    public void checkRelease() throws ClientException, PermissionException {
        // override this if additional permission validation required
    }

    public List<BusinessTemplate> getBusinessTemplates() {
        return businessTemplates;
    }

    /**
     * Provides access to businessTemplates list
     * @param businessTemplates
     */
    protected void setBusinessTemplates(List<BusinessTemplate> businessTemplates) {
        this.businessTemplates = businessTemplates;
    }

    /**
     * Loads business case template documents (e.g. initialize businessTemplates)
     * @param businessCase providing required contextName and branch.id
     */
    protected void loadBusinessTemplates(BusinessCase businessCase) {
        businessTemplates = getBusinessCaseTemplateManager().findTemplates(
                businessCase.getContextName(), businessCase.getBranch().getId());
        if (log.isDebugEnabled()) {
            log.debug("loadBusinessTemplates() done [found=" + businessTemplates.size() + "]");
        }
    }

    /**
     * Provides businessCase document template manager if required
     * @return businessCaseTemplateManager
     */
    protected BusinessCaseTemplateManager getBusinessCaseTemplateManager() {
        return (BusinessCaseTemplateManager) getService(
                BusinessCaseTemplateManager.class.getName());
    }

    // purchase records only

    public boolean isSupplierReferenceNumberEditMode() {
        return supplierReferenceNumberEditMode;
    }

    public void setSupplierReferenceNumberEditMode(boolean supplierReferenceNumberEditMode) {
        this.supplierReferenceNumberEditMode = supplierReferenceNumberEditMode;
    }

    public void updateSupplierReferenceNumber(String supplierReferenceNumber, Date supplierReferenceDate) {
        RecordManager manager = getRecordManager();
        Record record = getRecord();
        if (record instanceof PurchaseRecord) {
            PurchaseRecord pr = (PurchaseRecord) record;
            pr.setSupplierReferenceNumber(supplierReferenceNumber);
            pr.setSupplierReferenceDate(supplierReferenceDate);
            manager.persist(pr);
            supplierReferenceNumberEditMode = false;
        }
    }

    public boolean isSalesIdEditMode() {
        return salesIdEditMode;
    }

    public void setSalesIdEditMode(boolean salesIdEditMode) {
        this.salesIdEditMode = salesIdEditMode;
    }

    public final void updateSalesId(Long salesId) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("updateSalesId() invoked [sales=" + salesId + "]");
        }
        if (!(getRecord() instanceof PurchaseRecord)) {
            throw new UnsupportedOperationException();
        }
        if (isNotSet(salesId)) {
            throw new ClientException(ErrorCode.ID_INVALID);
        }
        SalesSearch salesSearch = getSalesSearch();
        if (!salesSearch.exists(salesId)) {
            throw new ClientException(ErrorCode.ID_INVALID);
        }
        Sales sales = salesSearch.fetchSales(salesId);
        PurchaseRecord record = (PurchaseRecord) getRecord();
        if (checkRecordItemsMatchSalesItems()) {
            checkRecordItemsMatchSalesItems(sales);
        }
        updateSalesReference(sales);
        salesIdEditMode = false;
        if (log.isDebugEnabled()) {
            log.debug("updateSalesId() done [record=" + record.getId()
                    + ", sales=" + salesId + "]");
        }
    }

    public final void updateSalesReference(BusinessCase businessCase) {
        if (!(getRecord() instanceof PurchaseRecord)) {
            throw new UnsupportedOperationException();
        }
        RecordManager manager = getRecordManager();
        PurchaseRecord record = (PurchaseRecord) getRecord();
        record.setSalesReference(businessCase);
        manager.persist(record);
        if (log.isDebugEnabled()) {
            log.debug("updateSalesReference() done [record=" + record.getId()
                    + ", businessCase=" + businessCase.getPrimaryKey() + "]");
        }
    }

    protected boolean checkRecordItemsMatchSalesItems() {
        return false;
    }

    protected void checkRecordItemsMatchSalesItems(Sales sales) throws ClientException {

        SalesSearch salesSearch = getSalesSearch();
        Record order = salesSearch.findOrder(sales);
        Record record = getRecord();

        if (order != null && !order.getId().equals(record.getId())) {
            if (record.getItems().isEmpty()) {
                // empty records cannot match
                throw new ClientException(ErrorCode.RECORD_ITEMS_MISSING);
            }
            for (int i = 0, j = record.getItems().size(); i < j; i++) {
                Item recordItem = (Item) record.getItems().get(i).clone();
                // add all other quantities if record has more than one items of same product
                for (int k = 0, l = record.getItems().size(); k < l; k++) {
                    Item otherRecordItem = record.getItems().get(k);
                    if (recordItem.getProduct().getProductId().equals(otherRecordItem.getProduct().getProductId())
                            && !recordItem.getId().equals(otherRecordItem.getId())) {
                        recordItem.setQuantity(recordItem.getQuantity() + otherRecordItem.getQuantity());
                    }
                }
                boolean match = false;
                double matchingQuantity = 0;
                for (int m = 0, n = order.getItems().size(); m < n; m++) {
                    Item orderItem = order.getItems().get(m);
                    if (orderItem.getProduct().getProductId().equals(recordItem.getProduct().getProductId())) {
                        matchingQuantity = matchingQuantity + orderItem.getQuantity();
                        match = true;
                    }
                }
                if (!match) {
                    throw new ClientException(ErrorCode.PRODUCT_MISSING);
                } else if (recordItem.getQuantity() > matchingQuantity) {
                    throw new ClientException(ErrorCode.QUANTITY_MAX);
                }
            }
        }
    }

    private SalesSearch getSalesSearch() {
        return (SalesSearch) getService(SalesSearch.class.getName());
    }

    // rebate methods

    public void addRebate() {
        performRebateAction(true);
    }

    public void removeRebate() {
        performRebateAction(false);
    }

    private void performRebateAction(boolean activate) {
        Record obj = getRecord();
        if (obj instanceof RebateAwareRecord) {
            RebateAwareRecord record = (RebateAwareRecord) obj;
            RebateManagerProvider provider = (RebateManagerProvider) getService(RebateManagerProvider.class.getName());
            RebateManager manager = provider.getRebateManager(record);
            if (manager != null) {
                if (activate) {
                    manager.rebate(record);
                } else {
                    manager.removeRebates(record);
                }
                setSetupMode(false);
            }
        }
    }

    // local utility methods

    protected void initLists() {
        EmployeeSearch manager = (EmployeeSearch) getService(EmployeeSearch.class.getName());
        List<Option> employeeList = manager.findActiveNames();
        signatureLeftEmployees = employeeList;
        signatureRightEmployees = employeeList;
        allInfos = getRecordInfoConfigManager().getActivated();
        TaxRateManager taxRateManager = (TaxRateManager) getService(TaxRateManager.class.getName());
        taxRates = taxRateManager.getRates();
        reducedTaxRates = taxRateManager.getReducedRates();
        SystemConfigManager configManager = (SystemConfigManager) getService(SystemConfigManager.class.getName());
        selectedStock = configManager.getDefaultStock().getId();
        branchOfficeList = configManager.getActivatedBranchs();
        multipleBranchsAvailable = (branchOfficeList.size() > 1);
    }

    public List<BankAccount> getBankAccounts() {
        List<BankAccount> bankAccounts = new ArrayList<>();
        if (getBean() != null) {
            SystemConfigManager configManager = (SystemConfigManager) getService(SystemConfigManager.class.getName());
            SystemCompany company = configManager.getCompany(getRecord().getCompany());
            bankAccounts = company.getBankAccounts();
        }
        return bankAccounts;
    }

    public List<BranchOffice> getBranchOfficeList() {
        return branchOfficeList;
    }

    protected void setBranchOfficeList(List<BranchOffice> branchOfficeList) {
        this.branchOfficeList = branchOfficeList;
    }

    public boolean isMultipleBranchsAvailable() {
        return multipleBranchsAvailable;
    }

    protected void setMultipleBranchsAvailable(boolean multipleBranchsAvailable) {
        this.multipleBranchsAvailable = multipleBranchsAvailable;
    }

    protected Item fetchSelectedItem(Long itemId) {
        if (getBean() == null) {
            return null;
        }
        return getSelectedItem(getRecord(), itemId);
    }

    private Item getSelectedItem(Record record, Long itemId) {
        for (int i = 0, j = record.getItems().size(); i < j; i++) {
            Item current = record.getItems().get(i);
            if (current.getId().equals(itemId)) {
                return current;
            }
        }
        String warn = "getSelectedItem() not found: " + itemId;
        log.warn(warn);
        throw new ActionException(warn);
    }

    private void initContactPersons(Contact c) {
        if (c != null && c.getType() != null &&
                (Contact.TYPE_BUSINESS.equals(c.getType().getId())
                        || Contact.TYPE_PUBLIC.equals(c.getType().getId()))) {
            ContactSearch search = getContactSearch();
            if (c.getContactId() != null && c.getContactId() != 0) {
                contactPersons = search.findContactPersons(c.getContactId());
            }
        }
    }

    protected void initContactPerson(Record record) {
        if (record.getPersonId() != null && record.getPersonId() != 0) {
            if (log.isDebugEnabled()) {
                log.debug("initContactPerson() contact person id found");
            }
            try {
                contactPerson = getContactSearch().find(
                        record.getPersonId());
                if (log.isDebugEnabled()) {
                    log.debug("initContactPerson() contact person "
                            + contactPerson.getContactId());
                }
            } catch (Exception ignorable) {
            }
        } else {
            contactPerson = null;
        }
    }

    protected void reloadProductSummary(Record record) {
        ProductSummaryManager summaryManager = (ProductSummaryManager) getService(ProductSummaryManager.class.getName());
        for (Iterator<Item> items = record.getItems().iterator(); items.hasNext();) {
            Item nextItem = items.next();
            summaryManager.reloadSummary(nextItem.getProduct());
            if (nextItem.getStockId() != null) {
                nextItem.getProduct().enableStock(nextItem.getStockId());
            }
        }
    }

    /**
     * Provides the responsible manager implementation for the view
     * @return record manager
     */
    public abstract RecordManager getRecordManager();

    protected RecordPrintManager getPrintManager() {
        return (RecordPrintManager) getService(RecordPrintManager.class.getName());
    }

    protected RecordOutputService getRecordOutputService() {
        return (RecordOutputService) getService(RecordOutputService.class.getName());
    }

    private ContactSearch getContactSearch() {
        return (ContactSearch) getService(ContactSearch.class.getName());
    }

    public final Document getRecordDocument(boolean preview) {
        Record current = getRecord();
        RecordOutputService outputService = getRecordOutputService();
        Document doc = outputService.createDocument(getDomainEmployee(), current, preview);
        if (isSet(current.getBusinessCaseId())) {
            BusinessCaseSearch search = (BusinessCaseSearch) getService(BusinessCaseSearch.class.getName());
            BusinessCase bc = search.findBusinessCase(current);
            if (bc != null) {
                outputService.addBusinessCase(doc, bc);
            }
        }
        return doc;
    }

    public final Document getRecordStyleheet() throws ClientException {
        Record current = getRecord();
        RecordOutputService outputService = getRecordOutputService();
        String xsl = outputService.createStylesheetText(getDomainEmployee(), current);
        return JDOMUtil.parseText(xsl);
    }

    protected Document createItemValues(Record record, Item item) {
        Document doc = new Document();
        Element root = new Element("root");
        root.setAttribute("type", "data");
        if (item != null) {
            root.addContent(new Element("id").setText(item.getId().toString()));

            if (item instanceof OrderItem && record instanceof Order) {
                OrderItem orderItem = (OrderItem) item;
                Date delivery = orderItem.getDelivery();
                if (delivery == null && record instanceof Order) {
                    Order order = (Order) record;
                    delivery = order.getDelivery();
                }
                if (delivery == null) {
                    delivery = new Date(System.currentTimeMillis());
                }
                root.addContent(new Element("delivery").setText(
                        DateFormatter.getDate(delivery)));
                root.addContent(new Element("deliveryConfirmed").setText(
                        Boolean.valueOf(orderItem.isDeliveryConfirmed()).toString()));
            } else {
                root.addContent(new Element("delivery"));
                root.addContent(new Element("deliveryConfirmed"));
            }

            root.addContent(new Element("quantity").setText(
                    NumberFormatter.getValue(item.getQuantity(), NumberFormatter.DECIMAL)));
            root.addContent(new Element("price").setText(
                    NumberFormatter.getValue(item.getPrice(), NumberFormatter.CURRENCY)));
            root.addContent(new Element("amount").setText(
                    NumberFormatter.getValue(item.getAmount(), NumberFormatter.CURRENCY)));
            if (record.getAmounts() != null) {
                if (record.isTaxFree()) {
                    root.addContent(new Element("netAmount").setText(
                            NumberFormatter.getValue(record.getAmounts().getAmount(), NumberFormatter.CURRENCY)));
                    root.addContent(new Element("taxAmount").setText("0,00"));
                    root.addContent(new Element("grossAmount").setText(
                            NumberFormatter.getValue(record.getAmounts().getAmount(), NumberFormatter.CURRENCY)));
                } else {
                    root.addContent(new Element("netAmount").setText(
                            NumberFormatter.getValue(record.getAmounts().getAmount(), NumberFormatter.CURRENCY)));
                    root.addContent(new Element("taxAmount").setText(
                            NumberFormatter.getValue(record.getAmounts().getTaxAmount(), NumberFormatter.CURRENCY)));
                    root.addContent(new Element("grossAmount").setText(
                            NumberFormatter.getValue(record.getAmounts().getGrossAmount(), NumberFormatter.CURRENCY)));
                }
            }
        }
        doc.setRootElement(root);
        return doc;
    }

    public boolean isSalesRecordView() {
        return true;
    }

    public boolean isPreviousRequestExit() {
        boolean result = previousRequestExit;
        previousRequestExit = false;
        return result;
    }

    public void previousRequestWasExit() {
        previousRequestExit = true;
    }

    protected Double fetchAbsoluteCurrencyValue(Double value) {
        if (value == null) {
            return 0d;
        }
        Double result = NumberUtil.round(value, 2);
        if (result < 0) {
            return result * (-1);
        }
        return result;
    }

    public Long getSelectedStock() {
        return selectedStock;
    }

    public void setSelectedStock(Long selectedStock) {
        this.selectedStock = selectedStock;
    }

    public void enableEmbeddedText() {
        RecordManager manager = getRecordManager();
        Record record = getRecord();
        if (!record.isPrintCoverLetter()) {
            record.setPrintCoverLetter(true);
            manager.persist(record);
        }
    }

    public void disableEmbeddedText() {
        RecordManager manager = getRecordManager();
        Record record = getRecord();
        if (record.isPrintCoverLetter()) {
            record.setPrintCoverLetter(false);
            manager.persist(record);
        }
    }

    public final DmsDocument getDocument() {
        return document;
    }

    protected final void setDocument(DmsDocument document) {
        this.document = document;
    }

    protected final void loadReferencedDocument(Record record) {
        document = getReferencedDocument(record);
    }

    protected DmsDocument getReferencedDocument(Record record) {
        DmsDocument result = null;
        if (record != null && record.getType() != null && record.getType().getDocumentTypeId() != null) {
            result = getDmsManager().findReferenced(new DmsReference(record.getType().getDocumentTypeId(), record.getId()));
            log.debug("getReferencedDocument() lookup done [found=" + (result != null) + "]");
        } else {
            log.debug("getReferencedDocument() no lookup performed [record="
                    + (record != null ? (record.getId() + ", type=" + (record.getType() != null ? record.getType().getDocumentTypeId() : "null")) : "null")
                    + "]");
        }
        return result;
    }

    protected LetterType fetchEmbeddedTextType(RecordType recordType) {
        if (isSet(recordType.getResourceKey())) {
            LetterType result = getLetterManager().findType(recordType.getResourceKey());
            if (result != null && result.isEmbedded() && !result.isDisabled()) {
                return result;
            }
        }
        return null;
    }

    protected final RecordInfoConfigManager getRecordInfoConfigManager() {
        return (RecordInfoConfigManager) getService(RecordInfoConfigManager.class.getName());
    }

    protected final LetterManager getLetterManager() {
        return (LetterManager) getService(LetterManager.class.getName());
    }

    private final DmsManager getDmsManager() {
        return (DmsManager) getService(DmsManager.class.getName());
    }
}
