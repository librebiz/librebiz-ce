/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 23, 2006 7:27:46 PM 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.sql.Types;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.common.dao.AbstractTablesAwareSpringDao;
import com.osserp.common.dao.Tables;

import com.osserp.core.Options;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.dao.records.RecordInfoConfigs;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RecordInfoConfigsDao extends AbstractTablesAwareSpringDao implements RecordInfoConfigs {
    private static Logger log = LoggerFactory.getLogger(RecordInfoConfigsDao.class.getName());
    private String infoConfigsTable = null;
    private String infoConfigDefaultsTable = null;

    public RecordInfoConfigsDao(JdbcTemplate jdbcTemplate, Tables tables) {
        super(jdbcTemplate, tables);
        infoConfigDefaultsTable = getTable(TableKeys.RECORD_INFO_CONFIG_DEFAULTS);
        infoConfigsTable = getTable(Options.RECORD_INFO_CONFIGS);
    }

    public void create(String text) {
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("INSERT INTO ")
                .append(infoConfigsTable)
                .append(" (name) VALUES (?)");
        jdbcTemplate.update(sql.toString(), new Object[] { text });
    }

    public void update(Long id, String text) {
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("UPDATE ")
                .append(infoConfigsTable)
                .append(" SET name = ? WHERE id = ")
                .append(id);
        jdbcTemplate.update(sql.toString(), new Object[] { text });
    }

    public void toggleEolFlag(Long id) {
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("UPDATE ")
                .append(infoConfigsTable)
                .append(" SET end_of_life = (not end_of_life) WHERE id = ")
                .append(id);
        jdbcTemplate.update(sql.toString());
    }

    public List<Option> getAll() {
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("SELECT id, name, end_of_life FROM ")
                .append(infoConfigsTable)
                .append(" ORDER BY name");
        List<Option> list = (List<Option>) jdbcTemplate.query(
                sql.toString(),
                getOptionRowMapper());
        if (log.isDebugEnabled()) {
            log.debug("getAll() done [count " + list.size() + "]");
        }
        return list;
    }

    public List<Long> getDefaultInfos(Long recordType, Long requestType) {
        if (isNotSet(requestType)) {
            return getDefaultInfos(recordType);
        } 
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("SELECT info_id FROM ")
                .append(infoConfigDefaultsTable)
                .append(" WHERE record_type = ? AND request_type = ? ORDER BY order_id");
        final Object[] params = { recordType, requestType };
        final int[] types = { Types.BIGINT, Types.BIGINT };
        List<Long> list = (List<Long>) jdbcTemplate.query(
                sql.toString(),
                params,
                types,
                getLongRowMapper());
        if (log.isDebugEnabled()) {
            log.debug("getDefaultInfos() done [count=" + list.size()
                    + ", recordType=" + recordType
                    + ", requestType=" + requestType
                    + "]");
        }
        return list;
    }

    public List<Long> getDefaultInfos(Long recordType) {
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("SELECT info_id FROM ")
                .append(infoConfigDefaultsTable)
                .append(" WHERE record_type = ? ORDER BY order_id");
        final Object[] params = { recordType };
        final int[] types = { Types.BIGINT };
        List<Long> list = (List<Long>) jdbcTemplate.query(
                sql.toString(),
                params,
                types,
                getLongRowMapper());
        if (log.isDebugEnabled()) {
            log.debug("getDefaultInfos() done [count=" + list.size()
                    + ", recordType=" + recordType
                    + ", requestType=none]");
        }
        return list;
    }

    public int addDefaultInfo(Long recordType, Long requestType, Long infoId)
            throws ClientException {
        if (alreadyExists(recordType, requestType, infoId)) {
            throw new ClientException(ErrorCode.VALUES_DUPLICATE);
        }
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("INSERT INTO ")
                .append(infoConfigDefaultsTable)
                .append(" (record_type,request_type,info_id) VALUES (?,?,?)");
        final Object[] params = { recordType, requestType, infoId };
        final int[] types = { Types.BIGINT, Types.BIGINT, Types.BIGINT };
        return jdbcTemplate.update(sql.toString(), params, types);
    }

    public int removeDefaultInfo(Long recordType, Long requestType, Long infoId) {
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("DELETE FROM ")
                .append(infoConfigDefaultsTable)
                .append(" WHERE record_type = ? AND request_type = ? AND info_id = ?");
        final Object[] params = { recordType, requestType, infoId };
        final int[] types = { Types.BIGINT, Types.BIGINT, Types.BIGINT };
        return jdbcTemplate.update(sql.toString(), params, types);
    }

    private boolean alreadyExists(Long recordType, Long requestType, Long infoId) {
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("SELECT count(*) FROM ")
                .append(infoConfigDefaultsTable)
                .append(" WHERE record_type = ? AND request_type = ? AND info_id = ?");
        final Object[] params = { recordType, requestType, infoId };
        final int[] types = { Types.BIGINT, Types.BIGINT, Types.BIGINT };
        return (jdbcTemplate.queryForObject(sql.toString(), params, types, Integer.class) > 0);
    }
    
    public void moveInfoUp(Long recordType, Long requestType, Long infoId) {
    	initInfoOrderIfRequired(recordType, requestType);
    	List<Long> infos = getDefaultInfos(recordType, requestType);
    	int itemCount = infos.size();
        if (!(itemCount > 1)) {
            // nothing to do
        	if (log.isDebugEnabled()) {
        		log.debug("moveInfoUp() nothing to do, info count not > 1 [itemCount=" + itemCount + ", providedInfo=" + infoId + "]");
        	}
            return;
        }
        int currentOrder = getActualOrderId(recordType, requestType, infoId);
        if (currentOrder < 1) {
        	if (log.isDebugEnabled()) {
        		log.debug("moveInfoUp() nothing to do, currentOrder < 1 [currentOrder=" + currentOrder + "]");
        	}
            // nothing to do, info is first
            return;
        }
        for (int i = 0, j = itemCount; i < j; i++) {
            Long next = infos.get(i);
            if (next.equals(infoId)) {
            	if (log.isDebugEnabled()) {
            		log.debug("moveInfoUp() found selected [id=" + next + ", actualIndex=" + currentOrder + ", newIndex=" + (currentOrder - 1) + "]");
            	}
            	updateOrderId(recordType, requestType, infoId, currentOrder - 1);
            	
            } else if (getActualOrderId(recordType, requestType, next) == currentOrder - 1) {
            	updateOrderId(recordType, requestType, next, currentOrder);
            	if (log.isDebugEnabled()) {
            		log.debug("moveInfoUp() found previous [id=" + next + ", actualIndex=" + (currentOrder - 1) + ", newIndex=" + currentOrder + "]");
            	}
            }
        }
    }
    
    public void moveInfoDown(Long recordType, Long requestType, Long infoId) {
    	initInfoOrderIfRequired(recordType, requestType);
    	List<Long> infos = getDefaultInfos(recordType, requestType);
        int itemCount = infos.size();
        if (!(itemCount > 1)) {
        	if (log.isDebugEnabled()) {
        		log.debug("moveInfoDown() nothing to do, info count not > 1 [itemCount=" + itemCount + ", providedInfo=" + infoId + "]");
        	}
            // nothing to do  
            return;
        }
        int currentOrder = getActualOrderId(recordType, requestType, infoId);
        if (currentOrder == itemCount) {
        	if (log.isDebugEnabled()) {
        		log.debug("moveInfoDown() nothing to do, currentOrder == last [currentOrder=" + currentOrder + "]");
        	}
            // nothing to do, info is last
            return;
        }
        for (int i = 0, j = itemCount; i < j; i++) {
            Long next = infos.get(i);
            if (next.equals(infoId)) {
            	if (log.isDebugEnabled()) {
            		log.debug("moveInfoDown() found selected [id=" + next + ", actualIndex=" + currentOrder + ", newIndex=" + (currentOrder - 1) + "]");
            	}
            	updateOrderId(recordType, requestType, infoId, currentOrder + 1);
            	
            } else if (getActualOrderId(recordType, requestType, next) == currentOrder + 1) {
            	updateOrderId(recordType, requestType, next, currentOrder);
            	if (log.isDebugEnabled()) {
            		log.debug("moveInfoDown() found previous [id=" + next + ", actualIndex=" + (currentOrder - 1) + ", newIndex=" + currentOrder + "]");
            	}
            }
        }
    }
    
    private void initInfoOrderIfRequired(Long recordType, Long requestType) {
    	if (orderInvalid(recordType, requestType)) {
    		if (log.isDebugEnabled()) {
    			log.debug("initInfoOrderIfRequired() found invalid order [recordType=" 
    				+ recordType + ", requestType=" + requestType + "]");
    		}
    		initOrderIds(recordType, requestType);
    	}
    }

    private int getActualOrderId(Long recordType, Long requestType, Long infoId) {
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("SELECT order_id FROM ")
                .append(infoConfigDefaultsTable)
                .append(" WHERE record_type = ? AND request_type = ? AND info_id = ?");
        final Object[] params = { recordType, requestType, infoId };
        final int[] types = { Types.BIGINT, Types.BIGINT, Types.BIGINT };
        return jdbcTemplate.queryForObject(sql.toString(), params, types, Integer.class);
    }

    private boolean orderInvalid(Long recordType, Long requestType) {
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("SELECT count(*) FROM ")
                .append(infoConfigDefaultsTable)
                .append(" WHERE record_type = ? AND request_type = ? AND order_id = 0");
        final Object[] params = { recordType, requestType };
        final int[] types = { Types.BIGINT, Types.BIGINT };
        int result = jdbcTemplate.queryForObject(
                sql.toString(), params, types, Integer.class);
        if (log.isDebugEnabled()) {
            log.debug("orderInvalid() done [result=" + (result > 1)
                    + ", recordType=" + recordType
                    + ", requestType=" + requestType
                    + "]");
        }
        return (result > 1);
    }
    
    private void initOrderIds(Long recordType, Long requestType) {
    	List<Long> list = getDefaultInfos(recordType, requestType);
    	for (int i = 0, j = list.size(); i < j; i++) {
    		Long next = list.get(i);
    		updateOrderId(recordType, requestType, next, i);
    	}
    }

    private void updateOrderId(Long recordType, Long requestType, Long infoId, int orderId) {
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("UPDATE ")
                .append(infoConfigDefaultsTable)
                .append(" SET order_id = ?")
                .append(" WHERE record_type = ? AND request_type = ? AND info_id = ?");
        final Object[] params = { orderId, recordType, requestType, infoId };
        final int[] types = { Types.INTEGER, Types.BIGINT, Types.BIGINT, Types.BIGINT };
        jdbcTemplate.update(sql.toString(), params, types);
    }
}
