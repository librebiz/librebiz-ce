/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 8, 2006 12:01:52 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.common.OptionsCache;
import com.osserp.common.User;
import com.osserp.common.dao.Tables;

import com.osserp.core.BusinessCase;
import com.osserp.core.BusinessCaseSearchRequest;
import com.osserp.core.BusinessType;
import com.osserp.core.FcsAction;
import com.osserp.core.FcsItem;
import com.osserp.core.Options;
import com.osserp.core.crm.Campaign;
import com.osserp.core.crm.CampaignAssignmentHistory;
import com.osserp.core.customers.Customer;
import com.osserp.core.customers.CustomerSummary;
import com.osserp.core.customers.SalesSummary;
import com.osserp.core.dao.BranchOffices;
import com.osserp.core.dao.Contacts;
import com.osserp.core.dao.RequestFcsActions;
import com.osserp.core.dao.Requests;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.employees.Employee;
import com.osserp.core.model.ContactRequestImpl;
import com.osserp.core.model.RequestImpl;
import com.osserp.core.model.RequestTypeImpl;
import com.osserp.core.model.SalesRequestImpl;
import com.osserp.core.model.crm.CampaignAssignmentHistoryImpl;
import com.osserp.core.model.crm.CampaignImpl;
import com.osserp.core.model.projects.ProjectRequestImpl;
import com.osserp.core.requests.Request;
import com.osserp.core.requests.RequestListItem;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.tickets.RequestResetWarningTicket;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RequestsDao extends AbstractBusinessCaseDao implements Requests {
    private static Logger log = LoggerFactory.getLogger(RequestsDao.class.getName());
    private String projectsTable = null;
    private String requestsTable = null;
    private String requestsTypesTable = null;
    private String campaignAssignmentHistoryTable = null;
    private String requestResetWarningTasksTable = null;
    private String requestSearchView = null;
    private String salesRecordIdsView = null;
    private BranchOffices branchOffices = null;
    private RequestFcsActions requestFcsActions = null;

    public RequestsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            Contacts contacts,
            OptionsCache optionsCache,
            BranchOffices branchOffices,
            RequestFcsActions requestFcsActions,
            String requestResetWarningTasks) {
        super(jdbcTemplate, tables, sessionFactory, contacts, optionsCache);
        this.campaignAssignmentHistoryTable = getTable(TableKeys.CAMPAIGN_ASSIGNMENT_HISTORY);
        this.projectsTable = getTable(TableKeys.PROJECTS);
        this.requestsTable = getTable(TableKeys.PLANS);
        this.requestsTypesTable = getTable(Options.REQUEST_TYPES);
        this.requestResetWarningTasksTable = getTable(requestResetWarningTasks);
        this.requestSearchView = getTable(TableKeys.REQUEST_SEARCH_VIEW);
        this.salesRecordIdsView = getTable(TableKeys.SALES_RECORD_IDS_VIEW);
        this.requestFcsActions = requestFcsActions;
        this.branchOffices = branchOffices;
    }

    public Request createTransient(User user, Customer customer, BusinessType type) {
        RequestImpl request = null;
        if (type == null) {
            throw new IllegalArgumentException("businessType must not be null");
        }
        if (type.isInterestContext()) {
            if (log.isDebugEnabled()) {
                log.debug("createEmpty() initializing project contact request [type=" + type.getId() + "]");
            }
            request = new ContactRequestImpl(user, type, customer);
        } else if (type.isProjectRequest()) {
            if (log.isDebugEnabled()) {
                log.debug("createEmpty() initializing project request [type=" + type.getId() + "]");
            }
            request = new ProjectRequestImpl(user, type, customer);
        } else {
            if (log.isDebugEnabled()) {
                log.debug("createEmpty() initializing sales request [type=" + type.getId() + "]");
            }
            request = new SalesRequestImpl(user, type, customer);
        }
        return (Request) initializeBranch(request, true);
    }
    
    @Override
    protected BusinessCase initializeBranch(BusinessCase businessCase, boolean force) {
        Request request = (Request) businessCase;
        BusinessType type = request.getType();
        if (type.getDefaultBranch() != null && (force || request.getBranch() == null)) {
            if (type.isForceDefaultBranch()) {
                request.updateBranch(branchOffices.getBranchOffice(type.getDefaultBranch()));
            } else if (request.getBranch() == null) {
                request.updateBranch(branchOffices.getBranchOffice(type.getDefaultBranch()));
            }
        }
        return request;
    }

    public BusinessCase createBusinessCase(
            Long businessId, 
            String externalReference,
            String externalUrl,
            Date createdDate, 
            BusinessType businessType, 
            BranchOffice branchOffice,
            Customer customer, 
            Long salesPersonId, 
            Long projectManagerId, 
            String name,
            String street,
            String streetAddon,
            String zipcode, 
            String city, 
            Long countryId, 
            Campaign campaign,
            Option origin) {
        
        RequestImpl request = createRequestImpl(
                businessId, 
                externalReference,
                externalUrl,
                createdDate, 
                businessType, 
                branchOffice, 
                customer, 
                salesPersonId, 
                projectManagerId, 
                name, 
                street, 
                streetAddon, 
                zipcode, 
                city, 
                countryId, 
                campaign, 
                origin);
        request = (RequestImpl) initializeBranch(request, false);
        getCurrentSession().saveOrUpdate(RequestImpl.class.getName(), request);
        if (log.isDebugEnabled()) {
            log.debug("createBusinessCase() done [id=" + request.getPrimaryKey()
                    + ", type=" + businessType.getId()
                    + ", class=" + request.getClass().getName()
                    + "]");
        }
        return request;
    }

    public boolean exists(Long requestId) {
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("SELECT count(*) FROM ")
                .append(requestsTable)
                .append(" WHERE plan_id = ?");
        final Object[] params = { requestId };
        final int[] types = { Types.BIGINT };
        int cnt = jdbcTemplate.queryForObject(sql.toString(), params, types, Integer.class);
        return (cnt > 0);
    }

    public List<RequestListItem> find(BusinessCaseSearchRequest request) {
        StringBuilder buffer = new StringBuilder("SELECT DISTINCT ON (plan_id) * FROM ");
        buffer.append(requestSearchView).append(" WHERE status_id < 100");
        if (!isEmpty(request.getBusinessTypeId())) {
            buffer.append(" AND type_id = ").append(request.getBusinessTypeId());
        }
        if (!isEmpty(request.getBranchId())) {
            buffer.append(" AND branch_id = ").append(request.getBranchId());
        }
        if (!request.isIncludeCanceled()) {
            buffer.append(" AND status_id >= 0");
        }
        if (!isEmpty(request.getPattern())) {
            buffer.append(" AND ("); 
            buffer.append(bindColumnToRegExPattern(
                (isEmpty(request.getColumnKey()) ? "idx" : request.getColumnKey()), 
                request.getPattern(), 
                request.isStartsWith()));
        
            Long id = request.getPatternAsNumber();
            if (isEmpty(id)) {
                buffer.append(")");
            } else {
                buffer
                    .append(" OR plan_id IN (SELECT sales_id FROM ")
                    .append(salesRecordIdsView)
                    .append(" WHERE id::varchar ~* '")
                    .append(fetchInsecureInput(request.getPattern())).append("'))");
            }
        } else {
            buffer.append(" ORDER BY plan_id DESC");
        }
        if (!request.isIgnoreFetchSize() && request.getFetchSize() > 0) {
            buffer.append(" LIMIT ").append(request.getFetchSize());
        }
        String queryString = buffer.toString();
        List<RequestListItem> result = (List<RequestListItem>) jdbcTemplate.query(
                queryString, RequestListRowMapper.getReader(getOptionsCache()));
        if (log.isDebugEnabled()) {
            log.debug("find() done [sql=" + queryString + ", found=" + result.size() + "]");
        }
        return result;
    }

    public BusinessCase findById(Long businessId) {
        if (exists(businessId)) {
            try {
                return load(businessId);
            } catch (Exception e) {
                log.warn("findById() caught exception loading request by an id reported as existing"
                        + " [id=" + businessId + ", message=" + e.getMessage() + "]", e);
            }
        }
        return null;
    }

    public BusinessCase findByExternalReference(String externalReference) {
        BusinessCase object = null;
        if (!isEmpty(externalReference)) {
            final Object[] params = { externalReference };
            final int[] types = { Types.VARCHAR };
            StringBuilder sql = new StringBuilder(96);
            sql
                .append("SELECT plan_id FROM ")
                .append(requestsTable)
                .append(" WHERE external_ref = ?");
            List<Long> result = (List<Long>) jdbcTemplate.query(
                sql.toString(), 
                params,
                types,
                getLongRowMapper());
            object = result.isEmpty() ? null : load(result.get(0));
            if (log.isDebugEnabled() && object != null) {
                log.debug("findByExternalReference() object retrieved [businessId="
                        + object.getPrimaryKey() + ", externalReference="
                        + externalReference + "]");
            }
        }
        return object;
    }

    public Request load(Long id) {
        return (Request) getCurrentSession().load(RequestImpl.class, id);
    }

    public Request changeBranch(Request request, Long branchId) {
        if (request != null && branchId != null) {
            BranchOffice office = branchOffices.getBranchOffice(branchId);
            request.updateBranch(office);
            getCurrentSession().saveOrUpdate(RequestImpl.class.getName(), request);
        }
        return request;
    }
    
    public Request changeType(Long user, Request request, BusinessType type) {
        if (log.isDebugEnabled()) {
            log.debug("changeType() invoked [id=" + request.getPrimaryKey() 
                    + " ,src=" + request.getType().getId()
                    + ", target=" + type.getId() + "]");
        }
        Request transfered = null;
        if (type.isInterestContext()) {
            transfered = new ContactRequestImpl(user, request, type);
        } else if (type.isProjectRequest()) {
            transfered = new ProjectRequestImpl(user, request, type);
        } else {
            transfered = new SalesRequestImpl(user, request, type);
        }
        save(transfered);
        if (log.isDebugEnabled()) {
            log.debug("changeType() done [id=" + request.getPrimaryKey()
                    + ", type=" + transfered.getTypeId() + "]");
        }
        return transfered;
    }

    public Request changeRequestId(Long user, Request request, Long requestId) {
        final int[] types = { Types.BIGINT, Types.BIGINT };
        final Long tempId = System.currentTimeMillis();
        // create temp request
        jdbcTemplate.update("INSERT INTO project_plans (plan_id) VALUES (?)", tempId);
        final Object[] tempAndOld = { tempId, request.getPrimaryKey() };
        // change current reference ids to tempId
        jdbcTemplate.update("UPDATE campaign_assignment_history SET reference_id = ? WHERE reference_id = ?", tempAndOld, types);
        jdbcTemplate.update("UPDATE project_contracts SET reference_id = ? WHERE reference_id = ?", tempAndOld, types);
        jdbcTemplate.update("UPDATE project_installation_dates SET plan_id = ? WHERE plan_id = ?", tempAndOld, types);
        jdbcTemplate.update("UPDATE project_trackings SET reference_id = ? WHERE reference_id = ?", tempAndOld, types);
        jdbcTemplate.update("UPDATE request_fcs SET reference_id = ? WHERE reference_id = ?", tempAndOld, types);
        jdbcTemplate.update("UPDATE request_fcs_wastebaskets SET project_id = ? WHERE project_id = ?", tempAndOld, types);
        jdbcTemplate.update("UPDATE request_reset_warning_tasks SET reference_id = ? WHERE reference_id = ?", tempAndOld, types);
        jdbcTemplate.update("UPDATE sales_offers SET reference_id = ? WHERE reference_id = ?", tempAndOld, types);
        jdbcTemplate.update("UPDATE sales_offers SET sales_id = ? WHERE sales_id = ?", tempAndOld, types);
        jdbcTemplate.update("UPDATE business_notes SET reference_id = ? WHERE type_id = 3 AND reference_id = ?", tempAndOld, types);
        jdbcTemplate.update("UPDATE business_properties SET reference_id = ? WHERE class_disc = 'businessCase' AND reference_id = ?", tempAndOld, types);
        jdbcTemplate.update("UPDATE calculations SET reference_id = ? WHERE context = 'request' AND reference_id = ?", tempAndOld, types);
        jdbcTemplate.update("UPDATE documents SET reference_id = ? WHERE type_id IN (11,12,3) AND reference_id = ?", tempAndOld, types);
        jdbcTemplate.update("UPDATE events SET reference_id = ? WHERE event_config_id IN (4,2,9) AND reference_id = ?", tempAndOld, types);
        jdbcTemplate.update("UPDATE letters SET business_id = ? WHERE type_id = 2 AND business_id = ?", tempAndOld, types);
        // Change existing request id to new  
        Object[] newAndOld = { requestId, request.getPrimaryKey() };
        jdbcTemplate.update("UPDATE project_plans SET plan_id = ? WHERE plan_id = ?", newAndOld, types);
        Object[] newAndTemp = { requestId, tempId };
        jdbcTemplate.update("UPDATE campaign_assignment_history SET reference_id = ? WHERE reference_id = ?", newAndTemp, types);
        jdbcTemplate.update("UPDATE project_contracts SET reference_id = ? WHERE reference_id = ?", newAndTemp, types);
        jdbcTemplate.update("UPDATE project_installation_dates SET plan_id = ? WHERE plan_id = ?", newAndTemp, types);
        jdbcTemplate.update("UPDATE project_trackings SET reference_id = ? WHERE reference_id = ?", newAndTemp, types);
        jdbcTemplate.update("UPDATE request_fcs SET reference_id = ? WHERE reference_id = ?", newAndTemp, types);
        jdbcTemplate.update("UPDATE request_fcs_wastebaskets SET project_id = ? WHERE project_id = ?", newAndTemp, types);
        jdbcTemplate.update("UPDATE request_reset_warning_tasks SET reference_id = ? WHERE reference_id = ?", newAndTemp, types);
        jdbcTemplate.update("UPDATE sales_offers SET reference_id = ? WHERE reference_id = ?", newAndTemp, types);
        jdbcTemplate.update("UPDATE sales_offers SET sales_id = ? WHERE sales_id = ?", newAndTemp, types);
        jdbcTemplate.update("UPDATE business_notes SET reference_id = ? WHERE type_id = 3 AND reference_id = ?", newAndTemp, types);
        jdbcTemplate.update("UPDATE business_properties SET reference_id = ? WHERE class_disc = 'businessCase' AND reference_id = ?", newAndTemp, types);
        jdbcTemplate.update("UPDATE calculations SET reference_id = ? WHERE context = 'request' AND reference_id = ?", newAndTemp, types);
        jdbcTemplate.update("UPDATE documents SET reference_id = ? WHERE type_id IN (11,12,3) AND reference_id = ?", newAndTemp, types);
        jdbcTemplate.update("UPDATE events SET reference_id = ? WHERE event_config_id IN (4,2,9) AND reference_id = ?", newAndTemp, types);
        jdbcTemplate.update("UPDATE letters SET business_id = ? WHERE type_id = 2 AND business_id = ?", newAndTemp, types);
        // remove temp request
        jdbcTemplate.update("DELETE FROM project_plans WHERE plan_id = ?", tempId);
        // load and return changed request by id
        Request result = load(requestId);
        result.setChangedBy(user);
        result.setChanged(new Date(System.currentTimeMillis()));
        save(result);
        if (log.isDebugEnabled()) {
            log.debug("changeRequestId() done [result=" + result.getPrimaryKey() + "]");
        }
        return result;
    }

    @Override
    protected List<FcsAction> findActions(Long businessType) {
        return requestFcsActions.findByType(businessType);
    }
    
    @Override
    protected final void persist(BusinessCase businessCase) {
        if (isEmpty(businessCase.getPrimaryKey()) && businessCase instanceof RequestImpl) {
            ((RequestImpl) businessCase).setId(getNextRequestId(businessCase.getType()));
        }
        getCurrentSession().saveOrUpdate(RequestImpl.class.getName(), businessCase);
    }

    public void save(Request request) {
        if (log.isDebugEnabled()) {
            log.debug("save() invoked [class=" + request.getClass().getName() + "]");
        }
        if (request.getTypeId() == null) {
            log.warn("save() saving request " + request.getPrimaryKey() + " with unset typeId");
        }
        persist(request);
        doAfterSave(request);
    }
    
    protected void doAfterSave(Request request) {
        // default implementation does nothing
    }

    public void save(FcsItem item) {
        getCurrentSession().saveOrUpdate("RequestFcsItemImpl", item);
    }

    public Long findIdByOffer(Long offerId) {
        final Object[] params = { offerId };
        final int[] types = { Types.BIGINT };
        List<Long> list = (List<Long>) jdbcTemplate.query(
                Procs.GET_REQUEST_ID_BY_OFFER, params, types, getLongRowMapper());
        return (list.isEmpty() ? null : list.get(0));
    }

    public List<Option> getRequestTypeKeys() {
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("SELECT id, shortkey AS name, end_of_life FROM ")
                .append(requestsTypesTable)
                .append(" ORDER BY shortkey");
        return (List<Option>) jdbcTemplate.query(sql.toString(), getOptionRowMapper());
    }

    public List<BusinessType> getRequestTypes() {
        return getCurrentSession().createQuery(
                "from RequestTypeImpl o order by o.name").list();
    }

    public BusinessType getRequestType(Long id) {
        return (BusinessType) getCurrentSession().load(RequestTypeImpl.class, id);
    }

    public BusinessType getRequestTypeByRequest(Long requestId) {
        StringBuilder sql = new StringBuilder(64);
        sql
                .append("SELECT type_id FROM ")
                .append(requestsTable)
                .append(" WHERE plan_id = ?");
        final Object[] params = { requestId };
        final int[] types = { Types.BIGINT };
        List<Long> list = (List<Long>) jdbcTemplate.query(
                sql.toString(), params, types, getLongRowMapper());
        return (empty(list) ? null : getRequestType(list.get(0)));
    }

    public List<BusinessType> getRequestTypesByUsage(boolean canceled) {
        List<BusinessType> result = new ArrayList<>();
        StringBuilder sql = new StringBuilder(64);
        sql.append("SELECT distinct type_id FROM ").append(requestsTable);
        if (canceled) {
            sql.append(" WHERE status_id < 0");
        } else {
            sql.append(" WHERE status_id >= 0");
        }
        sql
            .append(" AND type_id NOT IN (SELECT id FROM ")
            .append(requestsTypesTable)
            .append(" WHERE direct_sales = true)");
        List<Long> list = (List<Long>) jdbcTemplate.query(
                sql.toString(), getLongRowMapper());
        if (!empty(list)) {
            for (int i = 0, j = list.size(); i < j; i++) {
                Long id = list.get(i);
                result.add((BusinessType) getOption(Options.REQUEST_TYPES, id));
            }
        }
        return result;
    }

    public void addSummary(CustomerSummary summary) {
        if (summary.getId() == null) {
            throw new BackendException(ErrorCode.APPLICATION_CONFIG);
        }
        //		plan_id,type_id,sales_id,manager_id,name,created";
        StringBuilder sql = new StringBuilder(64);
        sql
                .append(RequestSummaryRowMapper.SELECT_FROM)
                .append(requestsTable)
                .append(" WHERE customer_id = ? AND plan_id NOT IN " +
                        "(SELECT plan_id FROM ")
                .append(projectsTable)
                .append(") ORDER BY plan_id DESC");
        final Object[] params = { summary.getId() };
        final int[] types = { Types.BIGINT };
        List<SalesSummary> list = (List<SalesSummary>) jdbcTemplate.query(
                sql.toString(),
                params,
                types,
                RequestSummaryRowMapper.getReader(getOptionsCache()));
        for (int i = 0, j = list.size(); i < j; i++) {
            SalesSummary ps = list.get(i);
            summary.addRequest(ps);
            if (log.isDebugEnabled()) {
                log.debug("addSummary() added request [id=" + ps.getId() 
                        + ", cancelled=" + ps.isCancelled() 
                        + ", closed=" + ps.isClosed()
                        + ", status=" + ps.getStatus()
                        + ", created=" + ps.getCreated()
                        + "]");
            }
        }
    }

    public BusinessCase update(
            BusinessCase businessCase, 
            BranchOffice office, 
            Long salesPersonId, 
            Long projectManagerId, 
            String name, 
            String street,
            String streetAddon, 
            String zipcode, 
            String city, 
            Long country, 
            Campaign campaign, 
            Option origin) {
        Request request = load(businessCase.getPrimaryKey());
        Long oldCampaign = request.getOrigin();
        request.update(
                office, 
                salesPersonId, 
                projectManagerId, 
                name, 
                street, 
                streetAddon, 
                zipcode, 
                city, 
                country, 
                campaign, 
                origin);
        save(request);
        if (!isEmpty(oldCampaign) && campaign != null && !campaign.getId().equals(oldCampaign)) {
            // TODO add history entry
        }
        return request;
    }

    public CampaignAssignmentHistory updateOrigin(Request request, Employee user, Long origin) {
        if (request != null && origin != null) {
            Campaign campaign = (Campaign) getCurrentSession().load(CampaignImpl.class.getName(), origin);
            if (campaign != null) {
                Campaign previous = request.getOrigin() == null ? null : (Campaign) getCurrentSession().load(CampaignImpl.class.getName(),
                        request.getOrigin());
                CampaignAssignmentHistoryImpl hist = new CampaignAssignmentHistoryImpl(user, request, campaign, previous);
                request.setOrigin(origin);
                getCurrentSession().saveOrUpdate(RequestImpl.class.getName(), request);
                try {
                    getCurrentSession().saveOrUpdate(CampaignAssignmentHistoryImpl.class.getName(), hist);
                } catch (Exception e) {
                    log.warn("updateOrigin() failed on attempt to create campaign assignment history [request="
                            + request.getRequestId()
                            + ", origin=" + origin
                            + ", message=" + e.getMessage()
                            + "]", e);
                }
                return hist;
            }
        }
        return null;
    }

    public boolean isCampaignAssignmentHistoryAvailable(Request request) {
        return (jdbcTemplate.queryForObject("SELECT count(*) FROM " + campaignAssignmentHistoryTable + " WHERE reference_id = "
                + request.getRequestId(), Long.class) > 0);
    }

    public List<CampaignAssignmentHistory> getCampaignAssignmentHistory(Request request) {
        List<CampaignAssignmentHistory> history = getCurrentSession().createQuery(
                "from " + CampaignAssignmentHistoryImpl.class.getName() 
                + " history where history.reference = :requestId order by history.id desc")
                .setParameter("requestId", request.getRequestId()).list();
        return history;
    }

    // invalid (incomplete) request reset handling

    public void executeResetWarningTasks() {
        int cnt = jdbcTemplate.queryForObject(Procs.EXECUTE_REQUEST_RESET_WARNING_TASKS, Integer.class);
        if (cnt > 0 && log.isDebugEnabled()) {
            log.debug("executeResetWarningTasks() done [" + cnt + "]");
        }
    }

    public List<RequestResetWarningTicket> getOpenRequestResetWarnings() {
        StringBuilder sql = new StringBuilder(RequestResetWarningTicketRowMapper.SELECT_ALL);
        sql
                .append(requestResetWarningTasksTable)
                .append(" w, ")
                .append(requestsTable)
                .append(" r WHERE w.reference_id = r.plan_id AND w.warning_sent IS NULL");
        List<RequestResetWarningTicket> list = (List<RequestResetWarningTicket>) jdbcTemplate.query(
                sql.toString(),
                RequestResetWarningTicketRowMapper.getReader());
        return list;
    }

    public void closeOpenRequestResetWarning(RequestResetWarningTicket ticket) {
        StringBuilder sql = new StringBuilder("UPDATE ");
        sql
                .append(requestResetWarningTasksTable)
                .append(" SET warning_sent = now() WHERE id = ")
                .append(ticket.getId());
        jdbcTemplate.update(sql.toString());
    }

    // local helpers

    protected List<Request> load(List<Long> ids) {
        List<Request> result = new ArrayList<Request>();
        for (int i = 0, j = ids.size(); i < j; i++) {
            result.add(load(ids.get(i)));
        }
        return result;
    }
}
