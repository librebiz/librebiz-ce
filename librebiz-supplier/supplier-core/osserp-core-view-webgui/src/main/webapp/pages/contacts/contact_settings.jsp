<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.contactSettingsView}">
    <c:redirect url="/errors/error_context.jsp" />
</c:if>
<c:set var="view" value="${sessionScope.contactSettingsView}" />
<c:set var="contact" value="${view.bean}" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="headline">
        <fmt:message key="settingsTo" />
        <o:out value="${contact.displayName}" />
    </tiles:put>
    <tiles:put name="headline_right">
        <ul>
            <li><a href="<c:url value="/contacts.do?method=exitSettings"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon" /></a></li>
            <li><v:link url="/index" title="backToMenu"><o:img name="homeIcon" /></v:link></li>
        </ul>
    </tiles:put>
    <tiles:put name="content" type="string">
        <div class="content-area" id="contactSettingsContent">
            <div class="row">

                <div class="col-md-6 panel-area panel-area-default">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>
                                <fmt:message key="salutation" />
                            </h4>
                        </div>
                    </div>
                    <div class="panel-body">

                        <div class="row next">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="addressField">
                                        <o:permission role="executive,office_management,customer_service" info="permissionContactEditAddressField">
                                            <o:ajaxLink linkId="addressField" targetElement="contact_user_salutations_popup" url="/contacts.do?method=forwardSalutationConfig">
                                                <fmt:message key="addressField" />
                                            </o:ajaxLink>
                                        </o:permission> <o:forbidden role="executive,office_management,customer_service">
                                            <fmt:message key="addressField" />
                                        </o:forbidden>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <c:choose>
                                        <c:when test="${empty contact.userNameAddressField}">
                                            <fmt:message key="standard" />
                                        </c:when>
                                        <c:otherwise>
                                            <o:out value="${contact.userNameAddressField}" />
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </div>
                        </div>

                        <div class="row next">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="manuallySchedulable">
                                        <o:permission role="executive,office_management,customer_service" info="permissionContactEditSalutation">
                                            <o:ajaxLink linkId="salutation" targetElement="contact_user_salutations_popup" url="/contacts.do?method=forwardSalutationConfig">
                                                <fmt:message key="salutation" />
                                            </o:ajaxLink>
                                        </o:permission> <o:forbidden role="executive,office_management,customer_service">
                                            <fmt:message key="salutation" />
                                        </o:forbidden>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <c:choose>
                                        <c:when test="${empty contact.userSalutation}">
                                            <fmt:message key="standard" />
                                        </c:when>
                                        <c:otherwise>
                                            <o:out value="${contact.userSalutation}" />
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <c:if test="${!contact.client && !contact.branchOffice}">

                    <div class="col-md-6 panel-area panel-area-default">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <fmt:message key="actions" />
                                </h4>
                            </div>
                        </div>
                        <div class="panel-body">

                            <div class="row next">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="contactDelete"> <c:choose>
                                                <c:when test="${view.contactDeletePermissionGrant}">
                                                    <a href="javascript:onclick=confirmLink('<fmt:message key="confirmation"/>:\n<fmt:message key="confirmContactDelete"/>','<c:url value="/contacts.do?method=delete"/>');" title="<fmt:message key="contactDelete"/>">
                                                        <fmt:message key="contactDelete" />
                                                    </a>
                                                </c:when>
                                                <c:otherwise>
                                                    <fmt:message key="contactDelete" />
                                                </c:otherwise>
                                            </c:choose>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <fmt:message key="contactDeleteHint" />
                                    </div>
                                </div>
                            </div>

                            <c:if test="${contact.customer && contact.business}">
                                <o:permission role="client_create,executive_saas" info="clientCreate">
                                    <div class="row next">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="contactDelete"> <a href="<v:url value="/company/clientCreate/forward?exit=/contactDisplay.do"/>"><fmt:message key="clientCreate" /></a>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <fmt:message key="clientCreateHint" />
                                            </div>
                                        </div>
                                    </div>
                                </o:permission>
                                <c:if test="${!contact.branchOffice}">
                                    <o:permission role="branch_create,executive_saas" info="branchCreate">
                                        <div class="row next">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="branchCreate"> <a href="<v:url value="/company/branchCreate/forward?exit=/contactDisplay.do"/>"><fmt:message key="branchCreate" /></a>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <fmt:message key="branchCreateHint" />
                                                </div>
                                            </div>
                                        </div>
                                    </o:permission>
                                </c:if>
                            </c:if>

                        </div>
                    </div>
                </c:if>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
