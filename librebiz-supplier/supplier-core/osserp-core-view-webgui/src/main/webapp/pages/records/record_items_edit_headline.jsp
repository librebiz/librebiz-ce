<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<c:set var="view" value="${sessionScope.recordView}"/>
<c:set var="record" value="${view.record}"/>
<span class="changeLabel"><fmt:message key="changeLabel"/>:</span>
<c:choose><c:when test="${!empty record.bookingType}"><span><o:out value="${record.bookingType.name}"/></span></c:when><c:otherwise><span><o:out value="${record.type.name}"/></span></c:otherwise></c:choose>
<span> - <o:out value="${record.number}"/> - <o:out value="${record.contact.displayName}"/></span>
