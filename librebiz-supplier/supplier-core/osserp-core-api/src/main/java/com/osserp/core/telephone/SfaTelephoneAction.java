/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Dec 17, 2010 2:48:27 PM 
 * 
 */
package com.osserp.core.telephone;

import com.osserp.common.Entity;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public interface SfaTelephoneAction extends Entity {

    /**
     * Returns the action
     * @return action
     */
    String getAction();

    /**
     * Sets the action
     * @param action
     */
    void setAction(String action);

    /**
     * Returns the macaddress
     * @return macaddress
     */
    String getMacaddress();

    /**
     * Sets the macaddress
     * @param macaddress
     */
    void setMacaddress(String macaddress);

    /**
     * Returns the telephoneSystemId
     * @return telephoneSystemId
     */
    Long getTelephoneSystemId();

    /**
     * Sets the telephoneSystemId
     * @param telephoneSystemId
     */
    void setTelephoneSystemId(Long telephoneSystemId);
}
