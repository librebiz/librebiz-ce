/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 12, 2016 
 * 
 */
package com.osserp.common.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.EntityRelation;
import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionError;
import com.osserp.common.dms.DocumentType;
import com.osserp.common.dms.dao.DocumentEncoder;
import com.osserp.common.dms.dao.DocumentEncoderImpl;
import com.osserp.common.service.AuthenticationProvider;
import com.osserp.common.service.SecurityService;
import com.osserp.common.util.FileUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractSecurityService extends AbstractService implements SecurityService {
    private static Logger log = LoggerFactory.getLogger(AbstractSecurityService.class.getName());
    
    private AuthenticationProvider authenticationProvider = null;
    private DocumentEncoder documentEncoder = null;
    
    protected AbstractSecurityService() {
        super();
        documentEncoder = new DocumentEncoderImpl();
    }
    
    protected AbstractSecurityService(AuthenticationProvider authenticationProvider) {
        this();
        this.authenticationProvider = authenticationProvider;
    }

    protected final void write(String filename, byte[] plainObject, String password) throws ClientException {
        if (isNotSet(filename) || isNotSet(password) || plainObject == null) {
            throw new IllegalStateException(
                    "missing filename, password and/or binary object");
        }
        File enc = new File(filename);
        byte[] ef = documentEncoder.encode(plainObject, DocumentType.FORMAT_CRYPT, password);
        FileUtil.persist(ef, enc.getAbsolutePath());
    }

    protected final byte[] read(String filename, String password) throws ClientException {
        if (isNotSet(filename) || isNotSet(password)) {
            throw new IllegalStateException("missing filename and/or password");
        }
        File dec = new File(filename);
        byte[] enc = FileUtil.getBytes(dec);
        return documentEncoder.decode(enc, DocumentType.FORMAT_CRYPT, password);
    }
    
    protected final String getMasterPassword() {
        AuthenticationProvider p = getAuthenticationProvider();
        if (p == null || p.getAuthenticationToken() == null) {
            throw new PermissionError(ErrorCode.PASSWORD_MISSING);
        }
        if (p.getAuthenticationToken().getToken() instanceof String) {
            return (String) p.getAuthenticationToken().getToken();
        }
        throw new PermissionError(ErrorCode.SETUP_REQUIRED);
    }

    protected String getPassword() throws ClientException {
        try {
            return getMasterPassword();
        } catch (Exception e) {
            log.warn("getPassword() reports false by exception [message="
                    + e.getMessage() + ", class=" + e.getClass().getName() + "]");
            throw new ClientException(e.getMessage());
        }
    }
    
    public boolean isConfigured() {
        try {
            String mp = getMasterPassword();
            return mp != null && !Constants.DEFAULT_DMS_AUTH_SECRET.equals(mp);
        } catch (Exception e) {
            log.warn("isConfigured() reports false by exception [message="
                    + e.getMessage() + ", class=" + e.getClass().getName() + "]");
            return false;
        }
    }
    
    public List<EntityRelation> textAvailable(String contextName, List<EntityRelation> references) {
        List<EntityRelation> withText = new ArrayList<EntityRelation>();
        if (contextName != null) {
            for (int i = 0, j = references.size(); i < j; i++) {
                EntityRelation next = references.get(i);
                try {
                    String key = readText(next, contextName);
                    if (isSet(key)) {
                        withText.add(next);
                    }
                } catch (Exception ignoreable) {
                    // do nothing here
                }
            }
        }
        return withText;
    }

    public void copyText(String contextName, EntityRelation source, EntityRelation target) {
        String text = readText(source, contextName);
        if (text != null) {
            writeText(target, contextName, text);
        }
    }

    public AuthenticationProvider getAuthenticationProvider() {
        return authenticationProvider;
    }

    public void setAuthenticationProvider(AuthenticationProvider authenticationProvider) {
        this.authenticationProvider = authenticationProvider;
    }
}
