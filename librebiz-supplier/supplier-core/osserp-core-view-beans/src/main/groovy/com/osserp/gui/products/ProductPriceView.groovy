/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 09-Jul-2010 
 * 
 */
package com.osserp.gui.products

import com.osserp.common.ActionException
import com.osserp.common.ClientException
import com.osserp.common.Constants
import com.osserp.common.ErrorCode
import com.osserp.common.PermissionException
import com.osserp.core.products.Product
import com.osserp.core.products.ProductManager
import com.osserp.core.products.ProductPriceManager
import com.osserp.core.products.ProductSalesPrice
import com.osserp.core.employees.Employee
import com.osserp.core.purchasing.PurchaseInvoiceManager
import com.osserp.core.system.BranchOffice
import com.osserp.core.system.BranchOfficeManager

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class ProductPriceView extends ProductPriceAwareView {

    Long historyEntryId
    boolean valuationMode
    List<ProductSalesPrice> salesPriceHistory = []
    String permissions = 'product_sp_lower_pp'

    ProductPriceView() {
        super()
        providesEditMode()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            if (!env.productView) {
                throw new ActionException(ActionException.NO_VIEW_BOUND)
            }
            bean = env.productView.product
        }
        loadSalesPriceHistory()
    }

    @Override
    void save() {
        super.save()
        loadSalesPriceHistory()
        env.productView.reload()
    }

    void reload() {
        bean = productManager.get(product.getProductId())
    }
    
    void enableHistoryEditMode() {
        historyEntryId = form.getLong('id')
    }

    boolean isHistoryEditMode() {
        (historyEntryId != null)
    }

    void updateHistory() {
        Date validFrom = form.getDate('validFrom')
        ProductPriceManager apm = getProductPriceManager()
        if (apm != null && validFrom != null) {
            salesPriceHistory = apm.updatePriceHistory(domainEmployee, historyEntryId, validFrom)
            env.productView.reload()
        }
        historyEntryId = null
    }

    private void loadSalesPriceHistory() {
        salesPriceHistory = productPriceManager.getPriceHistory(product.getProductId())
    }
}

