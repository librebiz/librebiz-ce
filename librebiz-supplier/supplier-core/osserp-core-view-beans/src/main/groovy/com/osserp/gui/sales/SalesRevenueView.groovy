/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.gui.sales

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.core.sales.SalesOrderManager
import com.osserp.core.sales.SalesRevenueManager
import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class SalesRevenueView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(SalesRevenueView.class.getName())

    String listName

    SalesRevenueView() {
        dependencies = ['businessCaseView']
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            Long id = form.getLong('id')
            if (id) {
                bean = salesRevenueManager.getRevenue(id)
            } else if (env.businessCaseView) {
                try {
                    def salesOrder = salesOrderManager.getBySales(env.businessCaseView.sales)
                    bean = salesRevenueManager.getRevenue(salesOrder.id)
                } catch (Exception e) {
                    logger.error("initRequest: failed [message=${e.getMessage()}")
                }
            }
            if (bean?.postCalculationRevenue) {
                logger.debug("initRequest: post calculation revenue found")
                togglePostCalculation()
            }
        }
    }

    def list() {
        listName = form.getString('name')
        if (listName) {
            if (postCalculation && bean?.postCalculationRevenue) {
                list = bean.postCalculationRevenue[listName]
                logger.debug("list: set list done [name=${listName}, size=${list.size()}, postCalculation=true]")
            } else if (bean) {
                list = bean[listName]
                logger.debug("list: set list done [name=${listName}, size=${list.size()}, postCalculation=false]")
            } else {
                logger.debug("list: did not find revenue bean [listName=${listName}]")
            }
        } else {
            list = []
            listName = null
        }
    }

    boolean isPostCalculation() {
        env.postCalculation
    }

    void togglePostCalculation() {
        if (postCalculation) {
            env.postCalculation = false
            logger.debug("togglePostCalculation: done [postCalculation=false]")
        } else {
            env.postCalculation = true
            logger.debug("togglePostCalculation: done [postCalculation=true]")
        }
    }

    private SalesOrderManager getSalesOrderManager() {
        getService(SalesOrderManager.class.getName())
    }

    private SalesRevenueManager getSalesRevenueManager() {
        getService(SalesRevenueManager.class.getName())
    }
}

