<%@ page import="com.osserp.common.Calendar" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="imgLeftUrl" scope="request"><c:url value="${applicationScope.leftIcon}"/></c:set>
<c:set var="imgRightUrl" scope="request"><c:url value="${applicationScope.rightIcon}"/></c:set>
<%
	// navigation menue

	Calendar cal = (Calendar) request.getAttribute("cal");
    if (cal != null) {
        String calUrl = (String) request.getAttribute("calUrl");
        String imageLeftUrl = (String) request.getAttribute("imgLeftUrl");
        String imageRightUrl = (String) request.getAttribute("imgRightUrl");
        out.print("<li><a href='" + calUrl + "&d=" + cal.getDay() + "&m=");
        if (cal.getMonth().intValue() == 0) {
            out.print("11&y=" + (cal.getYear().intValue() - 1));
        } else {
            out.print((cal.getMonth().intValue() - 1) + "&y=" + cal.getYear());
        }
        out.println("'><img src='" + imageLeftUrl + "' class='icon'/></a></li>");
        out.print("<li><a href='" + calUrl + "&d=" + cal.getDay() + "&m=");
        if(cal.getMonth().intValue() == 11) {
            out.print("0&y=" + (cal.getYear().intValue() + 1));
        } else {
            out.print((cal.getMonth().intValue() + 1) + "&y=" + cal.getYear());
        }
        out.println("'><img src='" + imageRightUrl + "' class='icon'/></a></li>");
    }
%>
