/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 31-Aug-2006 13:48:21 
 * 
 */
package com.osserp.core.model.records;

import java.util.ArrayList;
import java.util.List;

import com.osserp.common.beans.AbstractEntity;
import com.osserp.core.finance.DeliveryNote;
import com.osserp.core.finance.Order;
import com.osserp.core.sales.SalesInvoice;
import com.osserp.core.sales.SalesOrderVolumeExport;
import com.osserp.core.sales.SalesOrderVolumeExportConfig;
import com.osserp.core.sales.SalesOrderVolumeExportItem;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesOrderVolumeExportImpl extends AbstractEntity implements SalesOrderVolumeExport {

    private SalesOrderVolumeExportConfig config = null;
    private boolean ignorable = false;
    private List<SalesOrderVolumeExportItem> items = new ArrayList<SalesOrderVolumeExportItem>();

    protected SalesOrderVolumeExportImpl() {
        super();
    }

    public SalesOrderVolumeExportImpl(SalesOrderVolumeExportConfig config, SalesInvoice invoice) {
        super((Long) null, invoice.getId(), invoice.getCreatedBy());
        this.config = config;
    }

    public SalesOrderVolumeExportConfig getConfig() {
        return config;
    }

    protected void setConfig(SalesOrderVolumeExportConfig config) {
        this.config = config;
    }

    public void addItem(Long orderId) {
        SalesOrderVolumeExportItem existing = fetchItem(orderId);
        if (existing == null) {
            this.items.add(new SalesOrderVolumeExportItemImpl(getId(), orderId));
        }
    }

    public void addDeliveries(Order order, List<Long> deliveries) {
        for (int i = 0, j = items.size(); i < j; i++) {
            SalesOrderVolumeExportItem next = items.get(i);
            if (next.getOrderId().equals(order.getId())) {
                next.addDeliveries(order, deliveries);
                break;
            }
        }
    }

    public SalesOrderVolumeExportItem fetchItem(Order order) {
        for (int i = 0, j = items.size(); i < j; i++) {
            SalesOrderVolumeExportItem next = items.get(i);
            if (next.getOrderId().equals(order.getId())) {
                return next;
            }
        }
        return null;
    }

    public boolean added(DeliveryNote note) {
        for (int i = 0, j = items.size(); i < j; i++) {
            SalesOrderVolumeExportItem next = items.get(i);
            if (next.getOrderId().equals(note.getReference())) {
                return next.added(note);
            }
        }
        return false;
    }

    public List<DeliveryNote> getOpen(Order order) {
        List<DeliveryNote> result = new ArrayList<DeliveryNote>();
        for (int i = 0, j = items.size(); i < j; i++) {
            SalesOrderVolumeExportItem next = items.get(i);
            if (next.getOrderId().equals(order.getId())) {
                result.addAll(next.getOpen(order));
                break;
            }
        }
        return result;
    }

    public List<SalesOrderVolumeExportItem> getItems() {
        return items;
    }

    public boolean isIgnorable() {
        return ignorable;
    }

    private SalesOrderVolumeExportItem fetchItem(Long orderId) {
        for (int i = 0, j = items.size(); i < j; i++) {
            SalesOrderVolumeExportItem next = items.get(i);
            if (next.getOrderId().equals(orderId)) {
                return next;
            }
        }
        return null;
    }

    protected void setIgnorable(boolean ignorable) {
        this.ignorable = ignorable;
    }

    protected void setItems(List<SalesOrderVolumeExportItem> items) {
        this.items = items;
    }
}
