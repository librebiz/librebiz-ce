/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 23, 2006 5:06:00 PM 
 * 
 */
package com.osserp.core.sales;

import java.util.Map;

import com.osserp.core.Item;
import com.osserp.core.finance.CalculationAwareRecord;
import com.osserp.core.finance.DeliveryNote;
import com.osserp.core.finance.DiscountAwareRecord;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.RebateAwareRecord;
import com.osserp.core.finance.RecordType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SalesOrder extends CalculationAwareRecord, DiscountAwareRecord, Order, SalesRecord, RebateAwareRecord {
    static final Long RECORD_TYPE = RecordType.SALES_ORDER;
    static final Long BOOK_TYPE_SALES = 1L;
    static final Long BOOK_TYPE_SERVICE = 2L;
    static final Long BOOK_TYPE_WARRANTY = 3L;

    /**
     * Indicates that all ordered goods are delivered
     * @return delivered
     */
    boolean isDelivered();

    /**
     * Marks the order as delivered
     */
    void closeDelivery();

    /**
     * Resets the delivered flag if set
     */
    void resetDelivered();

    /**
     * Indicates that all ordered delivery note aware goods are delivered Flag will be automatically set when delivery notes are added
     * @return mailDelivered
     */
    boolean isMainDelivered();

    /**
     * Removes a delivery note from order and resets counters and flags
     * @param note to remove
     */
    void removeDelivery(DeliveryNote note);

    /**
     * Provides the total delivered goods
     * @return deliveredGoods
     */
    Map<Long, Item> getDeliveredGoods();
}
