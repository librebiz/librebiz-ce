/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 29-Jan-2007 08:19:48 
 * 
 */
package com.osserp.core.model.products;

import org.apache.commons.beanutils.BeanUtils;

import org.jdom2.Element;

import com.osserp.common.Constants;
import com.osserp.common.XmlAwareEntity;
import com.osserp.common.xml.JDOMUtil;

import com.osserp.core.model.AbstractDetails;
import com.osserp.core.model.BusinessPropertyImpl;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductDetails;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProductDetailsImpl extends AbstractDetails implements ProductDetails, XmlAwareEntity {
    private Integer height = Constants.INT_NULL;
    private Integer width = Constants.INT_NULL;
    private Integer depth = Constants.INT_NULL;
    private String measurementUnit = null;

    private Double weight = Constants.DOUBLE_NULL;
    private String weightUnit = null;

    private String techNote = null;
    private Long color = null;
    

    protected ProductDetailsImpl() {
        super();
    }

    protected ProductDetailsImpl(Long reference) {
        super(reference);
    }

    protected ProductDetailsImpl(Product reference) {
        super(reference.getProductId());
    }

    protected ProductDetailsImpl(ProductDetailsImpl other) {
        super((other == null ? null : other.getReference()), 
                (other == null ? null : other.getPropertyList()));
        if (other != null) {
            if (other.getDepth() != null) {
                this.depth = other.getDepth();
            }
            if (other.getHeight() != null) {
                this.height = other.getHeight();
            }
            if (other.getWidth() != null) {
                this.width = other.getWidth();
            }
            if (other.getMeasurementUnit() != null) {
                this.measurementUnit = other.getMeasurementUnit();
            }
            if (other.getWeight() != null) {
                this.weight = other.getWeight();
            }
            if (other.getWeightUnit() != null) {
                this.weightUnit = other.getWeightUnit();
            }
            if (other.getTechNote() != null) {
                this.techNote = other.getTechNote();
            }
        }
    }

    public ProductDetailsImpl(Element root) {
        for (Object child : root.getChildren()) {
            Element el = (Element) child;
            try {
                if (BeanUtils.getProperty(this, el.getName()) == null) {
                    BeanUtils.setProperty(this, el.getName(), JDOMUtil.getObjectFromElement(el));
                }
            } catch (Exception ex) {
            }
        }
    }

    @Override
    protected BusinessPropertyImpl newProperty(Long user, String referenceContext, String referenceType, Long reference, String name, String value) {
        return new ProductPropertyImpl(user, reference, name, value);
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getDepth() {
        return depth;
    }

    public void setDepth(Integer depth) {
        this.depth = depth;
    }

    public String getMeasurementUnit() {
        return measurementUnit;
    }

    public void setMeasurementUnit(String measurementUnit) {
        this.measurementUnit = measurementUnit;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getWeightUnit() {
        return weightUnit;
    }

    public void setWeightUnit(String weightUnit) {
        this.weightUnit = weightUnit;
    }

    public String getTechNote() {
        return techNote;
    }

    public void setTechNote(String techNote) {
        this.techNote = techNote;
    }

    public Long getColor() {
        return color;
    }

    public void setColor(Long color) {
        this.color = color;
    }

    public boolean isWidthIsDiameter() {
        return false;
    }

    public boolean isMeasurementsAvailable() {
        return (isSet(width) || isSet(height) || isSet(depth));
    }

    @Override
    public Element getXML(String rootElementName) {
        Element root = super.getXML(rootElementName);
        root.addContent(JDOMUtil.createElement("techNote", techNote));
        root.addContent(JDOMUtil.createElement("depth", depth));
        root.addContent(JDOMUtil.createElement("height", height));
        root.addContent(JDOMUtil.createElement("measurementUnit", measurementUnit));
        root.addContent(JDOMUtil.createElement("weight", weight));
        root.addContent(JDOMUtil.createElement("weightUnit", weightUnit));
        root.addContent(JDOMUtil.createElement("width", width));
        return root;
    }
    
    /**
     * Overrides default value 'properties'
     * @return string 'details'  
     */
    @Override
    protected String getPropertyXmlElementName() {
        return "details";
    }
}
