/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 30-Jan-2007 08:00:30 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import com.osserp.common.dao.AbstractRowMapper;
import com.osserp.core.finance.RecordStatusHistory;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RecordStatusHistoryRowMapper extends AbstractRowMapper {

    private static final String SELECT_VALUES =
            "SELECT id,name,lastname,firstname,created FROM ";
    private static final String WHERE_CLAUSE = " WHERE reference_id = ";
    private static final String TYPE_CLAUSE = " AND type_id = ";
    private static final String ORDER_CLAUSE = " ORDER BY created DESC";

    public static final String createQueryString(String tableName, Long recordId, Long recordType) {
        StringBuilder result = new StringBuilder(SELECT_VALUES);
        result
            .append(tableName).append(WHERE_CLAUSE).append(recordId)
            .append(TYPE_CLAUSE).append(recordType)
            .append(ORDER_CLAUSE);
        return result.toString();
    }

    public static RowMapperResultSetExtractor getReader() {
        return new RowMapperResultSetExtractor(new RecordStatusHistoryRowMapper());
    }

    public Object mapRow(ResultSet rs, int arg1) throws SQLException {
        StringBuilder employee = new StringBuilder();
        String firstname = rs.getString(4);
        if (firstname != null) {
            employee.append(firstname).append(" ");
        }
        employee.append(rs.getString(3));
        return new RecordStatusHistory(
                Long.valueOf(rs.getLong(1)),
                rs.getString(2),
                employee.toString(),
                rs.getDate(5));
    }
}
