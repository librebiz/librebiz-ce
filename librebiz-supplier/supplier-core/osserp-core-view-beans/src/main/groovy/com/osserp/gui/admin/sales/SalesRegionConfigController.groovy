/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.gui.admin.sales

import javax.servlet.http.HttpServletRequest

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.groovy.web.AdminViewController

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
@RequestMapping("/admin/sales/salesRegionConfig/*")
@Controller class SalesRegionConfigController extends AdminViewController {
    private static Logger logger = LoggerFactory.getLogger(SalesRegionConfigController.class.getName())

    @RequestMapping
    def enableZipcodeAddMode(HttpServletRequest request) {
        logger.debug("enableZipcodeAddMode: invoked [user=${getUserId(request)}]")
        SalesRegionConfigView view = getView(request)
        view.enableZipcodeAddMode()
        defaultPage
    }

    @RequestMapping
    def disableZipcodeAddMode(HttpServletRequest request) {
        logger.debug("disableZipcodeAddMode: invoked [user=${getUserId(request)}]")
        SalesRegionConfigView view = getView(request)
        view.disableZipcodeAddMode()
        defaultPage
    }

    @RequestMapping
    def addZipcode(HttpServletRequest request) {
        logger.debug("addZipcode: invoked [user=${getUserId(request)}]")
        SalesRegionConfigView view = getView(request)
        try {
            view.addZipcode()
        } catch (Exception e) {
            if (logger.debugEnabled) {
                logger.debug("addZipcode: caught exception [class=${e.getClass().getName()}, message=${e.message}]")
            }
            saveError(request, e.message)
        }
        defaultPage
    }

    @RequestMapping
    def removeZipcode(HttpServletRequest request) {
        logger.debug("removeZipcode: invoked [user=${getUserId(request)}]")
        SalesRegionConfigView view = getView(request)
        view.removeZipcode()
        defaultPage
    }

    @RequestMapping
    def toggleDisplayEol(HttpServletRequest request) {
        logger.debug("toggleDisplayEol: invoked [user=${getUserId(request)}]")
        SalesRegionConfigView view = getView(request)
        view.toggleDisplayEol()
        defaultPage
    }

    @RequestMapping
    def toggleEol(HttpServletRequest request) {
        logger.debug("toggleEol: invoked [user=${getUserId(request)}]")
        SalesRegionConfigView view = getView(request)
        view.toggleEol()
        defaultPage
    }

    @RequestMapping
    def delete(HttpServletRequest request) {
        logger.debug("delete: invoked [user=${getUserId(request)}]")
        SalesRegionConfigView view = getView(request)
        view.delete()
        defaultPage
    }
}
