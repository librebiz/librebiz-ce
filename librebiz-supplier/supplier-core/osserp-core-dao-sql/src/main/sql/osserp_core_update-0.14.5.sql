SET search_path = osserp, pg_catalog;

UPDATE contact_groups SET end_of_life = true WHERE id = 0;
UPDATE customer_types SET end_of_life = true WHERE id = 5;
UPDATE customer_types SET name = 'Endverbraucher' WHERE id = 1;

UPDATE system_properties SET pvalue = '0.14.5', ptype = 'java.lang.String' WHERE name = 'dbmsSchemaVersion';
