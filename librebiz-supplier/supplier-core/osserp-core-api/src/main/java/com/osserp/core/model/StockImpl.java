/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 17, 2008 12:30:19 PM 
 * 
 */
package com.osserp.core.model;

import java.util.Date;

import com.osserp.common.beans.AbstractOption;
import com.osserp.core.finance.Stock;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class StockImpl extends AbstractOption implements Stock {

    private Long branchId;
    private Date activationDate;
    private String shortKey;
    private boolean defaultStock = false;
    private boolean active = false;
    private boolean planningAware = false;

    protected StockImpl() {
        super();
    }

    public StockImpl(
            Long id,
            Long companyId,
            Long branchId,
            String name,
            String description,
            String shortKey) {
        super(id, companyId, name);
        setDescription(description);
        activationDate = new Date();
        active = true;
        planningAware = true;
        this.branchId = branchId;
        this.shortKey = shortKey;
        defaultStock = (branchId == null);
    }

    protected StockImpl(Stock src) {
        super(src);
        branchId = src.getBranchId();
        activationDate = src.getActivationDate();
        defaultStock = src.isDefaultStock();
        shortKey = src.getShortKey();
        active = src.isActive();
        planningAware = src.isPlanningAware();
    }

    public Long getBranchId() {
        return branchId;
    }

    public void setBranchId(Long branchId) {
        this.branchId = branchId;
    }

    public Date getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(Date activationDate) {
        this.activationDate = activationDate;
    }

    public String getShortKey() {
        return shortKey;
    }

    public void setShortKey(String shortKey) {
        this.shortKey = shortKey;
    }

    public boolean isDefaultStock() {
        return defaultStock;
    }

    public void setDefaultStock(boolean defaultStock) {
        this.defaultStock = defaultStock;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isPlanningAware() {
        return planningAware;
    }

    public void setPlanningAware(boolean planningAware) {
        this.planningAware = planningAware;
    }
}
