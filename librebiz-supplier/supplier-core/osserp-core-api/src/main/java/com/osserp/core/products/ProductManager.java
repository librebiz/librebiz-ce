/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 16.10.2004 
 * 
 */
package com.osserp.core.products;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.DetailsManager;
import com.osserp.common.PermissionException;

import com.osserp.core.employees.Employee;
import com.osserp.core.users.DomainUser;
import com.osserp.core.views.StockReportDisplay;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ProductManager extends DetailsManager {

    /**
     * Gets an existing product by primary key
     * @param id
     * @return product. Throws runtime exception if product not exists.
     */
    Product get(Long id);

    /**
     * Provides all stock input and output of an product
     * @param stockId
     * @param productId
     * @return stockReport
     */
    List<StockReportDisplay> getStockReport(Long stockId, Long productId);

    /**
     * Provides all stock aware products of given type
     * @param category
     * @return stockAware
     */
    List<Product> getStockAware(ProductCategory category);

    /**
     * Provides all stock aware products of given group
     * @param group
     * @return stockAware
     */
    List<Product> getStockAware(ProductGroup group);

    /**
     * Provides all stock aware products of given type
     * @param type
     * @return stockAware
     */
    List<Product> getStockAware(ProductType type);

    /**
     * Provides all serials as record related serial display
     * @return serialDisplay
     */
    List<SerialDisplay> getSerialDisplay();

    /**
     * Provides all serials as record related serial display by product
     * @param productId
     * @return serialDisplay
     */
    List<SerialDisplay> getSerialDisplay(Long productId);

    /**
     * Creates a new product
     * @param user
     * @param productId
     * @param config
     * @param matchcode
     * @param name
     * @param description
     * @param quantityUnit
     * @param packagingUnit
     * @param manufacturer
     * @param planningMode
     * @param source
     * @return new created product
     * @throws ClientException if validation of values failed
     */
    Product create(
            Employee user,
            Long productId,
            ProductClassificationConfig config,
            String matchcode,
            String name,
            String description,
            Long quantityUnit,
            Integer packagingUnit,
            Long manufacturer,
            ProductPlanningMode planningMode,
            Product source) throws ClientException;

    /**
     * Provides a classification based suggestion for a new product id
     * @param config
     * @return id suggestion
     */
    Long createIdSuggestion(ProductClassificationConfig config);

    /**
     * Removes type from product's classification config
     * @param product
     * @param type
     * @return updated product
     * @throws ClientException if product doesn't have alternative product assigned
     */
    Product removeType(Product product, ProductType type) throws ClientException;

    /**
     * Updates the main data of the product. Currently the manufacturer and quantity unit. We retrieve all other main values from corresponding host system
     * @param product
     * @throws ClientException if quantity unit missing
     */
    void update(Product product) throws ClientException;

    /**
     * Updates product classification
     * @param product
     * @param type
     * @param group
     * @param category
     * @throws ClientException if group category relation is ambiguous
     */
    Product updateClassification(
            Product product,
            ProductTypeConfig type,
            ProductGroupConfig group,
            ProductCategoryConfig category) throws ClientException;

    /**
     * Updates the currently loaded product details
     * @throws ClientException if validation failed
     */
    void updateDetails(Product product) throws ClientException;

    /**
     * Updates an products document availability flag
     * @param productId of the product to update
     * @param docType of the document
     * @param available wether datasheet available
     * @return updated values
     * @throws ClientException if specified product not exists
     */
    void updateDocumentFlag(Product product, Long docType, boolean available);
    
    /**
     * Sends document synchronization event if syncClient available and enabled
     * @param product
     * @param docType
     * @param docId
     */
    void syncClient(Product product, Long docType, Long docId);

    /**
     * Synchronizes all products with enabled public availability flag.
     * A client synchronization must be configured and enabled to work.
     */
    void syncPublicAvailable();
    
    /**
     * Synchronizes all product media with enabled public availability flag.
     * A client synchronization must be configured and enabled to work.
     */
    void syncPublicAvailableMedia();

    /**
     * Adds or updates a description
     * @param user
     * @param product
     * @param language
     * @param name
     * @param description
     * @param datasheetText
     * @throws ClientException if validation failed
     */
    void updateDescription(Long user, Product product, String language, String name, String description, String datasheetText) throws ClientException;

    /**
     * Finds all descriptions of an product
     * @param product
     * @return list of available descriptions
     */
    List<ProductDescription> findDescriptions(Product product);

    /**
     * Changes the purchase price limit flag
     * @param domainUser
     * @param product
     * @throws PermissionException if user has no permissions
     */
    void changePurchasePriceLimitFlag(DomainUser domainUser, Product product) throws PermissionException;

    /**
     * Finds the product's manufacturer
     * @param product
     * @return manufacturer
     */
    Manufacturer findManufacturer(Product product);
}
