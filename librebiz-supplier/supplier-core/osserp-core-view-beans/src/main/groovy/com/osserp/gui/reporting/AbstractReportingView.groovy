/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Oct 27, 2010 9:41:43 AM 
 * 
 */
package com.osserp.gui.reporting

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.util.StringUtil

import com.osserp.groovy.web.MenuItem
import com.osserp.gui.CoreView

/**
 *
 * @author jg <jg@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
abstract class AbstractReportingView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(AbstractReportingView.class.getName())

    // Paramters to use in reload method
    Map values = [
        branch: null,
        businessType: null,
        statusValue: 100L,
        statusLower: true,
        company: null,
        stopped: null,
        confirmed: true,
        confirmedIgnore: true,
        closedIgnore: true
    ]

    // Use to generate menu items
    Map selectors = [
        branch: null,
        businessType: null,
        status: null,
        company: null
    ]
    
    Map toggles = [
        stopped: false,
        confirmed: false
    ]
    
    def refreshLink = false
    Long listReferenceId

    AbstractReportingView() {
        super()
        enableAutoreload()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        logger.debug('initRequest: invoked...')

        values.branch = form.getLong('branch')?: values.branch
        values.businessType = form.getLong('businessType')?: values.businessType
        values.statusValue = form.getLong('statusValue')?: values.statusValue
        values.statusLower = form.getLong('statusLower')?: values.statusLower
        values.company = form.getLong('company')?: values.company
        values.stopped = form.getLong('stopped')?: values.stopped
        values.confirmed = form.getBoolean('confirmed', values.confirmed)
        values.confirmedIgnore = form.getBoolean('confirmedIgnore', values.confirmedIgnore)
        values.closedIgnore = form.getBoolean('closedIgnore', values.closedIgnore)
        
        init()

        if (!list) {
            reload()
        }
    }

    @Override
    protected void createCustomNavigation() {
        def tmpList = [nav.listNavigation[0]]
        if (refreshLink) {
            tmpList << navigationLink("/${context.name}/reload", 'reloadIcon', 'listRefresh')
        }
        toggles.each() {
            if (it.value) {
                tmpList << toggleLink(it.key)
            }
        }
        selectors.each() {
            if (it.value) {
                tmpList << selectorLink(it.key)
            }
        }
        nav.listNavigation = tmpList + nav.listNavigation[1..-1]
        nav.defaultNavigation = nav.listNavigation
    }

    protected void init() {
        //implement init parameter tasks before list reloading if required
    }

    abstract void reload();


    MenuItem selectorLink(String selector) {
        def capSelector = StringUtil.capitalize(selector)
        return new MenuItem(link: "${context.fullPath}/reporting/selectors/select${capSelector}Popup/forward?view=${name}", icon: "select${capSelector}Icon", title: "selector.${selector}Title", ajaxPopup: true, ajaxPopupName: "select${capSelector}PopupView")
    }

    MenuItem toggleLink(String valueName) {
        if (values[valueName] == null) {
            return null
        }
        def capValueName = StringUtil.capitalize(valueName)
        def value = values[valueName] ? 'True' : 'False'
        return navigationLink("/${context.name}/toggle?name=${valueName}", "toggle${capValueName}${value}Icon", "toggle.${valueName}${value}Title")
    }

    void toggle() {
        String valueName = form.getString('name')
        if (valueName && values[valueName] != null) {
            values[valueName] = !values[valueName]
            reload()
            nav.listNavigation = null
            generateNavigation()
        }
    }

    protected void resetValues() {
        values.branch = null
        values.businessType = null
        values.statusValue = null
        values.statusLower = true
        values.company = null
        values.stopped = null
        values.confirmed = true
    }
}
