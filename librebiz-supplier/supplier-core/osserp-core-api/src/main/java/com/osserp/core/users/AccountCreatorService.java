/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jul 31, 2014 
 * 
 */
package com.osserp.core.users;

import com.osserp.common.ClientException;
import com.osserp.common.User;
import com.osserp.core.employees.Employee;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface AccountCreatorService {
    
    
    /**
     * Indicates if create method invocation is required for an employee
     * @param employee the employee to check status for
     * @return true if create invocation required
     */
    boolean createRequired(Employee employee);

    /**
     * Creates a new authentication account
     * @param creator the user creating the new account
     * @param employee reference for new account
     * @param uid for the new account
     * @param password
     * @return new created user with saved uid and password set
     * @throws ClientException if uid already exists or other validation failure
     */
    User create(Employee creator, Employee employee, String uid, String password) throws ClientException;

    /**
     * Checks if invocation of the create method with the provided employee will fail
     * @param employee
     * @return the error string that invocation of create would deliver by exception.message or null if employee data is ok.
     */
    String createWillFail(Employee employee);

    /**
     * Creates a new uid suggestion based on internal rules and/or provided employee values.
     * @param employee reference for uid suggestion
     * @return new created uid suggestion
     */
    String createUidSuggestion(Employee employee);

}
