/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 27, 2013 2:33:35 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.dao.Tables;
import com.osserp.common.dms.DmsConfig;

import com.osserp.core.BusinessTemplate;
import com.osserp.core.BusinessTemplateType;
import com.osserp.core.dao.BranchOffices;
import com.osserp.core.dao.BusinessTemplates;
import com.osserp.core.model.BusinessTemplateImpl;
import com.osserp.core.model.BusinessTemplateTypeImpl;
import com.osserp.core.system.BranchOffice;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class BusinessTemplatesDao extends AbstractDao implements BusinessTemplates {
    private static Logger log = LoggerFactory.getLogger(BusinessTemplatesDao.class.getName());

    private BranchOffices branchOffices;

    /**
     * Creates a businessTemplates dao
     * @param jdbcTemplate
     * @param tables
     * @param sessionFactory
     * @param branchOffices
     */
    protected BusinessTemplatesDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            BranchOffices branchOffices) {
        super(jdbcTemplate, tables, sessionFactory);
        this.branchOffices = branchOffices;
    }

    public String createTemplateFilename(Long branchId, String filename) {
        BranchOffice office = isEmpty(branchId) ? null : branchOffices.getBranchOffice(branchId);
        if (office == null) {
            office = branchOffices.getHeadquarter();
        }
        StringBuilder file = new StringBuilder();
        String customPath = getCustomTemplatesPath();
        if (customPath != null) {
            file.append("/").append(customPath);
        }
        if (office != null && !isEmpty(office.getShortname())) {
            file.append("/").append(office.getShortname());
        }
        file.append("/").append(filename);
        return file.toString();
    }

    public String getCustomTemplatesPath() {
        String path = getPropertyString(DmsConfig.CUSTOM_TEMPLATE_PATH_PROPERTY);
        if (isEmpty(path)) {
            log.warn("getCustomTemplatesPath() property not found [name="
                    + DmsConfig.CUSTOM_TEMPLATE_PATH_PROPERTY + "]");
        }
        return path;
    }

    public BusinessTemplateType getType(Long id) {
        StringBuilder hql = new StringBuilder("from ");
        hql.append(BusinessTemplateTypeImpl.class.getName()).append(" obj where obj.id = :pk");
        try {
            List<BusinessTemplateType> types = getCurrentSession().createQuery(
                    hql.toString()).setParameter("pk", id).list();
            BusinessTemplateType result = types.isEmpty() ? null : types.get(0);
            if (result == null) {
                log.warn("getType() did not find type by id [id=" + id + "]");
            }
            return result;
        } catch (Exception e) {
            log.error("getType() failed [id=" + id + ", message=" + e.getMessage() + "]", e);
            throw new BackendException("template.type.missing");
        }
    }

    public BusinessTemplateType getType(String contextName) {
        if (log.isDebugEnabled()) {
            log.debug("getType() invoked [contextName=" + (isEmpty(contextName)
                    ? "null" : (contextName + ", class=" + contextName.getClass().getName()))
                    + "]");
        }
        StringBuilder hql = new StringBuilder("from ");
        hql
            .append(BusinessTemplateTypeImpl.class.getName())
            .append(" obj where obj.contextName = :name");
        try {
            List<BusinessTemplateType> types = getCurrentSession().createQuery(
                    hql.toString()).setParameter("name", contextName).list();
            BusinessTemplateType result = types.isEmpty() ? null : types.get(0);
            if (result == null) {
                log.warn("getType() did not find type by context [contextName=" + contextName + "]");
            }
            return result;
        } catch (Exception e) {
            log.error("getType() failed [contextName=" + contextName + ", message=" + e.getMessage() + "]", e);
            throw new BackendException("template.type.missing");
        }
    }

    public List<BusinessTemplateType> getTypes() {
        StringBuilder hql = new StringBuilder("from ");
        hql.append(BusinessTemplateTypeImpl.class.getName()).append(" obj order by obj.name");
        try {
            return getCurrentSession().createQuery(hql.toString()).list();
        } catch (Exception e) {
            log.error("getTypes() failed [message=" + e.getMessage() + "]", e);
            return new ArrayList<BusinessTemplateType>();
        }
    }

    public List<BusinessTemplate> findByContext(String contextName) {
        StringBuilder hql = new StringBuilder("from ");
        hql
            .append(BusinessTemplateImpl.class.getName())
            .append(" obj where obj.type.contextName = :name order by obj.name");
        try {
            return getCurrentSession().createQuery(
                    hql.toString()).setParameter("name", contextName).list();
        } catch (Exception e) {
            log.error("findByReference() failed [contextName=" + contextName + ", message=" + e.getMessage() + "]", e);
            return new ArrayList<BusinessTemplate>();
        }
    }

    public List<BusinessTemplate> findByType(Long typeId) {
        StringBuilder hql = new StringBuilder("from ");
        hql
            .append(BusinessTemplateImpl.class.getName())
            .append(" obj where obj.type.id = :typeId order by obj.name");
        try {
            return getCurrentSession().createQuery(hql.toString()).setParameter("typeId", typeId).list();
        } catch (Exception e) {
            log.error("findByType() failed [typeId=" + typeId + ", message=" + e.getMessage() + "]", e);
            return new ArrayList<BusinessTemplate>();
        }
    }

    public BusinessTemplate getTemplate(Long id) {
        try {
            return (BusinessTemplate) getCurrentSession().load(BusinessTemplateImpl.class, id);
        } catch (Exception e) {
            log.error("getTemplate() failed [id=" + id + ", message=" + e.getMessage() + "]", e);
            throw new BackendException("template.missing");
        }
    }

    public BusinessTemplate create(
            Long user,
            BusinessTemplateType type,
            Long reference, 
            Long branchId, 
            String name, 
            String description, 
            Long templateId) throws ClientException {

        BusinessTemplate obj = new BusinessTemplateImpl(
                user,
                type,
                reference, 
                branchId, 
                name, 
                description, 
                templateId);
        getCurrentSession().saveOrUpdate(BusinessTemplateImpl.class.getName(), obj);
        return obj;
    }

    public void delete(BusinessTemplate template) {
        try {
            getCurrentSession().delete(BusinessTemplateImpl.class.getName(), template);
            if (log.isDebugEnabled()) {
                log.debug("delete() done [id=" 
                        + (template == null ? "null" : template.getId())
                        + "]");
            }
        } catch (Exception e) {
            log.error("delete() failed [template=" 
                    + (template == null ? "null" : template.getId())
                    + ", message=" + e.getMessage() + "]", e);
        }
    }

    public BusinessTemplate save(BusinessTemplate template) {
        getCurrentSession().saveOrUpdate(BusinessTemplateImpl.class.getName(), template);
        return template;
    }
}
