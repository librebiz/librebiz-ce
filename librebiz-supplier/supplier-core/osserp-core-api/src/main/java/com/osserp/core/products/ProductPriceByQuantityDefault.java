/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 13, 2011 8:33:30 PM 
 * 
 */
package com.osserp.core.products;

import java.util.List;

import com.osserp.common.Option;
import com.osserp.core.employees.Employee;

/**
 * @author Rainer Kirchner <rk@osserp.com>
 */
public interface ProductPriceByQuantityDefault extends Option {

    /**
     * Adds a new range
     * @param user
     * @param rangeEnd
     */
    void addRange(Employee user, Double rangeEnd);

    /**
     * Removes the last range
     * @param id
     */
    void removeRange();

    /**
     * Provides the default ranges
     * @return ranges
     */
    List<ProductPriceByQuantityDefaultRange> getRanges();

    /**
     * Indicates default for products of type
     * @return typeId
     */
    Long getTypeId();

    /**
     * Indicates default for products of group
     * @return groupId
     */
    Long getGroupId();

    /**
     * Indicates default for products of category
     * @return categoryId
     */
    Long getCategoryId();

    /**
     * Indicates default for dedicated product
     * @return productId
     */
    Long getProductId();
}
