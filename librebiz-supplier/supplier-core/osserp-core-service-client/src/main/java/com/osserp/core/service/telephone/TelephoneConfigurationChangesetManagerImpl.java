/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Oct 13, 2010 11:54:27 AM 
 * 
 */
package com.osserp.core.service.telephone;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.osserp.core.dao.telephone.TelephoneConfigurationChangesets;
import com.osserp.core.model.telephone.TelephoneConfigurationChangesetImpl;
import com.osserp.core.service.impl.AbstractService;
import com.osserp.core.telephone.TelephoneConfigurationChangeset;
import com.osserp.core.telephone.TelephoneConfigurationChangesetManager;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public class TelephoneConfigurationChangesetManagerImpl extends AbstractService implements TelephoneConfigurationChangesetManager {

    private TelephoneConfigurationChangesets telephoneConfigurationChangesetsDao = null;

    public TelephoneConfigurationChangesetManagerImpl(TelephoneConfigurationChangesets telephoneConfigurationChangesetsDao) {
        super();
        this.telephoneConfigurationChangesetsDao = telephoneConfigurationChangesetsDao;
    }

    public TelephoneConfigurationChangeset create(Long createdBy, Long referenceId, String property, String oldValue, String newValue) {
        TelephoneConfigurationChangeset tcc = new TelephoneConfigurationChangesetImpl(
                createdBy,
                referenceId,
                property,
                oldValue,
                newValue);

        save(tcc);
        return tcc;
    }

    public Map<String, TelephoneConfigurationChangeset> getLatestChangesetsByConfigurationId(Long cId) {
        List<TelephoneConfigurationChangeset> tmp = telephoneConfigurationChangesetsDao.getAllChangesetsByConfigurationId(cId);
        Map<String, TelephoneConfigurationChangeset> newMap = new HashMap<String, TelephoneConfigurationChangeset>();

        for (Iterator<TelephoneConfigurationChangeset> i = tmp.iterator(); i.hasNext();) {
            TelephoneConfigurationChangeset next = i.next();
            if (newMap.containsKey(next.getProperty())) {
                if (next.getCreated().after(newMap.get(next.getProperty()).getCreated())) {
                    newMap.put(next.getProperty(), next);
                }
            } else {
                newMap.put(next.getProperty(), next);
            }
        }
        return newMap;
    }

    private void save(TelephoneConfigurationChangeset tcc) {
        this.telephoneConfigurationChangesetsDao.save(tcc);
    }
}
