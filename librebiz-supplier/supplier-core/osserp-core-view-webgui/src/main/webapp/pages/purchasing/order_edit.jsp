<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.purchaseOrderView}">
    <c:redirect url="/errors/error_context.jsp" />
</c:if>
<c:set var="recordView" scope="session" value="${sessionScope.purchaseOrderView}" />
<c:set var="recordView" value="${sessionScope.recordView}" />
<c:set var="record" value="${recordView.record}" />
<c:set var="delivery_address_header" scope="request" value="/pages/purchasing/order_delivery_address_header.jsp" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="styles" type="string">
        <style type="text/css">
            <c:import url="/css/records_edit.css"/>
        </style>
    </tiles:put>

    <tiles:put name="headline">
        <c:import url="/pages/records/record_headline_edit.jsp" />
    </tiles:put>
    <tiles:put name="headline_right">
        <a href="<c:url value="/purchaseOrder.do?method=disableEdit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon" /></a>
        <v:link url="/records/recordPrint/print?view=purchaseOrderView" title="documentPrint"><o:img name="printIcon" /></v:link>
        <v:link url="/index" title="backToMenu">
            <o:img name="homeIcon" />
        </v:link>
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="subcolumns">
            <div class="subcolumn">
                <div class="spacer"></div>

                <div class="contentBox">
                    <div class="contentBoxData">
                        <div class="subcolumns">

                            <div class="contentBoxHeader">
                                <c:import url="/pages/records/record_header.jsp" />
                            </div>

                            <o:form name="recordForm" url="/purchaseOrder.do">
                                <input type="hidden" name="method" value="update" />
                                <table class="table">
                                    <tr>
                                        <td class="recordEditLeft" valign="top">
                                            <table>
                                                <c:import url="/pages/records/record_branch_edit.jsp" />
                                                <c:import url="/pages/records/record_sigs_edit.jsp" />
                                                <c:import url="/pages/records/record_taxfree_edit.jsp" />
                                                <c:import url="/pages/records/record_shipping_edit.jsp" />
                                                <c:import url="/pages/records/record_delivery_address_edit.jsp" />
                                                <c:import url="/pages/records/record_notes_edit.jsp" />
                                            </table>
                                        </td>
                                        <td class="recordEditRight" valign="top">
                                            <table>
                                                <tr>
                                                    <td colspan="2"></td>
                                                </tr>
                                                <c:import url="/pages/records/record_language_edit.jsp" />
                                                <c:import url="/pages/records/record_currency_edit.jsp" />
                                                <c:import url="/pages/records/record_taxrate_edit.jsp" />
                                                <tr>
                                                    <td colspan="2"></td>
                                                </tr>
                                                <c:import url="/pages/records/record_save_changes.jsp" />
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </o:form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
<c:remove var="recordView" scope="session" />
