/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jul 20, 2007 3:35:58 AM 
 * 
 */
package com.osserp.core.model.records;

import java.util.Date;

import com.osserp.common.beans.AbstractEntity;
import com.osserp.core.finance.DeletedRecord;
import com.osserp.core.finance.RecordType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class DeletedRecordImpl extends AbstractEntity implements DeletedRecord {

    private RecordType type;
    private Date deleted;
    private Long deletedBy;
    private Long sales;

    protected DeletedRecordImpl() {
        super();
    }

    /**
     * Creates a new deleted record entry
     * @param id
     * @param type
     * @param reference
     * @param created
     * @param createdBy
     * @param deletedBy
     * @param sales
     */
    public DeletedRecordImpl(
            Long id,
            RecordType type,
            Long reference,
            Date created,
            Long createdBy,
            Long deletedBy,
            Long sales) {
        super(id, reference, createdBy);
        this.type = type;
        this.deleted = new Date(System.currentTimeMillis());
        this.deletedBy = deletedBy;
        this.sales = sales;
    }

    public RecordType getType() {
        return type;
    }

    public void setType(RecordType type) {
        this.type = type;
    }

    public Date getDeleted() {
        return deleted;
    }

    public void setDeleted(Date deleted) {
        this.deleted = deleted;
    }

    public Long getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(Long deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Long getSales() {
        return sales;
    }

    public void setSales(Long sales) {
        this.sales = sales;
    }
}
