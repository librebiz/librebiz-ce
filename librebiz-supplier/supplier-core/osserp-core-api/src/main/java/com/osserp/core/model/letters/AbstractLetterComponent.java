/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 25, 2008 
 * 
 */
package com.osserp.core.model.letters;

import java.util.Date;

import com.osserp.common.beans.AbstractOption;
import com.osserp.core.employees.Employee;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractLetterComponent extends AbstractOption {

    private String greetings = null;
    private boolean ignoreSalutation = false;
    private boolean ignoreGreetings = false;
    private boolean ignoreHeader = false;
    private boolean ignoreSubject = false;
    private boolean embedded = false;
    private boolean embeddedAbove = false;
    private String embeddedSpaceAfter = null;
    private String contentKeepTogether = "auto";

    protected AbstractLetterComponent() {
        super();
    }

    public AbstractLetterComponent(
            Long id,
            Long reference,
            Employee user,
            String name,
            String greetings,
            boolean ignoreSalutation,
            boolean ignoreGreetings,
            boolean ignoreHeader,
            boolean ignoreSubject,
            boolean embedded,
            boolean embeddedAbove,
            String embeddedSpaceAfter,
            String contentKeepTogether) {

        super(id, reference, name, (Date) null, user.getId());
        this.greetings = greetings;
        this.ignoreSalutation = ignoreSalutation;
        this.ignoreGreetings = ignoreGreetings;
        this.ignoreHeader = ignoreHeader;
        this.ignoreSubject = ignoreSubject;
        this.embedded = embedded;
        this.embeddedAbove = embeddedAbove;
        this.embeddedSpaceAfter = embeddedSpaceAfter;
        this.contentKeepTogether = contentKeepTogether;
    }

    protected AbstractLetterComponent(AbstractLetterComponent other) {
        super(other);
        this.greetings = other.getGreetings();
        this.ignoreSalutation = other.isIgnoreSalutation();
        this.ignoreGreetings = other.isIgnoreGreetings();
        this.ignoreHeader = other.isIgnoreHeader();
        this.ignoreSubject = other.isIgnoreSubject();
        this.embedded = other.isEmbedded();
        this.embeddedAbove = other.isEmbeddedAbove();
        this.embeddedSpaceAfter = other.getEmbeddedSpaceAfter();
        this.contentKeepTogether = other.getContentKeepTogether();
    }

    public String getGreetings() {
        return greetings;
    }

    public void setGreetings(String greetings) {
        this.greetings = greetings;
    }

    public boolean isIgnoreGreetings() {
        return ignoreGreetings;
    }

    public void setIgnoreGreetings(boolean ignoreGreetings) {
        this.ignoreGreetings = ignoreGreetings;
    }

    public boolean isIgnoreHeader() {
        return ignoreHeader;
    }

    public void setIgnoreHeader(boolean ignoreHeader) {
        this.ignoreHeader = ignoreHeader;
    }

    public boolean isIgnoreSalutation() {
        return ignoreSalutation;
    }

    public void setIgnoreSalutation(boolean ignoreSalutation) {
        this.ignoreSalutation = ignoreSalutation;
    }

    public boolean isIgnoreSubject() {
        return ignoreSubject;
    }

    public void setIgnoreSubject(boolean ignoreSubject) {
        this.ignoreSubject = ignoreSubject;
    }

    public boolean isEmbedded() {
        return embedded;
    }

    public void setEmbedded(boolean embedded) {
        this.embedded = embedded;
    }

    public boolean isEmbeddedAbove() {
        return embeddedAbove;
    }

    public void setEmbeddedAbove(boolean embeddedAbove) {
        this.embeddedAbove = embeddedAbove;
    }

    public String getEmbeddedSpaceAfter() {
        return embeddedSpaceAfter;
    }

    public void setEmbeddedSpaceAfter(String embeddedSpaceAfter) {
        this.embeddedSpaceAfter = embeddedSpaceAfter;
    }

    public String getContentKeepTogether() {
        return contentKeepTogether;
    }

    public void setContentKeepTogether(String contentKeepTogether) {
        this.contentKeepTogether = contentKeepTogether;
    }

}
