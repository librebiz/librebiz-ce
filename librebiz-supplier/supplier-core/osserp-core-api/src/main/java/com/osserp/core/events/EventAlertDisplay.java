/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jul 31, 2007 5:46:12 PM 
 * 
 */
package com.osserp.core.events;

import java.util.Date;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class EventAlertDisplay extends EventDisplay {

    private Long sourceId = null;
    private Long sourceConfigId = null;
    private Long sourceRecipientId = null;
    private String sourceRecipientName = null;
    private Date sourceCreated = null;
    private Date sourceExpires = null;

    /**
     * Creates a new eventAlertDisplay
     * @param id
     * @param name
     * @param salesId
     * @param salesName
     * @param managerId
     * @param managerName
     * @param alert
     * @param eventId
     * @param eventType
     * @param eventName
     * @param eventConfigId
     * @param eventCreated
     * @param eventExpires
     * @param recipientId
     * @param recipientName
     * @param sourceId
     * @param sourceConfigId
     * @param sourceRecipientId
     * @param sourceRecipientName
     * @param sourceCreated
     * @param sourceExpires
     */
    public EventAlertDisplay(Long id, String name, Long salesId,
            String salesName, Long managerId, String managerName,
            boolean alert, Long eventId, Long eventType, String eventName,
            Long eventConfigId, Date eventCreated, Date eventExpires,
            Long recipientId, String recipientName,
            Long sourceId,
            Long sourceConfigId,
            Long sourceRecipientId,
            String sourceRecipientName,
            Date sourceCreated,
            Date sourceExpires) {
        super(id, name, salesId, salesName, managerId, managerName, alert,
                eventId, eventType, eventName, eventConfigId, eventCreated,
                eventExpires, recipientId, recipientName);
        this.sourceId = sourceId;
        this.sourceConfigId = sourceConfigId;
        this.sourceRecipientId = sourceRecipientId;
        this.sourceRecipientName = sourceRecipientName;
        this.sourceCreated = sourceCreated;
        this.sourceExpires = sourceExpires;
    }

    public Long getSourceConfigId() {
        return sourceConfigId;
    }

    public Date getSourceCreated() {
        return sourceCreated;
    }

    public Date getSourceExpires() {
        return sourceExpires;
    }

    public Long getSourceId() {
        return sourceId;
    }

    public Long getSourceRecipientId() {
        return sourceRecipientId;
    }

    public String getSourceRecipientName() {
        return sourceRecipientName;
    }

    // setters protected, display is readonly

    protected void setSourceConfigId(Long sourceConfigId) {
        this.sourceConfigId = sourceConfigId;
    }

    protected void setSourceCreated(Date sourceCreated) {
        this.sourceCreated = sourceCreated;
    }

    protected void setSourceExpires(Date sourceExpires) {
        this.sourceExpires = sourceExpires;
    }

    protected void setSourceId(Long sourceId) {
        this.sourceId = sourceId;
    }

    protected void setSourceRecipientId(Long sourceRecipientId) {
        this.sourceRecipientId = sourceRecipientId;
    }

    protected void setSourceRecipientName(String sourceRecipientName) {
        this.sourceRecipientName = sourceRecipientName;
    }
}
