/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 27, 2013 2:33:35 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.BackendException;
import com.osserp.common.Constants;
import com.osserp.common.dao.Tables;

import com.osserp.core.BusinessNote;
import com.osserp.core.NoteType;
import com.osserp.core.dao.BusinessNotes;
import com.osserp.core.model.BusinessNoteImpl;
import com.osserp.core.model.BusinessNoteTypeImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class BusinessNotesDao extends AbstractRepository implements BusinessNotes {
    private static Logger log = LoggerFactory.getLogger(BusinessNotesDao.class.getName());

    /**
     * Creates businessNotes dao
     * @param jdbcTemplate
     * @param tables
     * @param sessionFactory
     */
    protected BusinessNotesDao(JdbcTemplate jdbcTemplate, Tables tables, SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
    }

    public NoteType getType(String name) {
        StringBuilder hql = new StringBuilder("from ");
        hql.append(BusinessNoteTypeImpl.class.getName()).append(" obj where obj.name = :typeName");
        try {
            List<NoteType> types = getCurrentSession().createQuery(hql.toString())
                    .setParameter("typeName", name)
                    .list();
            return types.isEmpty() ? null : types.get(0);
        } catch (Exception e) {
            log.error("getType() failed [name=" + name + ", message=" + e.getMessage() + "]", e);
            throw new BackendException("note.type.missing");
        }
    }

    public List<NoteType> getTypes() {
        StringBuilder hql = new StringBuilder("from ");
        hql.append(BusinessNoteTypeImpl.class.getName()).append(" obj order by obj.name");
        try {
            return getCurrentSession().createQuery(hql.toString()).list();
        } catch (Exception e) {
            log.error("getTypes() failed [message=" + e.getMessage() + "]", e);
            return new ArrayList<NoteType>();
        }
    }

    public BusinessNote findByMessageId(String messageId) {
        StringBuilder hql = new StringBuilder("from ");
        hql
            .append(BusinessNoteImpl.class.getName())
            .append(" obj where obj.messageId = :messageid");
        try {
            List<BusinessNote> notes = getCurrentSession().createQuery(hql.toString())
                    .setParameter("messageid", messageId)
                    .list();
            return notes.isEmpty() ? null : notes.get(0);
        } catch (Exception e) {
            log.error("findByMessageId() failed [messageId=" + messageId + ", message=" + e.getMessage() + "]", e);
            return null;
        }
    }

    public List<BusinessNote> findByReference(String typeName, Long referenceId) {
        StringBuilder hql = new StringBuilder("from ");
        hql
            .append(BusinessNoteImpl.class.getName())
            .append(" obj where obj.type.name = :typeName and ")
            .append("obj.reference = :referenceId order by obj.created desc");
        try {
            return getCurrentSession().createQuery(hql.toString())
                    .setParameter("typeName", typeName)
                    .setParameter("referenceId", referenceId)
                    .list();
        } catch (Exception e) {
            log.error("findByReference() failed [type=" + typeName + ", referenceId=" + referenceId + ", message=" + e.getMessage() + "]", e);
            return new ArrayList<BusinessNote>();
        }
    }

    public BusinessNote findSticky(String typeName, Long referenceId) {
        StringBuilder hql = new StringBuilder("from ");
        hql
            .append(BusinessNoteImpl.class.getName())
            .append(" obj where obj.type.name = :typeName and ")
            .append("obj.reference = :referenceId and obj.sticky = true order by obj.created desc");
        try {
            List<BusinessNote> notes = getCurrentSession().createQuery(hql.toString())
                    .setParameter("typeName", typeName)
                    .setParameter("referenceId", referenceId)
                    .list();
            return notes.isEmpty() ? null : notes.get(0);
        } catch (Exception e) {
            return null;
        }
    }

    public BusinessNote create(
            NoteType type,
            Long reference,
            Long createdBy,
            String note,
            String headline) {
        BusinessNoteImpl obj = new BusinessNoteImpl(
                type,
                reference,
                (createdBy != null ? createdBy : Constants.SYSTEM_EMPLOYEE),
                note,
                headline);
        getCurrentSession().save(BusinessNoteImpl.class.getName(), obj);
        return obj;
    }

    public BusinessNote create(
            NoteType type,
            Long reference,
            Long createdBy,
            String message,
            String subject,
            String messageId,
            boolean received) {
        BusinessNoteImpl obj = new BusinessNoteImpl(
                type,
                reference,
                (createdBy != null ? createdBy : Constants.SYSTEM_EMPLOYEE),
                message,
                subject,
                messageId,
                received);
        getCurrentSession().save(BusinessNoteImpl.class.getName(), obj);
        return obj;
    }

    public void delete(BusinessNote note) {
        getCurrentSession().delete(note);
    }

    public BusinessNote save(BusinessNote note) {
        getCurrentSession().saveOrUpdate(BusinessNoteImpl.class.getName(), note);
        return note;
    }
}
