/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.gui.client.calc.impl;

import com.osserp.common.BackendException;
import com.osserp.core.calc.Calculation;
import com.osserp.core.calc.CalculationManager;
import com.osserp.core.calc.CalculationSearchResult;
import com.osserp.core.sales.SalesOfferManager;
import com.osserp.core.sales.SalesOrderManager;
import com.osserp.gui.client.calc.CalculationAwareView;
import com.osserp.gui.client.impl.AbstractBusinessCaseAwareView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractCalculationAwareView extends AbstractBusinessCaseAwareView implements CalculationAwareView {

    protected AbstractCalculationAwareView() {
        super(true);
    }

    public Calculation getCalculation() {
        if (getBean() == null || !(getBean() instanceof Calculation)) {
            throw new BackendException("no calculation found");
        }
        return (Calculation) getBean();
    }
    
    public boolean isCalculationHistoryAvailable() {
        return (getList().size() > 1 && 
                (getList().get(0) instanceof CalculationSearchResult
                || getList().get(0) instanceof Calculation));
    }

    protected CalculationManager getCalculationManager() {
        return (CalculationManager) getService(CalculationManager.class.getName());
    }

    protected SalesOfferManager getOfferManager() {
        return (SalesOfferManager) getService(SalesOfferManager.class.getName());
    }

    protected SalesOrderManager getOrderManager() {
        return (SalesOrderManager) getService(SalesOrderManager.class.getName());
    }

}
