<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${sessionScope.mailingListView}" />

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="queriesAndActions" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <c:set var="oneDisplayed" value="false" />
        <o:permission role="customer_export,organisation_admin" info="permissionCustomerExport">
            <div class="row">
                <div class="col-md-7">
                    <div class="form-group">
                        <v:link url="/customers/customerQueryIndex/forward?type=listWithoutAction&exit=/mailingListsForward.do">
                            <fmt:message key="customerlistWithoutAction" />
                        </v:link>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-7">
                    <div class="form-group">
                        <v:link url="/customers/customerQueryIndex/forward?type=listWithoutOrder&exit=/mailingListsForward.do">
                            <fmt:message key="customerlistWithoutOrder" />
                        </v:link>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-7">
                    <div class="form-group">
                        <v:link url="/customers/customerQueryIndex/forward?type=listWithoutSales&exit=/mailingListsForward.do">
                            <fmt:message key="customerlistWithoutSales" />
                        </v:link>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-7">
                    <div class="form-group">
                        <v:link url="/customers/customerQueryIndex/forward?type=listLastSalesBefore&exit=/mailingListsForward.do">
                            <fmt:message key="customerlistLastSalesBefore" />
                        </v:link>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                    </div>
                </div>
            </div>
            <c:set var="oneDisplayed" value="true" />
        </o:permission>

        <oc:systemPropertyEnabled name="contactImportAvailable">
            <o:permission role="contact_import,executive" info="permissionContactImport">
                <div class="row next">
                    <div class="col-md-7">
                        <div class="form-group">
                             <v:link url="/contacts/contactImport/forward?exit=/mailingLists.do?method=forward">
                                 <fmt:message key="contactImport" />
                             </v:link>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <c:set var="oneDisplayed" value="true" />
                        </div>
                    </div>
                </div>
            </o:permission>
        </oc:systemPropertyEnabled>

        <o:permission role="contact_export,executive" info="permissionContactExport">
            <div class="row next">
                <div class="col-md-7">
                    <div class="form-group">
                        <v:link url="/contacts/contactExport/forward?exit=/mailingLists.do?method=forward">
                            <fmt:message key="contactExport" />
                        </v:link>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <c:set var="oneDisplayed" value="true" />
                    </div>
                </div>
            </div>
        </o:permission>

        <o:permission role="newsletter_config,executive" info="permissionConfigureSendNewsletter">
            <div class="row next">
                <div class="col-md-7">
                    <div class="form-group">
                        <v:link url="/mail/newsletterConfig/forward?exit=/mailingLists.do?method=forward">
                            <fmt:message key="newsletterConfigurationDispatch" />
                        </v:link>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <c:set var="oneDisplayed" value="true" />
                    </div>
                </div>
            </div>
        </o:permission>

        <c:if test="${!oneDisplayed}">
            <div class="row">
                <div class="col-md-7">
                    <div class="form-group">
                        <fmt:message key="noActionsAvailableOnCurrentPermissionState" />
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                    </div>
                </div>
            </div>
        </c:if>
    </div>
</div>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <c:choose>
                    <c:when test="${view.createMode}">
                        <fmt:message key="newMailingList" />
                    </c:when>
                    <c:when test="${view.editMode}">
                        <fmt:message key="changeLabel" />: <fmt:message key="mailingLists" />
                    </c:when>
                    <c:otherwise>
                        <fmt:message key="mailingLists" />
                    </c:otherwise>
                </c:choose>
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <c:choose>
            <c:when test="${view.createMode}">
                <o:form name="mailingListForm" url="/mailingLists.do">
                    <input type="hidden" name="method" value="save" />
                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-group">
                                <input type="text" name="name" class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <o:submit styleClass="form-control" onclick="setMethod('save');" value="create" />
                            </div>
                        </div>
                    </div>
                </o:form>
            </c:when>
            <c:when test="${view.editMode}">
                <o:form name="mailingListForm" url="/mailingLists.do">
                    <input type="hidden" name="method" value="save" />
                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-group">
                                <input type="text" name="name" value="<o:out value="${view.bean.name}"/>" class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <o:submit styleClass="form-control" onclick="setMethod('save');" />
                            </div>
                        </div>
                    </div>
                </o:form>
            </c:when>
            <c:otherwise>
                <div class="table-responsive table-responsive-default">
                    <table class="table">
                        <tbody>
                            <c:forEach var="obj" items="${view.list}">
                                <tr>
                                    <td>
                                        <a href="<c:url value="/mailingLists.do?method=select&id=${obj.id}"/>"><o:out value="${obj.name}" /></a>
                                    </td>
                                    <c:set var="recipients" value="${obj.recipients}" />
                                    <o:permission role="mailing_lists_edit,executive" info="permissionChangeMailingListName">
                                        <td class="icon center">
                                            <a href="<c:url value="/mailingLists.do?method=enableEdit&id=${obj.id}"/>" title="<fmt:message key="changeName"/>"><o:img name="writeIcon" /></a>
                                        </td>
                                    </o:permission>
                                    <td class="icon center">
                                        <c:if test="${!empty recipients and obj.active}">
                                            <a href="mailto:<o:out value="${recipients}"/>" title="<fmt:message key="mailToThisMailingList"/>"><o:img name="mailIcon" /></a>
                                        </c:if>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </c:otherwise>
        </c:choose>
    </div>
</div>
