<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${sessionScope.recordExportView}"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title"><o:displayTitle/></tiles:put>

    <tiles:put name="styles" type="string">
        <style type="text/css">
        <c:import url="/pages/records/record_export.css"/>
        </style>
    </tiles:put>
    
    <tiles:put name="headline">
        <fmt:message key="recordJournalFrom"/> <o:date value="${view.bean.created}"/> - <fmt:message key="type"/> <oc:options name="recordTypes" value="${view.type}"/> 
    </tiles:put>
    <tiles:put name="headline_right">
        <ul>
            <li><a href="<c:url value="/recordExports.do?method=exitExportDisplay"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a></li>
            <li><a href="<c:url value="/recordExports.do?method=xls"/>" target="_blank" title="<fmt:message key="printXml"/>"><o:img name="tableIcon"/></a></li>
            <li><a href="<c:url value="/recordExports.do?method=download"/>" target="_blank" title="<fmt:message key="downloadDocumentsAsArchiveTitle"/>"><o:img name="downloadIcon"/></a></li>
            <%-- TODO port to spring mvc, create stylesheet "record_export.vm" and invoke new pdf creator 
            <li><a href="<c:url value="/record/exports/print"/>" title="<fmt:message key="printPdf"/>"><o:img name="printIcon" styleClass="icon"/></a></li>
            --%>
            <li><v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link></li>
        </ul>
    </tiles:put>
    
    <tiles:put name="content" type="string">
        <div class="content-area" id="voucherExportDisplayContent">
            <div class="row">
                <div class="col-md-12 panel-area">
                    <div class="table-responsive table-responsive-default">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th class="recordId"><fmt:message key="number"/></th>
                                    <th class="recordContactId"><fmt:message key="customerId"/></th>
                                    <th class="recordContact"><fmt:message key="customer"/></th>
                                    <th class="recordDate"><fmt:message key="from"/></th>
                                    <th class="amount"><fmt:message key="amount"/></th>
                                    <th class="amount"><fmt:message key="turnoverTax"/></th>
                                    <th class="amount reducedTax"><fmt:message key="reducedTurnoverTax"/></th>
                                    <th class="amount"><fmt:message key="gross"/></th>
                                    <th class="action"> </th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="record" items="${view.bean.records}" varStatus="s">
                                    <tr>
                                        <c:choose>
                                            <c:when test="${view.cancellationSelected}">
                                                <td class="recordId"><a href="<c:url value="/salesCancellation.do?method=display&exit=journal&readonly=true&id=${record.id}"/>"><o:out value="${record.number}"/></a></td>
                                            </c:when>
                                            <c:when test="${view.creditNoteSelected}">
                                                <td class="recordId"><a href="<c:url value="/salesCreditNote.do?method=display&exit=journal&readonly=true&id=${record.id}"/>"><o:out value="${record.number}"/></a></td>
                                            </c:when>
                                            <c:when test="${view.downpaymentSelected}">
                                                <td class="recordId"><a href="<c:url value="/salesInvoice.do?method=displayDownpayment&exit=journal&readonly=true&id=${record.id}"/>"><o:out value="${record.number}"/></a></td>
                                            </c:when>
                                            <c:otherwise>
                                                <td class="recordId"><a href="<c:url value="/salesInvoice.do?method=display&exit=journal&readonly=true&id=${record.id}"/>"><o:out value="${record.number}"/></a></td>
                                            </c:otherwise>
                                        </c:choose>
                                        <td class="recordContactId"><o:out value="${record.contactId}"/></td>
                                        <td class="recordContact"><o:out value="${record.contactName}"/></td>
                                        <td class="recordDate"><o:date value="${record.created}"/></td>
                                        <c:choose>
                                            <c:when test="${view.downpaymentSelected}">
                                                <td class="amount"><o:number value="${record.prorateNetAmount}" format="currency"/></td>
                                                <td class="amount"><o:number value="${record.prorateTaxAmount}" format="currency"/></td>
                                                <td class="amount reducedTax">0,00</td>
                                                <td class="amount"><o:number value="${record.prorateGrossAmount}" format="currency"/></td>
                                            </c:when>
                                            <c:otherwise>
                                                <td class="amount"><o:number value="${record.netAmount}" format="currency"/></td>
                                                <td class="amount"><o:number value="${record.taxAmount}" format="currency"/></td>
                                                <td class="amount reducedTax"><o:number value="${record.reducedTaxAmount}" format="currency"/></td>
                                                <td class="amount"><o:number value="${record.grossAmount}" format="currency"/></td>
                                            </c:otherwise>
                                        </c:choose>
                                        <td class="action">
                                            <c:if test="${record.unchangeable and !empty record.typeName}">
                                                <v:link url="/records/recordPrint/print?name=${record.typeName}&id=${record.printId}" title="documentPrint"><o:img name="printIcon" /></v:link>
                                            </c:if>
                                        </td>
                                    </tr>
                                </c:forEach>
                                <tr>
                                    <th class="recordId"><fmt:message key="summary"/></th>
                                    <th class="recordContactId"> </th>
                                    <th class="recordContact"> </th>
                                    <th class="recordDate"> </th>
                                    <th class="amount"><o:number value="${view.bean.netAmount}" format="currency"/></th>
                                    <th class="amount"><o:number value="${view.bean.taxAmount}" format="currency"/></th>
                                    <th class="amount reducedTax"><o:number value="${view.bean.reducedTaxAmount}" format="currency"/></th>
                                    <th class="amount"><o:number value="${view.bean.grossAmount}" format="currency"/></th>
                                    <th class="action"> </th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
