/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.ods

import com.osserp.common.ds.Sheet2Table

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class Ods2TableUtil {

    static String createTableName(String fileName) {
        return fileName.substring(0, fileName.indexOf('.'))
    }

    static String createTableCommand(String name, String[] columnNames) {
        StringBuilder sql = new StringBuilder()
        sql
                .append('CREATE TABLE ')
                .append(name)
                .append(' (')
        int columnCount = columnNames.length
        for (int i = 0; i < columnCount; i++) {
            sql.append(columnNames[i]).append(' VARCHAR(255)')
            if (i < columnCount-1) {
                sql.append(',')
            }
        }
        sql.append(');')
        return sql.toString()
    }

    static String createTableInsert(String name, String[] columnNames) {
        StringBuilder sql = new StringBuilder()
        sql.append('INSERT INTO ').append(name).append(' (')
        int columnCount = columnNames.length
        for (int i = 0; i < columnCount; i++) {
            sql.append(columnNames[i])
            if (i < columnCount-1) {
                sql.append(',')
            }
        }
        sql.append(') VALUES (')
        for (int i = 0; i < columnCount; i++) {
            sql.append('?')
            if (i < columnCount-1) {
                sql.append(',')
            }
        }
        sql.append(');')
        return sql.toString()
    }

    static String createNextTableInsert(Sheet2Table file) {
        // String name, String[] columnNames
        StringBuilder sql = new StringBuilder()
        sql.append('INSERT INTO ').append(file.getTableName()).append(' (')
        int columnCount = file.columnNames.length
        for (int i = 0; i < columnCount; i++) {
            sql.append(file.columnNames[i])
            if (i < columnCount-1) {
                sql.append(',')
            }
        }
        sql.append(') VALUES (')
        Object[] values = file.getNextRow()
        for (int i = 0; i < columnCount; i++) {
            def val = createMaskedValue(values[i])
            sql.append('\'').append(val ?: '').append('\'')
            if (i < columnCount-1) {
                sql.append(',')
            }
        }
        sql.append(');')
        return sql.toString()
    }

    static String createMaskedValue(Object value) {
        if (!value) {
            return ''
        }
        value = value.replaceAll('\\\\') { a -> a = '' }
        value = value.replaceAll("'") { a -> a = '\\\'' }
        value.toString()
    }
}

