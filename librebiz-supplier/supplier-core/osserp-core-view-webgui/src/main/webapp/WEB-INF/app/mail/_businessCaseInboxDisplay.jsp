<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="message" /> - <fmt:message key="detail" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="received"><fmt:message key="dateTime" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:date value="${view.receivedMail.receivedDate}" addtime="true" />
                    </div>
                </div>
            </div>

            <c:import url="${viewdir}/mail/_receivedMailAddresses.jsp"/>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="status"><fmt:message key="status" /></label>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${empty view.receivedMail.businessId}">
                                <fmt:message key="notAssignedLabel" />
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="assignedLabel" />
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                        <c:choose>
                            <c:when test="${empty view.receivedMail.businessId}">
                                <v:link url="${view.baseLink}/setBusinessCase" title="assign">
                                    <o:img name="enabledIcon" />
                                </v:link>
                            </c:when>
                            <c:otherwise>
                                <v:link url="${view.baseLink}/resetBusinessCase" title="reset">
                                    <o:img name="deleteIcon" />
                                </v:link>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <c:if test="${!empty view.receivedMail.attachments}">
                <c:choose>
                    <c:when test="${!empty view.receivedMail.businessId}">
                        <div class="row next">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="attachments">
                                        <fmt:message key="attachments" />
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <div class="radio">
                                        <label for="attachmentAction">
                                            <input type="radio" name="attachmentAction" value="1" checked="checked" />
                                            <span title="<fmt:message key="defaultSetting" />">
                                                <fmt:message key="moveAttachmentsByType" />
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <div class="radio">
                                        <label for="attachmentAction">
                                            <input type="radio" name="attachmentAction" value="2" />
                                            <span> <fmt:message key="moveAttachmentsByTypeAndDeleteMessage" /></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <div class="radio">
                                        <label for="attachmentAction">
                                            <input type="radio" name="attachmentAction" value="3" />
                                            <span> <fmt:message key="moveMessageOnlyDeleteAttachments" /></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <c:forEach var="attachment" items="${view.receivedMail.attachments}" varStatus="s">
                            <div class="row<c:if test="${s.index == 0}"> next</c:if>">
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <v:link url="${view.baseLink}/ignoreAttachment?attachmentId=${attachment.id}" title="deleteAndIgnoreAttachmentTitle" >
                                            <o:img name="toggleStoppedTrueIcon" />
                                        </v:link>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <v:link url="${view.baseLink}/deleteAttachment?attachmentId=${attachment.id}" title="delete" confirm="true" message="confirmDeleteDocument">
                                            <o:img name="deleteIcon" />
                                        </v:link>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <v:link url="${view.baseLink}/moveAttachmentToPictures?attachmentId=${attachment.id}" title="moveAttachmentToPictures">
                                            <o:img name="snapshotIcon" />
                                        </v:link>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <v:link url="${view.baseLink}/moveAttachmentToDocuments?attachmentId=${attachment.id}" title="moveAttachmentToDocuments">
                                            <o:img name="letterIcon" />
                                        </v:link>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label for="recipients">
                                            <o:out value="${attachment.fileName}" />
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <v:link url="${view.baseLink}/downloadAttachment?attachmentId=${attachment.id}" title="downloadLabel">
                                            <o:img name="printIcon" />
                                        </v:link>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </c:when>
                    <c:otherwise>
                        <c:forEach var="attachment" items="${view.receivedMail.attachments}" varStatus="s">
                            <div class="row<c:if test="${s.index == 0}"> next</c:if>">
                                <div class="col-md-4">
                                    <div class="form-group">
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label for="recipients">
                                            <o:out value="${attachment.fileName}" />
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <v:link url="${view.baseLink}/downloadAttachment?attachmentId=${attachment.id}" title="downloadLabel">
                                            <o:img name="printIcon" />
                                        </v:link>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
            </c:if>

            <c:if test="${!empty view.receivedMail.businessId}">
                <div class="row next">
                    <div class="col-md-4"></div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <v:submitExit url="${view.baseLink}/delete" styleClass="form-control error" value="deleteFetchedMail"  />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <o:submit value="createAsNote" styleClass="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </c:if>

        </div>
    </div>
</div>
