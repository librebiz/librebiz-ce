/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 18, 2010 3:02:43 PM 
 * 
 */
package com.osserp.core.service.contacts;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.mail.Mail;

import com.osserp.core.BusinessNote;
import com.osserp.core.NoteAware;
import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.ContactNoteManager;
import com.osserp.core.dao.BusinessNotes;
import com.osserp.core.dao.Contacts;
import com.osserp.core.dao.Employees;
import com.osserp.core.dao.MailTemplates;
import com.osserp.core.employees.Employee;
import com.osserp.core.mail.MailMessageContext;
import com.osserp.core.mail.MailTemplate;
import com.osserp.core.model.NoteAwareVO;
import com.osserp.core.service.impl.AbstractNoteManager;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ContactNoteManagerImpl extends AbstractNoteManager implements ContactNoteManager {
    private static Logger log = LoggerFactory.getLogger(ContactNoteManagerImpl.class.getName());
    private Contacts contacts = null;

    protected ContactNoteManagerImpl(BusinessNotes businessNotes, MailTemplates mailTemplates, Employees employees, Contacts contacts) {
        super(businessNotes, mailTemplates, employees);
        this.contacts = contacts;
    }

    @Override
    public NoteAware load(Long primaryKey) {
        return create(contacts.loadContact(primaryKey));
    }

    public MailMessageContext setupMail(Employee user, NoteAware noteAware, Map<String, String> opts) throws ClientException {
        MailTemplate mtpl = getMailTemplate();
        String originator = getOriginator(user, mtpl);
        String message = opts.get("message");
        String[] recipients = getRecipients(mtpl.getRecipients());
        String recipient = recipients != null && recipients.length > 0 ? recipients[0] : null;
        Contact contact = getContact(noteAware.getPrimaryKey());
        if (recipient == null && contact.getPersonCreatedBy() != null) {
            recipient = getMailAddress(contact.getPersonCreatedBy());
        }
        Mail mail = null;
        try {
            mail = addRemainingRecipients(new Mail(originator, recipient, "Info: " + noteAware.getName(), message), recipients);
        } catch (ClientException e) {
            log.warn("createMailSetup() ignoring exception [message=" + e.getMessage() + "]");
            mail = new Mail(originator, "Info: " + noteAware.getName(), message);
        }
        return new MailMessageContext(mtpl, mail, opts);
    }

    public NoteAware create(Contact contact) {
        List<BusinessNote> notes = getBusinessNotes().findByReference(getTypeName(), contact.getContactId());
        return new NoteAwareVO(contact.getContactId(), contact.getName(), notes);
    }

    protected Contact getContact(Long id) {
        Contact contact = contacts.loadContact(id);
        if (contact == null) {
            throw new IllegalArgumentException("contact not exists: " + id);
        }
        return contact;
    }

    @Override
    protected String getTypeName() {
        return "contactNote";
    }
}
