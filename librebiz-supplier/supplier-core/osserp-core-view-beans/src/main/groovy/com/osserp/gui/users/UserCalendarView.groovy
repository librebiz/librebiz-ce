/**
 *
 * Copyright (C) 2003, 2008, 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 9, 2016
 * 
 */
package com.osserp.gui.users

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.Appointment
import com.osserp.common.Calendar
import com.osserp.common.ClientException
import com.osserp.common.beans.DefaultAppointment
import com.osserp.common.util.CalendarUtil
import com.osserp.common.util.StringUtil

import com.osserp.core.events.Event
import com.osserp.core.events.EventManager

import com.osserp.gui.common.CalendarAwareView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class UserCalendarView extends CalendarAwareView {
    private static Logger logger = LoggerFactory.getLogger(UserCalendarView.class.getName())

    UserCalendarView() {
        super();
    }

    @Override
    protected List<Appointment> fetchAppointments() {
        List result = []
        List<Event> events = eventManager.findAppointmentsByRecipient(domainUser.employee.id)
        events.each { Event next ->

            Appointment appointment = new DefaultAppointment(
                    next.id,
                    next.referenceId,
                    next.description,
                    next.action.name,
                    fetchMessage(next),
                    createLinkWithTarget(next.link),
                    (next.appointmentDate == null ? next.activation : next.appointmentDate))
            result.add(appointment)
        }
        return result
    }

    private String createLinkWithTarget(String link) {
        StringBuilder lnk = new StringBuilder(link)
        StringUtil.replace(lnk, 'todo', 'calendarStartup')
        lnk.toString()
    }

    private String fetchMessage(Event event) {
        StringBuilder message = new StringBuilder()
        if (event.message) {
            message.append(event.message).append('\n')
        }
        if (event.notes?.size() > 0) {
            message.append(event.notes.get(0).note)
        }
        message.toString()
    }

    protected EventManager getEventManager() {
        getService(EventManager.class.getName())
    }
}
