<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="productGroups" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive table-responsive-default">
            <table class="table">
                <tbody>
                    <c:if test="${!empty view.selectedProductGroup}">
                        <tr>
                            <td><span class="boldtext"><o:out value="${view.selectedProductGroup.name}" /></span></td>
                        </tr>
                    </c:if>
                    <c:forEach var="obj" items="${view.productGroups}">
                        <c:if test="${view.selectedProductGroup.id != obj.id}">
                            <tr>
                                <td><v:link url="${view.baseLink}/selectProductGroup?id=${obj.id}" title="selectionProductGroups">
                                        <o:out value="${obj.name}" />
                                    </v:link></td>
                            </tr>
                        </c:if>
                    </c:forEach>
                </tbody>
            </table>
        </div>

    </div>
</div>
