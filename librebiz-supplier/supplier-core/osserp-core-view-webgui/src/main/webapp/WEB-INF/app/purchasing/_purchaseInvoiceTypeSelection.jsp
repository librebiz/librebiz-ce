<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="recordAndBookingTypeSelection" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive table-responsive-default">
            <table class="table table-striped">
                <tbody>
                    <c:forEach var="type" items="${view.invoiceTypes}">
                        <tr>
                            <td class="center"><o:out value="${type.key}" /></td>
                            <td><v:link url="/purchasing/purchaseInvoiceCreator/selectType?id=${type.id}">
                                    <o:out value="${type.name}" />
                                </v:link></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>

