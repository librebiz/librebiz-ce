<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="flowControlNoteView"/>
<c:set var="businessCase" value="${view.businessCase}"/>
<c:set var="fcs" value="${view.selectedItem}"/>

<div class="modalBoxHeader">
    <div class="modalBoxHeaderLeft">
        <c:choose>
            <c:when test="${fcs.action.displayReverse}">
                <o:out value="${fcs.headline}"/>
            </c:when>
            <c:otherwise>
                <o:out value="${fcs.action.name}"/>
            </c:otherwise>
        </c:choose>
    </div>
</div>
<div class="modalBoxData">
    <div class="subcolumns">
        <div class="subcolumn">
            <div class="spacer"></div>
            
            <v:ajaxForm name="flowControlNote" url="/fcs/flowControlNote/save">
            
                <p><o:date value="${fcs.created}"/> / <oc:employee value="${fcs.employeeId}"/></p>
                <c:if test="${!empty fcs.closing.name}">
                <p><fmt:message key="status" />: <o:out value="${fcs.closing.name}"/></p>
                </c:if>
                <p>
                    <c:choose>
                        <c:when test="${empty fcs.note}">
                            <fmt:message key="noNotesAvailable"/>
                        </c:when>
                        <c:otherwise>
                            <o:textOut value="${fcs.note}"/>
                        </c:otherwise>
                    </c:choose>
                </p>
                <p><fmt:message key="addition" />:</p>
                <p>
                    <v:textarea styleClass="form-control" name="note" rows="8"/>
                </p>
                <p>
                    <input type="submit" value="<fmt:message key="save"/>" style="float:right;"/>
                </p>
                <br />
            </v:ajaxForm>
        </div>
    </div>
</div>
