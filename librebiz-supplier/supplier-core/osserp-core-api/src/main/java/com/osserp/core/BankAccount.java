/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 06.12.2004 
 * 
 */
package com.osserp.core;

import java.util.Date;
import java.util.List;

import com.osserp.common.Mappable;
import com.osserp.common.Option;
import com.osserp.common.dms.DmsDocument;

/**
 * 
 * @author cf <cf@osserp.com>
 * @author tn <tn@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public interface BankAccount extends Option, Mappable {

    static final Long PRIVATE = 1L;
    static final Long BUSINESS = 2L;

    /**
     * Provides an addon of the name
     * @return nameAddon
     */
    String getNameAddon();

    /**
     * Sets a name addon
     * @param nameAddon
     */
    void setNameAddon(String nameAddon);

    /**
     * Provides the owner name if differs from contact name
     * @return ownerName
     */
    String getOwnerName();

    /**
     * Sets the owner name if differs from contact name
     * @param ownerName
     */
    void setOwnerName(String ownerName);

    /**
     * @return Returns the international bank account number.
     */
    String getBankAccountNumberIntl();

    /**
     * @param bankAccountNumberIntl The international bank account number to set.
     */
    void setBankAccountNumberIntl(String bankAccountNumberIntl);

    /**
     * @return Returns the international bank identification code.
     */
    String getBankIdentificationCodeIntl();

    /**
     * @param bankIdentificationCodeIntl The international bank identification code to set.
     */
    void setBankIdentificationCodeIntl(String bankIdentificationCodeIntl);

    /**
     * @return Returns the direct debit authorization.
     */
    boolean isDirectDebitAuth();

    /**
     * @param directDebitAuth The direct debit authorization to set.
     */
    void setDirectDebitAuth(boolean directDebitAuth);

    /**
     * @return Returns the direct debit authorization date.
     */
    Date getDirectDebitAuthDate();

    /**
     * @param directDebitAuthDate The direct debit authorization date to set.
     */
    void setDirectDebitAuthDate(Date directDebitAuthDate);

    /**
     * Indicates if bankAccount should be print in document footers
     * @return useInDocumentFooter
     */
    boolean isUseInDocumentFooter();

    /**
     * Sets if bankAccount should be print in document footers
     * @param useInDocumentFooter
     */
    void setUseInDocumentFooter(boolean useInDocumentFooter);

    /**
     * Indicates if this account is a cash account or cash register
     * @return true if cash account
     */
    boolean isCashAccount();

    /**
     * Sets if this bankaccount is a cash account or cash register
     * @param cashAccount
     */
    void setCashAccount(boolean cashAccount);

    /**
     * Indicates if this bankaccount is -also- used to send and receive taxes
     * @return taxAccount
     */
    boolean isTaxAccount();

    /**
     * Sets if this bankaccount is -also- used to send and receive taxes
     * @param taxAccount
     */
    void setTaxAccount(boolean taxAccount);

    /**
     * Provides a shortname/key for the account
     * @return shortkey
     */
    String getShortkey();

    /**
     * Sets a shortname/key for the account
     * @param shortkey
     */
    void setShortkey(String shortkey);
    
    /**
     * Provides the id of the supplier providing the bank account
     * @return supplierId
     */
    Long getSupplierId();
    
    /**
     * Sets the id of the supplier providing the bank account
     * @param supplierId
     */
    void setSupplierId(Long supplierId);
    
    /**
     * Provides a list of associated documents. Note: The list is not 
     * persistent by default and is exclusively used by gui objects only. 
     * @return list of account documents
     */
    List<DmsDocument> getAccountDocuments();
}
