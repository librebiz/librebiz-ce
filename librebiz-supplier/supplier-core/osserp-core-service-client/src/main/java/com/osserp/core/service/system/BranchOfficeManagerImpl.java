/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 7, 2010 9:53:13 AM 
 * 
 */
package com.osserp.core.service.system;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.OptionsCache;
import com.osserp.common.util.CollectionUtil;
import com.osserp.common.util.StringUtil;

import com.osserp.core.Options;
import com.osserp.core.dao.BranchOffices;
import com.osserp.core.dao.SystemConfigs;
import com.osserp.core.dms.CoreStylesheetService;
import com.osserp.core.service.impl.AbstractService;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.system.BranchOfficeManager;
import com.osserp.core.system.SystemCompany;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class BranchOfficeManagerImpl extends AbstractService implements BranchOfficeManager {
    private static Logger log = LoggerFactory.getLogger(BranchOfficeManagerImpl.class.getName());
    
    private static final String SHORTKEY_MAXLENGTH_NAME = "companyBranchMaxLengthShortkey";
    private static final int SHORTKEY_MAXLENGTH_DEFAULT = 3;

    private BranchOffices branchOffices = null;
    private SystemConfigs systemConfigs = null;
    private CoreStylesheetService coreStylesheetService = null;
    private OptionsCache optionsCache = null;

    protected BranchOfficeManagerImpl(
            BranchOffices branchOffices, 
            SystemConfigs systemConfigs,
            CoreStylesheetService coreStylesheetService,
            OptionsCache optionsCache) {
        super();
        this.branchOffices = branchOffices;
        this.systemConfigs = systemConfigs;
        this.coreStylesheetService = coreStylesheetService;
        this.optionsCache = optionsCache;
    }

    public BranchOffice find(Long id) {
        try {
            return branchOffices.getBranchOffice(id);
        } catch (Exception e) {
            if (log.isDebugEnabled()) {
                log.debug("find() not found [id=" + id + "]");
            }
            return null;
        }
    }

    public List<BranchOffice> findAll() {
        return branchOffices.getBranchOffices();
    }

    public List<BranchOffice> findByCompany(Long id) {
        return branchOffices.getByCompany(id);
    }

    public List<BranchOffice> findByCustomer(Long customerContactId) {
        List<BranchOffice> existing = branchOffices.getBranchOffices();
        BranchOffice exclude = (BranchOffice) CollectionUtil.getByProperty(existing, "contactId", customerContactId);
        List<BranchOffice> result = new ArrayList<BranchOffice>();
        for (int i = 0, j = existing.size(); i < j; i++) {
            BranchOffice next = existing.get(i);
            // TODO remove test
            if (!next.isEndOfLife()
                    && (exclude == null || !next.getCompany().getId().equals(exclude.getCompany().getId()))
                    && (!next.isStatuaryOffice() || systemConfigs.isSystemPropertyEnabled("companyLtdRecordsEnabled"))) {
                if (next.isStatuaryOffice()) {
                    log.debug("findByCustomer() found statuary office");
                }
                result.add(next);
            }
        }
        return result;
    }

    public BranchOffice findByContact(Long contactId) {
        return branchOffices.getByContact(contactId);
    }

    public BranchOffice update(
            BranchOffice branchOffice,
            String name,
            SystemCompany company,
            String shortkey,
            String shortname,
            String email,
            String phoneCountry,
            String phonePrefix,
            String phoneNumber,
            Long costCenter,
            Boolean headquarter,
            Boolean thirdparty,
            Boolean customTemplates,
            Boolean recordTemplatesDefault) throws ClientException {

        validateNames(name, shortkey, shortname);
        
        boolean shortnameChanged = !shortname.equals(branchOffice.getShortname());
        boolean shortkeyChanged = !shortkey.equals(branchOffice.getShortkey());
        boolean nameChanged = !name.equals(branchOffice.getName());
        
        if (shortnameChanged || shortkeyChanged || nameChanged) {
            
            validateExisting(
                    branchOffice.getId(), 
                    shortnameChanged ? shortname : null, 
                    shortkeyChanged ? shortkey : null, 
                    shortnameChanged ? shortname : null);
        }
        if (company == null && branchOffice.getCompany() == null) {
            throw new ClientException(ErrorCode.BRANCH_MISSING);
        }
        if (company != null && (branchOffice.getCompany() == null 
                || !branchOffice.getCompany().getId().equals(company.getId()))) {
            branchOffice.setCompany(company);
        }
        String shortnameValue = !shortnameChanged ? shortname : StringUtil.deleteBlanks(shortname);
        branchOffice.setName(name);
        branchOffice.setShortkey(shortkey);
        branchOffice.setShortname(shortnameValue.toLowerCase());
        branchOffice.setEmail(email);
        branchOffice.setPhoneCountry(phoneCountry);
        branchOffice.setPhonePrefix(phonePrefix);
        branchOffice.setPhoneNumber(phoneNumber);
        branchOffice.setHeadquarter(headquarter);
        branchOffice.setCostCenter(costCenter);
        branchOffice.updateThirdparty(thirdparty, customTemplates, recordTemplatesDefault);
        branchOffices.save(branchOffice);
        if (branchOffice.isCustomTemplates() && coreStylesheetService != null) {
            coreStylesheetService.createCustomTemplatePathIfRequired(branchOffice);
        }
        reloadCaches();
        return branchOffice;
    }

    public BranchOffice create(BranchOffice branchOffice) throws ClientException {
        validateNames(branchOffice.getName(), branchOffice.getShortkey(), branchOffice.getShortname());
        validateExisting(null, branchOffice.getName(), branchOffice.getShortkey(), branchOffice.getShortname());
        
        branchOffices.save(branchOffice);
        reloadCaches();
        return branchOffices.getBranchOffice(branchOffice.getId());
    }
    
    protected void reloadCaches() {
        if (optionsCache != null) {
            try {
                optionsCache.refresh(Options.BRANCH_OFFICES);
                optionsCache.refresh(Options.BRANCH_OFFICE_KEYS);
            } catch (Exception e) {
                log.error("reloadCaches() ignoring exception [message=" + e.getMessage() + "]", e);
            }
        }
    }
    
    public int getShortkeyMaxlength() {
        Long val = systemConfigs.getSystemPropertyId(SHORTKEY_MAXLENGTH_NAME);
        return isSet(val) ? val.intValue() : SHORTKEY_MAXLENGTH_DEFAULT;
    }

    /**
     * Checks for name collission with existing branches. All params optional, null == OK.
     * @param existing id of the existing branch requesting the validation. Null if new branch.
     * @param name
     * @param shortkey
     * @param shortname
     * @throws ClientException
     */
    private void validateExisting(Long existing, String name, String shortkey, String shortname) throws ClientException {
        List<BranchOffice> all = findAll();
        for (int i = 0, j = all.size(); i < j; i++) {
            BranchOffice next = all.get(i);
            if (isSet(shortname) && shortname.equalsIgnoreCase(next.getShortname())) {
                if (isNotSet(existing) || !existing.equals(next.getId())) {
                    throw new ClientException(ErrorCode.SHORTNAME_EXISTS);
                }
            }
            if (isSet(shortkey) && shortkey.equalsIgnoreCase(next.getShortkey())) {
                if (isNotSet(existing) || !existing.equals(next.getId())) {
                    throw new ClientException(ErrorCode.SHORTKEY_EXISTS);
                }
            }
            if (isSet(name) && name.equalsIgnoreCase(next.getName())) {
                if (isNotSet(existing) || !existing.equals(next.getId())) {
                    throw new ClientException(ErrorCode.NAME_EXISTS);
                }
            }
        }
    }
    
    /**
     * Checks if none of provided names is null or empty and shortkey length not exceeds max.
     * @param name
     * @param shortkey
     * @param shortname
     * @throws ClientException
     */
    private void validateNames(String name, String shortkey, String shortname) throws ClientException {
        
        if (isNotSet(shortname)) {
            throw new ClientException(ErrorCode.SHORTNAME_MISSING);
        }
        if (StringUtil.containsUmlaut(shortname)) {
            // shortname is used as directory - we don't allow special chars
            throw new ClientException(ErrorCode.SHORTNAME_UMLAUT);
        }

        if (isNotSet(shortkey)) {
            throw new ClientException(ErrorCode.SHORTKEY_MISSING);
        }
        if (shortkey.length() > getShortkeyMaxlength()) {
            throw new ClientException(ErrorCode.SHORTKEY_MAXLENGTH);
        }

        if (isNotSet(name)) {
            throw new ClientException(ErrorCode.NAME_MISSING);
        }
    }
}
