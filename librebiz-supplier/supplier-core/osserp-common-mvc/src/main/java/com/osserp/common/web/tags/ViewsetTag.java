/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 24, 2009 3:19:13 PM 
 * 
 */
package com.osserp.common.web.tags;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.web.View;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
//TODO check if required
public class ViewsetTag extends AbstractViewActionTag {
    private static Logger log = LoggerFactory.getLogger(ViewsetTag.class.getName());

    private String name = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private Object value = null;

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    protected void execute() throws Exception {
        View view = lookupView();
        if (view != null) {
            view.setLocalValue(name, value);
            if (log.isDebugEnabled()) {
                log.debug("execute() added value [view=" + view.getName()
                        + ", name=" + name
                        + ", value=" + (value == null ? "null" :
                                (value instanceof String ? (String) value :
                                        (value instanceof Integer || value instanceof Long)
                                                ? value.toString() : value.getClass().getName()
                                )
                        )
                        + "]");
            }
        }
    }

    @Override
    public void release() {
        super.release();
        this.name = null;
        this.value = null;
    }
}
