<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<v:view viewName="requestDetailsView" />
<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/popup.jsp" flush="true">
    <tiles:put name="headline">
        <fmt:message key="${view.headerName}" />
    </tiles:put>
    <%-- <tiles:put name="styles" type="string"></tiles:put> --%>
    <tiles:put name="content" type="string">
        <v:ajaxForm url="/requests/requestDetails/save" targetElement="businessDetails_popup" onSuccess="" preRequest="" postRequest="">
        <c:import url="${viewdir}/common/_businessDetailsForm.jsp"/>
        </v:ajaxForm>
    </tiles:put>
</tiles:insert>
