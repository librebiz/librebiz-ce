/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.gui.client.impl;

import javax.servlet.http.HttpServletRequest;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.util.NumberUtil;
import com.osserp.common.web.RequestUtil;
import com.osserp.core.system.SystemConfigManager;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractView extends AbstractDomainView {

    private String serverUrl = null;

    /**
     * Initializes view name, actionUrl and selectionUrl as annotated and initializes action target as 'success'
     */
    protected AbstractView() {
        super();
    }

    /**
     * This method is invoked by view manager after initial bean creation. Override if view depends on values provided by servlet request. Default
     * implementation assigns local server url property.
     * @param request
     * @throws ClientException if a validation of request or session params/attributes failed
     * @throws PermissionException if current user has no permission to initialize the view
     */
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        serverUrl = RequestUtil.getServerURL(request);
        String exitId = RequestUtil.getExitId(request);
        if (isSet(exitId)) {
            setExitId(exitId);
        }
    }

    protected final Long getSystemPropertyId(String name) {
        SystemConfigManager systemConfigManager = (SystemConfigManager) getService(SystemConfigManager.class.getName());
        String value = systemConfigManager.getSystemProperty(name);
        return NumberUtil.createLong(value);
    }

    /**
     * Provides the complete server url. Note: The view retrieves the url during the invocation of {@link #init(HttpServletRequest)} so it's not available if a
     * subclass locally overrides the init method without explicit calling super's init.
     * @return serverUrl or null if not initialized
     */
    protected String getServerUrl() {
        return serverUrl;
    }

    /**
     * Sets the server url
     * @param serverUrl
     */
    protected void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }
}
