/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 4, 2010 9:09:32 AM 
 * 
 */
package com.osserp.core.service.contacts;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.Option;
import com.osserp.core.dao.Employees;
import com.osserp.core.employees.Employee;
import com.osserp.core.employees.EmployeeGroup;
import com.osserp.core.employees.EmployeeGroupManager;
import com.osserp.core.service.impl.AbstractService;
import com.osserp.core.tasks.EmployeeUpdateSender;

/**
 * @author Rainer Kirchner <rk@osserp.com>
 */
public class EmployeeGroupManagerImpl extends AbstractService implements EmployeeGroupManager {
    private static Logger log = LoggerFactory.getLogger(EmployeeGroupManagerImpl.class.getName());

    private Employees employees = null;
    private EmployeeUpdateSender ldapSyncSender = null;

    protected EmployeeGroupManagerImpl(Employees employees, EmployeeUpdateSender ldapSyncSender) {
        super();
        this.employees = employees;
        this.ldapSyncSender = ldapSyncSender;
    }

    public List<Option> findGroupDisplayByPermission(String permission) {
        return employees.findGroupsByPermission(permission);
    }

    public List<EmployeeGroup> getGroups() {
        return employees.getEmployeeGroups();
    }

    public EmployeeGroup getGroup(Long id) {
        return employees.getEmployeeGroup(id);
    }

    public EmployeeGroup createGroup(Employee user, String name, String key)
            throws ClientException {
        EmployeeGroup group = employees.createGroup(user, name, key);
        synchronize(group);
        return group;
    }

    public void delete(EmployeeGroup group) {
        employees.delete(group);
        synchronize(group);
    }

    public void save(EmployeeGroup group) {
        EmployeeGroup existing = employees.getEmployeeGroup(group.getId());
        if (existing != null && existing.getLdapGroup() != null && group.getLdapGroup() == null) {
            // don't override asynchronously set ldap reference
            group.setLdapGroup(existing.getLdapGroup());
        }
        employees.save(group);
        synchronize(group);
    }

    public Map<Long, EmployeeGroup> getLdapMap() {
        return employees.getLdapGroupMap();
    }

    public void synchronizeLdapGroups() {
        if (log.isDebugEnabled()) {
            log.debug("synchronizeLdapGroups() invoked");
        }
        ldapSyncSender.send((EmployeeGroup) null);
    }

    private void synchronize(EmployeeGroup group) {
        try {
            if (ldapSyncSender != null) {
                ldapSyncSender.send(group);
            }
        } catch (Throwable t) {
            log.warn("synchronize() ignoring failure on attempt to send ldap synchronization request [message=" + t.getMessage() + "]");
        }
    }
}
