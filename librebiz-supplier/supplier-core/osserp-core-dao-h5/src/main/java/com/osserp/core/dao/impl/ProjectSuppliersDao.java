/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.dao.Tables;

import com.osserp.core.BusinessCase;
import com.osserp.core.dao.ProjectSuppliers;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.employees.Employee;
import com.osserp.core.model.projects.ProjectSupplierImpl;
import com.osserp.core.model.records.PurchaseDeliveryNoteImpl;
import com.osserp.core.model.records.PurchaseInvoiceImpl;
import com.osserp.core.model.records.PurchaseOrderDeliveryDateChangedDeleted;
import com.osserp.core.model.records.PurchaseOrderDeliveryDateChangedInfoImpl;
import com.osserp.core.model.records.PurchaseOrderImpl;
import com.osserp.core.model.records.PurchaseOrderItemChangedDeleted;
import com.osserp.core.model.records.PurchaseOrderItemChangedInfoImpl;
import com.osserp.core.projects.ProjectSupplier;
import com.osserp.core.suppliers.Supplier;


/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProjectSuppliersDao extends AbstractRepository implements ProjectSuppliers {

    private String productGroupsTable;
    private String projectSuppliersTable;
    private String purchaseInvoicesTable;
    private String purchaseOrdersTable;

    /**
     * Creates a new project suppliers dao
     * @param jdbcTemplate
     * @param tables
     * @param sessionFactory
     */
    protected ProjectSuppliersDao(JdbcTemplate jdbcTemplate, Tables tables, SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
        productGroupsTable = getTable(TableKeys.PRODUCT_GROUPS);
        projectSuppliersTable = getTable(TableKeys.PROJECT_SUPPLIERS);
        purchaseInvoicesTable = getTable(TableKeys.PURCHASE_INVOICES);
        purchaseOrdersTable = getTable(TableKeys.PURCHASE_ORDERS);
    }

    public ProjectSupplier create(
            Employee user,
            BusinessCase businessCase,
            Supplier supplier,
            String groupName,
            Date deliveryDate) {
        ProjectSupplier ps = new ProjectSupplierImpl(
                user,
                businessCase,
                supplier,
                groupName,
                deliveryDate);
        save(ps);
        return ps;
    }

    public ProjectSupplier createDefault(
            Supplier supplier,
            Date created,
            Long createdBy,
            Long sales,
            Long voucherId,
            Long voucherType,
            String groupName,
            Date deliveryDate) {
        ProjectSupplierImpl ps = new ProjectSupplierImpl(
                supplier,
                created,
                createdBy,
                sales,
                voucherId,
                voucherType,
                groupName,
                deliveryDate);
        save(ps);
        return ps;
    }

    public ProjectSupplier getProjectSupplier(Long id) {
        return (ProjectSupplierImpl) getCurrentSession().load(
                ProjectSupplierImpl.class.getName(), id);
    }

    public ProjectSupplier findByVoucher(Long voucherType, Long voucherId) {
        ProjectSupplier supplier = null;
        if (voucherType != null && voucherId != null) {
            StringBuilder hql = new StringBuilder();
            hql
                    .append("from ")
                    .append(ProjectSupplierImpl.class.getName())
                    .append(" o where o.recordTypeId = :voucherType and o.recordId = :voucherId");
            List<ProjectSupplier> existing = getCurrentSession()
                    .createQuery(hql.toString())
                    .setParameter("voucherType", voucherType)
                    .setParameter("voucherId", voucherId)
                    .list();
            supplier = existing.isEmpty() ? null : existing.get(0);
        }
        return supplier;
    }

    public List<ProjectSupplier> getSuppliers(Long salesId) {
        StringBuilder hql = new StringBuilder();
        hql
                .append("from ")
                .append(ProjectSupplierImpl.class.getName())
                .append(" o where o.reference = :salesId order by o.id");
        return getCurrentSession().createQuery(hql.toString())
                .setParameter("salesId", salesId).list();
    }

    public void remove(ProjectSupplier supplier, boolean includePurchaseOrder) {
        if (supplier.isPurchaseInvoiceAssigned()) {
            if (purchaseInvoiceExists(supplier.getRecordId())) {
                PurchaseInvoiceImpl obj = getCurrentSession().load(
                        PurchaseInvoiceImpl.class, supplier.getRecordId());
                obj.deleteBusinessCaseId();
                getCurrentSession().saveOrUpdate(obj);
                if (obj.getReference() != null && purchaseOrderExists(obj.getReference())) {
                    PurchaseOrderImpl pobj = getCurrentSession().load(
                            PurchaseOrderImpl.class, obj.getReference());
                    pobj.deleteBusinessCaseId();
                    getCurrentSession().saveOrUpdate(pobj);
                }
            }
        } else if (supplier.isPurchaseOrderAssigned()
                && purchaseOrderExists(supplier.getRecordId())) {

            PurchaseOrderImpl obj = getCurrentSession().load(
                    PurchaseOrderImpl.class, supplier.getRecordId());
            if (includePurchaseOrder && !obj.isClosed()) {
                List<PurchaseDeliveryNoteImpl> dlns = getCurrentSession().createQuery(
                        "from " + PurchaseDeliveryNoteImpl.class.getName()
                                + " dn where dn.reference = :referenceId")
                                .setParameter("referenceId", obj.getId()).list();
                if (dlns.isEmpty()) {
                    List<PurchaseOrderDeliveryDateChangedDeleted> podd = getCurrentSession().createQuery(
                            "from " + PurchaseOrderDeliveryDateChangedDeleted.class.getName()
                                    + " dn where dn.reference = :referenceId")
                                    .setParameter("referenceId", obj.getId()).list();
                    if (!podd.isEmpty()) {
                        for (Iterator<PurchaseOrderDeliveryDateChangedDeleted> i = podd.iterator(); i.hasNext();) {
                            getCurrentSession().delete(i.next());
                        }
                    }
                    List<PurchaseOrderDeliveryDateChangedInfoImpl> podi = getCurrentSession().createQuery(
                            "from " + PurchaseOrderDeliveryDateChangedInfoImpl.class.getName()
                                    + " dn where dn.reference = :referenceId")
                                    .setParameter("referenceId", obj.getId()).list();
                    if (!podi.isEmpty()) {
                        for (Iterator<PurchaseOrderDeliveryDateChangedInfoImpl> i = podi.iterator(); i.hasNext();) {
                            getCurrentSession().delete(i.next());
                        }
                    }
                    List<PurchaseOrderItemChangedInfoImpl> poici = getCurrentSession().createQuery(
                            "from " + PurchaseOrderItemChangedInfoImpl.class.getName()
                                    + " dn where dn.reference = :referenceId")
                                    .setParameter("referenceId", obj.getId()).list();
                    if (!poici.isEmpty()) {
                        for (Iterator<PurchaseOrderItemChangedInfoImpl> i = poici.iterator(); i.hasNext();) {
                            getCurrentSession().delete(i.next());
                        }
                    }
                    List<PurchaseOrderItemChangedDeleted> poicd = getCurrentSession().createQuery(
                            "from " + PurchaseOrderItemChangedDeleted.class.getName()
                                    + " dn where dn.reference = :referenceId")
                                    .setParameter("referenceId", obj.getId()).list();
                    if (!poicd.isEmpty()) {
                        for (Iterator<PurchaseOrderItemChangedDeleted> i = poicd.iterator(); i.hasNext();) {
                            getCurrentSession().delete(i.next());
                        }
                    }
                    getCurrentSession().delete(obj);

                } else {
                    obj.deleteBusinessCaseId();
                    getCurrentSession().saveOrUpdate(obj);
                }
            } else {
                obj.deleteBusinessCaseId();
                getCurrentSession().saveOrUpdate(obj);
            }
        }
        getCurrentSession().delete(ProjectSupplierImpl.class.getName(), supplier);
    }

    protected boolean purchaseInvoiceExists(Long id) {
        StringBuilder sql = new StringBuilder("SELECT count(*) FROM ");
        sql.append(purchaseInvoicesTable).append(" WHERE id = ").append(id);
        return jdbcTemplate.queryForObject(sql.toString(), Integer.class).intValue() > 0;
    }

    protected boolean purchaseOrderExists(Long id) {
        StringBuilder sql = new StringBuilder("SELECT count(*) FROM ");
        sql.append(purchaseOrdersTable).append(" WHERE id = ").append(id);
        return jdbcTemplate.queryForObject(sql.toString(), Integer.class).intValue() > 0;
    }

    public void save(ProjectSupplier supplier) {
        getCurrentSession().saveOrUpdate(ProjectSupplierImpl.class.getName(), supplier);
    }

    public Set<String> findGroupNames() {
        StringBuilder sql = new StringBuilder();
        sql
            .append("SELECT name FROM ")
            .append(productGroupsTable)
            .append(" WHERE name IS NOT NULL")
            .append(" UNION SELECT group_name as name FROM ")
            .append(projectSuppliersTable)
            .append(" WHERE group_name IS NOT NULL");
        return new HashSet((List<String>) jdbcTemplate.query(
                sql.toString(), getStringRowMapper()));
    }
}
