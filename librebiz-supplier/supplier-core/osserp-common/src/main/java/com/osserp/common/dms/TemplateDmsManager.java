/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 4, 2009 
 * 
 */
package com.osserp.common.dms;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.FileObject;
import com.osserp.common.User;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface TemplateDmsManager {
    
    /**
     * Creates a new template document
     * @param user
     * @param filename
     * @param fileObject
     * @param note
     * @param categoryId
     * @return document
     * @throws ClientException if validation failed
     */
    DmsDocument create(
            User user,
            String filename,
            FileObject fileObject,
            String note,
            Long categoryId) throws ClientException; 

    /**
     * Deletes a document
     * @param document
     * @throws ClientException if document not accessible
     */
    void delete(DmsDocument document) throws ClientException;
    
    /**
     * Provides the supported document type
     * @return supported type
     */
    DocumentType getSupportedType();

    /**
     * Gets the document with given primary key id
     * @param id
     * @return document
     * @throws ClientException if no such document exists
     */
    DmsDocument getDocument(Long id) throws ClientException;

    /**
     * Finds all documents related to supported type
     * @return documents by reference
     */
    List<DmsDocument> findDocuments();

    /**
     * Updates an existing document using a file object as input
     * @param user
     * @param document
     * @param file
     * @param note
     * @param category
     * @throws ClientException
     */
    void updateDocument(
            User user, 
            DmsDocument document, 
            FileObject file, 
            String note, 
            Long category) throws ClientException;

    /**
     * Updates an existing document category
     * @param user
     * @param id
     * @param category or null to reset
     */
    void updateDocument(User user, Long id, Long category);

}
