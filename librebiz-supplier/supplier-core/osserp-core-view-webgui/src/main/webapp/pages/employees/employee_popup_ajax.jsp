<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>

<c:set var="contact" value="${sessionScope.employee}" />
<table class="table" style="width: 300px; background-color: #FFFFFF; text-align: left; margin: 0;">
    <c:choose>
        <c:when test="${empty contact}">
            <tr>
                <th><fmt:message key="unknown" /></th>
            </tr>
            <tr>
                <td><fmt:message key="noEmployeeInformationsAvailable" /></td>
            </tr>
            <tr>
                <td><fmt:message key="employeeDataCalledDueToAnEntryThenTheEntryWasMadeByTheAdministratorOrAutomatedByAScript" /></td>
            </tr>
        </c:when>
        <c:otherwise>
            <tr>
                <th><o:out value="${contact.id}" /></th>
                <th><o:out value="${contact.firstName}" /> <o:out value="${contact.lastName}" /></th>
            </tr>
            <tr>
                <td style="border: 0px;"><fmt:message key="street" /></td>
                <td style="border: 0px;"><o:out value="${contact.address.street}" /></td>
            </tr>
            <tr>
                <td style="border: 0px;"><fmt:message key="city" /></td>
                <td style="border: 0px;"><o:out value="${contact.address.zipcode}" /> <o:out value="${contact.address.city}" /></td>
            </tr>
            <tr>
                <td style="border: 0px;"><fmt:message key="email" /></td>
                <td style="border: 0px;" class="small"><o:email value="${contact.email}" /></td>
            </tr>
            <c:if test="${!empty contact.internalPhone}">
                <tr>
                    <td style="border: 0px;"><fmt:message key="office" /></td>
                    <td style="border: 0px;"><oc:phone value="${contact.internalPhone}" /></td>
                </tr>
                <tr>
                    <td style="border: 0px;"><fmt:message key="directDial" /></td>
                    <td style="border: 0px;"><o:out value="${contact.internalPhone.internal}" /></td>
                </tr>
            </c:if>
            <c:if test="${!empty contact.phone.number}">
                <tr>
                    <td style="border: 0px;"><fmt:message key="private" /></td>
                    <td style="border: 0px;"><oc:phone value="${contact.phone}" /></td>
                </tr>
            </c:if>
            <c:if test="${!empty contact.mobile.number}">
                <tr>
                    <td style="border: 0px;"><fmt:message key="mobile" /></td>
                    <td style="border: 0px;"><oc:phone value="${contact.mobile}" /></td>
                </tr>
            </c:if>
            <c:if test="${!empty contact.branch}">
                <tr>
                    <td style="border: 0px;"><fmt:message key="branch" /></td>
                    <td style="border: 0px;"><o:out value="${contact.branch.name}" /></td>
                </tr>
            </c:if>
            <c:remove var="employee" scope="session" />
        </c:otherwise>
    </c:choose>
</table>