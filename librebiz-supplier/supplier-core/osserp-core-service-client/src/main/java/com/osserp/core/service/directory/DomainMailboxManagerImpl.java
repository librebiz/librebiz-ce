/**
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 15, 2013 5:26:33 PM
 * 
 */
package com.osserp.core.service.directory;

import java.util.List;
import java.util.Map;

import com.osserp.common.ClientException;
import com.osserp.common.directory.DirectoryException;
import com.osserp.common.directory.DirectoryMailboxManager;
import com.osserp.common.directory.DirectoryManager;
import com.osserp.common.directory.DirectoryUser;
import com.osserp.common.directory.FetchmailAccount;

import com.osserp.core.directory.DomainMailboxManager;
import com.osserp.core.system.SystemConfigManager;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class DomainMailboxManagerImpl extends AbstractDirectoryService implements DomainMailboxManager {

    private DirectoryMailboxManager directoryMailboxManager;

    protected DomainMailboxManagerImpl() {
        super();
    }

    protected DomainMailboxManagerImpl(
            SystemConfigManager systemConfigManager,
            DirectoryManager directoryManager,
            DirectoryMailboxManager directoryMailboxManager) {
        super(systemConfigManager, directoryManager);
        this.directoryMailboxManager = directoryMailboxManager;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.directory.DomainMailboxManager#createMailboxUserAttributes()
     */
    public Map createMailboxUserAttributes() {
        return directoryMailboxManager.createMailboxUserAttributes(null);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.directory.DomainMailboxManager#createMailbox(java.util.Map, java.util.Map)
     */
    public DirectoryUser createMailbox(Map userValues, Map mailboxValues) throws ClientException {
        try {
            return directoryMailboxManager.createMailbox(userValues, mailboxValues, getDirectoryClientName());
        } catch (DirectoryException e) {
            throw new ClientException(e.getMessage());
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.directory.DomainMailboxManager#findMailbox(com.osserp.common.directory.DirectoryUser)
     */
    public FetchmailAccount findMailbox(DirectoryUser user) {
        return directoryMailboxManager.findMailbox(user);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.directory.DomainMailboxManager#findMailboxUsers()
     */
    public List<DirectoryUser> findMailboxUsers() {
        return directoryMailboxManager.findMailboxUsers(getDirectoryClientName());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.directory.DomainMailboxManager#update(com.osserp.common.directory.FetchmailAccount, java.util.Map)
     */
    public FetchmailAccount update(FetchmailAccount account, Map values) {
        return directoryMailboxManager.update(account, values);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.directory.DomainMailboxManager#activateMailbox(com.osserp.common.directory.FetchmailAccount)
     */
    public FetchmailAccount activateMailbox(FetchmailAccount account) {
        return directoryMailboxManager.updateMailboxActivation(account, true);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.directory.DomainMailboxManager#deactivateMailbox(com.osserp.common.directory.FetchmailAccount)
     */
    public FetchmailAccount deactivateMailbox(FetchmailAccount account) {
        return directoryMailboxManager.updateMailboxActivation(account, false);
    }

}
