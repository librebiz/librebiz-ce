<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<c:set var="contact" value="${requestScope.contactPopupObject}"/>

<div class="modalBoxHeader">
	<div class="modalBoxHeaderLeft">
		<c:choose>
			<c:when test="${empty contact}"><fmt:message key="unknown"/></c:when>
			<c:otherwise><o:out value="${contact.lastName}"/> [<o:out value="${contact.id}"/>]</c:otherwise>
		</c:choose>
	</div>
</div>
<div class="modalBoxData">
	<div class="subcolumns">
		<div class="subcolumn">
			<div class="spacer"></div>
			<table class="valueTable">
				<tbody>
					<c:choose>
						<c:when test="${empty contact}">
							<tr>
								<td><fmt:message key="noContactInformationAvailable"/></td>
							</tr>
							<tr>
								<td><fmt:message key="noConcreteContactAssigned"/></td>
							</tr>
						</c:when>
						<c:otherwise>
							<tr>
								<td><fmt:message key="street"/></td>
								<td><o:out value="${contact.address.street}"/></td>
							</tr>
							<tr>
								<td><fmt:message key="city"/></td>
								<td><o:out value="${contact.address.zipcode}"/> <o:out value="${contact.address.city}"/></td>
							</tr>
							<tr>
								<td><fmt:message key="phone"/></td>
								<td><oc:phone value="${contact.phone}"/></td>
							</tr>
							<tr>
								<td><fmt:message key="fax"/></td>
								<td><oc:phone value="${contact.fax}"/></td>
							</tr>
							<tr>
								<td><fmt:message key="mobile"/></td>
								<td><oc:phone value="${contact.mobile}"/></td>
							</tr>
							<tr>
								<td><fmt:message key="email"/></td>
								<td><o:email value="${contact.email}" limit="32"/></td>
							</tr>
						</c:otherwise>
					</c:choose>
				</tbody>
			</table>
			<div class="spacer"></div>
		</div>
	</div>
</div>
<c:remove var="contactPopupObject" scope="request"/>
