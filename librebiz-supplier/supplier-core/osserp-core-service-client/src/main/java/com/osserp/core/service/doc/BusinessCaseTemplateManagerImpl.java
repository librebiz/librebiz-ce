/**
 *
 * Copyright (C) 2015 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 3, 2015 
 * 
 */
package com.osserp.core.service.doc;


import org.jdom2.Document;

import com.osserp.common.ClientException;
import com.osserp.common.dms.TemplateDmsManager;

import com.osserp.core.BusinessCase;
import com.osserp.core.BusinessCaseTemplateManager;
import com.osserp.core.BusinessTemplate;
import com.osserp.core.dao.BusinessTemplates;
import com.osserp.core.dao.SystemConfigs;
import com.osserp.core.dms.CoreStylesheetService;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class BusinessCaseTemplateManagerImpl extends AbstractBusinessTemplateManager
        implements BusinessCaseTemplateManager {

    private CoreDocumentDataProvider documentDataProvider;

    /**
     * Creates a new business template manager
     * @param businessTemplates
     * @param stylesheetService
     * @param templateDmsManager
     * @param systemConfigs
     * @param documentDataProvider
     */
    protected BusinessCaseTemplateManagerImpl(
            BusinessTemplates businessTemplates,
            CoreStylesheetService stylesheetService,
            TemplateDmsManager templateDmsManager,
            SystemConfigs systemConfigs,
            CoreDocumentDataProvider documentDataProvider) {
        super(businessTemplates, stylesheetService, templateDmsManager, systemConfigs);
        this.documentDataProvider = documentDataProvider;
    }

    public byte[] createPdf(
            DomainUser domainUser,
            BusinessTemplate businessTemplate,
            BusinessCase businessCase)
            throws ClientException {

        Document xmlDocument = documentDataProvider.getData(
                domainUser, businessTemplate, businessCase);
        logXML(xmlDocument);
        return createPdf(businessTemplate, xmlDocument, businessCase.getBranch().getId(), null);
    }

}
