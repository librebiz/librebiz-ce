/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.gui.admin.crm

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.Option

import com.osserp.core.crm.Campaign

import com.osserp.groovy.web.MenuItem

import com.osserp.gui.crm.AbstractCampaignView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class CampaignConfigView extends AbstractCampaignView {
    private static Logger logger = LoggerFactory.getLogger(CampaignConfigView.class.getName())

    List<Option> availableParents = []
    Campaign parent

    CampaignConfigView() {
        super()
        enableAutoreload()
        providesEditMode()
        providesCreateMode()
        providesCustomNavigation()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (forwardRequest) {
            list = campaignManager.find()
            env.includeEol = false
        }
    }

    @Override
    void createCustomNavigation() {

        if (changeParentMode) {
            nav.beanListNavigation = [changeParentExitLink, homeLink]
        } else if (campaignDeletable) {
            nav.beanListNavigation = [selectExitLink, deleteLink, editLink, homeLink]
        } else {
            nav.beanListNavigation = [selectExitLink, editLink, homeLink]
        }

        if (parent) {
            nav.listNavigation = [parentExitLink, toggleDisplayEolLink, homeLink]
            nav.defaultNavigation = [parentExitLink, homeLink]
        } else {
            nav.defaultNavigation = [exitLink, createLink, toggleDisplayEolLink, homeLink]
            nav.listNavigation = [exitLink, createLink, toggleDisplayEolLink, homeLink]
        }
    }

    @Override
    public void reload() {
        super.reload()
        groups = campaignManager.findGroups()
        types = campaignManager.findTypes()
    }

    MenuItem getParentExitLink() {
        navigationLink("/${context.name}/selectParent", 'backIcon', 'backToLast')
    }

    MenuItem getDeleteLink() {
        navigationLink("/${context.name}/delete", 'deleteIcon', 'deleteCampaign')
    }

    MenuItem getChangeParentExitLink() {
        navigationLink("/${context.name}/disableChangeParentMode", 'backIcon', 'backToLast')
    }

    @Override
    void enableEditMode() {
        super.enableEditMode()
        if (bean?.company) {
            branchOfficeList = systemConfigManager.getBranchs(bean.company)
        }
    }

    @Override
    void disableEditMode() {
        super.disableEditMode()
        branchOfficeList = []
        selectedGroup = null
        selectedType = null
    }

    @Override
    void enableCreateMode() {
        super.enableCreateMode()
        if (parent) {
            if (parent.company && (!selectedCompany || selectedCompany.id != parent.company)) {
                selectedCompany = companies.find { it.id == parent.company }
                if (selectedCompany) {
                    branchOfficeList = systemConfigManager.getBranchs(selectedCompany.id)
                }
            }
            selectedGroup = parent.getGroup()
            selectedType = parent.getType()
        }
    }

    @Override
    void disableCreateMode() {
        super.disableCreateMode()
        if (multipleClientsAvailable) {
            branchOfficeList = []
            selectedCompany = null
        } else {
            List ltmp = branchOfficeList.findAll { it.activated }
            if (ltmp.size() > 1) {
                branchOfficeList = []
            }
        }
        selectedGroup = null
        selectedType = null
    }

    @Override
    void select() {
        super.select()
    }

    @Override
    void save() {
        String name = getString('name')
        String description = getString('description')

        Long groupId = getLong('group')
        Long typeId = getLong('type')
        selectedGroup = (groupId ? groups.find{ it.id == groupId } :null)
        selectedType = (typeId ? types.find{ it.id == typeId } :null)

        Long companyId = getLong('company')
        Long branchId = getLong('branch')
        if (branchId < 0) {
            branchId = null
        }
        Date start = getDate('campaignStarts')
        Date end = getDate('campaignEnds')

        String reach = getString('reach')
        if (!reach) {
            reach = Campaign.REACH_COMPANY_UP
        }
        Boolean eol = getBoolean('endOfLife')

        if (!name) {
            throw new ClientException(ErrorCode.NAME_MISSING)
        }

        if (createMode) {
            logger.debug('save: invoked in createMode')

            Campaign created
            if (parent) {
                logger.debug('save: found parent, creating sub-campaign...')
                created = campaignManager.create(
                        domainEmployee,
                        name,
                        reach,
                        description,
                        parent,
                        null,
                        branchId,
                        selectedGroup,
                        selectedType,
                        start,
                        end)

                list = campaignManager.find(parent.id)
            } else {
                logger.debug('save: no parent found, creating top level campaign...')
                created = campaignManager.create(
                        domainEmployee,
                        name,
                        reach,
                        description,
                        null,
                        selectedCompany.id,
                        branchId,
                        selectedGroup,
                        selectedType,
                        start,
                        end)
                list = campaignManager.find()
            }
            logger.debug("save: created new campaign [id=${created.id}]")
            disableCreateMode()
        } else {
            logger.debug('save: invoked in editMode')
            bean.name = name
            bean.description = description
            bean.branch = branchId
            bean.group = selectedGroup
            bean.type = selectedType
            bean.endOfLife = eol
            bean.campaignStarts = start
            bean.campaignEnds = end
            if (reach) {
                bean.reach = reach
            }
            campaignManager.update(bean)
            if (eol) {
                if (parent) {
                    list = campaignManager.find(parent.id)
                } else {
                    list = campaignManager.find()
                }
                bean = null
            }
            disableEditMode()
        }
    }

    def selectParent() {
        Long id = form.getLong('id')
        if (id) {
            parent = list.find { it.id == id }
            if (parent) {
                list = campaignManager.find(parent.id)
            }
        } else {
            if (parent?.reference) {
                list = campaignManager.find(parent.reference)
                parent = campaignManager.getCampaign(parent.reference)
            } else if (parent) {
                list = campaignManager.find()
                parent = null
            }
        }
    }

    boolean isChangeParentMode() {
        env.changeParentMode
    }

    void disableChangeParentMode() {
        env.changeParentMode = false
        availableParents = []
        createCustomNavigation()
    }

    void enableChangeParentMode() {
        if (bean) {
            availableParents = campaignManager.getAvailableParents(bean)
            if (availableParents) {
                env.changeParentMode = true
            }
        }
    }

    def changeParent() {
        Long id = form.getLong('id')
        if (id) {
            def selectedParent = campaignManager.getCampaign(id)
            if (campaignManager.isReferenced(selectedParent)) {
                throw new ClientException(ErrorCode.CAMPAIGN_ALREADY_REFERENCED)
            }
            bean = campaignManager.updateParent(bean, id)
            parent = campaignManager.getCampaign(id)
            list = campaignManager.find(id)
        }
        disableChangeParentMode()
    }

    def delete() {
        if (campaignDeletable) {
            campaignManager.delete(bean)
            bean = null
            if (parent) {
                list = campaignManager.find(parent.id)
            } else {
                list = campaignManager.find()
            }
        }
    }

    def getCampaignDeletable() {
        boolean result = false
        if (bean && bean instanceof Campaign && !bean.parent) {
            result = !campaignManager.isReferenced(bean)
            logger.debug("getCampaignDeletable: done [result=${result}]")
        }
        result
    }

    def getCampaignMovable() {
        (bean && bean instanceof Campaign && !bean.parent && !bean.reference)
    }
}
