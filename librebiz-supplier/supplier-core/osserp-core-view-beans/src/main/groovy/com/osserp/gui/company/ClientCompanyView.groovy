/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.osserp.gui.company


import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ErrorCode
import com.osserp.common.dms.DmsDocument
import com.osserp.common.dms.DmsReference

import com.osserp.core.system.BranchOffice
import com.osserp.core.system.SystemCompany

import com.osserp.groovy.web.MenuItem
import com.osserp.groovy.web.ViewContextException

/**
 * 
 * @author rk <rk@osserp.com>
 * @author cf <cf@osserp.com>
 * 
 */
public class ClientCompanyView extends AbstractCompanyAdminView {
    private static Logger logger = LoggerFactory.getLogger(ClientCompanyView.class.getName())


    ClientCompanyView() {
        dependencies = ['contactView', 'clientCreateView']
        providesEditMode()
        enableAutoreload()
        enableLogoPreloading()
        enableRegistrationPreloading()
        headerName = 'systemCompany'
        printConfigTargetAvailable = true
        changeCompanyLogoTargetAvailable = true
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (!bean) {
            Long id
            if (env.contactView?.bean || env.clientCreateView?.bean) {
                def contact = env.contactView?.bean ?: env.clientCreateView?.bean?.contact
                id = contact?.contactId
            } else if (form.params.id) {
                id = form.getLong('id')
            }
            load(id)
            if (!bean) {
                throw new ViewContextException(ErrorCode.INVALID_CONTEXT, this)
            }
            logger.debug("initRequest: done [id=${id}, multipleClientsAvailable=${multipleClientsAvailable}]")
        }
    }

    @Override
    void save() {

        Double internalMargin = form.getDouble('internalMargin')
        bean = systemCompanyManager.update(
                bean.id,
                form.getString('name'),
                form.getString('letterWindowName'),
                form.getString('localCourt'),
                form.getString('tradeRegisterEntry'),
                form.getString('taxId'),
                form.getString('vatId'),
                (multipleClientsAvailable ? internalMargin : bean.internalMargin))
        disableEditMode()
    }

    @Override
    protected void loadDependencies(SystemCompany comp, boolean assignBean) {
        super.loadDependencies(comp, assignBean)
        loadStock()
    }

    @Override
    void reload() {
        load(bean?.id)
    }

    @Override
    void loadStock() {
        if (!bean) {
            stock = null
        } else {
            stock = systemConfigManager.defaultStock
            if (!stock && stockManagementEnabled) {
                stock = systemConfigManager.createStock(domainUser, selectedCompany, null)
                logger.debug("loadStock: Stock not available, created new [id=${stock?.id}]")
            }
        }
    }

    @Override
    protected void createStock() {
        if (bean instanceof SystemCompany) {
            stock = systemConfigManager.createStock(domainUser, bean, null)
        }
    }

    @Override
    MenuItem getEditLink() {
        return context.popupView ? navigationLink("/${context.name}/enableEditMode", 'editIcon', 'edit') : navigationLink("/${context.name}/enableEditMode", 'writeIcon', 'edit', 'client_edit', 'clientEdit')
    }
}
