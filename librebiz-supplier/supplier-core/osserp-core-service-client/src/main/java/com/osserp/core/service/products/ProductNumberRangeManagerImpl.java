/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 7, 2009 8:42:35 AM 
 * 
 */
package com.osserp.core.service.products;

import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;

import com.osserp.core.dao.ProductNumberRanges;
import com.osserp.core.employees.Employee;
import com.osserp.core.products.ProductNumberRange;
import com.osserp.core.products.ProductNumberRangeManager;
import com.osserp.core.service.impl.AbstractService;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProductNumberRangeManagerImpl extends AbstractService implements ProductNumberRangeManager {

    private ProductNumberRanges numberRanges = null;

    protected ProductNumberRangeManagerImpl(ProductNumberRanges numberRanges) {
        super();
        this.numberRanges = numberRanges;
    }

    public ProductNumberRange createNumberRange(Employee user, String name, Long start, Long end) throws ClientException {
        return numberRanges.createNumberRange(user, name, start, end);
    }

    public List<ProductNumberRange> getNumberRanges() {
        return numberRanges.getNumberRanges();
    }

    public ProductNumberRange update(Employee user, ProductNumberRange range, String name, Long rangeStart, Long rangeEnd) throws ClientException {
        List<ProductNumberRange> existing = numberRanges.getNumberRanges();
        for (int i = 0, j = existing.size(); i < j; i++) {
            ProductNumberRange next = existing.get(i);
            if (next.getName().equals(range.getName()) && !next.getId().equals(range.getId())) {
                throw new ClientException(ErrorCode.NAME_EXISTS);
            }
        }
        range.setName(name);
        range.setRangeStart(rangeStart);
        range.setRangeEnd(rangeEnd);
        range.setChanged(new Date(System.currentTimeMillis()));
        range.setChangedBy(user != null ? user.getId() : Constants.SYSTEM_EMPLOYEE);
        numberRanges.save(range);
        return range;
    }

}
