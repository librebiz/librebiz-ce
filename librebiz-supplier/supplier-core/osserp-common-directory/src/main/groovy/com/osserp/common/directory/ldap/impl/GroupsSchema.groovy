/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.directory.ldap.impl

import com.osserp.common.directory.ldap.LdapConfig
import com.osserp.common.directory.ldap.StructuralSchema

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class GroupsSchema extends AbstractLdapConfigAware implements StructuralSchema {

    private static final List OBJECT_CLASSES = [
        'top',
        'posixGroup',
        'osserpGroup'
    ]

    static final String DEFAULT_SAMBA_GRP = '2'
    static final String DEFAULT_SAMBA_SID = 'S-1-5-21-3523851798-3655360463-2559965761-'

    GroupsSchema(LdapConfig configObj) {
        super(configObj)
    }

    /**
     * Creates a new ldap ready group object by default and values provided by map
     * @param values must contain at least 'name' for cn
     * @return group object without dn
     */
    @Override
    Map create(Map values) {
        String cn = values.cn
        Long idNumber = fetchLong(values.gidnumber)
        if (!cn || !idNumber) {
            return [ errors : (cn ? 'missing gidnumber' : 'missing cn')]
        }
        Map group = [
            objectclass: (config.groupObjectClasses ?: OBJECT_CLASSES),
            cn: cn,
            gidnumber: idNumber.toString(),
            description: (values.description ?: cn)
        ]
        if (config.sambaSupportEnabled) {
            group.sambagrouptype = 'sambaGroupMapping'
            group.sambasid = createSambaSID(idNumber)
        }
        return group
    }

    String getMinimumId() {
        config.groupIdStart ?: '6001'
    }

    String getQuery() {
        config.groupQuery ?: '(objectClass=osserpGroup)'
    }

    List<String> getObjectClasses() {
        config?.groupObjectClasses ?: OBJECT_CLASSES
    }

    @Override
    List<String> getAvailableAttributes() {
        def list = [
            'cn',
            'gidnumber',
            'description'
        ]
        if (config.sambaSupportEnabled) {
            list << 'sambagrouptype'
        }
        return list
    }

    Set<String> getAvailableArrays() {
        return new HashSet<String>()
    }


    @Override
    List<String> getDefaultDisplayAttributes() {
        [
            'cn',
            'gidnumber',
            'description'
        ]
    }

    private String createSambaSID(Long id) {
        StringBuilder sambaSID = new StringBuilder(DEFAULT_SAMBA_SID)
        sambaSID.append(id * 2 + 1001)
        return sambaSID.toString()
    }
}
