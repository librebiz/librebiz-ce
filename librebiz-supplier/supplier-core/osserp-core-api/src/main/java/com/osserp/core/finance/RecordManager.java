/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 15, 2006 8:25:18 PM 
 * 
 */
package com.osserp.core.finance;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.Option;
import com.osserp.core.BusinessCase;
import com.osserp.core.Item;
import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.employees.Employee;
import com.osserp.core.products.Product;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface RecordManager extends FinanceRecordManager {

    /**
     * The record type handled by the implementation 
     * @return recordType
     */
    RecordType getRecordType();

    /**
     * Provides stock by assigned company and branch office
     * @param record
     * @return assigned or default stock object
     */
    Stock getStockByRecord(Record record);

    /**
     * Checks whether a record of implementing class with given id exists
     * @param id
     * @return true if so
     */
    boolean exists(Long id);

    /**
     * Finds record by id
     * @param recordId
     * @return record or null if not exists
     */
    Record find(Long recordId);

    /**
     * Looks up for all sales related records of implementing type
     * @param salesId
     * @return records or empty list if none exist
     */
    List<RecordDisplay> findBySales(Long salesId);

    /**
     * Provides all unreleased records
     * @return unreleased records
     */
    List<Record> getUnreleased();

    /**
     * Tries to find all open records of implementing type
     * @param descending indicates if records should ordered descending
     * @return records
     */
    List<Record> getOpen(boolean descending);

    /**
     * Changes related customer
     * @param user
     * @param record
     * @param contact
     * @return updated record
     * @throws ClientException
     */
    Record changeContact(Employee user, Record record, ClassifiedContact contact) throws ClientException;

    /**
     * Changes record created date to booking date. The original date will be saved as initialDate.
     * @param record
     * @param bookingDate
     */
    void changeCreatedDate(Record record, Date bookingDate);

    /**
     * Indicates if user has sufficient permissions to delete the record.
     * Please note that this method may not recognize all situations, variants
     * and dependencies. Further some business logic may add other customized 
     * or legal rules to decide if deletion of a finance record is allowed.
     * @param user deleting the record
     * @param record the record to delete
     * @return true if deletable
     */
    boolean deletePermissionGrant(DomainUser user, Record record);

    /**
     * Deletes a record
     * @param record to delete
     * @param user deleting the record
     * @throws ClientException if record has dependencies
     */
    void delete(Record record, Employee user) throws ClientException;

    /**
     * Updates the record header
     * @param record
     * @param signatureLeft
     * @param signatureRight
     * @param personId
     * @param note
     * @param taxFree
     * @param taxFreeId
     * @param taxRate
     * @param reducedTaxRate
     * @param currency
     * @param language
     * @param branchId
     * @param shippingId
     * @param customHeader
     * @param printComplimentaryClose
     * @param printProjectmanager
     * @param printSalesperson
     * @param printBusinessId
     * @param printBusinessInfo
     * @param printRecordDate
     * @param printRecordDateByStatus
     * @param printPaymentTarget
     * @param printConfirmationPlaceholder
     * @param printCoverLetter
     * @throws ClientException if validation failed
     */
    void update(
            Record record,
            Long signatureLeft,
            Long signatureRight,
            Long personId,
            String note,
            boolean taxFree,
            Long taxFreeId,
            Double taxRate,
            Double reducedTaxRate,
            Long currency,
            String language,
            Long branchId,
            Long shippingId,
            String customHeader,
            boolean printComplimentaryClose,
            boolean printProjectmanager,
            boolean printSalesperson,
            boolean printBusinessId,
            boolean printBusinessInfo,
            boolean printRecordDate,
            boolean printRecordDateByStatus,
            boolean printPaymentTarget,
            boolean printConfirmationPlaceholder,
            boolean printCoverLetter) throws ClientException;

    /**
     * Updates the status of the offer
     * @param record
     * @param employee
     * @param newStatus
     * @throws ClientException if status could not be updated
     */
    void updateStatus(Record record, Employee employee, Long newStatus)
            throws ClientException;

    /**
     * Provides the record status history
     * @param record
     * @return statusHistory
     */
    List<RecordStatusHistory> getStatusHistory(Record record);

    /**
     * Adds a new item to record
     * @param user
     * @param record
     * @param product
     * @param stockId
     * @param customName
     * @param quantity
     * @param taxRate
     * @param price
     * @param note
     * @throws ClientException if validation failed
     */
    ItemChangedInfo addItem(
            Employee user,
            Record record,
            Product product,
            Long stockId,
            String customName,
            Double quantity,
            Double taxRate,
            BigDecimal price,
            String note)
            throws ClientException;

    /**
     * Deletes an item from a record
     * @param user
     * @param record
     * @param item
     * @return itemChangedInfo
     */
    ItemChangedInfo deleteItem(Employee user, Record record, Item item);

    /**
     * Deletes all items
     * @param user
     * @param record
     * @return record without items
     */
    Record deleteItems(Employee user, Record record);

    /**
     * Replaces record->items->item->itemId with provided item. Provided
     * item will be moved below previous in listing.
     * @param user
     * @param record
     * @param item the item to insert
     * @param itemId the id of the item to replace
     * @return record with replaced product
     */
    Record pasteItem(Employee user, Record record, Item item, Long itemId);

    /**
     * Replaces the product of an item
     * @param user
     * @param record
     * @param item
     * @param product
     * @return record with replaced product
     */
    Record replaceItem(Employee user, Record record, Item item, Product product);

    /**
     * Updates an item
     * @param user
     * @param record
     * @param item
     * @param customName
     * @param quantity
     * @param price
     * @param taxRate
     * @param stockId
     * @param note
     * @return itemChangedInfo
     * @throws ClientException if quantity is null or 0
     */
    ItemChangedInfo updateItem(
            Employee user, 
            Record record, 
            Item item,
            String customName,
            Double quantity,
            BigDecimal price,
            Double taxRate,
            Long stockId,
            String note)
        throws ClientException;

    /**
     * Unlocks locked items by enabling itemEdit and itemChange properties. 
     * @param user
     * @param record
     * @return updated record
     * @throws ClientException if unlock not possible due to context or user permissions
     */
    Record unlockItems(Employee user, Record record) throws ClientException;

    /**
     * Provides item changed history
     * @param recordId
     * @return item changed infos or empty list
     */
    List<ItemChangedInfo> getItemChangedInfos(Long recordId);

    /**
     * Moves down an item
     * @param user
     * @param record
     * @param itemId
     */
    void moveDownItem(Employee user, Record record, Long itemId);

    /**
     * Moves up an item
     * @param user
     * @param record
     * @param itemId
     */
    void moveUpItem(Employee user, Record record, Long itemId);

    /**
     * Adds an info to a record
     * @param user
     * @param record
     * @param info to add
     */
    void addInfo(Employee user, Record record, Option info);

    /**
     * Clears (removes) all infos of a record
     * @param record
     */
    void clearInfos(Record record);

    /**
     * Replaces all currently configured infos with defaults
     * @param record
     */
    void setDefaultInfos(Employee user, Record record);

    /**
     * Moves a record info list position down
     * @param record
     * @param info
     */
    void moveInfoDown(Record record, RecordInfo info);

    /**
     * Moves a record info list position up
     * @param record
     * @param info
     */
    void moveInfoUp(Record record, RecordInfo info);

    /**
     * Removes an infos from a record
     * @param record
     * @param info
     */
    void removeInfo(Record record, RecordInfo info);

    /**
     * Adds a new serial number
     * @param user
     * @param record
     * @param item
     * @param serialNumber
     * @param ignoreAdditionalChecks
     * @throws ClientException if serial number is empty or already added
     */
    void addSerial(Employee user, Record record, Item item, String serialNumber, boolean ignoreAdditionalChecks)
            throws ClientException;

    /**
     * Loads record by id
     * @param recordId
     * @return record
     * @throws RuntimeException if record not exists
     */
    Record load(Long recordId);

    /**
     * Persists current state of the record
     * @param record
     */
    void persist(Record record);

    /**
     * Provides booking types if record type supports this
     * @return bookingTypes or empty list
     */
    List<BookingType> getBookingTypes();

    /**
     * Fetches a booking type by id if record type supports this
     * @param id
     * @return bookingType or null
     */
    BookingType getBookingType(Long id);

    /**
     * Provides the default booking type if record type supports this
     * @return bookingType or null
     */
    BookingType getDefaultBookingType();

    /**
     * Updates global printOptionDefaults for a recordType by 
     * the actual settings of a record.
     * @param user the user performing the update
     * @param record
     */
    void updatePrintOptionDefaults(Employee user, Record record);

    /**
     * Validates record and settings to assure it's ready to print.
     * @param record
     * @param nextStatus
     * @throws ClientException if validation failed
     */
    void validatePrint(Record record, Long nextStatus) throws ClientException;

    /**
     * Overrides delivery address with address provided by businessCase
     * @param record
     * @param businessCase
     * @return object with updated values
     * @throws ClientException if validation fails
     */
    Record updateDeliveryAddress(Record record, BusinessCase businessCase)
        throws ClientException;

    /**
     * Deletes existing address values
     * @param record
     * @return record with empty address
     */
    Record resetDeliveryAddress(Record record);
}
