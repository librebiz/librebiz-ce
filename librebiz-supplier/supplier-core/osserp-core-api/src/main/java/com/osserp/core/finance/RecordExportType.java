/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 12-Sep-2014 14:21:23 
 * 
 */
package com.osserp.core.finance;

import java.io.Serializable;


/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RecordExportType implements Serializable {
    
    private RecordType recordType;
    private long archived;
    private int available;
    
    
    protected RecordExportType() {
        super();
    }
    
    public RecordExportType(RecordType type, long archived, int available) {
        super();
        assert (type != null);
        this.recordType = type;
        this.archived = archived;
        this.available = available;
    }

    public Long getType() {
        return recordType.getId();
    }
    
    public String getName() {
        return recordType.getName();
    }
    
    public boolean isPayment() {
        return recordType.isPayment();
    }
    
    public boolean isSupportingPrint() {
        return recordType.isSupportingPrint();
    }
    
    public Long getDocumentType() {
        return recordType.getDocumentTypeId();
    }
    
    public long getArchived() {
        return archived;
    }
    public void setArchived(long archived) {
        this.archived = archived;
    }
    
    public int getAvailable() {
        return available;
    }
    public void setAvailable(int available) {
        this.available = available;
    }
}
