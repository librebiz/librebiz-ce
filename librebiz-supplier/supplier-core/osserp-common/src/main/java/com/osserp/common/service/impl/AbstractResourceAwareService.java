/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 14, 2008 8:02:50 AM 
 * 
 */
package com.osserp.common.service.impl;

import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.Constants;
import com.osserp.common.beans.AbstractClass;
import com.osserp.common.service.ResourceLocator;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class AbstractResourceAwareService extends AbstractClass {
    private static Logger log = LoggerFactory.getLogger(AbstractResourceAwareService.class.getName());
    private ResourceLocator _resourceLocator = null;
    private Map<Locale, Map<String, Object>> resources = new java.util.HashMap<Locale, Map<String, Object>>();

    protected AbstractResourceAwareService() {
        super();
    }

    protected AbstractResourceAwareService(ResourceLocator resourceLocator) {
        super();
        this._resourceLocator = resourceLocator;
    }

    protected final String getResourceString(String name) {
        return getResourceString(name, (Locale) null);
    }

    protected final String getResourceString(String name, String localeKey) {
        Locale locale = Constants.DEFAULT_LOCALE_OBJECT;
        if ("en".equalsIgnoreCase(localeKey)) {
            locale = new Locale("en", "US");
        }
        return getResourceString(name, locale);
    }

    protected final String getResourceString(String name, Locale locale) {
        String value = "";
        try {
            Map<String, Object> res = getResourceMap(locale);
            Object val = res != null ? res.get(name) : null;
            if (val != null) {
                value = (val instanceof String ? (String) val : val.toString());
            }
        } catch (Throwable t) {
            log.warn("getResourceString() failed [message=" + t.getMessage() + "]");
        }
        return value == null ? name : value;
    }

    protected final Map<String, Object> getResourceMap(Locale locale) {
        Locale _locale = locale != null ? locale : Constants.DEFAULT_LOCALE_OBJECT;
        Map<String, Object> res = resources.get(_locale);
        if (res == null) {
            try {
                res = _resourceLocator.getResourceBundleMap(_locale);
            } catch (Throwable t) {
                log.warn("getResourceMap() failed [message=" + t.getMessage() + "]");
            }
            if (res != null) {
                resources.put(_locale, res);
            }
        }
        return res;
    }

    protected String getDefaultSalutation(String language) {
        return getResourceString("defaultSalutation", language);
    }
}
