/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 27, 2008 12:53:39 PM 
 * 
 */
package com.osserp.core.sales;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.util.DateUtil;
import com.osserp.core.BusinessType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesVolumeReport implements Serializable {
    private static Logger log = LoggerFactory.getLogger(SalesVolumeReport.class.getName());

    private int month = 0;
    private int year = 2007;

    private List<SalesListItem> monthList = new ArrayList<SalesListItem>();
    private List<SalesQueryReport> monthReport = new ArrayList<SalesQueryReport>();
    private Double monthCapacity = 0d;

    private List<SalesListItem> yearList = new ArrayList<SalesListItem>();
    private List<SalesQueryReport> yearReport = new ArrayList<SalesQueryReport>();
    private Double yearCapacity = 0d;

    private List<SalesListItem> monthCreatedList = new ArrayList<SalesListItem>();
    private List<SalesQueryReport> monthCreatedReport = new ArrayList<SalesQueryReport>();
    private Double monthCreatedCapacity = 0d;
    private Double monthCreatedUnconfirmedCapacity = 0d;

    private List<SalesListItem> yearCreatedList = new ArrayList<SalesListItem>();
    private List<SalesQueryReport> yearCreatedReport = new ArrayList<SalesQueryReport>();
    private Double yearCreatedCapacity = 0d;
    private Double yearCreatedUnconfirmedCapacity = 0d;

    private List<SalesListItem> cancelledList = new ArrayList<SalesListItem>();
    private Double cancelledCapacity = 0d;

    private List<SalesListItem> stoppedList = new ArrayList<SalesListItem>();
    private Double stoppedCapacity = 0d;

    // unreferenced records
    private List<SalesRecordSummary> monthUnreferencedList = new ArrayList<SalesRecordSummary>();
    
    private List<SalesRecordSummary> monthUnreferencedInvoiceList = new ArrayList<SalesRecordSummary>();
    private Double monthUnreferencedInvoiceCapacity = 0d;
    
    private List<SalesRecordSummary> monthUnreferencedCreditList = new ArrayList<SalesRecordSummary>();
    private Double monthUnreferencedCreditCapacity = 0d;
    
    private Double monthUnreferencedCapacity = 0d;

    private List<SalesRecordSummary> yearUnreferencedList = new ArrayList<SalesRecordSummary>();
    
    private List<SalesRecordSummary> yearUnreferencedInvoiceList = new ArrayList<SalesRecordSummary>();
    private Double yearUnreferencedInvoiceCapacity = 0d;

    private List<SalesRecordSummary> yearUnreferencedCreditList = new ArrayList<SalesRecordSummary>();
    private Double yearUnreferencedCreditCapacity = 0d;
    
    private Double yearUnreferencedCapacity = 0d;
    
    /**
     * Constructor without params required to implement serializable
     */
    protected SalesVolumeReport() {
        super();
    }

    /**
     * Creates a new sales volume report
     * @param month
     * @param year
     */
    public SalesVolumeReport(int month, int year) {
        super();
        this.month = month;
        this.year = year;
    }

    /**
     * Indicates if report contains sales items of current month
     * @return true if month and year property are same as reported by actual date 
     */
    public boolean isCurrent() {
        Date current = DateUtil.getCurrentDate();
        int currentMonth = DateUtil.getMonth(current);
        int currentYear = DateUtil.getYear(current);
        return (month == currentMonth && year == currentYear);
    }

    
    public void addYearValues(List<SalesListItem> list) {
        if (log.isDebugEnabled()) {
            log.debug("addYearValues() invoked [listSize=" + list.size() + "]");
        }
        yearList.clear();
        yearReport.clear();
        yearList.addAll(list);
        yearCapacity = 0d;
        yearUnreferencedCapacity = 0d;
        
        Map<BusinessType, SalesQueryReport> salesReportMap = new HashMap<BusinessType, SalesQueryReport>();
        for (int i = 0, j = yearList.size(); i < j; i++) {
            SalesListItem next = yearList.get(i);
            if (log.isDebugEnabled()) {
                log.debug("addYearValues() added item [id=" + next.getId() 
                        + ", type=" + next.getType().getId()
                        + ", company=" + next.getCompanyId() 
                        + ", branch=" + next.getBranchId() 
                        + ", sales=" + next.getSalesId()
                        + ", status=" + next.getStatus()
                        + ", created=" + next.getCreated() 
                        + ", capacity=" + next.getCapacity() 
                        + "]");
            }
            yearCapacity = yearCapacity
                    + (next.getCapacity() == null ? 0 : next.getCapacity());
            if (salesReportMap.containsKey(next.getType())) {
                SalesQueryReport existing = salesReportMap.get(next.getType());
                existing.addBusinessObject(next, next.getCapacity());
            } else {
                SalesQueryReport rep = new SalesQueryReport(
                        next.getType(), next, next.getCapacity());
                salesReportMap.put(rep.getType(), rep);
            }
        }
        yearReport = new ArrayList<SalesQueryReport>(salesReportMap.values());
        sort(yearReport);
        
        if (log.isDebugEnabled()) {
            log.debug("addYearValues() done [salesTypes=" + yearReport.size() 
                + ", sales=" + yearList.size()  
                + ", capacity=" + yearCapacity + "]");
        }
    }
    
    public void addYearUnreferencedValues(List<SalesRecordSummary> unreferenced) {
        if (log.isDebugEnabled()) {
            log.debug("addYearUnreferencedValues() invoked [listSize=" + unreferenced.size() + "]");
        }
        yearUnreferencedList.clear();
        yearUnreferencedList.addAll(unreferenced);
        
        yearUnreferencedInvoiceList.clear();
        yearUnreferencedCreditList.clear();
        
        yearUnreferencedInvoiceCapacity = 0d;
        yearUnreferencedCreditCapacity = 0d;
        
        yearUnreferencedCapacity = 0d;
        
        for (int i = 0, j = unreferenced.size(); i < j; i++) {
            SalesRecordSummary next = unreferenced.get(i);
            if (next.isInvoice()) {
                yearUnreferencedInvoiceCapacity =
                    yearUnreferencedInvoiceCapacity
                        + (next.getAmount() == null ? 0 : next.getAmount());
                yearUnreferencedInvoiceList.add(next);
                
                yearUnreferencedCapacity = 
                    yearUnreferencedCapacity
                        + (next.getAmount() == null ? 0 : next.getAmount());
            } else {
                yearUnreferencedCreditCapacity =
                    yearUnreferencedCreditCapacity
                        + (next.getAmount() == null ? 0 : next.getAmount());
                yearUnreferencedCreditList.add(next);
                    
                yearUnreferencedCapacity = 
                    yearUnreferencedCapacity
                        - (next.getAmount() == null ? 0 : next.getAmount());
            }
        }
        
        if (log.isDebugEnabled()) {
            log.debug("addYearUnreferencedValues() done [invoices=" + yearUnreferencedInvoiceList.size()
                + ", invoiceCapacity=" + yearUnreferencedInvoiceCapacity 
                + ", creditNotes=" + yearUnreferencedCreditList.size()  
                + ", creditCapacity=" + yearUnreferencedCreditCapacity 
                + ", totalCapacity=" + yearUnreferencedCapacity + "]");
        }
    }

    public void addMonthValues(List<SalesListItem> list) {
        if (log.isDebugEnabled()) {
            log.debug("addMonthValues() invoked [listSize=" + list.size() + "]");
        }
        monthList.clear();
        monthReport.clear();
        monthList.addAll(list);
        monthCapacity = 0d;
        monthUnreferencedCapacity = 0d;
        
        Map<BusinessType, SalesQueryReport> salesReportMap = new HashMap<BusinessType, SalesQueryReport>();
        for (int i = 0, j = monthList.size(); i < j; i++) {
            SalesListItem next = monthList.get(i);
            if (log.isDebugEnabled()) {
                log.debug("addMonthValues() added item [id=" + next.getId() 
                        + ", type=" + next.getType().getId()
                        + ", company=" + next.getCompanyId() 
                        + ", branch=" + next.getBranchId() 
                        + ", sales=" + next.getSalesId()
                        + ", created=" + next.getCreated() 
                        + ", capacity=" + next.getCapacity() 
                        + "]");
            }
            monthCapacity = monthCapacity
                    + (next.getCapacity() == null ? 0 : next.getCapacity());
            if (salesReportMap.containsKey(next.getType())) {
                SalesQueryReport existing = salesReportMap.get(next.getType());
                existing.addBusinessObject(next, next.getCapacity());
            } else {
                SalesQueryReport rep = new SalesQueryReport(
                        next.getType(), next, next.getCapacity());
                salesReportMap.put(rep.getType(), rep);
            }
        }
        monthReport = new ArrayList<SalesQueryReport>(salesReportMap.values());
        sort(monthReport);
        
        if (log.isDebugEnabled()) {
            log.debug("addMonthValues() done [salesTypes=" + monthReport.size() 
                + ", sales=" + monthList.size()  
                + ", capacity=" + monthCapacity + "]");
        }
    }
    
    public void addMonthUnreferencedValues(List<SalesRecordSummary> unreferenced) {
        if (log.isDebugEnabled()) {
            log.debug("addMonthUnreferencedValues() invoked [listSize=" + unreferenced.size() + "]");
        }
        monthUnreferencedList.clear();
        monthUnreferencedList.addAll(unreferenced);
        
        monthUnreferencedInvoiceList.clear();
        monthUnreferencedCreditList.clear();
        
        monthUnreferencedInvoiceCapacity = 0d;
        monthUnreferencedCreditCapacity = 0d;
        
        monthUnreferencedCapacity = 0d;
        
        for (int i = 0, j = monthUnreferencedList.size(); i < j; i++) {
            SalesRecordSummary next = monthUnreferencedList.get(i);
            if (next.isInvoice()) {
                monthUnreferencedInvoiceCapacity =
                    monthUnreferencedInvoiceCapacity
                        + (next.getAmount() == null ? 0 : next.getAmount());
                monthUnreferencedInvoiceList.add(next);
                
                monthUnreferencedCapacity = 
                    monthUnreferencedCapacity
                        + (next.getAmount() == null ? 0 : next.getAmount());
            } else {
                monthUnreferencedCreditCapacity =
                    monthUnreferencedCreditCapacity
                        + (next.getAmount() == null ? 0 : next.getAmount());
                monthUnreferencedCreditList.add(next);
                    
                monthUnreferencedCapacity = 
                    monthUnreferencedCapacity
                        - (next.getAmount() == null ? 0 : next.getAmount());
            }
        }
        
        if (log.isDebugEnabled()) {
            log.debug("addMonthUnreferencedValues() done [invoices=" + monthUnreferencedInvoiceList.size()
                + ", invoiceCapacity=" + monthUnreferencedInvoiceCapacity 
                + ", creditNotes=" + monthUnreferencedCreditList.size()  
                + ", creditCapacity=" + monthUnreferencedCreditCapacity 
                + ", totalCapacity=" + monthUnreferencedCapacity + "]");
        }
    }

    public void addYearCreatedValues(List<SalesListItem> list) {
        if (log.isDebugEnabled()) {
            log.debug("addYearCreatedValues() invoked [listSize=" + list.size() + "]");
        }
        yearCreatedList.clear();
        List<SalesListItem> items = new ArrayList<SalesListItem>(list);
        if (!isCurrent()) {
            for (Iterator<SalesListItem> i = items.iterator(); i.hasNext();) {
                SalesListItem next = i.next();
                int m = DateUtil.getMonth(next.getCreated());
                if (month < m) {
                    i.remove();
                }
            }
        }
        yearCreatedList.addAll(items);
        yearCreatedCapacity = 0d;
        yearCreatedUnconfirmedCapacity = 0d;
        Map<BusinessType, SalesQueryReport> salesReportMap = new HashMap<BusinessType, SalesQueryReport>();
        for (int i = 0, j = yearCreatedList.size(); i < j; i++) {
            SalesListItem next = yearCreatedList.get(i);
            if (log.isDebugEnabled()) {
                log.debug("addYearCreatedValues() added item [id=" + next.getId() 
                        + ", type=" + next.getType().getId()
                        + ", company=" + next.getCompanyId() 
                        + ", branch=" + next.getBranchId() 
                        + ", sales=" + next.getSalesId()
                        + ", created=" + next.getCreated() 
                        + ", capacity=" + next.getCapacity() 
                        + "]");
            }
            yearCreatedCapacity =
                    yearCreatedCapacity
                            + (next.getCapacity() == null ? 0 : next.getCapacity());
            if (next.getStatus() < Sales.STATUS_CONFIRMED) {
                yearCreatedUnconfirmedCapacity =
                        yearCreatedUnconfirmedCapacity
                                + (next.getCapacity() == null ? 0 : next.getCapacity());
            }
            if (salesReportMap.containsKey(next.getType())) {
                SalesQueryReport existing = salesReportMap.get(next.getType());
                existing.addBusinessObject(next, next.getCapacity());
            } else {
                SalesQueryReport rep = new SalesQueryReport(
                        next.getType(), next, next.getCapacity());
                salesReportMap.put(rep.getType(), rep);
            }
        }
        yearCreatedReport = new ArrayList<SalesQueryReport>(
                salesReportMap.values());
        sort(yearCreatedReport);
        if (log.isDebugEnabled()) {
            log.debug("addYearCreatedValues() done [size=" + yearCreatedReport.size() + ", capacity=" + yearCreatedCapacity + "]");
        }
    }

    public void addMonthCreatedValues(List<SalesListItem> list) {
        if (log.isDebugEnabled()) {
            log.debug("addMonthCreatedValues() invoked [listSize=" + list.size() + "]");
        }
        monthCreatedList.clear();
        monthCreatedList.addAll(list);
        monthCreatedCapacity = 0d;
        monthCreatedUnconfirmedCapacity = 0d;
        Map<BusinessType, SalesQueryReport> salesReportMap = new HashMap<BusinessType, SalesQueryReport>();
        for (int i = 0, j = monthCreatedList.size(); i < j; i++) {
            SalesListItem next = monthCreatedList.get(i);
            if (log.isDebugEnabled()) {
                log.debug("addMonthCreatedValues() added item [id=" + next.getId() 
                        + ", type=" + next.getType().getId()
                        + ", company=" + next.getCompanyId() 
                        + ", branch=" + next.getBranchId() 
                        + ", sales=" + next.getSalesId()
                        + ", created=" + next.getCreated() 
                        + ", capacity=" + next.getCapacity() 
                        + "]");
            }
            monthCreatedCapacity =
                    monthCreatedCapacity
                            + (next.getCapacity() == null ? 0 : next.getCapacity());
            if (next.getStatus() < Sales.STATUS_CONFIRMED) {
                monthCreatedUnconfirmedCapacity =
                        monthCreatedUnconfirmedCapacity
                                + (next.getCapacity() == null ? 0 : next.getCapacity());
            }
            if (salesReportMap.containsKey(next.getType())) {
                SalesQueryReport existing = salesReportMap.get(next.getType());
                existing.addBusinessObject(next, next.getCapacity());
            } else {
                SalesQueryReport rep = new SalesQueryReport(
                        next.getType(), next, next.getCapacity());
                salesReportMap.put(rep.getType(), rep);
            }
        }
        monthCreatedReport = new ArrayList<SalesQueryReport>(salesReportMap.values());
        sort(monthCreatedReport);
        if (log.isDebugEnabled()) {
            log.debug("addMonthCreatedValues() done [size=" + monthCreatedReport.size() + ", capacity=" + monthCreatedCapacity + "]");
        }
    }

    public void addCancelledValues(List<SalesListItem> list) {
        if (log.isDebugEnabled()) {
            log.debug("addCancelledValues() invoked [listSize=" + list.size() + "]");
        }
        cancelledList.clear();
        cancelledList.addAll(list);
        cancelledCapacity = 0d;
        for (int i = 0, j = cancelledList.size(); i < j; i++) {
            SalesListItem next = cancelledList.get(i);
            if (log.isDebugEnabled()) {
                log.debug("addCancelledValues() added item [id=" + next.getId() 
                        + ", type=" + next.getType().getId()
                        + ", company=" + next.getCompanyId() 
                        + ", branch=" + next.getBranchId() 
                        + ", sales=" + next.getSalesId()
                        + ", created=" + next.getCreated() 
                        + ", capacity=" + next.getCapacity() 
                        + "]");
            }
            cancelledCapacity =
                    cancelledCapacity
                            + (next.getCapacity() == null ? 0 : next.getCapacity());
        }
    }

    public void addStoppedValues(List<SalesListItem> list) {
        if (log.isDebugEnabled()) {
            log.debug("addStoppedValues() invoked [listSize=" + list.size() + "]");
        }
        stoppedList.clear();
        stoppedList.addAll(list);
        stoppedCapacity = 0d;
        for (int i = 0, j = stoppedList.size(); i < j; i++) {
            SalesListItem next = stoppedList.get(i);
            if (log.isDebugEnabled()) {
                log.debug("addStoppedValues() added item [id=" + next.getId() 
                        + ", type=" + next.getType().getId()
                        + ", company=" + next.getCompanyId() 
                        + ", branch=" + next.getBranchId() 
                        + ", sales=" + next.getSalesId()
                        + ", created=" + next.getCreated() 
                        + ", capacity=" + next.getCapacity() 
                        + "]");
            }
            stoppedCapacity =
                    stoppedCapacity
                            + (next.getCapacity() == null ? 0 : next.getCapacity());
        }
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    public String getMonthDisplay() {
        return DateUtil.createMonth(month);
    }

    public String getYearDisplay() {
        return Integer.toString(year);
    }

    public List<SalesListItem> getMonthList() {
        return monthList;
    }

    public List<SalesQueryReport> getMonthReport() {
        return monthReport;
    }

    public Double getMonthCapacity() {
        return monthCapacity;
    }

    public List<SalesListItem> getMonthCreatedList() {
        return monthCreatedList;
    }

    public List<SalesQueryReport> getMonthCreatedReport() {
        return monthCreatedReport;
    }

    public Double getMonthCreatedCapacity() {
        return monthCreatedCapacity;
    }

    public Double getMonthCreatedUnconfirmedCapacity() {
        return monthCreatedUnconfirmedCapacity;
    }

    public List<SalesListItem> getYearCreatedList() {
        return yearCreatedList;
    }

    public List<SalesQueryReport> getYearCreatedReport() {
        return yearCreatedReport;
    }

    public Double getYearCreatedCapacity() {
        return yearCreatedCapacity;
    }

    public Double getYearCreatedUnconfirmedCapacity() {
        return yearCreatedUnconfirmedCapacity;
    }

    public List<SalesListItem> getYearList() {
        return yearList;
    }

    public List<SalesQueryReport> getYearReport() {
        return yearReport;
    }

    public Double getYearCapacity() {
        return yearCapacity;
    }

    // cancelled and stopped orders
    
    public List<SalesListItem> getCancelledList() {
        return cancelledList;
    }

    public Double getCancelledCapacity() {
        return cancelledCapacity;
    }

    public List<SalesListItem> getStoppedList() {
        return stoppedList;
    }

    public Double getStoppedCapacity() {
        return stoppedCapacity;
    }

    // month unreferenced (invoices and credit notes without associated order)
    
    public List<SalesRecordSummary> getMonthUnreferencedInvoiceList() {
        return monthUnreferencedInvoiceList;
    }

    public Double getMonthUnreferencedInvoiceCapacity() {
        return monthUnreferencedInvoiceCapacity;
    }

    public List<SalesRecordSummary> getMonthUnreferencedCreditList() {
        return monthUnreferencedCreditList;
    }

    public Double getMonthUnreferencedCreditCapacity() {
        return monthUnreferencedCreditCapacity;
    }

    public Double getMonthUnreferencedCapacity() {
        return monthUnreferencedCapacity;
    }

    // year unreferenced

    public List<SalesRecordSummary> getYearUnreferencedInvoiceList() {
        return yearUnreferencedInvoiceList;
    }

    public Double getYearUnreferencedInvoiceCapacity() {
        return yearUnreferencedInvoiceCapacity;
    }

    public List<SalesRecordSummary> getYearUnreferencedCreditList() {
        return yearUnreferencedCreditList;
    }

    public Double getYearUnreferencedCreditCapacity() {
        return yearUnreferencedCreditCapacity;
    }

    public Double getYearUnreferencedCapacity() {
        return yearUnreferencedCapacity;
    }
    
    // mixed unreferenced lists
    
    /**
     * Provides all monthly unreferenced records (invoices and creditNotes)
     * @return monthUnreferencedList
     */
    public List<SalesRecordSummary> getMonthUnreferencedList() {
        return monthUnreferencedList;
    }

    /**
     * Provides all yearly unreferenced records (invoices and creditNotes)
     * @return monthUnreferencedList
     */
    public List<SalesRecordSummary> getYearUnreferencedList() {
        return yearUnreferencedList;
    }
    
    
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof SalesVolumeReport)) {
            return false;
        }
        SalesVolumeReport other = (SalesVolumeReport) obj;
        return (month == other.month && year == other.year);
    }

    @SuppressWarnings("unchecked")
    private void sort(List<SalesQueryReport> list) {
        Collections.sort(list, new QueryReportComparator());
    }

    private class QueryReportComparator implements Comparator {

        public int compare(Object arg0, Object arg1) {
            if (!(arg0 instanceof SalesQueryReport) || !(arg1 instanceof SalesQueryReport)) {
                return 0;
            }
            SalesQueryReport a = (SalesQueryReport) arg0;
            SalesQueryReport b = (SalesQueryReport) arg1;
            try {
                return a.getType().getName().compareTo(b.getType().getName());
            } catch (Throwable t) {
                log.warn("compare() ignoring exception [message=" + t.getMessage() + "]");
                return 0;
            }
        }
    }
}
