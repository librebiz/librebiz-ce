/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.groovy.web

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.common.Constants
import com.osserp.common.ContentType
import com.osserp.common.ErrorCode
import com.osserp.common.TimeoutException
import com.osserp.common.dms.DocumentData
import com.osserp.common.web.DocumentUtil
import com.osserp.common.web.Globals
import com.osserp.common.web.RequestUtil

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
@Controller
public abstract class AbstractController {
    private static Logger logger = LoggerFactory.getLogger(AbstractController.class.getName())
    
    private boolean _pageAreaOnly = false
    
    boolean isPageAreaOnly() {
        _pageAreaOnly
    }
    
    void disablePageAreaOnly() {
        _pageAreaOnly = false
    }
    
    void enablePageAreaOnly() {
        _pageAreaOnly = true
    }

    /**
     * Provides current page redirect
     * @return currentPage redirect
     */
    protected final String getCurrentPage() {
        'redirect:/currentPage'
    } 
    
    /**
     * Provides the default target page of implementing controller.
     * Default implementation calls {#getPageArea()} if pageAreaOnly 
     * and {#getPage()} otherwise.
     */
    protected String getDefaultPage() {
        pageAreaOnly ? pageArea : page
    }

    /**
     * Provides the logical name of the target page as implied by request mapping annotation. 
     * Default target is path/controller, e.g. 
     * '/products/product/*' results in 'products/product' and 
     * '/product/*' results in 'product'.
     * @return page or null if target could not be resolved
     */
    protected final String getPage() {
        String result = null
        RequestMapping mapping
        if (getClass().isAnnotationPresent(RequestMapping.class)) {
            mapping = getClass().getAnnotation(RequestMapping.class)
            String target = (mapping ? mapping.value()[0] : null)
            if (target && target =~ /^\/[a-zA-Z]+[\/[a-zA-Z]+]+/) {
                result = target.substring(1, target.lastIndexOf('/'))
            }
        }
        return result
    }

    /**
     * Provides the logical name of the target page area as implied by request mapping annotation. 
     * Default target is path/controller, e.g. 
     * '/products/_product/*' results in 'products/product' and
     * '/product/*' results in '_product'.
     * @return pageArea or null if not could be resolved
     */
    protected final String getPageArea() {
        String page = getPage()
        int indexOfSlash = page.lastIndexOf('/')
        if (indexOfSlash > -1) {
            return "${page.substring(0, indexOfSlash+1)}_${page.substring(indexOfSlash+1, page.length())}"
        }
        return "_$page"
    }
    
    /**
     * Creates a local target page link by logical name of the target page area,
     * e.g. local '/products/' results in 'products/_{providedName}'
     * @return local page area by provided name
     */
    protected final String localPageArea(String name) {
        String pg = page
        String base = pg?.substring(0, pg.lastIndexOf('/'))
        "${base}/_${name}"
    }

    /**
     * Redirects action to proper context
     * @param request
     * @param action
     * @return redirect action target
     */
    protected final String redirect(HttpServletRequest request, String action) {
        if (WebUtil.isStrutsRequest(action) || WebUtil.isNativeRequest(action)) {
            return "redirect:${action}"
        }
        WebUtil.redirect(request, action)
    }
    
    protected final String createLink(HttpServletRequest request, String action) {
        WebUtil.createLink(request, action)
    } 
    
    @RequestMapping
    def renderPdf(HttpServletRequest request, HttpServletResponse response) {

        Object obj = DocumentUtil.getPdfDocument(request)
        
        logger.debug("renderPdf: invoked [document=${(obj != null)}]")
        
        String header
        byte[] data
        String contentType
        int length
        
        if (obj instanceof DocumentData) {
            
            DocumentData document = obj
            length = document.getContentLength()
            contentType = document.getContentType()
            header = document.getContentDisposition()
            data = document.getBytes()
            logger.debug("renderPdf: documentData found [length=${length}, contentType=${contentType}, header=${header}]")
            
        } else if (obj instanceof byte[]) {
            data = obj
            length = data.length
            contentType = Constants.MIME_TYPE_PDF
            header = "attachment; filename=${System.currentTimeMillis()}.pdf"
            logger.debug("renderPdf: byte array found [length=${length}, header=${header}]")
        }
        DocumentUtil.removePdfDocument(request)
        
        if (data) {
            try {
                logger.debug('renderPdf: starting to render...')
                response.setContentLength(length)
                response.setContentType(contentType)
                response.setHeader("Content-disposition", header)
                response.getOutputStream().write(data)
                response.getOutputStream().flush()
            } catch (Exception e) {
                logger.error("renderPdf: failed [message=${e.message}, class=${e.getClass().getName()}]", e)
                saveError(request, ErrorCode.DOCUMENT_NOT_CREATED)
                return currentPage
            }
        } else {
            logger.warn('renderPdf: pdf document not found in request or session scope')
            saveError(request, ErrorCode.DOCUMENT_NOT_FOUND)
            return currentPage
        }
    }
    
    protected def downloadDocumentData(HttpServletRequest request, HttpServletResponse response, DocumentData document) {

        String header = document.getContentDisposition()
        byte[] data = document.getBytes()
        String contentType = document.getContentType()
        int length = document.getContentLength()
        
        logger.debug("downloadDocumentData: init done [length=${length}, contentType=${contentType}, header=${header}]")
        
        if (data) {
            try {
                logger.debug('downloadDocumentData: starting to render...')
                response.setContentLength(length)
                response.setContentType(contentType)
                response.setHeader("Content-disposition", header)
                response.getOutputStream().write(data)
                response.getOutputStream().flush()
            } catch (Exception e) {
                logger.error("downloadDocumentData: failed [message=${e.message}, class=${e.getClass().getName()}]", e)
                saveError(request, ErrorCode.FILE_ACCESS)
                return currentPage
            }
        } else {
            logger.warn('downloadDocumentData: pdf document not found in request or session scope')
            saveError(request, ErrorCode.FILE_MISSING)
            return currentPage
        }
    }
    
    protected def renderXls(HttpServletRequest request, HttpServletResponse response, String content) {
        try {
            DocumentUtil.flushDocument(response, ContentType.EXCEL, content)
        } catch (Exception e) {
            logger.error("renderXls: failed [message=${e.message}, class=${e.getClass().getName()}]", e)
            saveError(request, ErrorCode.DOCUMENT_NOT_CREATED)
            return currentPage
        }
    }
    
    protected def redirectXls(HttpServletRequest request, String xls) {
        DocumentUtil.setXlsDocument(request.session, xls)
        'redirect:/xlsServlet?file=export.xls'
    }

    protected void savePdf(HttpServletRequest request, Object binaryPdf) {
        DocumentUtil.setPdfDocument(request.session, binaryPdf)
    }

    @RequestMapping
    def sort(HttpServletRequest request) {
        logger.debug("sort: invoked [user=${getUserId(request)}, class=${getClass().getName()}]")
        View view = getView(request)
        view.sort()
        return defaultPage
    }

    /**
     * Provides the name of the view by controller name
     * @return name of associated view based on convention
     */
    protected final String getViewName() {
        WebUtil.getViewName(getClass())
    }

    /**
     * Fetches responsible view by request
     * @return view or null if none available
     * @throws TimeoutException if view.user not exists and view.userRequired is enabled 
     */
    protected final View fetchView(HttpServletRequest request) throws TimeoutException {
        viewResolved(request, getview(request, getClass(), false))
    }

    /**
     * Gets responsible view by request
     * @return view
     * @throws ViewException if view was not found
     * @throws TimeoutException if view.user not exists and view.userRequired is enabled 
     */
    protected final View getView(HttpServletRequest request) throws ViewException, TimeoutException {
        viewResolved(request, getview(request, getClass(), true))
    }
    
    /**
     * Fetches responsible view by class
     * @return view or null if none available
     * @throws TimeoutException if view.user not exists and view.userRequired is enabled 
     */
    protected final View fetchView(HttpServletRequest request, Class clazz) throws TimeoutException {
        viewResolved(request, getview(request, clazz, false))
    }

    /**
     * Gets responsible view by class
     * @return view
     * @throws ViewException if view was not found
     * @throws TimeoutException if view.user not exists and view.userRequired is enabled 
     */
    protected final View getView(HttpServletRequest request, Class clazz) throws ViewException, TimeoutException  {
        viewResolved(request, getview(request, clazz, true))
    }
    
    /**
     * Performs actions with the result of getView(request) and fetchView(request) invocations.
     * Default does nothing and returns the provided view.
     */
    protected View viewResolved(HttpServletRequest request, View justResolved) {
        return justResolved
    }

    /**
     * Removes view from session scope
     * @param request
     * @param view
     */
    protected final void removeView(HttpServletRequest request, View view) {
        request.getSession().removeAttribute(view.name)
    }

    private View getview(HttpServletRequest request, Class clazz, boolean required) throws ViewException, TimeoutException {
        View result = WebUtil.fetchView(request, clazz)
        if (required && !result) {
            logger.warn("getview: view not bound [class=${clazz.getName()}]")
            throw new ViewException("view not bound: ${clazz.getName()}")
        }
        if (result?.userRequired && !result.user) {
            logger.warn("getview: user required but not logged in [popupView=${result.isPopupView()}, class=${clazz.getName()}]")
            // TODO result may be
            if (result.isPopupView()) {
                throw new PopupTimeoutException()
            } else {
                throw new TimeoutException()
            }
        }
        return result
    }

    /**
     * Appends a param to an url
     * @param url
     * @param param name
     * @param value of param
     * @return url with added param or url if param value is null
     */
    protected final String appendParam(String url, String param, Object value) {
        return WebUtil.appendParam(url, param, value)
    }

    /**
     * Appends exitId param to an url
     * @param url
     * @param value of exitId param
     * @return url with added exitId or url if param value is null or exitId already added
     */
    protected final String appendExitIdParam(String url, Object value) {
        if (!value) {
            return url
        }
        if (url?.indexOf('exitId') < 0) {
            url = WebUtil.appendParam(url, 'exitId', value)
        }
        return url
    }

    /**
     * Appends exitId anchor to an url
     * @param url
     * @param value of exitId param
     * @return url with added exitId anchor or url if value is null
     *  or exitId already added as url param
     */
    protected final String appendExitId(String url, Object value) {
        if (!value) {
            return url
        }
        if (url?.indexOf('exitId') < 0) {
            url = "${url}#${value}"
        }
        return url
    }

    /**
     * Indicates that error message's available in sessionScope's 'errors' attribute
     * @param request
     * @return true if session attribute 'errors' exists
     */
    protected final boolean errorAvailable(HttpServletRequest request) {
        request.getSession().getAttribute(Globals.ERRORS) != null
    }
    
    /**
     * Pulls message assigned to errorKey from backend and adds the
     * i18n message to sessionScope's 'errors' attribute
     * @param request
     * @param errorKey
     */
    protected final void saveError(HttpServletRequest request, String errorKey) {
        RequestUtil.saveError(request, errorKey)
    }

    /**
     * Stores the provided error text as request attribute 'errors'.
     * @param request
     * @param errorText
     */
    protected final void saveLoginError(HttpServletRequest request, String errorText) {
        request.setAttribute(Globals.ERRORS, errorText)
    }

    /**
     * Pulls message assigned to errorKey from backend and adds the
     * i18n message to sessionScope's 'errors' attribute
     * @param request
     * @param errorKey
     */
    protected final void saveUnknownError(HttpServletRequest request) {
        request.getSession().setAttribute('error', ErrorCode.UNKNOWN)
        String value = RequestUtil.getResourceString(request, ErrorCode.UNKNOWN)
        if (value) {
            request.getSession().setAttribute(Globals.ERRORS, value)
        }
    }

    /**
     * Creates a session attribute by name
     * @param request
     * @param name 
     * @param object value
     */
    protected final void saveObject(HttpServletRequest request, String name, Object object) {
        if (object) {
            if (!name) {
                request.getSession().setAttribute(object.class.getName(), object)
            } else {
                request.getSession().setAttribute(name, object)
            }
        }
    }

    /**
     * Tries to fetch an object by request or session attribute
     * @param request
     * @param attributeName
     * @return object or null if attribute not exists in request or session
     */
    protected Object fetchObject(HttpServletRequest request, String attributeName) {
        def obj = request.getAttribute(attributeName)
        if (!obj) {
            obj = request.getSession().getAttribute(attributeName)
        }
        return obj
    }

    /**
     * Tries to fetch an object by request or session attribute
     * @param request
     * @param attributeName
     * @return object or null if attribute not exists in request or session
     */
    protected void removeObject(HttpServletRequest request, String attributeName) {
        if (request.getAttribute(attributeName)) {
            request.removeAttribute(attributeName)
        }
        if (request.getSession().getAttribute(attributeName)) {
            request.getSession().removeAttribute(attributeName)
        }
    }

    /**
     * Provides the current user 
     * @param request
     * @return user or null if no user logged in
     */
    def getUser(HttpServletRequest request) {
        request.getSession().getAttribute('user')
    }

    /**
     * Provides the id of the the current user
     * @param request
     * @return user.id or remote address
     */
    def getUserId(HttpServletRequest request) {
        def user = request.getSession().getAttribute('user')
        if (user) {
            return user.id
        }
        request.remoteAddr
    }
}
