/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 18, 2011 9:27:31 AM 
 * 
 */
package com.osserp.gui.selections

import com.osserp.common.util.CollectionUtil

import com.osserp.core.finance.Stock
import com.osserp.core.products.ProductCategoryConfig
import com.osserp.core.products.ProductClassificationConfigManager
import com.osserp.core.products.ProductGroupConfig
import com.osserp.core.products.ProductTypeConfig

/**
 *
 * @author cf <cf@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
class ProductClassificationSelectionView extends AbstractSelectionView {

    ProductCategoryConfig selectedCategory = null
    ProductGroupConfig selectedGroup = null
    ProductTypeConfig selectedType = null
    Stock selectedStock = null

    List<Stock> availableStocks = new ArrayList<Stock>()

    boolean typeBlocked = false
    boolean groupBlocked = false
    boolean showStock = true

    ProductClassificationSelectionView() {
        super()
        headerName = 'productClassificationSelection'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (!list) {
            list = productClassificationConfigManager.typesDisplay
        }
        if (!availableStocks) {
            availableStocks = systemConfigManager.availableStocks
        }
        if (forwardRequest) {
            showStock = form.getBoolean('showStock', showStock)
        }
        targetParams.remove('block')
        targetParams.remove('categoryId')
        targetParams.remove('groupId')
        targetParams.remove('typeId')
        targetParams.remove('stockId')
        targetParams.remove('showStock')
    }

    @Override
    String getUrl() {
        String result = super.getUrl()
        if (selectedCategory) {
            result += selectedCategory.id
        } else if (selectedGroup) {
            result += selectedGroup.id
        } else if (selectedType) {
            result += selectedType.id
        }
        if (selectedStock) {
            result += "&stockId=${selectedStock.id}"
        }
        return result
    }

    void selectStock() {
        Long id = form.getLong('stockId')
        if (id && id > -1) {
            selectedStock = CollectionUtil.getById(availableStocks, id)
        } else {
            selectedStock = null
        }
    }

    void selectCategory() {
        Long id = form.getLong('categoryId')
        if (id && id > -1) {
            selectedCategory = CollectionUtil.getById(selectedGroup.categories, id)
            selectionAttribute = 'categoryId'
        } else {
            selectedCategory = null
            selectionAttribute = 'groupId'
        }
    }

    void selectGroup() {
        String block = form.getString('block')
        if (block) {
            groupBlocked = (block != 'false')
            typeBlocked = (block != 'false')
        }
        Long id = form.getLong('groupId')
        if (id && id > -1) {
            selectionAttribute = 'groupId'
            selectedGroup = CollectionUtil.getById(selectedType.groups, id)
        } else {
            selectionAttribute = 'typeId'
            selectedGroup = null
        }
        selectedCategory = null
    }

    void selectType() {
        String block = form.getString('block')
        if (block) {
            typeBlocked = (block != 'false')
        }
        Long id = form.getLong('typeId')
        if (id && id > -1) {
            selectionAttribute = 'typeId'
            selectedType = productClassificationConfigManager.getType(id)
        } else {
            selectedType = null
        }
        selectedGroup = null
        selectedCategory = null
    }

    @Override
    String getSelectionAttribute() {
        return selectedCategory ? 'categoryId' : selectedGroup ? 'groupId' : 'typeId'
    }

    ProductClassificationConfigManager getProductClassificationConfigManager() {
        getService(ProductClassificationConfigManager.class.getName())
    }
}

