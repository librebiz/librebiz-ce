<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script src="<c:url value="/js/prototype-1.6.0.3.js"/>" type="text/javascript"></script>
<script src="<c:url value="/js/osserp-i18n-0.1.7.js"/>" type="text/javascript"></script>
<script src="<c:url value="/js/osserp-common-0.2.1.js"/>" type="text/javascript"></script>
<script src="<c:url value="/js/osserp-ajax-0.3.8.js"/>" type="text/javascript"></script>
<script src="<c:url value="/js/osserp-forms-0.1.2.js"/>" type="text/javascript"></script>
<script src="<c:url value="/js/osserp-1.5.0.js"/>" type="text/javascript"></script>
<script src="<c:url value="/layout/js/datepicker-1.3.js"/>" type="text/javascript"></script>
<c:import url="/layout/shared/bottom_jquery_js.jsp" />
<script type="text/javascript">
  var $j = jQuery.noConflict();
</script>
<c:import url="/layout/shared/bottom_bootstrap3_js.jsp" />
