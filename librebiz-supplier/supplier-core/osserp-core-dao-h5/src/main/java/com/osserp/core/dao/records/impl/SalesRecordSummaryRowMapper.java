/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 29, 2014 9:52:53 PM 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapperResultSetExtractor;

import com.osserp.common.OptionsCache;

import com.osserp.core.dao.impl.BusinessObjectRowMapper;
import com.osserp.core.finance.RecordType;
import com.osserp.core.sales.SalesRecordSummary;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesRecordSummaryRowMapper extends BusinessObjectRowMapper {

    public static final String SELECT_FROM =
            "SELECT id, type_id, reference, customer, name, status, created, created_by, taxfree, " +
                    "amount, tax, reducedtax, gross, sales, canceled, internal, company, branch, " +
                    "reference_type, paid_amount FROM ";

    public SalesRecordSummaryRowMapper(OptionsCache options) {
        super(options);
    }

    public static RowMapperResultSetExtractor getReader(OptionsCache options) {
        return new RowMapperResultSetExtractor(new SalesRecordSummaryRowMapper(options));
    }

    public Object mapRow(ResultSet rs, int arg1) throws SQLException {
        Long id = rs.getLong(1);
        RecordType recordType = fetchRecordType(rs.getLong(2));
        long reference = rs.getLong(3);
        long referenceType = rs.getLong(19);
        Long refTypeId = (referenceType == 0 ? null : referenceType);
        if (refTypeId == null && recordType != null) {
            if (RecordType.SALES_CANCELLATION.equals(recordType.getId())
                    || RecordType.SALES_CREDIT_NOTE.equals(recordType.getId())) {
                refTypeId = RecordType.SALES_INVOICE;
            }
        }
        SalesRecordSummary record = new SalesRecordSummary(
            id,
            recordType,
            (reference == 0 ? null : reference), // reference to sales or other entity
            (refTypeId == null ? null : fetchRecordType(refTypeId)), // type of related record
            rs.getLong(4),               // id of customer
            rs.getString(5),             // name of customer
            rs.getLong(6),               // status
            rs.getTimestamp(7),          // created
            rs.getLong(8),               // created_by
            rs.getBoolean(9),            // taxFree
            rs.getDouble(10),            // amount of the record (net)
            rs.getDouble(11),            // tax
            rs.getDouble(12),            // reducedTax
            rs.getDouble(13),            // gross
            rs.getLong(14),              // sales id if referenced to business case
            rs.getBoolean(15),           // canceled
            rs.getBoolean(16),           // internal
            fetchCompany(rs.getLong(17)),// company
            fetchBranch(rs.getLong(18)), // branch
            rs.getDouble(20));           // amount_paid
        return record;
    }

}
