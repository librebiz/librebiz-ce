/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.osserp.gui.telephones

import com.osserp.common.ClientException
import com.osserp.common.PermissionException
import com.osserp.groovy.web.PopupViewController
import javax.servlet.http.HttpServletRequest
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

/**
 * Display telephone configurations without page layout
 * 
 * @author so <so@osserp.com>
 * 
 */
@RequestMapping("/telephones/telephoneConfigurationPopup/*")
@Controller class TelephoneConfigurationPopupController extends PopupViewController {
    private static Logger logger = LoggerFactory.getLogger(TelephoneConfigurationPopupController.class.getName())

    @RequestMapping
    def load(HttpServletRequest request) {
        //tries to load TelephoneConfiguration, creates one if does not exists
        logger.debug('load invoked...')
        try {
            TelephoneConfigurationPopupView view = getView(request)
            view?.editOrCreate()
        } catch (ClientException e) {
            logger.debug("load: caught exception [message=${e.message}]")
            request.getSession().setAttribute('error', e.message)
        } catch (PermissionException e) {
            logger.debug("load: caught exception [message=${e.message}]")
            request.getSession().setAttribute('error', e.message)
        }
        return pageArea
    }

    @RequestMapping
    def create(HttpServletRequest request) {
        //tries to load TelephoneConfiguration, creates one if does not exists
        logger.debug('create invoked...')
        try {
            TelephoneConfigurationPopupView view = getView(request)
            view?.create()
        } catch (ClientException e) {
            logger.debug("create: caught exception [message=${e.message}]")
            request.getSession().setAttribute('error', e.message)
            return pageArea
        } catch (PermissionException e) {
            logger.debug("create: caught exception [message=${e.message}]")
            request.getSession().setAttribute('error', e.message)
            return pageArea
        }
        reloadParent
    }

    @RequestMapping
    def getLastChangeset(HttpServletRequest request) {
        logger.debug('getLastChangeset invoked...')
        TelephoneConfigurationPopupView view = getView(request)
        view?.getLastChangeset()
        return pageArea
    }

    @RequestMapping
    def sendPhoneAction(HttpServletRequest request) {
        logger.debug('sendPhoneAction invoked...')
        try {
            TelephoneConfigurationPopupView view = getView(request)
            view?.sendPhoneAction()
        } catch (PermissionException e) {
            logger.debug("create: caught exception [message=${e.message}]")
            request.getSession().setAttribute('error', e.message)
        }
        return pageArea
    }
}
