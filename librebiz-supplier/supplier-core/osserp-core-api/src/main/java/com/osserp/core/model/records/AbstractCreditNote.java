/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 29-Jan-2007 19:47:48 
 * 
 */
package com.osserp.core.model.records;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.util.NumberUtil;

import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.BookingType;
import com.osserp.core.finance.CancellableRecord;
import com.osserp.core.finance.CreditNote;
import com.osserp.core.finance.RecordType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractCreditNote extends AbstractCancellingRecord implements CreditNote {
    private static Logger log = LoggerFactory.getLogger(AbstractCreditNote.class.getName());

    private boolean canceled = false;
    private boolean deliveryExisting = false;

    /**
     * Default constructor required by Serializable
     */
    AbstractCreditNote() {
        super();
    }

    /**
     * Creates a new credit note by cancelled invoice
     * @param id
     * @param cancellable
     * @param type
     * @param bookingType
     * @param createdBy user
     * @param copyItems indicates if invoice items should be copied
     */
    protected AbstractCreditNote(
            Long id,
            CancellableRecord cancellable,
            RecordType type,
            BookingType bookingType,
            Employee createdBy,
            boolean copyItems) {
        super(id, type, createdBy, cancellable, copyItems, false);
        setBookingType(bookingType);
    }

    /**
     * Creates a new and empty credit note
     * @param id
     * @param company
     * @param branchId
     * @param type
     * @param bookingType
     * @param reference
     * @param contact
     * @param createdBy
     * @param sale
     * @param taxRate
     * @param reducedTaxRate
     * @param currency
     */
    protected AbstractCreditNote(
            Long id,
            Long company,
            Long branchId,
            RecordType type,
            BookingType bookingType,
            Long reference,
            ClassifiedContact contact,
            Employee createdBy,
            Long sale,
            Double taxRate,
            Double reducedTaxRate,
            Long currency) {
        super(
                id,
                company,
                branchId,
                type,
                reference,
                contact,
                createdBy,
                sale,
                taxRate,
                reducedTaxRate,
                true,
                true);
        setBookingType(bookingType);
        if (currency != null) {
            setCurrency(currency);
        }
    }

    /**
     * Creates a new credit note by other.
     * @param id
     * @param reference
     * @param createdBy
     */
    protected AbstractCreditNote(Long id, CreditNote reference, Employee createdBy) {
        super(id, reference.getType(), createdBy, reference, true, false);
        if (reference.getBookingType() != null) {
            setBookingType(reference.getBookingType());
        }
        if (reference.getCurrency() != null) {
            setCurrency(reference.getCurrency());
        }
    }

    @Override
    public BigDecimal getDueAmount() {
        if (dueAmount != null) {
            return dueAmount;
        }
        BigDecimal paid = getPaidAmount();
        if (isTaxFree()) {
            BigDecimal da = fetchBigDecimal(amounts.getAmount()).add(fetchBigDecimal(paid));
            dueAmount = NumberUtil.round(da, 2);
            if (log.isDebugEnabled()) {
                log.debug("getDueAmount() done [record=" + getId() 
                        + ", dueAmount=" + dueAmount + ", taxFree=true]");
            }
            return dueAmount;
        }
        if (amounts == null) {
            log.warn("getDueAmount() amounts is NULL! [record=" + getId()
                + ", type=" + (getType() == null ? "null" : getType().getId()) + "]");
            
        }
        BigDecimal da = fetchBigDecimal(amounts.getGrossAmount()).add(fetchBigDecimal(paid));
        dueAmount = NumberUtil.round(da, 2);
        if (log.isDebugEnabled()) {
            log.debug("getDueAmount() done [record=" + getId() 
                    + ", dueAmount=" + dueAmount + ", taxFree=false]");
        }
        return dueAmount;
    }

    @Override
    public boolean isCanceled() {
        return canceled;
    }

    protected void setCanceled(boolean canceled) {
        this.canceled = canceled;
    }

    public boolean isDeliveryExisting() {
        return deliveryExisting;
    }

    public void setDeliveryExisting(boolean deliveryExisting) {
        this.deliveryExisting = deliveryExisting;
    }

    public void toggleDeliveryNoteCreation() {
        this.deliveryExisting = !this.deliveryExisting;
    }
}
