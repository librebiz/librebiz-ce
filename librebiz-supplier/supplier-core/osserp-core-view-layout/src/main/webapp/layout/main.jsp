<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<o:page /> 
<c:set var="portalView" value="${sessionScope.portalView}" />
<!DOCTYPE html>
<html lang="de">
<head>
<jsp:include page="/${applicationScope.webConfig.layoutDefaultPath}/shared/head_application_css.jsp" />
<jsp:include page="/${applicationScope.webConfig.layoutDefaultPath}/shared/head_application_js.jsp" />
<tiles:getAsString name="styles" ignore="true" />
<tiles:insert attribute="javascript" ignore="true" />
<title><tiles:getAsString name="title" ignore="true" /></title>
</head>

<body <tiles:getAsString name="onload" ignore="true"/>>
  <jsp:include page="/${applicationScope.webConfig.layoutDefaultPath}/shared/header.jsp" />

  <div class="container-fluid">
    <div class="row">
      <%-- Navigation left --%>
      <div class="col-sm-3 col-md-2 sidebar">
        <div class="navlist">
        <jsp:include page="/${applicationScope.webConfig.layoutDefaultPath}/shared/navigation.jsp" />
        </div>
      </div>

      <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <%-- content header and navigation --%>
        <div class="row">
            <div class="col-lg-12">
                <div id="content_head_main"> 
                    <div id="content_header">
                        <div id="content_header_left">
                          <c:import url="/${applicationScope.webConfig.layoutDefaultPath}/shared/help.jsp" />
                        </div>
                        <div id="content_header_middle">
                          <tiles:getAsString name="headline" ignore="true" />
                          <c:import url="/errors/_error_message.jsp" />
                        </div>
                        <div id="content_header_right">
                            <div class="contentnav">
                                <tiles:getAsString name="headline_right" ignore="true" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- content header row end -->
        <tiles:insert attribute="content" ignore="true" />
        <%-- Ajax popup appends markup to popup-area element --%>
        <div id="popup-area"></div>
        <div id="datePicker" style="position: absolute; visibility: hidden; background-color: white; z-index: 10;"></div>
        <c:import url="/${applicationScope.webConfig.layoutDefaultPath}/shared/help_config.jsp" />
      </div> 
      <!-- main -->
    </div>
    <!-- main row -->
  </div>
  <!-- container-fluid -->
  <c:import url="/layout/shared/footer.jsp" />
  <c:import url="/layout/shared/bottom.jsp" />
</body>
</html>
