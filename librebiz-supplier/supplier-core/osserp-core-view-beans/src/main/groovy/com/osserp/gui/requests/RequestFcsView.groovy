/**
 *
 * Copyright (C) 2007, 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 1, 2007 
 * Created on Apr 17, 2014 (Groovy implementation) 
 * 
 */
package com.osserp.gui.requests

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.core.BusinessCase
import com.osserp.core.FcsManager
import com.osserp.core.requests.RequestFcsAction
import com.osserp.core.requests.RequestFcsManager
import com.osserp.gui.fcs.FlowControlActionView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class RequestFcsView extends FlowControlActionView {
    private static Logger logger = LoggerFactory.getLogger(RequestFcsView.class.getName())
    
    RequestFcsView() {
        super()
        enableClosings()
    }

    void addTransferAction(String note) {
        if (!selectedAction) {
            logger.warn('addTransferAction: interrupted [selectedAction=null]')
            return
        }
        flowControlManager.addFlowControl(
            domainEmployee,
            businessCase,
            selectedAction,
            new Date(System.currentTimeMillis()),
            note,
            null,
            null,
            false)
        businessCaseView.reload()
        businessCaseView.setFlowControlMode(false)
        selectedAction = null
        disableEditMode()
        load()
    }
    
    @Override
    String selectAction() throws ClientException {
        String result = super.selectAction()
        RequestFcsAction action = selectedAction
        if (action.isTransfering()) {
            disableEditMode()
            selectedAction = action
        }
        return result
    }

    @Override
    protected Map<String, String> createActionTargets() {
        [
            assign:         '/requests/requestTypeChange/forward?exit=/requests/requestFcs/reload&target=/requestDisplay.do',
            offerSelection: '/salesOffer.do?method=forward'
        ]
    }
    
    @Override
    BusinessCase getBusinessCase() {
        businessCaseView.request
    }

    @Override
    protected FcsManager getFlowControlManager() {
        getService(RequestFcsManager.class.getName())
    }
}
