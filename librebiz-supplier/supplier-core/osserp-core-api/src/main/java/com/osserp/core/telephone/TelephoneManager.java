/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Feb 23, 2010 1:53:09 PM 
 * 
 */
package com.osserp.core.telephone;

import java.util.List;

import com.osserp.common.ClientException;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public interface TelephoneManager {

    /**
     * Provides all telephones
     * @return telephones
     */
    List<Telephone> getAll();

    /**
     * Updates telephone with given id
     * @param id
     * @param telephoneSystemId
     */
    void updateTelephoneSystem(Long id, Long telephoneSystemId);

    /**
     * Updates telephone with given id
     * @param id
     * @param telephoneTypeId
     */
    void updateTelephoneType(Long id, Long telephoneTypeId);

    /**
     * Creates telephone
     * @param mac
     * @param telephoneSystemId
     * @param telephoneTypeId
     * @return telephone
     * @throws ClientException
     */
    Telephone create(String mac, Long telephoneSystemId, Long telephoneTypeId) throws ClientException;

    /**
     * Tries to find a telephone by id
     * @param id
     * @return telephone or null if not found
     */
    Telephone find(Long id);

    /**
     * Tries to find a telephone by mac, telephoneSystemId or telephoneTypeId
     * @param mac
     * @param telephoneSystemId
     * @param telephoneTypeId
     * @return telephones or null if not found
     */
    List<Telephone> find(String mac, Long telephoneSystemId, Long telephoneTypeId);

    /**
     * Tries to find a telephone by branch
     * @param id
     * @return telephone or null if not found
     */
    List<Telephone> findByBranch(Long id);

    /**
     * Tries to delete a telephone by id
     * @param id
     */
    void delete(Long id);
}
