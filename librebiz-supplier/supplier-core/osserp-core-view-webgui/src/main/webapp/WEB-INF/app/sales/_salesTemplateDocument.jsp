<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<v:view viewName="salesTemplateDocumentView"/>
<c:set var="templateView" scope="request" value="${view}"/>
<c:set var="templateDocumentUrl" scope="request" value="/sales/salesTemplateDocument"/>
<c:import url="${viewdir}/dms/_businessCaseTemplate.jsp"/>
