<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="requestOriginHistoryView"/>

<c:if test="${!empty view and !empty view.list}">
	
<div class="modalBoxHeader">
	<div class="modalBoxHeaderLeft">
		<fmt:message key="selectOriginLabel"/>
	</div>
</div>
<div class="modalBoxData">
	<div class="subcolumns">
		<div class="subcolumn">
			<div class="spacer"></div>
			<table class="table table-striped">
				<thead>
					<tr>
						<th class="date"><fmt:message key="date"/></th>
						<th class="from"><fmt:message key="changedBy"/></th>
						<th class="to"><fmt:message key="changedTo"/></th>
						<th class="name"><fmt:message key="name"/></th>
					</tr>
				</thead>
				<tbody style="overflow-x: hidden; overflow-y: auto;">
					<c:forEach var="entry" items="${view.list}">
						<tr>
							<td class="date" valign="top"><o:date value="${entry.created}"/></td>
							<td class="from" valign="top"><o:out value="${entry.previousCampaign.name}"/></td>
							<td class="to" valign="top"><o:out value="${entry.campaign.name}"/></td>
							<td class="name" valign="top"><oc:employee value="${entry.createdBy}"/></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<div class="spacer"></div>
		</div>
	</div>
</div>
</c:if>