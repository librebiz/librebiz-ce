/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 04.06.2014 10:44:04 
 * 
 */
package com.osserp.gui.admin.requests

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.Option

import com.osserp.core.BusinessType
import com.osserp.core.FcsAction
import com.osserp.core.FcsActionManager
import com.osserp.core.FcsConfig
import com.osserp.core.Options
import com.osserp.core.requests.RequestFcsActionManager

import com.osserp.groovy.web.MenuItem

import com.osserp.gui.admin.fcs.FlowControlConfigView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RequestFcsConfigView extends FlowControlConfigView {
    private static Logger logger = LoggerFactory.getLogger(RequestFcsConfigView.class.getName())

    FcsConfig fcsConfig

    RequestFcsConfigView() {
        super()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
    }

    @Override
    void select() {
        fcsConfig = requestFcsActionManager.addAction(domainUser, fcsConfig, getLong('id'))
        load()
    }

    void deselect() {
        fcsConfig = requestFcsActionManager.removeAction(domainUser, fcsConfig, getLong('id'))
        load()
    }

    @Override
    protected List<BusinessType> fetchBusinessTypes() {
        List<BusinessType> btlist = super.fetchBusinessTypes()
        return btlist.findAll { BusinessType bt ->
            !bt.directSales
        }
    }

    @Override
    void selectBusinessType() {
        super.selectBusinessType()
        load()
    }

    @Override
    void reload() {
        load()
        if (bean && list) {
            bean = list.find { it.id == bean.id }
        }
    }

    private void load() {
        if (selectedType) {
            fcsConfig = requestFcsActionManager.getConfig(selectedType.id)
            if (fcsConfig) {
                list = fcsConfig.actions
            }
            actualSortKey = 'orderId'
            otherActions = []
            List<FcsAction> all = requestFcsActionManager.findAll()
            all.each { FcsAction fcs ->
                boolean added = false
                list.each { FcsAction efcs ->
                    if (fcs.id == efcs.id) {
                        added = true
                    }
                }
                if (!added) {
                    logger.debug("load: adding unused action [id=${fcs.id}]")
                    otherActions << fcs
                }
            }
        } else {
            selectedType = null
            fcsConfig = null
            list = []
            otherActions = []
        }
    }

    void save() {
    }

    void moveUp() {
        fcsConfig = requestFcsActionManager.moveAction(fcsConfig, getLong('id'), true)
        load()
    }

    void moveDown() {
        fcsConfig = requestFcsActionManager.moveAction(fcsConfig, getLong('id'), false)
        load()
    }

    @Override
    void createCustomNavigation() {
        nav.defaultNavigation = [
            exitLink,
            flowControlActionConfigLink,
            homeLink
        ]
        nav.listNavigation = [
            businessTypeDeselectLink,
            editLink,
            homeLink
        ]
    }

    MenuItem getFlowControlActionConfigLink() {
        navigationLink("/admin/requests/requestFcsActionConfig/forward?exit=/admin/requests/requestFcsConfig/reload", 'configureIcon', 'createAndChangeActions')
    }

    MenuItem getBusinessTypeSelectionLink() {
        new MenuItem(
                link: "${context.fullPath}/selections/businessTypeSelection/forward?selectionTarget=/admin/sales/salesFcsConfig/selectBusinessType",
                icon: 'searchIcon',
                title: 'businessTypeSelection',
                ajaxPopup: true,
                ajaxPopupName: 'businessTypeSelectionView')
    }

    protected RequestFcsActionManager getRequestFcsActionManager() {
        getService(RequestFcsActionManager.class.getName())
    }

    @Override
    protected FcsActionManager getFcsActionManager() {
        requestFcsActionManager
    }
}
