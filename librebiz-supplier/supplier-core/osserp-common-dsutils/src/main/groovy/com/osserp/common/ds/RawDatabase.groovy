/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.common.ds

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import groovy.sql.Sql

import com.osserp.common.dao.DataSourceConfig
import com.osserp.common.ds.Sheet2Table

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class RawDatabase extends DataSourceConfig {
    private static Logger logger = LoggerFactory.getLogger(RawDatabase.class.getName())

    File logfile = new File('./error.log')

    RawDatabase() {
        super()
        logger.debug("RawDatabase: instance created [url=${url}, driver=${driver}, admin=${admin}]")
    }

    /**
     * Provides the application name used to determine the config file location.
     * @return default name jdbc
     */
    @Override
    String getApplicationName() {
        'osserp-importer'
    }

    /**
     * Transfers import file to database
     * @return count of transfered rows
     */
    Integer transfer(Sheet2Table importFile) {
        Integer result = 0
        try {
            dropTable(importFile, false)
            if (createTable(importFile)) {
                while (importFile.hasNext()) {
                    createTableEntry(importFile)
                    result++
                }
            }
        } catch (Exception e) {
            logger.error("transfer: caught exception [message=${e.message}]")
        }
        return result
    }


    /**
     * Fetches transfered data from database
     * @param tableName
     * @return columns with transfered values
     */
    List getTransfered(String tableName) {
        def result = []
        Sql sql
        try {
            sql = create()
            result = sql.rows("SELECT * FROM ${tableName}".toString())
        } catch (Exception e) {
            logger.error("getTransfered: caught exception [message=${e.message}]")
        } finally {
            try {
                if (sql) {
                    sql.close()
                }
            } catch (Throwable ignorable) {
                // ignore this
            }
        }
        return result
    }

    protected boolean createTable(Sheet2Table importFile) {
        def result = false
        Sql sql
        try {
            sql = create()
            sql.execute(importFile.tableDDL)
            createTableDef(importFile)
            def cnt = 0
            sql.eachRow("SELECT count(*) FROM ${importFile.getTableName()}".toString()) { row ->
                cnt = row[0]
            }
            result = (cnt == 0)
        } catch (Exception e) {
            logger.error("createTable: caught exception [message=${e.message}]")
        } finally {
            try {
                if (sql) {
                    sql.close()
                }
            } catch (Throwable ignorable) {
                // ignore this
            }
        }
        return result
    }

    protected boolean createTableEntry(Sheet2Table importFile) {
        def result = false
        Sql sql
        String insertStatement
        try {
            sql = create()
            insertStatement = importFile.getNextInsert()
            sql.executeUpdate(insertStatement)
            //            println insertStatement
            result = (sql.updateCount == 1)
        } catch (Exception e) {
            StringBuilder errmsg = new StringBuilder()
            errmsg
                    .append('createTableEntry failed on ')
                    .append(importFile.getTableName())
                    .append(', message: ')
                    .append(e.message)
                    .append(', statement:\n')
                    .append(insertStatement)
                    .append('\n')
            String message = errmsg.toString()
            println message
            logfile.append(message)
        } finally {
            try {
                if (sql) {
                    sql.close()
                }
            } catch (Throwable ignorable) {
                // ignore this
            }
        }
        return result
    }

    protected void dropTable(Sheet2Table importFile, boolean logError) {
        Sql sql
        try {
            sql = create()
            sql.execute("DROP TABLE ${importFile.getTableName()}".toString())
        } catch (Exception e) {
            if (logError) {
                logger.error("dropTable: caught exception [message=${e.message}]")
            }
        } finally {
            try {
                if (sql) {
                    sql.close()
                }
            } catch (Throwable ignorable) {
                // ignore this
            }
        }
    }

    protected void createTableDef(Sheet2Table importFile) {
        if (!tableDefTableExisting()) {
            createTableDefTable()
        }
        Sql sql = create()
        importFile.columnNames.each() {
            try {
                sql.executeUpdate(createTableDefInsert(importFile.tableName, it))
            } catch (Exception e) {
                println e.message
            } finally {
            }
        }
        try {
            if (sql) {
                sql.close()
            }
        } catch (Throwable ignorable) {
            // ignore this
        }
    }

    protected String createTableDefInsert(String tableName, String columnName) {
        StringBuilder sql = new StringBuilder()
        sql
                .append('INSERT INTO table_defs (tablename, colname) VALUES (\'')
                .append(tableName)
                .append('\', \'')
                .append(columnName)
                .append('\');')
        sql.toString()
    }

    protected void createTableDefTable() {
        Sql sql
        try {
            sql = create()
            sql.execute('''
                CREATE TABLE table_defs (
                    tablename varchar(255),
                    colname varchar(255),
                    rowcount bigint default 0,
                    filledcount bigint default 0,
                    deleted boolean default false
                );''')
            logger.debug("createTableDefTable: done")
        } catch (Exception e) {
            logger.error("createTableDefTable: caught exception [message=${e.message}]")
        } finally {
            try {
                if (sql) {
                    sql.close()
                }
            } catch (Throwable ignorable) {
                // ignore this
            }
        }
    }

    protected Boolean tableDefTableExisting() {
        def result = false
        Sql sql
        try {
            sql = create()
            def count
            sql.eachRow('SELECT count(*) FROM table_defs') { row ->
                count = row[0]
            }
            result = true
        } catch (Throwable e) {
            logger.error("tableDefTableExisting: caught exception [message=${e.message}]")
        } finally {
            try {
                if (sql) {
                    sql.close()
                }
            } catch (Throwable ignorable) {
                // ignore this
            }
        }
        return result
    }

    void createTableStats() {
        if (tableDefTableExisting()) {
            def start = System.currentTimeMillis()
            println "Database: createTableStats started..."

            Sql sql
            try {
                sql = create()
                sql.eachRow('SELECT tablename,colname FROM table_defs ORDER BY tablename') { row ->
                    def tb = row[0]
                    def col = row[1]
                    Long rows = getRowCount(tb)
                    Long filled = getFilledCount(tb, col)
                    sql.executeUpdate("""UPDATE table_defs SET
                    rowcount = ${rows},
                    filledcount = ${filled} WHERE
                    tablename = ${tb} AND colname = ${col};""")
                }
                logger.debug("createTableStats: done (${System.currentTimeMillis() - start}ms)")
            } catch (Throwable e) {
                logger.error("createTableStats: caught exception [message=${e.message}]", e)
            } finally {
                try {
                    if (sql) {
                        sql.close()
                    }
                } catch (Throwable ignorable) {
                    // ignore this
                }
            }
        } else {
            println "Database: createTableStats missing table_defs"
        }
    }

    void cleanupTables() {
        if (tableDefTableExisting()) {
            def start = System.currentTimeMillis()
            println "Database: cleanupTables started..."

            Sql sql
            try {
                sql = create()
                sql.eachRow('''SELECT tablename,colname
                               FROM table_defs
                               WHERE rowcount > 0 AND filledcount = 0
                               ORDER BY tablename''') { row ->
                            def tb = row[0]
                            def col = row[1]
                            try {
                                sql.execute("ALTER TABLE ${tb} DROP COLUMN ${col};".toString())
                            } catch (Throwable e) {
                            }
                        }
                logger.debug("cleanupTables: done (${System.currentTimeMillis() - start}ms)")
            } catch (Throwable e) {
                logger.error("cleanupTables: caught exception [message=${e.message}]", e)
            } finally {
                try {
                    if (sql) {
                        sql.close()
                    }
                } catch (Throwable ignorable) {
                    // ignore this
                }
            }
        }
    }

    void renameTables() {
        if (tableDefTableExisting()) {
            def start = System.currentTimeMillis()
            println "Database: renameTables started..."

            Sql sql
            try {
                sql = create()
                sql.eachRow('''SELECT tablename
                               FROM table_defs
                               ORDER BY tablename''') { row ->
                            def tb = row[0]
                            def tbnew = "x_am2_${tb}".toString()
                            try {
                                sql.execute("ALTER TABLE ${tb} RENAME TO ${tbnew};".toString())
                                sql.executeUpdate("UPDATE table_defs SET tablename = '${tbnew}' WHERE tablename = '${tb}';".toString())
                            } catch (Throwable e) {
                            }
                        }
                logger.debug("renameTables done (${System.currentTimeMillis() - start}ms)")
            } catch (Throwable e) {
                logger.error("renameTables: caught exception [message=${e.message}]", e)
            } finally {
                try {
                    if (sql) {
                        sql.close()
                    }
                } catch (Throwable ignorable) {
                    // ignore this
                }
            }
        }
    }

    protected Long getFilledCount(String tableName, String columnName) {
        Sql sql
        try {
            Long count
            sql = create()
            sql.eachRow("SELECT count(*) FROM ${tableName} WHERE ${columnName} IS NOT NULL AND ${columnName} <> ''".toString()) { row ->
                count = row[0]
            }
            return count
        } catch (Throwable e) {
            // do nothing
        } finally {
            try {
                if (sql) {
                    sql.close()
                }
            } catch (Throwable ignorable) {
                // ignore this
            }
        }
        return -1
    }

    protected Long getRowCount(String tableName) {
        Sql sql
        try {
            Long count
            sql = create()
            sql.eachRow("SELECT count(*) FROM ${tableName}".toString()) { row ->
                count = row[0]
            }
            return count
        } catch (Throwable e) {
            // do nothing
        } finally {
            try {
                if (sql) {
                    sql.close()
                }
            } catch (Throwable ignorable) {
                // ignore this
            }
        }
        return -1
    }
}
