<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:set var="view" scope="request" value="${requestScope.productView}"/>
<c:set var="isPopup" scope="request" value="true"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main_popup.jsp" flush="true">
<tiles:put name="title"><fmt:message key="productView"/></tiles:put>
<tiles:put name="content" type="string">
<c:import url="/pages/products/shared/product_display.jsp"/>
</tiles:put>
</tiles:insert>
