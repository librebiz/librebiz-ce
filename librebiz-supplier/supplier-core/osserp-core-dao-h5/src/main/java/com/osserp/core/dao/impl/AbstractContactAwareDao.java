/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 23, 2012 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.Types;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;
import com.osserp.common.mail.EmailAddress;

import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.ContactType;
import com.osserp.core.contacts.Phone;
import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.dao.Contacts;
import com.osserp.core.dao.Emails;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractContactAwareDao extends AbstractContactDao {
    private static Logger log = LoggerFactory.getLogger(AbstractContactAwareDao.class.getName());
    protected Contacts contacts = null;

    public AbstractContactAwareDao(
            JdbcTemplate jdbcTemplate, 
            Tables tables, 
            SessionFactory sessionFactory,
            Contacts contacts,
            Emails emails, 
            OptionsCache namesCache) {
        super(jdbcTemplate, tables, sessionFactory, emails, namesCache);
        this.contacts = contacts;
    }

    /**
     * Loads a contact by contact id
     * @param contactId
     * @return contact by contactId
     * @throws RuntimeException if contact not exists
     */
    protected Contact loadContact(Long contactId) {
        return contacts.loadContact(contactId);
    }

    /**
     * Loads all contact persons
     * @param contact
     * @return contactPersons or empty list
     */
    protected List<Contact> loadContactPersons(ClassifiedContact contact) {
        return contacts.findContactPersons(contact.getContactId());
    }

    /**
     * Provides the count of existing phone numbers
     * @param contact
     * @param device
     * @return count
     */
    protected final long getPhoneCount(Long contact, Long device) {
        long count = 0;
        try {
            StringBuilder sql = new StringBuilder("SELECT count(*) FROM ");
            sql.append(phoneTable).append(" WHERE contact_id = ? AND device_id = ?");
            final Object[] params = { contact, device };
            final int[] types = { Types.BIGINT, Types.BIGINT };
            count = jdbcTemplate.queryForObject(sql.toString(), params, types, Integer.class);
            
        } catch (Exception e) {
            log.warn("getPhoneCount: caught exception [contactId=" + contact 
                    + ", device=" + device 
                    + ", message=" + e.getMessage() + "]", e);
        }
        return count;
    }

    /**
     * Provides the default email type id depending on contact type.
     * @param contactType
     * @return private on persons and business on company
     */
    protected final Long getEmailTypeByContactType(ContactType contactType) {
        return (contactType != null && (contactType.isBusiness() || contactType.isFreelance() || contactType.isPublicInstitution())) ? EmailAddress.BUSINESS : EmailAddress.PRIVATE;
    }

    /**
     * Provides the default phone type id depending on contact type.
     * @param contactType
     * @return private on persons and business on company
     */
    protected final Long getPhoneTypeByContactType(ContactType contactType) {
        return (contactType != null && (contactType.isBusiness() || contactType.isFreelance() || contactType.isPublicInstitution())) ? Phone.BUSINESS : Phone.PRIVATE;
    }

}
