<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<c:if test="${!empty sessionScope.errors}">
	<c:set var="sessionErrorsFound" value="true"/>
	<o:logger write="current page has errors" level="debug"/>
	<c:choose>
		<c:when test="${!empty sessionScope.errorId}">
			<div class="errormessage"><p><o:out value="${sessionScope.errors}"/> / <o:out value="${sessionScope.errorId}"/></p></div>
			<c:remove var="errorId" scope="session"/>
		</c:when>
		<c:when test="${!empty sessionScope.errorDetails}">
			<o:logger write="current page has errorDetails" level="debug"/>
			<div class="errormessage"><p><o:out value="${sessionScope.errors}"/> : <o:out value="${sessionScope.errorDetails}"/></p></div>
			<c:remove var="errorDetails" scope="session"/>
		</c:when>
		<c:otherwise>
			<div class="errormessage"><p><o:out value="${sessionScope.errors}"/></p></div>
		</c:otherwise>
	</c:choose>
</c:if>
<c:if test="${empty sessionErrorsFound && !empty sessionScope.error}">
	<c:set var="sessionErrorsFound" value="true"/>
	<div class="errormessage"><p><o:out value="${sessionScope.error}"/></p></div>
</c:if>
<c:if test="${empty sessionErrorsFound && !empty requestScope.error}">
	<div class="errormessage"><p><o:out value="${requestScope.error}"/></p></div>
</c:if>
<o:removeErrors/>
<c:remove var="errorId" scope="session"/><c:remove var="errorDetails" scope="session"/>
