<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>

<c:set var="view" value="${sessionScope.recordView}" />
<c:set var="record" value="${view.record}" />
<c:set var="command" value="${requestScope.conditionCommand}" />

<div class="content-area" id="recordConditionEditorContent">
    <div class="row">
        <div class="col-md-6 panel-area panel-area-default">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4><fmt:message key="notUsedConditions" /></h4>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive table-responsive-default">
                    <table class="table">
                        <tbody>
                            <c:forEach var="available" items="${view.recordInfos}">
                                <tr>
                                    <td style="width: 20px;"><a href="<c:url value="${command}"><c:param name="method" value="add"/><c:param name="id" value="${available.id}"/></c:url>"><o:img name="enabledIcon" styleClass="bigicon" /></a></td>
                                    <td><oc:options name="recordInfoConfigs" value="${available.id}" format="true" /></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6 panel-area panel-area-default">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="table-responsive">
                        <table class="table table-head">
                            <tbody>
                                <tr>
                                    <td class="center">
                                        <a href="<c:url value="${command}"><c:param name="method" value="defaults"/></c:url>">
                                            [<fmt:message key="standardSettings" />]
                                        </a>
                                    </td>
                                    <td class="center">
                                        <a href="<c:url value="${command}"><c:param name="method" value="clear"/></c:url>">
                                            [<fmt:message key="deleteAllConditions" />]
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive table-responsive-default">
                    <table class="table">
                        <tbody>
                            <c:forEach var="info" items="${record.infos}">
                                <tr>
                                    <td>
                                        <a href="<c:url value="${command}"><c:param name="method" value="remove"/><c:param name="id" value="${info.id}"/></c:url>"><o:img name="deleteIcon" styleClass="bigicon" /></a>
                                    </td>
                                    <td>
                                        <a href="<c:url value="${command}"><c:param name="method" value="moveUp"/><c:param name="id" value="${info.id}"/></c:url>"><o:img name="upIcon" styleClass="bigicon" /></a>
                                    </td>
                                    <td>
                                        <a href="<c:url value="${command}"><c:param name="method" value="moveDown"/><c:param name="id" value="${info.id}"/></c:url>"><o:img name="downIcon" styleClass="bigicon" /></a>
                                    </td>
                                    <td><oc:options name="recordInfoConfigs" value="${info.infoId}" format="true" /></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
