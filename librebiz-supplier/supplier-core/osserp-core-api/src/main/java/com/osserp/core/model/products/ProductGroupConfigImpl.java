/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 28, 2009 7:28:05 AM 
 * 
 */
package com.osserp.core.model.products;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.osserp.common.util.CollectionUtil;

import com.osserp.core.products.ProductCategoryConfig;
import com.osserp.core.products.ProductClass;
import com.osserp.core.products.ProductClassificationEntity;
import com.osserp.core.products.ProductGroup;
import com.osserp.core.products.ProductGroupConfig;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProductGroupConfigImpl extends AbstractProductGroup implements ProductGroupConfig {
    private List<ProductCategoryConfig> categoryList = new ArrayList<ProductCategoryConfig>();

    protected ProductGroupConfigImpl() {
        super();
    }

    /**
     * Default constructor for new product groups
     * @param name
     * @param description
     * @param createdBy
     * @param classification
     */
    public ProductGroupConfigImpl(String name, String description, Long createdBy, ProductClass classification) {
        super(name, description, createdBy, classification);
    }

    protected ProductGroupConfigImpl(ProductGroupConfig o) {
        super(o);
        if (!o.getCategories().isEmpty()) {
            for (int i = 0, j = o.getCategories().size(); i < j; i++) {
                ProductCategoryConfig next = o.getCategories().get(i);
                this.categoryList.add((ProductCategoryConfig) next.clone());
            }
        }
    }

    public List<ProductCategoryConfig> getCategories() {
        return new ArrayList<ProductCategoryConfig>(categoryList);
    }

    public void addCategory(ProductCategoryConfig category) {
        if (!supportsCategory(category)) {
            this.categoryList.add(category);
        }
    }

    public void removeCategory(ProductCategoryConfig category) {
        for (Iterator<ProductCategoryConfig> i = categoryList.iterator(); i.hasNext();) {
            ProductCategoryConfig existing = i.next();
            if (existing.getId().equals(category.getId())) {
                i.remove();
                break;
            }
        }
    }

    public ProductCategoryConfig fetchCategory(Long id) {
        for (int i = 0, j = categoryList.size(); i < j; i++) {
            ProductCategoryConfig next = categoryList.get(i);
            if (next.getId().equals(id)) {
                return next;
            }
        }
        return null;
    }

    public boolean supportsCategory(ProductClassificationEntity category) {
        return (fetchCategory(category.getId()) != null);
    }

    public void moveNext(ProductCategoryConfig category) {
        if (category != null) {
            CollectionUtil.moveNext(categoryList, category);
        }
    }

    public void movePrevious(ProductCategoryConfig category) {
        if (category != null) {
            CollectionUtil.movePrevious(categoryList, category);
        }
    }

    public ProductGroup getProductGroup() {
        return new ProductGroupImpl(this);
    }

    protected List<ProductCategoryConfig> getCategoryList() {
        return categoryList;
    }

    protected void setCategoryList(List<ProductCategoryConfig> categoryList) {
        this.categoryList = categoryList;
    }

    @Override
    public Object clone() {
        return new ProductGroupConfigImpl(this);
    }
}
