/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 15-Jun-2005 20:28:40 
 * 
 */
package com.osserp.gui.common;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jdom2.Document;
import org.jdom2.Element;

import com.osserp.common.PermissionException;
import com.osserp.common.TimeoutException;
import com.osserp.common.User;
import com.osserp.common.UserManager;
import com.osserp.common.util.DateFormatter;
import com.osserp.common.web.ContextUtil;
import com.osserp.common.web.Globals;
import com.osserp.common.web.SessionUtil;

import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class UserUtil extends SessionUtil {
    private static Logger log = LoggerFactory.getLogger(UserUtil.class.getName());

    public static UserManager getManager(HttpSession session) {
        return (UserManager) ContextUtil.getService(session, UserManager.class.getName());
    }

    /**
     * Gets user from session
     * @param session
     * @return user
     * @throws TimeoutException if user not available
     */
    public static User getUser(HttpSession session) {
        try {
            return (User) getObject(session, Globals.USER);
        } catch (Exception e) {
            throw new TimeoutException();
        }
    }

    /**
     * Gets domainUser from session
     * @param session
     * @return user
     * @throws TimeoutException if user not available or user not domain user
     */
    public static DomainUser getDomainUser(HttpSession session) throws PermissionException {
        User user = getUser(session);
        if (user instanceof DomainUser) {
            return ((DomainUser) user);
        }
        throw new PermissionException("user is no valid domain user");
    }

    /**
     * Provides current logged in domain users name
     * @param session where we lookup for domain user
     * @return domain user name as first and lastname
     * @throws PermissionException if user is no domain user
     */
    public static String getDomainUserName(HttpSession session) throws PermissionException {
        return getDomainUser(session).getEmployee().getName();
    }

    public static Long getEmployeeId(HttpSession session) throws PermissionException {
        return getDomainUser(session).getEmployee().getId();
    }

    /**
     * Checks if currrent user is s sales person
     * @param session
     * @return true if so
     */
    public static boolean isCurrentUserSalesPerson(HttpSession session) {
        return ((getCurrentUsersSalesId(session) != null) ? true : false);
    }

    /**
     * Provides current users sales id if current user is s sales person
     * @param session
     * @return salesId or null if user is no sales person
     */
    private static Long getCurrentUsersSalesId(HttpSession session) {
        User user = getUser(session);
        if (user instanceof DomainUser) {
            DomainUser du = (DomainUser) user;
            if (du.isSales()) {
                return du.getEmployee().getId();
            }
        }
        return null;
    }

    /**
     * Checks if currrent user is a project manager
     * @param session
     * @return true if so
     */
    public static boolean isCurrentUserProjectManager(HttpSession session) {
        return ((getCurrentUsersProjectManagerId(session) != null) ? true : false);
    }

    /**
     * Provides current users project manager id if current user is a project manager
     * @param session
     * @return managerId or null if user is no project manager
     */
    public static Long getCurrentUsersProjectManagerId(HttpSession session) {
        User user = getUser(session);
        if (user instanceof DomainUser) {
            DomainUser du = (DomainUser) user;
            if (du.isProjectManager()) {
                return du.getEmployee().getId();
            }
        }
        return null;
    }

    /**
     * Checks whether current user is an exceutive
     * @param session
     * @return true if so
     */
    public static boolean isCurrentUserExecutive(HttpSession session) {
        User user = getUser(session);
        if (user instanceof DomainUser) {
            DomainUser du = (DomainUser) user;
            if (du.isExecutive()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Creates a new xml document with 'root' as root element name and child elements 'currentDate' and 'currentUser'
     * @param session where we lookup for domain user name
     * @return xml document
     */
    public static Document createXmlDocument(HttpSession session) {
        return createXmlDocument(session, "root");
    }

    /**
     * Creates a new xml document with given root element name and child elements 'currentDate' and 'currentUser'
     * @param session where we lookup for domain user name
     * @param name of the root element
     * @return xml document
     */
    private static Document createXmlDocument(HttpSession session, String rootName) {
        Document doc = new Document();
        Element root = new Element(rootName);
        root.addContent(new Element("currentDate").setText(DateFormatter.getCurrentDate()));
        try {
            root.addContent(new Element("currentUser").setText(getDomainUserName(session)));
        } catch (Exception e) {
            log.warn("createXmlDocument() no domain user has logged in to current session");
            root.addContent(new Element("currentUser"));
        }
        doc.setRootElement(root);
        return doc;
    }

    /**
     * Checks if user has given permission
     * @param session
     * @param permission key
     * @throws PermissionException if user has no permission
     */
    public static void checkPermission(HttpSession session, String permission)
            throws PermissionException {
        User user = getUser(session);
        if (!user.getPermissions().containsKey(permission)) {
            if (log.isDebugEnabled()) {
                log.debug("checkPermission(" + permission + ") ok");
            }
            throw new PermissionException();
        } else if (log.isDebugEnabled()) {
            log.debug("checkPermission(" + permission + ") failed");
        }
    }

    /**
     * Checks if user has given permission
     * @param session
     * @param permission key
     * @throws PermissionException if user has no permission
     */
    public static void checkPermission(HttpSession session, String[] permissions)
            throws PermissionException {
        User user = getUser(session);
        boolean ok = false;
        for (int i = 0, j = permissions.length; i < j; i++) {
            if (user.getPermissions().containsKey(permissions[i])) {
                ok = true;
                break;
            }
        }
        if (!ok) {
            throw new PermissionException();
        }
    }
}
