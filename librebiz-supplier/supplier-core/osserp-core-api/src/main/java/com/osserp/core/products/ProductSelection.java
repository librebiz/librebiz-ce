/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 27, 2009 4:41:24 PM 
 * 
 */
package com.osserp.core.products;

import java.util.List;

import com.osserp.common.PersistentEntity;
import com.osserp.core.employees.Employee;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ProductSelection extends ProductSelectionItemAware, PersistentEntity {

    /**
     * Provides the associated employee
     * @return employee
     */
    Employee getEmployee();

    /**
     * Indicates that all product selections should be ignored (executive mode)
     * @return ignoreSelections
     */
    boolean isIgnoreSelections();

    /**
     * Sets if configured productSelections should be ignored (executive mode)
     * @param ignoreSelections
     */
    void setIgnoreSelections(boolean ignoreSelections);

    /**
     * Provides the associated profiles
     * @return profiles
     */
    List<ProductSelectionProfile> getProfiles();

    /**
     * Adds a profile if not already added
     * @param profile
     */
    void addProfile(ProductSelectionProfile profile);

    /**
     * Removes a profile
     * @param id of the profile
     */
    void removeProfile(Long id);

    /**
     * Indicates that selection contains an product
     * @param product
     * @return true if ignore selections is enabled or any profile contains the product
     */
    boolean supports(Product product);

    /**
     * Indicates that selection contains an product type
     * @param type
     * @return true if ignore selections is enabled or any profile contains the type
     */
    boolean supports(ProductType type);

    /**
     * Filters an product list by invoking {@link #supports(Product)} for each product in list
     * @param products list of products
     * @return products filtered list
     */
    List<Product> filter(List<Product> products);
}
