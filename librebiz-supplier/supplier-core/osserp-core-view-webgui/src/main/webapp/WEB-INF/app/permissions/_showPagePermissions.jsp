<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>

<c:set var="view" value="${sessionScope.showPagePermissionsView}"/>
<c:set var="permissions" value="${view.bean}"/>

<div>
	<table class="table">
		<thead>
			<tr><th><fmt:message key="permission"/></th><th><fmt:message key="description"/></th></tr>
		</thead>
		<tbody>
		<c:choose>
		<c:when test="${empty permissions}">
			<tr><td colspan="2"><fmt:message key="noPermissionsAvailable"/></td></tr>
		</c:when>
		<c:otherwise>
			<c:forEach var="permission" items="${permissions}">
				<tr><td><o:out value="${permission.key}"/></td><td><o:out value="${permission.value}"/></td></tr>
			</c:forEach>
		</c:otherwise>
		</c:choose>
		</tbody>
	</table>
</div>