/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 18, 2014 
 * 
 */
package com.osserp.gui.contacts

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.Option
import com.osserp.core.contacts.Contact
import com.osserp.core.contacts.ContactExportManager
import com.osserp.groovy.web.MenuItem
import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class ContactExportView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(ContactExportView.class.getName())
    
    List<Option> selection = []
    def selected
    
    ContactExportView() {
        super()
        enableAutoreload()
        providesCustomNavigation()
        selection << [id: Contact.CUSTOMER, name: 'customerInterest', description: 'exportContactsHintGroup1'] as Option
        selection << [id: Contact.SUPPLIER, name: 'supplier', description: 'exportContactsHintGroup2'] as Option
        selection << [id: Contact.EMPLOYEE, name: 'employee', description: 'exportContactsHintGroup7'] as Option
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (forwardRequest) {
        }
    }
    
    void selectGroup() {
        Long id = form.getLong('id')
        if (!id) {
            selected = null
        } else {
            selected = selection.find { it.id == id }
        }
        reload()
    }

    void remove() {
        Long id = form.getLong('id')
        if (id) {
            for (Iterator i = list.iterator(); i.hasNext();) {
                Contact contact = i.next()
                if (contact.contactId == id) {
                    i.remove()
                    break
                }
            }
        }
    }
    
    String getSpreadsheet() {
        contactExportManager.createSpreadsheet(list, ';')
    }
    
    @Override
    void reload() {
        if (selected) {
            list = contactExportManager.find(selected.id)
        } else {
            list = []
        }
    }

    @Override
    void createCustomNavigation() {
        if (selected) {
            nav.currentNavigation = [deselectLink, xlsLink, homeLink]
        } else {
            nav.currentNavigation = [exitLink, homeLink]
        }
    }
    
    MenuItem getXlsLink() {
        navigationLink("/${context.name}/renderXls", 'tableIcon', 'printXls')
    }
    
    MenuItem getDeselectLink() {
        navigationLink("/${context.name}/selectGroup", 'backIcon', 'backToLast')
    }
    
    private ContactExportManager getContactExportManager() {
        getService(ContactExportManager.class.getName())
    }
}
