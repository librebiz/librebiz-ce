/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 14, 2011 10:15:50 AM 
 * 
 */
package com.osserp.gui.admin.events

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.core.events.EventConfigManager
import com.osserp.groovy.web.MenuItem
import com.osserp.gui.CoreView

/**
 *
 * @author eh <eh@osserp.com>
 * 
 */
class EventPoolsView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(EventPoolsView.class.getName())

    List poolMembers = []

    EventPoolsView() {
        providesCreateMode()
        headerName = 'jobReceiverPool'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (!list) {
            reload()
        }
    }

    @Override
    void createCustomNavigation() {
        nav.beanNavigation = nav.defaultNavigation
        nav.beanListNavigation = nav.defaultNavigation
        nav.listNavigation = nav.defaultNavigation
    }

    @Override
    MenuItem getCreateLink() {
        def createLink = super.createLink
        createLink.permissions = 'event_pool_config,executive'
        createLink.permissionInfo = 'permissionCreateEventPool'
        return createLink
    }

    @Override
    void reload() {
        list = eventConfigManager.findPools()
        if (bean) {
            def poolId = bean.id
            bean = eventConfigManager.findPool(poolId)
            updatePoolMembers()
        }
    }

    protected void updatePoolMembers() {
        poolMembers = []
        if (bean) {
            bean.members.each {
                poolMembers.add(it.recipientId)
            }
        }
    }

    @Override
    void select() {
        super.select()
        updatePoolMembers()
    }

    @Override
    void save() {
        def poolName = form.getString('name')
        if (poolName) {
            if (createMode) {
                bean = eventConfigManager.createPool(poolName)
                logger.debug("create: created new pool [id=${bean.id}, name=${bean.name}]")
                disableCreateMode()
            } else {
                bean.name = poolName
                eventConfigManager.save(bean)
                disableEditMode()
            }
            reload()
        } else {
            throw new ClientException(ErrorCode.VALUES_MISSING)
        }
    }

    void addEmployee() {
        def employeeId = form.getLong('id')
        if (employeeId) {
            bean.addEmployee(employeeId)
            eventConfigManager.save(bean)
            reload()
        }
    }

    void removeEmployee() {
        def employeeId = form.getLong('value')
        if (employeeId) {
            bean.removeEmployee(employeeId)
            eventConfigManager.save(bean)
            reload()
        }
    }

    void removePool() {
        Long id = form.getLong('id')
        if (id) {
            bean.removeEmployee(employeeId)
            eventConfigManager.save(bean)
            reload()
        }
    }

    @Override
    void disableEditMode() {
        super.disableEditMode()
        bean = null
    }

    @Override
    void enableEditMode() {
        Long id = form.getLong('id')
        bean = list.find { it.id == id }
        if (bean) {
            super.enableEditMode()
        }
    }

    def getEventConfigManager() {
        getService(EventConfigManager.class.getName())
    }
}

