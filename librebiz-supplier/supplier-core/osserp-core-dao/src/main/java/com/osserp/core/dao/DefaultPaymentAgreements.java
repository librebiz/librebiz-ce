/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 22, 2009 10:15:04 AM 
 * 
 */
package com.osserp.core.dao;

import java.util.List;

import com.osserp.core.BusinessType;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.PaymentAgreement;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface DefaultPaymentAgreements {

    /**
     * Creates a new default payment agreement by business type
     * @param user
     * @param businessType
     * @return new created default payment agreement
     */
    PaymentAgreement createDefaultPaymentAgreement(Employee user, BusinessType businessType);

    /**
     * Loads an existing default payment agreement by primary key
     * @param id
     * @return payment agreement
     * @throws RuntimeException if no such object exists
     */
    PaymentAgreement load(Long id);

    /**
     * Finds all default payment agreements
     * @return findAll
     */
    List<PaymentAgreement> findAll();

    /**
     * Finds default payment agreement of a business type
     * @param businessType
     * @return paymentAgreement or null of not exists
     */
    PaymentAgreement find(BusinessType businessType);

    /**
     * Persists a payment agreement
     * @param obj
     */
    void save(PaymentAgreement obj);
}
