/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 25-Jun-2005 16:29:04 
 * 
 */
package com.osserp.gui.web.projects.billing;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.ViewManager;
import com.osserp.common.web.struts.StrutsForm;

import com.osserp.core.finance.Invoice;
import com.osserp.gui.BusinessCaseView;
import com.osserp.gui.client.Actions;
import com.osserp.gui.client.sales.SalesBillingView;
import com.osserp.gui.client.sales.SalesInvoiceView;
import com.osserp.gui.client.sales.SalesUtil;
import com.osserp.gui.web.sales.AbstractSalesAction;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProjectBillingAction extends AbstractSalesAction {
    private static Logger log = LoggerFactory.getLogger(ProjectBillingAction.class.getName());

    /**
     * Creates view and forwards to project billing
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    @Override
    public ActionForward forward(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("forward() request from " + getUser(session).getId());
        }
        try {
            SalesUtil.createBilling(
                    servlet.getServletContext(),
                    request);
            return mapping.findForward(Actions.SUCCESS);
        } catch (Throwable e) {
            log.error("forward() caught exception [message=" + e.getMessage() + "]", e);
            saveError(request, ErrorCode.HISTORICAL_DATA);
            return mapping.findForward(Actions.ERROR);
        }
    }

    /**
     * Selects the invoice type and returns back to edit page
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward selectInvoiceType(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("selectInvoiceType() request from " + getUser(session).getId());
        }
        SalesBillingView billingView = SalesUtil.createBillingIfNotExists(servlet.getServletContext(), request);
        Long id = RequestUtil.getId(request);
        try {
            billingView.setBillingType(id);
            if (log.isDebugEnabled()) {
                log.debug("selectInvoiceType() invoice type [" + id
                        + "], billing type ["
                        + billingView.getBillingType()
                        + "]");
            }
        } catch (ClientException e) {
            saveError(request, e.getMessage());
        }
        try {
            // TODO avoid form, store values in view properties
            Form fh = new StrutsForm(form);
            if (id.equals(fh.getLong(Form.ID))) {
                fh.setValue(Form.ID, null);
                request.setAttribute("billingForm", form);
            }
        } catch (Exception e) {
            // ignore this
        }
        return mapping.findForward(Actions.EDIT);
    }

    /**
     * Creates a new invoice
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward createInvoice(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("createInvoice() request from " + getUser(session).getId());
        }
        ActionForward forward = null;
        SalesBillingView billingView = SalesUtil.getBillingView(session);
        Long type = billingView.getBillingTypeByCurrentFcs();
        if (billingView.isFlowControlContext()) {
            type = billingView.getBillingTypeByCurrentFcs();
        } else {
            type = billingView.getBillingType();
            if (type == null || type.equals(Constants.LONG_NULL)) {
                if (log.isDebugEnabled()) {
                    log.debug("createInvoice() type not set, returning to input");
                }
                return new ActionForward(mapping.getInput());
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("createInvoice() selected type " + type);
        }
        try {
            Invoice invoice = null;
            if (Invoice.PARTIAL.equals(type)) {
                invoice = billingView.addPartialInvoice(new StrutsForm(form));
            } else {
                invoice = billingView.addInvoice(type, new StrutsForm(form));
            }
            if (billingView.isFlowControlContext()) {
                forward = mapping.findForward(Actions.NEXT);
                SalesUtil.removeBillingView(session);
            } else {
                
                SalesInvoiceView view = createSalesInvoiceView(request);
                if (invoice.isDownpayment()) {
                    view.loadDownpayment(invoice.getId(), Actions.BILLING, null, false);
                } else {
                    view.loadRecord(invoice.getId(), Actions.BILLING, null, false);
                }
                forward = mapping.findForward(Actions.INVOICE);
            }
        } catch (ClientException cx) {
            saveError(request, cx.getMessage());
            forward = new ActionForward(mapping.getInput());
        }
        return forward;
    }

    /**
     * Creates a new credit note
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward createCreditNote(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exit() request from " + getUser(session).getId());
        }
        return mapping.findForward(Actions.SUCCESS);
    }

    /**
     * Exits form and forwards to billing list
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward exit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exit() request from " + getUser(session).getId());
        }
        SalesBillingView view = SalesUtil.getBillingView(session);
        view.addThirdPartyRecipient(null);
        if (view.isFlowControlContext()) {
            // billing is no longer required
            SalesUtil.removeBillingView(session);
            this.removeInvoiceView(session);
            return mapping.findForward(Actions.FCS_EDIT);

        }
        try {
            view.setBillingType(Constants.LONG_NULL);
        } catch (Exception ignorable) {
            // we do nothing as logging here
            if (log.isDebugEnabled()) {
                log.debug("exit() failed while resetting billing editor: " + ignorable.toString());
            }
        }
        return mapping.findForward(Actions.SUCCESS);
    }

    /**
     * Exits form and forwards to billing list
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward exitEdit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exitEdit() request from " + getUser(session).getId());
        }
        BusinessCaseView bcv = fetchBusinessCaseView(session);
        if (bcv != null && bcv.isFlowControlMode()) {
            return mapping.findForward(Actions.LIST);

        }
        try {
            SalesBillingView view = SalesUtil.getBillingView(session);
            view.setBillingType(Constants.LONG_NULL);
        } catch (Exception ignorable) {
            // we do nothing as logging here
            if (log.isDebugEnabled()) {
                log.debug("exit() failed while resetting billing editor: " + ignorable.toString());
            }
        }
        return mapping.findForward(Actions.EXIT);
    }

    /**
     * Removes billing objects and exits from overview to project display via 'exit' target
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward exitDisplay(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exitDisplay() request from " + getUser(session).getId());
        }
        try {
            BusinessCaseView bcv = fetchBusinessCaseView(session);
            if (bcv != null) {
                bcv.reload();
            }
        } catch (Exception ignorable) { 
        }
        try {
            SalesUtil.removeBillingView(session);
        } catch (Exception ignorable) { 
        }
        try {
            removeInvoiceView(session);
        } catch (Exception ignorable) {
        }
        return mapping.findForward(Actions.EXIT);
    }

    /**
     * Exits open invoice list and returns to fcs display
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward exitInvoiceSelection(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exitInvoiceSelection() request from " + getUser(session).getId());
        }
        exitDisplay(mapping, form, request, response);
        return mapping.findForward(Actions.FCS_DISPLAY);
    }

    /**
     * Adds a previous selected customer by customer search view and adds <br/>
     * selected customer as third party recipient for next record
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward selectCustomer(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("selectCustomer() request from " + getUser(session).getId());
        }
        SalesBillingView view = SalesUtil.getBillingView(session);
        Long id = RequestUtil.fetchId(request);
        if (id == null) {
            saveError(request, ErrorCode.SELECTION_MISSING);
        } else {
            view.addThirdPartyRecipient(id);
        }
        return mapping.findForward(Actions.EDIT);
    }

    /**
     * Deselects a previous added third party customer
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward deselectCustomer(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("deselectCustomer() request from " + getUser(session).getId());
        }
        SalesBillingView view = SalesUtil.getBillingView(session);
        view.addThirdPartyRecipient(null);
        return mapping.findForward(Actions.EDIT);
    }

    private SalesInvoiceView createSalesInvoiceView(HttpServletRequest request) throws ClientException, PermissionException {
        SalesInvoiceView view = (SalesInvoiceView) ViewManager.newInstance(servlet.getServletContext()).createView(request,
                SalesInvoiceView.class.getName());
        return view;
    }

    private void removeInvoiceView(HttpSession session) {
        ViewManager.removeView(session, SalesInvoiceView.class);
    }
}
