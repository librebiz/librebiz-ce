/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 29, 2007 7:45:44 AM 
 * 
 */
package com.osserp.core.service.fcs.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.common.OptionsCache;
import com.osserp.core.FcsAction;
import com.osserp.core.FcsActionManager;
import com.osserp.core.dao.FcsActions;
import com.osserp.core.service.impl.AbstractService;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractFcsActionManager extends AbstractService implements FcsActionManager {
    private static Logger log = LoggerFactory.getLogger(AbstractFcsActionManager.class.getName());

    private FcsActions actions = null;
    private OptionsCache optionsCache = null;

    protected AbstractFcsActionManager(FcsActions actions, OptionsCache optionsCache) {
        super();
        this.actions = actions;
        this.optionsCache = optionsCache;
    }

    public FcsAction findById(Long id) {
        if (log.isDebugEnabled()) {
            log.debug("findById() id: " + id);
        }
        Map<Long, Option> existing = optionsCache.getMap(getCachedName());
        return (FcsAction) existing.get(id);
    }

    public List<FcsAction> findByType(Long typeId) {
        return actions.findByType(typeId);
    }

    public Map<Long, FcsAction> createMap() {
        Map<Long, FcsAction> result = new HashMap<Long, FcsAction>();
        List<Option> all = optionsCache.getList(getCachedName());
        for (int i = 0, j = all.size(); i < j; i++) {
            FcsAction next = (FcsAction) all.get(i);
            result.put(next.getId(), next);
        }
        return result;
    }

    public final void update(FcsAction fca) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("update() invoked...");
        }
        if (fca.isDisplayEverytime() && fca.getStatus() != null
                && fca.getStatus().intValue() > 0) {
            throw new ClientException(ErrorCode.FLOW_CONTROL_DISPLAY_EVERYTIME);
        }
        actions.save(fca);
        if (log.isDebugEnabled()) {
            log.debug("update() done for action " + fca.getId());
        }
        optionsCache.refresh(getCachedName());
    }

    protected abstract String getCachedName();

    protected FcsActions getActions() {
        return actions;
    }

    protected OptionsCache getCache() {
        return optionsCache;
    }
}
