/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 30-Sep-2006 07:13:06 
 * 
 */
package com.osserp.core.views;

import java.io.Serializable;

import com.osserp.common.Entity;
import com.osserp.common.beans.AbstractEntity;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class EventDisplayView extends AbstractEntity implements Entity, Serializable {

    private Long actionId = null;
    private Long type = null;
    private String sourceName = null;
    private String targetName = null;
    private String groupName = null;
    private boolean recipientSales = false;
    private boolean recipientManager = false;
    private Long configType = null;
    private String configTypeName = null;

    protected EventDisplayView() {
        super();
    }

    /**
     * Constructor for start actions
     * @param id
     * @param type
     * @param source
     * @param target
     * @param group
     * @param sales
     * @param manager
     */
    public EventDisplayView(
            Long id,
            Long type,
            String source,
            String target,
            String group,
            boolean sales,
            boolean manager,
            Long configType,
            String configTypeName,
            Long actionId) {
        super(id);
        this.type = type;
        this.sourceName = source;
        this.targetName = target;
        this.groupName = group;
        this.recipientSales = sales;
        this.recipientManager = manager;
        this.configType = configType;
        this.configTypeName = configTypeName;
        this.actionId = actionId;
    }

    /**
     * Constructor for stop actions
     * @param id
     * @param type
     * @param source
     * @param target
     */
    public EventDisplayView(
            Long id,
            Long type,
            String source,
            String target) {

        super(id);
        this.type = type;
        this.sourceName = source;
        this.targetName = target;
    }

    public Long getActionId() {
        return actionId;
    }

    public void setActionId(Long actionId) {
        this.actionId = actionId;
    }

    public Long getType() {
        return type;
    }

    public String getSourceName() {
        return sourceName;
    }

    public String getTargetName() {
        return targetName;
    }

    public String getGroupName() {
        return groupName;
    }

    public boolean isRecipientSales() {
        return recipientSales;
    }

    public boolean isRecipientManager() {
        return recipientManager;
    }

    public Long getConfigType() {
        return configType;
    }

    public String getConfigTypeName() {
        return configTypeName;
    }

    // setters are protected for views

    protected void setType(Long type) {
        this.type = type;
    }

    protected void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    protected void setTargetName(String targetName) {
        this.targetName = targetName;
    }

    protected void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    protected void setRecipientSales(boolean recipientSales) {
        this.recipientSales = recipientSales;
    }

    protected void setRecipientManager(boolean recipientManager) {
        this.recipientManager = recipientManager;
    }

    protected void setConfigType(Long configType) {
        this.configType = configType;
    }

    protected void setConfigTypeName(String configTypeName) {
        this.configTypeName = configTypeName;
    }
}
