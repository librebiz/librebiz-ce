/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 29-Jan-2007 19:05:16 
 * 
 */
package com.osserp.core.model.records;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.common.beans.OptionImpl;
import com.osserp.common.util.AddressValidator;
import com.osserp.common.util.NumberUtil;

import com.osserp.core.Address;
import com.osserp.core.BusinessCase;
import com.osserp.core.Item;
import com.osserp.core.ItemPosition;
import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Amounts;
import com.osserp.core.finance.BookingType;
import com.osserp.core.finance.CalculationAwareRecord;
import com.osserp.core.finance.RebateAwareRecord;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordInfo;
import com.osserp.core.finance.RecordMailLog;
import com.osserp.core.finance.RecordType;
import com.osserp.core.model.AddressImpl;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductSelectionConfigItem;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractRecord extends AbstractFinanceRecord implements Record {
    private static Logger log = LoggerFactory.getLogger(AbstractRecord.class.getName());

    private BookingType bookingType = null;
    private Long status = Record.STAT_NEW;
    private Date statusDate = null;
    private Long signatureLeft = null;
    private Long signatureRight = null;
    private Long personId = null;
    private Long shippingId = null;
    private boolean itemsChangeable = false;
    private boolean itemsEditable = false;
    private String language = Constants.DEFAULT_LANGUAGE;
    private List<Item> items = new ArrayList<>();
    private List<Item> options = new ArrayList<>();
    // common calculated values
    protected AmountsImpl amounts = null;
    private Address deliveryAddress = null;
    private Date dateSent = null;
    private Date initialDate = null;
    private String timeOfSupply;
    private String placeOfPerformance;

    private boolean customizedInfos = false;
    private List<RecordInfo> infos = new ArrayList<>();
    private List<RecordMailLog> mailLogs = new ArrayList<>();

    private boolean printPaymentNote;
    private boolean printPaymentTarget;
    private boolean printComplimentaryClose;
    private boolean printProjectmanager;
    private boolean printSalesperson;

    private boolean printBusinessCaseId;
    private boolean printBusinessCaseInfo;
    private boolean printConfirmationPlaceholder;
    private boolean printCoverLetter;

    private boolean printRecordDate;
    private boolean printRecordDateByStatus;

    private boolean printTaxPoint;
    private boolean printTaxPointByItems;
    private boolean printTimeOfSupply;
    private boolean printPlaceOfPerformance;

    /**
     * Default constructor required by Serializable
     */
    protected AbstractRecord() {
        super();
    }

    /**
     * Creates an abstract record for usage with items calculated external. Items are not editable and changeable by default.
     * @param id
     * @param company
     * @param branchId
     * @param type
     * @param reference
     * @param contact
     * @param createdBy
     * @param businessCaseId
     * @param taxRate
     * @param reducedTaxRate
     */
    protected AbstractRecord(
            Long id,
            Long company,
            Long branchId,
            RecordType type,
            Long reference,
            ClassifiedContact contact,
            Employee createdBy,
            Long businessCaseId,
            Double taxRate,
            Double reducedTaxRate) {
        super(
                id,
                company,
                branchId,
                type,
                reference,
                contact,
                createdBy,
                businessCaseId);
        amounts = new AmountsImpl(taxRate, reducedTaxRate);
    }

    /**
     * Creates an abstract record with customized change-item flags
     * @param id
     * @param company
     * @param branchId
     * @param type
     * @param reference
     * @param contact
     * @param createdBy
     * @param businessCaseId
     * @param taxRate
     * @param reducedTaxRate
     * @param itemsChangeable
     * @param itemsEditable
     */
    protected AbstractRecord(
            Long id,
            Long company,
            Long branchId,
            RecordType type,
            Long reference,
            ClassifiedContact contact,
            Employee createdBy,
            Long businessCaseId,
            Double taxRate,
            Double reducedTaxRate,
            boolean itemsChangeable,
            boolean itemsEditable) {
        this(id, company, branchId, type, reference, contact, createdBy, businessCaseId, taxRate, reducedTaxRate);
        this.itemsChangeable = itemsChangeable;
        this.itemsEditable = itemsEditable;
    }

    /**
     * Creates an abstract record as copy of another record.
     * @param id
     * @param type
     * @param reference
     * @param createdBy
     * @param copyReferenceId indicates if id of reference should be set as reference of new record
     * @param changeable indictes if items should be changeable after create by reference.items
     */
    protected AbstractRecord(
            Long id,
            RecordType type,
            Record reference,
            Employee createdBy,
            boolean copyReferenceId,
            boolean changeable) {
        this(
                id,
                reference.getCompany(),
                reference.getBranchId(),
                type,
                (copyReferenceId ? reference.getReference() : null),
                reference.getContact(),
                createdBy,
                reference.getBusinessCaseId(),
                reference.getAmounts().getTaxRate(),
                reference.getAmounts().getReducedTaxRate());
        setNote(reference.getNote());
        setCurrency(reference.getCurrency());
        setPrintComplimentaryClose(reference.isPrintComplimentaryClose());
        setPrintPaymentTarget(reference.isPrintPaymentTarget());
        setPrintProjectmanager(reference.isPrintProjectmanager());
        setPrintRecordDate(reference.isPrintRecordDate());
        setPrintRecordDateByStatus(reference.isPrintRecordDateByStatus());
        setPrintSalesperson(reference.isPrintSalesperson());

        createItems(reference.getItems(), changeable);

        if (reference instanceof CalculationAwareRecord) {
            createOptions(((CalculationAwareRecord) reference).getOptions());
        }
    }

    /**
     * This constructor is currently used by AbstractOffer
     * @param id
     * @param company
     * @param branchId
     * @param type
     * @param reference
     * @param contact
     * @param createdBy
     * @param businessCaseId
     * @param taxRate
     * @param reducedTaxRate
     * @param calc
     */
    protected AbstractRecord(
            Long id,
            Long company,
            Long branchId,
            RecordType type,
            Long reference,
            ClassifiedContact contact,
            Employee createdBy,
            Long businessCaseId,
            Double taxRate,
            Double reducedTaxRate,
            List<ItemPosition> items,
            List<ItemPosition> optionalItems) {
        this(
                id,
                company,
                branchId,
                type,
                reference,
                contact,
                createdBy,
                businessCaseId,
                taxRate,
                reducedTaxRate);
        updateItems(items, optionalItems);
    }

    public abstract boolean isSales();

    public Address getDeliveryAddress() {
        return deliveryAddress;
    }

    protected void setDeliveryAddress(Address deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    /**
     * Updates the delivery address of the order
     * @param name
     * @param street
     * @param streetAddon
     * @param zipcode
     * @param city
     * @param country
     * @throws ClientException if any given value is empty or null
     */
    public void updateDeliveryAddress(
            String name,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long country)
            throws ClientException {

        AddressValidator.validateAddress(street, zipcode, city, country, true);
        if (deliveryAddress == null) {
            deliveryAddress = new AddressImpl(name, street, streetAddon, zipcode, city, country);
        } else {
            deliveryAddress.setName(name);
            deliveryAddress.setStreet(street);
            deliveryAddress.setZipcode(zipcode);
            deliveryAddress.setCity(city);
            deliveryAddress.setCountry(country);
        }
    }

    public void resetDeliveryAddress() {
        if (deliveryAddress != null) {
            deliveryAddress.setName(null);
            deliveryAddress.setStreet(null);
            deliveryAddress.setZipcode(null);
            deliveryAddress.setCity(null);
            deliveryAddress.setCountry(null);
        }
    }

    protected Item fetchItem(Long id) {
        for (int i = 0, j = items.size(); i < j; i++) {
            Item next = items.get(i);
            if (next.getId().equals(id)) {
                return next;
            }
        }
        return null;
    }

    protected abstract Item createItem(
            Long stockId,
            Product product,
            String customName,
            Double quantity,
            Double taxRate,
            BigDecimal price,
            Date priceDate,
            BigDecimal partnerPrice,
            boolean partnerPriceEditable,
            boolean partnerPriceOverridden,
            BigDecimal purchasePrice,
            String note,
            boolean includePrice,
            Long externalId);

    protected void addItem(Item item) {
        items.add(item);
    }

    public void changeContact(ClassifiedContact relatedContact, Long contactChangedBy) {
        super.changeContact(relatedContact);
        setChangedBy(contactChangedBy);
        setChanged(new Date(System.currentTimeMillis()));
    }

    public final void update(
            Long signatureLeft,
            Long signatureRight,
            Long personId,
            String note,
            boolean taxFree,
            Long taxFreeId,
            Double taxRate,
            Double reducedTaxRate,
            Long currency,
            String language,
            Long branchId,
            Long shippingId,
            String customHeader,
            boolean printComplimentaryClose,
            boolean printProjectmanager,
            boolean printSalesperson,
            boolean printBusinessId,
            boolean printBusinessInfo,
            boolean printRecordDate,
            boolean printRecordDateByStatus,
            boolean printPaymentTarget,
            boolean printConfirmationPlaceholder,
            boolean printCoverLetter) throws ClientException {

        this.signatureLeft = signatureLeft;
        this.signatureRight = signatureRight;
        setNote(note);
        if (taxFree) {
            if (taxFreeId == null || taxFreeId == 0) {
                throw new ClientException(ErrorCode.TAX_FREE_REASON_MISSING);
            }
            setTaxFree(true);
            setTaxFreeId(taxFreeId);
        } else {
            setTaxFree(false);
            setTaxFreeId(null);
        }
        setCurrency(currency);
        if (language == null) {
            this.language = Constants.DEFAULT_LANGUAGE;
        } else {
            this.language = language;
        }
        if (taxRate != null && reducedTaxRate != null
                && (!getAmounts().getTaxRate().equals(taxRate)
                        || !getAmounts().getReducedTaxRate().equals(reducedTaxRate))) {
            updateTax(taxRate, reducedTaxRate);
        }
        this.personId = personId;
        if (isSet(shippingId)) {
            this.shippingId = shippingId;
        } else {
            this.shippingId = null;
        }
        if (branchId != null) {
            setBranchId(branchId);
        }
        setCustomHeader(customHeader);
        this.printComplimentaryClose = printComplimentaryClose;
        this.printProjectmanager = printProjectmanager;
        this.printSalesperson = printSalesperson;
        this.printBusinessCaseId = printBusinessId;
        this.printBusinessCaseInfo = printBusinessInfo;
        this.printRecordDate = printRecordDate;
        this.printRecordDateByStatus = printRecordDateByStatus;
        this.printPaymentTarget = printPaymentTarget;
        this.printConfirmationPlaceholder = printConfirmationPlaceholder;
        this.printCoverLetter = printCoverLetter;
        calculateSummary();
    }

    protected void updateItems(Record other) {
        items.clear();
        for (int i = 0, j = other.getItems().size(); i < j; i++) {
            Item next = other.getItems().get(i);
            addItem(
                    next.getStockId(),
                    next.getProduct(),
                    next.getCustomName(),
                    next.getQuantity(),
                    next.getTaxRate(),
                    next.getPrice(),
                    next.getPriceDate(),
                    next.getPartnerPrice(),
                    next.isPartnerPriceEditable(),
                    next.isPartnerPriceOverridden(),
                    next.getPurchasePrice(),
                    next.getNote(),
                    next.isIncludePrice(),
                    next.getExternalId());
        }
        if (other instanceof CalculationAwareRecord) {
            options.clear();
            createOptions(((CalculationAwareRecord) other).getOptions());
        }
    }

    protected void addItems(List<Item> itemList) {
        for (int i = 0, j = itemList.size(); i < j; i++) {
            Item next = itemList.get(i);
            addItem(
                    next.getStockId(),
                    next.getProduct(),
                    next.getCustomName(),
                    next.getQuantity(),
                    next.getTaxRate(),
                    next.getPrice(),
                    next.getPriceDate(),
                    next.getPartnerPrice(),
                    next.isPartnerPriceEditable(),
                    next.isPartnerPriceOverridden(),
                    next.getPurchasePrice(),
                    next.getNote(),
                    next.isIncludePrice(),
                    next.getExternalId());
        }
    }

    public BookingType getBookingType() {
        return bookingType;
    }

    protected void setBookingType(BookingType bookingType) {
        this.bookingType = bookingType;
    }

    public String getLanguage() {
        return language;
    }

    protected void setLanguage(String language) {
        this.language = language;
    }

    public final Long getStatus() {
        return status;
    }

    protected void setStatus(Long status) {
        this.status = status;
    }

    public void updateStatus(Long newStatus) {
        Long current = status;
        if (current == null) {
            status = newStatus;
            statusDate = new Date(System.currentTimeMillis());
        } else if (!Record.STAT_HISTORICAL.equals(current)
                && newStatus.longValue() > current.longValue()) {
            status = newStatus;
            statusDate = new Date(System.currentTimeMillis());
        }
    }

    public Date getStatusDate() {
        return statusDate;
    }

    protected void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

    public void markChanged() {
        status = STAT_CHANGED;
        statusDate = new Date(System.currentTimeMillis());
    }

    public Long getSignatureLeft() {
        return signatureLeft;
    }

    public void setSignatureLeft(Long signatureLeft) {
        this.signatureLeft = signatureLeft;
    }

    public Long getSignatureRight() {
        return signatureRight;
    }

    public void setSignatureRight(Long signatureRight) {
        this.signatureRight = signatureRight;
    }

    public Long getShippingId() {
        return shippingId;
    }

    public void setShippingId(Long shippingId) {
        this.shippingId = shippingId;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public boolean isPrintPaymentTarget() {
        return printPaymentTarget;
    }

    public void setPrintPaymentTarget(boolean printPaymentTarget) {
        this.printPaymentTarget = printPaymentTarget;
    }

    public boolean isPrintPaymentNote() {
        return printPaymentNote;
    }

    public void setPrintPaymentNote(boolean printPaymentNote) {
        this.printPaymentNote = printPaymentNote;
    }

    public boolean isPrintComplimentaryClose() {
        return printComplimentaryClose;
    }

    public void setPrintComplimentaryClose(boolean printComplimentaryClose) {
        this.printComplimentaryClose = printComplimentaryClose;
    }

    public boolean isPrintProjectmanager() {
        return printProjectmanager;
    }

    public void setPrintProjectmanager(boolean printProjectmanager) {
        this.printProjectmanager = printProjectmanager;
    }

    public boolean isPrintSalesperson() {
        return printSalesperson;
    }

    public void setPrintSalesperson(boolean printSalesperson) {
        this.printSalesperson = printSalesperson;
    }

    public boolean isPrintBusinessCaseId() {
        return printBusinessCaseId;
    }

    public void setPrintBusinessCaseId(boolean printBusinessCaseId) {
        this.printBusinessCaseId = printBusinessCaseId;
    }

    public boolean isPrintBusinessCaseInfo() {
        return printBusinessCaseInfo;
    }

    public void setPrintBusinessCaseInfo(boolean printBusinessCaseInfo) {
        this.printBusinessCaseInfo = printBusinessCaseInfo;
    }

    public boolean isPrintConfirmationPlaceholder() {
        return printConfirmationPlaceholder;
    }

    public void setPrintConfirmationPlaceholder(boolean printConfirmationPlaceholder) {
        this.printConfirmationPlaceholder = printConfirmationPlaceholder;
    }

    public boolean isPrintCoverLetter() {
        return printCoverLetter;
    }

    public void setPrintCoverLetter(boolean printCoverLetter) {
        this.printCoverLetter = printCoverLetter;
    }

    public boolean isPrintRecordDate() {
        return printRecordDate;
    }

    public void setPrintRecordDate(boolean printRecordDate) {
        this.printRecordDate = printRecordDate;
    }

    public boolean isPrintRecordDateByStatus() {
        return printRecordDateByStatus;
    }

    public void setPrintRecordDateByStatus(boolean printRecordDateByStatus) {
        this.printRecordDateByStatus = printRecordDateByStatus;
    }

    public boolean isPrintTaxPoint() {
        return printTaxPoint;
    }

    public void setPrintTaxPoint(boolean printTaxPoint) {
        this.printTaxPoint = printTaxPoint;
    }

    public boolean isPrintTaxPointByItems() {
        return printTaxPointByItems;
    }

    public void setPrintTaxPointByItems(boolean printTaxPointByItems) {
        this.printTaxPointByItems = printTaxPointByItems;
    }

    public boolean isPrintTimeOfSupply() {
        return printTimeOfSupply;
    }

    public void setPrintTimeOfSupply(boolean printTimeOfSupply) {
        this.printTimeOfSupply = printTimeOfSupply;
    }

    public boolean isPrintPlaceOfPerformance() {
        return printPlaceOfPerformance;
    }

    public void setPrintPlaceOfPerformance(boolean printPlaceOfPerformance) {
        this.printPlaceOfPerformance = printPlaceOfPerformance;
    }

    public boolean isItemsLocked() {
        if (isUnchangeable() && !isHistorical()) {
            return true;
        }
        if (!isItemsChangeable() && !isItemsEditable()) {
            return true;
        }
        return false;
    }

    public boolean isItemsChangeable() {
        return itemsChangeable;
    }

    public void setItemsChangeable(boolean itemsChangeable) {
        this.itemsChangeable = itemsChangeable;
    }

    public boolean isItemsEditable() {
        return itemsEditable;
    }

    public void setItemsEditable(boolean itemsEditable) {
        this.itemsEditable = itemsEditable;
    }

    public boolean isItemsDeletable() {
        return false;
    }

    public final boolean isItemsAffectStock() {
        if (!getItems().isEmpty()) {
            for (int i = 0, j = getItems().size(); i < j; i++) {
                Item next = getItems().get(i);
                if (next.isDeliveryNoteAffecting()) {
                    return true;
                }
            }
        }
        return false;
    }

    public void addItem(
            Long stockId,
            Product product,
            String customName,
            Double quantity,
            Double taxRate,
            BigDecimal price,
            Date priceDate,
            BigDecimal partnerPrice,
            boolean partnerPriceEditable,
            boolean partnerPriceOverridden,
            BigDecimal purchasePrice,
            String note,
            boolean includePrice,
            Long externalId) {

        if (quantity == null || quantity == 0) {
            quantity = 1d;
        }
        Item created = createItem(
                stockId, product, customName, quantity, taxRate, price,
                priceDate, partnerPrice, partnerPriceEditable,
                partnerPriceOverridden, purchasePrice, note, includePrice,
                externalId);
        items.add(created);
        calculateSummary();
    }

    public void deleteItem(Item toDelete) {
        for (Iterator<Item> i = items.iterator(); i.hasNext();) {
            Item next = i.next();
            if (next.getId().equals(toDelete.getId())) {
                i.remove();
                break;
            }
        }
        int orderId = 0;
        for (Iterator<Item> i = items.iterator(); i.hasNext();) {
            Item next = i.next();
            next.setOrderId(orderId);
            orderId++;
        }
        calculateSummary();
    }

    public void disableItemChangeable() {
        setItemsChangeable(false);
        setItemsEditable(false);
    }

    public void pasteItem(Item itemToCut, Long itemToPasteToId) {
        assert itemToCut != null && itemToPasteToId != null;
        Item toPasteTo = fetchItem(itemToPasteToId);
        if (toPasteTo != null) {
            int orderToCut = itemToCut.getOrderId();
            int orderToPasteTo = toPasteTo.getOrderId();
            for (Iterator<Item> i = items.iterator(); i.hasNext();) {
                Item next = i.next();
                if (next.getId().equals(itemToCut.getId())) {
                    next.setOrderId(orderToPasteTo);
                } else if (next.getId().equals(toPasteTo.getId())) {
                    if (itemToCut.getOrderId() > orderToPasteTo) {
                        next.setOrderId(next.getOrderId() + 1);
                    } else {
                        next.setOrderId(next.getOrderId() - 1);
                    }
                } else if (!((next.getOrderId() > orderToPasteTo && next.getOrderId() > orderToCut) ||
                        (next.getOrderId() < orderToPasteTo && next.getOrderId() < orderToCut))) {

                    if (next.getOrderId() < orderToPasteTo) {
                        next.setOrderId(next.getOrderId() - 1);

                    } else if (next.getOrderId() > orderToPasteTo) {
                        next.setOrderId(next.getOrderId() + 1);
                    }
                }
            }
        }
    }

    public void replaceItem(Item item, Product product) {
        assert item != null && product != null;
        for (Iterator<Item> i = items.iterator(); i.hasNext();) {
            Item next = i.next();
            if (next.getId().equals(item.getId())) {
                next.replaceProduct(
                        product,
                        item.getQuantity(),
                        item.getPrice(),
                        item.getPriceDate(),
                        item.getPartnerPrice(),
                        item.isPartnerPriceEditable(),
                        item.getPurchasePrice(),
                        item.getNote());
                break;
            }
        }
    }

    public void updateItem(
            Item item,
            String customName,
            Double quantity,
            BigDecimal price,
            Double taxRate,
            Long stockId,
            String note)
            throws ClientException {

        if (log.isDebugEnabled()) {
            log.debug("updateItem() invoked [item="
                    + (item == null ? "null" : item.getId())
                    + ", quantity=" + quantity
                    + ", price=" + price
                    + ", taxRate=" + taxRate
                    + ", note=" + note
                    + ", customName=" + customName
                    + "]");
        }
        if (item == null) {
            throw new BackendException("required item was null!");
        }
        if (quantity == null || quantity == 0) {
            throw new ClientException(ErrorCode.QUANTITY_MISSING);
        }
        for (Iterator<Item> i = items.iterator(); i.hasNext();) {
            Item next = i.next();
            if (next.getId().equals(item.getId())) {
                next.setQuantity(quantity);
                next.setPrice(price == null ? new BigDecimal(Constants.DOUBLE_NULL) : price);
                if (taxRate != null) {
                    next.setTaxRate(taxRate);
                }
                next.setNote(note);
                next.setCustomName(customName);
                if (stockId != null) {
                    next.setStockId(stockId);
                }
                break;
            }
        }
        calculateSummary();
    }

    public List<Item> getItems() {
        return items;
    }

    protected void setItems(List<Item> items) {
        this.items = items;
    }

    protected void clearItems() {
        items.clear();
    }

    public final Map<Long, Item> getGoods() {
        Map<Long, Item> goods = new HashMap<>();
        for (int i = 0, j = getItems().size(); i < j; i++) {
            Item next = getItems().get(i);
            if (goods.containsKey(next.getProduct().getProductId())) {
                Item existing = goods.get(next.getProduct().getProductId());
                existing.setQuantity(existing.getQuantity() + next.getQuantity());
            } else {
                goods.put(next.getProduct().getProductId(), (Item) next.clone());
            }
        }
        return goods;
    }

    public final void moveUpItem(Long itemId) {
        int itemCount = getItems().size();
        if (!(itemCount > 1)) {
            // nothing to do
            return;
        }
        Item itemToMove = fetchItem(itemId);
        int currentOrder = itemToMove.getOrderId();
        if (currentOrder == 0) {
            // nothing to do, item is already first 
            return;
        }
        for (int i = 0, j = itemCount; i < j; i++) {
            Item next = getItems().get(i);
            if (next.getOrderId() == currentOrder - 1) {
                next.setOrderId(currentOrder);
                break;
            }
        }
        itemToMove.setOrderId(currentOrder - 1);
    }

    public final void moveDownItem(Long itemId) {
        int itemCount = getItems().size();
        if (!(itemCount > 1)) {
            // nothing to do  
            return;
        }
        Item itemToMove = fetchItem(itemId);
        int currentOrder = itemToMove.getOrderId();
        if (currentOrder == itemCount - 1) {
            // nothing to do, item is already last
            return;
        }
        for (int i = 0, j = itemCount; i < j; i++) {
            Item next = getItems().get(i);
            if (next.getOrderId() == currentOrder + 1) {
                next.setOrderId(currentOrder);
                break;
            }
        }
        itemToMove.setOrderId(currentOrder + 1);
    }

    public void unlockItems(Employee user) {
        itemsChangeable = true;
        itemsEditable = true;
        updateChanged(user == null ? Constants.SYSTEM_EMPLOYEE : user.getId());
    }

    public boolean isUnchangeable() {
        if (status == null) {
            return true;
        }
        if (isHistorical()) {
            if (!isItemsChangeable() && !isItemsEditable()) {
                return true;
            }
            return false;
        }
        return (status.longValue() >= Record.STAT_SENT.longValue());
    }

    public boolean isCanceled() {
        if (status == null) {
            return false;
        }
        return (status.longValue() == Record.STAT_CANCELED.longValue());
    }

    public final boolean isClosed() {
        if (status == null) {
            return false;
        }
        return (isCanceled() || Record.STAT_CLOSED.equals(status));
    }

    public boolean isHistorical() {
        if (status == null) {
            return true;
        }
        return (status.longValue() == Record.STAT_HISTORICAL.longValue());
    }

    public boolean isRebateAvailable() {
        int itemCount = (items == null ? 0 : items.size());
        if (itemCount != 0) {
            for (int i = 0; i < itemCount; i++) {
                Product next = items.get(i).getProduct();
                if (RebateAwareRecord.DISCOUNT_A.equals(next.getProductId())
                        || RebateAwareRecord.DISCOUNT_B.equals(next.getProductId())
                        || RebateAwareRecord.DISCOUNT_C.equals(next.getProductId())) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isCorrectionAvailable() {
        int itemCount = (items == null ? 0 : items.size());
        if (itemCount != 0) {
            for (int i = 0; i < itemCount; i++) {
                Item nextItem = items.get(i);
                if (nextItem.getQuantity() < 0) {
                    for (int k = 0; k < itemCount; k++) {
                        Item other = items.get(k);
                        if (!other.getId().equals(nextItem.getId())
                                && other.getProduct().getProductId().equals(
                                        nextItem.getProduct().getProductId())) {
                            if ((nextItem.getQuantity() + other.getQuantity()) == 0) {
                                if (log.isDebugEnabled()) {
                                    log.debug("isCorrectionAvailable() reports true [product="
                                            + nextItem.getProduct().getProductId() + "]");
                                }
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    public boolean matching(List<ProductSelectionConfigItem> selection) {
        if (items == null || items.isEmpty()) {
            return true;
        }
        for (int i = 0, j = items.size(); i < j; i++) {
            Item next = items.get(i);
            for (int k = 0, l = selection.size(); k < l; k++) {
                ProductSelectionConfigItem config = selection.get(k);
                if (config.isMatching(next.getProduct())) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean containsProductsOf(Record other) {
        if (items == null || items.isEmpty()) {
            return true;
        }
        for (int i = 0, j = other.getItems().size(); i < j; i++) {
            Item otherItem = other.getItems().get(i);
            for (int k = 0, l = items.size(); k < l; k++) {
                Item item = items.get(k);
                if (item.getProduct().getProductId().equals(otherItem.getProduct().getProductId())) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean containsProduct(Long productId) {
        if (items != null && !items.isEmpty() && productId != null) {
            for (int i = 0, j = items.size(); i < j; i++) {
                Item item = items.get(i);
                if (item.getProduct().getProductId().equals(productId)) {
                    return true;
                }
            }
        }
        return false;
    }

    public Amounts getAmounts() {
        return amounts;
    }

    protected void setAmounts(AmountsImpl amounts) {
        this.amounts = amounts;
    }

    public Amounts getSummary() {
        calculateSummary();
        return amounts;
    }

    public final BigDecimal getAmountWithBaseTax() {
        BigDecimal result = new BigDecimal(0);
        for (int i = 0, j = items.size(); i < j; i++) {
            Item next = items.get(i);
            if (next.isIncludePrice()
                    && !NumberUtil.isZeroTax(next.getTaxRate())
                    && !next.getProduct().isReducedTax()) {
                result = result.add(next.getAmount());
            }
        }
        return result;
    }

    public final BigDecimal getAmountWithReducedTax() {
        BigDecimal result = new BigDecimal(0);
        for (int i = 0, j = items.size(); i < j; i++) {
            Item next = items.get(i);
            if (next.isIncludePrice() && next.getProduct().isReducedTax()) {
                result = result.add(next.getAmount());
            }
        }
        return result;
    }

    public final BigDecimal getAmountWithZeroTax() {
        BigDecimal result = new BigDecimal(0);
        for (int i = 0, j = items.size(); i < j; i++) {
            Item next = items.get(i);
            if (next.isIncludePrice()
                    && NumberUtil.isZeroTax(next.getTaxRate())) {
                result = result.add(next.getAmount());
            }
        }
        return result;
    }

    @Override
    public final void enableTaxFree(Long tfId) {
        super.enableTaxFree(tfId);
        calculateSummary();
    }

    public void updateSummary(
            String taxAmount,
            String reducedTaxAmount,
            String grossAmount) throws ClientException {
        if (amounts != null) {
            amounts.fixedTaxCalculation(
                    taxAmount, reducedTaxAmount, grossAmount);
        }
    }

    public final void calculateSummary() {
        if (!isFixedTaxCalculation()) {
            if (isTaxFree() || !isReducedTaxAvailable()) {
                calculateByBaseTaxOnly();
            } else {
                calculateByMixedTax();
            }
        }
    }

    private boolean isFixedTaxCalculation() {
        return amounts != null && amounts.isFixedTaxCalculation();
    }

    protected void calculateByBaseTaxOnly() {
        BigDecimal amount = new BigDecimal(0);
        BigDecimal amountWithBaseTax = new BigDecimal(0);
        BigDecimal taxAmount = new BigDecimal(0);
        BigDecimal grossAmount = new BigDecimal(0);
        for (int i = 0, j = items.size(); i < j; i++) {
            Item next = items.get(i);
            if (next.isIncludePrice()) {
                BigDecimal nextAmount = next.getAmount();
                if (nextAmount != null) {
                    amount = amount.add(next.getAmount());
                }
            }
        }
        amount = NumberUtil.round(amount, 2);
        amountWithBaseTax = amount;
        if (isTaxFree()) {
            grossAmount = amount;
        } else {
            BigDecimal taxRate = new BigDecimal("1");
            if (amounts.getTaxRate() != null) {
                taxRate = new BigDecimal(amounts.getTaxRate().toString());
            }
            if (taxRate.doubleValue() > 1) {
                // we have a real tax rate
                grossAmount = amount.multiply(taxRate);
                grossAmount = NumberUtil.round(grossAmount, 2);
                taxAmount = grossAmount.subtract(amount);
                taxAmount = NumberUtil.round(taxAmount, 2);

            } else {
                log.warn("calculateByBaseTaxOnly() record " + getId()
                        + " was initialized without tax rate!");
                grossAmount = amount;
                taxAmount = new BigDecimal("0");
            }
        }
        amounts.setAmount(amount);
        amounts.setAmountWithBaseTax(amountWithBaseTax);
        amounts.setTaxAmount(taxAmount);
        amounts.setGrossAmount(grossAmount);
        amounts.setReducedTaxAmount(new BigDecimal(0));
        amounts.setAmountWithReducedTax(new BigDecimal(0));
        //this.amounts.logValues();
    }

    protected void calculateByMixedTax() {
        BigDecimal amount = new BigDecimal(0);
        BigDecimal amountWithBaseTax = new BigDecimal(0);
        BigDecimal taxAmount = new BigDecimal(0);
        BigDecimal amountWithReducedTax = new BigDecimal(0);
        BigDecimal reducedTaxAmount = new BigDecimal(0);
        BigDecimal amountWithZeroTax = new BigDecimal(0);
        Double reducedRate = null;
        Double rate = null;
        for (int i = 0, j = items.size(); i < j; i++) {
            Item next = items.get(i);
            if (next.isIncludePrice()) {
                BigDecimal itemAmount = next.getAmount();
                amount = amount.add(itemAmount);
                if (next.getProduct().isReducedTax()) {
                    reducedRate = next.getTaxRate();
                    amountWithReducedTax = amountWithReducedTax.add(itemAmount);
                    BigDecimal itemGross = itemAmount.multiply(new BigDecimal(reducedRate != null ? reducedRate : 1));
                    reducedTaxAmount = reducedTaxAmount.add(itemGross.subtract(itemAmount));
                } else if (NumberUtil.isZeroTax(next.getTaxRate())) {
                    amountWithZeroTax = amountWithZeroTax.add(itemAmount);
                } else {
                    rate = next.getTaxRate();
                    amountWithBaseTax = amountWithBaseTax.add(itemAmount);
                    BigDecimal itemGross = itemAmount.multiply(new BigDecimal(rate != null ? rate : 1));
                    taxAmount = taxAmount.add(itemGross.subtract(itemAmount));
                }
            }
        }
        BigDecimal grossAmount = amount.add(taxAmount).add(reducedTaxAmount);
        amounts.setAmount(NumberUtil.round(amount, 5));
        amounts.setAmountWithBaseTax(NumberUtil.round(amountWithBaseTax, 5));
        amounts.setAmountWithReducedTax(NumberUtil.round(amountWithReducedTax, 5));
        amounts.setAmountWithZeroTax(NumberUtil.round(amountWithZeroTax, 5));
        amounts.setTaxAmount(NumberUtil.round(taxAmount, 5));
        amounts.setReducedTaxAmount(NumberUtil.round(reducedTaxAmount, 5));
        amounts.setGrossAmount(NumberUtil.round(grossAmount, 5));
        if (reducedRate != null) {
            amounts.setReducedTaxRate(reducedRate);
        }
        if (rate != null) {
            amounts.setTaxRate(rate);
        }
        //this.amounts.logValues();
    }

    public boolean isReducedTaxAvailable() {
        if (items != null) {
            for (int i = 0, j = items.size(); i < j; i++) {
                Item next = items.get(i);
                if (next.getProduct().isReducedTax()
                        || NumberUtil.isZeroTax(next.getTaxRate())) {
                    return true;
                }
            }
        }
        return false;
    }

    private void updateTax(Double taxRate, Double reducedTaxRate) {
        amounts.setReducedTaxRate(reducedTaxRate);
        if (!NumberUtil.isZeroTax(taxRate)) {
            amounts.setTaxRate(taxRate);
        }
        for (int i = 0, j = items.size(); i < j; i++) {
            Item next = items.get(i);
            if (next.getProduct().isReducedTax()) {
                next.setTaxRate(reducedTaxRate);
            } else {
                next.setTaxRate(taxRate);
            }
        }
        calculateSummary();
    }

    public List<RecordInfo> getInfos() {
        return infos;
    }

    protected void setInfos(List<RecordInfo> infos) {
        this.infos = infos;
    }

    public void addInfos(Employee user, List<Long> defaults) {
        infos.clear();
        for (int i = 0, j = defaults.size(); i < j; i++) {
            Long next = defaults.get(i);
            infos.add(createOrderedInfo(user, new OptionImpl(next, null)));
        }
    }

    public void addInfo(Employee user, Option info) {
        for (int i = 0, j = infos.size(); i < j; i++) {
            RecordInfo next = infos.get(i);
            if (next.getInfoId().equals(info.getId())) {
                return;
            }
        }
        initInfoOrderIfRequired();
        infos.add(createOrderedInfo(user, info));
    }

    private RecordInfo createOrderedInfo(Employee user, Option info) {
        RecordInfo obj = createInfo(user, info);
        obj.setOrderId(infos.size());
        return obj;
    }

    public void clearInfos() {
        infos.clear();
    }

    public void removeInfo(RecordInfo info) {
        for (Iterator<RecordInfo> i = infos.iterator(); i.hasNext();) {
            RecordInfo next = i.next();
            if (next.getId().equals(info.getId())) {
                if (log.isDebugEnabled()) {
                    log.debug("removeInfo() found " + next.getId());
                }
                i.remove();
                break;
            }
        }
        for (int i = 0, j = infos.size(); i < j; i++) {
            RecordInfo existing = infos.get(i);
            existing.setOrderId(i);
        }
    }

    public void moveInfoUp(RecordInfo info) {
        initInfoOrderIfRequired();
        int itemCount = infos.size();
        if (!(itemCount > 1)) {
            // nothing to do
            if (log.isDebugEnabled()) {
                log.debug("moveInfoUp() nothing to do, info count not > 1 [itemCount=" + itemCount + ", providedIndex=" + info.getOrderId() + "]");
            }
            return;
        }
        int currentOrder = info.getOrderId();
        if (currentOrder < 1) {
            if (log.isDebugEnabled()) {
                log.debug("moveInfoUp() nothing to do, currentOrder < 1 [currentOrder=" + currentOrder + "]");
            }
            // nothing to do, info is first
            return;
        }
        for (int i = 0, j = itemCount; i < j; i++) {
            RecordInfo next = infos.get(i);
            if (next.getId().equals(info.getId())) {
                if (log.isDebugEnabled()) {
                    log.debug("moveInfoUp() found selected [id=" + next.getId() + ", actualIndex=" + info.getOrderId() + ", newIndex=" + (currentOrder - 1)
                            + "]");
                }
                next.setOrderId(currentOrder - 1);

            } else if (next.getOrderId() == currentOrder - 1) {
                next.setOrderId(currentOrder);
                if (log.isDebugEnabled()) {
                    log.debug("moveInfoUp() found previous [id=" + next.getId() + ", actualIndex=" + (currentOrder - 1) + ", newIndex=" + currentOrder + "]");
                }
            }
        }
    }

    public void moveInfoDown(RecordInfo info) {
        initInfoOrderIfRequired();
        int itemCount = infos.size();
        if (!(itemCount > 1)) {
            if (log.isDebugEnabled()) {
                log.debug("moveInfoDown() nothing to do, info count not > 1 [itemCount=" + itemCount + ", providedIndex=" + info.getOrderId() + "]");
            }
            // nothing to do  
            return;
        }
        int currentOrder = info.getOrderId();
        if (currentOrder == itemCount) {
            if (log.isDebugEnabled()) {
                log.debug("moveInfoDown() nothing to do, currentOrder == last [currentOrder=" + currentOrder + "]");
            }
            // nothing to do, info is last
            return;
        }
        for (int i = 0, j = itemCount; i < j; i++) {
            RecordInfo next = infos.get(i);
            if (next.getId().equals(info.getId())) {
                if (log.isDebugEnabled()) {
                    log.debug("moveInfoDown() found selected [id=" + next.getId() + ", actualIndex=" + info.getOrderId() + ", newIndex=" + (currentOrder + 1)
                            + "]");
                }
                next.setOrderId(currentOrder + 1);

            } else if (next.getOrderId() == currentOrder + 1) {
                next.setOrderId(currentOrder);
                if (log.isDebugEnabled()) {
                    log.debug("moveInfoDown() found previous [id=" + next.getId() + ", actualIndex=" + (currentOrder + 1) + ", newIndex=" + currentOrder + "]");
                }
            }
        }
    }

    protected abstract RecordInfo createInfo(Employee user, Option info);

    protected boolean isCustomizedInfos() {
        return customizedInfos;
    }

    protected void setCustomizedInfos(boolean customizedInfos) {
        this.customizedInfos = customizedInfos;
    }

    protected final void initInfoOrderIfRequired() {
        int updateCount = 0;
        for (int i = 0, j = infos.size(); i < j; i++) {
            RecordInfo info = infos.get(i);
            if (info.getOrderId() == 0) {
                updateCount++;
            }
        }
        if (infos.size() > 1 && updateCount > 1) {
            if (log.isDebugEnabled()) {
                log.debug("initInfoOrderIfRequired() reports true, updating list");
            }
            for (int i = 0, j = infos.size(); i < j; i++) {
                RecordInfo info = infos.get(i);
                info.setOrderId(i);
            }
        }
    }

    /**
     * Override this if implementing type supports optional items
     * @param product
     * @param customName
     * @param quantity
     * @param price
     * @param priceDate
     * @param partnerPrice
     * @param purchasePrice
     * @param note
     * @param includePrice
     * @return null by default
     */
    protected Item createOptionItem(
            Product product,
            String customName,
            Double quantity,
            BigDecimal price,
            Date priceDate,
            BigDecimal partnerPrice,
            BigDecimal purchasePrice,
            String note,
            boolean includePrice) {
        return null;
    }

    protected final void updateItems(List<ItemPosition> itemList, List<ItemPosition> optionalItemList) {
        if (itemList != null) {
            createItemsByPositions(itemList);
        }
        if (optionalItemList != null) {
            createOptionItemsByPositions(optionalItemList);
        }
    }

    protected final void createItems(List<Item> existing, boolean changeable) {
        itemsChangeable = changeable;
        itemsEditable = changeable;
        if (items == null) {
            items = new ArrayList<>();
        } else if (!items.isEmpty()) {
            items.clear();
        }
        for (int i = 0, j = existing.size(); i < j; i++) {
            Item next = existing.get(i);
            addItem(
                    next.getStockId(),
                    next.getProduct(),
                    next.getCustomName(),
                    next.getQuantity(),
                    next.getTaxRate(),
                    next.getPrice(),
                    next.getPriceDate(),
                    next.getPartnerPrice(),
                    next.isPartnerPriceEditable(),
                    next.isPartnerPriceOverridden(),
                    next.getPurchasePrice(),
                    next.getNote(),
                    next.isIncludePrice(),
                    next.getExternalId());
        }
    }

    private void createItemsByPositions(List<ItemPosition> source) {
        clearItems();
        for (int i = 0, j = source.size(); i < j; i++) {
            ItemPosition pos = source.get(i);
            for (int k = 0, l = pos.getItems().size(); k < l; k++) {
                Item next = pos.getItems().get(k);
                Item created = createItem(
                        next.getStockId(),
                        next.getProduct(),
                        next.getCustomName(),
                        next.getQuantity(),
                        next.getTaxRate(),
                        next.getPrice(),
                        next.getPriceDate(),
                        next.getPartnerPrice(),
                        next.isPartnerPriceEditable(),
                        next.isPartnerPriceOverridden(),
                        next.getPurchasePrice(),
                        next.getNote(),
                        next.isIncludePrice(),
                        next.getExternalId());
                if (created != null) {
                    addItem(created);
                }
            }
        }
        calculateSummary();
    }

    private void createOptionItemsByPositions(List<ItemPosition> source) {
        options.clear();
        for (int i = 0, j = source.size(); i < j; i++) {
            ItemPosition pos = source.get(i);
            for (int k = 0, l = pos.getItems().size(); k < l; k++) {
                Item next = pos.getItems().get(k);
                Item created = createOptionItem(
                        next.getProduct(),
                        next.getCustomName(),
                        next.getQuantity(),
                        next.getPrice(),
                        next.getPriceDate(),
                        next.getPartnerPrice(),
                        next.getPurchasePrice(),
                        next.getNote(),
                        next.isIncludePrice());
                if (created != null) {
                    options.add(created);
                }
            }
        }
    }

    public List<Item> getOptions() {
        return options;
    }

    protected void setOptions(List<Item> options) {
        this.options = options;
    }

    private void createOptions(List<Item> otherOptions) {
        for (int i = 0, j = otherOptions.size(); i < j; i++) {
            Item next = otherOptions.get(i);
            Item opt = createOptionItem(
                    next.getProduct(),
                    next.getCustomName(),
                    next.getQuantity(),
                    next.getPrice(),
                    next.getPriceDate(),
                    next.getPartnerPrice(),
                    next.getPurchasePrice(),
                    next.getNote(),
                    next.isIncludePrice());
            if (opt != null) {
                options.add(opt);
            }
        }
    }

    public Date getDateSent() {
        return dateSent;
    }

    public void setDateSent(Date dateSent) {
        this.dateSent = dateSent;
    }

    public Date getInitialDate() {
        return (initialDate == null ? getCreated() : initialDate);
    }

    protected void setInitialDate(Date initialDate) {
        this.initialDate = initialDate;
    }

    public void updateCreatedDate(Date date) {
        if (date != null) {
            if (initialDate == null) {
                initialDate = getCreated();
            }
            setCreated(date);
        }
    }

    public void setSalesReference(BusinessCase businessCase) {
        if (businessCase != null) {
            setBusinessCaseId(businessCase.getPrimaryKey());
            setSalesReferenceRequest(!businessCase.isSalesContext());
        } else {
            setBusinessCaseId(null);
        }
    }

    public String getTimeOfSupply() {
        return timeOfSupply;
    }

    public void setTimeOfSupply(String timeOfSupply) {
        this.timeOfSupply = timeOfSupply;
    }

    public String getPlaceOfPerformance() {
        return placeOfPerformance;
    }

    public void setPlaceOfPerformance(String placeOfPerformance) {
        this.placeOfPerformance = placeOfPerformance;
    }

    public RecordMailLog getMailLog() {
        return (mailLogs.isEmpty() ? null : mailLogs.get(0));
    }

    public List<RecordMailLog> getMailLogs() {
        return mailLogs;
    }

    public void setMailLogs(List<RecordMailLog> mailLogs) {
        this.mailLogs = mailLogs;
    }
}
