<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<c:set var="view" value="${sessionScope.recordView}"/>
<c:set var="record" value="${view.record}"/>
<c:if test="${!empty record.discountHistory}">
<div class="recordLabelValue">
<span><fmt:message key="authorizedIllegalDiscount"/>: <span style="margin-left: 30px;"><o:ajaxLink linkId="displayDiscountLink" url="${requestScope.discountDisplayUrl}"><o:number value="${record.discount}" format="currency"/> <c:choose><c:when test="${view.discountPercent}"> % </c:when><c:otherwise> &euro;/Eh </c:otherwise></c:choose></o:ajaxLink></span></span>
</div>
</c:if>
