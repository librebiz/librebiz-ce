/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.osserp.gui.tags;

import java.util.List;

import com.osserp.common.Option;
import com.osserp.common.SelectOption;
import com.osserp.common.util.NumberUtil;

import com.osserp.core.CoreServiceProvider;
import com.osserp.core.Options;
import com.osserp.core.contacts.Phone;

/**
 * 
 * @author eh <eh@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public class SelectCountryTag extends SelectTag {

    public static final String CODES = "codes";
    public static final String NAMES = "names";
    public static final String PREFIXES = "prefixes";

    protected String type = SelectCountryTag.NAMES;

    /**
     * Sets the type of which country option to render
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    @Override
    protected String createOptions() {
        StringBuilder buffer = new StringBuilder();
        if (type.equals(SelectCountryTag.CODES)) {
            this.options = Options.COUNTRY_CODES;
        } else if (type.equals(SelectCountryTag.PREFIXES)) {
            this.options = Options.COUNTRY_PREFIXES;
        } else {
            this.options = Options.COUNTRY_NAMES;
        }
        List<Option> optionList = CoreServiceProvider.getOptionList(
                getRequest(), options.toString());
        if (prompt) {
            buffer
                    .append("<option value=\"0\">")
                    .append(getResourceString("promptSelect"))
                    .append("</option>");
        }
        Long df = fetchDefault();
        if (type == SelectCountryTag.CODES) {
            for (int i = 0, j = optionList.size(); i < j; i++) {
                Option next = optionList.get(i);
                buffer
                        .append("<option value=\"")
                        .append(next.getId())
                        .append("\"");
                if (df != null && df.equals(Long.valueOf(next.getId()))) {
                    buffer.append(" selected=\"selected\"");
                }
                buffer
                        .append(">")
                        .append(next.getName())
                        .append("</option>");
            }
        } else if (type == SelectCountryTag.NAMES) {
            for (int i = 0, j = optionList.size(); i < j; i++) {
                Option next = optionList.get(i);
                if (!next.isEndOfLife()) {
                    buffer
                            .append("<option value=\"")
                            .append(next.getId())
                            .append("\"");
                    if (df != null && df.equals(next.getId())) {
                        buffer.append(" selected=\"selected\"");
                    }
                    buffer
                            .append(">")
                            .append(next.getName())
                            .append("</option>");
                }
            }
        } else if (type == SelectCountryTag.PREFIXES) {
            for (int i = 0, j = optionList.size(); i < j; i++) {
                SelectOption next = (SelectOption) optionList.get(i);
                buffer
                        .append("<option value=\"")
                        .append(next.getProperty())
                        .append("\"");
                if (df == null) {
                    df = Long.valueOf(49L);
                }
                if (df.equals(Long.valueOf(next.getProperty()))) {
                    buffer.append(" selected=\"selected\"");
                }
                buffer
                        .append(">")
                        .append(next.getLabelProperty())
                        .append("</option>");
            }
        }
        return buffer.toString();
    }

    @Override
    protected Long fetchDefault() {
        if (value != null) {
            if (value instanceof String) {
                return Long.valueOf((String) value);
            }
            if (value instanceof Long) {
                return (Long) value;
            }
            if (value instanceof Option) {
                return ((Option) value).getId();
            }
            if (value instanceof Phone) {
                return NumberUtil.createLong(((Phone) value).getCountry());
            }
        }
        return null;
    }
}
