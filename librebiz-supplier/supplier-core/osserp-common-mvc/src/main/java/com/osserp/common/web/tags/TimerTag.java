/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Oct 20, 2007 10:45:32 PM 
 * 
 */
package com.osserp.common.web.tags;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author cf <cf@osserp.com>
 * 
 */
public class TimerTag extends AbstractClassicValueTag {
    private static Logger log = LoggerFactory.getLogger(TimerTag.class.getName());
    private String url = null;
    private int interval;
    private int decay = 0;
    private String targetElement = null;

    @Override
    public int doStartTag() throws JspException {
        try {
            StringBuilder buffer = new StringBuilder();
            buffer
                    .append("<script type='text/javascript'>")
                    .append("new Ajax.PeriodicalUpdater('")
                    .append(targetElement)
                    .append("', '")
                    .append(getContextPath())
                    .append(url)
                    .append("', {frequency: ")
                    .append(interval)
                    .append(decay != 0 ? (", decay: " + decay) : "")
                    .append("});");
            JspWriter out = pageContext.getOut();
            out.print(buffer.toString());
        } catch (IOException ioe) {
            log.error("doStartTag() error getting writer");
        }
        return (EVAL_BODY_INCLUDE);
    }

    @Override
    public int doEndTag() throws JspException {
        try {
            JspWriter out = pageContext.getOut();
            out.print("</script>");
        } catch (IOException ioe) {
            log.error("doEndTag() error getting writer");
        }
        return (EVAL_BODY_INCLUDE);
    }

    /**
     * @return returns the url.
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url The url to set.
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return returns the timer interval.
     */
    public int getInterval() {
        return interval;
    }

    /**
     * @param interval The interval to set.
     */
    public void setInterval(int interval) {
        this.interval = interval;
    }

    /**
     * @return returns the target HTML element.
     */
    public String getTargetElement() {
        return targetElement;
    }

    /**
     * @param targetElement The target HTML element to set.
     */
    public void setTargetElement(String targetElement) {
        this.targetElement = targetElement;
    }

    /**
     * @return returns the decay for the timer.
     */
    public int getDecay() {
        return decay;
    }

    /**
     * @param decay The decay for the timer.
     */
    public void setDecay(int decay) {
        this.decay = decay;
    }

}
