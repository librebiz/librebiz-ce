<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="salesInvoicePaymentView" />
<c:set var="record" value="${view.record}" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="headline">
        <fmt:message key="paymentRecordHeader" /> <o:out value="${record.contact.displayName}" />
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="salesInvoicePaymentContent">
            <div class="row">
                <c:choose>
                    <c:when test="${view.recordCloseMode or view.paymentDeleteMode}">
                        <c:import url="${viewdir}/records/_recordPaymentActionConfirmation.jsp"/>
                    </c:when>
                    <c:when test="${view.cancellationMode}">
                        <v:form url="${view.baseLink}/createCancellation" name="salesInvoiceCancellationForm">
                            <c:import url="_salesInvoiceCancellationConfirmation.jsp"/>
                        </v:form>
                    </c:when>
                    <c:when test="${view.creditNoteMode}">
                        <v:form url="/sales/salesInvoicePayment/createCreditNote" name="salesInvoicePaymentForm">
                            <c:import url="_salesInvoiceCreditConfirmation.jsp"/>
                        </v:form>
                    </c:when>
                    <c:when test="${view.clearDecimalAmountMode}">
                        <v:form url="/sales/salesInvoicePayment/save" name="salesInvoicePaymentForm">
                            <c:import url="_salesInvoiceClearDecimalConfirmation.jsp"/>
                        </v:form>
                    </c:when>
                    <c:when test="${view.customPaymentMode}">
                        <v:form url="/sales/salesInvoicePayment/save" name="salesInvoicePaymentForm">
                            <c:import url="_salesInvoiceCustomPaymentConfirmation.jsp"/>
                        </v:form>
                    </c:when>
                    <c:when test="${empty view.selectedPaymentType}">
                        <c:import url="${viewdir}/records/_recordPaymentTypeSelection.jsp"/>
                    </c:when>
                    <c:when test="${view.selectedPaymentType.cashDiscount}">
                        <v:form url="/sales/salesInvoicePayment/save" name="salesInvoicePaymentForm">
                            <c:import url="${viewdir}/records/_recordPaymentCashDiscountInput.jsp"/>
                        </v:form>
                    </c:when>
                    <c:otherwise>
                        <v:form url="/sales/salesInvoicePayment/save" name="salesInvoicePaymentForm">
                            <c:import url="${viewdir}/records/_recordPaymentInput.jsp" />
                        </v:form>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
