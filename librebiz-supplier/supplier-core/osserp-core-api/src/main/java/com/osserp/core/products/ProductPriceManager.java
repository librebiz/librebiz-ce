/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 26, 2011 10:48:16 AM 
 * 
 */
package com.osserp.core.products;

import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.core.employees.Employee;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ProductPriceManager {

    /**
     * Provides the partner price by date
     * @param product
     * @param when
     * @param quantity
     * @return partner price by date, current if missing at time or 0 if never configured
     */
    double findPartnerPrice(Product product, Date when, Double quantity);

    /**
     * Provides the sales price history of an product
     * @param productId
     * @return priceHistory
     */
    List<ProductSalesPrice> getPriceHistory(Long productId);

    /**
     * Creates a sales price history entry for a given product
     * @param employee performing the update
     * @param product
     * @param validFrom
     */
    void createSalesPriceHistory(Employee employee, Product product, Date validFrom);

    /**
     * Updates validation date of price history entries
     * @param employee
     * @param priceHistoryId
     * @param validFrom
     * @return
     */
    List<ProductSalesPrice> updatePriceHistory(Employee employee, Long priceHistoryId, Date validFrom);

    /**
     * Creates a new price matrix by product. Deactivates current matrix if exists
     * @param user
     * @param product
     * @return priceMatrix new created matrix
     */
    ProductPriceByQuantityMatrix createMatrix(Employee user, Product product);

    /**
     * Fetches the price matrix of an product
     * @param product
     * @return priceMatrix matrix or null if not exists
     */
    ProductPriceByQuantityMatrix getMatrix(Product product);

    /**
     * Adds a range to a price matrix
     * @param user
     * @param matrix
     * @param value
     */
    void addRange(Employee user, ProductPriceByQuantityMatrix matrix, Double value);

    /**
     * Removes last range
     * @param user
     * @param matrix
     */
    void removeRange(Employee user, ProductPriceByQuantityMatrix matrix);

    /**
     * Updates minimum margins of a range
     * @param user
     * @param matrix
     * @param range
     * @param consumerMinMargin
     * @param resellerMinMargin
     * @param partnerMinMargin
     */
    void updateMargin(
            Employee user,
            ProductPriceByQuantityMatrix matrix,
            ProductPriceByQuantity range,
            Double consumerMinMargin,
            Double resellerMinMargin,
            Double partnerMinMargin);

    /**
     * Updates sales price of an product or other priceAware object
     * @param user
     * @param product
     * @param priceAware
     * @param validFrom
     * @param estimatedPurchasePrice
     * @param consumerPrice
     * @param consumerMinimumMargin
     * @param resellerPrice
     * @param resellerMinimumMargin
     * @param partnerPrice
     * @param partnerMinimumMargin
     * @return updatedPriceAware
     * @throws ClientException if required values are missing or sales price is lower purchasePrice and user has not required permissions to do that
     */
    PriceAware updateSalesPrice(
            Employee user,
            Product product,
            PriceAware priceAware,
            Date validFrom,
            Double estimatedPurchasePrice,
            Double consumerPrice,
            Double consumerMinimumMargin,
            Double resellerPrice,
            Double resellerMinimumMargin,
            Double partnerPrice,
            Double partnerMinimumMargin) throws ClientException;
}
