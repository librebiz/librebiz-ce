/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 16, 2010 10:45:32 PM 
 * 
 */
package com.osserp.common.web.tags;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * Tag renders an HTML-Link executing an ajax request on click. The function invoked depends on given params.
 * 
 * function ajaxLink(requestUrl, targetElement, targetIsPopup, activityElement, activityMessage, successFunction, link)
 * 
 * @author cf <cf@osserp.com>
 * 
 */
public class AjaxExecuteTag extends AbstractAjaxLinkTag {
    private static Logger log = LoggerFactory.getLogger(AjaxExecuteTag.class.getName());

    @Override
    public int doStartTag() throws JspException {
        try {
            StringBuilder buffer = new StringBuilder();
            buffer
                    .append("<a id='")
                    .append(getLinkId())
                    .append("' href='javascript:;' onclick=\"ojsAjax.ajaxLink('")
                    .append(createUrl())
                    .append("', $(this).up().identify(), false, $(this).up().identify(), '")
                    .append(getResourceString("waitPleaseWithDots"))
                    .append("', null, this); \"");
            if (getStyle() != null) {
                buffer.append(" style=\"").append(getStyle()).append("\"");
            }
            if (getStyleClass() != null) {
                buffer.append(" class=\"").append(getStyleClass()).append("\"");
            }
            if (getTitle() != null) {
                buffer.append(" title=\"").append(getResourceString(getTitle())).append("\"");
            }
            buffer.append(">");
            JspWriter out = pageContext.getOut();
            out.print(buffer.toString());

            setTargetElement(null);

        } catch (IOException ioe) {
            log.error("doStartTag() error getting writer");
        }
        return (EVAL_BODY_INCLUDE);
    }
}
