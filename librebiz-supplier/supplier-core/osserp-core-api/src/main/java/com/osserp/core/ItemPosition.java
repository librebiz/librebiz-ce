/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 4, 2005 6:42:00 PM 
 * 
 */
package com.osserp.core;

import java.util.List;
import java.util.Map;

import com.osserp.common.Option;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ItemPosition extends Option {

    /**
     * Provides a reference to the primary key of a parent object
     * @return reference
     */
    Long getReference();

    /**
     * Provides the related config group
     * @return groupId
     */
    Long getGroupId();

    /**
     * Indicates that items under this position are discounts
     * @return discounts
     */
    boolean isDiscounts();

    /**
     * Provides an item by primary key
     * @return item
     */
    Item getItem(Long id);

    /**
     * Provides all items related to this position
     * @return items
     */
    List<Item> getItems();

    /**
     * Provides all items as map of product id / item value
     * @return productMap
     */
    Map<Long, Item> getProductMap();

    /**
     * Indicates that group holds optional products
     * @return option
     */
    boolean isOption();

    /**
     * Indicates that group holds a reference to another -sub- calculation
     * @return partList
     */
    boolean isPartlist();

    /**
     * Indicates thats partlists are available for this position
     * @return
     */
    boolean isPartlistsAvailable();

    /**
     * Set that partlists are available for this position
     * @param partlistAvailable
     */
    void setPartlistsAvailable(boolean partlistsAvailable);

    /**
     * Provides the config id of the part list
     * @return partlistId
     */
    Long getPartlistId();
}
