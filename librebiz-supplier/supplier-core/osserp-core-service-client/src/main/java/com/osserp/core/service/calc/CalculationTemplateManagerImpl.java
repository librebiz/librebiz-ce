/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 29, 2011 3:21:18 PM 
 * 
 */
package com.osserp.core.service.calc;

import java.util.List;

import com.osserp.core.BusinessType;
import com.osserp.core.calc.Calculation;
import com.osserp.core.calc.CalculationTemplate;
import com.osserp.core.calc.CalculationTemplateManager;
import com.osserp.core.calc.CalculationTemplateService;
import com.osserp.core.dao.CalculationTemplates;
import com.osserp.core.products.Product;

/**
 * 
 * @author jg <jg@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public class CalculationTemplateManagerImpl implements CalculationTemplateManager {

    private CalculationTemplateService calculationTemplateService;
    private CalculationTemplates calculationTemplates;

    public CalculationTemplateManagerImpl(
            CalculationTemplateService calculationTemplateService,
            CalculationTemplates calculationTemplates) {
        this.calculationTemplateService = calculationTemplateService;
        this.calculationTemplates = calculationTemplates;
    }

    public CalculationTemplate load(Long id) {
        return calculationTemplates.load(id);
    }

    public List<CalculationTemplate> findAll() {
        return calculationTemplates.findAll();
    }

    public CalculationTemplate createTemplate(String name, Product product, BusinessType businessType) {
        return calculationTemplates.create(name, product, businessType);
    }

    public void addProduct(CalculationTemplate calculationTemplate, Product product, String note, Long group) {
        calculationTemplates.addProduct(calculationTemplate, product, note, group);
    }

    public void removeItem(CalculationTemplate calculationTemplate, Long productId) {
        calculationTemplates.removeItem(calculationTemplate, productId);
    }

    public void toggleOptionalItem(CalculationTemplate calculationTemplate, Long itemId) {
        calculationTemplates.toggleOptionalItem(calculationTemplate, itemId);
    }

    public void delete(Long id) {
        calculationTemplates.delete(id);
    }

    public boolean isTemplateAvailable(Long productId, Long businessTypeId) {
        return calculationTemplates.isTemplateAvailable(productId, businessTypeId);
    }

    public CalculationTemplate findByProductAndType(Calculation calculation, Long productId, Long businessTypeId) {
        return calculationTemplateService.findByProductAndType(calculation, productId, businessTypeId);
    }
}
