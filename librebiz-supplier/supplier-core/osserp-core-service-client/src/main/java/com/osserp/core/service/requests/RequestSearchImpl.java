/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 10-Feb-2007 09:18:03 
 * 
 */
package com.osserp.core.service.requests;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;

import com.osserp.core.BusinessCaseSearchRequest;
import com.osserp.core.dao.Requests;
import com.osserp.core.events.Event;
import com.osserp.core.requests.Request;
import com.osserp.core.requests.RequestListItem;
import com.osserp.core.requests.RequestSearch;
import com.osserp.core.sales.SalesUtil;
import com.osserp.core.service.impl.AbstractService;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RequestSearchImpl extends AbstractService implements RequestSearch {
    private static Logger log = LoggerFactory.getLogger(RequestSearchImpl.class.getName());
    private Requests requests = null;

    public RequestSearchImpl(Requests requests) {
        super();
        this.requests = requests;
    }

    public boolean exists(Long requestId) {
        return requests.exists(requestId);
    }

    public List<RequestListItem> find(BusinessCaseSearchRequest request) {
        List<RequestListItem> result = new ArrayList<>();
        if (request != null && request.getDomainUser() != null) {
            List<RequestListItem> all = requests.find(request);
            result = new ArrayList(SalesUtil.filter(request.getDomainUser(), all));
        }
        return result;
    }

    public Request findById(Long requestId) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("findById() invoked [id=" + requestId + "]");
        }
        return requests.load(requestId);
    }

    public Long findIdByOffer(Long offerId) {
        return requests.findIdByOffer(offerId);
    }

    private void addAppointments(final List<RequestListItem> list) {
        Map<Long, Event> appointments = new java.util.HashMap<Long, Event>(); // events.findAppointmentsByRecipient(recipient);
        for (int i = 0, j = list.size(); i < j; i++) {
            RequestListItem next = list.get(i);
            if (appointments.containsKey(next.getId())) {
                next.setAppointment(appointments.get(next.getId()));
            }
        }
    }
}
