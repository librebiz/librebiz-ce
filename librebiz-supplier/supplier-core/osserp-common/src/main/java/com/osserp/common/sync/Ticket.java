/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 27, 2008 8:03:19 AM 
 * 
 */
package com.osserp.common.sync;

import java.io.Serializable;

/**
 * Ticket for synchronization tasks.
 * 
 * CAUTION: Never change any content of this file!
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public final class Ticket implements Serializable {
    private String executor = null;
    private String message = null;
    private byte[] file = null;

    protected Ticket() {
        super();
    }

    public Ticket(String message) {
        super();
        this.message = message;
    }

    public Ticket(String message, byte[] file) {
        super();
        this.message = message;
        this.file = file;
    }

    public Ticket(String executor, String message) {
        super();
        this.message = message;
        this.executor = executor;
    }

    public Ticket(String executor, String message, byte[] file) {
        super();
        this.message = message;
        this.file = file;
        this.executor = executor;
    }

    public String getMessage() {
        return message;
    }

    protected void setMessage(String message) {
        this.message = message;
    }

    public byte[] getFile() {
        return file;
    }

    protected void setFile(byte[] file) {
        this.file = file;
    }

    public String getExecutor() {
        return executor;
    }

    protected void setExecutor(String executor) {
        this.executor = executor;
    }
}
