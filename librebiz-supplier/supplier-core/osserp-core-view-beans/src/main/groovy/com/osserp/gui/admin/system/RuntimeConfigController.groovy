/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.gui.admin.system

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.common.util.NumberUtil

import com.osserp.gui.CoreAdminController

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
@RequestMapping("/admin/system/runtimeConfig/*")
@Controller class RuntimeConfigController extends CoreAdminController {
    private static Logger logger = LoggerFactory.getLogger(RuntimeConfigController.class.getName())

    @RequestMapping
    def forward(HttpServletRequest request) {
        logger.debug("forward: invoked [user=${request.getSession().getAttribute('user')?.id}]")
        return defaultPage
    }

    @RequestMapping
    def save(HttpServletRequest request) {
        logger.debug("save: invoked [user=${request.getSession().getAttribute('user')?.id}]")
        def ctx = request.session.servletContext
        def uploadReadBufferSize = request.getParameter('uploadReadBufferSize')
        if (NumberUtil.isInteger(uploadReadBufferSize)) {
            ctx.setAttribute('uploadReadBufferSize', uploadReadBufferSize)
            logger.debug("save: updated uploadReadBufferSize [value=${uploadReadBufferSize}]")
        }
        return defaultPage
    }

    @RequestMapping
    def disableMXCheck(HttpServletRequest request) {
        logger.debug("disableMXCheck: invoked [user=${getUserId(request)}]")
        RuntimeConfigView view = getView(request)
        view.disableMXCheck()
        defaultPage
    }

    @RequestMapping
    def enableMXCheck(HttpServletRequest request) {
        logger.debug("enableMXCheck: invoked [user=${getUserId(request)}]")
        RuntimeConfigView view = getView(request)
        view.enableMXCheck()
        defaultPage
    }

    @RequestMapping
    def disableSMTPCheck(HttpServletRequest request) {
        logger.debug("disableSMTPCheck: invoked [user=${getUserId(request)}]")
        RuntimeConfigView view = getView(request)
        view.disableSMTPCheck()
        defaultPage
    }

    @RequestMapping
    def enableSMTPCheck(HttpServletRequest request) {
        logger.debug("enableSMTPCheck: invoked [user=${getUserId(request)}]")
        RuntimeConfigView view = getView(request)
        view.enableSMTPCheck()
        defaultPage
    }
    
    @RequestMapping
    def reloadProductSummary(HttpServletRequest request) {
        logger.debug("reloadProductSummary: invoked [user=${getUserId(request)}]")
        RuntimeConfigView view = getView(request)
        view.reloadProductSummary()
        request.setAttribute("productSummaryReloaded", true)
        defaultPage
    }

    @RequestMapping
    def reloadOptions(HttpServletRequest request) {
        logger.debug("reloadOptions: invoked [user=${getUserId(request)}]")
        RuntimeConfigView view = getView(request)
        view.reloadOptions()
        request.setAttribute("optionsReload", true)
        defaultPage
    }
    
    @RequestMapping
    def synchronizePublicProducts(HttpServletRequest request) {
        logger.debug("synchronizePublicProducts: invoked [user=${getUserId(request)}]")
        RuntimeConfigView view = getView(request)
        view.synchronizePublicProducts()
        request.setAttribute("productSynchronizationActivated", true)
        defaultPage
    }

    @RequestMapping
    def synchronizePublicProductMedia(HttpServletRequest request) {
        logger.debug("synchronizePublicProductMedia: invoked [user=${getUserId(request)}]")
        RuntimeConfigView view = getView(request)
        view.synchronizePublicProductMedia()
        request.setAttribute("productMediaSynchronizationActivated", true)
        defaultPage
    }

    @RequestMapping
    def reloadSalesMonitoringCache(HttpServletRequest request) {
        logger.debug("reloadSalesRevenueCalculatorConfigs: invoked [user=${getUserId(request)}]")
        RuntimeConfigView view = getView(request)
        view.reloadSalesMonitoringCache()
        request.setAttribute("salesMonitoringCacheReloaded", true)
        defaultPage
    }

    @RequestMapping
    def reloadSalesRevenueCalculatorConfigs(HttpServletRequest request) {
        logger.debug("reloadSalesRevenueCalculatorConfigs: invoked [user=${getUserId(request)}]")
        RuntimeConfigView view = getView(request)
        view.reloadSalesRevenueCalculatorConfigs()
        request.setAttribute("salesRevenueCalculatorConfigReloaded", true)
        defaultPage
    }
}
