/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 22, 2008 7:51:17 PM 
 * 
 */
package com.osserp.gui.web.contacts;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ActionException;
import com.osserp.gui.client.contacts.ContactView;
import com.osserp.gui.client.contacts.RelatedContactView;
import com.osserp.gui.web.struts.AbstractViewAction;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class AbstractContactViewAwareAction extends AbstractViewAction {

    private static Logger log = LoggerFactory.getLogger(AbstractContactViewAwareAction.class.getName());

    /**
     * Toggles default display group flag
     * @param mapping
     * @param form
     * @param httpRequest
     * @param httpResponse
     * @return forward
     * @throws Exception
     */
    public ActionForward toggleDefaultDisplayGroup(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse)
            throws Exception {

        HttpSession session = httpRequest.getSession();
        if (log.isDebugEnabled()) {
            log.debug("toggleDefaultDisplayGroup() request from " + getUser(session).getId());
        }
        ContactView view = getContactView(session);
        if (view instanceof RelatedContactView) {
            RelatedContactView rv = (RelatedContactView) view;
            rv.toggleDefaultDisplay();
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Toggles default display group flag
     * @param mapping
     * @param form
     * @param httpRequest
     * @param httpResponse
     * @return forward
     * @throws Exception
     */
    public ActionForward reload(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse)
            throws Exception {

        HttpSession session = httpRequest.getSession();
        if (log.isDebugEnabled()) {
            log.debug("reload() request from " + getUser(session).getId());
        }
        ContactView view = getContactView(session);
        view.refresh();
        return mapping.findForward(view.getActionTarget());
    }

    protected ContactView fetchContactView(HttpSession session) {
        return (ContactView) fetchView(session, ContactView.class);
    }

    protected ContactView getContactView(HttpSession session) {
        ContactView view = fetchContactView(session);
        if (view == null) {
            log.warn("getContactView() failed, view not bound [class=" + ContactView.class.getName() + "]");
            throw new ActionException("view not bound");
        }
        return view;
    }

}
