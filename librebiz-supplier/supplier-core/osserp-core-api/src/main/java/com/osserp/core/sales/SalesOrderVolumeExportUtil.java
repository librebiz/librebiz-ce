/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 29, 2008 9:16:13 AM 
 * 
 */
package com.osserp.core.sales;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.osserp.core.Item;
import com.osserp.core.finance.DeliveryNote;
import com.osserp.core.finance.Order;
import com.osserp.core.products.ProductSelectionConfig;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesOrderVolumeExportUtil {

    public static List<Item> createUnbilledItems(SalesOrderVolumeExportConfig config, Set<Long> exportedDeliveries, Order order) {
        List<Item> items = new ArrayList<Item>();
        ProductSelectionConfig filter = config.getProductSelectionConfig();
        for (int i = 0, j = order.getDeliveryNotes().size(); i < j; i++) {
            DeliveryNote dn = order.getDeliveryNotes().get(i);
            if (dn.isUnchangeable() && !exportedDeliveries.contains(dn.getId())) {
                for (int k = 0, l = dn.getItems().size(); k < l; k++) {
                    Item next = dn.getItems().get(k);
                    if ((filter == null || filter.isMatching(next.getProduct()))
                            && next.getStockId().equals(
                                    config.getCompany().getId())) {
                        Item clone = (Item) next.clone();
                        clone.updateAccountingReference(clone.getReference(), dn.getCreated());
                        items.add(clone);
                    }
                }
            }
        }
        return items;
    }
}
