/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 12.12.2012 
 * 
 */
package com.osserp.api;

import java.io.Serializable;
import java.util.Map;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface WSObject extends Serializable {

    /**
     * Provides the primary key of the parent system (CRM/ERP) 
     * @return primaryKey
     */
    Long getPrimaryKey();

    /**
     * Provides the primary key of the external system
     * @return externalKey
     */
    String getExternalKey();

    /**
     * Provides the external key as long if available and formattable as number.
     * @return externalId
     */
    Long getExternalId();

    /**
     * Provides an url pointing to the corresponding object of the external system
     * @return externalUrl
     */
    String getExternalUrl();

    /**
     * Provides a target id referencing an existing object of the target system.
     * The target will be overridden with the data and id provided by this object. 
     * @return targetId
     */
    Long getTargetId();

    /**
     * Provides a map with additional properties. The additional properties are
     * used to send customized properties.
     * @return properties
     */
    Map<String, Object> getProperties();
}
