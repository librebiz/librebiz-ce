<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="contactView" value="${sessionScope.contactView}"/>
<c:set var="contact" value="${sessionScope.contactView.customer}"/>
<div class="subcolumn">
	<c:choose>
		<c:when test="${contactView.requestListingMode}">
			<table class="table">
				<thead>
					<tr>
                        <th><fmt:message key="request"/></th>
                        <th> </th>
						<th><fmt:message key="type"/></th>
						<th><fmt:message key="ofDate"/></th>
						<th><fmt:message key="consignment"/></th>
						<th><fmt:message key="sales"/></th>
						<th class="right"><a href="<c:url value="/customers.do?method=disableAllModes"/>"><o:img name="backIcon" /></a></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="req" items="${contactView.summary.requests}" varStatus="s">
						<c:choose>
							<c:when test="${contactView.cancelledListingMode && req.cancelled}">
								<tr>
                                    <td style="text-decoration: line-through;">
                                        <a href="<c:url value="/requests.do"><c:param name="method" value="select"/><c:param name="id" value="${req.id}"/></c:url>" title="<o:out value="${req.lastAction}"/>"><o:out value="${req.id}"/></a>
                                    </td>
                                    <td class="right">
                                        <span title="<o:out value="${req.lastAction}"/>"><o:out value="${req.status}"/>%</span>
                                    </td>
									<td><o:out value="${req.type.name}"/></td>
									<td><o:date value="${req.created}" addcentury="false"/></td>
									<td><o:out value="${req.name}"/></td>
									<td colspan="2"><v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${req.salesId}"><oc:employee value="${req.salesId}"/></v:ajaxLink></td>
								</tr>
							</c:when>
							<c:when test="${!contactView.cancelledListingMode && !req.cancelled}">
								<tr>
                                    <td>
                                        <a href="<c:url value="/requests.do"><c:param name="method" value="select"/><c:param name="id" value="${req.id}"/></c:url>" title="<o:out value="${req.lastAction}"/>"><o:out value="${req.id}"/></a>
                                    </td>
                                    <td class="right<c:if test="${req.stopped}"> stopped</c:if>">
                                        <span title="<o:out value="${req.lastAction}"/>"><o:out value="${req.status}"/>%</span>
                                    </td>
									<td><o:out value="${req.type.name}"/></td>
									<td><o:date value="${req.created}" addcentury="false"/></td>
									<td><o:out value="${req.name}"/></td>
									<td colspan="2"><v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${req.salesId}"><oc:employee value="${req.salesId}"/></v:ajaxLink></td>
								</tr>
							</c:when>
						</c:choose>
					</c:forEach>
				</tbody>
			</table>
		</c:when>
		<c:otherwise>
			<table class="table">
				<tr>
					<th><fmt:message key="order"/></th>
                    <th> </th>
					<th><fmt:message key="type"/></th>
					<th><fmt:message key="ofDate"/></th>
					<th><fmt:message key="consignment"/></th>
					<th><fmt:message key="sales"/></th>
					<th class="right"><a href="<c:url value="/customers.do?method=disableAllModes"/>"><o:img name="backIcon"/></a></th>
				</tr>
				<c:forEach var="req" items="${contactView.summary.sales}" varStatus="s">
					<c:choose>
						<c:when test="${contactView.cancelledListingMode && req.cancelled}">
							<tr>
                                <td style="text-decoration: line-through;"><a href="<c:url value="/loadSales.do?exit=customer&id=${req.id}"/>" title="<o:out value="${req.lastAction}"/>"><o:out value="${req.id}"/></a> - <span title="<o:out value="${req.lastAction}"/>"><o:out value="${req.status}"/>%</span></td>
                                <td class="right"><span title="<o:out value="${req.lastAction}"/>"><o:out value="${req.status}"/>%</span></td>
								<td><o:out value="${req.type.name}"/></td>
								<td><o:date value="${req.created}" addcentury="false"/></td>
								<td><o:out value="${req.name}"/></td>
								<td colspan="2"><v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${req.salesId}"><oc:employee value="${req.salesId}"/></v:ajaxLink></td>
							</tr>
						</c:when>
						<c:when test="${!contactView.cancelledListingMode && !req.cancelled}">
							<c:if test="${contactView.includeAllBusinessCases or req.status < 100}">
								<tr>
                                    <td><a href="<c:url value="/loadSales.do?exit=customer&id=${req.id}"/>" title="<o:out value="${req.lastAction}"/>"><o:out value="${req.id}"/></a></td>
                                    <td class="right<c:if test="${req.stopped}"> stopped</c:if>"><span title="<o:out value="${req.lastAction}"/>"><o:out value="${req.status}"/>%</span></td>
									<td><o:out value="${req.type.name}"/></td>
									<td><o:date value="${req.created}" addcentury="false"/></td>
									<td><o:out value="${req.name}"/></td>
									<td colspan="2"><v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${req.salesId}"><oc:employee value="${req.salesId}"/></v:ajaxLink></td>
								</tr>
							</c:if>
						</c:when>
					</c:choose>
				</c:forEach>
			</table>
		</c:otherwise>
	</c:choose>
</div>
