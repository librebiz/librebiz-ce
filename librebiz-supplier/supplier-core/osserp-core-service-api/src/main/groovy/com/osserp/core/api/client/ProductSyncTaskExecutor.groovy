/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 *
 * Created on 16.12.2012
 *
 */
package com.osserp.core.api.client

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.api.WsMedia
import com.osserp.api.WsProduct
import com.osserp.api.client.ProductEventService

import com.osserp.common.dms.DmsDocument
import com.osserp.common.dms.DmsManager
import com.osserp.common.dms.DmsReference
import com.osserp.common.util.JsonObject
import com.osserp.common.util.JsonUtil
import com.osserp.common.util.NumberUtil

import com.osserp.core.api.WsMediaMapper
import com.osserp.core.api.WsProductMapper
import com.osserp.core.dao.Products
import com.osserp.core.dao.SystemConfigs
import com.osserp.core.products.Product
import com.osserp.core.system.SyncTask
import com.osserp.core.system.SyncTaskExcecutor

/**
 * 
 * ProductSyncTaskExecutor synchronizes products based on the osserp-api 
 * package. Implements 'productChanged' and 'productMediaChanged' events 
 * of the remote Client API.
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public class ProductSyncTaskExecutor implements SyncTaskExcecutor {
    private static Logger logger = LoggerFactory.getLogger(ProductSyncTaskExecutor.class.getName())
    
    private Products products
    private ProductEventService productEventService
    private WsMediaMapper mediaMapper
    private WsProductMapper productMapper
    private DmsManager dmsManager
    private SystemConfigs systemConfigs

    protected ProductSyncTaskExecutor(Products products,
        WsProductMapper productMapper,
        WsMediaMapper mediaMapper,
        ProductEventService productEventService, 
        DmsManager dmsManager,
        SystemConfigs systemConfigs) {
        super()
        this.products = products
        this.productMapper = productMapper
        this.mediaMapper = mediaMapper
        this.productEventService = productEventService
        this.dmsManager = dmsManager
        this.systemConfigs = systemConfigs
    }

    /* (non-Javadoc)
     * @see com.osserp.core.system.SyncTaskExcecutor#execute(com.osserp.core.system.SyncTask)
     */
    public void execute(SyncTask syncTask) throws Exception {
        Long productId = syncTask.objectId ? NumberUtil.createLong(syncTask.objectId) : null
        if (productId) {
            
            if ('productChanged' == syncTask.typeName) {
                
                Product product = products.load(productId)
                WsProduct wsProduct = productMapper.map(product)
                if ('delete' == syncTask.actionName) {
                    wsProduct.endOfLife = true
                    logger.warn("execute: known sync task found [name=${syncTask.actionName}]")
                }
                productEventService.productChanged(wsProduct)
    
            } else if ('productMediaChanged' == syncTask.typeName) {
                
                JsonObject json = JsonUtil.fetchInput(syncTask.objectData)
                Long id = json.getLong(syncTask.actionName)
                if (id) {
                    if ('documentType' == syncTask.actionName) {
                        // sync all documents of provided type
                        syncDocuments(productId, id)
                        
                    } else if ('documentId' == syncTask.actionName) {
                        // sync a document by provided id
                        DmsDocument document = dmsManager.findDocument(id)
                        if (document) {
                            syncDocument(document)
                        } else {
                            logger.info("execute: ignoring deleted document [name=${syncTask.actionName}, id=${id}]")
                        }
                    }
                } else {
                    logger.warn("execute: action requires not provided id [name=${syncTask.actionName}]")
                }
    
            } else {
                logger.warn("execute: found unknown syncTask [name=${syncTask.typeName}]")
            }
        }
    }
    
    private void syncDocuments(Long productId, Long mediaType) {
        DmsReference reference = new DmsReference(mediaType, productId)
        List<DmsDocument> documents = dmsManager.findByReference(reference)
        documents.each {
            syncDocument(it)
        }
    }
    
    private void syncDocument(DmsDocument document) {
        WsMedia wsMedia = mediaMapper.map(document)
        productEventService.productMediaChanged(wsMedia)
    }

}
