/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Dec 13, 2010 1:14:59 PM 
 * 
 */
package com.osserp.core.model.telephone;

import java.util.Date;
import java.util.List;

import com.osserp.common.beans.AbstractEntity;
import com.osserp.core.telephone.TelephoneCall;
import com.osserp.core.telephone.TelephoneCallContact;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public class TelephoneCallImpl extends AbstractEntity implements TelephoneCall {
    private Long telephoneSystem = null;
    private String destinationDisplayName;
    private Long destinationPhoneNumber;
    private String destinationUid;
    private String displayName;
    private Integer duration;
    private Date endedAt;
    private Date establishedAt;
    private Boolean incoming;
    private Boolean internal;
    private Long phoneNumber;
    private Date startedAt;
    private String uid;
    private Boolean clickToCall;
    private List<TelephoneCallContact> callContacts;

    protected TelephoneCallImpl() {
        super();
    }

    public Long getTelephoneSystem() {
        return telephoneSystem;
    }

    public void setTelephoneSystem(Long telephoneSystem) {
        this.telephoneSystem = telephoneSystem;
    }

    public String getDestinationDisplayName() {
        return destinationDisplayName;
    }

    public void setDestinationDisplayName(String destinationDisplayName) {
        this.destinationDisplayName = destinationDisplayName;
    }

    public Long getDestinationPhoneNumber() {
        return destinationPhoneNumber;
    }

    public void setDestinationPhoneNumber(Long destinationPhoneNumber) {
        this.destinationPhoneNumber = destinationPhoneNumber;
    }

    public String getDestinationUid() {
        return destinationUid;
    }

    public void setDestinationUid(String destinationUid) {
        this.destinationUid = destinationUid;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Date getEndedAt() {
        return endedAt;
    }

    public void setEndedAt(Date endedAt) {
        this.endedAt = endedAt;
    }

    public Date getEstablishedAt() {
        return establishedAt;
    }

    public void setEstablishedAt(Date establishedAt) {
        this.establishedAt = establishedAt;
    }

    public boolean getIncoming() {
        return incoming;
    }

    public void setIncoming(Boolean incoming) {
        this.incoming = incoming;
    }

    public boolean getInternal() {
        return internal;
    }

    public boolean getClickToCall() {
        return clickToCall;
    }

    public void setClickToCall(Boolean clickToCall) {
        this.clickToCall = clickToCall;
    }

    public void setInternal(Boolean internal) {
        this.internal = internal;
    }

    public Long getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Date getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(Date startedAt) {
        this.startedAt = startedAt;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public List<TelephoneCallContact> getCallContacts() {
        return callContacts;
    }

    public void setCallContacts(List<TelephoneCallContact> callContacts) {
        this.callContacts = callContacts;
    }
}
