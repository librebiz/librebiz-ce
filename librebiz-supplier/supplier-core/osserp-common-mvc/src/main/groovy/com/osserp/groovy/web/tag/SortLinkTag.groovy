/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Feb 23, 2011 3:26:47 PM 
 * 
 */
package com.osserp.groovy.web.tag

import com.osserp.groovy.web.WebUtil
import com.osserp.groovy.web.View

/**
 *
 * @author jg <jg@osserp.com>
 * 
 */
class SortLinkTag extends AbstractTag {

    String key

    @Override
    protected String getStartTag() {
        StringBuilder buffer = new StringBuilder()
        buffer.append('<a href="')
        buffer.append(url).append('"')
        buffer.append(renderCommonAttributes())
        buffer.append('>')
        return buffer.toString()
    }

    @Override
    protected String getEndTag() {
        '</a>'
    }

    protected String getUrl() {
        View view = request.getAttribute('view')
        return view ? "${viewContextPath}/${view.context.name}/sort?key=${key}" : ''
    }
}

