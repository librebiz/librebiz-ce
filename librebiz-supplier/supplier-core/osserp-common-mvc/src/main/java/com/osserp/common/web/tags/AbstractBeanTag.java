/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 16.10.2004 
 * 
 */
package com.osserp.common.web.tags;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

import org.apache.commons.beanutils.PropertyUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.web.Globals;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author cf <cf@osserp.com>
 * 
 */
@SuppressWarnings("unchecked")
public class AbstractBeanTag extends AbstractTagSupport {

    private static final long serialVersionUID = 42L;

    private static Logger log = LoggerFactory.getLogger(AbstractBeanTag.class.getName());

    /** Scope names to PageContext constant values map. */
    private static Map scopes = new HashMap<>();

    /** Set up scope values. */
    static {

        scopes.put(Globals.PAGE_SCOPE, Integer.valueOf(PageContext.PAGE_SCOPE));
        scopes.put(Globals.REQUEST_SCOPE, Integer.valueOf(PageContext.REQUEST_SCOPE));
        scopes.put(Globals.SESSION_SCOPE, Integer.valueOf(PageContext.SESSION_SCOPE));
        scopes.put(Globals.APPLICATION_SCOPE, Integer.valueOf(PageContext.APPLICATION_SCOPE));
    }

    /** Indicates that missing beans should be ignored */
    protected boolean ignore = false;

    /** Bean name to lookup for */
    protected String name = null;

    /** Property to be accessed on the specified bean. */
    protected String property = null;

    /** Scope where we should lookup for the bean */
    protected String scope = null;

    public boolean getIgnore() {
        return (ignore);
    }

    public void setIgnore(boolean ignore) {
        this.ignore = ignore;
    }

    public String getName() {
        return (name);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProperty() {
        return (property);
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getScope() {
        return (scope);
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    /**
     * Looks up for a bean under key name in all scopes if scope is null or specified scope
     * @return object
     * @throws JspException if scope is not null but key is unknown
     */
    protected Object getObject() throws JspException {
        return getObject(name, scope);
    }

    /**
     * Looks up for a bean under key name in all scopes if scope is null or specified scope
     * @param key
     * @param scopeName
     * @return object
     * @throws JspException if scope is not null but key is unknown
     */
    protected Object getObject(String key, String scopeName) throws JspException {
        if (scopeName == null) {
            return pageContext.findAttribute(key);
        }
        return pageContext.getAttribute(key, getScope(scopeName));
    }

    /**
     * Tries to retrieve the property value of the bean if property is specified. If no property specified the bean itself is returned
     * @return value or null if not found and ignore is true
     * @throws JspException if bean is null AND ignore is false OR specified property does not exist.
     */
    protected Object getObjectValue() throws JspException {
        Object obj = getObject();
        if (obj == null) {
            if (ignore) {
                return null;
            }
            throw new JspException("no bean found under name " + name);
        }
        if (property == null) {
            return obj;
        }
        try {
            return PropertyUtils.getProperty(obj, property);
        } catch (Exception e) {
            log.error("getObjectValue() failed [message=" + e.getMessage() + "]"
                    + "\nobject: " + obj.getClass().getName());

            if (ignore) {
                return null;
            }
            throw new JspException("no value found for property " +
                    property + " under name " + name);
        }
    }

    /**
     * Tries to retrieve the property value of the bean if property is specified. 
     * If no property specified the bean itself is returned.
     * @param key
     * @param propertyName
     * @param scopeName
     * @return value or null if not found and ignore is true
     * @throws JspException if bean is null AND ignore is false OR specified property does not exist.
     */
    protected Object getObjectValue(
            String key,
            String propertyName,
            String scopeName) throws JspException {

        Object obj = getObject(key, scopeName);
        if (obj == null) {
            if (ignore) {
                return null;
            }
            throw new JspException("no bean found under name " + name);
        }
        if (propertyName == null) {
            return obj;
        }
        try {
            return PropertyUtils.getProperty(obj, propertyName);
        } catch (Exception e) {
            log.error("getObjectValue() failed [message=" + e.getMessage() + "]"
                    + "\nobject: " + obj.getClass().getName()
                    + "\nparams: {name=" + name
                    + ",property=" + property
                    + ",scope=" + scope + "}");
            if (ignore) {
                return null;
            }
            log.error("getObjectValue() failed [message=" + e.getMessage() + "]");
            throw new JspException("no value found for property " +
                    property + " under name " + name);
        }
    }

    private int getScope(String scopeName) throws JspException {
        Integer i = (Integer) scopes.get(scopeName);
        if (i == null) {
            throw new JspException("getScope() invalid scope [name="
                    + scopeName + "]");
        }
        return i.intValue();
    }

    @Override
    public void release() {
        super.release();
        ignore = false;
        name = null;
        property = null;
        scope = null;
    }

}
