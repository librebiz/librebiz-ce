<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="message" /> - <fmt:message key="detail" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="received"><fmt:message key="dateTime" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:date value="${view.receivedMail.receivedDate}" addtime="true" />
                    </div>
                </div>
            </div>

            <c:import url="${viewdir}/mail/_receivedMailAddresses.jsp"/>

            <c:choose>
                <c:when test="${!empty view.receivedMail.businessId}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="businessCaseIdLabel"><fmt:message key="businessCaseIdLabel" /></label>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group">
                                <a href="<c:url value="/loadBusinessCase.do?exit=${fetchmailInboxExitTarget}&id=${view.receivedMail.businessId}"/>">
                                    <o:out value="${view.receivedMail.businessId}" />
                                </a>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <v:link url="${view.baseLink}/resetBusinessCase" title="reset">
                                    <o:img name="deleteIcon" />
                                </v:link>
                            </div>
                        </div>
                    </div>
                </c:when>
                <c:when test="${!empty view.receivedMail.reference and view.receivedMail.referenceType == 1}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="customer"><fmt:message key="customer" /></label>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group">
                                <a href="<c:url value="/loadCustomer.do?exit=${fetchmailInboxExitTarget}&id=${view.receivedMail.reference}"/>">
                                    <o:out value="${view.receivedMail.reference}" />
                                </a>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <v:link url="${view.baseLink}/resetContact" title="reset">
                                    <o:img name="deleteIcon" />
                                </v:link>
                            </div>
                        </div>
                    </div>
                    <c:set var="businessCaseSelectionExit" scope="request" >userFetchmailInbox</c:set>
                    <c:import url="${viewdir}/customers/_customerSummaryRows.jsp" />

                </c:when>
                <c:when test="${!empty view.receivedMail.reference and view.receivedMail.referenceType == 2}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="supplier"><fmt:message key="supplier" /></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <a href="<c:url value="/loadSupplier.do?exit=${fetchmailInboxExitTarget}&id=${view.receivedMail.reference}"/>">
                                    <o:out value="${view.receivedMail.reference}" />
                                </a>
                            </div>
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="status"><fmt:message key="status" /></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <fmt:message key="noBusinessDataFound" />
                            </div>
                        </div>
                    </div>
                    <v:form url="${view.baseLink}/save" name="fetchmailInboxForm">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="businessIdInput"><fmt:message key="manuallyInput" /></label>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="form-group">
                                    <v:text name="businessId" styleClass="form-control" placeholder="businessCaseIdLabel" />
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <o:save/>
                                </div>
                            </div>
                        </div>
                    </v:form>
                </c:otherwise>
            </c:choose>

            <c:forEach var="attachment" items="${view.receivedMail.attachments}" varStatus="s">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="recipients">
                                <c:if test="${s.index == 0}">
                                    <fmt:message key="attachments" />
                                </c:if>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="form-group">
                            <o:out value="${attachment.fileName}" />
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <v:link url="${view.baseLink}/downloadAttachment?attachmentId=${attachment.id}" title="downloadLabel">
                                <o:img name="printIcon" />
                            </v:link>
                        </div>
                    </div>
                </div>
            </c:forEach>

            <c:if test="${empty view.receivedMail.businessId}">
                <div class="row next">
                    <div class="col-md-4"></div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <v:submitExit url="${view.baseLink}/delete" styleClass="form-control error" value="deleteFetchedMail" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </c:if>

        </div>
    </div>
</div>
