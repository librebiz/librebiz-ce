/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 1, 2010 11:16:00 AM 
 * 
 */
package com.osserp.core.model.telephone;

import com.osserp.core.telephone.Telephone;
import com.osserp.core.telephone.TelephoneConfiguration;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public class TelephoneConfigurationImpl extends AbstractTelephoneConfigurationImpl implements TelephoneConfiguration {
    public static final String ACCOUNT_CODE = "account_code";
    public static final String AVAILABLE = "available";
    public static final String BUSY_ON_BUSY = "busy_on_busy";
    public static final String CALL_FORWARD_DELAY_SECONDS = "call_forward_delay_seconds";
    public static final String CALL_FORWARD_ENABLED = "call_forward_enabled";
    public static final String CALL_FORWARD_IF_BUSY_TARGET_PHONE_NUMBER = "call_forward_if_busy_target_phone_number";
    public static final String CALL_FORWARD_IF_BUSY_ENABLED = "call_forward_if_busy_enabled";
    public static final String CALL_FORWARD_TARGET_PHONE_NUMBER = "call_forward_target_phone_number";
    public static final String DISPLAY_NAME = "display_name";
    public static final String EMAIL_ADDRESS = "email_address";
    public static final String INTERNAL = "internal";
    public static final String PARALLEL_CALL_ENABLED = "parallel_call_enabled";
    public static final String PARALLEL_CALL_SET_DELAY_SECONDS = "parallel_call_set_delay_seconds";
    public static final String PARALLEL_CALL_TARGET_PHONE_NUMBER = "parallel_call_target_phone_number";
    public static final String REBOOT_PHONE = "reboot_phone";
    public static final String SEND_CHECK_CONFIG_TO_PHONE = "send_check_config_to_phone";
    public static final String TELEPHONE = "telephone";
    public static final String TELEPHONE_EXCHANGE = "telephone_exchange";
    public static final String TELEPHONE_GROUP = "telephone_group";
    public static final String VOICE_MAIL_ENABLED = "voice_mail_enabled";
    public static final String VOICE_MAIL_DELETE_AFTER_EMAIL_SEND = "voice_mail_delete_after_email_send";
    public static final String VOICE_MAIL_DELAY_SECONDS = "voice_mail_delay_seconds";
    public static final String VOICE_MAIL_ENABLED_IF_BUSY = "voice_mail_enabled_if_busy";
    public static final String VOICE_MAIL_ENABLED_IF_UNAVAILABLE = "voice_mail_enabled_if_unavailable";
    public static final String VOICE_MAIL_SEND_AS_EMAIL = "voice_mail_send_as_email";

    private Telephone telephone = null;
    private Long employeeId = null;
    private String internal = null;

    protected TelephoneConfigurationImpl() {
        super();
    }

    public TelephoneConfigurationImpl(Long employeeId, String uid, String internal, String accountCode, String displayName, String emailAddress,
            Telephone telephone) {
        super(accountCode, displayName, emailAddress, uid);
        this.employeeId = employeeId;
        this.internal = internal;
        this.telephone = telephone;
    }

    public Telephone getTelephone() {
        return telephone;
    }

    public void setTelephone(Telephone telephone) {
        this.telephone = telephone;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    @SuppressWarnings("unused")
    private void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public String getInternal() {
        return internal;
    }

    public void setInternal(String internal) {
        this.internal = internal;
    }

    public boolean isInternalEditable() {
        if (employeeId != null) {
            return false;
        }
        return true;
    }
}
