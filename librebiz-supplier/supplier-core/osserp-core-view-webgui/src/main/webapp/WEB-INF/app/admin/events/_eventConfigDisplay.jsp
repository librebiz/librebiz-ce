<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<v:view viewName="eventConfigView"/>
<c:set var="eventConfigView" scope="request" value="${view}"/>
<c:set var="eventConfig" scope="request" value="${view.bean}"/>
<c:import url="_eventConfigDisplayContent.jsp"/>
