/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 31-Oct-2006 18:48:20 
 * 
 */
package com.osserp.gui.client.sales.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionException;
import com.osserp.common.dms.DmsManager;
import com.osserp.common.dms.DmsReference;
import com.osserp.common.web.Form;
import com.osserp.common.web.ViewName;

import com.osserp.core.finance.RecordSearch;
import com.osserp.core.planning.ProductPlanningManager;
import com.osserp.core.projects.Project;
import com.osserp.core.projects.ProjectFcsManager;
import com.osserp.core.sales.Sales;
import com.osserp.core.sales.SalesListItem;
import com.osserp.core.sales.SalesSearch;

import com.osserp.gui.client.impl.AbstractBusinessCaseView;
import com.osserp.gui.client.sales.SalesView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("businessCaseView")
public class SalesViewImpl extends AbstractBusinessCaseView implements SalesView {
    private static Logger log = LoggerFactory.getLogger(SalesViewImpl.class.getName());

    private boolean deliveryWarning = false;

    private boolean serviceSales = false;
    private Sales serviceSalesParent = null;
    private List<SalesListItem> serviceSalesList = new ArrayList<>();

    private boolean changeValuesOnClosedMode = false;

    protected SalesViewImpl() {
        super();
        if (log.isDebugEnabled()) {
            log.debug("<init> done by default constructor");
        }
    }

    protected SalesViewImpl(String forwardTarget, Long requestLetterTypeId) {
        super(forwardTarget, requestLetterTypeId);
        if (log.isDebugEnabled()) {
            log.debug("<init> done [forwardTarget=" + forwardTarget + "]");
        }
    }

    public void init(Sales sales, String exitTarget) throws ClientException, PermissionException {
        super.load(sales, exitTarget);
        updateDeliveryStatus();
        loadServiceEnvironment();
    }

    public int getDocumentCount() {
        return (getSalesDocumentCount() + getSalesPictureCount() + getLetterCount());
    }

    // delivery warning

    public void updateDeliveryStatus() {
        long start = System.currentTimeMillis();
        try {
            Sales sales = getSales();
            if (sales != null && sales.getStatus() < Sales.STATUS_BILLED) {
                ProductPlanningManager planning = (ProductPlanningManager) getService(ProductPlanningManager.class.getName());
                deliveryWarning = !planning.isAvailableForDelivery(sales);
            }
        } catch (Throwable t) {
            log.debug("updateDeliveryStatus() ignoring failure: " + t.toString(), t);
        }
        if (log.isDebugEnabled()) {
            log.debug("updateDeliveryStatus() done, duration (ms) "
                    + (System.currentTimeMillis() - start));
        }
    }

    public boolean isDeliveryWarning() {
        return deliveryWarning;
    }

    @Override
    public void disableAllModes() {
        super.disableAllModes();
    }

    // check order delivery date

    protected void setDeliveryWarning(boolean deliveryWarning) {
        this.deliveryWarning = deliveryWarning;
    }

    // reference settings 

    private boolean referenceSetupMode = false;

    public boolean isReferenceSetupMode() {
        return referenceSetupMode;
    }

    public void setReferenceSetupMode(boolean referenceSetupMode) {
        this.referenceSetupMode = referenceSetupMode;
    }

    public void updateReferenceStatus(Form form) throws ClientException, PermissionException {
        if (!isUserRelatedSales()) {
            checkPermission(new String[] {
                    "project_as_reference",
                    "executive" }
                    );
        }
        Sales sales = getSales();
        sales.changeReference(
                form.getBoolean(Form.REFERENCE),
                form.getString(Form.NOTE),
                getDomainUser().getEmployee().getId());
        getSalesManager().save(sales);
        if (isPermissionGrant("executive,sales,project_internet_config")) {
            boolean currentExportStatus = sales.isInternetExportEnabled();
            boolean newExportStatus = form.getBoolean("internetExportEnabled");
            if (newExportStatus && !currentExportStatus) {
                DmsManager pictureManager = (DmsManager) getService(DmsManager.class.getName());
                if (pictureManager.getCountByReference(new DmsReference(Project.PICTURES, sales.getId())) < 1) {
                    throw new ClientException(ErrorCode.INTERNETEXPORT_PICTURE_MISSING);
                }
            }
            if (newExportStatus != currentExportStatus) {
                getSalesManager().changeWebsiteReferenceStatus(sales);
            }
        }
        referenceSetupMode = false;
    }

    private boolean parentEditMode = false;

    public boolean isParentEditMode() {
        return parentEditMode;
    }

    public void setParentEditMode(boolean parentEditMode) {
        this.parentEditMode = parentEditMode;
    }

    public void updateParent(Form form) throws ClientException {
        Sales sales = getSales();
        Long parentId = form.getLong("parentId");
        try {
            getSalesManager().load(parentId);
            sales.setParentId(parentId);
            getSalesManager().save(sales);
            loadServiceEnvironment();
            parentEditMode = false;
        } catch (Throwable t) {
            log.error("updateParent(): given parent id doesn't exist");
            throw new ClientException(ErrorCode.ID_INVALID);
        }
    }

    // service settings
    /**
     * @return the serviceSales
     */
    public boolean isServiceSales() {
        return serviceSales;
    }

    /**
     * @return the serviceSalesParent
     */
    public Sales getServiceSalesParent() {
        return serviceSalesParent;
    }

    /**
     * @return the serviceSalesList
     */
    public List<SalesListItem> getServiceSalesList() {
        return serviceSalesList;
    }

    @Override
    public int getLetterCount() {
        if (getBean() != null && getLetterType() != null) {
            Sales sales = getSales();
            return getLetterManager().getCount(getLetterType(), sales.getRequest().getCustomer(), sales.getId());
        }
        return 0;
    }

    @Override
    protected void loadRecordCounters() {
        Sales sales = getSales();
        setRecordCount(getSalesOrderManager().exists(sales) ? 1 : 0);
        RecordSearch recordSearch = (RecordSearch) getService(RecordSearch.class.getName());
        setInvoiceCount(recordSearch.getInvoiceCountAll(sales.getId()));
    }

    public Long restoreRequestStatus() throws ClientException {
        return getSalesManager().resetRequestStatus(
                getDomainUser(), getSales()).getPrimaryKey();
    }

    public void reopenClosed() throws ClientException {
        ProjectFcsManager fcsmng = (ProjectFcsManager) getService(ProjectFcsManager.class.getName());
        setBean(fcsmng.resetClosed(getDomainEmployee(), getSales()));
    }

    // helpers

    public boolean isChangeValuesOnClosedMode() {
        return changeValuesOnClosedMode;
    }

    public void setChangeValuesOnClosedMode(boolean changeValuesOnClosedMode) {
        this.changeValuesOnClosedMode = changeValuesOnClosedMode;
    }

    @Override
    public void reload() {
        setBean(getSalesManager().load(getSales().getId()));
        loadDependencies();
        loadServiceEnvironment();
        changeValuesOnClosedMode = false;
    }

    private void loadServiceEnvironment() {
        Sales sales = getSales();
        if (sales != null) {
            try {
                SalesSearch salesSearch = (SalesSearch) getService(SalesSearch.class.getName());
                if (!sales.getType().isService()) {
                    serviceSales = false;
                    serviceSalesList = salesSearch.findServiceSales(sales.getId());
                    if (log.isDebugEnabled() && !serviceSalesList.isEmpty()) {
                        log.debug("loadServiceEnvironment() found related service orders [count=" + serviceSalesList.size() + "]");
                    }
                } else {
                    serviceSales = true;
                    serviceSalesList.clear();
                    try {
                        serviceSalesParent = salesSearch.fetchSales(sales.getParentId());
                        if (log.isDebugEnabled() && serviceSalesParent != null) {
                            log.debug("loadServiceEnvironment() service sales [parent=" + serviceSalesParent.getId() + "]");
                        }
                    } catch (ClientException e) {
                        log.warn("loadServiceEnvironment() found service with missing parent [sales=" + sales.getId() + "]");
                    }
                }
            } catch (Throwable e) {
                log.warn("loadServiceEnvironment() ignoring exception [sales=" + sales.getId()
                        + ", message=" + e.getMessage()
                        + "]");
            }
        }
    }
}
