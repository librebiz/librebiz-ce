/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 1, 2006 3:43:11 PM 
 * 
 */
package com.osserp.gui.web.struts;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.jdom2.Document;
import org.jdom2.Element;

import com.osserp.common.Parameter;
import com.osserp.common.web.Globals;
import com.osserp.common.web.View;
import com.osserp.common.xml.JDOMUtil;
import com.osserp.core.users.DomainUser;
import com.osserp.gui.client.ServiceManager;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractAutocompleteAction extends Action {

    private static Logger log = LoggerFactory.getLogger(AbstractAutocompleteAction.class.getName());

    @Override
    public final ActionForward execute(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        String xml = null;

        try {
            xml = getXmlContent(mapping, form, request, response);
        } catch (Exception ex) {
            // Send back a 500 error code.
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
                    "Can not create response");
            return null;
        }
        // Set content to xml
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        PrintWriter pw = response.getWriter();
        pw.write(xml);
        pw.close();
        return null;
    }

    public String getXmlContent(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        List<Parameter> list = null;
        try {
            list = getParameterList(mapping, form, request, response);
        } catch (Exception e) {
            log.warn("getXmlContent() failed [message=" + e.getMessage() + "]");
            list = new ArrayList<Parameter>();
        }
        Document doc = createXml(list);
        String xml = JDOMUtil.getPrettyString(doc);
        return xml;
    }

    protected Document createXml(List<Parameter> params) {
        Element root = new Element("tbody");
        root.setAttribute("id", "autoCompleteTableBody");
        //Element response = new Element("div");
        for (int i = 0, j = params.size(); i < j; i++) {
            Parameter next = params.get(i);
            Element item = new Element("tr");
            Element name = new Element("td");
            String onclick = "$('value').value = '" + next.getValue() + "'; $('autoCompleteTable').style.display = 'none';";
            String onmouseover = "this.style.background='#DCDCDC'";
            String onmouseout = "this.style.background='#FFFAFA'";
            name.setAttribute("onclick", onclick);
            name.setAttribute("onmouseover", onmouseover);
            name.setAttribute("onmouseout", onmouseout);
            //name.setContent(new CDATA(next.getName()));
            //Element value = new Element("div");
            name.addContent((next.getValue()));
            item.addContent(name);
            //item.addContent(value);
            root.addContent(item);
        }
        //root.addContent(response);
        Document doc = new Document(root);
        return doc;
    }

    protected abstract List<Parameter> getParameterList(ActionMapping mapping,
            ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception;

    protected DomainUser getDomainUser(HttpServletRequest request) {
        try {
            return (DomainUser) request.getSession(true)
                    .getAttribute(Globals.USER);
        } catch (Throwable e) {
            return null;
        }
    }

    protected View getView(HttpSession session, Class viewClass) {
        View view = ServiceManager.fetchView(session, viewClass);
        return view;
    }
}
