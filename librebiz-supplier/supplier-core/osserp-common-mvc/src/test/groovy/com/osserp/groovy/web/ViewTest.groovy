/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.groovy.web

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static org.junit.Assert.*
import org.junit.Test

import com.osserp.groovy.web.View

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public class ViewTest {
	private static Logger logger = LoggerFactory.getLogger(ViewTest.class.getName())
	
	@Test 
	void viewName() {
		View view = new ExampleView()
		logger.debug("viewName=${view.name}")
		assertTrue(view.name == 'exampleView')
	}
	
	@Test 
	void editModeDefault() {
		View view = new ExampleView()
		logger.debug("editModeDefault=${view.editMode}")
		assertFalse(view.editMode)
	}
	
	@Test 
	void enableEditMode() {
		View view = new ExampleView()
		view.enableEditMode()
		logger.debug("enableEditMode=${view.editMode}")
		assertTrue(view.editMode)
	}
	
	@Test 
	void disableEditMode() {
		View view = new ExampleView()
		view.enableEditMode()
		view.disableEditMode()
		logger.debug("disableEditMode=${view.editMode}")
		assertFalse(view.editMode)
	}
	
	@Test 
	void loadList() {
		ExampleView view = new ExampleView()
		view.loadList()
		logger.debug("loadList=[size:${view.list.size()}]")
		assertTrue(view.list.size() == 3)
	}

	@Test
	void collect() {
		ExampleView view = new ExampleView()
		view.loadList()
		view.initRequest([method:'collect'], [:], [id: '2'])
		view.collect()
		logger.debug("collect=${view.collected.get(0)?.id}")
		assertTrue(view.collected.get(0)?.id == 2L)
	}

	@Test
	void select() {
		ExampleView view = new ExampleView()
		view.loadList()
		view.initRequest([method:'select'], [:], [id: '2'])
		view.select()
		logger.debug("select=${view.bean?.id}")
		assertTrue(view.bean?.id == 2L)
	}
	
	@Test 
	void destroyViewOnExit() {
		View view = new ExampleView()
		logger.debug("destroyViewOnExit=${view.destroyViewOnExit}")
		assertTrue(view.destroyViewOnExit)
	}
	
	@Test 
	void destroyViewOnHome() {
		View view = new ExampleView()
		logger.debug("destroyViewOnHome=${view.destroyViewOnHome}")
		assertTrue(view.destroyViewOnHome)
	}
}
