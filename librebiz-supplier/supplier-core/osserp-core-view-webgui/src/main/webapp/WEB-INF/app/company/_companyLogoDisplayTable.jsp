<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="exitTarget" value="${requestScope.clientLogoExit}"/>
<c:if test="${empty exitTarget}"><c:redirect url="/errors/error_context.jsp"/></c:if>
<c:choose>
    <c:when test="${!empty view.companyLogo}">
        <tr>
            <td><img src="<oc:img value="${view.companyLogo}"/>" style="max-width: 250px; max-height: 90px;" /></td>
            <td class="pvalue" style="vertical-align: bottom;"><v:link url="/company/clientLogo/forward?exit=${exitTarget}" title="changeLogo">
                    <fmt:message key="change" />
                </v:link></td>
        </tr>
    </c:when>
    <c:otherwise>
        <tr>
            <td><fmt:message key="logo" /></td>
            <td class="pvalue"><v:link url="/company/clientLogo/forward?exit=${exitTarget}" title="addLogo">
                    <fmt:message key="no" />
                </v:link></td>
        </tr>
    </c:otherwise>
</c:choose>
