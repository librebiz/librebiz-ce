#!/usr/bin/env bash
#
# Librebiz packaging script for common GNU/Linux distros.
#
#---------------------------------------------------------------------
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License Version 3 as 
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Author: Rainer Kirchner <rk@librebiz.org>
#
#---------------------------------------------------------------------

APP_TARGETNAME=librebiz
APP_TMPDIR=/dist/common

TMPD="$(dirname $0)"
echo ${TMPD} | grep '^/' >/dev/null 2>&1
if [ "$?" -eq 0 ]; then
    export SCRIPTDIR="${TMPD}"
else
    export SCRIPTDIR="$(pwd)"
fi

SUPPLIER_CORE="${SCRIPTDIR}/librebiz-supplier/supplier-core"

LIBREBIZ_VERSION=`cat ./build | grep LIBREBIZ_VERSION | cut -d "=" -f 2`

echo "Packaging release ${LIBREBIZ_VERSION}"

APP_TAR_NAME=${APP_TARGETNAME}-bin-${LIBREBIZ_VERSION}
APP_TAR_NAME_LATEST=${APP_TARGETNAME}-bin-latest
APP_TARGET=${APP_TMPDIR}/${APP_TARGETNAME}

APP_TARGET=${SCRIPTDIR}/${APP_TARGET}

# Cleanup previous if exists
rm -rf ${SCRIPTDIR}/${APP_TMPDIR}

BINDIR=${APP_TARGET}/sbin
mkdir -p ${BINDIR}
cp -f ${SCRIPTDIR}/librebiz-common/src/main/scripts/librebiz-admin.common \
 ${BINDIR}/librebiz-admin
chmod +x ${BINDIR}/librebiz-admin

CFGDIR=${APP_TARGET}/etc
mkdir -p ${CFGDIR}
cp -f ${SCRIPTDIR}/librebiz-common/src/main/config/apache-httpd.conf\
 ${SCRIPTDIR}/librebiz-common/src/main/config/apache-ajp.conf\
 ${SCRIPTDIR}/librebiz/src/main/config/.activemqrc\
 ${SCRIPTDIR}/librebiz/src/main/config/activemq.xml\
 ${SCRIPTDIR}/librebiz/src/main/config/librebiz-mq.service\
 ${SCRIPTDIR}/librebiz/src/main/config/librebiz.service\
 ${SCRIPTDIR}/librebiz/src/main/config/server.xml\
 ${SCRIPTDIR}/librebiz/src/main/config/setenv.sh\
 ${CFGDIR}

sed -e 's/\/share/\/local/g' ${SCRIPTDIR}/librebiz-runtime/librebiz-tomcat8/src/deb/tomcat8/librebiz-context.xml\
 > ${CFGDIR}/librebiz-context.xml

cp -R ${SCRIPTDIR}/librebiz-webapp/src/main/config ${CFGDIR}

LIBDIR=${APP_TARGET}/lib
mkdir -p ${LIBDIR}
cp -f ${SCRIPTDIR}/librebiz-webapp/target/librebiz-webapp.war ${LIBDIR}/librebiz-webapp-${LIBREBIZ_VERSION}.war
cp -f ${SCRIPTDIR}/librebiz-cli/target/librebiz-cli.jar ${LIBDIR}/librebiz-cli-${LIBREBIZ_VERSION}.jar
cd ${LIBDIR}
ln -s ./librebiz-webapp-${LIBREBIZ_VERSION}.war ./librebiz.war
ln -s ./librebiz-cli-${LIBREBIZ_VERSION}.jar ./librebiz-cli.jar

DATADIR=${APP_TARGET}/var
mkdir -p ${DATADIR}
mkdir ${DATADIR}/vmtpl
cp -R ${SUPPLIER_CORE}/osserp-core-templates/vmtpl/* ${DATADIR}/vmtpl

mkdir -p ${DATADIR}/dms/clients/
cp -R ${SUPPLIER_CORE}/osserp-core-templates/dms/clients ${DATADIR}/dms
mkdir -p ${DATADIR}/dms/attachments\
 ${DATADIR}/dms/clients/bankaccounts\
 ${DATADIR}/dms/clients/records\
 ${DATADIR}/dms/clients/registrations\
 ${DATADIR}/dms/contacts/documents\
 ${DATADIR}/dms/employees/pictures\
 ${DATADIR}/dms/exports/reports\
 ${DATADIR}/dms/imports\
 ${DATADIR}/dms/incoming\
 ${DATADIR}/dms/products/documents\
 ${DATADIR}/dms/products/pictures\
 ${DATADIR}/dms/purchasing/invoices\
 ${DATADIR}/dms/requests/documents\
 ${DATADIR}/dms/requests/pictures\
 ${DATADIR}/dms/sales/creditnotes\
 ${DATADIR}/dms/sales/documents\
 ${DATADIR}/dms/sales/invoices\
 ${DATADIR}/dms/sales/pictures

cp -f ${SCRIPTDIR}/COPYING ${APP_TARGET}
cp -f ${SCRIPTDIR}/README.md ${APP_TARGET}/README

cd ${SCRIPTDIR}/${APP_TMPDIR}
tar cfz ./${APP_TAR_NAME}.tar.gz ./${APP_TARGETNAME}
ln -s ./${APP_TAR_NAME}.tar.gz ${APP_TAR_NAME_LATEST}.tar.gz

if command -v sha256sum > /dev/null; then
    sha256sum ./${APP_TAR_NAME}.tar.gz > ${APP_TAR_NAME}.tar.gz.sha256
elif command -v md5 > /dev/null; then
    sha256 ./${APP_TAR_NAME}.tar.gz > ${APP_TAR_NAME}.tar.gz.sha256
fi

if which sha512sum > /dev/null; then
    sha512sum ./${APP_TAR_NAME}.tar.gz > ${APP_TAR_NAME}.tar.gz.sha512
elif which shasum > /dev/null; then
    sha512 ./${APP_TAR_NAME}.tar.gz > ${APP_TAR_NAME}.tar.gz.sha512
fi

rm -rf ./${APP_TARGETNAME}
