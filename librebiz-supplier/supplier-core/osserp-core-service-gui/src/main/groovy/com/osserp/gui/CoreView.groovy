/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 16, 2009 
 * 
 */
package com.osserp.gui

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.Option
import com.osserp.common.OptionsCache
import com.osserp.common.PermissionException
import com.osserp.common.TimeoutException
import com.osserp.common.UserManager
import com.osserp.common.service.SecurityService
import com.osserp.common.util.StringUtil

import com.osserp.core.employees.Employee
import com.osserp.core.employees.EmployeeGroup
import com.osserp.core.employees.EmployeeSearch
import com.osserp.core.system.BranchOffice
import com.osserp.core.system.SystemCompany
import com.osserp.core.system.SystemConfigManager
import com.osserp.core.users.DomainUser
import com.osserp.core.users.DomainUserManager

import com.osserp.groovy.web.AbstractView
import com.osserp.groovy.web.MenuItem

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class CoreView extends AbstractView {
    private static Logger logger = LoggerFactory.getLogger(CoreView.class.getName())

    List<SystemCompany> companies = []
    SystemCompany selectedCompany

    List<BranchOffice> branchOfficeList = []
    boolean branchSelectionRequired = false
    BranchOffice selectedBranch
    boolean multipleBranchsAvailable = false
    boolean multipleClientsAvailable = false
    boolean addressAutocompletionSupport = false

    boolean isSecurityServiceSetupRequired() {
        !getSecurityService().isConfigured()
    }

    protected SecurityService getSecurityService() {
        getService('primarySecurityService')
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            multipleClientsAvailable = (systemConfigManager.activeCompanies.size() > 1)
            if (companyPreselectionEnabled) {
                preselectSelectedCompany()
            }
        }
    }

    boolean isAddressAutocompletionSupportEnabled() {
        return isSystemPropertyEnabled('addressAutocompletionSupport')
    }

    boolean isStockManagementEnabled() {
        return systemConfigManager.stockManagementEnabled
    }

    /**
     * Indicates if company selected and more than one activated branchOffice 
     * was found
     * @return true if branch selection available 
     */
    boolean isBranchSelectionAvailable() {
        int a = 0
        if (selectedCompany && branchOfficeList) {
            branchOfficeList.each { BranchOffice branch ->
                if (!branch.endOfLife && branch.activated) {
                    a++
                }
            }
        }
        return a>1
    }

    /**
     * Indicates if headquarter should be preselected on initRequest.
     * This method requires to enable companyPreselectionEnabled to
     * take affect.      
     */
    boolean isBranchPreselectionEnabled() {
        env.branchPreselection
    }

    /**
     * Enables the preselection of headquarter on initRequest.
     */
    void enableBranchPreselection() {
        env.branchPreselection = true
    }

    /**
     * Disables the preselection of headquarter on initRequest.
     * This is the default.
     */
    void disableBranchPreselection() {
        env.branchPreselection = false
    }

    /**
     * Indicates if current users company relation should be preselected 
     * as selectedCompany property on initRequest.
     */
    boolean isCompanyPreselectionEnabled() {
        env.companyPreselection
    }

    /**
     * Enables preselection of company by current users company relation.
     */
    void enableCompanyPreselection() {
        env.companyPreselection = true
    }

    /**
     * Disables preselection of company by current users company relation.
     * This is the default.
     */
    void disableCompanyPreselection() {
        env.companyPreselection = false
    }

    /**
     * Selects company from companies list by form param 'id' 
     */
    final void selectCompany() {
        selectSelectedCompany(form.getLong('id'))
    }

    /**
     * Initializes selectedCompany and selectedBranch by provided ids.
     * @param company
     * @param branch
     */
    protected void selectCompany(Long company, Long branch) {
        selectedCompany = systemConfigManager.getCompany(company)
        branchOfficeList = systemConfigManager.getBranchs(selectedCompany?.id)
        selectSelectedBranch(branch)
    }

    /**
     * Sets selectedCompany by id, initializes branchOfficeList with associated
     * branchs and preselects selectedBranch if only a single branch is available.
     */
    void selectSelectedCompany(Long id) {
        if (id) {
            selectedCompany = companies.find { it.id == id }
            preselectBranchOfficeList(selectedCompany)
            logger.debug("selectSelectedCompany: done [company=${selectedCompany?.id}, branchs=${branchOfficeList?.size()}, preselectedBranch=${selectedBranch?.id}]")
            
        } else {
            selectedCompany = null
            preselectBranchOfficeList(null)
            logger.debug('selectSelectedCompany: reset done')
        }
    }

    /**
     * Initializes selectedCompany with the user related company.
     * Headquarter is used if user supports more than one company.
     */
    protected void preselectSelectedCompany() {
        selectedCompany = getClientByCurrentUser()
        preselectBranchOfficeList(selectedCompany)
        logger.debug("preselectSelectedCompany: done [company=${selectedCompany?.id}, branchs=${branchOfficeList?.size()}, preselectedBranch=${selectedBranch?.id}]")
    }

    /**
     * Initializes branchOfficeList with list of branchs related to provided company.
     * @param company
     */
    protected void preselectBranchOfficeList(SystemCompany company) {
        if (company) {
            branchOfficeList = systemConfigManager.getBranchs(company.id)
            if (branchOfficeList) {
                if (branchOfficeList.size() == 1) {
                    selectedBranch = branchOfficeList.get(0)
                } else {
                    List<BranchOffice> bol = []
                    BranchOffice preselected = null
                    branchOfficeList.each {
                        BranchOffice branch = it
                        if (branchPreselectionEnabled && branch.headquarter) {
                            preselected = branch
                        } else if (!branch.statuaryOffice) {
                            bol << branch
                        }
                    }
                    if (preselected) {
                        selectedBranch = preselected
                    } else if (bol.size() == 1) {
                        selectedBranch = bol.get(0)
                    }
                }
            }
        } else {
            branchOfficeList = []
            selectedBranch = null
        }
        if (branchOfficeList.size() > 1) {
            multipleBranchsAvailable = true
        } 
    }

    boolean isBranchSelectionMode() {
        env.branchSelectionMode
    }

    void disableBranchSelectionMode() {
        env.branchSelectionMode = false
    }

    void enableBranchSelectionMode() {
        env.branchSelectionMode = true
        branchOfficeList.clear()
        List<BranchOffice> allbranchs = []
        if (selectedCompany) {
            allbranchs = systemConfigManager.getBranchs(selectedCompany.id)
        } else {
            allbranchs = systemConfigManager.getBranchs()
        }
        allbranchs.each {
            BranchOffice branch = it
            // ignore eol branchs
            if (!branch.endup) {
                branchOfficeList << branch
            }
        }
        if (branchOfficeList.size() > 1) {
            multipleBranchsAvailable = true
        } 
    }

    /**
     * Selects branch from branchOfficeList by form param 'id' 
     */
    final void selectBranch() {
        selectSelectedBranch(form.getLong('id'))
    }

    protected void selectSelectedBranch(Long id) {
        if (id != null) {
            selectedBranch = branchOfficeList.find { id.equals(it.id) }
            logger.debug("selectSelectedBranch: done [id=${selectedBranch?.id}]")
        } else {
            selectedBranch = null
            logger.debug('selectSelectedBranch: reset done')
        }
        disableBranchSelectionMode()
    }

    protected void enableHeadquarterSelection() {
        List<BranchOffice> officeList = systemConfigManager.branchs
        officeList.each {
            if (it.headquarter) {
                branchOfficeList << it
                logger.debug("enableHeadquarterSelection: added branch [id=${it.id}. name=${it.name}]")
            }
        }
        if (branchOfficeList.size() == 1) {
            selectedBranch = branchOfficeList.get(0)
        }
        branchSelectionRequired = true
    }

    @Override
    Boolean isRoot() {
        isPermissionGrant({ "root" } as String[])
    }

    protected Employee getDomainEmployee() {
        DomainUser domainUser = getDomainUser()
        Employee employee = domainUser.getEmployee()
        if (employee?.isActive()) {
            return employee
        }
        throw new PermissionException()
    }

    protected Employee fetchDomainEmployee() {
        DomainUser domainUser = fetchDomainUser()
        Employee employee = domainUser?.getEmployee()
        if (employee?.isActive()) {
            return employee
        }
        return null
    }

    protected final DomainUser fetchDomainUser() {
        if (user instanceof DomainUser) {
            return (DomainUser) user
        }
        return null
    }

    protected final DomainUser getDomainUser() {
        DomainUser du = fetchDomainUser()
        if (!du && !user) {
            logger.debug("getDomainUser: throwing timeout exception - user not found or user not domainUser...")
            throw new TimeoutException()
        } else if (!du) {
            logger.debug("getDomainUser: throwing permission exception - user was found but not a domainUser...")
            throw new PermissionException()
        }
        return du
    }

    protected DomainUser getDomainUser(Long userId) {
        DomainUserManager userManager = getDomainUserManager()
        return (userManager == null ? null : (DomainUser) userManager.findById(userId))
    }

    protected List<Employee> getActivatedEmployeeList() {
        List<Employee> list = employeeSearch.findAllActive(domainUser)
        logger.debug("getActivatedEmployeeList: done [count=${list.size()}]")
        return list
    }

    protected List<Employee> getEmployeesByGroup(String groupKey) {
        List<Employee> list = employeeSearch.findByGroup(domainUser, groupKey)
        logger.debug("getEmployeesByGroup: done [count=${list.size()}, group=${groupKey}]")
        return list
    }

    protected List<Employee> getSalesPersonList() {
        getEmployeesByGroup(EmployeeGroup.SALES)
    }

    protected final DomainUserManager getDomainUserManager() {
        getService(UserManager.class.getName())
    }

    protected final EmployeeSearch getEmployeeSearch() {
        getService(EmployeeSearch.class.getName())
    }

    protected final SystemConfigManager getSystemConfigManager() {
        getService(SystemConfigManager.class.getName())
    }

    protected final BranchOffice getBranchByCurrentUser() {
        BranchOffice result = domainUser?.employee?.defaultRoleConfig?.branch
        if (!result) {
            List<BranchOffice> branchs = systemConfigManager.getBranchs(domainEmployee)
            branchs.each {
                if (it.isHeadquarter()) {
                    result = it
                }
            }
            if (!result && branchs) {
                // get first fetched branch if nothing assigned but any branch available 
                result = branchs.get(0)
            }
        }
        result
    }

    /**
     * Provides the company of current user. The company will be selected <br/>
     * - simply by id if domainEmployee.defaultRoleConfig is set or <br/>
     * - by the company defined as holding or if none of this matches <br/>
     * - the first company provided by systemConfigManager.activeCompanies
     */
    protected final SystemCompany getClientByCurrentUser() {
        SystemCompany result
        def companyId = domainUser?.employee?.defaultRoleConfig?.branch?.company?.id
        if (companyId) {
            result = systemConfigManager.getCompany(companyId)
        }
        if (!result) {
            List<SystemCompany> companies = systemConfigManager.activeCompanies
            companies.each {
                if (it.isHolding()) {
                    result = it
                }
            }
            if (!result && companies) {
                // get first fetched company if nothing assigned but any company available 
                result = companies.get(0)
            }
        }
        result
    }

    protected final boolean isSystemPropertyEnabled(String name) {
        'true' == systemConfigManager.getSystemProperty(name)
    }

    protected final String getSystemProperty(String name) {
        systemConfigManager.getSystemProperty(name)
    }

    protected final String getSystemProperty(String name, String defaultVal) {
        String val = systemConfigManager.getSystemProperty(name)
        return !val ? defaultVal : val
    }

    protected final int systemPropertyAsInt(String name) {
        try {
            return Integer.parseInt(getSystemProperty(name))
        } catch (Throwable e) {
            return 0
        }
    }

    protected final Option getOption(String name, Long id) {
        OptionsCache oc = getService(OptionsCache.class.getName())
        oc.getMapped(name, id)
    }

    protected final List<Option> getOptions(String name) {
        OptionsCache oc = getService(OptionsCache.class.getName())
        oc == null ? []: oc.getList(name)
    }

    protected final void reloadOptions(String name) {
        OptionsCache oc = getService(OptionsCache.class.getName())
        oc.refresh(name)
    }

    protected final List<String> getSystemPropertyArray(String name) {
        List result = []
        String pl = getSystemProperty(name)
        if (pl) {
            result = StringUtil.getTokenList(pl, ',')
        }
        return result
    }

    protected final boolean isPermissionGrantByConfig(String configName) {
        try {
            checkPermissionGrantByConfig(configName)
            return true
        } catch (PermissionException e) {
            logger.debug("isPermissionGrantByConfig: reports false [config=${configName}]")
            return false
        }
    }

    protected final void checkPermissionGrantByConfig(String configName) throws PermissionException {
        String[] permissions = systemConfigManager.getPermissionConfig(configName)
        if (permissions) {
            domainUser.checkPermission(permissions)
        } else {
            logger.warn("checkPermissionGrantByConfig: check against not configured config [name=${configName}]")
        }
    }

    protected MenuItem getXlsLink() {
        navigationLink("/${context.name}/renderXls", 'tableIcon', 'printXls')
    }

}
