/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 16, 2016 
 * 
 */
package com.osserp.gui.common

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.Details
import com.osserp.common.DetailsManager
import com.osserp.common.Property

import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class AbstractBusinessPropertyView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(AbstractBusinessPropertyView.class.getName())

    List<Property> existingProperties = []
    List<Property> existingValues = []
    String selectedLabel
    String selectedName
    Details detailsObject

    AbstractBusinessPropertyView() {
        providesCreateMode()
        headerName = 'propertyEdit'
    }
    
    protected abstract Details fetchDetails();
    
    protected abstract DetailsManager getDetailsManager();
    
    protected abstract void reloadDetails();

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            detailsObject = fetchDetails()
            list = detailsObject.propertyList
            logger.debug("initRequest: done [id=${detailsObject.primaryKey}, count=${list.size()}]")
        }
    }

    @Override
    void select() {
        super.select()
        if (bean) {
            selectedName = bean.name
            enableEditMode()
        } else {
            selectedName = null
            disableEditMode()
        }
        loadExistingValues()
    }
    
    boolean isSelectedNameAvailable() {
        isSelected(selectedName)
    }

    @Override
    void enableCreateMode() {
        super.enableCreateMode()
        loadExisting()
    }

    protected loadExisting() {
        existingProperties = detailsManager.findExistingProperties(detailsObject)
        logger.debug("loadExisting: done [id=${detailsObject.primaryKey}, count=${existingProperties.size()}]")
    }
    
    protected loadExistingValues() {
        if (selectedName) {
            existingValues = detailsManager.findExistingValues(selectedName, detailsObject.propertyList)
        } else {
            existingValues = []
        }
    }
    
    void removeProperty() {
        if (bean) {
            detailsObject = detailsManager.removeProperty(user, detailsObject, bean)
            list = detailsObject.propertyList
            disableEditMode()
        }
    }

    @Override
    void save() {
        // label == new property by manual input
        selectedLabel = form.getString('label')
        // name == existing label selected
        selectedName = form.getString('name')
        // value == new value by manual input
        String val = form.getString('value')
        // preselectedValue == existing value selected
        Long pval = form.getLong('preselectedValue')
        
        if (!updateOnly) {
            logger.debug("save: invoked [id=${detailsObject.primaryKey}, label=${selectedLabel}, name=${selectedName}, preselected=${pval}, manual=${val}]")
        }
        
        if (pval) {
            Property sp = existingValues.find { it.id == pval }
            if (sp) {
                val = sp.value
            }
        }
        if (!val && !updateOnly) {
            throw new ClientException(ErrorCode.VALUES_MISSING)
        }
        if (updateOnly) {
            
            loadExistingValues()
            
        } else if (createMode) {

            if (isSelected(selectedLabel)) {
                // new property by user input
                detailsManager.addProperty(
                    user, detailsObject, user.locale.language, selectedLabel, val)
                disableCreateMode()
                
            } else if (isSelected(selectedName)) {
                // new property by existing label
                detailsManager.addProperty(
                    user, detailsObject, user.locale.language, selectedName, val)
                disableCreateMode()
            } else {
                throw new ClientException(ErrorCode.VALUES_MISSING)
            }
            
        } else {
            // editMode; update existing property
            detailsManager.updateProperty(user, detailsObject, bean, val)
            bean = null
            disableEditMode()
        }
        if (!updateOnly) {
            reloadResourceBundle()
            reloadDetails()
            selectedLabel = null
            selectedName = null
            existingValues = []
            list = detailsObject.propertyList
        }
    }
    
    protected boolean isSelected(String value) {
        (value && value != 'undefined')
    }
}
