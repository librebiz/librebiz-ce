/**
 *
 * Copyright (C) 2022 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License (AGPL)
 * version 3 as published by the Free Software Foundation. In accordance
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code
 * versions must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area
 * or, if not reasonably feasible for technical reason, as a top-level
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute.
 * 
 */
package com.osserp.gui.customers

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ActionException
import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.util.EmailValidator
import com.osserp.core.customers.Customer
import com.osserp.core.customers.CustomerManager

import com.osserp.groovy.web.ViewContextException

import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class CustomerEditorView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(CustomerEditorView.class.getName())

    List<String> invoiceDeliveryList = [ 'optionally', 'email', 'postalDelivery' ]

    CustomerEditorView() {
        super()
        headerName = 'customerChange'
        dependencies = ['contactView']
        enableExternalInvocationMode()
    }

    String getAgentPermissions() {
        return 'executive,executive_sales,sales_agent_config'
    }

    Customer getCustomer() {
        bean
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (forwardRequest) {
            if (!env.contactView?.bean) {
                logger.warn("initRequest: not bound [user=${user?.id}, name=contactView]")
                throw new ViewContextException(this)
            }
            bean = env.contactView?.customer
            enableEditMode()
        }
    }

    void save() {
        if (!bean) {
            throw new ActionException()
        }
        if (!customer.isClient()) {
            Long type = form.getLong('typeId')
            if (type == null || type == 0) {
                throw new ClientException(ErrorCode.CUSTOMER_TYPE_MISSING)
            }
            customer.setTypeId(type)
        }
        Long status = form.getLong('statusId')
        if (status == null || status == 0) {
            throw new ClientException(ErrorCode.CUSTOMER_STATUS_MISSING)
        }
        customer.setStatusId(status);
        String vatId = form.getString('vatId')
        if (vatId) {
            customer.setVatId(vatId)
        }
        if (!customer.isClient()) {

            String invoiceBy = form.getString('invoiceBy')
            if (!invoiceBy || invoiceBy == 'optionally') {
                customer.setInvoiceBy(null)
            } else {
                customer.setInvoiceBy(form.getString('invoiceBy'))
            }
            String email = form.getString('invoiceEmail')
            if (email && !EmailValidator.validate(email)) {
                throw new ClientException(ErrorCode.EMAIL_INVALID)
            } 
            customer.setInvoiceEmail(email)

            if (isPermissionGrant(agentPermissions)) {
                customer.setAgent(form.getBoolean('agent'))
                if (customer.isAgent()) {
                    customer.setAgentCommission(form.getDouble('agentCommission'))
                    customer.setAgentCommissionPercent(form.getBoolean('agentCommissionPercent'))
                } else {
                    customer.setAgentCommission(null)
                    customer.setAgentCommissionPercent(false)
                }
                customer.setTipProvider(form.getBoolean('tipProvider'))
                if (customer.isTipProvider()) {
                    customer.setTipCommission(form.getDouble('tipCommission'))
                } else {
                    customer.setTipCommission(null)
                }
            }
        }
        customerManager.save(customer)

        if (env.contactView) {
            env.contactView.refresh()
        } 
    }

    protected CustomerManager getCustomerManager() {
        return getService(CustomerManager.class.getName())
    }
}
