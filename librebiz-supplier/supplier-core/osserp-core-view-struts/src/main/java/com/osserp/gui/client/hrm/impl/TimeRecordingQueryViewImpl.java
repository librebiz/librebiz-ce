/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Jan 15, 2010 11:51:22 AM 
 * 
 */
package com.osserp.gui.client.hrm.impl;

import javax.servlet.http.HttpServletRequest;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;
import com.osserp.common.web.ViewName;
import com.osserp.core.hrm.TimeRecordingQueryManager;
import com.osserp.gui.client.hrm.TimeRecordingQueryView;
import com.osserp.gui.client.impl.AbstractView;

/**
 * 
 * @author jg <jg@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
@ViewName("timeRecordingQueryView")
public class TimeRecordingQueryViewImpl extends AbstractView implements TimeRecordingQueryView {
    private String mail = null;

    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        mail = getDomainEmployee().getEmail();
        setEditMode(true);
    }

    @Override
    public void save(Form form) throws ClientException, PermissionException {
        this.checkPermission("time_recording_admin");
        String date = form.getString(Form.DATE);
        if (date == null) {
            throw new ClientException(ErrorCode.DATE_MISSING);
        }
        String email = form.getString(Form.EMAIL);
        if (email == null) {
            throw new ClientException(ErrorCode.EMAIL_MISSING);
        }
        Long typeId = form.getLong(Form.TYPE_ID);
        if (typeId == null) {
            typeId = 0L;
        }
        TimeRecordingQueryManager manager = (TimeRecordingQueryManager) getService(TimeRecordingQueryManager.class.getName());
        manager.query(date, email, typeId);
        setEditMode(false);
    }

    public String getMail() {
        return mail;
    }

}
