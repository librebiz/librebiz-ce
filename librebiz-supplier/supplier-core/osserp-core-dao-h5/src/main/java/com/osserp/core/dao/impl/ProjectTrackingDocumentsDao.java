/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 26, 2016 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.Date;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.ClientException;
import com.osserp.common.FileObject;
import com.osserp.common.User;
import com.osserp.common.dao.Tables;
import com.osserp.common.dms.DmsDocument;
import com.osserp.common.dms.DmsReference;
import com.osserp.common.dms.DocumentType;
import com.osserp.common.dms.dao.DocumentEncoder;
import com.osserp.common.dms.dao.impl.DatabaseDocumentsDao;
import com.osserp.common.service.SecurityService;

import com.osserp.core.dao.ProjectTrackingDocuments;
import com.osserp.core.model.projects.ProjectTrackingDocumentImpl;
import com.osserp.core.projects.ProjectTracking;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ProjectTrackingDocumentsDao extends DatabaseDocumentsDao implements ProjectTrackingDocuments {
    private String sequenceName = null;

    public ProjectTrackingDocumentsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            Long documentTypeId,
            String documentTableKey,
            JdbcTemplate imageTemplate,
            String documentBinaryTableKey,
            String documentSequenceKey,
            DocumentEncoder documentEncoder, 
            SecurityService authenticationProvider) {
        super(
                jdbcTemplate,
                tables,
                sessionFactory,
                documentTypeId,
                documentTableKey,
                null, // categoryTableKey not supported by trackings now
                imageTemplate,
                documentBinaryTableKey,
                documentEncoder, 
                authenticationProvider);
        sequenceName = getTable(documentSequenceKey);
    }

    public DmsDocument create(User user, ProjectTracking tracking, byte[] file)
            throws ClientException {
        StringBuilder fileName = new StringBuilder(96);
        if (tracking.getReference() != null) {
            fileName.append(tracking.getReference()).append("-");
        }
        DocumentType documentType = getDocumentType();
        DmsReference reference = new DmsReference(documentType.getId(), tracking.getId());
        fileName
                .append(tracking.getId())
                .append("-")
                .append(System.currentTimeMillis())
                .append(".")
                .append(getDocumentType().getSuffix());
        FileObject fileObject = new FileObject(fileName.toString(), file);
        DmsDocument doc = create(
                user,
                reference,
                fileObject,
                null,
                null,
                null,  // TODO:
                null,  // validFrom -> calculate by tracking records
                null,  // validTil -> same
                null);
        return doc;
    }

    @Override
    protected DmsDocument createObject(
            User user, 
            DocumentType documentType, 
            DmsReference reference, 
            String fileName, 
            long fileSize, 
            String note, 
            Date sourceDate, 
            Long categoryId, 
            Date validFrom, 
            Date validTil,
            String messageId) {
        Long id = getNextLong(sequenceName);
        return new ProjectTrackingDocumentImpl(
                id, 
                reference.getReferenceId(), 
                documentType, 
                fileName, 
                fileSize, 
                user.getId(), 
                note, 
                categoryId, 
                validFrom, 
                validTil,
                messageId);
    }

    @Override
    protected Class<ProjectTrackingDocumentImpl> getPersistentClass() {
        return ProjectTrackingDocumentImpl.class;
    }
}
