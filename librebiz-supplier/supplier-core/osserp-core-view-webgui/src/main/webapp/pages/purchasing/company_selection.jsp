<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.purchaseOrderView}">
<o:logger write="no purchaseOrderView bound!" level="debug"/>
<c:redirect url="/errors/error_context.jsp"/>
</c:if>
<c:set var="view" value="${sessionScope.purchaseOrderView}"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
<tiles:put name="title"><o:displayTitle/></tiles:put>
<tiles:put name="styles" type="string">
<style type="text/css">
</style>
</tiles:put>

<tiles:put name="headline"><fmt:message key="clientSelection"/></tiles:put>
<tiles:put name="headline_right">
<v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link>
</tiles:put>

<tiles:put name="content" type="string">
    <div class="subcolumns">
        <div class="subcolumn">
            <div class="spacer"></div>
            <div class="table-responsive table-responsive-default">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th style="width: 783px;" id="name">
                                <span style="margin-left: 140px;">
                                    <fmt:message key="company"/>
                                </span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="company" items="${view.clients}" varStatus="s">
                            <tr>
                                <td style="width: 783px;" valign="top" class="number">
                                    <span style="margin-left: 140px;">
                                        <a href="<c:url value="/purchaseOrder.do"><c:param name="method" value="setCompany"/><c:param name="id" value="${company.id}"/></c:url>"><oc:options name="systemCompanies" value="${company.id}"/></a>
                                    </span>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</tiles:put> 
</tiles:insert>
