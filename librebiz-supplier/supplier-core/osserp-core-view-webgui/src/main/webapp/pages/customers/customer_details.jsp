<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${sessionScope.contactView.customerView}">
<c:set var="contactView" value="${sessionScope.contactView}"/>
<c:set var="contact" value="${sessionScope.contactView.customer}"/>
<c:set var="paymentUrl" scope="request" value="/customers.do"/>
<div class="subcolumns">
	<c:choose>
		<c:when test="${!contactView.salesListingMode and !contactView.requestListingMode}">
			<c:import url="/pages/customers/customer_detail_default.jsp"/>
		</c:when>
		<c:otherwise>
			<c:import url="/pages/customers/customer_detail_listings.jsp"/>
		</c:otherwise>
	</c:choose>
</div>
</c:if>