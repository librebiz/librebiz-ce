/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 12-Aug-2006 12:56:53 
 * 
 */
package com.osserp.core.model.events;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.osserp.core.events.EventAction;
import com.osserp.core.events.EventConfigType;
import com.osserp.core.events.EventTermination;
import com.osserp.core.events.RecipientPool;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class EventActionImpl extends AbstractEventConfig implements EventAction {
    private String name = null;
    private String resourceKey = null;
    private String description = null;
    private int priority = 0;
    private List<EventTermination> closings = new ArrayList<EventTermination>();
    private String actionLink = null;
    // default termination time is 2 days
    private Long terminationTime = 172800000L;
    private Long terminationAlert;
    private EventAction alertEvent;
    private String targetQueue;
    private boolean closeableByTarget;
    private boolean sendAlert;
    private boolean sendPool;
    private RecipientPool pool = null;
    private boolean sendSales;
    private boolean sendManager;
    private boolean sendSelf;
    private boolean sendSelected;
    private boolean info = false;
    private boolean ignoringBranch = false;
    private boolean paused = false;
    private Long pausedBy;
    private Date pausedDate;
    
    protected EventActionImpl() {
        super();
    }

    /**
     * Constructor for new actions
     * @param id
     * @param type
     * @param caller
     * @param name
     */
    public EventActionImpl(
            Long id,
            EventConfigType type,
            Long caller,
            String name) {
        super(id, type, caller);
        this.name = name;
        this.actionLink = type.getActionLink();
    }

    /**
     * Constructor for informational actions
     * @param id
     * @param sourceAction
     * @param recipientPool
     */
    public EventActionImpl(Long id, EventAction sourceAction, RecipientPool recipientPool) {
        super(id, sourceAction.getType(), null);
        name = sourceAction.getName();
        sendManager = false;
        sendSales = false;
        sendPool = true;
        pool = recipientPool;
        priority = 1;
        actionLink = sourceAction.getType().getActionLink();
        setAlert(true);
        List<EventTermination> terminations = sourceAction.getClosings();
        for (int i = 0, j = terminations.size(); i < j; i++) {
            EventTermination t = terminations.get(i);
            addClosing(t.getCallerId());
        }
    }

    protected EventActionImpl(EventAction o) {
        super(o);
        name = o.getName();
        resourceKey = o.getResourceKey();
        description = o.getDescription();
        priority = o.getPriority();
        actionLink = o.getActionLink();
        terminationTime = o.getTerminationTime();
        terminationAlert = o.getTerminationAlert();
        alertEvent = o.getAlertEvent();
        targetQueue = o.getTargetQueue();
        closeableByTarget = o.isCloseableByTarget();
        sendAlert = o.isSendAlert();
        sendPool = o.isSendPool();
        pool = o.getPool();
        sendSales = o.isSendSales();
        sendManager = o.isSendManager();
        sendSelf = o.isSendSelf();
        sendSelected = o.isSendSelected();
        info = o.isInfo();
        ignoringBranch = o.isIgnoringBranch();
        closings = new ArrayList<EventTermination>();
        if (o.getClosings() != null && !o.getClosings().isEmpty()) {
            for (int i = 0, j = o.getClosings().size(); i < j; i++) {
                closings.add(o.getClosings().get(i));
            }
        }
    }

    @Override
    public Object clone() {
        return new EventActionImpl(this);
    }

    @Override
    public boolean isAction() {
        return true;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public List<EventTermination> getClosings() {
        return closings;
    }

    protected void setClosings(List<EventTermination> closings) {
        this.closings = closings;
    }

    public void addClosing(Long callerId) {
        if (!closingContains(callerId)) {
            EventTerminationImpl obj = new EventTerminationImpl(
                    this,
                    callerId);
            closings.add(obj);
        }
    }

    public String getActionLink() {
        return actionLink;
    }

    public void setActionLink(String actionLink) {
        this.actionLink = actionLink;
    }

    public Long getTerminationTime() {
        return terminationTime;
    }

    public void setTerminationTime(Long terminationTime) {
        this.terminationTime = terminationTime;
    }

    public Long getTerminationAlert() {
        return terminationAlert;
    }

    public void setTerminationAlert(Long terminationAlert) {
        this.terminationAlert = terminationAlert;
    }

    public EventAction getAlertEvent() {
        return alertEvent;
    }

    public void setAlertEvent(EventAction alertEvent) {
        this.alertEvent = alertEvent;
    }

    public void updateAlertEvent(RecipientPool pool) {
        if (alertEvent != null) {
            alertEvent.setPool(pool);
        }
    }

    public String getTargetQueue() {
        return targetQueue;
    }

    public void setTargetQueue(String targetQueue) {
        this.targetQueue = targetQueue;
    }

    public boolean isCloseableByTarget() {
        return closeableByTarget;
    }

    public void setCloseableByTarget(boolean closeableByTarget) {
        this.closeableByTarget = closeableByTarget;
    }

    public boolean isSendAlert() {
        return sendAlert;
    }

    public void setSendAlert(boolean sendAlert) {
        this.sendAlert = sendAlert;
    }

    public boolean isSendPool() {
        return sendPool;
    }

    public void setSendPool(boolean sendPool) {
        this.sendPool = sendPool;
    }

    public RecipientPool getPool() {
        return pool;
    }

    public void setPool(RecipientPool pool) {
        this.pool = pool;
    }

    public boolean isSendSales() {
        return sendSales;
    }

    public void setSendSales(boolean sendSales) {
        this.sendSales = sendSales;
    }

    public boolean isSendManager() {
        return sendManager;
    }

    public void setSendManager(boolean sendManager) {
        this.sendManager = sendManager;
    }

    public boolean isSendSelf() {
        return sendSelf;
    }

    public void setSendSelf(boolean sendSelf) {
        this.sendSelf = sendSelf;
    }

    public boolean isSendSelected() {
        return sendSelected;
    }

    public void setSendSelected(boolean sendSelected) {
        this.sendSelected = sendSelected;
    }

    public String getResourceKey() {
        return resourceKey;
    }

    public void setResourceKey(String resourceKey) {
        this.resourceKey = resourceKey;
    }

    public boolean isIgnoringBranch() {
        return ignoringBranch;
    }

    public void setIgnoringBranch(boolean ignoringBranch) {
        this.ignoringBranch = ignoringBranch;
    }

    public boolean isInfo() {
        return info;
    }

    public void setInfo(boolean info) {
        this.info = info;
    }

    private boolean closingContains(Long callerId) {
        for (int i = 0, j = closings.size(); i < j; i++) {
            EventTermination t = closings.get(i);
            if (t.getCallerId() != null && t.getCallerId().equals(callerId)) {
                return true;
            }
        }
        return false;
    }

    public boolean isSelectionLabel() {
        return false;
    }

    public boolean isInfoActionAvailable() {
        return isSet(getInfoActionId()) || (getType() != null && isSet(getType().getInfoActionId()));
    }

    public Long getInfoAction() {
        if (isSet(getInfoActionId())) {
            return getInfoActionId();
        }
        if (getType() != null && isSet(getType().getInfoActionId())) {
            return getType().getInfoActionId();
        }
        return null;
    }

    public boolean isPaused() {
        return paused;
    }

    public void setPaused(boolean paused) {
        this.paused = paused;
    }

    public Long getPausedBy() {
        return pausedBy;
    }

    public void setPausedBy(Long pausedBy) {
        this.pausedBy = pausedBy;
    }

    public Date getPausedDate() {
        return pausedDate;
    }

    public void setPausedDate(Date pausedDate) {
        this.pausedDate = pausedDate;
    }

}
