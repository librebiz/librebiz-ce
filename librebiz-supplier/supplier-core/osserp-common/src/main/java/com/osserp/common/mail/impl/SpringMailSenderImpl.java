/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 22, 2008 10:02:51 AM 
 * 
 */
package com.osserp.common.mail.impl;

import java.util.Iterator;
import java.util.List;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.mail.Attachment;
import com.osserp.common.mail.Mail;
import com.osserp.common.mail.MailSender;
import com.osserp.common.util.ExceptionUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SpringMailSenderImpl implements MailSender {
    private static Logger log = LoggerFactory.getLogger(SpringMailSenderImpl.class.getName());

    private JavaMailSender sender = null;
    private SimpleMailMessage defaultMessage = null;
    private SimpleMailMessage debugMessage = null;
    private SimpleMailMessage emptyMessage = null;
    private SimpleMailMessage errorMessage = null;
    private SimpleMailMessage infoMessage = null;
    private SimpleMailMessage noreplyMessage = null;
    private String defaultCharset = "utf-8";

    private boolean sendingDisabled = false;

    protected SpringMailSenderImpl(
            JavaMailSender sender,
            SimpleMailMessage defaultMessage,
            SimpleMailMessage emptyMessage,
            SimpleMailMessage debugMessage,
            SimpleMailMessage errorMessage,
            SimpleMailMessage infoMessage,
            SimpleMailMessage noreplyMessage,
            String defaultCharset,
            boolean sendingDisabled) {
        super();
        this.sender = sender;
        this.defaultMessage = defaultMessage;
        this.emptyMessage = emptyMessage;
        this.debugMessage = debugMessage;
        this.errorMessage = errorMessage;
        this.infoMessage = infoMessage;
        this.noreplyMessage = noreplyMessage;
        this.sendingDisabled = sendingDisabled;
        if (defaultCharset != null && defaultCharset.length() > 0) {
            this.defaultCharset = defaultCharset;
        }
    }

    public void sendDebug(String message) {
        debugMessage.setText(message);
        send(debugMessage);
    }

    public void sendError(String message) {
        errorMessage.setText(message);
        send(errorMessage);
    }

    public void sendError(String message, Throwable t) {
        StringBuffer buffer = new StringBuffer(256);
        if (message != null) {
            buffer.append(message).append("\n\nStackTrace:\n");
        }
        if (t != null) {
            buffer
                    .append("\nStackTrace:\n")
                    .append(ExceptionUtil.stackTraceToString(t));
        }
        errorMessage.setText(buffer.toString());
        send(errorMessage);
    }

    public void sendInfo(String[] recipients, String message) {
        infoMessage.setTo(recipients);
        infoMessage.setText(message);
        send(infoMessage);
    }

    public void sendNoreply(String[] recipients, String message) {
        noreplyMessage.setTo(recipients);
        noreplyMessage.setText(message);
        send(noreplyMessage);
    }

    public void send(String recipient, String subject, String message) {
        defaultMessage.setTo(recipient);
        defaultMessage.setSubject(subject);
        defaultMessage.setText(message);
        send(defaultMessage);
    }

    public void send(String[] recipients, String subject, String message) {
        defaultMessage.setTo(recipients);
        defaultMessage.setSubject(subject);
        defaultMessage.setText(message);
        send(defaultMessage);
    }

    public void send(String originator, String recipient, String subject, String message) {
        emptyMessage.setFrom(originator);
        emptyMessage.setTo(recipient);
        emptyMessage.setSubject(subject);
        emptyMessage.setText(message);
        send(emptyMessage);
    }

    public void send(String originator, String[] recipients, String subject, String message) {
        emptyMessage.setFrom(originator);
        emptyMessage.setTo(recipients);
        emptyMessage.setSubject(subject);
        emptyMessage.setText(message);
        send(emptyMessage);
    }

    public void send(List<Mail> mails) {
        for (int i = 0, j = mails.size(); i < j; i++) {
            Mail next = mails.get(i);
            try {
                send(next);
            } catch (Throwable t) {
                log.warn("send() failed for mail:\n" + next.toString());
            }
        }
    }

    public void send(Mail mail) throws ClientException {
        MimeMessage message = sender.createMimeMessage();
        if (mail.getOriginator() == null) {
            throw new ClientException(ErrorCode.ORIGINATOR_MISSING);
        }
        try {
            message.addFrom(
                    new Address[] { new InternetAddress(mail.getOriginator()) });
        } catch (Exception e) {
            log.error("send() failed on originator " + mail.getOriginator()
                    + ": " + e.toString(), e);
            throw new ClientException(ErrorCode.ORIGINATOR_INVALID);
        }

        if (mail.getRecipients().size() < 1) {
            throw new ClientException(ErrorCode.RECIPIENT_MISSING);
        }
        for (Iterator i = mail.getRecipients().iterator(); i.hasNext();) {
            String recipient = (String) i.next();
            try {
                message.addRecipient(Message.RecipientType.TO,
                        new InternetAddress(recipient));
            } catch (Exception e) {
                log.error("send() failed to add recipient " + recipient
                        + ": " + e.toString(), e);
            }
        }
        for (Iterator<String> i = mail.getRecipientsCC().iterator(); i.hasNext();) {
            String nextRecipient = i.next();
            try {

                message.addRecipient(Message.RecipientType.CC,
                        new InternetAddress(nextRecipient));
            } catch (Exception e) {
                log.error("send() ignoring failure while adding recipient (cc) " + nextRecipient
                        + ": " + e.toString());
            }
        }
        for (Iterator<String> i = mail.getRecipientsBCC().iterator(); i.hasNext();) {
            String nextRecipient = i.next();
            try {
                message.addRecipient(Message.RecipientType.BCC,
                        new InternetAddress(nextRecipient));
            } catch (Exception e) {
                log.error("send() ignoring failure while adding recipient (bcc) " + nextRecipient
                        + ": " + e.toString());
            }
        }
        if (mail.getSubject() != null) {
            try {
                message.setSubject(mail.getSubject());
            } catch (Exception ignorable) {
            }
        }
        try {
            if (message.getAllRecipients() != null
                    && message.getAllRecipients().length > 0) {
                addBody(message, mail);
                send(message);
                if (log.isDebugEnabled()) {
                    log.debug("send() done");
                }
            } else {
                log.warn("send() failed; ignoring message with subject '"
                        + (mail.getSubject() == null ? "null" : mail.getSubject())
                        + "'; no recipients found");
                throw new ClientException(ErrorCode.RECIPIENT_MISSING);
            }

        } catch (MessagingException e) {
            log.error("send() failed for message with subject '"
                    + (mail.getSubject() == null ? "null" : mail.getSubject())
                    + "': " + e.toString());
            throw new ClientException(ErrorCode.MAIL_SENDING_FAILED);
        }
    }

    private void addBody(MimeMessage message, Mail mail) throws ClientException {
        if (!mail.isAttachmentSet()) {
            if (mail.getText() != null) {
                try {
                    message.setText(mail.getText(), defaultCharset);
                } catch (Exception e) {
                    throw new ClientException(Mail.TEXT, Mail.TEXT_INVALID);
                }
            }
        } else {
            try {
                Multipart mp = new MimeMultipart("mixed");
                MimeBodyPart text = new MimeBodyPart();
                if (mail.getText() != null) {
                    text.setText(mail.getText(), defaultCharset);
                }
                mp.addBodyPart(text);
                for (Iterator i = mail.getAttachments().iterator(); i.hasNext();) {
                    Attachment attachment = (Attachment) i.next();
                    MimeBodyPart mimeBody = new MimeBodyPart64();
                    if (attachment.getBytes() != null) {
                        if (log.isDebugEnabled()) {
                            log.debug("addBody() adding binary [type=" + attachment.getMimeType() + "]");
                        }
                        mimeBody.setDataHandler(new DataHandler(
                                new MailDataSourceImpl(
                                        attachment.getBytes(),
                                        attachment.getMimeType())));
                        mimeBody.setHeader("Content-Type", attachment.getMimeType());
                        mimeBody.setDisposition(javax.mail.Part.ATTACHMENT);

                    } else if (attachment.getFile() != null) {
                        FileDataSource fds = new FileDataSource(attachment.getFile());
                        mimeBody.setDataHandler(new DataHandler(fds));
                        mimeBody.setDisposition(javax.mail.Part.ATTACHMENT);
                    }
                    if (attachment.getFileName() != null) {
                        mimeBody.setFileName(attachment.getFileName());
                    }
                    mp.addBodyPart(mimeBody);
                }
                // add the Multipart to the message
                message.setContent(mp);
                if (log.isDebugEnabled()) {
                    log.debug("addBody() done");
                }

            } catch (Exception e) {
                throw new ClientException(
                        Mail.ATTACHMENT,
                        Mail.ATTACHMENT_INVALID);
            }
        }
    }

    private void send(MimeMessage message) throws ClientException {
        if (!sendingDisabled) {
            try {
                sender.send(message);
            } catch (Throwable t) {
                log.warn("send() failed on attempt to send mime message: " + t.toString());
                throw new ClientException(ErrorCode.MAIL_SENDING_FAILED);
            }
        } else if (log.isDebugEnabled()) {
            log.debug("send() done (disabled by config)");
        }
    }

    private void send(SimpleMailMessage message) {
        if (!sendingDisabled) {
            try {
                sender.send(message);
            } catch (Throwable t) {
                log.warn("send() failed on attempt to send SimpleMailMessage [message=" + t.toString() + "]");
            }
        } else if (log.isDebugEnabled()) {
            log.debug("send() done (disabled by config)");
        }
    }
}
