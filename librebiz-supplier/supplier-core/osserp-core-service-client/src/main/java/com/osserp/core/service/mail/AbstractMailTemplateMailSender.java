/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 1, 2009 10:37:42 AM 
 * 
 */
package com.osserp.core.service.mail;

import java.util.Map;

import com.osserp.core.dao.MailTemplates;
import com.osserp.core.service.impl.AbstractService;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class AbstractMailTemplateMailSender extends AbstractService {

    private MailTemplates mailTemplates = null;
    private String templateName = null;

    /**
     * Creates a new mail template aware mail sender
     * @param sender
     * @param engine
     * @param templateName
     * @param systemConfigs
     */
    protected AbstractMailTemplateMailSender(MailTemplates mailTemplates, String templateName) {
        super();
        this.templateName = templateName;
        this.mailTemplates = mailTemplates;
    }

    /**
     * Creates a mail message by configured template name and recipients as provided by values or mail template config.
     * @param values
     */
    protected final void send(Map values) {
        assert mailTemplates != null : "mailTemplates property must not be null when using send(Map)";
        mailTemplates.send(templateName, values);
    }

    /**
     * Creates a mail message by configured template name and given recipient.
     * @param recipient
     * @param values
     */
    protected final void send(String recipient, Map values) {
        send(new String[] { recipient }, values);
    }

    /**
     * Creates a mail message by configured template name and given recipients.
     * @param recipients
     * @param values
     */
    protected final void send(String[] recipients, Map values) {
        assert mailTemplates != null : "mailTemplates property must not be null when using send(String[], Map)";
        mailTemplates.send(templateName, recipients, values);
    }
}
