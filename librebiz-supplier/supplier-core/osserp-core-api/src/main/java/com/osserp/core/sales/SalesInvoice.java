/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 23, 2006 4:33:59 PM 
 * 
 */
package com.osserp.core.sales;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.osserp.core.employees.Employee;
import com.osserp.core.finance.CancellableRecord;
import com.osserp.core.finance.CreditNote;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.RebateAwareRecord;
import com.osserp.core.finance.RecordType;
import com.osserp.core.products.Product;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SalesInvoice extends CancellableRecord, Invoice, RebateAwareRecord, SalesRecord {
    static final Long RECORD_TYPE = RecordType.SALES_INVOICE;
    public static final Long RECORD_DOCUMENT_TYPE = 17L;

    /**
     * Adds a credit note to the invoice
     * @param user addding the credit note
     * @param id for the credit note
     * @param type of record
     * @param bookingType of new creditNode
     * @param copyItems
     * @return new added credit note
     */
    CreditNote addCreditNote(Employee user, Long id, RecordType type, SalesCreditNoteType bookingType, boolean copyItems);

    /**
     * Adds an accounting item
     * @param stockId
     * @param product
     * @param customName
     * @param quantity
     * @param price
     * @param note
     * @param includePrice
     * @param unchecked
     * @param accountingReferenceId
     * @param accountingReferenceDate
     */
    void addItem(
            Long stockId,
            Product product,
            String customName,
            Double quantity,
            BigDecimal price,
            String note,
            boolean includePrice,
            boolean unchecked,
            Long accountingReferenceId,
            Date accountingReferenceDate);

    /**
     * Provides all credit notes related to the invoice
     * @return creditNotes
     */
    List<CreditNote> getCreditNotes();

    /**
     * Indicates that the invoice is credited in full, e.g. the summary 
     * of all related credit notes equals the total amount of invoice. 
     * @return true if credited in full
     */
    boolean isCreditedInFull();
    
    /**
     * Indicates that order items should be displayed on print output
     * @return printOrderItems
     */
    boolean isPrintOrderItems();

    /**
     * Provides a reference to a third party order
     * @return thirdPartyOrder
     */
    Long getThirdPartyOrder();

    /**
     * @return the amount that has been billed previously via partial invoices (net value)
     */
    BigDecimal getPartialInvoiceAmount();

    /**
     * @param partialInvoiceAmount
     */
    void setPartialInvoiceAmount(BigDecimal partialInvoiceAmount);

    /**
     * @return the amount that has been billed previously via partial invoices (gross value)
     */
    BigDecimal getPartialInvoiceAmountGross();

    /**
     * @param partialInvoiceAmountGross
     */
    void setPartialInvoiceAmountGross(BigDecimal partialInvoiceAmountGross);
    
    /**
     * Enables or disables historical status
     * @param enable
     */
    void changeHistoricalStatus(boolean enable);

    /**
     * Provides the est35a gross amount
     * @return deEst35a
     */
    String getDeEst35a();

    /**
     * Sets the est35a gross amount
     * @param deEst35a
     */
    void setDeEst35a(String deEst35a);

    /**
     * Provides the est35a tax rate percent
     * @return deEst35aTax
     */
    String getDeEst35aTax();

    /**
     * Sets the est35a tax rate percent
     * @param deEst35aTax
     */
    void setDeEst35aTax(String deEst35aTax);
}
