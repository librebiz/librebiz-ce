<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<c:if test="${!empty sessionScope.objectLock}">
<c:set var="lock" value="${sessionScope.objectLock}"/>
<div class="recordLabelValue">
	<span><fmt:message key="objectLocked"/>: 
		<fmt:message key="${lock.key}"/>
		/ <o:date value="${lock.lockTime}" addtime="true"/>
		/ <o:ajaxLink linkId="lockedBy" url="${'/employeeInfo.do?id='}${lock.lockedBy}"><oc:employee value="${lock.lockedBy}"/></o:ajaxLink>
	</span>
</div>
<c:remove var="objectLock" scope="session"/>
</c:if>
