SET search_path = osserp, pg_catalog;

INSERT INTO system_properties VALUES (234, 'popupPageSize', 'default', 'java.lang.String', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (235, 'baseTagEnabled', 'false', 'java.lang.String', false, false, NULL, false, 0);
INSERT INTO system_properties VALUES (236, 'voucherExportSortReverse', 'true', 'java.lang.Boolean', true, false, NULL, false, 0);
SELECT pg_catalog.setval('system_properties_id_seq', 236, true);

UPDATE system_properties SET pvalue = '0.15.19', ptype = 'java.lang.String' WHERE name = 'dbmsSchemaVersion';
