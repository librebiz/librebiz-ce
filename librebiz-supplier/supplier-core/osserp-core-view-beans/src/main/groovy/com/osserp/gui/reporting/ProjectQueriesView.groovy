/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Jul 5, 2011 3:04:11 PM 
 * 
 */
package com.osserp.gui.reporting

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.util.DateUtil

import com.osserp.core.sales.SalesVolumeReport
import com.osserp.core.sales.SalesVolumeReportService

import com.osserp.groovy.web.MenuItem

/**
 *
 * @author eh <eh@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public class ProjectQueriesView extends AbstractSalesListReportingView {
    private static Logger logger = LoggerFactory.getLogger(ProjectQueriesView.class.getName())

    Map types = [
        month: [ header: 'inCurrentMonth', monthly: true ],
        monthCreated: [ header: 'inCurrentMonthCreated', monthly: true ],
        monthUnreferenced: [ header: 'inCurrentMonthUnreferenced', monthly: true ],
        monthUnreferencedInvoice: [ header: 'inCurrentMonthUnreferencedInvoice', monthly: true ],
        monthUnreferencedCredit: [ header: 'inCurrentMonthUnreferencedCredit', monthly: true ],
        year: [ header: 'inCurrentYear' ],
        yearCreated: [ header: 'inCurrentYearCreated' ],
        yearUnreferenced: [ header: 'inCurrentYearUnreferenced'],
        yearUnreferencedInvoice: [ header: 'inCurrentYearUnreferencedInvoice'],
        yearUnreferencedCredit: [ header: 'inCurrentYearUnreferencedCredit'],
        cancelled: [ header: 'inCurrentYearCancelled' ],
        stopped: [ header: 'inCurrentYearStopped' ],
    ]

    List reports = []
    SalesVolumeReport report = null

    boolean currentListMonthly = false

    ProjectQueriesView() {
        super()
        env.listMode = false
        env.listUnreferencedMode = false
        values.closedIgnore = false
        closedIgnoreLinkAvailable = true
        selectors.status = true
        selectors.businessType = 'salesMonitoringProjects'
        selectors.branch = 'projectAwareBranchsSelection'
        refreshLink = true
        exportLink = true
    }

    @Override
    void createCustomNavigation() {
        super.createCustomNavigation()
        if (report?.current) {
            if (user?.startup?.key == 'orderBacklog') {
                nav.defaultNavigation = [ previousMonthLink, nextDisabledIcon ]
            } else {
                nav.defaultNavigation = [ previousMonthLink, nextDisabledIcon, exitLink, homeLink ]
            }
        } else {
            if (user?.startup?.key == 'orderBacklog') {
                nav.defaultNavigation = [ previousMonthLink, nextMonthLink ]
            } else {
                nav.defaultNavigation = [ previousMonthLink, nextMonthLink, exitLink, homeLink ]
            }
        }
        if (listUnreferencedMode) {
            nav.listNavigation = [ exitListLink, homeLink ]
        } else {
            nav.listNavigation = [ exitListLink ] + nav.listNavigation[1..-1]
        }
        if ((listMode || listUnreferencedMode) && !list) {
            nav.defaultNavigation = nav.listNavigation
        }
    }

    MenuItem getPreviousMonthLink() {
        navigationLink("/${context.name}/loadPreviousMonth", 'leftIcon', 'previousMonth')
    }
    
    MenuItem getNextMonthLink() {
        navigationLink("/${context.name}/loadNextMonth", 'rightIcon', 'nextMonth')
    }
    
    MenuItem getNextDisabledIcon() {
        new MenuItem(icon: 'rightDisabledIcon', iconOnly: true, title: 'endOfListTitle')
    }

    MenuItem getExitListLink() {
        navigationLink("/${context.name}/exitList", 'backIcon', 'backToLast')
    }

    @Override
    MenuItem getSalesListExportLink() {
        MenuItem link = super.getSalesListExportLink()
        link.permissions = 'executive,executive_tecstaff,executive_sales,executive_branch'
        link.permissionInfo = 'permissionExportAsTable'
        return link
    }

    @Override
    protected void init() {
        if (forwardRequest) {
            Date currentDate = DateUtil.getCurrentDate()
            int currentMonth = DateUtil.getMonth(currentDate)
            int currentYear = DateUtil.getYear(currentDate)
            createReport(currentMonth, currentYear)
        }
    }

    @Override
    void loadList() {
        if (report && listMode && !exitListRequest && values.type && types.containsKey(values.type)) {
            list = report."${values.type}List"
            logger.debug("loadList: list fetched from report [size=${list.size()}, type=${values.type}]")
            if ('cancelled' == values.type || 'stopped' == values.type) {
                ignoreStatus = true
            }
            headerName = types[values.type].header
            if (values.confirmed == false) {
                headerName += 'Unconfirmed'
            }
            currentListMonthly = types[values.type].monthly
        } else {
            logger.debug("loadList: nothing to do [type=${values.type}]")
        }
    }

    @Override
    protected void postFilter() {
        nav.listNavigation = null
        generateNavigation()
    }
    
    void forwardList() {
        values.type = form.getString('type') ?: values.type
        env.listMode = true
        logger.debug("forwardList: invoked [type=${values.type}]")
        reload()
    }

    void forwardListUnreferenced() {
        values.type = form.getString('type') ?: values.type
        env.listUnreferencedMode = true
        logger.debug("forwardListUnreferenced: invoked [type=${values.type}]")
        if (report && !exitListRequest && values.type && types.containsKey(values.type)) {
            list = report."${values.type}List"
            logger.debug("forwardListUnreferenced: list fetched from report [size=${list.size()}]")
            headerName = types[values.type].header
            currentListMonthly = types[values.type].monthly
        } else {
            logger.debug("forwardListUnreferenced: nothing to do [type=${values.type}]")
        }
        generateNavigation()
    }
    
    void exitList() {
        resetValues()
        currentListMonthly = false
        list = []
        env.listMode = false
        env.listUnreferencedMode = false
        nav.listNavigation = null
        generateNavigation()
    }
    
    boolean isListMode() {
        env.listMode
    }

    boolean isListUnreferencedMode() {
        env.listUnreferencedMode
    }

    private boolean isExitListRequest() {
        requestContext.method == 'exitList'
    }

    void loadPreviousMonth() {
        if (report.month == 0) {
            createReport(11, report.year - 1)
        } else {
            createReport(report.month - 1, report.year)
        }
        createCustomNavigation()
    }

    void loadNextMonth() {
        if (!report.current) {
            if (report.month == 11) {
                createReport(0, report.year + 1)
            } else {
                createReport(report.month + 1, report.year)
            }
        }
        createCustomNavigation()
    }

    private void createReport(int month, int year) {
        logger.debug("createReport: invoked [month=${month}, year=${year}]")
        SalesVolumeReport temp = reports.find { !it.current && it.year == year && it.month == month }
        if (temp) {
            report = temp
            logger.debug("createReport: using cached [report=${report.monthDisplay}/${report.year}]")
        } else {
            report = salesVolumeReportService.getReport(domainUser, month, year)
            reports << report
            logger.debug("createReport: added report [report=${report.monthDisplay}/${report.year}]")
        }
    }

    private SalesVolumeReportService getSalesVolumeReportService() {
        getService(SalesVolumeReportService.class.getName())
    }
}
