/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 09-Nov-2006 20:50:46 
 * 
 */
package com.osserp.core.model;

import com.osserp.common.ActionTarget;
import com.osserp.common.Constants;
import com.osserp.common.Property;
import com.osserp.common.User;
import com.osserp.common.UserPermission;
import com.osserp.common.beans.AbstractUser;

import com.osserp.core.contacts.Contact;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class UserImpl extends AbstractUser implements User {
    private static final String SETUP_ADMIN = "osserp-setup"; 

    private Long defaultFcsGroup = null;

    /**
     * Default constructor. Required to implement the serializable interface
     * 
     */
    protected UserImpl() {
        super();
    }

    /**
     * Constructor for derived classes
     * 
     */
    protected UserImpl(User user) {
        super((AbstractUser) user);
        if (user instanceof UserImpl) {
            UserImpl domainUser = (UserImpl) user;
            defaultFcsGroup = domainUser.getDefaultFcsGroup();
        }
    }

    /**
     * Creates a new user by minimal required values
     * @param id
     * @param createdBy
     * @param contact
     * @param loginName
     * @param startup
     */
    public UserImpl(
            Long id,
            Long createdBy,
            Contact contact,
            String loginName,
            ActionTarget startup) {
        super(id, createdBy, (contact == null ? null : contact.getContactId()), loginName, startup);
    }

    protected Long getDefaultFcsGroup() {
        return defaultFcsGroup;
    }

    protected void setDefaultFcsGroup(Long defaultFcsGroup) {
        this.defaultFcsGroup = defaultFcsGroup;
    }

    @Override
    protected UserPermission createPermission(Long grantBy, String permission) {
        return new UserPermissionImpl(getId(), grantBy, permission);
    }

    @Override
    protected Property createProperty(String name, String value) {
        return new UserPropertyImpl(this, name, value);
    }

    @Override
    public final boolean isSetupAdmin() {
        return SETUP_ADMIN.equals(getLdapUid());
    }

    @Override
    public boolean isAdmin() {
        return Constants.SYSTEM_USER.equals(getId());
    }

    @Override
    public Object clone() {
        return new UserImpl(this);
    }
}
