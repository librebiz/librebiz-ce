/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on May 5, 2010 at 2:40:18 PM 
 * 
 */
package com.osserp.gui.records


import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.groovy.web.ViewContextException
import com.osserp.core.finance.RecordSearch
import com.osserp.gui.CoreView

/**
 * 
 * @author jg <jg@osserp.com>
 *
 */
class DeliveryDateHistoryView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(DeliveryDateHistoryView.class.getName())

    def record
    def product
    def id

    DeliveryDateHistoryView() {
        super()
        enablePopupView()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        id = null
        record = false
        product = false
        if (form.getLong("record")) {
            id = form.getLong("record")
            list = recordSearch.findDeliveryDateHistoryByRecord(id)
            record = true
            logger.debug("initRequest: loaded delivery date history for sale order [id=$id]")
        } else if (form.getLong("product")) {
            id = form.getLong("product")
            list = recordSearch.findDeliveryDateHistoryByProduct(id)
            product = true
            logger.debug("initRequest: loaded delivery date history for product [id=$id]")
        } else {
            logger.error("initRequest: no correct parameter found")
        }
    }
    
    protected RecordSearch getRecordSearch() {
        getService(RecordSearch.class.getName())
    }
}

