<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="calculationView"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="title"><o:displayTitle/></tiles:put>

	<tiles:put name="headline">
		<fmt:message key="${view.headerName}"/><c:if test="${view.editMode}"> <fmt:message key="change"/></c:if> : <o:out value="${view.calculation.id}"/> - <o:out value="${view.calculation.name}"/>
	</tiles:put>

	<tiles:put name="headline_right">
        <v:navigation/>
	</tiles:put>

	<tiles:put name="content" type="string">
		<div class="subcolumns">
			<div class="subcolumn">
				<div class="spacer"></div>
				<c:choose>
					<c:when test="${view.itemChangeMode}">
						<c:import url="_partlistItemChange.jsp"/>
					</c:when>
					<c:when test="${view.editMode}">
						<c:import url="_partlistEdit.jsp"/>
					</c:when>
					<c:when test="${view.bean != null}">
						<c:import url="_partlistDisplay.jsp"/>
					</c:when>
					<c:when test="${!empty view.list}">
						<table class="table">
							<thead>
								<tr>
									<th><fmt:message key="productid"/></th>
									<th><fmt:message key="label"/></th>
									<th><fmt:message key="quantity"/></th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="item" items="${view.list}">
									<tr>
										<td><o:out value="${item.product.productId}"/></td>
										<td><a href="<v:url value="/calculations/partlist/select?id=${item.id}"/>"><o:out value="${item.product.name}"/></a></td>
										<td><o:out value="${item.quantity}"/></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</c:when>
					<c:otherwise>
						<div class=".errormessage">
							<fmt:message key="partlist.noProducts"/>
						</div>
					</c:otherwise>
				</c:choose>
				<div class="spacer"></div>
			</div>
		</div>

	</tiles:put>
</tiles:insert>