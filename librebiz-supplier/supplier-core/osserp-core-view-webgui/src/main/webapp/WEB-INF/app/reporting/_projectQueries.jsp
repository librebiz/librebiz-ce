<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<v:view viewName="projectQueriesView" />

<div class="col-md-6 panel-area panel-area-default">

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="queriesAndSummaries" />
            </h4>
        </div>
    </div>
    <div class="panel-body">

        <o:permission role="technics,sales,customer_service,executive,executive_tecstaff,executive_branch,purchasing,project_monitoring_service" info="permissionViewProjectMonitoringCheck">
            <table class="table table-striped">
                <o:permission role="technics,sales,customer_service,executive,executive_tecstaff,executive_branch,purchasing" info="permissionViewProjectMonitoring">
                    <tr>
                        <td><v:link url="/reporting/projectMonitoring/forward?selection=salesMonitoringProjects&exit=${view.baseLink}/reload">
                                <fmt:message key="projectMonitoring" />
                            </v:link></td>
                    </tr>
                </o:permission>
                <o:permission role="open_project_list,executive,executive_tecstaff,executive_branch" info="permissionViewProjectList">
                    <tr>
                        <td><a href="<v:url value="/reporting/salesProcess/forward?exit=${view.baseLink}/reload&company=100"/>"><fmt:message key="salesMonitoringLabel" /></a></td>
                    </tr>
                </o:permission>
            </table>
        </o:permission>

        <o:permission role="projects_by_employee,open_project_list,customer_service,accounting,executive,executive_tecstaff,executive_branch" info="permissionViewProjectsByEmployee">
            <table class="table table-striped">
                <o:permission role="projects_by_employee,executive,executive_tecstaff,executive_branch" info="permissionViewProjectsByEmployee">
                    <tr>
                        <td><v:link url="/selections/employeeCapacitySelection/forward" parameters="selectionTarget=/reporting/projectsByEmployee/forward&inactive=true&mode=sales&exit=${view.baseLink}/reload">
                                <fmt:message key="ordersBySalesperson" />
                            </v:link></td>
                    </tr>
                    <tr>
                        <td><v:link url="/selections/employeeCapacitySelection/forward" parameters="selectionTarget=/reporting/projectsByEmployee/forward&inactive=true&mode=manager&exit=${view.baseLink}/reload">
                                <fmt:message key="ordersByProjector" />
                            </v:link></td>
                    </tr>
                </o:permission>
                <o:permission role="projects_by_employee,open_project_list,customer_service,accounting,executive,executive_tecstaff,executive_branch" info="permissionViewProjectsByEmployee">
                    <tr>
                        <td><v:link url="/suppliers/subcontractorSelection/forward" parameters="selectionTarget=/reporting/projectsBySupplier/forward&exit=${view.baseLink}/reload">
                                <fmt:message key="ordersBySupplier" />
                            </v:link></td>
                    </tr>
                </o:permission>
            </table>
        </o:permission>

        <o:permission role="accounting,customer_service,executive,executive_tecstaff,executive_branch" info="permissionViewProjectsByActionSleeping">
            <table class="table table-striped">
                <tr>
                    <td><a href="<c:url value="/projectsByAction.do?method=forward"/>"><fmt:message key="ordersAfterFcsAction" /></a></td>
                </tr>
                <tr>
                    <td><v:link url="/reporting/sleepingProjects/forward?exit=/reporting/projectQueries/reload">
                            <fmt:message key="ordersAfterLastAction" />
                        </v:link></td>
                </tr>
            </table>
        </o:permission>

        <table class="table table-striped">
            <o:permission role="technics,sales,customer_service,open_project_list,executive,executive_tecstaff,executive_branch" info="businessCaseReportViewHeader">
                <tr>
                    <td><v:link url="/reporting/businessCaseReport/forward?exit=/reporting/projectQueries/reload">
                            <fmt:message key="businessCaseReportViewHeader" />
                        </v:link>
                    </td>
                </tr>
            </o:permission>
            <o:permission role="open_project_list,executive,executive_branch" info="permissionViewSalesReceivables">
                <tr>
                    <td>
                        <v:link url="/sales/salesReceivables/forward?exit=/reporting/projectQueries/reload">
                            <fmt:message key="ordersWithOutstandingPayments" />
                        </v:link>
                    </td>
                </tr>
            </o:permission>
            <tr>
                <td><a href="<c:url value="/salesPlanning.do?method=forward&exit=projectQueries&unreleased=false&stockId=100"/>"> <fmt:message key="ordersWithOpenDeliveries" />
                </a></td>
            </tr>
            <o:permission role="event_escalations,global_business_case_access" info="permissionViewEventEscalations">
                <tr>
                    <td><a href="<c:url value="/salesEventEscalations.do?method=forward&exit=projectQueries"/>"> <fmt:message key="ordersWithEscalation" />
                    </a></td>
                </tr>
            </o:permission>
            <tr>
                <td><a href="<c:url value="/salesPlanning.do?method=forward&exit=projectQueries&unreleased=true&stockId=100"/>"> <fmt:message key="ordersWithoutRelease" />
                </a></td>
            </tr>
            <tr>
                <td><a href="<c:url value="/salesDeliveryUnreleased.do?method=forward&exit=projectQueries"/>"> <fmt:message key="deliveryNotesWithoutRelease" />
                </a></td>
            </tr>
        </table>

        <table class="table table-striped">
            <oc:systemPropertyEnabled name="projectSupplierRelationSupport">
                <tr>
                    <td><v:link url="/projects/mountingCalendar/forward?exit=/reporting/projectQueries/reload"><fmt:message key="mountingCalendar" /></v:link></td>
                </tr>
            </oc:systemPropertyEnabled>
            <tr>
                <td><v:link url="/sales/salesDeliveryCalendar/forward?exit=/reporting/projectQueries/reload"><fmt:message key="deliveryCalendar" /></v:link></td>
            </tr>
            <o:permission role="executive,executive_tecstaff,executive_branch,installation,customer_service" info="permissionViewSalesDeliveryMonitoring">
                <tr>
                    <td><a href="<c:url value="/salesDeliveryMonitoring.do?method=forward"/>"><fmt:message key="monitoringOfDatesDelivery" /></a></td>
                </tr>
            </o:permission>
            <o:permission role="executive,executive_tecstaff,executive_branch,customer_service" info="permissionViewSalesBillingMonitoring">
                <tr>
                    <td><a href="<c:url value="/salesBillingMonitoring.do?method=forward"/>"><fmt:message key="monitoringOfBilling" /></a></td>
                </tr>
            </o:permission>
        </table>

    </div>

</div>
