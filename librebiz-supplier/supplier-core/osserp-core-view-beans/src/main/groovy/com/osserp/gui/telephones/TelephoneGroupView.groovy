/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.osserp.gui.telephones

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.PermissionException

import com.osserp.core.telephone.TelephoneGroupManager
import com.osserp.core.telephone.TelephoneSystem
import com.osserp.core.telephone.TelephoneSystemManager

import com.osserp.groovy.web.MenuItem

import com.osserp.gui.CoreView

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public class TelephoneGroupView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(TelephoneGroupView.class.getName())

    private static final String TELEPHONE_GROUP_EDIT_PERMISSIONS = 'telephone_system_admin'
    Long telephoneSystem
    List<TelephoneSystem> telephoneSystems

    TelephoneGroupView() {
        super()
        providesCreateMode()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            TelephoneSystemManager manager = getService(TelephoneSystemManager.class.getName())
            telephoneSystems = manager.getAll()
            reload()
        }
    }

    @Override
    MenuItem getCreateLink() {
        return new MenuItem(link: "/telephones/telephoneGroupCreate/forward", title: 'createNewTelephoneGroup', icon: 'smallNewIcon', newWindow: false, ajaxPopup: true, ajaxPopupName: 'telephoneGroupCreateView', permissions: TELEPHONE_GROUP_EDIT_PERMISSIONS, permissionInfo: 'permissionTelephoneSystemAdmin')
    }

    @Override
    void reload() {
        TelephoneGroupManager manager = getService(TelephoneGroupManager.class.getName())
        if (telephoneSystem) {
            list = manager.findByTelephoneSystem(telephoneSystem)
        } else {
            list = manager.getAll()
        }
        if (actualSortKey != null) {
            String[] keys = actualSortKey.split("\\.")
            def key = keys[0]
            if (keys.length > 1) {
                def childKey = keys[1]
                list.sort{ a, b -> a?."$key"?."$childKey" <=> b?."$key"?."$childKey" }
            } else {
                list.sort{ a, b -> a?."$key" <=> b?."$key" }
            }
        }
    }

    void search() throws PermissionException {
        logger.debug('search() invoked...')
        checkPermission(TELEPHONE_GROUP_EDIT_PERMISSIONS)
        telephoneSystem = form.getLong('system')
        TelephoneGroupManager manager = getService(TelephoneGroupManager.class.getName())
        if (telephoneSystem) {
            list = manager.findByTelephoneSystem(telephoneSystem)
        } else {
            list = manager.getAll()
        }
    }

    void delete() throws PermissionException {
        checkPermission(TELEPHONE_GROUP_EDIT_PERMISSIONS)
        Long id = form.getLong('id')
        if (id) {
            TelephoneGroupManager manager = getService(TelephoneGroupManager.class.getName())
            manager.delete(id)
            reload()
        }
    }
}
