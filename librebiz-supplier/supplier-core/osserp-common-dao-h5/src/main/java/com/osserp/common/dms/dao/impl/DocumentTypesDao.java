/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 13, 2006 7:32:01 PM 
 * 
 */
package com.osserp.common.dms.dao.impl;

import java.sql.Types;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.dao.AbstractHibernateAndSpringDao;
import com.osserp.common.dao.Tables;
import com.osserp.common.dms.DocumentType;
import com.osserp.common.dms.dao.DocumentTypes;
import com.osserp.common.dms.model.DocumentTypeImpl;

/**
 * 
 * Provides the document types supported by application.
 * 
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class DocumentTypesDao extends AbstractHibernateAndSpringDao implements DocumentTypes {
    private static Logger log = LoggerFactory.getLogger(DocumentTypesDao.class.getName());

    /**
     * Provides documents count. <br/>
     * Params: <br/>
     * reference bigint <br/>
     * typeid bigint
     */
    private static final String GET_DOC_COUNT_BY_REFERENCE = "SELECT get_doc_count_by_reference(?, ?)";

    public DocumentTypesDao(JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
    }

    @Override
    public List<DocumentType> getList() {
        return getCurrentSession().createQuery(
                "from " + DocumentTypeImpl.class.getName() + " dt order by dt.contextPath").list();
    }

    public DocumentType get(Long id) {
        return (DocumentType) getCurrentSession().load(DocumentTypeImpl.class.getName(), id);
    }

    public Long countByReference(Long id, Long reference) {
        if (id == null || reference == null) {
            return null;
        }
        log.debug("countByReference() invoked [id=" + id + ", reference=" + reference + "]");
        final Object[] params = {
                id,
                reference };
        final int[] types = {
                Types.BIGINT,
                Types.BIGINT };
        return jdbcTemplate.queryForObject(
                GET_DOC_COUNT_BY_REFERENCE,
                params,
                types,
                Long.class);
    }

    public DocumentType toggleDownloadName(DocumentType type) {
        type.toggleFilenameForDownload();
        return (DocumentType) getCurrentSession().merge(DocumentTypeImpl.class.getName(), type);
    }

    public DocumentType updateFormat(DocumentType documentType, int format) {
        documentType.setPersistenceFormat(format);
        documentType.setLocked(true);
        return (DocumentType) getCurrentSession().merge(DocumentTypeImpl.class.getName(), documentType);
    }

    public DocumentType unlock(DocumentType documentType) {
        documentType.setLocked(false);
        return (DocumentType) getCurrentSession().merge(DocumentTypeImpl.class.getName(), documentType);
    }
}
