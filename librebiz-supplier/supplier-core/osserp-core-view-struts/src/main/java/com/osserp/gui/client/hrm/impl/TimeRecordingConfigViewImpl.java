/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 14, 2008 3:03:55 PM 
 * 
 */
package com.osserp.gui.client.hrm.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;
import com.osserp.common.web.ViewName;
import com.osserp.core.hrm.TimeRecordingConfig;
import com.osserp.core.hrm.TimeRecordingConfigManager;
import com.osserp.gui.client.hrm.TimeRecordingConfigView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("timeRecordingConfigView")
public class TimeRecordingConfigViewImpl extends AbstractTimeRecordingConfigView implements TimeRecordingConfigView {

    protected TimeRecordingConfigViewImpl() {
        super();
    }

    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        TimeRecordingConfigManager manager = getTimeRecordingConfigManager();
        setList(manager.findConfigs());
    }

    @Override
    public void save(Form form) throws ClientException, PermissionException {
        if (isCreateMode()) {
            this.create(form);
        } else {
            update(form);
        }
    }

    private void create(Form form) throws ClientException, PermissionException {
        String name = form.getString(Form.NAME);
        String description = form.getString(Form.DESCRIPTION);
        TimeRecordingConfigManager manager = getTimeRecordingConfigManager();
        TimeRecordingConfig config = manager.createConfig(getDomainEmployee(), name, description);
        setList(manager.findConfigs());
        setSelection(config.getId());
        disableCreateMode();
        setEditMode(true);
    }

    private void update(Form form) throws ClientException, PermissionException {
        TimeRecordingConfig config = getTimeRecordingConfig();
        if (config != null) {
            TimeRecordingConfigManager manager = getTimeRecordingConfigManager();
            String name = form.getString(Form.NAME);
            if (isNotSet(name)) {
                throw new ClientException(ErrorCode.NAME_MISSING);
            }
            if (!config.isDefaultConfig() && !config.getName().equals(name)) {
                List<TimeRecordingConfig> existing = manager.findConfigs();
                boolean exists = false;
                for (int i = 0, j = existing.size(); i < j; i++) {
                    TimeRecordingConfig next = existing.get(i);
                    if (!next.getId().equals(config.getId())) {
                        if (next.getName().equalsIgnoreCase(name)) {
                            exists = true;
                            break;
                        }
                    }
                }
                if (!exists) {
                    config.setName(name);
                }
            }
            String description = form.getString(Form.DESCRIPTION);
            if (isNotSet(description)) {
                throw new ClientException(ErrorCode.DESCRIPTION_MISSING);
            }
            config.setDescription(description);
            config.setAnnualLeave(form.getDouble("annualLeave"));
            config.setCarryoverHoursMax(form.getDouble("carryoverHoursMax"));
            config.setCoreTimeEndHours(form.getIntVal("coreTimeEndHours"));
            config.setCoreTimeEndMinutes(form.getIntVal("coreTimeEndMinutes"));
            config.setCoreTimeStartHours(form.getIntVal("coreTimeStartHours"));
            config.setCoreTimeStartMinutes(form.getIntVal("coreTimeStartMinutes"));
            config.setHoursDaily(form.getDouble("hoursDaily"));
            config.setRecordingHours(form.getBoolean("recordingHours"));
            manager.save(getDomainEmployee(), config);
        }
        setEditMode(false);
    }

    protected TimeRecordingConfig getTimeRecordingConfig() {
        return (TimeRecordingConfig) getBean();
    }
}
