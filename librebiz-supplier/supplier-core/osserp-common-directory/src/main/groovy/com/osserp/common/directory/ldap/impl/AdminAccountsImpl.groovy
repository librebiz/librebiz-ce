/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 29, 2013
 *  
 */
package com.osserp.common.directory.ldap.impl

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.apache.directory.groovyldap.LDAP
import org.apache.directory.groovyldap.SearchScope

import com.osserp.common.directory.ldap.LdapConfig
import com.osserp.common.directory.ldap.LdapException
import com.osserp.common.directory.ldap.StructuralSchema
import com.osserp.common.directory.ldap.dao.AdminAccounts
import com.osserp.common.util.NumberUtil
import com.osserp.common.util.SecurityUtil

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class AdminAccountsImpl extends AbstractLdapDao implements AdminAccounts {
    private static Logger logger = LoggerFactory.getLogger(AdminAccountsImpl.class.getName())

    StructuralSchema schema

    protected AdminAccountsImpl() {
        super()
    }

    AdminAccountsImpl(LdapConfig configInstance) {
        super(configInstance)
        schema = new AdminAccountsSchema(configInstance)
    }

    List<Map> findAll(String client) {
        LDAP ldap = createConnection()
        ldap.search(schema.query, createBaseDn(client), SearchScope.SUB)
    }

    Map find(String uid, String client) {
        LDAP ldap = createConnection()
        def dn = createDn(uid, client)
        if (ldap.exists(dn)) {
            return ldap.read(dn)
        }
        List<Map> all = []
        if (client) {
            all = findAll(client)
        } else {
            all = findAll(null)
        }
        if (all) {
            Map result = [:]
            all.each {
                if (it.uid == uid || it.cn == uid) {
                    result = ldap.read(it.dn)
                }
            }
            return result
        }
        return [:]
    }

    Map create(Map attrs, String client) {
        if (!attrs.uid) {
            attrs.errors = 'error.uid.required'
            return attrs
        }
        def dn = createDn(attrs.uid, client)
        LDAP ldap = createConnection()
        if (ldap.exists(dn)) {
            attrs.errors = 'error.uid.exists'
            return attrs
        }
        if (!attrs.password) {
            attrs.errors = 'error.password.required'
            return attrs
        }

        def values = [
            uid: attrs.uid,
            userid: createUserid(client),
            password: attrs.uid
        ]
        ldap.add(dn, schema.create(values))
        def account = ldap.read(dn)
        return account
    }

    Map changePassword(Map account, String password) {
        LDAP ldap = createConnection()
        if (account && password) {
            def userPassword = SecurityUtil.createPassword(password).getBytes()
            def newValues = [ userpassword: userPassword ]
            ldap.modify(account.dn, 'REPLACE', newValues)
        }
        ldap.read(account.dn)
    }

    void delete(Map account) {
        LDAP ldap = createConnection()
        if (account?.dn && ldap.exists(account.dn)) {
            try {
                ldap.delete(account.dn)
            } catch (Throwable t) {
                println "delete: failed [message=${t.message}]"
            }
        }
    }

    Map login(String uid, String password, String client) throws LdapException {
        Map obj = find(uid, client)
        checkUserPassword(obj, password, null)
    }

    private String createUserid(String client) {
        Long adminStart = NumberUtil.createLong(config.adminIdStart)
        if (!adminStart) {
            adminStart = 1L
        }
        Long result = adminStart
        List<Map> existingAccounts = findAll(client)
        existingAccounts.each {
            Long existingId = NumberUtil.createLong(it.userid)
            if (existingId && existingId > result) {
                result = existingId
            }
        }
        if (result > adminStart) {
            return (result + 1L).toString()
        }
        result.toString()
    }

    @Override
    protected String createDn(String uid, String client) {
        return super.createDn(((client != null && config?.adminAccountsClientOu) ? "cn=${uid},ou=${config.adminAccountsClientOu}" : "uid=${uid}"), client)
    }
}
