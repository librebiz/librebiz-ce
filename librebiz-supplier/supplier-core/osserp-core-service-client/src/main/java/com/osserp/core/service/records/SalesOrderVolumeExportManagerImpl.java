/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 29, 2008 8:28:25 AM 
 * 
 */
package com.osserp.core.service.records;

import java.util.HashMap;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.util.CollectionUtil;
import com.osserp.core.dao.records.SalesOrderVolumeExportConfigs;
import com.osserp.core.dao.records.SalesOrderVolumeExports;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.model.records.SalesOrderVolumeExportOperationVO;
import com.osserp.core.sales.SalesOrderVolumeExportConfig;
import com.osserp.core.sales.SalesOrderVolumeExportManager;
import com.osserp.core.sales.SalesOrderVolumeExportOperation;
import com.osserp.core.service.impl.AbstractService;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesOrderVolumeExportManagerImpl extends AbstractService implements SalesOrderVolumeExportManager {
    private SalesOrderVolumeExportConfigs exportConfigs = null;
    private HashMap<String, SalesOrderVolumeExports> exportDaos = new HashMap<String, SalesOrderVolumeExports>();

    protected SalesOrderVolumeExportManagerImpl(
            SalesOrderVolumeExportConfigs exportConfigs,
            HashMap<String, SalesOrderVolumeExports> exports) {
        super();
        this.exportDaos = exports;
        this.exportConfigs = exportConfigs;
    }

    public List<SalesOrderVolumeExportConfig> findConfigs() {
        return exportConfigs.getConfigs();
    }

    public SalesOrderVolumeExportConfig findConfig(Long id) {
        List<SalesOrderVolumeExportConfig> list = findConfigs();
        return (SalesOrderVolumeExportConfig) CollectionUtil.getById(list, id);
    }

    public SalesOrderVolumeExportOperation getOperation(Employee user, SalesOrderVolumeExportConfig config) throws ClientException {
        SalesOrderVolumeExports exports = getExports(config);
        SalesOrderVolumeExportOperation result = new SalesOrderVolumeExportOperationVO(config);
        result.populate(
                exports.getAvailableOrders(config),
                exports.getInvoiceArchiveCount(config),
                exports.getOpenInvoiceCount(config),
                exports.getUnreleasedOrderCount(config));
        return result;
    }

    public void refresh(SalesOrderVolumeExportOperation operation) throws ClientException {
        SalesOrderVolumeExportConfig config = operation.getConfig();
        SalesOrderVolumeExports exports = getExports(config);
        operation.populate(
                exports.getAvailableOrders(config),
                exports.getInvoiceArchiveCount(config),
                exports.getOpenInvoiceCount(config),
                exports.getUnreleasedOrderCount(config));
    }

    public List<Invoice> getInvoiceArchive(SalesOrderVolumeExportOperation operation) {
        SalesOrderVolumeExportConfig config = operation.getConfig();
        return getExports(config).getInvoiceArchive(config);
    }

    public List<Invoice> getOpenInvoices(SalesOrderVolumeExportOperation operation) {
        SalesOrderVolumeExportConfig config = operation.getConfig();
        return getExports(config).getOpenInvoices(config);
    }

    public List<RecordDisplay> getUnreleasedOrders(SalesOrderVolumeExportOperation operation) {
        SalesOrderVolumeExportConfig config = operation.getConfig();
        return getExports(config).getUnreleasedOrders(config);
    }

    private SalesOrderVolumeExports getExports(SalesOrderVolumeExportConfig config) {
        return exportDaos.get(config.getExportProviderName());
    }
}
