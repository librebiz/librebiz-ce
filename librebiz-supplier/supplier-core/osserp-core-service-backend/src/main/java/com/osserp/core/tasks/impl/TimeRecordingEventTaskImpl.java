/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.tasks.impl;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.Parameter;
import com.osserp.common.beans.ParameterImpl;
import com.osserp.common.service.impl.AbstractObjectMessageHandler;
import com.osserp.common.util.DateFormatter;
import com.osserp.core.dao.Employees;
import com.osserp.core.dao.EventConfigs;
import com.osserp.core.dao.Events;
import com.osserp.core.dao.SystemConfigs;
import com.osserp.core.dao.hrm.TimeRecordingApprovals;
import com.osserp.core.dao.hrm.TimeRecordingConfigs;
import com.osserp.core.dao.hrm.TimeRecordings;
import com.osserp.core.employees.Employee;
import com.osserp.core.employees.EmployeeDisciplinarian;
import com.osserp.core.events.EventAction;
import com.osserp.core.hrm.TimeRecord;
import com.osserp.core.hrm.TimeRecordType;
import com.osserp.core.hrm.TimeRecording;
import com.osserp.core.hrm.TimeRecordingApproval;
import com.osserp.core.hrm.TimeRecordingPeriod;
import com.osserp.core.tasks.Task;
import com.osserp.core.tasks.TimeRecordingEvent;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class TimeRecordingEventTaskImpl extends AbstractObjectMessageHandler implements Task {
    private static Logger log = LoggerFactory.getLogger(TimeRecordingEventTaskImpl.class.getName());

    private Employees employees;
    private TimeRecordings timeRecordings;
    private TimeRecordingApprovals timeRecordingApprovals;
    private TimeRecordingConfigs timeRecordingConfigs;
    private EventConfigs eventConfigs;
    private Events events;
    private SystemConfigs systemConfigs;

    /**
     * Creates a new time recording event task
     * @param employees
     * @param timeRecordings
     * @param timeRecordingApprovals
     * @param timeRecordingConfigs
     * @param events
     * @param eventConfigs
     * @param approvalCreatedId
     * @param approvalUpdateId
     */
    public TimeRecordingEventTaskImpl(
            Employees employees,
            TimeRecordings timeRecordings,
            TimeRecordingApprovals timeRecordingApprovals,
            TimeRecordingConfigs timeRecordingConfigs,
            Events events,
            EventConfigs eventConfigs,
            SystemConfigs systemConfigs) {
        super();
        this.employees = employees;
        this.timeRecordings = timeRecordings;
        this.timeRecordingApprovals = timeRecordingApprovals;
        this.timeRecordingConfigs = timeRecordingConfigs;
        this.eventConfigs = eventConfigs;
        this.events = events;
        this.systemConfigs = systemConfigs;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.common.service.impl.AbstractObjectMessageHandler#execute(java.lang.Object, byte[])
     */
    @Override
    protected void execute(Object object, byte[] file) {
        if (object instanceof TimeRecordingEvent) {
            TimeRecordingEvent event = (TimeRecordingEvent) object;
            TimeRecording timeRecording = timeRecordings.load(event.getTimeRecordingId());
            Employee employee = employees.get(timeRecording.getReference());
            Employee disciplinarian = null;
            if (employee.getDefaultDisciplinarian() != null) {
                disciplinarian = employees.get(employee.getDefaultDisciplinarian());
            } else {
                List<EmployeeDisciplinarian> disciplinarians = employees.findDisciplinarians(employee);
                if (!disciplinarians.isEmpty()) {
                    disciplinarian = disciplinarians.get(0).getDisciplinarian();
                    employee.setDefaultDisciplinarian(disciplinarian.getId());
                    this.employees.save(employee);
                    if (log.isDebugEnabled()) {
                        log.debug("execute() employee has no default disciplinarian, stored first found as default "
                                + "[employee=" + employee.getId()
                                + ", disciplinarian=" + disciplinarian.getId() + "]");
                    }
                }
            }
            TimeRecord record = null;
            TimeRecordType type = null;
            if (event.getTimeRecordId() != null) {
                for (int i = 0, j = timeRecording.getAvailablePeriods().size(); i < j; i++) {
                    if (record != null) {
                        break;
                    }
                    TimeRecordingPeriod nextPeriod = timeRecording.getAvailablePeriods().get(i);
                    for (int k = 0, l = nextPeriod.getRecords().size(); k < l; k++) {
                        TimeRecord nextRecord = nextPeriod.getRecords().get(k);
                        if (nextRecord.getId().equals(event.getTimeRecordId())) {
                            record = nextRecord;
                            type = record.getType();
                            break;
                        }
                    }
                }
            } else {
                type = timeRecordingConfigs.getType(event.getTimeRecordTypeId());
            }
            TimeRecordingApproval approval = null;
            EventAction eventAction = null;
            if (record != null) {
                // update event
                approval = timeRecordingApprovals.createApproval(
                        employee,
                        timeRecording,
                        record,
                        event.getStartDate(),
                        event.getStopDate(),
                        event.getNote());
                eventAction = getApprovalEvent(TimeRecordingApprovals.APPROVAL_EVENT_UPDATED);
                if (log.isDebugEnabled()) {
                    log.debug("execute() time recording approval created [id="
                            + approval.getId()
                            + ", event=" + eventAction.getId()
                            + ", name=" + eventAction.getName() + "]");
                }
            } else {
                // create event
                approval = timeRecordingApprovals.createApproval(
                        employee,
                        timeRecording,
                        type,
                        event.getStartDate(),
                        event.getStopDate(),
                        event.getNote());
                eventAction = getApprovalEvent(TimeRecordingApprovals.APPROVAL_EVENT_CREATED);
                if (log.isDebugEnabled()) {
                    log.debug("execute() time recording approval created [id="
                            + approval.getId()
                            + ", event=" + eventAction.getId()
                            + ", name=" + eventAction.getName() + "]");
                }
            }
            if (approval != null && disciplinarian != null && eventAction != null) {
                if (log.isDebugEnabled()) {
                    log.debug("execute() creating approval event [employee="
                            + employee.getId()
                            + ", disciplinarian="
                            + disciplinarian.getId()
                            + "]");
                }
                StringBuilder message = new StringBuilder();
                message
                        .append(DateFormatter.getDate(approval.getStartDate()))
                        .append(" - ")
                        .append(DateFormatter.getDate(approval.getEndDate()));
                List<Parameter> parameters = new java.util.ArrayList<Parameter>();
                parameters.add(new ParameterImpl("id", approval.getId().toString()));
                parameters.add(new ParameterImpl("exit", "/events/userEvents/forward"));
                this.events.createEvent(
                        eventAction,
                        new Date(System.currentTimeMillis()),
                        employee.getId(),
                        approval.getId(),
                        disciplinarian.getId(),
                        employee.getName(),
                        message.toString(),
                        null,
                        parameters);
            }
        }
    }

    private EventAction getApprovalEvent(String eventName) {
        Long eventId = systemConfigs.getSystemPropertyId(eventName);
        return (eventId == null ? null : eventConfigs.loadAction(eventId));
    }

}
