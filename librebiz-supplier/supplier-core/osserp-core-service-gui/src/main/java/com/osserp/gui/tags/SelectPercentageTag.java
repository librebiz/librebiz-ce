/**
 *
 * Copyright (C) 2011, 2014 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.osserp.gui.tags;

import java.util.List;

import com.osserp.common.Option;

import com.osserp.core.CoreServiceProvider;

/**
 * 
 * @author eh <eh@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public class SelectPercentageTag extends SelectTag {

    @Override
    protected String createOptions() {
        StringBuilder buffer = new StringBuilder();
        List<Option> optionList = CoreServiceProvider.getOptionList(
                getRequest(), options.toString());
        String df = fetchDefaultDouble().toString();
        for (int i = 0, j = optionList.size(); i < j; i++) {
            Option next = optionList.get(i);
            buffer
                    .append("<option value=\"")
                    .append(next.getName())
                    .append("\"");
            if (df != null && df.equals(next.getName())) {
                buffer.append(" selected=\"selected\"");
            }
            if (next.getId() != null) {
                buffer
                        .append(">")
                        .append(next.getId())
                        .append("%</option>");
            } else {
                buffer.append(">0%</option>");
            }
        }
        return buffer.toString();
    }

    private Double fetchDefaultDouble() {
        if (value != null) {
            if (value instanceof Double) {
                return (Double) value;
            }
            if (value instanceof Option) {
                return Double.valueOf(((Option) value).getId());
            }
        }
        return 0d;
    }
}
