/**
 *
 * Copyright (C) 2015 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 3, 2015 
 * 
 */
package com.osserp.core.service.doc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.xml.transform.Source;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jdom2.Document;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.FileObject;
import com.osserp.common.dms.DmsDocument;
import com.osserp.common.dms.DmsUtil;
import com.osserp.common.dms.TemplateDmsManager;
import com.osserp.common.service.TemplateManager;
import com.osserp.common.xml.JDOMUtil;

import com.osserp.core.BusinessTemplate;
import com.osserp.core.BusinessTemplateType;
import com.osserp.core.dao.BusinessTemplates;
import com.osserp.core.dao.SystemConfigs;
import com.osserp.core.dms.CoreStylesheetService;
import com.osserp.core.dms.TemplateDocument;
import com.osserp.core.service.impl.AbstractService;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class AbstractBusinessTemplateManager extends AbstractService {

    private static Logger log = LoggerFactory.getLogger(
            BusinessCaseTemplateManagerImpl.class.getName());
    private static final String ADD_UNASSIGNED_TEMPLATES = 
            "documentTemplateAddUnassigned"; 
            
    private BusinessTemplates businessTemplates;
    private CoreStylesheetService stylesheetService;
    private TemplateDmsManager templateDmsManager;
    private SystemConfigs systemConfigs;
    private boolean addUnassignedTemplates;

    /**
     * Creates a new business template manager
     * @param businessTemplates
     * @param documentDataProvider
     * @param stylesheetService
     * @param systemConfigs
     */
    protected AbstractBusinessTemplateManager(
            BusinessTemplates businessTemplates,
            CoreStylesheetService stylesheetService,
            TemplateDmsManager templateDmsManager,
            SystemConfigs systemConfigs) {
        super();
        this.businessTemplates = businessTemplates;
        this.stylesheetService = stylesheetService;
        this.templateDmsManager = templateDmsManager;
        this.systemConfigs = systemConfigs;
        try {
            addUnassignedTemplates = systemConfigs.isSystemPropertyEnabled(
                    ADD_UNASSIGNED_TEMPLATES);
        } catch (Exception e) {
            log.error("<init> ignoring failed attempt to initialize property [message=" 
                    + e.getMessage() + "]");
            log.warn("<init> stacktrace:\n", e);
        }
    }
    
    /**
     * Creates a pdf by template and document
     * @param businessTemplate
     * @param document 
     * @param branchId may be stricly required, depends on stylesheet
     * @param locale optional locale, using DEFAULT_LOCALE_OBJECT if missing 
     * @return
     */
    protected final byte[] createPdf(
            BusinessTemplate businessTemplate,
            Document document,
            Long branchId,
            Locale locale) {
        Source xsl = stylesheetService.getStylesheetSource(
                businessTemplate,
                branchId,
                (locale == null ? Constants.DEFAULT_LOCALE_OBJECT : locale));
        return stylesheetService.createPdf(document, xsl);
    }

    public TemplateDocument createTemplate(
            DomainUser user,
            String contextName,
            Long reference,
            Long branchId,
            String name,
            String description,
            String templateName,
            FileObject fileObject,
            Long categoryId)
            throws ClientException {

        BusinessTemplateType type = businessTemplates.getType(contextName);
        if (type == null) {
            throw new ClientException(ErrorCode.DOCUMENT_TYPE_CONFIG);
        }
        String realFilename = businessTemplates.createTemplateFilename(
                branchId, TemplateManager.createFilename(templateName));

        DmsDocument document = templateDmsManager.create(
                user, realFilename, fileObject, name, categoryId);

        BusinessTemplate template = businessTemplates.create(
                user.getId(),
                type,
                reference,
                branchId,
                name,
                description,
                document.getId());
        return new TemplateDocument(document, template);
    }

    public void deleteTemplate(DomainUser user, TemplateDocument document) throws ClientException {
        if (document.getTemplate() == null) {
            throw new ClientException(ErrorCode.TYPE_INVALID);
        }
        DmsDocument doc = templateDmsManager.getDocument(document.getId());
        templateDmsManager.delete(doc);
        businessTemplates.delete(document.getTemplate());
    }

    public BusinessTemplateType getType(String contextName) {
        return businessTemplates.getType(contextName);
    }

    public List<BusinessTemplate> findTemplates(String contextName, Long branch) {
        Long branchSelection = isSet(branch) ? branch : Constants.HEADQUARTER; 
        if (log.isDebugEnabled() && isNotSet(branch)) {
            log.debug("findTemplates() invoked without branch [contextName=" + contextName + "]");
        }
        List<BusinessTemplate> result = new ArrayList<>();
        List<BusinessTemplate> existing = findAllTemplates(contextName);
        for (int i = 0, j = existing.size(); i < j; i++) {
            BusinessTemplate next = existing.get(i);
            if (isNotSet(next.getBranchId()) && 
                    (addUnassignedTemplates 
                            || Constants.HEADQUARTER.equals(branchSelection))) {
                result.add(next);
                if (log.isDebugEnabled()) {
                    log.debug("findTemplates() added unassigned template [id=" + next.getId()
                            + ", contextName=" + contextName + ", branch=" + branch
                            + ", addUnassignedTemplates=" + addUnassignedTemplates
                            + ", defaultBranch=" + Constants.HEADQUARTER.equals(branchSelection)
                            + "]");
                }
            } else if (branchSelection.equals(next.getBranchId())) {
                result.add(next);
                if (log.isDebugEnabled()) {
                    log.debug("findTemplates() added assigned template [id=" + next.getId()
                            + ", contextName=" + contextName + ", branch=" + branchSelection + "]");
                }
            }
        }
        return result;
    }

    private List<BusinessTemplate> findAllTemplates(String contextName) {
        List<BusinessTemplate> result = new ArrayList<>();
        BusinessTemplateType type = getType(contextName);
        if (type != null) {
            result = businessTemplates.findByType(type.getId());
        } else {
            log.warn("findTemplates() type not found [contextName=" + contextName + "]");
        }
        return result;
    }

    public List<BusinessTemplate> findTemplates(BusinessTemplateType type, Long branch) {
        List<BusinessTemplate> result = new ArrayList<>();
        List<BusinessTemplate> existing = businessTemplates.findByType(type.getId());
        for (int i = 0, j = existing.size(); i < j; i++) {
            BusinessTemplate next = existing.get(i);
            if (isNotSet(branch)) {
                if (isNotSet(next.getBranchId())) {
                    result.add(next);
                }
            } else if (isNotSet(next.getBranchId()) || branch.equals(next.getBranchId())) {
                result.add(next);
            }
        }
        return result;
    }

    public List<TemplateDocument> getAllTemplates(String contextName) {
        List<TemplateDocument> result = new ArrayList<>();
        List<BusinessTemplate> list = findAllTemplates(contextName);
        if (!list.isEmpty()) {
            Map<Long, DmsDocument> docs = createDocumentMap();

            for (int i = 0, j = list.size(); i < j; i++) {

                BusinessTemplate next = list.get(i);
                if (isSet(next.getTemplateId())
                        && docs.containsKey(next.getTemplateId())) {

                    TemplateDocument tpl = new TemplateDocument(
                            docs.get(next.getTemplateId()), next);
                    result.add(tpl);
                }
            }
        }
        return new ArrayList(DmsUtil.sortByFilename(result));
    }

    public List<TemplateDocument> getTemplates(String contextName, Long branch) {
        List<TemplateDocument> result = new ArrayList<>();
        List<BusinessTemplate> list = findTemplates(contextName, branch);
        if (!list.isEmpty()) {
            Map<Long, DmsDocument> docs = createDocumentMap();

            for (int i = 0, j = list.size(); i < j; i++) {

                BusinessTemplate next = list.get(i);
                if (isSet(next.getTemplateId())
                        && docs.containsKey(next.getTemplateId())) {

                    TemplateDocument tpl = new TemplateDocument(
                            docs.get(next.getTemplateId()), next);
                    result.add(tpl);
                }
            }
        }
        return new ArrayList(DmsUtil.sortByFilename(result));
    }
    
    protected boolean propertyEnabled(String name) {
        return systemConfigs.isSystemPropertyEnabled(name);
    }
    
    protected String getProperty(String name) {
        return systemConfigs.getSystemProperty(name);
    }
    
    protected void logXML(Document document) {
        if (propertyEnabled("documentOutputDebugXML")) {
            if (log.isDebugEnabled()) {
                log.debug("logXML() invoked, data\n" + JDOMUtil.getPrettyString(document));
            } else if (log.isInfoEnabled()) {
                log.info("logXML() invoked, data\n" + JDOMUtil.getPrettyString(document));
            } else {
                log.warn("logXML() xml logging enabled while loglevel > info or disabled");
            }
        }
    }

    private Map<Long, DmsDocument> createDocumentMap() {
        Map<Long, DmsDocument> result = new HashMap<>();
        List<DmsDocument> docs = templateDmsManager.findDocuments();
        for (int i = 0, j = docs.size(); i < j; i++) {
            DmsDocument next = docs.get(i);
            result.put(next.getId(), next);
        }
        return result;
    }
}
