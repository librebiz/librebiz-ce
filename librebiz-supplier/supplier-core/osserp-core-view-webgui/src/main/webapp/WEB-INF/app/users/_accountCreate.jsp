<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="accountManagementView"/>

<c:import url="${viewdir}/users/_accountCreateHelp.jsp"/>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4><fmt:message key="userAccountCreateDirectory" /></h4>
        </div>
    </div>
    <div class="panel-body">
    
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="shortName"><fmt:message key="loginname" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <v:text styleClass="form-control" name="uid" value="${view.uid}" />
                </div>
            </div>
        </div>

        <div class="row next">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="name"><fmt:message key="password" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <v:text styleClass="form-control" name="newPassword" password="true" />
                </div>
            </div>
        </div>

        <div class="row next">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="name"><fmt:message key="repeatPassword" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <v:text styleClass="form-control" name="confirmPassword" password="true" />
                    <br/><br/>
                    
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="name"><fmt:message key="hint" /></label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <fmt:message key="passwordMustNotContainAnyUmlauts"/>!
                </div>
            </div>
        </div>

        <div class="row next">
            <div class="col-md-4"></div>
            <div class="col-md-3">
                <div class="form-group">
                    <v:submitExit url="/users/accountManagement/exit" />
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group">
                    <label for="placeholder"> </label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <o:submit styleClass="form-control" />
                </div>
            </div>
        </div>

    </div>
</div>
