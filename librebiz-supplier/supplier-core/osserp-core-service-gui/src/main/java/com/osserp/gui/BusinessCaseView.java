/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 31-Oct-2006 03:23:37 
 * 
 */
package com.osserp.gui;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;
import com.osserp.common.web.View;
import com.osserp.common.web.ViewName;

import com.osserp.core.BusinessCase;
import com.osserp.core.BusinessCaseAware;
import com.osserp.core.BusinessNote;
import com.osserp.core.BusinessTemplate;
import com.osserp.core.crm.Campaign;
import com.osserp.core.customers.Customer;
import com.osserp.core.events.Event;
import com.osserp.core.projects.ProjectSupplier;
import com.osserp.core.requests.Request;
import com.osserp.core.sales.Sales;
import com.osserp.core.system.BranchOffice;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("businessCaseView")
public interface BusinessCaseView extends BusinessCaseAware, View {
    public static final String CO_SALES = "coSales";
    public static final String MANAGER = "manager";
    public static final String OBSERVER = "observer";
    public static final String SALES = "sales";
    public static final String SUBSTITUTE = "substitute";

    /**
     * Provides the business case
     * @return businessCase
     */
    public BusinessCase getBusinessCase();

    /**
     * Provides sticky note if available
     * @return note or null
     */
    public BusinessNote getStickyNote();

    /**
     * Indicates that the view was invoked in customer context 
     * (e.g. exitTarget refers to customer display)
     * @return true on customer exit target
     */
    public boolean isCustomerContext();

    /**
     * Indicates that the view is in sales context
     * @return salesContext
     */
    public boolean isSalesContext();

    /**
     * Provides the id of the business case
     * @return id
     */
    public Long getId();

    /**
     * Provides the name of the business case
     * @return name
     */
    String getName();

    /**
     * Provides the sale when view is in sales level
     * @return sales
     */
    public Sales getSales();

    /**
     * Provides the request when view is not in sales level
     * @return request
     */
    public Request getRequest();

    /**
     * Provides the count of new and not assigned messages
     * @return message count
     */
    public int getFetchmailInboxCount();

    /**
     * Provides count of available documents uploaded in request context
     * @return request document count
     */
    public int getRequestDocumentCount();

    /**
     * Provides count of available pictures uploaded in request context
     * @return request picture count
     */
    public int getRequestPictureCount();

    /**
     * Provides count of available documents uploaded in sales context
     * @return sales document count
     */
    public int getSalesDocumentCount();

    /**
     * Provides count of available pictures uploaded in sales context
     * @return sales picture count
     */
    public int getSalesPictureCount();
    
    /**
     * Provides the summary count of all documents
     * @return documentCount
     */
    public int getDocumentCount();

    /**
     * Provides the letter count
     * @return letterCount
     */
    public int getLetterCount();
    
    /**
     * Provides the count of related records 
     * (offers or orders, depends on context)
     * @return the recordCount
     */
    public int getRecordCount();

    /**
     * Provides the count of invoices related to selected businessCase.
     * The counter is only set in sales context and includes the count
     * of downpayments. 
     * @return the invoiceCount
     */
    public int getInvoiceCount();

    /**
     * Provides the count of document templates
     * @return the template count
     */
    public int getTemplateCount();

    /**
     * Provides the cuount of available trackings 
     * @return count of trackings
     */
    public int getTrackingCount();

    /**
     * Provides related suppliers
     * @return suppliers
     */
    public List<ProjectSupplier> getSuppliers();

    /**
     * Provides available document templates
     * @return document templates
     */
    public List<BusinessTemplate> getDocumentTemplates();

    /**
     * Creates a document by template
     * @param templateId
     * @return pdf document as binary
     * @throws ClientException if context validation or other failed
     */
    public byte[] createDocument(Long templateId) throws ClientException;

    /**
     * Provides all appointments related to business case
     * @return appointments
     */
    public List<Event> getAppointments();

    /**
     * Closes an appointment
     * @param id
     */
    public void closeAppointment(Long id);

    /**
     * Provides all events related to business case
     * @return appointments
     * @return
     */
    public List<Event> getEvents();

    /**
     * Reloads all events and appointments of the business case
     */
    public void reloadEvents();

    /**
     * Indicates that view is in address edit mode
     * @return addressEditMode
     */
    public boolean isAddressEditMode();

    /**
     * Enables/diables address edit mode
     * @param addressEditMode
     */
    public void setAddressEditMode(boolean addressEditMode);

    /**
     * Updates the address values of current business case
     * @param form
     * @throws ClientException if validation of address values failed
     */
    public void updateAddress(Form form) throws ClientException;

    /**
     * Indicates availability of map based project address search
     * @return projectAddressMapSupport
     */
    public boolean isProjectAddressMapSupport();

    /**
     * Provides all branch offices
     * @return branchOffice
     */
    public List<BranchOffice> getBranchOffices();

    /**
     * Indicates that view is in branch edit mode
     * @return branchEditMode
     */
    public boolean isBranchEditMode();

    /**
     * Enables/disables branch edit mode
     * @param branchEditMode
     */
    public void setBranchEditMode(boolean branchEditMode);

    /**
     * Updates the branch office on current business case
     * @param form
     * @throws ClientException
     */
    public void updateBranch(Form form) throws ClientException;

    /**
     * Provides available sales agents
     * @return agents
     */
    public List<Customer> getAgents();

    /**
     * Provides the selected agent after invoking {@link #setAgentConfig(Long)}
     * @return selectedAgent
     */
    public Customer getSelectedAgent();

    /**
     * Indicates that agent config deleteMode is enabled (previously select id == 0)
     * @return agentDeleteMode
     */
    public boolean isAgentDeleteMode();

    /**
     * Sets current selected agent
     * @param agentId
     * @throws ClientException if validation of agent values failed
     */
    public void setAgentConfig(Long agentId) throws ClientException;

    /**
     * Updates the agent config
     * @param form
     * @throws ClientException if validation of agent values failed
     */
    public void updateAgentConfig(Form form) throws ClientException;

    /**
     * Updates customer
     * @param customer
     * @throws ClientException
     */
    public void updateCustomer(Long customerId) throws ClientException;

    /**
     * Provides available tip providers
     * @return tips
     */
    public List<Customer> getTips();

    /**
     * Provides the selected tip provider after invoking {@link #setTipConfig(Long)}
     * @return selectedTipProvider
     */
    public Customer getSelectedTipProvider();

    /**
     * Indicates that tip provider config deleteMode is enabled (previously select id == 0)
     * @return tipProviderDeleteMode
     */
    public boolean isTipProviderDeleteMode();

    /**
     * Sets current selected tip provider
     * @param tipProviderId
     * @throws ClientException if validation of tip provider values failed
     */
    public void setTipConfig(Long tipProviderId) throws ClientException;

    /**
     * Updates the tip config
     * @param form
     * @throws ClientException if validation of tip values failed
     */
    public void updateTipConfig(Form form) throws ClientException;

    /**
     * Indicates that view is in name edit mode
     * @return nameEditMode
     */
    public boolean isNameEditMode();

    /**
     * Enables/disables name edit mode
     * @param nameEditMode
     */
    public void setNameEditMode(boolean nameEditMode);

    /**
     * Updates the name of the business case
     * @param form
     * @throws ClientException if validation failed
     */
    public void updateName(Form form) throws ClientException;

    /**
     * Indicates that request origin was changed and history is available
     * @return originHistoryAvailable
     */
    public boolean isOriginHistoryAvailable();

    /**
     * Sets businessCase origin
     * @param campaign
     */
    public void setCampaign(Campaign campaign);

    // employee selections for business case work members

    /**
     * Provides the employees whose were loaded from persistent storage during a previous enabling call to any of the employee selections of this view, see
     * below.
     * @return employees
     */
    public List getEmployees();

    /**
     * Enables co sales selection and loads matching employees
     * @throws PermissionException if user has no permission
     */
    public void enableCoSalesSelection() throws PermissionException;

    /**
     * Enables manager selection and loads matching employees
     * @throws PermissionException if user has no permission
     */
    public void enableManagerSelection() throws PermissionException;

    /**
     * Enables manager substitute selection and loads matching employees
     */
    public void enableManagerSubstituteSelection() throws PermissionException;

    /**
     * Enables observer selection and loads matching employees
     */
    public void enableObserverSelection();

    /**
     * Enables sales person selection and loads matching employees
     */
    public void enableSalesSelection() throws PermissionException;

    /**
     * Indicates that an employee selection is enabled
     * @return employeeSelectionMode
     */
    public boolean isEmployeeSelectionMode();

    /**
     * Indicates that employee select mode is co sales select mode
     * @return coSalesSelectMode
     */
    public boolean isCoSalesSelectMode();

    /**
     * Indicates if manager select mode is enabled
     * @return managerSelectMode
     */
    public boolean isManagerSelectMode();

    /**
     * Indicates that employee selection for observer enabled
     * @return observerSelectMode
     */
    public boolean isObserverSelectMode();

    /**
     * Updates an employee setting on current business case
     * @param id of the selected employee
     * @throws ClientException if update failed
     */
    public void updateEmployee(Long id) throws ClientException;

    /**
     * Removes a previous employee selection
     * @param target as 'observer' 'manager' or 'substitute'
     */
    public void removeEmployee(String target);

    public boolean isCoSalesPercentMode();

    public void setCoSalesPercentMode(boolean coSalesPercentMode);

    public void updateCoSalesPercent(Form form) throws ClientException;

    // flow control related actions

    /**
     * Indicates that sale view is in flow control mode
     * @return true if so
     */
    public boolean isFlowControlMode();

    /**
     * Enables/disbles flow control mode.
     * @param true for enabling, false otherwise
     */
    public void setFlowControlMode(boolean flowControlMode);

    // helpers

    /**
     * Disables all none standard modes of the view
     */
    public void disableAllModes();

    /**
     * Indicates that current user is the sales person of the sales request or sale this view represents
     * @return true if so
     */
    public boolean isUserRelatedSales();

    /**
     * Indicates that current user is the co-sales person
     * @return true if so
     */
    public boolean isUserRelatedCoSales();

    /**
     * Indicates that current user is a sales person without executive and sales executive role and not sales or sales-co of current selected sale.
     * @return true if so
     */
    public boolean isUserForeignSales();

    /**
     * Indicates that current user is the manager of the sales request or sale this view represents
     * @return true if so
     */
    public boolean isUserRelatedManager();

    /**
     * Checks wether current user is equal to any project manager of current business case
     * @return true if so
     */
    public boolean isUserResponsibleManager();

    /**
     * Checks if sales and has access permissions
     * @throws PermissionException if user has no access permissions
     */
    public void checkSalesAccess() throws PermissionException;

    /**
     * Reloads businessCase values from persistent storage
     */
    public void reload();

    /**
     * @return
     */
    public boolean isAddSelectNote();

    /**
     * @param addSelectNote
     */
    public void setAddSelectNote(boolean addSelectNote);
}
