/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 10 Mar 2007 09:45:59 
 * 
 */
package com.osserp.core.model;

import java.util.Date;

import com.osserp.common.Option;
import com.osserp.common.User;

import com.osserp.core.BusinessType;
import com.osserp.core.crm.Campaign;
import com.osserp.core.customers.Customer;
import com.osserp.core.requests.ContactRequest;
import com.osserp.core.requests.Request;
import com.osserp.core.system.BranchOffice;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ContactRequestImpl extends RequestImpl implements ContactRequest {

    protected ContactRequestImpl() {
        super();
    }

    public ContactRequestImpl(User user, BusinessType type, Customer customer) {
        super(user, type, customer);
    }

    public ContactRequestImpl(Long user, Request request, BusinessType type) {
        super(user, request, type);
    }

    public ContactRequestImpl(Request otherValue) {
        super(otherValue);
    }

    public ContactRequestImpl(
            Long businessId, 
            String externalReference,
            String externalUrl,
            Date createdDate, 
            BusinessType type, 
            BranchOffice office, 
            Customer customer,
            Long salesPersonId, 
            Long projectManagerId, 
            String name, 
            String street, 
            String streetAddon, 
            String zipcode, 
            String city, 
            Long country,
            Campaign campaign, 
            Option origin) {
        super(businessId, externalReference, externalUrl, createdDate, 
                type, office, customer, salesPersonId, projectManagerId, 
                name, street, streetAddon, zipcode, city, country, 
                campaign, origin);
    }
}
