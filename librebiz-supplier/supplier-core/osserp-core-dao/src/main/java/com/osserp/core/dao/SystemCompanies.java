/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 31-Jul-2006 
 * 
 */
package com.osserp.core.dao;

import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.Option;

import com.osserp.core.BankAccount;
import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.OfficeContact;
import com.osserp.core.finance.Stock;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.system.SystemCompany;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SystemCompanies {

    /**
     * Tries to find a system company by id, contactId or customer id
     * @param id
     * @return company or null if not found
     */
    SystemCompany find(Long id);

    /**
     * Loads a system company from persistent storage
     * @param id
     * @return company
     */
    SystemCompany getCompany(Long id);

    /**
     * Loads a system company by system customer
     * @param customerId
     * @return company or null if no such company exists (e.g. customer is no client)
     */
    SystemCompany getCompanyByCustomer(Long customerId);

    /**
     * Provides all active companies
     * @return active
     */
    List<SystemCompany> getActive();

    /**
     * Provides all system companies
     * @return companies
     */
    List<SystemCompany> getAll();

    /**
     * Provides company shortnames
     * @return shortnames
     */
    List<Option> getShortnames();

    /**
     * Provides all office contacts
     * @return contacts
     */
    List<OfficeContact> getOfficeContacts();

    /**
     * Creates a new stock for client company or branch
     * @param company
     * @param office optional
     * @return new created stock
     */
    Stock createStock(SystemCompany company, BranchOffice office);

    /**
     * Provides all available stocks
     * @return stocks
     */
    List<Stock> getStocks();

    /**
     * Provides the stock by branch
     * @param company
     * @param office
     * @return stock by branch or default stock
     */
    Stock getStockByBranch(Long company, Long office);

    /**
     * Provides existing stock object by primary key
     * @param id
     * @return stock
     * @throws BackendException if not exists
     */
    Stock getStockById(Long id);

    /**
     * Provides the default stock
     * @return default stock
     */
    Stock getDefaultStock();

    /**
     * Updates existing stock config
     * @param stock
     * @param shortKey
     * @param name
     * @param description
     * @param active
     * @param activationDate
     * @param planningAware
     * @return stock
     * @throws ClientException if another stock with same shortKey,
     *  name or description already exists
     */
    Stock updateStock(
            Stock stock,
            String shortKey,
            String name,
            String description,
            boolean active,
            Date activationDate,
            boolean planningAware)
        throws ClientException;

    /**
     * Provides all available bank accounts
     * @return bankAccounts
     */
    List<BankAccount> getBankAccounts();

    /**
     * Provides a bank account by primary key
     * @param id
     * @return bank account. Throws RuntimeException if no such account exists.
     */
    BankAccount getBankAccount(Long id);
    
    /**
     * Assigns a supplier reference to an existing company bank account.
     * @param bankAccountId
     * @param supplierId
     */
    void assignBankAccount(Long bankAccountId, Long supplierId);

    /**
     * Removes an employee group from all companies
     * @param id
     */
    void removeGroup(Long id);

    /**
     * Creates an new system company by proposal
     * @return companyProposal
     * @throws ClientException if contact validation failed
     */
    SystemCompany createCompany(SystemCompany companyProposal) throws ClientException;

    /**
     * Creates an new none persistent system company initialized by contact and next available company id assigned. Name is set to companies display name.
     * @return companySuggestion
     * @throws ClientException if contact validation failed
     */
    SystemCompany createCompanySuggestion(Contact contact) throws ClientException;

    /**
     * Persists current system company values
     * @param company
     * @throws ClientException if validation failed
     */
    void save(SystemCompany company) throws ClientException;
}
