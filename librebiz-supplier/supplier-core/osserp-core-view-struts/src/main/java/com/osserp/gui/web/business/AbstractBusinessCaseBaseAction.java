/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 31-Oct-2006 14:10:55 
 * 
 */
package com.osserp.gui.web.business;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.struts.StrutsForm;
import com.osserp.gui.BusinessCaseView;
import com.osserp.gui.client.Actions;
import com.osserp.gui.client.contacts.CustomerView;
import com.osserp.gui.client.sales.SalesUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractBusinessCaseBaseAction extends AbstractBusinessCaseAction {
    private static Logger log = LoggerFactory.getLogger(AbstractBusinessCaseBaseAction.class.getName());

    /**
     * Forwards to business case display
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward forward(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("forward() request from " + getUser(session).getId());
        }
        BusinessCaseView view = SalesUtil.createBusinessCaseView(
                servlet.getServletContext(),
                request,
                RequestUtil.getId(request));
        return mapping.findForward(view.getForwardTarget());
    }

    /**
     * Forwards to customer display
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward displayCustomer(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("displayCustomer() request from " + getUser(session).getId());
        }
        BusinessCaseView view = getBusinessCaseView(session);
        CustomerView customerView = (CustomerView) createView(request, CustomerView.class.getName());
        customerView.load(view.getBusinessCase());
        return mapping.findForward(Actions.CUSTOMER);
    }

    /**
     * Updates business case delivery address
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward updateAddress(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("updateAddress() request from " + getUser(session).getId());
        }
        BusinessCaseView view = getBusinessCaseView(session);
        try {
            view.updateAddress(new StrutsForm(form));
        } catch (ClientException e) {
            saveError(request, e.getMessage());

        }
        return mapping.findForward(view.getForwardTarget());
    }

    /**
     * Enables the employee selection
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward enableEmployeeSelection(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableEmployeeSelection() request from " + getUser(session).getId());
        }
        BusinessCaseView view = getBusinessCaseView(session);
        String target = (String) request.getAttribute(Actions.TARGET);
        if (target == null) {
            target = RequestUtil.getTarget(request, true);
        }
        if (target.equals(BusinessCaseView.MANAGER)) {
            view.enableManagerSelection();
        } else if (target.equals(BusinessCaseView.SUBSTITUTE)) {
            view.enableManagerSubstituteSelection();
        } else if (target.equals(BusinessCaseView.OBSERVER)) {
            view.enableObserverSelection();
        } else if (target.equals(BusinessCaseView.CO_SALES)) {
            view.enableCoSalesSelection();
        } else if (target.equals(BusinessCaseView.SALES)) {
            view.enableSalesSelection();
        } else {
            throw new BackendException("illegal target: " + target);
        }
        return mapping.findForward(Actions.EMPLOYEE_SELECTION);
    }

    /**
     * Disables employee selection
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward disableEmployeeSelection(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disableEmployeeSelection() request from " + getUser(session).getId());
        }
        BusinessCaseView view = getBusinessCaseView(session);
        view.disableAllModes();
        return mapping.findForward(view.getForwardTarget());
    }

    /**
     * Selects an employee
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward selectEmployee(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("selectEmployee() request from " + getUser(session).getId());
        }
        BusinessCaseView view = getBusinessCaseView(session);
        String target = view.getForwardTarget();
        if (view.isCoSalesSelectMode() && view.isSalesContext()) {
            target = "salesSettings";
        }
        view.updateEmployee(RequestUtil.getId(request));
        redirectAjax(request, response, mapping.findForward(target));
        return null;
    }

    /**
     * Closes an appointment
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward closeAppointmentNote(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("closeAppointmentNote() request from " + getUser(session).getId());
        }
        BusinessCaseView view = getBusinessCaseView(session);
        Long id = RequestUtil.getId(request);
        if (id != null) {
            view.closeAppointment(id);
        }
        return mapping.findForward(view.getForwardTarget());
    }

    public ActionForward enableAgentConfig(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableAgentConfig() request from " + getUser(session).getId());
        }
        return mapping.findForward("salesAgentConfigEditor");
    }

    public ActionForward enableAgentDisplay(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableAgentDisplay() request from " + getUser(session).getId());
        }
        return mapping.findForward("salesAgentConfigDisplay");
    }

    public ActionForward setAgentConfig(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("setAgentConfig() request from " + getUser(session).getId());
        }
        BusinessCaseView view = getBusinessCaseView(session);
        try {
            Long agentId = RequestUtil.fetchId(request);
            view.setAgentConfig(agentId);
        } catch (ClientException e) {
            saveError(request, e.getMessage());
        }
        return mapping.findForward("salesAgentConfigEditor");
    }

    public ActionForward updateAgentConfig(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("updateAgentConfig() request from " + getUser(session).getId());
        }
        BusinessCaseView view = getBusinessCaseView(session);
        try {
            view.updateAgentConfig(new StrutsForm(form));
        } catch (ClientException e) {
            saveError(request, e.getMessage());
        }
        String forwardTarget = view.getForwardTarget();
        if (log.isDebugEnabled()) {
            log.debug("updateAgentConfig() done [target=" + forwardTarget + "]");
        }
        return mapping.findForward(forwardTarget);
    }

    public ActionForward enableTipConfig(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableTipConfig() request from " + getUser(session).getId());
        }
        return mapping.findForward("salesTipConfigEditor");
    }

    public ActionForward enableTipDisplay(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableTipDisplay() request from " + getUser(session).getId());
        }
        return mapping.findForward("salesTipConfigDisplay");
    }

    public ActionForward setTipConfig(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("setTipConfig() request from " + getUser(session).getId());
        }
        BusinessCaseView view = getBusinessCaseView(session);
        try {
            Long tipId = RequestUtil.fetchId(request);
            view.setTipConfig(tipId);
        } catch (ClientException e) {
            saveError(request, e.getMessage());
        }
        return mapping.findForward("salesTipConfigEditor");
    }

    public ActionForward updateTipConfig(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("updateTipConfig() request from " + getUser(session).getId());
        }
        BusinessCaseView view = getBusinessCaseView(session);
        try {
            view.updateTipConfig(new StrutsForm(form));
        } catch (ClientException e) {
            saveError(request, e.getMessage());
        }
        String forwardTarget = view.getForwardTarget();
        if (log.isDebugEnabled()) {
            log.debug("updateTipConfig() done [target=" + forwardTarget + "]");
        }
        return mapping.findForward(forwardTarget);
    }
}
