/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 14, 2008 12:56:12 PM 
 * 
 */
package com.osserp.core.hrm;

import java.util.Date;
import java.util.List;

import com.osserp.common.EntityRelation;
import com.osserp.common.Month;
import com.osserp.common.PublicHoliday;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface TimeRecordingPeriod extends EntityRelation {

    /**
     * Provides the time recording config
     * @return config
     */
    TimeRecordingConfig getConfig();

    /**
     * Changes time recording config
     * @param config
     */
    void changeConfig(TimeRecordingConfig config);

    /**
     * Indicates that config is changeable
     * @return config changeable
     */
    boolean isConfigChangeable();

    /**
     * Indicates that period is currently valid
     * @return current
     */
    boolean isCurrent();

    /**
     * Start date of time recording
     * @return validFrom
     */
    Date getValidFrom();

    /**
     * Set the date of time recording
     * @param validFrom
     */
    void setValidFrom(Date validFrom);

    /**
     * Stop date of time recording
     * @return validTil
     */
    Date getValidTil();

    /**
     * Carryover leave from previous or -initial- external time recording
     * @return carryoverLeave
     */
    double getCarryoverLeave();

    /**
     * Tries to fetch a date.
     * @param mdate
     * @return timeRecordingDay or null if not supported
     */
    TimeRecordingDay fetchDate(Date date);

    /**
     * Carryover hours from previous or -initial- external time recording
     * @return carryoverHours
     */
    double getCarryoverHours();

    /**
     * Provides the carryover maximum hours
     * @return carryoverHoursMax
     */
    double getCarryoverHoursMax();

    /**
     * Sets the carryover hours maximum
     * @param carryoverHoursMax
     */
    void setCarryoverHoursMax(double carryoverHoursMax);

    /**
     * Provides the count of monthly ignorable carryover hours.
     * @return carryoverHoursIgnorable
     */
    double getCarryoverHoursIgnorable();

    /**
     * Sets monthly ignorable carryover hours
     * @param monthlyIgnorableCarryoverHours
     */
    void setCarryoverHoursIgnorable(double carryoverHoursIgnorable);

    /**
     * Provides the last recorded record in the past. Future bookings will be ignored
     * @return lastRecord or null if no booking in the past is available
     */
    TimeRecord getLastRecord();

    /**
     * Provides last created record
     * @return lastCreatedRecord
     */
    TimeRecord getLastCreatedRecord();

    /**
     * Removes record by id
     * @param id
     * @return true if record found and removed
     */
    boolean removeRecord(Long id);

    /**
     * Removes marker by id
     * @param id
     * @return true if record found and removed
     */
    boolean removeMarker(Long id);

    /**
     * Provides all time records of period
     * @return records
     */
    List<TimeRecord> getRecords();

    /**
     * Provides all time records markers of period
     * @return markers
     */
    List<TimeRecordMarker> getMarkers();

    /**
     * Provides all years of this period. For unknown future validation limits <br/>
     * next year will be included last
     * @return supportedYears
     */
    List<TimeRecordingYear> getSupportedYears();

    /**
     * Selects current year and month if period is current. Last month will be selected if when period is from the past.
     */
    void selectCurrentMonth();

    /**
     * Selects a year and month
     * @param month
     */
    void selectMonth(Month month);

    /**
     * Provides the selected year
     * @return selectedYear or null if none selected
     */
    TimeRecordingYear getSelectedYear();

    /**
     * Provides public holidays in period
     * @return publicHolidays
     */
    List<PublicHoliday> getPublicHolidays();

    /**
     * Fetches a month if supported by period
     * @return month or null if not supported
     */
    TimeRecordingMonth fetchMonth(Month month);

    /**
     * Creates a closing entries for open bookings in the past
     * @param stopType
     * @param stopStatus
     * @return true if any closing has been added
     */
    boolean createClosingsIfRequired(TimeRecordType stopType, TimeRecordStatus status);

    /**
     * Provides the extra leave
     * @return extraLeave
     */
    double getExtraLeave();

    /**
     * Sets the extra leave
     * @param extraLeave
     */
    void setExtraLeave(double extraLeave);

    /**
     * Provides the leave in the first year
     * @return firstYearLeave
     */
    double getFirstYearLeave();

    /**
     * Sets the leave in the first year
     * @param firstYearLeave
     */
    void setFirstYearLeave(double firstYearLeave);

    /**
     * Provides the country
     * @return country
     */
    Long getCountry();

    /**
     * Provides the state for this period
     * @return state
     */
    Long getState();

    /**
     * Sets the state for this period
     * @param state
     */
    void setState(Long state);
}
