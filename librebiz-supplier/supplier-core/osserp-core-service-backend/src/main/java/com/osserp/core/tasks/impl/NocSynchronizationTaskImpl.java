/**
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 22, 2013 10:01:15 AM
 * 
 */
package com.osserp.core.tasks.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.service.impl.AbstractObjectMessageHandler;
import com.osserp.core.dao.SyncTasks;
import com.osserp.core.dao.SystemConfigs;
import com.osserp.core.directory.NocSyncSupport;
import com.osserp.core.system.SyncTaskConfig;
import com.osserp.core.tasks.Task;
import com.osserp.core.tasks.NocSynchronizationTicket;

/**
 * 
 * Synchronizes remote NOC Directory Objects if NOC is enabled by systemConfig
 * 
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class NocSynchronizationTaskImpl extends AbstractObjectMessageHandler implements Task {
    private static Logger log = LoggerFactory.getLogger(NocSynchronizationTaskImpl.class.getName());
    private SyncTasks syncTasks;
    private SystemConfigs systemConfigs;
    private SyncTaskConfig taskConfig;

    public NocSynchronizationTaskImpl(SyncTasks syncTasks, SystemConfigs systemConfigs) {
        super();
        this.syncTasks = syncTasks;
        this.systemConfigs = systemConfigs;
    }

    @Override
    protected void execute(Object object, byte[] file) {
        if (taskConfig == null) {
            loadConfig();
        }
        if (taskConfig != null && object instanceof NocSynchronizationTicket) {
            NocSynchronizationTicket ticket = (NocSynchronizationTicket) object;

            String objectId = getObjectId(ticket);
            if (objectId != null) {
                syncTasks.create(taskConfig, ticket.getObjectType(), ticket.getSyncType(), objectId, getObjectData(ticket));
                if (log.isDebugEnabled()) {
                    log.debug("execute() done [object=" + ticket.getObjectType()
                            + ", type=" + ticket.getSyncType() + "]");
                }
            }
        }
    }

    private void loadConfig() {
        Long taskConfigId = systemConfigs.getSystemPropertyId("syncNocTaskId");
        if (taskConfigId != null) {
            taskConfig = syncTasks.getConfig(taskConfigId);
        } else {
            log.warn("loadConfig() did not find systemProperty [name=syncNocTaskId]");
        }
    }

    private String getObjectData(NocSynchronizationTicket ticket) {
        if ((NocSyncSupport.TYPE_GROUP.equals(ticket.getObjectType())
                || NocSyncSupport.TYPE_USER.equals(ticket.getObjectType()))
                && NocSyncSupport.ACTION_UPDATE.equals(ticket.getSyncType())
                && ticket.getDirectoryObject() != null) {
            return (String) ticket.getDirectoryObject().get("dn");
        }
        return null;
    }

    private String getObjectId(NocSynchronizationTicket ticket) {
        if (NocSyncSupport.TYPE_GROUP.equals(ticket.getObjectType())) {
            if (ticket.getDirectoryObject() != null && ticket.getDirectoryObject().containsKey("gidnumber")) {
                return (String) ticket.getDirectoryObject().get("gidnumber");
            }
        } else if (NocSyncSupport.TYPE_USER.equals(ticket.getObjectType())) {
            if (ticket.getDirectoryObject() != null && ticket.getDirectoryObject().containsKey("uid")) {
                return (String) ticket.getDirectoryObject().get("uid");
            }
        } else if (NocSyncSupport.TYPE_CLIENT.equals(ticket.getObjectType())) {
            if (ticket.getDirectoryObject() != null && ticket.getDirectoryObject().containsKey("cn")) {
                return (String) ticket.getDirectoryObject().get("cn");
            }
        } else if (NocSyncSupport.TYPE_DOMAIN.equals(ticket.getObjectType())) {
            if (ticket.getDirectoryObject() != null && ticket.getDirectoryObject().containsKey("dc")) {
                return (String) ticket.getDirectoryObject().get("dc");
            }
        }
        return null;
    }
}
