/**
 *
 * Copyright (C) 2017 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.librebiz

abstract class PackagingHelper extends groovy.lang.Script {

    String[] getMyProjects() {
        [ 'librebiz',
            'librebiz-common',
            'librebiz-webapp',
            'librebiz-runtime/librebiz-tomcat8',
            'librebiz-runtime/librebiz-tomcat9',
            'librebiz-runtime/librebiz-tomcat10'
            ]
    }

    int exec(String command) {
        executeOnShell(command, workingDir)
    }

    int executeOnShell(String command, File working) {
        def commandArray = new String[3]
        commandArray[0] = "sh"
        commandArray[1] = "-c"
        commandArray[2] = command
        def process = new ProcessBuilder(commandArray)
            .directory(working)
            .redirectErrorStream(true)
            .start()
        process.inputStream.eachLine {
            println it
        }
        process.waitFor()
        process.exitValue()
    }

    File getWorkingDir() {
        new File("${project.basedir}".toString())
    }
}
