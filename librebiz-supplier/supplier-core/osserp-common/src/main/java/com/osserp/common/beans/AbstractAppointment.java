/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 28, 2007 10:25:49 PM 
 * 
 */
package com.osserp.common.beans;

import java.util.Date;

import org.jdom2.CDATA;
import org.jdom2.Element;

import com.osserp.common.Appointment;
import com.osserp.common.Calendar;
import com.osserp.common.util.DateUtil;
import com.osserp.common.util.StringUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractAppointment extends AbstractOption implements Appointment {
    public static final int TITLE_LENGTH = 18;
    private static final String TARGET_KEY = "target=";
    private static final String EXIT_KEY = "exit=";
    private String url = null;
    private Date date = null;
    private String headline = null;
    private String message = null;

    private Integer day = null;
    private String dayDisplay = null;
    private Integer month = null;
    private String monthDisplay = null;
    private Integer year = null;
    private String yearDisplay = null;
    private Integer hour = null;
    private String hourDisplay = null;
    private Integer minute = null;
    private String minuteDisplay = null;
    private String timeDisplay = null;
    
    protected AbstractAppointment() {
        super();
    }

    protected AbstractAppointment(
            Long id,
            String name,
            String headline,
            String message,
            String url,
            String target,
            Date date) {
        super(id, name);
        this.headline = headline;
        this.message = message;
        updateDate(date);
        setUrl(url, target);
    }

    protected AbstractAppointment(
            Long id,
            Long reference,
            String name,
            String headline,
            String message,
            String url,
            Date date) {
        super(id, reference, name);
        this.headline = headline;
        this.message = message;
        this.url = url;
        updateDate(date);
    }

    protected AbstractAppointment(Appointment appointment) {
        super(appointment.getId(), appointment.getName());
        updateDate(appointment.getDate());
        headline = appointment.getHeadline();
        message = appointment.getMessage();
        url = appointment.getUrl();
    }

    public void overrideDefaults(String name, String headline, String message) {
        if (name != null) {
            setName(name);
        }
        if (headline != null) {
            setHeadline(headline);
        }
        if (message != null) {
            setMessage(message);
        }
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUrl() {
        return url;
    }

    protected void setUrl(String url) {
        this.url = url;
    }

    private void setUrl(String url, String target) {
        StringBuilder link = new StringBuilder(url);
        StringUtil.replace(link, "@@", getId());
        boolean addTarget = false;
        if (isSet(target) && link.indexOf(TARGET_KEY) > -1) {
            this.url = link.substring(
                    0, link.indexOf(TARGET_KEY) + TARGET_KEY.length());
            addTarget = true;
        } else if (isSet(target) && link.indexOf(EXIT_KEY) > -1) {
            this.url = link.substring(
                    0, link.indexOf(EXIT_KEY) + EXIT_KEY.length());
            addTarget = true;
        }
        if (addTarget) {
            this.url = this.url + target;
        } else {
            this.url = link.toString();
        }
    }

    public boolean isMatching(Calendar date) {
        if (date == null || date.getDay() == null || date.getMonth() == null || date.getYear() == null) {
            return false;
        }
        return (year.intValue() == date.getYear().intValue() &&
                month.intValue() == date.getMonth().intValue() &&
                day.intValue() == date.getDay().intValue());
    }

    public boolean isSameDay(Integer day) {
        return (this.day == null || day == null) ? false : this.day.equals(day);
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDayDisplay() {
        return dayDisplay;
    }

    public String getMonthDisplay() {
        return monthDisplay;
    }

    public String getYearDisplay() {
        return yearDisplay;
    }

    public String getHourDisplay() {
        return hourDisplay;
    }

    public String getMinuteDisplay() {
        return minuteDisplay;
    }

    public String getTimeDisplay() {
        return timeDisplay;
    }

    public void updateDate(Date newDate) {
        if (newDate != null) {
            date = newDate;

            Integer[] time = DateUtil.getTime(date);
            day = time[0];
            month = time[1];
            year = time[2];
            hour = time[3];
            minute = time[4];

            dayDisplay = day.toString();
            monthDisplay = month.toString();
            yearDisplay = year.toString();
            hourDisplay = hour.toString();
            minuteDisplay = minute.toString();
            timeDisplay = DateUtil.getTime(time);

        } else {
            date = null;

            day = null;
            month = null;
            year = null;
            hour = null;
            minute = null;
            
            dayDisplay = null;
            monthDisplay = null;
            yearDisplay = null;
            hourDisplay = null;
            minuteDisplay = null;
            timeDisplay = null;
        }
    }

    public Element getXml() {
        Element item = new Element("item");
        if (getId() != null) {
            item.addContent(new Element("id").setText(getId().toString()));
        }
        item.addContent(new Element("title").setText(getName()));
        item.addContent(new Element("titleShort").setText(
                StringUtil.cut(getName(), TITLE_LENGTH, true)));
        Element link = new Element("titleUrl");
        link.addContent(new CDATA(url));
        item.addContent(link);
        if (headline != null) {
            item.addContent(new Element("headline").setText(headline));
        } else {
            item.addContent(new Element("headline"));
        }
        if (message != null) {
            item.addContent(new Element("text").setText(message));
        } else {
            item.addContent(new Element("text"));
        }
        DateUtil.addDateAndTime(item, date);
        return item;
    }
}
