/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 2 Mar 2007 11:39:06 
 * 
 */
package com.osserp.common.beans;

import java.util.Date;
import java.util.Map;

import org.jdom2.Element;

import com.osserp.common.Entity;
import com.osserp.common.xml.JDOMUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@SuppressWarnings("serial")
public abstract class AbstractEntity extends AbstractPersistent implements Entity {

    private Long id;

    /**
     * Default constructor adding serialializable capability to entities
     */
    protected AbstractEntity() {
        super();
    }

    /**
     * Id assigning constructor
     * @param id
     */
    protected AbstractEntity(Long id) {
        this();
        this.id = id;
    }

    /**
     * Id assigning constructor
     * @param id
     * @param created date
     */
    protected AbstractEntity(Long id, Date created) {
        super(created);
        this.id = id;
    }

    /**
     * Creates a new entity assigning id and reference.
     * @param id
     * @param reference
     */
    protected AbstractEntity(Long id, Long reference) {
        super(reference);
        this.id = id;
    }

    /**
     * Creates a new entity assigning id, reference and creator.
     * @param id
     * @param reference
     * @param createdBy
     */
    protected AbstractEntity(Long id, Long reference, Long createdBy) {
        super(reference, createdBy);
        this.id = id;
    }

    /**
     * Creates a new entity assigning id, reference and creator.
     * @param id
     * @param reference
     * @param created
     * @param createdBy
     */
    protected AbstractEntity(Long id, Long reference, Date created, Long createdBy) {
        super(reference, created, createdBy);
        this.id = id;
    }

    /**
     * Creates a new entity assigning all properties.
     * @param id
     * @param reference
     * @param created
     * @param createdBy
     * @param changed
     * @param changedBy
     * @param endOfLife
     */
    protected AbstractEntity(
            Long id,
            Long reference,
            Date created,
            Long createdBy,
            Date changed,
            Long changedBy,
            boolean endOfLife) {
        super(reference, created, createdBy);
        this.id = id;
    }

    /**
     * Creates a new instance with all values copied by other
     * @param other
     */
    protected AbstractEntity(Entity other) {
        super(other instanceof AbstractPersistent ? (AbstractPersistent) other : null);
        id = other.getId();
    }

    /**
     * Constructs a new instance by xml element. 
     * Values will be retrieved by child elements.
     * @param xml
     */
    protected AbstractEntity(Element xml) {
        super(xml);
        id = JDOMUtil.fetchLong(xml, "id");
    }

    /**
     * Creates a new instance with all values copied found in the map
     * @param map
     */
    protected AbstractEntity(Map<String, Object> map) {
        super(map);
        id = fetchLong(map, "id");
    }

    @Override
    protected Map<String, Object> getMapped() {
        Map<String, Object> map = super.getMapped();
        map.put("id", id);
        return map;
    }

    public Long getPrimaryKey() {
        return id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Indicates if two entities are same by null status or equality of id.
     * @param first
     * @param second
     * @return true if both are same including id and test for null
     */
    protected boolean isSame(Entity first, Entity second) {
        if (first == null && second == null) {
            return true;
        }
        if ((first == null && second != null)
                || (first != null && second == null)) {
            return false;
        } else if (first != null && second != null) {
            if (first.getId() == null && second.getId() == null) {
                return true;
            }
            if ((first.getId() == null && second.getId() != null)
                    || (first.getId() != null && second.getId() == null)) {
                return false;
            }
            if (first.getId() != null && second.getId() != null) {
                if (!first.getId().equals(second.getId())) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public Element getXML() {
        return getXML("root");
    }

    @Override
    public Element getXML(String rootElementName) {
        Element root = super.getXML(rootElementName);
        root.addContent(JDOMUtil.createElement("id", id));
        return root;
    }
}
