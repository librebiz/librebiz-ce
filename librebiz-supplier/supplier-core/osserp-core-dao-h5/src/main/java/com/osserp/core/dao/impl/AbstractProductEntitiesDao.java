/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 2, 2009 11:13:45 AM 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;
import com.osserp.common.util.StringUtil;

import com.osserp.core.Item;
import com.osserp.core.dao.ProductEntities;
import com.osserp.core.dao.ProductSummaryCache;
import com.osserp.core.model.products.ProductDescriptionImpl;
import com.osserp.core.model.products.ProductImpl;
import com.osserp.core.products.Product;
import com.osserp.core.products.ProductDescription;
import com.osserp.core.products.ProductSummary;
import com.osserp.core.products.ProductSummaryImpl;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author cf <cf@osserp.com>
 * 
 */
public abstract class AbstractProductEntitiesDao extends AbstractProductAwareDao implements ProductEntities {
    private static Logger log = LoggerFactory.getLogger(AbstractProductEntitiesDao.class.getName());
    private ProductSummaryCache summaryCache = null;
    protected static final Long DEFAULT_STOCK = 100L;

    protected AbstractProductEntitiesDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache optionsCache,
            ProductSummaryCache summaryCache) {
        super(jdbcTemplate, tables, sessionFactory, optionsCache);
        this.summaryCache = summaryCache;
    }

    public Product find(Long id) {
        try {
            return id != null ? load(id) : null;
        } catch (Throwable t) {
            if (log.isInfoEnabled()) {
                log.info("find() product not found [id=" + id + "]");
            }
            return null;
        }
    }

    public List<ProductDescription> findDescriptions(Long productId) {
        StringBuilder sql = new StringBuilder();
        sql
                .append("from ")
                .append(ProductDescriptionImpl.class.getName())
                .append(" a where a.reference = :productId");
        return getCurrentSession().createQuery(sql.toString())
                .setParameter("productId", productId).list();
    }

    public ProductDescription findDescription(Long productId, String language) {
        if (language == null) {
            return null;
        }
        StringBuilder sql = new StringBuilder();
        sql
                .append("from ")
                .append(ProductDescriptionImpl.class.getName())
                .append(" a where a.reference = :productId and a.language = :lang");
        List<ProductDescription> list = getCurrentSession().createQuery(sql.toString())
                .setParameter("productId", productId)
                .setParameter("lang", language).list();
        return (list.isEmpty() ? null : list.get(0));
    }

    public boolean descriptionsAvailable(List<Item> items, String lang) {
        if (lang == null) {
            return false;
        }
        List<Long> ids = new ArrayList<Long>();
        for (int i = 0, j = items.size(); i < j; i++) {
            if (items.get(i).getProduct().getDescription() != null) {
                ids.add(items.get(i).getProduct().getProductId());
            }
        }
        if (ids.isEmpty()) {
            return false;
        }
        StringBuilder sql = new StringBuilder();
        sql
                .append("SELECT count(*) FROM ")
                .append(productDescriptionsTable)
                .append(" WHERE language = '")
                .append(lang)
                .append("' AND reference_id IN (SELECT product_id FROM ")
                .append(productTable)
                .append(" WHERE description IS NOT NULL) AND reference_id IN (")
                .append(StringUtil.createCommaSeparated(ids))
                .append(")");
        boolean result = jdbcTemplate.queryForObject(sql.toString(), Integer.class) == ids.size();
        if (log.isDebugEnabled()) {
            log.debug("descriptionsAvailable() done, returning " + result);
        }
        return result;
    }

    public Product load(Long id) {
        Product product = (Product) getCurrentSession().load(ProductImpl.class, id);
        loadSummary(product);
        return product;
    }

    public List<Product> load(List<Long> idList) {
        List<Product> result = new ArrayList<>();
        if (!idList.isEmpty()) {
            StringBuilder sql = new StringBuilder();
            sql
                .append("from ")
                .append(ProductImpl.class.getName())
                .append(" a where a.productId in (")
                .append(StringUtil.createCommaSeparated(idList))
                .append(")");
            result = getCurrentSession().createQuery(sql.toString()).list();
            for (int i = 0, j = result.size(); i < j; i++) {
                loadSummary(result.get(i));
            }
        }
        return result;
    }

    public void loadSummary(Product product) {
        if (summaryCache != null) {
            summaryCache.loadSummary(product);
        }
    }

    protected void loadSummary(List<Product> products) {
        if (summaryCache != null) {
            for (int i = 0, j = products.size(); i < j; i++) {
                Product next = products.get(i);
                summaryCache.loadSummary(next);
            }
        }
    }

    public ProductSummary loadSummary(Long stockId, Long productId) {
        if (summaryCache != null) {
            return summaryCache.getSummary(stockId, productId);
        }
        return new ProductSummaryImpl(DEFAULT_STOCK);
    }

    public void updateSummary(List<Item> items) {
        if (summaryCache != null && !items.isEmpty()) {
            for (int i = 0, j = items.size(); i < j; i++) {
                Item next = items.get(i);
                summaryCache.refreshAndLoad(next.getProduct());
            }
        }
    }

    public void save(Product product) {
        getCurrentSession().saveOrUpdate(ProductImpl.class.getName(), product);
        updateDescription(product);
    }

    private void updateDescription(Product product) {
        ProductDescription obj = findDescription(product.getProductId(), product.getDefaultLanguage());
        if (obj != null) {
            boolean changed = false;
            if (product.getName() != null && !product.getName().equals(obj.getName())) {
                obj.setName(product.getName());
                changed = true;
            }
            if (product.getDescription() != null && !product.getDescription().equals(obj.getDescription())) {
                obj.setDescription(product.getDescription());
                changed = true;
            }
            if (changed) {
                getCurrentSession().saveOrUpdate(ProductDescriptionImpl.class.getName(), obj);
            }
        }
    }

    public void saveDescription(Long user, Product product, String language, String name, String description, String datasheetText) {
        if (product == null) {
            throw new IllegalArgumentException("product must not be null");
        }
        if (isNotSet(language)) {
            throw new IllegalArgumentException("language must not be empty");
        }
        ProductDescription obj = null;
        List<ProductDescription> result = findDescriptions(product.getProductId());
        if (!result.isEmpty()) {
            for (int i = 0, j = result.size(); i < j; i++) {
                ProductDescription next = result.get(i);
                if (next.getLanguage().equalsIgnoreCase(language)) {
                    obj = next;
                    break;
                }
            }
        }
        if (obj == null) {
            obj = new ProductDescriptionImpl(user, product, language, name, description);
        } else {
            obj.setName(name);
            obj.setDescription(description);
            obj.setChanged(new Date(System.currentTimeMillis()));
            obj.setChangedBy(user);
        }
        if (datasheetText != null) {
            obj.setDatasheetText(datasheetText);
        } else {
            obj.setDatasheetText(null);
        }
        getCurrentSession().saveOrUpdate(ProductDescriptionImpl.class.getName(), obj);
        if (obj.getLanguage().equals(product.getDefaultLanguage())) {
            boolean changed = false;
            if (obj.getName() != null) {
                product.setName(obj.getName());
                changed = true;
            }
            if (obj.getDatasheetText() != null) {
                product.setDatasheetText(obj.getDatasheetText());
                changed = true;
            }
            if (obj.getDescription() != null) {
                product.setDescription(obj.getDescription());
                changed = true;
            }
            if (changed) {
                getCurrentSession().saveOrUpdate(ProductImpl.class.getName(), product);
            }
        }
    }
}
