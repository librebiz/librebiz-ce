/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 24, 2006 11:12:21 AM 
 * 
 */
package com.osserp.gui.web.sales;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.LockException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.ContextUtil;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.SessionUtil;

import com.osserp.core.calc.CalculationManager;
import com.osserp.core.finance.DeliveryNote;
import com.osserp.core.finance.Order;
import com.osserp.core.sales.Sales;
import com.osserp.gui.BusinessCaseView;
import com.osserp.gui.client.Actions;
import com.osserp.gui.client.records.OrderView;
import com.osserp.gui.client.sales.SalesBillingView;
import com.osserp.gui.client.sales.SalesOrderView;
import com.osserp.gui.client.sales.SalesUtil;
import com.osserp.gui.web.records.AbstractOrderAction;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class OrderAction extends AbstractOrderAction {
    private static Logger log = LoggerFactory.getLogger(OrderAction.class.getName());

    @Override
    protected Class<SalesOrderView> getViewClass() {
        return SalesOrderView.class;
    }

    /**
     * Creates an order view for a business case
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    @Override
    public ActionForward forward(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("forward() request from " + getUser(session).getId());
        }

        BusinessCaseView bcv = SalesUtil.getBusinessCaseView(session);
        try {
            SalesOrderView view = (SalesOrderView) createView(request);
            view.load(bcv);
            String exitTarget = RequestUtil.getExit(request);
            if (exitTarget != null) {
                view.setExitTarget(exitTarget);
            }
            return mapping.findForward(view.getActionTarget());
        } catch (Exception c) {
            log.warn("forward() failed [message=" + c.getMessage() + "]", c);
            if (bcv.isSalesContext()) {
                Sales sales = bcv.getSales();
                if (sales.isClosed() || sales.isCancelled()) {
                    return saveErrorAndReturn(request, ErrorCode.RECORD_HISTICAL_NONE_EXISTING);
                }
            }
            return saveErrorAndReturn(request, c.getMessage());
        }
    }

    /**
     * Redisplays sales order by existing view or loads sales order by current business case if available
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    @Override
    public ActionForward redisplay(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("redisplay() request from " + getUser(session).getId());
        }
        SalesOrderView view = (SalesOrderView) fetchView(session);
        if (view != null) {
            view.refresh();
            return mapping.findForward(Actions.DISPLAY);
        }
        if (log.isDebugEnabled()) {
            log.debug("redisplay() no view found, checking sales availability...");
        }
        view = loadByBusinessCase(request, null);
        if (view != null) {
            return mapping.findForward(view.getActionTarget());
        }
        if (log.isDebugEnabled()) {
            log.debug("redisplay() invalid context, forwarding to index");
        }
        return mapping.findForward(Actions.INDEX);
    }

    /**
     * Exits order display
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    @Override
    public ActionForward exit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exit() request from " + getUser(session).getId());
        }
        SalesOrderView view = getOrderView(session);
        SalesBillingView billingView = fetchBillingView(session);
        BusinessCaseView businessView = SalesUtil.fetchBusinessCaseView(session);
        if (businessView != null && businessView.isFlowControlMode()) {
            if (log.isDebugEnabled()) {
                log.debug("exit() invoked in fcs context");
            }
            return mapping.findForward(performExitCheck(
                    view,
                    request,
                    Actions.FCS_EDIT,
                    "exitFcsContext"));

        } else if (billingView != null) {
            if (log.isDebugEnabled()) {
                log.debug("exit() invoked in billing context");
            }
            // if billing is existing we insist that we have to come back to there
            // cause billing is destroyed in project display so it can't exist
            // if we return from default call location wich is project display
            String target = performExitCheck(
                    view,
                    request,
                    Actions.BILLING,
                    Actions.BILLING);
            billingView.reload();
            return mapping.findForward(target);

        } else if (view != null) {
            if (log.isDebugEnabled()) {
                log.debug("exit() invoked in view context");
            }
            if (businessView != null) {
                try {
                    businessView.reload();
                } catch (Throwable t) {
                    // we ignore this
                    log.warn("exit() failed on attempt to reload business case (ignored)");
                }
            }
            String target = Actions.EXIT;
            if (view.isExitTargetAvailable()) {
                target = view.getExitTarget();
                if (log.isDebugEnabled()) {
                    log.debug("exit() exit target available ["
                            + target + "]");
                }

            } else if (view.isActionTargetAvailable()) {
                target = view.getExitTarget();
                if (log.isDebugEnabled()) {
                    log.debug("exit() action target available ["
                            + target + "]");
                }
            }
            target = performExitCheck(
                    view,
                    request,
                    target,
                    target);
            if (!Actions.DISPLAY.equals(target)) {
                RequestUtil.saveExitId(request, view.getExitId());
                removeView(session);
            }
            return mapping.findForward(target);
        }
        return mapping.findForward(Actions.EXIT);
    }

    /**
     * Removes view and forwards to target 'fcsDisplay'
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward exitFcsContext(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exitFcsContext() request from " + getUser(session).getId());
        }
        try {
            removeView(session);
        } catch (Throwable ignorable) {
            // nothing to do
        }
        return mapping.findForward(Actions.FCS_DISPLAY);
    }

    /**
     * Exits sales order after a warning
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward exitWithoutChecks(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exitWithoutChecks() request from " + getUser(session).getId());
        }
        SalesOrderView view = getOrderView(session);
        SalesBillingView billingView = fetchBillingView(session);
        String target = Actions.EXIT;
        if (billingView != null) {
            if (log.isDebugEnabled()) {
                log.debug("exitWithoutChecks() invoked in billing context");
            }
            billingView.reload();
            target = Actions.BILLING;

        } else if (view != null) {
            if (log.isDebugEnabled()) {
                log.debug("exitWithoutChecks() invoked in view context");
            }
            if (view.isExitTargetAvailable()) {
                target = view.getExitTarget();

            } else if (view.isActionTargetAvailable()) {
                target = view.getExitTarget();
            }
            removeView(session);
        }
        return mapping.findForward(target);
    }

    @Override
    public ActionForward release(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("release() request from " + getUser(session).getId());
        }
        SalesOrderView view = getOrderView(session);
        CalculationManager manager = (CalculationManager) ContextUtil.getService(request, CalculationManager.class.getName());
        try {
            manager.checkLock(view.getRecord());
        } catch (LockException l) {
            SessionUtil.saveLock(session, l.getLock());
            return mapping.findForward(view.getActionTarget());
        }
        try {
            view.checkDeliveryStatus();
            return super.release(mapping, form, request, response);
            
        } catch (ClientException c) {
            saveError(request, c.getMessage());
            return mapping.findForward(view.getActionTarget());
        }
    }

    public ActionForward reopen(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("reopen() request from " + getUser(session).getId());
        }
        // call forward to create a salesOrderView for us
        ActionForward target = forward(mapping, form, request, response);
        if (isErrorAvailable(request)) {
            String exitTarget = RequestUtil.getExit(request);
            if (exitTarget != null) {
                return mapping.findForward(exitTarget);
            }
            return mapping.findForward(Actions.EXIT);
        }
        SalesOrderView view = getOrderView(session);
        try {
            view.reopen();
        } catch (ClientException e) {
            saveError(request, e.getMessage());
        }
        return target;
    }

    /**
     * Enables logistics display
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward enableLogisticsDisplay(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableLogisticsDisplay() request from " + getUser(session).getId());
        }
        SalesOrderView view = getOrderView(session);
        if (!view.isOpenDeliveryDisplay()) {
            view.setOpenDeliveryDisplay(true);
        }
        if (!view.isLogisticsDisplayMode()) {
            view.setLogisticsDisplayMode(true);
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Disables logistics display
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward disableLogisticsDisplay(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disableLogisticsDisplay() request from " + getUser(session).getId());
        }
        SalesOrderView view = getOrderView(session);
        view.setOpenDeliveryDisplay(false);
        view.setLogisticsDisplayMode(false);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Forwards to warranty replacement. Currently not supported.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward forwardWarrantyReplacement(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("forwardWarrantyReplacement() request from " + getUser(session).getId());
        }
        return saveErrorAndReturn(request, ErrorCode.OPERATION_NOT_SUPPORTED);
        /*
         * SalesOrderView view = getOrderView(session); return mapping.findForward("warrantyReplacementCreate");
         */
    }

    private String performExitCheck(
            SalesOrderView view,
            HttpServletRequest request,
            String successTarget,
            String blockTarget) {

        HttpSession session = request.getSession();
        if (view != null) {
            if (log.isDebugEnabled()) {
                log.debug("performExitCheck() invoked with success target '"
                        + successTarget + "' and block target '" + blockTarget + "'");

            }
            Order order = view.getOrder();
            boolean changeableDeliveriesAvailable = order.isChangeableDeliveryAvailable();
            if (!order.isUnchangeable() || changeableDeliveriesAvailable) {
                if (changeableDeliveriesAvailable) {
                    this.saveChangeableDeliveryNoteDetails(session, view);
                }
                saveError(request, ErrorCode.UNRELEASED_RECORD);
                view.setExitBlocked(true);
                view.setExitBlockedTarget(blockTarget);
                return view.getActionTarget();
            }
            // disables exit blocked if previously enabled 
            view.setExitBlocked(false);
            view.setExitBlockedTarget(null);
            if (log.isDebugEnabled()) {
                log.debug("performExitCheck() check ok");
            }
        }
        return successTarget;
    }

    private void saveChangeableDeliveryNoteDetails(
            HttpSession session, SalesOrderView view) {
        if (view != null && view.getBean() != null) {
            Order order = view.getOrder();
            List<DeliveryNote> notes = order.getDeliveryNotes();
            StringBuilder buffer = new StringBuilder();
            for (int i = 0, j = notes.size(); i < j; i++) {
                DeliveryNote note = notes.get(i);
                if (!note.isUnchangeable()) {
                    if (buffer.length() < 1) {
                        buffer.append(note.getId());
                    } else {
                        buffer.append(", ").append(note.getId());
                    }
                }
            }
            if (buffer.length() > 1) {
                saveErrorDetails(session, buffer.toString());
            }
        }
    }

    @Override
    protected SalesOrderView getOrderView(HttpSession session) throws PermissionException {
        if (log.isDebugEnabled()) {
            log.debug("getOrderView() invoked...");
        }
        return (SalesOrderView) getRecordView(session);
    }

    @Override
    protected OrderView createVersionView(HttpServletRequest request) {
        try {
            SalesOrderView view = (SalesOrderView) createView(request);
            loadByBusinessCase(request, view);
            return view;
        } catch (Throwable e) {
            throw new ActionException("createVersionView",
                    "failed to create sales order view [message=" + e.getMessage() + "]");
        }
    }

    private SalesOrderView loadByBusinessCase(HttpServletRequest request, SalesOrderView view) {
        try {
            BusinessCaseView bcv = SalesUtil.fetchBusinessCaseView(request.getSession());
            if (bcv != null && bcv.isSalesContext()) {
                if (view == null) {
                    view = (SalesOrderView) createView(request);
                }
                view.load(bcv);
                if (log.isDebugEnabled()) {
                    log.debug("loadByBusinessCase() done");
                }
                return view;
            }
        } catch (Exception e) {
        }
        return null;
    }

    private SalesBillingView fetchBillingView(HttpSession session) {
        return (SalesBillingView) fetchView(session, SalesBillingView.class);
    }
}
