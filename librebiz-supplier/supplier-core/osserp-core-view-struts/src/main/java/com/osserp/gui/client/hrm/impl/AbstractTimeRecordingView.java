/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 23, 2008 2:16:59 PM 
 * 
 */
package com.osserp.gui.client.hrm.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.util.StringUtil;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.View;
import com.osserp.core.employees.Employee;
import com.osserp.core.hrm.TimeRecordType;
import com.osserp.core.hrm.TimeRecording;
import com.osserp.core.hrm.TimeRecordingApprovalManager;
import com.osserp.core.hrm.TimeRecordingConfig;
import com.osserp.core.hrm.TimeRecordingManager;
import com.osserp.core.hrm.TimeRecordingPeriod;
import com.osserp.core.users.DomainUser;
import com.osserp.gui.client.contacts.EmployeeView;
import com.osserp.gui.client.hrm.TimeRecordingAwareView;
import com.osserp.gui.client.impl.AbstractView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractTimeRecordingView extends AbstractView implements TimeRecordingAwareView {
    private static Logger log = LoggerFactory.getLogger(AbstractTimeRecordingView.class.getName());

    private List<TimeRecordType> recordTypes = new ArrayList<TimeRecordType>();
    private EmployeeView employeeView = null;
    private Employee employee = null;
    private TimeRecording timeRecording = null;
    private boolean currentUser = false;

    protected AbstractTimeRecordingView() {
        super();
    }

    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        super.init(request);
        recordTypes = getTypes();
        Long id = RequestUtil.fetchLong(request, "employee");
        if (id != null) {
            employee = getEmployee(id);
            if (log.isDebugEnabled()) {
                log.debug("init() employee bound [user=" + getDomainEmployee().getId()
                        + ", employee=" + employee.getId() + ", context=employee]");
            }
        } else {
            View view = fetchView(request, EmployeeView.class);
            if (view instanceof EmployeeView) {
                employeeView = (EmployeeView) view;
                employee = employeeView.getEmployee();
                if (log.isDebugEnabled()) {
                    log.debug("init() employee bound [user=" + getDomainEmployee().getId()
                            + ", employee=" + employee.getId() + ", context=employeeView]");
                }
            } else {
                employee = getDomainUser().getEmployee();
                currentUser = true;
                if (log.isDebugEnabled()) {
                    log.debug("init() employee bound [user=" + getDomainEmployee().getId()
                            + ", employee=" + employee.getId() + ", context=currentEmployee]");
                }
            }
        }
        if (employee != null) {
            TimeRecordingManager manager = getTimeRecordingManager();
            timeRecording = manager.createClosingsIfRequired(manager.find(employee));
            TimeRecordingPeriod period = null;

            if (timeRecording != null) {
                timeRecording.selectCurrentPeriod();
                period = timeRecording.getSelectedPeriod();

                if (period == null) {
                    timeRecording.selectLatestPeriod();
                    period = timeRecording.getSelectedPeriod();
                }
            }
            if (log.isDebugEnabled()) {
                log.debug("init() done [user=" + getDomainEmployee().getId()
                        + ", employee=" + employee.getId()
                        + ", recording="
                        + (timeRecording == null ? "null" : timeRecording.getId())
                        + ", selectedPeriod="
                        + (period == null ? "null" : period.getId())
                        + "]");
            }
        }
    }

    public boolean isCurrentUser() {
        return currentUser;
    }

    public Employee getEmployee() {
        return employee;
    }

    protected void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public TimeRecording getTimeRecording() {
        return timeRecording;
    }

    protected void setTimeRecording(TimeRecording timeRecording) {
        this.timeRecording = timeRecording;
    }

    public boolean isAdministrator() {
        DomainUser user = getDomainUser();
        return user.isPermissionGrant(new String[] { "time_recording_config", "time_recording_admin" });
    }

    public boolean isApprovalAvailable() {
        return !getTimeRecordingApprovalManager().loadApprovals(getEmployee()).isEmpty();
    }

    protected List<TimeRecordType> getRecordTypes() {
        return recordTypes;
    }

    protected List<TimeRecordType> fetchTypes(boolean starting) {
        DomainUser user = getDomainUser();

        List<TimeRecordType> result = new ArrayList<TimeRecordType>();
        for (int i = 0, j = recordTypes.size(); i < j; i++) {
            TimeRecordType next = recordTypes.get(i);
            if ((next.isStarting() && starting) || (!next.isStarting() && !starting)) {
                String[] array = StringUtil.getTokenArray(next.getPermissions());
                if ((next.isBookable() && (next.getPermissions() == null || next.getPermissions().isEmpty()))
                        || user.isPermissionGrant(new String[] { "hrm" })
                        || user.isPermissionGrant(array)) {
                    result.add(next);
                }
            }
        }
        return result;
    }

    protected TimeRecordType fetchType(Long id) {
        for (int i = 0, j = recordTypes.size(); i < j; i++) {
            TimeRecordType next = recordTypes.get(i);
            if (next.getId().equals(id)) {
                return next;
            }
        }
        throw new ActionException("time record type not found [id=" + id + "]");
    }

    public void refresh() {
        TimeRecordingManager manager = getTimeRecordingManager();
        Employee tre = getEmployee();
        if (tre != null) {
            TimeRecording current = getTimeRecording();
            TimeRecordingPeriod currentPeriod = (current == null ? null : current.getSelectedPeriod());
            TimeRecording recording = manager.find(tre);
            if (recording != null) {
                if (current != null && currentPeriod != null) {
                    recording.selectPeriod(currentPeriod.getId());
                    if (currentPeriod.getSelectedYear() != null
                            && currentPeriod.getSelectedYear()
                                    .getSelectedMonth() != null) {
                        recording.getSelectedPeriod().selectMonth(
                                currentPeriod.getSelectedYear()
                                        .getSelectedMonth());
                        if (log.isDebugEnabled()) {
                            log.debug("refresh() done");
                        }
                    }
                }
                this.timeRecording = recording;
            }
        }
    }

    private List<TimeRecordType> getTypes() {
        return getTimeRecordingManager().findTypes();
    }

    protected List<TimeRecordingConfig> getConfigs() {
        return getTimeRecordingManager().findConfigs();
    }

    protected TimeRecordingApprovalManager getTimeRecordingApprovalManager() {
        return (TimeRecordingApprovalManager) getService(TimeRecordingApprovalManager.class.getName());
    }

    protected TimeRecordingManager getTimeRecordingManager() {
        return (TimeRecordingManager) getService(TimeRecordingManager.class.getName());
    }
}
