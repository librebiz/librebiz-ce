/**
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 9, 2012 08:12:52 AM
 * 
 */
package com.osserp.core.service.directory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.directory.DirectoryGroup;
import com.osserp.common.directory.DirectoryManager;
import com.osserp.core.dao.Employees;
import com.osserp.core.directory.DirectoryGroupSynchronizer;
import com.osserp.core.employees.EmployeeGroup;
import com.osserp.core.system.SystemConfigManager;
import com.osserp.core.tasks.NocSynchronizationSender;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class DirectoryGroupSynchronizerImpl extends AbstractDirectoryService implements DirectoryGroupSynchronizer {
    private static Logger log = LoggerFactory.getLogger(DirectoryGroupSynchronizerImpl.class.getName());

    private Employees employees;
    private NocSynchronizationSender nocSynchronizationSender;

    public DirectoryGroupSynchronizerImpl(
            SystemConfigManager systemConfigManager,
            DirectoryManager directoryManager,
            Employees employees,
            NocSynchronizationSender nocSynchronizationSender) {
        super(systemConfigManager, directoryManager);
        this.employees = employees;
        this.nocSynchronizationSender = nocSynchronizationSender;
    }

    public void synchronize(Long employeeGroupId) {
        log.debug("synchronize: invoked [group=" + employeeGroupId + "]");
        EmployeeGroup group = employees.getEmployeeGroup(employeeGroupId);
        if (group == null) {
            removeGroup(employeeGroupId);
        } else {
            addOrUpdateGroup(group);
        }
    }

    private void removeGroup(Long employeeGroupId) {
        List<DirectoryGroup> ldapGroups = getDirectoryManager().findGroups(getDirectoryClientName());
        for (int i = 0, j = ldapGroups.size(); i < j; i++) {
            DirectoryGroup dg = ldapGroups.get(i);
            if (dg.getEmployeeGroupId() != null && dg.getEmployeeGroupId() == employeeGroupId) {
                getDirectoryManager().deleteGroup(dg);
                if (log.isDebugEnabled()) {
                    log.debug("removeGroup() done [id=" + employeeGroupId + "]");
                }
                if (nocSynchronizationSender != null) {
                    nocSynchronizationSender.deleteGroup(dg.getValues());
                }
                break;
            }
        }
    }

    private void addOrUpdateGroup(EmployeeGroup employeeGroup) {
        List<DirectoryGroup> ldapGroups = getDirectoryManager().findGroups(getDirectoryClientName());
        DirectoryGroup directoryGroup = null;
        for (int i = 0, j = ldapGroups.size(); i < j; i++) {
            DirectoryGroup dg = ldapGroups.get(i);
            if (dg.getEmployeeGroupId() != null && dg.getEmployeeGroupId() == employeeGroup.getId()) {
                directoryGroup = dg;
                if (log.isDebugEnabled()) {
                    log.debug("addOrUpdateGroup() group found [id=" + employeeGroup.getId() + "]");
                }
                break;
            }
        }
        if (directoryGroup != null) {
            Map values = new HashMap<>();
            values.put("description", employeeGroup.getKey());
            getDirectoryManager().update(directoryGroup, values);
            if (log.isDebugEnabled()) {
                log.debug("addOrUpdateGroup() done [id=" + employeeGroup.getId() + ", action=update]");
            }
        } else {
            directoryGroup = getDirectoryManager().createGroup(
                    employeeGroup.getName(),
                    employeeGroup.getKey(),
                    getDirectoryClientName(),
                    employeeGroup.getId());
            Map values = new HashMap<>();
            values.put("osserpgid", employeeGroup.getId().toString());
            directoryGroup = getDirectoryManager().update(directoryGroup, values);
            if (log.isDebugEnabled()) {
                log.debug("addOrUpdateGroup() done [id=" + employeeGroup.getId() + ", action=add]");
            }
        }
        if (nocSynchronizationSender != null) {
            nocSynchronizationSender.updateGroup(directoryGroup.getValues());
        }
    }
}
