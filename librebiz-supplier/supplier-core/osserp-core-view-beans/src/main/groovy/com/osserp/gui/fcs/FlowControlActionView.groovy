/**
 *
 * Copyright (C) 2006, 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 15, 2006 
 * Created on Apr 16, 2014 (Groovy implementation) 
 * 
 */
package com.osserp.gui.fcs

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ActionException
import com.osserp.common.ClientException
import com.osserp.common.PermissionException
import com.osserp.common.util.CollectionUtil

import com.osserp.core.BusinessCase
import com.osserp.core.FcsAction
import com.osserp.core.FcsClosing
import com.osserp.core.FcsItem
import com.osserp.core.FcsManager
import com.osserp.core.employees.Employee
import com.osserp.core.users.DomainUser

import com.osserp.groovy.web.MenuItem
import com.osserp.groovy.web.ViewContextException
import com.osserp.gui.BusinessCaseView
import com.osserp.gui.CoreView

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
abstract class FlowControlActionView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(FlowControlActionView.class.getName())

    boolean flowControlCancelMode = false
    List<FcsAction> actions = []
    List<FcsClosing> closings = []
    boolean providesClosings = false
    List<FcsItem> wastebasket = []
    FcsAction selectedAction
    FcsItem selectedItem
    Long selectedFlowControlGroup
    Map<String, String> actionTargets = [:]

    FlowControlActionView() {
        super()
        dependencies = ['businessCaseView']
        providesCustomNavigation()
    }

    /**
     * Implement to provide the businessCase depending on context of implementing view
     * @return businessCase    
     */
    abstract BusinessCase getBusinessCase();

    /**
     * Inplement to provide the related flowControlManager
     * @return flowControlManager
     */
    protected abstract FcsManager getFlowControlManager();

    /**
     * Creates actionTarget name to url mappings. Override with required targets.
     * @return action target mappings or empty map wich is default.
     */
    protected Map<String, String> createActionTargets() {
        [:]
    }

    /**
     * Indicates if actionView provides feature to reset a cancelled businessCase
     * (e.g. remove cancelled action and set previous status). This is true for
     * any cancelled businessCase. Override if view implementation and businessCase 
     * requires another criteria.  
     * @return true if businessCase is cancelled
     */
    boolean isProvidingResetCancel() {
        businessCase?.cancelled
    }

    /**
     * Resets a prevoius cancellation if businessCase and view supports this.
     */
    void resetCancellation() {
        if (providingResetCancel) {
            flowControlManager.resetCancellation(domainEmployee, businessCase)
            businessCaseView.reload()
            load()
        }
    }

    /**
     * Provides the related businessCase view
     * @return businessCaseView
     */
    BusinessCaseView getBusinessCaseView() {
        env.businessCaseView
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (!businessCaseView) {
            throw new ViewContextException('businessCaseView not bound')
        }

        if (!actions) {
            actionTargets = createActionTargets()
            load()
            DomainUser user = getDomainUser()
            if (user.defaultFcsGroup) {
                List actList = fetchActionsByGroup(user.defaultFcsGroup)
                if (actList) { 
                    selectedFlowControlGroup = user.defaultFcsGroup
                }
            }
        }
    }

    String selectAction() throws ClientException {
        Long id = form.getLong('id')
        if (!id) {
            selectedAction = null
            return null
        } 
        selectedAction = CollectionUtil.getByProperty(actions, 'id', id)
        try {
            checkSelectedAction()
            flowControlManager.checkDependencies(actions, selectedAction)
            enableEditMode()

        } catch (Exception e) {
            selectedAction = null
            throw new ClientException(e.message)
        }
        if (selectedAction.performAction
            && selectedAction.performOnStart
            && selectedAction.startActionName) {
            
            logger.debug("selectAction: action target found [name=${selectedAction.startActionName}]")
            String forward = actionTargets[selectedAction.startActionName]
            logger.debug("selectAction: done [forward=${forward}]")
            return forward
        }
        return null
    }

    /**
     * Performs local checks against selectedAction. The method is invoked by selectAction
     * just before invocation of flowControlManagers checkDependencies method. 
     * Override this method if implementing view requires additional checks.
     * @throws ClientException if context checks failed      
     */
    void checkSelectedAction() throws ClientException {
        // override to perform context specific checks
    }

    void addAction() throws ClientException, PermissionException {
        if (!selectedAction) {
            logger.warn('addAction: failed [selectedAction=null]')
            throw new ActionException(ActionException.NO_OBJECT_BOUND)
        }
        Date created = form.getDate('created')
        String note = form.getString('note')
        String headline = form.getString('headline')
        FcsClosing closing = fetchClosing(form.getLong('closing'))
        if (!closing) {
            String closingName = form.getString('closingName')
            if (closingName) {
                closing = flowControlManager.createClosing(domainEmployee, closingName, null)
            }
        }
        flowControlManager.addFlowControl(
            domainEmployee,
            businessCase,
            selectedAction,
            created,
            note,
            headline,
            closing,
            false)
        businessCaseView.reload()
        businessCaseView.setFlowControlMode(false)
        selectedAction = null
        disableEditMode()
        load()
    }

    private FcsClosing fetchClosing(Long id) {
        !id ? null : closings.find { it.id == id }
    }

    @Override
    void disableEditMode() {
        super.disableEditMode()
        flowControlCancelMode = false
        selectedAction = null
        selectedItem = null
    }

    void enableClosings() {
        providesClosings = true
    }

    void disableClosings() {
        providesClosings = false
    }

    void updateAction(Long id, String note) {
        selectedItem = CollectionUtil.getByProperty(businessCase.flowControlSheet, 'id', id)
        flowControlManager.updateFcsNote(selectedItem, domainEmployee.displayName, note)
        businessCaseView.reload()
    }

    void selectCancel() throws PermissionException {
        DomainUser user = getDomainUser()
        if (user.isSales() &&
                (!user.isAccounting()
                        && !user.isCustomerService()
                        && !user.isExecutive()
                        && !user.isResponsibleManager(businessCase))
                && !user.isSalesExecutive()) {
            throw new PermissionException()
        }
        Long id = form.getLong('id')
        if (!id) {
            selectedItem = null
            flowControlCancelMode = false
        } else {
            selectedItem = CollectionUtil.getByProperty(businessCase.flowControlSheet, 'id', id)
            flowControlCancelMode = true
            enableEditMode()
        }
    }

    void cancelAction() throws ClientException, PermissionException {
        if (!selectedItem) {
            logger.warn('cancelAction: failed [selectedItem=null]')
            throw new ActionException(ActionException.NO_OBJECT_BOUND)
        }
        String note = form.getString("note")
        flowControlManager.cancelFlowControl(
            domainEmployee.id,
            businessCase,
            selectedItem,
            note)
        selectedItem = null
        disableEditMode()
        businessCaseView.reload()
        load()
    }

    boolean isDisplayCanceledMode() {
        env.displayCanceledMode
    }

    void toggleDisplayCanceledMode() {
        env.displayCanceledMode = !displayCanceledMode
    }

    boolean isCanceledActionsAvailable() {
        try {
            BusinessCase bc = getBusinessCase()
            if (bc != null && !bc.getFlowControlSheet().isEmpty()) {
                for (int i = 0; i < bc.getFlowControlSheet().size(); i++) {
                    FcsItem next = bc.getFlowControlSheet().get(i)
                    if (next.getCanceledBy() != null || next.isCancels()) {
                        return true
                    }
                }
            }
        } catch (Exception e) {
            // we ignore this
            logger.warn("isCanceledActionsAvailable: ignoring exception [message=${e.message}]")
        }
        logger.debug('isCanceledActionsAvailable: done [result=false]')
        return false
    }

    boolean isWastebasketMode() {
        env.wastebasketMode
    }

    void enableWastebasketMode() {
        env.wastebasketMode = true
    }

    void disableWastebasketMode() {
        env.wastebasketMode = false
    }

    void throwAction() throws ClientException, PermissionException {
        Long id = form.getLong('id')
        flowControlManager.throwFlowControl(domainEmployee.id, businessCase, id)
        businessCaseView.reload()
        load()
    }

    void restoreAction() {
        Long id = form.getLong('id')
        flowControlManager.restoreFlowControl(businessCase, id)
        businessCaseView.reload()
        load()
        if (!wastebasket) {
            disableWastebasketMode()
        }
    }

    void selectGroup() {
        Long id = form.getLong('id')
        if (id) {
            selectedFlowControlGroup = id
        } else {
            selectedFlowControlGroup = null
        } 
    }

    /**
     * Loads available actions
     */
    protected void load() {
        actions = flowControlManager.createActionList(businessCase)
        wastebasket = flowControlManager.getWastebasket(businessCase)
        closings = flowControlManager.findClosings(businessCase.type?.id)
    }

    @Override
    protected void createCustomNavigation() {

        if (wastebasketMode) {
            nav.defaultNavigation = [disableWastebasketLink, homeLink]

        } else if (canceledActionsAvailable) {
            if (displayCanceledMode) {
                nav.defaultNavigation = [exitLink, hideCanceledLink, homeLink]
            } else {
                nav.defaultNavigation = [exitLink, displayCanceledLink, homeLink]
            }
        } else {
            nav.defaultNavigation = [exitLink, homeLink]
        }
    }

    private List<FcsAction> fetchActionsByGroup(Long grpId) {
        List<FcsAction> result = []
        if (grpId) {
            actions.each { FcsAction action ->
                if (action.groupId == grpId) {
                    result << action
                }
            }
        }
        return result
    }

    MenuItem getDisplayCanceledLink() {
        navigationLink("/${context.name}/toggleDisplayCanceledMode", 'showDetailsIcon', 'showDetails')
    }

    MenuItem getHideCanceledLink() {
        navigationLink("/${context.name}/toggleDisplayCanceledMode", 'hideDetailsIcon', 'hideDetails')
    }

    MenuItem getDisableWastebasketLink() {
        navigationLink("/${context.name}/disableWastebasketMode", 'backIcon', 'backToLast')
    }
}
