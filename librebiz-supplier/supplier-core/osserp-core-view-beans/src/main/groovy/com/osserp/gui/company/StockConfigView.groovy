/**
 *
 * Copyright (C) 2021 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License (AGPL)
 * version 3 as published by the Free Software Foundation. In accordance
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code
 * versions must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area
 * or, if not reasonably feasible for technical reason, as a top-level
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute.
 *
 */
package com.osserp.gui.company

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ErrorCode
import com.osserp.core.finance.Stock
import com.osserp.core.system.BranchOffice
import com.osserp.groovy.web.ViewContextException
import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class StockConfigView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(StockConfigView.class.getName())

    boolean headquarter

    StockConfigView() {
        dependencies = [ 'branchView', 'clientCompanyView' ]
    }

    Stock getStock() {
        return bean
    }

    String getStockLocationName() {
        if (env.branchView) {
            return env.branchView.bean?.contact?.displayName
        }
        return env.clientCompanyView?.bean?.contact?.displayName
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            bean = env.branchView?.stock ?: env.clientCompanyView?.stock
            if (!bean) {
                logger.error('initRequest: Stock not bound')
                throw new ViewContextException(ErrorCode.INVALID_CONTEXT, this)
            }
            if (!env.branchView?.stock && env.clientCompanyView?.stock) {
                headquarter = true
            }
            enableEditMode()
            enableExternalInvocationMode()
        }
    }

    @Override
    void save() {
        if (!bean) { throw new ViewContextException(ErrorCode.INVALID_CONTEXT) }
        bean = systemConfigManager.updateStock(
            domainUser,
            stock,
            form.getString('shortKey'),
            form.getString('name'),
            form.getString('description'),
            form.getBoolean('active'),
            form.getDate('activationDate'),
            form.getBoolean('planningAware'))
        disableEditMode()
    }
}
