<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>

<c:set var="portalView" value="${sessionScope.portalView}"/>

<c:if test="${!empty portalView and portalView.helpConfigMode}">
<hr />
<div class="row" id="helpConfigContent">
    <div class="col-md-6">
        <div class="panel-body">
            <div class="form-body">
                <o:form name="pageForm" url="/helpConfig.do">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="linkTitle">
                                    <fmt:message key="title" /><input type="hidden" name="method" value="save" />
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" name="name" value="<o:out value="${portalView.helpLink.description}"/>" class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="status">
                                    <fmt:message key="status" />
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <c:choose>
                                    <c:when test="${empty portalView.helpLink.page}">
                                        <fmt:message key="notAssigned" />
                                    </c:when>
                                    <c:when test="${portalView.helpLink.activated}">
                                        <a href="<c:url value="/helpConfig.do?method=disablePage"/>" title="<fmt:message key="deactivate"/>">
                                            <fmt:message key="activated" />
                                        </a>
                                    </c:when>
                                    <c:otherwise>
                                        <a href="<c:url value="/helpConfig.do?method=enablePage"/>" title="<fmt:message key="activate"/>">
                                            <fmt:message key="deactivated" />
                                        </a>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                    <div class="row next">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="URL">URL-Part</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" name="value" value="<o:out value="${portalView.helpLink.page}"/>" class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="placeholder"> </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <o:submit styleClass="form-control" />
                            </div>
                        </div>
                    </div>
                </o:form>
            </div>
        </div>
    </div>
</div>
</c:if>
