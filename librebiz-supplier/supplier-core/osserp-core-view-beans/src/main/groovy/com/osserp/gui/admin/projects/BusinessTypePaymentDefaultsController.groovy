/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 11.04.2011 17:07:09 
 * 
 */
package com.osserp.gui.admin.projects

import javax.servlet.http.HttpServletRequest

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.groovy.web.PopupViewController

/**
 *
 * @author eh <eh@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
@RequestMapping("/admin/projects/businessTypePaymentDefaults/*")
@Controller class BusinessTypePaymentDefaultsController extends PopupViewController {
    private static Logger logger = LoggerFactory.getLogger(BusinessTypePaymentDefaultsController.class.getName())

    @RequestMapping
    def selectPercentage(HttpServletRequest request) {
        logger.debug("selectPercentage: invoked [user=${getUserId(request)}]")
        BusinessTypePaymentDefaultsView view = getView(request)
        view.selectPercentage()
        return defaultPage
    }
}
