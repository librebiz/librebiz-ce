/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 26, 2008 12:09:44 AM 
 * 
 */
package com.osserp.core.dms;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.dms.DocumentData;

import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface LetterContentManager {

    /**
     * Provides an existing letter type
     * @param id
     * @return type
     */
    LetterType getType(Long id);

    /**
     * Provides type by context name.
     * @return type or null if not exists
     */
    LetterType findType(String contextName);

    /**
     * Provides letter content by id
     * @param id
     * @return letterContent or null if not existing
     */
    LetterContent find(Long id);

    /**
     * Deletes a letter
     * @param user
     * @param letter
     * @throws PermissionException
     */
    void delete(DomainUser user, LetterContent letter) throws PermissionException;

    /**
     * Adds a new empty paragraph to a letter
     * @param user
     * @param letterContent
     * @return paragraph
     */
    LetterParagraph createParagraph(DomainUser user, LetterContent letterContent);

    /**
     * Adds a new empty paragraph to a letter
     * @param user
     * @param letterContent
     * @param paragraph to delete
     */
    void deleteParagraph(DomainUser user, LetterContent letterContent, LetterParagraph paragraph);

    /**
     * Moves down a paragraph
     * @param letterContent
     * @param id of the paragraph to move
     */
    void moveParagraphDown(LetterContent letterContent, Long id);

    /**
     * Moves up a paragraph
     * @param letterContent
     * @param id of the paragraph to move
     */
    void moveParagraphUp(LetterContent letterContent, Long id);

    /**
     * Updates a paragraph
     * @param user
     * @param letterContent
     * @param paragraph
     * @param content
     * @throws ClientException
     */
    void updateParagraph(
            DomainUser user,
            LetterContent letterContent,
            LetterParagraph paragraph,
            String content) throws ClientException;

    /**
     * Provides letter as pdf
     * @param user
     * @param letterConte document data
     */
    DocumentData getPdf(DomainUser user, LetterContent letterContent);

    /**
     * Provides a default salutation string
     * @param letter
     * @return salutation string
     */
    String getDefaultSalutation(LetterContent letter);

}
