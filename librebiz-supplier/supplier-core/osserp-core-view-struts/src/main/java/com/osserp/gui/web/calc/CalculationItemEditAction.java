/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 5, 2005 11:47:48 AM 
 * 
 */
package com.osserp.gui.web.calc;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.struts.StrutsForm;
import com.osserp.gui.client.Actions;
import com.osserp.gui.client.calc.CalculationView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CalculationItemEditAction extends AbstractCalculationAction {
    private static Logger log = LoggerFactory.getLogger(CalculationItemEditAction.class.getName());

    /**
     * Fetches the selected id param sets the selected object from the backend and forwards to 'success' target
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward select(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("select() request from " + getUser(session).getId());
        }
        CalculationView view = getCalculationView(session);
        view.getCalculator().setSelectedItem(RequestUtil.getId(request));
        return mapping.findForward(Actions.EDIT);
    }

    /**
     * Updates the quantity of a selected item position
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward update(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        if (log.isDebugEnabled()) {
            log.debug("update() requested");
        }
        HttpSession session = request.getSession();
        Form fh = new StrutsForm(form);
        CalculationView view = getCalculationView(session);
        try {
            BigDecimal partnerPrice = fh.getDecimal(Form.PARTNER_PRICE);
            if (partnerPrice == null) {
                partnerPrice = new BigDecimal(0);
            }
            BigDecimal partnerPriceSource = fh.getDecimal(Form.PARTNER_PRICE_SOURCE);
            if (partnerPriceSource == null) {
                partnerPriceSource = new BigDecimal(0);
            }
            boolean partnerPriceChanged = !partnerPrice.equals(partnerPriceSource);
            if (log.isDebugEnabled() && view.isCalculatorAvailable() && view.getCalculator().getSelectedItem() != null) {
                log.debug("update() fetched partner prices [product="
                        + view.getCalculator().getSelectedItem().getProduct().getProductId()
                        + ", partnerPriceChanged=" + partnerPriceChanged
                        + ", partnerPriceSource=" + partnerPriceSource
                        + ", partnerPriceInput=" + partnerPrice
                        + "]");
            }
            view.getCalculator().updateItem(
                    fh.getString(Form.CUSTOM_NAME),
                    fh.getDouble(Form.QUANTITY),
                    partnerPrice,
                    partnerPriceChanged,
                    fh.getDecimal(Form.PRICE),
                    fh.getString(Form.NOTE));
            return mapping.findForward(Actions.REFRESH);
        } catch (ClientException cx) {
            saveError(request, cx.getMessage());
            if (log.isDebugEnabled()) {
                log.debug("update() validation failed: " + cx.getMessage());
            }
            return mapping.findForward(Actions.EDIT);
        }
    }

    /**
     * Updates the quantity of a selected item position
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward exit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exit() request from " + getUser(session).getId());
        }
        return mapping.findForward(Actions.REFRESH);
    }

    @Override
    protected CalculationView getCalculationView(HttpSession session) throws PermissionException {
        CalculationView view = super.getCalculationView(session);
        if (!view.isCalculatorAvailable()) {
            log.warn("getCalculationView() failed: calculator not bound!");
            throw new ActionException();
        }
        return view;
    }
}
