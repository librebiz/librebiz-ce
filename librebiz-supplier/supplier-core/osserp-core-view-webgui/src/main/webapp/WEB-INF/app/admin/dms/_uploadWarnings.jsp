<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<c:set var="view" value="${requestScope.templateView}"/>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for="uploadWarning-1-2"><fmt:message key="hint" /></label>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <fmt:message key="templateUploadWarning1"/><br />
            <fmt:message key="templateUploadWarning2"/>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for="uploadWarning-3"></label>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <fmt:message key="templateUploadWarning3"/>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for="downloadBackup"><fmt:message key="template" /></label>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <o:doc obj="${view.bean}"><fmt:message key="fileDownloadLabel"/></o:doc>
        </div>
    </div>
</div>
