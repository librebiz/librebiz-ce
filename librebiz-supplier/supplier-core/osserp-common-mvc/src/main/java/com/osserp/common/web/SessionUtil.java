/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 3, 2005 
 * 
 */
package com.osserp.common.web;

import java.io.Serializable;
import java.util.Enumeration;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lowagie.text.Document;

import com.osserp.common.ActionException;
import com.osserp.common.Constants;
import com.osserp.common.Lock;
import com.osserp.common.TimeoutException;
import com.osserp.common.User;
import com.osserp.common.util.AbstractValidator;
import com.osserp.common.util.EmailValidator;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SessionUtil extends AbstractValidator {
    private static Logger log = LoggerFactory.getLogger(SessionUtil.class.getName());
    private static String ITEXT_DOC = "itextDocument";

    public static boolean isUserLoggedIn(HttpSession session) {
        return session.getAttribute(Globals.USER) != null;
    }

    /**
     * Cleans up all session objects except all objects whose key contains 'Search' or 'javax.servlet.jsp.jstl'.
     * @param session to clean up
     */
    public static void cleanUp(HttpSession session) {
        Enumeration atts = session.getAttributeNames();
        while (atts.hasMoreElements()) {
            String name = (String) atts.nextElement();
            if (!Globals.USER.equals(name)
                    && !Globals.LOCALE_KEY.equals(name)
                    && (name.indexOf("Search") == -1)
                    && (name.indexOf("javax.servlet.jsp.jstl") == -1)
                    && (name.indexOf("eventView") == -1)
                    && (name.indexOf("portalView") == -1)
                    && (name.indexOf("_const_cas_assertion_") == -1)) {
                /*
                 * if (log.isDebugEnabled()) { log.debug("cleanUp() removing " + name); }
                 */
                session.removeAttribute(name);
                /*
                 * } else if (log.isDebugEnabled()) { log.debug("cleanUp() ignoring attribute " + name);
                 */
            }
        }
    }

    public static void logDebug(HttpSession session) {
        if (log.isDebugEnabled()) {
            java.util.Enumeration attributeNames = session.getAttributeNames();
            while (attributeNames.hasMoreElements()) {
                String attributeName = (String) attributeNames.nextElement();
                Object next = session.getAttribute(attributeName);
                log.debug("logDebug() [" + attributeName + "=" + next.getClass().getName() + "]");
            }
        }
    }

    public static void logInfo(HttpSession session) {
        if (log.isInfoEnabled()) {
            java.util.Enumeration attributeNames = session.getAttributeNames();
            while (attributeNames.hasMoreElements()) {
                String attributeName = (String) attributeNames.nextElement();
                Object next = session.getAttribute(attributeName);
                log.info("logInfo() [" + attributeName + "=" + next.getClass().getName() + "]");
            }
        }
    }

    /**
     * Provides current page name
     * @param session
     * @return page or null if not exists
     */
    public static Page getPage(HttpSession session) {
        return (Page) session.getAttribute(Globals.CURRENT_PAGE_NAME);
    }

    /**
     * Provides current page name
     * @param session
     * @param page
     */
    public static void setPage(HttpSession session, Page page) {
        session.setAttribute(Globals.CURRENT_PAGE_NAME, page);
    }

    /**
     * Provides the currently logged in user
     * @param session
     * @return user currently logged in
     * @throws RuntimeException if no user logged in
     */
    public static User getUser(HttpSession session) {
        return (User) getObject(session, Globals.USER);
    }

    /**
     * Provides the currently logged in user
     * @param session
     * @return user currently logged in or null if no user exists
     */
    public static User lookupUser(HttpSession session) {
        return (User) session.getAttribute(Globals.USER);
    }

    public static void setUser(HttpSession session, User user) {
        session.setAttribute(Globals.USER, user);
    }

    public static void removeUser(HttpSession session) {
        session.removeAttribute(Globals.USER);
    }

    /**
     * Fetches the object with the specified key from the session context
     * @param session reference to the session context
     * @param key to lookup for
     * @return object related to specified key
     * @throws TimeoutException if session was null
     */
    public static Object getObject(HttpSession session, String key) {
        if (session == null) {
            throw new TimeoutException();
        }
        Object obj = session.getAttribute(key);
        if (obj == null) {
            throw new ActionException("getObject", "not bound [key=" + key + ", scope=session]");
        }
        return obj;
    }

    /**
     * Indicates that errors available in session scope
     * @param session
     * @return true if errors available
     */
    public static boolean isErrorAvailable(HttpSession session) {
        return (session.getAttribute(Globals.ERRORS) != null);
    }

    /**
     * Stores given error message under Globals.ERRORS
     * @param session
     * @param errorMessage
     */
    public static void saveError(HttpSession session, String errorMessage) {
        session.setAttribute(Globals.ERRORS, errorMessage);
    }

    /**
     * Saves given error details text under 'errorDetails' key in session scope
     * @param session
     * @param errorDetails
     */
    public static void saveErrorDetails(HttpSession session, String errorDetails) {
        session.setAttribute(
                Globals.ERROR_DETAILS,
                errorDetails);
    }

    /**
     * Saves related error id under 'errorId' key in session scope
     * @param session
     * @param errorId
     */
    public static void saveErrorId(HttpSession session, Long errorId) {
        session.setAttribute(Globals.ERROR_ID, errorId);
    }

    /**
     * Saves a lock
     * @param session
     * @param lock
     */
    public static void saveLock(HttpSession session, Lock lock) {
        session.setAttribute(Globals.OBJECT_LOCK, lock);
    }

    /**
     * Stores given status message under Globals.STATUS_MESSAGE
     * @param session
     * @param message
     */
    public static void saveStatusMessage(HttpSession session, String message) {
        session.setAttribute(Globals.STATUS_MESSAGE, message);
    }

    /**
     * Saves related message of given message key in session scope
     * @param request where message should saved
     * @param key with i18n error
     */
    public static void saveMessage(HttpServletRequest request, String key) {
        request.getSession(true).setAttribute(
                Globals.MESSAGE,
                RequestUtil.getResourceString(request, key));
    }

    /**
     * Saves related message of given message key in session scope
     * @param session
     */
    public static void removeMessage(HttpSession session) {
        session.removeAttribute(Globals.MESSAGE);
    }

    /**
     * Checks wether an action string is available in the session scope. This is determined if an object exists under key searchResult
     * @param session
     * @return true if so
     */
    public static boolean isActionAvailable(HttpSession session) {
        return (session.getAttribute(Globals.ACTION) != null);
    }

    /**
     * Fetches the current action string from the session context
     * @param session
     * @return action string
     */
    public static String getAction(HttpSession session) {
        return (String) getObject(session, Globals.ACTION);
    }

    /**
     * Puts the specified string as current action in the session context
     * @param session
     * @param action to save
     */
    public static void setAction(HttpSession session, String action) {
        if (session == null) {
            throw new TimeoutException();
        }
        session.setAttribute(Globals.ACTION, action);
    }

    /**
     * Removes the current action object from session context
     * @param session
     */
    public static void removeAction(HttpSession session) {
        if (session == null) {
            throw new TimeoutException();
        }
        session.removeAttribute(Globals.ACTION);
    }

    public static void setTarget(HttpSession session, String target) {
        session.setAttribute(Globals.TARGET, target);
    }

    public static void removeTarget(HttpSession session) {
        session.removeAttribute(Globals.TARGET);
    }

    public static boolean isTargetAvailable(HttpSession session) {
        return (session.getAttribute(Globals.TARGET) != null);
    }

    public static String getTarget(HttpSession session) {
        return (String) session.getAttribute(Globals.TARGET);
    }

    //     LAST REQUESTED ID HELPERS

    public static boolean isLastRequestedIdAvailable(HttpSession session) {
        return (session.getAttribute(Globals.LAST_REQUESTED_ID) != null);
    }

    public static Long getLastRequestedIdAsLong(HttpSession session) {
        return (Long) getObject(session, Globals.LAST_REQUESTED_ID);
    }

    public static Integer getLastRequestedIdAsInteger(HttpSession session) {
        return (Integer) getObject(session, Globals.LAST_REQUESTED_ID);
    }

    public static void setLastRequestedId(HttpSession session, Long id) {
        if (id != null) {
            session.setAttribute(Globals.LAST_REQUESTED_ID, id);
        }
    }

    public static void setLastRequestedId(HttpSession session, Integer id) {
        if (id != null) {
            session.setAttribute(Globals.LAST_REQUESTED_ID, id);
        }
    }

    public static void removeLastRequestedId(HttpSession session) {
        session.removeAttribute(Globals.LAST_REQUESTED_ID);
    }

    public static final Document getITextDocument(HttpSession session) {
        return (Document) getObject(session, ITEXT_DOC);
    }
    
    public static final void setITextDocument(HttpSession session, Document document) {
        session.setAttribute(ITEXT_DOC, document);
    }

    // MAX COUNT PER PAGE

    public static boolean isMaxCountPerPageAvailable(HttpSession session) {
        return (session.getAttribute(Globals.MAX_COUNT_PER_PAGE) != null);
    }

    public static void setMaxCountPerPage(HttpSession session, Integer count) {
        if (count != null) {
            session.setAttribute(Globals.MAX_COUNT_PER_PAGE, count);
        }
    }

    public static Integer getMaxCountPerPage(HttpSession session) {
        return (Integer) getObject(session, Globals.MAX_COUNT_PER_PAGE);
    }

    public static void removeMaxCountPerPage(HttpSession session) {
        session.removeAttribute(Globals.MAX_COUNT_PER_PAGE);
    }

    public static boolean isSet(String value) {
        return (value != null && value.length() > 1);
    }

    public static void disableShareMenu(ServletContext ctx) {
        removeContextObject(ctx, "shareMenuEnabled");
    }

    public static void enableShareMenu(ServletContext ctx) {
        addContextObject(ctx, "shareMenuEnabled", Boolean.TRUE);
    }

    public static boolean isShareMenu(HttpSession session) {
        Object flag = fetchContextObject(session.getServletContext(), "shareMenuEnabled");
        if (flag instanceof Boolean) {
            return ((Boolean) flag).booleanValue();
        }
        return false;
    }

    public static void addContextObject(ServletContext ctx, String name, Serializable object) {
        ctx.setAttribute(name, object);
    }

    public static void removeContextObject(ServletContext ctx, String name) {
        ctx.removeAttribute(name);
    }

    public static Object fetchContextObject(ServletContext ctx, String name) {
        return ctx.getAttribute(name);
    }

    public static boolean isContextObjectEnabled(ServletContext ctx, String name) {
        Object obj = ctx.getAttribute(name);
        if (obj instanceof Boolean) {
            return ((Boolean) obj).booleanValue();
        }
        if (obj instanceof String) {
            String bv = (String) obj;
            if ("true".equalsIgnoreCase(bv)
                    || "yes".equalsIgnoreCase(bv)) {
                return true;
            }
        }
        return false;
    }

    public static WebConfig fetchWebConfig(HttpSession session) {
        Object wcfg = fetchContextObject(session.getServletContext(), Globals.WEB_CONFIG);
        if (wcfg instanceof WebConfig) {
            return (WebConfig) wcfg;
        }
        return null;
    }

    public static WebConfig getWebConfig(HttpSession session) {
        WebConfig result = fetchWebConfig(session);
        if (result == null) {
            throw new ActionException("getWebConfig() not found [context=servlet, key="
                    + Globals.WEB_CONFIG + "]");
        }
        Object wcfg = fetchContextObject(session.getServletContext(), Globals.WEB_CONFIG);
        if (wcfg instanceof WebConfig) {
            return (WebConfig) wcfg;
        }
        return null;
    }

    public static String getSupportMailAddress(HttpSession session) {
        String email = Constants.DEFAULT_SUPPORT_ADDRESS;
        WebConfig config = fetchWebConfig(session);
        if (config != null 
                && EmailValidator.validate(config.getSupportMailAddress())) {
            email = config.getSupportMailAddress();
        }
        return email;
    }
}
