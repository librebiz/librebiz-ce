<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<c:set var="view" value="${sessionScope.stocktakingView}" />
<o:form name="stocktakingForm" url="/stocktakings.do">
    <input type="hidden" name="method" value="save" />
    <div class="col-md-6 panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4><fmt:message key="createNewStockTaking" /></h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-body">

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="stockTakingName" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" name="name" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="stockTakingType" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <select name="type" class="form-control" size="1">
                                <c:forEach var="type" items="${view.types}">
                                    <option value="<o:out value="${type.id}"/>"><o:out value="${type.name}" /></option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="recordDate"><fmt:message key="deadline" /></label>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <input type="text" name="recordDate" id="recordDate" class="form-control" />
                        </div>
                    </div>
                    <div class="col-md-5">
                        <o:datepicker input="recordDate"/>
                    </div>
                </div>

                <div class="row next">
                    <div class="col-md-4"></div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="button" class="form-control cancel" onclick="gotoUrl('<c:url value="/stocktakings.do?method=disableCreate"/>');" value="<fmt:message key="break"/>" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <o:submit styleClass="form-control" value="create" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</o:form>
