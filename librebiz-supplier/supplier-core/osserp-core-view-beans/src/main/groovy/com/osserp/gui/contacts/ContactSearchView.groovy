/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.gui.contacts

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.core.Options
import com.osserp.core.contacts.ContactSearchResult
import com.osserp.core.contacts.ContactType

import com.osserp.groovy.web.MenuItem

/**
 *
 * @author tn <tn@osserp.com>
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class ContactSearchView extends AbstractContactSearchView {
    private static Logger logger = LoggerFactory.getLogger(ContactSearchView.class.getName())

    List groups = [
        'contact',
        'customer',
        'supplier',
        'employee',
        'client',
        'branch',
        'other'
    ]
    String defaultGroup = groups[0]
    String selectedGroup = defaultGroup
    Boolean groupSelectionEnabled = true

    List<ContactType> contactTypes = []
    Long defaultType = 0L
    Long selectedType = defaultType

    List columns = ['address', 'lastname', 'firstname', 'email', 'zipcode', 'city']
    String defaultColumn = columns[0]
    String selectedColumn = defaultColumn

    String selectedValue

    List customerTypes = []
    Long selectedCustomerType
    List customerStatus = []
    Long selectedCustomerStatus
    boolean initialSearch = true
    boolean startsWith = false
    boolean supplierSearchIncludeEmployees = false

    /**
     * Creates a new contactSearchView preloading customerTypes, customerStatus
     * and contactTypes. Autoreload and providingPhoneSearch will be enabled. 
     */
    ContactSearchView() {
        super()
        headerName = 'contactSearch'
        providesCustomNavigation()
        env.providingPhoneSearch = true
        enableAutoreload()
    }

    /**
     * Configures contactSearchView with dedicated properties. 
     * @param group logical name of the contact group if dedicated group search 
     * @param target the selection target url
     * @param exit the selection exit target
     * @param providingPhoneSearch indicates if view provides phone search link
     * @param run perform an initial search after setting properties if true
     */
    void configure(String group, String target, String exit, boolean providingPhoneSearch, boolean run) {
        selectedGroup = group
        env.selectionTarget = createTarget(target)
        env.selectionExit = selectionExit
        env.providingPhoneSearch = providingPhoneSearch
        enableAutoreload()
        if (run) {
            list = filterResult(contactSearch.findFirstRows(selectedGroup))
            logger.debug("configure: done [count=${list.size()}]")
        }
    }

    Boolean getProvidingPhoneSearch() {
        env.providingPhoneSearch
    }

    Boolean getCustomerSearch() {
        (selectedGroup == 'customer')
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            customerTypes = getOptions(Options.CUSTOMER_TYPES)
            customerStatus = getOptions(Options.CUSTOMER_STATUS)
            contactTypes = getOptions(Options.CONTACT_TYPES)

            startsWith = isUserPropertyEnabled('contactSearchStartsWith')
            def searchContext = form.getString('context')
            if (searchContext) {
                selectedGroup = searchContext
                groupSelectionEnabled = false
            }

            selectionTargetIdParam = form.getString('selectionTargetIdParam')

            def selectionTarget = form.getString('selectionTarget')
            if (selectionTarget) {
                env.selectionTarget = createTarget(selectionTarget)
            } else {
                env.selectionTarget = createTarget('/contacts.do?method=load')
            }
            def selectionExit = form.getString('selectionExit')
            if (selectionExit) {
                if (selectionExit != 'ignore') {
                    env.selectionExit = selectionExit
                }
            } else {
                env.selectionExit = 'contactSearchResult'
            }
            String selectionTitle = form.getString('selectionTitle')
            if (selectionTitle) {
                headerName = selectionTitle
            }
            logger.debug("initRequest: forward request found [selectionTarget=${env.selectionTarget}, selectionExit=${env.selectionExit}]")
            list = filterResult(contactSearch.findFirstRows(selectedGroup))
            logger.debug("initRequest: done [resultCount=${list.size()}]")
        }
    }

    void selectGroup() {
        selectedGroup = form.getString('group')
        reload()
    }

    @Override
    void save() {
        selectedColumn = form.getString('method')
        selectedType = form.getLong('type')
        selectedValue = form.getString('value')
        //selectedGroup = form.getString('group')
        startsWith = form.getBoolean('startsWith')
        logger.debug("save: invoked [column=$selectedColumn, type=$selectedType, value=$selectedValue, startsWith=$startsWith, group=$selectedGroup]")
        if (customerSearch) {
            selectedCustomerType = form.getLong('customerType')
            selectedCustomerStatus = form.getLong('customerStatus')
            logger.debug("save: customer context enabled [customerType=${selectedCustomerType}, customerStatus=${selectedCustomerStatus}]")
        }
        initialSearch = false
        search()
    }

    @Override
    void reload() {
        logger.debug("reload: invoked")
        if (initialSearch) {
            searchInitial()
        } else {
            search()
        }
    }

    /**
     * Filters search result in inherited views if required.
     * @return result list as provided by default or filtered list if overridden
     */
    protected List filterResult(List result) {
        result
    }

    private void search() {
        list = filterResult(contactSearch.find(selectedGroup, selectedColumn, selectedType, selectedValue, startsWith))
        logger.debug("search: done [resultCount=${list.size()}]")
        finalizeSearchResult()
    }

    private void searchInitial() {
        list = filterResult(contactSearch.findFirstRows(selectedGroup))
        logger.debug("searchInitial: done [resultCount=${list.size()}]")
        finalizeSearchResult()
    }

    private void finalizeSearchResult() {
        if (customerSearch) {
            if (selectedCustomerType || selectedCustomerStatus) {
                for (Iterator i = list.iterator(); i.hasNext();) {
                    ContactSearchResult next = i.next()
                    Boolean removed = false
                    if (selectedCustomerType) {
                        if (selectedCustomerType != next.customerType) {
                            logger.debug("search: removed not matching customer [id=${next.id}, type=${next.customerType}]")
                            i.remove()
                            removed = true
                        }
                    }
                    if (!removed && selectedCustomerStatus) {
                        if (selectedCustomerStatus != next.customerStatus) {
                            logger.debug("search: removed not matching customer [id=${next.id}, status=${next.customerStatus}]")
                            i.remove()
                            removed = true
                        }
                    }
                    if (!removed) {
                        logger.debug("search: kept matching customer [id=${next.id}, type=${next.customerType}, status=${next.customerStatus}]")
                    }
                }
            }
        } else if (selectedGroup == 'employee') {
            for (Iterator i = list.iterator(); i.hasNext();) {
                ContactSearchResult next = i.next()
                if (next.client) {
                    i.remove()
                }
            }

        } else if (selectedGroup == 'supplier' && !supplierSearchIncludeEmployees) {
            for (Iterator i = list.iterator(); i.hasNext();) {
                ContactSearchResult next = i.next()
                if (next.employee) {
                    i.remove()
                }
            }

        } else {
            selectedCustomerType = null
            selectedCustomerStatus = null
        }
    }


    @Override
    void createCustomNavigation() {
        nav.listNavigation = [ exitLink, phoneNumberSearchLink, homeLink ]
        nav.defaultNavigation = [ exitLink, phoneNumberSearchLink, homeLink ]
    }
    
    MenuItem getPhoneNumberSearchLink() {
        navigationLink("/contacts/phoneNumberSearch/forward?exit=/contacts/contactSearch/reload", 'phoneIcon', 'phoneNumberSearch')
    }

}
