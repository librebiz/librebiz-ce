/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 25, 2008 5:46:55 PM 
 * 
 */
package com.osserp.core.sales;

import com.osserp.core.BusinessCase;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SalesRevenue {

    /**
     * Indicates that revenue is a post calculation revenue
     * @return postCalculation true if revenue is a result of salesRevenue.postCalculationRevenue
     */
    boolean isPostCalculation();

    /**
     * Provides the post calculation if available (revenueByItems and businessCase.status >= 95)
     * @return postCalculation
     */
    SalesRevenue getPostCalculationRevenue();

    /**
     * Provides the related businessCase
     * @return businessCase
     */
    BusinessCase getBusinessCase();

    /**
     * Indicates that implementing class provides dedicated lists of items for all cost types
     * @return providesItems
     */
    boolean isProvidesItems();

    /**
     * Indicates hardwareCostsA availability
     * @return
     */
    boolean isHardwareCostsAAvailable();

    /**
     * Provides hardware class A costs
     * @return hardwareCostsA
     */
    Double getHardwareCostsA();

    /**
     * Provides hardware class A costs description
     * @return hardwareCostsADescription
     */
    String getHardwareCostsADescription();

    /**
     * Provides hardware class A costs
     * @return hardwareCostsAPercent
     */
    Double getHardwareCostsAPercent();

    /**
     * Indicates hardwareCostsB availability
     * @return
     */
    boolean isHardwareCostsBAvailable();

    /**
     * Provides hardware class B costs
     * @return hardwareCostsB
     */
    Double getHardwareCostsB();

    /**
     * Provides hardware class B costs description
     * @return hardwareCostsBDescription
     */
    String getHardwareCostsBDescription();

    /**
     * Provides hardware class B costs percent
     * @return hardwareCostsBPercent
     */
    Double getHardwareCostsBPercent();

    /**
     * Indicates hardwareCostsC availability
     * @return
     */
    boolean isHardwareCostsCAvailable();

    /**
     * Provides hardware class C costs
     * @return hardwareCostsC
     */
    Double getHardwareCostsC();

    /**
     * Provides hardware class C costs description
     * @return hardwareCostsCDescription
     */
    String getHardwareCostsCDescription();

    /**
     * Provides hardware class C costs (e.g. rack)
     * @return hardwareCostsCPercent
     */
    Double getHardwareCostsCPercent();

    /**
     * Indicates accessoryCosts availability
     * @return
     */
    boolean isAccessoryCostsAvailable();

    /**
     * Provides accessory costs
     * @return accessoryCosts
     */
    Double getAccessoryCosts();

    /**
     * Provides accessory costs description
     * @return accessoryCostsDescription
     */
    String getAccessoryCostsDescription();

    /**
     * Provides accessory costs
     * @return accessoryCostsPercent
     */
    Double getAccessoryCostsPercent();

    /**
     * Indicates serviceCosts availability
     * @return
     */
    boolean isServiceCostsAvailable();

    /**
     * Provides service costs
     * @return serviceCosts
     */
    Double getServiceCosts();

    /**
     * Provides service costs description
     * @return serviceCostsDescription
     */
    String getServiceCostsDescription();

    /**
     * Provides service costs (e.g. installation, mounting, etc.)
     * @return serviceCostsPercent
     */
    Double getServiceCostsPercent();

    /**
     * Indicates project costs availability
     * @return
     */
    boolean isProjectCostsAvailable();

    /**
     * Provides project costs
     * @return projectCosts
     */
    Double getProjectCosts();

    /**
     * Provides service costs (e.g. installation, mounting, etc.)
     * @return serviceCostsPercent
     */
    Double getProjectCostsPercent();

    /**
     * Indicates unknown costs availability
     * @return
     */
    boolean isUnknownCostsAvailable();

    /**
     * Provides unknown costs
     * @return unknownCosts
     */
    Double getUnknownCosts();

    /**
     * Provides unknown costs (e.g. not matching any other cost group)
     * @return unknownCostsPercent
     */
    Double getUnknownCostsPercent();

    /**
     * Indicates agent costs availability
     * @return
     */
    boolean isAgentCostsAvailable();

    /**
     * Provides the costs of a sales agent if involved
     * @return agentCosts
     */
    Double getAgentCosts();

    /**
     * Provides the costs of a sales agent if involved
     * @return agentCostsPercent
     */
    Double getAgentCostsPercent();

    /**
     * Indicates sales costs availability
     * @return
     */
    boolean isSalesCostsAvailable();

    /**
     * Provides main sales costs
     * @return salesCosts
     */
    Double getSalesCosts();

    /**
     * Provides main sales costs
     * @return salesCostsPercent
     */
    Double getSalesCostsPercent();

    /**
     * Indicates tip costs availability
     * @return
     */
    boolean isTipCostsAvailable();

    /**
     * Provides the costs of a tip provider if involved
     * @return tipCosts
     */
    Double getTipCosts();

    /**
     * Provides the costs of a tip provider if involved
     * @return tipCostsPercent
     */
    Double getTipCostsPercent();

    /**
     * Costs without sales, agent costs & unknown costs
     * @return costs
     */
    Double getCosts();

    /**
     * All costs without sales & agent costs
     * @return netCosts
     */
    Double getNetCosts();

    /**
     * Costs without sales & agent costs in percent
     * @return costsPercent
     */
    Double getCostsPercent();

    /**
     * Costs including sales and agent costs
     * @return totalCosts
     */
    Double getTotalCosts();

    /**
     * Costs including sales and agent costs
     * @return totalCostsPercent
     */
    Double getTotalCostsPercent();

    /**
     * All costs
     * @return totalCosts
     */
    Double getFinalCosts();

    /**
     * All costs percent
     * @return finalCostsPercent
     */
    Double getFinalCostsPercent();

    /**
     * Provides the sales credit volume
     * @return salesCreditVolume
     */
    Double getSalesCreditVolume();

    /**
     * Sales volume as provided by record.getAmounts().getAmount()
     * @return salesVolume
     */
    Double getSalesVolume();

    /**
     * Sales volume as provided by record.getAmounts().getAmount()
     * @return salesVolumePercent
     */
    Double getSalesVolumePercent();

    /**
     * Provides the profit ignoring sales & agent costs
     * @return profit
     */
    Double getProfit();

    /**
     * Provides the profit ignoring sales & agent costs
     * @return profitPercent
     */
    Double getProfitPercent();

    /**
     * Provides the total profit including sales & agent costs
     * @return totalProfit
     */
    Double getTotalProfit();

    /**
     * Provides the total profit including sales & agent costs
     * @return totalProfitPercent
     */
    Double getTotalProfitPercent();

    /**
     * Provides the final profit including all costs
     * @return finalProfit
     */
    Double getFinalProfit();

    /**
     * Provides the final profit including all costs
     * @return finalProfitPercent
     */
    Double getFinalProfitPercent();

    /**
     * Provides the minimal required margin
     * @return minimalMarginPercent
     */
    Double getMinimalMarginPercent();

    /**
     * Provides the minimal required margin
     * @return missingMarginPercent
     */
    Double getMissingMarginPercent();

    /**
     * Provides the target margin
     * @return targetMarginPercent
     */
    Double getTargetMarginPercent();

    /**
     * Provides the target volume
     * @return targetVolume
     */
    Double getTargetVolume();

    /**
     * Indicates that profit is lower required margin
     * @return profitLowerMinimum
     */
    boolean isProfitLowerMinimum();

    /**
     * Indicates that profit is lower target margin
     * @return profitLowerTarget
     */
    boolean isProfitLowerTarget();
}
