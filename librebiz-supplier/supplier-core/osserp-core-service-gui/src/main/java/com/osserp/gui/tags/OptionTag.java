/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Apr 7, 2010 
 * 
 */
package com.osserp.gui.tags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;

import com.osserp.common.util.NumberUtil;
import com.osserp.common.web.tags.AbstractSimpleValueTag;

/**
 * 
 * @author eh <eh@osserp.com>
 */
public class OptionTag extends AbstractSimpleValueTag {
    private String name = null;

    public void setName(String name) {
        this.name = getResourceString(name);
    }

    @Override
    public void doTag() throws JspException {
        SelectTag parent;
        if (!(getParent() instanceof SelectTag)) {
            throw new JspTagException("The option tag must not be used outside the select tag");
        }
        parent = (SelectTag) getParent();
        if (parent.gainPermission()) {
            parent.putOption(NumberUtil.createLong(value), name);
            parent.subtagSucceeded();
        }
    }

}
