<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<!DOCTYPE html>
<html lang="de">
<head>
<jsp:include page="/${applicationScope.webConfig.layoutDefaultPath}/shared/head.jsp" />
<jsp:include page="/${applicationScope.webConfig.layoutDefaultPath}/shared/head_default_css.jsp" />
<jsp:include page="/${applicationScope.webConfig.layoutDefaultPath}/shared/head_default_custom_css.jsp" />
<tiles:getAsString name="styles" ignore="true" />
<tiles:insert attribute="javascript" ignore="true" />
<title><tiles:getAsString name="title" ignore="true" /></title>
</head>
<body>
    <jsp:include page="/${applicationScope.webConfig.layoutDefaultPath}/shared/header_public.jsp" />
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
                <div class="navlist">
                    <ul class="nav nav-sidebar">
                    </ul>
                </div>
            </div>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                <div class="page-header">
                    <div id="content_header">
                        <div id="content_header_left"></div>
                        <div id="content_header_middle">
                            <tiles:getAsString name="error" ignore="true" />
                        </div>
                        <div id="content_header_right">
                            <div class="contentnav">
                                <tiles:getAsString name="headline_right" ignore="true" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-area" id="errorContent">
                    <div class="row">
                        <div class="col-md-6 panel-area panel-area-default">
                            <div class="panel-body" style="padding-top:2em;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="alert alert-danger" role="alert">
                                            <tiles:getAsString name="message" ignore="true" />
                                        </div>
                                        <br/>
                                        <p><a class="btn btn-primary btn-lg" role="button" href="<v:url value="/login/forward"/>">Home</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- container-fluid -->
</body>
</html>
