<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.purchaseOrderView}">
<o:logger write="order_product_edit.jsp no purchaseOrderView found!" level="debug"/>
<c:redirect url="/errors/error_context.jsp"/>
</c:if>
<c:set var="recordView" scope="session" value="${sessionScope.purchaseOrderView}"/>
<c:set var="recordView" value="${sessionScope.recordView}"/>
<c:set var="record" value="${recordView.record}"/>
<c:if test="${(empty record.items) and (param.method != 'exit')}">
<o:logger write="items empty; forwarding to selection..." level="debug"/>
<c:redirect url="/purchaseOrderProducts.do?method=forwardSelection"/>		
</c:if>
<c:set var="productUrl" scope="request" value="/purchaseOrderProducts.do"/>
<c:set var="productExitTarget" scope="request" value="purchaseOrderProductsReload"/>
<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
<tiles:put name="title"><o:displayTitle/></tiles:put>
<tiles:put name="styles" type="string">
<style type="text/css">
<c:import url="/css/records.css"/>
</style>
</tiles:put>
<tiles:put name="headline">
<span class="changeLabel"><fmt:message key="changeLabel"/>:</span>
<c:choose>
    <c:when test="${!empty record.bookingType}">
        <span><o:out value="${record.type.name}"/> - <o:out value="${record.bookingType.name}"/></span>
    </c:when>
    <c:otherwise>
        <span><o:out value="${record.type.name}"/></span>
    </c:otherwise>
</c:choose>
<span> - <o:out value="${record.number}"/> - <o:out value="${record.contact.displayName}"/></span>
</tiles:put>
<tiles:put name="headline_right">
<a href="<c:url value="/purchaseOrderProducts.do?method=exit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon"/></a>
<c:if test="${!empty record.items}">
<a href="javascript:onclick=confirmLink('<fmt:message key="deleteAllItems"/>','<c:url value="/purchaseOrderProducts.do?method=deleteItems"/>');" title="<fmt:message key="deleteAllItems"/>"><o:img name="deleteIcon"/></a>
</c:if>
<v:link url="/index" title="backToMenu"><o:img name="homeIcon"/></v:link>
</tiles:put>

<tiles:put name="content" type="string">
<div class="subcolumns">
    <div class="subcolumn">
        <c:import url="/pages/records/record_items.jsp"/>
    </div>
</div>
</tiles:put> 
</tiles:insert>
<c:remove var="recordView" scope="session"/>
