/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 18, 2011 9:26:55 AM 
 * 
 */
package com.osserp.gui.selections

import javax.servlet.http.HttpServletRequest

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.groovy.web.PopupViewController

/**
 *
 * @author cf <cf@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
@RequestMapping("/selections/productClassificationSelection/*")
@Controller class ProductClassificationSelectionController extends PopupViewController {
    private static Logger logger = LoggerFactory.getLogger(ProductClassificationSelectionController.class.getName())

    @RequestMapping
    def selectStock(HttpServletRequest request) {
        logger.debug("selectStock: invoked [user=${getUserId(request)}]")
        ProductClassificationSelectionView view = getView(request)
        view.selectStock()
        return defaultPage
    }

    @RequestMapping
    def selectType(HttpServletRequest request) {
        logger.debug("selectType: invoked [user=${getUserId(request)}]")
        ProductClassificationSelectionView view = getView(request)
        view.selectType()
        return defaultPage
    }

    @RequestMapping
    def selectGroup(HttpServletRequest request) {
        logger.debug("selectGroup: invoked [user=${getUserId(request)}]")
        ProductClassificationSelectionView view = getView(request)
        view.selectGroup()
        return defaultPage
    }

    @RequestMapping
    def selectCategory(HttpServletRequest request) {
        logger.debug("selectCategory: invoked [user=${getUserId(request)}]")
        ProductClassificationSelectionView view = getView(request)
        view.selectCategory()
        return defaultPage
    }
}
