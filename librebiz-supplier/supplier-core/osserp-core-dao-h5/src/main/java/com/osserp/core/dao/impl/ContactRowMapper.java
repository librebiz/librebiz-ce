/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 12, 2006 10:45:44 AM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import com.osserp.common.Option;
import com.osserp.common.OptionsCache;
import com.osserp.common.dao.AbstractRowMapper;
import com.osserp.core.Options;
import com.osserp.core.contacts.ContactType;
import com.osserp.core.contacts.Salutation;
import com.osserp.core.model.contacts.ContactImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ContactRowMapper extends AbstractRowMapper {

    private OptionsCache namesCache = null;

    public ContactRowMapper(OptionsCache namesCache) {
        this.namesCache = namesCache;
    }

    public static StringBuilder getQuery(String contactsTable) {
        StringBuilder sql = new StringBuilder();
        sql
                .append("SELECT c.contact_id, c.contact_ref, c.type_id, " +
                        "c.salutation, c.title, c.firstname, c.lastname, " +
                        "c.spouse_salutation, c.spouse_title, c.spouse_firstname, c.spouse_lastname, " +
                        "c.street, c.street_addon, c.zipcode, c.city," +
                        "c.employee, c.customer, c.supplier, c.installer, c.other, c.vip, c.client, c.branch, " +
                        "c.created,c.created_by, c.firstname_prefix " +
                        "FROM ")
                .append(contactsTable).append(" c ");
        return sql;
    }

    public Object mapRow(ResultSet rs, int index) throws SQLException {
        return new ContactImpl(
                Long.valueOf(rs.getLong("contact_id")),
                Long.valueOf(rs.getLong("contact_ref")),
                getContactType(Long.valueOf(rs.getLong("type_id"))),
                getSalutation(Long.valueOf(rs.getLong("salutation"))),
                getTitle(Long.valueOf(rs.getLong("title"))),
                rs.getBoolean("firstname_prefix"),
                rs.getString("firstname"),
                rs.getString("lastname"),
                getSalutation(Long.valueOf(rs.getLong("spouse_salutation"))),
                getTitle(Long.valueOf(rs.getLong("spouse_title"))),
                rs.getString("spouse_firstname"),
                rs.getString("spouse_lastname"),
                rs.getString("street"),
                rs.getString("street_addon"),
                rs.getString("zipcode"),
                rs.getString("city"),
                rs.getBoolean("employee"),
                rs.getBoolean("customer"),
                rs.getBoolean("supplier"),
                rs.getBoolean("installer"),
                rs.getBoolean("other"),
                rs.getBoolean("vip"),
                rs.getBoolean("client"),
                rs.getBoolean("branch"),
                rs.getTimestamp("created"),
                Long.valueOf(rs.getLong("created_by")));
    }

    protected String createPersonName(String firstName, String lastName) {
        StringBuffer name = new StringBuffer(64);
        if (!isEmpty(firstName) && !isEmpty(lastName)) {
            name.append(lastName).append(", ").append(firstName);
        } else if (!isEmpty(firstName)) {
            name.append(firstName);
        } else if (!isEmpty(lastName)) {
            name.append(lastName);
        } else {
            return "";
        }
        return name.toString();
    }
    
    protected ContactType getContactType(Long id) {
        Map names = namesCache.getMap(Options.CONTACT_TYPES);
        if (id != null && names.containsKey(id)) {
            return (ContactType) names.get(id);
        }
        return null;
    }

    protected Salutation getSalutation(Long id) {
        Map names = namesCache.getMap(Options.SALUTATIONS);
        if (id != null && names.containsKey(id)) {
            return (Salutation) names.get(id);
        }
        return null;
    }

    protected Option getTitle(Long id) {
        Map names = namesCache.getMap(Options.TITLES);
        if (id != null && names.containsKey(id)) {
            return (Option) names.get(id);
        }
        return null;
    }

    protected OptionsCache getNamesCache() {
        return namesCache;
    }
}
