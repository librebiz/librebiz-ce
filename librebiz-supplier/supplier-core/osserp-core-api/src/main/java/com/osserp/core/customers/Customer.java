/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 01.10.2004 
 * 
 */
package com.osserp.core.customers;

import com.osserp.common.Details;

import com.osserp.core.contacts.PaymentAware;
import com.osserp.core.contacts.ClassifiedContact;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Customer extends Cloneable, Details, PaymentAware, ClassifiedContact {

    static final Long TYPE_CONSUMER = 1L;
    static final Long TYPE_RESELLER = 2L;
    static final Long TYPE_POSACCOUNT = 3L;
    static final Long TYPE_CLIENT = 5L;
    static final Long TYPE_HOLDING = 6L;

    static final Long STATUS_INTEREST = 1L;
    static final Long STATUS_CUSTOMER = 2L;

    /**
     * Returns the related type
     * @return type
     */
    Long getTypeId();

    /**
     * Sets the type id
     * @param typeId
     */
    void setTypeId(Long typeId);

    /**
     * Returns the related status
     * @return status
     */
    Long getStatusId();

    /**
     * Sets the status id
     * @param statusId
     */
    void setStatusId(Long statusId);

    /**
     * The job of the customer
     * @return job
     */
    String getJob();

    /**
     * Sets the job of the customer
     * @param job
     */
    void setJob(String job);

    /**
     * Provides the vat id
     * @return vatId
     */
    String getVatId();

    /**
     * Sets the vat id
     * @param vatId
     */
    void setVatId(String vatId);

    boolean isDiscountEnabled();

    void setDiscountEnabled(boolean discountEnabled);

    Double getDiscountA();

    void setDiscountA(Double discountA);

    Double getDiscountB();

    void setDiscountB(Double discountB);

    Double getDiscountC();

    void setDiscountC(Double discountC);

    String getInvoiceBy();

    void setInvoiceBy(String invoiceBy);

    String getInvoiceEmail();

    void setInvoiceEmail(String invoiceEmail);

    /**
     * Indicates that customer is holding (implies client)
     * @return holding
     */
    boolean isHolding();

    /**
     * Indicates that customer is client
     * @return client
     */
    boolean isClient();

    /**
     * Indicates dummy customer used as pos account.
     * @return true if pos type
     */
    boolean isPos();

    /**
     * Indicates that customer is reseller
     * @return reseller
     */
    boolean isReseller();

    /**
     * Indicates that customer has a website account
     * @return websiteAccountAvailable
     */
    boolean isWebsiteAccountAvailable();

    /**
     * Indicates that website account is active
     * @return websiteAccountEnabled
     */
    boolean isWebsiteAccountEnabled();

    void setWebsiteAccountEnabled(boolean websiteAccountAvailable);

    /**
     * Provides the website account name. Defaults to email address.
     * @return websiteAccountName
     */
    String getWebsiteAccountName();

    /**
     * Provides the website account password
     * @return websiteAccountPassword
     */
    String getWebsiteAccountPassword();

    /**
     * Provides the website account contact id if account is a company
     * @return websiteAccountContact
     */
    Long getWebsiteAccountContact();

    /**
     * Provides the website account contact id if account is a company
     * @return websiteAccountContact
     */
    void setWebsiteAccountContact(Long websiteAccountContact);

    /**
     * Initializes the website account
     * @param websiteAccountName
     */
    void setWebsiteAccount(String websiteAccountName);

    /**
     * Updates the website account values
     * @param websiteAccountName
     * @param websiteAccountPassword as clear text
     * @throws ClientException if any required value missing or invalid
     */
    void updateWebsiteAccount(String websiteAccountName, String websiteAccountPassword);

    /**
     * Checks if a clear text password matches current password hash
     * @param password in clear text
     * @return true if current password hash equals given password's hash
     */
    boolean checkWebsitePassword(String password);

    /**
     * Indicates that customer is an internal customer (client)
     * @return internal
     */
    boolean isInternal();

    /**
     * Sets that customer is an internal customer
     * @param internal
     */
    void setInternal(boolean internal);

    /**
     * Indicates that customer is an agent
     * @return agent
     */
    boolean isAgent();

    /**
     * Sets that customer is an agent
     * @param agent
     */
    void setAgent(boolean agent);

    /**
     * Provides the sales commission of the agent if set. Value is a fixed amount or percentage value depending on agentCommissionPercent flag
     * @return agentCommission
     */
    Double getAgentCommission();

    /**
     * Sets the agents commission
     * @param agentCommission
     */
    void setAgentCommission(Double agentCommission);

    /**
     * Indicates that agents sales commission is interpreted as percentage value
     * @return agentCommissionPercent
     */
    boolean isAgentCommissionPercent();

    /**
     * Sets agent commission is interpreted as percentage value
     * @param agentCommissionPercent
     */
    void setAgentCommissionPercent(boolean agentCommissionPercent);

    /**
     * Indicates that customer is a tip provider
     * @return tipProvider
     */
    boolean isTipProvider();

    /**
     * Enables/disables tip provider flag
     * @param tipProvider
     */
    void setTipProvider(boolean tipProvider);

    /**
     * Provides the sales commission of the agent if set. Value is a fixed amount.
     * @return tipCommission
     */
    Double getTipCommission();

    /**
     * Sets the tip commission
     * @param tipCommission
     */
    void setTipCommission(Double tipCommission);

    /**
     * Provides the customer values as tab separated string
     * @return xls
     */
    String getXls();

    Object clone();
}
