/**
 *
 * Copyright (C) 2013 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 16, 2013 at 23:18:44 PM 
 * 
 */
package com.osserp.gui.admin.employees

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.core.employees.EmployeeGroup
import com.osserp.core.employees.EmployeeGroupManager

import com.osserp.groovy.web.MenuItem

import com.osserp.gui.CoreView

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class EmployeeGroupConfigView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(EmployeeGroupConfigView.class.getName())
    private static final String[] EDIT_PERMISSIONS = [ 'runtime_config', 'executive_saas' ] 
         
    EmployeeGroupConfigView() {
        providesCreateMode()
        providesEditMode()
        providesSetupMode()
        enableAutoreload()
        headerName = 'employeeGroups'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        if (forwardRequest) {
            reload()
        }
    }

    @Override
    void save() {
        if (createMode) {
            String name = form.getString('name')
            String key = form.getString('key')
            EmployeeGroup createdGroup = employeeGroupManager.createGroup(domainEmployee, name, key)
            list = employeeGroupManager.groups
            if (createdGroup?.id) {
                bean = list.find { it.id == createdGroup.id }
                disableCreateMode()
            } else {
                logger.warn('save: employeeGroupManager.createGroup returned empty result!')
            }
        } else {
            EmployeeGroup group = bean
            if (group) {
                group.setKey(form.getString('key'))
                group.setLdapGroupIgnoreGid(form.getBoolean('ldapGroupIgnoreGid'))
                group.setLdapGroupwareGroup(form.getBoolean('ldapGroupwareGroup'))
                group.setFlowControlAware(form.getBoolean('flowControlAware'))
                group.setIgnoringBranch(form.getBoolean('ignoringBranch'))
                group.setBranchOnly(form.getBoolean('branchOnly'))
                group.setExecutive(form.getBoolean('executive'))
                group.setExecutiveCompany(form.getBoolean('executiveCompany'))
                group.setLogistics(form.getBoolean('logistics'))
                group.setSales(form.getBoolean('sales'))
                group.setTechnician(form.getBoolean('technician'))
                group.setIt(form.getBoolean('it'))
                group.setAccounting(form.getBoolean('accounting'))
                group.setCs(form.getBoolean('cs'))
                group.setHrm(form.getBoolean('hrm'))
                group.setInstallation(form.getBoolean('installation'))
                group.setPurchasing(form.getBoolean('purchasing'))
                group.setWholesale(form.getBoolean('wholesale'))
                group.setOm(form.getBoolean('om'))
                employeeGroupManager.save(group)
            }
            disableEditMode()
        }
    }

    @Override
    MenuItem getSetupLink() {
        new MenuItem(
                link: "${context.fullPath}/admin/employees/employeeGroupPermission/forward?exit=/admin/employees/employeeGroupConfig/reload",
                title: 'employeeGroupPermissions',
                icon: 'configureIcon')
    }

    protected void createCustomNavigation() {
        nav.listNavigation = [ exitLink, createLink, homeLink ]
        if (isPermissionGrant(EDIT_PERMISSIONS)) {
            nav.beanListNavigation = [ selectExitLink, editLink, setupLink, homeLink ]
        } else {
            nav.beanListNavigation = [ selectExitLink, setupLink, homeLink ]
        } 
    }

    @Override
    void reload() {
        list = employeeGroupManager.groups
    }

    private EmployeeGroupManager getEmployeeGroupManager() {
        getService(EmployeeGroupManager.class.getName())
    }
}
