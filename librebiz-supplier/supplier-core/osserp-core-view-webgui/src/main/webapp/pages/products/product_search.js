
function initSearch() {
  if (!document.getElementById) {
    return false;
  }
  try {
    var f = document.getElementById('productSearchForm');
    var u = f.elements[0];
    f.setAttribute("autocomplete", "off");
    u.focus();
  } catch (e) {
    return false;
  }
}

function checkInput() {
	try {
		if ($('showAll')) {
			if($('showAll').checked == true) {
				if ($('value')) {
					ojsCommon.setDisabled('value', true);
				}
			} else {
				if ($('value')) {
					ojsCommon.setDisabled('value', false);
				}
			}
		}
	} catch (e) {
		return true;
	}
}

function checkCheckbox() {
	try {
		if ($('value') && $('showAll')) {
			if($('value').value != '') {
					$('showAll').checked = false;
			}
		}
	} catch (e) {
		return true;
	}
}

function checkShowAll() {
	try {
		if ($('showAll')) {
			if($('showAll').checked == true) {
				if ($('noClassification')) {
					$('noClassification').checked = false;
				}
			}
		}
	} catch (e) {
		return true;
	}
}

function checkNoClassification() {
	try {
		if ($('noClassification')) {
			if($('noClassification').checked == true) {
				if ($('showAll')) {
					$('showAll').checked = false;
				}
			}
		}
	} catch (e) {
		return true;
	}
}

function checkSearchException() {
	try {
		//noClassification? then disable groups, types, categorys and prepared selections
		if ($('noClassification')) {
			if($('noClassification').checked == true) {
				//TYPES
				if ($('typeId')) $('typeId').disabled = 'disabled';
				if ($('typeIdFake')) $('typeIdFake').disabled = 'disabled';
				//GROUPS
				if ($('groupId')) $('groupId').disabled = 'disabled';
				if ($('groupIdFake')) $('groupIdFake').disabled = 'disabled';
				//CATEGORIES
				if ($('categoryId')) $('categoryId').disabled = 'disabled';
				if ($('categoryIdFake')) $('categoryIdFake').disabled = 'disabled';
			} else {
				//TYPES
				if ($('typeId')) {
					$('typeId').disabled = '';
					$('typeId').enabled = 'enabled';
				}
				if ($('typeIdFake')) {
					$('typeIdFake').disabled = '';
					$('typeIdFake').enabled = 'enabled';
				}
				//GROUPS
				if ($('groupId') && $('typeId') && !$('typeId').value.endsWith('?method=updateClassification')) {
					$('groupId').disabled = '';
					$('groupId').enabled = 'enabled';
				}
				if ($('groupIdFake') && $('typeIdFake') && !$('typeIdFake').value.endsWith('?method=updateClassification')) {
					$('groupIdFake').disabled = '';
					$('groupIdFake').enabled = 'enabled';
				}
				//CATEGORIES
				if ($('categoryId') && $('groupId') && !$('groupId').value.endsWith('?method=updateClassification')) {
					$('categoryId').disabled = '';
					$('categoryId').enabled = 'enabled';
				}
				if ($('categoryIdFake') && $('groupIdFake') && !$('groupIdFake').value.endsWith('?method=updateClassification')) {
					$('categoryIdFake').disabled = '';
					$('categoryIdFake').enabled = 'enabled';
				}
			}
		}
		if ($('showAll') && $('value')) {
			checkInput();
			minChars = 2;
			if ($('showAll').checked != true && $('value').value.length < minChars) {
				if ($('ajaxContent')) {
					$('ajaxContent').innerHTML = "<div style='width: 100%;'><div style='border:1px solid #D6DDE6; padding:10px; background:#FFFFFB;'>"+ ojsI18n.min_character_count + minChars +"</div></div>";
				}
				return false;
			}
		}
		if ($('ajaxContent')) {
			$('ajaxContent').innerHTML = "<div style='width: 100%; text-align: center;'><div style='text-align:center; border:1px solid #D6DDE6; padding:10px; background:#FFFFFB;'><img src='/osdb/img/ajax_loader.gif' style='margin:5px;'/><br/>"+ ojsI18n.loading+"</div></div>";
		}
		return true;
	} catch (e) {
		return true;
	}
}
