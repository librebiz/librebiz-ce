/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 1, 2007 9:02:47 AM 
 * 
 */
package com.osserp.core.products;

import java.util.Date;

import com.osserp.common.beans.AbstractEntity;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SerialDisplay extends AbstractEntity {

    private Long recordId = null;
    private Long productId = null;
    private String name = null;
    private String serial = null;
    private boolean out = false;

    public SerialDisplay(
            Long recordId,
            Long productId,
            String name,
            Long id,
            String serial,
            Date created,
            Long createdBy,
            boolean out) {
        super(id, (Long) null, created, createdBy);
        this.recordId = recordId;
        this.productId = productId;
        this.name = name;
        this.serial = serial;
        this.out = out;
    }

    public Long getRecordId() {
        return recordId;
    }

    public Long getProductId() {
        return productId;
    }

    public String getName() {
        return name;
    }

    public String getSerial() {
        return serial;
    }

    public boolean isOut() {
        return out;
    }

    public void setOut(boolean out) {
        this.out = out;
    }

    // readonly bean, protected setters

    protected void setProductId(Long productId) {
        this.productId = productId;
    }

    protected void setName(String name) {
        this.name = name;
    }

    protected void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    protected void setSerial(String serial) {
        this.serial = serial;
    }

}
