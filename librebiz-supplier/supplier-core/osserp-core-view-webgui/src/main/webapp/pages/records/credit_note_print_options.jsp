<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<c:set var="recordView" value="${sessionScope.recordView}" />
<c:set var="record" value="${recordView.record}" />
<c:if test="${!record.historical}">
    <tr>
        <td class="boldtext"><fmt:message key="printOptions" /></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td><fmt:message key="printOptionsGlobalDefaultsLabel" /></td>
        <td align="right"><input type="checkbox" name="printOptionTypeDefaults" title="printOptionsGlobalDefaultsLabel" /></td>
    </tr>
    <tr>
        <td><fmt:message key="businessCaseIdLabel" /></td>
        <td align="right"><v:checkbox name="printBusinessCaseId" value="${record.printBusinessCaseId}" title="businessCaseIdLabel" /></td>
    </tr>
    <tr>
        <td><fmt:message key="businessCaseInfoLabel" /></td>
        <td align="right"><v:checkbox name="printBusinessCaseInfo" value="${record.printBusinessCaseInfo}" title="businessCaseInfoLabel" /></td>
    </tr>
</c:if>
