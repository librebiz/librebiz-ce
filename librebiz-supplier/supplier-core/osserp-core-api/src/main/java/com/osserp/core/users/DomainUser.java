/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 28, 2005 
 * 
 */
package com.osserp.core.users;

import com.osserp.common.PermissionException;
import com.osserp.common.User;

import com.osserp.core.BusinessCase;
import com.osserp.core.BusinessType;
import com.osserp.core.employees.Employee;
import com.osserp.core.system.BranchOffice;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface DomainUser extends User {

    /**
     * Returns the related employee
     * @return employee
     */
    Employee getEmployee();

    /**
     * Checks whether accounting permission is grant
     * @return true if so
     */
    boolean isAccounting();
    
    /**
     * Checks whether this user is an accounting executive
     * @return true if so
     */
    boolean isExecutiveAccounting();

    /**
     * Checks whether this user is customer service
     * @return true if so
     */
    boolean isCustomerService();

    /**
     * Checks whether this user is a company executive (e.g. general manager)
     * @return true if so
     */
    boolean isExecutive();

    /**
     * Checks whether this user is a branch executive
     * @return true if so
     */
    boolean isBranchExecutive();

    /**
     * Checks whether user has global access to projects or user is member
     * of a global group (e.g. employee.ignoringBranch reports true).
     * @return globalAccessGrant
     */
    boolean isGlobalAccessGrant();

    /**
     * Checks whether user has global access to projects
     * @return globalBusinessCaseAccessGrant
     */
    boolean isGlobalBusinessCaseAccessGrant();

    /**
     * Checks whether this user is office management
     * @return true if so
     */
    boolean isOfficeManagement();

    /**
     * Checks whether this user is a project manager (e.g. tec staff)
     * @return true if so
     */
    boolean isProjectManager();

    /**
     * Checks whether this user is a project manager executive (e.g. team leader tec staff)
     * @return true if so
     */
    boolean isProjectExecutive();

    /**
     * Checks whether this user is a logistic person
     * @return true if so
     */
    boolean isLogistics();

    /**
     * Indicates that user is only in group logistic
     * @return logisticsOnly
     */
    boolean isLogisticsOnly();

    /**
     * Checks whether this user is logistic executive
     * @return true if so
     */
    boolean isLogisticsExecutive();

    /**
     * Checks whether this user is a sales person
     * @return true if so
     */
    boolean isPurchaser();

    /**
     * Checks whether this user is a sales person
     * @return true if so
     */
    boolean isSales();

    /**
     * Checks whether this user is sales executive
     * @return true if so
     */
    boolean isSalesExecutive();

    /**
     * Indicates that user is sales person only
     * @return salesOnly true if assigned to sales and not assigned to accounting, customerService, executive, salesExecutive and officeManagment
     */
    boolean isSalesOnly();

    /**
     * Checks whether this user is member of wholesale
     * @return true if so
     */
    boolean isWholesale();

    /**
     * Provides the default group for fcs display
     * @return defaultFcsGroup
     */
    Long getDefaultFcsGroup();

    /**
     * Sets the default group for fcs display
     * @param defaultFcsGroup
     */
    void setDefaultFcsGroup(Long defaultFcsGroup);

    /**
     * Indicates if user has access to branch by branch itself, by global
     * access permissions or headquarter.
     * @param branch
     * @return true if branch is accessible by assigned branch or permission.
     */
    boolean isBranchAccessible(BranchOffice branch);

    /**
     * Indicates that user supports actions with given business type
     * @param type
     * @return supportingType
     */
    boolean isSupportingType(BusinessType type);

    /**
     * Indicates that user is related as the sales person with the business case
     * @param businessCase
     * @return true if user is related as sales
     */
    boolean isUserRelatedSales(BusinessCase businessCase);

    /**
     * Indicates that user is related as a co-sales with the business case
     * @param businessCase
     * @return true if user is related as co-sales
     */
    boolean isUserRelatedCoSales(BusinessCase businessCase);

    /**
     * Indicates that user is a sales person but not related with the business case.
     * @param businessCase
     * @return true if user is not related to business case but a sales person 
     */
    boolean isUserForeignSales(BusinessCase businessCase);

    /**
     * Indicates that user is related as some manager of the business case
     * @param businessCase
     * @return true if user is related as some manager of business case
     */
    boolean isUserRelatedManager(BusinessCase businessCase);

    /**
     * Indicates that user is related as the responsible manager of the business case
     * @param businessCase
     * @return true if user is related as the responsible manager of business case
     */
    boolean isResponsibleManager(BusinessCase businessCase);

    /**
     * Checks users common access permissions against a dedicated business case   
     * @param businessCase
     * @throws PermissionException if permission is not grant for user
     */
    void checkPermissions(BusinessCase businessCase) throws PermissionException;

    /**
     * Checks special access permissions of a sales person against a dedicated business case   
     * @param businessCase
     * @throws PermissionException if permission is not grant for user
     */
    void checkSalesAccess(BusinessCase businessCase) throws PermissionException;
}
