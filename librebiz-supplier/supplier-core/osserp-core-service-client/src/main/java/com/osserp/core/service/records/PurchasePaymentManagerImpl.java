/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 02-Feb-2007 01:58:05 
 * 
 */
package com.osserp.core.service.records;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.osserp.core.finance.BillingType;
import com.osserp.core.finance.PaymentAwareRecord;
import com.osserp.core.finance.Records;
import com.osserp.core.dao.records.PurchasePayments;
import com.osserp.core.dao.records.RecordTypes;
import com.osserp.core.purchasing.PurchaseInvoiceType;
import com.osserp.core.purchasing.PurchasePaymentManager;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PurchasePaymentManagerImpl extends AbstractPaymentManager implements PurchasePaymentManager {
    
    private RecordTypes billingTypes = null;
    
    private Set<Long> paymentTypeIgnorables;
    private Set<Long> installationPaymentTypeIgnorables;

    protected PurchasePaymentManagerImpl(PurchasePayments payments, RecordTypes billingTypes) {
        super(payments);
        this.billingTypes = billingTypes;
        paymentTypeIgnorables = new HashSet<>();
        paymentTypeIgnorables.add(BillingType.TYPE_CASH_DISCOUNT);
        paymentTypeIgnorables.add(BillingType.TYPE_CANCELLATION);
        installationPaymentTypeIgnorables = new HashSet<>();
        installationPaymentTypeIgnorables.add(54L);
        installationPaymentTypeIgnorables.add(55L);
    }

    public List<BillingType> getPaymentTypes(PaymentAwareRecord record) {
        List<BillingType> result = new ArrayList<BillingType>();
        List<BillingType> all = billingTypes.getBillingTypes(BillingType.CLASS_PURCHASE_PAYMENTS);
        for (int i = 0, j = all.size(); i < j; i++) {
            BillingType next = all.get(i);
            if (!paymentTypeIgnorables.contains(next.getId())) {
                result.add(next);
            }
        }
        if (record.getBookingType() instanceof PurchaseInvoiceType &&
                !((PurchaseInvoiceType) record.getBookingType()).isCredit()) {
            result.add(billingTypes.loadBillingType(BillingType.TYPE_CREDIT_NOTE));
        }
        if (!record.getPayments().isEmpty() 
                && (record.getDueAmount() != null && record.getDueAmount().longValue() == 0)) {

            for (Iterator<BillingType> i = result.iterator(); i.hasNext();) {
                BillingType next = i.next();
                if (Records.isRegularPayment(next)) {
                    // don't add payments to paid invoice
                    i.remove();
                }
            }
        }
        return result;
    }
}
