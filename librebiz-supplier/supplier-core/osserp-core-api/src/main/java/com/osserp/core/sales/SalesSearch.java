/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 20 Mar 2007 10:07:46 
 * 
 */
package com.osserp.core.sales;

import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.dao.QueryResult;

import com.osserp.core.BusinessCase;
import com.osserp.core.BusinessCaseFilter;
import com.osserp.core.BusinessCaseSearch;
import com.osserp.core.BusinessCaseSearchRequest;
import com.osserp.core.FcsAction;
import com.osserp.core.finance.Record;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SalesSearch extends BusinessCaseSearch {

    /**
     * Checks if sales with specified id exists
     * @param salesId
     * @return true if exists
     */
    boolean exists(Long salesId);

    /**
     * Finds all sales by provided search request
     * @param request
     * @return list of matching sales or empty list
     */
    List<SalesListItem> find(BusinessCaseSearchRequest request);

    /**
     * Tries to find a sales by id
     * @param salesId
     * @return sales
     * @throws ClientException if no sale with given id exists
     */
    Sales fetchSales(Long salesId) throws ClientException;

    /**
     * Tries to fetch the most current record (order or invoice)
     * @param sales
     * @return most current record
     * @throws ClientException if no record exists
     */
    Record fetchMostCurrentRecord(Sales sales) throws ClientException;

    /**
     * Provides the order for a sales if exists
     * @param sales
     * @return order or null if none exists
     */
    Record findOrder(Sales sales);

    /**
     * Finds service sales by sales
     * @param salesId
     * @return service sales or empty list
     */
    List<SalesListItem> findServiceSales(Long salesId);

    /**
     * Provides all flow control actions related to the type of the provided 
     * businessCase. This method also modifies action.changed date with the 
     * date when the action was executed in the context of the provided case.
     * @param object
     * @return all fcs actions related to a businessCase.type
     */
    List<FcsAction> getActionStatusInfo(BusinessCase object);

    /**
     * Provides all business case matching provided filter
     * @param filter
     * @param export indicates if extended view for sheet export should be loaded
     * @return query result object 
     */
    QueryResult getBusinessCaseStatus(BusinessCaseFilter filter);

    /**
     * Provides all business case matching provided filter as sheet
     * @param filter
     * @return sheet as string of comma separated rows (csv).
     */
    String getBusinessCaseStatusSheet(BusinessCaseFilter filter);
    
    /**
     * Reloads cached business case status views.
     */
    void reloadCachedBusinessCaseStatus();
    
    /**
     * Provides the business case status cache reload time.
     * @return cache reload time or previous day if empty
     */
    Date getBusinessCaseStatusCacheTime();
}
