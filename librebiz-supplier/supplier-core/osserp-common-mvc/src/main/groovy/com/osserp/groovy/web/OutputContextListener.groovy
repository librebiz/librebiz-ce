/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.osserp.groovy.web


import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * 
 * @author eh <eh@osserp.com>
 * @author rk <rk@osserp.com>
 *
 */
public class OutputContextListener extends AbstractViewContextListener {
    private static Logger logger = LoggerFactory.getLogger(OutputContextListener.class.getName())

    @Override
    protected void filterHttpRequest(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain chain) throws IOException, ServletException {

        def session = request.getSession(true)

        boolean redirect = false
        String uri = request.getRequestURI()

        if (isPotentialViewRequest(uri)) {

            if (logger.isDebugEnabled()) {
                logger.debug("filterHttpRequest: invoked [uri=$uri]")
            }

            def viewparams = WebUtil.getViewParams(uri)
            OutputView view = WebUtil.fetchView(request, viewparams.viewName)
            try {
                if (!view || viewparams.method == 'forward') {
                    view = createView(request, viewparams, 'output')
                    if (view) {
                        initRequest(view, request)
                        session.setAttribute(viewparams.viewName + "View", view)
                    } else {
                        session.setAttribute('viewparams', viewparams)
                    }
                } else {
                    view.requestContext = viewparams
                    initRequest(view, request)
                }
                if (view) {
                    request.setAttribute(View.CURRENT, view)
                }
                if (logger.isDebugEnabled()) {
                    logger.debug("filterHttpRequest: [uri=$uri, params=$viewparams]")
                }
            } catch (ViewContextException e) {
                redirect = true
                request.getSession().setAttribute('error', e.getMessage())
                redirect(request, response, "/error")
            }
        } else {
            logger.debug("filterHttpRequest: not matching [uri=$uri]")
        }
        if (!redirect) {
            chain.doFilter(request, response)
        }
        logger.debug("filterHttpRequest: session=" + request.getSession().toString())
    }
}
