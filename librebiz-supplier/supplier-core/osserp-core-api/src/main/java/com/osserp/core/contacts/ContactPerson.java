/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 15, 2008 7:18:24 PM 
 * 
 */
package com.osserp.core.contacts;

import java.util.Date;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ContactPerson extends Person {

    /**
     * Returns the primary key
     * @return contactId
     */
    Long getContactId();

    /**
     * Returns the referenced contactId if this contact is a contact person of another contact
     * @return reference
     */
    Long getReference();

    /**
     * Returns the referenced contact if this contact is a contact person of another contact
     * @return reference
     */
    Contact getReferenceContact();

    /**
     * Sets the referenced contact
     * @param contact
     */
    void setReferenceContact(Contact contact);

    /**
     * The birthdate of the entity.
     * @return birthDate
     */
    Date getBirthDate();

    /**
     * The birthdate
     * @param birthDate
     */
    void setBirthDate(Date birthDate);

    /**
     * The position of the contact person
     * @return position
     */
    String getPosition();

    /**
     * Sets the position
     * @param position
     */
    void setPosition(String position);

    /**
     * The section
     * @return section
     */
    String getSection();

    /**
     * Sets the section
     * @param section
     */
    void setSection(String section);

    /**
     * The office
     * @return office
     */
    String getOffice();

    /**
     * Sets the office
     * @param office
     */
    void setOffice(String office);

}
