<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>

<c:set var="view" value="${sessionScope.recordView}"/>
<c:set var="record" value="${view.record}"/>
<tr><td colspan="2"><fmt:message key="recordNotes"/>:</td></tr>
<tr><td colspan="2"><textarea class="form-control" name="note" rows="10"><o:out value="${record.note}"/></textarea></td></tr>
