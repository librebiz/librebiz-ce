<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-12 panel-area">
    <div class="table-responsive table-responsive-default">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th class="docListDate"><fmt:message key="date"/></th>
                    <th class="docListType"><fmt:message key="type" /></th>
                    <th class="docListReference"><fmt:message key="reference" /></th>
                    <th class="docListName"><fmt:message key="label"/></th>
                    <th class="docListSize"><fmt:message key="size" /></th>
                    <th class="docListAuthor"><fmt:message key="uploadBy"/></th>
                    <th class="action"> </th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="obj" items="${view.list}">
                    <tr>
                        <td class="docListDate"><o:date value="${obj.created}" /></td>
                        <td class="docListType"><o:out value="${obj.type.name}" /></td>
                        <td class="docListReference"><o:out value="${obj.reference}" /></td>
                        <td class="docListName"><o:out value="${obj.displayName}" /></td>
                        <td class="docListSize"><o:number value="${obj.fileSize}" format="bytes" /></td>
                        <td class="docListAuthor"><oc:employee value="${obj.createdBy}" /></td>
                        <td class="action">
                            <o:doc obj="${obj}"><o:img name="printIcon" styleClass="bigIcon"/></o:doc>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
</div>
