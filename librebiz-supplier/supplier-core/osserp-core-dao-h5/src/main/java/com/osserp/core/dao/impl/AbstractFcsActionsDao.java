/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 27, 2007 9:18:29 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.dao.Tables;
import com.osserp.core.BusinessCase;
import com.osserp.core.BusinessType;
import com.osserp.core.FcsAction;
import com.osserp.core.FcsItem;
import com.osserp.core.dao.FcsActions;
import com.osserp.core.dao.FcsWastebaskets;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractFcsActionsDao extends SelectOptionsDao implements FcsActions {
    private static Logger log = LoggerFactory.getLogger(AbstractFcsActionsDao.class.getName());

    private FcsWastebaskets baskets = null;
    private String flowControlActionsTable = null;
    private String flowControlDependenciesTable = null;
    private String flowControlWastebasketDefaultsTable = null;

    public AbstractFcsActionsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            FcsWastebaskets baskets,
            String actionsTableKey,
            String dependencyTableKey,
            String wastebasketDefaultsTableKey) {
        super(jdbcTemplate, tables, sessionFactory);
        this.baskets = baskets;
        this.flowControlActionsTable = getTable(actionsTableKey);
        this.flowControlDependenciesTable = getTable(dependencyTableKey);
        this.flowControlWastebasketDefaultsTable = getTable(wastebasketDefaultsTableKey);
    }

    public FcsAction fetchAction(BusinessType type, Long id, String name) {
        if (type != null) {
            List<FcsAction> list = findByType(type.getId());
            for (int i = 0, j = list.size(); i < j; i++) {
                FcsAction action = list.get(i);
                if (action.getId().equals(id) || action.getName().equals(name)) {
                    return action;
                }
            }
            // check if name is an external status
            if (!isEmpty(name) && type.isMatchingExternalStatus(name)) {
                for (int i = 0, j = list.size(); i < j; i++) {
                    FcsAction action = list.get(i);
                    if (isMatchingExternalStatus(action, type, name)) {
                        return action;
                    }
                }
            }
        }
        return null;
    }
    
    protected abstract boolean isMatchingExternalStatus(FcsAction action, BusinessType businessType, String externalStatus);

    public Set<Long> getDependencies(Long action) {
        StringBuilder sql = new StringBuilder(96);
        sql
                .append("SELECT depends_on FROM ")
                .append(flowControlDependenciesTable)
                .append(" WHERE action_id = ?");
        final Object[] params = { action };
        final int[] types = { Types.BIGINT };
        return new HashSet((List) jdbcTemplate.query(sql.toString(), params, types, getLongRowMapper()));
    }

    public List<FcsAction> getWastebasketStartUp(BusinessType type) {
        StringBuilder sql = new StringBuilder(96);
        sql
                .append("SELECT fcs_id FROM ")
                .append(flowControlWastebasketDefaultsTable)
                .append(" WHERE fcs_id IN (SELECT id FROM ")
                .append(flowControlActionsTable)
                .append(" WHERE type_id = ?)");
        List<Long> list = (List<Long>) jdbcTemplate.query(
                sql.toString(),
                new Object[] { type.getId() },
                new int[] { Types.BIGINT },
                getLongRowMapper());
        List<FcsAction> waste = new ArrayList<FcsAction>();
        for (int i = 0, j = list.size(); i < j; i++) {
            FcsAction next = load(list.get(i));
            waste.add(next);
        }
        return waste;
    }

    public final List<FcsAction> createActionList(BusinessCase object) {
        List<FcsAction> result = new ArrayList<FcsAction>();
        List<FcsAction> allActions = findActions(object);
        if (log.isDebugEnabled()) {
            log.debug("createActionList() found "
                    + (allActions == null ? " null " : allActions.size())
                    + " available actions");
        }
        Set<Long> actions = new HashSet<Long>();
        Set<Long> waste = baskets.getWaste(object.getPrimaryKey());
        List<FcsItem> currentSheet = object.getFlowControlSheet();
        if (log.isDebugEnabled()) {
            log.debug("createActionList() current sheet contains "
                    + (currentSheet == null ? " null " : currentSheet.size())
                    + " actions");
        }
        if (currentSheet != null && currentSheet.size() > 0) {
            for (int i = 0, j = currentSheet.size(); i < j; i++) {
                actions.add(currentSheet.get(i).getAction().getId());
            }
        }
        if (allActions != null) {
            for (int i = 0, j = allActions.size(); i < j; i++) {
                FcsAction action = allActions.get(i);
                if (waste == null || !waste.contains(action.getId())) {
                    processAction(result, currentSheet, actions, action);
                }
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("createActionList() done [actions=" + result.size() + "]");
        }
        return result;
    }
    
    protected List<FcsAction> sortActionList(List<FcsAction> actionList) {
        return actionList;
    }

    private void processAction(
            List result,
            List currentSheet,
            Set actions,
            FcsAction action) {

        if (action.isDisplayEverytime()) {

            result.add(action);
        } else {
            if (!actions.contains(action.getId())) {
                result.add(action);

            } else {
                // check for available but canceled action
                int set = 0;
                int canceled = 0;
                for (Iterator j = currentSheet.iterator(); j.hasNext();) {
                    FcsItem it = (FcsItem) j.next();
                    if (it.getAction().getId().equals(action.getId())) {
                        if (it.isCancels()) {
                            canceled++;
                        } else {
                            set++;
                        }
                    }
                }
                if (canceled > 0 && set == canceled) {
                    result.add(action);
                }
            }
        }
    }

    protected List<Long> getWastebasket() {
        StringBuilder sql = new StringBuilder(96);
        sql
                .append("SELECT fcs_id FROM ")
                .append(flowControlWastebasketDefaultsTable);
        return (List<Long>) jdbcTemplate.query(sql.toString(), getLongRowMapper());
    }

    protected abstract List<FcsAction> findActions(BusinessCase object);

    protected String getFlowControlDependenciesTable() {
        return flowControlDependenciesTable;
    }

    protected String getFlowControlWastebasketDefaultsTable() {
        return flowControlWastebasketDefaultsTable;
    }

    protected String getFlowControlActionsTable() {
        return flowControlActionsTable;
    }
}
