/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 14, 2005 
 * 
 */
package com.osserp.core;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.jdom2.Element;

import com.osserp.common.EntityRelation;

import com.osserp.core.products.Product;
import com.osserp.core.products.SerialNumber;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Item extends EntityRelation, Cloneable {

    /**
     * Returns the productId
     * @return product
     */
    Product getProduct();
    
    /**
     * Provides customName if available or product.name if not
     * @return custom or product name
     */
    String getProductName();

    /**
     * Returns the quantity
     * @return quantity.
     */
    Double getQuantity();

    /**
     * Sets the quantity
     * @param quantity
     */
    void setQuantity(Double quantity);

    /**
     * Returns the price
     * @return price.
     */
    BigDecimal getPrice();

    /**
     * Sets the price
     * @param price
     */
    void setPrice(BigDecimal price);

    /**
     * Provides an [optional] date for partner & purchase price (since 01.04.2011)
     * @return priceDate
     */
    Date getPriceDate();

    /**
     * Indicates that price was overridden (prevents a manual revised price from being overridden by calculator)
     * @return priceOverridden
     */
    boolean isPriceOverridden();

    /**
     * Sets price as overriden
     * @param priceOverridden
     */
    void setPriceOverridden(boolean priceOverridden);

    /**
     * Returns the partner price at date when created
     * @return partnerPrice.
     */
    BigDecimal getPartnerPrice();

    /**
     * Updates partner price
     * @param partnerPrice
     */
    void setPartnerPrice(BigDecimal partnerPrice);

    /**
     * Returns if partner price is editable
     * @return partnerPriceEditable
     */
    boolean isPartnerPriceEditable();

    /**
     * Sets that partner price is editable
     * @param partnerPriceEditable
     */
    void setPartnerPriceEditable(boolean partnerPriceEditable);

    /**
     * Indicates that an editable partner price was overridden
     * @return partnerPriceOverridden
     */
    boolean isPartnerPriceOverridden();

    /**
     * Sets partner price by calculated value (packages or bundles)
     * @param calculatedPartnerPrice
     */
    void setCalculatedPartnerPrice(BigDecimal calculatedPartnerPrice);

    /**
     * Sets partner price by manual input (possible if partnerPriceEditable is enabled)
     * @param manualPartnerPrice
     */
    void setManualPartnerPrice(BigDecimal manualPartnerPrice);

    /**
     * Returns the purchase price at date when created
     * @return purchasePrice.
     */
    BigDecimal getPurchasePrice();

    /**
     * Returns the total amount
     * @return amount.
     */
    BigDecimal getAmount();

    /**
     * Returns the total partner amount if partnerPrice available
     * @return partnerAmount.
     */
    BigDecimal getPartnerAmount();

    /**
     * Returns the tax rate
     * @return taxRate.
     */
    Double getTaxRate();

    /**
     * Sets the tax rate
     * @param taxRate
     */
    void setTaxRate(Double taxRate);

    /**
     * Provides an optional name for product.name used to override the default.
     * @return customName
     */
    String getCustomName();

    /**
     * Sets a note for the item
     * @param customName
     */
    void setCustomName(String note);

    /**
     * Sets an optional name for product.name used to override the default.
     * @return note
     */
    String getNote();

    /**
     * Sets a note for the item
     * @param note
     */
    void setNote(String note);

    /**
     * Ignore item when creating documents
     * @return ignoreInDocument
     */
    boolean isIgnoreInDocument();

    /**
     * Sets that item should be ignored when creating document
     * @return ignoreInDocument
     */
    void setIgnoreInDocument(boolean ignoreInDocument);

    /**
     * Indicates that the price should be included in calculation
     * @return includePrice
     */
    boolean isIncludePrice();

    /**
     * Sets that the price should be included in calculation
     * @param includePrice
     */
    void setIncludePrice(boolean includePrice);

    /**
     * Replaces an product
     * @param product
     * @param quantity
     * @param price
     * @param priceDate
     * @param partnerPrice
     * @param partnerPriceEditable
     * @param purchasePrice
     * @param note
     */
    void replaceProduct(
            Product product,
            Double quantity,
            BigDecimal price,
            Date priceDate,
            BigDecimal partnerPrice,
            boolean partnerPriceEditable,
            BigDecimal purchasePrice,
            String note);

    /**
     * Indicates that serials are available and completed
     * @return serialComplete true if serial available and all serials booked or serials not available (e.g. complete without serial)
     */
    boolean isSerialComplete();

    /**
     * Provides the serial numbers
     * @return serials
     */
    List<SerialNumber> getSerials();

    /**
     * Provides all serials as comma separated list
     * @return serials or empty string if list is empty
     */
    String getSerialsDisplay();

    /**
     * The delivered quantity if item is used in an order context
     * @return delivered
     */
    Double getDelivered();

    /**
     * Sets the delivered quantity if item is used in an order context
     * @param delivered
     */
    void setDelivered(Double delivered);

    /**
     * Provides outstanding items by quantity and delivered
     * @return outstanding
     */
    Double getOutstanding();

    /**
     * Provides the count of items not ready for delivery
     * @return outOfStock
     */
    Double getOutOfStockCount();

    /**
     * Indicates that this item supports a delivery date as used by order item for example
     * @return true if delivery date is supported
     */
    boolean isProvidingDeliveryDate();

    /**
     * Indicates that item is delivery note affecting
     * @return deliveryNoteAffecting
     */
    boolean isDeliveryNoteAffecting();

    /**
     * Provides the stock id
     * @return stockId
     */
    Long getStockId();

    /**
     * Sets the stock id
     * @param stockId
     */
    void setStockId(Long stockId);

    /**
     * Provides the id of an accounting reference if required
     * @return accountingReferenceId
     */
    Long getAccountingReferenceId();

    /**
     * Provides the creation date of the accounting reference if available
     * @return accountingReferenceDate
     */
    Date getAccountingReferenceDate();

    /**
     * Updates accounting references
     * @param id accounting reference id
     * @param date accounting reference date
     */
    void updateAccountingReference(Long id, Date date);

    /**
     * Set that a partlist is available for this item
     * @param partlistAvailable
     */
    void setPartlistAvailable(boolean partlistAvailable);

    /**
     * Indicates that a partlist is available for this item
     * @return
     */
    boolean isPartlistAvailable();

    /**
     * Indicates that item is unckecked if implementing item requires validation support
     * @return unchecked
     */
    boolean isUnchecked();

    /**
     * Enables/disables unchecked flag
     * @param unchecked
     */
    void setUnchecked(boolean unchecked);

    /**
     * Provides the item list order id
     * @return orderId
     */
    int getOrderId();

    /**
     * Sets the item list order id
     * @param orderId
     */
    void setOrderId(int orderId);

    /**
     * Provides the externalId if item represents an item referenced
     * by an external system (e.g. shop or website id) 
     * @return externalId
     */
    Long getExternalId();
    
    /**
     * Sets an external id
     * @param externalId
     */
    void setExternalId(Long externalId);

    /**
     * Provides a dedicated taxPoint for the goods of this item.
     * Mostly used by invoice items.
     * @return taxPoint date
     */
    Date getTaxPoint();

    /**
     * Sets a dedicated taxPoint for the goods.
     * @param taxPoint date
     */
	void setTaxPoint(Date taxPoint);

	/**
	 * Provides a string with time of supply information.
	 * @return timeOfSupply
	 */
	String getTimeOfSupply();

	/**
	 * Sets a string with a time of supply information.
	 * @return timeOfSupply
	 */
	void setTimeOfSupply(String timeOfSupply);
    
    /**
     * Provides item values as xml element boolean formatValues indicates whether values should be formatted
     * @return item
     */
    Element getXML(boolean formatValues);

    /**
     * Item implements clonable
     * @return cloned object
     */
    Object clone();
}
