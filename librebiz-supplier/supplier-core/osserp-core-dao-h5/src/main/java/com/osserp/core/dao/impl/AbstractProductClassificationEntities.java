/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 12, 2008 10:56:11 AM 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;

import com.osserp.core.dao.ProductClassificationEntities;
import com.osserp.core.products.ProductClassificationEntity;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractProductClassificationEntities extends AbstractDao
        implements ProductClassificationEntities {
    private static Logger log = LoggerFactory.getLogger(AbstractProductClassificationEntities.class.getName());

    public AbstractProductClassificationEntities(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache optionsCache) {
        super(jdbcTemplate, tables, sessionFactory, optionsCache);
    }

    public List<? extends ProductClassificationEntity> getList() {
        StringBuilder hql = new StringBuilder(64);
        hql
                .append("from ")
                .append(getImmutableClass().getName())
                .append(" o order by o.name");
        return getCurrentSession().createQuery(hql.toString()).list();
    }

    public List<? extends ProductClassificationEntity> getConfigs() {
        StringBuilder hql = new StringBuilder(64);
        hql
                .append("from ")
                .append(getMutableClass().getName())
                .append(" o order by o.name");
        return getCurrentSession().createQuery(hql.toString()).list();
    }

    public Map<Long, ? extends ProductClassificationEntity> getMap() {
        Map<Long, ProductClassificationEntity> map = new HashMap<Long, ProductClassificationEntity>();
        List<? extends ProductClassificationEntity> list = getList();
        for (int i = 0, j = list.size(); i < j; i++) {
            ProductClassificationEntity next = list.get(i);
            map.put(next.getId(), next);
        }
        return map;
    }

    public void save(ProductClassificationEntity obj) {
        getCurrentSession().saveOrUpdate(getMutableClass().getName(), obj);
        getOptionsCache().refresh(getOptionsName());
    }

    public ProductClassificationEntity findByName(String name) {
        String providedName = isNotSet(name) ? "" : name.trim();
        List<ProductClassificationEntity> list = (List<ProductClassificationEntity>) getList();
        for (int i = 0, j = list.size(); i < j; i++) {
            ProductClassificationEntity next = list.get(i);
            if (next.getName().equals(providedName)) {
                if (log.isDebugEnabled()) {
                    log.debug("findByName() reports true [id=" + next.getId() + ", name=" + next.getName() + "]");
                }
                return next;
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("findByName() not found [name=" + name + "]");
        }
        return null;
    }

    protected boolean exists(String name) {
        return (findByName(name) != null);
    }

    protected abstract Class<? extends ProductClassificationEntity> getImmutableClass();

    protected abstract Class<? extends ProductClassificationEntity> getMutableClass();

    protected abstract String getOptionsName();
}
