package com.osserp.core.model.records;

import com.osserp.core.finance.DeliveryDateChangedInfo;

public class PurchaseOrderDeliveryDateChangedDeleted extends AbstractDeliveryDateChangedInfo {

    public PurchaseOrderDeliveryDateChangedDeleted() {
        super();
    }

    public PurchaseOrderDeliveryDateChangedDeleted(DeliveryDateChangedInfo other) {
        super(other);
        this.setId(other.getId());
    }

}
