<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.salesOrderView or empty sessionScope.salesOrderView.bean}">
    <o:logger write="salesOrderView not bound or order not available" level="warn"/>
    <c:redirect url="/errors/error_context.jsp"/>
</c:if>
<c:set var="recordView" scope="session" value="${sessionScope.salesOrderView}"/>
<c:set var="recordView" value="${sessionScope.recordView}"/>
<c:set var="record" value="${recordView.record}"/>
<c:set var="baseUrl" scope="request" value="/salesOrder.do"/>
<c:set var="infoUrl" scope="request" value="/salesOrderInfos.do"/>
<c:set var="discountDisplayUrl" scope="request" value="/salesOrderDiscountDisplay.do"/>
<c:set var="productExitTarget" scope="request" value="salesOrderReload"/>
<c:set var="productUrl" scope="request" value="/salesOrderProducts.do"/>
<c:set var="logisticUser" value="${recordView.user.logisticsOnly}"/>
<c:if test="${logisticUser}">
    <c:set var="displayPrice" scope="request" value="false"/>
    <o:logger write="disabling price display for logistic user" level="info"/>
    <c:if test="${!recordView.logisticsDisplayMode}">
        <c:redirect url="/salesOrder.do?method=enableLogisticsDisplay"/>
    </c:if>
</c:if>
<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title"><o:displayTitle/></tiles:put>
    <tiles:put name="styles" type="string">
    <style type="text/css">
        <c:import url="/css/records.css"/>
    </style>
    </tiles:put>
    <tiles:put name="headline">
        <c:import url="/pages/sales/records/voucher_header.jsp"/>
    </tiles:put>
    <tiles:put name="headline_right">

        <c:choose>
            <c:when test="${recordView.exitBlocked and !(empty recordView.exitBlockedTarget)}">
                <a href="<c:url value="/salesOrder.do?method=exitWithoutChecks"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon" /></a>
            </c:when>
            <c:otherwise>
                <a href="<c:url value="/salesOrder.do?method=exit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon" /></a>
                <o:permission role="executive,executive_sales,sales_revenue" info="permissionPostCalculation">
                    <v:ajaxLink linkId="salesRevenueView" url="/sales/salesRevenue/forward?id=${record.id}">
                        <o:img name="moneyIcon" />
                    </v:ajaxLink>
                </o:permission>
                <c:if test="${!empty recordView.businessTemplates}">
                    <v:ajaxLink linkId="businessTemplateDocumentView" url="/sales/salesTemplateDocument/forward?record=${record.id}">
                        <o:img name="letterIcon" />
                    </v:ajaxLink>
                </c:if>
            </c:otherwise>
        </c:choose>

        <c:choose>
            <c:when test="${recordView.logisticsDisplayMode}">
                <c:if test="${!logisticUser}">
                    <a href="<c:url value="/salesOrder.do?method=disableLogisticsDisplay"/>" title="<fmt:message key="disablePackagelistMode"/>"><o:img name="reloadIcon" styleClass="icon" /></a>
                </c:if>
            </c:when>
            <c:otherwise>
                <c:if test="${!logisticUser and !empty recordView.openDeliveries}">
                    <a href="<c:url value="/salesOrder.do?method=enableLogisticsDisplay"/>" title="<fmt:message key="enablePackagelistMode"/>"><o:img name="reloadIcon" styleClass="icon" /></a>
                </c:if>
            </c:otherwise>
        </c:choose>

        <c:choose>
            <c:when test="${!record.unchangeable and !empty record.items and !recordView.logisticsDisplayMode}">
                <c:set var="ignoreItemSupport" scope="request" value="true"/>
                <v:link url="/records/recordPrint/release?view=salesOrderView" confirm="true" message="confirmRecordRelease" title="recordRelease">
                    <o:img name="enabledIcon" />
                </v:link>
                <v:link url="/records/recordPrint/print?view=salesOrderView" title="documentPrint">
                    <o:img name="printIcon" />
                </v:link>
                <a href="<c:url value="/salesOrder.do?method=enableEdit"/>" title="<fmt:message key="change"/>"><o:img name="writeIcon" styleClass="icon" /></a>
            </c:when>
            <c:otherwise>
                <v:link url="/records/recordPrint/print?view=salesOrderView" title="documentPrint">
                    <o:img name="printIcon" />
                </v:link>
                <v:link url="/records/recordMail/forward?view=salesOrderView&exit=/salesOrder.do?method=reload" title="sendViaEmail">
                    <o:img name="mailIcon" />
                </v:link>
            </c:otherwise>
        </c:choose>
        <v:link url="/sales/salesOrderAction/forward?exit=/salesOrder.do?method=reload" title="settings">
            <o:img name="configureIcon" styleClass="icon" />
        </v:link>
        <c:if test="${!logisticUser and !empty recordView.relatedSales and recordView.relatedSales.type.recordByCalculation}">
            <a href="<c:url value="/calculations.do?method=forward&exit=order"/>" title="<fmt:message key="calculation"/>"><o:img name="calcIcon" styleClass="icon" /></a>
        </c:if>
        <v:link url="/index" title="backToMenu">
            <o:img name="homeIcon" />
        </v:link>
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="subcolumns">
            <div class="subcolumn">
                <div class="spacer"></div>
                    <div class="contentBox">
                    <div class="contentBoxData">
                        <div class="subcolumns">

                            <c:import url="/pages/shared/lock_handler.jsp" />
                            <c:import url="/pages/records/record_discount_confirmation.jsp" />

                            <div class="contentBoxHeader">
                                <c:import url="/pages/records/record_header.jsp" />
                            </div>

                            <div class="column66l">
                                <c:if test="${!empty record.confirmationNote}">
                                    <div class="recordLabelValue">
                                        <fmt:message key="orderConfirmationNoteLabel" /><span>:</span>
                                        <c:choose>
                                            <c:when test="${record.confirmationNote.length() > 80}">
                                                <a href="#confirmationNoteText"><o:out value="${record.confirmationNote}" limit="80" /></a>
                                            </c:when>
                                            <c:otherwise>
                                                <o:out value="${record.confirmationNote}" />
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                </c:if>
                                <c:import url="/pages/records/record_recipient_display.jsp" />
                                <c:import url="/pages/records/record_sigs_display.jsp" />
                                <c:import url="/pages/records/record_shipping_display.jsp"/>
                                <c:if test="${!record.bookingType.installationOrder}">
                                    <c:import url="/pages/records/record_payment_display.jsp" />
                                </c:if>
                                <c:import url="/pages/records/record_discount_display.jsp" />
                                <c:import url="/pages/records/record_embedded_text.jsp"/>
                                <c:if test="${!empty recordView.purchaseRecords or !record.closed}">
                                    <div class="recordLabelValue">
                                        <fmt:message key="purchaseOrders" /><span>:</span>
                                        <c:if test="${!record.closed}">
                                            <c:choose>
                                                <c:when test="${!empty recordView.purchasingProductSelectionId}">
                                                    <v:link url="/purchasing/orderBySalesProductCollector/forward?selectionTarget=/purchasing/orderBySalesCreator/forward&selectionConfig=${recordView.purchasingProductSelectionId}&exit=/salesOrderReload.do">[<fmt:message key="new" />]</v:link>
                                                </c:when>
                                                <c:otherwise>
                                                    <v:link url="/purchasing/orderBySalesProductCollector/forward?selectionTarget=/purchasing/orderBySalesCreator/forward&exit=/salesOrderReload.do">[<fmt:message key="new" />]</v:link>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:if>
                                        <c:if test="${!empty recordView.purchaseRecords}">
                                            <c:forEach var="po" items="${recordView.purchaseRecords}">
                                                <br />
                                                <c:choose>
                                                    <c:when test="${!empty recordView.businessCaseView.exitTarget and recordView.businessCaseView.exitTarget != 'purchaseOrder'
                                                            and recordView.businessCaseView.exitTarget != 'purchaseOrderReload'
                                                            and recordView.businessCaseView.exitTarget != 'purchaseInvoiceDisplay'}">
                                                        <c:choose>
                                                            <c:when test="${po.type == 102}">
                                                                <a href="<c:url value="/purchaseOrder.do?method=display&id=${po.id}&exit=salesOrderReload"/>"><o:out value="${po.number}" /></a>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <a href="<c:url value="/purchaseInvoice.do?method=display&id=${po.id}&exit=salesOrderReload"/>"><o:out value="${po.number}" /></a>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <c:choose>
                                                            <c:when test="${po.type == 102}">
                                                                <v:ajaxLink linkId="recordDisplay${po.id}" url="/records/recordDisplay/forward?name=purchaseOrder&id=${po.id}">
                                                                    <o:out value="${po.number}" />
                                                                </v:ajaxLink>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <v:ajaxLink linkId="recordDisplay${po.id}" url="/records/recordDisplay/forward?name=purchaseInvoice&id=${po.id}">
                                                                    <o:out value="${po.number}" />
                                                                </v:ajaxLink>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:otherwise>
                                                </c:choose>
                                                <fmt:message key="from" />
                                                <fmt:formatDate value="${po.created}" />
                                                <c:if test="${po.completed}">
                                                    <span> - </span><fmt:message key="completed" />
                                                </c:if>
                                            </c:forEach>
                                        </c:if>
                                    </div>
                                </c:if>
                                <c:if test="${!record.type.displayNotesBelowItems}">
                                    <c:import url="/pages/records/record_note_display.jsp" />
                                </c:if>
                            </div>
                            <div class="column33r">
                                <c:if test="${!empty record.confirmationDate and (record.confirmationDate != order.created) and !recordView.logisticsDisplayMode}">
                                    <div class="recordLabelValue">
                                        <fmt:message key="orderConfirmationOn" />
                                        <o:date value="${record.confirmationDate}" />
                                    </div>
                                </c:if>
                                <c:import url="/pages/records/record_status.jsp" />
                                <c:import url="/pages/records/record_delivery_date.jsp" />
                                <c:if test="${!record.bookingType.installationOrder}">
                                    <c:import url="/pages/sales/records/order_delivery_notes.jsp" />
                                </c:if>
                            </div>
                        </div>
                    </div>
                </div>

                <c:if test="${recordView.openDeliveryDisplay}">
                    <c:import url="/pages/records/record_order_deliveries.jsp" />
                </c:if>
                <c:if test="${!recordView.logisticsDisplayMode}">
                    <c:import url="/pages/records/record_items.jsp" />
                    <c:if test="${!empty record.confirmationNote and record.confirmationNote.length() > 80}">
                        <div id="confirmationNoteText" class="recordConditions">
                            <p class="recordConditionsHeader"><a href="#content_head_main"><fmt:message key="orderConfirmationNoteLabel" />:</a></p>
                            <p><o:textOut value="${record.confirmationNote}"/></p>
                        </div>
                    </c:if>
                    <c:if test="${record.type.displayNotesBelowItems}">
                        <c:import url="/pages/records/record_note_display.jsp" />
                    </c:if>
                    <c:if test="${!record.bookingType.installationOrder}">
                        <c:import url="/pages/records/record_conditions.jsp" />
                    </c:if>
                </c:if>
            </div>
        </div>
        </tiles:put>
</tiles:insert>
<c:remove var="recordView" scope="session" />
