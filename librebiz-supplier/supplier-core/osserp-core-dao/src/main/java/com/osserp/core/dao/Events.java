/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 12, 2006 1:17:38 PM 
 * 
 */
package com.osserp.core.dao;

import java.util.Date;
import java.util.List;

import com.osserp.common.Parameter;
import com.osserp.core.BusinessCase;
import com.osserp.core.events.Event;
import com.osserp.core.events.EventAction;
import com.osserp.core.events.EventAlertDisplay;
import com.osserp.core.events.EventTicket;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Events {

    /**
     * Creates a new event ticket
     * @param action
     * @param createdBy
     * @param reference
     * @param description
     * @param message
     * @param headline
     * @param canceled
     * @return ticket
     */
    EventTicket createTicket(
            EventAction action,
            Long createdBy,
            Long reference,
            String description,
            String message,
            String headline,
            boolean canceled);

    /**
     * Creates a new termination only event ticket
     * @param callerId
     * @param configTypeId
     * @param createdBy
     * @param reference
     * @param canceled
     * @return ticket
     */
    EventTicket createTicket(
            Long callerId,
            Long configTypeId,
            Long createdBy,
            Long reference,
            boolean canceled);

    /**
     * Enables sent flag for event ticket
     * @param ticket
     */
    void setTicketSent(EventTicket ticket);

    /**
     * Closes an open but sent ticket
     * @param ticket
     */
    void closeTicket(EventTicket ticket);

    /**
     * Provides all open tickets created longer than given minutes away
     * @param minutesPastCreated
     * @return openTickets
     */
    List<EventTicket> findOpenTickets(int minutesPastCreated);

    /**
     * Creates an auto triggered event if not already exists.
     * @param action
     * @param created
     * @param createdBy
     * @param reference
     * @param recipient
     * @param description
     * @param message
     * @param headline
     * @param parameters
     * @return true if event was created
     */
    boolean createEvent(
            EventAction action,
            Date created,
            Long createdBy,
            Long reference,
            Long recipient,
            String description,
            String message,
            String headline,
            List<Parameter> parameters);

    /**
     * Creates an appointment event.
     * @param action
     * @param createdBy
     * @param reference
     * @param recipient
     * @param description
     * @param message
     * @param parameters
     * @param activationDate
     * @param appointmentDate
     */
    void createEvent(
            EventAction action,
            Long createdBy,
            Long reference,
            Long recipient,
            String description,
            String message,
            List<Parameter> parameters,
            Date activationDate,
            Date appointmentDate);

    /**
     * Creates all alerts for given source event and sets alert send flag on all existing events of same action and reference type
     * @param sourceEvent
     */
    void createAlerts(Event sourceEvent);

    /**
     * Provides all open events of a given recipient
     * @param recipient
     * @param includeDeactivated
     * @param orderBy
     * @param descending
     * @param category
     * @return open events
     */
    List<Event> findByRecipient(Long recipient, boolean includeDeactivated, String orderBy, boolean descending, String category);

    /**
     * Provides all open events of a given reference
     * @param reference
     * @return open events
     */
    List<Event> findByReference(Long reference);

    /**
     * Provides all appointments of a business case
     * @param bc
     * @return open appointments
     */
    List<Event> findAppointments(BusinessCase bc);

    /**
     * Provides all appointments related to a recipient
     * @param recipient
     * @return appointments
     */
    List<Event> findAppointmentsByRecipient(Long recipient);

    /**
     * Tries to find the event with given id
     * @param eventId
     * @return event
     */
    Event findEvent(Long eventId);

    /**
     * Adds a new note to given event
     * @param event where we add the note
     * @param createdBy
     * @param note to add
     * @param activationDate
     * @param appointmentDate
     * @param distribute indicates that all other recipients of this event should also get and see the note
     * @param informRecipients creates additional info events for all provided employees
     */
    void addNote(
            Event event,
            Long createdBy,
            String note,
            Date activationDate,
            Date appointmentDate,
            boolean distribute,
            List<Long> informRecipients);

    /**
     * Closes an event
     * @param closedBy
     * @param id to close
     */
    void closeEvent(Long closedBy, Long id);

    /**
     * Closes all events of given configs
     * @param closedBy
     * @param reference
     * @param actionId
     */
    void closeEvents(Long closedBy, Long reference, Long actionId);

    /**
     * Closes all events related to given business case. Note:
     * This method does not close related appointments.
     * @param closedBy
     * @param reference
     * @return count of affected events
     */
    int closeAll(Long closedBy, Long reference);

    /**
     * Moves all closed events to event history
     * @return count of closed events moved
     */
    int moveClosedEvents();

    /**
     * Restores previously moved events from event history to events
     * @param reference
     * @param actionId
     */
    void restoreClosed(Long reference, Long actionId);

    /**
     * Provides all events whose are escalated
     * @return escalated events
     */
    Long[] findEscalatedEvents();

    /**
     * Provides a list of all available event escalations
     * @return eventAlertDisplay
     */
    List<EventAlertDisplay> getEventAlertDisplay();
}
