/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.gui.directory

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.directory.DirectoryException
import com.osserp.common.directory.DirectoryMailboxManager
import com.osserp.common.directory.DirectoryUser

/**
 *
 * @author tn <tn@osserp.com>
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
class MailboxView extends AbstractDirectoryUserView {
    private static Logger logger = LoggerFactory.getLogger(MailboxView.class.getName())


    MailboxView() {
        super()
        providesCreateMode()
    }

    @Override
    protected List loadList() {
        filterList(domainMailboxManager.findMailboxUsers())
    }

    @Override
    void save() {
        if (createMode) {
            logger.debug("save: invoked [form=${form.params}]")
            Map userMap = env.mailboxDefaults
            userMap.cn = form.getString('cn')
            userMap.description = form.getString('description')
            userMap.displayname = userMap.description
            userMap.mail = form.getString('mail')
            userMap.uid = form.getString('uid')
            userMap.uidnumber = form.getString('uidnumber')

            Map mailboxMap = [:]
            Boolean createMailaccount = form.getBoolean('createMailbox')
            if (createMailaccount) {
                mailboxMap.mail = userMap.mail
                mailboxMap.name = form.getString('mailboxUser')
                mailboxMap.password = form.getString('mailboxPassword')
            }
            Map defaults = domainMailboxManager.createMailboxUserAttributes()
            if (userMap.cn == defaults.cn) {
                throw new ClientException(ErrorCode.NAME_INVALID)
            }
            if (userMap.description == defaults.description) {
                throw new ClientException(ErrorCode.DESCRIPTION_MISSING)
            }
            if (mailboxMap.mailboxUser == defaults.mailboxUser) {
                throw new ClientException(ErrorCode.NAME_INVALID)
            }
            logger.debug("save: values deployed [user=${userMap}, mailbox=${mailboxMap}]")
            try {
                DirectoryUser directoryUser = domainMailboxManager.createMailbox(userMap, mailboxMap)
                disableCreateMode()
                if (directoryUser?.values.uidnumber) {
                    logger.debug("save: created mailbox [dn=${directoryUser.values.dn}]")
                    list = domainMailboxManager.findMailboxUsers()
                    form.params.id = directoryUser.values.uidnumber
                    select()
                }
            } catch (DirectoryException e) {
                logger.debug("save: caught exception [message=${e.message}]")
                throw new ClientException(e.message)
            }
        } else {
            super.save()
        }
    }

    @Override
    void enableCreateMode() {
        super.enableCreateMode()
        Map defaults = domainMailboxManager.createMailboxUserAttributes()
        form.params = form.params + defaults
        env.mailboxDefaults = defaults
        // adding mailbox to an existing account?
        if (bean?.values?.uid && bean.id) {
            form.params.uid = bean.values.uid
            if (bean.values.mail) {
                form.params.mail = bean.values.mail
            }
        }
    }
}

