/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 05-Aug-2006 
 * 
 */
package com.osserp.gui.client.records;

import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;
import com.osserp.core.Item;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.RecordDocument;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface OrderView extends RecordView {

    /**
     * Provides the open deliveries of current order
     * @return openDeliveries
     */
    public List<Item> getOpenDeliveries();

    /**
     * Indicates if open deliveries should be displayed
     * @return true if so
     */
    public boolean isOpenDeliveryDisplay();

    /**
     * Enables/disables open delivery display
     * @param openDeliveryDisplay
     */
    public void setOpenDeliveryDisplay(boolean openDeliveryDisplay);

    /**
     * Provides current record as order
     * @return order
     */
    public Order getOrder();

    /**
     * Provides a previously selected item
     * @return selectedItem
     */
    public Item getSelectedItem();

    /**
     * Indicates that view is in global delivery date edit mode
     * @return true if so
     */
    public boolean isDeliveryDateEditMode();

    /**
     * Enables/disables global delivery date edit mode
     * @param deliveryDateEditMode
     */
    public void setDeliveryDateEditMode(boolean deliveryDateEditMode);

    /**
     * Indicates that item based delivery date edit mode is enabled
     * @return true if so
     */
    public boolean isItemDeliveryDateEditMode();

    /**
     * Enables/disables item update edit mode
     * @param enableItemUpdateEditMode
     */
    public boolean changeItemDeliveryDateEditMode(Long itemId);

    /**
     * Updates the delivery date of current record or previously selected item
     * @param form with values to update, see form def. for values
     * @throws ClientException if validation of new values failed
     * @throws PermissionException if user has no permissions
     */
    public void updateDeliveryDate(Form fh) throws ClientException, PermissionException;

    /**
     * Changes delivery confirmation status of an item
     * @param id
     * @return document
     */
    public void changeDeliveryConfirmation(Long id);

    /**
     * Checks if current order has unreleased delivery notes
     * @throws ClientException if unreleased records found
     */
    public void checkDeliveryStatus() throws ClientException;

    public boolean isSupportingCustomDelivery();

    public Long getCustomDeliveryType();

    /**
     * Indicates that view is in stock edit mode
     * @return stockEditMode
     */
    public boolean isStockEditMode();

    /**
     * Enables/disables sock edit mode
     * @param stockEditMode
     */
    public void setStockEditMode(boolean stockEditMode);

    /**
     * Changes stock of an item
     * @param productId
     * @param stockId
     */
    public void updateStock(Long productId, Long stockId);

    /**
     * Creates a new version of current sales order
     * @throws ClientException if new version creation is not possible
     * @throws PermissionException if user has no permissions
     */
    public void createNewVersion() throws ClientException, PermissionException;

    /**
     * Provides all versions of current order (available only when setup mode is enabled)
     * @return versions
     */
    public List<RecordDocument> getVersions();

    /**
     * Provides pdf of an order version by document id
     * @param id
     * @return version
     */
    public byte[] getVersion(Long id);

}
