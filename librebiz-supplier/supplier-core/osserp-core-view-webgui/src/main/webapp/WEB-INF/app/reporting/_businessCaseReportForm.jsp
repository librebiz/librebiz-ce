<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="businessCaseReportView" />

<v:form name="businessCaseReportForm" url="/reporting/businessCaseReport/save">
    <div class="col-md-6 panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4><fmt:message key="configureQueryHeader" /></h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-body">

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="statusDate"><fmt:message key="statusQuo" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <fmt:message key="asAt" />
                            <c:choose>
                                <c:when test="${empty view.businessCaseStatusCacheTime}">
                                    <fmt:message key="previousDay" />
                                </c:when>
                                <c:otherwise>
                                    <o:date value="${view.businessCaseStatusCacheTime}" addtime="true" />
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="type"><fmt:message key="searchFor" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <oc:select name="branch" value="${view.filter.branchId}" options="${view.availableBranchs}" styleClass="form-control" prompt="false" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="ignoreBranch"> </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <div class="checkbox">
                                <label for="ignoreBranch"> <v:checkbox name="ignoreBranch" /> <fmt:message key="loadAllLabel" />
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <v:date label="from" name="dateFrom" value="${view.filter.dateFrom}" styleClass="form-control" picker="true" />

                <v:date label="until" name="dateTil" value="${view.filter.dateTil}" styleClass="form-control" picker="true" />

                <div class="row next">
                    <div class="col-md-4"> </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <v:submitExit url="/reporting/businessCaseReport/exit"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <o:submit value="search" styleClass="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</v:form>
