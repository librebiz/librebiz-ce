/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 9, 2012 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.Option;
import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;

import com.osserp.core.Options;
import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.ContactGroup;
import com.osserp.core.contacts.PrivateContact;
import com.osserp.core.dao.PrivateContacts;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.model.contacts.PrivateContactImpl;
import com.osserp.core.users.DomainUser;

/**
 * Synchronizes contacts with integrated backends such as directory services, mailservers, etc.
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PrivateContactsDao extends AbstractDao implements PrivateContacts {
    private static Logger log = LoggerFactory.getLogger(PrivateContactsDao.class.getName());

    private OptionsCache optionsCache = null;
    private String employeeContactsTable = null;

    protected PrivateContactsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache optionsCache) {
        super(jdbcTemplate, tables, sessionFactory);
        this.employeeContactsTable = getTable(TableKeys.EMPLOYEE_CONTACTS);
        this.optionsCache = optionsCache;
    }

    public PrivateContact createPrivateContact(DomainUser user, Contact contact, Contact company) {
        if (!isPrivateContact(user, contact)) {
            String countryName = getCountryName(contact);
            String contactType = createContactGroup(contact);
            PrivateContactImpl obj = new PrivateContactImpl(
                    user,
                    contact,
                    company,
                    countryName,
                    contactType);
            save(obj);
            if (log.isDebugEnabled()) {
                log.debug("createPrivateContact() done [id=" + obj.getId()
                        + ", user=" + user.getId()
                        + ", contact=" + contact.getContactId()
                        + (company == null ? "]" :
                                (", company=" + company.getContactId() + "]")));
            }
            return obj;
        }
        PrivateContact result = getPrivateContact(user, contact);
        if (result != null && result.isUnused()) {
            result.setUnused(false);
            save(result);
        }
        return result;
    }

    public PrivateContact getPrivateContact(Long id) {
        StringBuilder hql = new StringBuilder(256);
        hql
                .append("from ")
                .append(PrivateContactImpl.class.getName())
                .append(" o where o.id = :pk");
        List<PrivateContact> result = getCurrentSession().createQuery(hql.toString())
                .setParameter("pk", id).list();
        return result.isEmpty() ? null : result.get(0);
    }

    public PrivateContact getPrivateContact(DomainUser user, Contact contact) {
        StringBuilder hql = new StringBuilder(256);
        hql
                .append("from ")
                .append(PrivateContactImpl.class.getName())
                .append(" o where o.reference = :referenceId and")
                .append(" o.contact.contactId = :contactId");
        List<PrivateContact> result = getCurrentSession().createQuery(hql.toString())
                    .setParameter("referenceId", user.getEmployee().getId())
                    .setParameter("contactId", contact.getContactId())
                    .list();
        return result.isEmpty() ? null : result.get(0);
    }

    public boolean isPrivateContact(DomainUser user, Contact contact) {
        Set<Long> existing = getPrivateContactIds(user);
        return (existing.contains(contact.getContactId()));
    }

    public boolean isPrivateContactUnused(DomainUser user, Contact contact) {
        PrivateContact pc = getPrivateContact(user, contact);
        if (pc == null) {
            return true;
        }
        return pc.isUnused();
    }

    public boolean isPrivateContactAvailable(DomainUser user) {
        Set<Long> existing = getPrivateContactIds(user);
        return (!existing.isEmpty());
    }

    public List<PrivateContact> getPrivateContacts(DomainUser user) {
        StringBuilder hql = new StringBuilder(256);
        hql
                .append("from ")
                .append(PrivateContactImpl.class.getName())
                .append(" o where o.reference = :employeeId order by o.contact.lastName");
        return getCurrentSession().createQuery(
                hql.toString()).setParameter("employeeId", user.getEmployee().getId()).list();
    }

    public List<PrivateContact> getPrivateContacts(Long contactId) {
        StringBuilder hql = new StringBuilder(256);
        hql
                .append("from ")
                .append(PrivateContactImpl.class.getName())
                .append(" o where o.contact.contactId = :contactId");
        return getCurrentSession().createQuery(
                hql.toString()).setParameter("contactId", contactId).list();
    }

    public void save(PrivateContact contact) {
        getCurrentSession().saveOrUpdate(PrivateContactImpl.class.getName(), contact);
    }

    private Set<Long> getPrivateContactIds(DomainUser user) {
        Set<Long> result = new java.util.HashSet<Long>();
        StringBuilder sql = new StringBuilder("SELECT contact_id FROM ");
        sql
                .append(employeeContactsTable)
                .append(" WHERE employee_id = ")
                .append(user.getEmployee().getId());
        try {
            result.addAll((List<Long>) jdbcTemplate.query(sql.toString(), getLongRowMapper()));
        } catch (Throwable t) {
            log.error("getPrivateContactIds() failed [message=" + t.getMessage() + "]");
        }
        return result;
    }

    private String getCountryName(Contact contact) {
        Long id = (contact.getAddress() == null ? null : contact.getAddress().getCountry());
        if (!isEmpty(id)) {
            return optionsCache.getMappedValue(Options.COUNTRY_NAMES, id);
        }
        return null;
    }

    private String createContactGroup(Contact contact) {
        ContactGroup group = fetchContactGroup(contact);
        if (group != null) {
            group.getName();
        }
        return null;
    }

    private ContactGroup fetchContactGroup(Contact contact) {
        Map<Long, Option> map = optionsCache
                .getMap(Options.CONTACT_GROUPS);
        ContactGroup result = null;
        if (contact.isCustomer()) {
            result = (ContactGroup) map.get(Contact.CUSTOMER);
        } else if (contact.isSupplier()) {
            result = (ContactGroup) map.get(Contact.SUPPLIER);
        }
        if (result == null) {
            Set<Long> ids = map.keySet();
            for (Iterator<Long> i = ids.iterator(); i.hasNext();) {
                ContactGroup next = (ContactGroup) map.get(i.next());
                if (next.isUnclassified()) {
                    result = next;
                    /*
                     * if (log.isDebugEnabled()) { log.debug("fetchContactGroup() done [name=" + (result == null ? "null" : result.getName()) + "]"); }
                     */
                    break;
                }
            }
        }
        return result;
    }

}
