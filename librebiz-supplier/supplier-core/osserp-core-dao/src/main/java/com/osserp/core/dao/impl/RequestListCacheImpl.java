/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 7, 2010 1:38:25 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.service.CacheJob;
import com.osserp.core.dao.RequestListCache;
import com.osserp.core.dao.RequestLists;
import com.osserp.core.requests.RequestListItem;

/**
 * @author Rainer Kirchner <rk@osserp.com>
 */
public class RequestListCacheImpl implements RequestListCache {
    private static Logger log = LoggerFactory.getLogger(RequestListCacheImpl.class.getName());
    private transient boolean refreshing = false;
    private RequestLists queries = null;
    private List<RequestListItem> requestDisplay = Collections.synchronizedList(new ArrayList<RequestListItem>());

    protected RequestListCacheImpl(RequestLists queries) {
        super();
        this.queries = queries;
    }

    public void refresh(String eventXml) {
        if (log.isDebugEnabled()) {
            log.debug("refresh() invoked [event=" + eventXml + "]");
        }
        long time = System.currentTimeMillis();
        refreshing = true;
        List<RequestListItem> rd = queries.getCurrentRequests();
        requestDisplay = Collections.synchronizedList(rd);
        refreshing = false;
        if (log.isDebugEnabled()) {
            log.debug("refresh() done [duration=" + (System.currentTimeMillis() - time) + "ms]");
        }
    }

    public List<RequestListItem> getList() {
        long time = System.currentTimeMillis();
        if (requestDisplay.isEmpty()) {
            refresh("<localInit/>");
        }
        Object[] array = requestDisplay.toArray();
        List<RequestListItem> result = new ArrayList<RequestListItem>();
        for (int i = 0, j = array.length; i < j; i++) {
            result.add((RequestListItem) array[i]);
        }
        if (log.isDebugEnabled()) {
            log.debug("getList() done [size=" + result.size()
                    + ", duration=" + (System.currentTimeMillis() - time) + "ms]");
        }
        return result;
    }

    public void closeRunningTasks(CacheJob current) {
        if (log.isInfoEnabled()) {
            log.info("closeRunningTasks() empty method invoked!");
        }
    }

    public CacheJob getCurrentTask() {
        if (log.isInfoEnabled()) {
            log.info("getCurrentTask() empty method invoked, returning null!");
        }
        return null;
    }

    public boolean isRefreshing() {
        return refreshing;
    }

    public void setModified(String reference) {
        if (log.isInfoEnabled()) {
            log.info("setModified() invoked");
        }
        refresh(reference);
    }
}
