/**
 *
 * Copyright (C) 2015 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 9, 2015 
 * 
 */
package com.osserp.common.gui.service;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.beans.AbstractClass;
import com.osserp.common.gui.Menu;
import com.osserp.common.gui.MenuProvider;
import com.osserp.common.gui.model.MenuImpl;
import com.osserp.common.util.JsonObject;
import com.osserp.common.util.JsonUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class JsonMenuProvider extends AbstractClass implements MenuProvider {
    private static Logger log = LoggerFactory.getLogger(JsonMenuProvider.class.getName());

    private String filename = "Menu.json";
    
    protected JsonMenuProvider() {
        super();
    }

    public JsonMenuProvider(String filename) {
        super();
        this.filename = filename;
    }

    public List<Menu> getMenus() {
        List<Menu> result = new ArrayList<>();
        JsonObject json = JsonUtil.parseFileAndResource(filename);
        if (json != null) {
//            System.out.println("object receipt:\n" + json.toString());
            Object jsonData = json.get("menu");
            if (jsonData instanceof List) {
                List array = (List) jsonData;
                for (int i = 0, j = array.size(); i < j; i++) {
                    Object o = array.get(i);
                    if (o instanceof Map) {
                        MenuImpl menu = new MenuImpl((Map) o);
                        result.add(menu);
                    }
                }
            } else {
                log.warn("getMenus() unexpected object for 'menu' [class="
                        + (jsonData != null ? jsonData.getClass().getName() : "null")
                        + "]");
            }
        }
        return result;
    }
}
