<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" scope="page" value="${sessionScope.partlistView}"/>
<c:set var="partlist" value="${view.bean}"/>

<div class="contentBox">
	<div class="contentBoxHeader">
		<div class="contentBoxHeaderLeft">
			<fmt:message key="providedFor"/>
			<o:out value="${view.product.name}"/>
			<fmt:message key="by"/>
			<v:ajaxLink linkId="employeePopupView" url="/employees/employeePopup/forward?id=${partlist.createdBy}">
				<oc:employee value="${partlist.createdBy}"/>
			</v:ajaxLink>
			/ <o:date value="${partlist.created}"/>
		</div>
	</div>
</div>

<div class="spacer"></div>

<table class="table table-striped">
	<thead>
		<tr>
			<th style="width: 90px;" class="right"><fmt:message key="productid"/></th>
			<th><fmt:message key="label"/></th>
			<th><fmt:message key="quantity"/></th>
			<th><fmt:message key="quantityUnitShort"/></th>
			<th><fmt:message key="price"/></th>
			<th><fmt:message key="total"/></th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="position" items="${partlist.positions}">
			<c:if test="${!empty position.items}">
				<tr>
					<td></td>
					<td colspan="6" class="boldtext">
						<o:out value="${position.name}"/>
					</td>
				</tr>
				<c:forEach var="item" items="${position.items}">
					<tr class="lightgrey">
						<td class="right">
							<a href="<c:url value="/products.do"><c:param name="method" value="load"/><c:param name="id" value="${item.product.productId}"/><c:param name="exit" value="partlist"/></c:url>">
								<o:out value="${item.product.productId}"/>
							</a>
						</td>
						<td>
							<o:out value="${item.product.name}" limit="52"/>
							<c:if test="${!empty item.note}">
								<div class="smalltext tab"><o:textOut value="${item.note}"/></div>
							</c:if>
						</td>
						<td class="right"><o:number value="${item.quantity}" format="decimal"/></td>
						<td class="center"><oc:options name="quantityUnits" value="${item.product.quantityUnit}"/></td>
						<td class="right"><o:number value="${item.price}" format="currency"/></td>
						<td class="right"><o:number value="${item.amount}" format="currency"/></td>
						<td class="center">
							<c:choose>
								<c:when test="${item.includePrice}"><o:img name="eyesIcon"/></c:when>
								<c:otherwise>&nbsp;</c:otherwise>
							</c:choose>
						</td>
					</tr>
				</c:forEach>
			</c:if>
		</c:forEach>
		<tr>
			<td colspan="7"></td>
		</tr>
		<tr class="lightgrey">
			<td></td>
			<td colspan="4" class="boldtext"><fmt:message key="summary"/></td>
			<td class="right boldtext"><o:number value="${partlist.price}" format="currency"/></td>
			<td></td>
		</tr>
	</tbody>
</table>