/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 1, 2007 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.Constants;
import com.osserp.common.dao.Tables;

import com.osserp.core.dao.ResourceProvider;
import com.osserp.core.model.system.SystemI18nConfigImpl;
import com.osserp.core.model.system.SystemI18nImpl;
import com.osserp.core.system.SystemI18n;
import com.osserp.core.system.SystemI18nConfig;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SpringResourceProviderDao extends AbstractDao implements ResourceProvider {
    private static Logger log = LoggerFactory.getLogger(SpringResourceProviderDao.class.getName());

    private String resourceTable;

    public SpringResourceProviderDao(JdbcTemplate jdbcTemplate, Tables tables, SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
        this.resourceTable = getTable("systemI18n");
    }

    public SystemI18nConfig loadConfig(Long id) {
        SystemI18nConfigImpl obj = (SystemI18nConfigImpl) getCurrentSession().load(SystemI18nConfigImpl.class, id);
        return obj;
    }

    public SystemI18nConfig loadConfigByLanguage(String language) {
        List list = getCurrentSession().createQuery("from "
                + SystemI18nConfigImpl.class.getName()
                + " obj where obj.name = :languageName")
                .setParameter("languageName", (isEmpty(language) ? Constants.DEFAULT_LANGUAGE : language)).list();
        return (list.isEmpty() ? null : (SystemI18nConfig) list.get(0));
    }

    public SystemI18n loadResource(Long id) {
        SystemI18nImpl obj = (SystemI18nImpl) getCurrentSession().load(SystemI18nImpl.class.getName(), id);
        return obj;
    }

    public List<SystemI18n> loadResources(Long configId) {
        return getCurrentSession().createQuery(
                "from " + SystemI18nImpl.class.getName() 
                + " obj where obj.configId = :configId")
                .setParameter("configId", configId).list();
    }

    public boolean isResourceExisting(String context, String name) {
        List<SystemI18n> existing = loadResources(context);
        for (int i = 0, j = existing.size(); i < j; i++) {
            SystemI18n next = existing.get(i);
            if (next.getName().equals(name)) {
                return true;
            }
        }
        return false;
    }

    public List<SystemI18n> loadResources(String context) {
        return getCurrentSession().createQuery("from " + SystemI18nImpl.class.getName()
                + " obj where obj.name like :likeValue")
                .setParameter("likeValue", (context + "%")).list();
    }

    public String createResourceName(String context) {
        StringBuilder sql = new StringBuilder("SELECT DISTINCT name FROM ");
        sql
                .append(resourceTable)
                .append(" WHERE name like '")
                .append(fetchInsecureInput(context))
                .append("%'");
        List existing = (List) jdbcTemplate.query(sql.toString(), getStringRowMapper());
        if (log.isDebugEnabled()) {
            log.debug("createResourceName() context lookup done [existingCount=" + existing.size() + "]");
        }
        int num = existing.size() + 1;
        while (findResourceByName(context + num) != null) {
            num++;
        }
        if (log.isDebugEnabled()) {
            log.debug("createResourceName() done [result=" + (context + num) + "]");
        }
        return context + num;
    }

    public void updateResource(String language, String name, String value) {
        SystemI18nConfig config = loadConfigByLanguage(language);
        SystemI18n entryI18n = findResourceByName(name);
        if (entryI18n == null) {
            entryI18n = new SystemI18nImpl(config.getId(), name, value);
        } else {
            entryI18n.setText(value);
        }
        getCurrentSession().saveOrUpdate(SystemI18nImpl.class.getName(), entryI18n);
    }

    protected SystemI18n findResourceByName(String name) {
        List list = getCurrentSession().createQuery("from " + SystemI18nImpl.class.getName()
                + " obj where obj.name = :name").setParameter("name", name).list();
        return (list.isEmpty() ? null : (SystemI18n) list.get(0));
    }

}
