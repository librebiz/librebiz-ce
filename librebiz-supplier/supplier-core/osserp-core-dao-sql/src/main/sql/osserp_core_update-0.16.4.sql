SET search_path = osserp, pg_catalog;

ALTER TABLE system_queries ALTER COLUMN fullscreen SET DEFAULT false;
ALTER TABLE system_queries ALTER COLUMN deleted SET DEFAULT false;

INSERT INTO system_query_context (id, name, context) VALUES (1, 'Abfragen allgemein', 'common');
INSERT INTO system_query_context (id, name, context) VALUES (2, 'Kontakte', 'contact');
INSERT INTO system_query_context (id, name, context) VALUES (3, 'Anfragen', 'request');
INSERT INTO system_query_context (id, name, context) VALUES (4, 'Aufträge', 'sales');
INSERT INTO system_query_context (id, name, context) VALUES (5, 'Einkauf', 'purchase');
INSERT INTO system_query_context (id, name, context) VALUES (6, 'Buchhaltung', 'accounting');
INSERT INTO system_query_context (id, name, context) VALUES (7, 'Produkte', 'products');

SELECT pg_catalog.setval('system_queries_id_seq', 100, false);
SELECT pg_catalog.setval('system_query_outputs_id_seq', 1000, false);
SELECT pg_catalog.setval('system_query_permissions_id_seq', 1000, false);
SELECT pg_catalog.setval('system_query_parameters_id_seq', 1000, false);

INSERT INTO user_permissions VALUES ('sales_revenue', 'Deckungsbeitragsrechnung', true, false, false, NULL, false);

UPDATE system_properties SET pvalue = '0.16.4', ptype = 'java.lang.String' WHERE name = 'dbmsSchemaVersion';
