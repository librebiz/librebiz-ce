/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 26, 2009 6:34:17 AM 
 * 
 */
package com.osserp.core.model.contacts.groups;

import com.osserp.common.beans.AbstractOption;
import com.osserp.core.employees.EmployeeDisplay;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class EmployeeDisplayVO extends AbstractOption implements EmployeeDisplay {

    private Long salutation = null;
    private Long contactId = null;
    private String firstName = null;
    private String lastName = null;
    private String street = null;
    private String zipcode = null;
    private String city = null;
    private boolean active = false;
    private int events = 0;

    protected EmployeeDisplayVO() {
        super();
    }

    protected EmployeeDisplayVO(Long id, String name, Long salutation, Long contactId, String firstName, String lastName, String street, String zipcode,
            String city, boolean active, int events) {
        super(id, name);
        this.salutation = salutation;
        this.contactId = contactId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.street = street;
        this.zipcode = zipcode;
        this.city = city;
        this.active = active;
        this.events = events;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Long getContactId() {
        return contactId;
    }

    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }

    public int getEvents() {
        return events;
    }

    public void setEvents(int events) {
        this.events = events;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    protected boolean isActive() {
        return active;
    }

    protected void setActive(boolean active) {
        this.active = active;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String getName() {
        if (super.getName() == null) {
            super.setName(createName());
        }
        return super.getName();
    }

    /*
     * public void setName(String name) { this.name = name; }
     */
    private String createName() {
        if (firstName == null) {
            return (lastName == null) ? "" : lastName;
        }
        return ((lastName == null) ? "" : lastName) + ", " + firstName;
    }

    public Long getSalutation() {
        return salutation;
    }

    public void setSalutation(Long salutation) {
        this.salutation = salutation;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    @Override
    public Object clone() {
        return new EmployeeDisplayVO(
                getId(),
                getName(),
                salutation,
                contactId,
                firstName,
                lastName,
                street,
                zipcode,
                city,
                active,
                events);
    }

}
