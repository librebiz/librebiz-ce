<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<o:page />
<c:set var="portalView" value="${sessionScope.portalView}" />
<!DOCTYPE html>
<html lang="de">
<head>
<jsp:include page="/${applicationScope.webConfig.layoutDefaultPath}/shared/head_application_css.jsp" />
<tiles:getAsString name="styles" ignore="true" />
<tiles:insert attribute="javascript" ignore="true" />
<title><tiles:getAsString name="title" ignore="true" /></title>
</head>

<body <tiles:getAsString name="onload" ignore="true"/>>

  <jsp:include page="/${applicationScope.webConfig.layoutDefaultPath}/shared/header.jsp" />

  <div class="container-fluid">
    <div class="row">
      <%-- Navigation left --%>
      <div class="col-sm-3 col-md-2 sidebar">
        <div class="navlist">
          <jsp:include page="/${applicationScope.webConfig.layoutDefaultPath}/shared/navigation.jsp" />
        </div>
      </div>

      <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

        <div class="page-header">
          <c:import url="/${applicationScope.webConfig.layoutDefaultPath}/shared/help_config.jsp" />
          <div id="content_header">
            <div id="content_header_left"></div>
            <div id="content_header_middle">
              <fmt:message key="errorNotification" />
            </div>
            <div id="content_header_right">
              <div class="contentnav">
                <tiles:getAsString name="headline_right" ignore="true" />
              </div>
            </div>
          </div>
        </div>

        <div class="content-area" id="errorContent">
          <div class="row">
            <div class="col-md-6 panel-area panel-area-default">
              <div class="panel-body" style="padding-top:2em;">
                <div class="row">
                  <div class="col-md-12">
                    <div class="alert alert-danger" role="alert">
                      <tiles:getAsString name="content" ignore="true" />
                    </div>
                    <br/>
                    <p><strong><fmt:message key="doNotSendScreenshotsOfWellKnownPagesOnErrorsPlease" /></strong></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
  <c:import url="/layout/shared/bottom_legacy.jsp" />
</body>
</html>
