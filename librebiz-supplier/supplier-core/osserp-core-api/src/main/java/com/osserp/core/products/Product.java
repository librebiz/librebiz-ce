/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 08.10.2004 
 * 
 */
package com.osserp.core.products;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.jdom2.Element;

import com.osserp.common.Details;
import com.osserp.common.PermissionException;
import com.osserp.common.PersistentObject;

import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Product extends Details, PersistentObject, PriceAware, Serializable {

    static final Long DATASHEET = 9L;
    static final Long PICTURE = 10L;

    static final Long PLANNING_BY_STOCK = 0L;
    static final Long PLANNING_BY_ORDER = 1L;

    /**
     * Returns the product id
     * @return groupId
     */
    Long getProductId();

    /**
     * Indicates if product is assigned to types (types not empty)
     * @return true if products types not empty
     */
    boolean isTypeAvailable();

    /**
     * Indicates that product is member of a type
     * @param type
     * @return true if products types contain given type
     */
    boolean isType(ProductType type);

    /**
     * Returns all products types
     * @return types
     */
    List<ProductType> getTypes();

    /**
     * Provides the first associated type
     * @return type or null if no type assigned
     */
    ProductType getTypeDefault();

    /**
     * Provides a comma separated list of all assigned types
     * @return typeDisplay
     */
    String getTypeDisplay();

    /**
     * Returns the product group
     * @return group
     */
    ProductGroup getGroup();

    /**
     * Returns the products default category
     * @return category
     */
    ProductCategory getCategory();

    /**
     * Provides the quantity unit for this product
     * @return quantityUnit
     */
    Long getQuantityUnit();

    /**
     * Sets the quantity unit for this product
     * @param quantityUnit
     */
    void setQuantityUnit(Long quantityUnit);

    /**
     * Indicates if product quantity is time type
     * @return true if so
     */
    boolean isQuantityUnitTime();

    /**
     * Enables/disables quantity time flag
     * @param quantityUnitTime
     */
    void setQuantityUnitTime(boolean quantityUnitTime);

    /**
     * Provides the optional power value 
     * @return power
     */
    Double getPower();

    /**
     * Sets the power value
     * @param power
     */
    void setPower(Double power);

    /**
     * Provides the id of the power unit
     * @return powerUnit
     */
    Long getPowerUnit();

    /**
     * Sets the id of the power unit
     * @param powerUnit
     */
    void setPowerUnit(Long powerUnit);

    /**
     * The manufacturer
     * @return manufacturer
     */
    Long getManufacturer();

    /**
     * Sets the manufacturer
     * @param manufacturer
     */
    void setManufacturer(Long manufacturer);

    /**
     * The matchcode
     * @return matchcode
     */
    String getMatchcode();

    /**
     * Sets the matchcode
     * @param matchcode
     */
    void setMatchcode(String matchcode);

    /**
     * The full name of the product
     * @return name
     */
    String getName();

    /**
     * Sets the product name
     * @param name
     */
    void setName(String name);

    /**
     * The product description
     * @return description
     */
    String getDescription();

    /**
     * Sets the description
     * @param description
     */
    void setDescription(String description);

    /**
     * A short version of the product description. 
     * @return descriptionShort
     */
    String getDescriptionShort();

    /**
     * Sets the short version of the description
     * @param descriptionShort
     */
    void setDescriptionShort(String descriptionShort);

    /**
     * The marketing text of the product
     * @return marketingText
     */
    String getMarketingText();

    /**
     * Sets marketing text for the product
     * @param marketingText
     */
    void setMarketingText(String marketingText);
    
    /**
     * The GTIN-13 code of the product, or the product to which the offer refers. 
     * This is equivalent to 13-digit ISBN codes and EAN UCC-13. 
     * @return value or null
     */
    String getGtin13();
    
    /**
     * Sets the GTIN-13 code
     * @param gtin13
     */
    void setGtin13(String gtin13);
    
    /**
     * The GTIN-14 code of the product, or the product to which the offer refers.
     * @return value or null
     */
    String getGtin14();
    
    /**
     * Sets the GTIN-14 code
     * @param gtin14
     */
    void setGtin14(String gtin14);
    
    /**
     * The GTIN-8 code of the product, or the product to which the offer refers.
     * This code is also known as EAN/UCC-8 or 8-digit EAN.
     * @return
     */
    String getGtin8(); 
    
    /**
     * Sets the GTIN-8 code
     * @param gtin8
     */
    void setGtin8(String gtin8);

    /**
     * Indicates that a data sheet for this product is available
     * @return datasheetAvailable
     */
    boolean isDatasheetAvailable();

    /**
     * Sets that a data sheet for this product is available
     * @param datasheetAvailable
     */
    void setDatasheetAvailable(boolean datasheetAvailable);

    /**
     * Indicates that a picture for this product is available
     * @return pictureAvailable
     */
    boolean isPictureAvailable();

    /**
     * Sets that a picture for this product is available
     * @param pictureAvailable
     */
    void setPictureAvailable(boolean pictureAvailable);

    /**
     * returns the data sheet text
     * @return datasheetText
     */
    String getDatasheetText();

    /**
     * Sets the data sheet text
     * @param datasheetText
     */
    void setDatasheetText(String datasheetText);

    /**
     * Indicates that the product affects the stock
     * @return affectsStock
     */
    boolean isAffectsStock();

    /**
     * Sets the product affects the stock
     * @param affectsStock
     */
    void setAffectsStock(boolean affectsStock);

    /**
     * Indicates that product is available on any assigned stock
     * @return true if any assigned stock reports availability
     */
    boolean isAvailableOnAnyStock();

    /**
     * Indicates that this products end of life has reached
     * @return endOfLife
     */
    boolean isEndOfLife();

    /**
     * Sets that this products end of life has reached
     * @param endOfLife
     */
    void setEndOfLife(boolean endOfLife);

    /**
     * The creation-date of the entity. This value is set at creation time
     * @return created
     */
    Date getCreated();

    /**
     * The user who created this entity. This value is set at creation time
     * @return createdBy
     */
    Long getCreatedBy();

    /**
     * The last changed-date of the entity. This value is set at creation time
     * @return changed
     */
    Date getChanged();

    /**
     * Sets the date when the entity data was changed
     * @param changed
     */
    void setChanged(Date changed);

    /**
     * The user who changed this entity. This value is set at creation time
     * @return changedBy
     */
    Long getChangedBy();

    /**
     * Sets the user who changed the entity data
     * @param changedBy
     */
    void setChangedBy(Long changedBy);

    /**
     * Indicates that this product is sold with reduced tax rate
     * @return reducedTax
     */
    boolean isReducedTax();

    /**
     * Sets that this product is sold with reduced tax rate
     * @param reducedTax
     */
    void setReducedTax(boolean reducedTax);

    /**
     * Indicates that serial numbers are available
     * @return serialAvailable
     */
    boolean isSerialAvailable();

    /**
     * Indicates that serial numbers are required
     * @return serialRequired
     */
    boolean isSerialRequired();

    /**
     * Changes products serial number flags
     * @param available
     * @param required
     */
    void changeSerialFlags(boolean available, boolean required);

    /**
     * Indicates if product data is available for the public (e.g. data should be synchronized 
     * with an external client if one available).
     * @return true if so
     */
    boolean isPublicAvailable();

    /**
     * Enables/disables publish external flag
     * @param publicAvailable
     */
    void setPublicAvailable(boolean publicAvailable);

    /**
     * Indicates that the price of the product is automatically calculated by purchase prices
     * @return true if so
     */
    boolean isAutoCalculated();

    /**
     * Enables/disables auto calculation flag
     * @param autoCalculated
     */
    void setAutoCalculated(boolean autoCalculated);

    /**
     * Indicates that auto calculation is done by group margins
     * @return true if so
     */
    boolean isCalculateByGroup();

    /**
     * Enables/disables calculate by group
     * @param calculateByGroup
     */
    void setCalculateByGroup(boolean calculateByGroup);

    /**
     * Provides the estimated purchase price
     * @return estimatedPurchasePrice
     */
    Double getEstimatedPurchasePrice();

    /**
     * Sets the estimated purchase price
     * @param estimatedPurchasePrice
     */
    void setEstimatedPurchasePrice(Double estimatedPurchasePrice);

    /**
     * Provides the calculation purchase price. Returns estimated purchase price or last purchase price if estimated price is empty
     * @return calculationPurchasePrice
     */
    Double getCalculationPurchasePrice();

    /**
     * Provides a text for purchasing
     * @return purchaseText
     */
    String getPurchaseText();

    /**
     * Sets a text for purchasing
     * @param purchaseText
     */
    void setPurchaseText(String purchaseText);

    /**
     * Provides the packaging unit
     * @return packagingUnit
     */
    Integer getPackagingUnit();

    /**
     * Sets the packaging unit
     * @param packagingUnit
     */
    void setPackagingUnit(Integer packagingUnit);

    /**
     * Provides the default language for name & description
     * @return defaultLanguage
     */
    String getDefaultLanguage();

    /**
     * Sets the default language for name & description
     * @param defaultLanguage
     */
    void setDefaultLanguage(String defaultLanguage);

    /**
     * Indicates a customizable product
     * @return custom
     */
    boolean isCustom();

    /**
     * Indicates a customizable product (enables name override support)
     * @param custom true to enable overriding of names in records.
     */
    void setCustom(boolean custom);

    /**
     * Indicates that product is virtual product
     * @return true if so
     */
    boolean isVirtual();

    /**
     * Sets this product as virtual
     * @param virtual
     */
    void setVirtual(boolean virtual);

    /**
     * Indicates that product represents a plant
     * @return plant
     */
    boolean isPlant();

    /**
     * Sets that this product repesents a plant
     * @param plant
     */
    void setPlant(boolean plant);

    /**
     * Indicates that product is a bundle
     * @return isBundle
     */
    boolean isBundle();

    /**
     * Sets that product is a bundle
     * @param bundle
     */
    void setBundle(boolean bundle);

    /**
     * Indicates that this product is nullable in orders
     * @return nullable
     */
    boolean isNullable();

    /**
     * Sets that the product is nullable in orders
     * @param nullable
     */
    void setNullable(boolean nullable);

    /**
     * Indicates that this products stock rotation is managed by kanban
     * @return kanban
     */
    boolean isKanban();

    /**
     * Sets that this products stock rotation is managed by kanban
     * @param kanban
     */
    void setKanban(boolean kanban);

    /**
     * Indicates that this product is a component. A component acts as a placeholder for an individual set of other products.
     * @return component
     */
    boolean isComponent();

    /**
     * Sets that this product is a component set.
     * @param component
     */
    void setComponent(boolean component);

    /**
     * Indicates that product represents a product with term
     * @return true if so
     */
    boolean isTerm();

    /**
     * Enables/disables term flag
     * @param term
     */
    void setTerm(boolean term);

    /**
     * Indicates that this product is price aware so sales price checking by purchase prices is possible
     * @return priceAware
     */
    boolean isPriceAware();

    /**
     * Sets that this product is price aware
     * @param priceAware
     */
    void setPriceAware(boolean priceAware);

    /**
     * Indicates that product's price depends on an external provided quantity (example: plantPower)
     * @return priceByExternalQuanity true if (quantityUnit == empty && priceByQuantity == true)
     */
    boolean isPriceByExternalQuantity();

    /**
     * Indicates that product's price depends on quantity
     * @return priceByQuantity
     */
    boolean isPriceByQuantity();

    /**
     * Sets that price depends on quantity
     * @param priceByQuantity
     */
    void setPriceByQuantity(boolean priceByQuantity);

    /**
     * Provides readonly view of current price matrix.<br>
     * Hint: The matrix is configured by the dedicated configuration object ProductPriceByQuantityMatrix
     * @return priceByQuantityMatrix
     */
    List<ProductPriceByQuantity> getPriceByQuantityMatrix();

    /**
     * Provides consumer price by quantity
     * @param quantity
     * @return price
     */
    Double getConsumerPriceByQuantity(Double quantity);

    /**
     * Provides partner price by quantity
     * @param quantity
     * @return price
     */
    Double getPartnerPriceByQuantity(Double quantity);

    /**
     * Provides reseller price by quantity
     * @param quantity
     * @return price
     */
    Double getResellerPriceByQuantity(Double quantity);

    /**
     * Indicates that a billing note is required for this product
     * @return true if so
     */
    boolean isBillingNoteRequired();

    /**
     * Enables/disables that a billing note is required
     * @param billingNoteRequired
     */
    void setBillingNoteRequired(boolean billingNoteRequired);

    /**
     * Indicates that quantity is copied from plant if exists
     * @return true if so
     */
    boolean isQuantityByPlant();

    /**
     * Enables/disables that quantity is copied by plant
     * @param quantityByPlant
     */
    void setQuantityByPlant(boolean quantityByPlant);

    /**
     * Indicates that product should only be used in offers
     * @return true if so
     */
    boolean isOfferUsageOnly();

    /**
     * Enables/disables that product should only be used in offers
     * @param offerUsageOnly
     */
    void setOfferUsageOnly(boolean offerUsageOnly);

    /**
     * Indicates that product is service
     * @return service
     */
    boolean isService();

    /**
     * Sets that product is service
     * @param service
     */
    void setService(boolean service);

    /**
     * Indicates that purchase price limit flag is enabled
     * @return true if so
     */
    boolean isIgnorePurchasePriceLimit();

    /**
     * Changes the purchase price limit flag
     * @param user
     * @param perms required
     * @throws PermissionException if user has no permissions
     */
    void changeIgnorePurchasePriceFlag(DomainUser user, String[] perms) throws PermissionException;

    /**
     * Provides product details
     * @return details
     */
    ProductDetails getDetails();

    /**
     * Indicates whether details are available
     * @return detailAvailable.
     */
    boolean isDetailAvailable();

    /**
     * Provides the product summary
     * @return summary
     */
    ProductSummary getSummary();

    /**
     * Adds the product summary
     * @param summary
     */
    void addSummary(ProductSummary summary);

    /**
     * Provides the current selected stock of current summary
     * @return currentStock
     */
    Long getCurrentStock();

    /**
     * Sets the current selected stock of current summary
     * @param currentStock
     */
    void setCurrentStock(Long currentStock);

    /**
     * Provides the default stock to use when none selected
     * @return defaultStock
     */
    Long getDefaultStock();

    /**
     * Sets the default stock
     * @param stock
     */
    void setDefaultStock(Long stock);

    /**
     * Enables a stock (e.g. loads summary of that stock)
     * @param stockId
     */
    void enableStock(Long stockId);

    /**
     * Indicates if product is used for planning.
     * @return true if activated for planning
     */
    boolean isPlanning();

    /**
     * Enables/disables planning use
     * @param planning
     */
    void setPlanning(boolean planning);

    /**
     * Provides products planning mode
     * @return planingMode
     */
    ProductPlanningMode getPlanningMode();

    /**
     * Indicates that product is delivery note affecting. <br/>
     * This is the case if: <br/>
     * (bundle) or (affectsStock and !virtual and !service) <br/>
     * @return deliveryNoteAffecting
     */
    boolean isDeliveryNoteAffecting();

    /**
     * Indicates that product is lazy siganling that all summary values are zero
     * @return lazy
     */
    boolean isLazy();

    /**
     * Indicates that product provides a specific partner price
     * @return true if so
     */
    boolean isSpecificPartnerPriceAvailable();

    /**
     * Indicates if partner price is specific (depending on quantity unit for example)
     * @return specificPartnerPrice
     */
    Double getSpecificPartnerPrice();

    /**
     * Provides the purchase price to calculate the price minimum.
     * @return purchasePrice (lastPurchasePrice ?: averagePurchasePrice ?: estimatedPurchasePrice ?: 0)
     */
    Double getPurchasePriceForMinimumCalculation();
    
    /**
     * Provides an xml representation of product values
     * @param name of the created element
     * @return product values
     */
    Element getXML(String rootElementName);

    /**
     * Provides an xml representation of product values
     * @return product values
     */
    Element getXML();
}
