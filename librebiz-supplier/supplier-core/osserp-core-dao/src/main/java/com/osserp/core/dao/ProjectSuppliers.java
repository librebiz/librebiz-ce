/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.dao;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.osserp.core.BusinessCase;
import com.osserp.core.employees.Employee;
import com.osserp.core.projects.ProjectSupplier;
import com.osserp.core.suppliers.Supplier;

/**
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface ProjectSuppliers {

    /**
     * Provides supplier by purchase
     * @param voucherType
     * @param voucherId
     * @return supplier reference or null if not exists
     */
    ProjectSupplier findByVoucher(Long voucherType, Long voucherId);

    /**
     * Provides project supplier relation by id
     * @param id
     * @return project supplier relation
     */
    ProjectSupplier getProjectSupplier(Long id);

    /**
     * Provides all suppliers by project
     * @param businessCaseId
     * @return suppliers
     */
    List<ProjectSupplier> getSuppliers(Long businessCaseId);

    /**
     * Creates a new project supplier entry
     * @param user
     * @param businessCase
     * @param supplier
     * @param groupName
     * @param deliveryDate
     * @return project supplier
     */
    ProjectSupplier create(
            Employee user,
            BusinessCase businessCase,
            Supplier supplier,
            String groupName,
            Date deliveryDate);

    /**
     * Creates a new project supplier using default settings
     * @param supplier
     * @param created
     * @param createdBy
     * @param businessCaseId
     * @param voucherId
     * @param voucherType
     * @param groupName
     * @param deliveryDate
     * @return project supplier
     */
    ProjectSupplier createDefault(
            Supplier supplier,
            Date created,
            Long createdBy,
            Long businessCaseId,
            Long voucherId,
            Long voucherType,
            String groupName,
            Date deliveryDate);

    /**
     * Removes a supplier from project
     * @param supplier
     * @param includePurchaseOrder
     */
    void remove(ProjectSupplier supplier, boolean includePurchaseOrder);

    /**
     * Persists a supplier reference
     * @param supplier
     */
    void save(ProjectSupplier supplier);

    /**
     * Provides all available product group and custom supplier group names
     * @return group names
     */
    Set<String> findGroupNames();

}
