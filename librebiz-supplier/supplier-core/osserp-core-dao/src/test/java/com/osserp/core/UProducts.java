/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core;

import static org.junit.Assert.assertTrue;

import java.io.FileWriter;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jdom2.Element;

import org.junit.Test;

import com.osserp.common.service.impl.SpringContextImpl;
import com.osserp.common.xml.JDOMUtil;

import com.osserp.core.dao.Products;
import com.osserp.core.products.Product;

public class UProducts {
    private static Logger log = LoggerFactory.getLogger(UProducts.class.getName());

    @Test
    public void createProductXml() {
        assertTrue(true);
        Long start = System.currentTimeMillis();

        Properties props = new Properties();
        props.setProperty("ctx.i18n.path", "com/osserp/common/resources/application");
        props.setProperty("ctx.xml.path", "com/osserp/core/resources/UProducts.xml");
        SpringContextImpl ctx = new SpringContextImpl(props);
        Products productsDao = (Products) ctx.getService(Products.class.getName());

        Long end = System.currentTimeMillis();
        System.out.println("creating appCtx took " + (end - start) + "ms");

        start = System.currentTimeMillis();
        List<Product> products = productsDao.findByGroup(1L);
        //List<Product> products = new ArrayList<>();
        //products.add(productsDao.load(1101013L));

        end = System.currentTimeMillis();
        System.out.println("sql query took " + (end - start) + "ms");

        for (Iterator<Product> iterator = products.iterator(); iterator.hasNext();) {
            Product product = iterator.next();
            Element root = new Element("root");

            root.addContent(product.getXML("product"));
            try {
                FileWriter fw = new FileWriter("/tmp/productXmlTest/product_" + product.getProductId() + ".xml");
                fw.append(JDOMUtil.getPrettyString(root));
                fw.flush();
                fw.close();
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }

}
