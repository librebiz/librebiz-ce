<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${!empty requestScope.selectExitTarget}">
    <c:set var="exitTarget" value="${requestScope.selectExitTarget}" />
</c:if>
<c:set var="list" value="${requestScope.collection}" />

<div class="col-md-12 panel-area">
    <div class="table-responsive table-responsive-default">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th class="recordType"><fmt:message key="type" /></th>
                    <th class="recordNumber"><fmt:message key="number" /></th>
                    <th class="recordDate"><fmt:message key="ofDate" /></th>
                    <th class="recordName"><fmt:message key="customer" /></th>
                    <th class="recordNumber"><fmt:message key="referenceNumberShort" /></th>
                    <th class="recordAmount"><fmt:message key="turnover" /></th>
                    <th class="center"> </th>
                </tr>
            </thead>
            <tbody>
                <c:choose>
                    <c:when test="${empty list}">
                        <tr>
                            <td colspan="7"><span><fmt:message key="noRecordsAvailable" /></span></td>
                        </tr>
                    </c:when>
                    <c:otherwise>
                        <c:forEach var="record" items="${list}" varStatus="s">
                            <tr id="record${record.id}">
                                <td class="recordType"><o:out value="${record.type.numberPrefix}" /></td>
                                <td class="recordNumber">
                                    <c:choose>
                                        <c:when test="${record.downpayment}">
                                            <a href="<c:url value="/salesInvoice.do?method=displayDownpayment&exit=${exitTarget}&exitId=record${record.id}&readonly=true&id=${record.id}"/>"><o:out value="${record.number}" /></a>
                                        </c:when>
                                        <c:when test="${record.invoice}">
                                            <a href="<c:url value="/salesInvoice.do?method=display&exit=${exitTarget}&exitId=record${record.id}&readonly=true&id=${record.id}"/>"><o:out value="${record.number}" /></a>
                                        </c:when>
                                        <c:when test="${record.creditNote}">
                                            <a href="<c:url value="/salesCreditNote.do?method=display&exit=${exitTarget}&exitId=record${record.id}&readonly=true&id=${record.id}"/>"><o:out value="${record.number}" /></a>
                                        </c:when>
                                        <c:otherwise>
                                            <o:out value="${record.number}" />
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td class="recordDate"><o:date value="${record.created}" /></td>
                                <td class="recordName"><o:out value="${record.name}" /></td>
                                <td class="recordNumber"><o:out value="${record.reference}" /></td>
                                <td class="recordAmount"><o:number value="${record.amount}" format="currency" /></td>
                                <td class="center icon">
                                    <c:if test="${record.unchangeable}">
                                        <c:choose>
                                            <c:when test="${record.downpayment}">
                                                <v:link url="/records/recordPrint/print?name=salesDownpayment&id=${record.id}" title="documentPrint">
                                                    <o:img name="printIcon" />
                                                </v:link>
                                            </c:when>
                                            <c:when test="${record.invoice}">
                                                <v:link url="/records/recordPrint/print?name=salesInvoice&id=${record.id}" title="documentPrint">
                                                    <o:img name="printIcon" />
                                                </v:link>
                                            </c:when>
                                            <c:when test="${record.creditNote}">
                                                <v:link url="/records/recordPrint/print?name=salesCreditNote&id=${record.id}" title="documentPrint">
                                                    <o:img name="printIcon" />
                                                </v:link>
                                            </c:when>
                                        </c:choose>
                                    </c:if>
                                </td>
                            </tr>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
            </tbody>
        </table>
    </div>
</div>
