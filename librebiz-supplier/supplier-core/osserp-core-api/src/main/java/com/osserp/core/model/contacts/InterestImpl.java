/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 31, 2007 5:18:10 PM 
 * 
 */
package com.osserp.core.model.contacts;

import java.util.Date;

import org.jdom2.Element;

import com.osserp.common.Option;
import com.osserp.common.xml.JDOMUtil;
import com.osserp.core.contacts.ContactType;
import com.osserp.core.contacts.Interest;
import com.osserp.core.contacts.Salutation;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class InterestImpl extends AbstractContactReference implements Interest {

    private Long client = null;
    private String description = null;
    private String comment = null;

    protected InterestImpl() {
        super();
    }

    protected InterestImpl(
            Long client,
            ContactType type,
            String company,
            Salutation salutation,
            Option title,
            boolean firstNamePrefix,
            String firstName,
            String lastName,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            String description) {

        super(
                type,
                company,
                salutation,
                title,
                firstNamePrefix,
                firstName,
                lastName,
                street,
                streetAddon,
                zipcode,
                city);
        this.client = client;
        this.description = description;
    }

    protected InterestImpl(Interest other) {
        super(other);
        this.client = other.getClient();
        this.description = other.getDescription();
        this.comment = other.getComment();
    }

    public InterestImpl(Element contact) {
        super(contact);
        this.createCommunications(contact);
        this.client = JDOMUtil.fetchLong(contact, "clientId");
        this.description = contact.getChildText("description");
        this.comment = contact.getChildText("comment");
        setStatus(Interest.STATUS_CREATED);
        Date created = JDOMUtil.fetchDate(contact, "created");
        setPersonCreated(created == null ? new Date(System.currentTimeMillis()) : created);
    }

    @Override
    public Object clone() {
        return new InterestImpl(this);
    }

    public Long getClient() {
        return client;
    }

    public void setClient(Long client) {
        this.client = client;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public Element getXML() {
        Element xml = super.getXML("interest");
        if (client != null) {
            xml.addContent(new Element("clientId").setText(client.toString()));
        } else {
            xml.addContent(new Element("clientId"));
        }
        if (description != null) {
            xml.addContent(new Element("description").setText(description));
        } else {
            xml.addContent(new Element("description"));
        }
        if (comment != null) {
            xml.addContent(new Element("comment").setText(comment));
        } else {
            xml.addContent(new Element("comment"));
        }
        return xml;
    }
}
