/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 6, 2004 
 * 
 */
package com.osserp.common;

import java.util.Map;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface PropertyAware extends Entity {

    /**
     * Indicates enabled property
     * @param propertyName
     * @return true if property exists and value is true
     */
    boolean isPropertyEnabled(String propertyName);

    /**
     * Provides property value as long or default value if not exists
     * @param name
     * @param defaultValue
     * @return value or default
     */
    Long propertyAsLong(String name, Long defaultValue);

    /**
     * Provides additional user properties
     * @return properties
     */
    Map<String, Property> getProperties();

    /**
     * Sets/unsets a property
     * @param user property setting user
     * @param name property name
     * @param value property value or null to remove existing
     */
    void setProperty(User user, String name, String value);
}
