/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 7, 2006 8:47:53 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.Details;
import com.osserp.common.Option;
import com.osserp.common.OptionsCache;
import com.osserp.common.Property;
import com.osserp.common.User;
import com.osserp.common.dao.Tables;
import com.osserp.common.util.CollectionUtil;

import com.osserp.core.Comparators;
import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.Salutation;
import com.osserp.core.customers.Customer;
import com.osserp.core.dao.Contacts;
import com.osserp.core.dao.Customers;
import com.osserp.core.dao.Emails;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.model.contacts.ContactImpl;
import com.osserp.core.model.contacts.groups.CustomerImpl;
import com.osserp.core.model.contacts.groups.CustomerPropertyImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CustomersDao extends ClassifiedContactsDao implements Customers {
    private static Logger log = LoggerFactory.getLogger(CustomersDao.class.getName());
    private String customerSequence = null;
    private String customersTable = null;

    public CustomersDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            Contacts contacts,
            Emails emails,
            OptionsCache namesCache) {
        super(jdbcTemplate, tables, sessionFactory, contacts, emails, namesCache);
        customersTable = getTable(TableKeys.CUSTOMERS);
        customerSequence = getTable(TableKeys.CUSTOMER_SEQUENCE);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.impl.AbstractContactAwareDao#getEntityClass()
     */
    @Override
    protected final Class<CustomerImpl> getEntityClass() {
        return CustomerImpl.class;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.impl.AbstractContactAwareDao#getEntityTable()
     */
    @Override
    protected String getEntityTable() {
        return customersTable;
    }

    public ClassifiedContact create(Long createdBy, Contact contact, Long customId) {
        Long id = !isEmpty(customId) ? customId : getNextLong(customerSequence);
        if (exists(id)) {
            return load(id);
        }
        CustomerImpl obj = new CustomerImpl(id, contact, createdBy);
        if (obj.getRc() instanceof ContactImpl) {
            ContactImpl ci = (ContactImpl) obj.getRc();
            ci.setCustomer(true);
            ci.setOther(false);
        }
        save(obj);
        return obj;
    }

    public Customer create(Long primaryKey, Contact contact, Long createdBy, boolean reseller) {
        CustomerImpl obj = (CustomerImpl) create(createdBy, contact, primaryKey);
        if (reseller) {
            obj.setTypeId(Customer.TYPE_RESELLER);
        }
        save(obj);
        return obj;
    }

    public ClassifiedContact find(Long id) {
        try {
            return load(id);
        } catch (Throwable t) {
            return null;
        }
    }

    public Customer load(Long id) {
        CustomerImpl obj = (CustomerImpl) getCurrentSession().load(CustomerImpl.class, id);
        return obj;
    }
    
    public List<Customer> findAgents() {
        StringBuilder query = new StringBuilder("from ");
        query.append(CustomerImpl.class.getName()).append(" o where o.agent = true");
        List<Customer> result = getCurrentSession().createQuery(query.toString()).list();
        return CollectionUtil.sort(result, Comparators.createCustomerNameComparator(false));
    }
    
    public List<Customer> findTipProviders() {
        StringBuilder query = new StringBuilder("from ");
        query.append(CustomerImpl.class.getName()).append(" o where o.tipProvider = true");
        List<Customer> result = getCurrentSession().createQuery(query.toString()).list();
        return CollectionUtil.sort(result, Comparators.createCustomerNameComparator(false));
    }

    public List<Customer> getCustomers(Long[] customerIds) {
        List<Customer> list = new ArrayList<Customer>();
        if (customerIds == null || customerIds.length < 1) {
            return list;
        }
        Set<Long> existing = new HashSet<Long>();
        for (int i = 0, j = customerIds.length; i < j; i++) {
            Long cid = customerIds[i];
            if (!existing.contains(cid)) {
                try {
                    existing.add(cid);
                    list.add(load(cid));
                } catch (Throwable ignorable) {
                    log.warn("getCustomers() failed on customer id " + customerIds[i]);
                }
            }
        }
        return list;
    }

    public Customer fetchPrimaryPosAccount() {
        StringBuffer query = new StringBuffer(128);
        query
                .append("SELECT id FROM ")
                .append(customersTable)
                .append(" WHERE type_id = ")
                .append(Customer.TYPE_POSACCOUNT)
                .append(" AND shortkey = 'POS'");
        List<Long> result = (List<Long>) jdbcTemplate.query(
                query.toString(),
                getLongRowMapper());
        return result.isEmpty() ? null : load(result.get(0));
    }

    public Customer fetchInternal(Long company) {
        StringBuffer query = new StringBuffer(128);
        query
                .append("SELECT id FROM ")
                .append(customersTable)
                .append(" WHERE contact_id IN (SELECT contact_id FROM ")
                .append(getTable(TableKeys.SYSTEM_COMPANIES))
                .append(" WHERE id = ?)");
        List<Long> result = (List<Long>) jdbcTemplate.query(
                query.toString(),
                new Object[] { company },
                new int[] { Types.BIGINT },
                getLongRowMapper());
        return result.isEmpty() ? null : load(result.get(0));
    }

    private static final String LIST =
            "customers.id,customers.contact_id," +
                    "contacts.salutation,contacts.firstname,contacts.lastname," +
                    "contacts.street,contacts.zipcode,contacts.city";

    /**
     * Provides the basic statement required for queries
     * @return query
     */
    @Override
    protected StringBuilder getQuery() {
        StringBuilder query = new StringBuilder(64);
        query
                .append("SELECT ")
                .append(LIST)
                .append(" FROM ")
                .append(customersTable)
                .append(",")
                .append(contactsTable)
                .append(" WHERE customers.contact_id = contacts.contact_id");
        return query;
    }

    /**
     * Provides the statement required for ordered queries
     * @return orderedQuery
     */
    @Override
    protected String getOrderedQuery() {
        StringBuilder query = getQuery();
        query.append(" ORDER BY lastname,firstname");
        return query.toString();
    }

    public Details addProperty(User user, Details details, String name, String value) {
        Customer customer = (Customer) details;
        CustomerPropertyImpl property = new CustomerPropertyImpl(
                (user == null ? null : user.getId()), customer.getId(), name, value);
        getCurrentSession().saveOrUpdate(CustomerPropertyImpl.class.getName(), property);
        return load(customer.getId());
    }

    public Details removeProperty(User user, Details details, Property property) {
        Customer customer = (Customer) details;
        customer.removeProperty((user == null ? null : user.getId()), property);
        save(customer);
        return customer;
    }

    public Details updateProperty(User user, Details details, Property property, String value) {
        Customer customer = (Customer) details;
        if (!isEmpty(value)) {
            property.setValue(value);
            if (user != null) {
                property.updateChanged(user.getId());
            }
            getCurrentSession().saveOrUpdate(CustomerPropertyImpl.class.getName(), property);
        }
        return load(customer.getId());
    }

    public Customer update(
            Customer customer, 
            Salutation salutation, 
            Option title, 
            String firstName, 
            String lastName, 
            String street, 
            String streetAddon,
            String zipcode, 
            String city, 
            Long country, 
            Date birthDate, 
            boolean grantEmail,
            boolean grantPhone,
            boolean grantNone,
            String fax, 
            String phone, 
            String mobile) {

        Customer updated = (Customer) update(
                customer,
                salutation,
                title,
                firstName,
                lastName,
                street,
                streetAddon,
                zipcode,
                city,
                country,
                birthDate,
                grantEmail,
                null, // grantEmailNote,
                grantPhone,
                null, // grantPhoneNote,
                grantNone,
                null); // grantNoneNote) {
        // TODO handle phone numbers
        return updated;
    }

}
