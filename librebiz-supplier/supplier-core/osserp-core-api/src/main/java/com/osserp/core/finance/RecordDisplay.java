/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 18, 2008 1:37:12 PM 
 * 
 */
package com.osserp.core.finance;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.osserp.core.products.ProductSelectionConfigItem;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface RecordDisplay extends FinanceRecordDisplay {
    
    /**
     * The id of the related record type
     * @return type
     */
    Long getType();

    /**
     * The type specific sub type if provided by record type
     * @return subType or null if not supported
     */
    Long getSubType();

    /**
     * Provides the amount of included vat calculated by base rate
     * @return tax amount
     */
    Double getTax();

    /**
     * Provides the summary amount of vat by base and reduced rate 
     * @return summary of tax and reducedTax
     */
    Double getTaxTotal();

    /**
     * Provides the amount of included vat calculated by reduced rate
     * @return reduced tax amount
     */
    Double getReducedTax();

    /**
     * The primary key of selected payment condition
     * @return payment condition id
     */
    Long getPaymentConditionId();

    /**
     * The optional id of the employee signing the record on left side 
     * @return signatureLeft
     */
    Long getSignatureLeft();

    /**
     * The optional id of the employee signing the record on right side 
     * @return signatureRight
     */
    Long getSignatureRight();

    /**
     * Provides the maturity of the record. Depending on the type of the record
     * this may be an amount to pay or sending a pdf (for an order confirmation), 
     * etc. 
     * @return date of maturity
     */
    Date getMaturity();

    /**
     * The currently payed amount if record is invoice
     * @return paid amount
     */
    Double getPaidAmount();

    /**
     * Sets the payed amount if record is invoice
     * @param paid amount
     */
    void setPaidAmount(Double paidAmount);

    /**
     * Provides the due amount (gross - paid)
     * @return due amount
     */
    Double getDueAmount();

    /**
     * Indicates if record is a downpayment
     * @return true if downpayment record
     */
    boolean isDownpayment();

    /**
     * Provides the tax rate for prorate calculation 
     * @return prorate tax rate in percent
     */
    Double getProrateTax();

    /**
     * Provides the summary amount of prorated vat by base and reduced rate 
     * @return summary of prorate tax and reducedTax
     */
    Double getProrateTaxTotal();

    Double getProrateNetAmount();

    Double getProrateTaxAmount();

    Double getProrateReducedTaxAmount();

    Double getProrateGrossAmount();

    boolean isSupportingOverdue();

    boolean isOverdue();

    boolean isPaid();
    
    boolean isPaymentRecord();
    
    boolean isPurchaseRecord();

    Date getDelivery();

    boolean isDeliveryConfirmed();
    
    Long getPrintId();

    Date getDateSent();

    void addItem(Map<String, Object> map);

    List<RecordDisplayItem> getItems();

    boolean matching(List<ProductSelectionConfigItem> selection);
}
