/**
 *
 * Copyright (C) 2015 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 2, 2015 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.dao.Tables;

import com.osserp.core.dao.records.SalesPrintOptionDefaults;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordPrintOption;
import com.osserp.core.model.records.RecordPrintOptionDefaultImpl;


/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesPrintOptionDefaultsDao extends RecordPrintOptionDefaultsDao 
    implements SalesPrintOptionDefaults {

    public SalesPrintOptionDefaultsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
    }
    
    @Override
    protected Record applyDedicatedDefaults(Record record, List<RecordPrintOption> unknownOptions) {
        for (int i = 0, j = unknownOptions.size(); i < j; i++) {
            RecordPrintOption next = unknownOptions.get(i);
            if ("printConfirmationPlaceholder".equals(next.getName())) {
                record.setPrintConfirmationPlaceholder(next.isEnabled());
                break;
            }
        }
        return record;
    }

    @Override
    protected void saveDedicatedDefaults(Record record, Map<String, RecordPrintOption> defaults) {
        RecordPrintOption option = defaults.get("printConfirmationPlaceholder");
        if (option == null) {
            // create new option
            option = new RecordPrintOptionDefaultImpl(
                    record.getType().getId(),
                    "printConfirmationPlaceholder",
                    record.isPrintConfirmationPlaceholder());
        } else {
            option.setBoolean(record.isPrintConfirmationPlaceholder());
        }
        save(option);
    }

}
