/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 14, 2008 4:11:33 PM 
 * 
 */
package com.osserp.gui;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ActionTarget;
import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionError;
import com.osserp.common.PermissionException;
import com.osserp.common.User;
import com.osserp.common.UserManager;
import com.osserp.common.gui.HelpLinkManager;
import com.osserp.common.service.ResourceLocator;
import com.osserp.common.util.StringUtil;
import com.osserp.common.web.AbstractPortalView;
import com.osserp.core.system.SystemConfig;
import com.osserp.core.system.SystemConfigManager;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class CorePortalView extends AbstractPortalView {
    private static Logger log = LoggerFactory.getLogger(CorePortalView.class.getName());

    private List<String> supportedLanguages = new ArrayList<>();
    // default logout target is login form

    protected CorePortalView() {
        super();
    }

    @Override
    public void init(HttpServletRequest request) throws PermissionException {
        super.init(request);
        if (log.isDebugEnabled()) {
            log.debug("init() invoked [appNameSet="
                    + (getApplicationName() != null)
                    + ", helpServerSet=" + (getHelpServerUrl() != null)
                    + "]");
        }
        if (isNotSet(getApplicationName()) || isNotSet(getHelpServerUrl())
                || isNotSet(supportedLanguages)) {

            String exitTarget = (String) request.getSession().getServletContext().getAttribute(SystemConfig.APP_LOGOUT_PROPERTY);
            if (exitTarget != null) {
                setLogoutTarget(exitTarget);
            }

            try {
                SystemConfigManager manager = getSystemConfigManager();
                setApplicationName(manager.getSystemProperty(SystemConfig.APP_NAME_PROPERTY));
                setHelpServerUrl(manager.getSystemProperty(SystemConfig.HELP_SERVER_PROPERTY));
                String locales = manager.getSystemProperty(SystemConfig.SUPPORTED_LOCALES);
                if (locales != null) {
                    supportedLanguages = StringUtil.getTokenList(locales, StringUtil.COMMA);
                }

                exitTarget = manager.getSystemProperty(SystemConfig.APP_LOGOUT_PROPERTY);
                if (exitTarget != null) {
                    setLogoutTarget(exitTarget);
                }
                if (log.isDebugEnabled()) {
                    log.debug("init() done [appName=" + getApplicationName()
                            + ", helpServer=" + getHelpServerUrl()
                            + ", supportedLanguages=" + supportedLanguages.size()
                            + ", logoutTarget=" + getLogoutTarget()
                            + "]");
                }
            } catch (Exception e) {
                log.warn("init() failed on attempt to initialize local properties by systemConfig [message=" + e.getMessage() + "]");
                if (log.isDebugEnabled()) {
                    log.debug("init() done [logoutTarget=" + getLogoutTarget() + "]");
                }
            }
        }
    }
    
    protected boolean isSSOEnabled() {
        return false;
    }
    
    protected boolean isPropertyEnabled(String name) {
        try {
            return isNotSet(name) ? false : 
                getSystemConfigManager().isSystemPropertyEnabled(name);
        } catch (Exception e) {
            log.warn("isPropertyEnabled() failed [name=" + name
                    + ", message=" + e.getMessage() + "]");
            return false;
        }
    }
    
    protected String getProperty(String name) {
        try {
            return isNotSet(name) ? null : 
                getSystemConfigManager().getSystemProperty(name);
        } catch (Exception e) {
            log.warn("getProperty() failed [name=" + name
                    + ", message=" + e.getMessage() + "]");
            return null;
        }
    }

    public List<String> getSupportedLanguages() {
        return supportedLanguages;
    }

    public String getStartupTarget() {
        User user = getUser();
        if (user == null) {
            return null;
        }
        ActionTarget target = user.getStartup();
        if (target != null) {
            if (log.isDebugEnabled()) {
                log.debug("getStartupTaget() found user's startupTarget [name="
                        + target.getName()
                        + ", url="
                        + target.getUrl() + "]");
            }
            if (target.getUrl() != null) {
                return target.getUrl();
            }
            return target.getName();
        }
        if (log.isDebugEnabled()) {
            log.debug("getStartupTaget() missing user's startupTarget, trying to load default...");
        }
        return getDefaultStartupTarget();
    }

    private String getDefaultStartupTarget() {
        UserManager manager = getUserManager();
        if (manager != null) {
            List<ActionTarget> targets = manager.getStartupTargets();
            for (int i = 0, j = targets.size(); i < j; i++) {
                ActionTarget nextTarget = targets.get(i);
                if (nextTarget.isDefaultTarget()) {
                    if (log.isDebugEnabled()) {
                        log.debug("getStartupTaget() found default startupTarget [name="
                                + nextTarget.getName()
                                + ", url="
                                + nextTarget.getUrl() + "]");
                    }
                    if (nextTarget.getUrl() != null) {
                        return nextTarget.getUrl();
                    }
                    return nextTarget.getName();
                }
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("getDefaultStartupTaget() not found");
        }
        return null;
    }

    @Override
    public boolean isSystemSetupRequired() {
        String setupStatus = getProperty(SystemConfig.APP_CONFIG_STATUS_PROPERTY);
        if (!isSet(setupStatus)) {
            setupStatus = SystemConfig.APP_CONFIG_STATUS_SETUP;
            getSystemConfigManager().updateSystemProperty(SystemConfig.APP_CONFIG_STATUS_PROPERTY, setupStatus);
        }
        if (setupStatus.startsWith("setup")) {
            return true;
        }
        return false;
    }

    public String getSystemSetupTargetForward() {
        String setupStatus = getSystemSetupTarget();
        if (!isSet(setupStatus)) {
            throw new PermissionError(ErrorCode.APPLICATION_CONFIG);
        }
        return setupStatus;
    }

    public String getSystemSetupTarget() {
        String setupStatus = getProperty(SystemConfig.APP_CONFIG_STATUS_PROPERTY);
        if (!isSet(setupStatus)) {
            return null;
        }
        if (setupStatus.startsWith("setup")) {
            return setupStatus;
        }
        return null;
    }
    
    public boolean isSystemSetupOnly() {
        User user = getUser();
        String startup = (isUserLoggedIn() && user.getStartup() != null 
                ? user.getStartup().getKey() : null);
        if ("setup".equals(startup) && user != null && user.isAdmin()) { 
            return true;
        }
        if (user != null && user.isSetupAdmin()) {
            startup = getSystemSetupTarget();
            if ("setupInitial".equals(startup)) {
                return true;
            }
        }
        return false;
    }

    public void reload() {
        if (getUser() != null) {
            setUser(getUserManager().findById(getUser().getId()));
        }
    }

    protected SystemConfigManager getSystemConfigManager() {
        SystemConfigManager manager = (SystemConfigManager) getService(SystemConfigManager.class.getName());
        return manager;
    }

    protected UserManager getUserManager() {
        try {
            return (UserManager) getService(UserManager.class.getName());
        } catch (Exception e) {
            if (log.isDebugEnabled()) {
                log.debug("getUserManager() userManager not available");
            }
        }
        return null;
    }

    @Override
    protected HelpLinkManager getHelpLinkManager() {
        return (HelpLinkManager) getService(HelpLinkManager.class.getName());
    }
    
    protected ResourceLocator getResourceLocatorImpl() {
        return (ResourceLocator) getService(ResourceLocator.class.getName());
    }
}
