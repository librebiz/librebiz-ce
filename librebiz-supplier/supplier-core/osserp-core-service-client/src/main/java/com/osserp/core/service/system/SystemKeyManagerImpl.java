/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 16, 2016 
 * 
 */
package com.osserp.core.service.system;

import java.util.ArrayList;
import java.util.List;

import com.osserp.core.dao.SystemConfigs;
import com.osserp.core.service.impl.AbstractService;
import com.osserp.core.system.SystemKey;
import com.osserp.core.system.SystemKeyManager;
import com.osserp.core.system.SystemKeyType;


/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SystemKeyManagerImpl extends AbstractService implements SystemKeyManager {
    
    private SystemConfigs configs;

    public SystemKeyManagerImpl(SystemConfigs configs) {
        super();
        this.configs = configs;
    }

    public SystemKeyType getType(String context, String name) {
        List<SystemKeyType> all = getTypes();
        SystemKeyType byCtx = null;
        for (int i = 0, j = all.size(); i < j; i++) {
            SystemKeyType next = all.get(i);
            if (next.getContextName().equals(context)) {
                if (next.isContextOnly()) {
                    return next;
                }
                if (byCtx == null || byCtx.getId() < next.getId()) {
                    // select latest if more than one available
                    byCtx = next;
                }
            }
        }
        return byCtx;
    }

    public List<SystemKeyType> getTypes() {
        return configs.getKeyTypes();
    }

    public List<SystemKey> getKeys() {
        return configs.getSecuredKeys();
    }

    public List<SystemKey> getKeys(String contextName) {
        List<SystemKey> result = new ArrayList<>();
        List<SystemKey> existing = configs.getSecuredKeys();
        for (int i = 0, j = existing.size(); i < j; i++) {
            SystemKey next = existing.get(i);
            if (next.getContextName().equals(contextName)
                    && !next.isEndOfLife()) {
                result.add(next);
            }
        }
        return result;
    }

}
