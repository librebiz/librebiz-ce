/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 6, 2008 11:36:31 AM 
 * 
 */
package com.osserp.core.service.records;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.transform.Source;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.jdom2.Element;

import com.osserp.common.ClientException;
import com.osserp.common.OptionsCache;
import com.osserp.common.service.ResourceLocator;
import com.osserp.common.xml.JDOMUtil;
import com.osserp.core.Options;
import com.osserp.core.dao.Letters;
import com.osserp.core.dms.CoreStylesheetService;
import com.osserp.core.dms.Letter;
import com.osserp.core.dms.LetterType;
import com.osserp.core.finance.Record;
import com.osserp.core.service.doc.XmlService;
import com.osserp.core.service.impl.AbstractOutputService;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class AbstractRecordOutputService extends AbstractOutputService {
    private static Logger log = LoggerFactory.getLogger(AbstractRecordOutputService.class.getName());

    private Letters letters;

    /**
     * Creates a new outputService by required references
     * @param resourceLocator
     * @param optionsCache
     * @param stylesheetService
     * @param xmlService
     * @param letters
     */
    protected AbstractRecordOutputService(
            ResourceLocator resourceLocator, 
            OptionsCache optionsCache, 
            CoreStylesheetService stylesheetService,
            XmlService xmlService,
            Letters letters) {
        super(resourceLocator, optionsCache, stylesheetService, xmlService);
        this.letters = letters;
    }

    protected Source getXsl(Record record) throws ClientException {
        if (log.isDebugEnabled()) {
            log.debug("getXsl() invoked [record=" + (record == null ? "null" : (record.getId())
                    + ", type=" + record.getType().getId()) + "]");
        }
        Map<String, Object> templateOptions = createInitialTemplateOptions(record);
        Source source = stylesheetService.getStylesheetSource(record, templateOptions);
        if (source != null) {
            return source;
        }
        log.warn("getXsl() no stylesheet found [record="
            + (record == null ? "null" : 
                (record.getId() + ", type=" + record.getType())) + "]");
        return null;
    }

    protected Map<String, Object> createInitialTemplateOptions(Record record) {
        Map<String, Object> templateOptions = new HashMap<>();
        Letter cover = getCoverLetter(record);
        if (cover != null && isSet(cover.getEmbeddedSpaceAfter())) {
            templateOptions.put("embeddedSpaceAfterLetter", cover.getEmbeddedSpaceAfter());
        } else {
            templateOptions.put("embeddedSpaceAfterLetter", "0cm");
        }
        if (cover != null && isSet(cover.getContentKeepTogether())) {
            templateOptions.put("contentKeepTogether", cover.getContentKeepTogether());
        } else {
            templateOptions.put("contentKeepTogether", "auto");
        }
        return templateOptions;
    }

    protected Element createFormattedInfo(Long id) {
        String value = getOptionName(Options.RECORD_INFO_CONFIGS, id);
        Element note = new Element("note");
        note.addContent(JDOMUtil.createListByText("items", "item", value));
        return note;
    }

    protected Element createInfo(Long id) {
        String value = getOptionName(Options.RECORD_INFO_CONFIGS, id);
        return isSet(value) ? new Element("item").setText(value) : new Element("item");
    }

    protected Element createNote(String note) {
        return JDOMUtil.createListByText("note", "item", note);
    }

    protected Element getPaymentTarget(String name, Long id) {
        String value = getOptionName(Options.RECORD_PAYMENT_TARGETS, id);
        return isSet(value) ? new Element(name).setText(value) : new Element(name);
    }

    protected Element getTaxFreeDefinition(String name, Long taxFreeId) {
        String value = getOptionName(Options.TAX_FREE_DEFINITIONS, taxFreeId);
        return isSet(value) ? new Element(name).setText(value) : new Element(name);
    }

    protected Letter getCoverLetter(Record record) {
        Letter result = null;
        if (letters != null && isSet(record.getType().getResourceKey())) {
            LetterType letterType = letters.findType(record.getType().getResourceKey());
            if (letterType != null && letterType.isEmbedded() && !letterType.isDisabled()) {
                List<Letter> letterList = letters.find(
                        letterType.getId(), record.getContact().getId(), record.getId());
                if (!letterList.isEmpty()) {
                    result = letterList.size() == 1 ? letterList.get(0) : null;
                    if (result == null) {
                        for (int i = 0, j = letterList.size(); i < j; i++) {
                            Letter next = letterList.get(i);
                            if (next.isEmbedded() && next.isEmbeddedAbove()) {
                                result = next;
                                break;
                            }
                        }
                    }
                }
            }
        }
        return result;
    }

    protected Letters getLetters() {
        return letters;
    }

    protected void setLetters(Letters letters) {
        this.letters = letters;
    }

}
