/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 1, 2016 
 * 
 */
package com.osserp.gui.records

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.Parameter

import com.osserp.core.finance.RecordDisplay
import com.osserp.core.finance.RecordSearch
import com.osserp.core.finance.RecordSearchRequest
import com.osserp.core.products.ProductSelection
import com.osserp.core.products.ProductSelectionConfigItem
import com.osserp.core.products.ProductSelectionManager

import com.osserp.gui.common.AbstractSearchView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class AbstractRecordSearchView extends AbstractSearchView {
    private static Logger logger = LoggerFactory.getLogger(AbstractRecordSearchView.class.getName())

    protected ProductSelection employeeProductFilter
    ProductSelection config
    List<ProductSelectionConfigItem> productSelections = []

    List<Parameter> availableColumns = []

    private Set<Long> supportedSubTypes = [] as Set

    protected AbstractRecordSearchView() {
        super()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            availableColumns = recordSearch.searchRequestColumns
            config = productSelectionManager.getSelection(domainEmployee, true)
            if (config) {
                productSelections = config.getItems()
            }
        }
    }

    protected final List executeSearch() {
        return filterMatching(recordSearch.find(searchRequest))
    }

    /**
     * This method is called by execute search after invocation of the search 
     * service on the backend end before returning the result. 
     * Override to provide more specific filters.
     * @param result
     * @return filtered result
     */
    protected List<RecordDisplay> filterMatching(List<RecordDisplay> result) {
        return result
    }

    /**
     * Provides the name of the query context to create the query view name.
     * Override with recordType.tableName or leave unchanged to query
     * for all records.
     * @return query context name (e.g. unique part of database view name)
     */
    protected String getQueryContextName() {
        "record"
    }

    protected RecordSearchRequest createSearchRequest() {
        return new RecordSearchRequest(
                queryContextName,
                getString('column'),
                getString('value'),
                getBoolean('openOnly'))
    }

    protected RecordSearchRequest createInitialSearchRequest() {
        RecordSearchRequest rq = createSearchRequest()
        rq = initSearchRequest(rq)
        rq.pattern = initialSearchPattern
        rq.openOnly = true
        return rq
    }

    protected List<RecordDisplay> filterUnsupported(List<RecordDisplay> recordList) {
        if (!supportedSubTypes.isEmpty()) {
            for (Iterator<RecordDisplay> i = recordList.iterator(); i.hasNext();) {
                RecordDisplay next = i.next()
                if (!next.subType || !supportedSubTypes.contains(next.subType)) {
                    logger.debug("filterUnsupported: removing record [id=${next.id}, subType=${next.subType}]")
                    i.remove()
                }
            }
        }
        return recordList
    }

    protected RecordSearch getRecordSearch() {
        getService(RecordSearch.class.getName())
    }

    protected ProductSelectionManager getProductSelectionManager() {
        getService(ProductSelectionManager.class.getName())
    }
}
