/**
 *
 * Copyright (C) 2003, 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 9, 2016
 * 
 */
package com.osserp.common.web.tags;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.jsp.JspException;

import com.osserp.common.Appointment;
import com.osserp.common.Calendar;
import com.osserp.common.util.CalendarUtil;
import com.osserp.common.util.DateUtil;
import com.osserp.common.util.StringUtil;

import com.osserp.groovy.web.CalendarView;
import com.osserp.groovy.web.WebUtil;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CalendarRowsTag extends AbstractOutTag {

    private static final int LOT = 25;

    private CalendarView calendarView;
    private String dayUrl;
    private String targetUrl;
    private String exitTarget;
    private String exitUrl;

    public CalendarRowsTag() {
        super();
    }

    @Override
    public String getOutput() throws JspException {
        StringBuilder buffer = new StringBuilder();
        buffer.append(createTableHeaders());
        buffer.append(createTableRows());
        return buffer.toString();
    }

    protected List<Appointment> getAppointmentList() {
        if (calendarView == null) {
            List<Appointment> result = (List<Appointment>) fetchRequestObject("calendarItems");
            return result == null ? new ArrayList<>() : result;
        }
        return calendarView.getAppointments();
    }

    protected Calendar getCalendarObject() {
        if (calendarView == null) {
            Calendar calendarObj = (Calendar) fetchRequestObject("calendarObject");
            return calendarObj == null ? CalendarUtil.createCalendar() : calendarObj ;
        }
        return calendarView.getCalendar();
    }

    protected String createTableRows() {
        List<Appointment> terms = getAppointmentList();
        Calendar cal = getCalendarObject();
        String url = (dayUrl != null) ? dayUrl :
            (calendarView != null && calendarView.getDaySelectionUrl() != null) ?
                    calendarView.getDaySelectionUrl() : null;
        if (url != null) {
            url = WebUtil.createUrl(getRequest(), url);
        }
        if (calendarView != null && calendarView.getMaxTextLength() > 0) {
            limit = calendarView.getMaxTextLength();
        } else {
            limit = LOT;
        }
        StringBuilder buffer = new StringBuilder();
        buffer.append("\t<td class='kw'>"
                + cal.getWeekOfYear(1) 
                + "</td>");
        int dayOfWeek = cal.getDayOfWeek(1);
        if(dayOfWeek > 1) {
            buffer.append("\t<td colspan='" + (dayOfWeek-1) + "'> </td>");
        }
        int daysOfMonthCount = cal.getDaysOfMonthCount();
        for (int i = 1, j = daysOfMonthCount; i <= j; i++) {
            dayOfWeek = cal.getDayOfWeek(i);
            buffer.append("<td class='day");
            if (i == cal.getDay().intValue()) {
                buffer.append(" present");
            }
            buffer.append("' id='").append(i).append("'>");
            buffer.append("<div class='title'>");
            boolean dayViewAvailable = calendarView != null && calendarView.isDayViewAvailable();
            if (dayViewAvailable && url != null) {
                if (url.indexOf("?") > -1) {
                    buffer.append("<a href='").append(url).append("&");
                } else {
                    buffer.append("<a href='").append(url).append("?");
                }
                buffer.append("d=").append(i).append("'>").append(i).append("</a>");
            } else {
                buffer.append("<span>").append(i).append("</span>");
            }
            buffer.append("</div>");
            buffer.append(createContentOfDay(terms, cal, i));
            buffer.append("</td>");
            if (dayOfWeek == 7 && (i) < daysOfMonthCount) {
                buffer.append("</tr>\n<tr>\n\t<td>" + cal.getWeekOfYear(i+1) + "</td>");
            }
        }
        return buffer.toString();
    }

    protected String createContentOfDay(List<Appointment> terms, Calendar cal, Integer currentDay) {
        StringBuilder buffer = new StringBuilder();
        for (int i = 0, j = terms.size(); i < j; i++) {
            Appointment term = terms.get(i);
            int termYear = DateUtil.getYear(term.getDate());
            int termMonth = DateUtil.getMonth(term.getDate());
            int termDay = DateUtil.getDay(term.getDate());
            if (termYear == cal.getYear().intValue() &&
                    termMonth == cal.getMonth().intValue() &&
                    termDay == currentDay.intValue()) {
                buffer.append(createAppointmentDiv(term));
            }
        }
        return buffer.toString();
    }

    protected String createAppointmentDiv(Appointment appointment) {
        // <div class="term" onclick="chVis();" onmouseover="displayTerm(&quot;13&quot;, &quot;10:00&quot;, &quot;
        // name@example.org, Frankfurt&quot;, &quot;Please call us immediately&quot;);" onmouseout="hideTerm();">
        // <a href="/ctxname/loadRequest.do?id=9999999&amp;exit=calendarStartup">name@example.org, ...</a></div>
        StringBuilder buffer = new StringBuilder();
        buffer.append("<div class=\"term\" onclick=\"chVis();\" onmouseover=\"displayTerm('");
        buffer.append(appointment.getDayDisplay()).append("', '").append(appointment.getTimeDisplay()).append("', '");
        buffer.append(fetchString(appointment.getName())).append("'");
        if (appointment.getHeadline() != null) {
            buffer.append(", '").append(fetchString(appointment.getHeadline())).append("', ");
            if (appointment.getMessage() != null) {
                buffer.append("'").append(fetchString(getFormattedString(appointment.getMessage()))).append("');");
            } else {
                buffer.append("'');");
            }
        } else {
            buffer.append(", '").append(fetchString(getFormattedString(appointment.getMessage()))).append("', '');");
        }
        buffer.append("\" onmouseout=\"hideTerm();\">");
        buffer.append("<a href=\"").append(getAppointmentUrl(appointment));
        buffer.append("\">").append(StringUtil.cut(appointment.getName(), limit, true)).append("</a></div>");
        return buffer.toString();
    }

    protected String createTableHeaders() {
        StringBuilder buffer = new StringBuilder();
        buffer.append("<tr>");
        buffer.append("<th>").append(getResourceString("weekOfYearShort")).append("</th>");
        buffer.append("<th>").append(getResourceString("monday")).append("</th>");
        buffer.append("<th>").append(getResourceString("tuesday")).append("</th>");
        buffer.append("<th>").append(getResourceString("wednesday")).append("</th>");
        buffer.append("<th>").append(getResourceString("thursday")).append("</th>");
        buffer.append("<th>").append(getResourceString("friday")).append("</th>");
        buffer.append("<th>").append(getResourceString("saturday")).append("</th>");
        buffer.append("<th>").append(getResourceString("sunday")).append("</th>");
        buffer.append("</tr>");
        return buffer.toString();
    }

    protected String fetchString(String src) {
        return encodeForAttribute((src != null) ? src.trim() : "");
    }

    protected String getAppointmentUrl(Appointment appointment) {
        StringBuffer buffer = new StringBuffer();
        if (targetUrl != null) {
            buffer.append(createUrl(targetUrl)).append(appointment.getId());
        } else if (appointment.getUrl() != null) {
            buffer.append(createUrl(appointment.getUrl()));
        }
        if (exitTarget != null) {
            buffer.append("&exit=").append(exitTarget);
        }
        return buffer.toString();
    }

    public CalendarView getCalendarView() {
        return calendarView;
    }

    public void setCalendarView(CalendarView calendarView) {
        this.calendarView = calendarView;
    }

    public String getDayUrl() {
        return dayUrl;
    }

    public void setDayUrl(String dayUrl) {
        this.dayUrl = dayUrl;
    }

    public String getTargetUrl() {
        return targetUrl;
    }

    public void setTargetUrl(String targetUrl) {
        this.targetUrl = targetUrl;
    }

    public String getExitTarget() {
        return exitTarget;
    }

    public void setExitTarget(String exitTarget) {
        this.exitTarget = exitTarget;
    }

    public String getExitUrl() {
        return exitUrl;
    }

    public void setExitUrl(String exitUrl) {
        this.exitUrl = exitUrl;
    }

}
