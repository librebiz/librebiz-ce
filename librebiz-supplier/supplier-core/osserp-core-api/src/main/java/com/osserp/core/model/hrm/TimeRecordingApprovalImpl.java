/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.model.hrm;

import java.util.Date;

import com.osserp.common.Constants;
import com.osserp.common.beans.AbstractEntity;
import com.osserp.core.employees.Employee;
import com.osserp.core.hrm.TimeRecord;
import com.osserp.core.hrm.TimeRecordCorrection;
import com.osserp.core.hrm.TimeRecordType;
import com.osserp.core.hrm.TimeRecording;
import com.osserp.core.hrm.TimeRecordingApproval;

/**
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class TimeRecordingApprovalImpl extends AbstractEntity implements TimeRecordingApproval {

    private TimeRecord record;
    private TimeRecordCorrection correction;
    private TimeRecordType type;
    private Long employeeId; // future use; currently always value of createdBy
    private Long status = TimeRecordingApproval.STATUS_NEW;
    private Date startDate;
    private Date endDate;
    private String note;

    /**
     * Default constructor
     */
    protected TimeRecordingApprovalImpl() {
        super();
    }

    /**
     * Creates an update approval
     * @param user
     * @param recording
     * @param type
     * @param start
     * @param end
     * @param note
     */
    public TimeRecordingApprovalImpl(
            Employee user,
            TimeRecording recording,
            TimeRecordType type,
            Date start,
            Date end,
            String note) {
        super(null, recording.getId(), user.getId());
        this.employeeId = user.getId();
        this.type = type;
        this.startDate = start;
        this.endDate = end;
        this.note = note;
    }

    /**
     * Creates an update approval
     * @param user
     * @param recording
     * @param record
     * @param correction
     * @param start
     * @param end
     * @param note
     */
    public TimeRecordingApprovalImpl(
            Employee user,
            TimeRecording recording,
            TimeRecord record,
            TimeRecordCorrection correction,
            Date start,
            Date end,
            String note) {
        super(null, recording.getId(), user.getId());
        this.employeeId = user.getId();
        this.record = record;
        this.correction = correction;
        this.startDate = start;
        this.endDate = end;
        this.note = note;
    }

    public TimeRecord getRecord() {
        return record;
    }

    public TimeRecordCorrection getCorrection() {
        return correction;
    }

    public TimeRecordType getType() {
        return type;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public Long getStatus() {
        return status;
    }

    protected void setStatus(Long status) {
        this.status = status;
    }

    public void updateStatus(Employee user, Long status, boolean update) {
        if (TimeRecordingApproval.STATUS_NOT_APPROVED.equals(status)) {
            if (update) {
                this.correction = null;
            } else {
                if (record != null && record.getType() != null) {
                    this.type = record.getType();
                }
                this.record = null;
            }
        }
        this.status = status;
        setChanged(new Date(System.currentTimeMillis()));
        setChangedBy(user != null ? user.getId() : Constants.SYSTEM_EMPLOYEE);
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public String getNote() {
        return note;
    }

    public boolean isApprovalRequired() {
        return status == null || TimeRecordingApproval.STATUS_NEW.equals(status);
    }

    protected void setRecord(TimeRecord record) {
        this.record = record;
    }

    protected void setCorrection(TimeRecordCorrection correction) {
        this.correction = correction;
    }

    protected void setType(TimeRecordType type) {
        this.type = type;
    }

    protected void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    protected void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    protected void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    protected void setNote(String note) {
        this.note = note;
    }
}
