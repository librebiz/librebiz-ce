/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Feb 2, 2010 
 * 
 */
package com.osserp.groovy.web.tag

import com.osserp.groovy.web.WebUtil

/**
 *
 * @author rk <rk@osserp.com>
 * @author cf <cf@osserp.com>
 * 
 */
public class AjaxLinkTag extends com.osserp.common.web.tags.AjaxLinkTag {
	String closeOnly
	def left = 0
	def top = 0
	def align = null

	@Override
	protected String createUrl() {
		String ret = url
		if (!url.startsWith(WebUtil.getViewPath(servletContext, request))) {
			ret = "${WebUtil.getViewPath(servletContext, request)}${url}"
		}
		return ret
	}

	protected String createExitUrl() {
		if (!url.startsWith(WebUtil.getViewPath(servletContext, request))) {
			return "${WebUtil.getViewPath(servletContext, request)}${url}"
		}
		return url
	}

	@Override
	public String getOnSuccess() {
		String os = super.onSuccess != null ? super.onSuccess : ''
		if (popup) {
			if (!closeOnly) {
				def exitUrl = createExitUrl()
				if (exitUrl.indexOf('?') > -1) {
					exitUrl = exitUrl.substring(0, exitUrl.indexOf('?'))
				}
				exitUrl = appendCSRFToken("${exitUrl.substring(0, exitUrl.lastIndexOf('/'))}/exit")
				os += " \$('${targetElement}_close').onclick = function() { ojsAjax.loadAndExecute('${exitUrl}'); };"
			}
			if (align) {
				os += " ojsAjax.alignPopup('${targetElement}', '${align}', event.clientX, event.clientY);"
			} else if ((Integer.valueOf(left)) > 0 || (Integer.valueOf(top)) > 0) {
				os += " ojsAjax.alignPopup('${targetElement}', 'right-down', $left, $top);"
			}
		}
		return os
	}

	protected final String getViewContextPath() {
		"${contextPath}${WebUtil.getViewServletPath(servletContext)}"
	}
}
