/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 30, 2008 9:51:58 AM 
 * 
 */
package com.osserp.core.model.hrm;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.Calendar;
import com.osserp.common.ClientException;
import com.osserp.common.Month;
import com.osserp.common.beans.AbstractMonth;
import com.osserp.common.util.CollectionUtil;
import com.osserp.common.util.DateFormatter;
import com.osserp.common.util.DateUtil;
import com.osserp.core.hrm.TimeRecord;
import com.osserp.core.hrm.TimeRecordMarker;
import com.osserp.core.hrm.TimeRecordingDay;
import com.osserp.core.hrm.TimeRecordingMonth;
import com.osserp.core.hrm.TimeRecordingPeriod;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class TimeRecordingMonthVO extends AbstractMonth implements TimeRecordingMonth {
    private static Logger log = LoggerFactory.getLogger(TimeRecordingMonthVO.class.getName());

    private TimeRecordingPeriodImpl period = null;
    private TimeRecordingYearVO recordingYear = null;

    private int workingDaysCount;

    private int minutesCarryover;
    private int minutesTotal;
    private int minutesIllness;
    private int minutesLeave;
    private int minutesLost;
    private int prevMinutesCarryover;
    private int remainingMinutesLeave;
    private int expiredMinutesLeave;

    private double leaveMarkerValue = 0;
    private double monthMarkerValue = 0;

    private List<TimeRecordMarker> markers = new ArrayList<TimeRecordMarker>();

    private List<TimeRecordingDay> days = new ArrayList<TimeRecordingDay>();

    protected TimeRecordingMonthVO() {
        super();
    }

    protected TimeRecordingMonthVO(TimeRecordingYearVO recordingyear, Month month, int previousMonthCarryoverMinutes, int previousMonthLostMinutes) {
        super(month);
        this.recordingYear = recordingyear;
        this.period = recordingyear.getPeriod();
        this.prevMinutesCarryover = previousMonthCarryoverMinutes;
        this.minutesLost = previousMonthLostMinutes;
        Month current = DateUtil.createMonth();
        if (log.isDebugEnabled() && (month.isBefore(current) || current.isSame(month))) {
            log.debug("init<> invoked [year=" + recordingYear.getYear()
                    + ", month=" + month.getMonth()
                    + ", previousMonthCarryoverMinutes=" + previousMonthCarryoverMinutes
                    + "]");
        }

        List<TimeRecord> records = period.getRecords();

        for (int i = 0, j = period.getMarkers().size(); i < j; i++) {
            TimeRecordMarker next = period.getMarkers().get(i);
            if (next.isMemberOf(getMonth(), getYear())) {
                if (next.getType().isExpireMonth()) {
                    this.monthMarkerValue += next.getMarkerValue();
                } else if (next.getType().isExpireLeave()) {
                    this.leaveMarkerValue += next.getMarkerValue();
                }
                this.markers.add(next);
            }
        }

        int markerMinutes = (int) Math.round(monthMarkerValue * 60);
        if (previousMonthCarryoverMinutes > this.period.getConfig().getCarryoverHoursMaxMinutes() + markerMinutes) {
            this.minutesCarryover = period.getConfig().getCarryoverHoursMaxMinutes() + markerMinutes;
        } else {
            this.minutesCarryover = previousMonthCarryoverMinutes;
        }

        List<Calendar> dom = getCalendarDays();
        for (int i = 0, j = dom.size(); i < j; i++) {
            Calendar next = dom.get(i);
            TimeRecordingDayVO day = new TimeRecordingDayVO(
                    recordingyear,
                    next,
                    records,
                    this.markers,
                    period.getPublicHoliday(next.getDate()));

            if (!day.isWeekend() && !day.isPublicHoliday()) {
                this.workingDaysCount = workingDaysCount + 1;
            }

            this.minutesLeave = minutesLeave + day.getMinutesLeave();
            this.minutesIllness = minutesIllness + day.getMinutesIllness();
            this.minutesTotal = minutesTotal + day.getMinutesNet();

            this.minutesLost = minutesLost + day.getMinutesLost();

            if (!day.isFuture() && !day.isToday()) {
                this.minutesCarryover = minutesCarryover + day.getMinutesDifference() - day.getMinutesSubtraction() + day.getMinutesAddition();
                day.setMinutesCarryoverInMonth(minutesCarryover);
            }
            this.days.add(day);
        }

        /**
         * if (log.isDebugEnabled() && (month.isBefore(current) || month.isSame(current))) {
         * 
         * StringBuilder buffer = new StringBuilder("<init>() done [month="); buffer .append(getMonthAndYearDisplay())
         * .append(", workingDaysCount=").append(workingDaysCount) .append(", minutesTotal=").append(minutesTotal)
         * .append(", minutesLeave=").append(minutesLeave) .append(", minutesCarryover=").append(minutesCarryover)
         * .append(", minutesIllness=").append(minutesIllness) .append(", minutesLeaveSubtraction=").append(minutesLeaveSubtraction) .append("]");
         * log.debug(buffer.toString()); }
         **/
    }

    public Date getFirstDayDate() {
        StringBuilder mb = new StringBuilder();
        Integer month = getMonth();
        if (month != null) {
            month = month + 1;
            mb.append("01.");
            mb.append((month < 10 ? ("0" + month) : month.toString()));
            Integer year = getYear();
            if (year != null) {
                mb.append(".").append(year);
                try {
                    return DateUtil.createDate(mb.toString(), true);
                } catch (ClientException e) {
                    return null;
                }
            }
        }
        return null;
    }

    public List<TimeRecordMarker> getMarkers() {
        return markers;
    }

    public double getMonthMarkerValue() {
        return period.getConfig().getCarryoverHoursMax() + monthMarkerValue;
    }

    public double getLeaveMarkerValue() {
        return leaveMarkerValue;
    }

    public boolean isLeaveMarker() {
        if (leaveMarkerValue == 0) {
            return false;
        }
        return true;
    }

    public boolean isMonthMarker() {
        if (monthMarkerValue == 0) {
            return false;
        }
        return true;
    }

    public int getPrevMinutesCarryover() {
        return prevMinutesCarryover;
    }

    public int getMinutesLost() {
        return minutesLost;
    }

    public String getLeaveDisplay() {
        return DateFormatter.getHoursAndMinutes(minutesLeave);
    }

    public String getTotalDisplay() {
        return DateFormatter.getHoursAndMinutes(minutesTotal);
    }

    public String getIllnessDisplay() {
        return DateFormatter.getHoursAndMinutes(minutesIllness);
    }

    public String getCarryoverDisplay() {
        return DateFormatter.getHoursAndMinutes(minutesCarryover);
    }

    public String getWorkingDaysCountDisplay() {
        return DateFormatter.getHoursAndMinutes(workingDaysCount * period.getConfig().getDailyMinutes());
    }

    public int getWorkingDaysCount() {
        return workingDaysCount;
    }

    public int getMinutesLeave() {
        return minutesLeave;
    }

    public int getMinutesIllness() {
        return minutesIllness;
    }

    public int getMinutesCarryover() {
        return minutesCarryover;
    }

    public int getMinutesLeaveMarker() {
        return (int) Math.round(leaveMarkerValue * this.period.getConfig().getDailyMinutes());
    }

    public List<TimeRecordingDay> getDays() {
        return days;
    }

    public List<TimeRecordingDay> getReverseDays() {
        return CollectionUtil.reverse(days);
    }

    public boolean isCurrentMonth() {
        return isSame(DateUtil.createMonth());
    }

    protected TimeRecordingPeriod getPeriod() {
        return period;
    }

    protected TimeRecordingYearVO getRecordingYear() {
        return recordingYear;
    }

    protected void setWorkingDaysCount(int workingDaysCount) {
        this.workingDaysCount = workingDaysCount;
    }

    protected void setMinutesTotal(int minutesTotal) {
        this.minutesTotal = minutesTotal;
    }

    protected void setMinutesLeave(int minutesLeave) {
        this.minutesLeave = minutesLeave;
    }

    protected void setDays(List<TimeRecordingDay> days) {
        this.days = days;
    }

    public TimeRecordingDay fetchDate(Date date) {
        for (int i = 0, j = getDays().size(); i < j; i++) {
            TimeRecordingDay next = getDays().get(i);
            if (DateUtil.isSameDay(next.getDate(), date)) {
                return next;
            }
        }
        return null;
    }

    public int getRemainingMinutesLeave() {
        return remainingMinutesLeave;
    }

    public void setRemainingMinutesLeave(int remainingMinutesLeave) {
        this.remainingMinutesLeave = remainingMinutesLeave;
    }

    public double getRemainingLeave() {
        return remainingMinutesLeave / this.period.getConfig().getHoursDaily() / 60;
    }

    public int getExpiredMinutesLeave() {
        return expiredMinutesLeave;
    }

    public void setExpiredMinutesLeave(int expiredMinutesLeave) {
        this.expiredMinutesLeave = expiredMinutesLeave;
    }

    public double getExpiredLeave() {
        return expiredMinutesLeave / this.period.getConfig().getHoursDaily() / 60;
    }

    /**
     * Not necessary yet. When needed some changes are required in this methods
     */

    /**
     * public Element getXml() { Element root = new Element("recordingMonth"); root.addContent(new
     * Element("month").setText(DateFormatter.getFormatted(getMonth() + 1))); root.addContent(new
     * Element("year").setText(DateFormatter.getFormatted(getYear()))); root.addContent(new Element("workingDays").setText(new
     * Integer(workingDaysCount).toString())); root.addContent(new Element("workingHours").setText(getWorkingHoursDisplay())); root.addContent(new
     * Element("carryoverHours").setText(DateFormatter.getHoursAndMinutes(minutesCarryover))); root.addContent(new
     * Element("carryoverHoursLostDaily").setText(DateFormatter.getHoursAndMinutes(minutesCarryoverLostDaily))); root.addContent(new
     * Element("carryoverHoursLostMonthly").setText(DateFormatter.getHoursAndMinutes(minutesCarryoverLostMonthly))); List<TimeRecordingDay> list =
     * getDays(); int size = list.size(); TimeRecordingDay first = (size == 0 ? null : list.get(0)); TimeRecordingDay last = (size == 0 ? null :
     * list.get(size-1)); if (first != null && last != null) { root.addContent(new Element("startDay").setText(DateFormatter.getDate(first.getDate())));
     * root.addContent(new Element("endDay").setText(DateFormatter.getDate(last.getDate()))); root.addContent(createDays(list)); } return root; }
     * 
     * private Element createDays(List<TimeRecordingDay> list) { Element root = new Element("days"); int completedSummary = 0; int periodSummary =
     * this.previousMonthCarryoverMinutes; for (int i = 0, j = list.size(); i < j; i++) { TimeRecordingDay next = list.get(i); completedSummary =
     * completedSummary + next.getMinutesNet(); periodSummary = periodSummary + next.getMinutesCarryover() + next.getMinutesMissing();
     * root.addContent(next.getXml(completedSummary, periodSummary)); } return root; }
     **/

}
