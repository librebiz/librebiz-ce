/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 21, 2006 9:34:42 AM 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.Option;
import com.osserp.common.OptionsCache;
import com.osserp.common.beans.OptionImpl;
import com.osserp.common.dms.dao.DocumentTypes;
import com.osserp.common.dms.dao.Documents;
import com.osserp.common.service.Locator;
import com.osserp.core.Options;
import com.osserp.core.dao.BranchOffices;
import com.osserp.core.dao.BusinessCaseUtilities;
import com.osserp.core.dao.Campaigns;
import com.osserp.core.dao.Contacts;
import com.osserp.core.dao.Countries;
import com.osserp.core.dao.Districts;
import com.osserp.core.dao.Employees;
import com.osserp.core.dao.EventConfigs;
import com.osserp.core.dao.FlowControlActions;
import com.osserp.core.dao.Infos;
import com.osserp.core.dao.Legalforms;
import com.osserp.core.dao.Months;
import com.osserp.core.dao.PaymentConditions;
import com.osserp.core.dao.ProductCategories;
import com.osserp.core.dao.ProductGroups;
import com.osserp.core.dao.ProductTypes;
import com.osserp.core.dao.Products;
import com.osserp.core.dao.ProjectTrackings;
import com.osserp.core.dao.QuantityUnits;
import com.osserp.core.dao.RequestFcsActions;
import com.osserp.core.dao.Requests;
import com.osserp.core.dao.SalesContracts;
import com.osserp.core.dao.Salutations;
import com.osserp.core.dao.SelectOptions;
import com.osserp.core.dao.Suppliers;
import com.osserp.core.dao.SystemCompanies;
import com.osserp.core.dao.Users;
import com.osserp.core.dao.hrm.TimeRecordingConfigs;
import com.osserp.core.dao.records.DeliveryNoteTypes;
import com.osserp.core.dao.records.PurchaseInvoices;
import com.osserp.core.dao.records.PurchaseOrders;
import com.osserp.core.dao.records.RecordTypes;
import com.osserp.core.dao.records.SalesCreditNotes;
import com.osserp.core.dao.records.SalesOrders;
import com.osserp.core.finance.BookingType;
import com.osserp.core.finance.Stock;
import com.osserp.core.system.BranchOffice;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class OptionsCacheImpl implements OptionsCache {
    private static Logger log = LoggerFactory.getLogger(OptionsCacheImpl.class.getName());

    private Locator locator = null;
    private Map listMap = null;
    private Map mapsMap = null;
    private Set availableNames = null;
    private SelectOptions options = null;

    @SuppressWarnings("unchecked")
    public OptionsCacheImpl() {
        listMap = Collections.synchronizedMap(new HashMap<>());
        mapsMap = Collections.synchronizedMap(new HashMap<>());
        availableNames = getAvailableNames();
    }

    @SuppressWarnings("unchecked")
    public List getList(String optionKey) {
        if (!listMap.containsKey(optionKey)) {
            createOptions(optionKey);
        }
        return (List) listMap.get(optionKey);
    }

    public void refresh(String optionKey) {
        if (log.isDebugEnabled()) {
            log.debug("refresh() invoked [key=" + optionKey + "]");
        }
        createOptions(optionKey);
    }

    @SuppressWarnings("unchecked")
    public void refreshAll() {
        List<String> currentCached = new ArrayList(listMap.keySet());
        for (int i = 0, j = currentCached.size(); i < j; i++) {
            refresh(currentCached.get(i));
        }
    }

    @SuppressWarnings("unchecked")
    public Map getMap(String optionKey) {
        if (!mapsMap.containsKey(optionKey)) {
            createOptions(optionKey);
        }
        return (Map) mapsMap.get(optionKey);
    }

    public Option getMapped(String optionKey, Long id) {
        Map opts = getMap(optionKey);
        if (opts == null) {
            log.warn("getMapped() options not found [name=" + optionKey + "]");
        }
        return ((opts == null || id == null) ? null : (Option) opts.get(id));
    }

    public String getMappedValue(String optionKey, Long id) {
        Option opt = getMapped(optionKey, id);
        return (opt == null ? "" : opt.getName());
    }

    public Option add(String optionKey, String value) throws ClientException {
        Option result = null;
        options.add(optionKey, value);
        createOptions(optionKey);
        List<Option> opts = getList(optionKey);
        for (int i = 0, j = opts.size(); i < j; i++) {
            Option opt = opts.get(i);
            if (opt.getName().equals(value)) {
                result = opt;
            }
        }
        return result;
    }

    public void rename(String optionKey, Long id, String newValue) throws ClientException {
        options.rename(optionKey, id, newValue);
        createOptions(optionKey);
    }

    public void toogleEol(String optionKey, Long id) {
        options.toggleEol(optionKey, id);
    }

    @SuppressWarnings("unchecked")
    private void createOptions(String option) {
        try {
            List list = null;
            if (availableNames.contains(option)) {
                list = createNames(option);
            } else {
                list = options.getList(option);
            }
            if (list != null) {
                listMap.put(option, list);
                addMap(option, list);
            }
        } catch (Throwable t) {
            log.error("createOptions() failed [option=" + option
                    + ", message=" + t.toString() + "]");
        }
    }

    @SuppressWarnings("unchecked")
    protected List createNames(String option) {
        List list = new ArrayList<>();
        if (Options.ACTIVATED_EMPLOYEES.equals(option)) {
            list = ((Employees) getService(Employees.class.getName())).findActiveNames();
        } else if (Options.PRODUCT_CLASSES.equals(option)) {
            list = ((ProductGroups) getService(ProductGroups.class.getName())).getClassifications();
        } else if (Options.PRODUCT_CATEGORIES.equals(option)) {
            list = ((ProductCategories) getService(ProductCategories.class.getName())).getList();
        } else if (Options.PRODUCT_GROUPS.equals(option)) {
            list = ((ProductGroups) getService(ProductGroups.class.getName())).getList();
        } else if (Options.PRODUCT_TYPES.equals(option)) {
            list = ((ProductTypes) getService(ProductTypes.class.getName())).getList();
        } else if (Options.PRODUCT_MANUFACTURERS.equals(option)) {
            list = ((Products) getService(Products.class.getName())).getManufacturers();
        } else if (Options.BANK_ACCOUNTS.equals(option)) {
            list = ((SystemCompanies) getService(SystemCompanies.class.getName())).getBankAccounts();
        } else if (Options.BILLING_TYPES.equals(option)) {
            list = ((RecordTypes) getService(RecordTypes.class.getName())).getBillingTypes();
        } else if (Options.BRANCH_OFFICES.equals(option)) {
            list = ((BranchOffices) getService(BranchOffices.class.getName())).getBranchOffices();
        } else if (Options.BRANCH_OFFICE_KEYS.equals(option)) {
            List<BranchOffice> offices = ((BranchOffices) getService(BranchOffices.class.getName())).getBranchOffices();
            List<Option> keys = new ArrayList<Option>();
            for (int i = 0, j = offices.size(); i < j; i++) {
                BranchOffice next = offices.get(i);
                keys.add(new OptionImpl(next.getId(), next.getShortkey()));
            }
            list = keys;
        } else if (Options.BUSINESS_STATUS.equals(option)) {
            list = ((BusinessCaseUtilities) getService(BusinessCaseUtilities.class.getName())).getBusinessStatusList();
        } else if (Options.CAMPAIGN_GROUPS.equals(option)) {
            list = ((Campaigns) getService(Campaigns.class.getName())).getGroups();
        } else if (Options.CAMPAIGN_TYPES.equals(option)) {
            list = ((Campaigns) getService(Campaigns.class.getName())).getTypes();
        } else if (Options.CAMPAIGNS.equals(option)) {
            list = ((Campaigns) getService(Campaigns.class.getName())).findAllCampaigns();
        } else if (Options.CONTACT_GROUPS.equals(option)) {
            list = ((Contacts) getService(Contacts.class.getName())).getContactGroups();
        } else if (Options.CONTACT_TYPES.equals(option)) {
            list = ((Contacts) getService(Contacts.class.getName())).getContactTypes();
        } else if (Options.COUNTRY_NAMES.equals(option)) {
            list = ((Countries) getService(Countries.class.getName())).getList();
        } else if (Options.COUNTRY_CODES.equals(option)) {
            list = ((Countries) getService(Countries.class.getName())).getCodeList();
        } else if (Options.COUNTRY_PREFIXES.equals(option)) {
            list = ((Countries) getService(Countries.class.getName())).getPrefixList();
        } else if (Options.CURRENCIES.equals(option)) {
            list = ((Infos) getService(Infos.class.getName())).getList(Options.CURRENCIES);
        } else if (Options.DELIVERY_NOTE_TYPES.equals(option)) {
            list = ((DeliveryNoteTypes) getService(DeliveryNoteTypes.class.getName())).getList();
        } else if (Options.DISTRICTS.equals(option)) {
            list = ((Districts) getService(Districts.class.getName())).getList();
        } else if (Options.DOCUMENT_CATEGORIES.equals(option)) {
            list = ((Documents) getService(Documents.class.getName())).getCategories();
        } else if (Options.DOCUMENT_TYPES.equals(option)) {
            list = ((DocumentTypes) getService(DocumentTypes.class.getName())).getList();
        } else if (Options.EMPLOYEES.equals(option)) {
            list = ((Employees) getService(Employees.class.getName())).findNames();
        } else if (Options.EMPLOYEE_GROUPS.equals(option)) {
            list = ((Employees) getService(Employees.class.getName())).getEmployeeGroups();
        } else if (Options.EMPLOYEE_GROUP_KEYS.equals(option)) {
            list = ((Employees) getService(Employees.class.getName())).getEmployeeGroupKeys();
        } else if (Options.EMPLOYEE_KEYS.equals(option)) {
            list = ((Employees) getService(Employees.class.getName())).findShortKeys();
        } else if (Options.EMPLOYEE_STATUS.equals(option)) {
            list = ((Employees) getService(Employees.class.getName())).getEmployeeStatus();
        } else if (Options.EMPLOYEE_TYPES.equals(option)) {
            list = ((Employees) getService(Employees.class.getName())).getEmployeeTypes();
        } else if (Options.EVENT_ACTIONS.equals(option)) {
            list = ((EventConfigs) getService(EventConfigs.class.getName())).loadActions();
        } else if (Options.FEDERAL_STATES.equals(option)) {
            list = ((SelectOptions) getService(SelectOptions.class.getName())).getList(Options.FEDERAL_STATES);
        } else if (Options.FLOW_CONTROL_ACTIONS.equals(option)) {
            list = ((FlowControlActions) getService(FlowControlActions.class.getName())).getList();
        } else if (Options.LEGALFORMS.equals(option)) {
            list = ((Legalforms) getService(Legalforms.class.getName())).getList();
        } else if (Options.MONTHS.equals(option)) {
            list = ((Months) getService(Months.class.getName())).getList();
        } else if (Options.PAYMENT_TYPES.equals(option)) {
            list = ((RecordTypes) getService(RecordTypes.class.getName())).getPaymentTypes();
        } else if (Options.QUANTITY_UNITS.equals(option)) {
            list = ((QuantityUnits) getService(QuantityUnits.class.getName())).getList();
        } else if (Options.PROJECT_TRACKING_RECORD_TYPES.equals(option)) {
            list = ((ProjectTrackings) getService(ProjectTrackings.class.getName())).getRecordTypes();
        } else if (Options.PROJECT_TRACKING_TYPES.equals(option)) {
            list = ((ProjectTrackings) getService(ProjectTrackings.class.getName())).getTrackingTypes();
        } else if (Options.PURCHASE_INVOICE_SHORTTYPES.equals(option)) {
            List<BookingType> subTypes = ((PurchaseInvoices) getService(PurchaseInvoices.class.getName())).getBookingTypes();
            for (int i = 0, j = subTypes.size(); i < j; i++) {
                BookingType next = subTypes.get(i);
                list.add(new OptionImpl(next.getId(), next.getKey()));
            }
        } else if (Options.PURCHASE_INVOICE_TYPES.equals(option)) {
            list = ((PurchaseInvoices) getService(PurchaseInvoices.class.getName())).getBookingTypes();
        } else if (Options.PURCHASE_ORDER_TYPES.equals(option)) {
            list = ((PurchaseOrders) getService(PurchaseOrders.class.getName())).getBookingTypes();
        } else if (Options.RECORD_PAYMENT_CONDITIONS.equals(option)) {
            list = ((PaymentConditions) getService(PaymentConditions.class.getName())).getPaymentConditions();
        } else if (Options.RECORD_TYPES.equals(option)) {
            list = ((RecordTypes) getService(RecordTypes.class.getName())).getRecordTypes();
        } else if (Options.REQUEST_FCS_ACTIONS.equals(option)) {
            list = ((RequestFcsActions) getService(RequestFcsActions.class.getName())).getList();
        } else if (Options.REQUEST_STATUS.equals(option)) {
            list = ((RequestFcsActions) getService(RequestFcsActions.class.getName())).getStatusList();
        } else if (Options.REQUEST_TYPES.equals(option)) {
            list = ((Requests) getService(Requests.class.getName())).getRequestTypes();
        } else if (Options.REQUEST_TYPE_KEYS.equals(option)) {
            list = ((Requests) getService(Requests.class.getName())).getRequestTypeKeys();
        } else if (Options.SALES_CONTRACT_STATUS.equals(option)) {
            list = ((SalesContracts) getService(SalesContracts.class.getName())).getStatusList();
        } else if (Options.SALES_CONTRACT_TYPES.equals(option)) {
            list = ((SalesContracts) getService(SalesContracts.class.getName())).getTypesList();
        } else if (Options.SALES_CREDIT_NOTE_TYPES.equals(option)) {
            list = ((SalesCreditNotes) getService(SalesCreditNotes.class.getName())).getBookingTypes();
        } else if (Options.SALES_ORDER_TYPES.equals(option)) {
            list = ((SalesOrders) getService(SalesOrders.class.getName())).getBookingTypes();
        } else if (Options.SALUTATIONS.equals(option)) {
            list = ((Salutations) getService(Salutations.class.getName())).getSalutations();
        } else if (Options.SHIPPING_COMPANIES.equals(option)) {
            list = ((Suppliers) getService(Suppliers.class.getName())).getShippingCompanies();
        } else if (Options.SHIPPING_COMPANY_KEYS.equals(option)) {
            list = ((Suppliers) getService(Suppliers.class.getName())).getShippingCompanyKeys();
        } else if (Options.STOCK_KEYS.equals(option)) {
            List<Stock> stocks = ((SystemCompanies) getService(SystemCompanies.class.getName())).getStocks();
            for (int i = 0, j = stocks.size(); i < j; i++) {
                Stock next = stocks.get(i);
                list.add(new OptionImpl(next.getId(), next.getShortKey()));
            }
        } else if (Options.STOCK_NAMES.equals(option)) {
            list = ((SystemCompanies) getService(SystemCompanies.class.getName())).getStocks();
        } else if (Options.SUPPLIER_TYPES.equals(option)) {
            list = ((Suppliers) getService(Suppliers.class.getName())).getSupplierTypes();
        } else if (Options.SUPPLIERS.equals(option)) {
            list = ((Suppliers) getService(Suppliers.class.getName())).findNames();
        } else if (Options.SYSTEM_COMPANIES.equals(option)) {
            list = ((SystemCompanies) getService(SystemCompanies.class.getName())).getAll();
        } else if (Options.SYSTEM_COMPANY_KEYS.equals(option)) {
            list = ((SystemCompanies) getService(SystemCompanies.class.getName())).getShortnames();
        } else if (Options.TIME_RECORD_MARKER_TYPES.equals(option)) {
            list = ((TimeRecordingConfigs) getService(TimeRecordingConfigs.class.getName())).getMarkerTypes();
        } else if (Options.TIME_RECORD_STATUS.equals(option)) {
            list = ((TimeRecordingConfigs) getService(TimeRecordingConfigs.class.getName())).getStatusList();
        } else if (Options.TIME_RECORD_TYPES.equals(option)) {
            list = ((TimeRecordingConfigs) getService(TimeRecordingConfigs.class.getName())).getTypes();
        } else if (Options.TIME_RECORDING_CONFIGS.equals(option)) {
            list = ((TimeRecordingConfigs) getService(TimeRecordingConfigs.class.getName())).getConfigs();
        } else if (Options.TITLES.equals(option)) {
            list = ((Salutations) getService(Salutations.class.getName())).getTitles();
        } else if (Options.USERS.equals(option)) {
            list = ((Users) getService(Users.class.getName())).getNames();
        } else if (Options.USER_LOGINS.equals(option)) {
            list = ((Users) getService(Users.class.getName())).getLoginNames();
        }
        return list;
    }

    @SuppressWarnings("unchecked")
    private void addMap(String option, List list) {
        Map map = new HashMap<>();
        if (!list.isEmpty() && list.get(0) instanceof Option) {
            for (Iterator i = list.iterator(); i.hasNext();) {
                Option o = (Option) i.next();
                map.put(o.getId(), o);
            }
            mapsMap.put(option, map);
        }
    }

    @SuppressWarnings("unchecked")
    private Set getAvailableNames() {
        Set avn = new HashSet();
        for (int i = 0, j = Options.ALL_NAMES.length; i < j; i++) {
            avn.add(Options.ALL_NAMES[i]);
        }
        return avn;
    }

    protected Object getService(String name) {
        return locator.lookupService(name);
    }

    public void setLocator(Locator locator) {
        this.locator = locator;
    }

    public void setOptions(SelectOptions options) {
        this.options = options;
    }
}
