/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 14, 2006 1:26:10 AM 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;

import com.osserp.core.Item;
import com.osserp.core.dao.PaymentConditions;
import com.osserp.core.dao.Suppliers;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.dao.SystemCompanies;
import com.osserp.core.dao.TaxRates;
import com.osserp.core.dao.records.PurchaseDeliveryNotes;
import com.osserp.core.dao.records.RecordInfoConfigs;
import com.osserp.core.dao.records.RecordMailLogs;
import com.osserp.core.dao.records.RecordPrintOptionDefaults;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.DeliveryNote;
import com.osserp.core.finance.DeliveryNoteType;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordNumberCreator;
import com.osserp.core.finance.RecordType;
import com.osserp.core.model.records.PurchaseDeliveryNoteImpl;
import com.osserp.core.purchasing.PurchaseDeliveryNote;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PurchaseDeliveryNotesDao extends AbstractPurchaseRecords implements PurchaseDeliveryNotes {
    private static Logger log = LoggerFactory.getLogger(PurchaseDeliveryNotesDao.class.getName());
    private static final String DELETE_PERMISSION_DEFAULT = "purchase_delivery_note_change";

    public PurchaseDeliveryNotesDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            OptionsCache namesCache,
            TaxRates taxRates,
            SystemCompanies companies,
            RecordInfoConfigs infoConfigs,
            PaymentConditions paymentConditions,
            RecordNumberCreator recordNumberCreator,
            RecordPrintOptionDefaults recordPrintOptionDefaults,
            RecordMailLogs recordMailLogs,
            Suppliers suppliers) {
        super(
                jdbcTemplate,
                tables,
                sessionFactory,
                namesCache,
                taxRates,
                companies,
                infoConfigs,
                paymentConditions,
                recordNumberCreator,
                recordPrintOptionDefaults,
                recordMailLogs,
                suppliers,
                TableKeys.PURCHASE_DELIVERY_NOTES,
                TableKeys.PURCHASE_DELIVERY_NOTE_ITEMS);
        initializeDeletePermissions(DELETE_PERMISSION_DEFAULT);
    }

    @Override
    protected Long getDefaultRecordTypeId() {
        return RecordType.PURCHASE_RECEIPT;
    }

    public DeliveryNote create(Employee user, Order order, DeliveryNoteType type) throws ClientException {
        List<Item> items = order.getOpenDeliveries();
        if (items.isEmpty()) {
            throw new ClientException(ErrorCode.DELIVERY_ITEMS_MISSING);
        }
        RecordType recordType = getRecordType();
        PurchaseDeliveryNoteImpl obj = new PurchaseDeliveryNoteImpl(
                getNextRecordId(user, order.getCompany(), recordType, null),
                recordType,
                type,
                order,
                user);
        saveInitial(obj);
        order.getDeliveryNotes().add(obj);
        return obj;
    }

    public Double getStockReceiptQuantity(Long productId) {
        StringBuilder query = new StringBuilder();
        query
                .append("SELECT sum(quantity) FROM ")
                .append(getRecordTableName())
                .append(" WHERE product_id = ? AND reference_id IN (SELECT id FROM ")
                .append(getRecordItemTableName())
                .append(" WHERE status = ")
                .append(Record.STAT_SENT)
                .append(")");
        List<Double> result = (List<Double>) jdbcTemplate.query(
                query.toString(),
                new Object[] { productId },
                new int[] { Types.BIGINT },
                getDoubleRowMapper());
        if (result.isEmpty()) {
            return 0d;
        }
        return result.get(0);
    }

    public List<DeliveryNote> getByOrder(Order order) {
        List<Record> records = new ArrayList(getByReference(order.getId()));
        List<DeliveryNote> result = new ArrayList<DeliveryNote>();
        for (int i = 0, j = records.size(); i < j; i++) {
            result.add((DeliveryNote) records.get(i));
        }
        return result;
    }

    public void reopenClosed(Employee user, PurchaseDeliveryNote note) {
        note.reopen(user);
        save(note);
    }

    // SELECT distinct reference_id FROM purchase_delivery_note_items WHERE stock_id = stockId AND reference_id IN (SELECT id FROM purchase_delivery_notes WHERE status > 2 and reference_id not in (select id from purchase_orders where id in (select reference_id from purchase_invoices))) order by reference_id;
    public List<DeliveryNote> getOpen(Long stockId, Long productId) {
        List<DeliveryNote> result = new ArrayList<DeliveryNote>();
        if (stockId != null) {
            StringBuilder sql = new StringBuilder(256);
            sql
                    .append("SELECT distinct reference_id FROM ")
                    .append(getRecordItemTableName()); // purchase_delivery_note_items

            if (productId == null) {
                sql.append(" WHERE stock_id = ").append(stockId);
            } else {
                sql
                        .append(" WHERE product_id = ")
                        .append(productId)
                        .append(" AND stock_id = ")
                        .append(stockId);
            }
            sql
                    .append(" AND reference_id IN (SELECT id FROM ")
                    .append(getRecordTableName()) // purchase_delivery_notes 
                    .append(" WHERE status > ")
                    .append(DeliveryNote.STAT_PRINT)
                    .append(" AND reference_id NOT IN (SELECT id FROM ")
                    .append(getTable(TableKeys.PURCHASE_ORDERS)) // purchase_orders 
                    .append(" WHERE id IN (SELECT reference_id FROM ")
                    .append(getTable(TableKeys.PURCHASE_INVOICES)) // purchase_invoices
                    .append("))) ORDER BY reference_id");
            String query = sql.toString();
            if (log.isDebugEnabled()) {
                log.debug("findOpen() invoked [sql=" + query + "]");
            }
            List<Long> ids = (List<Long>) jdbcTemplate.query(query, getLongRowMapper());
            for (int i = 0, j = ids.size(); i < j; i++) {
                Long next = ids.get(i);
                DeliveryNote note = (DeliveryNote) load(next);
                result.add(note);
            }
        }
        return result;
    }

    @Override
    protected Class<PurchaseDeliveryNoteImpl> getPersistentClass() {
        return PurchaseDeliveryNoteImpl.class;
    }

}
