/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 6, 2005 
 * 
 */
package com.osserp.core.employees;

import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.Option;

import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.OfficePhone;
import com.osserp.core.contacts.PersonManager;
import com.osserp.core.system.BranchOffice;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface EmployeeManager extends PersonManager {

    /**
     * Creates and assigns an employee object to provided contact.
     * @param user the user creating the new employee
     * @param contact
     * @param type
     * @param branch
     * @param group
     * @param status
     * @param beginDate
     * @param initials
     * @param role
     * @return primary key of new created or existing employee
     * @throws ClientException
     */
    public Long create(
            DomainUser user,
            Contact contact,
            EmployeeType type,
            BranchOffice branch,
            EmployeeGroup group,
            EmployeeStatus status,
            Date beginDate,
            String initials,
            String role) throws ClientException;

    /**
     * Provides all available office phones
     * @param branchId or null to get all phones
     * @return officePhones
     */
    List<OfficePhone> getOfficePhones(Long branchId);

    /**
     * Adds the phone with the given id to the given employee
     * @param userId
     * @param employee
     * @param phone
     */
    void addOfficePhone(Long userId, Employee employee, OfficePhone phone);

    /**
     * Adds a new phone to list and employee. The phone number is created by concatenation of number and direct dial
     * @param employee where to add the number
     * @param userId who creates and adds the number
     * @param country
     * @param prefix
     * @param number like 999 88-
     * @param directDial something like 10
     * @param branchId
     * @throws ClientException if any required value missing or number already existing
     */
    void addOfficePhone(
            Employee employee,
            Long userId,
            String country,
            String prefix,
            String number,
            String directDial,
            Long branchId) throws ClientException;

    /**
     * Removes the office phone
     * @param userId
     * @param employee
     */
    void removeOfficePhone(Long userId, Employee employee);

    /**
     * Adds a new role config
     * @param user
     * @param employee
     * @param branch
     * @param description
     * @return employee with new role config added
     */
    Employee addRoleConfig(Employee user, Employee employee, BranchOffice branch, String description);

    /**
     * Adds a new role to a config
     * @param user
     * @param employee
     * @param config
     * @param group
     * @param status
     * @return employee with new role added
     */
    Employee addRole(
            DomainUser user,
            Employee employee,
            EmployeeRoleConfig config,
            EmployeeGroup group,
            EmployeeStatus status);

    /**
     * Removes a role from a config
     * @param user
     * @param employee
     * @param config
     * @param roleId
     * @return employee without removed role
     */
    Employee removeRole(
            DomainUser user,
            Employee employee,
            EmployeeRoleConfig config,
            Long roleId);

    /**
     * Removes a complete role config
     * @param user
     * @param employee
     * @param configId
     * @return employee without removed role config
     */
    Employee removeRoleConfig(
            DomainUser user,
            Employee employee,
            Long configId);

    /**
     * Updates details of employee
     * @param user
     * @param employee
     * @param branch (optional)
     * @param employeeType
     * @param beginDate
     * @param endDate
     * @param initials
     * @param role
     * @param costCenter
     * @param signatureSource
     * @param businessCard
     * @param internationalBusinessCard
     * @param cellularPhone
     * @param emailAccount
     * @param timeRecordingEnabled
     * @return updated employee
     * @throws ClientException
     * @throws PermissionException
     */
    Employee updateDetails(
            Employee user,
            Employee employee,
            BranchOffice branch,
            Long employeeType,
            Date beginDate,
            Date endDate,
            String initials,
            String role,
            String costCenter,
            String signatureSource,
            boolean businessCard,
            boolean internationalBusinessCard,
            boolean cellularPhone,
            boolean emailAccount,
            boolean timeRecordingEnabled) throws ClientException;

    /**
     * Persists employee
     * @param employee
     */
    void save(Employee employee);

    /**
     * Provides a type by id
     * @param id
     * @return type or null if not exists
     */
    EmployeeType getType(Long id);

    /**
     * Provides all active employees ordered by lastname
     * @return list of employees
     */
    List<Option> findAllActiveEmployees();

    /**
     * Provides all active employees
     * @return list of employees
     */
    List<Employee> findActiveEmployees();

    /**
     * Provides all employee disciplinarians
     * @param employee
     * @return list of disciplinarians
     */
    List<EmployeeDisciplinarian> findDisciplinarians(Employee employee);

    /**
     * Adds a disciplinarian
     * @param user
     * @param employee
     * @param disciplinarian
     * @throws ClientException if validation fails (e.g. user has no permissions, etc.)
     */
    void addDisciplinarian(Employee user, Employee employee, Employee disciplinarian) throws ClientException;

    /**
     * Adds a disciplinarian
     * @param user
     * @param disciplinarian
     * @throws ClientException if validation fails (e.g. user has no permissions, etc.)
     */
    void removeDisciplinarian(Employee user, EmployeeDisciplinarian disciplinarian) throws ClientException;

    /**
     * Checks if employee the disciplinarian from other employee
     * @param employee
     * @param disciplinarian
     * @return true if employee the disciplinarian from other employee
     */
    boolean isDisciplinarian(Employee employee, Employee disciplinarian);

    /**
     * Provides all available employee status
     * @return status
     */
    List<EmployeeStatus> getStatus();

    /**
     * Synchronizes employees with ldap users
     */
    void synchronizeLdapUsers();

    /**
     * Synchronizes employee with ldap user
     */
    void synchronizeLdapUser(Employee employee);
}
