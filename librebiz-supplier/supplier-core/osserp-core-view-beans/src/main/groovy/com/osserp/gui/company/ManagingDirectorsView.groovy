/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.osserp.gui.company


import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ErrorCode
import com.osserp.core.system.SystemCompany
import com.osserp.core.system.SystemCompanyManager
import com.osserp.groovy.web.ViewContextException
import com.osserp.gui.CoreView

/**
 * 
 * @author rk <rk@osserp.com>
 * @author cf <cf@osserp.com>
 *
 */
public class ManagingDirectorsView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(ManagingDirectorsView.class.getName())

    ManagingDirectorsView() {
        dependencies = ['clientCompanyView']
        headerName = 'management'
        enablePopupView()
        providesEditMode()
        providesCreateMode()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (!env.clientCompanyView?.bean) {
            logger.error('initRequest: client not bound')
            throw new ViewContextException(ErrorCode.INVALID_CONTEXT, this)
        }
        if (!editMode && !createMode && !bean) {
            def client = systemCompanyManager.find(env.clientCompanyView.bean.id)
            list = client?.managingDirectors ?: []
        }
    }

    @Override
    void save() {
        SystemCompany client = systemCompanyManager.find(env.clientCompanyView.bean.id)
        String name = form.getString('name')
        if (createMode) {
            systemCompanyManager.addManagingDirector(domainEmployee, client, name)
            disableCreateMode()
        } else if (editMode) {
            systemCompanyManager.renameManagingDirector(client, bean.id, name)
            bean.name = name
            disableEditMode()
        }
        list = client?.managingDirectors ?: []
        env.clientCompanyView.reload()
    }

    void delete() {
        SystemCompany client = systemCompanyManager.find(env.clientCompanyView.bean.id)
        def id = form.getLong('id')
        systemCompanyManager.removeManagingDirector(client, id)
        list = client?.managingDirectors ?: []
        env.clientCompanyView.reload()
    }

    protected SystemCompanyManager getSystemCompanyManager() {
        getService(SystemCompanyManager.class.getName())
    }
}
