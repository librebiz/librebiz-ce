/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 08-Jul-2006 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.Types;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;

import com.osserp.core.clients.Client;
import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.contacts.Contact;
import com.osserp.core.dao.Clients;
import com.osserp.core.dao.Contacts;
import com.osserp.core.dao.Emails;
import com.osserp.core.dao.TableKeys;
import com.osserp.core.model.contacts.ContactImpl;
import com.osserp.core.model.contacts.groups.ClientImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ClientsDao extends ClassifiedContactsDao implements Clients {
    private static final String LIST =
            "system_companies.id,system_companies.contact_id," +
                    "contacts.salutation,contacts.firstname,contacts.lastname," +
                    "contacts.street,contacts.zipcode,contacts.city";

    private String mainTable = null;

    /**
     * Creates clientsDao
     * @param jdbcTemplate
     * @param tables
     * @param sessionFactory
     * @param contacts
     * @param emails
     * @param namesCache
     */
    public ClientsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            Contacts contacts,
            Emails emails,
            OptionsCache namesCache) {
        super(jdbcTemplate, tables, sessionFactory, contacts, emails, namesCache);
        this.mainTable = getTable(TableKeys.SYSTEM_COMPANIES);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.impl.AbstractContactAwareDao#getEntityClass()
     */
    @Override
    protected Class<ClientImpl> getEntityClass() {
        return ClientImpl.class;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.impl.AbstractContactAwareDao#getEntityTable()
     */
    @Override
    protected String getEntityTable() {
        return mainTable;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.ContactRelations#find(java.lang.Long)
     */
    public ClassifiedContact find(Long id) {
        try {
            return load(id);
        } catch (Throwable t) {
            return null;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.dao.ContactRelations#create(java.lang.Long, com.osserp.core.contacts.Contact, java.lang.Long)
     */
    public ClassifiedContact create(Long createdBy, Contact contact, Long customId) {
        ClientImpl obj = new ClientImpl(createdBy, contact);
        if (obj.getRc() instanceof ContactImpl) {
            ContactImpl ci = (ContactImpl) obj.getRc();
            ci.setClient(true);
            this.contacts.save(ci);
        }
        save(obj);
        return obj;
    }

    /**
     * @param id
     * @return
     */
    public ClassifiedContact load(Long id) {
        ClientImpl obj = (ClientImpl) getCurrentSession().load(ClientImpl.class, id);
        return obj;
    }

    public List<Client> loadAll() {
        List<Client> all = getCurrentSession().createQuery(("from " + ClientImpl.class.getName())).list();
        return all;
    }

    /**
     * @param id
     * @return
     */
    public String findName(Long id) {
        if (id == null) {
            return "";
        }
        StringBuffer query = new StringBuffer(64);
        query
                .append(" SELECT c.lastname FROM ")
                .append(mainTable)
                .append(" i, ")
                .append(contactsTable)
                .append(" c WHERE i.contact_id = c.contact_id AND i.id = ?");
        final Object[] params = { id };
        final int[] types = { Types.BIGINT };
        return jdbcTemplate.queryForObject(query.toString(), params, types, String.class);
    }

    /**
     * Provides the basic statement required for queries
     * @return query
     */
    @Override
    protected StringBuilder getQuery() {
        StringBuilder query = new StringBuilder(64);
        query
                .append("SELECT ")
                .append(LIST)
                .append(" FROM ")
                .append(mainTable)
                .append(",")
                .append(contactsTable)
                .append(" WHERE system_companies.contact_id = contacts.contact_id");
        return query;
    }

    /**
     * Provides the statement required for ordered queries
     * @return query
     */
    @Override
    protected String getOrderedQuery() {
        StringBuilder query = getQuery();
        query.append(" ORDER BY lastname,firstname");
        return query.toString();
    }

}
