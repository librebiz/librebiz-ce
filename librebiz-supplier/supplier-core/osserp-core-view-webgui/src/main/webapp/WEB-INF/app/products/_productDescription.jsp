<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<v:view viewName="productDescriptionView" />
<o:logger write="page invoked..." level="debug" />
<c:choose>
    <c:when test="${view.editMode}">
        <div class="modalBoxHeader">
            <div class="modalBoxHeaderLeft">
                <fmt:message key="productDescriptionEdit" />
            </div>
        </div>
        <div class="modalBoxData">
            <div class="subcolumns">
                <div class="subcolumn">
                    <div class="spacer"></div>
                    <v:ajaxForm name="dynamicForm" targetElement="productDescriptionConfig_popup" url="/products/productDescription/save">
                        <o:logger write="edit mode enabled, rendering form..." level="debug" />
                        <table class="valueTable first20 inputsFull" style="width: 600px;">
                            <c:if test="${!empty sessionScope.errors}">
                                <c:set var="errorsColspanCount" scope="request" value="2" />
                                <c:import url="/errors/_errors_table_entry.jsp" />
                            </c:if>
                            <tr>
                                <td><select name="language" size="1" class="fix150">
                                        <c:forEach var="lang" items="${sessionScope.portalView.supportedLanguages}">
                                            <c:choose>
                                                <c:when test="${lang.startsWith(view.bean.language)}">
                                                    <option value="<o:out value="${lang}"/>" selected="selected"><fmt:message key="${lang}" /></option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option value="<o:out value="${lang}"/>"><fmt:message key="${lang}" /></option>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>
                                </select></td>
                                <td><input type="text" name="name" value="<o:out value="${view.bean.name}"/>" /></td>
                            </tr>
                            <tr>
                                <td><fmt:message key="description" /></td>
                                <td><textarea class="form-control recordTextColumn" name="description" rows="8"><o:out value="${view.bean.description}" /></textarea></td>
                            </tr>
                            <o:permission role="product_datasheet_upload_admin" info="permissionEditDatasheetText">
                                <tr>
                                    <td><fmt:message key="datasheetText" /> <br /> <br /> <span class="smalltext"> (<fmt:message key="newParagraph" />: <fmt:message key="enterKey" />)<br /> <br /> (<fmt:message key="lineBreak" />:<br /> <fmt:message key="shiftKey" />+<fmt:message key="enterKey" />)
                                    </span></td>
                                    <td><textarea class="form-control recordTextColumn" id="datasheetText" name="datasheetText" rows="10"><o:out value="${view.bean.datasheetText}" /></textarea></td>
                                </tr>
                            </o:permission>
                            <tr>
                                <td class="row-submit"><v:ajaxLink url="/products/productDescription/disableEditMode" linkId="editLink" targetElement="productDescriptionConfig_popup">
                                        <input type="button" class="cancel" value="<fmt:message key="exit"/>" />
                                    </v:ajaxLink></td>
                                <td class="row-submit"><o:submit /></td>
                            </tr>
                        </table>
                        <o:logger write="form rendering done!" level="debug" />
                    </v:ajaxForm>

                    <div class="spacer"></div>
                </div>
            </div>
        </div>
    </c:when>
    <c:otherwise>
        <div class="modalBoxHeader">
            <div class="modalBoxHeaderLeft">
                <fmt:message key="productDescription" />
            </div>
            <div class="modalBoxHeaderRight">
                <div class="boxnav">
                    <ul>
                        <li><v:ajaxLink url="/products/productDescription/exit" linkId="editLink" targetElement="productDescriptionConfig_popup">
                                <o:img name="backIcon" />
                            </v:ajaxLink></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="modalBoxData">
            <div class="subcolumns">
                <div class="subcolumn">
                    <div class="spacer"></div>

                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th style="width: 6%;"></th>
                                <th style="width: 34%;"><fmt:message key="description" /></th>
                                <th style="width: 60%;"><fmt:message key="datasheetText" /></th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:choose>
                                <c:when test="${empty view.list}">
                                    <tr>
                                        <td colspan="3"><fmt:message key="${view.errorMessage}" /></td>
                                    </tr>
                                </c:when>
                                <c:otherwise>
                                    <c:forEach var="obj" items="${view.list}">
                                        <tr>
                                            <td style="width: 6%;"><o:out value="${obj.language}" /></td>
                                            <td style="width: 94%;" colspan="2"><v:ajaxLink url="/products/productDescription/select?id=${obj.id}" linkId="productDescriptionLink" targetElement="productDescriptionConfig_popup">
                                                    <o:out value="${obj.name}" />
                                                </v:ajaxLink></td>
                                        </tr>
                                        <tr>
                                            <td style="width: 6%;"></td>
                                            <td style="width: 34%;"><o:textOut value="${obj.description}" /></td>
                                            <td style="width: 60%;"><c:out escapeXml="false" value="${obj.datasheetText}" /></td>
                                        </tr>
                                    </c:forEach>
                                </c:otherwise>
                            </c:choose>
                        </tbody>
                    </table>

                    <div class="spacer"></div>
                </div>
            </div>
        </div>
    </c:otherwise>
</c:choose>
