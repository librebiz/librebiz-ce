/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Oct 13, 2010 12:09:01 PM 
 * 
 */
package com.osserp.core.dao.telephone.impl;

import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.dao.Tables;

import com.osserp.core.dao.impl.AbstractDao;
import com.osserp.core.dao.telephone.TelephoneConfigurationChangesets;
import com.osserp.core.model.telephone.TelephoneConfigurationChangesetImpl;
import com.osserp.core.telephone.TelephoneConfigurationChangeset;

/**
 * 
 * @author so <so@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public class TelephoneConfigurationChangesetsDao extends AbstractDao implements TelephoneConfigurationChangesets {

    protected TelephoneConfigurationChangesetsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory) {
        super(jdbcTemplate, tables, sessionFactory);
    }

    public void save(TelephoneConfigurationChangeset telephoneConfigurationChangeset) {
        getCurrentSession().saveOrUpdate(TelephoneConfigurationChangesetImpl.class.getName(), telephoneConfigurationChangeset);
    }

    public List<TelephoneConfigurationChangeset> getAllChangesetsByConfigurationId(Long cId) {
        return getCurrentSession().createQuery(
                "from " + TelephoneConfigurationChangesetImpl.class.getName()
                + " c where c.referenceId = :referenceid order by c.property, c.created")
                .setParameter("referenceid", cId ).list();
    }
}
