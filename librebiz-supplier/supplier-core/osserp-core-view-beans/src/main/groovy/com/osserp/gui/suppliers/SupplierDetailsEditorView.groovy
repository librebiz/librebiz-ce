/**
 *
 * Copyright (C) 2019 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License (AGPL)
 * version 3 as published by the Free Software Foundation. In accordance
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code
 * versions must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area
 * or, if not reasonably feasible for technical reason, as a top-level
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute.
 *
 */
package com.osserp.gui.suppliers

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ErrorCode

import com.osserp.core.suppliers.Supplier
import com.osserp.core.suppliers.SupplierManager

import com.osserp.groovy.web.ViewContextException

import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public class SupplierDetailsEditorView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(SupplierDetailsEditorView.class.getName())

    SupplierDetailsEditorView() {
        super()
        providesEditMode()
        headerName = 'supplierDetailsEditorHeader'
        dependencies = ['contactView']
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            def selectedContact = env.contactView?.bean
            if (!selectedContact && form.params.id) {
                selectedContact = supplierManager.find(getLong('id'))
            }
            if (selectedContact) {
                if (!(selectedContact instanceof Supplier)) {
                    selectedContact = supplierManager.find(selectedContact.contactId)
                }
                bean = selectedContact
            }
            enableEditMode()
        }
        if (!bean) {
            throw new ViewContextException(ErrorCode.INVALID_CONTEXT)
        }
    }

    @Override
    void save() {
        bean.shortkey = getString('shortkey')
        supplierManager.save(bean)
    }

    private SupplierManager getSupplierManager() {
        getService(SupplierManager.class.getName())
    }
}
