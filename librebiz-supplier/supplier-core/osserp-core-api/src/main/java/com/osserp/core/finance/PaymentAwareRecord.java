/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 22-Jun-2006 10:53:46 
 * 
 */
package com.osserp.core.finance;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.core.employees.Employee;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface PaymentAwareRecord extends Record {

    /**
     * Provides all payments of this invoice
     * @return payments
     */
    List<Payment> getPayments();

    /**
     * Provides a payment note
     * @return note
     */
    String getPaymentNote();

    /**
     * Sets the payment note
     * @param paymentNote
     */
    void setPaymentNote(String paymentNote);

    /**
     * Adds a payment
     * @param user
     * @param amount
     * @param paid
     * @param customHeader
     * @param note
     * @param bankAccountId
     * @return payment created
     * @throws ClientException if validation failed
     */
    Payment addCustomPayment(
            Employee user,
            BillingType type,
            BigDecimal amount,
            Date paid,
            String customHeader,
            String note,
            Long bankAccountId)
            throws ClientException;

    /**
     * Adds a payment
     * @param user
     * @param type
     * @param amount
     * @param paid
     * @param bankAccountId
     * @return payment created
     * @throws ClientException if validation failed
     */
    Payment addPayment(
            Employee user,
            BillingType type,
            BigDecimal amount,
            Date paid,
            Long bankAccountId)
            throws ClientException;

    /**
     * Provides the paid amount
     * @return paidAmount
     */
    BigDecimal getPaidAmount();

    /**
     * Provides the tax paid
     * @return paidTax
     */
    BigDecimal getPaidTax();

    /**
     * Provides the net paid amount
     * @return paidNetAmount
     */
    BigDecimal getPaidNetAmount();

    /**
     * Provides the due amount
     * @return due amount to pay
     */
    BigDecimal getDueAmount();

    /**
     * Provides most current correction if any exists
     * @return correction or null if corrections is empty
     */
    RecordCorrection getCorrection();

    /**
     * Provides available correction count
     * @return correctionCount
     */
    int getCorrectionCount();

    /**
     * Provides all available corrections
     * @return corrections
     */
    List<RecordCorrection> getCorrections();

    /**
     * Adds a correction
     * @param user
     * @param name
     * @param street
     * @param streetAddon
     * @param zipcode
     * @param city
     * @param country
     * @param note
     * @throws ClientException if address validation failed
     */
    void addCorrection(
            Employee user,
            String name,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long country,
            String note) throws ClientException;

    /**
     * Updates an existing correction
     * @param user
     * @param id
     * @param name
     * @param street
     * @param streetAddon
     * @param zipcode
     * @param city
     * @param country
     * @param note
     * @throws ClientException if address validation failed
     */
    void updateCorrection(
            Employee user,
            Long id,
            String name,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long country,
            String note) throws ClientException;

    /**
     * Releases current correction
     */
    void releaseCorrection();
}
