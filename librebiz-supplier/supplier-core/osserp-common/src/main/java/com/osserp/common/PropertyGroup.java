/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 20, 2016 
 * 
 */
package com.osserp.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * 
 * Provides an object with a list of properties.
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PropertyGroup implements Serializable {

    private String name = null;
    private List<Property> properties = new ArrayList<>();

    
    protected PropertyGroup() {
        super();
    }
    
    protected PropertyGroup(String name, List<Property> properties) {
        super();
        this.name = name;
        this.properties = properties;
    }
    
    /**
     * Craetes a list of properties grouped by name.
     * @param propertyList
     * @return list of propery groups
     */
    public static List<PropertyGroup> create(List<Property> propertyList) {
        Set<String> added = new java.util.HashSet();
        List<PropertyGroup> result = new ArrayList<>();
        for (int i = 0, j = propertyList.size(); i<j; i++) {
            Property next = propertyList.get(i);
            if (!added.contains(next.getName())) {
                result.add(create(next.getName(), propertyList));
            }
        }
        return result;
    }
    
    protected static PropertyGroup create(String name, List<Property> propertyList) {
        List<Property> result = new ArrayList<>();
        for (int i = 0, j = propertyList.size(); i<j; i++) {
            Property next = propertyList.get(i);
            if (next.getName().equals(name)) {
                result.add(next);
            }
        }
        return new PropertyGroup(name, result);
    }

    /**
     * Provides the name of the group 
     * @return name
     */
    public String getName() {
        return name;
    }
    
    protected void setName(String name) {
        this.name = name;
    }
    
    /**
     * Provides all values as comma separated list 
     * @return
     */
    public String getValuesAsString() {
        StringBuilder buffer = new StringBuilder();
        for (int i = 0, j = properties.size(); i<j; i++) {
            Property next = properties.get(i);
            if (buffer.length() == 0) {
                buffer.append(next.getValue());
            } else {
                buffer.append(", ").append(next.getValue());
            }
        }
        return buffer.toString();
    }

    /**
     * Provides all associated properties 
     * @return properties
     */
    public List<Property> getProperties() {
        return properties;
    }

    protected void setProperties(List<Property> properties) {
        this.properties = properties;
    }

}
