/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 6, 2009 8:46:45 PM 
 * 
 */
package com.osserp.core.model.calc;

import java.math.BigDecimal;
import java.util.Date;

import com.osserp.core.products.Product;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PartlistPositionImpl extends AbstractItemPositionImpl {

    /**
     * Default constructor to implement java.io.Serializable interface
     */
    protected PartlistPositionImpl() {
        super();
    }

    /**
     * Constructor for creating new persistent objects
     * @param name
     * @param reference
     * @param groupId of the related config group
     * @param discounts
     * @param option
     */
    public PartlistPositionImpl(String name, Long reference, Long groupId, boolean discounts, boolean option) {
        super(name, reference, groupId, discounts, option);
    }

    @Override
    public void addItem(
            Product product, 
            String customName, 
            Double quantity, 
            BigDecimal price, 
            Date priceDate, 
            boolean priceOverridden, 
            BigDecimal partnerPrice, 
            boolean partnerPriceEditable, 
            boolean partnerPriceOverridden, 
            BigDecimal purchasePrice, 
            Double taxRate, 
            String note, 
            boolean includePrice, 
            Long externalId) {
        int order = getItems().size() + 1;
        PartlistItemImpl obj = new PartlistItemImpl(
                getId(),
                product,
                customName,
                quantity,
                price,
                priceDate,
                partnerPrice,
                partnerPriceEditable,
                partnerPriceOverridden,
                purchasePrice,
                taxRate,
                note,
                includePrice,
                order,
                externalId);
        obj.setPriceOverridden(priceOverridden);
        addItem(obj);
    }
}
