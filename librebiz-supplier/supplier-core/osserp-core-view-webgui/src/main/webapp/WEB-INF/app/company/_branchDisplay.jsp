<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${!empty requestScope.branchDisplay}">

    <div class="col-md-6 panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4><o:out value="${branchDisplay.name}"/></h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-body">

                <c:if test="${view.multipleClientsAvailable}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="companyName"><fmt:message key="company"/></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <o:out value="${branchDisplay.company.name}"/>
                            </div>
                        </div>
                    </div>
                </c:if>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="shortname"><fmt:message key="logicalName"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:choose>
                                <c:when test="${!empty branchForwardExit}">
                                    <v:link url="/company/branch/forward?id=${branchDisplay.id}&enableEdit=true&exit=${branchForwardExit}">
                                        <o:out value="${branchDisplay.shortname}"/>
                                    </v:link>
                                </c:when>
                                <c:otherwise>
                                    <o:out value="${branchDisplay.shortname}"/>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="shortkey"><fmt:message key="shortkey"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${branchDisplay.shortkey}"/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="headquarter"><fmt:message key="headquarter"/></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${branchDisplay.headquarter}" mode="yesnocap"/>
                        </div>
                    </div>
                </div>

                <c:if test="${branchDisplay.thirdparty}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="thirdparty"><fmt:message key="dedicatedLegalform"/></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <o:out value="${branchDisplay.thirdparty}" mode="yesnocap"/>
                            </div>
                        </div>
                    </div>
                </c:if>

                <c:if test="${branchDisplay.customTemplates}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="customTemplates"><fmt:message key="customTemplates"/></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <o:out value="${branchDisplay.customTemplates}" mode="yesnocap"/>
                            </div>
                        </div>
                    </div>
                </c:if>

                <c:if test="${branchDisplay.recordTemplatesDefault}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="defaultRecordTemplates"><fmt:message key="defaultRecordTemplates"/></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <o:out value="${branchDisplay.recordTemplatesDefault}" mode="yesnocap"/>
                            </div>
                        </div>
                    </div>
                </c:if>

                <c:if test="${!empty branchDisplay.email}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="email"><fmt:message key="email"/></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <o:out value="${branchDisplay.email}"/>
                            </div>
                        </div>
                    </div>
                </c:if>

                <c:if test="${!empty branchDisplay.costCenter}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="costCenter"><fmt:message key="costCenter"/></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <o:out value="${branchDisplay.costCenter}"/>
                            </div>
                        </div>
                    </div>
                </c:if>

                <c:if test="${branchDisplay.phoneAvailable}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="costCenter"><fmt:message key="phone"/></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <o:out value="${branchDisplay.phoneCountry}" /> 
                                <o:out value="${branchDisplay.phonePrefix}" /> 
                                <o:out value="${branchDisplay.phoneNumber}" />
                            </div>
                        </div>
                    </div>
                </c:if>

                <c:if test="${!view.stockManagementEnabled && branchDisplay.headquarter}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="stockManagement"><fmt:message key="stockManagement"/></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <o:permission role="${view.stockConfigAdminPermissions}" info="permissionAdmin">
                                    <v:link url="${view.baseLink}/toggleStockActivation" title="activate" confirm="true" message="confirmStockActivation" >
                                        <fmt:message key="deactivated"/>
                                    </v:link>
                                </o:permission>
                                <o:forbidden role="${view.stockConfigAdminPermissions}">
                                    <fmt:message key="deactivated"/>
                                </o:forbidden>
                            </div>
                        </div>
                    </div>
                </c:if>

            </div>
        </div>

        <c:if test="${view.stockManagementEnabled}">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-4">
                            <h4><fmt:message key="stockManagement"/></h4>
                        </div>
                        <div class="col-md-8">
                            <o:permission role="${view.stockConfigAdminPermissions}" info="permissionAdmin">
                                <v:link url="${view.baseLink}/toggleStockActivation" title="deactivate" confirm="true" message="confirmStockDeactivation" >
                                    <fmt:message key="activated"/>
                                </v:link>
                            </o:permission>
                            <o:forbidden role="${view.stockConfigAdminPermissions}">
                                <fmt:message key="activated"/>
                            </o:forbidden>
                            <c:if test="${!empty view.stock.activationDate}">
                                <fmt:message key="since"/> <o:date value="${view.stock.activationDate}"/>
                            </c:if>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="form-body">
                    <c:if test="${branchDisplay.headquarter}">
                        <c:choose>
                            <c:when test="${view.stocktakingSupportEnabled}">
                                <c:set var="stLinkTitle" value="activate" />
                                <c:set var="stLabel" value="activated" />
                            </c:when>
                            <c:otherwise>
                                <c:set var="stLinkTitle" value="deactivate" />
                                <c:set var="stLabel" value="deactivated" />
                            </c:otherwise>
                        </c:choose>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="stocktakingSupport"><fmt:message key="stockTakingSupport" /></label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <o:permission role="${view.stockConfigAdminPermissions}" info="permissionAdmin">
                                        <v:link url="${view.baseLink}/toggleStocktakingSupport" title="${stLinkTitle}" >
                                            <fmt:message key="${stLabel}"/>
                                        </v:link>
                                    </o:permission>
                                    <o:forbidden role="${view.stockConfigAdminPermissions}">
                                        <fmt:message key="${stLabel}"/>
                                    </o:forbidden>
                                </div>
                            </div>
                        </div>
                    </c:if>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <c:choose>
                                    <c:when test="${branchDisplay.headquarter}">
                                        <label for="stockName"><fmt:message key="stockName"/></label>
                                    </c:when>
                                    <c:otherwise>
                                        <label for="stockName"><fmt:message key="dedicatedInventory"/></label>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <c:choose>
                                    <c:when test="${!empty view.stock}">
                                        <v:link url="/company/stockConfig/forward?id=${view.stock.id}&exit=${companyExit}" title="change">
                                            <o:out value="${view.stock.name}" />
                                        </v:link>
                                    </c:when>
                                    <c:otherwise>
                                        <v:link url="${view.baseLink}/createStock" title="create" confirm="true" message="confirmStockCreate" >
                                            <fmt:message key="bigNo"/>
                                        </v:link>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                    <c:if test="${!empty view.stock}">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="shortKey"><fmt:message key="shortkey"/></label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <o:out value="${view.stock.shortKey}" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="description"><fmt:message key="description"/></label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <o:out value="${view.stock.description}" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="active"><fmt:message key="activatedLabel"/></label>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <c:choose>
                                        <c:when test="${view.stock.active}">
                                            <fmt:message key="yes" />
                                        </c:when>
                                        <c:otherwise>
                                            <fmt:message key="no" />
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <fmt:message key="planning" />
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <c:choose>
                                        <c:when test="${view.stock.planningAware}">
                                            <fmt:message key="yes" />
                                        </c:when>
                                        <c:otherwise>
                                            <fmt:message key="no" />
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </div>
                        </div>

                    </c:if>
                </div>
            </div>
        </c:if>
    </div>
</c:if>
