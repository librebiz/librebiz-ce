<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="letterView" />
<v:form name="letterForm" url="${view.baseLink}/save">

    <div class="col-md-6 panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>
                    <fmt:message key="existingTemplateLabel" />
                </h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped">
                            <tbody>
                                <c:choose>
                                    <c:when test="${empty view.availableTemplates}">
                                        <tr>
                                            <td><fmt:message key="noLetterTemplatesAvailable" /></td>
                                        </tr>
                                    </c:when>
                                    <c:otherwise>
                                        <c:forEach var="opt" items="${view.availableTemplates}" varStatus="s">
                                            <tr>
                                                <td class="chkbx"><input type="radio" name="templateId" value="<o:out value="${opt.id}"/>" /></td>
                                                <td class="letterName"><o:out value="${opt.name}" /></td>
                                                <td class="center">
                                                    <v:link url="${view.baseLink}/printTemplate?id=${opt.id}">
                                                        <o:img name="printIcon" styleClass="bigIcon" />
                                                    </v:link>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </c:otherwise>
                                </c:choose>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6 panel-area panel-area-default">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>
                    <fmt:message key="createNewLetter" />
                </h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="nameOrMotivationForLetter"><fmt:message key="nameOrMotivationForLetter" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <v:text styleClass="form-control" name="name" value="${view.bean.name}" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="branch"><fmt:message key="branch" /></label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <select class="form-control" name="branch" size="1">
                                <c:forEach var="opt" items="${letterView.branchOffices}">
                                    <c:choose>
                                        <c:when test="${letterView.currentBranch == opt.id}">
                                            <option value="<o:out value="${opt.id}"/>" selected="selected"><o:out value="${opt.company.name}" /> -
                                                <o:out value="${opt.name}" /></option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="<o:out value="${opt.id}"/>"><o:out value="${opt.company.name}" /> -
                                                <o:out value="${opt.name}" /></option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row next">
                    <div class="col-md-4"></div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <v:submitExit url="${view.baseLink}/disableCreateMode" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <o:submit styleClass="form-control" value="create" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</v:form>
