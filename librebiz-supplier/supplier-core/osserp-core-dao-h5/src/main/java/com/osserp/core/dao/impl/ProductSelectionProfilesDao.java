/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 27, 2009 2:50:50 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.dao.AbstractHibernateAndSpringEntityDao;
import com.osserp.common.dao.Tables;

import com.osserp.core.dao.ProductSelectionProfiles;
import com.osserp.core.model.products.ProductSelectionProfileImpl;
import com.osserp.core.products.ProductSelectionProfile;

/**
 * @author Rainer Kirchner <rk@osserp.com>
 */
public class ProductSelectionProfilesDao extends AbstractHibernateAndSpringEntityDao
        implements ProductSelectionProfiles {

    protected ProductSelectionProfilesDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory) {

        super(jdbcTemplate, tables, sessionFactory);
    }

    @Override
    protected Class<ProductSelectionProfileImpl> getEntityClass() {
        return ProductSelectionProfileImpl.class;
    }

    public ProductSelectionProfile getDefaultProfile() {
        StringBuilder hql = new StringBuilder(128);
        hql
                .append("from ")
                .append(ProductSelectionProfile.class.getName())
                .append(" selectionProfile where selectionProfile.defaultProfile = true");
        List<ProductSelectionProfile> profiles = getCurrentSession().createQuery(hql.toString()).list();
        return profiles.isEmpty() ? null : profiles.get(0);
    }
}
