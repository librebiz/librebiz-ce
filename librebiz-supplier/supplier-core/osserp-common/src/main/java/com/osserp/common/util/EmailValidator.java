/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Aug 6, 2004 
 * 
 */
package com.osserp.common.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

import org.apache.commons.io.IOUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;

/**
 * 
 * Note: The EmailValidator checks emails against real mx and smtp servers. You cannot use the EmailValidator on hosts without an internet connection or hosts
 * with a dialup ip assigned!
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class EmailValidator extends AbstractValidator {
    private static Logger log = LoggerFactory.getLogger(EmailValidator.class.getName());

    private static final String EMAIL_PATTERN = "([a-zA-Z0-9_\\.\\+]+)@"
            + "((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)"
            + "|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)";

    private static boolean checkSmtp = false;
    private static boolean checkMx = false;

    static {
        try {
            String value = System.getProperty("com.osserp.common.util.EmailValidator.checkSmtp");
            if (value != null && "true".equalsIgnoreCase(value)) {
                checkSmtp = true;
                if (log.isDebugEnabled()) {
                    log.debug("<init-static{}> enabled SMTP check");
                }
            } else if (log.isDebugEnabled()) {
                log.debug("<init-static{}> disabled SMTP check (config property false or missing)");
            }
            if (!checkSmtp) {
                value = System.getProperty("com.osserp.common.util.EmailValidator.checkMx");
                if (value != null && "true".equalsIgnoreCase(value)) {
                    checkMx = true;
                    if (log.isDebugEnabled()) {
                        log.debug("<init-static{}> enabled MX-Record check");
                    }
                } else if (log.isDebugEnabled()) {
                    log.debug("<init-static{}> disabled MX-Record check (config property false or missing)");
                }
            }
        } catch (Throwable t) {
            log.error("<init-static{}> failed to get configuration [message=" + t.getMessage() + "]", t);
        }
    }

    /**
     * Disables MX-Record validation.
     * 
     */
    public static void disableCheckMX() {
        checkMx = false;
        if (log.isDebugEnabled()) {
            log.debug("disableCheckMX() done");
        }
    }

    /**
     * Enables MX-Record validation.
     * 
     */
    public static void enableCheckMX() {
        checkMx = true;
        if (log.isDebugEnabled()) {
            log.debug("enableCheckMX() done");
        }
    }

    /**
     * Indicates if MX-Record check is enabled
     * 
     */
    public static boolean isCheckMXEnabled() {
        return checkMx;
    }

    /**
     * Disables SMTP online validation (Checks MX only).
     * 
     */
    public static void disableCheckSMTP() {
        checkSmtp = false;
        if (log.isDebugEnabled()) {
            log.debug("disableCheckSMTP() done");
        }
    }

    /**
     * Enables SMTP online validation (Check MX and SMTP).
     * 
     */
    public static void enableCheckSMTP() {
        checkSmtp = true;
        if (log.isDebugEnabled()) {
            log.debug("enableCheckSMTP() done");
        }
    }

    /**
     * Indicates if SMTP check is enabled
     * 
     */
    public static boolean isCheckSMTPEnabled() {
        return checkSmtp;
    }

    /**
     * Fetches the domain part of an email adress
     * @param address
     * @return domain part if address is valid mail address, unchanged param value if not address, null if address is null
     */
    public static String fetchDomainPart(String address) {
        if (address != null) {
            return address.substring(address.indexOf("@") + 1, address.length());
        }
        return null;
    }

    /**
     * Validates email address against DNS- and SMTP-Server.
     * @param email
     * @return true if any server accepts mail for address
     */
    public static boolean validate(String email) {
        try {
            checkPattern(email);
        } catch (ClientException e) {
            return false;
        }
        if (checkSmtp) {
            return checkSMTP(email);
        }
        if (checkMx) {
            try {
                checkMX(email);
            } catch (ClientException e) {
                return false;
            }
        }
        return true;
    }

    /**
     * Validates an email address against DNS- and SMTP-Server.
     * @param email address to validate or comma separated list of email addresses
     * @param required if missing exception should be thrown if email is not set
     * @throws ClientException if email is invalid or email is not set in case that required is true
     */
    public static void validate(String email, boolean required) throws ClientException {
        if (required && isNotSet(email)) {
            throw new ClientException(ErrorCode.EMAIL_MISSING);
        }
        if (isSet(email)) {

            if (email.indexOf(",") > -1) {
                validateEmails(email);

            } else if (!validate(email)) {
                throw new ClientException(ErrorCode.EMAIL_INVALID, (Object) email);
            }
        }
    }

    /**
     * Validates a comma separated list of email addresses
     * @param commaSeparatedEmails
     * @throws ClientException if validation failed
     */
    protected static void validateEmails(String commaSeparatedEmails) throws ClientException {
        String[] recipients = StringUtil.getTokenArray(commaSeparatedEmails, ",");
        if (recipients != null) {
            for (int i = 0, j = recipients.length; i < j; i++) {
                if (!validate(recipients[i])) {
                    throw new ClientException(ErrorCode.EMAIL_INVALID, (Object) recipients[i]);
                }
            }
        }
    }

    /**
     * Checks if given domain name or email address has a valid MX-Record configured
     * @param domainOrEmailAddress
     * @throws ClientException if domainOrEmailAddress is null, domain invalid or domain has no valid MX
     */
    protected static void checkMX(String domainOrEmailAddress)
            throws ClientException {
        List<String> mailServers = getMailServers(domainOrEmailAddress, true);
        if (mailServers.size() == 0) {
            throw new ClientException(ErrorCode.EMAIL_UNDELIVERABLE);
        }
    }

    /**
     * Verifies email address against dns and mail server
     * @param address address
     * @return true if any SMTP-Host accepts mail for address
     */
    protected static boolean checkSMTP(String address) {
        if (isNotSet(address)) {
            return false;
        }
        String domain = fetchDomainPart(address);
        List<String> servers = new ArrayList<String>();
        try {
            servers = getMailServers(domain, false);
        } catch (ClientException ex) {
            return false;
        }
        if (servers.size() == 0) {
            return false;
        }
        for (int i = 0, j = servers.size(); i < j; i++) {
            boolean valid = false;
            Socket socket = null;
            try {
                int status;
                String server = servers.get(i);
                socket = new Socket(server, 25);
                BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

                status = listen(reader);
                if (status != 220) {

                    throw new Exception("Receipt invalid header for " + server + ", status: " + status);
                }
                talk(writer, "EHLO osserp.com");

                status = listen(reader);
                if (status != 250) {
                    throw new Exception("Server " + server + " does not support ESMTP");
                }
                // set sender address
                talk(writer, "MAIL FROM: <info@osserp.com>");
                status = listen(reader);
                if (status != 250) {
                    throw new Exception("Server " + server + " rejects sender info@osserp.com");
                }

                talk(writer, "RCPT TO: <" + address + ">");
                status = listen(reader);

                // be polite
                talk(writer, "RSET");
                listen(reader);
                talk(writer, "QUIT");
                listen(reader);
                if (status != 250) {
                    throw new Exception("Address is not valid [" + address + "]");
                }

                valid = true;
                reader.close();
                writer.close();
                socket.close();
            } catch (Exception ex) {
                // ignore this and check next server
                log.warn("checkSMTP() check failed [message=" + ex.getMessage() + "]");
            } finally {
                IOUtils.closeQuietly(socket);
                if (valid) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Provides a list of mailserver names as provided by DNS.
     * @param domainOrEmailAddress
     * @param lookupMXOnly Only valid MX-Records will be returned if true. If false, an additional A-Record lookup will be performed if no MX-Record was found.
     * @return list of mailservers
     * @throws ClientException if domainOrEmailAddress is null or domain is invalid
     */
    @SuppressWarnings("unchecked")
    protected static List<String> getMailServers(String domainOrEmailAddress, boolean lookupMXOnly) throws ClientException {
        if (isNotSet(domainOrEmailAddress)) {
            throw new ClientException(ErrorCode.DOMAIN_MISSING);
        }
        List<String> servers = new ArrayList<String>();
        String valueToTest = fetchDomainPart(domainOrEmailAddress);
        Hashtable env = new Hashtable();
        env.put("java.naming.factory.initial",
                "com.sun.jndi.dns.DnsContextFactory");
        try {
            DirContext ctx = new InitialDirContext(env);
            Attributes attrs = ctx.getAttributes(valueToTest,
                    new String[] { "MX" });
            Attribute attr = attrs.get("MX");
            if (attr == null || attr.size() < 1 && !lookupMXOnly) {
                attrs = ctx.getAttributes(valueToTest, new String[] { "A" });
                attr = attrs.get("A");
            }
            if (attr == null) {
                throw new NamingException("Domain '" + valueToTest
                        + "' has no DNS entry");
            }
            NamingEnumeration enumeration = attr.getAll();
            while (enumeration.hasMore()) {
                String next = (String) enumeration.next();
                String entries[] = next.split(" ");
                if (entries[1].endsWith(".")) {
                    entries[1] = entries[1].substring(0,
                            (entries[1].length() - 1));
                }
                String server = entries[1];
                servers.add(server);
            }
        } catch (NamingException e) {
            log.warn("getMailServers() caught naming exception [address="
                    + domainOrEmailAddress
                    + ", message="
                    + e.getMessage() + "]");
            throw new ClientException(ErrorCode.DOMAIN_INVALID);
        }
        return servers;
    }

    private static int listen(BufferedReader input) throws IOException {
        String line = null;
        int result = 0;
        while ((line = input.readLine()) != null) {
            //System.out.println("listen receipt response: " + line);
            String statusCode = line.substring(0, 3);
            try {
                result = Integer.parseInt(statusCode);
            } catch (Exception ex) {
                result = -1;
            }
            if (line.charAt(3) != '-') {
                break;
            }
        }
        return result;
    }

    private static void talk(BufferedWriter writer, String command) throws IOException {
        writer.write(command + "\r\n");
        writer.flush();
        return;
    }

    /**
     * Checks an email address against regular expression pattern
     * @param email
     * @throws ClientException if pattern validation failed
     */
    protected static void checkPattern(String email) throws ClientException {
        if (isNotSet(email)) {
            throw new ClientException(ErrorCode.EMAIL_MISSING);
        }
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher("");
        matcher.reset(email);
        if (!matcher.find()) {
            throw new ClientException(ErrorCode.EMAIL_INVALID);
        }
    }

}
