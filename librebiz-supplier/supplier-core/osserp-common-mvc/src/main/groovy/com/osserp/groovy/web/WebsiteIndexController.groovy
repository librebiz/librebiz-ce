/**
 *
 * Copyright (C) 2017 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 *
 * Created on Mar 10, 2017 
 *
 */
package com.osserp.groovy.web

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.servlet.http.HttpSession

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.common.web.PortalView
import com.osserp.common.web.RequestUtil

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
abstract class WebsiteIndexController extends AbstractIndexController {
    private static Logger logger = LoggerFactory.getLogger(WebsiteIndexController.class.getName())
    private Locale defaultLocale = new Locale('de')
    
    @RequestMapping('/index')
    def index(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("index: invoked [host=${RequestUtil.getRemoteAddress(request)}]")
        HttpSession session = request.session
        PortalView view = getPortalView(request)
        if (view?.getMenu() == null) {
            logger.debug("index: menu not available [view=${(view != null)}]")
        } else {
            logger.debug("index: menu assigned [headers=${(view.getMenu().getHeaders().size())}]")
        }
        String startup = view.startupTarget
        if (startup) {
            logger.debug("index: redirect found [target=${startup}]")
            return redirect(request, startup)
        }
        logger.debug('index: did not find startupTarget [result=index]')
        'index'
    }
    
    @RequestMapping('/credits')
    def credits(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("credits: invoked [host=${RequestUtil.getRemoteAddress(request)}]")
        'credits'
    }
    
    @RequestMapping('/faq')
    def faq(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("faq: invoked [host=${RequestUtil.getRemoteAddress(request)}]")
        'faq'
    }
    
    @RequestMapping('/imprint')
    def imprint(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("imprint: invoked [host=${RequestUtil.getRemoteAddress(request)}]")
        'imprint'
    }

    @RequestMapping('/privacy')
    def privacy(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("privacy: invoked [host=${RequestUtil.getRemoteAddress(request)}]")
        'privacy'
    }

    @RequestMapping('/tos')
    def tos(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("tos: invoked [host=${RequestUtil.getRemoteAddress(request)}]")
        'tos'
    }

    @RequestMapping('/error')
    def error(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("error: invoked [host=${RequestUtil.getRemoteAddress(request)}]")
        'error'
    }
}
