<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>

<c:set var="recordView" scope="session" value="${sessionScope.purchaseOrderView}"/>
<c:set var="record" value="${recordView.record}"/>
<c:set var="hadSomethingToDisplay" value="false"/>

<div class="subcolumns">
    <%-- Left column --%>
    <div class="column50l">
        <div class="subcolumnl">

            <div class="spacer"></div>
            <div class="contentBox">
                <div class="contentBoxHeader" style="width: 98%;">
                    <div class="contentBoxHeaderLeft">
                        <fmt:message key="actions" />
                    </div>
                </div>

                <div class="contentBoxData">
                    <div class="subcolumns">
                        <div class="subcolumn">
                            <div class="spacer"></div>
                            
                            <c:if test="${empty record.deliveryNotes and recordView.importSupported and recordView.itemBackupAvailable}">
                            <o:forbidden role="executive,purchase_order_change">
                                <div class="recordLabelValue">
                                    <fmt:message key="restorePreImportItems"/>
                                </div>
                            </o:forbidden>
                            <o:permission role="executive,purchase_order_change" info="permissionChangePurchaseOrder">
                                <div class="recordLabelValue">
                                    <a href="javascript:onclick=confirmLink('<fmt:message key="confirmPlease"/>:\n\n<fmt:message key="restorePreImportItems"/>','<c:url value="/purchaseOrder.do?method=restoreItems"/>');"><fmt:message key="restorePreImportItems"/></a>
                                </div>
                            </o:permission>
                            <c:set var="hadSomethingToDisplay" value="true"/>
                            </c:if>

                            <c:if test="${record.unchangeable and empty record.deliveryNotes}">
                            <o:forbidden role="executive,purchase_order_change">
                                <div class="recordLabelValue">
                                    <fmt:message key="changePurchaseOrder"/>
                                </div>
                            </o:forbidden>
                            <o:permission role="executive,purchase_order_change" info="permissionChangePurchaseOrder">
                                <div class="recordLabelValue">
                                    <a href="javascript:onclick=confirmLink('<fmt:message key="confirmPlease"/>:\n\n<fmt:message key="changePurchaseOrder"/>','<c:url value="/purchaseOrder.do?method=createNewVersion"/>');"><fmt:message key="changePurchaseOrder"/></a>
                                </div>
                            </o:permission>
                            <c:set var="hadSomethingToDisplay" value="true"/>
                            </c:if>
                            <c:if test="${!hadSomethingToDisplay}">
                                <div class="recordLabelValue">
                                    <fmt:message key="noActionsAvailableOnCurrentRecordState"/>
                                </div>
                            </c:if>

                            <div class="spacer"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="spacer"></div>
        </div>
    </div>

    <%-- Right column --%>
    <div class="column50r">
        <div class="subcolumnr">
            <div class="spacer"></div>
            <div class="contentBox">
                <div class="contentBoxHeader">
                    <div class="contentBoxHeaderLeft">
                        <fmt:message key="versionArchive"/>                        
                    </div>
                </div>
                <div class="contentBoxData">
                    <div class="subcolumns">
                        <div class="subcolumn">
                            <div class="spacer"></div>
                            <c:choose>
                            <c:when test="${!empty recordView.versions}">
                            <c:forEach var="obj" items="${recordView.versions}">
                            <div class="recordLabelValue">
                                <a href="<c:url value="/purchaseOrder.do"><c:param name="method" value="printVersion"/><c:param name="id" value="${obj.id}"/></c:url>"><fmt:message key="version"/> <o:out value="${obj.version}"/> <fmt:message key="from"/> <o:date value="${obj.created}"/></a>
                            </div>
                            </c:forEach>
                            </c:when>
                            <c:otherwise>
                            <div class="recordLabelValue">
                                <fmt:message key="noEarlyVersionsAvailable"/>
                            </div>
                            </c:otherwise>
                            </c:choose>
                            
                            <div class="spacer"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="spacer"></div>
        </div>
    </div>
    <%-- End right column --%>
</div>
