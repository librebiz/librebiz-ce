/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Oct 25, 2009 1:29:44 PM 
 * 
 */
package com.osserp.gui.web.records;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.web.RequestUtil;

import com.osserp.gui.client.records.RecordProductSearchView;
import com.osserp.gui.web.products.AbstractProductSearchAction;

/**
 * @author Rainer Kirchner <rk@osserp.com>
 */
public class RecordProductSearchAction extends AbstractProductSearchAction {
    private static Logger log = LoggerFactory.getLogger(RecordProductSearchAction.class.getName());

    @Override
    protected Class<RecordProductSearchView> getViewClass() {
        return RecordProductSearchView.class;
    }

    /**
     * Displays record product search. Search view will be created if not exists.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward display(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("display() request from " + getUser(session).getId());
        }
        RecordProductSearchView view = (RecordProductSearchView) getView(session);
        if (view == null) {
            return forward(mapping, form, request, response);
        }
        return mapping.findForward(view.getActionTarget());
    }

    @Override
    public ActionForward exit(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        RecordProductSearchView view = (RecordProductSearchView) getView(request.getSession());
        if (view != null && view.getRecordView() != null) {
            view.getRecordView().previousRequestWasExit();
            if (view.getRecordView().isItemReplaceMode()) {
                view.getRecordView().setItemReplace(null);
            }
        }
        return super.exit(mapping, form, request, response);
    }

    /**
     * Selects an item
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward selectItem(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("selectItem() request from " + getUser(session).getId());
        }
        RecordProductSearchView view = (RecordProductSearchView) getView(session);
        if (view == null) {
            return forward(mapping, form, request, response);
        }
        view.selectItem(RequestUtil.getId(request));
        return super.exit(mapping, form, request, response);
    }

}
