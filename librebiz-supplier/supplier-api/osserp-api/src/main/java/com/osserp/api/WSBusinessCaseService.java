/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 16.12.2012 
 * 
 */
package com.osserp.api;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface WSBusinessCaseService {

    /**
     * Tries to fetch an existing businessCase by id.
     * @param businessCaseId id of the object to fetch
     * @return businessCase object or null if not exists
     */
    WSBusinessCase fetchBusinessCaseById(Long businessCaseId);

    /**
     * Gets an existing businessCase by id or throws runtime exception
     * @param businessCaseId of the object to get
     * @return businessCase object
     * @throws ServiceException if businessCase does not exist
     */
    WSBusinessCase getBusinessCaseById(Long businessCaseId) throws WSObjectException;

    /**
     * Updates an existing or creates a new businessCase
     * @param wsBusinessCase object to persist
     * @return created or updated businessCase object
     */
    WSBusinessCase createOrUpdate(WSBusinessCase wsBusinessCase);

}
