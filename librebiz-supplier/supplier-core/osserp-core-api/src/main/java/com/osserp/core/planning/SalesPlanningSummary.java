/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 24-Sep-2006 08:26:39 
 * 
 */
package com.osserp.core.planning;

import com.osserp.core.products.ProductSummaryImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SalesPlanningSummary extends ProductSummaryImpl implements ProductPlanningSummary {

    private Long productId = null;
    private Long productGroup = null;
    private String productName = null;
    private Double quantity = null;
    private boolean lazy = false;

    protected SalesPlanningSummary() {
        super();
    }

    public SalesPlanningSummary(
            Long stockId,
            Long productId,
            Long productGroup,
            String productName,
            Double quantity,
            Double vacant,
            Double stock,
            Double receipt,
            Double expected,
            Double salesReceipt) {
        super(
                stockId,
                stock,
                receipt,
                expected,
                quantity, // ordered
                vacant,
                null, // currentUnreleased, 
                null, // avg pp
                null, // last pp
                salesReceipt);
        this.productId = productId;
        this.productGroup = productGroup;
        this.productName = productName;
        this.quantity = (quantity == null) ? 0d : quantity;
    }

    public Long getProductId() {
        return productId;
    }

    protected void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getProductGroup() {
        return productGroup;
    }

    protected void setProductGroup(Long productGroup) {
        this.productGroup = productGroup;
    }

    public String getProductName() {
        return productName;
    }

    protected void setProductName(String productName) {
        this.productName = productName;
    }

    public Double getQuantity() {
        return quantity;
    }

    protected void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public void addQuantity(Double _quantity) {
        this.quantity = quantity + ((_quantity == null) ? 0 : _quantity);
        setOrdered(quantity);
    }

    public boolean isLazy() {
        return lazy;
    }

    public void setLazy(boolean lazy) {
        this.lazy = lazy;
    }
}
