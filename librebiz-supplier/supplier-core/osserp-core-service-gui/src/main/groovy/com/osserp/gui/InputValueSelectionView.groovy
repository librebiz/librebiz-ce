/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Mar 28, 2011 9:43:58 AM 
 * 
 */
package com.osserp.gui

/**
 *
 * @author jg <jg@osserp.com>
 * @author rk <rk@osserp.com>
 *
 */
abstract class InputValueSelectionView extends CoreView {

    String inputId = null
    String currentValue = null

    InputValueSelectionView() {
        super()
        enablePopupView()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        inputId = form.getString('inputId')
        currentValue = form.getString('currentValue')
    }
}

