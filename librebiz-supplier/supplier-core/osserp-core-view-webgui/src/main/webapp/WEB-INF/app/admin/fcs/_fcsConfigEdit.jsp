<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <o:out value="${view.bean.id}"/> - <o:out value="${view.bean.name}"/>
            </h4>
        </div>
    </div>
    <div class="panel-body">

        <div class="form-body">

            <c:import url="${viewdir}/admin/fcs/_fcsConfigInput.jsp"/>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <fmt:message key="displayAlways" />
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="checkbox">
                            <label for="displayAlways"> <v:checkbox name="displayEverytime" /> <span class="smalltext">(<fmt:message key="canBeoptionalAccomplished" />)
                            </span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="status"><fmt:message key="status" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <v:text name="status" styleClass="form-control" />
                    </div>
                </div>
            </div>

            <c:if test="${!empty requestScope.fcsConfigCustomCustomImport}">
                <c:import url="${requestScope.fcsConfigCustomCustomImport}" />
            </c:if>

            <div class="row next">
                <div class="col-md-4"></div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <v:submitExit url="${view.baseLink}/disableEditMode" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <o:submit value="save" styleClass="form-control" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
