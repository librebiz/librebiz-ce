/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 18, 2012 12:43:46 PM 
 * 
 */
package com.osserp.gui.notes

import javax.servlet.http.HttpServletRequest

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import com.osserp.core.mail.MailMessageContext

import com.osserp.groovy.web.ViewController

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
@RequestMapping("/notes/note/*")
@Controller class NoteController extends ViewController {
    private static Logger logger = LoggerFactory.getLogger(NoteController.class.getName())

    @Override
    @RequestMapping
    def save(HttpServletRequest request) {
        logger.debug("save: invoked [user=${getUserId(request)}]")
        NoteView view = getView(request)
        def result = super.save(request)
        if (!view.sendMail) {
            return result
        }
        gotoMail(view, request)
    }

    @RequestMapping
    def setSticky(HttpServletRequest request) {
        logger.debug("setSticky: invoked [user=${getUserId(request)}]")
        NoteView view = getView(request)
        view.setSticky()
        defaultPage
    }

    @RequestMapping
    def addMailStatus(HttpServletRequest request) {
        logger.debug("addMailStatus: invoked [user=${getUserId(request)}]")
        NoteView view = getView(request)
        view.addMailStatus()
        defaultPage
    }

    @RequestMapping
    def forwardMail(HttpServletRequest request) {
        logger.debug("forwardMail: invoked [user=${getUserId(request)}]")
        NoteView view = getView(request)
        view.createMailMessageContext()
        gotoMail(view, request)
    }

    protected String gotoMail(NoteView view, HttpServletRequest request) {
        request.session.setAttribute(MailMessageContext.NAME, view.mailMessageContext)
        redirect(request, '/mail/mailEditor/forward')
    }
}
