/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 18, 2014 
 * 
 */
package com.osserp.core.service.contacts;


import java.util.List;

import com.osserp.common.OptionsCache;
import com.osserp.common.mail.EmailAddress;
import com.osserp.common.util.DateFormatter;
import com.osserp.common.util.StringUtil;
import com.osserp.core.Options;
import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.ContactExportManager;
import com.osserp.core.contacts.ContactSearch;
import com.osserp.core.contacts.Phone;
import com.osserp.core.employees.Employee;
import com.osserp.core.employees.EmployeeGroup;
import com.osserp.core.employees.EmployeeSearch;
import com.osserp.core.service.impl.AbstractService;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */

public class DefaultContactExportManager extends AbstractService implements ContactExportManager {
    
    private OptionsCache options;
    private ContactSearch contactSearch;
    private EmployeeSearch employeeSearch;


    protected DefaultContactExportManager(OptionsCache options, ContactSearch contactSearch, EmployeeSearch employeeSearch) {
        super();
        this.options = options;
        this.contactSearch = contactSearch;
        this.employeeSearch = employeeSearch;
    }

    

    /* (non-Javadoc)
     * @see com.osserp.core.contacts.ContactExportManager#find(java.lang.Long)
     */
    public List<Contact> find(Long group) {
        if (Contact.EMPLOYEE.equals(group)) {
            return new java.util.ArrayList(employeeSearch.findAllActive());        
        }
        return contactSearch.findAllByGroup(group);
    }



    /* (non-Javadoc)
     * @see com.osserp.core.contacts.ContactExportManager#createSpreadsheet(java.util.List, java.lang.String)
     */
    public String createSpreadsheet(List<Contact> contacts, String separator) {
        StringBuffer buffer = new StringBuffer();
        for (int i = 0, j = contacts.size(); i < j; i++) {
            Contact next = contacts.get(i);
            if (next instanceof Employee) {
                buffer.append(createEmployeeRow((Employee) next, separator));
            } else {
                buffer.append(createContactRow(contacts.get(i), separator));
            }
        }
        return buffer.toString();
    }


    private String createEmployeeRow(Employee e, String separator) {
        StringBuilder buffer = new StringBuilder();
        /* Anrede */
        if (e.getSalutation() == null) {
            buffer.append(separator);
        } else {
            buffer.append(getCleanedUp(e.getSalutation().getName(), separator)).append(separator);
        }
        /* Vorname */
        buffer.append(getCleanedUp(e.getFirstName(), separator)).append(separator);
        /* Weitere Vornamen */
        buffer.append(separator);
        /* Nachname */
        buffer.append(getCleanedUp(e.getLastName(), separator)).append(separator);
        /* Suffix */
        buffer.append(separator);
        /* Firma */
        buffer.append(separator);
        /* Abteilung */
        EmployeeGroup group = e.getDefaultGroup();
        if (group == null) {
            buffer.append(separator);
        } else {
            buffer.append(getCleanedUp(group.getName(), separator)).append(separator);
        }
        /* Position */
        buffer.append(getCleanedUp(e.getRole(), separator)).append(separator);
        /* Strasse geschaeftlich */
        if (e.getAddress() == null || e.getAddress().getStreet() == null) {
            buffer.append(separator);
        } else {
            buffer.append(getCleanedUp(e.getAddress().getStreet(), separator)).append(separator);
        }
        /* Strasse geschaeftlich 2 */
        buffer.append(separator);
        /* Strasse geschaeftlich 3 */
        buffer.append(separator);
        /* Ort geschaeftlich */
        if (e.getAddress() == null || e.getAddress().getCity() == null) {
            buffer.append(separator);
        } else {
            buffer.append(getCleanedUp(e.getAddress().getCity(), separator)).append(separator);
        }
        /* Region geschaeftlich */
        buffer.append(separator);
        /* Postleitzahl geschaeftlich */
        if (e.getAddress() == null || e.getAddress().getZipcode() == null) {
            buffer.append(separator);
        } else {
            buffer.append(getCleanedUp(e.getAddress().getZipcode(), separator)).append(separator);
        }
        /* Land geschaeftlich */
        if (e.getAddress() == null || e.getAddress().getCountry() == null) {
            buffer.append(separator);
        } else {
            buffer.append(getCountryName(e.getAddress().getCountry())).append(separator);
        }
        /* Strasse privat */
        buffer.append(separator);
        /* Strasse privat 2 */
        buffer.append(separator);
        /* Strasse privat 3 */
        buffer.append(separator);
        /* Ort privat */
        buffer.append(separator);
        /* Region privat */
        buffer.append(separator);
        /* Postleitzahl privat */
        buffer.append(separator);
        /* Land privat */
        buffer.append(separator);
        /* Weitere Strasse */
        buffer.append(separator);
        /* Weitere Strasse 2 */
        buffer.append(separator);
        /* Weitere Strasse 3 */
        buffer.append(separator);
        /* Weiterer Ort */
        buffer.append(separator);
        /* Weitere Region */
        buffer.append(separator);
        /* Weitere Postleitzahl */
        buffer.append(separator);
        /* Weiteres Land */
        buffer.append(separator);
        /* Telefon Assistent */
        buffer.append(separator);
        /* Fax geschaeftlich */
        buffer.append(getCleanedUp(getBusinessFax(e), separator)).append(separator);
        /* Telefon geschaeftlich */
        String bphone = (e.getInternalPhone() != null ? e.getInternalPhone().getFormattedOutlookNumber() : null);
        buffer.append(getCleanedUp(bphone, separator)).append(separator);
        /* Telefon geschaeftlich 2 */
        buffer.append(separator);
        /* Rueckmeldung */
        buffer.append(separator);
        /* Autotelefon */
        buffer.append(separator);
        /* Telefon Firma */
        buffer.append(separator);
        /* Fax privat */
        String pfax = getPrivateFax(e);
        if (pfax == null) {
            buffer.append(separator);
        } else {
            buffer.append(pfax).append(separator);
        }
        /* Telefon privat */
        buffer.append(getCleanedUp(getPrivatePhone(e), separator)).append(separator);
        /* Telefon privat 2 */
        buffer.append(separator);
        /* ISDN */
        buffer.append(separator);
        /* Mobiltelefon */
        String bmobile = getBusinessMobile(e);
        if (bmobile == null) {
            bmobile = getPrivateMobile(e);
        }
        if (bmobile == null) {
            buffer.append(separator);
        } else {
            buffer.append(getCleanedUp(bmobile, separator)).append(separator);
        }
        /* Weiteres Fax */
        buffer.append(separator);
        /* Weiteres Telefon */
        buffer.append(separator);
        /* Pager */
        buffer.append(separator);
        /* Haupttelefon */
        buffer.append(separator);
        /* Mobiltelefon 2 */
        String pmobile = null;
        if (bmobile != null && e.getMobiles().size() > 1) {
            for (int i = 0, j = e.getMobiles().size(); i < j; i++) {
                String next = e.getMobiles().get(i).getFormattedOutlookNumber();
                if (!next.equals(bmobile)) {
                    pmobile = next;
                    break;
                }
            }
        }
        if (pmobile == null) {
            buffer.append(separator);
        } else {
            buffer.append(getCleanedUp(pmobile, separator)).append(separator);
        }
        /* Telefon fuer Hoerbehinderte */
        buffer.append(separator);
        /* Telex */
        buffer.append(separator);
        /* Abrechnungsinformation */
        buffer.append(separator);
        /* Benutzer1 */
        buffer.append(separator);
        /* Benutzer2 */
        buffer.append(separator);
        /* Benutzer3 */
        buffer.append(separator);
        /* Benutzer4 */
        buffer.append(separator);
        /* Beruf */
        buffer.append(separator);
        /* Buero */
        buffer.append(separator);
        /* E-Mail-Adresse */
        String email = getBusinessEmail(e);
        if (email == null) {
            email = getPrivateEmail(e);
        }
        if (email == null) {
            buffer.append(separator);
        } else {
            buffer.append(getCleanedUp(email, separator)).append(separator);
        }
        /* E-Mail-Typ */
        if (email == null) {
            buffer.append(separator);
        } else {
            buffer.append("SMTP").append(separator);
        }
        /* E-Mail: Angezeigter Name */
        if (email == null) {
            buffer.append(separator);
        } else {
            buffer.append(getCleanedUp(e.getName(), separator)).append(separator);
        }
        /* E-Mail 2: Adresse */
        String pemail = null;
        if (email != null && e.getEmails().size() > 1) {
            for (int i = 0, j = e.getEmails().size(); i < j; i++) {
                String next = e.getEmails().get(i).getEmail();
                if (!next.equals(email)) {
                    pemail = next;
                    break;
                }
            }
        }
        if (pemail == null) {
            buffer.append(separator);
        } else {
            buffer.append(getCleanedUp(pemail, separator)).append(separator);
        }
        /* E-Mail 2: Typ */
        if (pemail == null) {
            buffer.append(separator);
        } else {
            buffer.append("SMTP").append(separator);
        }
        /* E-Mail 2: Angezeigter Name */
        if (pemail == null) {
            buffer.append(separator);
        } else {
            buffer.append(getCleanedUp(e.getName(), separator)).append(separator);
        }
        /* E-Mail 3: Adresse */
        buffer.append(separator);
        /* E-Mail 3: Typ */
        buffer.append(separator);
        /* E-Mail 3: Angezeigter Name */
        buffer.append(separator);
        /* Empfohlen von */
        buffer.append(separator);
        /* Geburtstag */
        if (e.getBirthDate() == null) {
            buffer.append(separator);
        } else {
            buffer.append(DateFormatter.getDate(e.getBirthDate())).append(separator);
        }
        /* Geschlecht */
        buffer.append(separator);
        /* Hobby */
        buffer.append(separator);
        /* Initialen */
        if (e.getInitials() == null) {
            buffer.append(separator);
        } else {
            buffer.append(getCleanedUp(e.getInitials(), separator)).append(separator);
        }
        /* Internet-Frei/Gebucht */
        buffer.append(separator);
        /* Jahrestag */
        buffer.append(separator);
        /* Kategorien */
        buffer.append(separator);
        /* Kinder */
        buffer.append(separator);
        /* Konto */
        buffer.append(separator);
        /* Name Assistent */
        buffer.append(separator);
        /* Name des/der Vorgesetzten */
        buffer.append(separator);
        /* Notizen */
        buffer.append(separator);
        /* Organisations-Nr. */
        buffer.append(separator);
        /* Ort */
        buffer.append(separator);
        /* Partner */
        buffer.append(separator);
        /* Postfach geschaeftlich */
        buffer.append(separator);
        /* Postfach privat */
        buffer.append(separator);
        /* Prioritaet */
        buffer.append(separator);
        /* Privat */
        buffer.append(separator);
        /* Regierungs-Nr. */
        buffer.append(separator);
        /* Reisekilometer */
        buffer.append(separator);
        /* Sprache */
        buffer.append(separator);
        /* Stichwoerter */
        buffer.append(separator);
        /* Vertraulichkeit */
        buffer.append(separator);
        /* Verzeichnisserver */
        buffer.append(separator);
        /* Webseite */
        buffer.append(separator);
        /* Weiteres Postfach */
        // end
        buffer.append("\r\n");
        return buffer.toString();
    }

    private String createContactRow(Contact e, String separator) {
        StringBuilder buffer = new StringBuilder();
        /* Anrede */
        if (e.getSalutation() == null) {
            buffer.append(separator);
        } else {
            buffer.append(e.getSalutation().getName()).append(separator);
        }
        /* Vorname */
        if (e.getFirstName() == null) {
            buffer.append(separator);
        } else {
            buffer.append(e.getFirstName()).append(separator);
        }
        /* Weitere Vornamen */
        buffer.append(separator);
        /* Nachname */
        if (e.getLastName() == null) {
            buffer.append(separator);
        } else {
            buffer.append(e.getLastName()).append(separator);
        }
        /* Suffix */
        buffer.append(separator);
        /* Firma */
        buffer.append(separator);
        /* Abteilung */
        String group = e.getSection();
        if (group == null) {
            buffer.append(separator);
        } else {
            buffer.append(group).append(separator);
        }
        /* Position */
        if (e.getPosition() == null) {
            buffer.append(separator);
        } else {
            buffer.append(e.getPosition()).append(separator);
        }
        /* Strasse geschaeftlich */
        if (e.getAddress() == null || e.getAddress().getStreet() == null) {
            buffer.append(separator);
        } else {
            buffer.append(e.getAddress().getStreet()).append(separator);
        }
        /* Strasse geschaeftlich 2 */
        buffer.append(separator);
        /* Strasse geschaeftlich 3 */
        buffer.append(separator);
        /* Ort geschaeftlich */
        if (e.getAddress() == null || e.getAddress().getCity() == null) {
            buffer.append(separator);
        } else {
            buffer.append(e.getAddress().getCity()).append(separator);
        }
        /* Region geschaeftlich */
        buffer.append(separator);
        /* Postleitzahl geschaeftlich */
        if (e.getAddress() == null || e.getAddress().getZipcode() == null) {
            buffer.append(separator);
        } else {
            buffer.append(e.getAddress().getZipcode()).append(separator);
        }
        /* Land geschaeftlich */
        if (e.getAddress() == null || e.getAddress().getCountry() == null) {
            buffer.append(separator);
        } else {
            buffer.append(getCountryName(e.getAddress().getCountry())).append(separator);
        }
        /* Strasse privat */
        buffer.append(separator);
        /* Strasse privat 2 */
        buffer.append(separator);
        /* Strasse privat 3 */
        buffer.append(separator);
        /* Ort privat */
        buffer.append(separator);
        /* Region privat */
        buffer.append(separator);
        /* Postleitzahl privat */
        buffer.append(separator);
        /* Land privat */
        buffer.append(separator);
        /* Weitere Strasse */
        buffer.append(separator);
        /* Weitere Strasse 2 */
        buffer.append(separator);
        /* Weitere Strasse 3 */
        buffer.append(separator);
        /* Weiterer Ort */
        buffer.append(separator);
        /* Weitere Region */
        buffer.append(separator);
        /* Weitere Postleitzahl */
        buffer.append(separator);
        /* Weiteres Land */
        buffer.append(separator);
        /* Telefon Assistent */
        buffer.append(separator);
        /* Fax geschaeftlich */
        String bfax = getBusinessFax(e);
        if (bfax == null) {
            buffer.append(separator);
        } else {
            buffer.append(bfax).append(separator);
        }
        /* Telefon geschaeftlich */
        String bphone = getBusinessPhone(e);
        if (bphone == null) {
            buffer.append(separator);
        } else {
            buffer.append(bphone).append(separator);
        }
        /* Telefon geschaeftlich 2 */
        buffer.append(separator);
        /* Rueckmeldung */
        buffer.append(separator);
        /* Autotelefon */
        buffer.append(separator);
        /* Telefon Firma */
        buffer.append(separator);
        /* Fax privat */
        String pfax = getPrivateFax(e);
        if (pfax == null) {
            buffer.append(separator);
        } else {
            buffer.append(pfax).append(separator);
        }
        /* Telefon privat */
        String pphone = getPrivatePhone(e);
        if (pphone == null) {
            buffer.append(separator);
        } else {
            buffer.append(pphone).append(separator);
        }
        /* Telefon privat 2 */
        buffer.append(separator);
        /* ISDN */
        buffer.append(separator);
        /* Mobiltelefon */
        String bmobile = getBusinessMobile(e);
        if (bmobile == null) {
            bmobile = getPrivateMobile(e);
        }
        if (bmobile == null) {
            buffer.append(separator);
        } else {
            buffer.append(bmobile).append(separator);
        }
        /* Weiteres Fax */
        buffer.append(separator);
        /* Weiteres Telefon */
        buffer.append(separator);
        /* Pager */
        buffer.append(separator);
        /* Haupttelefon */
        buffer.append(separator);
        /* Mobiltelefon 2 */
        String pmobile = null;
        if (bmobile != null && e.getMobiles().size() > 1) {
            for (int i = 0, j = e.getMobiles().size(); i < j; i++) {
                String next = e.getMobiles().get(i).getFormattedOutlookNumber();
                if (!next.equals(bmobile)) {
                    pmobile = next;
                    break;
                }
            }
        }
        if (pmobile == null) {
            buffer.append(separator);
        } else {
            buffer.append(pmobile).append(separator);
        }
        /* Telefon fuer Hoerbehinderte */
        buffer.append(separator);
        /* Telex */
        buffer.append(separator);
        /* Abrechnungsinformation */
        buffer.append(separator);
        /* Benutzer1 */
        buffer.append(separator);
        /* Benutzer2 */
        buffer.append(separator);
        /* Benutzer3 */
        buffer.append(separator);
        /* Benutzer4 */
        buffer.append(separator);
        /* Beruf */
        buffer.append(separator);
        /* Buero */
        buffer.append(separator);
        /* E-Mail-Adresse */
        String email = getBusinessEmail(e);
        if (email == null) {
            email = getPrivateEmail(e);
        }
        if (email == null) {
            buffer.append(separator);
        } else {
            buffer.append(email).append(separator);
        }
        /* E-Mail-Typ */
        if (email == null) {
            buffer.append(separator);
        } else {
            buffer.append("SMTP").append(separator);
        }
        /* E-Mail: Angezeigter Name */
        if (email == null) {
            buffer.append(separator);
        } else {
            buffer.append(e.getName()).append(separator);
        }
        /* E-Mail 2: Adresse */
        String pemail = null;
        if (email != null && e.getEmails().size() > 1) {
            for (int i = 0, j = e.getEmails().size(); i < j; i++) {
                String next = e.getEmails().get(i).getEmail();
                if (!next.equals(email)) {
                    pemail = next;
                    break;
                }
            }
        }
        if (pemail == null) {
            buffer.append(separator);
        } else {
            buffer.append(pemail).append(separator);
        }
        /* E-Mail 2: Typ */
        if (pemail == null) {
            buffer.append(separator);
        } else {
            buffer.append("SMTP").append(separator);
        }
        /* E-Mail 2: Angezeigter Name */
        if (pemail == null) {
            buffer.append(separator);
        } else {
            buffer.append(e.getName()).append(separator);
        }
        /* E-Mail 3: Adresse */
        buffer.append(separator);
        /* E-Mail 3: Typ */
        buffer.append(separator);
        /* E-Mail 3: Angezeigter Name */
        buffer.append(separator);
        /* Empfohlen von */
        buffer.append(separator);
        /* Geburtstag */
        if (e.getBirthDate() == null) {
            buffer.append(separator);
        } else {
            buffer.append(DateFormatter.getDate(e.getBirthDate())).append(separator);
        }
        /* Geschlecht */
        buffer.append(separator);
        /* Hobby */
        buffer.append(separator);
        /* Initialen */
        buffer.append(separator);
        /* Internet-Frei/Gebucht */
        buffer.append(separator);
        /* Jahrestag */
        buffer.append(separator);
        /* Kategorien */
        buffer.append(separator);
        /* Kinder */
        buffer.append(separator);
        /* Konto */
        buffer.append(separator);
        /* Name Assistent */
        buffer.append(separator);
        /* Name des/der Vorgesetzten */
        buffer.append(separator);
        /* Notizen */
        buffer.append(separator);
        /* Organisations-Nr. */
        buffer.append(separator);
        /* Ort */
        buffer.append(separator);
        /* Partner */
        buffer.append(separator);
        /* Postfach geschaeftlich */
        buffer.append(separator);
        /* Postfach privat */
        buffer.append(separator);
        /* Prioritaet */
        buffer.append(separator);
        /* Privat */
        buffer.append(separator);
        /* Regierungs-Nr. */
        buffer.append(separator);
        /* Reisekilometer */
        buffer.append(separator);
        /* Sprache */
        buffer.append(separator);
        /* Stichwoerter */
        buffer.append(separator);
        /* Vertraulichkeit */
        buffer.append(separator);
        /* Verzeichnisserver */
        buffer.append(separator);
        /* Webseite */
        buffer.append(separator);
        /* Weiteres Postfach */
        //buffer.append(separator);
        buffer.append("\r\n");
        return buffer.toString();
    }

    private String getBusinessEmail(Contact e) {
        return getEmail(e, EmailAddress.BUSINESS);
    }

    private String getPrivateEmail(Contact e) {
        return getEmail(e, EmailAddress.PRIVATE);
    }

    private String getBusinessFax(Contact e) {
        return getBusiness(e.getFaxNumbers());
    }

    private String getPrivateFax(Contact e) {
        return getPrivate(e.getFaxNumbers());
    }

    private String getBusinessMobile(Contact e) {
        return getBusiness(e.getMobiles());
    }

    private String getPrivateMobile(Contact e) {
        return getPrivate(e.getMobiles());
    }

    private String getBusinessPhone(Contact e) {
        return getBusiness(e.getPhoneNumbers());
    }

    private String getPrivatePhone(Contact e) {
        return getPrivate(e.getPhoneNumbers());
    }

    private String getCountryName(Long id) {
        return options.getMappedValue(Options.COUNTRY_NAMES, id);
    }

    private String getCleanedUp(String text, String separator) {
        if (isNotSet(text)) {
            return "";
        }
        StringBuilder b = new StringBuilder(text);
        if (b.indexOf(separator) > -1) {
            StringUtil.remove(b, separator);
        }
        return b.toString();
    }

    private String getBusiness(List<Phone> list) {
        return getPhoneByType(list, Phone.BUSINESS);
    }

    private String getPrivate(List<Phone> list) {
        return getPhoneByType(list, Phone.PRIVATE);
    }

    private String getPhoneByType(List<Phone> list, Long type) {
        Phone phone = null;
        if (list != null && !list.isEmpty()) {
            for (int i = 0, j = list.size(); i < j; i++) {
                Phone next = list.get(i);
                if (type.equals(next.getType())) {
                    if (phone != null) {
                        if (!phone.isPrimary()) {
                            if (next.isPrimary()) {
                                phone = next;
                                break;
                            }
                        }
                    } else {
                        phone = next;
                    }
                }
            }
        }
        return (phone != null ? phone.getFormattedOutlookNumber() : null);
    }

    private String getEmail(Contact e, Long type) {
        EmailAddress address = null;
        for (int i = 0, j = e.getEmails().size(); i < j; i++) {
            EmailAddress next = e.getEmails().get(i);
            if (type.equals(next.getType())) {
                if (address != null) {
                    if (!address.isPrimary()) {
                        if (next.isPrimary()) {
                            address = next;
                            break;
                        }
                    }
                } else {
                    address = next;
                }
            }
        }
        return (address != null ? address.getEmail() : null);
    }
    
}
