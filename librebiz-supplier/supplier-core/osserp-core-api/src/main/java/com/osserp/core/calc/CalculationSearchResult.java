/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 27 Oct 2011 17:38:41 
 * 
 */
package com.osserp.core.calc;

import java.util.Date;

import com.osserp.common.beans.OptionImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class CalculationSearchResult extends OptionImpl {

    private Integer calculationNumber = 0;
    private Double netAmount = 0d;
    private Double capacity = 0d;
    private boolean planAvailable = false;
    private boolean initial = false;
    private boolean historical = false;

    protected CalculationSearchResult() {
        super();
    }

    /**
     * Creates a new CalculationSearchResult instance setting all values
     * @param id
     * @param name
     * @param reference
     * @param created
     * @param createdBy
     * @param calculationNumber
     * @param netAmount
     * @param capacity
     * @param planAvailable
     * @param initial
     */
    public CalculationSearchResult(
            Long id,
            String name,
            Long reference,
            Date created,
            Long createdBy,
            Integer calculationNumber,
            Double netAmount,
            Double capacity,
            boolean planAvailable,
            boolean initial,
            boolean historical) {
        super(id, reference, name, created, createdBy);
        this.calculationNumber = calculationNumber;
        this.netAmount = netAmount;
        this.capacity = capacity;
        this.planAvailable = planAvailable;
        this.initial = initial;
        this.historical = historical;
    }

    public Integer getCalculationNumber() {
        return calculationNumber;
    }

    void setCalculationNumber(Integer calculationNumber) {
        this.calculationNumber = calculationNumber;
    }

    public Double getNetAmount() {
        return netAmount;
    }

    void setNetAmount(Double netAmount) {
        this.netAmount = netAmount;
    }

    public Double getCapacity() {
        return capacity;
    }

    void setCapacity(Double capacity) {
        this.capacity = capacity;
    }

    public boolean isPlanAvailable() {
        return planAvailable;
    }

    void setPlanAvailable(boolean planAvailable) {
        this.planAvailable = planAvailable;
    }

    public boolean isInitial() {
        return initial;
    }

    void setInitial(boolean initial) {
        this.initial = initial;
    }

    public boolean isHistorical() {
        return historical;
    }

    void setHistorical(boolean historical) {
        this.historical = historical;
    }
}
