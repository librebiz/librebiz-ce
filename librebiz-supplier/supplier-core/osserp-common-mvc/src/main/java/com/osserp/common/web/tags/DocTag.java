/**
*
* Copyright (C) 2006 The original author or authors.
*
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
* 
* Created on 22 Sep 2006 
* 
*/
package com.osserp.common.web.tags;

import javax.servlet.jsp.JspException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.dms.DmsDocument;

/**
 * 
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class DocTag extends AbstractLinkTag {
    private static Logger log = LoggerFactory.getLogger(DocTag.class.getName());

    private DmsDocument obj = null;
    private boolean noblank = true;
    
    public DmsDocument getObj() {
        return obj;
    }

    public void setObj(DmsDocument obj) {
        this.obj = obj;
    }
    
    public boolean isNoblank() {
        return noblank;
    }

    public void setNoblank(boolean noblank) {
        this.noblank = noblank;
    }

    @Override
    public String getStartTag() throws JspException {
        StringBuilder buffer = new StringBuilder(256);
        buffer
            .append(LINK_START)
            .append(getDocumentUrl());
        if (!noblank) {
            buffer.append("\" onclick=\"target='_blank';\"");
        } else {
            buffer.append("\"");
        }
        appendEnd(buffer);
        return buffer.toString();
    }

   protected String getDocumentUrl() {
       try {
           if (obj != null) {
               return createUrl(obj.getUrl());
           }
       } catch (Exception e) {
           // getUrl may throw an exception if filename
           // or other required property is null.
           log.warn("getDocumentUrl() ignoring exception [message=" + e.getMessage() + "]");
       }
       return "";
   }
}
