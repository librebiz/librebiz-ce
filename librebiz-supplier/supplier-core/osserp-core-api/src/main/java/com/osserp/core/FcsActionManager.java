/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 29, 2007 7:19:29 AM 
 * 
 */
package com.osserp.core;

import java.util.List;
import java.util.Map;

import com.osserp.common.ClientException;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface FcsActionManager {

    /**
     * Provides the flow control action values of the action specified by id
     * @param id of the requested action
     * @return requested action values
     */
    FcsAction findById(Long id);

    /**
     * Provides an ordered list of all flow control action value objects for config purpose.
     * @param typeId of the actions
     * @return ordered flow control action list
     */
    List<FcsAction> findByType(Long typeId);

    /**
     * Provides all flow control actions mapped by id
     * @return flow control action map
     */
    Map<Long, FcsAction> createMap();

    /**
     * Updates a flow control action on the backend
     * @param flowControlAction
     */
    void update(FcsAction flowControlAction) throws ClientException;
}
