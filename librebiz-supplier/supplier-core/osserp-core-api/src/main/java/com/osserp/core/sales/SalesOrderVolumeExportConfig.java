/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 4, 2008 11:10:55 AM 
 * 
 */
package com.osserp.core.sales;

import java.util.Date;

import com.osserp.common.Option;

import com.osserp.core.customers.Customer;
import com.osserp.core.products.ProductSelectionConfig;
import com.osserp.core.system.SystemCompany;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface SalesOrderVolumeExportConfig extends Option {

    /**
     * Provides the invoicing company
     * @return the company
     */
    SystemCompany getCompany();

    /**
     * Provides the recipient of the invoice
     * @return the customer
     */
    Customer getCustomer();

    /**
     * Provides the originating company of the exportable sales order
     * @return company reference of the sales order to lookup for
     */
    SystemCompany getExportCompany();

    /**
     * Provides the recipient of the exportable sales order if required
     * @return customer or null if all orders of the export company should be included
     */
    Customer getExportCustomer();

    /**
     * Indicates that reference to associated order is provided by sales property
     * @return true if reference to sales is provided by sales property, false if reference property
     */
    boolean getSalesReferenceBySales();

    /**
     * Indicates that delivery notes should be included
     * @return include billed orders only
     */
    boolean isIncludeByDeliveryNote();

    /**
     * Indicates that sales price should be calculated instead of using prices by order.
     * @return true if sales price should be calculated
     */
    boolean isCalculateSalesPrice();

    /**
     * Indicates that volume export is currently running
     * @return running
     */
    boolean isRunning();

    /**
     * Sets running flag
     * @param running
     */
    void setRunning(boolean running);

    /**
     * Provides the class name of the responsible export provider
     * @return export provider class name
     */
    String getExportProviderName();

    /**
     * Provides the class name of the responsible volume processor
     * @return volume processor class name
     */
    String getVolumeProcessorName();

    /**
     * Provides an optional product selection config
     * @return product selection config
     */
    ProductSelectionConfig getProductSelectionConfig();

    /**
     * Provides the required permissions for display volume invoices of this config
     * @return display permissions
     */
    String getDisplayPermissions();

    /**
     * Provides the required permissions for creating volume invoices of this config
     * @return create permissions
     */
    String getCreatePermissions();

    /**
     * Provides the date until records should be included. This value should be null by default to include all open records.
     * @return export end or null for default operation
     */
    Date getExportEnd();

    /**
     * Provides the date when config starts
     * @return export start
     */
    Date getExportStart();

    /**
     * Indicates that exports with this config should no longer be created
     * @return eol
     */
    boolean isEol();
}
