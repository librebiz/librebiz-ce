/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.osserp.gui.telephones

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.util.DateUtil
import com.osserp.core.telephone.TelephoneCallManager
import com.osserp.core.telephone.TelephoneConfiguration
import com.osserp.core.telephone.TelephoneConfigurationManager
import com.osserp.core.telephone.TelephoneSystemManager
import com.osserp.core.telephone.TelephoneSystem
import com.osserp.core.users.Permissions
import com.osserp.groovy.web.MenuItem
import com.osserp.gui.CoreView

/**
 * 
 * @author so <so@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public class TelephoneCallView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(TelephoneCallView.class.getName())

    private static final String TELEPHONE_PERMISSIONS = Permissions.TELEPHONE_EXCHANGE_CALLS+','+Permissions.TELEPHONE_EXCHANGE_EDIT+','+Permissions.TELEPHONE_SYSTEM_ADMIN
    TelephoneConfiguration currentConfig
    Date beginDate
    Date endDate
    List<TelephoneConfiguration> telephoneConfigurations = []

    TelephoneCallView() {
        dependencies = ['portalView']
        beginDate = DateUtil.subtractDays(DateUtil.resetTime(DateUtil.getCurrentDate()), 5)
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)

        this.generateMyNavigation()
        telephoneConfigurations = []
        List<TelephoneConfiguration> tmplist = []
        TelephoneConfigurationManager tcmanager = getService(TelephoneConfigurationManager.class.getName())
        if (isPermissionGrant(TELEPHONE_PERMISSIONS)) {
            Long id = (domainEmployee?.defaultRoleConfig?.branch) ? domainEmployee.defaultRoleConfig.branch.id : null
            if (id) {
                TelephoneSystemManager manager = getService(TelephoneSystemManager.class.getName())
                TelephoneSystem telephoneSystem = manager.findByBranch(id)
                if (telephoneSystem?.branchId) {
                    tmplist = tcmanager.search(telephoneSystem.branchId, false, true)
                }
            }
        }
        TelephoneConfiguration config = tcmanager.findByEmployeeId(domainEmployee?.id)
        if (config) {
            telephoneConfigurations.add(config)
            if (currentConfig == null) {
                currentConfig = config
            }
        }
        for (TelephoneConfiguration tmp in tmplist) {
            telephoneConfigurations.add(tmp)
            if (currentConfig == null) {
                currentConfig = tmp
            }
        }
    }

    void generateMyNavigation() {
        nav.currentNavigation = [
            exitLink,
            new MenuItem(link: "${context.fullPath}/telephones/telephoneCall/reload", title: 'refresh', icon: 'reloadIcon', newWindow: false, ajaxPopup: false, execAction: false)
        ]
        if (isPermissionGrant(Permissions.TELEPHONE_SYSTEM_ADMIN+','+Permissions.TELEPHONE_CONFIGURATION_OWN_EDIT)) {
            nav.currentNavigation.add(new MenuItem(link: "${context.fullPath}/telephones/telephoneConfigurationPopup/load?employeeId=${user.employee.id}", title: 'telephoneConfiguration', icon: 'configureIcon', ajaxPopup: true, ajaxPopupName: 'telephone_configuration'))
        }
        if (env.portalView?.clickToCallEnabledWizard) {
            nav.currentNavigation.add(new MenuItem(link: (env.portalView.clickToCallEnabled) ? "${context.fullPath}/users/clickToCall/disableClickToCall" : "${context.fullPath}/users/clickToCall/enableClickToCall", title: (env.portalView.clickToCallEnabled) ? 'deactivate' : 'activate', icon: (env.portalView.clickToCallEnabled) ? 'telephoneErrorIcon' : 'telephoneLinkIcon', newWindow: false, ajaxPopup: false, execAction: true, ajaxPopupName: 'click_to_call'))
        }
        nav.currentNavigation.add(homeLink)
    }

    @Override
    void reload() {
        TelephoneCallManager manager = getService(TelephoneCallManager.class.getName())
        if (currentConfig) {
            for (TelephoneConfiguration tmp in telephoneConfigurations) {
                if (tmp.id == currentConfig.id) {
                    list = (beginDate != null || endDate != null) ? manager.find(currentConfig.uid, beginDate, endDate) : manager.find(currentConfig.uid)
                    break
                } else {
                    list = []
                }
            }
        } else {
            list = []
            throw new ClientException(ErrorCode.CONFIG_MISSING)
        }
        if (actualSortKey != null) {
            String[] keys = actualSortKey.split("\\.")
            def key = keys[0]
            if (keys.length > 1) {
                def childKey = keys[1]
                list.sort{ a, b -> a?."$key"?."$childKey" <=> b?."$key"?."$childKey" }
            } else {
                list.sort{ a, b -> a?."$key" <=> b?."$key" }
            }
        }
    }

    void search() {
        logger.debug('search: invoked')
        Long id = form.getLong('value')
        if (id) {
            TelephoneConfigurationManager manager = getService(TelephoneConfigurationManager.class.getName())
            currentConfig = manager.find(id)
        } else {
            currentConfig = null
        }
        beginDate = form.getDate('beginDate')
        endDate = form.getDate('endDate')
        TelephoneCallManager tcmanager = getService(TelephoneCallManager.class.getName())
        if (currentConfig) {
            for (TelephoneConfiguration tmp in telephoneConfigurations) {
                if (tmp.id == currentConfig.id) {
                    list = (beginDate != null || endDate != null) ? tcmanager.find(currentConfig.uid, beginDate, endDate) : tcmanager.find(currentConfig.uid)
                    break
                } else {
                    list = []
                }
            }
        } else {
            list = []
            throw new ClientException(ErrorCode.CONFIG_MISSING)
        }
    }
}
