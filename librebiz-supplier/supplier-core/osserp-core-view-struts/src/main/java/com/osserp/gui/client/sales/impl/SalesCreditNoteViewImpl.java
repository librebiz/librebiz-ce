/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 29, 2006 12:44:06 PM 
 * 
 */
package com.osserp.gui.client.sales.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;
import com.osserp.common.web.ViewManager;
import com.osserp.common.web.ViewName;

import com.osserp.core.finance.BookingType;
import com.osserp.core.finance.CreditNote;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.ReceivablesManager;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.finance.RecordManager;
import com.osserp.core.sales.SalesCreditNoteManager;
import com.osserp.core.sales.SalesInvoice;
import com.osserp.core.sales.SalesInvoiceManager;
import com.osserp.core.users.DomainUser;

import com.osserp.gui.client.records.impl.AbstractRecordView;
import com.osserp.gui.client.sales.SalesCreditNoteView;
import com.osserp.gui.client.sales.SalesInvoiceView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("salesCreditNoteView")
public class SalesCreditNoteViewImpl extends AbstractRecordView implements SalesCreditNoteView {
    private static Logger log = LoggerFactory.getLogger(SalesCreditNoteViewImpl.class.getName());

    private SalesInvoice invoice = null;
    private SalesInvoiceView invoiceView = null;
    private List<RecordDisplay> relationSuggestions = new ArrayList<>();

    protected SalesCreditNoteViewImpl() {
        super();
    }

    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        super.init(request);
        invoiceView = (SalesInvoiceView) ViewManager.fetchView(request.getSession(), SalesInvoiceView.class);
        if (invoiceView != null) {
            invoice = (SalesInvoice) invoiceView.getRecord();
            List<CreditNote> existing = invoice.getCreditNotes();
            setList(existing);
            if (!existing.isEmpty() && existing.size() == 1) {
                setRecord(existing.get(0));
            }
        }
    }

    public void create() throws ClientException, PermissionException {
        if (invoiceView == null) {
            log.warn("create() invoked while no invoice view bound!");
            throw new ActionException();
        }
        SalesCreditNoteManager manager = getSalesCreditNoteManager();
        BookingType selectedBookingType = null;
        if (getForm() != null) {
            Long bookingTypeId = getForm().getLong(Form.BOOKING_TYPE);
            if (bookingTypeId != null) {
                selectedBookingType = manager.getBookingType(bookingTypeId);

                if (log.isDebugEnabled()) {
                    if (selectedBookingType != null) {
                        log.debug("create() found selected booking type [id="
                                + selectedBookingType.getId()
                                + ", name="
                                + selectedBookingType.getName()
                                + "]");
                    } else {
                        log.debug("create() booking type selection missing!");
                    }
                }
            }
        }
        CreditNote note = manager.create(
                getDomainUser().getEmployee(),
                (Invoice) invoiceView.getRecord(),
                (selectedBookingType != null ? selectedBookingType : manager.getDefaultBookingType()),
                false);
        setRecord(note);
    }

    @Override
    public void save(Form form) throws ClientException, PermissionException {
        SalesCreditNoteManager manager = getSalesCreditNoteManager();
        Record record = getRecord();
        manager.update(
                record,
                form.getLong("signatureLeft"),
                form.getLong("signatureRight"),
                form.getLong("personId"),
                form.getString("note"),
                form.getBoolean("taxFree"),
                form.getLong("taxFreeId"),
                form.getDouble("taxRate"),
                form.getDouble("reducedTaxRate"),
                form.getLong("currency"),
                form.getString("language"),
                form.getLong("branch"),
                form.getLong("shippingId") != null ? form.getLong("shippingId") : record.getShippingId(),
                form.getString("customHeader"),
                form.getBoolean("printComplimentaryClose"),
                form.getBoolean("printProjectmanager"),
                form.getBoolean("printSalesperson"),
                form.getBoolean("printBusinessCaseId"),
                form.getBoolean("printBusinessCaseInfo"),
                form.getBoolean("printRecordDate"),
                form.getBoolean("printRecordDateByStatus"),
                form.getBoolean("printPaymentTarget"),
                form.getBoolean("printConfirmationPlaceholder"),
                form.getBoolean("printCoverLetter"));
        refresh();
    }

    public SalesInvoiceView getInvoiceView() {
        return invoiceView;
    }

    public void changeSalesCommissionStatus() throws PermissionException {
        getDomainUser().checkPermission(new String[] { "executive", "executive_sales" });
        SalesCreditNoteManager manager = getSalesCreditNoteManager();
        CreditNote record = (CreditNote) getRecord();
        manager.changeSalesCommissionStatus(record);
    }

    public boolean isDeliveryRequiredFlagChangeable() {
        CreditNote record = (CreditNote) getRecord();
        if (record.isUnchangeable()) {
            return false;
        }
        DomainUser user = getDomainUser();
        if (record.getCreatedBy() != null && record.getCreatedBy().equals(user.getEmployee().getId())) {
            return true;
        }
        if (user.isPermissionGrant(new String[] { "executive", "accounting" })) {
            return true;
        }
        return false;
    }

    public void toggleDeliveryRequiredFlag() throws PermissionException {
        CreditNote record = (CreditNote) getRecord();
        DomainUser user = getDomainUser();
        if (record.getCreatedBy() == null || !record.getCreatedBy().equals(user.getEmployee().getId())) {
            getDomainUser().checkPermission(new String[] { "executive", "accounting" });
        }
        SalesCreditNoteManager manager = getSalesCreditNoteManager();
        manager.toggleDeliveryRequiredFlag(record);
    }

    public void createInvoiceRelation(Long invoiceId) {
        if (invoiceId != null) {
            CreditNote record = (CreditNote) getRecord();
            setRecord(getSalesCreditNoteManager().createInvoiceRelation(
                    getDomainEmployee(),
                    record,
                    invoiceId));
        }
        disableSetupMode();
    }

    public List<RecordDisplay> getRelationSuggestions() {
        return relationSuggestions;
    }

    protected void setRelationSuggestions(List<RecordDisplay> relationSuggestions) {
        this.relationSuggestions = relationSuggestions;
    }

    public SalesInvoice getInvoice() {
        return invoice;
    }

    protected final void setInvoice(SalesInvoice invoice) {
        this.invoice = invoice;
    }

    @Override
    public void setRecord(Record record) {
        super.setRecord(record);
        loadReferencedDocument(record);
        if (log.isDebugEnabled()) {
            log.debug("setRecord() done [id=" + record.getId() + ", payments="
                    + ((CreditNote) record).getPayments().size() + "]");
        }
        if (record.getReference() == null) {
            ReceivablesManager invoiceManager = (ReceivablesManager) getService(ReceivablesManager.class.getName());
            relationSuggestions = invoiceManager.getInvoices(record.getContact().getId(), false);
            if (relationSuggestions.size() > 0 && log.isDebugEnabled()) {
                log.debug("setRecord() credit note without reference found while sales exists [count=" + relationSuggestions.size() + "]");
            }
        } else if (invoice == null) {
            SalesInvoiceManager invoiceManager = (SalesInvoiceManager) getService(SalesInvoiceManager.class.getName());
            if (invoiceManager.exists(record.getReference())) {
                invoice = (SalesInvoice) invoiceManager.load(record.getReference());
            }
        }
    }

    protected SalesInvoiceManager getSalesInvoiceManager() {
        return (SalesInvoiceManager) getService(SalesInvoiceManager.class.getName());
    }

    protected SalesCreditNoteManager getSalesCreditNoteManager() {
        return (SalesCreditNoteManager) getRecordManager();
    }

    @Override
    public RecordManager getRecordManager() {
        return (SalesCreditNoteManager) getService(SalesCreditNoteManager.class.getName());
    }

    @Override
    public boolean isProvidingStatusHistory() {
        return true;
    }
}
