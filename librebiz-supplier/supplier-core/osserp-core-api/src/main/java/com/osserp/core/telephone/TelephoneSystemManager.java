/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Feb 23, 2010 3:01:27 PM 
 * 
 */
package com.osserp.core.telephone;

import java.util.List;

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public interface TelephoneSystemManager {

    /**
     * Creates a new telephoneSystem.
     * @param telephoneSystem
     * @return telephoneSystem
     */
    TelephoneSystem create(TelephoneSystem telephoneSystem);

    /**
     * Provides all telephone systems
     * @return telephone systems or null if not found
     */
    List<TelephoneSystem> getAll();

    /**
     * Tries to find a telephone system by id
     * @param id
     * @return telephone system or null if not found
     */
    TelephoneSystem find(Long id);

    /**
     * Tries to find a telephone system by branch
     * @param branch
     * @return telephone system or null if not found
     */
    TelephoneSystem findByBranch(Long branch);
}
