/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on 15-Oct-2006 11:35:43 
 * 
 */
package com.osserp.core.model.contacts.groups;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.jdom2.Element;

import com.osserp.common.Option;
import com.osserp.common.beans.AbstractOption;
import com.osserp.common.xml.JDOMUtil;
import com.osserp.core.employees.Employee;
import com.osserp.core.employees.EmployeeGroup;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author cf <cf@osserp.com>
 * 
 */
public class EmployeeGroupImpl extends AbstractOption implements EmployeeGroup {

    private String key;
    
    private Long ldapGroup = null;
    private boolean ldapGroupIgnoreGid = false;
    private boolean ldapGroupwareGroup = false;
    
    private boolean flowControlAware = false;
    private boolean ignoringBranch = false;
    private boolean branchOnly = false;
    
    private boolean applicationAdmin = false;
    private boolean executive = false;
    private boolean executiveCompany = false;
    private boolean logistics = false;
    private boolean sales = false;
    private boolean technician = false;
    private boolean it = false;
    private boolean accounting = false;
    private boolean cs = false;
    private boolean hrm = false;
    private boolean installation = false;
    private boolean purchasing = false;
    private boolean wholesale = false;
    private boolean om = false;
    
    private List<Option> permissions = new ArrayList<Option>();

    protected EmployeeGroupImpl() {
        super();
    }

    public EmployeeGroupImpl(Employee user, String name, String key) {
        super(name, user.getId());
        this.key = key;
    }

    protected EmployeeGroupImpl(EmployeeGroup o) {
        super(o);
        this.key = o.getKey();
        this.ldapGroup = o.getLdapGroup();
        this.ldapGroupIgnoreGid = o.isLdapGroupIgnoreGid();
        this.ldapGroupwareGroup = o.isLdapGroupwareGroup();
        this.flowControlAware = o.isFlowControlAware();
        this.ignoringBranch = o.isIgnoringBranch();
        this.branchOnly = o.isBranchOnly();
        this.executive = o.isExecutive();
        this.executiveCompany = o.isExecutiveCompany();
        this.logistics = o.isLogistics();
        this.sales = o.isSales();
        this.technician = o.isTechnician();
        this.it = o.isIt();
        this.accounting = o.isAccounting();
        this.cs = o.isCs();
        this.hrm = o.isHrm();
        this.installation = o.isInstallation();
        this.purchasing = o.isPurchasing();
        this.wholesale = o.isWholesale();
        this.om = o.isOm();
        if (o.getPermissions() != null && !o.getPermissions().isEmpty()) {
            permissions = new ArrayList<Option>();
            for (int i = 0, j = o.getPermissions().size(); i < j; i++) {
                permissions.add((Option) o.getPermissions().get(i).clone());
            }
        }
    }

    @Override
    public Object clone() {
        return new EmployeeGroupImpl(this);
    }

    public Long getLdapGroup() {
        return ldapGroup;
    }

    public void setLdapGroup(Long ldapGroup) {
        this.ldapGroup = ldapGroup;
    }

    public boolean isLdapGroupIgnoreGid() {
        return ldapGroupIgnoreGid;
    }

    public void setLdapGroupIgnoreGid(boolean ldapGroupIgnoreGid) {
        this.ldapGroupIgnoreGid = ldapGroupIgnoreGid;
    }

    public boolean isLdapGroupwareGroup() {
        return ldapGroupwareGroup;
    }

    public void setLdapGroupwareGroup(boolean ldapGroupwareGroup) {
        this.ldapGroupwareGroup = ldapGroupwareGroup;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public boolean isFlowControlAware() {
        return flowControlAware;
    }

    public void setFlowControlAware(boolean flowControlAware) {
        this.flowControlAware = flowControlAware;
    }

    public boolean isBranchOnly() {
        return branchOnly;
    }

    public void setBranchOnly(boolean branchOnly) {
        this.branchOnly = branchOnly;
    }

    public boolean isIgnoringBranch() {
        return ignoringBranch;
    }

    public void setIgnoringBranch(boolean ignoringBranch) {
        this.ignoringBranch = ignoringBranch;
    }

    public boolean isApplicationAdmin() {
        return applicationAdmin;
    }

    public void setApplicationAdmin(boolean applicationAdmin) {
        this.applicationAdmin = applicationAdmin;
    }

    public boolean isExecutive() {
        return executive;
    }

    public void setExecutive(boolean executive) {
        this.executive = executive;
    }

    public boolean isExecutiveCompany() {
        return executiveCompany;
    }

    public void setExecutiveCompany(boolean executiveCompany) {
        this.executiveCompany = executiveCompany;
    }

    public boolean isLogistics() {
        return logistics;
    }

    public void setLogistics(boolean logistics) {
        this.logistics = logistics;
    }

    public boolean isSales() {
        return sales;
    }

    public void setSales(boolean sales) {
        this.sales = sales;
    }

    public boolean isTechnician() {
        return technician;
    }

    public void setTechnician(boolean technician) {
        this.technician = technician;
    }

    public boolean isIt() {
        return it;
    }

    public void setIt(boolean it) {
        this.it = it;
    }

    public boolean isAccounting() {
        return accounting;
    }

    public void setAccounting(boolean accounting) {
        this.accounting = accounting;
    }

    public boolean isCs() {
        return cs;
    }

    public void setCs(boolean cs) {
        this.cs = cs;
    }

    public boolean isHrm() {
        return hrm;
    }

    public void setHrm(boolean hrm) {
        this.hrm = hrm;
    }

    public boolean isInstallation() {
        return installation;
    }

    public void setInstallation(boolean installation) {
        this.installation = installation;
    }

    public boolean isPurchasing() {
        return purchasing;
    }

    public void setPurchasing(boolean purchasing) {
        this.purchasing = purchasing;
    }

    public boolean isWholesale() {
        return wholesale;
    }

    public void setWholesale(boolean wholesale) {
        this.wholesale = wholesale;
    }

    public boolean isOm() {
        return om;
    }

    public void setOm(boolean om) {
        this.om = om;
    }

    public List<Option> getPermissions() {
        return permissions;
    }

    protected void setPermissions(List<Option> permissions) {
        this.permissions = permissions;
    }

    public boolean isPermissionGrant(String permission) {
        for (int i = 0, j = permissions.size(); i < j; i++) {
            Option next = permissions.get(i);
            if (next.getName().equals(permission)) {
                return true;
            }
        }
        return false;
    }

    public void addPermission(Employee user, String name) {
        for (int i = 0, j = permissions.size(); i < j; i++) {
            Option next = permissions.get(i);
            if (next.getName().equals(name)) {
                return;
            }
        }
        this.permissions.add(new EmployeeGroupPermissionImpl(user, this, name));
    }

    public void removePermission(String permission) {
        for (Iterator<Option> i = permissions.iterator(); i.hasNext();) {
            if (i.next().getName().equals(permission)) {
                i.remove();
                break;
            }
        }
    }

    @Override
    public Element getXML(String rootElementName) {
        Element root = super.getXML(rootElementName);
        root.addContent(JDOMUtil.createElement("key", key));
        root.addContent(JDOMUtil.createElement("ldapGroup", ldapGroup));
        root.addContent(JDOMUtil.createElement("ldapGroupIgnoreGid", ldapGroupIgnoreGid));
        root.addContent(JDOMUtil.createElement("flowControlAware", flowControlAware));
        root.addContent(JDOMUtil.createElement("ignoringBranch", ignoringBranch));
        root.addContent(JDOMUtil.createElement("branchOnly", branchOnly));
        root.addContent(JDOMUtil.createElement("executive", executive));
        root.addContent(JDOMUtil.createElement("executiveCompany", executiveCompany));
        root.addContent(JDOMUtil.createElement("logistics", logistics));
        root.addContent(JDOMUtil.createElement("sales", sales));
        root.addContent(JDOMUtil.createElement("technician", technician));
        root.addContent(JDOMUtil.createElement("it", it));
        root.addContent(JDOMUtil.createElement("accounting", accounting));
        root.addContent(JDOMUtil.createElement("cs", cs));
        root.addContent(JDOMUtil.createElement("hrm", hrm));
        root.addContent(JDOMUtil.createElement("installation", installation));
        root.addContent(JDOMUtil.createElement("purchasing", purchasing));
        root.addContent(JDOMUtil.createElement("wholesale", wholesale));
        root.addContent(JDOMUtil.createElement("om", om));
        root.addContent(JDOMUtil.createElement("permissions", permissions));
        return root;
    }
}
