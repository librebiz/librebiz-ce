/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Feb 10, 2006 10:01:06 PM 
 * 
 */
package com.osserp.core.dao.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.BackendException;
import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.dao.Tables;
import com.osserp.core.ItemList;
import com.osserp.core.dao.ItemLists;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractItemLists extends AbstractDao implements ItemLists {
    private static Logger log = LoggerFactory.getLogger(AbstractItemLists.class.getName());
    private String itemListsTable = null;
    private String itemListPositionsTable = null;
    private String itemListPositionItemsTable = null;

    public AbstractItemLists(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            String itemListsTableKey,
            String itemListPositionsTableKey,
            String itemListPositionItemsTableKey) {
        super(jdbcTemplate, tables, sessionFactory);
        this.itemListsTable = getTable(itemListsTableKey);
        this.itemListPositionsTable = getTable(itemListPositionsTableKey);
        this.itemListPositionItemsTable = getTable(itemListPositionItemsTableKey);
    }

    /**
     * Provides the entity class
     * @return entity class
     */
    protected abstract Class<? extends ItemList> getItemListClass();

    public ItemList load(Long id) {
        return (ItemList) getCurrentSession().load(getItemListClass().getName(), id);
    }

    public void save(ItemList itemList) {
        getCurrentSession().saveOrUpdate(getItemListClass().getName(), itemList);
    }

    public boolean exists(Long id) {
        if (id == null) {
            return false;
        }
        StringBuilder sql = new StringBuilder(96);
        sql
                .append("SELECT count(*) FROM ")
                .append(itemListsTable)
                .append(" WHERE id = ?");
        final Object[] params = { id };
        final int[] types = { Types.BIGINT };
        return (jdbcTemplate.queryForObject(sql.toString(), params, types, Integer.class) > 0);
    }

    /**
     * Provides the count of item lists related to given reference id
     * @param referenceId
     * @return count
     */
    protected int getCount(Long referenceId) {
        if (referenceId == null) {
            throw new BackendException(ErrorCode.APPLICATION_CONFIG);
        }
        StringBuilder sql = new StringBuilder(96);
        sql
                .append("SELECT count(*) FROM ")
                .append(itemListsTable)
                .append(" WHERE reference_id = ?");
        final Object[] params = { referenceId };
        final int[] types = { Types.BIGINT };
        return jdbcTemplate.queryForObject(sql.toString(), params, types, Integer.class);
    }

    /**
     * Indicates that name already exists
     * @param referenceId
     * @param name
     * @throws ClientException
     */
    protected void nameExists(Long referenceId, String name)
            throws ClientException {
        if (referenceId == null) {
            throw new BackendException(ErrorCode.APPLICATION_CONFIG);
        }
        if (name == null || name.length() < 1) {
            throw new ClientException(ErrorCode.NAME_MISSING);
        }
        StringBuilder sql = new StringBuilder(96);
        sql
                .append("SELECT count(*) FROM ")
                .append(itemListsTable)
                .append(" WHERE reference_id = ? AND name = ?");
        final Object[] params = { referenceId, name };
        final int[] types = { Types.BIGINT, Types.VARCHAR };
        int cnt = jdbcTemplate.queryForObject(
                sql.toString(), params, types, Integer.class);
        if (cnt > 0) {
            throw new ClientException(ErrorCode.NAME_EXISTS);
        }
    }

    public ItemList getActive(Long referenceId) {
        if (referenceId == null) {
            throw new BackendException(ErrorCode.APPLICATION_CONFIG);
        }
        StringBuilder sql = new StringBuilder(96);
        sql
                .append("SELECT id FROM ")
                .append(itemListsTable)
                .append(" WHERE reference_id = ? AND historical = false ORDER BY id DESC");
        final Object[] params = { referenceId };
        final int[] types = { Types.BIGINT };
        List result = (List) jdbcTemplate.query(
                sql.toString(), params, types, getLongRowMapper());
        if (log.isDebugEnabled()) {
            log.debug("getActive() found "
                    + ((result == null) ? " 0 " : Integer.valueOf(result.size()).toString())
                    + " results for reference "
                    + referenceId);
        }
        return (result == null || result.isEmpty()) ? null : load((Long) result.get(0));
    }

    public void delete(ItemList list) {
        int count = list.getReference() == null ? 1 : getCount(list.getReference());
        final Object[] params = { list.getId() };
        final int[] types = { Types.BIGINT };
        StringBuilder deleteItems = new StringBuilder();
        deleteItems
                .append("DELETE FROM ")
                .append(itemListPositionItemsTable)
                .append(" WHERE reference_id IN (SELECT id FROM ")
                .append(itemListPositionsTable)
                .append(" WHERE reference_id IN (SELECT id FROM ")
                .append(itemListsTable)
                .append(" WHERE id = ?))");
        jdbcTemplate.update(deleteItems.toString(), params, types);
        StringBuilder deletePositions = new StringBuilder();
        deletePositions
                .append("DELETE FROM ")
                .append(itemListPositionsTable)
                .append(" WHERE reference_id IN (SELECT id FROM ")
                .append(itemListsTable)
                .append(" WHERE id = ?)");
        jdbcTemplate.update(deletePositions.toString(), params, types);
        StringBuilder deleteCalculation = new StringBuilder();
        deleteCalculation
                .append("DELETE FROM ")
                .append(itemListsTable)
                .append(" WHERE id = ?");
        jdbcTemplate.update(deleteCalculation.toString(), params, types);
        if (count > 1) {
            Long id = getLastId(list.getReference(), null);
            if (id != null) {
                StringBuilder sql = new StringBuilder();
                sql
                        .append("UPDATE ")
                        .append(itemListsTable)
                        .append(" SET historical = false WHERE id = ?");
                jdbcTemplate.update(
                        sql.toString(),
                        new Object[] { id },
                        new int[] { Types.BIGINT });
            }
        }
    }

    public int updateReference(Long itemList, Long reference) {
        StringBuilder sql = new StringBuilder();
        sql
                .append("UPDATE ")
                .append(itemListsTable)
                .append(" SET reference_id = ?, historical = true WHERE id = ?");
        final Object[] params = { reference, itemList };
        final int[] types = { Types.BIGINT, Types.BIGINT };
        return jdbcTemplate.update(sql.toString(), params, types);
    }

    public int setOldHistorical(Long newItemList, Long reference) {
        StringBuilder sql = new StringBuilder();
        sql
                .append("UPDATE ")
                .append(itemListsTable)
                .append(" SET historical = true WHERE reference_id = ? AND id <> ?");
        final Object[] params = { reference, newItemList };
        final int[] types = { Types.BIGINT, Types.BIGINT };
        return jdbcTemplate.update(sql.toString(), params, types);
    }

    protected final String getItemListsTable() {
        return itemListsTable;
    }

    protected final String getItemListPositionsTable() {
        return itemListPositionsTable;
    }

    public ItemList getLast(Long referenceId, String contextName) {
        if (referenceId == null) {
            throw new BackendException(ErrorCode.APPLICATION_CONFIG);
        }
        Long pk = getLastId(referenceId, contextName);
        return isNotSet(pk) ? null : load(pk);
    }

    /**
     * Provides the id of the last item list related to given reference
     * @param referenceId
     * @param contextName restrict context if provided
     * @return last id or null if no item list exists
     */
    private Long getLastId(Long referenceId, String contextName) {
        if (referenceId == null) {
            return null;
        }
        List<Long> result = new ArrayList<>();
        StringBuilder sql = new StringBuilder(96);
        sql
                .append("SELECT max(id) FROM ")
                .append(itemListsTable)
                .append(" WHERE reference_id = ?");
        if (contextName == null) {
            final Object[] params = { referenceId };
            final int[] types = { Types.BIGINT };
            result = (List) jdbcTemplate.query(sql.toString(), params, types, getLongRowMapper());
        } else {
            sql.append(" AND context = ?");
            final Object[] params = { referenceId, contextName };
            final int[] types = { Types.BIGINT, Types.VARCHAR };
            result = (List) jdbcTemplate.query(sql.toString(), params, types, getLongRowMapper());
        }
        Long pk = empty(result) ? null : (Long) result.get(0);
        return isNotSet(pk) ? null : pk;
    }
}
