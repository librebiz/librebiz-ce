<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>

<c:set var="view" value="${sessionScope.showPagePermissionsView}"/>
<c:set var="permissions" value="${view.bean}"/>

<div>
	<div class="modalBoxHeader">
		<div class="modalBoxHeaderLeft">
			<fmt:message key="permissionPopup"/>
		</div>
	</div>
	<div class="modalBoxData">
		<div class="subcolumns">
			<div class="subcolumn">
				<div class="spacer"></div>
				<table class="table table-striped">
					<thead>
						<tr><th><fmt:message key="permission"/></th><th><fmt:message key="description"/></th></tr>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${empty permissions}">
								<tr><td colspan="2"><fmt:message key="noPermissionsAvailable"/></td></tr>
							</c:when>
							<c:otherwise>
								<c:forEach var="permission" items="${permissions}">
									<tr><td><o:out value="${permission.key}"/></td><td><o:out value="${permission.value}"/></td></tr>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
				<div class="spacer"></div>
			</div>
		</div>
	</div>
</div>