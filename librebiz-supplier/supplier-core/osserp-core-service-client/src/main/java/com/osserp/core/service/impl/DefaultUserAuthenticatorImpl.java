/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jul 27, 2014 
 * 
 */
package com.osserp.core.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionException;
import com.osserp.common.User;
import com.osserp.common.service.SysInfo;
import com.osserp.common.util.SecurityUtil;

import com.osserp.core.DefaultUserAuthenticator;
import com.osserp.core.dao.Users;

/**
 * DefaultUserAuthenticatorImpl is an implementation of a UserAuthenticator using the internal database as user/password store.
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class DefaultUserAuthenticatorImpl implements DefaultUserAuthenticator {
    private static Logger log = LoggerFactory.getLogger(DefaultUserAuthenticatorImpl.class.getName());
    private Users users;

    public DefaultUserAuthenticatorImpl(Users users) {
        super();
        this.users = users;
    }

    public User authenticate(String name, String remoteHost) throws PermissionException {
        // We don't provide passwordless logins with defaultAuthenticator.
        throw new UnsupportedOperationException("method not supported - sso authenticators only");
    }

    public User authenticate(String name, String password, String remoteHost) throws PermissionException {

        if (password == null || password.length() < 1) {
            log.warn("authenticate() failed [name=" + name
                    + ", message=" + ErrorCode.LOGIN_PASSWORD_MISSING
                    + ", ip=" + remoteHost + "]");
            throw new PermissionException(ErrorCode.LOGIN_FAILED);
        }
        User user = null;
        try {
            user = users.get(name);
            if (user == null || !user.getLdapUid().equals(name)) {
                log.warn("authenticate() failed [name=" + name
                        + ", message=" + ErrorCode.LOGIN_NAME_INVALID
                        + ", ip=" + remoteHost + "]");
                throw new PermissionException(ErrorCode.LOGIN_FAILED);
            }
        } catch (ClientException e) {
            log.warn("authenticate() failed [name=" + name
                    + ", message=" + e.getMessage()
                    + ", ip=" + remoteHost + "]");
            throw new PermissionException(ErrorCode.LOGIN_FAILED);
        }
        boolean cleartextOK = !SecurityUtil.isPasswordHash(password) 
                && password.equals(user.getPassword());
        if (!cleartextOK 
                && !SecurityUtil.checkPassword(password, user.getPassword())) {
            log.warn("authenticate() failed [name=" + name
                    + ", message=" + ErrorCode.LOGIN_PASSWORD_INVALID
                    + ", ip=" + remoteHost + "]");
            throw new PermissionException(ErrorCode.LOGIN_FAILED);
        }
        if (log.isDebugEnabled()) {
            log.debug("login() success via local login [id=" + user.getId() 
                + ", ip=" + remoteHost
                + ", uid=" + user.getLdapUid() + "]");
        }
        if (cleartextOK) {
            SysInfo sys = new SysInfo();
            if (!sys.isSetupEnabled()) {
                log.warn("authenticate() invalid attempt from " + remoteHost
                        + " to login with cleartext, enable setup to allow this!");
                throw new PermissionException(ErrorCode.SETUP_DISABLED);
            } 
            // found initial cleartext password, we fix this now
            String nuhash = SecurityUtil.createPassword(password);
            user.setPassword(nuhash);
            user.setPasswordResetRequired(true);
            users.save(user);
            if (log.isDebugEnabled()) {
                log.debug("authenticate() ok; fixed initial cleartext password [user="
                        + name + ", remote=" + remoteHost
                        + ", admin=" + user.isAdmin() 
                        + ", hash=" + nuhash 
                        + "]");
            }
        } 
        user.setRemoteIp(remoteHost);
        try {
            users.updateHistory(user, remoteHost);
        } catch (Exception e) {
            log.warn("authenticate() failed to update history [ip=" + remoteHost
                    + ", message=" + e.getMessage() + "]");
        }
        return user;
    }

    public User changePassword(User user, String password) throws ClientException {
        assert user != null;
        if (password == null || password.length() < 1) {
            throw new ClientException(ErrorCode.PASSWORD_MISSING);
        }
        User result = users.find(user.getId());
        result.setPassword(SecurityUtil.createPassword(password));
        users.save(result);
        return result;
    }

    public void activate(User user) {
        assert user != null;
        User result = users.find(user.getId());
        result.setActive(true);
        users.save(result);
    }

    public void deactivate(User user) {
        assert user != null;
        User result = users.find(user.getId());
        result.setActive(false);
        users.save(result);
    }
}
