<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="executiveLabel" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for=""><fmt:message key="salutation" /> / <fmt:message key="title" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <oc:select name="salutation" options="salutations" styleClass="form-control" prompt="false" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <oc:select name="title" options="titles" styleClass="form-control" prompt="false" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for=""><fmt:message key="firstName" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <v:text name="firstName" styleClass="form-control" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for=""><fmt:message key="lastName" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <v:text name="lastName" styleClass="form-control" />
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for=""><fmt:message key="hint" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <fmt:message key="accountCreateAdviceText1" />.<br />
                        <fmt:message key="accountCreateAdviceText2" />.<br />
                        <fmt:message key="accountCreateAdviceText3" />.
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for=""><fmt:message key="loginname" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <v:text name="loginname" styleClass="form-control" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="currentPassword"><fmt:message key="currentPassword" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <input type="password" name="currentPassword" class="form-control" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="password"><fmt:message key="password" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="repeatPassword"><fmt:message key="repeatPassword" /></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <input type="password" name="confirmPassword" class="form-control" />
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-4"></div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <o:submit value="save" styleClass="form-control" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group"></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

