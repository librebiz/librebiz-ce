<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>
                <fmt:message key="invoiceNumberShort" /> 
                <o:out value="${view.record.number}" /><span> / </span><o:date value="${view.record.created}" />
            </h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="selectedActionName">
                        <v:link url="${view.baseLink}/selectPaymentType" title="selectType">
                            <fmt:message key="action" />
                        </v:link>
                    </label>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <c:choose>
                        <c:when test="${view.cancellationMode}">
                            <fmt:message key="salesCancellation"/>
                        </c:when>
                        <c:when test="${view.recordCloseMode}">
                            <fmt:message key="closeBill"/>
                        </c:when>
                        <c:otherwise>
                            <%-- view.paymentDeleteMode --%>
                            <fmt:message key="paymentDelete"/>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>
        <c:choose>
            <c:when test="${view.cancellationMode}">
                <div class="row next">
                    <div class="col-md-4"></div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <v:button url="${view.baseLink}/createCancellation" value="salesCancellationCreate" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </c:when>
            <c:when test="${view.paymentDeleteMode}">
                <div class="row next">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">
                                <fmt:message key="payment" />
                            </label>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <ul>
                                <c:forEach var="payment" items="${view.record.payments}" varStatus="s">
                                    <v:link url="${view.baseLink}/save?id=${payment.id}" title="paymentDelete" confirm="true" message="confirmPaymentDelete">
                                        <li><o:number value="${payment.amount}" format="currency" /> <fmt:message key="from" /> <o:date value="${payment.paid}" /></li>
                                    </v:link>
                                </c:forEach>
                            </ul>
                        </div>
                    </div>
                </div>
            </c:when>
            <c:otherwise>
                <div class="row next">
                    <div class="col-md-4"></div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <v:button url="${view.baseLink}/setPaid" value="finalizeInvoice" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </c:otherwise>
        </c:choose>
    </div>
</div>
