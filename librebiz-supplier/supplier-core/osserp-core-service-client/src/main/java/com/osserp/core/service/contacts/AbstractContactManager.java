/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Apr 1, 2008 1:33:33 PM 
 * 
 */
package com.osserp.core.service.contacts;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.Constants;
import com.osserp.common.ErrorCode;
import com.osserp.common.Option;
import com.osserp.common.OptionsCache;
import com.osserp.common.PermissionException;
import com.osserp.common.service.ResourceLocator;
import com.osserp.common.util.EmailValidator;

import com.osserp.core.Address;
import com.osserp.core.Options;
import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.ContactPerson;
import com.osserp.core.contacts.ContactType;
import com.osserp.core.contacts.ContactValidator;
import com.osserp.core.contacts.Person;
import com.osserp.core.contacts.Salutation;
import com.osserp.core.dao.Contacts;
import com.osserp.core.employees.Employee;
import com.osserp.core.model.contacts.ContactVO;
import com.osserp.core.tasks.ContactUpdateSender;
import com.osserp.core.users.DomainUser;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractContactManager extends AbstractPersonManager {
    private static Logger log = LoggerFactory.getLogger(AbstractContactManager.class.getName());
    private Contacts contacts = null;
    private ContactUpdateSender contactUpdateSender = null;
    private OptionsCache options;
    private ResourceLocator resourceLocator;

    /**
     * Creates a new contact manager
     * @param contacts
     * @param contactUpdateSender
     */
    public AbstractContactManager(Contacts contacts, ContactUpdateSender contactUpdateSender) {
        this.contacts = contacts;
        this.contactUpdateSender = contactUpdateSender;
    }

    public AbstractContactManager(
            Contacts contacts, 
            ContactUpdateSender contactUpdateSender, 
            OptionsCache options, 
            ResourceLocator resourceLocator) {
        super();
        this.contacts = contacts;
        this.contactUpdateSender = contactUpdateSender;
        this.options = options;
        this.resourceLocator = resourceLocator;
    }

    public Person find(Long id) {
        return contacts.loadContact(id);
    }
    
    public ContactType getContactPersonType() {
        return (ContactType) options.getMapped(Options.CONTACT_TYPES, Contact.TYPE_PERSON);
    }

    public Contact createEmpty() {
        return new ContactVO();
    }

    public void changeType(
            Employee changingUser,
            Person personToChange,
            String company) throws ClientException {
        if (personToChange instanceof Contact) {
            contacts.changeType(changingUser.getId(), (Contact) personToChange, company);
        }
    }

    /**
     * @param user
     * @param contact
     * @param type
     * @param email
     * @param primary
     * @param aliasOf
     * @throws ClientException
     */
    public void addEmail(
            Employee user,
            Contact contact,
            Long type,
            String email,
            boolean primary,
            Long aliasOf) throws ClientException {

        EmailValidator.validate(email, true);
        List<Option> existing = getContactsDao().findExistingEmailContacts(email);
        if (!existing.isEmpty()) {
            throw new ClientException(ErrorCode.EMAIL_EXISTS);
        }
        contact.addEmail(user.getId(), type, email, primary, aliasOf);
        save(contact);
    }
    
    public ContactPerson updateContactPerson(
            DomainUser user,
            ContactPerson contact,
            Salutation salutation,
            Option title,
            boolean firstNamePrefix,
            String firstName,
            String lastName,
            String street,
            String zipcode,
            String city,
            Long federalState,
            String federalStateName,
            Long country,
            String position,
            String section,
            String office) throws ClientException, PermissionException {
        
        contact.setOffice(office);
        contact.setPosition(position);
        contact.setSection(section);
        
        contact = (ContactPerson) updatePerson(
                user, 
                contact, 
                salutation, 
                title, 
                firstNamePrefix, 
                firstName, 
                lastName, 
                street, 
                zipcode, 
                city, 
                federalState, 
                federalStateName, 
                country);
        
        return contact;
    }

    
    public Contact updateContact(
            DomainUser user,
            Contact contact,
            Salutation salutation,
            Option title,
            boolean firstNamePrefix,
            String firstName,
            String lastName,
            String street,
            String zipcode,
            String city,
            Long federalState,
            String federalStateName,
            Long country,
            String position,
            String section,
            String office,
            String website,
            Date birthdate,
            Salutation spouseSalutation,
            Option spouseTitle,
            String spouseFirstName, 
            String spouseLastName,
            boolean grantContactPerEmail, 
            String grantContactPerEmailNote, 
            boolean grantContactPerPhone, 
            String grantContactPerPhoneNote,
            boolean grantContactNone,
            String grantContactNoneNote,
            boolean newsletterConfirmationValue, 
            String newsletterConfirmationNoteValue,
            boolean vipValue, 
            String vipNoteValue) throws ClientException, PermissionException {
        
        contact = (Contact) deploy(
                user, 
                contact, 
                salutation, 
                title, 
                firstNamePrefix, 
                firstName, 
                lastName, 
                street, 
                zipcode, 
                city, 
                federalState, 
                federalStateName, 
                country);
        
        Long addressType = (contact.isContactPerson() || contact.isPrivatePerson()) ? Address.HOME : Address.BUSINESS;
        
        ContactValidator.validateAddress(
                contact.getType().getId(), 
                addressType, 
                street, 
                zipcode, 
                city, 
                country, 
                !contact.isContactPerson());
        contact.setOffice(office);
        contact.setPosition(position);
        contact.setSection(section);
            
        if (birthdate != null) {
            if (contact.getBirthDate() == null || !birthdate.equals(contact.getBirthDate())) {
                contact.setBirthDate(birthdate);
            }
        } else {
            if (contact.getBirthDate() != null) {
                contact.setBirthDate(null);
            }
        }
        if (contact.isBusiness()) {
            if (isSet(website)) {
                contact.setWebsite(website);
                if ((website.indexOf("ttp:") > -1)
                        || (website.indexOf("ttps") > -1)
                        || (website.indexOf(":") > -1)
                        || (website.indexOf("//") > -1)) {
                    throw new ClientException(ErrorCode.INVALID_DOMAIN_NAME);
                }
            } else if (isSet(contact.getWebsite())) {
                contact.setWebsite(null);
            }
        }
        if (contact.isContactPerson() || contact.isPrivatePerson()) {
            
            String userSalutation = createCustomSalutation(
                    user.getLocale(),
                    contact.getSalutation(),
                    contact.getTitle(),
                    contact.getFirstName(),
                    contact.getLastName(),
                    spouseSalutation,
                    spouseTitle,
                    spouseFirstName,
                    spouseLastName);
            String userNameAddressField = createCustomAddressName(
                    user.getLocale(),
                    contact.getSalutation(),
                    contact.getTitle(),
                    contact.getFirstName(),
                    contact.getLastName(),
                    spouseSalutation, 
                    spouseTitle, 
                    spouseFirstName, 
                    spouseLastName);

            StringBuilder sBufferContact = new StringBuilder();
            sBufferContact.append(contact.getSpouseFirstName()).append(contact.getSpouseLastName()).append(contact.getUserSalutation())
                    .append(contact.getUserNameAddressField());

            StringBuilder sBufferForm = new StringBuilder();
            sBufferForm.append(spouseFirstName).append(spouseLastName).append(userSalutation).append(userNameAddressField);

            if (!sBufferContact.equals(sBufferForm) ||
                    !spouseTitle.getId().equals(contact.getSpouseTitle().getId()) ||
                    !spouseSalutation.getId().equals(contact.getSpouseSalutation().getId())) {

                contact.updateSpouse(spouseSalutation, spouseTitle, spouseFirstName, spouseLastName);
                contact.setUserSalutation(userSalutation);
                contact.setUserNameAddressField(userNameAddressField);
            }
        } else if (contact.isFreelance()) {
            contact.updateSpouse(spouseSalutation, spouseTitle, spouseFirstName, spouseLastName);
        }
        boolean grantsChanged = contact.updateContactGrants(
            user.getId(),
            grantContactPerEmail, 
            grantContactPerEmailNote, 
            grantContactPerPhone, 
            grantContactPerPhoneNote,
            grantContactNone,
            grantContactNoneNote,
            newsletterConfirmationValue, 
            newsletterConfirmationNoteValue,
            vipValue,
            vipNoteValue);
        save(contact);
        if (grantsChanged) {
            sendGrantsChangedEvent(contact);
        }
        return contact;
    }

    protected String createCustomSalutation(
            Locale userLocale,
            Salutation salutation,
            Option title,
            String firstName, 
            String lastName,
            Salutation spouseSalutation,
            Option spouseTitle,
            String spouseFirstName, 
            String spouseLastName) {
        
        Locale locale = fetchLocale(userLocale);
        StringBuilder sBuffer = new StringBuilder();

        //info of spouse
        String sln = null;
        if (spouseLastName != null && spouseTitle != null && isSet(spouseTitle.getId())) {
            sln = sBuffer.append(spouseTitle.getName()).append(Constants.BLANK).append(spouseLastName).toString();
        } else if (spouseLastName != null) {
            sln = spouseLastName;
        }

        //clear sBuffer
        sBuffer = new StringBuilder();

        //info of contact
        if (title != null && isSet(title.getId())) {
            sBuffer.append(title.getName()).append(Constants.BLANK).append(lastName);
        } else {
            sBuffer.append(lastName);
        }
        String ln = sBuffer.toString();

        //clear sBuffer
        sBuffer = new StringBuilder();
         // if (log.isDebugEnabled()) { log.debug("createUserSalutation() [spouseSalutation=["+sS+"]"+ ", spouseTitle=["+st+"]"+ ", spouseLastName=["+sln+"]"+
         // ", contactSalutation=["+s+"]"+ ", contactTitle=["+t+"]"+ ", contactLastName=["+ln+"]"); }
        if (salutation != null 
                && (salutation.getId().equals(Salutation.FAMILY) || salutation.getId().equals(Salutation.MARRIED_COUPLE))) {
            //FAMILY or MARRIED_COUPLE
            if (sln != null && spouseSalutation != null) {
                if (spouseSalutation.isFemale()) {
                    //spouse is female
                    sBuffer.append(spouseSalutation.getDisplayName()).append(Constants.BLANK).append(sln);
                    sBuffer.append(getResourceString(locale, Constants.SALUTATION_MR_LOWER)).append(Constants.BLANK).append(ln);
                } else {
                    //spouse is male
                    sBuffer.append(getResourceString(locale, Constants.SALUTATION_MRS)).append(Constants.BLANK).append(ln);
                    sBuffer.append(getResourceString(locale, Constants.SALUTATION_MR_LOWER)).append(Constants.BLANK).append(sln);
                }
            } else {
                sBuffer.append(salutation.getDisplayName());
                if (salutation.isAddName()) {
                    sBuffer.append(Constants.BLANK).append(ln);
                }
            }
        } else if (sln != null && spouseSalutation != null) {
            //NO FAMILY or MARRIED_COUPLE, but SPOUSE given

            if (sln.equals(ln) && salutation != null) {
                //lastname equal

                if (salutation.isFemale() && spouseSalutation.isFemale()) {
                    //female - female
                    sBuffer.append(getResourceString(locale, Constants.SALUTATION_MRS_PARTNER)).append(Constants.BLANK).append(ln);

                } else if (salutation.isFemale() && !spouseSalutation.isFemale()) {
                    //female - male
                    sBuffer.append(salutation.getDisplayName()).append(Constants.BLANK).append(ln);
                    sBuffer.append(getResourceString(locale, Constants.SALUTATION_MR_LOWER)).append(Constants.BLANK).append(sln);

                } else if (!salutation.isFemale() && spouseSalutation.isFemale()) {
                    //male - female
                    sBuffer.append(spouseSalutation.getDisplayName()).append(Constants.BLANK).append(sln);
                    sBuffer.append(getResourceString(locale, Constants.SALUTATION_MR_LOWER)).append(Constants.BLANK).append(ln);

                } else if (!salutation.isFemale() && !spouseSalutation.isFemale()) {
                    //male - male
                    sBuffer.append(getResourceString(locale, Constants.SALUTATION_MR_PARTNER)).append(Constants.BLANK).append(ln);
                }
            } else if (salutation != null) {
                //lastname not equal

                if (salutation.isFemale() && spouseSalutation.isFemale()) {
                    //female - female
                    sBuffer.append(salutation.getDisplayName()).append(Constants.BLANK).append(ln);
                    sBuffer.append(getResourceString(locale, Constants.SALUTATION_MRS_LOWER)).append(Constants.BLANK).append(sln);

                } else if (salutation.isFemale() && !spouseSalutation.isFemale()) {
                    //female - male
                    sBuffer.append(salutation.getDisplayName()).append(Constants.BLANK).append(ln);
                    sBuffer.append(getResourceString(locale, Constants.SALUTATION_MR_LOWER)).append(Constants.BLANK).append(sln);

                } else if (!salutation.isFemale() && spouseSalutation.isFemale()) {
                    //male - female
                    sBuffer.append(spouseSalutation.getDisplayName()).append(Constants.BLANK).append(sln);
                    sBuffer.append(getResourceString(locale, Constants.SALUTATION_MR_LOWER)).append(Constants.BLANK).append(ln);

                } else if (!salutation.isFemale() && !spouseSalutation.isFemale()) {
                    //male - male
                    sBuffer.append(salutation.getDisplayName()).append(Constants.BLANK).append(ln);
                    sBuffer.append(getResourceString(locale, Constants.SALUTATION_MR_LOWER)).append(Constants.BLANK).append(sln);
                }
            }
            return sBuffer.toString();
        } else {
            sBuffer.append((salutation != null) ? salutation.getDisplayName() : "");
            if (salutation != null && salutation.isAddName()) {
                sBuffer.append(Constants.BLANK).append(ln);
            }
        }
        return sBuffer.toString();
    }
    
    protected String createCustomAddressName(
            Locale userLocale,
            Salutation salutation,
            Option title,
            String firstName, 
            String lastName,
            Salutation spouseSalutation,
            Option spouseTitle,
            String spouseFirstName, 
            String spouseLastName) {

        Locale locale = fetchLocale(userLocale);
        
        StringBuilder sBuffer = new StringBuilder();

        String sfn = spouseFirstName;
        if (spouseTitle != null) {
            sBuffer.append(spouseTitle.getName());
            if (spouseFirstName != null) {
                sBuffer.append(Constants.BLANK).append(spouseFirstName);
            }
            sfn = sBuffer.toString();
            sBuffer = new StringBuilder();
        }
        String sln = spouseLastName;

        String fn = firstName;
        if (title != null) {
            sBuffer.append(title.getName());
            if (firstName != null) {
                sBuffer.append(Constants.BLANK).append(firstName);
            }
            fn = sBuffer.toString();
            sBuffer = new StringBuilder();
        }
        String ln = lastName;
        // if (log.isDebugEnabled()) { log.debug("createUserNameAddressField() [spouseSalutation=["+sS+"]"+ ", spouseTitle=["+st+"]"+
        //  ", spouseFirstName=["+sfn+"]"+ ", spouseLastName=["+sln+"]"+ ", contactSalutation=["+s+"]"+ ", contactTitle=["+t+"]"+ ", contactFirstName=["+fn+"]"+
        //  ", contactLastName=["+ln+"]"); }
        if (salutation != null && 
                (salutation.getId().equals(Salutation.FAMILY) 
                        || salutation.getId().equals(Salutation.MARRIED_COUPLE))) {
            
            //FAMILY or MARRIED_COUPLE
            if (sfn != null && sfn.length() > 0) {
                if (spouseSalutation != null && spouseSalutation.isFemale()) {
                    //spouse is female
                    sBuffer
                            .append(sfn)
                            .append(Constants.BLANK);
                    if (ln != null &&
                            sln != null &&
                            !ln.equals(sln)) {
                        sBuffer.append(sln).append(Constants.BLANK);
                    }
                    sBuffer
                            .append(getResourceString(locale, "and"))
                            .append(Constants.BLANK)
                            .append(fn)
                            .append(Constants.BLANK)
                            .append(ln);
                } else {
                    //spouse is male
                    sBuffer
                            .append(fn)
                            .append(Constants.BLANK);
                    if (ln != null &&
                            sln != null &&
                            !ln.equals(sln)) {
                        sBuffer.append(ln).append(Constants.BLANK);
                    }
                    sBuffer
                            .append(getResourceString(locale, "and"))
                            .append(Constants.BLANK)
                            .append(sfn)
                            .append(Constants.BLANK);
                    if (sln != null) {
                        sBuffer.append(sln);
                    } else {
                        sBuffer.append(ln);
                    }

                }
            }
        }
        return sBuffer.toString();
    }

    /**
     * Persists person and synchronizes private contacts
     * @param person
     */
    protected void persist(Contact contact) {
        contacts.save(contact);
    }

    protected void sendChangedEvent(Contact contact) {
        if (contactUpdateSender != null) {
            contactUpdateSender.send(contact);
        }
    }

    /**
     * Override to perform actions after contact grants changed
     * @param contact
     */
    protected void sendGrantsChangedEvent(Contact contact) {
        // Does nothing by default
    }

    protected Contacts getContactsDao() {
        return contacts;
    }

    protected Salutation fetchSalutation(Long id) {
        if (id == null || options == null) { 
            return null;
        }
        return (Salutation) options.getMapped(Options.SALUTATIONS, id);
    }

    protected Option fetchTitle(Long id) {
        if (id == null || options == null) { 
            return null;
        }
        return options.getMapped(Options.TITLES, id);
    }
    
    protected String getResourceString(Locale locale, String key) {
        String result = null;
        if (key != null && resourceLocator != null) {
            ResourceBundle bundle = resourceLocator.getResourceBundle(locale);
            if (bundle != null) {
                try {
                    result = bundle.getString(key);
                } catch (Throwable t) {
                    log.warn("getResourceString() failed [message=" + t.getMessage() + ", key=" + key + "]");
                }
            }
        }
        return result == null ? "" : result;
    }
    
    protected Locale fetchLocale(Locale locale) {
        if (locale != null) {
            return locale;
        }
        if (resourceLocator != null) {
            return resourceLocator.getDefaultLocale();
        }
        return null;
    }
}
