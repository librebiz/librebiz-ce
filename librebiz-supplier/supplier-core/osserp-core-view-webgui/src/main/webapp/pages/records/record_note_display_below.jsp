<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<c:set var="view" value="${sessionScope.recordView}"/>
<c:set var="record" value="${view.record}"/>
<div class="recordConditions">
<p class="recordConditionsHeader">
<fmt:message key="recordNotes"/>:
<c:choose>
<c:when test="${empty record.note}"> <fmt:message key="none"/></c:when>
<c:otherwise><br/><br/><o:textOut value="${record.note}"/></c:otherwise>
</c:choose>
</p>
</div>
