<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.salesInvoiceView}">
    <o:logger write="invoice_delete.jsp no view found" level="warn" />
</c:if>
<c:set var="recordView" scope="session" value="${sessionScope.salesInvoiceView}" />
<c:set var="recordView" value="${sessionScope.recordView}" />
<c:set var="record" value="${recordView.record}" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>
    <tiles:put name="styles" type="string">
        <style type="text/css">
            <c:import url="/css/records.css"/>
        </style>
    </tiles:put>

    <tiles:put name="headline">
        <span class="bolderror" title="<fmt:message key="deletionModeActivated"/>">&nbsp;<fmt:message key="deletionModeRecordTo" />&nbsp;
        </span>
        <o:out value="${record.number}" /> / <o:out value="${record.contact.displayName}" />
    </tiles:put>
    <tiles:put name="headline_right">
        <a href="<c:url value="/salesInvoice.do?method=exitDelete"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon" /></a>
        <v:link url="/index" title="backToMenu">
            <o:img name="homeIcon" />
        </v:link>
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="subcolumns">
            <div class="subcolumn">
                <div class="spacer"></div>

                <div class="contentBox">
                    <div class="contentBoxData">
                        <div class="subcolumns">

                            <div class="contentBoxHeader">
                                <c:import url="/pages/records/record_header.jsp" />
                            </div>
                            <div class="column66l">
                                <c:import url="/pages/records/record_sigs_display.jsp" />
                                <c:import url="/pages/records/record_note_display.jsp" />
                                <br />
                            </div>
                            <div class="column33r">
                                <c:import url="/pages/records/record_status.jsp" />
                                <div class="recordLabelValue">
                                    <fmt:message key="positions" />
                                    <span>:</span>
                                    <c:choose>
                                        <c:when test="${record.itemsLocked}">
                                            <fmt:message key="statusLocked" />
                                            <span style="margin-left: 8px"> 
                                                <a href="<c:url value="/salesInvoice.do?method=unlockItems"/>"> [<fmt:message key="unlock" />]</a>
                                            </span>
                                        </c:when>
                                        <c:otherwise>
                                            <fmt:message key="changeable" />
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </div>
                        </div>
                        <div class="spacer"></div>
                        <c:import url="/pages/sales/records/invoice_summary_deletemode.jsp" />
                    </div>
                </div>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
<c:remove var="recordView" scope="session" />
