/**
 *
 * Copyright (C) 2003 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 */
package com.osserp.core.model.records;

import java.math.BigDecimal;

import com.osserp.common.Constants;
import com.osserp.common.beans.AbstractEntity;

import com.osserp.core.Item;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.ItemChangedInfo;
import com.osserp.core.products.Product;

public class ItemChangedInfoVO extends AbstractEntity implements ItemChangedInfo {

    private Product product;
    private Long previousStockId;
    private Long newStockId;
    private Double previousQuantity = 0d;
    private Double newQuantity = 0d;
    private BigDecimal previousPrice = new BigDecimal(0);
    private BigDecimal newPrice = new BigDecimal(0);

    /**
     * Required for object serialization
     */
    protected ItemChangedInfoVO() {
        super();
    }

    /**
     * Creates an item changed info used for update operations.
     * @param user
     * @param currentItem
     * @param previousItem
     */
    public ItemChangedInfoVO(Employee user, Item currentItem, Item previousItem) {
        super((Long) null,
                currentItem != null ? currentItem.getReference() : (previousItem != null ? previousItem.getReference() : null),
                (user == null ? (Long) null : user.getId()));
        this.product = currentItem != null ? currentItem.getProduct() : (previousItem != null ? previousItem.getProduct() : null);
        if (currentItem != null) {
            this.newStockId = currentItem.getStockId();
            this.newQuantity = currentItem.getQuantity();
            this.newPrice = currentItem.getPrice();
        }
        if (previousItem != null) {
            this.previousStockId = previousItem.getStockId();
            this.previousQuantity = previousItem.getQuantity();
            this.previousPrice = previousItem.getPrice();
        }
    }

    /**
     * Creates an item changed info used for add and remove operations.
     * @param user
     * @param item
     * @param deleted
     */
    public ItemChangedInfoVO(Employee user, Item item, boolean deleted) {
        super((Long) null, item.getReference(), (user == null ? (Long) null : user.getId()));
        this.product = item.getProduct();
        if (deleted) {
            this.previousStockId = item.getStockId();
            this.previousQuantity = item.getQuantity();
            this.previousPrice = item.getPrice();
        } else {
            this.newStockId = item.getStockId();
            this.newQuantity = item.getQuantity();
            this.newPrice = item.getPrice();
        }
    }

    /**
     * Creates an item changed info used for update operations.
     * @param user
     * @param reference
     * @param product
     * @param previousStockId
     * @param newStockId
     * @param previousQuantity
     * @param newQuantity
     * @param previousPrice
     * @param newPrice
     */
    public ItemChangedInfoVO(
            Employee user,
            Long reference,
            Product product,
            Long previousStockId,
            Long newStockId,
            Double previousQuantity,
            Double newQuantity,
            BigDecimal previousPrice,
            BigDecimal newPrice) {
        super((Long) null, reference, (user == null ? Constants.SYSTEM_EMPLOYEE : user.getId()));
        this.product = product;
        this.previousStockId = previousStockId;
        this.newStockId = newStockId;
        this.previousQuantity = previousQuantity;
        this.newQuantity = newQuantity;
        this.previousPrice = previousPrice;
        this.newPrice = newPrice;
    }

    protected ItemChangedInfoVO(ItemChangedInfo other) {
        super(other);
        this.product = other.getProduct();
        this.newStockId = other.getNewStockId();
        this.newQuantity = other.getNewQuantity();
        this.newPrice = other.getNewPrice();
        this.previousStockId = other.getPreviousStockId();
        this.previousQuantity = other.getPreviousQuantity();
        this.previousPrice = other.getPreviousPrice();
    }

    public Product getProduct() {
        return product;
    }

    public Long getPreviousStockId() {
        return previousStockId;
    }

    public Long getNewStockId() {
        return newStockId;
    }

    public Double getPreviousQuantity() {
        return previousQuantity;
    }

    public Double getNewQuantity() {
        return newQuantity;
    }

    public BigDecimal getPreviousPrice() {
        return previousPrice;
    }

    public BigDecimal getNewPrice() {
        return newPrice;
    }

    protected void setProduct(Product product) {
        this.product = product;
    }

    protected void setPreviousStockId(Long previousStockId) {
        this.previousStockId = previousStockId;
    }

    protected void setNewStockId(Long newStockId) {
        this.newStockId = newStockId;
    }

    protected void setNewQuantity(Double newQuantity) {
        this.newQuantity = newQuantity;
    }

    protected void setPreviousQuantity(Double previousQuantity) {
        this.previousQuantity = previousQuantity;
    }

    protected void setPreviousPrice(BigDecimal previousPrice) {
        this.previousPrice = previousPrice;
    }

    protected void setNewPrice(BigDecimal newPrice) {
        this.newPrice = newPrice;
    }
}
