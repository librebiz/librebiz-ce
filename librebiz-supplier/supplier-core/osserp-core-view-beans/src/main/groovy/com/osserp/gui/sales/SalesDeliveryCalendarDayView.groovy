/**
 *
 * Copyright (C) 2018 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License (AGPL)
 * version 3 as published by the Free Software Foundation. In accordance
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code
 * versions must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area
 * or, if not reasonably feasible for technical reason, as a top-level
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute.
 *
 * Created on Feb 6, 2018
 *
 */
package com.osserp.gui.sales

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.Calendar

import com.osserp.core.projects.ProjectSearch

import com.osserp.groovy.web.MenuItem
import com.osserp.groovy.web.ViewContextException

import com.osserp.gui.CoreView

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class SalesDeliveryCalendarDayView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(SalesDeliveryCalendarDayView.class.getName())

    Calendar calendar

    SalesDeliveryCalendarDayView() {
        super()
        dependencies = [ 'salesDeliveryCalendarView' ]
        headerName = 'salesDeliveryCalendar'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            if (!env.salesDeliveryCalendarView?.calendar) {
                logger.warn("initRequest: salesDeliveryCalendarView not bound")
                throw new ViewContextException('missing.businessCaseView', this)
            }
            calendar = env.salesDeliveryCalendarView.calendar
            loadList()
        }
    }

    protected void loadList() {
        list = projectSearch.findDeliveriesByDay(calendar, itemMode)
        logger.debug("loadList: done [count=${list.size()}, itemMode=${itemMode}]")
    }

    boolean isItemMode() {
        env.itemMode
    }

    void toggleItemMode() {
        if (itemMode) {
            env.itemMode = false
        } else {
            env.itemMode = true
        }
        loadList()
        createCustomNavigation()
    }

    protected MenuItem getToggleItemModeLink() {
        if (itemMode) {
            return navigationLink("/${context.name}/toggleItemMode", 'hideDetailsIcon', 'displayOrder')
        }
        return navigationLink("/${context.name}/toggleItemMode", 'showDetailsIcon', 'displayProducts')
    }

    @Override
    protected void createCustomNavigation() {
        nav.listNavigation = [exitLink, toggleItemModeLink, homeLink]
    }

    protected ProjectSearch getProjectSearch() {
        getService(ProjectSearch.class.getName())
    }
}
