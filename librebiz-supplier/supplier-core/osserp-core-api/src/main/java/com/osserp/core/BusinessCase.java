/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 27, 2007 5:10:13 PM 
 * 
 */
package com.osserp.core;

import java.util.Date;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.Createable;
import com.osserp.common.PersistentObject;

import com.osserp.core.customers.Customer;
import com.osserp.core.system.BranchOffice;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface BusinessCase extends Createable, PersistentObject {

    static final String NOTE_TYPE = "salesNote";
    static final Long CLOSED = 100L;

    /**
     * Provides the associated customer
     * @return customer
     */
    Customer getCustomer();

    /**
     * Returns the accounting reference
     * @return accountingReference
     */
    String getAccountingReference();

    /**
     * Sets the accounting reference
     * @param accountingReference
     */
    void setAccountingReference(String accountingReference);

    /**
     * Provides the status of the business case
     * @return status
     */
    Long getStatus();

    /**
     * Indicates that business case represents a sales case
     * @return salesContext
     */
    boolean isSalesContext();

    /**
     * Provides the context name (e.g. 'sales' or 'request')
     * @return context name
     */
    String getContextName();

    /**
     * Provides the business case name
     * @return name
     */
    String getName();

    /**
     * Provides the business address
     * @return address
     */
    BusinessAddress getAddress();

    /**
     * Provides the type of the business case
     * @return type
     */
    BusinessType getType();

    /**
     * Provides the related branch office
     * @return branch
     */
    BranchOffice getBranch();

    /**
     * Provides the manager id
     * @return managerId
     */
    Long getManagerId();

    /**
     * Provides the sales id
     * @return salesId
     */
    Long getSalesId();

    /**
     * Provides the name of the salesAssignment strategy
     * @return salesAssignment
     */
    String getSalesAssignment();

    /**
     * Indicates that sales assignment is strict. E.g. salesPerson cannot be overridden by businessCase creator if assignment is strict
     * @return true if strict assignment
     */
    boolean isSalesAssignmentStrict();

    /**
     * Provides the sales id
     * @return salesCoId
     */
    Long getSalesCoId();

    /**
     * Provides the payment id
     * @return payment id
     */
    Long getPaymentId();

    /**
     * Provides the shipping company id
     * @return shipping id
     */
    Long getShippingId();

    /**
     * The flow control sheet
     * @return flow control sheet
     */
    List<FcsItem> getFlowControlSheet();

    /**
     * Proovides the last fcs action
     * @return lastFlowControl
     */
    FcsItem getLastFlowControlSheet();

    /**
     * Adds a new flow control item
     * @param action
     * @param employeeId
     * @param created
     * @param note
     * @param headline
     * @param closing optional status
     * @return true if added
     * @throws ClientException if validation failed, note required, etc.
     */
    boolean addFlowControl(
            FcsAction action,
            Long employeeId,
            Date created,
            String note,
            String headline,
            FcsClosing closing) throws ClientException;

    /**
     * Removes a flow control item
     * @param actionId
     */
    void removeFlowControl(Long actionId);

    /**
     * Adds a cancelling flow control items and decreases status if required
     * @param cancelling
     */
    void addCancelling(FcsItem cancelling);

    /**
     * Indicates that business case is cancelled
     * @return cancelled
     */
    boolean isCancelled();

    /**
     * Indicates that business case is closed
     * @return closed
     */
    boolean isClosed();

    /**
     * Indicates that business case is stopped
     * @return stopped
     */
    boolean isStopped();

    /**
     * Enables/disables stopped flag
     * @param stopped
     */
    void setStopped(boolean stopped);

    /**
     * Provides the salesCommission
     * @return salesCommission
     */
    Double getSalesCommission();

    /**
     * Sets the salesCommission
     * @param salesCommission
     */
    void setSalesCommission(Double salesCommission);

    /**
     * Provides the primary key of an external source (if case was created by another application or other source)
     * @return externalReference or null
     */
    String getExternalReference();

    /**
     * Sets the primary key of an external source
     * @param externalReference
     */
    void setExternalReference(String externalReference);

    /**
     * Provides an url to invoke the businessCase at the external source
     * @return externalUrl
     */
    String getExternalUrl();

    /**
     * Sets an external url of the businessCase at the external source
     * @param externalUrl
     */
    void setExternalUrl(String externalUrl);
}
