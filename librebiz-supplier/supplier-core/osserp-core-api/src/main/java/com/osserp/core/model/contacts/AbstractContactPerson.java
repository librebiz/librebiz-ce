/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Jun 15, 2008 7:14:06 PM 
 * 
 */
package com.osserp.core.model.contacts;

import java.util.Date;

import org.jdom2.Element;

import com.osserp.common.ClientException;
import com.osserp.common.Option;
import com.osserp.common.xml.JDOMUtil;
import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.ContactPerson;
import com.osserp.core.contacts.ContactType;
import com.osserp.core.contacts.Salutation;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author cf <cf@osserp.com>
 * 
 */
public abstract class AbstractContactPerson extends AbstractPerson implements ContactPerson {

    private Long contactId;
    private Long reference = null;
    private Contact referenceContact = null;
    private Date birthDate;

    private String position;
    private String section;
    private String office;

    protected AbstractContactPerson() {
        super();
    }

    protected AbstractContactPerson(
            Long createdBy,
            ContactType type,
            Salutation salutation,
            Option title,
            boolean firstNamePrefix,
            String firstName,
            String lastName,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long country,
            Long federalState,
            String federalStateName,
            Long district,
            Long initialStatus,
            Date birthDate,
            boolean validate,
            String externalReference,
            String externalUrl) throws ClientException {
        super(createdBy, type, salutation, title, firstNamePrefix, firstName, lastName, street,
                streetAddon, zipcode, city, country, federalState, federalStateName, district,
                initialStatus, validate, externalReference, externalUrl);
        this.birthDate = birthDate;
    }

    protected AbstractContactPerson(
            ContactType type,
            ContactPerson reference,
            Long status,
            Salutation salutation,
            Option title,
            boolean firstNamePrefix,
            String firstName,
            String lastName,
            Date birthDate,
            String position,
            String section,
            String office,
            String addressName,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long country,
            Long federalStateId,
            String federalStateName,
            Long districtId,
            Date contactCreated,
            Long contactCreatedBy,
            Date contactChanged,
            Long contactChangedBy) {
        super(type, status, salutation, title, firstNamePrefix, firstName, lastName,
                addressName, street, streetAddon, zipcode, city, country,
                federalStateId, federalStateName, districtId, contactCreated, contactCreatedBy,
                contactChanged, contactChangedBy);
        this.reference = (reference == null ? null : reference.getContactId());
        this.birthDate = birthDate;
        this.position = position;
        this.section = section;
        this.office = office;
    }

    protected AbstractContactPerson(
            ContactType type,
            Salutation salutation,
            Option title,
            boolean firstNamePrefix,
            String firstName,
            String lastName,
            String street,
            String streetAddon,
            String zipcode,
            String city) {
        super(type, salutation, title, firstNamePrefix, firstName, lastName, street, streetAddon, zipcode, city);
    }

    protected AbstractContactPerson(
            ContactType type,
            ContactPerson reference,
            Salutation salutation,
            Option title,
            boolean firstNamePrefix,
            String firstName,
            String lastName,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long federalState,
            String federalStateName,
            Long country,
            Date birthDate,
            String position,
            String section,
            String office) throws ClientException {
        super(type, salutation, title, firstNamePrefix, firstName, lastName, street, streetAddon,
                zipcode, city, federalState, federalStateName, country);
        this.reference = (reference == null ? null : reference.getContactId());
        this.birthDate = birthDate;
        this.position = position;
        this.section = section;
        this.office = office;
    }

    protected AbstractContactPerson(
            ContactType type,
            ContactPerson reference,
            Salutation salutation,
            Option title,
            boolean firstNamePrefix,
            String firstName,
            String lastName,
            Date birthDate,
            String position,
            String section,
            String office) throws ClientException {
        super(type, salutation, title, firstNamePrefix, firstName, lastName);
        this.reference = (reference == null ? null : reference.getContactId());
        this.birthDate = birthDate;
        this.position = position;
        this.section = section;
        this.office = office;
    }

    protected AbstractContactPerson(ContactPerson otherValue) {
        super(otherValue);
        birthDate = otherValue.getBirthDate();
        position = otherValue.getPosition();
        contactId = otherValue.getContactId();
        reference = otherValue.getReference();
        office = otherValue.getOffice();
        section = otherValue.getSection();
    }

    public Long getPrimaryKey() {
        return contactId;
    }

    public Long getContactId() {
        return contactId;
    }

    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }

    public Long getReference() {
        return reference;
    }

    public void setReference(Long reference) {
        this.reference = reference;

    }

    public Contact getReferenceContact() {
        return referenceContact;
    }

    public void setReferenceContact(Contact referenceContact) {
        this.referenceContact = referenceContact;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    public void update(ContactPerson otherValue) {
        super.update(otherValue);
        contactId = otherValue.getContactId();
        reference = otherValue.getReference();
        birthDate = otherValue.getBirthDate();
        office = otherValue.getOffice();
        position = otherValue.getPosition();
        section = otherValue.getSection();
    }

    /* (non-Javadoc)
     * @see com.osserp.core.model.contacts.AbstractPerson#getDisplayName()
     */
    @Override
    public String getDisplayName() {
        if (getType().isFreelance() && isSet(office)) {
            return office;
        }
        return super.getDisplayName();
    }

    @Override
    public Element getXML() {
        return getXML("root");
    }

    @Override
    public Element getXML(String rootElementName) {
        Element root = super.getXML(rootElementName);
        root.addContent(JDOMUtil.createElement("contactId", contactId));
        root.addContent(JDOMUtil.createElement("reference", reference));
        root.addContent(JDOMUtil.createElement("birthDate", birthDate));
        root.addContent(JDOMUtil.createElement("position", position));
        root.addContent(JDOMUtil.createElement("section", section));
        root.addContent(JDOMUtil.createElement("office", office));
        return root;
    }
}
