<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="employeeGroupPermissionView" />
<c:set var="availablePermissions" value="${view.availablePermissions}" />
<c:set var="currentPermissions" value="${view.currentPermissions}" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="styles" type="string">
        <style type="text/css">
        </style>
    </tiles:put>

    <tiles:put name="headline">
        <fmt:message key="${view.headerName}" /> - <o:out value="${view.bean.id}" /> - <o:out value="${view.bean.name}" />
    </tiles:put>
    <tiles:put name="headline_right">
        <v:navigation />
    </tiles:put>
    <tiles:put name="content" type="string">

        <div class="content-area" id="employeeGroupPermissionContent">
            <div class="row">

                <div class="col-md-6 panel-area panel-area-default">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>
                                <fmt:message key="activatedPermissions" />
                            </h4>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive table-responsive-default">
                            <table class="table table-striped">
                                <tbody>
                                    <c:forEach var="perm" items="${currentPermissions}" varStatus="s">
                                        <tr>
                                            <td><c:choose>
                                                    <c:when test="${perm.displayable}">
                                                        <o:out value="${perm.description}" />
                                                    </c:when>
                                                    <c:otherwise>
                                                        <span class="error"><o:out value="${perm.description}" /></span>
                                                    </c:otherwise>
                                                </c:choose></td>
                                            <td class="action"><v:link url="/admin/employees/employeeGroupPermission/removePermission?permission=${perm.permission}">
                                                    <o:img name="deleteIcon" />
                                                </v:link> <v:ajaxLink linkId="enabled${perm.permission}" url="/admin/employees/employeeGroupPermission/displayGroupsByPermission?permission=${perm.permission}">
                                                    <span title="<fmt:message key="displayMembers"/>"><o:img name="searchIcon" /></span>
                                                </v:ajaxLink></td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 panel-area panel-area-default">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>
                                <fmt:message key="deactivatedPermissions" />
                            </h4>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive table-responsive-default">
                            <table class="table table-striped">
                                <tbody>
                                    <c:forEach var="perm" items="${availablePermissions}" varStatus="s">
                                        <c:choose>
                                            <c:when test="${perm.displayable}">
                                                <tr>
                                                    <td><o:out value="${perm.description}" /></td>
                                                    <td class="action"><o:permission role="hrm,permission_grant,client_edit" info="permissionEditPermissions">
                                                            <v:link url="/admin/employees/employeeGroupPermission/addPermission?permission=${perm.permission}">
                                                                <o:img name="enabledIcon" styleClass="bigIcon" />
                                                            </v:link>
                                                        </o:permission> <v:ajaxLink linkId="disabled${perm.permission}" url="/admin/employees/employeeGroupPermission/displayGroupsByPermission?permission=${perm.permission}">
                                                            <span title="<fmt:message key="displayMembers"/>"><o:img name="searchIcon" styleClass="bigIcon" /></span>
                                                        </v:ajaxLink></td>
                                                </tr>
                                            </c:when>
                                            <c:otherwise>
                                                <%-- only accounts with runtime_config permission are allowed to see and assign not-displayable permissions --%>
                                                <o:permission role="runtime_config">
                                                    <tr>
                                                        <td><span class="error"><o:out value="${perm.description}" /></span></td>
                                                        <td class="action"><v:link url="/admin/employees/employeeGroupPermission/addPermission?permission=${perm.permission}">
                                                                <o:img name="enabledIcon" styleClass="bigIcon" />
                                                            </v:link> <v:ajaxLink linkId="disabled${perm.permission}" url="/admin/employees/employeeGroupPermission/displayGroupsByPermission?permission=${perm.permission}">
                                                                <span title="<fmt:message key="displayMembers"/>"><o:img name="searchIcon" styleClass="bigIcon" /></span>
                                                            </v:ajaxLink></td>
                                                    </tr>
                                                </o:permission>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </tiles:put>
</tiles:insert>