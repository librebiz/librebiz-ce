<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="exitTarget" value="${requestScope.clientLogoExit}" />
<c:if test="${empty exitTarget}">
    <c:redirect url="/errors/error_context.jsp" />
</c:if>
<v:link url="/company/clientLogo/forward?exit=${exitTarget}" title="changeLogo"></v:link>

<c:choose>
    <c:when test="${!empty view.companyLogo}">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <c:choose>
                        <c:when test="${companyEditable == 'true'}">
                            <c:if test="${view.printConfigTargetAvailable}">
                                <v:link url="/company/companyPrintConfig/forward?exit=${exitTarget}" title="companyPrintConfigView">
                                    <label for="printOptions"><fmt:message key="printOptions" /></label>
                                </v:link>
                                <br />
                                <br />
                            </c:if>
                            <v:link url="/company/clientLogo/forward?exit=${exitTarget}" title="changeLogo">
                                <label for="logo"><fmt:message key="changeLogo" /></label>
                            </v:link>
                        </c:when>
                        <c:otherwise>
                            <label for="logo"><fmt:message key="logo" /></label>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <img src="<oc:img value="${view.companyLogo}"/>" style="width: 99%;" />
                </div>
            </div>
        </div>
    </c:when>
    <c:otherwise>
        <c:choose>
            <c:when test="${view.printConfigTargetAvailable}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <v:link url="/company/companyPrintConfig/forward?exit=${exitTarget}" title="companyPrintConfigView">
                                <label for="printOptions"><fmt:message key="printOptions" /></label>
                            </v:link>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <fmt:message key="logo" />
                            <span> - </span>
                            <c:choose>
                                <c:when test="${companyEditable == 'true'}">
                                    <v:link url="/company/clientLogo/forward?exit=${exitTarget}" title="addLogo">
                                        <fmt:message key="notAvailable" />
                                    </v:link>
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="notAvailable" />
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>
            </c:when>
            <c:otherwise>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="logo" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:choose>
                                <c:when test="${companyEditable == 'true'}">
                                    <v:link url="/company/clientLogo/forward?exit=${exitTarget}" title="addLogo">
                                        <fmt:message key="notAvailable" />
                                    </v:link>
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="notAvailable" />
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>
            </c:otherwise>
        </c:choose>
    </c:otherwise>
</c:choose>
