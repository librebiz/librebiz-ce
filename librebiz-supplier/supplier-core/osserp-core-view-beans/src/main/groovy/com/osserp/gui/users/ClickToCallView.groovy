/**
 *
 * Copyright (C) 2011 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Feb 16, 2011 
 * 
 */
package com.osserp.gui.users

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode
import com.osserp.common.web.PortalView

import com.osserp.core.employees.Employee
import com.osserp.core.telephone.TelephoneConfiguration
import com.osserp.core.telephone.TelephoneConfigurationManager
import com.osserp.core.users.Permissions

import com.osserp.gui.CoreView

/**
 * 
 * @author so <so@osserp.com>
 * 
 */
public class ClickToCallView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(ClickToCallView.class.getName())

    List<TelephoneConfiguration> phones = []
    TelephoneConfiguration currentTelephoneConfiguration = null
    String phoneNumber = null
    String internal = null

    ClickToCallView() {
        dependencies = ['telephoneCallView']
        headerName = 'callPhoneNumber'
        enablePopupView()
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        phoneNumber = form.getString('phoneNumber')
        if (form.getLong('phone')) {
            currentTelephoneConfiguration = telephoneConfigurationManager.find(form.getLong('phone'))
        } else {
            currentTelephoneConfiguration = null
        }
    }

    /*@Override
     void enableEditMode() {
     env.editMode = true
     BranchOfficeManager bom = getService(BranchOfficeManager.class.getName())
     branchs = bom.findAll()
     PortalView view = env.portalView
     if (view.getClickToCallEnabledBranchId() != null) {
     setPhonesByBranchId(view.getClickToCallEnabledBranchId())
     } else {
     phones = []
     }
     }
     void selectBranch() {
     logger.debug("selectBranch() invoked [id="+this.form.getLong('id')+"]")
     PortalView view = env.portalView
     Long id = form.getLong('id')
     if (id == null) {
     view.setClickToCallEnabledBranchId(null)
     view.setClickToCallEnabledPhoneUid(null)
     phones = []
     } else {
     view.setClickToCallEnabledBranchId(id)
     view.setClickToCallEnabledPhoneUid(null)
     setPhonesByBranchId(id)
     }
     }
     private void setPhonesByBranchId(Long id) {
     phones = []
     List<TelephoneConfiguration> tcList = telephoneConfigurationManager.findByBranchId(id)
     for (Iterator<TelephoneConfiguration> iterator = tcList.iterator(); iterator.hasNext();) {
     TelephoneConfiguration tc = iterator.next()
     if (tc.getTelephone()?.getInternal() != null) {
     phones.add(tc)
     }
     }
     }
     private void selectPhone(String internal) {
     logger.debug("selectPhone() invoked [internal="+internal+"]")
     PortalView view = env.portalView
     if (internal == null) {
     view.setClickToCallEnabledPhoneUid(null)
     } else {
     view.setClickToCallEnabledPhoneUid(internal)
     }
     }*/

    void enableClickToCall() {
        logger.debug('enableClickToCall() invoked')
        Employee employee = getDomainEmployee()
        if (employee) {
            TelephoneConfiguration tc = telephoneConfigurationManager.findByEmployeeId(employee.getId())
            if (tc?.getUid() && tc?.getTelephone()?.getTelephoneSystem()) {
                PortalView view = env.portalView
                view.setClickToCallEnabled(true)
                env.telephoneCallView?.generateMyNavigation()
            }
        }
    }

    void disableClickToCall() {
        logger.debug('disableClickToCall() invoked')
        PortalView view = env.portalView
        view.setClickToCallEnabled(false)
        env.telephoneCallView?.generateMyNavigation()
    }

    void clickToCall() {
        logger.debug('clickToCall() invoked')
        PortalView view = env.portalView
        if (view?.isClickToCallEnabled()) {
            Employee employee = getDomainEmployee()
            phones = []
            if (employee && phoneNumber) {
                TelephoneConfiguration tc = telephoneConfigurationManager.findByEmployeeId(employee.getId())
                def gotOwnConfig = false
                if (tc?.getTelephone()?.getTelephoneSystem()?.getBranchId()) {
                    phones.add(tc)
                    gotOwnConfig = true
                }
                if (isPermissionGrant(Permissions.TELEPHONE_EXCHANGE_CLICK_TO_CALL) && employee.branch?.id != null) {
                    phones.addAll(telephoneConfigurationManager.search(employee.branch.id, false, true))
                }
                if (phones.size() == 1) {
                    currentTelephoneConfiguration = phones.get(0)
                    doClickToCall()
                } else if (phones.size() > 1) {
                    enableEditMode()
                } else {
                    throw new ClientException(ErrorCode.NO_TELEPHONE_CONFIGURATIONS_FOUND)
                }
            } else {
                throw new ClientException(ErrorCode.PHONE_NUMBER_MISSING)
            }
        } else {
            throw new ClientException(ErrorCode.NO_CLICK_TO_CALL_ENABLED)
        }
    }

    private void doClickToCall() {
        logger.debug('doClickToCall() invoked')
        PortalView view = env.portalView
        if (view?.isClickToCallEnabled()) {
            Employee employee = getDomainEmployee()
            internal = null
            if (currentTelephoneConfiguration == null) {
                enableEditMode()
                throw new ClientException(ErrorCode.NO_TELEPHONE_CONFIGURATION_SELECTED)
            }
            if (employee && phoneNumber) {
                internal = currentTelephoneConfiguration.getInternal()
                String uid = currentTelephoneConfiguration.getUid()
                String branchId = currentTelephoneConfiguration.getTelephone()?.getTelephoneSystem()?.getBranchId()
                String liveServiceUrl = currentTelephoneConfiguration.getTelephone()?.getTelephoneSystem()?.getSystemType()?.getLiveServiceUrl()

                if (uid && branchId && liveServiceUrl) {
                    try {
                        logger.debug("clickToCall() try http request [phoneNumber=${phoneNumber}, uid=${uid}, branchId=${branchId}]")
                        // Construct data String
                        String data = URLEncoder.encode('uid', 'UTF-8') + '=' + URLEncoder.encode(uid, 'UTF-8')
                        data += "&" + URLEncoder.encode('phone_number', 'UTF-8') + '=' + URLEncoder.encode(phoneNumber, 'UTF-8')
                        data += "&" + URLEncoder.encode('branch_id', 'UTF-8') + '=' + URLEncoder.encode(branchId, 'UTF-8')

                        // Send data URL
                        URL url = new URL("${liveServiceUrl}clickToCall")
                        HttpURLConnection conn = url.openConnection()
                        conn.setDoOutput(true)
                        OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream())
                        wr.write(data)
                        wr.flush()

                        // Get the responses
                        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()))
                        String line
                        boolean error = false
                        while ((line = rd.readLine()) != null) {
                            // Process line...
                            logger.debug("clickToCall() BufferedReader= ${line}")
                            if (line == 'error') error = true
                        }
                        rd.close()
                        wr.close()
                        if (!conn.responseCode.equals(HttpURLConnection.HTTP_OK) || error) {
                            throw new ClientException(ErrorCode.CONNECTION_NOT_POSSIBLE_AT_THE_MOMENT_DIAL_MANUALLY)
                        }
                    } catch (Exception e) {
                        logger.debug("clickToCall() http request failed [Exception=${e}]")
                        throw new ClientException(ErrorCode.CONNECTION_NOT_POSSIBLE_AT_THE_MOMENT_DIAL_MANUALLY)
                    }
                    disableEditMode()
                } else {
                    throw new ClientException(ErrorCode.CLICK_TO_CALL_IMPOSSIBLE_WITH_THIS_DEVICE)
                }
            } else {
                throw new ClientException(ErrorCode.PHONE_NUMBER_MISSING)
            }
        }
    }

    protected TelephoneConfigurationManager getTelephoneConfigurationManager() {
        return getService(TelephoneConfigurationManager.class.getName())
    }
}
