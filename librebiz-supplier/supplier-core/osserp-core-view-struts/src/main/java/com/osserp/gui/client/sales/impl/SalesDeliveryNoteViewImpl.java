/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 31, 2006 3:35:11 PM 
 * 
 */
package com.osserp.gui.client.sales.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.PermissionException;
import com.osserp.common.web.Form;
import com.osserp.common.web.ViewName;

import com.osserp.core.Item;
import com.osserp.core.finance.DeliveryNote;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordManager;
import com.osserp.core.sales.SalesDeliveryNoteManager;
import com.osserp.gui.BusinessCaseView;
import com.osserp.gui.client.records.impl.AbstractDeliveryNoteView;
import com.osserp.gui.client.sales.SalesDeliveryNoteView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
@ViewName("salesDeliveryNoteView")
public class SalesDeliveryNoteViewImpl extends AbstractDeliveryNoteView implements SalesDeliveryNoteView {
    private static Logger log = LoggerFactory.getLogger(SalesDeliveryNoteViewImpl.class.getName());
    private boolean deleteMode = false;
    private String deliveryNoteCorrectionChangePermissions = null;

    protected SalesDeliveryNoteViewImpl() {
        super();
        setPriceInputEnabled(false);
    }

    protected SalesDeliveryNoteViewImpl(
            String orderViewKey,
            String ignoreNotExistingSerialPermissions,
            String deliveryNoteCorrectionChangePermissions) {
        super(orderViewKey, true, ignoreNotExistingSerialPermissions);
        setPriceInputEnabled(false);
        this.deliveryNoteCorrectionChangePermissions = deliveryNoteCorrectionChangePermissions;
    }

    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        super.init(request);
    }

    public void initTransferToStockMode(BusinessCaseView bcv, String exitTarget) throws ClientException {
        SalesDeliveryNoteManager manager = (SalesDeliveryNoteManager) getRecordManager();
        DeliveryNote created = manager.createRollin(getDomainEmployee(), bcv.getSales());
        setBean(created);
        if (exitTarget != null) {
            setExitTarget(exitTarget);
        }
        if (log.isDebugEnabled()) {
            log.debug("initTransferToStockMode() done, exit target ["
                    + getExitTarget() + "]");
        }
    }

    public boolean isNoteDeletable() {
        DeliveryNote selected = (DeliveryNote) getBean();
        if (selected == null) {
            log.warn("isNoteDeletable() false, no note selected!");
            return false;
        }
        if (selected.isCorrection()) {
            if (log.isDebugEnabled()) {
                log.debug("isNoteDeletable() found correction [id=" + selected.getId() + "]");
            }
            if (isPermissionGrant(deliveryNoteCorrectionChangePermissions)) {
                if (log.isDebugEnabled()) {
                    log.debug("isNoteDeletable() user has permission to change correction [id="
                            + getUser().getId()
                            + ", permissions="
                            + deliveryNoteCorrectionChangePermissions + "]");
                }
                return true;
            }
            if (log.isDebugEnabled()) {
                log.debug("isNoteDeletable() user has no permission to change correction [id="
                        + getUser().getId()
                        + ", permissions="
                        + deliveryNoteCorrectionChangePermissions + "]");
            }
            return false;
        }
        for (int i = 0, j = selected.getItems().size(); i < j; i++) {
            Item next = selected.getItems().get(i);
            if (next.getQuantity() > 0) {
                return true;
            }
        }
        if (selected.getItems().isEmpty()) {
            // It should not be possible to delete all items of a delivery note, see deleteItems check for last item.
            // However, if someone is able to delete all items, he should also be able to delete the record iteself.
            return true;
        }
        if (log.isDebugEnabled()) {
            log.debug("isNoteDeletable() false, note is indirect correction [id=" + selected.getId() + "]");
        }
        return false;
    }

    public void initDeleteMode(BusinessCaseView bcv, String exitTarget) throws ClientException {
        SalesDeliveryNoteManager manager = (SalesDeliveryNoteManager) getRecordManager();
        List<DeliveryNote> result = new ArrayList<>();
        List<DeliveryNote> list = manager.getBySales(bcv.getSales());
        for (int i = 0, j = list.size(); i < j; i++) {
            DeliveryNote next = list.get(i);
            if (manager.deletePermissionGrant(getDomainUser(), next)) {
                result.add(next);
            }
        }
        if (result.isEmpty()) {
            throw new ClientException(ErrorCode.SEARCH);
        }
        setList(result);
        setDeleteMode(true);
        if (exitTarget != null) {
            setExitTarget(exitTarget);
        }
        if (log.isDebugEnabled()) {
            log.debug("initDeleteMode() done, exit target ["
                    + getExitTarget() + "]");
        }
    }

    public boolean isAllAvailableOnStock() {
        SalesDeliveryNoteManager manager = (SalesDeliveryNoteManager) getRecordManager();
        return manager.isAvailableOnStock(getNote());
    }

    public boolean isDeleteMode() {
        return deleteMode;
    }

    public void setDeleteMode(boolean deleteMode) {
        if (log.isDebugEnabled()) {
            log.debug("setDeleteMode() invoked [mode=" + deleteMode + "]");
        }
        this.deleteMode = deleteMode;
    }

    @Override
    public void checkRelease() throws PermissionException {
        if (log.isDebugEnabled()) {
            log.debug("checkCreatePdf() invoked for final pdf, checking permission [required=delivery_note_create]");
        }
        checkPermission(new String[] { "delivery_note_create" });
    }

    @Override
    public void delete() throws ClientException, PermissionException {
        try {
            super.delete();
            setDeleteMode(false);
        } catch (PermissionException p) {
            setDeleteMode(false);
            setConfirmDelete(false);
            throw p;
        }
    }

    @Override
    protected void deleteItemValidation(Record record, Item selected) throws ClientException {
        // don't use local copy to prevent user from removing all items using several windows/tabs
        DeliveryNote deliveryNote = (DeliveryNote) getRecordManager().load(getRecord().getId());
        if (deliveryNote.isCorrection()) {
            if (log.isDebugEnabled()) {
                log.debug("deleteItem() failed, note is correction [record=" + deliveryNote.getId() + "]");
            }
            if (!isPermissionGrant(deliveryNoteCorrectionChangePermissions)) {
                throw new ClientException(ErrorCode.RECORD_OPERATION_NOT_SUPPORTED);
            }
        }
        if (deliveryNote.getItems().size() == 1) {
            if (log.isDebugEnabled()) {
                log.debug("deleteItem() failed, attempt to delete last item [record=" + deliveryNote.getId() + "]");
            }
            throw new ClientException(ErrorCode.DELIVERY_ITEMS_MISSING);
        }
    }

    @Override
    public void updateItem(Form fh) throws ClientException {
        DeliveryNote selected = (DeliveryNote) getRecord();
        if (selected.isCorrection()) {
            if (log.isDebugEnabled()) {
                log.debug("updateItem() failed, note is correction [record=" + selected.getId() + "]");
            }
            if (!isPermissionGrant(deliveryNoteCorrectionChangePermissions)) {
                throw new ClientException(ErrorCode.RECORD_OPERATION_NOT_SUPPORTED);
            }
        }
        super.updateItem(fh);
    }

    @Override
    public RecordManager getRecordManager() {
        return (RecordManager) getService(SalesDeliveryNoteManager.class.getName());
    }

    @Override
    public boolean isProvidingStatusHistory() {
        return true;
    }

}
