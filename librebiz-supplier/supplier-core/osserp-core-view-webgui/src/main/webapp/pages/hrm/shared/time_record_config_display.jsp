<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>

<c:if test="${!empty requestScope.timeRecordType}">
	<c:set var="type" value="${requestScope.timeRecordType}"/>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4><o:out value="${type.id}"/><span> - </span><o:out value="${type.name}"/></h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-body">

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="name" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${type.name}"/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="description" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${type.description}" />
                        </div>
                    </div>
                </div>

                <div class="row next">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="interval" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:choose>
                                <c:when test="${type.interval}">
                                    <fmt:message key="bigYes"/>
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="bigNo"/>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>

                <c:if test="${type.interval}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="intervalPeriod" />
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <c:choose>
                                    <c:when test="${type.intervalPeriod}">
                                        <fmt:message key="bigYes"/>
                                    </c:when>
                                    <c:otherwise>
                                        <fmt:message key="bigNo"/>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                </c:if>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="lengthByProfileLabel"/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:choose>
                                <c:when test="${type.intervalHoursByConfig}">
                                    <fmt:message key="bigYes"/>
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="bigNo"/>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>

                <c:if test="${type.intervalHoursByConfig}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <fmt:message key="intervalLength"/>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <o:out value="${type.intervalHours}"/> : <o:out value="${type.intervalMinutes}"/> <fmt:message key="hourShort"/>
                            </div>
                        </div>
                    </div>
                </c:if>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="intervalStart"/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <o:out value="${type.intervalStartHours}"/> : <o:out value="${type.intervalStartMinutes}"/> <fmt:message key="clock"/>
                        </div>
                    </div>
                </div>

                <div class="row next">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="needApproval"/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:choose>
                                <c:when test="${type.needApproval}">
                                    <fmt:message key="bigYes"/>
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="bigNo"/>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="manuallyInput"/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:choose>
                                <c:when test="${type.manualInputRequired}">
                                    <fmt:message key="bigYes"/>
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="bigNo"/>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="ignoreBreakRules"/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:choose>
                                <c:when test="${type.ignoreBreakRules}">
                                    <fmt:message key="bigYes"/>
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="bigNo"/>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="visibleBooking"/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:choose>
                                <c:when test="${type.visible}">
                                    <fmt:message key="bigYes"/>
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="bigNo"/>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="bookable"/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:choose>
                                <c:when test="${type.bookable}">
                                    <fmt:message key="bigYes"/>
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="bigNo"/>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>

                <div class="row next">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="bookingPast"/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:choose>
                                <c:when test="${type.bookingPast}">
                                    <fmt:message key="bigYes"/>
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="bigNo"/>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="bookingFuture"/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:choose>
                                <c:when test="${type.bookingFuture}">
                                    <fmt:message key="bigYes"/>
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="bigNo"/>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>

                <div class="row next">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="illnessLabel"/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:choose>
                                <c:when test="${type.illness}">
                                    <fmt:message key="bigYes"/>
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="bigNo"/>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="leave"/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:choose>
                                <c:when test="${type.leave}">
                                    <fmt:message key="bigYes"/>
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="bigNo"/>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="unpaidLeave"/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:choose>
                                <c:when test="${type.unpaidLeave}">
                                    <fmt:message key="bigYes"/>
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="bigNo"/>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="specialLeave"/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:choose>
                                <c:when test="${type.specialLeave}">
                                    <fmt:message key="bigYes"/>
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="bigNo"/>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="subtractionBooking"/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:choose>
                                <c:when test="${type.subtraction}">
                                    <fmt:message key="bigYes"/>
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="bigNo"/>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <fmt:message key="additionBooking"/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <c:choose>
                                <c:when test="${type.addition}">
                                    <fmt:message key="bigYes"/>
                                </c:when>
                                <c:otherwise>
                                    <fmt:message key="bigNo"/>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</c:if>
