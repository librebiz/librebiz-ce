/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 2, 2006 10:25:26 PM 
 * 
 */
package com.osserp.gui.web.employees;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.ActionException;
import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.web.RequestUtil;
import com.osserp.common.web.struts.StrutsForm;
import com.osserp.gui.client.Actions;
import com.osserp.gui.client.contacts.ContactView;
import com.osserp.gui.client.contacts.EmployeeView;
import com.osserp.gui.web.contacts.AbstractContactViewAwareAction;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class EmployeeAction extends AbstractContactViewAwareAction {
    private static Logger log = LoggerFactory.getLogger(EmployeeAction.class.getName());

    /**
     * Creates new employee view by existing contact view.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward display(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("display() request from " + getUser(session).getId());
        }
        ContactView contactView = getContactView(session);
        EmployeeView employeeView = (EmployeeView) createView(request, EmployeeView.class.getName());
        employeeView.load(contactView);
        return mapping.findForward(employeeView.getActionTarget());
    }

    /**
     * Changes employees activation status
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward changeActivation(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("changeActivation() request from " + getUser(session).getId());
        }
        EmployeeView view = getEmployeeView(session);
        view.changeActivationStatus();
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Changes employees website display status
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward changeWebsiteDisplay(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("changeWebsiteDisplay() request from " + getUser(session).getId());
        }
        EmployeeView view = getEmployeeView(session);
        view.changeWebsiteDisplay();
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Enables details edit mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward enableDetailsEdit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableDetailsEdit() request from " + getUser(session).getId());
        }
        EmployeeView view = getEmployeeView(session);
        view.setDetailsEditMode(true);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Disables details edit mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward disableDetailsEdit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disableDetailsEdit() request from " + getUser(session).getId());
        }
        EmployeeView view = getEmployeeView(session);
        view.setDetailsEditMode(false);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Updates details
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward updateDetails(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("updateDetails() request from " + getUser(session).getId());
        }
        EmployeeView view = getEmployeeView(session);
        try {
            view.updateDetails(new StrutsForm(form));
        } catch (ClientException c) {
            saveError(request, c.getMessage());
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Enables employee description edit mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward enableDescriptionEdit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableDescriptionEdit() request from " + getUser(session).getId());
        }
        EmployeeView view = getEmployeeView(session);
        view.setDescriptionEditMode(true);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Disables employee description edit mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward disableDescriptionEdit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disableDescriptionEdit() request from " + getUser(session).getId());
        }
        EmployeeView view = getEmployeeView(session);
        view.setDescriptionEditMode(false);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Updates employees description
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward updateDescription(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("updateDescription() request from " + getUser(session).getId());
        }
        EmployeeView view = getEmployeeView(session);
        try {
            view.updateDescription(new StrutsForm(form));
        } catch (ClientException c) {
            saveError(request, c.getMessage());
        }
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Enables record phone selection mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward enableRecordPhoneSelectionMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableRecordPhoneSelectionMode() request from " + getUser(session).getId());
        }
        EmployeeView view = getEmployeeView(session);
        view.setRecordPhoneSelectionMode(true);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Disables record phone selection mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward disableRecordPhoneSelectionMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disableRecordPhoneSelectionMode() request from " + getUser(session).getId());
        }
        EmployeeView view = getEmployeeView(session);
        view.setRecordPhoneSelectionMode(false);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Selects record phone
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward selectRecordPhone(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("selectRecordPhone() request from " + getUser(session).getId());
        }
        EmployeeView view = getEmployeeView(session);
        view.changePhoneForRecord(RequestUtil.getId(request));
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Enables employee internet phone selection mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward enableInternetPhoneSelectionMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("enableInternetPhoneSelectionMode() request from " + getUser(session).getId());
        }
        EmployeeView view = getEmployeeView(session);
        view.setInternetPhoneSelectionMode(true);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Disables employee internet phone selection mode
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward disableInternetPhoneSelectionMode(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("disableInternetPhoneSelectionMode() request from " + getUser(session).getId());
        }
        EmployeeView view = getEmployeeView(session);
        view.setInternetPhoneSelectionMode(true);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Selects employees internet phone
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward selectInternetPhone(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("selectInternetPhone() request from " + getUser(session).getId());
        }
        EmployeeView view = getEmployeeView(session);
        view.changePhoneForInternet(RequestUtil.fetchId(request));
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Selects employees default disciplinarian
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward setDefaultDisciplinarian(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("setDefaultDisciplinarian() request from " + getUser(session).getId());
        }
        EmployeeView view = getEmployeeView(session);
        view.setDefaultDisciplinarian(RequestUtil.getId(request));
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Return from contact display
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward exitDisplay(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exitDisplay() request from " + getUser(session).getId());
        }
        EmployeeView view = fetchEmployeeView(session);
        if (view != null
                && view.getList() != null
                && view.getList().size() > 1) {
            return mapping.findForward(Actions.LIST);
        }
        return mapping.findForward(Actions.SEARCH);
    }

    protected EmployeeView fetchEmployeeView(HttpSession session) throws PermissionException {
        return (EmployeeView) fetchView(session, EmployeeView.class);
    }

    protected EmployeeView getEmployeeView(HttpSession session) throws PermissionException {
        EmployeeView view = fetchEmployeeView(session);
        if (view == null) {
            throw new ActionException("view not bound!");
        }
        return view;
    }
}
