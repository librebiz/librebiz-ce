<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" scope="page" value="${sessionScope.telephoneGroupView}"/>
<c:set var="sortUrl" scope="page" value="/telephones/telephoneGroup/sort"/>

<c:if test="${!empty sessionScope.error}">
	&nbsp;
	<div class="errormessage">
		<fmt:message key="error"/>: <fmt:message key="${sessionScope.error}"/>
	</div>
    <o:removeErrors/>
</c:if>
<c:if test="${!empty view}">
	<table class="table table-striped">
		<thead>
			<tr>
				<th><v:ajaxLink url="${sortUrl}?key=name" targetElement="ajaxContent"><fmt:message key="name"/></v:ajaxLink></th>
				<th><v:ajaxLink url="${sortUrl}?key=telephoneSystem.name" targetElement="ajaxContent"><fmt:message key="telephoneSystem"/></v:ajaxLink></th>
				<th><v:ajaxLink url="${sortUrl}?key=created" targetElement="ajaxContent"><fmt:message key="lastChange"/></v:ajaxLink></th>
				<th><v:ajaxLink url="${sortUrl}?key=createdBy" targetElement="ajaxContent"><fmt:message key="of"/></v:ajaxLink></th>
				<th class="scrollbar"></th>
			</tr>
		</thead>
		<tbody class="scrolly" style="height:414px;">
			<c:choose>
				<c:when test="${empty view.list}">
					<tr>
						<td colspan="5"><fmt:message key="error.no.telephoneGroups.found"/></td>
					</tr>
				</c:when>
				<c:otherwise>
					<c:forEach var="obj" items="${view.list}">
						<tr id="telephone_group_${obj.id}">
							<td id="name_${obj.id}">
								<v:ajaxLink url="/telephones/telephoneGroupEdit/enableEditMode?name=name&id=${obj.id}" targetElement="name_${obj.id}" title="edit">
									<o:out value="${obj.name}"/>
								</v:ajaxLink>
							</td>
							<td>
								<c:choose>
									<c:when test="${!empty obj.telephoneSystem.name}"><o:out value="${obj.telephoneSystem.name}"/></c:when>
									<c:otherwise>(<fmt:message key="notSet"/>)</c:otherwise>
								</c:choose>
							</td>
							<td>
								<c:choose>
									<c:when test="${empty obj.changed}">
										<o:date value="${obj.created}"/>
									</c:when>
									<c:otherwise>
										<o:date value="${obj.changed}"/>
									</c:otherwise>
								</c:choose>
							</td>
							<td>
								<c:choose>
									<c:when test="${empty obj.changedBy}">
										<oc:employee value="${obj.createdBy}"/>
									</c:when>
									<c:otherwise>
										<oc:employee value="${obj.changedBy}"/>
									</c:otherwise>
								</c:choose>
							</td>
							<td></td>
						</tr>
					</c:forEach>
					<tr><td colspan="5" style="height:100%;"></td></tr>
				</c:otherwise>
			</c:choose>
		</tbody>
	</table>
</c:if>