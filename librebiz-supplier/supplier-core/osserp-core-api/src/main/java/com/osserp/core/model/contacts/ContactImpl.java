/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 13-Oct-2006 14:34:32 
 * 
 */
package com.osserp.core.model.contacts;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jdom2.Element;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.Mappable;
import com.osserp.common.Option;
import com.osserp.common.mail.EmailAddress;
import com.osserp.common.util.EmailValidator;
import com.osserp.common.util.PhoneUtil;
import com.osserp.common.xml.JDOMUtil;

import com.osserp.core.contacts.Contact;
import com.osserp.core.contacts.ContactType;
import com.osserp.core.contacts.Phone;
import com.osserp.core.contacts.PhoneType;
import com.osserp.core.contacts.Salutation;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ContactImpl extends AbstractContactPerson implements Contact {
    private static Logger log = LoggerFactory.getLogger(ContactImpl.class.getName());

    private boolean client;
    private boolean branchOffice;
    private boolean employee;
    private boolean customer;
    private boolean supplier;
    private boolean installer;
    private boolean other;
    private boolean vip;
    private String vipNote;
    private boolean user;
    private String defaultGroupDisplay;

    private boolean newsletterConfirmation = false;
    private String newsletterConfirmationNote;

    private boolean grantContactPerEmail; 
    private Date grantContactPerEmailChanged; 
    private Long grantContactPerEmailChangedBy; 
    private String grantContactPerEmailNote;
    
    private boolean grantContactPerPhone;
    private Date grantContactPerPhoneChanged; 
    private Long grantContactPerPhoneChangedBy; 
    private String grantContactPerPhoneNote;
    
    private boolean grantContactNone;
    private Date grantContactNoneChanged;
    private Long grantContactNoneChangedBy; 
    private String grantContactNoneNote;

    private String website = null;

    private List<EmailAddress> emails = new ArrayList<>();
    private List<Phone> phoneNumbers = new ArrayList<>();

    protected ContactImpl() {
        super();
    }

    protected ContactImpl(Contact otherValue) {
        update(otherValue);
    }

    /**
     * Default constructor to create new contacts
     * @param createdBy
     * @param type
     * @param salutation
     * @param title
     * @param firstName
     * @param lastName
     * @param street
     * @param streetAddon
     * @param zipcode
     * @param city
     * @param country
     * @param federalState
     * @param federalStateName
     * @param district
     * @param birthDate
     * @param initialStatus
     * @param validate
     * @param externalReference
     * @param externalUrl
     * @throws ClientException
     */
    public ContactImpl(
            Long createdBy,
            ContactType type,
            Salutation salutation,
            Option title,
            boolean firstNamePrefix,
            String firstName,
            String lastName,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long country,
            Long federalState,
            String federalStateName,
            Long district,
            Date birthDate,
            Long initialStatus,
            boolean validate,
            String externalReference,
            String externalUrl)
            throws ClientException {
        super(
                createdBy,
                type,
                salutation,
                title,
                firstNamePrefix,
                firstName,
                lastName,
                street,
                streetAddon,
                zipcode,
                city,
                country,
                federalState,
                federalStateName,
                district,
                initialStatus,
                birthDate,
                validate,
                externalReference,
                externalUrl);
        other = true;
    }

    /**
     * Constructor to create an initial contact person for type changes
     * @param createdBy
     * @param personType
     * @param contactReference
     * @param salutation
     * @param title
     * @param firstNamePrefix
     * @param firstName
     * @param lastName
     * @param position
     * @param section
     * @param office
     * @throws ClientException if lastname is missing
     */
    public ContactImpl(
            Long createdBy,
            ContactType personType,
            Contact contactReference,
            Salutation salutation,
            Option title,
            boolean firstNamePrefix,
            String firstName,
            String lastName,
            String position,
            String section,
            String office) throws ClientException {

        super(
                personType,
                contactReference,
                salutation,
                title,
                firstNamePrefix,
                firstName,
                lastName,
                null, // birthdate not present on type change
                position,
                section,
                office);
        if (isNotSet(lastName)) {
            throw new ClientException(ErrorCode.LASTNAME_MISSING);
        }
        setPersonCreated(new java.util.Date(System.currentTimeMillis()));
        setPersonCreatedBy(createdBy);
        other = true;
    }

    /**
     * Default constructor to create a new contact person
     * @param createdBy
     * @param personType
     * @param contactReference
     * @param salutation
     * @param title
     * @param firstNamePrefix
     * @param firstName
     * @param lastName
     * @param street
     * @param streetAddon
     * @param zipcode
     * @param city
     * @param federalState
     * @param federalStateName
     * @param country
     * @param birthDate
     * @param position
     * @param section
     * @param office
     * @throws ClientException if lastname is missing
     */
    public ContactImpl(
            Long createdBy,
            ContactType personType,
            Contact contactReference,
            Salutation salutation,
            Option title,
            boolean firstNamePrefix,
            String firstName,
            String lastName,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            Long federalState,
            String federalStateName,
            Long country,
            Date birthDate,
            String position,
            String section,
            String office) throws ClientException {

        super(
                personType,
                contactReference,
                salutation,
                title,
                firstNamePrefix,
                firstName,
                lastName,
                street,
                streetAddon,
                zipcode,
                city,
                federalState,
                federalStateName,
                country,
                birthDate,
                position,
                section,
                office);
        if (isNotSet(lastName)) {
            throw new ClientException(ErrorCode.LASTNAME_MISSING);
        }
        setPersonCreated(new java.util.Date(System.currentTimeMillis()));
        setPersonCreatedBy(createdBy);
        other = true;
    }

    /**
     * Invoked by related contact
     * @param contactId
     * @param type
     * @param salutation
     * @param title
     * @param firstNamePrefix
     * @param firstName
     * @param lastName
     * @param street
     * @param streetAddon
     * @param zipcode
     * @param city
     */
    public ContactImpl(
            Long contactId,
            ContactType type,
            Salutation salutation,
            Option title,
            boolean firstNamePrefix,
            String firstName,
            String lastName,
            String street,
            String streetAddon,
            String zipcode,
            String city) {

        super(
                type,
                salutation,
                title,
                firstNamePrefix,
                firstName,
                lastName,
                street,
                streetAddon,
                zipcode,
                city);
        setContactId(contactId);
    }

    /**
     * Invoked by related
     * @param contactId
     * @param reference
     * @param type
     * @param salutation
     * @param title
     * @param firstNamePrefix
     * @param firstName
     * @param lastName
     * @param spouseSalutation
     * @param spouseTitle
     * @param spouseFirstName
     * @param spouseLastName
     * @param street
     * @param streetAddon
     * @param zipcode
     * @param city
     * @param employee
     * @param customer
     * @param supplier
     * @param installer
     * @param other
     * @param vip
     * @param client
     * @param branch
     * @param created
     * @param createdBy
     */
    public ContactImpl(
            Long contactId,
            Long reference,
            ContactType type,
            Salutation salutation,
            Option title,
            boolean firstNamePrefix,
            String firstName,
            String lastName,
            Salutation spouseSalutation,
            Option spouseTitle,
            String spouseFirstName,
            String spouseLastName,
            String street,
            String streetAddon,
            String zipcode,
            String city,
            boolean employee,
            boolean customer,
            boolean supplier,
            boolean installer,
            boolean other,
            boolean vip,
            boolean client,
            boolean branch,
            Date created,
            Long createdBy) {

        this(
                contactId,
                type,
                salutation,
                title,
                firstNamePrefix,
                firstName,
                lastName,
                street,
                streetAddon,
                zipcode,
                city);
        setReference(reference);
        updateSpouse(spouseSalutation, spouseTitle, spouseFirstName, spouseLastName);
        setPersonCreated(created);
        setPersonCreatedBy(createdBy);
        this.employee = employee;
        this.customer = customer;
        this.supplier = supplier;
        this.installer = installer;
        this.other = other;
        this.vip = vip;
        this.client = client;
        this.branchOffice = branch;
    }

    public boolean isClient() {
        return client;
    }

    public void setClient(boolean client) {
        this.client = client;
    }

    public boolean isBranchOffice() {
        return branchOffice;
    }

    public void setBranchOffice(boolean branchOffice) {
        this.branchOffice = branchOffice;
    }

    public boolean isEmployee() {
        return employee;
    }

    public void setEmployee(boolean employee) {
        this.employee = employee;
    }

    public boolean isCustomer() {
        return customer;
    }

    public void setCustomer(boolean customer) {
        this.customer = customer;
    }

    public boolean isSupplier() {
        return supplier;
    }

    public void setSupplier(boolean supplier) {
        this.supplier = supplier;
    }

    public boolean isInstaller() {
        return installer;
    }

    public void setInstaller(boolean installer) {
        this.installer = installer;
    }

    public boolean isOther() {
        return other;
    }

    public void setOther(boolean other) {
        this.other = other;
    }

    public boolean isVip() {
        return vip;
    }

    public void setVip(boolean vip) {
        this.vip = vip;
    }

    public String getVipNote() {
        return vipNote;
    }

    public void setVipNote(String vipNote) {
        this.vipNote = vipNote;
    }

    public boolean isUser() {
        return user;
    }

    public void setUser(boolean user) {
        this.user = user;
    }

    public String getDefaultGroupDisplay() {
        return defaultGroupDisplay;
    }

    public void setDefaultGroupDisplay(String defaultGroupDisplay) {
        this.defaultGroupDisplay = defaultGroupDisplay;
    }

    public boolean isNewsletterConfirmation() {
        return newsletterConfirmation;
    }

    public void setNewsletterConfirmation(boolean newsletterConfirmation) {
        this.newsletterConfirmation = newsletterConfirmation;
    }

    public String getNewsletterConfirmationNote() {
        return newsletterConfirmationNote;
    }

    public void setNewsletterConfirmationNote(String newsletterConfirmationNote) {
        this.newsletterConfirmationNote = newsletterConfirmationNote;
    }

    public final String getGrantContactStatus() {
        if (grantContactNone) {
            return "contactForbidden";
        }
        if (grantContactPerEmail && grantContactPerPhone) {
            return "contactPerEmailAndPhone";
        }
        if (grantContactPerEmail) {
            return "contactPerEmail";
        }
        if (grantContactPerPhone) {
            return "contactPerPhone";
        }
        return "contactPerLetterOnly";
    }

    public final boolean isGrantContactStatusAvailable() {
        return !(!grantContactPerEmail && grantContactPerEmailChanged == null
                && !grantContactPerPhone && grantContactPerPhoneChanged == null
                && !grantContactNone && grantContactNoneChanged == null);
    }

    public boolean isGrantContactPerEmail() {
        return grantContactPerEmail;
    }

    public void setGrantContactPerEmail(boolean grantContactPerEmail) {
        this.grantContactPerEmail = grantContactPerEmail;
    }

    public Date getGrantContactPerEmailChanged() {
        return grantContactPerEmailChanged;
    }

    public void setGrantContactPerEmailChanged(Date grantContactPerEmailChanged) {
        this.grantContactPerEmailChanged = grantContactPerEmailChanged;
    }
    
    public Long getGrantContactPerEmailChangedBy() {
        return grantContactPerEmailChangedBy;
    }

    public void setGrantContactPerEmailChangedBy(Long grantContactPerEmailChangedBy) {
        this.grantContactPerEmailChangedBy = grantContactPerEmailChangedBy;
    }

    public String getGrantContactPerEmailNote() {
        return grantContactPerEmailNote;
    }

    public void setGrantContactPerEmailNote(String grantContactPerEmailNote) {
        this.grantContactPerEmailNote = grantContactPerEmailNote;
    }

    public boolean isGrantContactPerPhone() {
        return grantContactPerPhone;
    }

    public void setGrantContactPerPhone(boolean grantContactPerPhone) {
        this.grantContactPerPhone = grantContactPerPhone;
    }

    public Date getGrantContactPerPhoneChanged() {
        return grantContactPerPhoneChanged;
    }

    public void setGrantContactPerPhoneChanged(Date grantContactPerPhoneChanged) {
        this.grantContactPerPhoneChanged = grantContactPerPhoneChanged;
    }

    public Long getGrantContactPerPhoneChangedBy() {
        return grantContactPerPhoneChangedBy;
    }

    public void setGrantContactPerPhoneChangedBy(Long grantContactPerPhoneChangedBy) {
        this.grantContactPerPhoneChangedBy = grantContactPerPhoneChangedBy;
    }

    public String getGrantContactPerPhoneNote() {
        return grantContactPerPhoneNote;
    }

    public void setGrantContactPerPhoneNote(String grantContactPerPhoneNote) {
        this.grantContactPerPhoneNote = grantContactPerPhoneNote;
    }

    public boolean isGrantContactNone() {
        return grantContactNone;
    }

    public void setGrantContactNone(boolean grantContactNone) {
        this.grantContactNone = grantContactNone;
    }

    public Date getGrantContactNoneChanged() {
        return grantContactNoneChanged;
    }

    public void setGrantContactNoneChanged(Date grantContactNoneChanged) {
        this.grantContactNoneChanged = grantContactNoneChanged;
    }

    public Long getGrantContactNoneChangedBy() {
        return grantContactNoneChangedBy;
    }

    public void setGrantContactNoneChangedBy(Long grantContactNoneChangedBy) {
        this.grantContactNoneChangedBy = grantContactNoneChangedBy;
    }

    public String getGrantContactNoneNote() {
        return grantContactNoneNote;
    }

    public void setGrantContactNoneNote(String grantContactNoneNote) {
        this.grantContactNoneNote = grantContactNoneNote;
    }

    public boolean updateContactGrants(
        Long changedBy,
        boolean grantContactPerEmailValue,
        String grantContactPerEmailNoteValue,
        boolean grantContactPerPhoneValue,
        String grantContactPerPhoneNoteValue,
        boolean grantContactNoneValue,
        String grantContactNoneNoteValue,
        boolean newsletterConfirmationValue,
        String newsletterConfirmationNoteValue,
        boolean vipValue,
        String vipNoteValue) {
        
        boolean changed = false;
        Date changeDate = new Date(System.currentTimeMillis());
        if (grantContactPerEmail != grantContactPerEmailValue
                || grantContactPerEmailNote != grantContactPerEmailNoteValue) {
            grantContactPerEmail = grantContactPerEmailValue;
            grantContactPerEmailNote = grantContactPerEmailNoteValue;
            grantContactPerEmailChanged = changeDate;
            grantContactPerEmailChangedBy = changedBy;
            changed = true;
        }
        if (grantContactPerPhone != grantContactPerPhoneValue
                || grantContactPerPhoneNote != grantContactPerPhoneNoteValue) {
            grantContactPerPhone = grantContactPerPhoneValue;
            grantContactPerPhoneNote = grantContactPerPhoneNoteValue;
            grantContactPerPhoneChanged = changeDate;
            grantContactPerPhoneChangedBy = changedBy;
            changed = true;
        }
        if (grantContactNone != grantContactNoneValue
                || grantContactNoneNote != grantContactNoneNoteValue) {
            grantContactNone = grantContactNoneValue;
            grantContactNoneNote = grantContactNoneNoteValue;
            grantContactNoneChanged = changeDate;
            grantContactNoneChangedBy = changedBy;
            changed = true;
        }
        if (grantContactNone) {
            newsletterConfirmation = false;
        } else {
            newsletterConfirmation = newsletterConfirmationValue;
            newsletterConfirmationNote = newsletterConfirmationNoteValue;
        }
        vip = vipValue;
        vipNote = vipNoteValue;
        return changed;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public List<EmailAddress> getEmails() {
        return emails;
    }

    protected void setEmails(List<EmailAddress> emails) { // NO_UCD - hql
        this.emails = emails;
    }

    public void addEmail(
            Long createdBy,
            Long type,
            String emailAddress,
            boolean primary,
            Long aliasOf) throws ClientException {

        if (!EmailValidator.validate(emailAddress)) {
            throw new ClientException(ErrorCode.EMAIL_INVALID);
        }
        Long etypeId = type;
        if (isNotSet(type)) {
            if (isBusiness() || isContactPerson()) {
                etypeId = EmailAddress.BUSINESS;
            } else {
                etypeId = EmailAddress.PRIVATE;
            }
        }

        if (primary) {
            for (Iterator<EmailAddress> i = emails.iterator(); i.hasNext();) {
                EmailAddress ea = i.next();
                ea.setPrimary(false);
            }
        }
        EmailAddressImpl impl = new EmailAddressImpl(
                createdBy,
                getContactId(),
                etypeId,
                emailAddress,
                primary,
                aliasOf);
        emails.add(impl);
    }

    public String getEmail() {
        for (Iterator<EmailAddress> i = emails.iterator(); i.hasNext();) {
            EmailAddress ea = i.next();
            if (ea.isInternal() && ea.isPrimary()) {
                return emailAddress(ea.getEmail());
            }
        }
        for (Iterator<EmailAddress> i = emails.iterator(); i.hasNext();) {
            EmailAddress ea = i.next();
            if (ea.isPrimary()) {
                return emailAddress(ea.getEmail());
            }
        }
        if (!emails.isEmpty()) {
            return emailAddress(emails.get(0).getEmail());
        }
        return null;
    }

    private String emailAddress(String email) {
        return email == null ? null : email.trim().toLowerCase();
    }

    public boolean hasEmail(String emailAddress) {
        for (Iterator<EmailAddress> i = emails.iterator(); i.hasNext();) {
            EmailAddress ea = i.next();
            if (isSet(ea.getEmail()) && ea.getEmail().equalsIgnoreCase(emailAddress)) {
                return true;
            }
        }
        return false;
    }

    public void removeEmail(Long id) {
        for (Iterator<EmailAddress> i = emails.iterator(); i.hasNext();) {
            EmailAddress ea = i.next();
            if (ea.getId().equals(id)) {
                i.remove();
                break;
            }
        }
    }

    public List<EmailAddress> getEmailAliases(EmailAddress address) {
        List<EmailAddress> result = new ArrayList<>();
        if (address != null) {
            for (int i = 0, j = emails.size(); i < j; i++) {
                EmailAddress next = emails.get(i);
                if (next.isAlias()
                        && next.getAliasOf() != null
                        && next.getAliasOf().equals(address.getId())) {
                    result.add(next);
                }
            }
        }
        return result;
    }

    public List<Phone> getPhoneDeviceNumbers(Long device) {
        List<Phone> result = new ArrayList<>();
        for (int i = 0, j = phoneNumbers.size(); i < j; i++) {
            Phone next = phoneNumbers.get(i);
            if (next.getDevice().equals(device)) {
                result.add(next);
            }
        }
        return result;
    }

    public List<Phone> getPhoneNumbers() {
        return phoneNumbers;
    }

    protected void setPhoneNumbers(List<Phone> phones) { // NO_UCD - JPA
        this.phoneNumbers = phones;
    }

    public void addPhone(
            Long createdBy,
            Long device,
            Long type,
            String country,
            String prefix,
            String number,
            String note,
            boolean primary)
            throws ClientException {

        validatePhone(device, prefix, number, true);
        Long ptypeId = type;
        if (isNotSet(type)) {
            if (isBusiness() || isContactPerson()) {
                ptypeId = Phone.BUSINESS;
            } else {
                ptypeId = Phone.PRIVATE;
            }
        }

        if (!phoneExisting(device, country, prefix, number)) {
            if (primary) {
                for (int i = 0, j = phoneNumbers.size(); i < j; i++) {
                    Phone next = phoneNumbers.get(i);
                    if (next.getDevice().equals(device)) {
                        next.setPrimary(false);
                    }
                }
            }
            PhoneImpl phone = new PhoneImpl(
                    createdBy,
                    getContactId(),
                    device,
                    ptypeId,
                    country,
                    prefix,
                    number,
                    note,
                    primary);
            phoneNumbers.add(phone);

        } else {
            log.warn("addPhone() preventing an attempt to add an existing number [contactId="
                    + getContactId() + ", device=" + device + ", prefix=" + prefix
                    + ", number=" + number + "]");
        }
    }

    private boolean phoneExisting(Long device, String country, String prefix, String number) {
        String phonekey = PhoneUtil.createPhoneNumber(country, prefix, number);
        for (int i = 0, j = phoneNumbers.size(); i < j; i++) {
            Phone next = phoneNumbers.get(i);
            if (phonekey.equals(next.getPhoneKey()) && next.getDevice().equals(device)) {
                return true;
            }
        }
        return false;
    }

    public void updatePhone(
            Long updateBy,
            Phone phone,
            Long type,
            String country,
            String prefix,
            String number,
            String note,
            boolean primary) throws ClientException {

        validatePhone(phone.getDevice(), prefix, number, true);
        for (Iterator<Phone> i = phoneNumbers.iterator(); i.hasNext();) {
            Phone next = i.next();
            if (next.getId().equals(phone.getId())) {
                next.setChanged(new Date(System.currentTimeMillis()));
                next.setChangedBy(updateBy);
                next.setCountry(country);
                next.setNote(note);
                next.setNumber(number);
                next.setPrefix(prefix);
                if (isSet(type)) {
                    next.setType(type);
                }
                next.setPrimary(primary);

            } else if (primary && next.getDevice().equals(phone.getDevice())) {
                next.setPrimary(false);
            }
        }
    }

    public List<Phone> getFaxNumbers() {
        return getNumbers(PhoneType.FAX);
    }

    public Phone getFax() {
        return getPhone(getFaxNumbers());
    }

    @Override
    public Map<String, Object> getMapped() {
        Map<String, Object> map = super.getMapped();
        putIfExists(map, "address", getAddress().getMapped());
        putFormatted(map, "birthDate", getBirthDate());
        putIfExists(map, "contactId", getContactId());
        putIfExists(map, "displayName", getDisplayName());
        putIfExists(map, "email", getEmail());
        putIfExists(map, "fax", fetchMapped(getFax()));
        putIfExists(map, "firstName", getFirstName());
        putIfExists(map, "lastName", getLastName());
        putIfExists(map, "mobile", fetchMapped(getMobile()));
        putIfExists(map, "name", getName());
        putIfExists(map, "office", getOffice());
        putIfExists(map, "phone", fetchMapped(getPhone()));
        putIfExists(map, "position", getPosition());
        putIfExists(map, "reference", getReference());
        putIfExists(map, "salutation", fetchMapped(getSalutation()));
        putIfExists(map, "salutationDisplay", getSalutationDisplay());
        putIfExists(map, "section", getSection());
        putIfExists(map, "status", getStatus());
        putIfExists(map, "title", fetchMapped(getTitle()));
        putIfExists(map, "type", fetchMapped(getType()));
        putIfExists(map, "website", getWebsite());
        putIfExists(map, "contactClient", isClient());
        putIfExists(map, "contactEmployee", isEmployee());
        putIfExists(map, "contactBranchOffice", isBranchOffice());
        putIfExists(map, "contactCustomer", isCustomer());
        putIfExists(map, "contactSupplier", isSupplier());
        putIfExists(map, "contactInstaller", isInstaller());
        putIfExists(map, "contactUser", isUser());
        if (!isBusiness()) {
            map.put("contactTypeName", "private");
        } else {
            map.put("contactTypeName", "business");
        }
        return map;
    }
    
    private Map fetchMapped(Object o) {
        if (o instanceof Phone) {
            Phone p = (Phone) o;
            if (isSet(p.getId()) || isSet(p.getNumber())) {
                return p.getMapped();
            }
        } else if (o instanceof Mappable) {
            return ((Mappable) o).getMapped();
        }
        return null;
    }

    public List<Phone> getMobiles() {
        return getNumbers(PhoneType.MOBILE);
    }

    public Phone getMobile() {
        return getPhone(getMobiles());
    }

    public List<Phone> getPhones() {
        return getNumbers(PhoneType.PHONE);
    }

    public Phone getPhone() {
        return getPhone(getPhones());
    }

    public void update(Contact otherValue) {
        super.update(otherValue);
        website = otherValue.getWebsite();
        employee = otherValue.isEmployee();
        customer = otherValue.isCustomer();
        supplier = otherValue.isSupplier();
        installer = otherValue.isInstaller();
        other = otherValue.isOther();
        // we create new lists cause of hessian serialization problem with hibernate collections
        phoneNumbers = new ArrayList<>();
        if (!otherValue.getPhoneNumbers().isEmpty()) {
            for (int i = 0, j = otherValue.getPhoneNumbers().size(); i < j; i++) {
                phoneNumbers.add(otherValue.getPhoneNumbers().get(i));
            }
        }
        emails = new ArrayList<>();
        if (!otherValue.getEmails().isEmpty()) {
            for (int i = 0, j = otherValue.getEmails().size(); i < j; i++) {
                emails.add(otherValue.getEmails().get(i));
            }
        }
        client = otherValue.isClient();
        vip = otherValue.isVip();
        newsletterConfirmation = otherValue.isNewsletterConfirmation();
    }

    @Override
    public Element getXML() {
        return getXML("contact");
    }

    @Override
    public Element getXML(String rootElementName) {
        Element contactValues = super.getXML(rootElementName);
        Element address = contactValues.getChild("address");
        if (address != null && getContactId() != null) {
            address.addContent(new Element("contactId").setText(getContactId().toString()));
        }
        if (website != null) {
            contactValues.addContent(new Element("website").setText(website));
        }
        return contactValues;
    }

    private Phone getPhone(List<Phone> plist) {
        for (Iterator<Phone> i = plist.iterator(); i.hasNext();) {
            Phone p = i.next();
            if (p.isPrimary()) {
                return p;
            }
        }
        if (!plist.isEmpty()) {
            return plist.get(0);
        }
        return new PhoneImpl();
    }

    private List<Phone> getNumbers(Long device) {
        List<Phone> list = new ArrayList<>();
        for (int i = 0, j = phoneNumbers.size(); i < j; i++) {
            Phone next = phoneNumbers.get(i);
            if (next.getDevice().equals(device)) {
                list.add(next);
            }
        }
        return list;
    }

    private void validatePhone(Long device, String prefix, String number, boolean required)
            throws ClientException {
        if (PhoneType.FAX.equals(device)) {
            PhoneUtil.validateFax(prefix, number, required);
        } else if (PhoneType.MOBILE.equals(device)) {
            PhoneUtil.validateMobile(prefix, number, required);
        } else {
            PhoneUtil.validatePhone(prefix, number, required);
        }
    }

    @Override
    protected void createCommunications(Element contact) {
        Element phoneNumber = contact.getChild("phoneNumber");
        phoneNumbers = new ArrayList<>();
        if (phoneNumber != null) {
            PhoneImpl pobj = new PhoneImpl();
            pobj.setId(JDOMUtil.fetchLong(phoneNumber, "id"));
            pobj.setCountry(phoneNumber.getChildText("country"));
            pobj.setPrefix(phoneNumber.getChildText("prefix"));
            pobj.setNumber(phoneNumber.getChildText("number"));
            phoneNumbers.add(pobj);
        }
        Element faxNumber = contact.getChild("faxNumber");
        if (faxNumber != null) {
            PhoneImpl fobj = new PhoneImpl();
            fobj.setId(JDOMUtil.fetchLong(faxNumber, "id"));
            fobj.setCountry(faxNumber.getChildText("country"));
            fobj.setPrefix(faxNumber.getChildText("prefix"));
            fobj.setNumber(faxNumber.getChildText("number"));
            phoneNumbers.add(fobj);
        }
        Element mobileNumber = contact.getChild("mobileNumber");
        if (mobileNumber != null) {
            PhoneImpl mobj = new PhoneImpl();
            mobj.setId(JDOMUtil.fetchLong(mobileNumber, "id"));
            mobj.setCountry(mobileNumber.getChildText("country"));
            mobj.setPrefix(mobileNumber.getChildText("prefix"));
            mobj.setNumber(mobileNumber.getChildText("number"));
            phoneNumbers.add(mobj);
        }
        emails = new ArrayList<>();
        String email = contact.getChildText("email");
        if (email != null && email.length() > 0) {
            emails.add(new EmailAddressImpl(email));
        }
    }

    @Override
    public Object clone() {
        return new ContactImpl(this);
    }
}
