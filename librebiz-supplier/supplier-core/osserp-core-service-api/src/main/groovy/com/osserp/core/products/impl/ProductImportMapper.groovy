/**
 *
 * Copyright (C) 2013, 2017 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 1, 2017 
 * 
 */
package com.osserp.core.products.impl

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ds.ImportMapper
import com.osserp.common.util.DateUtil
import com.osserp.common.util.NumberUtil
import com.osserp.common.util.StringUtil

import com.osserp.core.api.ProductImportVO

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class ProductImportMapper extends ImportMapper {

    private static Logger logger = LoggerFactory.getLogger(ProductImportMapper.class.getName())

    static boolean isMappable(Map<String, String> productRow) {
        productRow && productRow[rowName]
    }

    @Override
    Object map(Map<String, String> product) {
        ProductImportVO obj = new ProductImportVO()

        String idString = fetchString(product[rowProductId])
        Long productId = createLong(idString)
        if (productId) {
            obj.primaryKey = productId
        }
        obj.name = fetchString(product[rowName])
        String matchcode = fetchString(product[rowMatchcode])
        if (matchcode) {
            obj.matchcode = matchcode
        } else {
            obj.matchcode = StringUtil.cut(obj.name, 127, false)
        }

        obj.description = fetchString(product[rowDescription])
        obj.type = fetchString(product[rowTypeName])
        obj.typeId = createLong(product[rowTypeId])
        obj.group = fetchString(product[rowGroupName])
        obj.groupId = createLong(product[rowGroupId])
        obj.category = fetchString(product[rowCategoryName])
        obj.categoryId = createLong(product[rowCategoryId])

        obj.width = createInteger(product[rowWidth])
        obj.height = createInteger(product[rowHeight])
        obj.lenght = createInteger(product[rowDepth])
        obj.measurementUnit = fetchString(product[rowDimensionUnit])

        obj.weight = createDouble(product[rowWeight])
        obj.weightUnit = fetchString(product[rowWeightUnit])

        obj.purchasePrice = createDoubleValue(product[rowEstimatedPurchasePrice])
        obj.consumerPrice = createDoubleValue(product[rowConsumerPrice])
        obj.resellerPrice = createDoubleValue(product[rowResellerPrice])
        obj.partnerPrice = createDoubleValue(product[rowPartnerPrice])

        obj.quantityUnit = fetchString(product[rowQuantityUnit])

        Double pow = createDouble(product[rowPower])
        if (pow) {
            obj.power = pow
            obj.powerUnit = fetchString(product[rowPowerUnit])
        }
        obj.manufacturer = fetchString(product[rowManufacturer])
        obj.color = fetchString(product[rowColorName])

        obj.rangeStart = createLong(product[rowRangeStart])
        obj.rangeEnd = createLong(product[rowRangeEnd])

        obj.bundle = fetchBoolean(product[rowBundle])
        obj.kanban = fetchBoolean(product[rowKanban])
        obj.nullable = fetchBoolean(product[rowNullable])
        obj.plant = fetchBoolean(product[rowPlant])
        obj.quantityByPlant = fetchBoolean(product[rowQuantityByPlant])
        obj.stockId = fetchBoolean(product[rowStock])
        obj.stockAffecting = fetchBoolean(product[rowStockAffecting])

        return obj
    }

    static String rowProductId = 'product_id'
    static String rowName = 'product'
    static String rowMatchcode = 'matchcode'
    static String rowManufacturer = 'manufacturer'
    static String rowDescription = 'description'

    static String rowTypeId = 'type_id'
    static String rowTypeName = 'type'
    static String rowGroupId = 'group_id'
    static String rowGroupName = 'group'
    static String rowCategoryId = 'category_id'
    static String rowCategoryName = 'category'
    static String rowRangeStart = 'range_start'
    static String rowRangeEnd = 'range_end'

    static String rowWidth = 'width'
    static String rowHeight = 'height'
    static String rowDepth = 'depth'
    static String rowDimensionUnit = 'dunit'
    static String rowWeight = 'weight'
    static String rowWeightUnit = 'wunit'

    static String rowQuantityUnitId = 'qunit_id'
    static String rowQuantityUnit = 'qunit'

    static String rowPower = 'power'
    static String rowPowerUnit = 'punit'
    static String rowPowerUnitId = 'punit_id'
    
    static String rowColorId = 'color_id'
    static String rowColorName = 'color'

    static String rowEstimatedPurchasePrice = 'ep'
    static String rowConsumerPrice = 'cp'
    static String rowResellerPrice = 'rp'
    static String rowPartnerPrice = 'pp'

    static String rowBundle = 'bundle'
    static String rowKanban = 'kanban'
    static String rowNullable = 'nullable'
    static String rowPlant = 'pkg'
    static String rowQuantityByPlant = 'qty_pkg'
    static String rowStock = 'stock_id'
    static String rowStockAffecting = 'stock'

    String[] knownColumns = [
        rowProductId,
        rowName,
        rowMatchcode,
        rowManufacturer,
        rowDescription,
        rowTypeId,
        rowTypeName,
        rowGroupId,
        rowGroupName,
        rowCategoryId,
        rowCategoryName,
        rowWidth,
        rowHeight,
        rowDimensionUnit,
        rowDepth,
        rowWeight,
        rowWeightUnit,
        rowPower,
        rowPowerUnit,
        rowPowerUnitId,
        rowQuantityUnitId,
        rowQuantityUnit,
        rowColorId,
        rowColorName,
        rowEstimatedPurchasePrice,
        rowConsumerPrice,
        rowResellerPrice,
        rowPartnerPrice,
        rowRangeStart,
        rowRangeEnd,
        rowBundle,
        rowKanban,
        rowNullable,
        rowPlant,
        rowQuantityByPlant,
        rowStock,
        rowStockAffecting
    ]

}
