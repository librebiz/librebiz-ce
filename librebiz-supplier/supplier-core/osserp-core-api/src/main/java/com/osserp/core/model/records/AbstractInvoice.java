/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 29-Jan-2007 18:33:43 
 * 
 */
package com.osserp.core.model.records;

import java.util.Date;

import com.osserp.common.Constants;
import com.osserp.common.util.DateUtil;

import com.osserp.core.contacts.ClassifiedContact;
import com.osserp.core.employees.Employee;
import com.osserp.core.finance.Invoice;
import com.osserp.core.finance.Order;
import com.osserp.core.finance.PaymentCondition;
import com.osserp.core.finance.Record;
import com.osserp.core.finance.RecordPaymentAgreement;
import com.osserp.core.finance.RecordType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractInvoice extends AbstractPaymentAwareRecord implements Invoice {

    private PaymentCondition paymentCondition = null;
    private Date maturity;
    private boolean canceled = false;
    private boolean partial = false;
    private Double overrideNetPaidAmount = Constants.DOUBLE_NULL;
    private Double overrideTaxPaidAmount = Constants.DOUBLE_NULL;
    private Double overrideGrossPaidAmount = Constants.DOUBLE_NULL;

    /**
     * Default constructor required by Serializable
     */
    protected AbstractInvoice() {
        super();
    }

    /**
     * Creates a new -none order depending- invoice
     * @param id
     * @param company
     * @param branchId
     * @param type
     * @param reference
     * @param contact
     * @param createdBy
     * @param businessCaseId
     * @param taxRate
     * @param reducedTaxRate
     * @param itemsChangeable
     * @param itemsEditable
     */
    protected AbstractInvoice(
            Long id,
            Long company,
            Long branchId,
            RecordType type,
            Long reference,
            ClassifiedContact contact,
            Employee createdBy,
            Long businessCaseId,
            Double taxRate,
            Double reducedTaxRate,
            boolean itemsChangeable,
            boolean itemsEditable) {
        super(
                id,
                company,
                branchId,
                type,
                reference,
                contact,
                createdBy,
                businessCaseId,
                taxRate,
                reducedTaxRate,
                itemsChangeable,
                itemsEditable);
        setTaxPoint(new Date(System.currentTimeMillis()));
    }

    /**
     * Creates a new invoice by other record.
     * @param id
     * @param type
     * @param reference
     * @param createdBy
     * @param copyReferenceId indicates if id of reference should be set as reference of new record
     * @param changeable indictes if items should be changeable after create by reference.items 
     */
    protected AbstractInvoice(
            Long id,
            RecordType type,
            Record reference,
            Employee createdBy,
            boolean copyReferenceId,
            boolean changeable) {
        super(id, type, reference, createdBy, copyReferenceId, changeable);
        if (reference instanceof Invoice) {
            Invoice invoice = (Invoice) reference;
            paymentCondition = invoice.getPaymentCondition();
        }
        setTaxPoint(new Date(System.currentTimeMillis()));
    }

    /**
     * Creates a new invoice by order
     * @param id
     * @param type
     * @param order
     * @param createdBy
     */
    protected AbstractInvoice(
            Long id,
            RecordType type,
            Order order,
            Employee createdBy) {
        super(
                id,
                order.getCompany(),
                order.getBranchId(),
                type,
                order.getId(),
                order.getContact(),
                createdBy,
                order.getBusinessCaseId(),
                order.getAmounts().getTaxRate(),
                order.getAmounts().getReducedTaxRate(),
                false,
                false);
        setTaxPoint(new Date(System.currentTimeMillis()));
        if (order.isTaxFree() && order.getTaxFreeId() != null) {
            setTaxFree(true);
            setTaxFreeId(order.getTaxFreeId());
        }
        if (order.getPersonId() != null) {
            setPersonId(order.getPersonId());
        }
        setCurrency(order.getCurrency());
        initPayment(order.getPaymentAgreement());
    }

    /**
     * Initializes payment condition and maturity by payment agreement
     * @param paymentAgreement
     */
    protected void initPayment(RecordPaymentAgreement paymentAgreement) {
        if (paymentAgreement != null) {
            if (paymentAgreement.getPaymentCondition() != null && paymentAgreement.getPaymentCondition().getPaymentTarget() != null) {
                setMaturity(DateUtil.addDays(
                        DateUtil.getCurrentDate(),
                        paymentAgreement.getPaymentCondition().getPaymentTarget().intValue()));
            } else {
                setMaturity(DateUtil.addDays(
                        DateUtil.getCurrentDate(),
                        Invoice.DEFAULT_MATURITY));
            }
            if (paymentAgreement.getPaymentCondition() != null) {
                paymentCondition = paymentAgreement.getPaymentCondition();
            }
        }
    }

    public PaymentCondition getPaymentCondition() {
        return paymentCondition;
    }

    public void setPaymentCondition(PaymentCondition paymentCondition) {
        this.paymentCondition = paymentCondition;
    }

    public Date getMaturity() {
        return maturity;
    }

    public void setMaturity(Date maturity) {
        this.maturity = maturity;
    }

	/**
     * Instead of determing canceled flag by status as default isCanceled() does, <br/>
     * invoices are using a persistent property for this purpose
     * @return canceled
     */
    @Override
    public boolean isCanceled() {
        return canceled;
    }

    /**
     * An invoices provides a persistent property for saving canceled flag while<br/> 
     * default records are using the record status value to determine the result of
     * isCanceled(). <br/>
     * This method sets the persistent property
     * @param canceled
     */
    public void setCanceled(boolean canceled) {
        this.canceled = canceled;
    }

    public boolean isDownpayment() {
        return false;
    }

    public boolean isPartial() {
        return partial;
    }

    protected void setPartial(boolean partial) {
        this.partial = partial;
    }

    public Double getOverrideNetPaidAmount() {
        return overrideNetPaidAmount;
    }

    public void setOverrideNetPaidAmount(Double overrideNetPaidAmount) {
        this.overrideNetPaidAmount = overrideNetPaidAmount;
    }

    public Double getOverrideTaxPaidAmount() {
        return overrideTaxPaidAmount;
    }

    public void setOverrideTaxPaidAmount(Double overrideTaxPaidAmount) {
        this.overrideTaxPaidAmount = overrideTaxPaidAmount;
    }

    public Double getOverrideGrossPaidAmount() {
        return overrideGrossPaidAmount;
    }

    public void setOverrideGrossPaidAmount(Double overrideGrossPaidAmount) {
        this.overrideGrossPaidAmount = overrideGrossPaidAmount;
    }

    public boolean isOverridePaidAmount() {
        return (isSet(overrideNetPaidAmount) && isSet(overrideGrossPaidAmount) && isSet(overrideTaxPaidAmount));
    }

    public void overridePaidAmounts(Double netPaidAmount, Double taxPaidAmount, Double grossPaidAmount) {
        this.overrideNetPaidAmount = netPaidAmount;
        this.overrideTaxPaidAmount = taxPaidAmount;
        this.overrideGrossPaidAmount = grossPaidAmount;
    }
}
