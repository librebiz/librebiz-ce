/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 *
 * Created on Oct 6, 2016
 *
 */
package com.osserp.gui.purchasing

import com.osserp.core.finance.Record
import com.osserp.core.finance.RecordDisplay

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class PurchaseOrderSearchView extends AbstractPurchaseRecordSearchView {
    
    PurchaseOrderSearchView() {
        super();
        searchPropertyNames = [
            orderByColumn: 'purchaseOrderSearchOrderByColumn',
            orderByDescendent: 'purchaseOrderSearchOrderByDescendent',
            ignoreFetchSize: 'purchaseOrderSearchFetchSizeDisabled',
            fetchSize: 'purchaseOrderSearchFetchSize'
        ]
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            reload()
        }
    }

    @Override
    protected String getQueryContextName() {
        "purchase_orders"
    }

    @Override
    String getSelectionExitDefault() {
        'purchaseOrderSearchDisplay'
    }

    @Override
    String getSelectionTargetDefault() {
        // /purchaseOrder.do?method=display&exit=purchaseOrderSearchDisplay&id=200867
        '/purchaseOrder.do?method=display'
    }

    protected String getInitialSearchPattern() {
        return null
    }

    protected List<RecordDisplay> filterMatching(List<RecordDisplay> result) {
        for (Iterator<RecordDisplay> i = result.iterator(); i.hasNext();) {
            RecordDisplay next = i.next()
            if (next.id.toString() != searchRequest?.pattern) {
                if (Record.STAT_CLOSED.equals(next.getStatus())
                    || Record.STAT_CANCELED.equals(next.getStatus())) {
                    i.remove()
                }
            }
        }
        return result
    }
}
