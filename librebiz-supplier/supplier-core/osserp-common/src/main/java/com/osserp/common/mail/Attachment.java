/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jan 21, 2005 
 * 
 */
package com.osserp.common.mail;

import com.osserp.common.Entity;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Attachment extends Entity {

    /**
     * Returns an optional content id
     * @return contentID
     */
    String getContentID();

    /**
     * Returns an optional content type
     * @return contentType
     */
    String getContentType();

    /**
     * Returns the displayed file name
     * @return fileName.
     */
    String getFileName();

    /**
     * Sets the file name to display
     * @param fileName
     */
    void setFileName(String fileName);

    /**
     * @return fileSize
     */
    long getFileSize();

    /**
     * Returns the mimeType
     * @return mimeType.
     */
    String getMimeType();

    /**
     * Sets the mimeType
     * @param mimeType
     */
    void setMimeType(String mimeType);

    /**
     * Returns the file to attach as byte array as alternative to provide the attachment via file system.
     * @return bytes or null if attachment is provided as file. See getFile().
     */
    byte[] getBytes();

    /**
     * Sets the file to attach as byte array
     * @param bytes
     */
    void setBytes(byte[] bytes);

    /**
     * Returns the file name and path if attachment is any file on a filesystem
     * @return file or null if attachment is provided as binary. See getBytes().
     */
    String getFile();

    /**
     * Sets the file name and path
     * @param file
     */
    void setFile(String file);
}
