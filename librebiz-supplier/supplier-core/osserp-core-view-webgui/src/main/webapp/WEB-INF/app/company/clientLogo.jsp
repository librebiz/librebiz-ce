<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<o:resource/>
<v:view viewName="clientLogoView"/>
<c:set var="dmsDocumentView" scope="request" value="${sessionScope.clientLogoView}"/>
<c:set var="dmsDocumentUrl" scope="request" value="/company/clientLogo"/>
<c:set var="dmsDeletePermissions" scope="request" value="executive,client_logo_admin"/>
<c:set var="dmsReferencePermissions" scope="request" value="executive,client_logo_admin"/>
<c:set var="dmsReferencePermissions" scope="request" value="executive,project_billing_delete,accounting"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="title"><o:displayTitle/></tiles:put>
	<tiles:put name="headline">
		<fmt:message key="companyLogo"/> <o:out value="${view.client.name}"/>
	</tiles:put>
	<tiles:put name="headline_right">
		<v:navigation/>
	</tiles:put>
	<tiles:put name="content" type="string">
        <div class="content-area">
            <div class="row">
                <c:import url="${viewdir}/dms/_commonDocumentUpload.jsp"/>
            </div>
        </div>
	</tiles:put>
</tiles:insert>

