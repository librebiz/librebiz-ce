/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 05-Feb-2007 14:51:06 
 * 
 */
package com.osserp.core.service.records;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.util.CollectionUtil;
import com.osserp.core.Comparators;
import com.osserp.core.dao.PaymentConditions;
import com.osserp.core.finance.PaymentCondition;
import com.osserp.core.finance.PaymentConditionManager;
import com.osserp.core.service.impl.AbstractService;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class PaymentConditionManagerImpl extends AbstractService implements PaymentConditionManager {
    private static Logger log = LoggerFactory.getLogger(PaymentConditionManagerImpl.class.getName());

    private PaymentConditions paymentConditions = null;

    /**
     * Creates a new paymentConditionManager
     * @param paymentConditions
     */
    public PaymentConditionManagerImpl(PaymentConditions paymentConditions) {
        super();
        this.paymentConditions = paymentConditions;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.finance.PaymentConditionManager#findPaymentConditions()
     */
    public List<PaymentCondition> findAll() {
        return CollectionUtil.sort(
                paymentConditions.getPaymentConditions(),
                Comparators.createOptionNameComparator(false));
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.finance.PaymentConditionManager#addPaymentCondition(java.lang.String, java.lang.Integer, boolean, boolean, java.lang.Double,
     * java.lang.Integer, java.lang.Double, java.lang.Integer)
     */
    public void addPaymentCondition(
            String name,
            Integer paymentTarget,
            boolean paid,
            boolean withDiscount,
            Double discount1,
            Integer discount1Target,
            Double discount2,
            Integer discount2Target) throws ClientException {

        PaymentCondition pc = paymentConditions.create(
                name,
                paymentTarget,
                paid,
                withDiscount,
                discount1,
                discount1Target,
                discount2,
                discount2Target);
        if (log.isDebugEnabled()) {
            log.debug("addPaymentCondition() done [id=" + pc.getId() 
                    + ", name=" + pc.getName() + "]");
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.osserp.core.finance.PaymentConditionManager#updatePaymentCondition(com.osserp.core.finance.PaymentCondition, java.lang.String,
     * java.lang.Integer, boolean, boolean, java.lang.Double, java.lang.Integer, java.lang.Double, java.lang.Integer)
     */
    public void updatePaymentCondition(
            PaymentCondition paymentCondition,
            String name,
            Integer paymentTarget,
            boolean donation,
            boolean paid,
            boolean withDiscount,
            Double discount1,
            Integer discount1Target,
            Double discount2,
            Integer discount2Target) throws ClientException {

        if (isNotSet(name)) {
            throw new ClientException(ErrorCode.NAME_MISSING);
        }

        List<PaymentCondition> existing = findAll();
        for (int i = 0, j = existing.size(); i < j; i++) {
            PaymentCondition con = existing.get(i);

            if (!con.getId().equals(paymentCondition.getId())) {
                if (con.getName().equals(name)) {
                    throw new ClientException(ErrorCode.NAME_EXISTS);
                }
            }
        }

        paymentCondition.setName(name);
        paymentCondition.setPaymentTarget(paymentTarget);
        paymentCondition.setDonation(donation);
        paymentCondition.setPaid(paid);
        paymentCondition.setWithDiscount(withDiscount);
        paymentCondition.setDiscount1(discount1);
        paymentCondition.setDiscount1Target(discount1Target);
        paymentCondition.setDiscount2(discount2);
        paymentCondition.setDiscount2Target(discount2Target);
        paymentConditions.save(paymentCondition);
    }
}
