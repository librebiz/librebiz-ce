SET search_path = osserp, pg_catalog;

--
-- Name: v_sales_monitoring; Type: VIEW; Schema: osserp; Owner: osserp
--

DROP VIEW osserp.v_sales_monitoring;

CREATE VIEW osserp.v_sales_monitoring AS
 SELECT v_sales_list_fcs.salutation,
    v_sales_list_fcs.firstname,
    v_sales_list_fcs.lastname,
    v_sales_list_fcs.contact_type,
    v_sales_list_fcs.id,
    v_sales_list_fcs.plan_id,
    v_sales_list_fcs.type_id,
    v_sales_list_fcs.customer_id,
    v_sales_list_fcs.status,
    v_sales_list_fcs.created,
    v_sales_list_fcs.created_by,
    v_sales_list_fcs.manager_id,
    v_sales_list_fcs.manager_sub_id,
    v_sales_list_fcs.sales_id,
    v_sales_list_fcs.branch_id,
    v_sales_list_fcs.name,
    v_sales_list_fcs.city,
    v_sales_list_fcs.plant_capacity,
    v_sales_list_fcs.stopped,
    v_sales_list_fcs.cancelled,
    v_sales_list_fcs.installer_id,
    v_sales_list_fcs.installation_days,
    v_sales_list_fcs.installation_date,
    v_sales_list_fcs.delivery_date,
    v_sales_list_fcs.confirmation_date,
    v_sales_list_fcs.fcs_id,
    v_sales_list_fcs.action_created,
    v_sales_list_fcs.action_created_by,
    v_sales_list_fcs.action_cancels_other,
    v_sales_list_fcs.action_canceled_by,
    v_sales_list_fcs.action_id,
    v_sales_list_fcs.action_note,
    v_sales_list_fcs.action_name,
    v_sales_list_fcs.action_order_id,
    v_sales_list_fcs.action_status,
    v_sales_list_fcs.action_group,
    v_sales_list_fcs.action_cancels_sales,
    v_sales_list_fcs.action_stops_sales,
    v_sales_list_fcs.action_starts_up,
    v_sales_list_fcs.action_manager_selecting,
    v_sales_list_fcs.action_initializing,
    v_sales_list_fcs.action_releaeseing,
    v_sales_list_fcs.requests_dp,
    v_sales_list_fcs.response_dp,
    v_sales_list_fcs.requests_di,
    v_sales_list_fcs.response_di,
    v_sales_list_fcs.requests_fi,
    v_sales_list_fcs.response_fi,
    v_sales_list_fcs.action_closing_delivery,
    v_sales_list_fcs.action_enabling_delivery,
    v_sales_list_fcs.action_partial_delivery,
    v_sales_list_fcs.action_confirming,
    v_sales_list_fcs.action_verifying,
    v_sales_list_fcs.action_partial_payment,
    osserp.get_sales_invoice_open_amount(v_sales_list_fcs.id) AS open_amount,
    v_sales_list_fcs.origin_id,
    v_sales_list_fcs.origin_type_id,
    v_sales_list_fcs.company_id,
    v_sales_list_fcs.request_created,
    v_sales_list_fcs.street,
    v_sales_list_fcs.zipcode,
    v_sales_list_fcs.accounting_ref,
    v_sales_list_fcs.shipping_id,
    v_sales_list_fcs.payment_id
   FROM osserp.v_sales_list_fcs
  WHERE ((v_sales_list_fcs.cancelled = false) AND (v_sales_list_fcs.status < 100))
  ORDER BY v_sales_list_fcs.id, v_sales_list_fcs.action_created DESC;

--
-- Name: get_sales_monitoring(text); Type: FUNCTION; Schema: osserp; Owner: osserp
--

CREATE OR REPLACE FUNCTION osserp.get_sales_monitoring(selection text) RETURNS SETOF osserp.tp_sales_fcs
    LANGUAGE plpgsql
    AS $$
DECLARE
    obj         osserp.tp_sales_fcs%ROWTYPE;
    selectionid bigint;
BEGIN
SELECT id INTO selectionid FROM osserp.request_type_selections where name = selection;
IF selectionid IS NOT NULL THEN
    FOR obj IN
        SELECT
            salutation,
            firstname,
            lastname,
            contact_type,
            id,
            plan_id,
            type_id,
            customer_id,
            status,
            created,
            created_by,
            manager_id,
            manager_sub_id,
            sales_id,
            branch_id,
            name,
            city,
            plant_capacity,
            stopped,
            cancelled,
            installer_id,
            installation_days,
            installation_date,
            delivery_date,
            confirmation_date,
            fcs_id,
            action_created,
            action_created_by,
            action_cancels_other,
            action_canceled_by,
            action_id,
            action_note,
            action_name,
            action_order_id,
            action_status,
            action_group,
            action_cancels_sales,
            action_stops_sales,
            action_starts_up,
            action_manager_selecting,
            action_initializing,
            action_releaeseing,
            requests_dp,
            response_dp,
            requests_di,
            response_di,
            requests_fi,
            response_fi,
            action_closing_delivery,
            action_enabling_delivery,
            action_partial_delivery,
            action_confirming,
            action_verifying,
            action_partial_payment,
            origin_id,
            origin_type_id,
            company_id,
            request_created,
            street,
            zipcode,
            accounting_ref,
            shipping_id,
            payment_id
        FROM osserp.v_sales_list_fcs
        WHERE cancelled = false AND status < 100
        AND (type_id IN (SELECT type_id FROM osserp.request_type_selection_relations WHERE selection_id = selectionid))
        ORDER BY id, action_created DESC LOOP
        RETURN NEXT obj;
    END LOOP;
END IF;
RETURN;
END;
$$;

UPDATE system_properties SET displayable = true WHERE name = 'contactImportAvailable';
UPDATE system_properties SET pvalue = '0.15.18', ptype = 'java.lang.String' WHERE name = 'dbmsSchemaVersion';
