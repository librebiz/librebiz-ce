<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<c:set var="view" value="${sessionScope.recordView}"/>
<c:set var="record" value="${view.record}"/>
<c:set var="url" value="${requestScope.baseUrl}"/>
<c:if test="${!empty requestScope.supplierReferenceNumberPermissionGrant}">
<c:set var="changePermissionGrant" value="true"/>
</c:if>
<c:choose>
<c:when test="${view.supplierReferenceNumberEditMode}">
<o:form name="recordForm" url="${url}">
	<input type="hidden" name="method" value="updateSupplierReferenceNumber" />
	<div class="recordLabelValue">
		<fmt:message key="supplierReferenceNumber"/>:<span style="margin-left: 20px;"><input type="text" name="number" id="numberValue" value="<o:out value="${record.supplierReferenceNumber}"/>" style="width:200px;" /></span>
        <span style="margin-left: 6px;"><fmt:message key="withDate"/></span>
        <span style="margin-left: 6px;"><input type="text" name="createdDate" id="createdDate" style="width:100px;" value="<o:date value="${record.supplierReferenceDate}"/>"/> <o:datepicker input="createdDate"/></span>
		<span style="margin-left: 10px;">
			<a href="<c:url value="${url}"><c:param name="method" value="disableSupplierReferenceNumberEditMode"/></c:url>" title="<fmt:message key="exitEdit"/>"><o:img name="backIcon" styleClass="bigicon"/></a>
		</span>
		<input type="image" name="method" src="<c:url value="${applicationScope.saveIcon}"/>" value="updateSupplierReferenceNumber" onclick="setMethod('updateSupplierReferenceNumber');" style="margin-left: 5px; margin-right: 5px;" class="bigicon" title="<fmt:message key="saveInput"/>"/>
	</div>
</o:form>
</c:when>
<c:otherwise>
<div class="recordLabelValue">
<c:if test="${!(empty record.supplierReferenceNumber and record.unchangeable and !view.supplierReferenceNumberEditModeDisplayedWhenClosed)}">
	<c:choose>
		<c:when test="${changePermissionGrant and (!record.unchangeable or view.supplierReferenceNumberEditModeDisplayedWhenClosed)}">
			<a href="<c:url value="${url}"><c:param name="method" value="enableSupplierReferenceNumberEditMode"/></c:url>" title="<fmt:message key="change"/>"><fmt:message key="supplierReferenceNumber"/>:</a>
			<c:set var="setLabelDone" value="true"/>
		</c:when>
		<c:otherwise>
			<o:permission role="purchasing,accounting,executive" info="permissionChangePurchasingSupplierReferenceNumber">
				<c:if test="${!record.unchangeable or view.supplierReferenceNumberEditModeDisplayedWhenClosed}">
					<a href="<c:url value="${url}"><c:param name="method" value="enableSupplierReferenceNumberEditMode"/></c:url>" title="<fmt:message key="change"/>"><fmt:message key="supplierReferenceNumber"/>:</a>
					<c:set var="setLabelDone" value="true"/>
				</c:if>
			</o:permission>
		</c:otherwise>
	</c:choose>
	<c:if test="${!setLabelDone}"><fmt:message key="supplierReferenceNumber"/>:</c:if>
	<span style="margin-left: 10px;">
        <c:choose><c:when test="${empty record.supplierReferenceNumber}"><fmt:message key="none"/></c:when><c:otherwise><o:out value="${record.supplierReferenceNumber}"/></c:otherwise></c:choose>
        <c:if test="${!empty record.supplierReferenceDate}"><fmt:message key="withDate"/> <o:date value="${record.supplierReferenceDate}"/></c:if>
	</span>
</c:if>
</div>
</c:otherwise>
</c:choose>
