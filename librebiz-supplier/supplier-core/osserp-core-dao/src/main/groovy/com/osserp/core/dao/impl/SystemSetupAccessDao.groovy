/**
 *
 * Copyright (C) 2014 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 15, 2014 
 * 
 */
package com.osserp.core.dao.impl

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.hibernate.SessionFactory

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.dao.DataSourceService

import com.osserp.core.dao.SystemSetupAccess
import com.osserp.core.model.system.SystemSetupImpl
import com.osserp.core.system.SystemSetup

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class SystemSetupAccessDao extends DataSourceService implements SystemSetupAccess {
    private static Logger logger = LoggerFactory.getLogger(SystemSetupAccessDao.class.getName())

    SessionFactory sessionFactory

    SystemSetup getActiveSetup() throws ClientException {
        String hql = "from ${SystemSetupImpl.class.getName()} o where o.endOfLife = false"
        List result = sessionFactory.getCurrentSession().createQuery(hql)
        if (result.isEmpty()) {
            logger.warn('getActiveSetup: did not find config, please insert one [table=system_setup]')
            throw new ClientException(ErrorCode.SETUP_DB_CONFIG)
        }
        result.get(0)
    }

    public SystemSetup resetSetup(SystemSetup setup) {
        setup.resetInput()
        updateSetup(setup)
    }

    SystemSetup updateSetup(SystemSetup setup) {
        sessionFactory.getCurrentSession().saveOrUpdate(SystemSetupImpl.class.getName(), setup)
        sessionFactory.getCurrentSession().load(SystemSetupImpl.class.getName(), setup.id)
    }
}
