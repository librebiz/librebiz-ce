/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 16, 2016 
 * 
 */
package com.osserp.core.model.system;

import com.osserp.common.beans.AbstractOption;

import com.osserp.core.system.SystemKeyType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SystemKeyTypeImpl extends AbstractOption implements SystemKeyType {

    private String contextName;
    
    private boolean contextOnly;
    private boolean nameSupported;
    private boolean nameRequired;

    private String contextUrl;
    private String contextPermissions;

    protected SystemKeyTypeImpl() {
        super();
    }
    
    public SystemKeyTypeImpl(String contextName, String name, boolean nameRequired) {
        super(name);
        this.contextName = contextName;
        this.nameRequired = nameRequired;
        if (isSet(name)) {
            nameSupported = true;
        }
    }
    
    public String getContextName() {
        return contextName;
    }

    public void setContextName(String contextName) {
        this.contextName = contextName;
    }

    public boolean isContextOnly() {
        return contextOnly;
    }

    public void setContextOnly(boolean contextOnly) {
        this.contextOnly = contextOnly;
    }

    public String getContextUrl() {
        return contextUrl;
    }

    public void setContextUrl(String contextUrl) {
        this.contextUrl = contextUrl;
    }

    public String getContextPermissions() {
        return contextPermissions;
    }

    public void setContextPermissions(String contextPermissions) {
        this.contextPermissions = contextPermissions;
    }

    public boolean isNameSupported() {
        return nameSupported;
    }

    public void setNameSupported(boolean nameSupported) {
        this.nameSupported = nameSupported;
    }

    public boolean isNameRequired() {
        return nameRequired;
    }

    public void setNameRequired(boolean nameRequired) {
        this.nameRequired = nameRequired;
    }

}
