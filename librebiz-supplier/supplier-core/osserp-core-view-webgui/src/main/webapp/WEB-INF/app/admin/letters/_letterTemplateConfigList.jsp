<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="col-md-12 panel-area">
    <div class="table-responsive table-responsive-default">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th><fmt:message key="label" /></th>
                    <th><fmt:message key="created" /></th>
                    <th><fmt:message key="author" /></th>
                    <th class="action"><fmt:message key="action" /></th>
                </tr>
            </thead>
            <tbody>
                <c:choose>
                    <c:when test="${empty view.list}">
                        <tr>
                            <td class="emptyTemplate" colspan="4"><span><fmt:message key="noLetterTemplatesAvailable" /></span> <span style="margin-left: 20px;"> <v:link url="/admin/letters/letterTemplateConfig/enableCreateMode">[<fmt:message key="create" />]</v:link>
                            </span></td>
                        </tr>
                    </c:when>
                    <c:otherwise>
                        <c:forEach var="obj" items="${view.list}">
                            <tr>
                                <td><v:link url="/admin/letters/letterTemplateConfig/select?id=${obj.id}">
                                        <o:out value="${obj.name}" />
                                    </v:link></td>
                                <td><o:date value="${obj.created}" /></td>
                                <td><oc:employee value="${obj.createdBy}" /></td>
                                <td class="action"><v:link url="/admin/letters/letterTemplateConfig/print?id=${obj.id}">
                                        <o:img name="printIcon" styleClass="bigIcon" />
                                    </v:link></td>
                            </tr>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
            </tbody>
        </table>
    </div>
</div>
