/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 10-Aug-2006 15:11:22 
 * 
 */
package com.osserp.core.dao.records.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;

import org.hibernate.SessionFactory;

import com.osserp.common.ActionException;
import com.osserp.common.OptionsCache;
import com.osserp.common.dao.Tables;
import com.osserp.common.util.CollectionUtil;

import com.osserp.core.dao.TableKeys;
import com.osserp.core.dao.records.RecordExports;
import com.osserp.core.dao.records.RecordTypes;
import com.osserp.core.finance.RecordComparators;
import com.osserp.core.finance.RecordDisplay;
import com.osserp.core.finance.RecordExport;
import com.osserp.core.finance.RecordType;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RecordExportsDao extends AbstractRecordViews implements RecordExports {

    private static final String CREATE_CANCELLATION_EXPORT =
            "SELECT create_sales_cancellation_export(?)";

    private static final String CREATE_CREDIT_NOTE_EXPORT =
            "SELECT create_sales_credit_note_export(?)";

    private static final String CREATE_DOWNPAYMENT_EXPORT =
            "SELECT create_sales_order_downpayment_export(?)";

    private static final String CREATE_INVOICE_EXPORT =
            "SELECT create_sales_invoice_export(?)";

    private static final String CREATE_PAYMENT_EXPORT =
            "SELECT create_sales_payment_export(?)";

    private static final String CREATE_PURCHASE_INVOICE_EXPORT =
            "SELECT create_purchase_invoice_export(?)";

    private static final String CREATE_PURCHASE_PAYMENT_EXPORT =
            "SELECT create_purchase_payment_export(?)";

    private static final String GET_CANCELLATION_EXPORT_COUNT =
            "SELECT get_sales_cancellation_export_count()";

    private static final String GET_CREDIT_NOTE_EXPORT_COUNT =
            "SELECT get_sales_credit_note_export_count()";

    private static final String GET_DOWNPAYMENT_EXPORT_COUNT =
            "SELECT get_sales_order_downpayment_export_count()";

    private static final String GET_INVOICE_EXPORT_COUNT =
            "SELECT get_sales_invoice_export_count()";

    private static final String GET_PAYMENT_EXPORT_COUNT =
            "SELECT get_sales_payment_export_count()";

    private static final String GET_PURCHASE_INVOICE_EXPORT_COUNT =
            "SELECT get_purchase_invoice_export_count()";

    private static final String GET_PURCHASE_PAYMENT_EXPORT_COUNT =
            "SELECT get_purchase_payment_export_count()";

    private OptionsCache namesCache = null;
    private String exportsTable = null;
    private String exportItemsTable = null;

    public RecordExportsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            RecordTypes recordTypesDao,
            OptionsCache optionsCache) {
        super(jdbcTemplate, tables, sessionFactory, recordTypesDao);
        namesCache = optionsCache;
        exportItemsTable = getTable(TableKeys.RECORD_EXPORT_ITEMS);
        exportsTable = getTable(TableKeys.RECORD_EXPORTS);
    }

    public int getExportCount(Long recordType) {
        if (RecordType.SALES_DOWNPAYMENT.equals(recordType)) {
            return getDownpaymentCount();
        } else if (RecordType.SALES_CANCELLATION.equals(recordType)) {
            return getCancellationCount();
        } else if (RecordType.SALES_INVOICE.equals(recordType)) {
            return getInvoiceCount();
        } else if (RecordType.SALES_CREDIT_NOTE.equals(recordType)) {
            return getCreditNoteCount();
        } else if (RecordType.SALES_PAYMENT.equals(recordType)) {
            return getPaymentCount();
        } else if (RecordType.PURCHASE_INVOICE.equals(recordType)) {
            return getPurchaseInvoiceCount();
        } else if (RecordType.PURCHASE_PAYMENT.equals(recordType)) {
            return getPurchasePaymentCount();
        }
        throw new ActionException("record type " + recordType + " not supported");
    }

    public List<RecordDisplay> getRecords(Long type, boolean reverse) {
        StringBuilder query = createQuerySelectFrom(type);
        query
                .append(" WHERE id NOT IN (SELECT record_id FROM ")
                .append(exportItemsTable)
                .append(" WHERE export_id IN (SELECT id FROM ")
                .append(exportsTable)
                .append(" WHERE type_id = ")
                .append(type)
                .append(")) ORDER BY id");
        List<Map<String, Object>> result = (List<Map<String, Object>>) jdbcTemplate.query(
                query.toString(),
                RecordDisplayRowMapper.getReader());
        List<RecordDisplay> list = RecordDisplayRowMapper.createDisplayList(namesCache, result);
        CollectionUtil.sort(list, RecordComparators.createRecordDisplayByCreatedComparator(reverse));
        return list;
    }

    public int createExport(Long recordType, Long employeeId) {
        if (RecordType.SALES_DOWNPAYMENT.equals(recordType)) {
            return createDownpaymentExport(employeeId);
        } else if (RecordType.SALES_CANCELLATION.equals(recordType)) {
            return createCancellationExport(employeeId);
        } else if (RecordType.SALES_INVOICE.equals(recordType)) {
            return createInvoiceExport(employeeId);
        } else if (RecordType.SALES_CREDIT_NOTE.equals(recordType)) {
            return createCreditNoteExport(employeeId);
        } else if (RecordType.SALES_PAYMENT.equals(recordType)) {
            return createPaymentExport(employeeId);
        } else if (RecordType.PURCHASE_INVOICE.equals(recordType)) {
            return createPurchaseInvoiceExport(employeeId);
        } else if (RecordType.PURCHASE_PAYMENT.equals(recordType)) {
            return createPurchasePaymentExport(employeeId);
        }
        throw new ActionException("record type " + recordType + " not supported");
    }

    public int createExport(Long recordType, Long employeeId, List<RecordDisplay> records) {
        Long nextId = getNextLongByTable(exportsTable);
        Date start = null, end = null;
        for (int i = 0, j = records.size(); i < j; i++) {
            RecordDisplay rd = records.get(i);
            if (rd.getCreated() != null) {
                if (start == null || rd.getCreated().before(start)) {
                    start = rd.getCreated();
                }
                if (end == null || rd.getCreated().after(end)) {
                    end = rd.getCreated();
                }
            }
        }
        StringBuilder stmt = new StringBuilder("INSERT INTO ");
        stmt
            .append(exportsTable)
            .append(" (id,type_id,created_by,start_date,end_date,export_count) ")
            .append("VALUES (?,?,?,?,?,?)");
        int count = jdbcTemplate.update(
                stmt.toString(),
                new Object[] { nextId, recordType, employeeId, start, end, records.size() },
                new int[] { Types.BIGINT, Types.BIGINT, Types.BIGINT, Types.TIMESTAMP, Types.TIMESTAMP, Types.INTEGER });
        if (count > 0) {
            count = 0;
            stmt = new StringBuilder("INSERT INTO ")
                    .append(exportItemsTable)
                    .append(" (export_id,record_id) ")
                    .append("VALUES (?,?)");
            for (int i = 0, j = records.size(); i < j; i++) {
                RecordDisplay rd = records.get(i);
                count = count + jdbcTemplate.update(
                        stmt.toString(),
                        new Object[] { nextId, rd.getId() },
                        new int[] { Types.BIGINT, Types.BIGINT });
            }
        }
        return count;
    }

    public int createExport(Long recordType, Long employeeId, Date start, Date end) {
        if (recordType == null || start == null || end == null) {
            throw new IllegalArgumentException("value must not be null [recordType=" + recordType + ", start=" + start + ", end=" + end + "]");
        }
        List<RecordDisplay> records = new ArrayList<RecordDisplay>();
        List<RecordDisplay> list = getRecords(recordType, false);
        for (int i = 0, j = list.size(); i < j; i++) {
            RecordDisplay rd = list.get(i);
            if (start.before(rd.getCreated()) && end.after(rd.getCreated())) {
                records.add(rd);
            }
        }
        Long nextId = getNextLongByTable(exportsTable);
        StringBuilder stmt = new StringBuilder("INSERT INTO ");
        stmt
            .append(exportsTable)
            .append(" (id,type_id,created_by,start_date,end_date) ")
            .append("VALUES (?,?,?,?,?)");
        int count = jdbcTemplate.update(
                stmt.toString(),
                new Object[] { nextId, recordType, employeeId, start, end },
                new int[] { Types.BIGINT, Types.BIGINT, Types.BIGINT,
                        Types.TIMESTAMP, Types.TIMESTAMP });
        if (count > 0) {
            count = 0;
            stmt = new StringBuilder("INSERT INTO ")
                    .append(exportItemsTable)
                    .append(" (export_id,record_id) ")
                    .append("VALUES (?,?)");
            for (int i = 0, j = records.size(); i < j; i++) {
                RecordDisplay rd = records.get(i);
                count = count + jdbcTemplate.update(
                        stmt.toString(),
                        new Object[] { nextId, rd.getId() },
                        new int[] { Types.BIGINT, Types.BIGINT });
            }
            stmt = new StringBuilder("UPDATE ");
            stmt.append(exportsTable).append(" SET export_count = ? WHERE id = ?");
            jdbcTemplate.update(stmt.toString(),
                    new Object[] { count, nextId },
                    new int[] { Types.INTEGER, Types.BIGINT });
        }
        return count;
    }

    public RecordExport getExport(Long exportId) {
        StringBuilder query = new StringBuilder(RecordExportRowMapper.SELECT_VALUES);
        query
                .append(exportsTable)
                .append(" WHERE id = ?");
        List<RecordExport> exports = (List<RecordExport>) jdbcTemplate.query(
                query.toString(),
                new Object[] { exportId },
                new int[] { Types.BIGINT },
                RecordExportRowMapper.getReader());
        RecordExport export = exports.get(0);
        addRecords(export);
        return export;
    }

    public RecordExport getLatest(Long recordType) {
        StringBuilder query = new StringBuilder("SELECT max(id) FROM ");
        query
                .append(exportsTable)
                .append(" WHERE type_id = ?");
        List<Long> list = (List<Long>) jdbcTemplate.query(
                query.toString(),
                new Object[] { recordType },
                new int[] { Types.BIGINT },
                getLongRowMapper());
        if (list.isEmpty()) {
            return null;
        }
        return getExport(list.get(0));
    }

    public List<RecordExport> getExports(Long recordType) {
        StringBuilder query = new StringBuilder(
                RecordExportRowMapper.SELECT_VALUES);
        query
                .append(exportsTable)
                .append(" WHERE type_id = ? ORDER BY created DESC");
        List<RecordExport> result = (List<RecordExport>) jdbcTemplate.query(
                query.toString(),
                new Object[] { recordType },
                new int[] { Types.BIGINT },
                RecordExportRowMapper.getReader());
        for (Iterator<RecordExport> iter = result.iterator(); iter.hasNext();) {
            RecordExport export = iter.next();
            addRecords(export);
        }
        return CollectionUtil.sort(result, RecordExportRowMapper.createComparator(true));
    }

    public List<RecordExport> getExportsList(Long recordType) {
        StringBuilder query = new StringBuilder(
                RecordExportRowMapper.SELECT_VALUES);
        query
                .append(exportsTable)
                .append(" WHERE type_id = ? ORDER BY created DESC");
        List<RecordExport> result = (List<RecordExport>) jdbcTemplate.query(
                query.toString(),
                new Object[] { recordType },
                new int[] { Types.BIGINT },
                RecordExportRowMapper.getReader());
        return CollectionUtil.sort(result, RecordExportRowMapper.createComparator(true));
    }

    public long getExistingExportCount(Long recordType) {
        StringBuilder query = new StringBuilder("SELECT count(*) FROM ");
        query
                .append(exportsTable)
                .append(" WHERE type_id = ?");
        return jdbcTemplate.queryForObject(query.toString(), Long.class, recordType);
    }

    public void addRecords(RecordExport export) {
        StringBuilder query = createQuerySelectFrom(export.getType());
        query
                .append(" WHERE id IN (SELECT record_id FROM ")
                .append(exportItemsTable)
                .append(" WHERE export_id = ?) ORDER BY id");
        List<Map<String, Object>> result = (List<Map<String, Object>>) jdbcTemplate.query(
                query.toString(),
                new Object[] { export.getId() },
                new int[] { Types.BIGINT },
                RecordDisplayRowMapper.getReader());
        List<RecordDisplay> list = RecordDisplayRowMapper.createDisplayList(namesCache, result);
        for (int i = 0, j = list.size(); i < j; i++) {
            export.getRecords().add(list.get(i));
        }
    }

    private int getCancellationCount() {
        return jdbcTemplate.queryForObject(GET_CANCELLATION_EXPORT_COUNT, Integer.class);
    }

    private int getCreditNoteCount() {
        return jdbcTemplate.queryForObject(GET_CREDIT_NOTE_EXPORT_COUNT, Integer.class);
    }

    private int getDownpaymentCount() {
        return jdbcTemplate.queryForObject(GET_DOWNPAYMENT_EXPORT_COUNT, Integer.class);
    }

    private int getInvoiceCount() {
        return jdbcTemplate.queryForObject(GET_INVOICE_EXPORT_COUNT, Integer.class);
    }

    private int getPaymentCount() {
        return jdbcTemplate.queryForObject(GET_PAYMENT_EXPORT_COUNT, Integer.class);
    }

    private int getPurchaseInvoiceCount() {
        return jdbcTemplate.queryForObject(GET_PURCHASE_INVOICE_EXPORT_COUNT, Integer.class);
    }

    private int getPurchasePaymentCount() {
        return jdbcTemplate.queryForObject(GET_PURCHASE_PAYMENT_EXPORT_COUNT, Integer.class);
    }

    private int createCancellationExport(Long employeeId) {
        return jdbcTemplate.queryForObject(
                CREATE_CANCELLATION_EXPORT,
                new Object[] { employeeId },
                new int[] { Types.BIGINT }, 
                Integer.class);
    }

    private int createCreditNoteExport(Long employeeId) {
        return jdbcTemplate.queryForObject(
                CREATE_CREDIT_NOTE_EXPORT,
                new Object[] { employeeId },
                new int[] { Types.BIGINT }, 
                Integer.class);
    }

    private int createDownpaymentExport(Long employeeId) {
        return jdbcTemplate.queryForObject(
                CREATE_DOWNPAYMENT_EXPORT,
                new Object[] { employeeId },
                new int[] { Types.BIGINT }, 
                Integer.class);
    }

    private int createInvoiceExport(Long employeeId) {
        return jdbcTemplate.queryForObject(
                CREATE_INVOICE_EXPORT,
                new Object[] { employeeId },
                new int[] { Types.BIGINT }, 
                Integer.class);
    }

    private int createPaymentExport(Long employeeId) {
        return jdbcTemplate.queryForObject(
                CREATE_PAYMENT_EXPORT,
                new Object[] { employeeId },
                new int[] { Types.BIGINT }, 
                Integer.class);
    }

    private int createPurchaseInvoiceExport(Long employeeId) {
        return jdbcTemplate.queryForObject(
                CREATE_PURCHASE_INVOICE_EXPORT,
                new Object[] { employeeId },
                new int[] { Types.BIGINT }, 
                Integer.class);
    }

    private int createPurchasePaymentExport(Long employeeId) {
        return jdbcTemplate.queryForObject(
                CREATE_PURCHASE_PAYMENT_EXPORT,
                new Object[] { employeeId },
                new int[] { Types.BIGINT }, 
                Integer.class);
    }
}
