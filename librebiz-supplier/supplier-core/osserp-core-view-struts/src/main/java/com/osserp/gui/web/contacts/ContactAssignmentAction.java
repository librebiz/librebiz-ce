/**
 *
 * Copyright (C) 2004 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Nov 9, 2004 
 * 
 */
package com.osserp.gui.web.contacts;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.osserp.common.User;

import com.osserp.core.contacts.Contact;

import com.osserp.gui.client.contacts.ContactView;
import com.osserp.gui.client.contacts.CustomerView;
import com.osserp.gui.client.contacts.SupplierView;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ContactAssignmentAction extends AbstractContactAction {

    private static Logger log = LoggerFactory.getLogger(ContactAssignmentAction.class.getName());

    /**
     * Creates a new customer based on current contact
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward assignCustomer(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        HttpSession session = request.getSession();
        User user = getUser(session);
        if (log.isDebugEnabled()) {
            log.debug("assignCustomer() request from " + user.getId());
        }
        ContactView view = getContactView(session);
        view.assignContact(Contact.CUSTOMER);
        CustomerView customerView = (CustomerView) createView(request, CustomerView.class.getName());
        customerView.load(view);
        customerView.setDetailsEditMode(true);
        return mapping.findForward("customerEditor");
    }

    /**
     * Creates a new supplier based on current contact
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward assignSupplier(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        HttpSession session = request.getSession();
        User user = getUser(session);
        if (log.isDebugEnabled()) {
            log.debug("assignSupplier() request from " + user.getId());
        }
        ContactView view = getContactView(session);
        view.assignContact(Contact.SUPPLIER);
        SupplierView supplierView = (SupplierView) createView(request, SupplierView.class.getName());
        supplierView.load(view);
        return mapping.findForward(supplierView.getActionTarget());
    }

    /**
     * Creates a other based on current contact
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    public ActionForward assignOther(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        HttpSession session = request.getSession();
        User user = getUser(session);
        if (log.isDebugEnabled()) {
            log.debug("assignOther() request from " + user.getId());
        }
        ContactView view = getContactView(session);
        return mapping.findForward(view.getActionTarget());
    }

    /**
     * Exits contact assignment form
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return forward
     * @throws Exception
     */
    @Override
    public ActionForward exit(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        HttpSession session = request.getSession();
        if (log.isDebugEnabled()) {
            log.debug("exit() request from " + getUser(session).getId());
        }
        ContactView view = getContactView(session);
        view.setAssignMode(false);
        return mapping.findForward(view.getActionTarget());
    }
}
