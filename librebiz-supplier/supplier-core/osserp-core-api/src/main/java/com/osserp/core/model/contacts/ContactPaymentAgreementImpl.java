/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Mar 1, 2006 10:52:55 AM 
 * 
 */
package com.osserp.core.model.contacts;

import java.util.Date;

import com.osserp.core.contacts.ContactPaymentAgreement;
import com.osserp.core.finance.PaymentCondition;
import com.osserp.core.model.PaymentAgreementImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class ContactPaymentAgreementImpl extends PaymentAgreementImpl implements ContactPaymentAgreement {

    protected boolean taxFree = false;
    protected Long taxFreeId = null;

    protected ContactPaymentAgreementImpl() {
        super();
    }

    public ContactPaymentAgreementImpl(Long referenceId) {
        super(referenceId);
    }

    public ContactPaymentAgreementImpl(ContactPaymentAgreement other) {
        super(other);
        this.taxFree = other.isTaxFree();
        this.taxFreeId = other.getTaxFreeId();
    }

    public ContactPaymentAgreementImpl(
            Long id,
            Long referenceId,
            Double downpaymentPercent,
            Double deliveryInvoicePercent,
            Double finalInvoicePercent,
            Long downpaymentTargetId,
            Long deliveryInvoiceTargetId,
            Long finalInvoiceTargetId,
            PaymentCondition paymentCondition,
            String note,
            Date created,
            Long createdBy,
            Date changed,
            Long changedBy,
            boolean taxFree,
            Long taxFreeId) {
        super(
                id,
                referenceId,
                downpaymentPercent,
                deliveryInvoicePercent,
                finalInvoicePercent,
                downpaymentTargetId,
                deliveryInvoiceTargetId,
                finalInvoiceTargetId,
                paymentCondition,
                note,
                created,
                createdBy,
                changed,
                changedBy);
        this.taxFree = taxFree;
        this.taxFreeId = taxFreeId;
    }

    public boolean isTaxFree() {
        return taxFree;
    }

    public void setTaxFree(boolean taxFree) {
        this.taxFree = taxFree;
    }

    public Long getTaxFreeId() {
        return taxFreeId;
    }

    public void setTaxFreeId(Long taxFreeId) {
        this.taxFreeId = taxFreeId;
    }
}
