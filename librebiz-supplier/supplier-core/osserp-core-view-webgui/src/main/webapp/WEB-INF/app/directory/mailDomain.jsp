<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<v:view viewName="mailDomainView"/>

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
	<tiles:put name="styles" type="string">
		<style type="text/css">
		</style>
	</tiles:put>
	<tiles:put name="headline">
		<fmt:message key="${view.headerName}"/> - <fmt:message key="context"/>: <o:out value="${view.client.name}"/>
	</tiles:put>
	<tiles:put name="headline_right">
		<v:navigation/>
	</tiles:put>

	<tiles:put name="content" type="string" >
        <div class="content-area" id="mailDomainContent">
            <div class="row">
                <c:choose>
                    <c:when test="${view.createMode}">
                        <c:import url="_mailDomainCreate.jsp"/>
                    </c:when>
                    <c:when test="${view.editMode}">
                        <c:import url="_mailDomainEdit.jsp"/>
                    </c:when>
                    <c:when test="${!empty view.bean}">
                        <c:import url="_mailDomainDisplay.jsp"/>
                    </c:when>
                    <c:otherwise>
                        <c:import url="_mailDomainList.jsp"/>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
	</tiles:put>
</tiles:insert>