/**
 *
 * Copyright (C) 2016 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Sep 26, 2016 
 * 
 */
package com.osserp.gui.sales

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.ErrorCode

import com.osserp.core.finance.Cancellation
import com.osserp.core.finance.Payment
import com.osserp.core.sales.SalesCancellationManager
import com.osserp.core.sales.SalesCreditNoteManager
import com.osserp.core.sales.SalesPaymentManager

/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
class SalesCreditNotePaymentView extends AbstractSalesPaymentView {

    private static Logger logger = LoggerFactory.getLogger(SalesCreditNotePaymentView.class.getName())

    SalesCreditNotePaymentView() {
        super()
        dependencies = ['salesCreditNoteView', 'salesBillingView']
    }

    protected String getDefaultExitLink() {
        '/salesCreditNoteReload'
    }

    protected String getPaymentAwareRecordViewName() {
        'salesCreditNoteView'
    }

    void selectPaymentType() {
        // not supported by creditNotePayment
    }

    String createCreditNote() {
        throw new ClientException(ErrorCode.OPERATION_NOT_SUPPORTED)
    }

    String createCancellation() {
        Date cancelDate = form.getDate('cancelDate')
        resultingRecord = salesCancellationManager.create(domainEmployee, record, cancelDate, null)
        if (resultingRecord instanceof Cancellation) {
            return "/salesCancellation.do?method=forward&id=${resultingRecord.id}&canceledRecordView=salesCreditNoteView"
        }
        return defaultExitTarget
    }

    String resetCancellation() {
        if (record?.cancellation) {
            salesCancellationManager.delete(record.cancellation, domainEmployee)
        }
        return defaultExitTarget
    }

    void deletePayment() {
        Long id = getLong('id')
        Payment pm = record.payments.find { it.id == id }
        if (pm) {
            salesPaymentManager.delete(pm)
            paymentDeleteMode = false
        }
    }

    void toggleDeliveryBooking() {
        salesCreditNoteManager.toggleDeliveryBooking(domainEmployee, record)
    }

    @Override
    void save() {
        user.checkPermission((['accounting', 'sales_credit_note_delete', 'sales_credit_note_payment'] as String[]))
        Date datePaid = getDate('datePaid')
        BigDecimal amount = getDecimal('amount')
        Long bankAccountId = getLong('bankAccountId')
        salesCreditNoteManager.addPayment(
                domainEmployee, record, Payment.OUTPAYMENT, amount, datePaid, bankAccountId)
    }

    protected SalesCreditNoteManager getSalesCreditNoteManager() {
        getService(SalesCreditNoteManager.class.getName())
    }

    protected SalesCancellationManager getSalesCancellationManager() {
        getService(SalesCancellationManager.class.getName())
    }

    protected SalesPaymentManager getSalesPaymentManager() {
        getService(SalesPaymentManager.class.getName())
    }

}
