/**
 *
 * Copyright (C) 2006 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 19-Jul-2006 
 * 
 */
package com.osserp.core.model;

import java.util.Date;

import com.osserp.common.beans.AbstractEntity;

import com.osserp.core.Item;
import com.osserp.core.products.SerialNumber;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class SerialNumberImpl extends AbstractEntity implements SerialNumber {

    private Long productId;
    private String serial;

    protected SerialNumberImpl() {
        super();
    }

    protected SerialNumberImpl(
            Item item,
            Long createdBy,
            String serial) {
        super(null, item.getId(), createdBy);
        this.productId = item.getProduct().getProductId();
        this.serial = serial;
    }

    public SerialNumberImpl(
            Long id,
            Date created,
            Long createdBy,
            Long productId,
            String serial,
            Long recordId) {
        super(id, recordId, created, createdBy);
        this.productId = productId;
        this.serial = serial;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    @Override
    public Object clone() {
        SerialNumberImpl obj = new SerialNumberImpl();
        obj.setId(getId());
        obj.setProductId(getProductId());
        obj.setCreated(getCreated());
        obj.setCreatedBy(getCreatedBy());
        obj.setReference(getReference());
        obj.setSerial(getSerial());
        return obj;
    }
}
