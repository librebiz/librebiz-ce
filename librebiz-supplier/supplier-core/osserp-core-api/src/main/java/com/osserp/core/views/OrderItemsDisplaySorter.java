/**
 *
 * Copyright (C) 2010 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Nov 12, 2010 11:39:59 AM 
 * 
 */
package com.osserp.core.views;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.core.Comparators;

/**
 * 
 * @author cf <cf@osserp.com>
 * 
 */
public class OrderItemsDisplaySorter {
    private static Logger log = LoggerFactory.getLogger(OrderItemsDisplaySorter.class.getName());

    public static final String[] COMPARATORS = {
            "bySalesId",
            "bySalesIdReverse",
            "byIdOrSalesId",
            "byIdOrSalesIdReverse",
            "bySalesName",
            "bySalesNameReverse",
            "byProductId",
            "byProductIdReverse",
            "byProductName",
            "byProductNameReverse",
            "byDelivery",
            "byQuantityReverse",
            "byQuantity",
            "byQuantityReverse",
            "byStock",
            "byStockReverse"
    };

    private Map<String, Comparator> comparators = null;

    private static OrderItemsDisplaySorter cinstance = null;

    @SuppressWarnings("unchecked")
    private OrderItemsDisplaySorter() {
        super();
        comparators = Collections.synchronizedMap(new HashMap<>());
        comparators.put("bySalesId", createBySalesIdComparator(false));
        comparators.put("bySalesIdReverse", createBySalesIdComparator(true));
        comparators.put("byIdOrSalesId", createByIdOrSalesIdComparator(false));
        comparators.put("byIdOrSalesIdReverse", createByIdOrSalesIdComparator(true));
        comparators.put("bySalesName", createBySalesNameComparator(false));
        comparators.put("bySalesNameReverse", createBySalesNameComparator(true));
        comparators.put("byProductId", createByProductIdComparator(false));
        comparators.put("byProductIdReverse", createByProductIdComparator(true));
        comparators.put("byProductName", createByProductNameComparator(false));
        comparators.put("byProductNameReverse", createByProductNameComparator(true));
        comparators.put("byDelivery", createByDeliveryComparator(false));
        comparators.put("byDeliveryReverse", createByDeliveryComparator(true));
        comparators.put("byQuantity", createByQuantityComparator(false));
        comparators.put("byQuantityReverse", createByQuantityComparator(true));
        comparators.put("byOpen", createByOpenComparator(false));
        comparators.put("byOpenReverse", createByOpenComparator(true));
    }

    public static OrderItemsDisplaySorter instance() {
        if (cinstance == null) {
            cinstance = new OrderItemsDisplaySorter();
        }
        return cinstance;
    }

    @SuppressWarnings("unchecked")
    public void sort(String name, List<OrderItemsDisplay> items) {
        if (log.isDebugEnabled()) {
            log.debug("sort() invoked [name=" + name + "]");
        }
        Comparator c = comparators.get(name);
        if (c == null) {
            c = comparators.get("byName");
            if (log.isDebugEnabled()) {
                log.debug("sort() comparator '" + name + "' not found");
            }
        }
        Collections.sort(items, c);
    }

    private Comparator createBySalesIdComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    return Comparators.compare(((OrderItemsDisplay) a).getSalesId(), ((OrderItemsDisplay) b).getSalesId(), true);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                return Comparators.compare(((OrderItemsDisplay) a).getSalesId(), ((OrderItemsDisplay) b).getSalesId(), false);
            }
        };
    }

    private Comparator createByIdOrSalesIdComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    OrderItemsDisplay oa = (OrderItemsDisplay) a;
                    OrderItemsDisplay ob = (OrderItemsDisplay) b;
                    Long x = oa.isSalesItem() ? oa.getSalesId() : oa.getId();
                    Long y = ob.isSalesItem() ? ob.getSalesId() : ob.getId();
                    return Comparators.compare(x, y, true);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                OrderItemsDisplay oa = (OrderItemsDisplay) a;
                OrderItemsDisplay ob = (OrderItemsDisplay) b;
                Long x = oa.isSalesItem() ? oa.getSalesId() : oa.getId();
                Long y = ob.isSalesItem() ? ob.getSalesId() : ob.getId();
                return Comparators.compare(x, y, false);
            }
        };
    }

    private Comparator createBySalesNameComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    return Comparators.compare(((OrderItemsDisplay) a).getSalesName(), ((OrderItemsDisplay) b).getSalesName(), true);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                return Comparators.compare(((OrderItemsDisplay) a).getSalesName(), ((OrderItemsDisplay) b).getSalesName(), false);
            }
        };
    }

    private Comparator createByProductIdComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    return Comparators.compare(((OrderItemsDisplay) a).getProductId(), ((OrderItemsDisplay) b).getProductId(), true);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                return Comparators.compare(((OrderItemsDisplay) a).getProductId(), ((OrderItemsDisplay) b).getProductId(), false);
            }
        };
    }

    private Comparator createByProductNameComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    return Comparators.compare(((OrderItemsDisplay) a).getProductName(), ((OrderItemsDisplay) b).getProductName(), true);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                return Comparators.compare(((OrderItemsDisplay) a).getProductName(), ((OrderItemsDisplay) b).getProductName(), false);
            }
        };
    }

    private Comparator createByDeliveryComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    return Comparators.compare(((OrderItemsDisplay) a).getDelivery(), ((OrderItemsDisplay) b).getDelivery(), true);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                return Comparators.compare(((OrderItemsDisplay) a).getDelivery(), ((OrderItemsDisplay) b).getDelivery(), false);
            }
        };
    }

    private Comparator createByQuantityComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    return Comparators.compare(((OrderItemsDisplay) a).getQuantity(), ((OrderItemsDisplay) b).getQuantity(), true);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                return Comparators.compare(((OrderItemsDisplay) a).getQuantity(), ((OrderItemsDisplay) b).getQuantity(), false);
            }
        };
    }

    private Comparator createByOpenComparator(boolean reverse) {
        if (reverse) {
            return new Comparator() {
                public int compare(Object a, Object b) {
                    return Comparators.compare(((OrderItemsDisplay) a).getOutstanding(), ((OrderItemsDisplay) b).getOutstanding(), true);
                }
            };
        }
        return new Comparator() {
            public int compare(Object a, Object b) {
                return Comparators.compare(((OrderItemsDisplay) a).getOutstanding(), ((OrderItemsDisplay) b).getOutstanding(), false);
            }
        };
    }

}
