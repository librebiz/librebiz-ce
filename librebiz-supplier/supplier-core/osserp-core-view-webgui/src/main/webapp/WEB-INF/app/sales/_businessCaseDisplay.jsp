<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>

<v:view viewName="businessCaseDisplayView" />
<c:set var="bean" scope="page" value="${view.bean}" />

<div class="row row-modal">
    <div class="col-md-6 panel-area panel-area-modal">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>
                    <o:out value="${bean.primaryKey}"/> - <o:out value="${bean.name}"/>
                </h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="table-responsive table-responsive-default">
                <table class="table">
                    <tbody>
                        <c:choose>
                            <c:when test="${!empty view.bean && view.notesDisplay}">
                                <c:if test="${empty view.notes}">
                                    <tr>
                                        <td class="left"><fmt:message key="noComment" /></td>
                                    </tr>
                                </c:if>
                                <c:forEach var="note" items="${view.notes}">
                                    <tr class="altrow">
                                        <td><o:date value="${note.created}" addtime="true" /></td>
                                        <td style="border-right: none;"><oc:employee value="${note.createdBy}" /></td>
                                        <td class="right"><c:if test="${note.email}">
                                                <fmt:message key="noteWasSent" />
                                            </c:if></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td style="padding-left: 5px;" colspan="2"><c:if test="${!empty note.headline}">
                                                <o:out value="${note.headline}" />
                                                <c:if test="${!empty note.note}">: </c:if>
                                            </c:if> <o:out value="${note.note}" /></td>
                                    </tr>
                                </c:forEach>
                            </c:when>
                            <c:when test="${!empty view.bean && !view.notesDisplay}">
                                <tr>
                                    <td><fmt:message key="sales" /></td>
                                    <td><oc:employee value="${bean.salesId}" /></td>
                                </tr>
                                <tr>
                                    <c:choose>
                                        <c:when test="${bean.salesContext}">
                                            <td><fmt:message key="projector" /></td>
                                            <td><oc:employee value="${bean.managerId}" /></td>
                                        </c:when>
                                        <c:otherwise>
                                            <td><fmt:message key="createdBy" /></td>
                                            <td><oc:employee value="${bean.createdBy}" /></td>
                                        </c:otherwise>
                                    </c:choose>
                                </tr>
                                <tr>
                                    <td><fmt:message key="status" /></td>
                                    <td><o:out value="${bean.status}" /> %</td>
                                </tr>
                                <tr>
                                    <td><fmt:message key="lastAction" /></td>
                                    <td><o:date value="${bean.lastFlowControlSheet.created}" casenull="-" addcentury="false" /> - <o:out value="${bean.lastFlowControlSheet.action.name}" /></td>
                                </tr>
                                <tr>
                                    <td><fmt:message key="commentForAction" /></td>
                                    <td><c:choose>
                                            <c:when test="${!empty bean.lastFlowControlSheet.note}">
                                                <o:out value="${bean.lastFlowControlSheet.note}" />
                                            </c:when>
                                            <c:otherwise>
                                                <fmt:message key="noComment" />
                                            </c:otherwise>
                                        </c:choose></td>
                                </tr>
                                <tr>
                                    <td><fmt:message key="comment2" /></td>
                                    <td><c:choose>
                                            <c:when test="${!empty view.lastNote.note}">
                                                <oc:employee value="${view.lastNote.createdBy}" /> - <o:textOut value="${view.lastNote.note}" />
                                            </c:when>
                                            <c:otherwise>
                                                <fmt:message key="noComment" />
                                            </c:otherwise>
                                        </c:choose></td>
                                </tr>
                            </c:when>
                            <c:otherwise>
                                <tr>
                                    <td><fmt:message key="noInformationForRequestProjectAvailable" /></td>
                                </tr>
                            </c:otherwise>
                        </c:choose>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
