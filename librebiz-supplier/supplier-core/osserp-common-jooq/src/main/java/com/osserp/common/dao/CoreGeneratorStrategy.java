/**
 *
 * Copyright (C) 2020 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License (AGPL)
 * version 3 as published by the Free Software Foundation. In accordance
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code
 * versions must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area
 * or, if not reasonably feasible for technical reason, as a top-level
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute.
 *
 */
package com.osserp.common.dao;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import org.jooq.codegen.DefaultGeneratorStrategy;
import org.jooq.meta.Definition;


/**
 *
 * @author Rainer Kirchner <rk@osserp.com>
 *
 */
public class CoreGeneratorStrategy extends DefaultGeneratorStrategy {
    //private static Logger log = LoggerFactory.getLogger(CoreGeneratorStrategy.class.getName());

    private static final String PROPERTIES_FILE = "src/test/resources/jooq-generator.properties";

    private String daoPackageName;
    private String objectPrefix;
    private String pojoSuffix;
    private String pojoPackagePrefix;
    private boolean pojoPackageByName;

    public CoreGeneratorStrategy() {
        super();
        Properties props = fetchProperties();
        if (!props.isEmpty()) {
            daoPackageName = props.getProperty("jooq.dao.package.name", "dao");
            objectPrefix = props.getProperty("jooq.object.prefix");
            pojoSuffix = props.getProperty("jooq.pojo.suffix");
            pojoPackagePrefix = props.getProperty("jooq.pojo.package.prefix");
            pojoPackageByName =  Boolean.valueOf(props.getProperty("jooq.pojo.package.byname"));
        }
    }

    @Override
    public String getJavaClassName(Definition definition, Mode mode) {
        if (mode == Mode.DEFAULT || mode == null) {
            return super.getJavaClassName(definition, mode) +  "Table";
        } else if (mode == Mode.INTERFACE || mode == Mode.POJO) {
            String name = super.getJavaClassName(definition, Mode.POJO);
            if (name.endsWith("ies")) {
                name = name.substring(0, name.length() -3) + "y";
            } else if (name.endsWith("s")) {
                name = name.substring(0, name.length() -1);
            }
            if (!isView(definition)) {
                if (mode == Mode.POJO) {
                    name = fetchObjectPrefix() + name + fetchPojoSuffix();
                } else if (mode == Mode.INTERFACE) {
                    name = fetchObjectPrefix() + name;
                }
            }
            return name;
        }
        return super.getJavaClassName(definition, mode);
    }

    @Override
    public String getJavaPackageName(Definition definition, Mode mode) {
        String packageName = null;
        if (mode == Mode.INTERFACE || mode == Mode.POJO) {
            StringBuilder sb = new StringBuilder();         
            sb.append(getTargetPackage());
            if (pojoPackageByName) {
                String pojoPkg = getPojoPackage(definition);
                if (pojoPkg != null && pojoPkg.length() > 0) {
                    sb.append(".").append(pojoPkg);
                }
            }
            if (mode == Mode.POJO && pojoPackagePrefix != null) {
                sb.append(".").append(pojoPackagePrefix);
            }
            packageName = sb.toString();
        } else {
            packageName = super.getJavaPackageName(definition, mode);
            String target = getTargetPackage();
            if (packageName.startsWith(target)) {
                String suffix = packageName.substring(target.length());
                packageName = target + "." + daoPackageName + suffix;
            }
        }
        return packageName;
    }

    public String getObjectPrefix() {
        return objectPrefix;
    }

    public void setObjectPrefix(String objectPrefix) {
        this.objectPrefix = objectPrefix;
    }

    public String getPojoSuffix() {
        return pojoSuffix;
    }

    public void setPojoSuffix(String pojoSuffix) {
        this.pojoSuffix = pojoSuffix;
    }

    protected String fetchObjectPrefix() {
        return (getObjectPrefix() == null) || (getObjectPrefix().length() < 1)
                ? "" : getObjectPrefix();
    }

    protected String fetchPojoSuffix() {
        return (getPojoSuffix() == null) || (getPojoSuffix().length() < 1)
                ? "" : getPojoSuffix();
    }

    protected Properties fetchProperties() {
        Properties props = new Properties();
        File file = new File(PROPERTIES_FILE);
        if (file.exists() && file.canRead()) {
            FileInputStream result = null;
            try {
                result = new FileInputStream(file);
                props.load(result);
            } catch (Exception readInIgnorable) {
            } finally {
                if (result != null) {
                    try {
                        result.close();
                    } catch (Exception closeIgnorable) {
                    }
                }
            }
        }
        return props;
    }

    protected String getPojoPackage(Definition definition) {
        String input = definition.getInputName();
        if (isView(definition)) {
            input = input.substring(2, input.length());
        }
        if (input.indexOf("_") > -1) {
            input = input.substring(0, input.indexOf("_"));
        }
        if (input.endsWith("y")) {
            return input.substring(0, input.length() -1) + "ies";
        }
        if (!input.endsWith("s")) {
            return input + "s";
        }
        return input;
    }

    protected boolean isView(Definition definition) {
        return (definition != null && definition.getInputName().startsWith("v_"));
    }
}
