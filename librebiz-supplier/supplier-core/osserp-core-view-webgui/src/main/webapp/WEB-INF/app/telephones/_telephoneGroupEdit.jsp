<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>
<c:set var="view" value="${sessionScope.telephoneGroupEditView}"/>
<c:set var="group" value="${sessionScope.telephoneGroupEditView.bean}"/>
<c:set var="url" value="/telephones/telephoneGroupEdit"/>

<c:if test="${!empty sessionScope.error}">
	&nbsp;
	<div class="errormessage">
		<fmt:message key="error"/>: <fmt:message key="${sessionScope.error}"/>
	</div>
    <o:removeErrors/>
</c:if>
<c:if test="${!empty view}">
	<o:logger write="view not empty" level="debug"/>
	<c:choose>
		<c:when test="${view.editMode}">
			<o:logger write="view in editMode" level="debug"/>
			<c:choose>
				<c:when test="${!empty group}">
					<o:logger write="group not empty" level="debug"/>
					<c:choose>
						<c:when test="${view.edit == 'name'}">
							<v:ajaxForm name="dynamicForm" targetElement="${view.edit}_${group.id}" url="${url}/save">
								<input type="text" name="value" id="value" value="${group.name}"/>
								<v:ajaxLink url="${url}/disableEditMode?name=${view.edit}&id=${group.id}" targetElement="${view.edit}_${group.id}">
									<o:img name="backIcon" styleClass="bigicon"/>
								</v:ajaxLink>
								<input type="hidden" name="id" id="id" value="${group.id}"/>
								<input type="hidden" name="name" id="name" value="${view.edit}"/>
								<input type="image" name="submit" src="<c:url value="${applicationScope.saveIcon}"/>" class="bigicon" title="<fmt:message key="save"/>" />
							</v:ajaxForm>
						</c:when>
					</c:choose>
				</c:when>
				<c:otherwise>
					&nbsp;
				</c:otherwise>
			</c:choose>
		</c:when>
		<c:otherwise>
			<c:choose>
				<c:when test="${!empty group}">
					<o:logger write="group not empty" level="debug"/>
					<c:choose>
						<c:when test="${view.edit == 'name'}">
							<v:ajaxLink url="${url}/enableEditMode?name=${view.edit}&id=${group.id}" targetElement="${view.edit}_${group.id}" title="edit">
								<o:out value="${group.name}"/>
							</v:ajaxLink>
						</c:when>
					</c:choose>
				</c:when>
				<c:otherwise>
					&nbsp;
				</c:otherwise>
			</c:choose>
		</c:otherwise>
	</c:choose>
</c:if>
