<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" scope="page" value="${sessionScope.zipcodeSearchPopupView}" />
<c:set var="sortUrl" scope="page" value="/contacts/zipcodeSearchPopup/sort" />

<c:if test="${!empty view}">
    <div class="modalBoxHeader">
        <div class="modalBoxHeaderLeft">
            <fmt:message key="${view.headerName}" />
        </div>
        <div class="modalBoxHeaderRight">
            <div class="boxnav">
                <ul>
                    <c:forEach var="nav" items="${view.navigation}">
                        <li><v:ajaxLink url="${nav.link}" targetElement="${view.name}_popup" title="${nav.title}">
                                <o:img name="${nav.icon}" />
                            </v:ajaxLink></li>
                    </c:forEach>
                </ul>
            </div>
        </div>
    </div>
    <div class="modalBoxData">
        <div class="subcolumns">
            <div class="subcolumn">
                <c:if test="${!empty sessionScope.error}">
                    <div class="spacer"></div>
                    <div class="errormessage">
                        <fmt:message key="error" />
                        :
                        <fmt:message key="${sessionScope.error}" />
                    </div>
                    <o:removeErrors />
                </c:if>
                <div class="subcolumns">
                    <div class="subcolumn">
                        <div class="spacer"></div>
                        <c:choose>
                            <c:when test="${view.finda=='city'}">
                                <div class="contentBox">
                                    <div class="contentBoxHeader">
                                        <div class="contentBoxHeaderLeft">
                                            <fmt:message key="findCityByZipcode" />
                                        </div>
                                    </div>
                                    <div class="contentBoxData">
                                        <div class="subcolumns">
                                            <div class="subcolumn">
                                                <v:ajaxForm name="dynamicForm" targetElement="${view.name}_popup" url="/contacts/zipcodeSearchPopup/save">
                                                    <div class="spacer"></div>
                                                    <input type="hidden" name="street" value="<o:out value="${view.street}"/>">
                                                    <input type="hidden" name="city" value="<o:out value="${view.city}"/>">
                                                    <table class="valueTable">
                                                        <tbody>
                                                            <tr>
                                                                <td><fmt:message key="zipcode" /></td>
                                                                <td><v:text style="width:300px;" name="zipcode" value="${view.zipcode}" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td><input onchange="$('dynamicForm').onsubmit();" style="margin-right: 5px;" type="radio" name="finda" value="plz">
                                                                <fmt:message key="zipcodeFull" /> <input onchange="$('dynamicForm').onsubmit();" style="margin-right: 5px; margin-left: 10px;" type="radio" name="finda" value="city" checked="checked">
                                                                <fmt:message key="city" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="row-submit"></td>
                                                                <td class="row-submit"><o:submit value="search" /></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </v:ajaxForm>
                                                <div class="spacer"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:when>
                            <c:when test="${view.finda=='plz'}">
                                <div class="contentBox">
                                    <div class="contentBoxHeader">
                                        <div class="contentBoxHeaderLeft">
                                            <fmt:message key="findZipcodeByAddress" />
                                        </div>
                                    </div>
                                    <div class="contentBoxData">
                                        <div class="subcolumns">
                                            <div class="subcolumn">
                                                <v:ajaxForm name="dynamicForm" targetElement="${view.name}_popup" url="/contacts/zipcodeSearchPopup/save">
                                                    <div class="spacer"></div>
                                                    <input type="hidden" name="zipcode" value="<o:out value="${view.zipcode}"/>">
                                                    <table class="valueTable">
                                                        <tbody>
                                                            <tr>
                                                                <td><fmt:message key="street" /></td>
                                                                <td><v:text style="width:300px;" name="street" value="${view.street}" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td><fmt:message key="city" /></td>
                                                                <td><v:text style="width:300px;" name="city" value="${view.city}" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td><input onchange="$('dynamicForm').onsubmit();" style="margin-right: 5px;" type="radio" name="finda" value="plz" checked="checked">
                                                                <fmt:message key="zipcodeFull" /> <input onchange="$('dynamicForm').onsubmit();" style="margin-right: 5px; margin-left: 10px;" type="radio" name="finda" value="city">
                                                                <fmt:message key="city" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="row-submit"></td>
                                                                <td class="row-submit"><o:submit value="search" /></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </v:ajaxForm>
                                                <div class="spacer"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:when>
                        </c:choose>
                        <div class="spacer"></div>
                        <c:choose>
                            <c:when test="${!empty view.list}">
                                <c:choose>
                                    <c:when test="${view.resultType == 'city'}">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th><fmt:message key="zipcode" /></th>
                                                    <th><v:ajaxLink url="${sortUrl}?key=city" targetElement="${view.name}_popup">
                                                            <fmt:message key="city" />
                                                        </v:ajaxLink></th>
                                                    <th style="width: 16px;"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach var="row" items="${view.list}">
                                                    <tr onMouseover="this.className='yellow'" onMouseout="this.className=''">
                                                        <td><v:ajaxLink title="showStreets" url="/contacts/zipcodeSearchPopup/select?id=${row.id}" targetElement="${view.name}_popup">
                                                                <o:out value="${row.zipcode}" />
                                                            </v:ajaxLink></td>
                                                        <td><o:out value="${row.city}" /></td>
                                                        <td><v:ajaxLink preRequest="applyZipcodeSearchValues('${row.zipcode}','${row.city}');" url="/contacts/zipcodeSearchPopup/exit" targetElement="${view.name}_popup">
                                                                <o:img name="enabledIcon" styleClass="bigicon" />
                                                            </v:ajaxLink></td>
                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                        </table>
                                    </c:when>
                                    <c:when test="${view.resultType == 'district'}">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th><fmt:message key="zipcode" /></th>
                                                    <th><v:ajaxLink url="${sortUrl}?key=city" targetElement="${view.name}_popup">
                                                            <fmt:message key="city" />
                                                        </v:ajaxLink></th>
                                                    <th><v:ajaxLink url="${sortUrl}?key=district" targetElement="${view.name}_popup">
                                                            <fmt:message key="district" />
                                                        </v:ajaxLink></th>
                                                    <th style="width: 16px;"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach var="row" items="${view.list}">
                                                    <tr onMouseover="this.className='yellow'" onMouseout="this.className=''">
                                                        <td><v:ajaxLink title="showStreets" url="/contacts/zipcodeSearchPopup/select?id=${row.id}" targetElement="${view.name}_popup">
                                                                <o:out value="${row.zipcode}" />
                                                            </v:ajaxLink></td>
                                                        <td><o:out value="${row.city}" /></td>
                                                        <td><o:out value="${row.district}" /></td>
                                                        <td><v:ajaxLink preRequest="applyZipcodeSearchValues('${row.zipcode}','${row.district}');" url="/contacts/zipcodeSearchPopup/exit" targetElement="${view.name}_popup">
                                                                <o:img name="enabledIcon" styleClass="bigicon" />
                                                            </v:ajaxLink></td>
                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                        </table>
                                    </c:when>
                                    <c:when test="${view.resultType == 'cityaddition'}">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th><fmt:message key="zipcode" /></th>
                                                    <th><v:ajaxLink url="${sortUrl}?key=city" targetElement="${view.name}_popup">
                                                            <fmt:message key="city" />
                                                        </v:ajaxLink></th>
                                                    <th><v:ajaxLink url="${sortUrl}?key=cityaddition" targetElement="${view.name}_popup">
                                                            <fmt:message key="cityaddition" />
                                                        </v:ajaxLink></th>
                                                    <th style="width: 16px;"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach var="row" items="${view.list}">
                                                    <tr onMouseover="this.className='yellow'" onMouseout="this.className=''">
                                                        <td><v:ajaxLink title="showStreets" url="/contacts/zipcodeSearchPopup/select?id=${row.id}" targetElement="${view.name}_popup">
                                                                <o:out value="${row.zipcode}" />
                                                            </v:ajaxLink></td>
                                                        <td><o:out value="${row.city}" /></td>
                                                        <td><o:out value="${row.cityaddition}" /></td>
                                                        <td><v:ajaxLink preRequest="applyZipcodeSearchValues('${row.zipcode}','${row.city}');" url="/contacts/zipcodeSearchPopup/exit" targetElement="${view.name}_popup">
                                                                <o:img name="enabledIcon" styleClass="bigicon" />
                                                            </v:ajaxLink></td>
                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                        </table>
                                    </c:when>
                                    <c:when test="${view.resultType == 'street'}">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th><fmt:message key="zipcode" /></th>
                                                    <th><v:ajaxLink url="${sortUrl}?key=city" targetElement="${view.name}_popup">
                                                            <fmt:message key="city" />
                                                        </v:ajaxLink></th>
                                                    <th><v:ajaxLink url="${sortUrl}?key=street" targetElement="${view.name}_popup">
                                                            <fmt:message key="street" />
                                                        </v:ajaxLink></th>
                                                    <th><v:ajaxLink url="${sortUrl}?key=number" targetElement="${view.name}_popup">
                                                            <fmt:message key="number" />
                                                        </v:ajaxLink></th>
                                                    <th><v:ajaxLink url="${sortUrl}?key=district" targetElement="${view.name}_popup">
                                                            <fmt:message key="district" />
                                                        </v:ajaxLink></th>
                                                    <th style="width: 16px;"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach var="row" items="${view.list}">
                                                    <tr onMouseover="this.className='yellow'" onMouseout="this.className=''">
                                                        <td><o:out value="${row.zipcode}" /></td>
                                                        <td><o:out value="${row.city}" /></td>
                                                        <td><o:out value="${row.street}" /></td>
                                                        <td><o:out value="${row.number}" /></td>
                                                        <td><o:out value="${row.district}" /></td>
                                                        <td><v:ajaxLink preRequest="applyZipcodeSearchValues('${row.zipcode}','${row.district}','${row.street}');" url="/contacts/zipcodeSearchPopup/exit" targetElement="${view.name}_popup">
                                                                <o:img name="enabledIcon" styleClass="bigicon" />
                                                            </v:ajaxLink></td>
                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                        </table>
                                    </c:when>
                                </c:choose>
                            </c:when>
                            <c:when test="${view.resultType == 'none'}">
                                <fmt:message key="error.search" />
                            </c:when>
                        </c:choose>
                        <div class="spacer"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</c:if>

