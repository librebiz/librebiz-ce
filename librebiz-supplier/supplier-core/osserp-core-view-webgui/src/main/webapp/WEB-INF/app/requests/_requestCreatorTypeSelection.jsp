<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<div class="row">
    <div class="col-md-12 panel-area">
        <div class="table-responsive table-responsive-default">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th class="name"><fmt:message key="name" /></th>
                        <th class="type"><fmt:message key="description" /></th>
                        <th class="company"><fmt:message key="client" /></th>
                    </tr>
                </thead>
                <tbody>
                    <c:choose>
                        <c:when test="${empty view.requestTypes}">
                            <tr>
                                <td colspan="3" class="error empty"><fmt:message key="noSelectionAvailable" /></td>
                            </tr>
                        </c:when>
                        <c:otherwise>
                            <c:forEach var="type" items="${view.requestTypes}">
                                <c:choose>
                                    <c:when test="${type.directSales}">
                                        <o:permission role="executive,executive_sales,accounting,customer_service" info="permissionSelectDirectSales">
                                            <tr>
                                                <td class="name"><v:link url="${view.baseLink}/selectType?id=${type.id}">
                                                        <o:out value="${type.name}" />
                                                    </v:link></td>
                                                <td class="type"><o:out value="${type.workflow.description}" /></td>
                                                <td class="company"><oc:options name="systemCompanies" value="${type.company}" /></td>
                                            </tr>
                                        </o:permission>
                                    </c:when>
                                    <c:otherwise>
                                        <tr>
                                            <td class="name"><v:link url="${view.baseLink}/selectType?id=${type.id}">
                                                    <o:out value="${type.name}" />
                                                </v:link></td>
                                            <td class="type"><o:out value="${type.workflow.description}" /></td>
                                            <td class="company"><oc:options name="systemCompanies" value="${type.company}" /></td>
                                        </tr>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </c:otherwise>
                    </c:choose>
                </tbody>
            </table>
        </div>
    </div>
</div>
