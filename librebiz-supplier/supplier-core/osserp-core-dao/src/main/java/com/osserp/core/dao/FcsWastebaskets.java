/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on May 28, 2007 10:38:49 AM 
 * 
 */
package com.osserp.core.dao;

import java.util.List;
import java.util.Set;

import com.osserp.core.FcsItem;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface FcsWastebaskets {

    /**
     * Provides all action templates of the given project whose were moved to the wastebasket
     * @param projectId
     * @return set of template id's moved to wastebasket
     */
    Set<Long> getWaste(Long projectId);

    /**
     * Adds an action to the wastebasket
     * @param referenceId under which we add the action
     * @param templateId of the action to add
     */
    void add(Long referenceId, Long employeeId, Long templateId);

    /**
     * Deletes wastebasket items by reference.
     * @param reference
     */
    void cleanup(Long reference);

    /**
     * Removes an action from the wastebasket
     * @param id
     * @return template id of removed wastebasket entry
     */
    Long remove(Long id);

    /**
     * Provides all fcs items of given reference whose where thrown to wastebasket
     * @param referenceId
     * @return list of waste items
     */
    List<FcsItem> getWasteItems(Long referenceId);
}
