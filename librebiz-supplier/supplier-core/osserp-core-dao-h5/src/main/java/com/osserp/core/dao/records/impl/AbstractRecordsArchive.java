/**
 *
 * Copyright (C) 2007 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 25 Mar 2007 10:29:46 
 * 
 */
package com.osserp.core.dao.records.impl;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.dao.Tables;
import com.osserp.common.dms.dao.DocumentEncoder;
import com.osserp.common.dms.dao.impl.DatabaseDocumentsDao;
import com.osserp.common.service.SecurityService;

import com.osserp.core.dao.records.RecordBinaries;
import com.osserp.core.finance.Record;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractRecordsArchive extends DatabaseDocumentsDao
        implements RecordBinaries {

    /**
     * Constructor for record archive daos
     * @param jdbcTemplate for document objects
     * @param tables
     * @param hibernateTemplete
     * @param documentTypeId
     * @param documentTableKey for document table
     * @param categoryTableKey an optional document categories table
     * @param binaryTableKey for image table
     * @param documentEncoder
     * @param authenticationProvider
     */
    public AbstractRecordsArchive(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            Long documentTypeId,
            String documentTableKey,
            String categoryTableKey,
            String binaryTableKey,
            DocumentEncoder documentEncoder, 
            SecurityService authenticationProvider) {
        super(
                jdbcTemplate,
                tables,
                sessionFactory,
                documentTypeId,
                documentTableKey,
                categoryTableKey,
                jdbcTemplate,
                binaryTableKey,
                documentEncoder, 
                authenticationProvider);
    }

    public String createFileName(Record record) {
        // handle id for canceled record source document 
        Long id = record.getId() < 0 ? record.getId() * (-1) : record.getId(); 
        StringBuilder buffer = new StringBuilder(id.toString());
        buffer.append("X").append(System.currentTimeMillis()).append(".pdf");
        return buffer.toString();
    }
}
