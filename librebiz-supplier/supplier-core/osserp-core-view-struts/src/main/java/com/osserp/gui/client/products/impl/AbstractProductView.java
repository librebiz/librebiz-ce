/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Created on Jan 24, 2008 6:28:32 PM 
 * 
 */
package com.osserp.gui.client.products.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.osserp.common.ClientException;
import com.osserp.common.PermissionException;
import com.osserp.common.dms.DmsManager;
import com.osserp.common.util.CollectionUtil;

import com.osserp.core.products.ProductCategory;
import com.osserp.core.products.ProductCategoryManager;
import com.osserp.core.products.ProductGroup;
import com.osserp.core.products.ProductGroupManager;
import com.osserp.core.products.ProductManager;
import com.osserp.core.products.ProductType;
import com.osserp.core.products.ProductTypeManager;

import com.osserp.gui.client.LazyIgnoringView;

/**
 * 
 * @author rk <rk@osserp.com>
 * @author cf <cf@osserp.com>
 */
public class AbstractProductView extends AbstractStockSelectingView implements LazyIgnoringView {
    private List<ProductCategory> productCategories = new ArrayList<ProductCategory>();
    private List<ProductGroup> productGroups = new ArrayList<ProductGroup>();
    private List<ProductType> productTypes = new ArrayList<ProductType>();
    private boolean ignoreLazy = false;

    /**
     * Initializes view name if annotated, loads available stocks and preselects selected stock with default stock. View then initializes available product
     * groups and action target is initialized as 'success'.
     */
    protected AbstractProductView() {
        super();
    }

    @Override
    public void init(HttpServletRequest request) throws ClientException, PermissionException {
        super.init(request);
        productCategories = fetchCategories();
        productGroups = fetchGroups();
        productTypes = fetchTypes();
    }

    public List<ProductCategory> getProductCategories() {
        return productCategories;
    }

    protected void setProductCategories(List<ProductCategory> productCategories) {
        this.productCategories = productCategories;
    }

    public List<ProductGroup> getProductGroups() {
        return productGroups;
    }

    protected void setProductGroups(List<ProductGroup> productGroups) {
        this.productGroups = productGroups;
    }

    public List<ProductType> getProductTypes() {
        return productTypes;
    }

    protected void setProductTypes(List<ProductType> productTypes) {
        this.productTypes = productTypes;
    }

    public boolean isIgnoreLazy() {
        return ignoreLazy;
    }

    public void setIgnoreLazy(boolean ignoreLazy) {
        this.ignoreLazy = ignoreLazy;
    }

    @SuppressWarnings("unchecked")
    protected List<ProductCategory> fetchCategories() {
        return new ArrayList(getProductCategoryManager().getList());
    }

    protected ProductCategory fetchCategory(Long id) {
        return (ProductCategory) CollectionUtil.getById(productCategories, id);
    }

    @SuppressWarnings("unchecked")
    protected List<ProductGroup> fetchGroups() {
        return new ArrayList(getProductGroupManager().getList());
    }

    protected ProductGroup fetchGroup(Long id) {
        return (ProductGroup) CollectionUtil.getById(productGroups, id);
    }

    @SuppressWarnings("unchecked")
    protected List<ProductType> fetchTypes() {
        return new ArrayList(getProductTypeManager().getList());
    }

    protected ProductType fetchType(Long id) {
        return (ProductType) CollectionUtil.getById(productTypes, id);
    }

    /* managers */

    protected ProductManager getProductManager() {
        return (ProductManager) getService(ProductManager.class.getName());
    }

    protected ProductCategoryManager getProductCategoryManager() {
        return (ProductCategoryManager) getService(ProductCategoryManager.class.getName());
    }

    protected ProductGroupManager getProductGroupManager() {
        return (ProductGroupManager) getService(ProductGroupManager.class.getName());
    }

    protected DmsManager getDmsManager() {
        return (DmsManager) getService(DmsManager.class.getName());
    }

    protected ProductTypeManager getProductTypeManager() {
        return (ProductTypeManager) getService(ProductTypeManager.class.getName());
    }

}
