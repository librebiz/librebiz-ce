/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 22-Oct-2005 23:26:41 
 * 
 */
package com.osserp.core.model.events;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.beans.OptionImpl;
import com.osserp.core.contacts.Contact;
import com.osserp.core.events.Recipient;
import com.osserp.core.events.RecipientPool;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class RecipientPoolImpl extends OptionImpl implements RecipientPool {

    private List<Recipient> members = new ArrayList<Recipient>();

    protected RecipientPoolImpl() {
        super();
    }

    public RecipientPoolImpl(String name) {
        super(null, name);

    }

    public RecipientPoolImpl(Long id, String name, List<Recipient> members) {
        super(id, name);
        if (members != null) {
            this.members = members;
        }
    }

    public List<Recipient> getMembers() {
        return members;
    }

    protected void setMembers(List<Recipient> members) {
        this.members = members;
    }

    public void addMember(Recipient member) throws ClientException {
        for (int i = 0, j = members.size(); i < j; i++) {
            Recipient existing = members.get(i);
            if (existing.getRecipientId() != null) {
                if (existing.getRecipientId().equals(member.getRecipientId())) {
                    throw new ClientException(ErrorCode.VALUES_DUPLICATE);
                }
            }
        }
        this.members.add(member);
    }

    public void addEmployee(Long id) throws ClientException {
        addMember(new RecipientImpl(getId(), id));
    }

    public void removeEmployee(Long id) {
        for (Iterator i = members.iterator(); i.hasNext();) {
            Recipient rcp = (Recipient) i.next();
            if (rcp.getType().equals(Contact.EMPLOYEE)
                    && id.equals(rcp.getRecipientId())) {
                i.remove();
                break;
            }
        }
    }
}
