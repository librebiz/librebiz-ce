/**
 *
 * Copyright (C) 2009 The original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package com.osserp.gui.telephones

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.osserp.common.ClientException
import com.osserp.common.Constants
import com.osserp.common.ErrorCode

import com.osserp.core.telephone.TelephoneManager
import com.osserp.core.telephone.TelephoneSystem
import com.osserp.core.telephone.TelephoneSystemManager

import com.osserp.gui.CoreView

/**
 * 
 * @author so <so@osserp.com>
 * @author rk <rk@osserp.com>
 * 
 */
public class TelephoneCreateView extends CoreView {
    private static Logger logger = LoggerFactory.getLogger(TelephoneCreateView.class.getName())

    private static final String TELEPHONE_CREATE_PERMISSIONS = 'telephone_system_admin'
    String macAddress
    Long system
    Long type
    List<TelephoneSystem> telephoneSystems = []

    TelephoneCreateView() {
        enablePopupView()
        dependencies = ['telephoneView']
        headerName = 'createNewTelephone'
    }

    @Override
    void initRequest(Map atts, Map headers, Map params) {
        super.initRequest(atts, headers, params)
        if (forwardRequest) {
            if (!telephoneSystems) {
                TelephoneSystemManager manager = getService(TelephoneSystemManager.class.getName())
                telephoneSystems = manager.getAll()
            }
        }
    }

    @Override
    void save() {
        bean = null
        this.checkPermission(TELEPHONE_CREATE_PERMISSIONS)
        macAddress = form.getString('macAddress')
        system = form.getLong('system')
        type = form.getLong('type')
        if (!macAddress || !macAddress.matches(Constants.MACADDRESS_REGEX)) {
            throw new ClientException(ErrorCode.INVALID_MACADDRESS)
        }
        TelephoneManager tm = getService(TelephoneManager.class.getName())
        try {
            bean = tm.create(macAddress, system, type)
        } catch (ClientException t) {
            throw new ClientException(t.message)
        }
        if (bean) {
            env?.telephoneView?.reload()
            disableCreateMode()
            headerName = 'phone'
        }
    }
}
