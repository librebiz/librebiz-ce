/**
 *
 * Copyright (C) 2012 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Jun 25, 2012 01:05:34 PM 
 * 
 */
package com.osserp.core.service.calc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osserp.core.BusinessCase;
import com.osserp.core.calc.Calculation;
import com.osserp.core.calc.CalculationByTemplateConfig;
import com.osserp.core.calc.CalculationByTemplateCreator;
import com.osserp.core.calc.CalculationTemplate;
import com.osserp.core.dao.CalculationTemplates;
import com.osserp.core.dao.Calculations;
import com.osserp.core.finance.Offer;
import com.osserp.core.requests.Request;
import com.osserp.core.sales.SalesOfferManager;

/**
 * 
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public abstract class AbstractCalculationByTemplateCreator implements CalculationByTemplateCreator {
    private static Logger log = LoggerFactory.getLogger(AbstractCalculationByTemplateCreator.class.getName());

    private Calculations calculations;
    private CalculationTemplates calculationTemplates;
    private SalesOfferManager salesOfferManager;

    public AbstractCalculationByTemplateCreator(
            Calculations calculations,
            CalculationTemplates calculationTemplates,
            SalesOfferManager salesOfferManager) {
        super();
        this.calculations = calculations;
        this.calculationTemplates = calculationTemplates;
        this.salesOfferManager = salesOfferManager;
    }

    public final void execute(BusinessCase businessCase) {
        if (businessCase.getType().getCalculationByEventConfig() != null) {
            CalculationByTemplateConfig config = calculationTemplates
                    .loadCalculationByTemplateConfig(businessCase.getType().getCalculationByEventConfig());
            if (config != null && !config.getTemplates().isEmpty()) {
                if (log.isDebugEnabled()) {
                    log.debug("execute() found config with configured templates [config="
                            + config.getId() + "templateCount=" + config.getTemplates().size() + "]");
                }
                for (int i = 0, j = config.getTemplates().size(); i < j; i++) {
                    CalculationTemplate template = config.getTemplates().get(i);
                    Calculation calculation = createCalculation(businessCase, template);

                    if (calculation != null && config.isCreateRecord()) {

                        if (!businessCase.isSalesContext()) {
                            try {
                                Offer offer = salesOfferManager.create(null, (Request) businessCase, calculation);
                                if (log.isDebugEnabled()) {
                                    log.debug("execute() offer created [id=" + offer.getId() + "]");
                                }
                            } catch (Exception e) {

                            }
                        } else {
                            // implement service method for sales order creation
                        }
                    }
                }
            }
        }
    }

    protected abstract Calculation createCalculation(BusinessCase businessCase, CalculationTemplate template);

    protected Calculations getCalculations() {
        return calculations;
    }

    protected CalculationTemplates getCalculationTemplates() {
        return calculationTemplates;
    }

    protected SalesOfferManager getSalesOfferManager() {
        return salesOfferManager;
    }
}
