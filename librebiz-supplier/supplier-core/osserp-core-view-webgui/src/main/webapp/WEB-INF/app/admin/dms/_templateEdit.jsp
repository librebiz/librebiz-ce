<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:set var="view" value="${requestScope.templateView}"/>
<c:set var="dmsUrl" value="${requestScope.templateDocumentUrl}"/>

<v:form url="${dmsUrl}/save" multipart="true">
<div class="col-md-6 panel-area panel-area-default">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 style="text-transform: none;"><o:out value="${view.bean.fileName}"/></h4>
        </div>
    </div>
    <div class="panel-body">
        <div class="form-body">

            <c:import url="_uploadWarnings.jsp"/>

            <c:choose>
                <c:when test="${view.customTemplate}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="name"><fmt:message key="name" /></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <o:out value="${view.bean.template.name}"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="description"><fmt:message key="description" /></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <v:textarea styleClass="form-control" name="note" value="${view.bean.template.description}"/>
                            </div>
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="description"><fmt:message key="description" /></label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <v:textarea styleClass="form-control" name="note" value="${view.bean.note}"/>
                            </div>
                        </div>
                    </div>
                </c:otherwise>
            </c:choose>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="fileUpload"><fmt:message key="templateUploadFileLabel"/></label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <o:fileUpload />
                    </div>
                </div>
            </div>

            <div class="row next">
                <div class="col-md-4"> </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <v:submitExit url="${dmsUrl}/select"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <o:submit value="save" styleClass="form-control" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
</v:form>
