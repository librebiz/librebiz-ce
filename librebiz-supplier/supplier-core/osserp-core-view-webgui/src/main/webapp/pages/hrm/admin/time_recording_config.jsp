<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-view" prefix="v"%>

<c:if test="${empty sessionScope.timeRecordingConfigView}">
    <c:redirect url="/errors/error_context.jsp" />
</c:if>
<c:set var="configView" scope="request" value="${sessionScope.timeRecordingConfigView}" />
<c:set var="config" scope="request" value="${configView.bean}" />

<tiles:insert page="/${applicationScope.webConfig.layoutDefaultPath}/main.jsp" flush="false">
    <tiles:put name="title">
        <o:displayTitle />
    </tiles:put>

    <tiles:put name="headline">
        <fmt:message key="timeRecordingConfigurationHeader" />
    </tiles:put>

    <tiles:put name="headline_right">
        <ul>
            <c:choose>
                <c:when test="${configView.editMode}">
                    <li><a href="<c:url value="/timeRecordingConfig.do?method=disableEdit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon" /></a></li>
                </c:when>
                <c:when test="${configView.createMode}">
                    <li><a href="<c:url value="/timeRecordingConfig.do?method=disableCreate"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon" /></a></li>
                </c:when>
                <c:when test="${!empty config}">
                    <li><a href="<c:url value="/timeRecordingConfig.do?method=select"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon" /></a></li>
                    <li><a href="<c:url value="/timeRecordingConfig.do?method=enableEdit"/>" title="<fmt:message key="change"/>"><o:img name="writeIcon" /></a></li>
                </c:when>
                <c:otherwise>
                    <li><a href="<c:url value="/timeRecordingConfig.do?method=exit"/>" title="<fmt:message key="backToLast"/>"><o:img name="backIcon" /></a></li>
                    <li><a href="<c:url value="/timeRecordingConfig.do?method=enableCreate"/>" title="<fmt:message key="createNewProfile"/>"><o:img name="newdataIcon" /></a></li>
                    <li><a href="<c:url value="/timeRecordConfig.do?method=forward"/>" title="<fmt:message key="configureBookingTypes"/>"><o:img name="configureIcon" /></a></li>
                    <li><a href="<c:url value="/timeRecordingQuery.do?method=forward"/>" title="<fmt:message key="timeRecordingQuery"/>"><o:img name="searchIcon" /></a></li>
                </c:otherwise>
            </c:choose>
            <li>
                <v:link url="/admin" title="backToMenu">
                    <o:img name="homeIcon" />
                </v:link>
            </li>
        </ul>
    </tiles:put>

    <tiles:put name="content" type="string">
        <div class="content-area" id="timeRecordingConfigContent">
            <div class="row">
                <c:choose>
                    <c:when test="${configView.createMode}">
                        <c:import url="/pages/hrm/admin/_timeRecordingConfigCreate.jsp"/>
                    </c:when>
                    <c:when test="${configView.editMode}">
                        <c:import url="/pages/hrm/admin/_timeRecordingConfigEdit.jsp"/>
                    </c:when>
                    <c:when test="${!empty config}">
                        <c:set var="timeRecordingConfig" scope="request" value="${config}" />
                        <c:import url="/pages/hrm/shared/time_recording_config_display.jsp" />
                    </c:when>
                    <c:otherwise>
                        <c:import url="/pages/hrm/admin/_timeRecordingConfigList.jsp"/>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </tiles:put>
</tiles:insert>
