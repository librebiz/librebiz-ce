/**
 *
 * Copyright (C) 2008 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on Dec 6, 2008 8:51:58 AM 
 * 
 */
package com.osserp.core.dao.hrm.impl;

import java.util.Date;

import org.springframework.jdbc.core.JdbcTemplate;
import org.hibernate.SessionFactory;

import com.osserp.common.ClientException;
import com.osserp.common.ErrorCode;
import com.osserp.common.FileObject;
import com.osserp.common.User;
import com.osserp.common.dao.Tables;
import com.osserp.common.dms.DmsDocument;
import com.osserp.common.dms.DmsReference;
import com.osserp.common.dms.DocumentType;
import com.osserp.common.dms.dao.DocumentEncoder;
import com.osserp.common.dms.dao.impl.DatabaseDocumentsDao;
import com.osserp.common.service.SecurityService;
import com.osserp.common.util.DateFormatter;

import com.osserp.core.dao.hrm.TimeRecordingDocuments;
import com.osserp.core.hrm.TimeRecording;
import com.osserp.core.hrm.TimeRecordingMonth;
import com.osserp.core.hrm.TimeRecordingPeriod;
import com.osserp.core.hrm.TimeRecordingYear;
import com.osserp.core.model.hrm.TimeRecordingDocumentImpl;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public class TimeRecordingDocumentsDao extends DatabaseDocumentsDao implements TimeRecordingDocuments {

    public TimeRecordingDocumentsDao(
            JdbcTemplate jdbcTemplate,
            Tables tables,
            SessionFactory sessionFactory,
            Long documentTypeId,
            String documentTableKey,
            JdbcTemplate imageTemplate,
            String documentBinaryTableKey,
            DocumentEncoder documentEncoder, 
            SecurityService authenticationProvider) {
        super(
                jdbcTemplate,
                tables,
                sessionFactory,
                documentTypeId,
                documentTableKey,
                null, // categoryTableKey not supported by record type
                imageTemplate,
                documentBinaryTableKey,
                documentEncoder, 
                authenticationProvider);
    }

    public DmsDocument create(User user, TimeRecording recording, byte[] file)
            throws ClientException {
        TimeRecordingPeriod period = recording.getSelectedPeriod();
        if (period == null) {
            throw new ClientException(ErrorCode.PROFILE_SELECTION_MISSING);
        }
        TimeRecordingYear year = period.getSelectedYear();
        if (year == null) {
            throw new ClientException(ErrorCode.YEAR_SELECTION_MISSING);
        }
        TimeRecordingMonth month = year.getSelectedMonth();
        if (month == null) {
            throw new ClientException(ErrorCode.MONTH_SELECTION_MISSING);
        }
        DocumentType documentType = getDocumentType();
        StringBuilder fileName = new StringBuilder(96);
        fileName
                .append(recording.getReference())
                .append("-")
                .append(DateFormatter.getFormatted(month.getYear()))
                .append("-")
                .append(DateFormatter.getFormatted(month.getMonth()))
                .append(".")
                .append(documentType.getSuffix());
        DmsReference reference = new DmsReference(period, null);
        FileObject fileObject = new FileObject(fileName.toString(), file);
        DmsDocument doc = create(
                user,
                reference,
                fileObject,
                null,
                null,
                null,
                null,
                null,
                null);
        return doc;
    }

    @Override
    protected DmsDocument createObject(
            User user, 
            DocumentType documentType, 
            DmsReference reference, 
            String fileName, 
            long fileSize, 
            String note, 
            Date sourceDate, 
            Long categoryId, 
            Date validFrom, 
            Date validTil,
            String messageId) {
        return new TimeRecordingDocumentImpl(user, (TimeRecordingPeriod) reference.getReference(), documentType, fileName, fileSize);
    }

    @Override
    protected Class<? extends DmsDocument> getPersistentClass() {
        return TimeRecordingDocumentImpl.class;
    }
}
