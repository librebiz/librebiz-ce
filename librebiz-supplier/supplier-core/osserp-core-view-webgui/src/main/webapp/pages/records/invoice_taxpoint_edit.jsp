<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<c:set var="view" value="${sessionScope.recordView}"/>
<c:set var="record" value="${view.record}"/>
<tr>
    <td><fmt:message key="taxPoint"/></td>
    <td>
       <input type="text" name="taxPoint" id="taxPoint" style="width: 125px;" value="<o:date value="${record.taxPoint}"/>"/>
       <o:datepicker input="taxPoint"/>
    </td>
</tr>
<tr>
    <td><fmt:message key="timeOfSupply"/></td>
    <td>
       <input type="text" name="timeOfSupply" id="timeOfSupply" class="recordInput" value="<o:out value="${record.timeOfSupply}"/>"/>
    </td>
</tr>
<tr>
    <td><fmt:message key="placeOfPerformance"/></td>
    <td>
       <input type="text" name="placeOfPerformance" id="placeOfPerformance" class="recordInput" value="<o:out value="${record.placeOfPerformance}"/>"/>
    </td>
</tr>
