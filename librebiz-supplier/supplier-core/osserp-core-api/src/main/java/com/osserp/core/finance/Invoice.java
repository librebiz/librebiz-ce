/**
 *
 * Copyright (C) 2005 The original author or authors.
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Affero General Public License (AGPL) 
 * version 3 as published by the Free Software Foundation. In accordance 
 * with Section 7(a) of the GNU AGPL its Section 15 shall be amended to 
 * the effect that the original authors expressly exclude the warranty of
 * non-infringement of any third-party rights. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU AGPL
 * at http://www.gnu.org/licenses/agpl-3.0.html for more details.
 *
 * The interactive user interfaces in modified source and object code 
 * versions must display Appropriate Legal Notices, as required under 
 * Section 5 of the GNU AGPL version 3. Pursuant to Section 7 § 3(b) of 
 * the GNU AGPL you must include a clickable link "Powered by osserp.com"
 * that leads directly to the URL http://osserp.com in the footer area 
 * or, if not reasonably feasible for technical reason, as a top-level 
 * link of the primary navigation of the graphical user interface in
 * every copy of the program you distribute. 
 * 
 * Created on 21-Jun-2005 20:08:34 
 * 
 */
package com.osserp.core.finance;

import java.util.Date;

/**
 * 
 * @author Rainer Kirchner <rk@osserp.com>
 * 
 */
public interface Invoice extends PaymentAwareRecord {

    static final Long DOWNPAYMENT = 1L;
    static final Long DELIVERY = 2L;
    static final Long FINAL = 3L;
    static final Long CUSTOM = 4L;
    static final Long PARTIAL = 5L;
    static final Long PARTIAL_DOWNPAYMENT = 7L;
    static final Long CUSTOM_DOWNPAYMENT = 6L;

    static final Long SERVICE_PURCHASE = 21L;
    static final Long PURCHASE = 22L;

    static final int DEFAULT_MATURITY = 7;

    /**
     * Indicates whether this invoice is a downpayment
     * @return downpayment
     */
    boolean isDownpayment();

    /**
     * Indicates that invoice is a partial invoice
     * @return partial
     */
    boolean isPartial();

    /**
     * The maturity date of the invoice
     * @return maturity
     */
    Date getMaturity();

    /**
     * This method is not used cause date is set at creation time
     * @param maturity
     */
    void setMaturity(Date maturity);

	/**
	 * Provides a string with a time of supply information. This value may override
	 * a taxPoint setting in document output and is used if setting a taxPoint only  
	 * does not describe the time of supply completely (e.g. if time of supply took
	 * a whole month).
	 * @return timeOfSupply
	 */
	String getTimeOfSupply();

	/**
	 * Sets a string with a time of supply information. This value may override
	 * a taxPoint setting in document output.
	 * @return timeOfSupply
	 */
	void setTimeOfSupply(String timeOfSupply);

	/**
	 * Provides the place of performance
	 * @return place of performance
	 */
    String getPlaceOfPerformance();

    /**
     * Sets the place of performance
     * @param placeOfPerformance
     */
    void setPlaceOfPerformance(String placeOfPerformance);

    /**
     * Provides the payment condition of this invoice
     * @return paymentCondition
     */
    PaymentCondition getPaymentCondition();

    /**
     * Sets the payment condition
     * @param paymentCondition
     */
    void setPaymentCondition(PaymentCondition paymentCondition);

    /**
     * Provides the netPaidAmount if rounding errors occured with manual correction required
     * @return overrideNetPaidAmount
     */
    Double getOverrideNetPaidAmount();

    /**
     * Provides the taxPaidAmount if rounding errors occured with manual correction required
     * @return overrideTaxPaidAmount
     */
    Double getOverrideTaxPaidAmount();

    /**
     * Provides the grossPaidAmount if rounding errors occured with manual correction required
     * @return overrideGrossPaidAmount
     */
    Double getOverrideGrossPaidAmount();

    /**
     * Indicates that paid amounts should be overriden in output document
     * @return overridePaidAmount
     */
    boolean isOverridePaidAmount();

    /**
     * Override paid amounts if rounding errors occure on invoices with many downpayments. Method is required because plant price is caluclated by numerics
     * with 3 digits instead of 2 as defined for common amounts.
     * @param overrideNetPaidAmount
     * @param overrideTaxPaidAmount
     * @param overrideGrossPaidAmount
     */
    void overridePaidAmounts(
            Double overrideNetPaidAmount,
            Double overrideTaxPaidAmount,
            Double overrideGrossPaidAmount);

    /**
     * Indicates if taxPoint date should be printed
     * @return printTaxPoint
     */
    boolean isPrintTaxPoint();

    /**
     * Enables/disables taxPoint date print flag
     * @param printTaxPoint
     */
	void setPrintTaxPoint(boolean printTaxPoint);

    /**
     * Indicates if taxPoint information should be printed for each item
     * @return printTaxPointByItems
     */
	boolean isPrintTaxPointByItems();

    /**
     * Enables/disables taxPoint information print for each item
     * @param printTaxPointByItems
     */
	void setPrintTaxPointByItems(boolean printTaxPointByItems);

	/**
	 * Indicates if print of timeOfSupply informations is enabled.
	 * Enabling this mostly overrides taxPoint date with timeOfSupply value.
	 * @return printTimeOfSupply
	 */
	boolean isPrintTimeOfSupply();

	/**
	 * Enables/disables print of timeOfSupply information.
	 * @param printTimeOfSupply
	 */
	void setPrintTimeOfSupply(boolean printTimeOfSupply);
    
    /**
     * Indicates if place of performance should be print.
     * @return  true if print enabled
     */
    boolean isPrintPlaceOfPerformance();

    /**
     * Enables/disables place of performance print option
     * @param printPlaceOfPerformance
     */
    void setPrintPlaceOfPerformance(boolean printPlaceOfPerformance);
}
