<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o" %>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp-core" prefix="oc" %>
<footer class="footer">
    <div class="container">
        <p class="text-muted" style="text-align: center;">
            <span>&copy; <a href="<c:url value="/license"/>"><o:date format="psyear"/>-<o:date format="year"/></a></span>
            <span style="margin-left: 2px;"><o:displayCopyright/></span>
            <span>-</span><span><o:displayRevision/></span>
            <span>-</span><span>Powered by <a href="${applicationScope.webConfig.distributionPoweredByUrl}" title="${applicationScope.webConfig.distributionPoweredByText}" target="_blank"><o:out value="${applicationScope.webConfig.distributionPoweredByLabel}"/></a></span>
            <oc:systemPropertyEnabled name="footerDisplayUserEnabled">
            <c:if test="${!empty sessionScope.user.loginName}">
                <span>-</span><span><fmt:message key="currentlyLoggedInAs"/>
                <c:choose>
                <c:when test="${!empty sessionScope.portalView and sessionScope.portalView.systemSetupOnly}">
                    <o:out value="${sessionScope.user.loginName}"/>
                </c:when>
                <c:otherwise>
                    <a href="<c:url value="/contacts.do?method=load&id=${sessionScope.user.contactId}&exit=index"/>">
                        <o:out value="${sessionScope.user.loginName}"/>
                    </a>
                </c:otherwise>
                </c:choose>
                <fmt:message key="since"/> <o:time value="${user.loginTime}"/></span>
            </c:if>
            </oc:systemPropertyEnabled>
        </p>
    </div>
</footer>
<o:removeErrors/>
