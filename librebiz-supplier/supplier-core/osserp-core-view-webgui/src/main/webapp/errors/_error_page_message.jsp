<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://osserp.org/jsp/tag/osserp" prefix="o"%>

<c:set var="errorPageDefaultHeader" value="${requestScope.errorPageDefaultHeader}"/>
<c:set var="errorPageDefaultMessage" value="${requestScope.errorPageDefaultMessage}"/>

<c:if test="${!empty errorPageDefaultHeader}">
    <h4><fmt:message key="${errorPageDefaultHeader}" /></h4>
    <c:choose>
        <c:when test="${empty sessionScope.errorSource}">
            <div class="spacer"></div>
            <p><fmt:message key="${errorPageDefaultMessage}" /></p>
        </c:when>
        <c:otherwise>
            <div class="spacer"></div>
            <p><fmt:message key="errorMessage" />: 
                <span class="error"><fmt:message key="${sessionScope.errorSource.message}" /></span>
            </p>
            <c:if test="${!empty sessionScope.errorSource.cause.message 
                            and sessionScope.errorSource.message != sessionScope.errorSource.cause.message}">
                <div class="spacer"></div>
                <p><fmt:message key="systemMessage" />: 
                    <span><o:out value="${sessionScope.errorSource.cause.message}"/></span>
                </p>
            </c:if>
            <c:remove var="errorSource" scope="session"/>
        </c:otherwise>
    </c:choose>
    <div class="spacer"></div>
    <p><fmt:message key="error.informAdministratorOnRepeatedError" /> Email: <o:emailToSupport /></p>
</c:if>
